using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace lntechNew.Reports
{
	
	public partial class frmUpdate : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btn_submit;
		clsENationWebComponents clsdb = new clsENationWebComponents();
		clsSession cSession	 =new clsSession();
		clsCourts ClsCourts= new clsCourts();
		clsLogger bugTracker = new clsLogger();
		clsCaseStatus ClsCaseStatus = new clsCaseStatus();
		int docid;
		int empid;
		protected System.Web.UI.WebControls.TextBox txt_TicketNo;
		protected System.Web.UI.WebControls.TextBox txt_SequenceNo;
		protected System.Web.UI.WebControls.DropDownList ddl_Status;
		protected System.Web.UI.WebControls.DropDownList ddl_Time;
		protected System.Web.UI.WebControls.DropDownList ddl_Date_Year;
		protected System.Web.UI.WebControls.DropDownList ddl_Date_Day;
		protected System.Web.UI.WebControls.DropDownList ddl_Date_Month;
		protected System.Web.UI.WebControls.TextBox txt_courtno;
		protected System.Web.UI.WebControls.Label lbl_courtname;
		protected System.Web.UI.WebControls.Label lbl_message;
		protected System.Web.UI.WebControls.Label lbl_courtid;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.TextBox txt_CauseNumber;
		int docnum;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (cSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx");
			}
			docid =Convert.ToInt32(Request.QueryString["docid"]);			
			docnum=Convert.ToInt32(Request.QueryString["docnnum"]);
			empid=Convert.ToInt32(cSession.GetCookie("sEmpID",this.Request));
			//empid=3992;
			//Adding Attribute For Save button
			btn_submit.Attributes.Add("onclick","return Validate();");
			lbl_message.Text="";
			if(!IsPostBack)
			{
				//Violation Status
				DataSet ds_status=ClsCaseStatus.GetAllCourtCaseStatus();
				ddl_Status.DataSource=ds_status;
				ddl_Status.DataTextField=ds_status.Tables[0].Columns[1].ColumnName;
				ddl_Status.DataValueField=ds_status.Tables[0].Columns[0].ColumnName;
				ddl_Status.DataBind();					
				GetInfo();				
			}

		}
		//GetInfo Based on docID,Docnum in order to populate controls 		
		private void GetInfo()
		{   
            //Change by Ajmal
            //SqlDataReader dr_popup = null;
			IDataReader dr_popup = null;
			try{
				clsGeneralMethods cgMethods = new clsGeneralMethods();				
				string[] key={"@DocID","@DocNum"};
				object[] value1={docid,docnum};
				dr_popup=clsdb.Get_DR_BySPArr("usp_hts_get_ScanDoc",key,value1); 				
				if ( dr_popup.Read() )
				{
					txt_TicketNo.Text=dr_popup["ticketnumber"].ToString();
					txt_SequenceNo.Text=dr_popup["seqnum"].ToString();
					txt_CauseNumber.Text = dr_popup["causenumber"].ToString();
					txt_courtno.Text=dr_popup["courtno"].ToString();
					lbl_courtname.Text=dr_popup["courtname"].ToString();
					lbl_courtid.Text=dr_popup["courtid"].ToString();
					ddl_Status.SelectedValue=ddl_Status.Items.FindByText(dr_popup["description"].ToString()).Value;
					DateTime dt = new DateTime();
					if ( dr_popup["courtdate"] != DBNull.Value )
						dt = Convert.ToDateTime(dr_popup["courtdate"]);

					// DO NOT SET COURT DATE & TIME COMBOs IF COURT DATE IS 01/01/1900........
					if (dt.ToShortDateString() != "1/1/1900" && dt.ToShortDateString() != "1/1/0001" )
					{
						try
						{
							ddl_Date_Month.SelectedValue=cgMethods.ParseDate("M",dt);				
						}
						catch{}

						try
						{
							ddl_Date_Day.SelectedValue=cgMethods.ParseDate("D",dt);
						}
						catch{}

						try
						{
							ddl_Date_Year.SelectedValue=cgMethods.ParseDate("Y",dt);
						}
						catch{}
						try
						{
							ddl_Time.SelectedValue=dt.ToShortTimeString();				
						}
						catch{}
					}
				}
				
			}
			catch(Exception ex)	{
				lbl_message.Text= ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		

			finally{ 
				dr_popup.Close(); 
			}
		}
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			//this.ID = "frmUpdate";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		//In order to save the information		
		private void btn_submit_Click(object sender, System.EventArgs e)
		{		
			try
			{			
				string date=ddl_Date_Month.SelectedValue+"/"+ddl_Date_Day.SelectedValue+"/"+ddl_Date_Year.SelectedValue+" "+ddl_Time.SelectedValue;
				DateTime dt =Convert.ToDateTime(date);	
				bool IsUpdated=false;
				
				string [] key1 =    {"@refcasenumber", "@sequencenumber", "@Causenumber", "@courtnumber", "@courtdate", "@courtviolationstatusid", "@docid", "@docnum", "@employeeid"} ;
				object [] value1 =  {txt_TicketNo.Text.Trim(), txt_SequenceNo.Text.Trim(), txt_CauseNumber.Text.Trim(),  txt_courtno.Text.Trim(), dt, Convert.ToInt32(ddl_Status.SelectedValue), docid, docnum, Convert.ToInt32(cSession.GetCookie("sEmpID",this.Request))} ;
				IsUpdated = ((bool) (Convert.ChangeType( clsdb.ExecuteScalerBySp  ( "USP_HTS_Update_ScannedReset",key1,value1),typeof(bool))))  ;						

				if (IsUpdated )
				{
					lbl_message.Text = "Record updated.";
					HttpContext.Current.Response.Write("<script language='javascript'> window.opener.document.Form1.btn_submit.click(); self.close();   </script>");
				}
				else
					lbl_message.Text = "Not Updated.";

			}
			catch(Exception ex)
			{
				lbl_message.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			//HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");	
			HttpContext.Current.Response.Write("<script language='javascript'> window.opener.document.Form1.btn_submit.click(); self.close();   </script>");
		}		
	}
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplaintComments.aspx.cs"
    Inherits="HTP.Reports.ComplaintComments" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Namespace="eWorld.UI" TagPrefix="ew" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="obout" Namespace="OboutInc.Flyout2" Assembly="obout_Flyout2_NET"%>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Complaint Comment Report</title>
    <link href="../Styles.css" type="text/css" rel="Stylesheet" />
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script src="../Scripts/boxover.js" type="text/javascript"></script>
    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>

     <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
   
    <script  type="text/javascript">
		
	    //to show commment on mouse hower
		function CommentPoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg()
		     
		}
		
		
		//change cursor icon
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
		function ShowMsg()
        {   
            document.getElementById("txt_CommentsMsg").value=err;
        }
        
        
      //check if update with out selection and list of comma seperate ticket ids
    function get_check_value()
        {
        debugger;                    
            var a = 0;        
                        
            var c_value = "";
            var grid = document.getElementById("<%= gv_Records.ClientID%>");
            var cell;
            if(grid != null && grid.rows.length > 0)
            {
            
                
               for(i = 2; i<= grid.rows.length;i++)
                {
                    if(i<10)
                    {
                    var chk = 'gv_Records_ctl0'+i.toString()+'_chkReview';
                    var hf='gv_Records_ctl0'+i.toString()+'_hf_NoteID';
                    }
                    else
                    {
                    var chk = 'gv_Records_ctl'+i.toString()+'_chkReview';
                    var hf='gv_Records_ctl'+i.toString()+'_hf_NoteID';
                    }
                    var chkbox=document.getElementById(chk);
                    if(chkbox != null && chkbox.checked)
                    {
                        a=1;
                       c_value = c_value + document.getElementById(hf).value + ",";
                    }
                    
                  }
               }
                                              
               if(a==0)
                    {
                   // alert("Please select a contact to associate.");
                   $("#txtErrorMessage").text("Please select a contact to associate.");
                   $("#errorAlert").modal();
                        return false;
                    }
               document.getElementById("hf_NoteIDs").value=c_value;
               
        }
        
        
        function CheckDateValidation()
        { 
            // abid ali 5387 1/15/2009 date comparision
            if (IsDatesEqualOrGrater(document.form1.cal_FromDateFilter.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
                // alert("Please enter valid date, Start Date must be grater then or equal to 1/1/1900");
                $("#txtErrorMessage").text("Please enter valid date, Start Date must be grater then or equal to 1/1/1900");
                $("#errorAlert").modal();
			    // Fahad 4354 1/16/2009 comment out
				//document.form1.cal_FromDateFilter.focus(); 
				return false;
			}
			
			if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    //alert("Please enter valid date, End Date must be greater than or equal Start Date 1/1/1900");
			    $("#txtErrorMessage").text("Please enter valid date, End Date must be greater than or equal Start Date 1/1/1900");
			    $("#errorAlert").modal();
				// Fahad 4354 1/16/2009 comment out
				//document.form1.cal_ToDateFilter.focus(); 
				return false;
			}
            if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value,'MM/dd/yyyy',document.form1.cal_FromDateFilter.value,'MM/dd/yyyy')==false)
            {
                // alert("Please enter valid date, End Date must be greater than or equal to Start Date");
                $("#txtErrorMessage").text("Please enter valid date, End Date must be greater than or equal to Start Date");
                $("#errorAlert").modal();
			    // Fahad 4354 1/16/2009 comment out
				//document.form1.cal_ToDateFilter.focus(); 
				return false;
			}
			else
			{
			    return true;
			}
        }
		
    </script>
    

</head>
<body>
 
    <form id="form1" runat="server">
    
    
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />

        <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="complaintCommentsPage" >
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                

                 </div>

                 <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>

                 </div>
                 </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Complaint Comment Report</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
            <div class="clearfix"></div>

                  <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Complaint Comment Report</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Start Date :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="cal_FromDateFilter" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                     <%--<ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">End Date :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="cal_ToDateFilter" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                     <%--<ew:CalendarPopup Visible="true" ID="cal_ToDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-4">
                                                            <div class="form-group showAllCheck">
                              <label class="form-label">&nbsp;</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:CheckBox ID="chkShowAll" class="checkbox-custom" Text="Show All" runat="server" />
                                    </div>
                                                                </div>
                         </div>

                   

                     <div class="clearfix"></div>

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Attorneys :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:DropDownList ID="ddlAttorneys" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Case Type :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:DropDownList ID="ddlCaseType" CssClass="form-control" DataMember="CaseTypeName"
                                    DataValueField="CaseTypeId" DataTextField="CaseTypeName" runat="server">
                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>
                     <div class="clearfix"></div>

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                       <asp:Button ID="btnSearch" Text="Search" OnClientClick="return CheckDateValidation();" OnClick="btnSearch_Click" CssClass="btn btn-primary"
                                    runat="server" />
									

									
                                    </div>
                                                                </div>
                         </div>

                    
                     

                    </div>
                     </div>
                                                               </section>

             <div class="clearfix"></div>

            <section class="box" id="TableGrid" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">LIST</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                          <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <uc3:pagingcontrol ID="Pagingctrl" runat="server" />
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                    
                </div>
            </header>

                 <div class="content-body">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="pull-right">
					 <asp:Button ID="btnUpdate" Text="Update"  CssClass="btn btn-primary"
runat="server" onclick="btnUpdate_Click" Visible="false" OnClientClick="return get_check_value()" />
</div>
				 </div>
				 </div>
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">Start Date :</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                    <ContentTemplate>
                                        <div class="table-responsive" data-pattern="priority-columns">
   <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-small-font table-bordered table-striped" AllowSorting="True" AllowPaging="True" PageSize="20"
                                            OnPageIndexChanging="gv_Records_PageIndexChanging" OnSorting="gv_Records_Sorting"
                                            OnRowDataBound="gv_Records_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Name" runat="server" CssClass="" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>' ></asp:Label>
                                                       <%--Asad Ali 7480 04/06/2010 using obout tool tip--%>
                                                        <obout:Flyout runat="server" ID="FlyoutToolTipFullName"  AttachTo="lbl_Name" Align="TOP" FadingEffect="true" Position="TOP_RIGHT" >
                                                               <asp:Panel ID="pnlTooltipFullName" runat="server" CssClass="QuickLinksFlyoutPopup">
                                                               </asp:Panel>                                                               
                                                            </obout:Flyout>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Crt Loc</u>" SortExpression="CourtLocation" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CrtLoc" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtLocation") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="Status" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Crt Date & Time</u>" SortExpression="CourtDateMain" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Courtdate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtdate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Attorney" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>C-Date</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CDate" runat="server" CssClass="form-label" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Complaint" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>   
                                                    <asp:Label ID="lbl_Complaint" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Complaint") %>' ></asp:Label>
                                                    <%--Asad Ali 7480 04/06/2010 using obout tool tip--%>
                                                           <obout:Flyout runat="server" ID="FlyoutToolTip"  AttachTo="lbl_Complaint" Align="TOP" FadingEffect="false" FlyingEffect="BOTTOM_LEFT" Position="TOP_LEFT" >
                                                               <asp:Panel ID="pnlTooltip" GroupingText="Complaint Comments" runat="server" CssClass="QuickLinksFlyoutPopup"  Height="210px" Width="425" ScrollBars="Auto" >
                                                               </asp:Panel>
	                                                        </obout:Flyout>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <%-- Abid Ali 5018 12/24/2008 Criminal Report Addition --%>
                                                <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                      <%--Asad Ali 7480 04/06/2010 using obout tool tip--%>
                                                          <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Comments") %>' />
                                                          <asp:HiddenField ID="hf_NoteID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.NotesID_PK") %>' />                                         
                                                          <asp:Image ID="imgComments" ImageUrl="~/Images/comments.png" runat="server" />
                                                          <obout:Flyout runat="server" ID="ToolTipComments"  AttachTo="imgComments" Align="TOP" FadingEffect="false" FlyingEffect="BOTTOM_LEFT" Position="TOP_LEFT" >
                                                               <asp:Panel ID="pnlTooltipComments" runat="server" CssClass="QuickLinksFlyoutPopup"  Height="190px" Width="420px" ScrollBars="Auto" >
                                                               </asp:Panel>
                                                            </obout:Flyout>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Review" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkReview"  runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"  />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" CssClass="paginationBox1" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="Last Page &gt;&gt;" />
                                        </asp:GridView>



                                            </div>
                                        </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <asp:HiddenField ID="hf_NoteIDs" runat="server" />
                                     
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                </section>









            </section>
                     </section>
            </div>
       
    
    </form>
    
      <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script language="JavaScript" type="text/javascript" src="../Scripts/wz_tooltip.js"></script>
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

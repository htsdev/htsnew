using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class ServiceTicketAlert : System.Web.UI.Page
    {
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        OpenServiceTickets GetAlert = new OpenServiceTickets();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Alert();
            }

        }
        private void Alert()
        {
            try
            {
                

                object[] value ={ Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)) };

                GetAlert.Values = value;

                DataSet ds = GetAlert.GetServiceTicketAlerts();
                if (ds.Tables[0].Rows.Count > 0)
                    HPPendingTask.Text = ds.Tables[0].Rows[0]["servicecount"].ToString();
                HPPendingTask.Attributes.Add("onClick", "return RedirectWindow('http://"+Request.ServerVariables["SERVER_NAME"]+"/Reports/OpenServiceTicketReport.aspx?fromPopup=1')");
                

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (chkShow.Checked)
                Update();
            else
                Response.Write("<script language='javascript'> self.close();</script>");
        }
        private void Update()
        {
            try
            {
                object[] value ={ Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)) };
                GetAlert.Values = value;
                GetAlert.Update();
                Response.Write("<script language='javascript'> self.close();</script>");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
           
        }
      
    }
}

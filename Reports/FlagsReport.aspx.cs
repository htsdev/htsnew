﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    //noufil 4213 06/16/2008 Code copied from quickentrynew/arrcombin and make this report as a separate report
    public partial class FlagsReport : System.Web.UI.Page
    {
        #region Variables
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        DataView dv;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Session["DV_flag"] = null;
                    FillDDLWithFlags();
                    ddl_Flags.SelectedIndex = 0;
                    FillGrid(Convert.ToInt32(ddl_Flags.Items[0].Value));
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }

        }

        protected void ddl_Flags_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid(Convert.ToInt32(ddl_Flags.SelectedValue));
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }
       
        protected void ASdg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    ((HyperLink)e.Item.FindControl("AScauseno")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        protected void dg_NotOnSystem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dg_NotOnSystem.PageIndex = e.NewPageIndex;
                FillGrid(Convert.ToInt32(ddl_Flags.SelectedValue));
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void dg_NotOnSystem_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region methods

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {
            
                dv = (DataView)Session["DV_flag"];
                dv.Sort = sortExpression + " " + direction;
                Session["DV_flag"] = dv;

                FillGrid(Convert.ToInt32(ddl_Flags.SelectedValue));
                Pagingctrl.PageCount = dg_NotOnSystem.PageCount;
                Pagingctrl.PageIndex = dg_NotOnSystem.PageIndex;

                ViewState["sortExpression"] = sortExpression;
                Session["SortDirection"] = direction;
                Session["SortExpression"] = sortExpression;
            
        }

        private void FillGrid(int FlagID)
        {
            string[] key = { "@FlagID" };
            object[] value = { FlagID };           
            DataTable dt_NOS = ClsDb.Get_DT_BySPArr("USP_HTS_NOTONSYSTEM_REPORT", key, value);
            
            if (dt_NOS.Rows.Count > 0)
            {
                dg_NotOnSystem.Visible = true;
                lbl_Message.Text = "";
                if (dt_NOS.Columns.Contains("SNO") == false)
                {
                    dt_NOS.Columns.Add("SNO");
                    int sno = 1;
                    if (dt_NOS.Rows.Count >= 1)
                        dt_NOS.Rows[0]["SNO"] = 1;
                    if (dt_NOS.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt_NOS.Rows.Count; i++)
                        {
                            if (dt_NOS.Rows[i - 1]["TicketID"].ToString() != dt_NOS.Rows[i]["TicketID"].ToString())
                            {
                                dt_NOS.Rows[i]["SNO"] = ++sno;
                            }
                        }
                    }
                }
            }

            if (Session["DV_flag"] == null)
            {
                dv = new DataView(dt_NOS);
                Session["DV_flag"] = dv;
            }
            else
            {
                dv = (DataView)Session["DV_flag"];
                string SortExp = dv.Sort;
                dv = new DataView(dt_NOS);
                dv.Sort = SortExp;
            }

            dg_NotOnSystem.DataSource = dv;
            dg_NotOnSystem.DataBind();
            Pagingctrl.PageCount = dg_NotOnSystem.PageCount;
            Pagingctrl.PageIndex = dg_NotOnSystem.PageIndex;
            Pagingctrl.SetPageIndex();
        }

        void Pagingctrl_PageIndexChanged()
        {
            dg_NotOnSystem.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid(Convert.ToInt32(ddl_Flags.SelectedValue));
        }

        private void FillDDLWithFlags()
        {

            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_GET_ALL_FLAGS");
            ddl_Flags.DataTextField = "Description";
            ddl_Flags.DataValueField = "FlagID_PK";
            ddl_Flags.DataSource = dt;
            ddl_Flags.DataBind();
        }

        #endregion

    }
}

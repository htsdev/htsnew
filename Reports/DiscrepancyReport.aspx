<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateViolationSRV.ascx" TagName="UpdateViolationSRV"
    TagPrefix="uc4" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.DiscrepancyReport"
    Codebehind="DiscrepancyReport.aspx.cs" EnableEventValidation="false" %>
<%--
<%@ Register Src="../WebControls/UpdateViolation.ascx" TagName="UpdateViolation"
    TagPrefix="uc2" %>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Discrepancy Report</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script>
	      function OpenEditWin(violationid)
	      {
	      
	     //a;
	          var PDFWin
		      PDFWin = window.open("../ClientInfo/UpdateViolation.aspx?ticketsViolationID="+violationid,"","");	
		      return false;				      
	       } 
	  
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        &nbsp;<table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0"
            width="780" align="center" border="0">
            <tbody>
                <tr>
                    <td style="height: 14px" colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Discrepancy (s=a) </font></STRONG>
									</td>
								</tr>-->
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="Table6" cellspacing="0" cellpadding="0" width="780" bgcolor="white" border="0">
                                        <tr>
                                            <td>
                                                &nbsp;
                                                <ew:CalendarPopup ID="calQueryFrom" runat="server" DisableTextboxEntry="False" SelectedDate="2005-09-01"
                                                    ToolTip="Select Report Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                                    Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
                                                    ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif"
                                                    Width="90px">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                                &nbsp;-</td>
                                            <td>
                                                &nbsp;
                                                <ew:CalendarPopup ID="calQueryTo" runat="server" SelectedDate="2006-09-01" ToolTip="Select Report Date Range"
                                                    PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)"
                                                    AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                                    Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif" Width="90px"
                                                    DisableTextboxEntry="False">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chk_showall" runat="server" Font-Size="XX-Small" Text="Show All"></asp:CheckBox></td>
                                            <td align="left" style="width: 14%">
                                                &nbsp;
                                                <asp:DropDownList ID="ddl_courtlocation" runat="server" CssClass="clsinputadministration"
                                                    Width="94px">
                                                    <asp:ListItem Selected="True" Value="0">All Courts</asp:ListItem>
                                                    <asp:ListItem Value="1">Inside Courts</asp:ListItem>
                                                    <asp:ListItem Value="2">Outside Courts</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td align="left">
                                                <asp:Button ID="btn_submit" runat="server" Text="Submit" CssClass="clsbutton"></asp:Button>&nbsp;
                                            </td>
                                            <td width="39%" align="right">
                                                <asp:Label ID="lblCurrPage" runat="server" Font-Size="Smaller" Width="83px" CssClass="cmdlinks"
                                                    Height="8px" Font-Bold="True" ForeColor="#3366cc">Current Page :</asp:Label><asp:Label
                                                        ID="lblPNo" runat="server" Font-Size="Smaller" Width="9px" CssClass="cmdlinks"
                                                        Height="10px" Font-Bold="True" ForeColor="#3366cc">a</asp:Label>&nbsp;<asp:Label
                                                            ID="lblGoto" runat="server" Font-Size="Smaller" Width="16px" CssClass="cmdlinks"
                                                            Height="7px" Font-Bold="True" ForeColor="#3366cc">Goto</asp:Label>&nbsp;<asp:DropDownList
                                                                ID="cmbPageNo" runat="server" Font-Size="Smaller" CssClass="clinputcombo" Font-Bold="True"
                                                                ForeColor="#3366cc" AutoPostBack="True">
                                                            </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="780" background="../../images/separator_repeat.gif" colspan="7" height="11">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                        border="0">
                                        <tr>
                                            <td valign="top" align="center" colspan="2">
                                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                                        <asp:DataGrid ID="dg_valrep" runat="server" Width="100%" PageSize="20" AllowPaging="True"
                                                            BackColor="#EFF4FB" AutoGenerateColumns="False" BorderColor="White" BorderStyle="None"
                                                            CssClass="clsLeftPaddingTable">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="S#" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Sno" runat="server" CssClass="label"></asp:Label>
                                                                        <asp:Label ID="lbl_violationid" runat="server" Width="17px" Text='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>'
                                                                            CssClass="label" Visible="False">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lbl_ticketid" runat="server" Width="17px" Text='<%# DataBinder.Eval(Container.DataItem, "TicketID_pk") %>'
                                                                            CssClass="label" Visible="False">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lbl_search" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "activeflag") %>'
                                                                            Visible="False" Width="17px"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Ticket No.">
                                                                    <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="HLTicketno" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumberseq") %>'
                                                                            Target="_self">
                                                                        </asp:HyperLink>&nbsp;&nbsp;
                                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>' />
                                                                        <asp:HiddenField ID="hf_bondamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.Bondamount","{0:F0}") %>' />
                                                                        <asp:HiddenField ID="hf_fineamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.FineAmount","{0:F0}") %>' />
                                                                        <asp:HiddenField ID="hf_courtloc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.shortname") %>' />
                                                                        <asp:HiddenField ID="hf_priceplan" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' />
                                                                        <asp:HiddenField ID="hf_bondflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' />
                                                                        <asp:HiddenField ID="hf_violationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>' />
                                                                        <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>' />
                                                                        <asp:HiddenField ID="hf_courtviolationstatusid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtViolationStatusidmain") %>' />
                                                                        <asp:HiddenField ID="hf_categoryid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' />
                                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' />
                                                                        <asp:HiddenField ID="hf_OscareCourt" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.OscarCourtDetail") %>' />
                                                                        <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refcasenumberseq") %>' />
                                                                        <asp:HiddenField ID="hf_courtdate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:d}") %>' />
                                                                        <asp:HiddenField ID="hf_crm" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' />
                                                                        <asp:HiddenField ID="hf_ctime" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>' />
                                                                        <asp:HiddenField ID="hf_VDesc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>' />
                                                                        <asp:HiddenField ID="hf_courtstatus" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.VerCourtDesc") %>' />
                                                                        <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ticketid_pk") %>' />
                                                                        <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.iscriminalcourt") %>' />
                                                                        <asp:HiddenField ID="hf_chargelevel" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.chargelevel") %>' />
                                                                        <asp:HiddenField ID="hf_cdi" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.cdi") %>' />
                                                                        <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.isFTAViolation") %>' />
                                                                        <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasFTAViolations") %>' />
                                                                        <asp:HiddenField ID="hf_hasNOS" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasNOS") %>' />
                                                                        <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value='<%# Bind("coveringFirmId") %>' />
                                                                        <asp:HiddenField ID="hf_arrestdate" runat="server" Value='<%# Bind("arrestdate","{0:d}") %>' />
                                                                        <asp:HiddenField ID="hf_casetype" runat="server" Value='<%# Bind("casetypeid") %>' />
                                                                        <asp:HiddenField ID="hf_missedcourttype" runat="server" Value='<%# Bind("missedcourttype") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Scan Court Info">
                                                                    <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_verified" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatescan") %>'
                                                                            CssClass="label">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lbl_verifiedcrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumberscan") %>'
                                                                            CssClass="label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_vdescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sDesc") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_vcourtname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Auto Court Info">
                                                                    <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_upload" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Courtdate") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label>#
                                                                        <asp:Label ID="lbl_uploadcrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumber") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_adescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ADesc") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_acourtname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Select">
                                                                    <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkb_manverify" runat="server" CommandName="Clicked">Manual Verify</asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                            <PagerStyle VerticalAlign="Middle" NextPageText="   Next &gt;" PrevPageText="  &lt; Previous        "
                                                                HorizontalAlign="Center"></PagerStyle>
                                                        </asp:DataGrid>
                                                        <table style="display: none" id="tbl_plzwait1" width="800">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="clssubhead" valign="middle" align="center" style="width: 785px">
                                                                        <img src="../Images/plzwait.gif" />
                                                                        Please wait While we update your Violation information.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click" />
                                                        <aspnew:PostBackTrigger ControlID="dg_valrep" />
                                                    </Triggers>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="780" background="../../images/separator_repeat.gif" height="11">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <uc4:UpdateViolationSRV ID="UpdateViolationSRV1" runat="server" GridID="dg_valrep"
                            GridField="hf_ticketviolationid" DivID="tbl_plzwait1" IsCaseDisposition="false" />
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>

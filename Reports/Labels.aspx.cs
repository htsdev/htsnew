using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using MetaBuilders.WebControls;
using HTP.Components;
using System.Text;
using System.Drawing;
using System.IO;

namespace HTP.Reports
{
    public partial class Labels : System.Web.UI.Page
    {
        clsBatch batch = new clsBatch();
        clsLogger clog = new clsLogger();
        DateTime datetime;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // Noufil 3650 04/30/2008 getting label reports
            try
            {
                if (!Page.IsPostBack)
                {
                    datetime = DateTime.Now;
                    fillgrid();
                }
                
                //Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgbtn_trialprint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                lbl_message.Visible = false;
                string BatchIDList = string.Empty;

                SelectorField.SelectorFieldCheckAllBox objHeadercheck = (SelectorField.SelectorFieldCheckAllBox)gv_records.HeaderRow.Cells[5].Controls[0];
                objHeadercheck.Checked= false;

                foreach (GridViewRow gvr in gv_records.Rows)
                {

                    if (gvr.RowType == DataControlRowType.DataRow)
                    {
                        clsSession cSession = new clsSession();
                        int EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                        HiddenField ticket = (HiddenField)gvr.FindControl("hf_ticketid");
                        int ticketid = Convert.ToInt32(ticket.Value);
                        //int ticketid = Convert.ToInt32(((HiddenField)gvr.FindControl("hf_ticketid")).Value);

                        SelectorField.SelectorFieldCheckBox objcheck = (SelectorField.SelectorFieldCheckBox)gvr.Cells[5].Controls[0];
                        if (objcheck.Selected == true)
                        {
                            HiddenField batchID = (HiddenField)gvr.FindControl("hf_BatchID");
                            BatchIDList += batchID.Value + ",";
                            string description = "Edelivery Label Printed";
                            clog.AddNote(EmpID, description, "", ticketid);
                            objcheck.Selected = false;
                        }
                    }
                
                }
           
                Session["batchids"] = BatchIDList;
                HttpContext.Current.Response.Write("<script> window.open('/reports/rptlabelimages.aspx');</script>");
                //Response.Redirect(Request.RawUrl);
                //fillgrid();
                
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void todate_DateChanged(object sender, EventArgs e)
        {
            try
            {
                datetime = todate.SelectedDate;
                fillgrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gv_records.PageIndex = e.NewPageIndex;
        //    fillgrid();
        //}
        
        private void fillgrid()
        {

            DataTable dt = batch.getlabels(datetime, cb_sleelctall.Checked);
            if (dt.Rows.Count > 0)
            {
                lbl_message.Visible = false;
                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();

                //Pagingctrl.PageCount = gv_records.PageCount;
                //Pagingctrl.PageIndex = gv_records.PageIndex;
                //Pagingctrl.SetPageIndex();
            }
            else
            {
                lbl_message.Visible = true;
                gv_records.Visible = false;
                lbl_message.Text = "No Records Found";
            }
        }

        //void Pagingctrl_PageIndexChanged()
        //{
        //    gv_records.PageIndex = Pagingctrl.PageIndex - 1;
        //    fillgrid();
        //}

        protected void cb_sleelctall_CheckedChanged(object sender, EventArgs e)
        {
            datetime = todate.SelectedDate;
            fillgrid();
        }
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;





//////////////////////////////
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.Components; //khalid 31-8-07

namespace HTP.Reports
{
    public partial class BondWaitingReport : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        DataTable dtRecords = new DataTable();
        clsLogger bugTracker = new clsLogger();
        //khalid 31-8-07
         public ValidationReports reports = new ValidationReports();
         string StrExp = String.Empty;
         string StrAcsDec = String.Empty;
         DataSet DS;
         DataView DV;



        protected void Page_Load(object sender, EventArgs e)
        {
            
            lbl_Message.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    DS = new DataSet();
                    DV = new DataView();
                  //khalid 31-8-07
                     reports.getReportData(DG_pastcases,lbl_Message, ValidationReport.BondWaitingReport);
                     DS = reports.records;
                     if (DS.Tables[0].Rows.Count > 0)
                     {
                         DV = new DataView(DS.Tables[0]);
                         Session["DS"] = DV;
                     }
                   
                    //khalid 31-8-07
                    Fill_ddlist();

                    LB_curr.Text = (DG_pastcases.PageIndex + 1).ToString();
                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }

        }


        private void Fill_ddlist()
        {
            for (int i = 1; i <= this.DG_pastcases.PageCount; i++)
            {
                DL_pages.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));
            }

        }
        protected void DG_pastcases_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;
                DV.Table.Columns.Remove("sno");
                if (DV.Table.Columns.Contains("sno") == false)
                {
                    DV.Table.Columns.Add("sno");
                }
                DG_pastcases.DataSource = DV;
                DG_pastcases.DataBind();
                Session["DS"] = DV;


                FixSerial();
            }



            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }
        public void FixSerial()
        {
            int stind = (DG_pastcases.PageIndex * DG_pastcases.PageSize) + 1;

            try
            {
                for (int i = 0; i <= DG_pastcases.PageSize; i++)
                {

                    HyperLink lb_sno = (HyperLink)DG_pastcases.Rows[i].FindControl("sno");
                    lb_sno.Text = stind.ToString();
                    stind++;

                }
            }
            catch (Exception e)
            {
                //lbl_Message.Text = e.Message;
            }


        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "DESC")
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "DESC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }


        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);

        }



        protected void DG_pastcases_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {//validation report component method

            //reports.getReportData(DG_pastcases, lbl_Message, ValidationReport.PastCourtDates,e.NewPageIndex);
            //DataSet ds = (DataSet)DG_pastcases.DataSource;



            //reports.getReportData(e.NewPageIndex, DG_pastcases, lbl_Message,"9");

            //reports.getReportData(e.NewPageIndex, DG_pastcases, lbl_Message, e.NewPageIndex);
            DataView dv = (DataView)Session["DS"];

            LB_curr.Text = (e.NewPageIndex + 1).ToString();
            DG_pastcases.PageIndex = e.NewPageIndex;
            DG_pastcases.DataSource = dv;
            DG_pastcases.DataBind();
            FixSerial();

            DL_pages.SelectedIndex = e.NewPageIndex;


        }

        protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        {
            int x = Convert.ToInt32(this.DL_pages.SelectedValue);
            //validation report component method
            //reports.getReportData(x-1, DG_pastcases, lbl_Message,"9");

            // reports.getReportData(e.NewPageIndex, DG_pastcases, lbl_Message, e.NewPageIndex);

            // reports.getReportData(DG_pastcases, lbl_Message, ValidationReport.PastCourtDates, x-1);
            DataView dv = (DataView)Session["DS"];

            //LB_curr.Text = (e.NewPageIndex + 1).ToString();
            DG_pastcases.PageIndex = x - 1;// e.NewPageIndex;
            DG_pastcases.DataSource = dv;
            DG_pastcases.DataBind();
            FixSerial();

            LB_curr.Text = x.ToString();

        }
       /* private void Fill_ddlist() 
        {
            for (int i = 1; i <=this.DG_pastcases.PageCount; i++)
            {  DL_pages.Items.Add(new ListItem(Convert.ToString(i),Convert.ToString(i)));
            }
           
        }
        protected void DG_pastcases_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;
                DG_pastcases.DataSource = DV;
                DG_pastcases.DataBind();

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }


        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);

        }


        
        protected void DG_pastcases_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {//validation report component method

           // reports.getReportData(DG_pastcases, lbl_Message, ValidationReport.BondWaitingReport,e.NewPageIndex);
                   
            
            //reports.getReportData(e.NewPageIndex, DG_pastcases, lbl_Message,"9");

            //reports.getReportData(e.NewPageIndex, DG_pastcases, lbl_Message, e.NewPageIndex);
            
            DL_pages.SelectedIndex = e.NewPageIndex;
            LB_curr.Text = (e.NewPageIndex + 1).ToString();
            DG_pastcases.DataSource = DV;
            DG_pastcases.DataBind();

          
        }

        protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        {
           int x=Convert.ToInt32(this.DL_pages.SelectedValue);
           //validation report component method
           //reports.getReportData(x-1, DG_pastcases, lbl_Message,"9");

          // reports.getReportData(e.NewPageIndex, DG_pastcases, lbl_Message, e.NewPageIndex);

           reports.getReportData(DG_pastcases, lbl_Message, ValidationReport.BondWaitingReport, x-1);
            
            LB_curr.Text = x.ToString();
        }

        protected void DG_pastcases_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

         
        */
         }
 
}




///////////////
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlertValidation.aspx.cs"
    Inherits="HTP.Reports.AlertValidation" %>

<%--EnableEventValidation="false"--%>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Wrong Telephone Number</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        .clsInputadministration
        {
            border-right: #3366cc 1px solid;
            border-top: #3366cc 1px solid;
            font-size: 8pt;
            border-left: #3366cc 1px solid;
            width: 90px;
            color: #123160;
            border-bottom: #3366cc 1px solid;
            font-family: Tahoma;
            background-color: white;
            text-align: left;
        }
        .clsInputCombo
        {
            border-right: #e52029 1px solid;
            border-top: #e52029 1px solid;
            font-weight: normal;
            list-style-position: outside;
            font-size: 8pt;
            border-left: #e52029 1px solid;
            color: #123160;
            border-bottom: #e52029 1px solid;
            font-family: Tahoma;
            list-style-type: square;
            background-color: white;
        }
        .label
        {
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            border-bottom-width: 0;
            border-left-width: 0;
            border-right-width: 0;
            border-top-width: 0;
            text-align: left;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <aspnew:ScriptManager ID="smFrmMain" runat="server">
                                                    <Scripts>
                                                        <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                                                    </Scripts>
                                                </aspnew:ScriptManager>
                                                <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td colspan="5" width="780">
                                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                                            PageSize="30" CssClass="clsleftpaddingtable" OnRowCommand="gv_records_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S#">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid")+"&search=0" %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Name" HeaderText="Name">
                                                                    <ItemStyle CssClass="GridItemStyle" Width="175px" />
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Language" HeaderText="Language">
                                                                    <ItemStyle CssClass="GridItemStyle" />
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="100px" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Contact Info">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblcontact1" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.contact1")%>'></asp:Label><br />
                                                                        <asp:Label ID="lblcontact2" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.contact2")%>'></asp:Label><br />
                                                                        <asp:Label ID="lblcontact3" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.contact3")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Bond" HeaderText="Bond">
                                                                    <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="25px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CRT" HeaderText="CRT">
                                                                    <ItemStyle CssClass="GridItemStyle" Width="50px" />
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="TrailDesc" HeaderText="Trial Desc">
                                                                    <ItemStyle CssClass="GridItemStyle" Width="140px" />
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="infoimage" runat="server" Text="<img src='../Images/Previouse.gif' border='0'/>"
                                                                            CommandName="ticket" CommandArgument='<%#Eval("TicketId") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="25px" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <AjaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                    PopupControlID="panel1" CancelControlID="lbtnclose" BackgroundCssClass="modalBackground">
                </AjaxToolkit:ModalPopupExtender>
                <asp:Panel ID="panel1" runat="server" BackColor="white" Width="730px">
                    <table id="Table1" bgcolor="white" border="0" style="border-top: black thin solid;
                        border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid"
                        cellpadding="0" cellspacing="0">
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="Left" style="height: 36px">
                                <table id="tablepopup" cellpadding="0" cellspacing="0" style="width: 727px">
                                    <tr>
                                        <td class="clssubhead" style="height: 17px">
                                            &nbsp;&nbsp;<strong>Contact Information </strong>
                                        </td>
                                        <td align="right" style="height: 17px; width: 33px;">
                                            &nbsp;
                                            <asp:LinkButton ID="lbtnclose" runat="server">X</asp:LinkButton>&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblPopupmessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" width="780" align="center">
                                <asp:Panel ID="pngrid" runat="server" BackColor="white" ScrollBars="Auto" Width="730px"
                                    Height="200px" Style="">
                                    <asp:GridView ID="gv_info" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        PageSize="20" Width="710px" Height="100px" CellPadding="3">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Full Name">
                                                <ItemTemplate>
                                                    &nbsp;
                                                    <asp:HyperLink ID="hl_Name" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid")+"&search=0" %>'
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.fullname")%>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Address" HeaderText="Address">
                                                <ItemStyle CssClass="GridItemStyle" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Contact Info">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcontact1" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.contact1")%>'></asp:Label><br />
                                                    <asp:Label ID="lblcontact2" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.contact2")%>'></asp:Label><br />
                                                    <asp:Label ID="lblcontact3" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.contact3")%>'></asp:Label><br />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <asp:Button ID="Button1" runat="server" Text="Cancel" Style="display: none;" /></asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
        &nbsp;&nbsp;
    </div>
    </form>
</body>
</html>

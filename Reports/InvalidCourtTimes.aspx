<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvalidCourtTimes.aspx.cs" Inherits="lntechNew.Reports.InvalidCourtTimes" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Invalid Court Times</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
   
   
    
    
</head>
<body>
    <form id="form1" runat="server">
   
    <table  id = "MainTable" cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 780px">
    <tr>
    <td style="height: 16px">
        &nbsp;
        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
    </td>
    </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
    
        <tr>
            <td align="center">
                <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gv_Result" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                    Width="780px" AllowPaging="True" PageSize="50" AllowSorting="True" OnRowDataBound="gv_Result_RowDataBound" OnPageIndexChanging="gv_Result_PageIndexChanging">
                    <Columns>
                            <asp:TemplateField HeaderText="NO" SortExpression="S NO">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid" Visible=false runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")  %>'></asp:Label>
                                    <asp:HyperLink ID="hf_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text=""></asp:HyperLink>
                                    <asp:HyperLink ID="hl_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.S No") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CAUSE NO"  >
                                <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hf_CauseNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>' Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TICKET NO" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>' Visible="False"></asp:Label>
                                    <asp:HyperLink ID="hf_TicketNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="CLIENT NAME">
                        <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                            <ItemTemplate>
                                <asp:Label ID="lbl_Client" CssClass="clsLabel"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.clientname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="COURT">
                        <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                            <ItemTemplate>
                                <asp:Label ID="lbl_Court" CssClass="clsLabel"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.court") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="COURT DATE"  >
                            <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                                <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Update" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="COURT #">
                        <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                            <ItemTemplate>
                                <asp:Label ID="lbl_Courtroom" CssClass="clsLabel"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtroom") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" >
                                <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    <PagerSettings Mode="NumericFirstLast"  />
                    <PagerStyle HorizontalAlign="Center" />
                </asp:GridView>
               
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server" />
            </td>
        </tr>
    </table>
    
   
    </form>
</body>
</html>

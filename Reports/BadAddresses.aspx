<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BadAddresses.aspx.cs" Inherits="HTP.Reports.BadAddresses" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bad Addresses</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>



</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <div>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" width="870px" align="center" border="0">
                    <tr>
                        <td>
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" cellspacing="0" cellpadding="0" width="870px" border="0">
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr class="clsLeftPaddingTable">
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 600px;" align="left">
                                                <uc5:ShowSetting ID="ShowSetting" lbl_TextAfter="(Business Days)" lbl_TextBefore="Follow Up Date in Next"
                                                    runat="server" Attribute_Key="Days" />
                                            </td>
                                                <td align="right">
                                                    <asp:CheckBox ID="chkShowAll" runat="server" Text="Show All " CssClass="clssubhead"
                                                        OnCheckedChanged="chkShowAllUserRecords_CheckedChanged" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="right">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                        </aspnew:UpdateProgress>
                                                
                            <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True"
                                OnPageIndexChanging="gv_Records_PageIndexChanging" CellPadding="3" OnRowCommand="gv_Records_RowCommand"
                                OnRowDataBound="gv_Records_RowDataBound" OnSorting="gv_Records_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="4%">
                                        <ItemTemplate>
                                        <%--sameeullah  1/4/2011 8488 No need to generate serail Numbers manually ,serial number are generated through <%# Container.DataItemIndex + 1 %>--%>
                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                Text='<%# Container.DataItemIndex + 1 %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ControlStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Last Name&lt;/u&gt;" SortExpression="lastname" ItemStyle-Width="20%" HeaderStyle-Font-Underline="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Existing Address&lt;/u&gt;" SortExpression="ADDRESS" ItemStyle-Width="45%" HeaderStyle-Font-Underline="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Address" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.ADDRESS") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="&lt;u&gt;Court Date&lt;/u&gt;" SortExpression="CourtDateMain" HeaderStyle-Font-Underline="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="&lt;u&gt;Hire Date&lt;/u&gt;" SortExpression="Hiredate" HeaderStyle-Font-Underline="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_HireDate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Hiredate") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>                                                                                                           
                                    <asp:TemplateField HeaderText="&lt;u&gt;Follow Up Date&lt;/u&gt;" SortExpression="FollowUpDate" HeaderStyle-Font-Underline="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_followupdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="img_Add" runat="server" CommandName="btnclick" Text="&lt;img src='../Images/add.gif' border='0'/&gt;" />
                                            <%--<asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("lastname") %>' />--%>
                                            <%--<asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("courtid") %>' />--%>
                                            <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("Lastname") %>' />
                                          <%--  <asp:HiddenField ID="hf_court_loc" runat="server" Value='<%#Eval("ShortName") %>' />--%>
                                            <%--<asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumber") %>' />--%>
                                            <asp:HiddenField ID="hf_TicketId" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                           <%-- <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("casenumassignedbycourt") %>' />                                            
                                            <%-- <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("COURTVIOLATIONSTATUSIDMAIN") %>' />--%>
                                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" Value='<%#Eval("FollowUpDate") %>' />                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc4:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="FollowUp Date" />
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlFollowup" TargetControlID="btn">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc2:Footer ID="Footer1" runat="server" />                            
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
           
        </aspnew:UpdatePanel>
    </div>
    </form>
    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 100003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 100004;
    </script>
</body>
</html>

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.SessionState;
using System.Xml.Linq;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace HTP.Reports
{
    public partial class NonupdatedResetCases : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase cCase = new clsCase();
        ValidationReports objValidation = new ValidationReports();
        DataSet ds_val;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        FillGrid(); // binding data in grid.
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_NonupdatedResetCases;
                    ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();

            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {

            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion




        private void FillGrid()
        {
            try
            {
                ds_val = objValidation.GetNonupdatedResetCasesReport(true);
                GenerateSerialNo(ds_val.Tables[0]); // generating serial number for data grid.
                Pagingctrl.GridView = gv_NonupdatedResetCases;
                gv_NonupdatedResetCases.DataSource = ds_val.Tables[0];
                gv_NonupdatedResetCases.DataBind();
                Pagingctrl.PageCount = gv_NonupdatedResetCases.PageCount;
                Pagingctrl.PageIndex = gv_NonupdatedResetCases.PageIndex;
                Pagingctrl.SetPageIndex();
                lbl_message.Text = "";
                if (ds_val.Tables[0].Rows.Count < 1)
                {
                    lbl_message.Text = "No Records Found";
                }

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }
        }

        /// <summary>
        /// Babar Ahmad 9055 04/01/2011
        /// Generate the serial no on the basis of Ticket Id
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
            }

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["TICKETID_PK"].ToString() != dtRecords.Rows[i]["TICKETID_PK"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        /// <summary>
        /// Babar Ahmad 9055 04/01/2011
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_NonupdatedResetCases.PageIndex = Pagingctrl.PageIndex - 1;  // set page index to grid.
                FillGrid();
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }

        /// <summary>
        /// Babar Ahmad 9055 04/01/2011
        /// Maintained the data in update followUp date Control
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();
        }


        /// <summary>
        /// Babar Ahmad 9055 04/01/2011
        /// Maintained the page size
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_NonupdatedResetCases.PageIndex = 0;
                gv_NonupdatedResetCases.PageSize = pageSize;
                gv_NonupdatedResetCases.AllowPaging = true;
            }
            else
            {
                gv_NonupdatedResetCases.AllowPaging = false;
            }
            FillGrid();
        }



        protected void chkShowAllUserRecords_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_NonupdatedResetCases_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_NonupdatedResetCases.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();

            }

        }

        // Babar Ahmad 9727 10/21/2011 This function is used to delete records from the report.
        protected void gv_NonupdatedResetCases_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnRemove")
                {
                    int rowIndex = int.Parse(e.CommandArgument.ToString());


                    int ticketid = Convert.ToInt32(((HiddenField)gv_NonupdatedResetCases.Rows[rowIndex].FindControl("hf_TicketID")).Value);

                    int ticketviolationid = Convert.ToInt32(((HiddenField)gv_NonupdatedResetCases.Rows[rowIndex].FindControl("hf_TicketViolationID")).Value);
                    // Babar Ahmad 9727 10/21/2011 Remove records from the report when "X is clicked".
                    objValidation.UpdadeNonUpdateWaitingCases(ticketid, ticketviolationid);
                    FillGrid();

                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }








    }
}

﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

// Abbas Qamar 9957 03-01-2012 
namespace HTP.Reports
{
    public partial class PaymentTypes : System.Web.UI.Page
    {
        NewClsPayments ClsPayments = new NewClsPayments();
        private clsENationWebComponents cls_db = new clsENationWebComponents();
        clsLogger clog = new clsLogger();        

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                FillGrid();
            }

        }


        private void FillGrid()
        {

            gv_Records.DataSource = ClsPayments.GetAllPaymentTypes();
            gv_Records.DataBind();

        }

        // Abbas Qamar 9957 03-01-2012 Adding the payment type record.
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string[] key = { "@Description", "@ShortDescription", "@IsActive" };
                object[] value = { txtPaymentType.Text, txtShortDesc.Text, ChkIsActive.Checked };
                cls_db.ExecuteSP("USP_HTP_Insert_AllPaymentTypes", key, value);
                FillGrid();
                lbl_Message.Text = "Record Added sucessfully";
                txtPaymentType.Text = "";
                txtShortDesc.Text = "";
                ChkIsActive.Checked = false;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }


        // Abbas Qamar 9957 03-01-2012 Deleting the Record.
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    int paymenttypeId = Convert.ToInt32(e.CommandArgument);
                    string rowcount = ClsPayments.DeletePaymentType(paymenttypeId);

                    if (rowcount != "0")
                    {
                        lbl_Message.Text = "Record Deleted sucessfully";
                        FillGrid();
                    }
                    else
                    {

                        lbl_Message.Text = "Payment type could not deleted because it's associated with record(s).";
                    }

                }



            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        
        protected void gv_Records_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAdd.Visible = false;
            btnUpdate.Visible = btnCancel.Visible = true;
            txtPaymentType.Text = ((LinkButton)gv_Records.SelectedRow.FindControl("lbtn_Desc")).Text;
            txtShortDesc.Text = ((Label)gv_Records.SelectedRow.FindControl("LblShortDesc")).Text;
            HF_PaymentTypeId.Value = Convert.ToString(gv_Records.SelectedDataKey.Value);
            ChkIsActive.Checked = ((CheckBox)gv_Records.SelectedRow.FindControl("ChkIsActive")).Checked;
        }


        // Abbas Qamar 9957 03-01-2012 Updating the records
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string rowcount = ClsPayments.UpdatePaymentType(Convert.ToInt32(HF_PaymentTypeId.Value), txtPaymentType.Text, txtShortDesc.Text, ChkIsActive.Checked);
                //("USP_HTP_Insert_AllPaymentTypes", key, value);

                if (rowcount != "0")
                {
                    lbl_Message.Text = "Record Updated sucessfully";
                    FillGrid();
                    txtPaymentType.Text = "";
                    txtShortDesc.Text = "";
                    HF_PaymentTypeId.Value = "";
                    btnUpdate.Visible = btnCancel.Visible = false;
                    btnAdd.Visible = true;
                    ChkIsActive.Checked = false;
                }
                else
                {

                    lbl_Message.Text = "Record could not updated sucessfull.";
                }


            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Records_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        // Abbas Qamar 9957 03-01-2012 clear the data and reset buttons.
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtPaymentType.Text = "";
            txtShortDesc.Text = "";
            HF_PaymentTypeId.Value = "";
            ChkIsActive.Checked = false;
            btnAdd.Visible = true;
            btnUpdate.Visible = btnCancel.Visible = false;
            lbl_Message.Text = "";

        }






    }
}

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="HCCCriminalPopup.aspx.cs"
    Inherits="HTP.Reports.HCCCriminalPopup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Styles.css" type="text/css" rel="stylesheet"/>
    <title>Harris County Criminal Case Information</title>
</head>
<body leftmargin="0" topmargin="0">
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblmain" runat="server" Visible="False" Font-Bold="True" ForeColor="Red"
                Text="Information Not Found" CssClass="label"></asp:Label>
            <table id="TableMain" cellspacing="0" cellpadding="0" border="0" align="left" width="750"
                runat="server">
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="1" style="width: 763px;
                        height: 35px">
                    </td>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="height: 35px;
                        width: 763px;">
                        &nbsp;Section I : &nbsp;Information from the online arrest website</td>
                </tr>
                <tr>
                    <td class="label" colspan="1" style="width: 762px">
                    </td>
                    <td class="label" colspan="2" style="width: 762px">
                        <asp:GridView ID="gvSection1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            PageSize="1" ShowHeader="False">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0" class="clsleftpaddingtable" width="750">
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Booking Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBookingNumber" runat="server" CssClass="clslabel" Text='<%# Bind("BookingNumber") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Booking Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBookingDate" runat="server" CssClass="clslabel" Text='<%# Bind("BookingDate","{0:d}") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Name
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblName" runat="server" CssClass="clslabel" Text='<%# Bind("Name") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Case Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCaseNumber" runat="server" CssClass="clslabel" Text='<%# Bind("CaseNumber") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Sex</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblSex" runat="server" CssClass="clslabel" Text='<%# Bind("Sex") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Address</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblAddress" runat="server" CssClass="clslabel" Text='<%# Bind("Address") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Race</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblRace" runat="server" CssClass="clslabel" Text='<%# Bind("Race") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    State</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblState" runat="server" CssClass="clslabel" Text='<%# Bind("State") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Date of Birth</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lbldob" runat="server" CssClass="clslabel" Text='<%# Bind("dob","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    City</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCity" runat="server" CssClass="clslabel" Text='<%# Bind("city") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Charge Wording</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblChargeWording" runat="server" CssClass="clslabel" Text='<%# Bind("Chargewording") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Charge Code</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblChargeCode" runat="server" CssClass="clslabel" Text='<%# Bind("chargecode") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Charge Level</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblChargelevel" runat="server" CssClass="clslabel" Text='<%# Bind("chargelevel") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Disposition</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblDisposition" runat="server" CssClass="clslabel" Text='<%# Bind("disposition") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Data Load Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblRecdate" runat="server" CssClass="clslabel" Text='<%# Bind("recdate","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Arrest Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblArrestDate" runat="server" CssClass="clslabel" Text='<%# Bind("arrestdate","{0:d}") %>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblError1" runat="server" Font-Bold="True" ForeColor="Red" Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="1" style="width: 763px;
                        height: 35px">
                    </td>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="height: 35px;
                        width: 763px;">
                        &nbsp;Section II : Information from Sheriff's Department Report</td>
                </tr>
                <tr>
                    <td colspan="1" style="width: 762px">
                    </td>
                    <td colspan="2" style="width: 762px">
                        <asp:GridView ID="gvSection2" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            PageSize="1" ShowHeader="False">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0" class="clsleftpaddingtable" width="750">
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Name
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblName" runat="server" CssClass="clslabel" Text='<%# Bind("Name") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    SPN</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblSpn" runat="server" CssClass="clslabel" Text='<%# Bind("spn") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Booking Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBookingNumber" runat="server" CssClass="clslabel" Text='<%# Bind("BookingNumber") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Booking Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBookingDate" runat="server" CssClass="clslabel" Text='<%# Bind("BookingDate","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Booking Time</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBookingTime" runat="server" CssClass="clslabel" Text='<%# Bind("BookingTime") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Deputy Booking
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblDeputy" runat="server" CssClass="clslabel" Text='<%# Bind("Deputy") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Deputy Release</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblDeputyRelease" runat="server" CssClass="clslabel" Text='<%# Bind("ReleaseDeputy") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Release Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblReleaseDate" runat="server" CssClass="clslabel" Text='<%# Bind("ReleaseDate","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Release Time</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblReleaseTime" runat="server" CssClass="clslabel" Text='<%# Bind("ReleaseTime") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Release Reason</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="Label6" runat="server" CssClass="clslabel" Text='<%# Bind("Reas","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Off</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="Label3" runat="server" CssClass="clslabel" Text='<%# Bind("Off") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Case Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblcasenumber" runat="server" CssClass="clslabel" Text='<%# Bind("casenumber") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Bond Amount</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBondAmount" runat="server" CssClass="clslabel" Text='<%# Bind("BondAmount") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Room</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCourtRoom" runat="server" CssClass="clslabel" Text='<%# Bind("CCR") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    JPI</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblJPI" runat="server" CssClass="clslabel" Text='<%# Bind("jpi") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Data Load Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblRecdate" runat="server" CssClass="clslabel" Text='<%# Bind("recdate","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    JPI Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblJPINumber" runat="server" CssClass="clslabel" Text='<%# Bind("Jpinumber") %>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblerror2" runat="server" CssClass="label" Font-Bold="True" ForeColor="Red"
                            Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="1" style="width: 763px;
                        height: 35px">
                    </td>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="height: 35px;
                        width: 763px;">
                        &nbsp;Section III &amp; IV &nbsp;: &nbsp;Information from the online offense inquiry
                        &nbsp;website &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="hlSection3And4" runat="server" Target="_blank">(View)</asp:HyperLink>
                        &nbsp; &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="clssubhead" colspan="1" style="width: 762px">
                    </td>
                    <td class="clssubhead" colspan="2" style="width: 762px;">
                        <asp:GridView ID="gvSection3" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            PageSize="1" ShowHeader="False" OnRowDataBound="gvSection3_RowDataBound">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0" class="clsleftpaddingtable" width="750">
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Offense / Incident Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblOffenseno" runat="server" CssClass="clslabel" Text='<%# Bind("casenumber") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    SPN</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblSpn" runat="server" CssClass="clslabel" Text='<%# Bind("spn") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Defendent In Jail</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lbldefendent" runat="server" CssClass="clslabel" Text='<%# Bind("isdefendantinjail") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCourt" runat="server" CssClass="clslabel" Text='<%# Bind("ccr") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Sentence End Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblSentenceEndDate" runat="server" CssClass="clslabel" Text='<%# Bind("sentenceenddate","{0:d}") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Next Setting Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblNextSettingDate" runat="server" CssClass="clslabel" Text='<%# Bind("nextsettingdate","{0:d}") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Setting Reason</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblSettingReason" runat="server" CssClass="clslabel" Text='<%# Bind("settingreason") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Data Load Date</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblRecDate" runat="server" CssClass="clslabel" Text='<%# Bind("RecDate","{0:d}") %>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblerror3" runat="server" CssClass="label" Font-Bold="True" ForeColor="Red"
                            Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="1" style="width: 763px;
                        height: 35px">
                    </td>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="height: 35px;
                        width: 763px;">
                        &nbsp;Section V &nbsp;: Criminal Court Information &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp;&nbsp;
                        <asp:HyperLink ID="HyperLink1" NavigateUrl="http://www.justex.net/CriminalCourtBuilding.aspx" runat="server" Target="_blank">(View)</asp:HyperLink></td>
                </tr>
                <tr>
                    <td class="clssubhead" colspan="1" style="width: 762px">
                    </td>
                    <td class="clssubhead" colspan="2" style="width: 762px">
                        <asp:GridView ID="gvSection5" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            PageSize="1" ShowHeader="False">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0" class="clsleftpaddingtable" width="750">
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Presiding</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblPresiding" runat="server" CssClass="clslabel" Text='<%# Bind("Presiding") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Floor</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblFloor" runat="server" CssClass="clslabel" Text='<%# Bind("Floor") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Room</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblRoom" runat="server" CssClass="clslabel" Text='<%# Bind("Room") %>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblerror5" runat="server" CssClass="label" Font-Bold="True" ForeColor="Red"
                            Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="1" style="width: 763px;
                        height: 35px">
                    </td>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="height: 35px;
                        width: 763px;">
                        &nbsp;Section VI &nbsp;: Case assignment information &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <asp:HyperLink ID="hlSection5" runat="server" Target="_blank">(View)</asp:HyperLink></td>
                </tr>
                <tr>
                    <td class="clssubhead" colspan="1" style="width: 763px">
                    </td>
                    <td class="clssubhead" colspan="4" style="width: 763px;">
                        <asp:GridView ID="gvSection6" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            PageSize="1" ShowHeader="False" OnRowDataBound="gvSection6_RowDataBound">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0" class="clsleftpaddingtable" width="750">
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Phone</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCourtPhone" runat="server" CssClass="clslabel" Text='<%# bind("courtphone") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Clerk Name</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCClerkname" runat="server" CssClass="clslabel" Text='<%# Bind("CourtClerkName") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Clerk Telephone</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCourtClerkNo" runat="server" CssClass="clslabel" Text='<%# Bind("CourtClerkTelephone") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Assistant Court Clerk Name</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblACClerkName" runat="server" CssClass="clslabel" Text='<%# Bind("AssistantCourtClerkName") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Assistant Court Clerk Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblClerkANo" runat="server" CssClass="clslabel" Text='<%# Bind("AssistantCourtClerkNumber") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Liaison Officer</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblLiasonOfficer" runat="server" CssClass="clslabel" Text='<%# Bind("CourtLiasionOfficer") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                </td>
                                                <td style="width: 25%; height: 16px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Coordinator Name</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCoordinatorname" runat="server" CssClass="clslabel" Text='<%# Bind("CoordinatorName") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Coordinator Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblCoordinatorNo" runat="server" CssClass="clslabel" Text='<%# Bind("CoordinatorNumber") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Bailiff Name</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBaliffName" runat="server" CssClass="clslabel" Text='<%# Bind("BailiffName") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Baliff Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblBaliffNo" runat="server" CssClass="clslabel" Text='<%# Bind("BailiffNumber") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Process Server Name</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblServerName" runat="server" CssClass="clslabel" Text='<%# Bind("ProcessServerName") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Process Server Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblServerNo" runat="server" CssClass="clslabel" Text='<%# Bind("ProcessServerNumber") %>'></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Reporter Name</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblReporterName" runat="server" CssClass="clslabel" Text='<%# Bind("CourtReporterName") %>'></asp:Label></td>
                                                <td class="clssubhead" style="width: 25%; height: 16px">
                                                    Court Reporter Number</td>
                                                <td style="width: 25%; height: 16px">
                                                    <asp:Label ID="lblReporterNo" runat="server" CssClass="clslabel" Text='<%# Bind("CourtReporterNumber") %>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblerror6" runat="server" CssClass="label" Font-Bold="True" ForeColor="Red"
                            Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td runat="server" align="left" background="../Images/subhead_bg.gif" class="clssubhead"
                        colspan="1" style="height: 34px" id="Td1">
                    </td>
                    <td id="tr_qcoc" runat="server" align="left" class="clssubhead" background="../Images/subhead_bg.gif"
                        colspan="4" style="height: 34px">
                        &nbsp;Section VII : &nbsp;Persons connected to the case &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                        &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <asp:HyperLink ID="hlSection7" runat="server" Target="_blank" >(View)</asp:HyperLink>
           </td>
                </tr>
                <tr>
                    <td align="left" colspan="1" style="width: 762px" valign="top">
                    </td>
                    <td valign="top" colspan="2" align="left" style="width: 762px">
                        <asp:GridView ID="gv_qcoc" runat="server" AutoGenerateColumns="False" Width="750px"
                            CssClass="clsleftpaddingtable">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" CssClass="clslabel" Text='<%# Bind("name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Connection">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_connection" runat="server" CssClass="clslabel" Text='<%# Bind("connection") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SPN">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_spn" runat="server" CssClass="clslabel" Text='<%# Bind("spn") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblerror7" runat="server" CssClass="label" Font-Bold="True" ForeColor="Red"
                            Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" colspan="1" style="width: 762px; height: 10px" valign="top">
                    </td>
                    <td align="left" background="../Images/separator_repeat.gif" colspan="2" style="width: 762px;
                        height: 10px" valign="top">
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="1" style="width: 762px" valign="top">
                    </td>
                    <td align="center" colspan="2" style="width: 762px" valign="top">
                        <asp:Button ID="btnSubmit" runat="server" Text="Close This Window" CssClass="clsbutton"
                            Width="119px" OnClientClick="return self.close();"></asp:Button></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

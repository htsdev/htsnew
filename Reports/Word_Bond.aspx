<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.Word_Bond" Codebehind="Word_Bond.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Word_Bond</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<script>
		function refresh()
		{
		 //window.opener.location.reload(); //onload="refresh();"
		 opener.location.href = opener.location;
		}
	</script>
	<body MS_POSITIONING="GridLayout" onload="refresh();" >
		<form id="Form1" method="post" runat="server">
			<table border="0" width="100%" height="100%">
				<TR>
					<TD height="46" colSpan="3" background="../Images/subhead_bg.gif" style="HEIGHT: 34px"></TD>
				</TR>
				<tr>
					<td style="HEIGHT: 33px" vAlign="bottom" align="right"><table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="185" align="left"
							border="0">
							<tr>
								<td width="31"><IMG height="17" src="../Images/head_icon.gif" width="31"></td>
								<td width="140" valign="baseline" class="clssubhead">Bond Report
								</td>
							</tr>
						</table>
					</td>
					<td style="HEIGHT: 33px" colSpan="2" align="right"><asp:ImageButton ID="IBtn" runat="server" Width="78px" ImageUrl="../Images/MSSmall.gif" Height="23px"></asp:ImageButton></td>
				</tr>
				<tr>
					<td style="HEIGHT: 3px" colSpan="3">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<iframe src="rptBond.aspx?casenumber=<%=ViewState["vTicketId"]%>&CourtType=<%= ViewState["CourtType"] %> " width="100%" height="95%" scrolling="auto" frameborder="1">
						</iframe>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class NoSignedContractReport : System.Web.UI.Page
    {
        ValidationReports vreports = new ValidationReports();
        DataTable dt = new DataTable();
        #region events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    fillgrid();

                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    fillgrid();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }



        #endregion

        #region Methods
        public void fillgrid()
        {
            // Noufil 3999 06/12/2008 Binding Grid With database
            dt = vreports.GetNoLORReport();
            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No reocrds Found";

            }
            else
            {

                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }
                //DataView dv = new DataView(dt);
                //if (ViewState["sortexp"] != null || ViewState["sortdir"] != null)
                //{
                //    dv.Sort = ViewState["sortexp"] + " " + ViewState["sortdir"];
                //}
                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }

        #endregion
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewDocketCloseOut.aspx.cs" Inherits="lntechNew.Reports.testpopup" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<script runat="server">

    
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Docket Close Out</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
 
 <script type="text/javascript">
   
function poorman_toggle(id)
{
	var tr = document.getElementById(id);
	if (tr==null) { return; }
	var bExpand = tr.style.display == '';
	tr.style.display = (bExpand ? 'none' : '');
}
function poorman_changeimage(id, sMinus, sPlus)
{
	var img = document.getElementById(id);
	if (img!=null)
	{
	    
	     var bExpand = img.src.indexOf(sPlus.substring(3,sPlus.length -3)) >= 0;
		if (!bExpand)
			img.src = sPlus;
		else
			img.src = sMinus;
	}
}

function closepopup()
        {	    
		    document.form1.ddl_dockets.style.visibility = ''
		    document.form1.ddl_status.style.visibility = ''
		    document.form1.ddl_courts.style.visibility = ''
		    document.getElementById("pnl_Update").style.display = 'none';
		    return false;
	    }
	    
function dispose_validation()
        {
            if(document.getElementById('chk_DisposeAll').checked==false)
            {
            alert('Please select the check box and then click submit to dispose');
            return(false);
            }	    
            else
            return(true);
        }
	    
function PopUpCallBack(DocID)
        {
            window.open ("../Activities/processPDF.aspx?docid="+ DocID);
            return false;
        }
	    
function close_docket_popup()
        {
        document.getElementById("ddl_dockets").style.visibility = ''
        document.getElementById("ddl_status").style.visibility = ''
        document.getElementById("ddl_courts").style.visibility = ''
        document.getElementById("pnl_Dockets").style.display = 'none';
        }
	    
 function setpopuplocation()
		{
	           
	    var top  = 400;
	    var left = 400;
	    var height = document.body.offsetHeight;
	    var width  = document.body.offsetWidth
	    
	    var resolution =  width  + "x" + height;
	       	    
	    if ( width > 1100 || width <= 1280)
	         left = 575
	       
	    if ( width < 1100)
	         left = 500;
	    
	    // Setting popup display
	    document.getElementById("tbl_updatepanel").style.top =top*2;
		document.getElementById("tbl_updatepanel").style.left =left*2;
		document.getElementById("tbl_updatepanel").style.display = 'block'

		}
		
function EnableOptions()
		{
		    document.getElementById('tbl_Reset').style.display='block';
		}
		
function DisableOptions()
		{
		    document.getElementById('tbl_Reset').style.display='none';
		}
		
function ProcessData(CourtNumber, CourtTime, MM, DD, YY, TicketsViolationID, RecordID, CourtID, BondFlag, TicketID)
		{
		
		    var room = document.getElementById('txt_CourtNumber');
		    room.value = CourtNumber;
		    document.form1.ddl_CourtTime.value = CourtTime;
		    document.form1.txt_MM.value =MM;
		    document.form1.txt_DD.value = DD;
		    document.form1.txt_YY.value = YY;
		    document.form1.hf_TicketViolationID.value = TicketsViolationID;
		    document.form1.hf_RecordID.value =RecordID;
		    document.form1.hf_CourtIDMain.value =CourtID;
		    document.form1.hf_BondFlagMain.value = BondFlag;
		    document.form1.hf_TicketIDMain.value = TicketID;
		
        }
        
        function ShowProgress()
        {
        
        document.getElementById("tbl_Data").style.display='none'
        document.getElementById("tbl_image").style.display='block'
        }
        
        function HidePanel()
        {
         document.getElementById('pnl_Update').style.display='none';
        }

    </script>
    
    <script type="text/javascript">
    
    function ValidateForm()
    {   
    if(document.getElementById('rb_Reset').checked==true)
    {
    var courtid = document.form1.hf_CourtIDMain.value;
    var courtnum =document.form1.txt_CourtNumber.value;
    var bflag =   document.form1.hf_BondFlagMain.value;
    var datetype = document.form1.ddl_QuickUpdate_Status.value;
    var strdatewithtime = document.form1.txt_MM.value + "/" +document.form1.txt_DD.value+"/" + document.form1.txt_YY.value + " " + document.form1.ddl_CourtTime.value;
    var varhours = document.form1.ddl_CourtTime.value;
    var strdate = document.form1.txt_MM.value + "/" +document.form1.txt_DD.value+"/" + document.form1.txt_YY.value;
        //a;
        if(document.form1.ddl_QuickUpdate_Status.value == '<-- Choose -->')
        {
        alert('Please select a valid court status');
        return false;
        }
        
        if(chkdate(strdate)==false)
        {
            alert('Invalid Date');
            return false;
        }
        
        if(document.form1.ddl_CourtTime.value=="<>")
        {
            alert('Please select a valid time');
            return false;
        }
        
        
        //Check For Court Date
			
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
			
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					//UpdateViolation.txt_Month.focus();
					return false;			
				}
			}

        if(courtid == "3001")
		{ 
			var courtno = courtnum * 1;
			if ((courtno < 0) || (courtno > 17)||(courtno > 12 && courtno < 15))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
		}			
			
	    if((courtid == "3002")||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}
				
				
				

    if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM" ) && (varhours != "10:30 AM" ) && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							//UpdateViolation.ddl_Time.focus();
							return false;
						}
						
						
						
						var bodflag=bflag;
						
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
							if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
							{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								//UpdateViolation.txt_CourtNo.focus();
								return false;
							}
						}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						//UpdateViolation.ddl_Status.focus();
						return false;
					}
				}

        var bdflag = bflag;
        if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (courtid != "3003") 
					{
						if ((courtnum == "18") ) 
						{
							alert("This court number can only be set for Dairy Ashford Court");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					if (courtid == "3003") 
					{
						if ((courtnum != "18") ) 
						{
							alert("Court number 18 can only be set for Dairy Ashford Court");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					}
					
				}
	
	            var courtnumber = document.form1.txt_CourtNumber.value;
				if (courtnumber.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the CourtNumber input box"); 
					document.form1.txt_CourtNumber.focus();
					return false;
				}
				if (courtnumber=="") 
				{
					 document.form1.txt_CourtNumber.value=0;
				}
				var ValidChars = "0123456789";
				var Char;
				for (j = 0; j < courtnumber.length; j++) 
				{ 
					Char = courtnumber.charAt(j); 
					if (ValidChars.indexOf(Char) == -1) 
					{
						alert("Invalid Court Number");
						document.form1.txt_CourtNumber.focus();
						return false;
					}
				}

    //Validating Status
    var status =document.form1.ddl_QuickUpdate_Status.value;
    
    if(status == "<>")
    {
        alert("Please select a valid status");
        return false;
    }
			closepopup();
			ShowProgress();
			return(true);
    }
    
    </script>
    <style type="text/css">
    .treetable {
    }
    
    .treetable th {
      
       FONT-SIZE: 17pt;
     
    
    }
    
    .treetable td {
      
    
    FONT-SIZE: 7pt;
    
    FONT-FAMILY: Tahoma;
    border-color:3366cc
    
    }
    
    
    a {
      text-decoration:none;
      color:#090;
    }
    
   
  </style>
   
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
        <tr>
        <td>
            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
        </td></tr>
        <tr>
    <td width="100%" background="../Images/separator_repeat.gif"  height="11"  >
    </td>
    </tr>
    <tr>
    <td> <ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif" Width="86px" EnableHideDropDown="True">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
        <TextboxLabelStyle CssClass="clsinputadministration" />
                    </ew:CalendarPopup>
        &nbsp;
        <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" 
            Text="Submit" OnClick="btn_Submit_Click" />
        <cc1:ModalPopupExtender
                        ID="ModalPopupExtender2" runat="server" TargetControlID="ibtn_Docs" PopupControlID="pnl_Dockets" CancelControlID="lbtn_Docket_Close" OnCancelScript="close_docket_popup()" HideDropDownList="false">
                    </cc1:ModalPopupExtender>
                    </td>
    </tr>
    <tr>
    <td width="100%" background="../Images/separator_repeat.gif"  height="11"  >
    </td>
        </tr>

            <tr>
                <td runat="server" align="right" class="clslabel" colspan="1" style="border-left: medium none;
                    height: 38px" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_sort" runat="server" visible="false">
                        <tr>
                            <td class="clslabel" style="height: 25px" colspan="2">
                    <asp:ImageButton ID="ibtn_Docs" runat="server" ImageUrl="~/Images/head_icon.gif" />
                    <asp:Label ID="lbl_Title" runat="server" Font-Bold="True"></asp:Label></td>
                            <td class="clslabel" style="width: 100px; height: 20px">
                            </td>
                            <td style="width: 100px; height: 20px">
                            </td>
                            <td align="right" colspan="2" style="height: 20px">
        <asp:LinkButton ID="lnkbtn_showall" runat="server" OnClick="lnkbtn_showall_Click">View All Closed Dockets with Discrepancies</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td class="clslabel" style="width: 100px; height: 20px">
                                Court Location</td>
                            <td style="width: 100px; height: 20px">
                                <asp:DropDownList ID="ddl_courts" runat="server" CssClass="clsinputadministration" AutoPostBack="True" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
                                </asp:DropDownList></td>
                            <td class="clslabel" style="width: 100px; height: 20px">
                                Auto Statuses</td>
                            <td style="width: 100px; height: 20px">
                                <asp:DropDownList ID="ddl_status" runat="server" CssClass="clsinputadministration" AutoPostBack="True" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
                                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                                    <asp:ListItem Value="1">DLQ/FTA</asp:ListItem>
                                    <asp:ListItem Value="2">Appearance</asp:ListItem>
                                    <asp:ListItem Value="3">Disposed</asp:ListItem>
                                    <asp:ListItem Value="4">NIR</asp:ListItem>
                                    <asp:ListItem Value="5">Blank</asp:ListItem>
                                </asp:DropDownList></td>
                            <td style="width: 100px; height: 20px">
                            </td>
                            <td style="height: 20px" align="right">
                                <asp:DropDownList ID="ddl_dockets" runat="server" CssClass="clsinputadministration" Width="150px" >
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
            </tr>
    <tr>
            <td align="right">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <table id="tbl_Data" width="100%" style="display:block" runat="server">
            <tr>
            <td>
            <asp:Repeater ID="rptrtable" runat="server" OnItemDataBound="rptOrder_ItemDataBound" OnItemCommand="rptrtable_ItemCommand" >
    <HeaderTemplate>
        <table width="780" id="table2" class="treetable" border="1" style="BORDER-COLLAPSE: collapse" bordercolor="#3366cc">
        <tr>
        <td class="ClsSubHead" style="width :10px"> 
        S.No
        </td>
        <td class="ClsSubHead" style="width :25px"> 
        
        </td>
        <td class="ClsSubHead" style="width :1px"> 
        
        </td>
        <td class="ClsSubHead" style="width :150px">
        Last Name, First Name
        </td>
        <td class="ClsSubHead" style="width :70px">
        Ticket No.
        </td>
        
        <td class="ClsSubHead" style="width :70px">
        Cause No.
        </td>
        <td style="width :18px">
        
        </td>
        <td class="ClsSubHead" style="width :200px">
        Snap Shot Status
        </td>
        <td class="ClsSubHead" style="width :200px">
        Auto Status
        </td>
        <td class="ClsSubHead" style="width :200px">
        Vefified Status
        </td>
        <td style="width :18px">
        
        </td>
               
        </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr runat="server" id="rowGroupHeader" valign="middle">
            <td colspan="7" valign="middle" style="border-right:none">
                <span runat="server" id="idClickable" >
                    <asp:Image ID="idImage" runat="server" ImageUrl="~/Images/collapse.gif" CssClass="button" Width="16" Height="16" />
                    <asp:Label ID="lblGroupName" runat="server" Font-Bold="true"></asp:Label></span></td>
                    
                    
        </tr>
        <tr runat="server" id="rowItem">
            <td >&nbsp;<a href="../ClientInfo/CaseDisposition.aspx?casenumber=<%# DataBinder.Eval(Container.DataItem, "ticketid_pk")%>&search=0" ><%# DataBinder.Eval(Container.DataItem, "blankspace")%>
                </a></td>
            <td >&nbsp; 
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/norgie_closed_dna.gif" /><asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/x_d.gif" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID")%>'  />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" TargetControlID="ImageButton1" PopupControlID="pnl_Update" runat="server" HideDropDownList="false"  > 
        </cc1:ModalPopupExtender>
              
                </td>
                <td style="width :0px"></td>
            <td  >&nbsp;<%# DataBinder.Eval(Container.DataItem, "LastName")%> ,<%# DataBinder.Eval(Container.DataItem, "FirstName") %><asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' /><asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' /> <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' /></td>
            <td  ><%# DataBinder.Eval(Container.DataItem, "TicketNumber")%>
                       
            </td>
            <td ><%# DataBinder.Eval(Container.DataItem, "CauseNo")%></td>
            <td>   <asp:CheckBox ID="cb_view" runat="server" />  </td>
             <td  style="width :150px"> 
             <asp:Label ID="lbl_sstatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus")%>'></asp:Label>
            </td>
            <td id="td_astatus" runat="server"  >
              <asp:Label ID="lbl_astatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AutoStatus")%>'></asp:Label>
            </td>
           
            <td  > 
             <asp:Label ID="lbl_vstatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus")%>'></asp:Label>
             <asp:HiddenField ID="hf_discrepency" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.discrepency") %>'/> 
             <asp:HiddenField ID="hf_showquestionmark" runat=server Value='<%# DataBinder.Eval(Container, "DataItem.showquestionmark") %>' />
             </td>
             <td>
                <asp:Image ID="img_dis" runat="server"/>
            </td>
            <td style="display :none">
            &nbsp;<asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'></asp:Label>
            <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'></asp:Label><asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain")%>' /> <asp:HiddenField ID="hf_MM" runat="server" /><asp:HiddenField ID="hf_DD" runat="server" /><asp:HiddenField ID="hf_YY" runat="server" />
            <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'></asp:Label> 
            <asp:HiddenField ID="hf_TicketsViolationID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>'/> <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID") %>'/> &nbsp;<%# DataBinder.Eval(Container.DataItem, "ShortDescription") %>
            <%# DataBinder.Eval(Container.DataItem, "MidNum") %>
            <%# DataBinder.Eval(Container.DataItem, "TrialComments") %>
              </td>
            </tr>
        <tr runat="server" id="RowRoomFooter">
            <td bgcolor="gray" colspan="15">
                &nbsp;</td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
            </td>
            </tr>
            <tr>
            <td>
            <asp:Repeater ID="rptr_table" runat="server" OnItemDataBound="rptOrder_ItemDataBound" OnItemCommand="rptrtable_ItemCommand" >
    <HeaderTemplate>
        <table width="780" id="table2" class="treetable" border="1" style="BORDER-COLLAPSE: collapse" bordercolor="#3366cc">
    </HeaderTemplate>
    <ItemTemplate>
        <tr runat="server" id="rowGroupHeader" valign="middle">
            <td colspan="7" valign="middle" style="border-right:none">
                <span runat="server" id="idClickable" >
                    <asp:Image ID="idImage" runat="server" ImageUrl="~/Images/collapse.gif" CssClass="button" Width="16" Height="16" />
                    <asp:Label ID="lblGroupName" runat="server" Font-Bold="true"></asp:Label></span></td>
                    
                    
        </tr>
        <tr runat="server" id="rowItem">
            <td style="width :10px" >&nbsp;<a href="../ClientInfo/CaseDisposition.aspx?casenumber=<%# DataBinder.Eval(Container.DataItem, "ticketid_pk")%>&search=0" ><%# DataBinder.Eval(Container.DataItem, "blankspace")%>
                </a></td>
            <td style="width :25px">&nbsp; 
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/norgie_closed_dna.gif" /><asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/x_d.gif" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID")%>'  />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" TargetControlID="ImageButton1" PopupControlID="pnl_Update" runat="server" HideDropDownList="false" > 
        </cc1:ModalPopupExtender>
                </td>
                <td style="width :1px" ></td>
            <td style="width :150px" >&nbsp;<%# DataBinder.Eval(Container.DataItem, "LastName") %> ,<%# DataBinder.Eval(Container.DataItem, "FirstName") %><asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' /><asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' /> <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' /></td>
           <td style="width :70px"><%# DataBinder.Eval(Container.DataItem, "TicketNumber")%>
                       
            </td>
            <td style="width :70px"><%# DataBinder.Eval(Container.DataItem, "CauseNo")%></td>
            <td style="width :18px">   <asp:CheckBox ID="cb_view" runat="server" />  </td>
                 <td  style="width :200px"> 
             <asp:Label ID="lbl_sstatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus")%>'></asp:Label>
            </td>
            <td id="td_astatus" runat="server"  style="width :200px">
              <asp:Label ID="lbl_astatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AutoStatus")%>'></asp:Label>
            </td>
           
            <td  style="width :200px"> 
             <asp:Label ID="lbl_vstatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus")%>'></asp:Label>
            </td>
             <td style="width :18px">
                <asp:Image ID="img_dis" runat="server" />
            </td>
            <td style="display :none" >&nbsp;<asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'></asp:Label>&nbsp;<asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'></asp:Label><asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>' /> <asp:HiddenField ID="hf_MM" runat="server" /><asp:HiddenField ID="hf_DD" runat="server" /><asp:HiddenField ID="hf_YY" runat="server" />
            &nbsp;<asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'></asp:Label> 
            <asp:HiddenField ID="hf_TicketsViolationID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>'/> <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID") %>'/> &nbsp;<%# DataBinder.Eval(Container.DataItem, "ShortDescription") %>
             <%# DataBinder.Eval(Container.DataItem, "MidNum") %>
            <%# DataBinder.Eval(Container.DataItem, "TrialComments") %>
             <asp:HiddenField ID="hf_discrepency" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.discrepency") %>'/> 
             <asp:HiddenField ID="hf_showquestionmark" runat=server Value='<%# DataBinder.Eval(Container, "DataItem.showquestionmark") %>' />
            </td>
            </tr>
        <tr runat="server" id="RowRoomFooter">
            <td bgcolor="gray" colspan="15">
                &nbsp;</td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
            </td>
            </tr>
            
            </table>
           <table width="785">
           
           <tbody>
           <tr>
           <td align="center">
            <table id="tbl_image"  style="display:none" runat="server">
                <tr >
                    <td align="center" class="clslabel" >
                        <img src="../Images/plzwait.gif" /><strong> Please wait while your request is being processed </strong>
                    </td>
                    </tr>
                
            </table>
           </td>
           </tr>
           </tbody>
           </table>
          
            
                </ContentTemplate>
                <Triggers>
                <asp:PostBackTrigger ControlID="btn_Update" />
                </Triggers>
                </asp:UpdatePanel>
            
            </td>
        </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btn_DisposeSelected" runat="server" CssClass="clsbutton" OnClick="btn_DisposeSelected_Click"
                        Text="Dispose Selected Cases" Visible="False" />
                    <asp:Button ID="btn_updatestatus" runat="server" CssClass="clsbutton" OnClick="btn_updatestatus_Click"
                        Text="Update Selected Verified Court Date with Auto" Visible="False" /></td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" colSpan="5" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                        <asp:CheckBox ID="chk_DisposeAll" runat="server" Text="Dispose 'All Open' HMC Cases" Visible="False"/><asp:Button
                            ID="btn_Dispose" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_Dispose_Click" OnClientClick="return dispose_validation()" Visible="False"  /></td>
            </tr>
            <tr>
                <td id="td_Title" runat="server" colspan="1" visible="false">
                   </td>
            </tr>
    <tr>
    <td id="td_HMC" runat="server" colspan="1" align="right" visible="false" style="border-left:none; height: 17px;">
              </td>
    </tr>
        </table>
         
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
        <asp:Panel ID="pnl_Update" runat="server" Height="105px" Width="450px" style="display:none"  >
                    
                    
                    <table id="tbl_updatepanel" width="100%" style="left: 500px; position: absolute;top: 500px;border-collapse : collapse;border-color :Navy;" border="2" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable">
              <tr>
                   <td style="width: 447px">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                  
                <tr>
                <td width="100%" align="right" background="../Images/subhead_bg.gif" height="34" class ="clssubhead" >
                    <asp:LinkButton ID="lbtn_close" runat="server"  OnClick="lbtn_close_Click" OnClientClick="return closepopup();">X</asp:LinkButton>&nbsp;</td>
                </tr>
                <tr>
                <td>
                <table id="tbl_options" width="100%">
                <tr>
                <td align="center">
                    <asp:RadioButton ID="rb_Disposed" Text = "DISPOSED" runat="server" GroupName="group1" onClick="DisableOptions();" Checked="True" />
                </td>
                <td align="center">
                    <asp:RadioButton ID="rb_Missed" Text = "MISSED" runat="server" GroupName="group1" onClick="DisableOptions();" />
                </td>
                <td align="center">
                    <asp:RadioButton ID="rb_Reset" Text = "RESET" runat="server" GroupName="group1" onClick="EnableOptions();"  />
                </td>
                </tr>
                </table>
                    <asp:HiddenField ID = "hf_TicketViolationID" runat="server" Value="" />
                    <asp:HiddenField ID = "hf_RecordID" runat="server" Value="" />
                    <asp:HiddenField ID = "hf_CourtIDMain" runat="server" Value="" />
                    <asp:HiddenField ID = "hf_BondFlagMain" runat="server" Value="" />
                    <asp:HiddenField ID = "hf_TicketIDMain" runat="server" Value="" />
                </tr>
                <tr>
                <td>
                <table id="tbl_Reset" width="100%" style="display:none">
                <tr>
                <td style="height: 50px" align="center">
                    &nbsp;<asp:DropDownList ID="ddl_QuickUpdate_Status" runat="server" CssClass="clsinputcombo">
                        <asp:ListItem>&lt;--Choose--&gt;</asp:ListItem>
                        <asp:ListItem Value="135">Bond</asp:ListItem>
                        <asp:ListItem Value="26">Jury</asp:ListItem>
                        <asp:ListItem Value="103">Judge</asp:ListItem>
                        <asp:ListItem Value="66">Pretrial</asp:ListItem>
                        <asp:ListItem Value="3">Arraignment</asp:ListItem>
                        <asp:ListItem Value="161">Non-Issue</asp:ListItem>
                        <asp:ListItem Value="27">Scire</asp:ListItem>
                        <asp:ListItem Value="147">Other</asp:ListItem>
                        <asp:ListItem Value="104">Waiting</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="height: 50px" align="center">
                    <asp:TextBox ID="txt_MM" onkeyup="return autoTab(this, 2, event)" runat="server" Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_DD" onkeyup="return autoTab(this, 2, event)" runat="server" Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_YY" runat="server" Width="25px"></asp:TextBox>
                </td>
                <td style="height: 50px" align="center">
                    &nbsp;<asp:DropDownList ID="ddl_CourtTime" runat="server" CssClass="clsinputcombo">
                        <asp:ListItem>&lt;&gt;</asp:ListItem>
                        <asp:ListItem>8:00 AM</asp:ListItem>
                        <asp:ListItem>8:15 AM</asp:ListItem>
                        <asp:ListItem>8:30 AM</asp:ListItem>
                        <asp:ListItem>8:45 AM</asp:ListItem>
                        <asp:ListItem>9:00 AM</asp:ListItem>
                        <asp:ListItem>9:15 AM</asp:ListItem>
                        <asp:ListItem>9:30 AM</asp:ListItem>
                        <asp:ListItem>9:45 AM</asp:ListItem>
                        <asp:ListItem>10:00 AM</asp:ListItem>
                        <asp:ListItem>10:15 AM</asp:ListItem>
                        <asp:ListItem>10:30 AM</asp:ListItem>
                        <asp:ListItem>10:45 AM</asp:ListItem>
                        <asp:ListItem>11:00 AM</asp:ListItem>
                        <asp:ListItem>11:15 AM</asp:ListItem>
                        <asp:ListItem>11:30 AM</asp:ListItem>
                        <asp:ListItem>11:45 AM</asp:ListItem>
                        <asp:ListItem>12:00 PM</asp:ListItem>
                        <asp:ListItem>12:15 PM</asp:ListItem>
                        <asp:ListItem>12:30 PM</asp:ListItem>
                        <asp:ListItem>12:45 PM</asp:ListItem>
                        <asp:ListItem>1:00 PM</asp:ListItem>
                        <asp:ListItem>1:15 PM</asp:ListItem>
                        <asp:ListItem>1:30 PM</asp:ListItem>
                        <asp:ListItem>1:45 PM</asp:ListItem>
                        <asp:ListItem>2:00 PM</asp:ListItem>
                        <asp:ListItem>2:15 PM</asp:ListItem>
                        <asp:ListItem>2:30 PM</asp:ListItem>
                        <asp:ListItem>2:45 PM</asp:ListItem>
                        <asp:ListItem>3:00 PM</asp:ListItem>
                        <asp:ListItem>3:15 PM</asp:ListItem>
                        <asp:ListItem>3:30 PM</asp:ListItem>
                        <asp:ListItem>3:45 PM</asp:ListItem>
                        <asp:ListItem>4:00 PM</asp:ListItem>
                        <asp:ListItem>4:15 PM</asp:ListItem>
                        <asp:ListItem>4:30 PM</asp:ListItem>
                        <asp:ListItem>4:45 PM</asp:ListItem>
                        <asp:ListItem>5:00 PM</asp:ListItem>
                        <asp:ListItem>5:15 PM</asp:ListItem>
                        <asp:ListItem>5:30 PM</asp:ListItem>
                        <asp:ListItem>5:45 PM</asp:ListItem>
                        <asp:ListItem>6:00 PM</asp:ListItem>
                        <asp:ListItem>6:15 PM</asp:ListItem>
                        <asp:ListItem>6:30 PM</asp:ListItem>
                        <asp:ListItem>6:45 PM</asp:ListItem>
                        <asp:ListItem>7:00 PM</asp:ListItem>
                        <asp:ListItem>7:15 PM</asp:ListItem>
                        <asp:ListItem>7:30 PM</asp:ListItem>
                        <asp:ListItem>7:45 PM</asp:ListItem>
                        <asp:ListItem>8:00 PM</asp:ListItem>
                        <asp:ListItem>8:15 PM</asp:ListItem>
                        <asp:ListItem>8:30 PM</asp:ListItem>
                        <asp:ListItem>8:45 PM</asp:ListItem>
                        <asp:ListItem>9:00 PM</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="height: 50px" align="center">
                    <asp:TextBox ID="txt_CourtNumber" runat="server" Width="28px"></asp:TextBox>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                        <tr>
                            <td align="center" width="100%">
                                <asp:CheckBox ID="chk_UpdateAll" runat="server" Text="Update all" /></td>
                        </tr>
                <tr><td align="center" width="100%">
                <asp:Button id="btn_Update" runat="server" Text="Update" CssClass="clsbutton" OnClick="btn_Update_Click" OnClientClick="return ValidateForm()"></asp:Button>
                </td>
                </tr>
                </td>
                </tr>
                </table>
             
                </table>
             
                </asp:Panel>
                </ContentTemplate>
                <Triggers>
                
                <asp:PostBackTrigger ControlID="btn_Update" />
                
                
                </Triggers>
        </asp:UpdatePanel>
        
            <asp:Panel ID="pnl_Dockets" runat="server" Height="50px"  style="display:none">
            <table width="100%" style="left: 400px; position: absolute;top: 400px;" border="1" class="clsLeftPaddingTable">
            <tr>
            <td width="100%" align="right" background="../Images/subhead_bg.gif" height="34" class ="clssubhead" >
                    <asp:LinkButton ID="lbtn_Docket_Close" runat="server">X</asp:LinkButton>&nbsp;</td>
            </tr>
            <tr>
            <td>
                <asp:GridView ID="gv_ScannedDockets" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable" OnRowDataBound="gv_ScannedDockets_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Docket Date">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtn_DocketDate" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dateofdoc", "{0:d}") %>'></asp:LinkButton>
                                <asp:Label id="lbl_docid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doc_id") %>' Visible="false">
													</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scan Date">
                            <ItemTemplate>
                                <asp:Label ID="lbl_ScanDate" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntered") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attorney">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Attorney" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Importance") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pages">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Pages" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DocNum") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                
                </asp:GridView>
            </td>
            </tr>
            <tr>
            <td>
            <asp:Label ID="lbl_Popup_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
            </td>
            </tr>
            </table>
            </asp:Panel>
        
        
    </div>
        
        
    </form>
</body>
</html>

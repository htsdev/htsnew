using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    //Fahad 2867 ( 02 - 09 - 08 )
    /// <summary>
    /// This page will create crystal report & display into the iframe.
    /// </summary>
    public partial class frmDocketReport : System.Web.UI.Page
    {
       
        

        int courtid = 0;
        int type = 0;
        DateTime datefrom;
        DateTime dateto;
        int alldates = 0;
        string status = String.Empty;
        int coverfirm = 0;
        int showbonds = 0;
        clsCrsytalComponent clscrystal = new clsCrsytalComponent();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            courtid = Convert.ToInt32(Request.QueryString["courtid"]);
            type = Convert.ToInt32(Request.QueryString["type"]);
            datefrom = Convert.ToDateTime(Request.QueryString["datefrom"]);
            dateto = Convert.ToDateTime(Request.QueryString["dateto"]);
            alldates = Convert.ToInt32(Request.QueryString["alldates"]);
            status = Convert.ToString(Request.QueryString["status"]);
            coverfirm = Convert.ToInt32(Request.QueryString["coverfirm"]);
            showbonds = Convert.ToInt32(Request.QueryString["showbonds"]);
            CreateReport();

        }
        //Fahad 2867 ( 02 - 09 - 08 )
        /// <summary>
        /// The Crystal report method that will create a pdf of crystal report
        /// </summary>
        public void CreateReport()
        {
           
           
                string filename = Server.MapPath("") + "\\DocketReport.rpt";
                string[] keys = { "@CourtID", "@Type", "@datefrom", "@dateto", "@AllDates", "@status", "@ShowOnlyBonds", "@CoverFirm" };
                object[] values = { courtid, type, datefrom, dateto, alldates, status, showbonds, coverfirm };
                clscrystal.CreateReport(filename, "usp_hts_Search_DocketReport", keys, values, this.Session, this.Response);
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using lntechNew;
using FrameWorkEnation.Components;
using HTP.Components;
using System.Text.RegularExpressions;


namespace HTP.Reports
{
    /// <summary>
    /// Summary description for frmBatchReport.
    /// </summary>
    public partial class frmBatchReport : System.Web.UI.Page
    {

        clsBatch batch = new clsBatch();
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsSession ClsSession = new clsSession();
        protected System.Web.UI.WebControls.Label lbl_message;
        clsENationWebComponents ClsDB = new clsENationWebComponents();



        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //Zeeshan Ahmed 3486 04/02/2008
                    BatchLetterType batchLetterType = (BatchLetterType)Convert.ToInt32(Session["Lettertype"]);

                    Session["updatebatch"] = "true";
                    //Aziz 3220 02/22/08 Changed to session from queryString
                    int EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    String LetterType = Session["Lettertype"].ToString();
                    String TicketID = Session["bTicketID"].ToString();
                    String TicketIDBatch = Session["bTicketIDBatch"].ToString();
                    BatchReport batchReport = batch.getReportName(batchLetterType);
                    //Yasir Kamal 7590 03/29/2010 trial letter modifications
                    String Batch = "True";

                    //if (batchLetterType == BatchLetterType.MissedCourtLetter)
                    //{
                    //    Hashtable ticketTable = new Hashtable();
                    //    string[] arr = Regex.Split(TicketID, ",");
                    //    for (int i = 0; i < arr.Length - 1; i++)
                    //    {
                    //        ticketTable.Add(arr[i], arr[i]);
                    //    }
                    //    clsDocketCloseOut docketCloseOut = new clsDocketCloseOut();
                    //    docketCloseOut.EmpId = EmpID;
                    //    docketCloseOut.SendMissedCourtLetterToBatch(ticketTable, BatchLetterType.MissedCourtLetter);

                    //}

                    //Create Preview File
                    string[] key1 = { "@empid", "@LetterType", "@TicketIDList"};
                    object[] value1 = { EmpID, Convert.ToInt32(batchLetterType), TicketIDBatch};

                    if (LetterType == "2")
                    {
                        Cr.CreateReport(Server.MapPath("") + "\\" + batchReport.ReportName, batchReport.ReportProcedure, key1, value1, this.Session, this.Response, LetterType, TicketIDBatch, Batch, TicketID);
                    }
                    else
                    {
                        Cr.CreateReport(Server.MapPath("") + "\\" + batchReport.ReportName, batchReport.ReportProcedure, key1, value1, this.Session, this.Response);

                        //Update Batch Information
                        batch.updateBatchPrintInformation(batchLetterType, TicketID, EmpID, TicketIDBatch);
                    }
                }

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }



        private void DeleteBatch()
        {

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion


    }

}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;

using lntechNew.Components;
using System.IO;

using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;

namespace lntechNew.Reports
{
  
    public partial class frmPrintAllBonds : System.Web.UI.Page
    {
        string TicketIDs = "";
        string[] Array;
        DataTable dt = new DataTable();

        clsCrsytalComponent Cr = new clsCrsytalComponent();
            
            clsSession uSession = new clsSession();
        int empid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
                empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                if (Session["TicketID4Bonds"] != null)
                {
                    TicketIDs = Session["TicketID4Bonds"].ToString();
                    Session["TicketID4Bonds"] = null;
                    generateReport(TicketIDs);
                }
                
            }
        }
        private void generateReport(string ticketid)
        {
            try
            {

                string TempPath = "";

                string picDestination = "";
                Array = ticketid.Split(',');
                Hashtable ht = new Hashtable();
                string filename = Server.MapPath("") + "\\Bond.rpt";
                string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "PRINTALLBONDS_"+1  + ".pdf";
                string exportedFile = "PRINTALLBONDS_1.pdf";
                if (Array.Length > 1)
                {
                    for (int i = 0; i < Array.Length - 1; i++)
                    {
                        if (!ht.ContainsKey(Array[i]))
                            ht.Add(Array[i], Array[i]);
                    }

                    object[] objArray = new object[ht.Keys.Count];
                    ht.Keys.CopyTo(objArray, 0);

                    if (objArray.Length > 1)
                    {
                        for (int i = 0; i < objArray.Length; i++)
                        {
                            string[] keyMain = { "@TicketIDList", "@empid" };
                            object[] valueMain = { Convert.ToInt32(objArray[i]), Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };
                            string[] keySub = { "@TicketID" };
                            object[] valueSub = { Convert.ToInt32(objArray[i]) };

                            TempPath = ViewState["FilePath"].ToString() + "PRINTALLBONDS" + "_Temp_" + i + ".pdf";
                            Cr.CreateBondReport(filename, "sp_BondLetter", "USP_HTS_LOR", keyMain, valueMain, keySub, valueSub, this.Session, this.Response, TempPath);
                        }
                        string searchpat = "*" + "PRINTALLBONDS_TEMP_" + "*.pdf";
                        string[] fileName = Directory.GetFiles(ViewState["FilePath"].ToString(), searchpat);

                        if (fileName.Length > 1)
                        {
                            fileName = sortfilesbydate(fileName);
                        }
                        
                        picDestination = ViewState["FilePath"].ToString() + exportedFile;

                        Doc doc1 = new Doc();
                        Doc doc2 = new Doc();

                        if (fileName.Length > 1)
                        {
                            for (int i = 0; i < fileName.Length; i++)
                            {

                                doc2.Read(fileName[i].Replace("\\\\", "\\").ToString());
                                doc1.Append(doc2);
                            }

                            if (System.IO.File.Exists(picDestination))
                                System.IO.File.Delete(picDestination);
                            doc1.Save(picDestination.Replace("\\\\", "\\"));
                            
                            doc2.Dispose();
                            doc1.Dispose();
                            for (int j = 0; j < fileName.Length; j++)
                            {
                                System.IO.File.Delete(fileName[j].ToString());
                            }

                            //Response.Redirect("C://Docstorage" + fPath, false);
                            // Rab Nawaz Khan 02/19/2015 Resolved the pdf not load issue. . .
                            Response.Buffer = false; //transmitfile self buffers
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Disposition", "filename="+exportedFile);
                            Response.TransmitFile(picDestination); //transmitfile keeps entire file from loading into memory
                            Response.End();

                        }
        
                    }
                    if (objArray.Length == 1)
                    {
                        CreateBondReport(objArray[0].ToString());
                    }


                    
                }

                

               

                
            }
            catch (Exception ex)
            {
                Response.Write("Report Cannot be Loaded!");
            }
        }
        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }
        
        private  void CreateBondReport(string ticketno)
        {
            string[] keyMain = { "@TicketIDList", "@empid" };
            object[] valueMain = { Convert.ToInt32(ticketno), Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };
            string[] keySub = { "@TicketID" };
            object[] valueSub = { Convert.ToInt32(ticketno) };

            string filename = Server.MapPath("") + "\\Bond.rpt";
            Cr.CreateBondReport(filename, "sp_BondLetter", "USP_HTS_LOR", keyMain, valueMain, keySub, valueSub, "false", 4, this.Session, this.Response);
        }
    }
}

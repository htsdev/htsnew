using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using System.Threading;
using System.IO;

namespace HTP.Reports
{
    public partial class DocketReport : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCourts ClsCourt = new clsCourts();
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();
        clsLogger bugTracker = new clsLogger();
        clsFirms ClsFirms = new clsFirms();
        clsCrsytalComponent clscrystal = new clsCrsytalComponent();
        DataView dv;

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    Session["DS"] = null;
                    dtp_From.SelectedDate = DateTime.Today;
                    dtp_To.SelectedDate = DateTime.Today;
                    FillFirms(ddl_firms);
                    FillCourts();
                    FillCourtStatuses();
                    FillGrid();
                    

                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }
            Pagingctrl.PageIndexChanged +=new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);

        }

        private void FillCourts()
        {
            try
            {
                DataSet dsCourts = ClsCourt.GetAllActiveCourtName();
                ddl_Courthouse.Items.Clear();
                ddl_Courthouse.Items.Add(new ListItem("All Courts", "1"));
                ddl_Courthouse.Items.Add(new ListItem("Houston Municipal Court", "2"));
                ddl_Courthouse.Items.Add(new ListItem("All Outside Courts", "3"));
                ddl_Courthouse.Items.Add(new ListItem("All Criminal Courts", "4"));

                foreach (DataRow dr in dsCourts.Tables[0].Rows)
                {
                    if (dr["courtid"].ToString() == "0" || dr["courtid"].ToString() == "3001" || dr["courtid"].ToString() == "3002" || dr["courtid"].ToString() == "3003")
                        continue;
                    ddl_Courthouse.Items.Add(new ListItem(dr["shortcourtname"].ToString(), dr["courtid"].ToString()));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        private void FillCourtStatuses()
        {
            try
            {
                ddl_Status.Items.Clear();

                ddl_Status.Items.Add(new ListItem("OPEN CASES", "-1"));
                ddl_Status.Items.Add(new ListItem("APPEARANCE DATES", "-2"));

                //DataSet dsStatuses = ClsCaseStatus.GetAllCaseStatus();
                DataSet dsStatuses = ClsCaseStatus.GetDispositionCaseStatus();

                foreach (DataRow dr in dsStatuses.Tables[0].Rows)
                {
                    ddl_Status.Items.Add(new ListItem(dr["Description"].ToString(), dr["id"].ToString()));
                }

                ddl_Status.Items.Add(new ListItem("---------------", "-5"));
                ddl_Status.Items.Add(new ListItem("QUOTE", "-3"));
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void FillGrid()
        {
            try
            {
                DataTable dtRecords = GetRecords();//ClsDb.Get_DT_BySPArr("usp_hts_Search_DocketReport", keys, values);
                if (dtRecords.Rows.Count == 0)
                {
                    lbl_Message.Text = "No records!";
                }
                //Kazim 3164 2/27/2008 Change code for correct sorting
                if (Session["DS"] == null)
                {
                    dv = new DataView(dtRecords);
                    Session["DS"] = dv;
                }
                else
                {
                    dv = (DataView)Session["DS"];
                    string SortExp = dv.Sort;
                    dv = new DataView(dtRecords);
                    dv.Sort = SortExp;
                }
                hf_RecordCount.Value = dtRecords.Rows.Count.ToString();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtRecords.Copy());
                Session["curDT"] = dtRecords;
                gv_Result.DataSource = dv;
                gv_Result.DataBind();
                Pagingctrl.PageCount = gv_Result.PageCount;
                Pagingctrl.PageIndex = gv_Result.PageIndex;
                Pagingctrl.SetPageIndex();
                //GenerateSerial();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private DataTable GetRecords()
        {
            string[] keys = { "@CourtID", "@Type", "@datefrom", "@dateto", "@AllDates", "@status", "@ShowOnlyBonds", "@CoverFirm" };
            object[] values = { ddl_Courthouse.SelectedValue, ddl_CourtDate.SelectedValue, dtp_From.SelectedDate, dtp_To.SelectedDate, Convert.ToInt16(chk_AllDates.Checked), ddl_Status.SelectedValue, chk_ShowOnlyBonds.Checked, Convert.ToInt32(ddl_firms.SelectedValue) };

            //Generating serial numbers
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_hts_Search_DocketReport", keys, values);
            DataColumn dc = new DataColumn("S No", typeof(int));
            dt.Columns.Add(dc);
            dc.SetOrdinal(0);

            int no = 1;
            string ticketid = "";

            foreach (DataRow dr in dt.Rows)
            {
                string tid = dr["TicketID_PK"].ToString();
                if (tid != "")
                {
                    if (no == 1)
                    {
                        ticketid = tid;
                        dr["S No"] = no;
                        no = no + 1;
                    }
                    else
                    {
                        if (ticketid != tid)
                        {
                            dr["S No"] = no;
                            ticketid = tid;
                            no = no + 1;
                        }
                        else
                        {
                            dr["S No"] = no - 1;
                        }

                    }

                }
            }
            //end

            return dt;
        }

        private DataView GetRecords(string sd, string sortExpression)
        {

            DataTable dt = GetRecords();

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + " " + sd;

            return dv;
        }

        protected void Button_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SortDirection"] = null;
                Session["SortExpression"] = null;
                Session["DS"] = null;
                Session["curDT"] = null;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenerateSerial()
        {

            try
            {

                DataTable dt = (DataTable)Session["curDT"];
                DataColumn dc = new DataColumn("S No", typeof(int));
                dt.Columns.Add(dc);
                dc.SetOrdinal(0);

                int no = 1;
                string ticketid = "";
                foreach (GridViewRow ItemX in gv_Result.Rows)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("hf_SNo");
                    Label tid = (Label)ItemX.FindControl("lbl_Ticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            dt.Rows[ItemX.RowIndex]["S No"] = no;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                dt.Rows[ItemX.RowIndex]["S No"] = no;
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
                Session["curDT"] = dt;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgExp_Ds_to_Excel_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_session = new DataTable();
            DataTable dt_exp = new DataTable();
            try
            {
                dt_session = (DataTable)Session["curDT"];
                dt_exp = dt_session.Copy();
                dt_exp.Columns.Remove("TicketID_PK");
                dt_exp.Columns.Remove("ActiveFlag");
                dt_exp.Columns.Remove("ticketsviolationid");
                cmpDataGridToExcel.DataTableToExcel(dt_exp, this.Response);

            }

            catch (Exception ex)
            {

            }



        }



        protected void imgPrint_Ds_to_Printer_Click(object sender, ImageClickEventArgs e)
        {
            string URL = "./PrintDocketReport.aspx?CourtHouse=" + ddl_Courthouse.SelectedValue + "&DateType=" + ddl_CourtDate.SelectedValue + "&From=" + dtp_From.SelectedDate.ToShortDateString() + "&To=" + dtp_To.SelectedDate.ToShortDateString() + "&AllDates=" + Convert.ToInt16(chk_AllDates.Checked).ToString() + "&Status=" + ddl_Status.SelectedValue;

            if (Session["SortDirection"] != null && Session["SortExpression"] != null)
            {

                URL += "&SortDirection=" + Session["SortDirection"].ToString() + "&SortExpression=" + Session["SortExpression"].ToString();
            }

            Response.Redirect(URL, false);
            //Response.Redirect("./PrintDocketReport.aspx?CourtHouse=" + ddl_Courthouse.SelectedValue + "&DateType=" + ddl_CourtDate.SelectedValue + "&From=" + dtp_From.SelectedDate.ToShortDateString() + "&To=" + dtp_To.SelectedDate.ToShortDateString() + "&AllDates=" + Convert.ToInt16(chk_AllDates.Checked).ToString() + "&Status=" + ddl_Status.SelectedValue);
        }





        protected void SendMail(object sender, ImageClickEventArgs e)
        {
            SendMail();
        }
        // Fahad 2867 (02-09-08)
        /// <summary>
        /// This method will generate pdf of crystal report & send mail.
        /// </summary>
        public void SendMail()
        {
            try
            {
                string filename = Server.MapPath("") + "\\DocketReport.rpt";
                string name = "DocketReport-" + DateTime.Now.ToFileTime() + ".pdf";
                string path = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                string[] keys = { "@CourtID", "@Type", "@datefrom", "@dateto", "@AllDates", "@status", "@ShowOnlyBonds", "@CoverFirm" };
                object[] values = { ddl_Courthouse.SelectedValue, ddl_CourtDate.SelectedValue, dtp_From.SelectedDate, dtp_To.SelectedDate, Convert.ToInt16(chk_AllDates.Checked), ddl_Status.SelectedValue, chk_ShowOnlyBonds.Checked, Convert.ToInt32(ddl_firms.SelectedValue) };
                string docketreportpath = clscrystal.CreateReport(filename, "usp_hts_Search_DocketReport", keys, values, HttpContext.Current.Response, path, name.ToString());
                lntechNew.Components.MailClass.SendMailToAttorneys("Docket Report", "Docket Report", docketreportpath.ToString(), hf_Emails.Value);
                FileInfo fi = new FileInfo(docketreportpath.ToString());
                fi.Delete();
                lbl_Message.Text = "Email Sent Successfully";

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = "Email Failed Due To Following Exception : " + ex.Message.ToString();

            }
        }
        private void FillFirms(DropDownList ddl_FirmAbbreviation)
        {
            try
            {
                DataSet ds_Firms = ClsFirms.GetActiveFirms(0);
                if ((ds_Firms.Tables.Count > 0) && (ds_Firms.Tables[0].Rows.Count > 0))
                {
                    ddl_FirmAbbreviation.DataSource = ds_Firms.Tables[0];
                    ddl_FirmAbbreviation.DataBind();
                    ddl_FirmAbbreviation.Items.Insert(0, new ListItem("-ALL Firms-", "0"));
                    ddl_FirmAbbreviation.SelectedValue = "0";
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        protected void gv_Result_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Result.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        void Pagingctrl_PageIndexChanged()
        {
            gv_Result.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }

        public SortDirection GridViewSortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)

                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {


            dv = (DataView)Session["DS"];
            dv.Sort = sortExpression + " " + direction;
            Session["DS"] = dv;

            FillGrid();
            Pagingctrl.PageCount = gv_Result.PageCount;
            Pagingctrl.PageIndex = gv_Result.PageIndex;

            ViewState["sortExpression"] = sortExpression;
            Session["SortDirection"] = direction;
            Session["SortExpression"] = sortExpression;

        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {

            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;

                SortGridView(sortExpression, "DESC");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;

                SortGridView(sortExpression, "ASC");

            }

        }

        protected void gv_Result_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex > 0)
                {
                    GridViewRow gvr = gv_Result.Rows[e.Row.RowIndex - 1];
                    HyperLink hl_prevSno = (HyperLink)gvr.FindControl("hl_SNo");
                    HyperLink hl_currSno = (HyperLink)e.Row.FindControl("hl_SNo");

                    if (hl_currSno != null && hl_prevSno != null)
                    {
                        if (hl_prevSno.Text == hl_currSno.Text)
                        {
                            //hl_currSno.Attributes.Add("Display", "none");
                            hl_currSno.Style[HtmlTextWriterStyle.Display] = "none";
                        }
                    }




                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string hireDate = ((Label)e.Row.FindControl("lbl_Hire")).Text;
                    if (hireDate.Length == 10)
                    {
                        DateTime hDate = Convert.ToDateTime(hireDate);
                        ((Label)e.Row.FindControl("lbl_Hire")).Text = hDate.ToString("MM/dd/yy");
                    }

                    string UpdateDate = ((Label)e.Row.FindControl("lbl_Update")).Text;
                    if (UpdateDate.Length == 10)
                    {
                        DateTime upDate = Convert.ToDateTime(UpdateDate);
                        ((Label)e.Row.FindControl("lbl_Update")).Text = upDate.ToString("MM/dd/yy");
                    }

                    string dOb = ((Label)e.Row.FindControl("lbl_DOB")).Text;
                    if (dOb.Length == 10)
                    {
                        DateTime doDate = Convert.ToDateTime(dOb);
                        ((Label)e.Row.FindControl("lbl_DOB")).Text = doDate.ToString("MM/dd/yy");
                    }

                    string CrtDate = ((Label)e.Row.FindControl("lbl_CourtDate")).Text;
                    if (CrtDate.Length == 10)
                    {
                        DateTime cDate = Convert.ToDateTime(CrtDate);
                        ((Label)e.Row.FindControl("lbl_CourtDate")).Text = cDate.ToString("MM/dd/yy");
                    }

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
        }



    }
}

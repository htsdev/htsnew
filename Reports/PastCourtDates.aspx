<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PastCourtDates.aspx.cs"
    Inherits="HTP.Reports.PastCourtDates" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Past Court Date (HMC) Alert</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                    border="0">
                    <tr>
                        <td style="width: 896px; height: 118px">
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr class="clsLeftPaddingTable">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" style="height: 25px">
                                        <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of Business Days after case reset:"
                                            runat="server" Attribute_Key="Days" />
                                    </td>
                                    <td align="right" style="height: 25px">
                                        <asp:CheckBox ID="cb_ShowAll" runat="server" Text="Show All" CssClass="clssubhead"
                                            OnCheckedChanged="cb_ShowAll_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                            <table>
                                <tr>
                                    <td colspan="2" style="text-align: right;" valign="middle">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                        CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="DG_pastcases" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="DG_pastcases_PageIndexChanging"
                                AllowSorting="True" OnSorting="gv_Records_Sorting" CellPadding="0" CellSpacing="0"
                                OnRowCommand="DG_pastcases_RowCommand" OnRowDataBound="DG_pastcases_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="<u>S.No</u>" SortExpression="sno">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                Text='<%# Eval("sno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="ticketnumber">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CaseNumber" runat="server" Text='<%# Eval("ticketnumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="fname">
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" Text='<%# Eval("fname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lname">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" Text='<%# Eval("lname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Crt DateTime</u>" SortExpression="CourtDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# Eval("courtdatemain","{0:g}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Crt No</u>" SortExpression="courtnumber">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_courtnumber" runat="server" Text='<%# Eval("courtnumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Crt. Loc</u>" SortExpression="shortname">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtNum" runat="server" Text='<%# Eval("shortname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Trial Category</u>" SortExpression="status">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Follow Update</u>" SortExpression="PastCourtDateFollowUpDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("PastCourtDateFollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add" CommandName="ShowFolloupdate"
                                                runat="server" />
                                            <asp:HiddenField ID="hf_Courtid" runat="server" Value='<%#Eval("CourtID") %>' />
                                            <asp:HiddenField ID="hf_causenumber" runat="server" Value='<%#Eval("causenumber") %>' />
                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" height="11px" style="width: 896px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Past Court Date HMC Follow Up Date" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                            PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                    </ContentTemplate>
                </aspnew:UpdatePanel>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="cb_ShowAll" />
        </Triggers>
    </aspnew:UpdatePanel>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

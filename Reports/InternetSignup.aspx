<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternetSignup.aspx.cs"
    Inherits="HTP.Reports.InternetSignup" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc2" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Online Internet Signup</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png" />
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png" />
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png" />
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png" />
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />


    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
     <style type="text/css">
        .style2 {
        }

        .style3 {
            width: 13px;
        }

        .style5 {
            width: 100px;
            direction: ltr;
        }

        table .table td {
            background-color: #fff !important;
            color: black !important;
        }

            table .table td a {
                color: blue !important;
            }
             table .table td span {
                background-color: #fff !important;
            }
            table .table td .form-label {
                color: black !important;
                font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
                font-size: 11px !important;
                font-weight:normal !important;
            }
    </style>
    <%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 22px;
        }
    </style>--%>
</head>
<body>
    <form id="Form1" method="post" runat="server">


        <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="ScriptManager2" runat="server" />

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>


            <section id="main-content" class="reminderCallsPage" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">

            <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" Width="320px" CssClass="form-label" ForeColor="Red"
                                            EnableViewState="False"></asp:Label>
                 </div>
           
        </div>

             <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Online Internet Signup</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>

               <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">Start Date :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <picker:datepicker id="datefromnew" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                                    <%-- <ew:CalendarPopup ID="datefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="82px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                     <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">End Date :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <picker:datepicker id="datetonew" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                                   <%-- <ew:CalendarPopup ID="dateto" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="85px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">Call back status :	</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <asp:DropDownList ID="ddlContactType" runat="server" CssClass="clsInputadministration"
                                    Width="100px">
                                    <asp:ListItem Value="0">--- All --</asp:ListItem>
                                    <asp:ListItem Value="1">Contacted</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">Not Contacted</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>
                    
                     <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                   <asp:CheckBox ID="cb_alldates" runat="server" CssClass="checkbox-custom" Text="All Dates" Checked="True" />
                                    </div>
                                                                </div>
                         </div>
                     
                    </div>
                     <div class="row">
                         <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"> </label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary pull-right"></asp:Button>
                                    
                                    </div>
                                                                </div>
                         </div>
                     </div>
                     </div>

                 </section>


             <div class="clearfix"></div>


            <aspnew:UpdatePanel ID="upnlResult" runat="server">
                        <ContentTemplate>
            <section class="box" id="TableGrid" style="">
                <header class="panel_header" id="FlagsE">
                     <h2 class="title pull-left"></h2>

                     <div class="actions panel_actions pull-right">
                      <uc3:PagingControl ID="Pagingctrl" runat="server" />
                     <%--<a class="box_toggle fa fa-chevron-down" style="float: right;"></a>--%>
                     
                     
                                              
                         
                    
                </div>
            </header>
                     
                

                 <div class="content-body">
                <div class="row">

                    
                    <table width="100%">
                        <tr>
                            <td id="tdData" runat="server" visible="true">

                         
                    <div class="col-md-12" id="" >
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls" >
                                    <div class="table-responsive" data-pattern="priority-columns">


                                         <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped"
                        OnRowDataBound="gv_records_RowDataBound" Width="100%" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                        PageSize="20" AllowSorting="True" OnSorting="gv_records_Sorting">
                        <PagerSettings PreviousPageText="&amp;lt; Previous " Mode="NextPreviousFirstLast"
                            LastPageText="Last &amp;gt;&amp;gt;" FirstPageText="&amp;lt;&amp;lt; First" NextPageText="Next &amp;gt; ">
                        </PagerSettings>
                        <Columns>
                            <asp:TemplateField HeaderText="S#">
                                <HeaderStyle Width="10%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hl_sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbtn_lname" runat="server" CommandArgument='<%# "../DOCSTORAGE/ScanDoc/images/" + DataBinder.Eval(Container, "DataItem.FileName") %>'
                                        Text='<%# Eval("LastName") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("firstname") %>' CssClass="clsLeftPaddingTable"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="recdate" HeaderText="Signup Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_recdate" runat="server" Text='<%# Eval("recdate","{0:d}") %>'
                                        CssClass="clsLeftPaddingTable">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="recdate" HeaderText="Signup Time">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_rectime" runat="server" Text='<%# Eval("recdate","{0:t}") %>'
                                        CssClass="clsLeftPaddingTable">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>

                                        </div>
                                        <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                    
                                    </div>
                                                                </div>
                         </div>
   </td>
                        </tr>
                        </table>


                    </div>
                     </div>

                </section>
                            
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" 
                                            AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server" 
                                                    CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>

                            </ContentTemplate>
                </aspnew:UpdatePanel>

             <div class="clearfix"></div>

               <section class="box" id="TableComment" style="display:none;">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:TextBox ID="txt_totalrecords" runat="server">
                                </asp:TextBox>
                                    
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>
            </section>
                   </section>


        </div>
         <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
        <script type="text/javascript">
            $('.datepicker').datepicker({ autoclose: true });
            $(document).ready(function () {
                $("#mainmenu li, #limatter").attr("class", "");
                $("#ulmatter").css("display", "none");
                $("#livalidtion").attr("class", "open");
                $("#ulvalidtion").css("display", "block");
                $("#ActiveMenu1_Ab53").addClass("active");
                $("#ActiveMenu1_Ab53 > a li > span:nth-child(2)").addClass("arrow open");

            });
        </script>

    <%--<table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
        <tbody>
            <tr>
                <td>
                    <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td id="tr_pagging" class="clsLeftPaddingTable" align="center" runat="server" valign="middle">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr class="clsLeftPaddingTable">
                            <td class="clssubhead" style="width: 70px;" align="left">
                                Start Date :
                            </td>
                            <td style="width: 110px;" align="left">
                                <ew:CalendarPopup ID="datefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="82px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clssubhead" style="width: 60px;" align="left">
                                End Date :
                            </td>
                            <td align="left" style="width: 110px;">
                                <ew:CalendarPopup ID="dateto" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="85px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 100px">
                                <span class="clssubhead">Call back status :</span>
                            </td>
                            <td align="left" style="width: 110px">
                                &nbsp;<asp:DropDownList ID="ddlContactType" runat="server" CssClass="clsInputadministration"
                                    Width="100px">
                                    <asp:ListItem Value="0">--- All --</asp:ListItem>
                                    <asp:ListItem Value="1">Contacted</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">Not Contacted</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_alldates" runat="server" CssClass="clssubhead" Text="All Dates"
                                    Checked="True" />
                            </td>
                            <td style="width: 68px;" align="center">
                                <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" Text="Search" OnClick="btn_search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="img_pagging" runat="server">
                <td background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        OnRowDataBound="gv_records_RowDataBound" Width="100%" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                        PageSize="20" AllowSorting="True" OnSorting="gv_records_Sorting">
                        <PagerSettings PreviousPageText="&amp;lt; Previous " Mode="NextPreviousFirstLast"
                            LastPageText="Last &amp;gt;&amp;gt;" FirstPageText="&amp;lt;&amp;lt; First" NextPageText="Next &amp;gt; ">
                        </PagerSettings>
                        <Columns>
                            <asp:TemplateField HeaderText="S#">
                                <HeaderStyle Width="10%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hl_sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbtn_lname" runat="server" CommandArgument='<%# "../DOCSTORAGE/ScanDoc/images/" + DataBinder.Eval(Container, "DataItem.FileName") %>'
                                        Text='<%# Eval("LastName") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("firstname") %>' CssClass="clsLeftPaddingTable"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="recdate" HeaderText="Signup Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_recdate" runat="server" Text='<%# Eval("recdate","{0:d}") %>'
                                        CssClass="clsLeftPaddingTable">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="recdate" HeaderText="Signup Time">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_rectime" runat="server" Text='<%# Eval("recdate","{0:t}") %>'
                                        CssClass="clsLeftPaddingTable">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td height="11">
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>--%>
    </form>
</body>
</html>

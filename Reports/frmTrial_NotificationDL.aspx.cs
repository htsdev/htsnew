using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
	/// <summary>
    /// Summary description for frmTrial_NotificationDL.
	/// </summary>
    public partial class frmTrial_NotificationDL : System.Web.UI.Page
	{		
		clsCrsytalComponent ClsCr = new clsCrsytalComponent();
		clsLogger clog = new clsLogger();
		clsSession uSession = new clsSession();
		int TicketIDList;
        int SelectedReport;
		int empid;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			TicketIDList = Convert.ToInt32(Request.QueryString["casenumber"]);
            SelectedReport = Convert.ToInt32(Request.QueryString["reportnumber"]);
			empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));
			Create_TrialNotification_Report();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void Create_TrialNotification_Report()
		{			
			try
			{
                int violationNumber = (SelectedReport == 1) ? 16160 : 18534;
                string[] key2 = { "@TicketID", "@EmployeeID", "@ViolationNumber" };
                object[] value12 = { TicketIDList, empid, violationNumber };

                Int32 LetterType = (SelectedReport == 1) ? 16 : 17;				
				string filename  = Server.MapPath("") + "\\Trial_Letter_DL.rpt";
                ClsCr.CreateReport(filename,"USP_HTP_TrialletterDL", key2, value12,"false",LetterType,this.Session, this.Response);
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}		
	}
}

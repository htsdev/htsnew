using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.Reports
{
      
    public partial class frmLORRpts : System.Web.UI.Page
    {
        clsSession uSession = new clsSession();
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsENationWebComponents cls_db = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {
            string[] key ={"@Mode"} ;
            object[] val = { Convert.ToString(Request.QueryString["Mode"]) };
            Cr.CreateReport(Server.MapPath("") + "\\LOR_Rpt.rpt", "USP_HTS_LOR_Rpt", key , val , this.Session, this.Response);
            //Mode = Convert.ToString(Request.QueryString["Mode"]);
            //Response.Write(Mode);  
        }
    }
}

﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    /// <summary>
    /// PastDueCallsFamily event.
    /// </summary>
    public partial class PastDueCallsFamily : System.Web.UI.Page
    {
        #region Variables

        readonly PaymentPlan _clspaymentplan = new PaymentPlan();
        DataView _dv;

        #endregion

        #region Properties

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        #endregion

        #region Events

        /// <summary>
        /// Page_Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Pagingctrl.Size = lntechNew.WebControls.RecordsPerPage.Twenty;
                    Fillgrid();

                }
                Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }
        }
        /// <summary>
        /// Gridview row index change event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_records.PageIndex = e.NewPageIndex;
                Fillgrid();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Gridview Sorting event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method used to fetch the data which will display on the Gridview.
        /// </summary>
        private void Fillgrid()
        {
            var dt = _clspaymentplan.GetPastDueReports(4);
            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No Records Found";
                Pagingctrl.Visible = false;
            }
            else
            {
                lblMessage.Text = string.Empty;
                Pagingctrl.Visible = true;
                if (Session["DV_PastDueCallFamily"] == null)
                {
                    _dv = new DataView(dt);
                    Session["DV_PastDueCallFamily"] = _dv;
                }
                else
                {
                    _dv = (DataView)Session["DV_PastDueCallFamily"];
                    var sortExp = _dv.Sort;
                    _dv = new DataView(dt) {Sort = sortExp};
                    dt = _dv.ToTable();
                    if (dt.Columns.Contains("SNo"))
                    {
                        dt.Columns.Remove("SNo");
                    }
                }


                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    var sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (var i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["TicketID_PK"].ToString() != dt.Rows[i]["TicketID_PK"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }

                _dv = dt.DefaultView;
                gv_records.Visible = true;
                gv_records.DataSource = _dv;
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            Fillgrid();
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            Fillgrid();
        }

        /// <summary>
        /// This method used to sort the data in the Gridview.
        /// </summary>
        /// <param name="sortExpression">Sorting Expression on which sorting will apply.</param>
        /// <param name="direction">Direction of the sorting either Ascending OR Descending</param>
        private void SortGridView(string sortExpression, string direction)
        {

            _dv = (DataView)Session["DV_PastDueCallFamily"];
            _dv.Sort = sortExpression + " " + direction;
            Session["DV_PastDueCallFamily"] = _dv;
            //Fahad 7791 08/25/2010 Fill Grid Call on Sorting
            Fillgrid();
            Pagingctrl.PageCount = gv_records.PageCount;
            Pagingctrl.PageIndex = gv_records.PageIndex;

            ViewState["sortExpression"] = sortExpression;
            Session["SortDirection"] = direction;
            Session["SortExpression"] = sortExpression;

        }

        #endregion






    }
}

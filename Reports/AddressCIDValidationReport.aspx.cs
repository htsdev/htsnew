﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;   //SAEED 7844 06/03/2010 BussinessLogic object need reference of this namespce.

//Waqas 5770 04/09/2009 Created.

namespace HTP.Reports
{
    public partial class AddressCIDValidationReport : System.Web.UI.Page
    {
        #region Variables
        ValidationReports reports = new ValidationReports();
        clsLogger cBugTracker = new clsLogger();
        clsCase cCase = new clsCase();
        clsSession cSession = new clsSession();
        //SAEED 7844 06/03/2010 varaibles need for followup date functionality. 
        BussinessLogic Blogic = new BussinessLogic();
        int Report_ID = 0;
        int day = 0;
        //SAEED 7844 06/03/2010 END
        #endregion

        #region Properties
        //SAEED 7844 06/02/2010 ShowAll property, if this property is 'false' the report will display those recrods which have followup date of past,today or null. if
        //the value is 'true' then it will display all records.
        private bool ShowAllRecords
        {
            get
            {
                if (ViewState["ShowAllRecords"] == null)
                    ViewState["ShowAllRecords"] = false;

                return Convert.ToBoolean(ViewState["ShowAllRecords"]);
            }
            set
            {
                ViewState["ShowAllRecords"] = value;
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    lbl_Message.Text = "";
                    if (!IsPostBack)
                    {
                        //SAEED 7844 06/03/2010 setting showAll propertry=false for the first time.
                        chkShowAll.Checked = false;
                        ShowAllRecords = false;
                        GetRecords();
                    }
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);    //SAEED 7844 06/03/2010  attaching update control followup method with 'PageMethod' event.
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Data;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //SAEED 7844 06/02/2010 gridview RowDataBound event.
        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        //SAEED 7844 06/02/2010 gridview RowCommand event.
        protected void gv_Data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddFollowupDate")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string firstName = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_fname")).Value);
                    string lastName = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_Lname")).Value);
                    string followupDate = (((Label)gv_Data.Rows[RowID].FindControl("lbl_FollowUpDate")).Text);
                    string causeno = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_causeno")).Value);
                    string refCaseNumber = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_refCaseNumber")).Value);
                    string contactID = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ContactID")).Value);


                    cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketno")).Value));
                    string comm = cCase.GetGeneralCommentsByTicketId();

                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Address validation Follow up date";
                    UpdateFollowUpInfo2.Freezecalender = true;
                    string url = Request.Url.AbsolutePath.ToString();

                    DataTable dtDays = Blogic.GetBusinessLogicInformationByURL(url, "Days");
                    if (dtDays.Rows.Count > 0)
                        Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);

                    dtDays = Blogic.GetBusinessLogicSetDaysInformation("Days", Report_ID.ToString());
                    if (dtDays.Rows.Count > 0)
                        day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());

                    UpdateFollowUpInfo2.binddate(contactID, day.ToString(), clsGeneralMethods.GetBusinessDayDate(DateTime.Now, day), DateTime.Today, firstName, lastName, refCaseNumber, cCase.TicketID.ToString(), causeno, comm, mpeTrafficwaiting.ClientID, "AddressValidation", HTP.Components.FollowUpType.AddressValidationReport.ToString(), followupDate);
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.AddressValidationReport;
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }
        //SAEED 7844 06/02/2010 button submit event.
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ShowAllRecords = chkShowAll.Checked;
                gv_Data.PageIndex = 0;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// To Fill data into Grid
        /// </summary>
        private void GetRecords()
        {
            try
            {
                DataTable dt = reports.GetRecords_AddressValidationReport(ShowAllRecords);  //SAEED 7844 06/03/2010 passing showAll parameter so that the returning resultset will display records based on this parameter.
                lbl_Message.Text = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    gv_Data.Visible = true;
                    dt.Columns.Add("sno");
                    GenerateSerialNo(dt); ;
                    gv_Data.DataSource = dt;
                    gv_Data.DataBind();
                    Pagingctrl.PageCount = gv_Data.PageCount;
                    Pagingctrl.PageIndex = gv_Data.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    // Fahad Muhammad Qureshi 6429 09/01/2009 Set grivview page size
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records Found";
                    gv_Data.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method when Page index changed
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Data.PageIndex = 0;
                    gv_Data.PageSize = pageSize;
                    gv_Data.AllowPaging = true;
                }
                else
                {
                    gv_Data.AllowPaging = false;
                }
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        /// <summary>
        /// Method to insert Serial numbers in Gridview
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (!dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        //ozair 5929 05/21/2009 Sno generated on behalf of CIDs 
                        if (dtRecords.Rows[i - 1]["ContactID"].ToString() != dtRecords.Rows[i]["ContactID"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        //SAEED 7844 06/03/2010, To Fill data into Grid  
        void UpdateFollowUpInfo2_PageMethod()
        {
            GetRecords();
        }

        #endregion
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SmsClientRepliesReport.aspx.cs" Inherits="HTP.Reports.SmsClientRepliesReport" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SMS Client Replies Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        .clsLeftPaddingTable
        {
            padding-left: 5px;
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
        }
        .clsInputadministration
        {
            border-right: #3366cc 1px solid;
            border-top: #3366cc 1px solid;
            font-size: 8pt;
            border-left: #3366cc 1px solid;
            width: 90px;
            color: #123160;
            border-bottom: #3366cc 1px solid;
            font-family: Tahoma;
            background-color: white;
            text-align: left;
        }
        .clsInputCombo
        {
            border-right: #e52029 1px solid;
            border-top: #e52029 1px solid;
            font-weight: normal;
            list-style-position: outside;
            font-size: 8pt;
            border-left: #e52029 1px solid;
            color: #123160;
            border-bottom: #e52029 1px solid;
            font-family: Tahoma;
            list-style-type: square;
            background-color: white;
        }
        .label
        {
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            border-bottom-width: 0;
            border-left-width: 0;
            border-right-width: 0;
            border-top-width: 0;
            text-align: left;
        }
        .clsbutton
        {
            font-weight: bold;
            font-size: 8pt;
            border-left-color: #ffcc66;
            border-bottom-color: #ffcc66;
            cursor: hand;
            color: #3366cc;
            border-top-color: #ffcc66;
            font-family: Tahoma;
            background-color: #ffcc66;
            text-align: center;
            border-right-color: #ffcc66;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
    
    <script language="javascript" type="text/javascript">
        function refreshContent() {
             document.getElementById("tdData").style.display = 'none';
             __doPostBack('lbRefresh', '');
        }

        function IsCheck() {
            var chkAlldates = document.getElementById("cb_showalldates");
            var fromdate = document.getElementById("fromdate");
            var todate = document.getElementById("todate");

            if (chkAlldates.checked == true) {
                fromdate.disabled = true;
                todate.disabled = true;

            }
            else {
                fromdate.disabled = false;
                todate.disabled = false;
            }

        }
        
        function DeleteConfirm() {
            var isDelete = confirm("Are you sure you want to Cancel this fax?");
            if (isDelete == true) {
                return true;
            }

            else {
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="950px">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 21px">
                                    <strong><span class="clssubhead">Message Received Date :&nbsp;&nbsp; </span></strong>
                                    <strong><span class="clssubhead">From&nbsp; </span></strong>
                                    <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                        ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                        ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                        ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                        LowerBoundDate="1900-01-01">
                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></WeekdayStyle>
                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGray"></WeekendStyle>
                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></HolidayStyle>
                                    </ew:CalendarPopup>
                                    <strong><span class="clssubhead">To&nbsp; </span></strong>
                                    <ew:CalendarPopup ID="todate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                        ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                        ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                        ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                        LowerBoundDate="1900-01-01">
                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></WeekdayStyle>
                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGray"></WeekendStyle>
                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></HolidayStyle>
                                    </ew:CalendarPopup>
                                    <strong><span class="clssubhead">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SMS outcome&nbsp; </span>
                                    </strong>
                                    <asp:DropDownList ID="dd_SmsOutCome" runat="server" CssClass="clsInputCombo">
                                        <asp:ListItem Value="-1" Selected="True">--- All ---</asp:ListItem>
                                        <asp:ListItem Value="1">SMS Reply Confirmed</asp:ListItem>
                                        <asp:ListItem Value="2">Non SMS Reply Confirmed</asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btn_faxsearch" runat="server" Text="Search" CssClass="clsbutton"
                                        OnClick="btn_faxsearch_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 850px">
                                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                        <ProgressTemplate>
                                            <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                                        </ProgressTemplate>
                                    </aspnew:UpdateProgress>
                                </td>
                            </tr>
                            <tr>
                                <td id="tdData" runat="server">
                                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                                PageSize="20" CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand"
                                                OnRowDataBound="gv_records_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.TicketId")+"&search=0" %>'
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_TicketViolationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketViolationIds") %>'
                                                                CssClass="label" ForeColor="#123160" Visible="False">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId") %>'
                                                                CssClass="label" ForeColor="#123160" Visible="False">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate") %>'
                                                                CssClass="label" ForeColor="#123160" Visible="False">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_SmsTextType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SmsTextType") %>'
                                                                CssClass="label" ForeColor="#123160" Visible="False">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FromNumber" HeaderText="From" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" Width="80px" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="TicketNumber" HeaderText="Ticket Number" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Firstname" HeaderText="First Name" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="150" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Lastname" HeaderText="Last Name" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="50" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Text" HeaderText="Information" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="200" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="BondFlag" HeaderText="Bond" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="120px" />
                                                    </asp:BoundField>
                                                     <asp:BoundField DataField="SMSType" HeaderText="Alert Type" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CallBackStatus" HeaderText="Call Back Status" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AutoDialerStatus" HeaderText="Dialer Status" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="140px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ReceivedDate" HeaderText="Received Date & Time" HtmlEncode="false">
                                                        <ItemStyle CssClass="GridItemStyle" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnk_Comments" runat="server" Width="100px">Add Comments </asp:HyperLink>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="100px" />
                                                    </asp:TemplateField>                                                    
                                                </Columns>
                                                 <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            </asp:GridView>
                                            
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                    <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:Footer ID="Footer1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

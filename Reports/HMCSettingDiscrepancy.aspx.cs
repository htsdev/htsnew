using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Reports
{
    /// <summary>
    /// Summary description for HMCSettingDiscrepancy.
    /// </summary>
    
    public partial class HMCSettingDiscrepancy : System.Web.UI.Page
    {
        #region Web Form Designer generated code
        

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        
        #endregion
        
        #region Controls

        //protected eWorld.UI.CalendarPopup calQueryFrom;
        protected System.Web.UI.WebControls.DataGrid dg_valrep;
        //protected System.Web.UI.WebControls.Button btn_submit;
        //protected eWorld.UI.CalendarPopup calQueryTo;
        protected System.Web.UI.WebControls.Label lbl_message;
        //protected System.Web.UI.WebControls.DropDownList cmbPageNo;
        //protected System.Web.UI.WebControls.Label lblGoto;
        //protected System.Web.UI.WebControls.Label lblPNo;
        //protected System.Web.UI.WebControls.Label lblCurrPage;
        //protected System.Web.UI.WebControls.CheckBox chk_showall;

        #endregion  

        #region Variables

        DataSet ds_val;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();       
        clsCase cCase = new clsCase();
        //Sabir Khan 5977 07/06/2009 HMC Setting Discrepancy...
        //DataView dvHMCSetting;
        ValidationReports reports = new ValidationReports();
        BussinessLogic Blogic = new BussinessLogic();
        //Sabir Khan 5977 07/06/2009 Variable for sorting...
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataView DV;
        int day = 0;
        int Report_ID = 0;
        //Sabir Khan 5977 07/06/2009 Implement sorting...
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        //-------------------------------------------------
        #endregion
       
        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lbl_message.Text = ErrorMsg;
        }


        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            BindGrid();
        }
        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            //Validating session
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                lbl_message.Text = "";
                //Sabir Khan 5977 07/06/2009 Show setting control added...
                ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
                ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                //base.OnInit(e);


                Pagingctrl.GridView = gv_Records;
                if (!IsPostBack)
                {                    
                    lbl_message.Text = "";
                    GridViewSortDirection = SortDirection.Ascending;
                    GridViewSortExpression = "TicketID_PK";
                    BindGrid();
                }
               
                //Sabir Khan 5977 07/01/2009 Follow Up Date....
                UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                //Sabir Khan 4635 09/30/2008
                //Sabir Khan 5977 07/02/2009 paging control added...
                //Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                //if (gv_Records.PageCount == 0 || gv_Records.PageCount == 1)
                //{ Pagingctrl.Visible = false; }
               
             
                Pagingctrl.Visible = true;
              
              
            }

        }
        void UpdateFollowUpInfo2_PageMethod()
        {
            BindGrid();
        }

        //Sabir Khan 4635 09/30/2008
        protected void BindGrid()
        {
            //Sabir Khan 5977 07/03/2009 Bind HMC Discrepancy Report...
            //reports.getRecords("usp_hts_hmcsettingdiscrepancy");
            DataTable dt;
            int showall = (cb_ShowAll.Checked==true)?1:0;
            dt = reports.GetRecords_HMCSettingDiscrepancy(showall);
            //reports.generateSerialNo();
            //Sabir Khan 5977 07/06/2009 generating serial number...
            if (dt.Rows.Count > 0)
            {
                lbl_message.Text = "";
                if (dt.Columns.Contains("sno") == false)
                {
                    dt.Columns.Add("sno");
                    DataView dv = dt.DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    dt = dv.ToTable();
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["sno"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                            {
                                dt.Rows[i]["sno"] = ++sno;
                            }
                            else
                                dt.Rows[i]["sno"] = ".";
                        }
                    }
                }
                
                //ds_val= reports.records;

                //dvHMCSetting = new DataView(ds_val.Tables[0]);
                //gv_Records.DataSource = dvHMCSetting;
               
            }
            else
            {
                lbl_message.Text = "No Records Found";
            }
            gv_Records.DataSource = dt;
            gv_Records.DataBind();
            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();
           
        }
        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }
            
        //private void dg_valrep_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        //{
        //    GotoPage(e.NewPageIndex);
        //}

        //sets paging in gv_records
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Sabir Khan 5977 07/06/2009 follow up date control associated with each records...
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                int RowID = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_causeno")).Value);
                string ticketno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketnumber")).Value);
                string lastname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_lname")).Value);
                string followupDate = (((Label)gv_Records.Rows[RowID].FindControl("lbl_FollowUp")).Text);
                int ticketid = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketid")).Value));
                cCase.TicketID = ticketid;
                string courtname = "HMCSettingDiscrepancy";
                followupDate = (followupDate.Trim() == "") ? "1/1/1900" : followupDate;
                //string bonddate = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_bonddate")).Value);
                //bonddate = bonddate.Substring(0, bonddate.IndexOf("@")).Trim();
                cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketno")).Value));
                string comm = cCase.GetGeneralCommentsByTicketId();
                UpdateFollowUpInfo2.Freezecalender = true;
                string url = Request.Url.AbsolutePath.ToString();
                DataTable dtDays = Blogic.GetBusinessLogicInformationByURL(url, "Days");
                if (dtDays.Rows.Count > 0)
                    Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);

                dtDays = Blogic.GetBusinessLogicSetDaysInformation("Days", Report_ID.ToString());
                if (dtDays.Rows.Count > 0)
                    day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());

                UpdateFollowUpInfo2.binddate(day.ToString(), clsGeneralMethods.GetBusinessDayDate(DateTime.Now, day), DateTime.Today, firstname, lastname, ticketno, ticketid.ToString(), causeno, comm, mpehmcsetting.ClientID, courtname, courtname, followupDate);
                //UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpehmcsetting.ClientID, "HMCSettingDiscrepancy", Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketno")).Value)));

                UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.HMCSettingDiscrepancyFollowUpDate;
                mpehmcsetting.Show();
                Pagingctrl.Visible = true;
            }

        }
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                Label followup = (Label)e.Row.FindControl("lbl_followup");
                followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();

            }
            catch (Exception)
            {

            }
        }
        protected void cb_ShowAll_CheckedChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        /// <summary>
        /// Handling PaggingControl page Size Change event
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;

            }
            else
            {
                gv_Records.AllowPaging = false;
            }

            BindGrid();


        }
        #endregion 

        #region Methods

        
     
        //Method To Generate Serial No
        private void BindReport()
        {
            //Ozair 5057 03/24/2009 Warnings Removed
            long sNo = (gv_Records.PageIndex) * (gv_Records.PageSize);
            try
            {
                foreach (DataGridItem ItemX in dg_valrep.Items)
                {
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lbl_Sno"))).Text = sNo.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //public SortDirection GridViewSortDirection
        //{

        //    get
        //    {

        //        if (ViewState["sortDirection"] == null)

        //            ViewState["sortDirection"] = SortDirection.Ascending;

        //        return (SortDirection)ViewState["sortDirection"];

        //    }

        //    set { ViewState["sortDirection"] = value; }

        //}
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            //SortGrid(e.SortExpression);
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                BindGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        #region Code Commented...
        //protected void gv_Data_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    string sortExpression = e.SortExpression;
        //    Session["SortExpression"] = sortExpression;

        //    if (GridViewSortDirection == SortDirection.Ascending)
        //    {

        //        GridViewSortDirection = SortDirection.Descending;
        //        Session["SortDirection"] = " desc";
        //        SortGridView(sortExpression, " desc");

        //    }

        //    else
        //    {

        //        GridViewSortDirection = SortDirection.Ascending;
        //        Session["SortDirection"] = " asc";
        //        SortGridView(sortExpression, " asc");

        //    }
        //    //khalid 3260  3/4/08 to add paging control
        //    ////Fill_ddlist();
        //    //Pagingctrl.SetPageIndex();


        //}

        //private void SortGridView(string sortExpression, string direction)
        //{

        //    // You can cache the DataTable for improving performance

        //    //GetRecords();
        //    reports.getRecords("usp_hts_hmcsettingdiscrepancy");
        //    reports.generateSerialNo();
        //    ds_val = reports.records;

        //    DataTable dt = ds_val.Tables[0];

        //    DataView dv = new DataView(dt);

        //    dv.Sort = sortExpression + direction;

        //    gv_Records.DataSource = dv;

        //    gv_Records.DataBind();

        //    //khalid 3260  3/4/08 to add paging control
        //    //Pagingctrl.PageCount = gv_Data.PageCount;
        //    //Pagingctrl.PageIndex = gv_Data.PageIndex;
        //    //Pagingctrl.SetPageIndex();


        //}
        #endregion

        //Sabir Khan 5977 07/06/2009 Implement sorting....
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;               
                gv_Records.DataSource = DV;
                gv_Records.DataBind();
                Session["DS"] = DV;                
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }
        //Sabir Khan 5977 07/06/2009 Implement sorting....
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {
                
            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "DESC")
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "DESC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }
                

        #endregion       
      

    }
}

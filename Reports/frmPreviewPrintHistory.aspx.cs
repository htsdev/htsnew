using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class frmPreviewPrintHistory : System.Web.UI.Page
    {
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                    ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
                    string Path = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + Request.QueryString["filename"].ToString() + ".pdf";

                    Response.Redirect("../Docstorage" + Path, false);
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

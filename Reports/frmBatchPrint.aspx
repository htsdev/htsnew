<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" Codebehind="frmBatchPrint.aspx.cs" AutoEventWireup="false" Inherits="lntechNew.Reports.frmBatchPrint" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>frmBatchPrint</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<script language="javascript">
		
		function PromptDelete()
		{	
		 var count=document.getElementById("txt_letcount1").value;
		 if (count>'0')
		 {	
			doyou = confirm("Are you sure you want to delete all letter(s) in this batch. Press OK to Yes and Cancel to No"); 
			if (doyou == true)
			{
				return true;
			}
			return false;			
		}
		else
		{
			alert('No letter in batch');
			return false;
		}
	}
		
	function AssignValue(val)
	{
	var txt = document.getElementById("txtHidID");
	txt.value = val;
	}
	
	function AssignValueType(val)
	{
	var txt = document.getElementById("txtHidType");
	txt.value = val;
	}
	function CheckAll()
	{
	var chk = document.getElementById("chkHeader");
	//var txt = document.getElementById("txthidCount");
	var grdName =  "dg_Names";
	//var tcnt = txt.value;
	var cnt = parseInt(document.getElementById("txthidCount").value);
	cnt = cnt +2;
	var idx = 2;
	if (chk.checked==true)
		{
		for (idx=2;idx<cnt;idx++)
			{
			var chk = grdName+ "__ctl" + idx + "_chk";
			var GetChk = document.getElementById(chk);
			GetChk.checked =  true;
			}
		}
		else if (chk.checked==false)
		{
		for (idx=2;idx<cnt;idx++)
			{
			var chk = grdName+ "__ctl" + idx + "_chk";
			var GetChk = document.getElementById(chk);
			GetChk.checked =  false;
			}
		}
	}
	
	function OpenNewWindow(flag,Lid,chk,fdate,tid,printed)
	{	
	//a;		
		if (flag = 1) //for grid 1 & 2 
		{
		var str;
		var p;
		var w;
			if (printed!='0')
			{
				doyou=confirm("Some letter(s) are already printed. Do you want to print them again. Press OK to Yes and Cancel to No"  )	
				if(doyou==true)
				{
				 p=1;
				 str = "PreviewBatch.aspx?Lettertype=" + Lid + "&Type=" + chk + "&FileDate=" + fdate + "&TicketID=" + tid+"&Print=" + p;
				}				
				else
				{
					return false;
				}
			}
			else
			{
				p=2;
				str = "PreviewBatch.aspx?Lettertype=" + Lid + "&Type=" + chk + "&FileDate=" + fdate + "&TicketID=" + tid+"&Print=" + p;								
			}	
			w = window.open(str,"");			
			return false;
		}
		
	}
	
	function OpenLastGrid()
	{
		var grdName =  "dg_Names";
		
		var cnt = parseInt(document.getElementById("txthidCount").value);
		
		//a;
		cnt = cnt + 2;
		var idx = 2;
		var StrTid = "";
		var print=0*1;
			for (idx=2;idx<cnt;idx++)
				{
				var chk = grdName+ "__ctl" + idx + "_chk";
				var GetChk = document.getElementById(chk);
					if (GetChk.checked == true)
						{
							var tkid = grdName+ "__ctl" + idx + "_lbl_TicketID";
							var GetTiD = document.getElementById(tkid);
							StrTid = StrTid + GetTiD.innerText +",";
							var printid=grdName+ "__ctl" + idx + "_lbl_print3";
							var getprint=document.getElementById(printid);
							print=print+getprint.innerText*1;
						}
				}
				
		var Lid = document.Form1.txtHidID.value;
		var chk =3;
		var fdate = document.Form1.txtHidType.value;
		var FileDate = fdate;
		var tid = StrTid;
		var w;
		if (tid != "")		
		{
			if(print!=0)
			{
				doyou=confirm("Some letter(s) are already printed. Do you want to print them again. Press OK to Yes and Cancel to No"  )	
				if(doyou==true)
				{
				 p=1;
				 str = "PreviewBatch.aspx?Lettertype=" + Lid + "&Type=" + chk + "&FileDate=" + FileDate + "&TicketID=" + tid+"&Print=" + p;
				}				
				else
				{
					return false;
				}		
			}
			else
			{			
				p=2;
				str = "PreviewBatch.aspx?Lettertype=" + Lid + "&Type=" + chk + "&FileDate=" + FileDate + "&TicketID=" + tid+"&Print=" + p;								
			}
			w = window.open(str,"");			
			
		//a;
		return false;
		}
		else
		{
		alert("Select atleast one record");
		return false;
		}
	}
		//To check selected for Del
	function OpenLastGridDel()
	{
		var grdName =  "dg_Names";	
		var cnt = parseInt(document.getElementById("txthidCount").value);	
		cnt = cnt + 2;
		var idx = 2;
		var StrTid = "";
		for (idx=2;idx<cnt;idx++)
			{
			var chk = grdName+ "__ctl" + idx + "_chk";
			var GetChk = document.getElementById(chk);
				if (GetChk.checked == true)
					{
						var tkid = grdName+ "__ctl" + idx + "_lbl_TicketID";
						var GetTiD = document.getElementById(tkid);
						StrTid = StrTid + GetTiD.innerText +",";
					}
			}		
		var tid = StrTid;
		if (tid == "")
		{
			alert("Select atleast one record");
			return false;	
		}
		else
		{
			doyou = confirm("Are you sure you want to delete letter(s) in this batch. Press OK to Yes and Cancel to No"); 
			if (doyou == true)
			{
				return true;
			}
			else
			{
			return false;		
			}		
		}
	}
		</script>
		<form id="Form1" method="post" runat="server">
			<TABLE id="MainTable" cellSpacing="1" cellPadding="1" width="780" align="center" border="0">
				<TR>
					<TD align="left"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD class="clssubhead" align="left" background="../../images/headbar_midextend.gif">&nbsp;Letters 
						In 
						Batch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
				</TR>
				<TR>
					<TD align="left" background="../../images/separator_repeat.gif"><STRONG></STRONG></TD>
				</TR>
				<TR>
					<TD align="left"><asp:datagrid id="dg_Letters" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
							Width="815px">
							<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Batch Letter Types ">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" Width="30%" CssClass="label"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id=lnkbtn_Letter runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LetterName") %>' CssClass="label">
										</asp:LinkButton>
										<asp:Label id=lbl_LetterID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LetterID") %>' CssClass="label" Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Count">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" Width="39%" CssClass="label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lbl_LetterCount runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BatchCount") %>'>
										</asp:Label>
										<asp:Label id=lbl_print1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.printed") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:Button id="btn_Print" runat="server" Text="Print" CssClass="clsbutton"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="Delete" CommandName="Delete">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:ButtonColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="left" background="../../images/separator_repeat.gif"></TD>
				</TR>
				<TR>
					<TD class="clssubhead" align="left" background="../../images/headbar_midextend.gif">&nbsp;Batch 
						Wise Letters</TD>
				</TR>
				<TR>
					<TD align="left"><asp:datagrid id="dg_LetterType" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
							Width="815px">
							<FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></FooterStyle>
							<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Letter Type">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" Width="30%" CssClass="label"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id=lnkLetterType runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LetterName") %>' CommandName="clicked">
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Count">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" Width="14%" CssClass="label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lbl_LetterTypeCount runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Count") %>'>
										</asp:Label>
										<asp:Label id=lbl_print2 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.printed") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="File Date">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lbl_LetterTypeFileDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileDate", "{0: MM/dd/yyyy}") %>' CssClass="label">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:Button id="btn_printType" runat="server" Text="Print" CssClass="clsbutton"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="Delete" CommandName="Delete">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:ButtonColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 1px" align="left" background="../../images/separator_repeat.gif"></TD>
				</TR>
				<TR>
					<TD class="clssubhead" style="HEIGHT: 1px" align="left" background="../../images/headbar_midextend.gif">&nbsp;Letters</TD>
				</TR>
				<TR id="TRLetters" runat="server">
					<TD class="clsaspcolumnheader" align="left" bgColor="#eff4fb" borderColorDark="#000000">&nbsp;&nbsp;&nbsp;
						<asp:checkbox id="chkHeader" runat="server" Width="104px"></asp:checkbox>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trial 
						Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						File Date</TD>
				</TR>
				<TR>
					<TD align="left"><asp:datagrid id="dg_Names" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
							Width="815px" ShowHeader="False">
							<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
							<HeaderStyle Font-Bold="True"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
									<ItemTemplate>
										<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD width="195">&nbsp;
													<asp:CheckBox id="chk" runat="server" Width="58px"></asp:CheckBox>
													<asp:Label id=lbl_TicketID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Ticketid_pk") %>' ForeColor="White">
													</asp:Label></TD>
												<TD width="195">
													<asp:Label id=lbl_Name runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'>
													</asp:Label></TD>
												<TD width="195">
													<asp:Label id=lbl_TrialDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0: MM/dd/yyyy}") %>'>
													</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:Label id=lbl_print3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>' ForeColor="White">
													</asp:Label></TD>
												<TD width="195">
													<asp:Label id=lbl_FileDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BatchDate","{0: MM/dd/yyyy}") %>'>
													</asp:Label></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="left"><asp:button id="btn_PrintNames" runat="server" CssClass="clsbutton" Width="72px" Text="Print"></asp:button><asp:button id="btn_delete1" runat="server" CssClass="clsbutton" Width="72px" Text="Delete"></asp:button></TD>
				</TR>
				<TR>
					<TD align="left"><asp:label id="lbl_Message" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD style="VISIBILITY: hidden" align="left"><asp:textbox id="txtHidID" runat="server" Width="32px">0</asp:textbox><asp:textbox id="txtHidType" runat="server" Width="24px">0</asp:textbox><asp:textbox id="txthidCount" runat="server" Width="24px"></asp:textbox><asp:textbox id="txt_letcount1" runat="server" Width="24px"></asp:textbox>
						<asp:Label id="lbl_mis" runat="server">0</asp:Label></TD>
				</TR>
				<TR>
					<TD align="left"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

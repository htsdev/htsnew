using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using ReportSettingControl;

namespace HTP.Reports
{
    //Ozair 8101 09/29/2010 code refactored where required
    //Noufil Khan 4748 09/08/2008 new Paging control included removing the old one and make seperate function to fill the grid.
    /// <summary>
    /// HCJP Auto Update Alert Report class
    /// </summary>
    public partial class Outsidecases : System.Web.UI.Page
    {
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsSession _clsSession = new clsSession();
        readonly clsCase _cCase = new clsCase();
        DataSet _dsVal; //Saeed 8101 09/28/2010 variable to hold dataset object.
        private int _reportId;   //Saeed 8101 09/28/2010 varaibel to hold report id.
        private ReportSettingControl.ReportSetting ReportSetting1 = null;
        private const string PageUrl = "/Reports/Outsidecases.aspx";  //Saeed 8101 09/28/2010 hold page url.

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lnkbtn_ShowSetting.Attributes.Add("onclick", "btnClick('btnSimple')");
                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {

                    //Saeed 8101 09/27/2010 code to set report setting properties for reportId & current user id.
                    if ((Session["DISPLAY_REPORT_SETTING"] != null && Session["DISPLAY_REPORT_SETTING"].ToString() == "1"))
                    {
                        InitializeReportSettingControl();
                    }
                    if (!IsPostBack)
                    {
                        Fillgrid();
                    }
                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    //Saeed 8101 09/24/2010 PageMethod handler added.
                    UpdateFollowUpInfo2.PageMethod += UpdateFollowUpInfo2_PageMethod;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_Records;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Show Setting click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtn_ShowSetting_Click(object sender, EventArgs e)
        {
            Session["DISPLAY_REPORT_SETTING"] = "1";
            InitializeReportSettingControl();
        }

        /// <summary>
        /// This method dynamically create report setting control.
        /// </summary>
        void InitializeReportSettingControl()
        {
            mpeReportSetting.Enabled = true;
            ReportSetting1 = new ReportSetting();
            ReportSetting1.ReportTitle = "Report Attribute";
            ReportSetting1.ConnectionStringKey = "Connection String";
            ReportSetting1.HeaderCssClass = "clssubhead";
            ReportSetting1.LabelCssClass = "clsLeftPaddingTable";
            ReportSetting1.LinkButtonCssClass = "clssubhead";
            ReportSetting1.GridviewCssClass = "clsLeftPaddingTable";
            ReportSetting1.CellBackgroundImageUrl = "CellBackgroundImage";
            ReportSetting1.TextboxCssClass = "clsInputadministration";
            ReportSetting1.DropdownCssClass = "clsInputCombo";
            ReportSetting1.MainTableBackgroundColor = "#EFF4FB";

            _reportId = ReportSetting1.GetReportId(PageUrl);
            ReportSetting1.ReportId = _reportId;
            ReportSetting1.CurrentUserId = Convert.ToInt32(_clsSession.GetCookie("sEmpID", Page.Request));
            phReportSetting.Controls.Add(ReportSetting1);
            mpeReportSetting.Show();
            pnl_popup.Visible = true;
            

        }

        //Saeed 8101 09/28/2010 handler created.
        /// <summary>
        /// Gridview page size handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            Fillgrid();

        }
        //Saeed 8101 09/24/2010  method created.
        /// <summary>
        /// Refresh grid with updated records
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            Fillgrid();
        }

        /// <summary>
        /// Grid View Page Index Changing Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_Records.PageIndex = e.NewPageIndex;
                    Fillgrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Grid View Row Command Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {
                    InitializeReportSettingControl();
                    mpeReportSetting.Enabled = false;
                    pnl_popup.Visible = false;
                    
                    var rowId = Convert.ToInt32(e.CommandArgument);
                    var firstname = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_fname")).Value;
                    var lastname = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_lname")).Value;
                    var causeno = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_causeno")).Value;
                    var ticketno = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value;
                    var ticketid = ((HiddenField)gv_Records.Rows[rowId].FindControl("hf_TicketID")).Value;
                    var followupDate = ((Label)gv_Records.Rows[rowId].FindControl("lbl_followupdate")).Text;
                    DateTime nextfollow = (followupDate == string.Empty) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(followupDate);
                    ViewState["TicketId"] = ticketid;
                    const string court = "HCJPAutoUpdateAlert";
                    _cCase.TicketID = Convert.ToInt32(ticketid);
                    var commments = _cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "HCJP Auto Update Alert";
                    UpdateFollowUpInfo2.followUpType = Components.FollowUpType.HCJPAutoAlertFollowUpDate;

                    //Saeed 8101 09/27/2010 court id.
                    var courtid = Convert.ToInt32(((HiddenField)gv_Records.Rows[rowId].FindControl("hf_courtid")).Value);
                    //Saeed 8101 09/27/2010 set followup date using report setting control. Attribute ID=2 is used for followup date days. 
                    int businessDaysToStop = Convert.ToInt16(ReportSetting1.GetAttributeValue(_reportId, courtid, 2));
                    //Saeed 8101 09/27/2010 set default followup date using report setting control. Attribute ID=3 is used for Default followup date.
                    var defaultFollowupDate = ReportSetting1.GetAttributeValue(ReportSetting1.GetReportId(PageUrl), courtid, 3);
                    defaultFollowupDate = string.IsNullOrEmpty(defaultFollowupDate) ? "0" : defaultFollowupDate;

                    //Saeed 8101 09/27/2010 set follow up control values.
                    UpdateFollowUpInfo2.binddate(businessDaysToStop, clsGeneralMethods.GetBusinessDayDate(DateTime.Now, Convert.ToInt32(defaultFollowupDate)), Convert.ToDateTime(nextfollow), firstname, lastname, ticketno, causeno, commments, mpeTrafficwaiting.ClientID, court, Convert.ToInt32(ticketid));
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }

        /// <summary>
        /// Grid View Row Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }
        /// <summary>
        /// Show All Check box Checked Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkShowAllUserRecords_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Fillgrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Populate grid control
        /// </summary>
        private void Fillgrid()
        {
            //Saeed 8101 09/27/2010 call GetHcjpAutoUpdateAlert method with show all parameter. Also apply serial no. generation code.
            try
            {
                _dsVal = _reports.GetHcjpAutoUpdateAlert(chkShowAllUserRecords.Checked);
                GenerateSerialNo(_dsVal.Tables[0]); // generating serial number for data grid.
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = _dsVal.Tables[0];
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "";
                if (_dsVal.Tables[0].Rows.Count < 1)
                {
                    lbl_Message.Text = "No Records Found";
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Paging Control Page Index Changed Method
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            Fillgrid();
        }
        //Saeed 8101 09/27/2010 handler created.
        /// <summary>
        /// Refresh parent grid after closing report setting control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPostBack_Click(object sender, EventArgs e)
        {
            //Saeed 8101 11/29/2010 code close report setting control.
            Session["DISPLAY_REPORT_SETTING"] = "0";
            ReportSetting1 = null;
            Session["InsertFlag"] = "0";
            Response.Redirect("~/Reports/Outsidecases.aspx?sMenu=137", false);
        }
        //Saeed 8101 09/28/2010 method created.
        /// <summary>
        /// Generate serial numbers
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            var sno = 1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
            }

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (var i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["TICKETID_PK"].ToString() != dtRecords.Rows[i]["TICKETID_PK"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

    }
}

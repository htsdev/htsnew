using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    /// <summary>
    /// OpenServiceTicketReportCriminal class file.
    /// </summary>
    public partial class OpenServiceTicketReportCriminal : System.Web.UI.Page
    {

        #region Variables

        readonly clsSession _cSession = new clsSession();
        readonly clsLogger _clog = new clsLogger();
        readonly OpenServiceTickets _openServiceTickets = new OpenServiceTickets();
        DataView _dv;
        string _strExp = String.Empty;
        string _strAcsDec = String.Empty;
        string _usertype = "";
        string _empid = "";
        int _showAllfollowup;

        #endregion

        /// <summary>
        /// OnInit event.
        /// </summary>
        /// <param name="e"></param>
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// InitializeComponent
        /// </summary>
        private void InitializeComponent()
        {
            PagingControl1.PageIndexChanged += PagingControl1_PageIndexChanged;
            PagingControl1.PageSizeChanged += Pagingctrl_PageSizeChanged;
        }

        #region Events

        /// <summary>
        /// Page_Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_cSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {

                    PagingControl1.GridView = gv;

                    if (!IsPostBack)
                    {
                        if (Request.QueryString["showAllMyCriminal"] != null || Request.QueryString["fromPopup"] != null)
                        {
                            Session["ShowIncompleteCriminal"] = "1";
                        }
                        Session["OSDSCriminal"] = null;
                        Session["showAllMyCriminal"] = null;
                        Session["ShowIncompleteCriminal"] = null;
                        var empid = _cSession.GetCookie("sEmpID", Request);
                        ViewState["empId"] = empid;
                        lblMessage.Text = "";
                        caldatefrom.SelectedDate = DateTime.Now;
                        caldateto.SelectedDate = DateTime.Now;
                        FillGrid();


                    }
                }
                PagingControl1.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDum_Click(object sender, EventArgs e)
        {
            try
            {
                Session["OSDSCriminal"] = null;
                Session["showAllMyCriminal"] = null;
                Session["ShowIncompleteCriminal"] = null;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Gridview page index change evnet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Gridview row command event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "close")
                {
                    var parameters = e.CommandArgument.ToString().Split(new[] { '-' });
                    var caseTypeId = Convert.ToInt32(parameters[2]);
                    var ticketId = Convert.ToInt32(parameters[1]);
                    var fId = Convert.ToInt32(parameters[0]);
                    var employeeName = _cSession.GetCookie("sEmpName", HttpContext.Current.Request);
                    //Close Service Ticket
                    _openServiceTickets.CloseServiceTicket(Convert.ToInt32(ticketId), Convert.ToInt32(fId));
                    //Send Clode Service Ticket Notification Email                
                    _openServiceTickets.SendServiceTicketNotificationEmail(ServiceTicketType.Closed, ticketId, fId, employeeName, Request.Url.Authority, caseTypeId);
                    _clog.AddNote(Convert.ToInt32(ViewState["empId"]), "Service Ticket Closed", "", ticketId);
                    Session["OSDSCriminal"] = null;
                    FillGrid();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        /// <summary>
        /// Search button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                Session["OSDSCriminal"] = null;
                Session["showAllMyCriminal"] = null;
                Session["ShowIncompleteCriminal"] = null;
                gv.PageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Gridview row data bound event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var ticketid = ((HiddenField)e.Row.FindControl("HFTicketid")).Value;
                    var fid = ((HiddenField)e.Row.FindControl("HFFid")).Value;
                    var catgory = ((Label)e.Row.FindControl("lblCategory")).Text;
                    e.Row.FindControl("lblCategoryOption").Visible = catgory.ToUpper() == "CONTINUANCE";
                    var slabel = ((Label)e.Row.FindControl("lblCategoryOption")).Text;
                    ((Label)e.Row.FindControl("lblCategoryOption")).Text = slabel != ""
                                                                                ? "[" + slabel + "]"
                                                                                : string.Empty;


                    ViewState["ticketid2"] = Convert.ToInt32(ticketid);
                    var clientName = ((HyperLink)e.Row.FindControl("HPClientName")).Text;

                    //Remove Extra Character From Client Name
                    clientName = clientName.Replace("\n", "");
                    clientName = clientName.Replace("\r", "");
                    clientName = clientName.Replace(" ", "");
                    clientName = clientName.Replace(",", " , ");
                    //Adding Delete Javascript
                    ((ImageButton)e.Row.FindControl("imgbtn_close")).OnClientClick = "return Delete('" + clientName + "','" + ((Label)e.Row.FindControl("lbl_percentage")).Text + "');";
                    ((HyperLink)e.Row.FindControl("HPClientName")).NavigateUrl = "../clientinfo/violationfeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((ImageButton)e.Row.FindControl("HpLastUpdate")).Attributes.Add("onclick", "return PopUp('" + ticketid + "','" + fid + "')");
                    ((Label)e.Row.FindControl("lbl_crtdate")).Text = clsGeneralMethods.FormatDate(((Label)e.Row.FindControl("lbl_crtdate")).Text);

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Gridview sorting event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// This method used to fetch the data which will display on the Gridview.
        /// </summary>
        private void FillGrid()
        {
            lblMessage.Text = "";
            _showAllfollowup = (chkShowAllfollowupRecords.Checked ? 1 : 0);
            var showIncomplete = (chkAllPastDue.Checked ? "1" : "0");
            var dt = _openServiceTickets.GetRecords(caldatefrom.SelectedDate.ToShortDateString(), caldateto.SelectedDate.ToShortDateString(),
                Convert.ToInt32(_cSession.GetCookie("sEmpID", Request)), rbl_tickettype.SelectedValue, showIncomplete, 2, _showAllfollowup);
            if (dt.Rows.Count > 0)
            {
                SerialNumber(dt);

                if (Session["OSDSCriminal"] == null)
                {
                    _dv = new DataView(dt);
                    Session["OSDSCriminal"] = _dv;
                }
                else
                {

                    _dv = (DataView)Session["OSDSCriminal"];
                }

                _usertype = _cSession.GetCookie("sAccessType", Request);
                _empid = _cSession.GetCookie("sEmpID", Request);
                var isSpnUser = _cSession.GetCookie("sIsSPNUser", Request);
                PagingControl1.GridView = gv;
                gv.DataSource = _dv;
                gv.DataBind();
                gv.Columns[11].Visible = false;
                PagingControl1.PageIndex = gv.PageIndex;
                PagingControl1.PageCount = gv.PageCount;
                PagingControl1.SetPageIndex();

                if (_usertype == "2" || _empid == "4028" || isSpnUser == "True")
                    gv.Columns[16].Visible = true;
                else
                    gv.Columns[16].Visible = false;

                if (rbl_tickettype.SelectedValue == "0")
                {
                    gv.Columns[11].Visible = false;
                    gv.Columns[16].Visible = false;
                    gv.Columns[12].Visible = true;


                }
                else
                {
                    gv.Columns[11].Visible = true;
                    gv.Columns[16].Visible = true;
                    gv.Columns[12].Visible = false;
                }

                gv.Visible = true;

            }
            else
            {
                PagingControl1.PageCount = 0;
                PagingControl1.PageIndex = 0;
                PagingControl1.SetPageIndex();
                lblMessage.Text = "No Record Found";
                gv.Visible = false;
            }

        }

        /// <summary>
        /// This method used to generate the Serial Number to the given DataTable.
        /// </summary>
        /// <param name="dt">DataTable on which serial number will generate.</param>
        private void SerialNumber(DataTable dt)
        {
            var sno = 1;
            if (!dt.Columns.Contains("sno"))
                dt.Columns.Add("sno");


            if (dt.Rows.Count >= 1)
                dt.Rows[0]["sno"] = 1;

            if (dt.Rows.Count >= 2)
            {
                for (var i = 1; i < dt.Rows.Count; i++)
                {

                    dt.Rows[i]["sno"] = ++sno;

                }
            }

        }

        /// <summary>
        /// This method used to sort the data in the Gridview.
        /// </summary>
        /// <param name="sortExp">Sorting Expression on which sorting will apply.</param>
        private void SortGrid(string sortExp)
        {
            SetAcsDesc(sortExp);
            _dv = (DataView)Session["OSDSCriminal"];
            _dv.Sort = _strExp + " " + _strAcsDec;
            var sortData = _dv.ToTable();
            SerialNumber(sortData);
            gv.DataSource = sortData;
            gv.DataBind();
            PagingControl1.PageCount = gv.PageCount;
            PagingControl1.PageIndex = gv.PageIndex;
            PagingControl1.SetPageIndex();
            Session["OSDSCriminal"] = sortData.DefaultView;


        }

        /// <summary>
        /// This method used to define the direction of the sorting, either Ascending OR Descending.
        /// </summary>
        /// <param name="val">Direction on which sorting will apply</param>
        private void SetAcsDesc(string val)
        {
            try
            {
                _strExp = Session["_strExp"].ToString();
                _strAcsDec = Session["_strAcsDec"].ToString();
            }
            catch
            {

            }

            if (_strExp == val)
            {
                if (_strAcsDec == "ASC")
                {
                    _strAcsDec = "DESC";
                    Session["_strAcsDec"] = _strAcsDec;
                }
                else
                {
                    _strAcsDec = "ASC";
                    Session["_strAcsDec"] = _strAcsDec;
                }
            }
            else
            {
                _strExp = val;
                _strAcsDec = "ASC";
                Session["_strExp"] = _strExp;
                Session["_strAcsDec"] = _strAcsDec;
            }
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void PagingControl1_PageIndexChanged()
        {
            gv.PageIndex = PagingControl1.PageIndex - 1;
            FillGrid();
            PagingControl1.SetPageIndex();
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv.PageIndex = 0;
                gv.PageSize = pageSize;
                gv.AllowPaging = true;

            }
            else
            {
                gv.AllowPaging = false;
            }

            FillGrid();
        }

        #endregion



    }
}

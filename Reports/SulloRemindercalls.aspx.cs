﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using FrameWorkEnation.Components;


namespace lntechNew.Reports
{

    public partial class SulloRemindercalls : System.Web.UI.Page
    {
        clscalls clscall = new clscalls();
        protected void Page_Load(object sender, EventArgs e)
        {
            dg_ReminderCalls.DataSource = clscall.Sulloremindercallgrid();
            dg_ReminderCalls.DataBind();
        }
    }
}


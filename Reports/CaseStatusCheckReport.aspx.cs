using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.Reports
{
    public partial class CaseStatusCheckReport : System.Web.UI.Page
    {
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        clsCase ClsCase = new clsCase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable ClsCase.GetCaseStatusReport(cal_From.SelectedDate, cal_To.SelectedDate);
                
            }
            catch (Exception ex)
            {

            }
        }
    }
}

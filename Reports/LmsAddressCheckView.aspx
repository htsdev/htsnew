﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LmsAddressCheckView.aspx.cs" Inherits="lntechNew.Reports.LmsAddressCheckView" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>LMS Address Check</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="javascript" type="text/javascript">
     
    </script>
    
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        .style5
        {
            width: 100px;
            direction: ltr;
        }
        .style1
    {
    	text-align: left;
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 130px;
        padding-left: 6px;
        height: 30px;
    }
    
    
    .style2
    {
    	font-family: Tahoma;
	    font-size: 8pt;
	    color: #123160;
	    border-bottom-width: 0;
	    border-left-width: 0;
	    border-right-width: 0;
	    border-top-width: 0;
	    text-align: left;
	    width: 310px;
    }
    
    .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .HellowWorldPopup
        {
            min-width:200px;
            min-height:150px;
            background:white;
        }
        
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp35:ScriptManager ID="ScriptManager1" runat="server">
        </asp35:ScriptManager>
    <div> 
     <table id="TableMain" cellspacing="0" cellpadding="0" width="820" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="height: 9px" width="820" background="../../images/separator_repeat.gif" height="9">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                        <td class="clsLeftPaddingTable" colspan="2" width="100%">
                            <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td class="clsLeftPaddingTable" align="left" style="width: 32px">
                                        <asp:Label ID="lblStartDate" runat="server" CssClass="clssubhead">From</asp:Label>
                                    </td>
                                    <td class="clsLeftPaddingTable" style="width: 120px" align="left">
                                        <ew:CalendarPopup ID="cal_EffectiveFrom" runat="server" Width="90px" ImageUrl="../images/calendar.gif"
                                            Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                            ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                            PadSingleDigits="True" ToolTip="Select Report Date Range" SelectedDate="2005-09-01"
                                            DisableTextboxEntry="true" ClearDateText="">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                    </td>
                                    <td class="clsLeftPaddingTable" style="width: 25px" align="right">
                                        <asp:Label ID="lblEndDate" runat="server" CssClass="clssubhead">To</asp:Label>
                                    </td>
                                    <td class="clsLeftPaddingTable" style="width: 120px" align="right">
                                        <ew:CalendarPopup ID="cal_EffectiveTo" runat="server" Width="90px" ImageUrl="../images/calendar.gif"
                                            Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                            ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                            PadSingleDigits="True" ToolTip="Select Report Date Range" SelectedDate="2006-09-01"
                                            DisableTextboxEntry="true">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                    </td>
                                    <td align="right" class="clsLeftPaddingTable" style="width: 220px;">
                                    <asp:Label ID="lbl_letterType" runat="server" CssClass="clssubhead">Letter Type :</asp:Label>
                                    &nbsp;
                                <asp:DropDownList ID="ddl_letterType" CssClass="clsInputCombo" runat="server" Width="100">
                                </asp:DropDownList>
                                    </td>
                                    <td class="clsLeftPaddingTable" align="right">
                                         <asp:Button ID = "btn_Submit" onclick="btn_Submit_Click" runat = "server" Text = "Submit" CssClass ="clsbutton" /> &nbsp; &nbsp; &nbsp; &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
            <tr>
                <td>
                
                    <asp35:UpdatePanel ID="upnlResult" runat="server">
                        <ContentTemplate>
                        <asp:Button ID="btn" runat="server" Text="Button" style="display:none;" /> 
                            <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white" border="0">
                                <tr>
                                    <td style="height: 11px" width="820" background="../../images/separator_repeat.gif" colspan="5" height="11"> </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="right" valign="middle">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 11px" width="820" background="../../images/separator_repeat.gif"
                                        colspan="5" height="11">
                                    </td>
                                </tr>
                                <tr id = "tr_errorMessage" runat = "server" >
                                    <td align="center">
                                        <asp:Label ID="lblMessage" runat="server" Width="320px" CssClass="label" ForeColor="Red" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp35:UpdateProgress ID="updateprogress1" runat="server">                                        
                                            <ProgressTemplate>
                                                <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                    CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                            </ProgressTemplate>
                                        </asp35:UpdateProgress>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdData" runat="server" align="center" colspan="2" valign="top">
                                        <asp:GridView AllowPaging="true" HeaderStyle-HorizontalAlign="Left" PageSize="25" 
                                            ID="dg_lmsAddresses" runat="server" OnRowDataBound="dg_lmsAddresses_RowDataBound" 
                                            OnPageIndexChanging = "dg_lmsAddresses_PageIndexChanging" BorderColor="DarkGray"
                                            CssClass="clsLeftPaddingTable" AutoGenerateColumns="False"
                                             BorderStyle="Solid" Width="100%" PagerSettings-Mode="NextPreviousFirstLast">
                                            <Columns>
                                                <asp:TemplateField HeaderText = "S#">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_serialNo" CssClass="clsLeftPaddingTable" runat = "server" Text = '<%# Bind("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText = "Letter Type">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_letterType" CssClass = "clsLeftPaddingTable" runat = "server" Text = '<%# Bind("LetterType") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText = "Mail Date">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_mailDate" CssClass = "clsLeftPaddingTable" runat = "server" Text = '<%# Bind("LetterPrintDate") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText = "Cause Number">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_causenumber" CssClass = "clsLeftPaddingTable" runat = "server" Text = '<%# Bind("CauseNumber") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText = "Last Name">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_lastName" CssClass = "clsLeftPaddingTable" runat = "server" Text = '<%# Bind("LastName") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField  HeaderText = "First Name">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_firstName" CssClass = "clsLeftPaddingTable" runat = "server" Text = '<%# Bind("FirstName") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField  HeaderText = "Existing Address">
                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign = "Left" Width="310px"/>
                                                    <ItemTemplate>
                                                        <asp:Label ID = "lbl_existingAddress" CssClass = "clsLeftPaddingTable" runat = "server" Text = '<%# Bind("ExistingAddress") %>' />
                                                         <asp:HiddenField ID = "hf_recordId" runat = "server" Value = '<%# Eval("RecordId") %>' />
                                                        <asp:HiddenField ID = "hf_letterId" runat = "server" Value = '<%# Eval("LetterID_PK") %>' />
                                                        <asp:HiddenField ID = "hf_NoteId" runat = "server" Value = '<%# Eval("NoteId") %>' />
                                                        <asp:HiddenField ID = "hf_newAddress1" runat = "server" Value = '<%# Eval("NewAddress1") %>' />
                                                        <asp:HiddenField ID = "hf_NewCity1" runat = "server" Value = '<%# Eval("NewCity1") %>' />
                                                        <asp:HiddenField ID = "hf_NewState1" runat = "server" Value = '<%# Eval("NewState1") %>' />
                                                        <asp:HiddenField ID = "hf_NewZipCode1" runat = "server" Value = '<%# Eval("NewZipCode1") %>' />
                                                        <asp:HiddenField ID = "hf_newAddress2" runat = "server" Value = '<%# Eval("NewAddress2") %>' />
                                                        <asp:HiddenField ID = "hf_NewCity2" runat = "server" Value = '<%# Eval("NewCity2") %>' />
                                                        <asp:HiddenField ID = "hf_NewState2" runat = "server" Value = '<%# Eval("NewState2") %>' />
                                                        <asp:HiddenField ID = "hf_NewZipCode2" runat = "server" Value = '<%# Eval("NewZipCode2") %>' />
                                                        <asp:HiddenField ID = "hf_IsNewAddress2Updated" runat = "server" Value = '<%# Eval("IsNewAddress2Updated") %>' />
                                                        <asp:HiddenField ID = "hf_FirstName" runat = "server" Value = '<%# Eval("FirstName") %>' />
                                                        <asp:HiddenField ID = "hf_LastName" runat = "server" Value = '<%# Eval("LastName") %>' />
                                                        <asp:HiddenField ID = "hf_ExistingAddress" runat = "server" Value = '<%# Eval("ExistingAddress") %>' />
                                                        <asp:HiddenField ID = "hf_CauseNumbers" runat = "server" Value = '<%# Eval("CauseNumbers") %>' />
                                                        <asp:HiddenField ID = "hf_IsNoNewAddressMarked" runat = "server" Value = '<%# Eval("IsNoNewAddressMarked") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings NextPageText = "Next >&nbsp;&nbsp;" LastPageText="Last Page >>&nbsp;&nbsp;" PreviousPageText = "< Previous&nbsp;&nbsp;" FirstPageText="<< First&nbsp;&nbsp;" Visible ="true" Position = "Bottom" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                     </ContentTemplate>
                     <Triggers>
                           <asp35:AsyncPostBackTrigger ControlID = "btn_Submit" EventName = "click" />
                    </Triggers>
                    </asp35:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableComment" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                        border="0">
                        <tr class="clsLeftPaddingTable">
                            <td class="clsaspcolumnheader">
                            </td>
                            <td class="clsaspcolumnheader" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td width="780" background="../../images/separator_repeat.gif" colspan="5" height="11">
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>

﻿

<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" Codebehind="FrmVoidPayment.aspx.cs" AutoEventWireup="false" Inherits="lntechNew.Reports.FrmVoidPayment" smartNavigation="False"%>
<%--<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>--%>

<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<%--<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>--%>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Void Payment Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />

         <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
            <asp35:ScriptManager ID="ScriptManager1" runat="server"></asp35:ScriptManager>
			 <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" >
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>
                

                 </div>

                 <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl_Message" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>

                 </div>
                 </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">  Void Payment</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div> <%--(moiz check it--%>

               <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">  Void Payment</h2>
                     <div class="actions panel_actions pull-right">

                         
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label"> <strong>From:</strong></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <%--<picker:datepicker id="Calendarpopup1" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>

                                    <picker:datepicker id="Calendarpopup1" runat="server" Dateformat="mm/dd/yyyy" Enabled="true">

                                    </picker:datepicker>


                                   <%-- <ew:calendarpopup id="Calendarpopup1" runat="server" Width="84px" ToolTip="Select Report Date"
											PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
											ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma"
											Font-Size="8pt" ImageUrl="../images/calendar.gif">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup>--%>


                                    </div>
                                                                </div>
                         </div>
                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label"><strong>To:</strong></label>
                                <span class="desc"></span>
                                <div class="controls">
                                       <picker:datepicker id="Calendarpopup2" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                   <%--<ew:calendarpopup id="Calendarpopup2" runat="server" Width="84px" ToolTip="Select Report Date"
											PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
											ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma"
											Font-Size="8pt" ImageUrl="../images/calendar.gif">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup>--%>


                                    </div>
                                                                </div>
                         </div>


                     <div class="clearfix"></div>


                     <div class="col-md-12">
                                                            <div class="form-group">
                             <%-- <label class="form-label"> <strong>From:</strong></label>--%>
                                <span class="desc"></span>
                                <div class="controls">

                                    

                                  <asp:imagebutton id="Btn_search" CssClass="btn btn-primary" runat="server" ImageUrl="../Images/search.jpg"></asp:imagebutton>
                                    <asp:imagebutton id="btn_Reset" CssClass="btn btn-primary" runat="server" ImageUrl="../Images/reset.jpg"></asp:imagebutton>


                                    </div>
                                                                </div>
                         </div>
                                                                </div>
                         </div>
                   </section>


            <div class="clearfix"></div>


            
               <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">  Void Payment Report</h2>
                     <div class="actions panel_actions pull-right">

                         
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label"> <strong>From:</strong></label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <div class="table-responsive" data-pattern="priority-columns">
                                    <asp:datagrid id="dg_VoidReport" runat="server" Width="" Font-Names="Verdana" Font-Size="2px" CssClass="table table-small-font table-bordered table-striped"
								ShowFooter="True" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" AllowSorting="True">
								<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
								<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
								<FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></FooterStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Record ID">
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:LinkButton id="lblSno" runat="server" CssClass="form-label" ForeColor="Black" 
												CommandName="MkSession"></asp:LinkButton>&nbsp;
											<asp:LinkButton id=HLTicketno runat="server" Width="56px" Font-Size="XX-Small" ForeColor="Black"  CommandName="MkSession" Text='<%# DataBinder.Eval(Container.DataItem, "TicketID") %>' Visible="False">
											</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Firstname" HeaderText="First Name">
										<HeaderStyle HorizontalAlign="Center" ForeColor="#006699" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label6 runat="server" CssClass="form-label" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Lastname" HeaderText="Last Name">
										<HeaderStyle HorizontalAlign="Center" ForeColor="#006699" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=Label4 runat="server" CssClass="form-label" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Payment Amount">
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=lblPaymentAmout runat="server" CssClass="form-label" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'>
											</asp:Label>&nbsp;
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Payment Date">
										<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=Label3 runat="server" CssClass="form-label" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentDate") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Font-Size="XX-Small" HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"
									Mode="NumericPages"></PagerStyle>
							</asp:datagrid>
                                         </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                   </section>

















            </section>
                     </section>
                 </div>
				
				<asp:LinkButton id="lnk" ForeColor="White" Runat="server"></asp:LinkButton>
		</form>
         <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script language="JavaScript" type="text/javascript" src="../Scripts/wz_tooltip.js"></script>
        <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
	</body>
</HTML>

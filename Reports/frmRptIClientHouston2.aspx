<%@ Page Language="C#" AutoEventWireup="true" Codebehind="frmRptIClientHouston2.aspx.cs"
    Inherits="HTP.Reports.frmRptIClientHouston2" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Internet Clients - Houston</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
            <tbody>
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="12">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="clsleftpaddingtable" id="Table1" cellspacing="1" cellpadding="1" width="100%"
                            bordercolorlight="#ffffff" border="0">
                            <tr>
                                <td style="width: 37px" class="clssubhead">
                                    From:</td>
                                <td style="width: 147px">
                                    <ew:CalendarPopup ID="calDateFrom" runat="server" EnableHideDropDown="True" SelectedDate="2006-02-21"
                                        Width="117px" ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif"
                                        CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                        Nullable="True" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                        ToolTip="Mail Date From" DisplayOffsetY="20" Font-Names="Tahoma" Font-Size="8pt">
                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></WeekdayStyle>
                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGray"></WeekendStyle>
                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></HolidayStyle>
                                    </ew:CalendarPopup>
                                </td>
                                <td style="width: 25px" class="clssubhead">
                                    &nbsp;To:</td>
                                <td style="width: 195px">
                                    <ew:CalendarPopup ID="calDateTo" runat="server" EnableHideDropDown="True" SelectedDate="2006-02-21"
                                        Width="116px" ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif"
                                        CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                        Nullable="True" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                        ToolTip="Mail Date From" DisplayOffsetY="20" Font-Names="Tahoma" Font-Size="8pt">
                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></WeekdayStyle>
                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGray"></WeekendStyle>
                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></HolidayStyle>
                                    </ew:CalendarPopup>
                                </td>
                                <td style="width: 4px">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsbutton" Width="87px"
                                        OnClick="btnSubmit_Click"></asp:Button></td>
                                <td valign="middle" align="right">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td style="width: 980px">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td style="width: 896px;" align="center">
                                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label></td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px"
                                    valign="middle">
                                    <uc2:PagingControl ID="PagingControl1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" background="../Images/separator_repeat.gif" style="width: 896px;
                                    height: 11px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="clslabel" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gv_Results" runat="server" Width="980px" AutoGenerateColumns="False"
                                        BackColor="#EFF4FB" BorderColor="White" CssClass="clsLeftPaddingTable" AllowPaging="True"
                                        AllowSorting="True" PageSize="30" OnPageIndexChanging="gv_PageIndexChanging" Height="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="S#">
                                                <HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlkSNo" runat="server" Text='<%#bind("SNO") %>' NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber=" + Eval("TicketId")   %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="customerid" HeaderText="Cust ID" Visible="False" >
                                                <ItemStyle CssClass="label" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Name">
                                                <HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlkName" runat="server" NavigateUrl='<%#  ConfigurationManager.AppSettings["SullolawURL"] +  "/Reports/OnlineReport.aspx?CustomerID=" + Eval("CustomerId") %>'
                                                        Text='<%#bind("Name") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="HomeAddress" HeaderText="Address" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="city" HeaderText="City , State" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="zip" HeaderText="Zip" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="crnumber" HeaderText=" Credit Card" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="amount" HeaderText="Amount" DataFormatString="{0:C0}" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="approvedflag" HeaderText="Status" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="telephone" HeaderText="Phone" >
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="recdate" HeaderText="Transaction Date"  DataFormatString="{0:d}">
                                                <ItemStyle CssClass="GridItemStyleBig" />
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" FirstPageText="&amp;lt;&amp;lt; First" LastPageText="Last &amp;gt;&amp;gt;" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td height="11">
                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>

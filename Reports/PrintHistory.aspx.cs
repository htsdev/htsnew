using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.WebControls;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Reports
{
    public partial class PrintHistory : System.Web.UI.Page
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dg_Viewbug_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string pid = ((Label)e.Item.FindControl("lblPID")).Text.ToString();
                    string type = ((Label)e.Item.FindControl("lbltype")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("HPView")).Attributes.Add("OnClick", "javascript:return PopUpSummaryPreview('" + type + "-" + pid + "');");
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindGrid()
        {
            try
            {
                lblMessage.Text = "";
                if (cal_todate.SelectedDate.ToShortDateString() != "1/1/0001" && cal_fromDate.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    string[] key ={ "@StartDate", "@EndDate", "@ReportType" };
                    object[] value ={ cal_todate.SelectedDate.ToString(), cal_fromDate.SelectedDate.ToString(),"1" };
                    DataSet ds = clsDb.Get_DS_BySPArr("usp_hts_GetPrintSummaryHistory_Appearance", key, value);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dgPrintHistory.DataSource = ds;
                        dgPrintHistory.DataBind();

                    }
                    else
                    {


                        lblMessage.Text = "No Record Found";
                        dgPrintHistory.DataSource = ds;
                        dgPrintHistory.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
        }


    }
}

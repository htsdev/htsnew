using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.Reports
{
    public partial class GeneralComments : System.Web.UI.Page
    {
        public int ticketid = 0;
        public int empid = 0 ;
        public int commentsid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
                empid = Convert.ToInt32(Request.QueryString["empid"]);
                commentsid = Convert.ToInt32(Request.QueryString["commentsid"]);
                WCC_GeneralComments.Initialize(ticketid,empid,commentsid );
            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (WCC_GeneralComments.FullText.Length > 0)
            {
                WCC_GeneralComments.AddComments();
            }
            HttpContext.Current.Response.Write("<script language = 'javascript'>  opener.focus(); opener.location.href = opener.location; self.close();    </script>");
        }
    }
}

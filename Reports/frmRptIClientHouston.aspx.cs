using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmRptIClientHouston.
	/// </summary>
	public partial class frmRptIClientHouston : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.DataGrid dg_results;
		protected eWorld.UI.CalendarPopup dateto;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected eWorld.UI.CalendarPopup datefrom;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.DropDownList ddlPageNo;
		protected System.Web.UI.HtmlControls.HtmlTable tblPageNavigation;
	
		clsENationWebComponents ClsDB = new clsENationWebComponents("traffic");
		clsSession ClsSession =new clsSession();
		clsLogger bugTracker = new clsLogger();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
			
			
				if ( ! IsPostBack)
				{
					dateto.SelectedDate = DateTime.Now;
					datefrom.SelectedDate= DateTime.Now;
					tblPageNavigation.Visible=false;
				
			
				}
			
				lblMessage.Text= "";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.ddlPageNo.SelectedIndexChanged += new System.EventHandler(this.ddlPageNo_SelectedIndexChanged);
			this.dg_results.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_results_PageIndexChanged);
			this.dg_results.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_results_ItemDataBound);
			this.dg_results.SelectedIndexChanged += new System.EventHandler(this.dg_results_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
				
			bindData();


		}

		private void dg_results_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
					((Label)e.Item.FindControl("lbl_status")).Text="Accept";
                    ((HyperLink)e.Item.FindControl("hhl_name")).NavigateUrl = "http://www.sullolaw.com/files/report.aspx?CustomerID=" + ((Label)e.Item.FindControl("lbl_cid")).Text;
                    if (((Label)e.Item.FindControl("lbl_ticketid")).Text.Equals("0") == false )
                        ((HyperLink)e.Item.FindControl("lbl_sno")).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("lbl_ticketid")).Text;
        	}
			catch
			{


			}


		}

		private void SetNavigation()
		{
			// Procedure for setting data grid's current  page number on header ........
			
			try
			{
				// setting current page number
				lblCurrPage.Text =Convert.ToString(dg_results.CurrentPageIndex+1);

				for (int idx =0 ; idx < ddlPageNo.Items.Count ;idx++)
				{
					ddlPageNo.Items[idx].Selected = false;
				}
				ddlPageNo.Items[dg_results.CurrentPageIndex].Selected=true;
			
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}


		public void  updateDataSet( DataSet ds )
		{
		
			ds.Tables[0].Columns.Add("Color");
			ds.Tables[0].Columns.Add("NavigateUrl");


			foreach ( DataRow dr in ds.Tables[0].Rows)
			{

				if ( dr["approvedflag"].ToString()=="0")
				{
					
					dr["Color"]="RED";
					dr["NavigateUrl"]="";
				
				}
				else
				{
					dr["Color"]="BLACK";
					dr["NavigateUrl"]="Home.aspx?CustomerID" + dr["id_customer"].ToString();
				}

			}
		
		}

		private void dg_results_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		
		
		public void bindData()
		{
		
			try
			{

				string[] keys={ "@FromDate","@Todate"};
				object[] values = {datefrom.SelectedDate.ToShortDateString(),dateto.SelectedDate.ToShortDateString()};

				// Finding List Of Clients

				DataSet ds = ClsDB.Get_DS_BySPArr("usp_HTS_Get_TicketTransaction1",keys,values);
				
				//updateDataSet(ds);
				if ( ds.Tables[0].Rows.Count > 0 )
				{	
					dg_results.DataSource = ds;
					dg_results.DataBind();
					GenerateSerial();
					FillPageList();
					SetNavigation();
					tblPageNavigation.Visible=true;
					dg_results.Visible=true;

				
				}
				else
				{
					lblMessage.Text="No Record Found";
					tblPageNavigation.Visible=false;
					dg_results.Visible=false;

				}
							
				
			}
			catch(Exception ex)
			{
				lblMessage.Text="No Record Found";
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}
		
		public  void GenerateSerial()
		{
			// Procedure for writing serial numbers in "S.No." column in data grid.......	

			
			long sNo=(dg_results.CurrentPageIndex)*(dg_results.PageSize);									
			try
			{
				// looping for each row of record.............
				foreach (DataGridItem ItemX in dg_results.Items) 
				{ 					
					sNo +=1 ;
					// setting text of hyper link to serial number after bouncing of hyper link...
					((HyperLink)(ItemX.FindControl("lbl_SNo"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
				 
				}
			}
			catch(Exception ex)

			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		private void dg_results_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg_results.CurrentPageIndex=e.NewPageIndex;
			bindData();

		}

		private void ddlPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg_results.CurrentPageIndex=ddlPageNo.SelectedIndex;
			bindData();
			
		}

		private void FillPageList()
		{
			try
			{
				int i =0;
				ddlPageNo.Items.Clear();
				for(i=1; i<=dg_results.PageCount;i++ )
				{
					ddlPageNo.Items.Add("Page - " + i.ToString());
					ddlPageNo.Items[i-1].Value = i.ToString();
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
	

	}
}

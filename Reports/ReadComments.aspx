﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ReadComments.aspx.cs" Inherits="HTP.Reports.ReadComments" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Read Comments</title>
     <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->


    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <script language="jscript" type="text/jscript">
    function Validation()
    {
      var text = document.getElementById("txtreadcomments").value;
      if(text == "")
      {
       alert("Please Enter Read Comments");
       return false;
      }
      
      //Kazim 3985 5/26/2008 Change the max length of textbox
      
      if(text.length > 950)
      {
           
           alert("Sorry You cannot type in more than 1000 characters in Read comments box")
			return false;		  
      }
      return true;
    }
    </script>

</head>
<%--<body  class="clsleftpaddingtable">--%>
    <body  style="margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px">
    <form id="form1" runat="server">
         <div class="page-container row-fluid container-fluid">
        <section id="main-contentpopup" class="">
    <section class="wrapper main-wrapper row" style="">
        <%--<div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Read Comments</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>


            </div>--%>

        <div class="clearfix"></div>

         <div class="col-xs-12">

             <section class="box" id="">
                <%-- <header class="panel_header">
                     <h2 class="title pull-left">Scan/Upload</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>--%>
                 <div class="content-body">
            <div class="row">

                 <div class="col-md-12">
                                                        <div class="form-group">
                            <label class="form-label">Read Comments</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <%--<asp:TextBox ID="txtreadcomments" runat="server" Height="116px" TextMode="MultiLine"
                                    Width="348px" CssClass="form-control" MaxLength="500"></asp:TextBox>--%>
                                <asp:TextBox ID="txtreadcomments" runat="server"  TextMode="MultiLine"
                                     CssClass="form-control" MaxLength="500"></asp:TextBox>
                                <asp:Button ID="Button1" runat="server" Text="Submit " CssClass="btn btn-primary" OnClick="Button1_Click" OnClientClick="return Validation();" />
                                </div>
                                                            </div>
                     </div>
                </div>
                     </div>
                 </section>

















             </div>











        </section>
            </section>
        </div>
    </form>
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

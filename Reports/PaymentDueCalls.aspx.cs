﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    // Noufil 5618 04/02/2009 New report for Future Payment follow update.
    public partial class PaymentDueCalls : System.Web.UI.Page
    {
        PaymentPlan Clspaymentplan = new PaymentPlan();
        //Nasir 6049 07/10/2009 declare variable dv
        DataView dv;

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblMessage.Text = "";
                    fillgrid();
                    //Nasir 6049 07/11/2009 Page size control
                    Pagingctrl.Size = lntechNew.WebControls.RecordsPerPage.Thirty;
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }
        }

        public void fillgrid()
        {
            DataTable dt = Clspaymentplan.GetPaymentDueReports();

            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No Records Found";
                Pagingctrl.Visible = false;
            }
            else
            {


                //Nasir 6049 07/10/2009 For Page size change
                if (Session["DV_PaymentDueCall"] == null)
                {
                    dv = new DataView(dt);
                    Session["DV_PaymentDueCall"] = dv;
                }
                else
                {
                    dv = (DataView)Session["DV_PaymentDueCall"];
                    string SortExp = dv.Sort;
                    dv = new DataView(dt);
                    dv.Sort = SortExp;
                    dt = dv.ToTable();
                    if (dt.Columns.Contains("SNo"))
                    {
                        dt.Columns.Remove("SNo");
                    }
                }

                lblMessage.Text = "";
                Pagingctrl.Visible = true;
                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["TicketID_PK"].ToString() != dt.Rows[i]["TicketID_PK"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }

                //Nasir 6049 07/16/2009 sorting
                dv = dt.DefaultView;

                gv_records.Visible = true;
                //Nasir 6049 07/10/2009 For Page size change
                gv_records.DataSource = dv;
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            fillgrid();
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    fillgrid();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {

            fillgrid();

        }
        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lblMessage.Text = ErrorMsg;

        }

        //Nasir 6049 07/10/2009 For record sorting
        /// <summary>
        /// sort the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {

            dv = (DataView)Session["DV_PaymentDueCall"];
            dv.Sort = sortExpression + " " + direction;
            Session["DV_PaymentDueCall"] = dv;

            fillgrid();
            Pagingctrl.PageCount = gv_records.PageCount;
            Pagingctrl.PageIndex = gv_records.PageIndex;

            ViewState["sortExpression"] = sortExpression;
            Session["SortDirection"] = direction;
            Session["SortExpression"] = sortExpression;

        }
    }
}


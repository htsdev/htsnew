using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmRptFTAThree.
	/// </summary>
	public partial class frmRptFTAThreeDetail: System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgFTA;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsSession cSession = new clsSession();
		clsLogger bugTracker = new clsLogger();
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Label lblTotal;
		string mDate= String.Empty;
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblMailDate;
		string mDetail =  String.Empty;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			mDate = Request.QueryString["maildate"];
			mDetail = Request.QueryString["detail"];

			

			if (mDate != null && mDate != "" && mDetail != null && mDetail != "")
			{
				
				if (mDetail.ToUpper() == "HIRED")
					lblType.Text = "HIRED CLIENTS";
				else
					lblType.Text = "QUOTED CLIENTS";

				lblMailDate.Text =mDate;
				FillGrid();
				GenerateSerial();
				CalculateTotal();
				SetHyperLinks();
			}
			else
			{
				lblMessage.Text = "Incorrect value passed in query string.";
			}
		}

		private void CalculateTotal()
		{	double dTotal =0;
			foreach (DataGridItem ItemX in dgFTA.Items)
			{
				string sAmount = ItemX.Cells[3].Text;	
				sAmount = sAmount.Substring(1,sAmount.Length -1);
				dTotal +=   ((double) (Convert.ChangeType(sAmount,typeof(double))));  
			}
			lblTotal.Text = "Total: $" + dTotal.ToString();
		}

		private void SetHyperLinks()
		{
			foreach(DataGridItem ItemX in dgFTA.Items)
			{
				string sTicketId =  ((Label) ( ItemX.FindControl("lblTicketId"))).Text;
				//((HyperLink) (ItemX.FindControl("hlkCaseNo"))).NavigateUrl="../ClientInfo/violationsfees.asp?search=0&caseNumber=" + sTicketId ;
				if  (lblType.Text.ToUpper() ==  "HIRED CLIENTS")
					((HyperLink) (ItemX.FindControl("hlkCaseNo"))).NavigateUrl="../ClientInfo/ViolationFeeOld.aspx?search=0&casenumber=" + sTicketId ;
				else
					((HyperLink) (ItemX.FindControl("hlkCaseNo"))).NavigateUrl="../ClientInfo/ViolationFeeOld.aspx?search=1&casenumber=" + sTicketId ;
			}
		}
		
		private void FillGrid()
		{
			try
			{
				DateTime MailDate =  ((DateTime) (Convert.ChangeType( mDate ,typeof(DateTime)))  )   ;
				
				string [] key = {"@MailDate","@DetailType"};
				object [] value1 = {MailDate, mDetail };

				dgFTA.DataSource = ClsDb.Get_DS_BySPArr("USP_HTS_GET_FTAThreeReportDetail",key, value1);
				dgFTA.DataBind();
				}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		private void GenerateSerial()
		{
			// Procedure for writing serial numbers in "S.No." column in data grid.......	

			
			long sNo=(dgFTA.CurrentPageIndex)*(dgFTA.PageSize);									
			try
			{
				// looping for each row of record.............
				foreach (DataGridItem ItemX in dgFTA.Items) 
				{ 					
					sNo +=1 ;
					// setting text of hyper link to serial number after bouncing of hyper link...
					((Label)(ItemX.FindControl("lblSerial"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
				 
				}
			}
			catch(Exception ex)

			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
	}
}

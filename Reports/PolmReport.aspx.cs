﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.POLMCLient;
using lntechNew.Components.ClientInfo;
using POLM.ProspectWCFClient;

namespace HTP.Reports
{
    public partial class PolmReport : System.Web.UI.Page
    {
        PolmController polmController = new PolmController();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FilCaseStatus();
                    FillAttorney();
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        private void FillGrid()
        {
            ProspectReportDto prospectDto = new ProspectReportDto();

            if (string.IsNullOrEmpty(Convert.ToString(calstartdate.SelectedDate)))
                prospectDto.AssignStartDate = null;
            else
                prospectDto.AssignStartDate = calstartdate.SelectedDate;

            if (string.IsNullOrEmpty(Convert.ToString(calenddate.SelectedDate)))
                prospectDto.AssignEndDate = null;
            else
                prospectDto.AssignEndDate = calenddate.SelectedDate;

            if (Convert.ToString(ddCaseStatus.SelectedValue) == "-1")
                prospectDto.CaseStatus = null;
            else
                prospectDto.CaseStatus = Convert.ToInt32(ddCaseStatus.SelectedValue);

            if (Convert.ToString(ddAttorney.SelectedValue) == "-1")
                prospectDto.AssignAttorney = null;
            else
                prospectDto.AssignAttorney = Convert.ToInt32(ddAttorney.SelectedValue);

            prospectDto.FirstName = null;
            prospectDto.LastName = null;
            prospectDto.OpenedDate = null;
            prospectDto.AssignedDate = null;
            prospectDto.LastContactDate = null;
            prospectDto.FollowupDate = null;
            prospectDto.Source = null;
            prospectDto.refferingAtotrney = null;

            prospectDto.Division = null;
            prospectDto.ShowAll = null;

            gvPolm.DataSource = polmController.GetProspectReport(prospectDto);
            gvPolm.DataBind();
        }

        private void FillAttorney()
        {
            ddAttorney.DataSource = polmController.GetAllAttorney();
            ddAttorney.DataBind();
            ddAttorney.Items.Insert(0, new ListItem("--- Choose ---", "-1"));

        }

        private void FilCaseStatus()
        {
            ddCaseStatus.DataSource = polmController.GetAllCaseStatus(true);
            ddCaseStatus.DataBind();
            ddCaseStatus.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="CurrentClientCourtDateChecker.aspx.cs"
    MaintainScrollPositionOnPostback="true" Inherits="HTP.Reports.CurrentClientCourtDateChecker" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateViolationSRV.ascx" TagName="UpdateViolationSRV"
    TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Current Client Court Date Checker</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script>

    </script>

</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <%-- Abbas Qamar 9223 07/16/2011 added the 6 minute because script manager was time out on the page AsyncPostBackTimeout="360000"--%>
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780"
        align="center" border="0">
        <tr>
            <td style="height: 14px" colspan="4">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td style="height: 9px" background="../images/separator_repeat.gif">
            </td>
        </tr>
        <tr>
            <td>
                <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clsLeftPaddingTable" colspan="2" width="100%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="5" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                            <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </ContentTemplate>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11" align="center">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" colspan="2" style="height: 144px">
                <aspnew:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                    <ProgressTemplate>
                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                            CssClass="clsLabel"></asp:Label>
                    </ProgressTemplate>
                </aspnew:UpdateProgress>
                <table id="tbl1" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td align="center" width="100%">
                            <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dg_records" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                                        OnRowCommand="dg_records_ItemCommand" AllowPaging="True" AllowSorting="True"
                                        PageSize="20" OnRowDataBound="dg_records_RowDataBound" OnPageIndexChanging="dg_records_PageIndexChanged"
                                        AutoGenerateColumns="False">
                                        <PagerStyle NextPageText="   Next &gt;" HorizontalAlign="Center" PrevPageText="  &lt; Previous       "
                                            VerticalAlign="Middle"></PagerStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="S#">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlnk_SNo" runat="server" CssClass="GridItemStyle" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>' />
                                                    <asp:HiddenField ID="hf_bondamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.Bondamount","{0:F0}") %>' />
                                                    <asp:HiddenField ID="hf_fineamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.FineAmount","{0:F0}") %>' />
                                                    <asp:HiddenField ID="hf_courtloc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.shortname") %>' />
                                                    <asp:HiddenField ID="hf_priceplan" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' />
                                                    <asp:HiddenField ID="hf_bondflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' />
                                                    <asp:HiddenField ID="hf_violationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>' />
                                                    <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>' />
                                                    <asp:HiddenField ID="hf_courtviolationstatusid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Vrs") %>' />
                                                    <asp:HiddenField ID="hf_categoryid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.vtypeid") %>' />
                                                    <asp:HiddenField ID="hf_courtid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' />
                                                    <asp:HiddenField ID="hf_OscareCourt" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.OscarCourtDetail") %>' />
                                                    <asp:HiddenField ID="hf_ticketNumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refcasenumberseq") %>' />
                                                    <asp:HiddenField ID="hf_courtdate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtdatemain_1","{0:d}") %>' />
                                                    <asp:HiddenField ID="hf_crm" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' />
                                                    <asp:HiddenField ID="hf_ctime" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain_1","{0:t}") %>' />
                                                    <asp:HiddenField ID="hf_VDesc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>' />
                                                    <asp:HiddenField ID="hf_courtstatus" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.VDesc") %>' />
                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ticketid_pk") %>' />
                                                    <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.iscriminalcourt") %>' />
                                                    <asp:HiddenField ID="hf_chargelevel" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.chargelevel") %>' />
                                                    <asp:HiddenField ID="hf_cdi" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.cdi") %>' />
                                                    <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.isFTAViolation") %>' />
                                                    <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasFTAViolations") %>' />
                                                    <asp:HiddenField ID="hf_hasNOS" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasNOS") %>' />
                                                    <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value='<%# Bind("coveringFirmId") %>' />
                                                    <asp:HiddenField ID="hf_arrestdate" runat="server" Value='<%# Bind("arrestdate","{0:d}") %>' />
                                                    <asp:HiddenField ID="hf_casetype" runat="server" Value='<%# Bind("casetypeid") %>' />
                                                    <asp:HiddenField ID="hf_missedcourttype" runat="server" Value='<%# Bind("missedcourttype") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="CaseNo" Visible="false">
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Cause No" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CauseNo" runat="server" CssClass="GridItemStyle" Text='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ticket No" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TicketNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumberseq") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Last Name" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Verified Court Info" HeaderStyle-CssClass="clssubhead"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVerifiedCourtInfo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Verified_Court_Info") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Verified Court Status" HeaderStyle-CssClass="clssubhead"
                                                HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVerifiedCourtStatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Verified_Status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Court Info" HeaderStyle-CssClass="clssubhead"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAutoCourtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Auto_Court_Info") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Court Status" HeaderStyle-CssClass="clssubhead"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAutoStatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Auto_Status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkb_manverify" runat="server" CommandName="Clicked">Manual Verify</asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Lnk_Change" CommandArgument='<%#Eval("TicketsViolationID") %>'
                                                        runat="server" CommandName="NoChangeClick">No Change</asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                    <table style="display: none" id="tbl_plzwait1" runat="server">
                                        <tbody>
                                            <tr>
                                                <td style="width: 785px" class="clssubhead" valign="middle" align="center">
                                                    <img src="../Images/plzwait.gif" />
                                                    Please wait While we update your Violation information.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="780" background="../images/separator_repeat.gif" colspan="5" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <uc4:UpdateViolationSRV runat="server" ID="UpdateViolationSRV1" GridID="dg_records"
        GridField="hf_ticketviolationid" DivID="tbl_plzwait1" IsCaseDisposition="false" />
    </form>
</body>
</html>

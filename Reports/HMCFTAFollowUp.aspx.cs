﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

#region Created By
//Waqas Javed 5697 03/26/2009
#endregion 

namespace HTP.Reports
{
    public partial class HMCFTAFollowUp : System.Web.UI.Page
    {
        #region Properties
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        #endregion

        #region Variables
        clsENationWebComponents cDb = new clsENationWebComponents();
        clsLogger cBugTracker = new clsLogger();
        clsCase cCase = new clsCase();
        clsSession cSession = new clsSession();
        #endregion 

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                if (!IsPostBack)
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    //Waqas 5756 04/08/2009 For Sorting 
                    GridViewSortExpression = "HMCFTAFollowUpDateSort";
                    //Yasir Kamal 5947 05/20/2009 allow to set default size of Records per page
                    Pagingctrl.Size = lntechNew.WebControls.RecordsPerPage.All;
                    gv_Data.AllowPaging = false;
                    GetRecords();
                }
                UpdateFollowUpInfo.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo_PageMethod);
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_Data;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }        

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string firstname = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_fname")).Value);
                    string causeno = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_causeno")).Value);
                    string ticketno = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketnumber")).Value);
                    string lastname = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_lname")).Value);
                    string courtname = (((HiddenField)gv_Data.Rows[RowID].FindControl("hf_courtid")).Value);
                    string followupDate = (((Label)gv_Data.Rows[RowID].FindControl("lbl_followup")).Text);

                    followupDate = (followupDate.Trim() == "") ? "" : followupDate;

                    cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketid_pk")).Value));
                    string comm = cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo.Freezecalender = true;

                    UpdateFollowUpInfo.Title = "HMC FTA Follow-Up";
                    UpdateFollowUpInfo.followUpType = HTP.Components.FollowUpType.HMCFTAFollowUpDate;

                    DateTime NextDate = new DateTime();
                    if (followupDate == "")
                    {
                        NextDate = clsGeneralMethods.GetBusinessDayDate(DateTime.Now, 2);
                    }
                    else
                    {
                        NextDate = Convert.ToDateTime(followupDate);
                    }

                    UpdateFollowUpInfo.binddate("5", NextDate, DateTime.Today, firstname, lastname, ticketno, ((HiddenField)gv_Data.Rows[RowID].FindControl("hf_ticketid_pk")).Value, causeno, comm, mpeTrafficwaiting.ClientID, "HMC-FTA", "", followupDate);

                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                GetRecords();
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// To Fill data into Grid
        /// </summary>
        private void GetRecords()
        {
            try
            {
                DataTable dt = cDb.Get_DT_BySPArr("USP_HTP_HMC_FTA_FOLLOWUP");

                if (dt.Rows.Count == 0)
                {
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records!";
                }
                else
                {
                    dt.Columns.Add("sno");
                    DataView dv = dt.DefaultView;
                    dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                    dt = dv.ToTable();
                    GenerateSerialNo(dt); ;
                    gv_Data.DataSource = dt;
                    gv_Data.DataBind();
                    Pagingctrl.PageCount = gv_Data.PageCount;
                    Pagingctrl.PageIndex = gv_Data.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method to be called by UpdateFollowUpControl
        /// </summary>
        void UpdateFollowUpInfo_PageMethod()
        {
            try
            {
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method when Page index changed
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Data.PageIndex = 0;
                    gv_Data.PageSize = pageSize;
                    gv_Data.AllowPaging = true;
                }
                else
                {
                    gv_Data.AllowPaging = false;
                }
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        /// <summary>
        /// Method to insert Serial numbers in Gridview
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (!dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        #endregion
    }
}

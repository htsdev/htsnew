﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
namespace HTP.Reports
{
    public partial class RptCeritifiedLORPrint : System.Web.UI.Page
    {        
        string certifiedMailNumber = string.Empty;
        clsSession uSession = new clsSession();
        clsLogger clog = new clsLogger();

        protected void Page_Load( object sender, EventArgs e )
        {
            if ( !Page.IsPostBack )
            {
                if ( uSession.GetCookie( "sEmpID", this.Request ) != "" )
                {
                    if ( Request.QueryString["CMN"] != null )
                    {
                        certifiedMailNumber = Request.QueryString["CMN"].ToString();

                        CreateApprovalReport();
                    }
                }
                else
                {
                    Response.Redirect( "../frmlogin.aspx" );
                }
            }
        }

        public void CreateApprovalReport()
        {   
            if ( certifiedMailNumber != string.Empty )
            {
                clsCrsytalComponent Cr = new clsCrsytalComponent();
                clsENationWebComponents ClsDb = new clsENationWebComponents();

                string filename;
                string filepath;               

                DataSet ds;
                String[] reportname = new String[] { "LORBatchHistoryCoverReport.rpt" };
                string[] keySub = { "@CertifiedMailNumber" };
                object[] valueSub = { certifiedMailNumber };
                ds = ClsDb.Get_DS_BySPArr( "USP_HTP_GET_LORBatchHistoryGroupByTicketID", keySub, valueSub );
                
                string[] key = { "@CertifiedMailNumber","@STATUS" };
                object[] value1 = { certifiedMailNumber, 0 };
                filename = Server.MapPath( "" ) + "\\LORBatchHistoryReport.rpt";
                filepath = Cr.CreateCertifiedLORSubReports( filename, "USP_HTP_GET_CertifiedLetterOfRep", key, value1, reportname, "false", Convert.ToInt32( uSession.GetCookie( "sEmpID", this.Request ) ), ds, this.Session, this.Response );
            }

        }
    }
}

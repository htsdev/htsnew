<%@ Page Language="C#" AutoEventWireup="true" Codebehind="DocketCloseOutRedX.aspx.cs"
    Inherits="HTP.Reports.DocketCloseOutRedX" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Docket Close Out Red X Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;
            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            </aspnew:ScriptManager>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        &nbsp;<uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif"
                            Width="86px" EnableHideDropDown="True">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <TextboxLabelStyle CssClass="clsinputadministration" />
                        </ew:CalendarPopup>
                        <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClick="btn_Submit_Click"
                            Text="Submit" /></td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td id="Td1" runat="server" align="right" class="clslabel" colspan="1" style="border-left: medium none;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_sort" runat="server"
                            visible="false">
                            <tr>
                                <td class="clslabel" style="height: 25px" colspan="2">
                                    <asp:ImageButton ID="ibtn_Docs" runat="server" ImageUrl="~/Images/head_icon.gif" />
                                    <asp:Label ID="lbl_Title" runat="server" Font-Bold="True"></asp:Label></td>
                                <td class="clslabel" style="width: 100px; height: 20px">
                                </td>
                                <td style="width: 100px; height: 20px">
                                </td>
                                <td align="right" colspan="2" style="height: 20px">
                                </td>
                            </tr>
                            <tr>
                                <td class="clslabel" style="width: 100px; height: 20px">
                                    Court Location</td>
                                <td style="width: 100px; height: 20px">
                                    <asp:DropDownList ID="ddl_courts" runat="server" CssClass="clsinputadministration"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
                                    </asp:DropDownList></td>
                                <td class="clslabel" style="width: 100px; height: 20px">
                                    Auto Statuses</td>
                                <td style="width: 100px; height: 20px">
                                    <asp:DropDownList ID="ddl_status" runat="server" CssClass="clsinputadministration"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">ALL</asp:ListItem>
                                        <asp:ListItem Value="1">DLQ/FTA</asp:ListItem>
                                        <asp:ListItem Value="2">Appearance</asp:ListItem>
                                        <asp:ListItem Value="3">Disposed</asp:ListItem>
                                        <asp:ListItem Value="4">NIR</asp:ListItem>
                                        <asp:ListItem Value="5">Blank</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 20px" align="left">
                                </td>
                                <td style="height: 20px" align="right">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td id="td_sep" runat="server" align="right" visible="false" class="clslabel" colspan="1"
                        style="border-left: medium none; height: 11px" background="../Images/separator_repeat.gif">
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table width="785">
                                    <tbody>
                                        <tr>
                                            <td align="center">
                                                <asp:GridView ID="gv_records" AllowPaging="false" runat="server" AutoGenerateColumns="False"
                                                    OnRowDataBound="gv_records_RowDataBound" Width="780px" CssClass="clsleftpaddingtable">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# Eval("SNo") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HyperLink ID="hl_sno" runat="server" Text='<%# Eval("SNo") %>' NavigateUrl='<%# "../ClientInfo/CaseDisposition.aspx?search=0&casenumber=" +Eval("TicketID_pk") %>'></asp:HyperLink>
                                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' />
                                                                <asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' />
                                                                <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                                                <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' />
                                                                <asp:HiddenField ID="hf_discrepency" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.discrepency") %>' />
                                                                <asp:HiddenField ID="hf_showquestionmark" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.showquestionmark") %>' />
                                                                <asp:HiddenField ID="hf_TicketsViolationID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>' />
                                                                <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID") %>' />
                                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:D}")%>' />
                                                                <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HiddenField ID="hf_MM" runat="server" />
                                                                <asp:HiddenField ID="hf_DD" runat="server" />
                                                                <asp:HiddenField ID="hf_YY" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Bond" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBondFlag" runat="server" Text="" Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last Name ,First Name" HeaderStyle-HorizontalAlign="Left">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="250px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("LastName") + " , " +  Eval("FirstName")%>'
                                                                    Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause No">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="70px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_causeno" runat="server" Text='<%# Eval("CauseNo") %>' Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Snap Shot Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Auto Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_astatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "AutoStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Verified Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_vstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Image ID="img_dis" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" Width="18px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gv_records1" AllowPaging="false" runat="server" AutoGenerateColumns="False"
                                                    OnRowDataBound="gv_records_RowDataBound" Width="780px" CssClass="clsleftpaddingtable">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# Eval("SNo") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HyperLink ID="hl_sno" runat="server" Text='<%# Eval("SNo") %>' NavigateUrl='<%# "../ClientInfo/CaseDisposition.aspx?search=0&casenumber=" +Eval("TicketID_pk") %>'></asp:HyperLink>
                                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' />
                                                                <asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' />
                                                                <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                                                <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' />
                                                                <asp:HiddenField ID="hf_discrepency" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.discrepency") %>' />
                                                                <asp:HiddenField ID="hf_showquestionmark" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.showquestionmark") %>' />
                                                                <asp:HiddenField ID="hf_TicketsViolationID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>' />
                                                                <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID") %>' />
                                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:D}")%>' />
                                                                <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HiddenField ID="hf_MM" runat="server" />
                                                                <asp:HiddenField ID="hf_DD" runat="server" />
                                                                <asp:HiddenField ID="hf_YY" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Bond" HeaderStyle-Font-Size="XX-Small">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBondFlag" runat="server" Text="" Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last Name ,First Name" HeaderStyle-HorizontalAlign="Left">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="250px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("LastName") + " , " +  Eval("FirstName")%>'
                                                                    Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause No">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="70px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_causeno" runat="server" Text='<%# Eval("CauseNo") %>' Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Snap Shot Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Auto Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_astatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "AutoStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Verified Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_vstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Image ID="img_dis" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" Width="18px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" colspan="5" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

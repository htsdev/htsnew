﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BadEmailReport.aspx.cs"
    Inherits="HTP.Reports.BadEmailReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc5" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfoEmailAddress.ascx" TagName="UpdateFollowUpInfoEmailAddress"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bad Email Address</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png" />
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png" />
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png" />
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png" />
    <!-- For iPad Retina display -->
    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->
    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->
    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />


    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            $('.input-group-addon').click(function () {
                $(this).closest('div').find('input').focus();
            });

            $('.datepicker').datepicker({ autoclose: true });
        }
        $('.datepicker').datepicker({ autoclose: true });
    </script>
    <style type="text/css">
        .style2 {
        }

        .style3 {
            width: 13px;
        }

        .style5 {
            width: 100px;
            direction: ltr;
        }

        table .table td {
            background-color: #fff !important;
            color: black !important;
        }

            table .table td a {
                color: blue !important;
            }
             table .table td span {
                background-color: #fff !important;
            }
            table .table td .form-label {
                color: black !important;
                font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
                font-size: 11px !important;
                font-weight:normal !important;
            }
    </style>
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript"></script>--%>
</head>
<body>
    <form id="form1" runat="server">
        <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="ScriptManager2" runat="server" />

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>


            <section id="main-content" class="reminderCallsPage" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">

            <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" Width="320px" CssClass="form-label" ForeColor="Red"
                                            EnableViewState="False"></asp:Label>
                 </div>
           
        </div>

             <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Bad Email Address</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>

               <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">                    
                        <div class="col-md-2">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                    
                                   <asp:CheckBox ID="chkFamilyShowAll" runat="server" CssClass="checkbox-custom" Text="Show All" />
                            </div>
                     <div class="col-md-2">
                                <label class="form-label"> </label>
                                <span class="desc"></span>
                                    <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary pull-right"></asp:Button>
                                    
                         </div>
                    </div>
                      

                     </div>

                 </section>


             <div class="clearfix"></div>
            <aspnew:UpdatePanel ID="upnlResult" runat="server">
                        <ContentTemplate>
            <section class="box" id="TableGrid" style="">
                <header class="panel_header" id="FlagsE">
                     <h2 class="title pull-left"></h2>

                     <div class="actions panel_actions pull-right">
                      <uc3:PagingControl ID="Pagingctrl" runat="server" />
                    <%-- <a class="box_toggle fa fa-chevron-down" style="float: right;"></a>--%>
                </div>
            </header>
                 <div class="content-body">
                <div class="row">

                    
                    <table>
                        <tr>
                            <td id="tdData" runat="server" visible="true">

                         
                    <div class="col-md-12" id="" >
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls" >
                                    <div class="table-responsive" data-pattern="priority-columns">


                                       <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-small-font table-bordered table-striped" OnRowDataBound="gv_Records_RowDataBound" AllowPaging="True"
                                            AllowSorting="true" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="20"
                                            OnRowCommand="gv_Records_RowCommand" OnSorting="GridView1_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Existing Email" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Email" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Rep Name</u>" HeaderStyle-CssClass="clssubhead"
                                                    SortExpression="RepLastName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_replastname" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RepLastName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Court Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Hire Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_HireDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.HireDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Follow Up Date" SortExpression="FollowUpDate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FollowUpDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="AddFollowupDate" ImageUrl="../Images/add.gif" />
                                                        <asp:HiddenField ID="hf_Fname" runat="server" Value='<%#Eval("FirstName") %>' />
                                                        <asp:HiddenField ID="hf_Lname" runat="server" Value='<%#Eval("LastName") %>' />
                                                        <asp:HiddenField ID="hf_Email" runat="server" Value='<%#Eval("Email") %>' />
                                                        <asp:HiddenField ID="hf_Courtdate" runat="server" Value='<%#Eval("CourtDate") %>' />
                                                        <asp:HiddenField ID="hf_HireDate" runat="server" Value='<%#Eval("HireDate") %>' />
                                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%#Eval("TicketID_PK") %>' />
                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("CauseNumber") %>' />
                                                        <asp:HiddenField ID="hf_refCaseNumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                        <asp:HiddenField ID="hf_comments" runat="server" Value='<%#Eval("GeneralComments") %>' />
                                                        <asp:HiddenField ID="hf_Contact1" runat="server" Value='<%#Eval("Contact1") %>' />
                                                        <asp:HiddenField ID="hf_Contact2" runat="server" Value='<%#Eval("Contact2") %>' />
                                                        <asp:HiddenField ID="hf_Contact3" runat="server" Value='<%#Eval("Contact3") %>' />
                                                        <asp:HiddenField ID="hf_reviewedEmailStatus" runat="server" Value='<%#Eval("ReviewedEmailStatus") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                        </div>
                                        <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                    
                                    </div>
                                                                </div>
                         </div>
   </td>
                        </tr>
                          <%--<tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                        width: 780px">
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            <uc3:UpdateFollowUpInfoEmailAddress ID="UpdateFollowUpInfo2" runat="server" />
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                    </td>
                                </tr>
                               <%-- <tr>
                                    <td>
                                        <uc5:Footer ID="Footer" runat="server"></uc5:Footer>
                                    </td>
                                </tr>--%>
                        </table>


                    </div>
                     </div>

                </section>
                            
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" 
                                            AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server" 
                                                    CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>

                            </ContentTemplate>
                </aspnew:UpdatePanel>

             <div class="clearfix"></div>

               <section class="box" id="TableComment" style="display:none;">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:TextBox ID="txt_totalrecords" runat="server">
                                </asp:TextBox>
                                    
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>
            </section>
                   </section>
        </div>





        <script type="text/javascript">
            $(document).ready(function () {
                $("#mainmenu li, #limatter").attr("class", "");
                $("#ulmatter").css("display", "none");
                $("#livalidtion").attr("class", "open");
                $("#ulvalidtion").css("display", "block");
                $("#ActiveMenu1_Ac53").addClass("active");
                $("#ActiveMenu1_Ac53 > a li > span:nth-child(2)").addClass("arrow open");

            });
        </script>

        <%-- <div>
        <aspnew:ScriptManager ID="ScriptManager" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 70px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkFamilyShowAll" runat="server" Text="Show All" />
                                </span>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnFamilySubmit" runat="server" Text="Submit" CssClass="clsbutton"
                                    OnClick="btnFamilySubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Bad Email Address
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                    width="780">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <td align="center" colspan="2" valign="top">
                                        <aspnew:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                    CssClass="clsLabel"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" OnRowDataBound="gv_Records_RowDataBound" AllowPaging="True"
                                            AllowSorting="true" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="20"
                                            OnRowCommand="gv_Records_RowCommand" OnSorting="GridView1_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Existing Email" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Email" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Rep Name</u>" HeaderStyle-CssClass="clssubhead"
                                                    SortExpression="RepLastName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_replastname" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RepLastName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Court Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Hire Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_HireDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.HireDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Follow Up Date" SortExpression="FollowUpDate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FollowUpDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="AddFollowupDate" ImageUrl="../Images/add.gif" />
                                                        <asp:HiddenField ID="hf_Fname" runat="server" Value='<%#Eval("FirstName") %>' />
                                                        <asp:HiddenField ID="hf_Lname" runat="server" Value='<%#Eval("LastName") %>' />
                                                        <asp:HiddenField ID="hf_Email" runat="server" Value='<%#Eval("Email") %>' />
                                                        <asp:HiddenField ID="hf_Courtdate" runat="server" Value='<%#Eval("CourtDate") %>' />
                                                        <asp:HiddenField ID="hf_HireDate" runat="server" Value='<%#Eval("HireDate") %>' />
                                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%#Eval("TicketID_PK") %>' />
                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("CauseNumber") %>' />
                                                        <asp:HiddenField ID="hf_refCaseNumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                        <asp:HiddenField ID="hf_comments" runat="server" Value='<%#Eval("GeneralComments") %>' />
                                                        <asp:HiddenField ID="hf_Contact1" runat="server" Value='<%#Eval("Contact1") %>' />
                                                        <asp:HiddenField ID="hf_Contact2" runat="server" Value='<%#Eval("Contact2") %>' />
                                                        <asp:HiddenField ID="hf_Contact3" runat="server" Value='<%#Eval("Contact3") %>' />
                                                        <asp:HiddenField ID="hf_reviewedEmailStatus" runat="server" Value='<%#Eval("ReviewedEmailStatus") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                        width: 780px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            <uc3:UpdateFollowUpInfoEmailAddress ID="UpdateFollowUpInfo2" runat="server" />
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc5:Footer ID="Footer" runat="server"></uc5:Footer>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>--%>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

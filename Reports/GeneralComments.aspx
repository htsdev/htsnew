<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralComments.aspx.cs" Inherits="lntechNew.Reports.GeneralComments" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>General Comments Popup</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <script language="jscript" type="text/jscript">
      function checkGComments()
		{
          var dateLenght = 0;
		  var newLenght = 0;
		  
          newLenght = document.getElementById("WCC_GeneralComments_txt_comments").value.length
		  if(newLenght > 0){dateLenght = 27}else{dateLenght = 0}

		  if (document.getElementById("WCC_GeneralComments_txt_comments").value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerText.length > (2000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
		  {
		    alert("Sorry You cannot type in more than 2000 characters in General comments box")
			return false;		  
		  }
		}
      </script>
    
</head>
<body>
   <form id="form1" runat="server">
        <table border="2" style="border-left-color: navy; border-bottom-color: navy; border-top-color: navy;
            border-collapse: collapse; border-right-color: navy">
            <tr>
                <td style="height: 190px">
                    <table border="1" cellpadding="0" cellspacing="0" class="clsleftpaddingtable" style="border-top-style: none;
                        border-collapse: collapse; border-bottom-style: none" width="350">
                        <tr id="Tr1" runat="server">
                        </tr>
                    </table>
                    <table style="width: 100%; height: 100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" valign="middle" style="height: 36px">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td align="left" class="clssubhead" style="width: 50%">
                                            General &nbsp;Comments&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" class="clsleftpaddingtable" id="TD1" runat="server">
                                <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Height="105px" Width="545px" />
                                </td>
                        </tr>
                        <tr>
                            <td align="center" class="clsleftpaddingtable">
                                <asp:Button ID="btnsubmit" runat="server" CssClass="clsbutton" OnClick="btnsubmit_Click" OnClientClick="return checkGComments();" Text="Submit" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

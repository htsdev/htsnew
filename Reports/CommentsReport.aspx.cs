﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components.Services;

namespace HTP.Reports
{
    public enum CommentsType
    {
        General = 1,
        ReminderCall = 3,
        Complaint = 4
    }

    public partial class CommentsReport : System.Web.UI.Page
    {
        #region Variables

        bool IsSearch;
        DataView dv;



        #endregion

        #region Properites

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillddlFirms();
                FillddlCaseType();
                cal_FromDateFilternew.SelectedDate = DateTime.Today;
                cal_ToDateFilternew.SelectedDate = DateTime.Today;
                tr_General.Style["display"] = "none";
                tr_ReminderCall.Style["display"] = "none";
                tr_Complaint.Style["display"] = "none";


            }
            PagingctrlGenComments.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingctrlGenComments_PageIndexChanged);
            PagingctrlGenComments.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(PagingctrlGenComments_PageSizeChanged);
            PagingctrlGenComments.GridView = gv_GenComments;

            PagingcontrolRemCall.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingcontrolRemCall_PageIndexChanged);
            PagingcontrolRemCall.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(PagingcontrolRemCall_PageSizeChanged);
            PagingcontrolRemCall.GridView = gv_RemCallComments;

            PagingcontrolComplaints.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingcontrolComplaints_PageIndexChanged);
            PagingcontrolComplaints.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(PagingcontrolComplaints_PageSizeChanged);
            PagingcontrolComplaints.GridView = gv_Complaint;

        }

        protected void gv_GenComments_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            GridViewSortExpression = sortExpression;
            BindGrid(CommentsType.General, gv_GenComments, PagingctrlGenComments, lblMessage_general);
            //SortGrid(gv_Complaint, (DataView)Session["GeneralData"]);

            PagingctrlGenComments.PageCount = gv_GenComments.PageCount;
            PagingctrlGenComments.PageIndex = gv_GenComments.PageIndex;
        }

        protected void gv_RemCallComments_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            GridViewSortExpression = sortExpression;
            //SortGrid(gv_Complaint, (DataView)Session["ReminderCallData"]);
            BindGrid(CommentsType.ReminderCall, gv_RemCallComments, PagingcontrolRemCall, lblMessage_ReminderCall);
            PagingcontrolRemCall.PageCount = gv_RemCallComments.PageCount;
            PagingcontrolRemCall.PageIndex = gv_RemCallComments.PageIndex;
        }

        protected void gv_Complaint_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            GridViewSortExpression = sortExpression;
            BindGrid(CommentsType.Complaint, gv_Complaint, PagingcontrolComplaints, lblMessage_Complaint);
            //SortGrid(gv_Complaint, (DataView)Session["ComplaintData"]);

            PagingcontrolComplaints.PageCount = gv_Complaint.PageCount;
            PagingcontrolComplaints.PageIndex = gv_Complaint.PageIndex;
        }

        protected void gv_GenComments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_GenComments.PageIndex = e.NewPageIndex;
                BindGrid(CommentsType.General, gv_GenComments, PagingctrlGenComments, lblMessage_general);
            }
            catch (Exception ex)
            {

                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_RemCallComments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_RemCallComments.PageIndex = e.NewPageIndex;
                BindGrid(CommentsType.ReminderCall, gv_RemCallComments, PagingcontrolRemCall, lblMessage_ReminderCall);
            }
            catch (Exception ex)
            {

                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Complaint_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Complaint.PageIndex = e.NewPageIndex;
                BindGrid(CommentsType.Complaint, gv_Complaint, PagingcontrolComplaints, lblMessage_Complaint);

            }
            catch (Exception ex)
            {

                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            IsSearch = true;

            GridViewSortExpression = "FullName";

            if (ChkGenCom.Checked || ChkAll.Checked)
            {
                BindGrid(CommentsType.General, gv_GenComments, PagingctrlGenComments, lblMessage_general);
                tr_General.Style["display"] = "block";
            }
            else
            {

                tr_General.Style["display"] = "none";

            }
            if (ChkRemCallCom.Checked || ChkAll.Checked)
            {
                BindGrid(CommentsType.ReminderCall, gv_RemCallComments, PagingcontrolRemCall, lblMessage_ReminderCall);
                tr_ReminderCall.Style["display"] = "block";

            }
            else
            {
                tr_ReminderCall.Style["display"] = "none";
            }
            if (ChkComp.Checked || ChkAll.Checked)
            {
                BindGrid(CommentsType.Complaint, gv_Complaint, PagingcontrolComplaints, lblMessage_Complaint);
                tr_Complaint.Style["display"] = "block";
            }
            else
            {

                tr_Complaint.Style["display"] = "none";
            }

            IsSearch = false;

        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ControlName = e.Row.FindControl("hf_Comments").NamingContainer.ClientID;
                ((Label)e.Row.FindControl("lbl_Comments")).Attributes.Add("OnMouseOver", "CommentPoupup('" + ControlName + "_hf_Comments" + "');");
                ((Label)e.Row.FindControl("lbl_Comments")).Attributes.Add("OnMouseOut", "CursorIcon2()");
            }
        }
        #endregion

        #region Method

        /// <summary>
        /// Fill firm drop down of attorneys
        /// </summary>
        private void FillddlFirms()
        {
            try
            {
                clsFirms ClsFirms = new clsFirms();
                DataSet ds_Firms = ClsFirms.GetAllFullFirmName();
                ddlAttorneys.DataSource = ds_Firms.Tables[0];
                ddlAttorneys.DataTextField = "FirmFullName";
                ddlAttorneys.DataValueField = "FirmID";
                ddlAttorneys.DataBind();
                ListItem lstAttor = new ListItem("All", "-1");
                ddlAttorneys.Items.Insert(0, lstAttor);

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        /// <summary>
        /// Fil case type drop down
        /// </summary>
        private void FillddlCaseType()
        {
            CaseType ctype = new CaseType();
            ddlCaseType.DataSource = ctype.GetAllCaseType();
            ddlCaseType.DataBind();
            ListItem lstCaseType = new ListItem("All", "-1");
            ddlCaseType.Items.Insert(0, lstCaseType);
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void PagingctrlGenComments_PageIndexChanged()
        {
            try
            {
                gv_GenComments.PageIndex = PagingctrlGenComments.PageIndex - 1;
                BindGrid(CommentsType.General, gv_GenComments, PagingctrlGenComments, lblMessage_general);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void PagingctrlGenComments_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_GenComments.PageIndex = 0;
                    gv_GenComments.PageSize = pageSize;
                    gv_GenComments.AllowPaging = true;
                }
                else
                {
                    gv_GenComments.AllowPaging = false;
                }
                BindGrid(CommentsType.General, gv_GenComments, PagingctrlGenComments, lblMessage_general);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void PagingcontrolRemCall_PageIndexChanged()
        {
            try
            {
                gv_RemCallComments.PageIndex = PagingcontrolRemCall.PageIndex - 1;
                BindGrid(CommentsType.ReminderCall, gv_RemCallComments, PagingcontrolRemCall, lblMessage_ReminderCall);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void PagingcontrolRemCall_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_RemCallComments.PageIndex = 0;
                    gv_RemCallComments.PageSize = pageSize;
                    gv_RemCallComments.AllowPaging = true;
                }
                else
                {
                    gv_RemCallComments.AllowPaging = false;
                }
                BindGrid(CommentsType.ReminderCall, gv_RemCallComments, PagingcontrolRemCall, lblMessage_ReminderCall);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void PagingcontrolComplaints_PageIndexChanged()
        {
            try
            {
                gv_Complaint.PageIndex = PagingcontrolComplaints.PageIndex - 1;
                BindGrid(CommentsType.Complaint, gv_Complaint, PagingcontrolComplaints, lblMessage_Complaint);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void PagingcontrolComplaints_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Complaint.PageIndex = 0;
                    gv_Complaint.PageSize = pageSize;
                    gv_Complaint.AllowPaging = true;
                }
                else
                {
                    gv_Complaint.AllowPaging = false;
                }
                BindGrid(CommentsType.Complaint, gv_Complaint, PagingcontrolComplaints, lblMessage_Complaint);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Generate serial number in given datatable ticket wise
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Remove("sno");

                dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// get data and bind grid for comment report
        /// </summary>
        public void BindGrid(CommentsType CommentType, GridView Grid, lntechNew.WebControls.PagingControl PagingCrtl, Label lbl_Message)
        {
            DataTable dt = new DataTable();
            if (IsSearch)
            {
                Grid.PageIndex = 0;
                CommentService ComtSer = new CommentService();
                dt = ComtSer.GetCommentReport(cal_FromDateFilternew.SelectedDate, cal_ToDateFilternew.SelectedDate, Convert.ToInt32(ddlCaseType.SelectedValue), Convert.ToInt32(ddlAttorneys.SelectedValue), Convert.ToInt32(CommentType), chkShowAll.Checked);
                GenerateSerialNo(dt);
                dv = dt.DefaultView;
                Grid.PageSize = 20;
                Grid.PageIndex = 0;
                SetSession(CommentType, dv);
            }

            dv = GetSession(CommentType);
            dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
            dt = dv.ToTable();


            if (dt.Rows.Count != 0)
            {

                GenerateSerialNo(dt);
                dv = dt.DefaultView;
                SetSession(CommentType, dv);
                Grid.DataSource = dv;
                Grid.DataBind();
                PagingCrtl.PageCount = Grid.PageCount;
                PagingCrtl.PageIndex = Grid.PageIndex;
                PagingCrtl.SetPageIndex();
                Grid.Visible = true;
                lbl_Message.Text = "";

            }
            else
            {
                PagingCrtl.PageCount = 0;
                PagingCrtl.PageIndex = 0;
                PagingCrtl.SetPageIndex();
                lbl_Message.Text = "No Records Found";
                Grid.Visible = false;
            }


        }

        /// <summary>
        /// Maintain session of dataview given comment type
        /// </summary>
        /// <param name="CommentType"></param>
        /// <param name="DV"></param>
        private void SetSession(CommentsType CommentType, DataView DV)
        {
            switch (CommentType)
            {
                case CommentsType.General:
                    Session["GeneralData"] = DV;
                    break;
                case CommentsType.ReminderCall:
                    Session["ReminderCallData"] = DV;
                    break;
                case CommentsType.Complaint:
                    Session["ComplaintData"] = DV;
                    break;
            }
        }

        /// <summary>
        /// Maintain dataview of given comment type from session
        /// </summary>
        /// <param name="CommentType"></param>
        /// <param name="DV"></param>
        private DataView GetSession(CommentsType CommentType)
        {
            switch (CommentType)
            {
                case CommentsType.General:
                    return (DataView)Session["GeneralData"];
                case CommentsType.ReminderCall:
                    return (DataView)Session["ReminderCallData"];
                case CommentsType.Complaint:
                    return (DataView)Session["ComplaintData"];
                default:
                    return (DataView)Session["ComplaintData"];
            }
        }



        #endregion






    }
}

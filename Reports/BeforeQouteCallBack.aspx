<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BeforeQouteCallBack.aspx.cs"
    Inherits="HTP.Reports.BeforeQouteCallBack" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="BusinessDaysCon" TagPrefix="uc5" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quote Follow Up Calls(Before Court Date)</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr class="clsLeftPaddingTable">
                        <td>
                            <uc5:BusinessDaysCon ID="BusinessDaysCon1" runat="server" Attribute_Key="Days" lbl_TextBefore="Court Date before"
                                lbl_TextAfter="(Number of Business Days)" NegativeValueAllowed="false" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 34px" background="../Images/subhead_bg.gif">
                            <table>
                                <tr>
                                    <td align="left" class="clssubhead" width="50%">
                                        Quote Follow Up Calls(Before Court Date)
                                    </td>
                                    <td align="right" width="50%">
                                        <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                        CssClass="clsLabel"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="100%" AllowPaging="True" OnPageIndexChanging="gv_Data_PageIndexChanging"
                                PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText="S.No" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                Text='<%# Eval("sno") %>'></asp:HyperLink>
                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("TicketID_PK") %>' />                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Client Name" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Client" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("Customer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("LastContactDate")+ "("+ Eval("Days") +")" %>'>															
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Court Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDateMain" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CourtDateMain") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Court Location" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_shortcourtname" runat="server" CssClass="clsLeftPaddingTable"
                                                Text='<%# Eval("shortcourtname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qoute" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Qoute" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("calculatedtotalfee", "{0:C0}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Follow Up Status" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_QuoteComments" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("QuoteResultDescription") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Call Back Date" HeaderStyle-CssClass="clssubhead"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CallBackDate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CallBackDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_STATUS" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("STATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Case Type" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CaseType" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CaseType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <%--<Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="Pagingctrl$DL_Records" EventName="SelectedIndexChanged" />
            </Triggers>--%>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.Reports
{
    public partial class fmBondSummary : System.Web.UI.Page
    {
        #region Variables

        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
        clsLogger clog = new clsLogger();
        clsSession uSession = new clsSession();
        clsENationWebComponents clsDB = new clsENationWebComponents();        
        //ozair 4073 05/21/2008
        clsDocumentTracking clsDT = new clsDocumentTracking();
        int documentBatchID = 0;
        //end ozair 4073

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                    ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
                    
                    Create_BondSummary_Report();
                }
            }
            catch (Exception ex)
            {
                Response.Write("Report Cannot be Loaded Session Timeoout Expired.");
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methonds

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sTVIDS"></param>
        /// <returns>int</returns>
        private int GetDocumentID()
        {
            //ozair 3643 on 05/01/2008 implemented the logic for document ID.
            clsDT.BatchDate = DateTime.Now;
            clsDT.DocumentID = 3; // 3: Bond Settings            
            clsDT.EmpID = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
            //get the document id for this report
            return clsDT.GetDBIDByInsertingBatch();
        }

        /// <summary>
        /// ozair 4073 05/21/2008
        /// </summary>
        private void Create_BondSummary_Report()
        {
            try
            {
                string sTVIDS = "";

                if (Session["sTVIDS"] != null)
                {
                    sTVIDS = Session["sTVIDS"].ToString();
                    Session["sTVIDS"] = null;
                }

                string filename = Server.MapPath("") + "\\BondSummary.rpt";

                if (Request.QueryString["Flag"] == "1")//for generating document ID..
                {                    
                    documentBatchID = GetDocumentID();

                    string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "BOND-" + documentBatchID.ToString() + ".pdf";
                    string Path = ViewState["FilePath"].ToString() + "BOND" + "-" + documentBatchID.ToString() + ".pdf";

                    string[] key2 = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                    object[] value2 = { sTVIDS, documentBatchID, 1, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };
                    
                    ClsCr.CreateReport(filename, "usp_HTS_Bond_Report", key2, value2, this.Session, this.Response, Path);
                    // Afaq 8293 09/20/2010 Maintain print history by inserting the printed record in datbase.
                    string[] key = { "@PrintDate", "PrintType", "@EmpID", "@BatchID" };
                    object[] value1 = { DateTime.Now, "BOND", Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)), documentBatchID.ToString() };
                    clsDB.ExecuteSP("usp_hts_InsertInPrintSummary", key, value1);
                    //string Path = ViewState["FilePath"].ToString() + "APP" + "-" + PrintID.ToString() + ".pdf";
                    //Response.Redirect("C:\\Docstorage" + fPath, false);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.WriteFile("C:\\Docstorage" + fPath);
                    Response.Flush();
                    Response.Close();
                }
                else if (Request.QueryString["Flag"] == "2")//only for preview..
                {
                    string[] key2 = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                    object[] value2 = { sTVIDS, 0, 0, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    ClsCr.CreateReport(filename, "usp_HTS_Bond_Report", key2, value2, this.Session, this.Response);
                }
                else if (Request.QueryString["Flag"] == "3")//print all with docid
                {
                    documentBatchID = GetDocumentID();

                    string[] key2 = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                    object[] value2 = { "", documentBatchID, 1, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "BOND-" + documentBatchID.ToString() + ".pdf";
                    string Path = ViewState["FilePath"].ToString() + "BOND" + "-" + documentBatchID.ToString() + ".pdf";
                    
                    ClsCr.CreateReport(filename, "usp_HTS_Bond_Report", key2, value2, this.Session, this.Response, Path);
                    // Afaq 8293 09/20/2010 Maintain print history by inserting the printed record in datbase.
                    string[] key = { "@PrintDate", "PrintType", "@EmpID", "@BatchID" };
                    object[] value1 = { DateTime.Now, "BOND", Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)), documentBatchID.ToString() };
                    clsDB.ExecuteSP("usp_hts_InsertInPrintSummary", key, value1);
                    //Response.Redirect("C:\\Docstorage" + fPath, false);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.WriteFile("C:\\Docstorage" + fPath);
                    Response.Flush();
                    Response.Close();
                }
                else if (Request.QueryString["Flag"] == "4")//print all with out doc id
                {
                    string[] key2 = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                    object[] value2 = { "", 0, 0, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    ClsCr.CreateReport(filename, "usp_HTS_Bond_Report", key2, value2, this.Session, this.Response);
                }
                else if (Request.QueryString["Flag"] == "5")//for generating document ID..and printing bond waiting report
                {
                    documentBatchID = GetDocumentID();

                    string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "BOND_WAITING-" + documentBatchID.ToString() + ".pdf";
                    string Path = ViewState["FilePath"].ToString() + "BOND_WAITING" + "-" + documentBatchID.ToString() + ".pdf";

                    string[] key2 = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                    object[] value2 = { sTVIDS, documentBatchID, 0, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };
                    

                    ClsCr.CreateReport(filename, "usp_HTS_Bond_Report_Main", key2, value2, this.Session, this.Response, Path);

                    //Response.Redirect("C:\\Docstorage" + fPath, false);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.WriteFile("C:\\Docstorage" + fPath);
                    Response.Flush();
                    Response.Close();
                }
                else if (Request.QueryString["Flag"] == "6")//only for preview..(bond waiting report)
                {
                    string[] key2 = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                    object[] value2 = { sTVIDS, 0, 0, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    ClsCr.CreateReport(filename, "usp_HTS_Bond_Report_Main", key2, value2, this.Session, this.Response);
                }


            }
            catch (Exception ex)
            {
                Response.Write("Report Cannot be Loaded Session Timeoout Expired.");
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BondingReport.aspx.cs" Inherits="lntechNew.Reports.BondingReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Bonding Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
   
   
    
    
</head>
<body>
    <form id="form1" runat="server">
   
    <table  id = "MainTable" cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 780px">
    <tr>
    <td style="height: 16px">
        &nbsp; 
        <uc2:activemenu id="ActiveMenu1" runat="server"></uc2:activemenu>
    </td>
    </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
    
        <tr>
            <td align="center">
                <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gv_Result" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                    Width="780px" AllowPaging="True" PageSize="50" OnRowDataBound="gv_Result_RowDataBound" OnPageIndexChanging="gv_Result_PageIndexChanging" OnSorting="gv_Result_Sorting">
                    <Columns>
                            <asp:TemplateField HeaderText="NO" SortExpression="S NO">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid" Visible=false runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")  %>'></asp:Label>
                                    <asp:HyperLink ID="hf_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text=""></asp:HyperLink>
                                    <asp:HyperLink ID="hl_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.S No") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CAUSE NO" SortExpression="causenumber"  >
                                <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hf_CauseNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>' Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TICKET NO" SortExpression="ticketnumber" >
                                <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>' Visible="False"></asp:Label>
                                    <asp:HyperLink ID="hf_TicketNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="CLIENT" SortExpression="CLIENTNAME">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" CssClass="clsLabel"  Text='<%# DataBinder.Eval(Container, "DataItem.clientname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="AUTO" SortExpression="AUTOSTATUS" >
                                <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.autostatus") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                        <asp:TemplateField HeaderText="VERIFIED" SortExpression="VERIFIEDSTATUS">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.verifiedstatus") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BAR #" SortExpression="BARcardNUMBER">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.barcardnumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BONDING #" SortExpression="BONDINGNUMBER">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.bondingnumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                    <PagerSettings Mode="NumericFirstLast"  />
                    <PagerStyle HorizontalAlign="Center" />
                </asp:GridView>
               
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server" />
            </td>
        </tr>
    </table>
    
   
    </form>
</body>
</html>

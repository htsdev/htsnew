using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace lntechNew.Reports
{
    public partial class frmAppearancePreiew : System.Web.UI.Page
    {
        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
        clsCrsytalComponent ClsCrReport = new clsCrsytalComponent();
        clsLogger clog = new clsLogger();
        clsSession uSession = new clsSession();
        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsGeneralMethods ZipPdf = new clsGeneralMethods();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                    ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();

                    string[] key ={ "@PrintDate", "PrintType", "@EmpID", "@BatchID" };
                    object[] value1 ={ DateTime.Now, "APP", Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)), 0 };
                    int PrintID = (int)clsDB.InsertBySPArrRet("usp_hts_InsertInPrintSummary", key, value1);
                    string Path = ViewState["FilePath"].ToString() + "APP" + "-" + PrintID.ToString() + ".pdf";
                    Create_ArriagnmentSummary_Report(Path, PrintID);
                }
            }
            catch (Exception Ex)
            {

            }
        }

        public void Create_ArriagnmentSummary_Report(string Path, int PrintID)
        {
            try
            {



                string TempPath = ViewState["FilePath"].ToString() + "APP" + "-Temp1" + ".pdf";
                string TempPath2 = ViewState["FilePath"].ToString() + "APP" + "-Temp2" + ".pdf";


                string filename = Server.MapPath("") + "\\Appearance.rpt";
                ClsCr.CreateReport(filename, "USP_HTS_APPEARANCE_REPORT", this.Session, this.Response, TempPath);




                string Filename = Server.MapPath("") + "\\AppearancePlea.rpt";
                ClsCrReport.CreateReport(Filename, "USP_HTS_APPEARANCE_PleaNotGuilyReport", this.Session, this.Response, TempPath2);


                string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "APP-" + PrintID.ToString() + ".pdf";


                string FilePath1 = ViewState["FilePath"].ToString() + "APP-Temp1" + ".pdf";
                string FilePath2 = ViewState["FilePath"].ToString() + "APP-Temp2" + ".pdf";

                if (File.Exists(FilePath1))
                {
                    if (File.Exists(FilePath2))
                    {
                        FilePath1 = FilePath1.Replace("\\\\", "\\");
                        FilePath2 = FilePath2.Replace("\\\\", "\\");
                        Path = Path.Replace("\\\\", "\\");
                        WebSupergoo.ABCpdf6.Doc doc1 = new Doc();
                        WebSupergoo.ABCpdf6.Doc doc2 = new Doc();
                        doc1.Read(FilePath1);
                        doc2.Read(FilePath2);
                        doc1.Append(doc2);
                        doc1.Save(Path);
                        doc2.Dispose();
                        doc1.Dispose();
                        File.Delete(FilePath1);
                        File.Delete(FilePath2);

                        /*if (ZipPdf.Zip(Path, "APP-" + PrintID.ToString() + ".zip", ViewState["FilePath"].ToString()) == true)
                        {
                            System.IO.File.Delete(Path);

                            if (ZipPdf.Extract(ViewState["FilePath"].ToString(), "APP-" + PrintID.ToString() + ".zip") == true)
                            {
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/pdf";
                                Response.WriteFile(Path);
                                Response.Flush();
                                Response.Close();

                                System.IO.File.Delete(Path);
                            }
                        }*/
                        Response.Redirect("../Docstorage" + fPath, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }		
    }
}

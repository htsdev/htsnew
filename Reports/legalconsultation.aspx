﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="legalconsultation.aspx.cs"
    Inherits="HTP.Reports.legalconsultation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%--Sabir Khan 5214 11/25/2008 Title has been changed into 'Online inquiries...--%>
    <title>Leads</title>
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet" />--%>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
</head>


<body class=" ">

    <form id="form1" runat="server">

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
            </asp:Panel>


            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>
                    
<div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" style="margin-top: 9px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                <div class='col-xs-12'>
                    <div class="page-title">

                        <div class="pull-left">
                            <!-- PAGE HEADING TAG - START -->
                            <h1 class="title">Leads</h1>
                            <!-- PAGE HEADING TAG - END --> 
                        </div>     
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- MAIN CONTENT AREA STARTS -->

                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <%--<h2 class="title pull-left">Basic Table</h2>--%>
                                <div class="actions panel_actions pull-right">
                                    <%--<a class="box_toggle fa fa-chevron-down"></a>
                                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                    <a class="box_close fa fa-times"></a>--%>
                                
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">

                                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20" CssClass="table" AllowSorting="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hl_Sno" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Name" HeaderText="Name" HtmlEncode="false">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Phone" HeaderText="Phone" HtmlEncode="false">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Comments" HeaderText="Question" HtmlEncode="false">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RepComments" HeaderText="Comments" HtmlEncode="false">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="recdate" HeaderText="Email Received Date">
                                                </asp:BoundField>
                                                 <asp:BoundField DataField="followupdate" HeaderText="Follow-Up date">
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->
            <div class="page-chatapi hideit">

            <div class="search-bar">
                <input type="text" placeholder="Search" class="form-control">
            </div>

            <div class="chat-wrapper">
                <h4 class="group-head">Groups</h4>
                <ul class="group-list list-unstyled">
                    <li class="group-row">
                        <div class="group-status available">
                            <i class="fa fa-circle"></i>
                        </div>
                        <div class="group-info">
                            <h4><a href="#">Work</a></h4>
                        </div>
                    </li>
                    <li class="group-row">
                        <div class="group-status away">
                            <i class="fa fa-circle"></i>
                        </div>
                        <div class="group-info">
                            <h4><a href="#">Friends</a></h4>
                        </div>
                    </li>

                </ul>


                <h4 class="group-head">Favourites</h4>
                <ul class="contact-list">

                        <li class="user-row " id='chat_user_1' data-user-id='1'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clarine Vassar</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_2' data-user-id='2'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Brooks Latshaw</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_3' data-user-id='3'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clementina Brodeur</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                </ul>


                <h4 class="group-head">More Contacts</h4>
                <ul class="contact-list">

                        <li class="user-row " id='chat_user_4' data-user-id='4'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Carri Busey</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_5' data-user-id='5'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Melissa Dock</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_6' data-user-id='6'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Verdell Rea</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_7' data-user-id='7'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Linette Lheureux</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_8' data-user-id='8'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Araceli Boatright</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_9' data-user-id='9'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clay Peskin</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_10' data-user-id='10'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Loni Tindall</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_11' data-user-id='11'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Tanisha Kimbro</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_12' data-user-id='12'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Jovita Tisdale</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                </ul>
            </div>

        </div>


        <div class="chatapi-windows ">




        </div>    </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


    <!-- General section box modal start -->
    <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog animated bounceInDown">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Section Settings</h4>
                </div>
                <div class="modal-body">

                    Body goes here...

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success" type="button">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal end -->
</body>































<%--<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="7" height="11">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                </td>
                                <td style="text-align: right;">
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="7" height="11">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                            PageSize="20" CssClass="clsLeftPaddingTable" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField HeaderText="S#">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Name" HtmlEncode="false">
                                    <ItemStyle CssClass="GridItemStyle" Width="110px" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="110px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Phone" HeaderText="Phone" HtmlEncode="false">
                                    <ItemStyle CssClass="GridItemStyle" Width="80px" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Comments" HeaderText="Question" HtmlEncode="false">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RepComments" HeaderText="Comments" HtmlEncode="false">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="recdate" HeaderText="Email Received Date">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="followupdate" HeaderText="Follow-Up date">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" />
                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>--%>
</html>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.frmRptIClientDallas" Codebehind="frmRptIClientDallas.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Internet Clients - Dallas</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" style="Z-INDEX: 102; LEFT: 80px; POSITION: absolute; TOP: 16px" cellSpacing="0"
				cellPadding="0" width="780" align="center" border="0">
				<tbody>
				<TR>
					<TD>
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
					<TD background="../images/separator_repeat.gif" height="12"></TD>
				</TR>
				<TR>
					<TD >
						<TABLE class="clsleftpaddingtable" id="Table1" cellSpacing="1" cellPadding="1" width="100%"
							borderColorLight="#ffffff" border="0">
							<TR>
                                <td style="width: 37px">
                                    From:</td>
								<TD style="WIDTH: 153px">
									<ew:calendarpopup id="datefrom" runat="server" EnableHideDropDown="True" SelectedDate="2006-02-21"
										Width="115px" ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif" CalendarLocation="Left"
										ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" Nullable="True" ShowClearDate="True"
										UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Mail Date From" DisplayOffsetY="20"
										Font-Names="Tahoma" Font-Size="8pt">
										<TEXTBOXLABELSTYLE CssClass="clstextarea"></TEXTBOXLABELSTYLE>
										<WEEKDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></WEEKDAYSTYLE>
										<MONTHHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow"
											ForeColor="Black"></MONTHHEADERSTYLE>
										<OFFMONTHSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="AntiqueWhite"
											ForeColor="Gray"></OFFMONTHSTYLE>
										<GOTOTODAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></GOTOTODAYSTYLE>
										<TODAYDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGoldenrodYellow"
											ForeColor="Black"></TODAYDAYSTYLE>
										<DAYHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Orange"
											ForeColor="Black"></DAYHEADERSTYLE>
										<WEEKENDSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGray"
											ForeColor="Black"></WEEKENDSTYLE>
										<SELECTEDDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow"
											ForeColor="Black"></SELECTEDDATESTYLE>
										<CLEARDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></CLEARDATESTYLE>
										<HOLIDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></HOLIDAYSTYLE>
									</ew:calendarpopup>
									</TD>
                                <td style="width: 23px">
                                    To:</td>
								<TD style="WIDTH: 147px">
									<ew:calendarpopup id="dateto" runat="server" EnableHideDropDown="True" SelectedDate="2006-02-21" Width="118px"
										ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif" CalendarLocation="Left" ShowGoToToday="True"
										AllowArbitraryText="False" Culture="(Default)" Nullable="True" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
										PadSingleDigits="True" ToolTip="Mail Date From" DisplayOffsetY="20" Font-Names="Tahoma" Font-Size="8pt">
										<TEXTBOXLABELSTYLE CssClass="clstextarea"></TEXTBOXLABELSTYLE>
										<WEEKDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></WEEKDAYSTYLE>
										<MONTHHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow"
											ForeColor="Black"></MONTHHEADERSTYLE>
										<OFFMONTHSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="AntiqueWhite"
											ForeColor="Gray"></OFFMONTHSTYLE>
										<GOTOTODAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></GOTOTODAYSTYLE>
										<TODAYDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGoldenrodYellow"
											ForeColor="Black"></TODAYDAYSTYLE>
										<DAYHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Orange"
											ForeColor="Black"></DAYHEADERSTYLE>
										<WEEKENDSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGray"
											ForeColor="Black"></WEEKENDSTYLE>
										<SELECTEDDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow"
											ForeColor="Black"></SELECTEDDATESTYLE>
										<CLEARDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></CLEARDATESTYLE>
										<HOLIDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White"
											ForeColor="Black"></HOLIDAYSTYLE>
									</ew:calendarpopup>
									</TD>
								<TD style="WIDTH: 37px">
									</TD>
								<TD style="WIDTH: 4px">
									<asp:button id="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" Width="96px"></asp:button></TD>
								<TD vAlign="middle" align="right">
									<TABLE id="tblPageNavigation" cellSpacing="0" cellPadding="0" border="0" runat="server">
										
											<TR>
												<TD class="clssubhead" align="right">Page # &nbsp;
												</TD>
												<TD align="left">
													<asp:label id="lblCurrPage" runat="server" Width="25px" CssClass="clssubhead"></asp:label></TD>
												<TD class="clssubhead" align="right">Goto</TD>
												<TD align="right">
													<asp:dropdownlist id="ddlPageNo" runat="server" CssClass="clssubhead" AutoPostBack="True"></asp:dropdownlist></TD>
											</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			
			<TR>
				<TD background="../images/separator_repeat.gif" height="11"></TD>
			</TR>
			<TR>
				<TD>
					<asp:datagrid id="dg_results" runat="server" Width="789px" CssClass="clsleftpaddingtable" AllowPaging="True"
						BorderStyle="None" BorderColor="White" AutoGenerateColumns="False">
						<Columns>
							<asp:TemplateColumn HeaderText="S.No">
								<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="lbl_sno" runat="server" CssClass="label"></asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Cust ID">
								<HeaderStyle HorizontalAlign="Left" Width="7%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_cid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id_customer") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Name">
								<HeaderStyle HorizontalAlign="Left" Width="15%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_name runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' CssClass="label" Visible="False">
									</asp:Label>
									<asp:HyperLink id=hhl_name runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Address">
								<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_add runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.homeaddress") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="City , State">
								<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_city runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.city") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Zip">
								<HeaderStyle HorizontalAlign="Left" Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
									<asp:Label id=lbl_zip runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zip") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Credit Card">
								<HeaderStyle HorizontalAlign="Left" Width="12%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_credit runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.crnumber") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Amount">
								<HeaderStyle HorizontalAlign="Left" Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_amount runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.amount","{0:C0}") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Status">
								<HeaderStyle HorizontalAlign="Left" Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_status runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.approvedflag") %>' CssClass="Label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Phone">
								<HeaderStyle HorizontalAlign="Left" Width="12%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_phone runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.telephone") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Tran Date">
								<HeaderStyle HorizontalAlign="Left" Width="12%" CssClass="clsaspcolumnheader"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=lbl_recdate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recdate") %>' CssClass="label">
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle VerticalAlign="Middle" NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Prevous"
							HorizontalAlign="Center"></PagerStyle>
					</asp:datagrid></TD>
			</TR>
			<TR>
				<TD align="center">
					<asp:label id="lblMessage" runat="server" Width="481px" ForeColor="Red" Height="13px"></asp:label></TD>
			</TR>
			<TR>
				<TD background="../images/separator_repeat.gif" height="11"></TD>
			</TR>
			<TR>
				<TD height="11">
					<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
			</TR>
			</TBODY>
			</TABLE>
		</form>
		</body>
	
</HTML>

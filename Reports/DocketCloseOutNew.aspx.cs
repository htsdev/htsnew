using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;



namespace lntechNew.Reports
{
    public partial class DocketCloseOutNew : System.Web.UI.Page
    {
        private DataTable dtRecords;
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        protected GroupingHelper m_oGroupingHelper = null;
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords();
                m_oGroupingHelper = new GroupingHelper();
                dtRecords.Columns.Add("blankspace");
                DataTable dt = m_oGroupingHelper.SetupGroupedTable(dtRecords, "CourtNameComplete");
                rptrtable.DataSource = dt;
                rptrtable.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                //bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetRecords()
        {
            string[] keys = { "@CourtDate" };
            object[] values = { cal_Date.SelectedDate };
            dtRecords = clsDb.Get_DT_BySPArr("USP_HTS_DOCKETCLOSEOUT_GETRECORDS", keys, values);

        }

        protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlTableRow rowHeader = e.Item.FindControl("rowGroupHeader") as HtmlTableRow;
                HtmlTableRow rowItem = e.Item.FindControl("rowItem") as HtmlTableRow;
                HtmlTableRow RowRoomFooter = e.Item.FindControl("RowRoomFooter") as HtmlTableRow;

                //Check if it is groupheader
                DataRowView oRow = e.Item.DataItem as DataRowView;
                if (oRow["poorman_type"].ToString() == "groupheader")
                {
                    System.Web.UI.WebControls.Image oImage = e.Item.FindControl("idImage") as System.Web.UI.WebControls.Image;
                    m_oGroupingHelper.AddGroup(rowHeader.ClientID, oImage.ClientID);
                    HtmlGenericControl oCtrl = e.Item.FindControl("idClickable") as HtmlGenericControl;
                    oCtrl.Attributes["onclick"] = "javascript:" + m_oGroupingHelper.CurrentGroupItem.FunctionName + "();";


                    string sThisGroupValue = oRow["CourtNameComplete"].ToString();

                    rowHeader.Visible = true;
                    rowItem.Visible = false;
                    RowRoomFooter.Visible = false;
                    //Bind group stuff
                    Label lblGroupname = e.Item.FindControl("lblGroupname") as Label;
                    lblGroupname.Text = sThisGroupValue;
                }
                else if (oRow["poorman_type"].ToString() == "CourtRoomChangefooter")
                {



                    m_oGroupingHelper.CurrentGroupItem.AddClientRow(RowRoomFooter.ClientID);
                    string sThisGroupValue = oRow["CourtNameComplete"].ToString();

                    rowHeader.Visible = false;
                    rowItem.Visible = false;
                    RowRoomFooter.Visible = true;
                    //Bind group stuff
                    Label lblGroupname = e.Item.FindControl("lblGroupname") as Label;
                    lblGroupname.Text = sThisGroupValue;
                }
                else
                {
                    //if (DropDownList1.SelectedValue != "None")
                    m_oGroupingHelper.CurrentGroupItem.AddClientRow(rowItem.ClientID);
                    rowHeader.Visible = false;
                    rowItem.Visible = true;
                    RowRoomFooter.Visible = false;
                }
            }
        }

        protected void rptrtable_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Response.Write("Item Command");
        }

    }
}

using System;
using System.Data;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    /// <summary>
    /// Summary description for RptWordLetter.
    /// </summary>
    public partial class Rptreceipt : System.Web.UI.Page
    {
        int ticketno;
        int empid;
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession uSession = new clsSession();
        clsLogger clog = new clsLogger();
        private clsCase _clscase = new clsCase();

        private void Page_Load(object sender, System.EventArgs e)
        {
            //Zeeshan Haider 11279 07/15/2013 Released Session["Tfilepath"] for new path to be assigned.
            Session["Tfilepath"] = null;
            //ticketno = Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));
            ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
            empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
            //Newly added as Receipt print was removed from procedure
            //clog.AddNote(empid,"Receipt Printed","",ticketno);
            CreateReport();
        }
        public void CreateReport()
        {
            string[] key = { "@ticketid", "@employeeid" };
            object[] value1 = { ticketno, empid };
            _clscase.TicketID = ticketno;
            int LetterType = 3;
            string filename;
            // Mohammad Ali 9949 12/29/2011 Check when case is criminal the execute CriminalReceipt.rpt else receipt.rpt
            string filepath = string.Empty;
            if (_clscase.IsCriminalCase())
            {
                filename = Server.MapPath("") + "\\CriminalReceipt.rpt";
                filepath = Cr.CreateReport_Retfname(filename, "USP_HTP_get_Criminal_receipt_info", key, value1, "false", LetterType, this.Session, this.Response);
            }
            else
            {
                filename = Server.MapPath("") + "\\receipt.rpt";
                filepath = Cr.CreateReport_Retfname(filename, "USP_HTS_get_receipt_info", key, value1, "false", LetterType, this.Session, this.Response);
            }


            filepath = filepath.Replace("\\\\", "\\"); Session["Tfilepath"] = filepath;
        }

        #region old

        private void CheckLanguage()
        {
            //Change by Ajmal
            IDataReader SqlDr = ClsDb.Get_DR_BySPByOneParmameter("sp_getLang_reiept", "TicketID_PK", ticketno);
            SqlDr.Read();
            if (SqlDr.GetValue(0).ToString() == "ENGLISH")
                CreateReport();
            else
                CreateBondReportSp();
        }
        public void CreateBondReportSp()
        {
            string[] key = { "@ticketid", "@employeeid" };
            object[] value1 = { ticketno, empid };

            string filename = Server.MapPath("") + "\\print_receipt_spanish.rpt";
            //string filename = "http://" + Request.ServerVariables["SERVER_NAME"] + "/lntechNew/Reports/print_receipt_spanish.rpt";
            Cr.CreateReport(filename, "USP_HTS_get_receipt_info", key, value1, this.Session, this.Response);
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}

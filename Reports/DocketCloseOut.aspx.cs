using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using FrameWorkEnation.Components;
using System.IO;
using lntechNew.Components.ClientInfo;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace lntechNew.Reports
{
    public partial class DocketCloseOut : System.Web.UI.Page
    {
        private DataTable dtRecords;
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        private ArrayList arlIndexes = new ArrayList();
        private AppSettingsReader re = new AppSettingsReader();
        private ArrayList arlGV = new ArrayList();
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        private int rec_count = 0;
        string FilePath;
        int employeeid;

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";
            if (!IsPostBack)
            {
                try
                {
                    tbl_Main.Visible = false;

                    string strServer;
                    strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                    //txtSrv.Text = strServer;
                    

                    //ViewState["TempFolder"] = "F:\\\\Farhan\\\\DocketCloseOut\\\\Temp\\\\";
                    //ViewState["ImageFolder"] = "F:\\Farhan\\DocketCloseOut\\Images\\\\";

                    ViewState["TempFolder"] = re.GetValue("NPATHDocketCloseOutTemp", typeof(string)).ToString();
                    ViewState["ImageFolder"] = re.GetValue("NPATHDocketCloseOutUploads", typeof(string)).ToString();

                    ViewState["SessionID"] = Session.SessionID.ToString();
                    ViewState["EmployeeID"] = ClsSession.GetCookie("sEmpID", this.Request).ToString();
                    employeeid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString());
                    Session["objTwain"] = "<OBJECT id='VSTwain1' codeBase='" + strServer + "/VintaSoft.Twain.dll#version=1,6,1,1' height='1' width='1' classid='" + strServer + "/VintaSoft.Twain.dll#VintaSoft.Twain.VSTwain' VIEWASTEXT> </OBJECT>";
                    //FilePath = @"F:\Farhan\DocketCloseOut\Images\" +Session.SessionID.ToString() +"3991img";

                    GetRecords();
                    CourtHeaderIndex();
                    
                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }

        }

      
        private void GetRecords()
        {
            try
            {
                string[] keys = { "@CourtDate" };
                object[] values = { cal_Date.SelectedDate };
                dtRecords = clsDb.Get_DT_BySPArr("USP_HTS_DocketCloseOut_Get_Records", keys, values);
                if (dtRecords.Rows.Count > 0)
                    tbl_Main.Visible = true;
                else
                    tbl_Main.Visible = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords();
                if (dtRecords.Rows.Count != 0)
                {
                    CourtHeaderIndex();
                    FillGridView();
                    gv_Details.HeaderRow.Visible = false;
                }
                else
                {
                    lbl_Message.Text = "No records found!";
                    gv_Details.DataSource = dtRecords;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


            
            
        }

        private void FillGridView()
        {
            try
            {
                arlGV.Clear();
                rec_count = 0;
                int LastCourtNo = Convert.ToInt32(dtRecords.Rows[0]["OldCourtNumbermain"].ToString());
                DataTable dtNew = dtRecords.Clone();
                string lastvalue = dtRecords.Rows[0]["CourtName"].ToString().Trim();

                for (int i = 0; i < dtRecords.Rows.Count; i++)
                {

                    if (dtRecords.Rows.Count > 0)
                    {

                        int row = 0;
                        arlIndexes.Add(row);

                        if ((lastvalue != dtRecords.Rows[i]["CourtName"].ToString().Trim()) || i == 0)
                        {
                            arlIndexes.Add(i);
                            lastvalue = dtRecords.Rows[i]["CourtName"].ToString().Trim();
                            string fullCourtName = "<Font Face=Tahoma>" + lastvalue + " - " + dtRecords.Rows[i]["CourtAddress"].ToString() + "-" + Convert.ToDateTime(dtRecords.Rows[i]["OldCourtDate"]).ToShortDateString();
                            LastCourtNo = Convert.ToInt32(dtRecords.Rows[i]["OldCourtNumberMain"].ToString());

                            for (int k = 0; k < 3; k++)
                            {
                                DataRow dr = dtRecords.NewRow();
                                if (k == 1)
                                    dr[1] = fullCourtName;
                                else
                                    dr[1] = "&nbsp;";
                                dtRecords.Rows.InsertAt(dr, i);
                                i++;
                            }
                            continue;
                        }


                    }


                    if (Convert.ToInt32(dtRecords.Rows[i]["OldCourtNumbermain"].ToString()) != LastCourtNo)
                    {

                        DataRow dr = dtRecords.NewRow();
                        dr[1] = "&nbsp;";
                        dtRecords.Rows.InsertAt(dr, i);
                        i++;
                        LastCourtNo = Convert.ToInt32(dtRecords.Rows[i]["OldCourtNumberMain"].ToString());
                    }
                    //dtNew.Rows.InsertAt(dtRecords.Rows[i], dtNew.Rows.Count);

                }

                //ViewState["RecordCount"] = dtRecords.Rows.Count.ToString();
                gv_Details.DataSource = dtRecords;
                gv_Details.DataBind();
                ViewState["RecordCount"] = (gv_Details.Rows.Count + 2).ToString();
                chk_SelectAll.Checked = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void CourtHeaderIndex()
        {
            if (dtRecords.Rows.Count > 0)
            {
                string lastvalue = dtRecords.Rows[0]["CourtName"].ToString().Trim();
                int row = 0;
                arlIndexes.Add(row);
                for (int i = 0; i < dtRecords.Rows.Count; i++)
                {
                    if (lastvalue != dtRecords.Rows[i]["CourtName"].ToString().Trim())
                    {
                        arlIndexes.Add(i);
                        lastvalue = dtRecords.Rows[i]["CourtName"].ToString().Trim();
                    }
                }
            }
        }

        protected void gv_Details_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //string l = gv_Details.Rows[e.Row].Controls[0].ToString();
                //if(gv_Details.Rows[e.Row].Style.Add(HtmlTextWriterStyle.BorderStyle,"0"));

                ImageButton ib = (ImageButton)e.Row.FindControl("ibtn_Scan");
                if (ib != null)
                {
                    //ib.Attributes.Add("OnClick", "StartScan()");
                    if (dtRecords.Rows[e.Row.RowIndex]["ImagePath"] == DBNull.Value)
                    {
                        ib.Style.Add(HtmlTextWriterStyle.Visibility, "Hidden");
                    }
                }

                Label lbl_tic = (Label)e.Row.FindControl("lbl_TickedID");

                Label lbl_ticvio = (Label)e.Row.FindControl("lbl_TicketsViolationID");

                if (lbl_ticvio != null)
                    lbl_ticvio.Style.Add(HtmlTextWriterStyle.Display, "None");

                if (lbl_tic != null)
                    lbl_tic.Style.Add(HtmlTextWriterStyle.Display, "None");

                Label lbl_courtid = (Label)e.Row.FindControl("lbl_CourtID");
                if (lbl_courtid != null)
                    lbl_courtid.Style.Add(HtmlTextWriterStyle.Display, "None");

                HyperLink hlnk_LastName = (HyperLink)e.Row.FindControl("hlnk_LastName");

                if (hlnk_LastName != null)
                {

                    if (dtRecords.Rows[e.Row.RowIndex]["CourtDateMain"].ToString() == null || dtRecords.Rows[e.Row.RowIndex]["CourtDateMain"].ToString() == String.Empty)
                    {
                        hlnk_LastName.Style.Add(HtmlTextWriterStyle.Display, "None");

                    }

                    else
                    {
                        hlnk_LastName.Attributes.Add("onClick", "ShowPopup(" + dtRecords.Rows[e.Row.RowIndex]["TicketsViolaionID"].ToString() + "," + dtRecords.Rows[e.Row.RowIndex]["CourtID"].ToString() + ")");
                        Label lbl_Lastname = (Label)e.Row.FindControl("lbl_LastName");
                        if (lbl_Lastname != null)
                            lbl_Lastname.Style.Add(HtmlTextWriterStyle.Display, "None");
                    }
                }

                Label lbl = (Label)e.Row.FindControl("lbl_CourtTime");
                if (lbl != null)
                {
                    if (lbl.Text == "" || lbl.Text == String.Empty)
                    {

                        CheckBox chk = (CheckBox)e.Row.FindControl("chk_Select");
                        if (chk != null)
                        {
                            ImageButton ibtn = (ImageButton)e.Row.FindControl("ibtn_Scan");
                            if (ibtn != null)
                                ibtn.Style.Add(HtmlTextWriterStyle.Visibility, "Hidden");

                            chk.Style.Add(HtmlTextWriterStyle.Visibility, "hidden");

                            Label crt = (Label)e.Row.FindControl("lbl_FirstName");
                            if (crt != null)
                            {
                                // Style s=new Style();
                                // s.
                                for (int i = 0; i < e.Row.Cells.Count; i++)
                                {

                                    e.Row.Cells[i].Style.Add(HtmlTextWriterStyle.BorderWidth, "0");

                                }
                                Style s = new Style();
                                s.BorderWidth = 100;

                                e.Row.Attributes.Add("colspan", "0");

                                for (int j = 0; j < e.Row.Cells.Count; j++)
                                {
                                    if (j == 2)
                                        continue;
                                    e.Row.Cells[j].Style.Add(HtmlTextWriterStyle.Display, "None");
                                }
                                e.Row.Cells[2].ColumnSpan = e.Row.Cells.Count;


                                Label text = (Label)e.Row.FindControl("lbl_LastName");
                                if (text != null)
                                {
                                    if (text.Text != "&nbsp;")
                                    {
                                        text.Font.Bold = true;
                                        text.Font.Underline = true;
                                    }
                                }

                                //e.Row.Cells[0].Style.Add(HtmlTextWriterStyle.Width, "100%");

                            }

                        }

                    }

                }

                Label l2 = (Label)e.Row.FindControl("lbl_CourtDate");

                if (l2 != null)
                {
                    if (l2.Text != null && l2.Text != String.Empty)
                    {
                        rec_count++;
                        //Label count = (Label)e.Row.FindControl("lbl_SerialNo");
                        HyperLink count = (HyperLink)e.Row.FindControl("lbl_SerialNo");
                        count.Text = rec_count.ToString();
                        count.NavigateUrl = "http://" + Request.ServerVariables["SERVER_NAME"]+"/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + lbl_tic.Text;
                        //count.Attributes.Add("onClick", "window.open('http://newpakhouston.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + lbl_tic.Text +"')");
                    }
                }

                Label l3 = (Label)e.Row.FindControl("lbl_ClientCount");
                if (l3 != null)
                {
                    if (l3.Text == "0" || l3.Text == "1")
                        l3.Text = "";
                }

                Label l4 = (Label)e.Row.FindControl("lbl_MiddleName");
                if (l4 != null)
                {
                    if (l4.Text != String.Empty)
                    {
                        l4.Text.Insert(0, ",");
                    }
                }

                // Compare cells and change color if the data is different

                // Courtdate

                Label lbl_cur = (Label)e.Row.FindControl("lbl_CourtDate");
                Label lbl_old = (Label)e.Row.FindControl("lbl_CourtDateOld");
                if (lbl_cur != null && lbl_old != null)
                    if (lbl_cur.Text != lbl_old.Text)
                    {
                        lbl_cur.ForeColor = System.Drawing.Color.Red;
                        lbl_old.ForeColor = System.Drawing.Color.Red;
                    }

                //Court Time
                lbl_cur = (Label)e.Row.FindControl("lbl_CourtTime");
                lbl_old = (Label)e.Row.FindControl("lbl_CourtTimeOld");
                if (lbl_cur != null && lbl_old != null)
                    if (lbl_cur.Text != lbl_old.Text)
                    {
                        lbl_cur.ForeColor = System.Drawing.Color.Red;
                        lbl_old.ForeColor = System.Drawing.Color.Red;
                    }

                //Court Location
                lbl_cur = (Label)e.Row.FindControl("lbl_CourtLoc");
                lbl_old = (Label)e.Row.FindControl("lbl_CourtLocOld");
                if (lbl_cur != null && lbl_old != null)
                    if (lbl_cur.Text != lbl_old.Text)
                    {
                        lbl_cur.ForeColor = System.Drawing.Color.Red;
                        lbl_old.ForeColor = System.Drawing.Color.Red;
                    }

                lbl_cur = (Label)e.Row.FindControl("lbl_ViolationStatus");
                lbl_old = (Label)e.Row.FindControl("lbl_ViolationStatusOld");
                if (lbl_cur != null && lbl_old != null)
                    if (lbl_cur.Text != lbl_old.Text)
                    {
                        lbl_cur.ForeColor = System.Drawing.Color.Red;
                        lbl_old.ForeColor = System.Drawing.Color.Red;
                    }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void btn_DDL_Update_Click(object sender, EventArgs e)
        {
            try
            {

                if (ddl_Options.SelectedValue == "0")
                    return;

                foreach (GridViewRow grv in gv_Details.Rows)
                {
                    if (grv.Cells.Count > 5)
                    {
                        CheckBox chkbox = (CheckBox)grv.FindControl("chk_Select");

                        if (chkbox.Checked)
                        {
                            //Response.Write(dtRecords.Rows[grv.RowIndex]["TicketID_PK"].ToString() + " " + dtRecords.Rows[grv.RowIndex]["LastName"].ToString());
                            Label lbl = (Label)grv.FindControl("lbl_TickedID");
                            Label vio = (Label)grv.FindControl("lbl_TicketsViolationID");
                            Label lbl2 = (Label)grv.FindControl("lbl_CourtDate");
                            if (lbl != null && lbl2 != null)
                                if (lbl2.Text != "")
                                {
                                    employeeid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString());
                                    if (ddl_Options.SelectedValue == "Upload Docket")
                                    {
                                        string filename = SaveAsPDF(lbl.Text);
                                        
                                        /*string temppath = re.GetValue("NPATHDocketCloseOutTemp", typeof(string)).ToString() + Session.SessionID.ToString() + "3991img.jpg";
                                        
                                        //string temppath = @"F:\Farhan\DocketCloseOut\Temp\" + Session.SessionID.ToString() + "3991img.jpg";
                                        //FilePath = @"F:\Farhan\DocketCloseOut\Images\" + lbl.Text +"-" + cal_Date.SelectedDate.ToShortDateString().Replace("/","-") + ".jpg";
                                        FilePath = re.GetValue("NPATHDocketCloseOutUploads", typeof(string)).ToString() + lbl.Text + "-" + cal_Date.SelectedDate.ToShortDateString().Replace("/", "-") + ".jpg";

                                        FileInfo fi = new FileInfo(temppath);
                                        fi.CopyTo(FilePath, true);
                                        */
                                        //Response.Write(filename);
                                        string[] keys = { "@TicketID", "@FilePath", "@UploadDate" };
                                        object[] values = { lbl.Text, filename, cal_Date.SelectedDate.ToShortDateString() };
                                        clsDb.ExecuteSP("USP_HTS_DocketCloseOut_Save_Image", keys, values);
                                        UpdateCaseHistory(Convert.ToInt32(lbl.Text), "Docket Uploaded", String.Empty, employeeid);
                                    }
                                    else if (ddl_Options.SelectedValue == "Remove Docket")
                                    {
                                        string[] keys = { "@TicketID", "@UploadDate" };
                                        object[] values = { lbl.Text, cal_Date.SelectedDate.ToShortDateString() };
                                        clsDb.ExecuteSP("USP_HTS_DocketCloseOut_Delete_Image", keys, values);
                                        UpdateCaseHistory(Convert.ToInt32(lbl.Text), "Docket Removed", String.Empty, employeeid);
                                    }
                                    else
                                    {
                                        //Response.Write(lbl.Text);
                                        string[] keys = { "@TicketsViolationID", "@StatusID", "@EmpID" };
                                        object[] values = { vio.Text, ddl_Options.SelectedValue, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString()) };
                                        clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_STATUS", keys, values);
                                        //UpdateCaseHistory(Convert.ToInt32(lbl.Text), "Status Updated", String.Empty, employeeid);

                                    }
                                }

                        }
                    }
                }
                GetRecords();
                FillGridView();
                gv_Details.HeaderRow.Visible = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        

        protected void gv_Details_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                /*string temppath = @"F:\Farhan\DocketCloseOut\Temp\" + Session.SessionID.ToString() + "3991img.jpg";
                * FilePath = @"F:\Farhan\DocketCloseOut\Images\" + e.CommandArgument.ToString() + ".jpg";
                *
                * FileInfo fi = new FileInfo(temppath);
                * fi.CopyTo(FilePath, true);
                *
                * //Response.Write(FilePath);
                * string[] keys = { "@TicketID", "@FilePath" };
                * object[] values = { e.CommandArgument.ToString(), FilePath };
                * clsDb.ExecuteSP("USP_HTS_DocketCloseOut_Save_Image", keys, values);
                */

                if (e.CommandArgument.ToString() == "0")
                {
                    ImageButton btn = (ImageButton)e.CommandSource;
                    btn.Enabled = false;
                }
                else
                {
                    Session["DocketPDFPath"] = e.CommandArgument.ToString();
                    //Response.Redirect("PreviewDocket.aspx?ImgPath=" +e.CommandArgument.ToString());
                    Response.Redirect("PreviewDocket.aspx", false);
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void UpdateCaseHistory(int ticketno, string sub, string notes, int empid)
        {

            string[] keys = { "@ticketid", "@subject", "@notes", "@employeeid" };
            object[] values = { ticketno, sub, DBNull.Value, empid };
            clsDb.InsertBySPArr("SP_ADD_NOTES", keys, values);
        }

        protected void gv_Details_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private string SaveAsPDF(string TicketNo)
        {
            Document doc = new Document();
            AppSettingsReader re = new AppSettingsReader();
            int rec_count = Convert.ToInt32(txtnoofscandoc.Text);
            string SaveFilename = re.GetValue("NPATHDocketCloseOutUploads", typeof(string)).ToString();
            SaveFilename += TicketNo + "-" + cal_Date.SelectedDate.ToShortDateString().Replace("/", "-") + ".pdf";
            PdfWriter.getInstance(doc, new FileStream(SaveFilename, FileMode.Create));
            doc.Open();
            doc.setMargins(1, 0, 0, 0);
            for(int i=0;i<rec_count;i++)
            {
                string fn = re.GetValue("NPATHDocketCloseOutTemp", typeof(string)).ToString() + i.ToString() + Session.SessionID.ToString() + ClsSession.GetCookie("sEmpID", this.Request).ToString() + "img.jpg";
                iTextSharp.text.Image pic = iTextSharp.text.Image.getInstance(fn);
                pic.scaleToFit(PageSize.A4.Width - 50, PageSize.A4.Height - 50);
                doc.Add(pic);
            }
            
            doc.Close();
            return SaveFilename;
        }
      
    }
}

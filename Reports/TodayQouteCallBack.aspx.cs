using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;



namespace HTP.Reports
{
    public partial class TodayQouteCallBack : System.Web.UI.Page
    {
        //Fahad 6934 12/17/2009 Report Added.
        #region Variables

        ValidationReports clsValidationRep = new ValidationReports();
        clsSession ClsSession = new clsSession();
        //Ozair 7496 03/03/2010 changed variable name
        DataView dvTQ;

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                clsSession cSession = new clsSession();
                if (!cSession.IsValidSession(this.Request, this.Response, this.Session))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    lbl_Message.Text = "";
                    if (!IsPostBack)
                    {
                        calQueryFrom.SelectedDate = DateTime.Now;
                        calQueryTo.SelectedDate = DateTime.Now;
                        FillGrid(DateTime.Today, DateTime.Today);
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Data;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }


        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_Data.PageIndex = e.NewPageIndex;
                    //Ozair 7496 03/03/2010 changed session variable name
                    gv_Data.DataSource = (DataView)Session["dvTQ"];
                    gv_Data.DataBind();

                    Pagingctrl.PageCount = gv_Data.PageCount;
                    Pagingctrl.PageIndex = gv_Data.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.Message;
            }
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid(calQueryFrom.SelectedDate, calQueryTo.SelectedDate);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.Message;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method is used for populating the records according to the Date range for GridView Column's.
        /// </summary>
        /// <param name="StartDate">Starting Date for the Report.</param>
        /// <param name="EndDate">End Date for the Report.</param>
        private void FillGrid(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                DataTable dt = clsValidationRep.GetTodayQouteCallBack(StartDate, EndDate);
                if (dt.Rows.Count > 0)
                {
                    gv_Data.Visible = true;
                    GenerateSerialNo(dt);
                    if (Session[""] != null) Session.Clear();
                    //Ozair 7496 03/03/2010 changed variable name
                    dvTQ = new DataView(dt);
                    Session["dvTQ"] = dvTQ;
                    this.lbl_Message.Text = "";
                    gv_Data.DataSource = dt;
                    gv_Data.DataBind();
                    Pagingctrl.Visible = true;

                }
                else
                {
                    this.lbl_Message.Text = "No Records Found";
                    Pagingctrl.Visible = false;
                    gv_Data.Visible = false;
                }
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception e)
            {
                clsLogger.ErrorLog(e);
                lbl_Message.Text = e.Message;
            }
        }

        /// <summary>
        /// Generate the serial no on the basis of Ticket Id
        /// </summary>
        /// <param name="dtRecords">This method accepts the datatable for generating Serial Number.</param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
            }

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        /// <summary>
        /// Maintain the page index
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                DataTable dtView;
                //Ozair 7496 03/03/2010 changed variable name
                dvTQ = (DataView)Session["dvTQ"];
                dtView = dvTQ.ToTable();
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                gv_Data.DataSource = dtView;
                gv_Data.DataBind();
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();
                //Ozair 7496 03/03/2010 changed session variable name
                Session["dvTQ"] = dtView.DefaultView;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Maintained the page size
        /// </summary>
        /// <param name="pageSize">No of pages in whole RecordSet.</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Data.PageIndex = 0;
                    gv_Data.PageSize = pageSize;
                    gv_Data.AllowPaging = true;
                }
                else
                {
                    gv_Data.AllowPaging = false;
                }
                //Ozair 7496 03/03/2010 changed session variable name
                gv_Data.DataSource = (DataView)Session["dvTQ"];
                gv_Data.DataBind();
                Pagingctrl.PageCount = gv_Data.PageCount;
                Pagingctrl.PageIndex = gv_Data.PageIndex;
                Pagingctrl.SetPageIndex();

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion


    }
}

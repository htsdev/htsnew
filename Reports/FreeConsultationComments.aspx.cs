using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace lntechNew.Reports
{
    public partial class FreeConsultationComments : System.Web.UI.Page
    {
        DataTable dtRecords;
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        clsSession cSession = new clsSession();
        FreeConsultation FillData = new FreeConsultation();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                if (cSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }

        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {

                GetRecords();

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }
        private void SortGridView(string sortExpression, string direction)
        {

            // You can cache the DataTable for improving performance

            GetRecords();

            DataTable dt = dtRecords;

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + direction;

            gv_Records.DataSource = dv;

            gv_Records.DataBind();

        }
        private void GetRecords()
        {
            try
            {
                object[] values = { dtp_From.SelectedDate, dtp_To.SelectedDate };
                FillData.Value = values;
                FillData.GetData();
                dtRecords = FillData.GetData();
                gv_Records.DataSource = dtRecords;
                gv_Records.DataBind();
                if (dtRecords.Rows.Count == 0)
                    lbl_Message.Text = "No records found";
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        public SortDirection GridViewSortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)

                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            Session["SortExpression"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {

                GridViewSortDirection = SortDirection.Descending;
                Session["SortDirection"] = " desc";
                SortGridView(sortExpression, " desc");

            }

            else
            {

                GridViewSortDirection = SortDirection.Ascending;
                Session["SortDirection"] = " asc";
                SortGridView(sortExpression, " asc");

            } 
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["SortExpression"] != null && Session["SortDirection"] != null)
            {
                
                GetRecords();
                DataTable dt = dtRecords;
                DataView dv = new DataView(dt);
                dv.Sort = Convert.ToString(Session["SortExpression"]) + Convert.ToString(Session["SortDirection"]);
                gv_Records.DataSource = dv;
                gv_Records.PageIndex = e.NewPageIndex;
                gv_Records.DataBind();
            }
            else
            {
                GetRecords();
                gv_Records.DataSource = dtRecords;
                gv_Records.PageIndex = e.NewPageIndex;
                gv_Records.DataBind();
            }
        }
    }

}

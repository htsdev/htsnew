﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using MetaBuilders.WebControls;
using HTP.Components;
using MSXML2;
using System.Xml;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace HTP.Reports
{
    public partial class BatchPrintNew : System.Web.UI.Page
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);


        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgbtnsetcalldelete.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_delete_Click);
            this.imgbtnSOLdelete.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_delete_Click);
            this.imgbtn_delete.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_delete_Click);
            this.imgbtnMissedLetterDelete.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_delete_Click);
            this.imgbtnPledOutDeleted.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_delete_Click);
            this.imgbtn_trialdeletedprint.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_trialdeletedprint_Click);
            this.imgbtnMissedLetterDeletedPrint.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_trialdeletedprint_Click);
            this.imgbtnPledOutDeletedPrint.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_trialdeletedprint_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        #region Variables



        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataView dv_Result;

        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        Documents TrialNotRestrict = new Documents();

        clsBatch batch = new clsBatch();

        #endregion

        #region Methods

        //Method to Fill Drop Down List with Sales Reps
        private void FillSalesRepsList()
        {
            DataTable dtSalesRep = new DataTable();
            dtSalesRep = clsdb.Get_DT_BySPArr("USP_HTS_GET_ALL_SalesReps");
            if (dtSalesRep.Rows.Count > 0)
            {
                foreach (DataRow dr in dtSalesRep.Rows)
                {
                    ListItem item = new ListItem();
                    item.Value = dr["Abbreviation"].ToString();
                    item.Text = dr["Abbreviation"].ToString();
                    ddl_SalesRep.Items.Add(item);
                }
            }
        }



        //Fill Court Location
        private void FillCourts()
        {
            clsCourts courts = new clsCourts();
            DataSet courtlocations = courts.GetAllActiveCourtName();

            if (courtlocations.Tables.Count > 0 && courtlocations.Tables[0].Rows.Count > 0)
            {
                ddl_Courts.DataSource = courtlocations.Tables[0];
                ddl_Courts.DataBind();

                if (ddl_Courts.Items.FindByValue("0") != null)
                    ddl_Courts.Items.Remove(ddl_Courts.Items.FindByValue("0"));

                ddl_Courts.Items.Insert(0, new ListItem("ALL", "0"));
            }
        }

        //Method To Fill Grids with batch Records
        private void FillGrids(BatchLetterType batchLetterType, DataGrid dgRecords, ImageButton imgBtnPrint, ImageButton imgBtnDelete, ImageButton imgDeletedPrint, Label lblPrintedCount)
        {
            try
            {
                string dvName = (batchLetterType != BatchLetterType.TrialNotificationLetter) ? dgRecords.ClientID : "dv_Result";
                DataSet ds_batch = batch.getBatchbyLetterType(batchLetterType, ddl_SalesRep.SelectedValue.ToUpper(), Convert.ToInt32(ddl_Courts.SelectedValue), chk_ShowPrintedLetters.Checked, fromdate.SelectedDate, todate.SelectedDate, 0);

                DataView dv = new DataView();
                dv = ds_batch.Tables[0].DefaultView;
                //Save DataView In Session
                cSession.SetSessionVariable(dvName, dv, this.Session);

                //Display Records
                dgRecords.DataSource = ds_batch;
                dgRecords.DataBind();

                if (chk_ShowPrintedLetters.Checked == true)
                {
                    foreach (DataRow row in dv.Table.Rows)
                    {

                        string value = row.ItemArray[24].ToString();
                        if (value != "0")
                        {
                            dg_batchtrial.Columns[7].Visible = true;
                        }
                        else
                            dg_batchtrial.Columns[7].Visible = false;
                    }
                }
                else
                {
                    dg_batchtrial.Columns[7].Visible = false;
                }

                int reccount = dgRecords.Items.Count;
                imgBtnPrint.Attributes.Add("onclick", "return PrintLetter('" + dgRecords.ClientID + "'," + reccount + "," + Convert.ToInt32(batchLetterType).ToString() + ");");

                imgBtnDelete.Attributes.Add("onclick", "return PromptDelete('" + dgRecords.ClientID + "'," + reccount + "," + Convert.ToInt32(batchLetterType).ToString() + ");");


                //For already printed letters
                string filterstr = "isprinted=1";
                DataRow[] dr1;
                dr1 = ds_batch.Tables[0].Select(filterstr);
                imgDeletedPrint.Attributes.Add("onclick", "return PromptDeletedPrint(" + dr1.Length + ");");

                if (ds_batch.Tables.Count > 0 && ds_batch.Tables[0].Rows.Count > 0)
                {
                    lblPrintedCount.Text = "Printable# ";
                    lblPrintedCount.Text += Convert.ToString(ds_batch.Tables[0].Rows[0]["countp"]);
                }
                else
                {
                    lblPrintedCount.Text = "Printable# 0";
                }


            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        private void FillCount()
        {
            DataSet DsCount = batch.getBatchPrintCount(chk_ShowPrintedLetters.Checked, fromdate.SelectedDate, todate.SelectedDate, ddl_SalesRep.SelectedValue.ToUpper(), Convert.ToInt32(ddl_Courts.SelectedValue));

            lbl_trialprintedcpunt.Text = "Printable# " + Convert.ToString(DsCount.Tables[0].Rows[0]["Counts"]);
            lblMissedCourtLetterPrinted.Text = "Printable# " + Convert.ToString(DsCount.Tables[1].Rows[0]["Counts"]);
            lblPledOutLetterPrinted.Text = "Printable# " + Convert.ToString(DsCount.Tables[2].Rows[0]["Counts"]);
            lblsetcallLetterPrinted.Text = "Printable# " + Convert.ToString(DsCount.Tables[3].Rows[0]["Counts"]);
            lblSOLLetterPrinted.Text = "Printable# " + Convert.ToString(DsCount.Tables[4].Rows[0]["Counts"]);
            lblLORLetterPrinted.Text = "Printable# " + Convert.ToString(DsCount.Tables[5].Rows[0]["Counts"]);



        }

        private void ResetAllGrids()
        {
            ResetGrid(dg_batchtrial, "TrialNotificationLetter");
            ResetGrid(dgMissedCourt, "MissedCourtLetter");
            ResetGrid(dgPledOut, "ImgBtnPledOutLetterPrinted");
            ResetGrid(dgsetcall, "setcalldetail");
            ResetGrid(dgSOL, "SOLLetterPrinted1");


            gv_LOR.DataSourceID = null;
            gv_LOR.DataBind();
            gvLORPrinter.DataSourceID = null;
            gvLORPrinter.DataBind();




            FillCount();
        }

        private void ResetGrid(DataGrid dgName, string btnName)
        {
            dgName.DataSourceID = null;
            dgName.DataBind();
            ViewState[btnName] = null;
        }



        #region LOR Printed

        private void LoadLORGrid()
        {
            if (chk_ShowPrintedLetters.Checked)
            {
                LoadLORPrintedGrid();
                gv_LOR.Visible = false;
                imgbtnLORDelete.Visible = false;
                imgbtnLORPrint.Visible = false;
            }
            else
            {
                LoadLORNonPrintedGrid();
                gv_LOR.Visible = true;
                gvLORPrinter.Visible = false;
                imgbtnLORDelete.Visible = true;
                imgbtnLORPrint.Visible = true;
            }
        }

        /// <summary>
        /// Abid Ali 5359 01/27/2009 Load Printed Grid
        /// </summary>
        private void LoadLORPrintedGrid()
        {
            try
            {
                gvLORPrinter.Visible = true;
                string[] keys = { "@FromDate", "@ToDate", "@Status", "@ShowAll", "@FilterType", "@SalesRep", "@CourtId" };
                object[] values = { fromdate.SelectedDate, todate.SelectedDate, 0, false, 3, (ddl_SalesRep.SelectedIndex == 0 ? string.Empty : ddl_SalesRep.SelectedValue), ddl_Courts.SelectedValue };
                DataSet DS = clsdb.Get_DS_BySPArr("USP_HTP_GET_CertifiedLetterOfRep", keys, values);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    DataView dv = DS.Tables[0].DefaultView;
                    dv.Sort = "PrintDate DESC";
                    gvLORPrinter.DataSource = DS.Tables[0].DefaultView;
                    gvLORPrinter.DataBind();
                    SortLORPrintedGrid();
                    gvLORPrinter.Visible = true;
                }
                else
                {
                    lblLORLetterPrinted.Text = "Printable# 0";
                    gvLORPrinter.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Abid Ali 5359 01/27/2009 Organized LOR printed grid
        /// </summary>
        protected void SortLORPrintedGrid()
        {
            String oldCertifiedMailNumber = String.Empty;
            int renderIndex = 1;
            try
            {
                foreach (GridViewRow grv in gvLORPrinter.Rows)
                {
                    if (grv.RowType == DataControlRowType.DataRow)
                    {

                        Table tbl = (grv.Parent as Table);
                        //Creating Cells 
                        TableCell tc = new TableCell();
                        //Nasir 5661 04/01/2009
                        TableCell tcBond = new TableCell();
                        TableCell tcBatchDateTime = new TableCell();
                        TableCell tcScanDateTime = new TableCell();
                        TableCell tcDocument = new TableCell();

                        TableCell tcRep = new TableCell();
                        TableCell tcPrintRep = new TableCell();

                        Label lbRep = new Label();
                        lbRep.Text = ((Label)grv.FindControl("lblRep")).Text;

                        Label lbPrintRep = new Label();
                        lbPrintRep.Text = ((Label)grv.FindControl("lblPrintRep")).Text;

                        Label lblCon = new Label();
                        lblCon.Text = ((Label)grv.FindControl("lblCmail")).Text;

                        HyperLink hplDocument = new HyperLink();
                        hplDocument.NavigateUrl = "#";
                        string docstoragLORPath = "../docstorage/" + System.Configuration.ConfigurationSettings.AppSettings["NTPATHLORPrint"].ToString().Replace(System.Configuration.ConfigurationSettings.AppSettings["NTPATH"].ToString(), string.Empty);
                        docstoragLORPath = docstoragLORPath.Replace("\\/", "");
                        hplDocument.Attributes.Add("onclick", "return OpenReport('" + docstoragLORPath + "/" + lblCon.Text + "_Printed.pdf');");
                        hplDocument.Target = "_blank";
                        hplDocument.Text = "<img src='/images/preview.gif' alt='" + lblCon.Text + "-Printed.pdf'  border='0'/>";


                        Label lblBatchDateTime = new Label();
                        lblBatchDateTime.Text = ((Label)grv.FindControl("lblBatchDate")).Text;

                        DateTime date;
                        if (DateTime.TryParse(lblBatchDateTime.Text, out date))
                        {
                            lblBatchDateTime.Text = date.ToString("MM/dd/yyyy @ HH:mm:ss");
                        }
                        else
                        {
                            lblBatchDateTime.Text = string.Empty;
                        }

                        Label lblScanDateTime = new Label();
                        lblScanDateTime.Text = ((Label)grv.FindControl("lblPrintDate")).Text;
                        if (DateTime.TryParse(lblScanDateTime.Text, out date))
                        {
                            lblScanDateTime.Text = date.ToString("MM/dd/yyyy @ HH:mm:ss");
                        }
                        else
                        {
                            lblScanDateTime.Text = string.Empty;
                        }


                        tc.CssClass = "TDHeading";
                        tc.ColumnSpan = 3;
                        tc.VerticalAlign = VerticalAlign.Middle;
                        tc.HorizontalAlign = HorizontalAlign.Left;

                        tcDocument.CssClass = "TDHeading";
                        tcDocument.VerticalAlign = VerticalAlign.Middle;
                        tcDocument.HorizontalAlign = HorizontalAlign.Center;

                        //Nasir 5661 04/01/2009 
                        tcBond.CssClass = "TDHeading";
                        tcBond.VerticalAlign = VerticalAlign.Middle;
                        tcBond.HorizontalAlign = HorizontalAlign.Center;

                        tcBatchDateTime.CssClass = "TDHeading";
                        tcBatchDateTime.VerticalAlign = VerticalAlign.Middle;
                        tcBatchDateTime.HorizontalAlign = HorizontalAlign.Center;

                        tcRep.CssClass = "TDHeading";
                        tcRep.VerticalAlign = VerticalAlign.Middle;
                        tcRep.HorizontalAlign = HorizontalAlign.Center;

                        tcPrintRep.CssClass = "TDHeading";
                        tcPrintRep.VerticalAlign = VerticalAlign.Middle;
                        tcPrintRep.HorizontalAlign = HorizontalAlign.Center;

                        tcScanDateTime.CssClass = "TDHeading";
                        tcScanDateTime.VerticalAlign = VerticalAlign.Middle;
                        tcScanDateTime.HorizontalAlign = HorizontalAlign.Center;

                        lbRep.Font.Bold = true;
                        lbRep.ForeColor = System.Drawing.Color.White;

                        lbPrintRep.Font.Bold = true;
                        lbPrintRep.ForeColor = System.Drawing.Color.White;

                        lblCon.Font.Bold = true;
                        lblCon.ForeColor = System.Drawing.Color.White;

                        lblBatchDateTime.Font.Bold = true;
                        lblBatchDateTime.ForeColor = System.Drawing.Color.White;

                        //Nasir 5661 04/01/2009 
                        Label lbl_Bond = (Label)grv.FindControl("lbl_Bond");
                        HiddenField hf_Bond = (HiddenField)grv.FindControl("hf_Bond");

                        if (hf_Bond.Value.ToLower() == "true")
                        {
                            lbl_Bond.Text = "B";
                        }
                        else
                        {
                            lbl_Bond.Text = String.Empty;
                        }


                        //If Row is first .....
                        if (grv.RowIndex == 0)
                        {
                            oldCertifiedMailNumber = String.Empty;
                            GridViewRow gv_row = new GridViewRow(grv.RowIndex, grv.DataItemIndex, DataControlRowType.DataRow, DataControlRowState.Normal);

                            gv_row.Controls.Clear();

                            tc.Controls.Add(lblCon);
                            tcDocument.Controls.Add(hplDocument);

                            tcBatchDateTime.Controls.Add(lblBatchDateTime);

                            tcRep.Controls.Add(lbRep);
                            tcPrintRep.Controls.Add(lbPrintRep);
                            tcScanDateTime.Controls.Add(lblScanDateTime);

                            gv_row.Controls.Add(tc);
                            //Nasir 5661 04/01/2009 
                            gv_row.Controls.Add(tcBond);
                            gv_row.Controls.Add(tcBatchDateTime);
                            gv_row.Controls.Add(tcRep);
                            gv_row.Controls.Add(tcScanDateTime);
                            gv_row.Controls.Add(tcPrintRep);
                            gv_row.Controls.Add(tcDocument);
                            tbl.Rows.AddAt(grv.RowIndex + 1, gv_row);


                            oldCertifiedMailNumber = lblCon.Text;
                            renderIndex++;

                        }
                        //if the Row is not first.... 
                        else
                        {

                            if (lblCon.Text != oldCertifiedMailNumber)
                            {
                                GridViewRow gv_row = new GridViewRow(grv.RowIndex, grv.DataItemIndex, DataControlRowType.DataRow, DataControlRowState.Normal);

                                tc.Controls.Add(lblCon);
                                tcDocument.Controls.Add(hplDocument);

                                tcBatchDateTime.Controls.Add(lblBatchDateTime);
                                tcRep.Controls.Add(lbRep);
                                tcPrintRep.Controls.Add(lbPrintRep);
                                tcScanDateTime.Controls.Add(lblScanDateTime);

                                gv_row.Controls.Add(tc);
                                //Nasir 5661 04/01/2009 
                                gv_row.Controls.Add(tcBond);
                                gv_row.Controls.Add(tcBatchDateTime);
                                gv_row.Controls.Add(tcRep);
                                gv_row.Controls.Add(tcScanDateTime);
                                gv_row.Controls.Add(tcPrintRep);
                                gv_row.Controls.Add(tcDocument);

                                tbl.Rows.AddAt(gv_row.RowIndex + renderIndex, gv_row);
                                renderIndex++;
                            }
                            //Setting oldCertifiedMailNumber
                            oldCertifiedMailNumber = lblCon.Text;
                        }

                    }

                }

                lblLORLetterPrinted.Text = "Printable# " + gvLORPrinter.Rows.Count;
            }
            catch (Exception ex)
            {

                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Abid Ali 5359 01/27/2009 Load Printed Grid
        /// </summary>
        private void LoadLORNonPrintedGrid()
        {
            try
            {
                gv_LOR.Visible = true;
                DataSet DS = batch.getBatchbyLetterType(BatchLetterType.LOR, ddl_SalesRep.SelectedValue.ToUpper(), Convert.ToInt32(ddl_Courts.SelectedValue), chk_ShowPrintedLetters.Checked, fromdate.SelectedDate, todate.SelectedDate, 0);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    DataView dv = DS.Tables[0].DefaultView;
                    dv.Sort = "PrintDate DESC";
                    gv_LOR.DataSource = DS.Tables[0].DefaultView;
                    gv_LOR.DataBind();
                    SortLORNonPrintedGrid();
                    gv_LOR.Visible = true;

                    btnSubmit.OnClientClick = "return CreateStringOrPromtDelete('" + gv_LOR.ClientID + "'," + gv_LOR.Rows.Count + "," + Convert.ToInt32(BatchLetterType.LOR).ToString() + ",1);";
                    imgbtnLORDelete.Attributes.Add("onclick", "return CreateStringOrPromtDelete('" + gv_LOR.ClientID + "'," + gv_LOR.Rows.Count + "," + Convert.ToInt32(BatchLetterType.LOR).ToString() + ",2);");


                    //For already printed letters
                    string filterstr = "isprinted=1";
                    DataRow[] dr1;
                    dr1 = DS.Tables[0].Select(filterstr);
                    imgbtnLORPrintDelete.Attributes.Add("onclick", "return PromptDeletedPrint(" + dr1.Length + ");");
                }
                else
                {
                    lblLORLetterPrinted.Text = "Printable# 0";
                    gv_LOR.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Abid Ali 5359 02/09/2009 Organized LOR non-printed grid
        /// </summary>
        protected void SortLORNonPrintedGrid()
        {
            try
            {
                String oldCourtName = String.Empty;
                int renderIndex = 1;

                foreach (GridViewRow grv in gv_LOR.Rows)
                {
                    if (grv.RowType == DataControlRowType.DataRow)
                    {
                        int courtId = int.Parse(((HiddenField)grv.FindControl("hdnCourtID")).Value);
                        ((CheckBox)grv.FindControl("chb")).ID += "_" + courtId.ToString();
                        if (grv.FindControl("lbl_tictrial") != null)
                        {
                            ((Label)grv.FindControl("lbl_tictrial")).ID = ((Label)grv.FindControl("lbl_tictrial")).ID.Replace("lbl_tictrial", "chb_" + courtId.ToString() + "_lblticket");
                        }
                        //Get the table of current Row
                        Table tbl = (grv.Parent as Table);
                        //Creating Cells 
                        TableCell tc = new TableCell();
                        TableCell tcChk = new TableCell();
                        //Get the Value of Row & Specified Item and making a label available for display
                        Label lblCon = new Label();
                        lblCon.Text = ((HiddenField)grv.FindControl("hdnCourtName")).Value;

                        //If Row is first .....
                        if (grv.RowIndex == 0)
                        {
                            //Clear oldCourtName
                            oldCourtName = String.Empty;
                            //creating row with the attribute of current row and deleting every item inside it
                            GridViewRow gv_row = new GridViewRow(grv.RowIndex, grv.DataItemIndex, DataControlRowType.DataRow, DataControlRowState.Normal);

                            gv_row.Controls.Clear();
                            CheckBox chb = new CheckBox();
                            chb.ID = "chb_parent_" + gv_LOR.ID + "_" + courtId.ToString();
                            chb.Attributes.Add("onclick", "checkall(this,'" + gv_LOR.ID + "')");


                            //Adding in parent objects                            
                            tc.CssClass = "TDHeading";
                            tc.HorizontalAlign = HorizontalAlign.Left;
                            lblCon.Font.Bold = true;
                            lblCon.ForeColor = System.Drawing.Color.White;
                            tc.Controls.Add(lblCon);
                            //Nasir 5661 04/01/2009
                            tc.ColumnSpan = 7;

                            tcChk.Controls.Add(chb);
                            tcChk.HorizontalAlign = HorizontalAlign.Right;
                            tcChk.CssClass = "TDHeading";

                            gv_row.Controls.Add(tc);
                            gv_row.Controls.Add(tcChk);
                            tbl.Rows.AddAt(grv.RowIndex + 1, gv_row);
                            //Setting OldCourtName
                            oldCourtName = lblCon.Text;
                            renderIndex++;

                        }
                        //if the Row is not first.... 
                        else
                        {

                            if (lblCon.Text != oldCourtName)
                            {
                                //creating row with the attribute of current row and deleting every item inside it

                                CheckBox chb = new CheckBox();
                                chb.ID = "chb_parent_" + gv_LOR.ID + "_" + courtId.ToString();
                                chb.Attributes.Add("onclick", "checkall(this,'" + gv_LOR.ID + "')");


                                GridViewRow gv_row = new GridViewRow(grv.RowIndex, grv.DataItemIndex, DataControlRowType.DataRow, DataControlRowState.Normal);

                                //gv_row.Controls.Clear();
                                //Adding in parent objects
                                tc.CssClass = "TDHeading";
                                tc.HorizontalAlign = HorizontalAlign.Left;
                                lblCon.Font.Bold = true;
                                lblCon.ForeColor = System.Drawing.Color.White;
                                tc.Controls.Add(lblCon);
                                //Nasir 5661 04/01/2009
                                tc.ColumnSpan = 7;
                                tc.Controls.Add(lblCon);

                                tcChk.Controls.Add(chb);
                                tcChk.HorizontalAlign = HorizontalAlign.Right;
                                tcChk.CssClass = "TDHeading";

                                gv_row.Controls.Add(tc);
                                gv_row.Controls.Add(tcChk);

                                tbl.Rows.AddAt(gv_row.RowIndex + renderIndex, gv_row);
                                renderIndex++;
                            }
                            //Setting OldCourtName
                            oldCourtName = lblCon.Text;
                        }
                    }
                }

                lblLORLetterPrinted.Text = "Printable# " + gv_LOR.Rows.Count.ToString();
            }
            catch (Exception ex)
            {

                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion

        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Abid Ali 5359 2/10/2009 set deault style none
                btndummynone.Style.Add("display", "none");
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        //Trial
                        fromdate.SelectedDate = DateTime.Now.AddDays(-7);
                        todate.SelectedDate = DateTime.Now;

                        //TODO:Need To Add In ASPX 
                        //img_fclosetrial.Attributes.Add("Onclick", "return ShowGrid('dg_batchtrial');");
                        //img_fopentrial.Attributes.Add("Onclick", "return HideGrid('dg_batchtrial');");
                        chk_ShowPrintedLetters.Attributes.Add("Onclick", "ShowDatesTable();");

                        txt_repprintcount.Text = "";
                        StrAcsDec = "ASC";
                        Session["StrAcsDec"] = StrAcsDec;

                        FillSalesRepsList();
                        FillCourts();
                        FillCount();
                        //FillGrids();

                    }

                    lbl_message.Text = "";

                    //When letter is printed
                    if (Convert.ToString(Session["updatebatch"]) == "true")
                    {
                        txt_repprintcount.Text = "";
                        //Method To Fill Grids
                        ResetAllGrids();
                        Session["updatebatch"] = "false";
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void fromdate_DateChanged(object sender, EventArgs e)
        {
            ResetAllGrids();
            UpdatePanel1.Update();
            UpdatePanel2.Update();
            UpdatePanel3.Update();
            UpdatePanel4.Update();
            UpdatePanel5.Update();
            UpdatePanel6.Update();

        }

        protected void todate_DateChanged(object sender, EventArgs e)
        {
            ResetAllGrids();
            UpdatePanel1.Update();
            UpdatePanel2.Update();
            UpdatePanel3.Update();
            UpdatePanel4.Update();
            UpdatePanel5.Update();
            UpdatePanel6.Update();
        }

        protected void ddl_Courts_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetAllGrids();
        }

        protected void ddl_SalesRep_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetAllGrids();
        }

        protected void chk_ShowPrintedLetters_CheckedChanged(object sender, EventArgs e)
        {
            ResetAllGrids();

        }

        protected void dg_batchtrial_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
                    return;

                Label lbl = (Label)e.Item.FindControl("lbl_printdate");
                if (lbl != null && lbl.Text != "")
                    lbl.Text += " & ";

                Label lbl2 = (Label)e.Item.FindControl("lbl_batchdate");
                if (lbl2 != null && lbl2.Text != "")
                    lbl2.Text += " & ";

                Label lbl3 = (Label)e.Item.FindControl("lbl_trialdate");
                Label lbltroom = (Label)e.Item.FindControl("lbl_RoomNo");
                if ((lbl3 != null && lbl3.Text != ""))// && (lbltroom.Text != "0"))
                    lbl3.Text += " & ";
                else if (lbltroom != null && lbltroom.Text == "0")
                    lbltroom.Text = "";


                //if print check box is check then a image in the grid will show
                if (chk_ShowPrintedLetters.Checked == true)
                {

                    dg_batchtrial.Columns[15].Visible = true;
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        HiddenField hf = ((HiddenField)e.Item.FindControl("hf_response"));
                        if (hf != null)
                        {
                            string respons = hf.Value.ToString();
                            XmlDocument doc = new XmlDocument();
                            if (respons != "")
                            {
                                doc.LoadXml(respons);
                                if (respons.Contains("DeliveryConfirmationNumber") == true)
                                {
                                    string Trackingnumber = doc.InnerText.ToString();
                                    ((HiddenField)e.Item.FindControl("hf_response")).Value = "Tracking Number :" + Trackingnumber;
                                }
                                else if (respons.Contains("Description") == true)
                                {
                                    string errordesc = doc.InnerText.ToString();
                                    ((HiddenField)e.Item.FindControl("hf_response")).Value = errordesc;
                                }
                            }
                            else
                            {
                                ((HiddenField)e.Item.FindControl("hf_response")).Value = "Regular Mail";
                            }


                        }

                        //Pupose: to check email status and make view of green tick appropirately//                        
                        HiddenField hfEmailFlag1 = e.Item.FindControl("hfEmailFlag1") as HiddenField;
                        if (hfEmailFlag1 != null && hfEmailFlag1.Value != string.Empty)
                        {

                            if (hfEmailFlag1.Value == "1")
                            {
                                System.Web.UI.WebControls.Image img = ((System.Web.UI.WebControls.Image)e.Item.FindControl("img_status1"));
                                img.ImageUrl = "../Images/right.gif";
                                img.Visible = true;
                            }
                            else
                            {
                                System.Web.UI.WebControls.Image img = ((System.Web.UI.WebControls.Image)e.Item.FindControl("img_status1"));
                                img.Visible = false;
                                img.ImageUrl = "../Images/right.gif";
                            }
                        }

                    }
                }
                else
                {
                    dg_batchtrial.Columns[15].Visible = false;
                }

            }
            catch (Exception Ex)
            {
                this.lbl_message.Text = Ex.Message + Ex.InnerException;
            }
        }

        protected void dg_batchtrial_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            lbl_message.Text = "";
            string gridName = ((DataGrid)source).ClientID;

            if (e.SortExpression.Equals("courtdate"))
            {
                if (e.CommandSource.Equals("courtdate"))
                    SortGrid(e.SortExpression, gridName);
            }
            else
                SortGrid(e.SortExpression, gridName);
        }

        #region "Grids Sorting Methods"

        protected void lbtnReceiptTialDate_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "trialdate";
                SortGrid("trialdate", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnReceiptRoom_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "trilroom";
                SortGrid("trilroom", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnReceiptBatchDate_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "batchdate1";
                SortGrid("batchdate1", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnReceiptBatchRep_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "batchemp";
                SortGrid("batchemp", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnReceiptPrintDate_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "printdate1";
                SortGrid("printdate1", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnReceiptPrintRep_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "printemp";
                SortGrid("printemp", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        private void SortGrid(string SortExp, string GridName)
        {
            try
            {
                DataGrid dgResult = null;
                string DataViewName = "";


                switch (GridName)
                {
                    case "dg_batchtrial":
                        SetAcsDesc(SortExp);
                        DataViewName = "dv_Result";
                        dgResult = dg_batchtrial;
                        break;
                    case "dgMissedCourt":
                        SetAcsDesc(SortExp, GridName);
                        DataViewName = "dgMissedCourt";
                        dgResult = dgMissedCourt;
                        break;
                    case "dgPledOut":
                        SetAcsDesc(SortExp, GridName);
                        DataViewName = "dgPledOut";
                        dgResult = dgPledOut;
                        break;
                    // noufil 3498 04/17/2008 setcall sort Expressions
                    case "dgsetcall":
                        SetAcsDesc(SortExp, GridName);
                        DataViewName = "dgsetcall";
                        dgResult = dgsetcall;
                        break;
                    case "dgSOL":
                        SetAcsDesc(SortExp, GridName);
                        DataViewName = "dgSOL";
                        dgResult = dgSOL;
                        break;
                    // Abid Ali 5359 12/30/2008 LOR Letter added
                    case "dgLOR":
                        //SetAcsDesc( SortExp, GridName );
                        //DataViewName = gv_LOR.ClientID;
                        //dgResult = gv_LOR;
                        break;

                }

                if ((DataView)(cSession.GetSessionObject(DataViewName, this.Session)) == null)
                {
                    switch (GridName)
                    {
                        case "dg_batchtrial":
                            FillGrids(BatchLetterType.TrialNotificationLetter, dg_batchtrial, imgbtn_trialprint, imgbtn_delete, imgbtn_trialdeletedprint, lbl_trialprintedcpunt);
                            break;
                        case "dgMissedCourt":
                            FillGrids(BatchLetterType.MissedCourtLetter, dgMissedCourt, imgbtnMissedLetterPrint, imgbtnMissedLetterDelete, imgbtnMissedLetterDeletedPrint, lblMissedCourtLetterPrinted);
                            dgResult = dgMissedCourt;
                            break;
                        case "dgPledOut":
                            FillGrids(BatchLetterType.PledOutLetter, dgPledOut, imgPledOutPrint, imgbtnPledOutDeleted, imgbtnPledOutDeletedPrint, lblPledOutLetterPrinted);
                            break;
                    }
                }

                dv_Result = (DataView)(cSession.GetSessionObject(DataViewName, this.Session));
                dv_Result.Sort = StrExp + " " + StrAcsDec;

                dgResult.DataSource = dv_Result;
                dgResult.DataBind();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                //StrExp =  ClsSession.GetSessionVariable ("StrExp",this.Session);
                //StrAcsDec = ClsSession.GetSessionVariable("StrAcsDec",this.Session);
                if (Session["StrExp"].ToString() == null)
                    return;
                if (Session["StrAcsDec"] == null)
                    return;

                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch (Exception ex)
            {
                //lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    //ClsSession.SetSessionVariable("StrAcsDec",StrAcsDec, this.Session);
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    //ClsSession.SetSessionVariable("StrAcsDec",StrAcsDec,this.Session);
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                //ClsSession.SetSessionVariable("StrExp", StrExp,this.Session) ;
                //ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session) ;				
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        #endregion

        #region Grid Sorting Events

        protected void btn_Court_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "courtname";
                SortGrid("courtname", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void btn_Status_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "status";
                SortGrid("status", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void btn_RCourt_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "courtname";
                SortGrid("courtname", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void btn_RStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "status";
                SortGrid("status", "dg_Receipt");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnTrialTrialdate_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "trialdate";
                SortGrid("trialdate", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnTrialTrialRoom_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "CourtNumber";
                SortGrid("CourtNumber", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnTrialBatchDate_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "batchdate1";
                SortGrid("batchdate1", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnTrialBatchRep_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "b_Emp";
                SortGrid("b_Emp", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnTrialPrintDate_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "printdate1";
                SortGrid("printdate1", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        protected void lbtnTrialPrintRep_Click(object sender, EventArgs e)
        {
            try
            {
                Session["StrExp"] = "p_Emp";
                SortGrid("p_Emp", "dg_batchtrial");
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message + ex.InnerException;
            }
        }

        #endregion

        #region Print Email Delete Events

        ////Delete Selected Trial Records LetterType=4
        private void imgbtn_delete_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                //Gets Letter Type To Be Deleted
                BatchLetterType batchLetterType = (BatchLetterType)Convert.ToInt32(((ImageButton)sender).CommandArgument);

                DataGrid dgResult = null;

                switch (batchLetterType)
                {
                    case BatchLetterType.TrialNotificationLetter: dgResult = dg_batchtrial; break;
                    case BatchLetterType.MissedCourtLetter: dgResult = dgMissedCourt; break;
                    case BatchLetterType.PledOutLetter: dgResult = dgPledOut; break;
                    case BatchLetterType.SetCallLetter: dgResult = dgsetcall; break;
                    case BatchLetterType.SOL: dgResult = dgSOL; break;
                }

                string BatchIDList = "";
                int CellIndex = 8;

                if (batchLetterType != BatchLetterType.TrialNotificationLetter)
                {
                    CellIndex = 6;
                }
                foreach (DataGridItem dgi in dgResult.Items)
                {
                    CheckBox mycheck = (CheckBox)dgi.Cells[CellIndex].Controls[1];
                    if (mycheck.Checked)
                    {
                        HiddenField batchID = (HiddenField)dgResult.Items[dgi.ItemIndex].FindControl("hf_BatchID");
                        BatchIDList += batchID.Value + ",";
                    }
                }
                ViewState["BatID"] = BatchIDList.ToString();
                ViewState["LT"] = Convert.ToInt32(batchLetterType);
                MPExtConfirm.Show();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        ////Delete Already printed Trial Records
        private void imgbtn_trialdeletedprint_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                batch.deletePrintedLetterFromBatch((BatchLetterType)Convert.ToInt32(((ImageButton)sender).CommandArgument));
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        /// <summary>
        ///  Fahad 5071 11/14/2008//
        ///  Pupose: to send email //
        /// </summary>
        private void SendTrialEmail()
        {
            try
            {
                //Gets Letter Type To Be Deleted


                //Pupose: Not using CommandArgument and now just assign the hardcoded value 2 to BatchLetterType //

                BatchLetterType batchLetterType = (BatchLetterType)2;

                string Ticketlist = "";
                string BatchIDList = "";
                int countEmail = 0;

                foreach (DataGridItem dgi in dg_batchtrial.Items)
                {
                    CheckBox mycheck = (CheckBox)dgi.Cells[8].Controls[1];
                    if (mycheck.Checked)
                    {
                        Label TicketID = (Label)dg_batchtrial.Items[dgi.ItemIndex].FindControl("lbl_tictrial");
                        HiddenField batchID = (HiddenField)dg_batchtrial.Items[dgi.ItemIndex].FindControl("hf_BatchID");
                        Label Email = (Label)dg_batchtrial.Items[dgi.ItemIndex].FindControl("lbl_email");
                        Ticketlist += TicketID.Text + ",";
                        BatchIDList += batchID.Value + ",";

                        if (Email.Text.Length > 0)
                            countEmail++;
                    }
                }

                if (countEmail > 0)
                {
                    DataTable dt = batch.emailBatchReport(batchLetterType, Ticketlist);
                    if (dt.Rows.Count >= 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            LNHelper.SerializableMailMessage objMessage = new LNHelper.SerializableMailMessage();
                            objMessage.To.Add(dr["recipients"].ToString());
                            objMessage.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["TrialMailFrom"]);
                            objMessage.Body = dr["body"].ToString().Replace("&", "&amp;");
                            objMessage.Subject = dr["subject"].ToString().Replace("&", "&amp;");
                            if (String.Compare(dr["bodyformat"].ToString(), "HTML", true) == 0)
                            {
                                objMessage.IsBodyHtml = true;
                            }
                            LNHelper.MailClass.SendEmailToQueue(objMessage);
                        }
                        batch.UpdateBatchPrintEmail(batchLetterType, Ticketlist, BatchIDList);
                    }

                    lbl_message.Visible = true;
                    if (lbl_message.Text != string.Empty)
                        lbl_message.Text = lbl_message.Text + "for Printing While Email has been sent";
                    else
                        lbl_message.Text = lbl_message.Text + "Email has been sent";

                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }

        }
        /// <summary>
        /// Fahad 5071 11/17/2008
        /// Pupose: to check email status and send email accordingly
        /// 
        /// </summary>
        private void TrialEmailSendCheck()
        {
            if (chk_ShowPrintedLetters.Checked)
            {
                if (hfEmailStatus.Value == "1")
                {
                    SendTrialEmail();
                }
            }
            else
            {
                SendTrialEmail();
            }
        }

        protected void imgbtn_trialprint_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                int EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                Session["bTicketID"] = txt_TicketIDs.Text;
                Session["bTicketIDBatch"] = txt_TicketIDsbatch.Text;
                Session["Lettertype"] = txt_LetterType.Text;


                if (Convert.ToInt32(txt_LetterType.Text) == 2)
                {
                    if (txtStopBubbling.Text != null)
                    {
                        if (rbregmail.Checked == true)
                        {
                            lbl_message.Visible = false;
                            if (txtStopBubbling.Text == "0")
                            {
                                ViewState["IsopenWindow"] = "1";
                                ViewState["TrialNotificationLetter"] = null;
                                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "opnewin", "window.open('PreviewBatch.aspx',''); void(0);", true);
                                TrialEmailSendCheck();
                            }
                        }
                        else if (rbemail.Checked == true)
                        {
                            HTP.Components.clsEDeliverySettings config = (HTP.Components.clsEDeliverySettings)System.Configuration.ConfigurationManager.GetSection("EDeliverySettings");

                            if (chk_ShowPrintedLetters.Checked == false)
                            {
                                #region Parameter setting

                                RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg_batchtrial);
                                foreach (Int32 selectedIndex in rsc.SelectedIndexes)
                                {
                                    lbl_message.Visible = false;
                                    StreamReader objStreamReader = new StreamReader(Server.MapPath("/Reports/RequestSchemaXml.xml"));
                                    string fileContent = objStreamReader.ReadToEnd();
                                    objStreamReader.Close();
                                    Label TicketID = (Label)dg_batchtrial.Items[selectedIndex].FindControl("lbl_tictrial");
                                    Label batchid_old = (Label)dg_batchtrial.Items[selectedIndex].FindControl("lbl_batchid");
                                    string batchid = batchid_old.Text.Replace(",", "");
                                    DataTable dt = batch.getinfo(Convert.ToInt32(TicketID.Text));
                                    fileContent = fileContent.Replace("{userid}", config.UserId);
                                    fileContent = fileContent.Replace("{frmname}", config.FromName);
                                    fileContent = fileContent.Replace("{frmAddress}", config.FromAddress2);
                                    fileContent = fileContent.Replace("{frmCity}", config.FromCity);
                                    fileContent = fileContent.Replace("{frmState}", config.FromState);
                                    fileContent = fileContent.Replace("{frmZip}", config.FromZip5);
                                    fileContent = fileContent.Replace("{ToName}", dt.Rows[0].ItemArray[0].ToString());
                                    fileContent = fileContent.Replace("{ToAddress1}", dt.Rows[0].ItemArray[2].ToString());
                                    fileContent = fileContent.Replace("{ToAddress2}", dt.Rows[0].ItemArray[1].ToString());
                                    fileContent = fileContent.Replace("{ToCity}", dt.Rows[0].ItemArray[3].ToString());
                                    fileContent = fileContent.Replace("{ToState}", dt.Rows[0].ItemArray[4].ToString());
                                    fileContent = fileContent.Replace("{tozip5}", dt.Rows[0].ItemArray[5].ToString());
                                    fileContent = fileContent.Replace("{tozip4}", dt.Rows[0].ItemArray[6].ToString());
                                    string strUrl = "https://secure.shippingapis.com/ShippingAPI.dll?API=DeliveryConfirmationV3&XML=" + fileContent;
                                    string strResponse = batch.GetdataFromJims(strUrl.ToString());
                                    string userName = ((HiddenField)dg_batchtrial.Items[selectedIndex].Cells[0].FindControl("hf_fullname")).Value;
                                    fileContent = "";
                                    if (strResponse != " " && strResponse.Contains("Error") == false)
                                    {
                                        XmlDocument doc = new XmlDocument();
                                        doc.LoadXml(strResponse);
                                        UTF8Encoding encoder = new UTF8Encoding();
                                        Decoder utf8Decode = encoder.GetDecoder();
                                        XmlNode DeliveryConfirmationNumber = doc.DocumentElement["DeliveryConfirmationNumber"];
                                        XmlNode DeliveryConfirmationLabel = doc.DocumentElement["DeliveryConfirmationLabel"];
                                        string Trackingnumber = DeliveryConfirmationNumber.InnerText.ToString();
                                        string xmlresponse = strResponse;
                                        byte[] imgDate;//= new byte[];
                                        if (DeliveryConfirmationLabel.InnerText != "")
                                        {
                                            byte[] todecode_byte = Convert.FromBase64String(DeliveryConfirmationLabel.InnerText);
                                            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

                                            MemoryStream ms = new MemoryStream(todecode_byte);
                                            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                                            ms.Close();

                                            ////returnImage = 
                                            //Noufil 3962 05/07/2008 get path for docstroage
                                            Documents doc1 = new Documents();
                                            string path = doc1.SaveAndGetDocumentPath(Convert.ToInt32(TicketID.Text), EmpID, "Labels Printed");
                                            Bitmap objBitmap = new Bitmap(returnImage);
                                            Graphics objGraphics = Graphics.FromImage(objBitmap);
                                            StringFormat objStringFormat = new StringFormat();
                                            objStringFormat.Alignment = StringAlignment.Far;
                                            objGraphics.DrawString(userName, new Font("Arial", 22, FontStyle.Bold), Brushes.Black, new Rectangle(100, 110, 680, 35), objStringFormat);
                                            objGraphics.Save();

                                            MemoryStream msToSave = new MemoryStream();
                                            //cropImage(objBitmap, new Rectangle(75, 99, 730, 540)).Save(msToSave, System.Drawing.Imaging.ImageFormat.Jpeg);
                                            System.Drawing.Image finalImage = cropImage(objBitmap, new Rectangle(75, 99, 730, 540));
                                            finalImage.Save(msToSave, System.Drawing.Imaging.ImageFormat.Jpeg);
                                            finalImage.Save(path);
                                            imgDate = msToSave.ToArray();
                                            string description = "Edelivery Track Number Generated:" + Trackingnumber;
                                            clog.AddNote(EmpID, description, "", Convert.ToInt32(TicketID.Text));




                                            //objImage.Save(@"C:\Testfile" + selectedIndex + ".tif", System.Drawing.Imaging.ImageFormat.Tiff);

                                        }
                                        else
                                        {
                                            imgDate = null;
                                        }

                                        if (imgDate != null)
                                        {
                                            batch.updateBatchPrintInformation(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), Trackingnumber, xmlresponse, Convert.ToInt32(batchid), 1, imgDate);
                                        }
                                        else
                                        {
                                            batch.updateBatchPrintInformation(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), Trackingnumber, xmlresponse, Convert.ToInt32(batchid), 1);
                                        }

                                        //lbl_message.Visible = true;
                                        //lbl_message.Text = "Electronic Delivery is Succesfully Track";
                                    }
                                    else
                                    {
                                        XmlDocument doc = new XmlDocument();
                                        doc.LoadXml(strResponse);
                                        batch.updateBatchPrintInformation(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "0", strResponse, Convert.ToInt32(batchid), 0);
                                        XmlNode Error = doc.DocumentElement["Description"];
                                        string errordesc = Error.InnerText.ToString();
                                        lbl_message.Text = errordesc + "For " + userName;
                                    }
                                }                                
                                TrialEmailSendCheck();
                                
                                #endregion
                            }

                            if (txtStopBubbling.Text == "0")
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('PreviewBatch.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');;</script>"));
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "opnewin", "alert('Please Select Regular or Edelivery check ');", true);
                        }
                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "opnewin", "window.open('PreviewBatch.aspx',''); void(0);", true);
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private static System.Drawing.Image cropImage(System.Drawing.Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);
            return (System.Drawing.Image)(bmpCrop);
        }

        #endregion

        #region Refactor Sorting Method

        protected void dg_Result_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            lbl_message.Text = "";
            SortGrid(e.SortExpression, ((DataGrid)source).ClientID);
        }

        protected void dg_Result_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            //Zeeshan Ahmed 3486 04/04/2008
            string GridName = ((DataGrid)source).ClientID;
            string SortExpression = GridName + "StrExp";

            switch (e.CommandName)
            {
                case "court":
                    Session[SortExpression] = "courtname";
                    SortGrid("courtname", GridName);
                    break;
                case "status":
                    Session[SortExpression] = "status";
                    SortGrid("status", GridName);
                    break;
                case "courtdate":
                    Session[SortExpression] = "courtdate";
                    SortGrid("courtdate", GridName);
                    break;
                case "courtroom":
                    Session[SortExpression] = "courtnumber";
                    SortGrid("courtnumber", GridName);
                    break;
                case "batchdate":
                    Session[SortExpression] = "batchdate";
                    SortGrid("batchdate", GridName);
                    break;
                case "batchrep":
                    Session[SortExpression] = "b_emp";
                    SortGrid("b_emp", GridName);
                    break;
                case "printrep":
                    Session[SortExpression] = "p_emp";
                    SortGrid("p_emp", GridName);
                    break;
                case "printdate":
                    Session[SortExpression] = "printdate";
                    SortGrid("printdate", GridName);
                    break;
            }
        }

        private void SetAcsDesc(string Val, string GridName)
        {
            try
            {
                //Zeeshan Ahmed 3486 04/04/2008
                //StrExp =  ClsSession.GetSessionVariable ("StrExp",this.Session);
                //StrAcsDec = ClsSession.GetSessionVariable("StrAcsDec",this.Session);
                // Abid Ali 5359 01/27/2009 sorting
                if (Session[GridName + "StrExp"] == null)
                {
                    StrExp = string.Empty;
                    //return;
                }
                else
                {
                    StrExp = Session[GridName + "StrExp"].ToString();
                }

                //Kazim 4148 6/10/2008 Remove tostring method
                // Abid Ali 5359 01/27/2009 sorting
                if (Session[GridName + "StrAcsDec"] == null)
                {
                    StrAcsDec = string.Empty;
                    // return;
                }
                else
                {
                    StrAcsDec = Session[GridName + "StrAcsDec"].ToString();
                }
                //StrExp = Session[GridName + "StrExp"].ToString();
                //StrAcsDec = Session[GridName + "StrAcsDec"].ToString();
            }
            catch (Exception ex)
            {
                //lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    //ClsSession.SetSessionVariable("StrAcsDec",StrAcsDec, this.Session);
                    Session[GridName + "StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session[GridName + "StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session[GridName + "StrExp"] = StrExp;
                Session[GridName + "StrAcsDec"] = StrAcsDec;
            }
        }

        #endregion

        //Add LOR Batch History
        protected void btnSubmit_Click(object sender, EventArgs e)
        {                        
            if (IsAllowToInsertCertifiedMailNumber())
            {


                if (hfLORIds.Value.Trim() != string.Empty)
                {
                    batch.InsertLORBatchHistory(hfLORIds.Value.Trim(), hfLORBatchIds.Value.Trim(), Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), txtCertifiedMailNumber.Text);
                }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "opnewin", "window.open('frmCertifiedLORBatchPrint.aspx?CMN=" + txtCertifiedMailNumber.Text + "&',''); void(0);", true);                                
            }
            else
            {
                lbl_message.Text = "Certified Mail Number already exists in the system please insert another certified mail number";                
            }

            MPECertifiedMainNumberPopup.Hide();
        }

        //Delete LOR letters
        protected void imgbtnLORDelete_Click(object sender, ImageClickEventArgs e)
        {
            //Delete Letters From Batch
            if (hfLORIds.Value.Trim() != string.Empty)
            {
                batch.deleteLetterFromBatch(BatchLetterType.LOR, hfLORBatchIds.Value);
                //Page.Response.Redirect(Page.Request.Url.ToString(), false);
            }

            //FillGrids();
            ImgBtnLORLetterPrinted_Click(null, null);
            UpdatePanel6.Update();
        }

        /// <summary>
        /// Check unique Certified Mail Number
        /// </summary>
        /// <returns></returns>
        private bool IsAllowToInsertCertifiedMailNumber()
        {
            bool isAllowed = false;
            try
            {


                string[] keys = { "@CertifiedMailNumber", "@CourtId" };
                object[] values = { txtCertifiedMailNumber.Text, hfLORCourtId.Value };

                DataSet ds = clsdb.Get_DS_BySPArr("USP_HTP_Check_CertifiedMailNumber", keys, values);

                bool CMNExistsOnCurrentDate = Convert.ToBoolean(ds.Tables[0].Rows[0]["CMNExistsOnCurrentDate"]);
                bool IsCMNExist = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsCMNExist"]);

                if (IsCMNExist)
                    isAllowed = CMNExistsOnCurrentDate;
                else
                    isAllowed = true;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

            return isAllowed;
        }

        protected void ImgBtnTrialNotification_Click(object sender, ImageClickEventArgs e)
        {
            if (ViewState["TrialNotificationLetter"] == null)
            {
                dg_batchtrial.DataSourceID = null;
                ViewState["TrialNotificationLetter"] = 1;
                FillGrids(BatchLetterType.TrialNotificationLetter, dg_batchtrial, imgbtn_trialprint, imgbtn_delete, imgbtn_trialdeletedprint, lbl_trialprintedcpunt);

            }
        }

        protected void ImgBtnMissedCourtLetters_Click(object sender, ImageClickEventArgs e)
        {
            if (ViewState["MissedCourtLetter"] == null)
            {
                ViewState["MissedCourtLetter"] = 1;

                FillGrids(BatchLetterType.MissedCourtLetter, dgMissedCourt, imgbtnMissedLetterPrint, imgbtnMissedLetterDelete, imgbtnMissedLetterDeletedPrint, lblMissedCourtLetterPrinted);

            }
        }

        protected void ImgBtnPledOutLetterPrinted_Click(object sender, ImageClickEventArgs e)
        {
            if (ViewState["ImgBtnPledOutLetterPrinted"] == null)
            {
                ViewState["ImgBtnPledOutLetterPrinted"] = 1;
                FillGrids(BatchLetterType.PledOutLetter, dgPledOut, imgPledOutPrint, imgbtnPledOutDeleted, imgbtnPledOutDeletedPrint, lblPledOutLetterPrinted);
            }
        }

        protected void ImgBtnsetcalldetail_Click(object sender, ImageClickEventArgs e)
        {
            if (ViewState["setcalldetail"] == null)
            {
                ViewState["setcalldetail"] = 1;

                FillGrids(BatchLetterType.SetCallLetter, dgsetcall, imgbtnsetcallprint, imgbtnsetcalldelete, imgbtnsetcallprintdelete, lblsetcallLetterPrinted);
            }
        }

        protected void ImgBtnSOLLetterPrinted_Click(object sender, ImageClickEventArgs e)
        {
            if (ViewState["SOLLetterPrinted1"] == null)
            {
                ViewState["SOLLetterPrinted1"] = 1;

                FillGrids(BatchLetterType.SOL, dgSOL, imgbtnSOLprint, imgbtnSOLdelete, imgbtnSOLprintdelete, lblSOLLetterPrinted);
            }
        }

        protected void ImgBtnLORLetterPrinted_Click(object sender, ImageClickEventArgs e)
        {
            //if (ViewState["LORLetterPrinted"] == null)
            // {
            //    ViewState["LORLetterPrinted"] = 1;

            LoadLORGrid();
            // }
        }

        protected void ChkAll_CheckedChanged(object sender, EventArgs e)
        {

            int LetterType = 0;
            int cell = 8;

            switch (((System.Web.UI.WebControls.WebControl)(sender)).ID.ToString())
            {
                case "chbTNAll": LetterType = 2; cell = 8; break;
                case "chbMCAll": LetterType = 11; cell = 6; break;
                case "chbPLOAll": LetterType = 12; cell = 6; break;
                case "chbSCAll": LetterType = 13; cell = 6; break;
                case "chbSOLAll": LetterType = 15; cell = 6; break;
            }

            BatchLetterType batchLetterType = (BatchLetterType)LetterType;
            DataGrid dgResult = null;

            switch (batchLetterType)
            {
                case BatchLetterType.TrialNotificationLetter: dgResult = dg_batchtrial; break;
                case BatchLetterType.MissedCourtLetter: dgResult = dgMissedCourt; break;
                case BatchLetterType.PledOutLetter: dgResult = dgPledOut; break;
                case BatchLetterType.SetCallLetter: dgResult = dgsetcall; break;
                case BatchLetterType.SOL: dgResult = dgSOL; break;                                
            }


            if (((System.Web.UI.WebControls.CheckBox)(sender)).Checked)
            {
                foreach (DataGridItem dgi in dgResult.Items)
                {
                    CheckBox mycheck = (CheckBox)dgi.Cells[cell].Controls[1];
                    mycheck.Checked = true;
                }
            }
            else
                foreach (DataGridItem dgi in dgResult.Items)
                {
                    CheckBox mycheck = (CheckBox)dgi.Cells[cell].Controls[1];
                    mycheck.Checked = false;
                }


        }

        protected void btnok_Click(object sender, EventArgs e)
        {
            if (LetterType.Value == "6")
            {
                imgbtnLORDelete_Click(null, null);
            }
            else
            {
                //Delete Letters From Batch
                batch.deleteLetterFromBatch((BatchLetterType)Convert.ToInt32(ViewState["LT"].ToString()), ViewState["BatID"].ToString());
                
                switch (ViewState["LT"].ToString())
                {
                    case "2": ViewState["TrialNotificationLetter"] = null; ImgBtnTrialNotification_Click(null, null); UpdatePanel1.Update(); break;
                    case "11": ViewState["MissedCourtLetter"] = null; ImgBtnMissedCourtLetters_Click(null, null); UpdatePanel2.Update(); break;
                    case "12": ViewState["ImgBtnPledOutLetterPrinted"] = null; ImgBtnPledOutLetterPrinted_Click(null, null); UpdatePanel3.Update(); break;
                    case "13": ViewState["setcalldetail"] = null; ImgBtnsetcalldetail_Click(null, null); UpdatePanel4.Update(); break;
                    case "15": ViewState["SOLLetterPrinted1"] = null; ImgBtnSOLLetterPrinted_Click(null, null); UpdatePanel5.Update(); break;

                }
            }



            // Page.Response.Redirect(Page.Request.Url.ToString(), false);
        }

        #endregion


    }
}

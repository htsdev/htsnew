using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace lntechNew.Reports
{
    public partial class BondFlagDiscrepancy : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsLogger bugTracker = new clsLogger();

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataSet DS;
        DataView dvBondFlagDiscrepancy;
        ValidationReports reports = new ValidationReports();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        // USP_HTS_Get_BondWaitingViolations
                        reports.getRecords("usp_hts_bondflagdiscrepancy");
                        reports.generateSerialNo();                                                   
                        DS = reports.records;
                        gv_Records.DataSource = DS.Tables[0];
                        gv_Records.DataBind();

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            dvBondFlagDiscrepancy = new DataView(DS.Tables[0]);
                            Session["dvBondFlagDiscrepancy"] = dvBondFlagDiscrepancy;
                            lblMessage.Text = "";
                            //Sabir Khan 4635  09/19/2008
                            //--------------------------
                            totlb.Visible = true;
                            LB_curr.Visible = true;
                            Label2.Visible = true;
                            DL_pages.Visible = true;
                            if (gv_Records.PageCount == 1)
                            {
                                DL_pages.Visible = false;
                                totlb.Visible = false;
                                LB_curr.Visible = false;
                                Label2.Visible = false;
                            }
                            
                        }                            
                        else
                        {
                            lblMessage.Text = "No Records found";
                            //Sabir Khan 4635 09/19/2008
                            //--------------------------
                            DL_pages.Visible = false;
                            totlb.Visible = false;
                            LB_curr.Visible = false;
                            Label2.Visible = false;
                            //--------------------------
                        }
                        Fill_ddlist();
                        LB_curr.Text = (gv_Records.PageIndex + 1).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }

        private void Fill_ddlist()
        {
            for (int i = 1; i <= this.gv_Records.PageCount; i++)
            {
                DL_pages.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));
            }

        }
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                dvBondFlagDiscrepancy = (DataView)Session["dvBondFlagDiscrepancy"];
                dvBondFlagDiscrepancy.Sort = StrExp + " " + StrAcsDec;
                gv_Records.DataSource = dvBondFlagDiscrepancy;
                gv_Records.DataBind();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }


        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                gv_Records.DataSource =  (DataView)Session["dvBondFlagDiscrepancy"] ;
                gv_Records.DataBind();
                
                DL_pages.SelectedIndex = e.NewPageIndex;
                LB_curr.Text = (e.NewPageIndex + 1).ToString();

            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        {

            int x = Convert.ToInt32(this.DL_pages.SelectedValue);

            gv_Records.PageIndex = x-1;
            gv_Records.DataSource = (DataView)Session["dvBondFlagDiscrepancy"];
            gv_Records.DataBind();
            LB_curr.Text = x.ToString();
        }

      
    }
}

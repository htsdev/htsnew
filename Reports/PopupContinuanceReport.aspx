<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopupContinuanceReport.aspx.cs" Inherits="lntechNew.Reports.PopupContinuanceReport" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

    
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- CORE CSS TEMPLATE - START -->
   


    <script language="javascript">
        function PopUpShowPreviewPDF(DocID, refType, DocNum, DocExt) {
            //window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
            window.open("../PaperLess/PreviewMain.aspx?DocID=" + DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
            return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="page-container row-fluid container-fluid">
            <section id="main-content" class="continuanceReportPage">
                <section class="wrapper main-wrapper row" style=''>
                        
               <div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" style="background: #f44336;color: white;">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                          <div class="clearfix"></div>
                    <div class='col-xs-12'>
                        <div class="page-title">
                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        </div>
                    </div>
        
            <table id="TableMain" cellspacing="0" cellpadding="0" width="550" align="center" border="0">
               <%-- <tr>
                    <td background="../images/separator_repeat.gif" colspan="2" height="11"></td>
                </tr>--%>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 226px">
                        <table class="clsLeftPaddingTable" id="tblsub" bordercolor="#ffffff" height="20" cellspacing="1"
                            cellpadding="0" width="100%" align="center" border="1">
                            <tr>
                                <td align="center" style="height: 30px; font-size: medium" class="clssubhead">
                                    <asp:Label ID="Label1" runat="server" Text="Scan Documents" CssClass="TopHeading"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 136px">
                                    <asp:GridView ID="GV_ContinuanceDOC" runat="server" AutoGenerateColumns="False" Width="100%" OnRowDataBound="GV_ContinuanceDOC_RowDataBound" CssClass="clsleftpaddingtable">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldate" runat="server" Text='<%# bind("UPDATEDATETIME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sub Type">
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsubtype" runat="server" Text='<%# bind("subdoctype") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldescription" runat="server" Text='<%# bind("ResetDesc") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnview" runat="server" ImageUrl="~/Images/preview.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>

                                                    <asp:Label ID="lbldid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lblocrid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lblDocNum" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lblRecType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>'
                                                        Visible="False" Width="10px" Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lblDocExtension" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lblEvent" Visible="false" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Events") %>' Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnClose" runat="server" Width="60px" CssClass="bt btn-default" Text="Close" OnClick="btnClose_Click" OnClientClick="window.close();"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

               <%-- <tr>
                    <td background="../images/separator_repeat.gif" colspan="2" height="11"></td>
                </tr>--%>
            </table>
            <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
    </form>
</body>
</html>

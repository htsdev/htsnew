using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;



namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for frmCONTINUANCE.
	/// </summary>
	public partial class frmCONTINUANCE : System.Web.UI.Page
	{
		int ticketno;
		int empid;
		clsSession uSession = new clsSession();
		clsLogger clog = new clsLogger();

		
		private void Page_Load(object sender, System.EventArgs e)
		{
			//ticketno =Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));
			ticketno =Convert.ToInt32(Request.QueryString["casenumber"]);
			empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));
			//Newly added as TRialletter print was removed from procedure
			//clog.AddNote(empid,"Continuance Letter Printed","",ticketno);
			
			CreateCONTINUANCEReport();
		}

		public void CreateCONTINUANCEReport() 
		{ 
			clsCrsytalComponent Cr = new clsCrsytalComponent();
			string[] key = {"@Ticketid","@employeeid"};
			object[] value1 = {ticketno,empid};
			string filename  = Server.MapPath("") + "\\CONTINUANCE.rpt";
			Int32  LetterType = 5;			
			Cr.CreateReport(filename ,"USP_HTS_Continuance_Letter", key, value1,"false",LetterType, this.Session, this.Response );
						
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

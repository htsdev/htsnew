<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Outsidecases.aspx.cs" Inherits="HTP.Reports.Outsidecases" %>

<%@ Register Assembly="ReportSettingControl" Namespace="ReportSettingControl" TagPrefix="cc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HCJP Auto Update Alert</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript" language="javascript">
    
   //Saeed 8101 09/28/2010 close report setting popup & refresh parent grid.
   function CloseReportSettingPopup()
   {
        document.getElementById("<%=btnPostBack.ClientID %>").click();
        //$find("ModalBehaviour").hide(); 
        return true;
   }   
   
    function btnClick(id) 
    { 
        document.getElementById(id).click();
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <div>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" width="870px" align="center" border="0">
                    <tr>
                        <td>
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" cellspacing="0" cellpadding="0" width="870px" border="0">
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr class="clsLeftPaddingTable">
                                    <td>
                                        <table>
                                            <tr>
                                                <td>                                                          
                                                    <!--START IMPLMENTING REPORT SETTING CONTRTOL-->
                                                    <asp:LinkButton ID="lnkbtn_ShowSetting" runat="server" OnClick="lnkbtn_ShowSetting_Click">Show Settings</asp:LinkButton>
                                                    <asp:Button ID="btnPostBack" runat="server" Style="display: none" OnClick="btnPostBack_Click" />
                                                    <asp:Panel ID="pnl_popup" runat="server" >
                                                        <table border="0" width="800px;">
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                <asp:PlaceHolder ID="phReportSetting" runat="server"></asp:PlaceHolder>
                                                                
                                                                    <%--<cc1:ReportSetting ID="ReportSetting1" runat="server" ReportTitle="Report Attribute"
                                                                        ConnectionStringKey="Connection String" HeaderCssClass="clssubhead" LabelCssClass="clsLeftPaddingTable"
                                                                        LinkButtonCssClass="clssubhead" GridviewCssClass="clsLeftPaddingTable" CellBackgroundImageUrl="CellBackgroundImage"
                                                                        TextboxCssClass="clsInputadministration" DropdownCssClass="clsInputCombo" ModalPopupExtenderId="mpeReportSetting" MainTableBackgroundColor="#EFF4FB" />--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnSimple" runat="server"  style="display:none" />
                                                    <ajaxToolkit:ModalPopupExtender ID="mpeReportSetting" runat="server" TargetControlID="btnSimple"
                                                        PopupControlID="pnl_popup" HideDropDownList="false" BehaviorID="ModalBehaviour" BackgroundCssClass="modalBackground" >
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <!--END IMPLMENTING REPORT SETTING CONTRTOL-->
                                                </td>
                                                <td width="685px">
                                                    &nbsp;
                                                </td>
                                                <td align="right">
                                                    <asp:CheckBox ID="chkShowAllUserRecords" runat="server" Text="Show All " CssClass="clssubhead"
                                                        OnCheckedChanged="chkShowAllUserRecords_CheckedChanged" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="right">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                        </aspnew:UpdateProgress>
                                                
                            <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True"
                                OnPageIndexChanging="gv_Records_PageIndexChanging" CellPadding="3" OnRowCommand="gv_Records_RowCommand"
                                OnRowDataBound="gv_Records_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ControlStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="bondflag" HeaderText="Bond">
                                        <ItemStyle CssClass="GridItemStyle" Width="5%" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="refcasenumber" HeaderText="Cause#">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="lastname" HeaderText="Last Name">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="firstname" HeaderText="First Name">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="shortname" HeaderText="CRT">
                                        <ItemStyle CssClass="GridItemStyle" Width="50px" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="autostatus" HeaderText="A">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="autocourtdate" HeaderText="Auto Date">
                                        <ItemStyle CssClass="GridItemStyle" Width="120px" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="verifiedcourtstatus" HeaderText="V">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="verifiedcourtdate" HeaderText="Verified Date">
                                        <ItemStyle CssClass="GridItemStyle" Width="120px" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="waittime" HeaderText="Days">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="LORSortDate" HeaderText="LOR">
                                        <ItemStyle CssClass="GridItemStyle" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Follow-Up Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_followupdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="img_Add" runat="server" CommandName="btnclick" Text="&lt;img src='../Images/add.gif' border='0'/&gt;" />
                                            <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("Firstname") %>' />
                                            <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("courtid") %>' />
                                            <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("Lastname") %>' />
                                            <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumber") %>' />
                                            <asp:HiddenField ID="hf_TicketId" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("casenumassignedbycourt") %>' />
                                            <asp:HiddenField ID="hf_court_loc" runat="server" Value='<%#Eval("shortname") %>' />
                                            <%-- <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("COURTVIOLATIONSTATUSIDMAIN") %>' />--%>
                                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" Value='<%#Eval("FollowUpDate") %>' />                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc4:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="FollowUp Date" />
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlFollowup" TargetControlID="btn">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc2:Footer ID="Footer1" runat="server" />                            
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <aspnew:PostBackTrigger ControlID="btnPostBack" />
            </Triggers>
        </aspnew:UpdatePanel>
    </div>
    </form>
    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 1111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 100004;
    </script>
</body>
</html>

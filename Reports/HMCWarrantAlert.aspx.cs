﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace lntechNew.Reports
{
    public partial class HMCWarrantAlert : System.Web.UI.Page
    {
       clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsLogger bugTracker = new clsLogger();
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataSet DS;
        DataView dvHMCWarrantAlert;
        ValidationReports reports = new ValidationReports();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                         
                        // USP_HTS_Get_HMCWarrantAlert

                        reports.getRecords("USP_HTS_Get_HMCWarrantAlert");
                        // Generate Serial no
                        reports.generateSerialNo();
                        DS = reports.records;
                        gv_Records.DataSource = DS.Tables[0];
                        gv_Records.DataBind();

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            dvHMCWarrantAlert = new DataView(DS.Tables[0]);
                            // Create Session of the DVHMCWarrantAlert data view
                            Session["dvHMCWarrantAlert"] = dvHMCWarrantAlert;
                            //Sabir Khan 4635 09/19/2008
                            //---------------------------
                            this.lblMessage.Text = "";
                            DL_pages.Visible = true;
                            totlb.Visible = true;
                            LB_curr.Visible = true;
                            Label2.Visible = true;
                            if (gv_Records.PageCount == 1)
                            {
                                DL_pages.Visible = false;
                                totlb.Visible = false;
                                LB_curr.Visible = false;
                                Label2.Visible = false;
                            }
                          

                        }
                        else
                        {
                            lblMessage.Text = "No records found";
                            DL_pages.Visible = false;
                            totlb.Visible = false;
                            LB_curr.Visible = false;
                            Label2.Visible = false;
                        }
                        //--------------------------------

                        //Fill Drop Down
                        Fill_ddlist();
                        //Display the current page no.
                        LB_curr.Text = (gv_Records.PageIndex + 1).ToString();
                    }
                }
            }
            catch (Exception ex)
            {               
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
         
        private void Fill_ddlist()
        {
            try
            {
                for (int i = 1; i <= this.gv_Records.PageCount; i++)
                {
                    DL_pages.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));
                }
            }
            catch(Exception ex)
            {              
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);  
            }

        }     
        //sets paging in gv_records
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {             
                gv_Records.PageIndex = e.NewPageIndex;
                gv_Records.DataSource = (DataView)Session["dvHMCWarrantAlert"];
                gv_Records.DataBind();

                DL_pages.SelectedIndex = e.NewPageIndex;
                LB_curr.Text = (e.NewPageIndex + 1).ToString();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Change the page of gv_Records
        protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int x = Convert.ToInt32(this.DL_pages.SelectedValue);
                gv_Records.PageIndex = x - 1;
                gv_Records.DataSource = (DataView)Session["dvHMCWarrantAlert"];
                gv_Records.DataBind();
                LB_curr.Text = x.ToString();
            }
            catch (Exception ex)
            {
              bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
    
}

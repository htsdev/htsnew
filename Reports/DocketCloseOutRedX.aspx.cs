using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HTP.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class DocketCloseOutRedX : System.Web.UI.Page
    {

        DataSet dsRecords;
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {

            GetRecords("", "", 0);

            if (dsRecords.Tables[0].Rows.Count > 0)
            {
                fillSortingLists(dsRecords);
                fillgrid();
                tbl_sort.Visible = true;
                td_sep.Visible = true;
                lbl_Title.Text = cal_Date.SelectedDate.ToShortDateString() + " Docket";
            }
            else
            {
                tbl_sort.Visible = false;
                lbl_Message.Text = "No records found!";
                gv_records.Visible = false;
                gv_records1.Visible = false;
                td_sep.Visible = false;
            }

        }

        // Grid Row Databound Event
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField rowtype = (HiddenField)e.Row.FindControl("hf_rowtype");

                    if (rowtype != null)
                    {
                        if (rowtype.Value == "1")
                        {
                            try
                            {
                                Label lbl1 = (Label)e.Row.FindControl("lbl_sno");
                                Label astatus = (Label)e.Row.FindControl("lbl_astatus");
                                Label vstatus = (Label)e.Row.FindControl("lbl_vstatus");
                                Image img = (Image)e.Row.FindControl("img_dis");

                                string discrepency = ((HiddenField)e.Row.FindControl("hf_discrepency")).Value;

                                switch (discrepency)
                                {
                                    case "0": img.ImageUrl = "../Images/right.gif"; break;
                                    case "1": img.ImageUrl = "../Images/cross.gif"; break;
                                    case "2": img.ImageUrl = "../Images/yellow_check.gif"; break;
                                    case "3": img.ImageUrl = "../Images/questionmark.gif"; break;
                                    default: img.ImageUrl = "../Images/cross.gif"; break;
                                }

                                //if (((HiddenField)e.Row.FindControl("hf_showquestionmark")).Value == "True")
                                //    img.ImageUrl = "../Images/questionmark.gif";

                                string status = astatus.Text.Substring(0, 4);
                                string courtdate = astatus.Text.Substring(4, 10);

                                if (status[3] != ' ')
                                    courtdate = astatus.Text.Substring(5, 10);

                                DateTime cdate = DateTime.Parse(courtdate);

                                if (cdate > DateTime.Now && (status.Trim() == "ARR" || status.Trim() == "JUR" || status.Trim() == "PRE" || status.Trim() == "JUD" || status.Trim() == "WAIT"))
                                {
                                    e.Row.Cells[5].Style.Add("background-color", "Yellow");
                                }

                            }
                            catch { }

                            ImageButton ibtn = (ImageButton)e.Row.FindControl("ImageButton1");
                            if (ibtn != null)
                            {
                                if (ibtn.Visible == true)
                                {
                                    HiddenField hf_date = (HiddenField)e.Row.FindControl("hf_CourtDate");

                                    if (hf_date.Value != "")
                                    {
                                        DateTime dt = Convert.ToDateTime(hf_date.Value);

                                        HiddenField hf_MM = (HiddenField)e.Row.FindControl("hf_MM");
                                        hf_MM.Value = dt.Month.ToString();

                                        HiddenField hf_DD = (HiddenField)e.Row.FindControl("hf_DD");
                                        hf_DD.Value = dt.Day.ToString();

                                        HiddenField hf_YY = (HiddenField)e.Row.FindControl("hf_YY");
                                        hf_YY.Value = dt.Year.ToString().Substring(2, 2);


                                        Label lbl_CourtNumber = (Label)e.Row.FindControl("lbl_CourtNumber");
                                        Label lbl_CourtTime = (Label)e.Row.FindControl("lbl_CourtTime");

                                        HiddenField hf_CourtID = (HiddenField)e.Row.FindControl("hf_CourtID");
                                        HiddenField hf_TicketsViolationID = (HiddenField)e.Row.FindControl("hf_TicketsViolationID");
                                        HiddenField hf_BondFlag = (HiddenField)e.Row.FindControl("hf_BondFlag");
                                        HiddenField hf_TicketID = (HiddenField)e.Row.FindControl("hf_TicketID");
                                        Label lbl_snapshotstatus = (Label)e.Row.FindControl("lbl_sstatus");

                                        //Zeeshan Ahmed 3486 03/31/2008
                                        //Display Bond Flag
                                        if (hf_BondFlag.Value == "1")
                                            ((Label)e.Row.FindControl("lblBondFlag")).Text = "B";
                                    }
                                }

                            }
                            Image ibtn2 = (ImageButton)e.Row.FindControl("ImageButton2");
                            Label lbl = (Label)e.Row.FindControl("lbl_LastUpdate");
                            if (ibtn2 != null)
                            {
                                if (lbl != null)
                                {
                                    if (lbl.Text.Contains("/"))
                                    {
                                        ibtn2.Visible = true;
                                        ibtn.Visible = false;
                                    }
                                    else
                                    {
                                        ibtn2.Visible = false;
                                        ibtn.Visible = true;
                                    }
                                }

                            }
                        }

                        else if (rowtype.Value == "2")
                        {

                            for (int i = 1; i < e.Row.Cells.Count; i++)
                                e.Row.Cells[i].Visible = false;

                            e.Row.Cells[0].ColumnSpan = 10;
                            e.Row.Cells[0].Style.Add("height", "20px");
                            e.Row.Cells[0].BackColor = System.Drawing.Color.Gray;

                        }

                        else if (rowtype.Value == "3")
                        {
                            for (int i = 1; i < e.Row.Cells.Count; i++)
                                e.Row.Cells[i].Visible = false;


                            e.Row.Cells[0].Style.Add("height", "20px");
                            e.Row.Cells[0].ColumnSpan = 10;

                            Label lbl = (Label)e.Row.FindControl("lbl_sno");

                            if (lbl.Text != "")
                            {
                                lbl.ForeColor = System.Drawing.Color.White;
                                e.Row.Cells[0].BackColor = System.Drawing.Color.Black;
                                e.Row.Cells[0].Font.Bold = true;
                                lbl.Visible = true;
                                ((HyperLink)e.Row.FindControl("hl_sno")).Visible = false; ;

                            }
                            if (lbl.Text == " , ")
                                e.Row.Visible = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        // Display Filtered Records
        protected void ddl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
            fillgrid();
        }

        private void GetRecords(string courtname, string status, int showclosedDocket)
        {
            try
            {
                lbl_Message.Text = "";
                clsDocketCloseOut docketCloseOutReport = new clsDocketCloseOut();
                dsRecords = docketCloseOutReport.GetDocketCloseOutReport(cal_Date.SelectedDate, courtname, status, 0, true, false, false, false);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void fillgrid()
        {

            if (dsRecords.Tables[0].Rows.Count > 0)
            {
                //gv_records.AllowPaging = true;
                formatDataSet(dsRecords, 0);
                gv_records.DataSource = dsRecords.Tables[0];
                gv_records.DataBind();


                if (dsRecords.Tables[1].Rows.Count > 0)
                {
                    formatDataSet(dsRecords, 1);

                    gv_records1.DataSource = dsRecords.Tables[1];
                    gv_records1.DataBind();
                    gv_records1.Visible = true;

                }
                else
                {
                    gv_records1.Visible = false;
                }

                gv_records.Visible = true;
            }
            else
            {
                lbl_Message.Text = "No records found!";
                gv_records.Visible = false;
                gv_records1.Visible = false;
            }

        }

        private void fillSortingLists(DataSet ds_records)
        {

            ddl_courts.Items.Clear();
            ddl_courts.Items.Add(new ListItem("ALL", ""));

            object[] rows = GetDistinctValues(ds_records.Tables[0], "ShortName");

            foreach (object OBJ in rows)
                ddl_courts.Items.Add((string)OBJ);

        }

        // Format Docket Report Records
        private void formatDataSet(DataSet ds_records, int tableno)
        {
            int sno = 1;

            DataColumn dc = new DataColumn("RowType");
            dc.DefaultValue = 1;

            ds_records.Tables[tableno].Columns.Add("SNO");
            ds_records.Tables[tableno].Columns.Add(dc);
            ds_records.Tables[tableno].Rows[0]["SNO"] = sno;
            DataRow dr1 = ds_records.Tables[tableno].NewRow();
            dr1["SNO"] = ds_records.Tables[tableno].Rows[0]["courtnamecomplete"].ToString();
            dr1["RowType"] = 3;
            ds_records.Tables[tableno].Rows.InsertAt(dr1, 0);
            sno++;

            for (int i = 1; i < ds_records.Tables[tableno].Rows.Count; i++)
            {

                if (ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"] != null)
                {
                    try
                    {

                        if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["ticketid_pk"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"]))
                        {
                            ds_records.Tables[tableno].Rows[i]["SNO"] = sno;
                            sno++;
                        }

                        if (ds_records.Tables[tableno].Rows[i]["courtnamecomplete"].ToString() != ds_records.Tables[tableno].Rows[i - 1]["courtnamecomplete"].ToString())
                        {
                            DataRow dr = ds_records.Tables[tableno].NewRow();
                            dr["RowType"] = 3;
                            dr["SNO"] = ds_records.Tables[tableno].Rows[i]["courtnamecomplete"].ToString();
                            ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                        }
                        else if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["courtnumber"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["courtnumber"]))
                        {
                            DataRow dr = ds_records.Tables[tableno].NewRow();
                            dr["RowType"] = 2;
                            dr["SNO"] = "\n";
                            ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                        }
                    }
                    catch { }
                }

            }
        }

        // Get Distinct Court Names
        public object[] GetDistinctValues(DataTable dtable, string colName)
        {
            Hashtable hTable = new Hashtable();
            foreach (DataRow drow in dtable.Rows)
            {
                try
                {
                    hTable.Add(drow[colName], string.Empty);
                }
                catch { }
            }
            object[] objArray = new object[hTable.Keys.Count];
            hTable.Keys.CopyTo(objArray, 0);
            return objArray;
        }

    }

}
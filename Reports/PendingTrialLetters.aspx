﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="PendingTrialLetters.aspx.cs"
    Inherits="lntechNew.Reports.PendingTrialLetters" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No Trial letter Report</title>
   <%-- <link href="../Styles.css" type="text/css" rel="stylesheet" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />


    <script language="javascript">
    function DeleteConfirm()
    {
        var isDelete=confirm("Are you sure you want to remove this client from this report?");
        if(isDelete== true)
        {
           return true;
        }
        
        else
        {
            return false;
        }
    }
    
    </script>

</head>

    <body class=" ">

        <form id="form1" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">
                
                <asp:Panel ID="pnl" runat="server">
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>
                        
<div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" style="margin-top: 8px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">
                                    <!-- PAGE HEADING TAG - START -->
                                    <h1 class="title">No Trial letter Report</h1>
                                    <!-- PAGE HEADING TAG - END -->
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>

                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <div class="actions panel_actions pull-right">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                                        <div class="col-xs-12">

                                            <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="table"
                                                OnRowDataBound="gv_Data_RowDataBound" AllowPaging="True" OnPageIndexChanging="gv_Data_PageIndexChanging" OnRowCommand="gv_Data_RowCommand" AllowSorting="True" OnSorting="gv_Data_Sorting" PageSize="30">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hl_SNo" runat="server" Text='<%# Container.DataItemIndex +1  %>'></asp:HyperLink>
                                                            <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ticket Number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="&lt;u&gt;Name&lt;/u&gt;" SortExpression="clientName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Name" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.clientName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="&lt;u&gt;Crt Date &amp; Time&lt;/u&gt;" SortExpression="CourtDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# bind("CourtDate","{0:MM/dd/yyyy @hh:mm tt}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="&lt;u&gt;Crt Loc &lt;/u&gt;" SortExpression="crtlocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcrtloc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.crtlocation") %>' CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="&lt;u&gt;Status&lt;/u&gt;" SortExpression="status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>' CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rep">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRepName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>' CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandArgument='<%# bind("ticketid_pk") %>'
                                                                CommandName="Del" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerSettings Mode="NextPrevious" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" />
                                                <PagerStyle HorizontalAlign="Center" />
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                </section>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->

        </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>





































<%--<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tr>
                    <td>
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../../images/separator_repeat.gif"  height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                </tr>
                <br />
                       <tr>
                      
                  <td style="text-align: right" class="clsleftpaddingtable">
                  <uc3:PagingControl ID="Pagingctrl" runat="server" />
                             
                  </td>
                </tr>
            
                <tr>
                    <td background="../../images/separator_repeat.gif"  height="11">
                    </td>
                </tr>
                <tr>
                  <td style="text-align: right">
                     
                  </td>
                </tr>
                <tr>
                                      
                    <td>
                        <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                            Width="100%" OnRowDataBound="gv_Data_RowDataBound" AllowPaging="True" OnPageIndexChanging="gv_Data_PageIndexChanging" OnRowCommand="gv_Data_RowCommand" AllowSorting="True" OnSorting="gv_Data_Sorting" PageSize="30">
                            <Columns>
                                <asp:TemplateField HeaderText="S#">
                                 <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                       <ItemTemplate>
                                        <asp:HyperLink ID="hl_SNo" runat="server" Text='<%# Container.DataItemIndex +1  %>'></asp:HyperLink>
                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Number">
                                 	 <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                           			
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Name&lt;/u&gt;" SortExpression="clientName">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Name" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.clientName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Crt Date &amp; Time&lt;/u&gt;" SortExpression="CourtDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" Text='<%# bind("CourtDate","{0:MM/dd/yyyy @hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Crt Loc &lt;/u&gt;" SortExpression="crtlocation">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcrtloc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.crtlocation") %>' CssClass="label"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Status&lt;/u&gt;" SortExpression="status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>' CssClass="label"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rep">
                             	 <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                           				
                                    <ItemTemplate>
                                        <asp:Label ID="lblRepName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>' CssClass="label"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandArgument='<%# bind("ticketid_pk") %>'
                                            CommandName="Del" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NextPrevious" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" />
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
                <TR>
				<TD background="../images/separator_repeat.gif" height="11"></TD>
			</TR>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>--%>

</html>

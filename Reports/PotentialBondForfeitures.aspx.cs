﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using System.IO;
using System.Text;
using System.Configuration;

namespace HTP.Reports
{
    ///<summary>
    ///</summary>
    public partial class PotentialBondForfeitures : System.Web.UI.Page
    {
        #region Variables

        readonly clsSession _clsSession = new clsSession();
        DataSet _ds;
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsCaseDetail _clsCaseDetail = new clsCaseDetail();
        readonly clsCase _clsCase = new clsCase();
        int _empId;

        #endregion

        #region Event Handler

        /// <summary>
        /// Page Load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // kashif 8771 07/03/2011 add xml comments
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    _empId = Convert.ToInt32(_clsSession.GetCookie("sEmpID", Request));
                    lblMessage.Text = string.Empty;

                    if (!IsPostBack)
                    {
                        chkShowAll.Checked = false;
                        FillGrid();
                    }

                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_Records;

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// grid view row command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // kashif 8771 07/03/2011 add xml comments
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {
                    rdb_Email.Checked = false;
                    rdb_SMS.Checked = false;
                    txt_Notes.Text = "";
                    txt_SmsNotes.Text = "";
                    txt_Subject.Text = "";
                    var dt = (DataTable)ViewState["_ds"];
                    string[] ticketId = e.CommandArgument.ToString().Split(';');
                    hf_Email_TicketId.Value = ticketId[0];
                    DataTable dtAtt = _clsCaseDetail.GetAttorneysByViolation(ticketId[0], ticketId[1]);
                    if (dtAtt.Rows.Count > 0)
                    {
                        txtAttorneyEmail.Text = Convert.ToString(dtAtt.Rows[0]["Email"]);
                        txt_CellNumber.Text = Convert.ToString(dtAtt.Rows[0]["AttorneyCellNumber"]);
                    }

                    mpeTrafficwaiting.Show();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }

        }

        /// <summary>
        /// send SMS event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // kashif 8771 07/03/2011 add xml comments
        protected void btnSmS_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = (DataTable)ViewState["_ds"];
                DataRow[] dr = dt.Select("ticketid_pk=" + hf_Email_TicketId.Value);
                if (dr.Length > 0)
                {
                    var clssms = new ClsSMS();
                    int ticketId = Convert.ToInt32(hf_Email_TicketId.Value);
                    clssms.UserName = Convert.ToString(ConfigurationManager.AppSettings["SmsServiceUserID"]);
                    clssms.Password = Convert.ToString(ConfigurationManager.AppSettings["SmsServicePassword"]);
                    clssms.Api_id = Convert.ToString(ConfigurationManager.AppSettings["SmsServiceAPI_ID"]);
                    string response = clssms.IsvalidAccount();
                    if (!response.Contains("Error"))
                    {
                        var message = new StringBuilder();
                        message.Append("Potential Bond Forfeiture\r\n");
                        message.Append(Convert.ToString(dr[0]["lastname"]));
                        message.Append(",");
                        message.Append(Convert.ToString(dr[0]["firstname"]));
                        message.Append(Convert.ToString(dr[0]["violationDescription"]));
                        message.Append(Convert.ToString(dr[0]["refcasenumber"]));
                        message.Append(" ");
                        message.Append(Convert.ToString(dr[0]["ShortName"]));
                        message.Append(" on ");
                        message.Append(Convert.ToDateTime(Convert.ToString(dr[0]["CourtDateMain"])).ToString("MM/dd/yyyy"));
                        message.Append(Convert.ToDateTime(Convert.ToString(dr[0]["CourtDateMain"])).ToShortTimeString());
                        if (dr.Length > 1)
                            message.Append("More Violations Omitted");
                        clssms.MessageText = Convert.ToString(message);

                        if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsKey"]).Trim() == "ON")
                        {
                            clssms.PhoneNumber = Convert.ToString(ConfigurationManager.AppSettings["TestPhoneNumbertoSMS"]).Trim();
                            if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                            {
                                if (clssms.SendSms(response))
                                {
                                    clssms.AddNoteToHistory(ticketId, "Potential Bond Forfeiture SMS Sent to Test Mobile Number", _empId);
                                    _clsCase.UpdateGeneralComments(ticketId, _empId, txt_SmsNotes.Text.Replace("<", "< "));

                                }
                            }
                        }
                        else
                        {
                            clssms.PhoneNumber = "1"+ txt_CellNumber.Text.Trim();
                            if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                            {
                                if (clssms.SendSms(response))
                                {
                                    clssms.AddNoteToHistory(ticketId, "Potential Bond Forfeiture SMS Sent to attorney", _empId);
                                    _clsCase.UpdateGeneralComments(ticketId, _empId, txt_SmsNotes.Text.Replace("<", "< "));

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }
        }

        /// <summary>
        /// end email event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // kashif 8771 07/03/2011 add xml comments
        protected void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = (DataTable)ViewState["_ds"];
                var dr = dt.Select("ticketid_pk=" + hf_Email_TicketId.Value);
                if (dr.Length > 0)
                    SendMail(dr);
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }
        }

        /// <summary>
        /// check box checkall event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // kashif 8771 07/03/2011 add xml comments
        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }
        }

        /// <summary>
        /// grid view page index change event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // kashif 8771 07/03/2011 add xml comments
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method used to fetch the data which will display on the Gridview.
        /// </summary>
        private void FillGrid()
        {
            gv_Records.Visible = true;
            string[] keys = { "@showAll" };
            object[] values = { chkShowAll.Checked };
            _reports.getRecords("usp_htp_get_potentialBond", keys, values);
            _ds = _reports.records;
            if (_ds.Tables[0].Rows.Count > 0)
            {
                _ds.Tables[0].Columns.Add("sno");
                ViewState.Add("_ds", _ds.Tables[0]);
                var dt = _ds.Tables[0];
                GenerateSerialNo(dt); ;
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = dt;
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lblMessage.Text = "No Records Found";
                gv_Records.Visible = false;
            }

        }

        /// <summary>
        /// This method used to generate the Serial Number to the given DataTable.
        /// </summary>
        /// <param name="dtRecords">DataTable on which serial number will generate.</param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            var sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (var i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();

        }

        private void SendMail(DataRow[] dr)
        {
            var subject = txt_Subject.Text.Replace("<", "< ");
            var message = GetEmailCaseBody(dr);
            var ticketId = Convert.ToInt32(hf_Email_TicketId.Value);
            string toUser = txtAttorneyEmail.Text;
            MailClass.SendMailToAttorneys(message, subject, "", toUser, "");
            _clsCase.UpdateGeneralComments(ticketId, _empId, txt_Notes.Text.Replace("<", "< "));
            var doc = new Documents();
            var fileName = "PotentialBondForfeitures" + Session.SessionID + DateTime.Now.ToFileTime() + ".pdf";
            doc.SaveEmailPDF(fileName, message, "Potential Bond Forfeiture Email Sent to attorney", _empId, ticketId);
        }

        private string GetEmailCaseBody(DataRow[] drEmail)
        {
            TextReader reader = null;
            var sbMessage = new StringBuilder();
            var sbViolationRow = new StringBuilder();
            var sbAllViolations = new StringBuilder();
            string singleRow;
            sbViolationRow.Append("<tr>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_VIOLATION_DESC</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_TICKET_NUMBER</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_CAUSE_NUMBER</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_LOCATION</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_DATE</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_TIME</span></td>");
            //Farrukh 8771 07/13/2011 Added Contact Info in Email 
            sbViolationRow.Append("<td><span class=\"Label\">VAR_CONTACT_DETAILS</span></td>");
            sbViolationRow.Append("</tr>");

            try
            {
                reader = new StreamReader(Server.MapPath("../EmailTemplates/BondForfeitures.htm"));
                {
                    sbMessage.Append(reader.ReadToEnd());
                    reader.Close();
                    foreach (var dr in drEmail)
                    {
                        singleRow = sbViolationRow.ToString();
                        singleRow = singleRow.Replace("VAR_VIOLATION_DESC", dr["violationDescription"].ToString());
                        singleRow = singleRow.Replace("VAR_TICKET_NUMBER", dr["refcasenumber"].ToString());
                        singleRow = singleRow.Replace("VAR_CAUSE_NUMBER", dr["casenumassignedbycourt"].ToString());
                        singleRow = singleRow.Replace("VAR_COURT_LOCATION", dr["ShortName"].ToString());
                        singleRow = singleRow.Replace("VAR_COURT_DATE", (Convert.ToDateTime(dr["CourtDateMain"].ToString())).ToString("MM/dd/yyyy"));
                        singleRow = singleRow.Replace("VAR_COURT_TIME", (Convert.ToDateTime(dr["CourtDateMain"].ToString())).ToShortTimeString());
                        //Farrukh 8771 07/13/2011 Added Contact Info in Email
                        singleRow = singleRow.Replace("VAR_CONTACT_DETAILS",
                                                      dr["contact1"] + "<br />" + dr["contact2"] +
                                                      "<br />" + dr["contact3"]);
                        sbAllViolations.Append(singleRow);
                    }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            sbMessage.Replace("VAR_Last_NAME", drEmail[0]["lastname"].ToString());
            sbMessage.Replace("VAR_First_NAME", drEmail[0]["firstname"].ToString());
            sbMessage.Replace("VAR_Notes", txt_Notes.Text.Replace("<", "< "));
            sbMessage.Replace("VAR_VIOLATION_ROWS", sbAllViolations.ToString());
            return sbMessage.ToString();
        }

        #endregion

    }
}

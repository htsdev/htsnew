<%@ Page Language="C#" AutoEventWireup="true" Codebehind="CaseStatusCheckReport.aspx.cs"
    Inherits="lntechNew.Reports.CaseStatusCheckReport" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Status Check Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table style="width: 100%">
                            <tr>
                                <td class="clslabel" width="5%">
                                    From:</td>
                                <td width="25%">
                                    <ew:CalendarPopup ID="cal_From" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                            Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td class="clslabel" width="5%">
                                    To:</td>
                                <td width="25%">
                                    <ew:CalendarPopup ID="cal_To" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                            Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td style="width: 100px">
                        <asp:Button ID="btn_Submit" runat="server" Text="Submit"
                            CssClass="clsbutton" OnClick="btn_Submit_Click" /></td>
                            </tr>
                        </table>
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td height="11" width="100%">
                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Search Date &amp; Time">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_LogDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.LogDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lastname">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Lastname" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                        <asp:HyperLink ID="hl_Lastname" runat="server" NavigateUrl='<%# ../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber= +DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DL Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_DLNumber" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_DOB" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PhoneNumber" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.PhoneNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Email" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/separator_repeat.gif" height="11" width="100%">
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SulloRemindercalls.aspx.cs"
    Inherits="lntechNew.Reports.SulloRemindercalls" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sullow Law Reminder Calls</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="height: 13px">
                                <asp:Label ID="lblMessage" runat="server" Width="320px" CssClass="label" ForeColor="Red"
                                    EnableViewState="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px" width="780" background="../../images/separator_repeat.gif"
                                height="9">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 9px" colspan="4">
                    <table>
                        <tr>
                            <td style="height: 9px" class="clsLabel">
                                Crt/Set date:
                            </td>
                            <td class="style2">
                                &nbsp;&nbsp;<ew:CalendarPopup ID="cal_EffectiveFrom" runat="server" EnableHideDropDown="True"
                                    ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                    AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
                                    PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Text=" " Width="80px">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                                <span class="clsLabel">&nbsp;Call status :</span>
                            </td>
                            <td class="style2">
                                <asp:DropDownList runat="server" ID="ddl_rStatus" CssClass="clsInputCombo">
                                </asp:DropDownList>
                            </td>
                            <td class="clsLabel">
                                &nbsp;
                            </td>
                            <td class="clsLabel">
                                &nbsp; Language :
                            </td>
                            <td class="style3">
                                &nbsp;
                            </td>
                            <td class="style5">
                                <asp:DropDownList ID="dd_language" CssClass="clsInputCombo" runat="server" Width="100">
                                    <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="English" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Spanish" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px" class="clsLabel">
                                &nbsp;
                            </td>
                            <td class="style2" colspan="2">
                                &nbsp;
                            </td>
                            <td>
                                <%--Sabir 4272 07/16/2008 two check boxes has been added. one for bond client and the other for reguler client.--%>
                                <asp:CheckBox ID="chkBondClient" runat="server" CssClass="clsLabel" Text="Bond Client"
                                    Checked="true" />&nbsp;
                                <asp:CheckBox ID="chkRegulerClient" runat="server" CssClass="clsLabel" Text="Reguler Client"
                                    Checked="true" />&nbsp;
                            </td>
                            <td class="clsLabel">
                                <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="clsbutton"></asp:Button>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_showall" runat="server" Text="Show All Cases" CssClass="clsLabel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                        border="0">
                        <tr>
                            <td style="height: 11px" width="780" background="../../images/separator_repeat.gif"
                                colspan="5" height="11">
                            </td>
                        </tr>
                        <tr>
                            <%--<td id="tdWait" runat="server" class="clssubhead" valign="middle" align="center"
                                        style="display: none">
                                        <img alt="Please wait" src="/Images/plzwait.gif" />
                                        Please wait While we update information.
                                    </td>--%>
                            <td id="tdData" runat="server" valign="top" align="center" colspan="2">
                                <asp:DataGrid ID="dg_ReminderCalls" runat="server" CssClass="clsLeftPaddingTable"
                                    AutoGenerateColumns="False" BorderStyle="Solid">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <table class="clsLeftPaddingTable" id="Table1" cellspacing="1" cellpadding="0" width="780"
                                                    align="center" border="0">
                                                    <tr>
                                                        <td colspan="4">
                                                            <asp:Label ID="lbl_owes" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.owedamount", "{0:C0}") %>'
                                                                CssClass="label" ForeColor="Red" Visible="False">
                                                            </asp:Label>
                                                            <%--<asp:Label ID="lbl_Firm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'
                                                                Visible="False">
                                                            </asp:Label> --%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" valign="middle" bgcolor="#eff4fb" align="left" height="20"
                                                            style="width: 130px">
                                                            <asp:Label ID="Label11" runat="server" CssClass="clssubhead" ForeColor="#123160">Name:</asp:Label>
                                                        </td>
                                                        <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 376px">
                                                            <asp:HyperLink ID="lnkName" runat="server" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.LastName"),", ",(DataBinder.Eval(Container, "DataItem.FirstName") )) %>'
                                                                CssClass="Label">
                                                            </asp:HyperLink>
                                                            <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_pk") %>'
                                                                CssClass="Label" ForeColor="#123160" Visible="False">
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_RID" runat="server" CssClass="Label" ForeColor="#123160" Visible="False"></asp:Label>
                                                        </td>
                                                        <td valign="top" width="123" bgcolor="#eff4fb" height="20" rowspan="4" align="left">
                                                            <p>
                                                                <asp:Label ID="lbl_contact1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'
                                                                    CssClass="Label">
                                                                </asp:Label><br>
                                                                <asp:Label ID="lbl_Contact2" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'
                                                                    CssClass="Label">
                                                                </asp:Label><br>
                                                                <asp:Label ID="lbl_Contact3" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'
                                                                    CssClass="Label">
                                                                </asp:Label><br>
                                                                <asp:Label ID="lbl_Language" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Languagespeak") %>'
                                                                    CssClass="Label">
                                                                </asp:Label><br>
                                                                <asp:HyperLink ID="lnk_Comments" runat="server">Add Comments </asp:HyperLink>
                                                                <%-- <asp:Label ID="lbl_phone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lbl_Fax" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fax") %>'
                                                                    Visible="False">
                                                                </asp:Label>--%>
                                                            </p>
                                                        </td>
                                                        <td valign="top" width="205" bgcolor="#eff4fb" height="20" rowspan="4">
                                                            <div style="width: 280px; height: 83px; overflow: auto">
                                                                <asp:Label ID="lbl_comments" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Comments") %>'
                                                                    CssClass="Label">
                                                                </asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" bgcolor="#eff4fb" height="20" style="width: 130px" align="left">
                                                            <asp:Label ID="Label12" runat="server" CssClass="Label" ForeColor="#123160">Case Status:</asp:Label>
                                                        </td>
                                                        <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 376px">
                                                            <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialDesc") %>'
                                                                CssClass="Label" ForeColor="#123160" Font-Bold="True">
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" valign="middle" bgcolor="#eff4fb" height="20" align="left"
                                                            style="width: 130px">
                                                            <asp:Label ID="Label13" runat="server" CssClass="label" ForeColor="#123160">Location:</asp:Label>
                                                        </td>
                                                        <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 376px">
                                                            <asp:Label ID="lbl_Location" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtAddress") %>'
                                                                CssClass="Label" ForeColor="#123160">
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_TicketViolationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketID_pk") %>'
                                                                CssClass="Label" ForeColor="#123160" Visible="False">
                                                                    
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>'
                                                                CssClass="Label" ForeColor="#123160" Visible="False">
                                                                    
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" valign="middle" bgcolor="#eff4fb" height="20" align="left"
                                                            style="width: 130px">
                                                            <asp:Label ID="Label14" runat="server" CssClass="Label" ForeColor="#123160">Call Back:</asp:Label>
                                                            <asp:TextBox ID="txt_sno" runat="server" Width="1px" ForeColor="#EFF4FB" Height="1px"
                                                                BackColor="#EFF4FB" BorderColor="#EFF4FB" BorderStyle="Solid"></asp:TextBox>
                                                        </td>
                                                        <td align="center" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 376px">
                                                            <div align="left">
                                                                <asp:Label ID="lbl_CallBacks" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                                                    CssClass="Label" ForeColor="#123160"></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#eff4fb" colspan="4" height="20" rowspan="1" valign="top" align="left">
                                                            <asp:Label ID="lbl_BondFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'
                                                                Visible="False"></asp:Label>
                                                            <asp:Label ID="lbl_Bond" runat="server" ForeColor="Red" Font-Bold="True" BackColor="#FFCC66"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'></asp:Label>
                                                            <asp:Label ID="lbl_Insurance" runat="server" BackColor="#FFCC66" Font-Bold="True"
                                                                ForeColor="Red" Text='<%# DataBinder.Eval(Container, "DataItem.Insurance") %>'></asp:Label>
                                                            <asp:Label ID="lbl_Child" runat="server" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.Child") %>'></asp:Label>
                                                            <asp:HiddenField ID="hf_GeneralComments" runat="server" Value="<%# Bind('GeneralComments') %>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 11px" width="780" background="../../images/separator_repeat.gif"
                                                            colspan="4" height="11">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableComment" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                        border="0">
                        <tr class="clsleftpaddingtable">
                            <td class="clsaspcolumnheader">
                            </td>
                            <td class="clsaspcolumnheader" colspan="2">
                            </td>
                        </tr>
                        <tr class="clsleftpaddingtable">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td valign="bottom" align="right" colspan="1" rowspan="1">
                            </td>
                        </tr>
                        <tr>
                            <td width="780" background="../../images/separator_repeat.gif" colspan="5" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none">
                                <asp:TextBox ID="txt_totalrecords" runat="server">
                                </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

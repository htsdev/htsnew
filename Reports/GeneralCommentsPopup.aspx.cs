using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Threading;

namespace HTP.Reports
{
    public partial class GeneralCommentsPopup : System.Web.UI.Page
    {

        #region Variables

        OpenServiceTickets ServiceTicket = new OpenServiceTickets();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();



        int TicketID = 0;
        int empid = 0;
        int fid = 0;

        string GeneralComments = string.Empty;
        string LName = string.Empty;
        string FName = string.Empty;
        string CourtLoc = string.Empty;
        string CourtStatus = string.Empty;
        string EmployeeName = string.Empty;
        string Url = string.Empty;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                //Kazim 4259 6/18/2008 Check valid session at page load 

                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Write("<script language='javascript'>opener.location.href='../frmLogin.aspx'; self.close();</script>");

                }
                else
                {

                    this.Form1.Attributes.Add("onload", "ShowPopUp_1()");
                    if (!IsPostBack)
                    {

                        //Get Case Number
                        TicketID = Convert.ToInt32(Request.QueryString["ticketid"]);
                        ViewState["TicketID"] = TicketID.ToString();

                        //Agha Usman 4271  07/01/08
                        bindCaseType();


                        clsUser ServiceTicketAdmin = new clsUser(int.Parse(ViewState["ServiceTicketEmpId"].ToString()));

                        //khalid 3188 2/20/08  service ticket admin
                        hf_defaultassigned.Value = Convert.ToString(ServiceTicketAdmin.EmpID);



                        //Get Service Ticket ID 
                        if (Request.QueryString["Fid"] == null)
                        {
                            //if new case from general info page
                            fid = 0;
                            ddl_assignto.Enabled = false;
                        }
                        else
                            fid = Convert.ToInt32(Request.QueryString["Fid"]);

                        ViewState["Fid"] = fid.ToString();

                        //task 2595, khalid 14-1-08, to diplay percentage
                        //completion when called from serviceticket report to primary user
                        if (Request.QueryString["OSTR"] == null)
                        {
                            ViewState["aOSTR"] = 0;
                        }
                        else
                        {
                            ViewState["aOSTR"] = Convert.ToInt32(Request.QueryString["OSTR"]);

                        }
                        //end khalid task 2595

                        //Get Employee ID 
                        empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                        ViewState["vEmpID"] = empid;

                        //Getting List of Service Ticket Category
                        FillServiceTicketCategory();

                        //Get Service Ticket Information
                        DisplayServiceTicketInformation();

                        ShowHide();


                        //Nasir 6098 08/21/2009 general comment overload method
                        WCC_GeneralComments.Initialize(TicketID, empid, 1, "Label", "clsinputadministration", "clsbutton", "Label");

                        WCC_GeneralComments.btn_AddComments.Visible = false;
                        WCC_GeneralComments.Button_Visible = false;
                        btnSubmit.Visible = true;

                        if (cb_showondocket.Checked == false)
                        {
                            this.cb_gencomments.Attributes.Add("disabled", "true");
                            this.cb_serinstruction.Attributes.Add("disabled", "true");
                        }

                    }
                    cb_showondocket.Attributes.Add("onclick", "ShowHide();");


                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void bindCaseType()
        {
            // Get Case Type Id from Case Information
            clsCase objCase = new clsCase();
            TicketID = Convert.ToInt32(ViewState["TicketID"]);
            int CaseTypeId = objCase.GetCaseType(TicketID);

            // Get Case Type information 
            CaseType objCaseType = new CaseType();
            objCaseType.CaseTypeId = CaseTypeId;
            DataTable dt = objCaseType.GetCaseTypeInfo();
            lblDivision.Text = dt.Rows[0]["CaseTypeName"].ToString();
            //ddl_assignto.SelectedValue = dt.Rows[0]["ServiceTicketEmpId"].ToString();

            //ViewState["ServiceTicketEmail"] = dt.Rows[0]["ServiceTicketEmailAlert"].ToString();
            ViewState["casetypeId"] = CaseTypeId.ToString();
            ViewState["ServiceTicketEmpId"] = dt.Rows[0]["ServiceTicketEmpId"].ToString();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //this.ddl_per.Attributes.Remove("disabled");
            //Added by Zeeshan Ahmed 1/21/2008
            TicketID = Convert.ToInt32(ViewState["TicketID"]);
            EmployeeName = cSession.GetCookie("sEmpName", HttpContext.Current.Request);
            Url = Request.Url.Authority;

            //Yasir Kamal 5759 04/09/2009 showTimeStamp in OpenServiceTicket Email
            WCC_GeneralComments.AddComments();


            //Update Service Ticket Information
            UpdateGC();

            ////Added by khalid Task Id : 2595
            //WCC_GeneralComments.AddComments();


            //Yasir Kamal 5759 04/14/2009 Refresh Grid when service ticket updated.
            //Sabir Khan 4910 10/07/2008
            if (Request.QueryString["Report"] != null)
            {
                // Session["ReloadWindow"] = true;
                ScriptManager.RegisterStartupScript(this.uppnl_main, this.GetType(), "Alert", "opener.focus(); window.opener.document.getElementById('btnDum').click();  self.close(); ", true);
                //Response.Write("<script language = 'javascript'>  opener.focus(); window.opener.document.getElementById('btnDum').click();  self.close();    </script>");
            }
            else
                ScriptManager.RegisterStartupScript(this.uppnl_main, this.GetType(), "Alert", "opener.focus(); opener.location.href = opener.location; self.close(); ", true);
            //Response.Write("<script language = 'javascript'>  opener.focus(); opener.location.href = opener.location; self.close();    </script>");
            //5759 end



        }

        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Zahoor 4770 09/12/08: for Continuance Sevice ticket category.
            if (drpCategory.SelectedItem.Text == "Continuance")
            {
                ddl_ContinuanceOption.Visible = true;
                lbl_ContinuanceOption.Visible = true;
                FillContinuanceoption(ddl_ContinuanceOption);
                this.ddl_per.Attributes.Add("disabled", "true");
                SetPercentOption();
            }
            else
            {
                ddl_ContinuanceOption.Visible = false;
                lbl_ContinuanceOption.Visible = false;
                this.ddl_per.Attributes.Remove("disabled");
            }
            // 4770
            if (drpCategory.SelectedValue == "8")
            {
                FillAttorneyList(1);
            }
            else
            {
                FillAttorneyList(0);
            }

            if (cb_showondocket.Checked)
            {
                this.cb_gencomments.Attributes.Remove("disabled");
                this.cb_serinstruction.Attributes.Remove("disabled");
            }
            else
            {
                this.cb_gencomments.Attributes.Add("disabled", "true");
                this.cb_serinstruction.Attributes.Add("disabled", "true");
            }


            WCC_GeneralComments.btn_AddComments.Visible = false;
            WCC_GeneralComments.Button_Visible = false;
        }


        #endregion

        #region Methods



        // Zahoor 4770 09/29/08 for Continuance Case
        private string getContinuancePercentValue()
        {
            if (ddl_ContinuanceOption.SelectedValue == "1")
            {
                return "0";
            }
            else if (ddl_ContinuanceOption.SelectedValue == "2" || ddl_ContinuanceOption.SelectedValue == "3")
            {
                return "25";
            }
            else if (ddl_ContinuanceOption.SelectedValue == "4" || ddl_ContinuanceOption.SelectedValue == "5")
            {
                return "50";
            }
            else
            {
                return "100";
            }

        }

        private void UpdateGC()
        {
            try
            {

                clsUser ServiceTicketAdmin = new clsUser(int.Parse(ViewState["ServiceTicketEmpId"].ToString()));

                //Set Parameter For Servie Ticket
                TicketID = Convert.ToInt32(ViewState["TicketID"]);
                ServiceTicket.TicketID = TicketID;
                ServiceTicket.FID = Convert.ToInt32(ViewState["Fid"].ToString());
                int empId = Convert.ToInt32(ViewState["vEmpID"]);
                ServiceTicket.EmpID = empId;
                if (drpCategory.SelectedItem.Text == "Continuance")
                {
                    ServiceTicket.Percentage = getContinuancePercentValue();
                }
                else
                {
                    ServiceTicket.Percentage = ddl_per.SelectedValue;
                }
                //Farrukh 9349 09/16/2011 Set Priority for Continuance and Deferred type service ticket
                if (drpCategory.SelectedItem.Text == "Deferred" || drpCategory.SelectedItem.Text == "Continuance")
                {
                    drpPriority.SelectedValue = "1"; //  1 for Medium
                }
                ServiceTicket.Category = drpCategory.SelectedValue;
                ServiceTicket.Priority = drpPriority.SelectedValue;
                ServiceTicket.ShowOnTrialDocket = Convert.ToInt32(cb_showondocket.Checked);
                ServiceTicket.ShowServiceTicketInstruction = cb_serinstruction.Checked;
                // Sabir Khan 5711 04/01/2009 Cancatinate old and new service ticket comments...
                // Sabir Khan 5738 06/08/2009 Fixed service ticket comments issue...
                if (lblServiceComments.Text.Length > 0)
                {
                    ServiceTicket.ServiceInstruction = lblServiceComments.Text + " " + tb_serinstruction.Text.Trim();
                }
                else
                {
                    ServiceTicket.ServiceInstruction = tb_serinstruction.Text.Trim();
                }
                ServiceTicket.ShowGeneralComments = cb_gencomments.Checked;
                //Fahad 5815 04/28/2009 
                ServiceTicket.EnteredComments = tb_serinstruction.Text.Trim();
                //Sabir Khan 5738 05/25/2009 Adding follow update filed with service ticket...
                //Sabir Khan 5738 06/03/2009 Allow null follow update...
                if (Convert.ToString(calfollowupdate.Text) == "")
                {
                    ServiceTicket.ServiceTicketFollowUpdate = "";
                }
                else
                {
                    ServiceTicket.ServiceTicketFollowUpdate = Convert.ToString(calfollowupdate.Text);
                }

                // Zahoor 4770 09/12/08: for Continuance Sevice ticket category.
                if (drpCategory.SelectedItem.Text == "Continuance")
                {
                    ServiceTicket.ContinuanceOption = int.Parse(ddl_ContinuanceOption.SelectedValue.ToString());
                }
                else
                {
                    ServiceTicket.ContinuanceOption = 0;
                }
                // 4770

                //Aziz 2382 12/19/2007
                //Adding of service ticket option
                bool result = false;

                //Modified By Zeeshan Ahmed 
                //Send Email On Adding New Service Ticket Or High Priority
                bool isNewServiceTicket = false;

                if (Convert.ToInt32(ViewState["Fid"].ToString()) == 0)
                {
                    //TODO ADD Application Section For Service Ticket

                    //khalid 3188 2/20/08 service ticket admin 
                    ServiceTicket.AssignTo = Convert.ToInt32(ServiceTicketAdmin.EmpID);
                    result = ServiceTicket.AddServiceTicket();
                    isNewServiceTicket = true;

                }
                else
                {

                    ServiceTicket.AssignTo = Convert.ToInt32(ddl_assignto.SelectedValue);
                    result = ServiceTicket.UpdateServiceTicketInformation();
                    //khalid 3016 2/12/08 adding note for open ticket
                    if (result == true)
                    {
                        if (Convert.ToInt32(ViewState["Assignedto"]) != Convert.ToInt32(ddl_assignto.SelectedValue))
                            clog.AddNote(empId, "Service Ticket Assigned to " + ddl_assignto.SelectedItem.Text.ToString(), "", TicketID);

                        //Sabir Khan 5738 06/03/2009 Add follow update date note in case history...
                        if (lblCurrentFollowupdate.Text != "N/A")
                        {
                            if (Convert.ToDateTime(lblCurrentFollowupdate.Text) != Convert.ToDateTime(calfollowupdate.Text))
                                clog.AddNote(empId, "Service Ticket Follow Up Date has changed from " + lblCurrentFollowupdate.Text + " to " + Convert.ToString(calfollowupdate.Text), "", TicketID);
                        }
                        else
                        {
                            clog.AddNote(empId, "Service Ticket Follow Up Date has changed from N/A to " + Convert.ToString(calfollowupdate.Text), "", TicketID);
                        }
                    }
                }

                if (result)
                {
                    ParameterizedThreadStart threadStart = new ParameterizedThreadStart(SendOpenServiceEmail);
                    Thread emailThread = new Thread(threadStart);
                    ArrayList objArrayList = new ArrayList();
                    objArrayList.Add(isNewServiceTicket);
                    objArrayList.Add(ViewState["casetypeId"]);
                    emailThread.Start(objArrayList);
                }
                else
                    lblMessage.Text = "GeneralComments Not Upated.";

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Display Service Ticket Information
        public void DisplayServiceTicketInformation()
        {
            if (fid == 0)
            {    //0=not attorneys, 1=attorneys, 2=all
                FillAttorneyList(0);
                clsCase oCase = new clsCase();
                DataSet dsCase = oCase.GetCase(TicketID);
                //Sabir Khan 6245 08/03/2009 Get court date....
                OpenServiceTickets objserviceticket = new OpenServiceTickets();
                objserviceticket.TicketID = TicketID;
                DataTable dsserviceticket = objserviceticket.GetCaseInformation();
                //Sabir Khan 6245 08/03/2009 Get max court date...
                DateTime maxcourtdate = Convert.ToDateTime(dsserviceticket.Rows[0]["courtDate"].ToString());
                foreach (DataRow dr in dsserviceticket.Rows)
                {
                    if (Convert.ToDateTime(dr["courtDate"].ToString()) > maxcourtdate)
                        maxcourtdate = Convert.ToDateTime(dr["courtDate"].ToString());
                }
                hfcourtdate.Value = maxcourtdate.ToString();
                lbl_ClientName.Text = Convert.ToString(dsCase.Tables[0].Rows[0]["Firstname"]);
                lbl_fisrtname.Text = Convert.ToString(dsCase.Tables[0].Rows[0]["Lastname"]);
                lblCurrentFollowupdate.Text = "N/A";
                //Sabir Khan 5738 05/25/2009 Get business days and assign to follow up calander control...
                //Sabir Khan 6082 07/03/2009 Set current date for adding new service ticket...
                //calfollowupdate.SelectedDate = GetBusinessDays(5);
                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                {
                    DateTime dt = DateTime.Now.AddDays(2);
                    calfollowupdate.Text = dt.ToShortDateString();
                }
                else
                {
                    calfollowupdate.Text = DateTime.Now.ToShortDateString();
                }
                if (hf_defaultassigned.Value.Length > 0)
                {
                    ddl_assignto.SelectedValue = hf_defaultassigned.Value;
                }
                return;
            }

            try
            {
                if (ServiceTicket.GetServiceTicketInformation(TicketID, fid))
                {
                    cb_gencomments.Checked = ServiceTicket.ShowGeneralComments;
                    cb_serinstruction.Checked = ServiceTicket.ShowServiceTicketInstruction;
                    cb_showondocket.Checked = Convert.ToBoolean(ServiceTicket.ShowOnTrialDocket);
                    OpenServiceTickets objserviceticket = new OpenServiceTickets();
                    objserviceticket.TicketID = TicketID;
                    DataTable dsserviceticket = objserviceticket.GetCaseInformation();
                    DateTime maxcourtdate = Convert.ToDateTime(dsserviceticket.Rows[0]["courtDate"].ToString());
                    foreach (DataRow dr in dsserviceticket.Rows)
                    {
                        if (Convert.ToDateTime(dr["courtDate"].ToString()) > maxcourtdate)
                            maxcourtdate = Convert.ToDateTime(dr["courtDate"].ToString());
                    }
                    hfcourtdate.Value = maxcourtdate.ToString();
                    //Sabir Khan 5738 05/25/2009  assign current follow up date to label and also set follow update in follow up calander....
                    lblCurrentFollowupdate.Text = Convert.ToString(ServiceTicket.ServiceTicketFollowUpdate);
                    if (ServiceTicket.ServiceTicketFollowUpdate == "N/A")
                    {
                        calfollowupdate.Text = GetBusinessDays(5).ToShortDateString();
                    }
                    else
                    {
                        calfollowupdate.Text = lblCurrentFollowupdate.Text;
                    }
                    if (cb_showondocket.Checked == true)
                    {
                        cb_gencomments.Enabled = true;
                        cb_serinstruction.Enabled = true;
                    }
                    //Select Category
                    drpCategory.SelectedValue = ServiceTicket.Category;
                    // Zahoor 4770 09/12/08: for Continuance Sevice ticket category.
                    if (drpCategory.SelectedItem.Text == "Continuance")
                    {
                        ddl_ContinuanceOption.Visible = true;
                        lbl_ContinuanceOption.Visible = true;
                        FillContinuanceoption(ddl_ContinuanceOption);

                        //Zeeshan 4906 10/06/2008 Handle Drop Down Value Selection Bug Which Occur When Item Not Exist In The List
                        if (ddl_ContinuanceOption.Items.FindByValue(ServiceTicket.ContinuanceOption.ToString()) != null)
                            ddl_ContinuanceOption.SelectedValue = ServiceTicket.ContinuanceOption.ToString();

                        SetPercentOption();
                    }
                    else
                    {
                        ddl_ContinuanceOption.Visible = false;
                        lbl_ContinuanceOption.Visible = false;
                    }

                    if (drpCategory.SelectedValue == "8")
                    {
                        FillAttorneyList(1);
                    }
                    else
                    {
                        FillAttorneyList(0);
                    }

                    //Select Priority
                    drpPriority.SelectedValue = ServiceTicket.Priority;

                    //Select Completion Percentage
                    ddl_per.SelectedValue = ServiceTicket.Percentage;

                    //If User Exist In List Than Select
                    string Assignto = Convert.ToString(ServiceTicket.AssignTo);
                    //Added by khalid 15-1-08 task 2595
                    ViewState["Assignedto"] = ServiceTicket.AssignTo;


                    //Sabir Khan 5711 04/01/2009 Display service ticket comments.
                    //Display Service Ticket Instruction
                    lblServiceComments.Text = ServiceTicket.ServiceInstruction.Trim();
                    //tb_serinstruction.Text = ServiceTicket.ServiceInstruction.Trim();

                    if (ddl_assignto.Items.FindByValue(Assignto) != null)
                        ddl_assignto.SelectedValue = Assignto;
                    else
                        ddl_assignto.SelectedIndex = 0;

                    //Display Client Name
                    lbl_ClientName.Text = ServiceTicket.LastName;
                    lbl_fisrtname.Text = ServiceTicket.FirstName;

                    btnSubmit.Text = "Update Service Ticket";

                    //Allow Only Primary User Or The User To Whom Ticket is Assigned Can Update the information                    
                    string usertype = cSession.GetCookie("sAccessType", this.Request);

                    if ((empid == ServiceTicket.AssignTo) || (usertype == "2") || (empid == 4028))
                        btnSubmit.Visible = true;
                    else
                        btnSubmit.Visible = false;


                }
                else
                {
                    lblMessage.Text = "Information Not Found";
                }



            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        //Get List Of Service Ticket Categories
        public void FillServiceTicketCategory()
        {
            try
            {

                // Agha Usman 4172 06/27/2008
                // Accessing Case Information 
                // Specifically Case type id
                clsCase objCase = new clsCase();
                TicketID = Convert.ToInt32(ViewState["TicketID"]);
                int CaseTypeId = objCase.GetCaseType(TicketID);


                //Getting Categories against Case Type Id
                DataTable dt = ServiceTicket.GetServiceTicketCategories(CaseTypeId);
                drpCategory.DataSource = dt;
                drpCategory.DataTextField = "Description";
                drpCategory.DataValueField = "ID";
                drpCategory.DataBind();

                if (fid <= 0)
                {
                    drpCategory.Items.Insert(0, new ListItem("----Choose---", "-1"));
                    drpCategory.SelectedValue = "-1";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Getting List Of Attorneys
        //0=not attorneys, 1=attorneys, 2=all
        public void FillAttorneyList(int IsAttorney)
        {
            try
            {
                clsUser Attorney = new clsUser();
                clsUser ServiceTicketAdmin = new clsUser(int.Parse(ViewState["ServiceTicketEmpId"].ToString()));

                DataSet AttorneyList = Attorney.GetAttorneyList(IsAttorney);

                ddl_assignto.Items.Clear();
                DataView dv = AttorneyList.Tables[0].DefaultView;
                dv.Sort = "name asc";
                ddl_assignto.DataSource = dv;
                ddl_assignto.DataTextField = "Name";
                ddl_assignto.DataValueField = "Employeeid";
                ddl_assignto.DataBind();

                ddl_assignto.Items.Insert(0, new ListItem("-- Select --", "0"));

                //khalid 3188 2/20/08 service ticket admin
                if (ddl_assignto.Items.FindByValue(Convert.ToString(ServiceTicketAdmin.EmpID)) == null)
                {
                    //khalid 3188 2/20/08 service ticket admin assigining default Assigned To if not exixts
                    ddl_assignto.Items.Insert(2, new ListItem(ServiceTicketAdmin.LastName + " " + ServiceTicketAdmin.FirstName, Convert.ToString(ServiceTicketAdmin.EmpID)));

                }
                //Task-2595 khalid , 14-1-07-8 to make default assigned Default service Ticket Admin     
                ddl_assignto.SelectedValue = hf_defaultassigned.Value;
                //End Fahad

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Evant Added By Fahad For Show Hide GeneralComments & Service Ticket Comments Fahad(11-1-08)
        //protected void cb_showondocket_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cb_showondocket.Checked == true)
        //    {
        //        generalcomments2.Visible = true;
        //        serviceticket2.Visible = true;

        //    }
        //    else if (cb_showondocket.Checked == false)
        //    {
        //        generalcomments2.Visible = false;

        //        serviceticket2.Visible = false;

        //    }

        //}
        //End Fahad
        //Method Created By Fahad To Hide/Show controls from secondary user Fahad(11-1-08)
        protected void ShowHide()
        {
            string usertype = cSession.GetCookie("sAccessType", this.Request).ToString();
            clsUser ServiceTicketAdmin = new clsUser(int.Parse(ViewState["ServiceTicketEmpId"].ToString()));
            if (Convert.ToInt32(ViewState["Fid"].ToString()) == 0)
            {
                ddl_assignto.Enabled = false;
                //ddl_per.Enabled = false;
                //ddl_per.Attributes.Add("disabled", "true");

            }
            else if ((usertype == "1") && (Convert.ToInt32(ViewState["Fid"].ToString()) > 0))
            {

                string IsSPNUser = cSession.GetCookie("sIsSPNUser", this.Request).ToString();
                ddl_assignto.Enabled = (IsSPNUser == "True" ? true : false);
                //10151
                if (ServiceTicketAdmin.IsSPNUser)
                {
                    ddl_assignto.Enabled = true;                
                }
                //ddl_assignto.Enabled = false;
                drpPriority.Enabled = false;
                drpCategory.Enabled = false;
                //task 2595, khalid 14-1-08 to hide when called from general info
                if (ViewState["aOSTR"] != null && Convert.ToInt32(ViewState["aOSTR"]) == 1)
                {
                    //ddl_per.Enabled = true;//means called from serviceticketreport
                    //ddl_per.Attributes.Remove("disabled");
                }
                else
                {
                    //ddl_per.Enabled = false;//means called from generalinfo
                    //ddl_per.Attributes.Add("disabled", "true");
                }

            }
            else if ((usertype == "2") && (Convert.ToInt32(ViewState["Fid"].ToString()) > 0))
            {
                ddl_assignto.Enabled = true;
                drpCategory.Enabled = true;
                drpPriority.Enabled = true;
                WCC_GeneralComments.Button_Visible = false;
            }
            // Zahoor 4770 09/30/08
            if (drpCategory.SelectedItem.Text == "Continuance")
            {
                ddl_ContinuanceOption.Visible = true;
                lbl_ContinuanceOption.Visible = true;
                FillContinuanceoption(ddl_ContinuanceOption);
                this.ddl_per.Attributes.Add("disabled", "true");
                SetPercentOption();
            }
            else
            {
                ddl_ContinuanceOption.Visible = false;
                lbl_ContinuanceOption.Visible = false;
                this.ddl_per.Attributes.Remove("disabled");
            }

        }
        //End Fahad (11-1-08)

        //Added By Zeeshan Ahmed To Send Email In A Separate Thread
        private void SendOpenServiceEmail(object Parameter)
        {

            ArrayList objArraList = (ArrayList)Parameter;

            bool isNewServiceTicket = Convert.ToBoolean(objArraList[0]);
            int CaseTypeId = Convert.ToInt32(objArraList[1]);



            //Modified By Zeeshan Ahmed On 1/22/2008
            if (isNewServiceTicket || ddl_per.SelectedValue == "100")
            {
                ServiceTicketType serviceTicketType = (isNewServiceTicket == true ? ServiceTicketType.Open : ServiceTicketType.Updated);
                //Modified by Ozair for Task 2710 on 01/26/2008 [added percentage]
                //Send Service Ticket Notification Email
                //Yasir Kamal 5759 04/09/2009 ShowTimeStamp on General Comments of openServiceTicket Email
                ServiceTicket.SendServiceTicketNotificationEmail(serviceTicketType, (ServiceTicketCategories)Convert.ToInt32(drpCategory.SelectedValue), Url, drpCategory.SelectedItem.Text, drpPriority.SelectedItem.Text, EmployeeName, WCC_GeneralComments.Label_Text, TicketID, ddl_assignto.SelectedItem.Text, ddl_per.SelectedItem.Text, "", CaseTypeId);
                //5759 end

                // ServiceTicket.SendServiceTicketNotificationEmail(serviceTicketType, (ServiceTicketCategories)Convert.ToInt32(drpCategory.SelectedValue), Url, drpCategory.SelectedItem.Text, drpPriority.SelectedItem.Text, EmployeeName, WCC_GeneralComments.Label_Text + WCC_GeneralComments.TextBox_Text, TicketID, ddl_assignto.SelectedItem.Text, ddl_per.SelectedItem.Text, "", CaseTypeId);
            }

            //Added By Khalid To Stop Multiple Mail 
            //Task ID 2595
            if (ViewState["Assignedto"] == null) ViewState["Assignedto"] = "0000";
            if (fid >= 0 && Convert.ToInt32(ViewState["Assignedto"]) != Convert.ToInt32(ddl_assignto.SelectedValue))
            {
                string email = ServiceTicket.GetEmail(Convert.ToInt32(ddl_assignto.SelectedValue));
                if (email.Length > 0)
                {
                    //Modified by Ozair for Task 2710 on 01/26/2008 [added percentage]
                    //Modified By Zeeshan Ahmed On 1/22/2008
                    //Send Service Ticket Notification Email
                    ServiceTicket.SendServiceTicketNotificationEmail(ServiceTicketType.Assigned, (ServiceTicketCategories)Convert.ToInt32(drpCategory.SelectedValue), Url, drpCategory.SelectedItem.Text, drpPriority.SelectedItem.Text, EmployeeName, WCC_GeneralComments.Label_Text, TicketID, ddl_assignto.SelectedItem.Text, ddl_per.SelectedItem.Text, email, CaseTypeId);
                }
            }

        }

        //Zahoor 4770 09/12/08
        /// <summary>
        /// This method populates the Option for service ticket category, Continuance
        /// </summary>
        /// <param name="ddl_continuanceoption"></param>
        private void FillContinuanceoption(DropDownList ddl_continuanceoption)
        {
            try
            {
                DataTable dt = ServiceTicket.GetServiceTicketContinueanceOptions();
                ddl_continuanceoption.DataSource = dt;
                ddl_continuanceoption.DataValueField = "OptionID";
                ddl_continuanceoption.DataTextField = "Description";
                ddl_continuanceoption.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

        }

        // Zahoor 4770 09/16/08 
        /// <summary>
        /// To Set the Percent Completion value in Continuance case.
        /// </summary>
        private void SetPercentOption()
        {
            if (ddl_ContinuanceOption.SelectedValue == "1")
            {
                ddl_per.SelectedValue = "0";
            }
            //else if (option == "Contract Sent" || option == "Contract Signed")
            else if (ddl_ContinuanceOption.SelectedValue == "2" || ddl_ContinuanceOption.SelectedValue == "3")
            {
                ddl_per.SelectedValue = "25";
            }
            else if (ddl_ContinuanceOption.SelectedValue == "4" || ddl_ContinuanceOption.SelectedValue == "5")
            {
                ddl_per.SelectedValue = "50";
            }
            else
            {
                ddl_per.SelectedValue = "100";
            }
        }

        /// <summary>
        /// Sabir Khan 5738 05/25/2009
        /// This method is used to get number of business days         
        ///</summary>        
        /// <param name="Businessdays">no of business days</param>
        /// <returns>Return the date based on the business days</returns>
        private DateTime GetBusinessDays(int Businessdays)
        {
            int dayscount = 0;
            DateTime returndate = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString());
            for (int i = 0; i <= Businessdays; i++)
            {
                if (returndate.AddDays(i).DayOfWeek == DayOfWeek.Saturday)
                    dayscount += 2;
                else
                    dayscount += 1;
            }
            returndate = DateTime.Now.AddDays(dayscount);
            return returndate;
        }

        #endregion

    }
}

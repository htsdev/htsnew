using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.Reports
{
    public partial class DocketReportCR : System.Web.UI.Page
    {
        // Fahad 2867 ( 2-09-08 )
        /// <summary>
        /// This page is just an interface to display Crystal Report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["courtid"]   = Convert.ToInt32(Request.QueryString["courtid"]);
            ViewState["type"]     = Convert.ToInt32(Request.QueryString["type"]);
            ViewState["datefrom"] = Convert.ToDateTime(Request.QueryString["datefrom"]);
            ViewState["dateto"]    = Convert.ToDateTime(Request.QueryString["dateto"]);
            ViewState["alldates"] = Convert.ToString(Request.QueryString["alldates"]);
            ViewState["status"]    = Convert.ToString(Request.QueryString["status"]);
            ViewState["coverfirm"]= Convert.ToInt32(Request.QueryString["coverfirm"]);
            ViewState["showbonds"] = Convert.ToString(Request.QueryString["showbonds"]);
           
        }
    }
}

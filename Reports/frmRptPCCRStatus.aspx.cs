using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient ;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
	/// <summary>
	/// Summary description for frmRptPCCRStatus.
	/// </summary>
	public partial class frmRptPCCRStatus : System.Web.UI.Page
	{
		DataView dv;
		DataSet ds;
		clsENationWebComponents ClsDb = new clsENationWebComponents();		
		clsLogger bugTracker = new clsLogger();
		int recCount ;


		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.DataGrid dgPCCR;
		protected System.Web.UI.WebControls.Label lblFromDate;
		protected System.Web.UI.WebControls.Label lblToDate;
		protected eWorld.UI.CalendarPopup dtTo;
		protected eWorld.UI.CalendarPopup dtFrom;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblPCCR;
		protected System.Web.UI.WebControls.ImageButton btnSubmit;
		protected System.Web.UI.WebControls.ImageButton btn_Reset;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.DropDownList ddlPCCR;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.CheckBox chkHiredHistory;
		protected System.Web.UI.WebControls.Label lblMessage;
        protected HtmlTableRow tr;
        
		clsSession ClsSession =new clsSession();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				//Calling procedure to populate grid with the data as per selection criteria...
				if (!IsPostBack)
				{

					// filling PCCR Statuses in list for selection criteria......
					FillPCCR();
				
					// filling user names in list for selection criteria......
					FillUsers();

					// getting records...
					SearchRecords();
					btnSubmit.Attributes.Add("OnClick","return CalendarValidation();");
					btnSubmit.Attributes.Add("OnClick", "return validate();");


				
				}
			}
		}

		private void FillPageList()
		{
			// Procedure for filling page numbers in page number combo........
			Int16 idx;

			if (dgPCCR.Items.Count == 0 )
			{
				cmbPageNo.Items.Clear();
				cmbPageNo.Items.Add ("--" );
				cmbPageNo.Items[0].Value= "--";
				lblPNo.Text = "";
				return;
			}

			cmbPageNo.Items.Clear();
			for (idx=1; idx<=dgPCCR.PageCount;idx++)
			{
				cmbPageNo.Items.Add ("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value=idx.ToString();
			}
		}

		private void SetNavigation()
		{
			// Procedure for setting data grid's current  page number on header ........
			
			try
			{
				//pgSize=dgClient.PageSize ;
				//pgCounter=dgClient.PageCount;
				
				if (dgPCCR.Items.Count == 0 )
				{
					lblCurrPage.Visible = false;
					lblPNo.Visible = false;
					lblGoto.Visible = false;
					cmbPageNo.Visible = false;
				}
				else
				{
				
					// setting current page number
					lblPNo.Text =Convert.ToString(dgPCCR.CurrentPageIndex+1);
					lblCurrPage.Visible=true;
					lblPNo.Visible=true;

					// filling combo with page numbers
					lblGoto.Visible=true;
					cmbPageNo.Visible=true;
					//FillPageList();

					for (int idx =0 ; idx < cmbPageNo.Items.Count ;idx++)
					{
						cmbPageNo.Items[idx].Selected = false;
					}
					cmbPageNo.Items[dgPCCR.CurrentPageIndex].Selected=true;
				}
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
		}


		private void FillGrid()
		{
			dgPCCR.DataSource = PullData();

			
			try
			{
				dgPCCR.DataBind();
			}
			catch
			{	// setting current page to first page......
				if (dgPCCR.CurrentPageIndex > dgPCCR.PageCount -1)
				{
					dgPCCR.CurrentPageIndex=dgPCCR.PageCount -1;
					dgPCCR.DataBind();
					dgPCCR.SelectedIndex = dgPCCR.Items.Count -1; 
				}

			}

			
		}
		private void GenerateSerial()
		{
			// Procedure for writing serial numbers in "S.No." column in data grid.......	

			
			long sNo=(dgPCCR.CurrentPageIndex)*(dgPCCR.PageSize);									
			try
			{
				// looping for each row of record.............
				foreach (DataGridItem ItemX in dgPCCR.Items) 
				{ 					
					sNo +=1 ;
					// setting text of hyperlink to serial number after bouncing of hyperlink...
					((Label)(ItemX.FindControl("lblSerial"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
				 
				}
			}
			catch(Exception ex)

			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		private void FillPCCR()
		{
            //Change by Ajmal
            //SqlDataReader drPCCR;
			IDataReader drPCCR;
			int idx=1;

			drPCCR = ClsDb.Get_DR_BySP("usp_MID_Get_PCCRStatus");

			while(drPCCR.Read()) 
			{
				ddlPCCR.Items.Add(drPCCR.GetString(1).ToUpper ());
				ddlPCCR.Items[idx].Value=drPCCR.GetString(0);
				idx+=1;
			}
			drPCCR.Close();
		}

		private void FillUsers()
		{
            //Change By Ajmal
            //SqlDataReader drUsers;
			IDataReader drUsers;
			int idx=1;
			
			drUsers = ClsDb.Get_DR_BySP("usp_PCCR_Get_UserInfo");
			while (drUsers.Read())
			{
				ddlUser.Items.Add(drUsers.GetString(1).ToUpper ());
				ddlUser.Items[idx].Value= drUsers.GetString(0);
				idx+=1;
			}

			drUsers.Close();
		}

		private DataView PullData()
		{
			// Procedure for fetching data from the database.......
			

			// Declaring Variables for inpurt parameters for stored procedures........
			string	User;
			string	PCCRType;
			bool	IsShowHiredHistory;
			
			// Setting variables for input parameters for stored procedures.....
			User=ddlUser.SelectedValue;
			PCCRType = ddlPCCR.SelectedValue;
			if (chkHiredHistory.Checked == true )
				IsShowHiredHistory = true;
			else
				IsShowHiredHistory =false;

 
			try
			{
                // Nasir 5019 11/19/2008 for checking and calling fill gridview 

                if (chkPCCRSumary.Checked == true)
                    FillGrdSum();
                else
                    GrdPCCRStaSumm.Visible = false;



               


                // Assigning stored procedure's parameter names in an array......
				string[] key    = {"@FromDate","@ToDate","@User","@PCCR", "@IsShowHiredHistory" };

				// Assigning stored procedure's parameter values in an array......
				object[] value1 = {dtFrom.SelectedDate ,dtTo.SelectedDate ,User ,PCCRType, IsShowHiredHistory};
				
				// Calling wrapper class function for fetching in data set.........
				ds = ClsDb.Get_DS_BySPArr("usp_PCCR_Get_Info",key,value1);	
				dv = new DataView(ds.Tables[0]) ;
				Session["dv"] = dv;

				lblMessage.Text="";

				// saving record count in a global variable....
				recCount=ds.Tables[0].Rows.Count;
				Session["intRecordCount"] = recCount.ToString();

			

				if (recCount  < 1 )
				{
					lblMessage.Text = "No record found";
				}
			}
			
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			return dv;
		}
        
        
        /// <summary>
        /// Nasir 5019 11/19/2008
        /// //Fill the PCCR Status Summary GridView
        /// </summary>
        private void FillGrdSum()
        {
            DataSet ds;

            String[] key = { "@sdate", "@edate" };

            Object[] value1 = { dtFrom.SelectedDate, dtTo.SelectedDate };

            ds = ClsDb.Get_DS_BySPArr("USP_HTP_Get_PCCRStatusSummary", key, value1);

            GrdPCCRStaSumm.DataSource = ds.Tables[0];

            GrdPCCRStaSumm.DataBind();

            GrdPCCRStaSumm.Visible = true;

            //tr = FindControl("tr_SumHead") as HtmlTableRow;
                //tr.Style="";
            //Response.Write("<script language='javascript'>GrdviewPCCRSumdisplay()</script>");

        }
        /// <summary>
        /// Nasir 5019 11/24/2008
        /// //Add total to GridView footer
        /// </summary>
        private decimal counttot;
        protected void GrdPCCRStaSumm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                counttot+=Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem,"Counts"));

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text="Total";
                e.Row.Cells[0].Font.Bold=true;
                e.Row.Cells[1].Text=counttot.ToString();
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                e.Row.Cells[1].Font.Bold = true;
            }

        }
		private void GotoPage(int intNewIndex)
		{
			// Procedure for handling page navigation in data grid......

			// setting current page to new page index in data grid....
			dgPCCR.CurrentPageIndex = intNewIndex;
			
			// Filling & binding data for new page index with data grid......
			SearchRecords();

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.btnSubmit_Click);
			this.btn_Reset.Click += new System.Web.UI.ImageClickEventHandler(this.btn_Reset_Click);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
			this.dgPCCR.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPCCR_ItemCommand);
			this.dgPCCR.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgPCCR_PageIndexChanged);
			this.dgPCCR.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgPCCR_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			// validating the user inputs...
			if (DoValidation())
			{
				// calling procedure for populating the records...
				dgPCCR.CurrentPageIndex = 0;
				SearchRecords();
			}
		}

		private bool DoValidation()
		{
			if (dtTo.SelectedDate<dtFrom.SelectedDate)
			{
				lblMessage.Text="From Date should be less than To Date.";
				CreateBlankDataSet();
				return false;
			}
			return true;
		}

		private void SearchRecords()
		{
			// procedure for filling grid with data...
			FillGrid();
			
			// procedure for filling serial number in grid....
			GenerateSerial();

			// filling page numbers in combo for page navigation...
			FillPageList();

			// setting page combo value to the grid's selected page number....
			SetNavigation();

		}

		private void dgPCCR_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			// procedure for changing page in grid...
			GotoPage(e.NewPageIndex );
		}

		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Procedure for setting the grid page number as per selected from page no. combo.......
			dgPCCR.CurrentPageIndex= cmbPageNo.SelectedIndex ;
			SearchRecords();
		}

		private void dgPCCR_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
/*
			if (e.CommandName == "cmdCaseNo")
			{
				string strCaseNo = ((LinkButton)(e.Item.FindControl("btnCaseNo"))).Text ;
				Response.Redirect( "../../ClientInfo/violationsfees.asp?search=999&caseNumber=" + strCaseNo,false ) ;
			}
*/
		}

		private void dgPCCR_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
/*			if (e.Item.ItemType  != ListItemType.Header && e.Item.ItemType != ListItemType.Footer )
			{
				if (((LinkButton ) (e.Item.FindControl("btnCaseNo"))).Text.ToUpper().Substring(0,1) == "F")
				{
					((LinkButton ) (e.Item.FindControl("btnCaseNo"))).Enabled = false;
				}
			}
*/
		}

		private void btn_Reset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			//Procedure for reseting all controls on the page...
			ResetAll();

		}

		private void ResetAll()
		{
			lblMessage.Text ="";
			ddlUser.SelectedIndex =0;
			ddlPCCR.SelectedIndex =0;
			dtFrom.SelectedDate = DateTime.Now.Date;
			dtTo.SelectedDate = DateTime.Now.Date;

			// Procedure for creating a blank dummy data set....
			CreateBlankDataSet();

            //Nasir 5265 12/01/2008 on reset still display summary

            chkPCCRSumary.Checked = false;
            GrdPCCRStaSumm.Visible = false;


		}

		private void CreateBlankDataSet()
		{
			ds = new DataSet() ;
			ds.Tables.Add("temp");
			ds.Tables[0].Columns.Add("casenumber");
			ds.Tables[0].Columns.Add("username");
			ds.Tables[0].Columns.Add("insertdate");
			ds.Tables[0].Columns.Add("pccrstatus");
			ds.Tables[0].Columns.Add("comments");

			dgPCCR.CurrentPageIndex = 0;
			dgPCCR.SelectedIndex = 0 ;
			dgPCCR.DataSource = ds;
			dgPCCR.DataBind();
			cmbPageNo.Visible = false;
			lblGoto.Visible =false;
			lblCurrPage.Visible =false;
			lblPNo.Visible =false;

			
		}

      
      
       

       
	}
}


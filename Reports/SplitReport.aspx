﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.SplitReport"
    MaintainScrollPositionOnPostback="true" CodeBehind="SplitReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Split Report</title>
    <%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />



    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script language="javascript" type="text/javascript">
	      function OpenEditWin(violationid)
	      {
              var PDFWin
		      PDFWin = window.open("FrmComments.aspx?ticketID="+violationid,"","fullscreen=no,toolbar=no,width=380,height=200,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	  
    </script>

    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
</head>

<body class=" ">
    <form id="Form1" method="post" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
        
            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                <div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <!-- MAIN CONTENT AREA STARTS -->    
                            <div class="actions panel_actions pull-right">
                                <asp:CheckBox ID="chkShowAllUserRecords" runat="server" Text="Show All " CssClass="form-label"
                                                        OnCheckedChanged="chkShowAllUserRecords_CheckedChanged" AutoPostBack="true" />
                            </div>
                            <div class="col-lg-12">
                                <section class="box ">
                                    <header class="panel_header">
                                        <h2 class="title pull-left">Split Reports</h2>
                                        <div class="actions panel_actions pull-right">
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </div>
                                    </header>
                                    <div class="content-body">
                                        <div class="row">

                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>

                                            <asp:Label ID="lbl_message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                    
                                            <div class="col-xs-12">

                                                <asp:GridView ID="gv_SplitReport" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                    CssClass="table" OnPageIndexChanging="gv_SplitReport_PageIndexChanging"
                                                    OnRowCommand="gv_SplitReport_RowCommand" OnRowDataBound="gv_SplitReport_RowDataBound"
                                                    PageSize="20">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <ItemTemplate>
                                                                &nbsp;<asp:HyperLink ID="HLTicketno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ticket No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_RefCaseNo" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumberseq") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Causeno" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.casenumassignedbycourt") %>' />
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="X-ref number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Xrefnumber" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Midnum") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Verified Court Info">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_verifieddate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedCourtInfo") %>'>
                                                                </asp:Label>
                                                                <asp:Label ID="lbl_violationid" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId_pk") %>'
                                                                    Visible="false"> </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Court Location">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_courtname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'> </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Follow-Up Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_followupdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="img_Add" runat="server" CommandName="btnclick" Text="&lt;img src='../Images/add.gif' border='0'/&gt;" />
                                                                <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("Firstname") %>' />
                                                                <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("Lastname") %>' />
                                                                <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumberseq") %>' />
                                                                <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("casenumassignedbycourt") %>' />
                                                                <asp:HiddenField ID="hf_court_loc" runat="server" Value='<%#Eval("shortname") %>' />
                                                                <asp:HiddenField ID="hf_FollowUpDate" runat="server" Value='<%#Eval("FollowUpDate") %>' />
                                                                <asp:HiddenField ID="hf_NextFollowUpdate" runat="server" Value='<%#Eval("NextFollowUpDate") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgRemove" runat="server" CommandName="btnRemove" ImageUrl="../Images/cross.gif"
                                                                    ToolTip="Mark as No Split" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                        Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                                </asp:GridView>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div class="col-lg-12">
                                <section class="box ">
                                    <div class="content-body">
                                        <div class="row">

                                            <div class="col-xs-12">

                                                 <asp:Panel ID="pnlFollowup" runat="server">
                                                        <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="FollowUp Date" />
                                                    </asp:Panel>
                                                    <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                        PopupControlID="pnlFollowup" TargetControlID="btn">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <asp:Button ID="btn" runat="server" Style="display: none;" />

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <!-- MAIN CONTENT AREA ENDS -->

                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </section>
                </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 
</body>































<%--<body>
    <form id="Form1" method="post" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="800"
                    align="center" border="0">
                    <tbody>
                        <tr>
                            <td style="height: 14px" colspan="4">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableSub" cellspacing="0" cellpadding="0" width="800" border="0">
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td align="right">
                                                        <asp:CheckBox ID="chkShowAllUserRecords" runat="server" Text="Show All " CssClass="clssubhead"
                                                            OnCheckedChanged="chkShowAllUserRecords_CheckedChanged" AutoPostBack="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <td align="center" style="width: 100%">
                                            <asp:Label ID="lbl_message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <asp:GridView ID="gv_SplitReport" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                CssClass="clsLeftPaddingTable" OnPageIndexChanging="gv_SplitReport_PageIndexChanging"
                                                OnRowCommand="gv_SplitReport_RowCommand" OnRowDataBound="gv_SplitReport_RowDataBound"
                                                PageSize="20" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            &nbsp;<asp:HyperLink ID="HLTicketno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                            <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ticket No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_RefCaseNo" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumberseq") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cause Number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Causeno" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.casenumassignedbycourt") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="X-ref number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Xrefnumber" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Midnum") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Verified Court Info">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_verifieddate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedCourtInfo") %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_violationid" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId_pk") %>'
                                                                Visible="false"> </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Court Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_courtname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Follow-Up Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_followupdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="img_Add" runat="server" CommandName="btnclick" Text="&lt;img src='../Images/add.gif' border='0'/&gt;" />
                                                            <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("Firstname") %>' />
                                                            <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("Lastname") %>' />
                                                            <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumberseq") %>' />
                                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("casenumassignedbycourt") %>' />
                                                            <asp:HiddenField ID="hf_court_loc" runat="server" Value='<%#Eval("shortname") %>' />
                                                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" Value='<%#Eval("FollowUpDate") %>' />
                                                            <asp:HiddenField ID="hf_NextFollowUpdate" runat="server" Value='<%#Eval("NextFollowUpDate") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle Font-Bold="true" ForeColor="black" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgRemove" runat="server" CommandName="btnRemove" ImageUrl="../Images/cross.gif"
                                                                ToolTip="Mark as No Split" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                    Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" width="780">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pnlFollowup" runat="server">
                                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="FollowUp Date" />
                                            </asp:Panel>
                                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                PopupControlID="pnlFollowup" TargetControlID="btn">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc1:Footer ID="Footer1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>--%>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class InternetSignup : System.Web.UI.Page
    {

        #region Variables

        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        int pageindex = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    datefromnew.SelectedDate = DateTime.Now;
                    datetonew.SelectedDate = DateTime.Now;
                    GetInteretSignUpRecords("");
                }
                // Noufil 5524 03/16/2009 Paging control added
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hl_sno = (HyperLink)e.Row.FindControl("hl_sno");
                hl_sno.Text = (e.Row.RowIndex + 1 + pageindex).ToString();

                LinkButton lbtn_lname = (LinkButton)e.Row.FindControl("lbtn_lname");
                lbtn_lname.OnClientClick = "window.open('" + lbtn_lname.CommandArgument + "','','');return false;";

            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            pageindex = e.NewPageIndex * gv_records.PageSize;
            GetInteretSignUpRecords("");
        }

        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            GetInteretSignUpRecords(e.SortExpression);
        }

        protected void ddlPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //gv_records.PageIndex = ddlPageNo.SelectedIndex;
            //pageindex = ddlPageNo.SelectedIndex * gv_records.PageSize;
            //GetInteretSignUpRecords("");
        }


        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                gv_records.PageIndex = 0;
                GetInteretSignUpRecords("");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        public void GetInteretSignUpRecords(string SortExpression)
        {

            //Zeeshan Ahmed 3931 06/10/2008 Change Internet Signup Procedure
            string[] keys = { "@StartDate", "@EndDate", "@AllDates", "@ContactType" };
            object[] values = { datefromnew.SelectedDate, datetonew.SelectedDate, cb_alldates.Checked, ddlContactType.SelectedValue };

            DataSet records = clsDB.Get_DS_BySPArr("USP_HTS_Internet_Signup_Report", keys, values);

            if (records.Tables[0].Rows.Count > 0)
            {

                if (SortExpression != "")
                {
                    if (Session[SortExpression] != null)
                    {
                        if (Convert.ToString(Session[SortExpression]) == "ASC")
                            Session[SortExpression] = "DESC";
                        else
                            Session[SortExpression] = "ASC";
                    }
                    else
                        Session[SortExpression] = "ASC";

                    DataView dv = new DataView(records.Tables[0]);
                    dv.Sort = SortExpression + " " + Session[SortExpression];
                    gv_records.DataSource = dv;
                    gv_records.DataBind();
                    Pagingctrl.PageCount = gv_records.PageCount;
                    Pagingctrl.PageIndex = gv_records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    gv_records.DataSource = records.Tables[0];
                    gv_records.DataBind();
                    Pagingctrl.PageCount = gv_records.PageCount;
                    Pagingctrl.PageIndex = gv_records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }

            //    FillPageList();
            //    //lblCurrPage.Text = (gv_records.PageIndex + 1).ToString();
            //    //ddlPageNo.SelectedIndex = gv_records.PageIndex;
            //    //Sabir Khan 4635 09/26/2008
            //    if (gv_records.PageCount  == 1)
            //    {
            //        //tblPageNavigation.Visible = false;
            //        img_pagging.Visible = false;
            //    }
            //    else
            //    {
            //        //tblPageNavigation.Visible = true;
            //        img_pagging.Visible = true;    
            //    }
            //    gv_records.Visible = true;
                
            //    lblMessage.Text = "";
            //}
            //else
            //{
            //    lblMessage.Text = "No Record Found.";
            //    gv_records.Visible = false;
            //    tblPageNavigation.Visible = false;
            //    img_pagging.Visible = false;
            //}

        }

        private void FillPageList()
        {
            try
            {
                //int i = 0;
                //ddlPageNo.Items.Clear();
                //for (i = 1; i <= gv_records.PageCount; i++)
                //{
                //    ddlPageNo.Items.Add("Page - " + i.ToString());
                //    ddlPageNo.Items[i - 1].Value = i.ToString();
                //}
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            GetInteretSignUpRecords("");
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            GetInteretSignUpRecords("");
        }

        #endregion





    }
}

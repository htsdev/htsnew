using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    /// <summary>
    /// WaitingFollowUpFamily class file.
    /// </summary>
    public partial class WaitingFollowUpFamily : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 7791 07/24/2010  grouping variables
        #region Variables

        readonly clsSession _clsSession = new clsSession();
        readonly clsCase _cCase = new clsCase();
        readonly clsLogger _bugTracker = new clsLogger();
        DataSet _ds;
        readonly ValidationReports _reports = new ValidationReports();

        #endregion

        // Fahad Muhammad Qureshi 7791 07/24/2010 grouping Report Properties
        #region Properties

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }
        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        private bool ShowAllRecords
        {
            get
            {
                if (ViewState["ShowAllRecords"] == null)
                    ViewState["ShowAllRecords"] = false;

                return Convert.ToBoolean(ViewState["ShowAllRecords"]);
            }
            set
            {
                ViewState["ShowAllRecords"] = value;
            }
        }
        private bool ShowPastRecords
        {
            get
            {
                if (ViewState["ShowPastRecords"] == null)
                    ViewState["ShowPastRecords"] = true;

                return Convert.ToBoolean(ViewState["ShowPastRecords"]);
            }
            set
            {
                ViewState["ShowPastRecords"] = value;
            }
        }
        private DateTime FromDate
        {
            get
            {
                if (ViewState["FromDate"] == null)
                    ViewState["FromDate"] = new DateTime(1900, 1, 1);

                return Convert.ToDateTime(ViewState["FromDate"]);
            }
            set
            {
                ViewState["FromDate"] = value;
            }
        }
        private DateTime ToDate
        {
            get
            {
                if (ViewState["ToDate"] == null)
                    ViewState["ToDate"] = DateTime.Now;

                return Convert.ToDateTime(ViewState["ToDate"]);
            }
            set
            {
                ViewState["ToDate"] = value;
            }
        }

        #endregion

        // Fahad Muhammad Qureshi 7791 07/24/2010 grouping Event Handlers
        #region Event Handler

        /// <summary>
        /// Page_Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    lbl_Message.Text = string.Empty;

                    if (!IsPostBack)
                    {
                        chkFamilyShowAll.Checked = false;
                        ShowAllRecords = false;
                        chkFamilyShowPast.Checked = true;
                        ShowPastRecords = true;
                        cal_FromDateFilter.SelectedDate = DateTime.Now;
                        FromDate = DateTime.Now;
                        cal_ToDateFilter.SelectedDate = DateTime.Now;
                        ToDate = DateTime.Now;
                        gv_Records.Visible = false;
                    }
                    UpdateFollowUpInfo2.PageMethod += UpdateFollowUpInfo2_PageMethod;
                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_Records;
                    const string javascript = "EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});";
                    chkFamilyShowAll.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 2));
                    chkFamilyShowPast.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 3));

                    if (chkFamilyShowAll.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 2));
                    }
                    else if (chkFamilyShowPast.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 3));
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// RowDataBound event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    var desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    var followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Gridview RowCommand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {
                    var rowId = Convert.ToInt32(e.CommandArgument);
                    var firstname = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_fname")).Value);
                    var causeno = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_causeno")).Value);
                    var ticketno = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketnumber")).Value);
                    var lastname = (((HiddenField)gv_Records.Rows[rowId].FindControl("hf_lname")).Value);
                    var followupDate = (((Label)gv_Records.Rows[rowId].FindControl("lbl_followup")).Text);
                    followupDate = (followupDate.Trim() == "") ? "1/1/1900" : followupDate;
                    _cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value));
                    var comm = _cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Family Waiting Follow Up Date";
                    UpdateFollowUpInfo2.followUpType = Components.FollowUpType.FamilyWaitingFollowUpDate;
                    // Noufil 8099 08/04/2010 Use overloaded method to get court name in javascript message                    
                    //UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Family Waiting", Convert.ToInt32((((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value)));
                    //UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Family Waiting", ((Label)gv_Records.Rows[rowId].FindControl("lbl_LOC")).Text, followupDate);
                    //Saeed 7791 08/11/2010 call overloaded 'binddate' method to display default follow up date after 1 week.
                    UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "Family Waiting", Convert.ToInt32((((HiddenField)gv_Records.Rows[rowId].FindControl("hf_ticketno")).Value)));
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// btnFamilySubmit_Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ShowAllRecords = chkFamilyShowAll.Checked;
                ShowPastRecords = chkFamilyShowPast.Checked;
                FromDate = cal_FromDateFilter.SelectedDate;
                ToDate = cal_ToDateFilter.SelectedDate;
                gv_Records.PageIndex = 0;
                GridViewSortDirection = SortDirection.Descending;
                GridViewSortExpression = "TrafficWaitingFollowUp";
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        /// <summary>
        /// Gridview Sorting event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        // Fahad Muhammad Qureshi 7791 07/24/2010 grouping Methods
        #region Methods

        /// <summary>
        /// This method used to fetch the data which will display on the Gridview.
        /// </summary>
        private void FillGrid()
        {
            gv_Records.Visible = true;
            string[] keys = { "@ShowAllRecords", "@ShowAllPastRecords", "@FromFollowUpDate", "@ToFollowUpDate" };
            object[] values = { ShowAllRecords, ShowPastRecords, FromDate, ToDate };
            _reports.getRecords("usp_HTP_Get_FamilyWaitingFollowUpReport", keys, values);
            _ds = _reports.records;
            if (_ds.Tables[0].Rows.Count > 0)
            {
                _ds.Tables[0].Columns.Add("sno");
                var dv = _ds.Tables[0].DefaultView;
                dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                var dt = dv.ToTable();
                GenerateSerialNo(dt); ;
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = dt;
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "No Records Found";
                gv_Records.Visible = false;
            }

        }

        /// <summary>
        /// This method used to generate the Serial Number to the given DataTable.
        /// </summary>
        /// <param name="dtRecords">DataTable on which serial number will generate.</param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            var sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (var i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        /// <summary>
        /// This method is supporting method to Update Follow Up information.
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();

        }

        #endregion

    }
}

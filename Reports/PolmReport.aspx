﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolmReport.aspx.cs" Inherits="HTP.Reports.PolmReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POLM Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <table width="900px">
                    <tr>
                        <td style="height: 118px">
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="clsLeftPaddingTable">
                            <table>
                                <tr>
                                    <td>
                                        <span class="clssubhead">Start Date :</span>&nbsp;
                                    </td>
                                    <td>
                                        <ew:CalendarPopup ID="calstartdate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                            ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                            CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                            Width="99px" Font-Size="8pt" Font-Names="Tahoma">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <span class="clssubhead">End Date :</span>&nbsp;
                                    </td>
                                    <td>
                                        <ew:CalendarPopup ID="calenddate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
                                            ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
                                            CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
                                            Width="99px" Font-Size="8pt" Font-Names="Tahoma">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <span class="clssubhead">Case Status :</span>&nbsp;
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddCaseStatus" CssClass="clsInputCombo" runat="server" DataTextField="CaseStatus"
                                            DataValueField="CaseStatusID">
                                        </asp:DropDownList>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <span class="clssubhead">Attorney :</span>&nbsp;
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddAttorney" runat="server" CssClass="clsInputCombo" DataTextField="NAME"
                                            DataValueField="EmployeeID">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="right">
                                        <asp:CheckBox ID="chkShowAll" runat="server" Text="Show All" CssClass="clsLeftPaddingTable" />&nbsp;
                                        <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <span class="clssubhead">Polm Report</span>
                                    </td>
                                    <td align="right" style="text-align: right;" valign="middle">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                        CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="gvPolm" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" AllowSorting="True"
                                CellPadding="0" CellSpacing="0">
                                <Columns>
                                    <asp:TemplateField HeaderText="<u>S.No</u>" SortExpression="sno">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                Text='<%# Eval("sno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Opened</u>" SortExpression="FiledDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CaseNumber" runat="server" Text='<%# Eval("FiledDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="NAME">
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" Text='<%# Eval("NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="CaseStatus">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" Text='<%# Eval("CaseStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Value</u>" SortExpression="Damagesvalue">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# Eval("Damagesvalue") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Fact</u>" SortExpression="QuickLegalMatterDescription">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_courtnumber" runat="server" Text='<%# Eval("QuickLegalMatterDescription") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Last Contact</u>" SortExpression="LastCaseStatusUpdateDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtNum" runat="server" Text='<%# Eval("LastCaseStatusUpdateDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="Followupdate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("Followupdate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Assignment</u>" SortExpression="AssignedAttorney">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("AssignedAttorney","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Date Assign</u>" SortExpression="AssignedDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("AssignedDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Date Closed</u>" SortExpression="ClosedDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("ClosedDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Referring Sullo</u>" SortExpression="ReferringAttorneyID">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("ReferringAttorneyID","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

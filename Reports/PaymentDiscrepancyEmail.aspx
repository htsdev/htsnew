<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentDiscrepancyEmail.aspx.cs" Inherits="lntechNew.Reports.PaymentDiscrepancyEmail" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payment Discrepancy Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" width="780" cellpadding="0" cellspacing="0" align="center">
            
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                       <tr>
                       <td align="center">
                           
                           <asp:Label ID="lbl" runat="server" Text="Payment Discrepancy Report" Font-Bold="True" Font-Names="Arial" Font-Size="X-Large" ForeColor="RoyalBlue"></asp:Label>
                       </td>
                       </tr>
                       <tr>
                        <td height="5px">
                        </td>
                       </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" height="5px">
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                              <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                              
                              <tr>
                                <td>
                                    <asp:GridView ID="gv" runat="server" Width="100%" AutoGenerateColumns="False"  BackColor="#EFF4FB" BorderColor="White" CssClass="clsLeftPaddingTable" OnRowDataBound="gv_RowDataBound"  >
                                        <Columns>
                                            <asp:TemplateField HeaderText="S.NO">
                                            <HeaderStyle CssClass="clsaspcolumnheader"   HorizontalAlign="Left" />
                                                    <ItemStyle VerticalAlign="Top"  />       
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNO" runat="server" CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Ticket Number">
                                                <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top"
                                                         HorizontalAlign="Left"        />
                                                 <ItemStyle VerticalAlign="Top"  />                 
                                                <ItemTemplate>
                                                    <asp:Label ID="lblticketno" runat="server" CssClass="label" Text='<%# bind("TicketNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="First Name">
                                             <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                  <ItemStyle VerticalAlign="Top"  />                
                                                <ItemTemplate>
                                           
                                                    <asp:Label ID="lblfirstname" CssClass="label" runat="server" Text='<%# bind("firstname") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name">
                                             <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                                 <ItemStyle VerticalAlign="Top"  /> 
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllastname" CssClass="label" runat="server" Text='<%# bind("lastname") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Phone Number">
                                            <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                  <ItemStyle VerticalAlign="Top"  />                
                                                <ItemTemplate>
                                                  <table id="tbl_contacts" border="0" cellpadding="0" cellspacing="0" 
                                                                    width="100%">
                                                                    <tr>
                                                                    <td>
                                                    <asp:Label ID="lblContact1" CssClass="label" runat="server" Text='<%# bind("contact1") %>'></asp:Label>
                                                    </td></tr>
                                                    <tr><td>
                                                    <asp:Label ID="lblcontact2"  CssClass="label" runat="server" Text='<%# bind("contact2") %>' ></asp:Label>
                                                    </td></tr>
                                                    <tr><td>
                                                    <asp:Label ID="lblcontact3" CssClass="label" runat="server" Text='<%# bind("contact3") %>'></asp:Label>
                                                    </td></tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Transaction Date">
                                             <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                                 <ItemStyle VerticalAlign="Top"  /> 
                                                <ItemTemplate>
                                                    <asp:Label ID="transDate" CssClass="label" runat="server" Text='<%# bind("TransactionDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblticketid" runat="server" Text='<%# bind("ticketid_pk") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        
                                       
                                        
                                        
                                    </asp:GridView>
                                </td>
                              </tr>
                              </table>
                            </td>
                        </tr>
                    </table>
                </td>
                
            </tr>
           
                            
        </table>
    </div>
    </form>
</body>
</html>

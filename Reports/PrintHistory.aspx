<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintHistory.aspx.cs" Inherits="HTP.Reports.PrintHistory" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<%@ Register Assembly="Microsoft.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="Microsoft.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print History</title>
    
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
     <script language="javascript">
           function PopUpSummaryPreview(filename)
			{
				
				window.open ("PreviewPrintHistory.aspx?filename="+ filename,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
				return false;
			}
			</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tr><td >
			    <uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
			</td></tr>
			<TR>
								<TD background="../../images/separator_repeat.gif" Height="11" ></TD>
							</TR>
				<TR>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" height="34">
					    <table border="0" cellpadding="0" cellspacing="0" width="100%">
					    <tr>
					    <td class="clssubhead" style="height: 13px">
					&nbsp;Arraignment Summary Report History&nbsp;</td>
					<td class="clssubhead" align="right" style="height: 13px">
                        <asp:HyperLink ID="HPBack" runat="server" NavigateUrl="~/Reports/AppearanceSummary.aspx">Back</asp:HyperLink>&nbsp;
					</td>
					</tr>
					</table>
					
					</TD>
					
				</TR>
				<tr>
					<td align="center">
						<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
				</Tr>
				<tr><td>
				    <table border="0" width="100%" cellpadding="0" cellspacing="0">
				    <tr>
				    
                    <td width="5%">From</td>
                    <td width="15%">
                        <ew:CalendarPopup ID="cal_todate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                            ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                            ToolTip="Select Court Date Range" UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                        </ew:CalendarPopup>
                            </td>
                            <td width="5%">To</td>
                            <td width="20%">
                                <ew:CalendarPopup ID="cal_fromDate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Court Date Range" UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsButton" OnClick="btnSearch_Click"
                                    Text="Search" /></td>
                    </tr>
                    </table>
                    </td>
                    
                    </tr>
				<TR>
					<TD vAlign="top" colSpan="2">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td >
									<asp:DataGrid id="dgPrintHistory" runat="server" AutoGenerateColumns="False" Width="100%" OnItemDataBound="dg_Viewbug_ItemDataBound">
										<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn SortExpression="bug_id" HeaderText="id" Visible="False">
											<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblPID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.batchid") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="shortdescription" HeaderText="Print Date">
											<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblPrintDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.printdate") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="TicketNumber" HeaderText="Report Type">
											<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbltype" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.PrintType") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="username" HeaderText="UserName">
											    <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblusername" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
                                                    &nbsp;<asp:HyperLink ID="HPView" runat="server" NavigateUrl="#">View</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
									</asp:DataGrid>
								</td>
							</tr>
							<tr>
									<td background="../../images/separator_repeat.gif" colSpan="5" height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
    </div>
    </form>
</body>
</html>

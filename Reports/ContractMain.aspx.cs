using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Windows.Forms;




namespace HTP.Reports
{
	/// <summary>
    /// Summary description for TrialNotificationDL.
	/// </summary>
	public partial class ContractMain : System.Web.UI.Page
	{		
		clsCrsytalComponent Cr = new clsCrsytalComponent();				
		clsSession uSession = new clsSession();
		clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsENationWebComponents ClsDb = new clsENationWebComponents(); 
		
		int TicketIDList;
        int LetterType;
		int empid;

        // Waqas 5630 03/09/2009 properties added
        int violationCategoryID = 0;
        int casetypeid = 0;
        int categoryid = 0;
        string filename = string.Empty;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
                // get ticket id and report to display
				TicketIDList = Convert.ToInt32(Request.QueryString["casenumber"]);
                LetterType = Convert.ToInt32(Request.QueryString["lettertype"]); ;
				ViewState["vTicketId"]=TicketIDList;
                ViewState["vLetterType"] = LetterType;
                
                // get emp id
				empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));									
                
                
                if (!IsPostBack)
                {
                    // Waqas 5630 03/11/2009 Updated Labels as in DB
                    switch (LetterType)
                    {
                        case 18:
                                lblTitle.Text = "AG Contract";
                                break;
                        case 19:
                                lblTitle.Text = "ALR Contract";
                                break;
                        case 20:
                                lblTitle.Text = "DWI Contract";
                                break;
                        case 21:
                                lblTitle.Text = "DL Suspension Contract";
                                break;
                        case 22:
                                lblTitle.Text = "OccupLic Contract";
                                break;
                        case 23:
                                lblTitle.Text = "Pre-Trial Contract";
                                break;
                        case 24:
                                lblTitle.Text = "Trial Contract";
                                break; 
                        case 25:
                                lblTitle.Text = "Trial Contract";
                                break; 

                    }

                                      
                }
			}
			catch(Exception ex)
			{
				
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   			
			
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

        // Waqas 5630 03/09/2009 Event region added.
        #region Events
        /// <summary>
        /// Waqas 5630 03/09/2009
        /// Exporting Word report.
        /// </summary>
        protected void IBtnExportWord_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CreateWordReport();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion

        // Waqas 5630 03/09/2009 Methods regin added.
        #region Methods
        /// <summary>
        /// Waqas 5630 03/09/2009
        /// Generating Word report.
        /// </summary>
        public void CreateWordReport()
        {
            try
            {
                switch (LetterType)
                {
                    case 18:
                        casetypeid = 4;
                        filename = Server.MapPath("") + "\\AG_Case_Contract.rpt";
                        break;
                    case 19:
                        violationCategoryID = 28;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_ALR.rpt";
                        break;
                    case 20:
                        violationCategoryID = 27;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_DWI.rpt";
                        break;
                    case 21:
                        violationCategoryID = 30;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_DLSuspension_Hearing.rpt";
                        break;
                    case 22:
                        violationCategoryID = 29;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_OccupLicense.rpt";
                        break;
                    case 23:
                        categoryid = 3;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_Pretrial.rpt";
                        break;
                    case 24:
                        categoryid = 4;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_Trial.rpt";
                        break;
                    case 25:
                        LetterType = 24;
                        categoryid = 5;
                        casetypeid = 2;
                        filename = Server.MapPath("") + "\\Criminal_Contract_Trial.rpt";
                        break;
                }                
                
                string[] key2 = { "@TicketID", "@EmployeeID", "@ViolationCategoryID", "@CaseTypeID", "@CategoryID" };
                object[] value12 = { TicketIDList, empid, violationCategoryID, casetypeid, categoryid };
                Cr.CreateReportWord(filename, "USP_HTP_Get_ContractByParameter", key2, value12, this.Session, this.Response);

                clog.AddNote(empid, lblTitle.Text + " Printed with Changes", lblTitle.Text + " Printed with Changes", TicketIDList);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        #endregion



    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
    public partial class rptlabelimages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Noufil 3650 04/30/2008 Create Crystal report to show Labels image
            clsCrsytalComponent Cr = new clsCrsytalComponent();
            string newbatchids = Session["batchids"].ToString();            
            string[] keySub = { "@BatchId" };
            object[] valueSub = { newbatchids };           
            string reportpath = Server.MapPath("/reports/labelimage.rpt");
            Cr.CreateReport(reportpath, "Usp_Hts_Edelivery_Get", keySub, valueSub, this.Session, this.Response);
        }
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    public partial class SignedContractReport : System.Web.UI.Page
    {

        #region Variables
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        DataSet DS;
        ValidationReports reports = new ValidationReports();
        clsGeneralMethods Gmethods = new clsGeneralMethods();

        #endregion


        #region Properties

        private WaitingFollowUpCategory ReportCategory
        {
            get
            {
                if (ViewState["SelectedReport"] == null)
                    return WaitingFollowUpCategory.Traffic;

                return ((WaitingFollowUpCategory)ViewState["SelectedReport"]);
            }
            set
            {

                ViewState["SelectedReport"] = value;
            }
        }

        private bool ShowAllRecords
        {
            get
            {
                if (ViewState["ShowAllRecords"] == null)
                    ViewState["ShowAllRecords"] = false;

                return Convert.ToBoolean(ViewState["ShowAllRecords"]);
            }
            set
            {
                ViewState["ShowAllRecords"] = value;
            }
        }

        private bool ShowPastRecords
        {
            get
            {
                if (ViewState["ShowPastRecords"] == null)
                    ViewState["ShowPastRecords"] = true;

                return Convert.ToBoolean(ViewState["ShowPastRecords"]);
            }
            set
            {
                ViewState["ShowPastRecords"] = value;
            }
        }

        
        #endregion


        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {

                    lbl_Message.Text = string.Empty;

                    if (!IsPostBack)
                    {
                        FillCriminalGrid();
                    }
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);

                    //Sabir Khan 5492 02/05/2009 fixed paging issue...
                    // Yasir Kamal 5427 02/06/2009 GridView Name Changed in PaggingControl
                    Pagingctrl.GridView = gv_CriminalFollowUp;
                    // Yasir Kamal 5427 end.

                }

           
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void UpdateFollowUpInfo2_PageMethod()
        {

            FillCriminalGrid();

        }

        void Pagingctrl_PageIndexChanged()
        {
            try
            {

                gv_CriminalFollowUp.PageIndex = Pagingctrl.PageIndex - 1;
                FillCriminalGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ReportCategory = WaitingFollowUpCategory.Criminal;
                gv_CriminalFollowUp.PageIndex = 0;
                FillCriminalGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
             
        }

        protected void gv_CriminalFollowUp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string firstname = (((HiddenField)gv_CriminalFollowUp.Rows[RowID].FindControl("hf_criminal_fname")).Value);
                    string causeno = (((HiddenField)gv_CriminalFollowUp.Rows[RowID].FindControl("hf_criminal_causeno")).Value);
                    string ticketno = (((HiddenField)gv_CriminalFollowUp.Rows[RowID].FindControl("hf_criminal_ticketno")).Value);
                    string lastname = (((HiddenField)gv_CriminalFollowUp.Rows[RowID].FindControl("hf_criminal_lname")).Value);
                    string courtname = (((HiddenField)gv_CriminalFollowUp.Rows[RowID].FindControl("hf_criminal_loc")).Value);
                    string court = (((HiddenField)gv_CriminalFollowUp.Rows[RowID].FindControl("hf_criminal_courtid")).Value);
                    string followupDate = (((Label)gv_CriminalFollowUp.Rows[RowID].FindControl("lbl_FollowUpD")).Text);
                    cCase.TicketID = Convert.ToInt32(ticketno);
                    string comm = cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Contract Follow Up Date";
                    // Abid Ali 5425 1/20/2009 update FollowUpType referrence
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.ContractFollowUpDate;
                    UpdateFollowUpInfo2.binddate(clsGeneralMethods.GetBusinessDayDate(DateTime.Today, 5), DateTime.Today, firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, "ContractFollowUp", courtname, followupDate);
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;

                }
            }

            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_CriminalFollowUp_PageIndexChanged(object sender, EventArgs e)
        {
            

        }
        
        protected void gv_CriminalFollowUp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_AddCriminal")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_CriminalFollowUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CriminalFollowUp.PageIndex = e.NewPageIndex;
                FillCriminalGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }


        }
       
        #endregion

        #region Methods

        private void FillCriminalGrid()
        {
            try
            {
                HccCriminalReport hccCrim = new HccCriminalReport();
                DataView dv;
                int courtid = -1;
               
                DataTable dt = hccCrim.Get_ContractFollowUp();
                if (dt.Rows.Count > 0)
                {
                    gv_CriminalFollowUp.Visible = true;
                    lbl_Message.Text = "";
                    if (dt.Columns.Contains("sno") == false)
                    {
                        dt.Columns.Add("sno");
                        int sno = 1;
                        if (dt.Rows.Count >= 1)
                            dt.Rows[0]["sno"] = 1;
                        if (dt.Rows.Count >= 2)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                                {
                                    dt.Rows[i]["sno"] = ++sno;
                                }
                            }
                        }
                    }

                    dv = dt.DefaultView;
                    
                    gv_CriminalFollowUp.DataSource = dv;
                    gv_CriminalFollowUp.DataBind();
                    Pagingctrl.PageCount = gv_CriminalFollowUp.PageCount;
                    Pagingctrl.PageIndex = gv_CriminalFollowUp.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    LblSucessdiv.Attributes.Add("class", "alert alert-danger alert-dismissable fade in");
                    LblSucesstext.Text = "No Records Found";
                    LblSucessdiv.Visible = true;
                   // lbl_Message.Text = "No Records Found";
                    gv_CriminalFollowUp.Visible = false;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }
        }
        
        #endregion

        

    }
}

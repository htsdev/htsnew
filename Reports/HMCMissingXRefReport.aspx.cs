using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace HTP.Reports
{
    public partial class HMCMissingXRefReport : System.Web.UI.Page
    {
        ValidationReports reports = new ValidationReports();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            //Sabir Khan 5809 04/21/2009 fixed paging control issue...
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            Pagingctrl.GridView = gv_Data;
        }

        //Binding data to the Grid.
        protected void BindGrid()
        {
            reports.getReportData(gv_Data, lbl_message, ValidationReport.HMCMissingXRefReport);
            Pagingctrl.PageCount = gv_Data.PageCount;
            Pagingctrl.PageIndex = gv_Data.PageIndex;
            Pagingctrl.SetPageIndex();

        }

        //handling pageIndexChange  of pagging control
        void Pagingctrl_PageIndexChanged()
        {
            gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid();
        }

        //handling pageIndexChange  of Grid 
        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_Data.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        //Sabir Khan 5809 04/21/2009 add records per page functionality...
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            if (pageSize > 0)
            {
                gv_Data.PageIndex = 0;
                gv_Data.PageSize = pageSize;
                gv_Data.AllowPaging = true;
            }
            else
                gv_Data.AllowPaging = false;
                BindGrid();
       }
             
    }
}



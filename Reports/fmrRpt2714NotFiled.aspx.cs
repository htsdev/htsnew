using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechDallasNew.Components;

namespace lntechDallasNew
{
    public partial class fmrRpt2714NotSelected  : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ValidationReports reports = new ValidationReports();
                reports.getReportData(gv_records, lbl_message, ValidationReport.Not2714Filed);
            }
        }
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Labels.aspx.cs" Inherits="HTP.Reports.Labels" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Delivery Labels</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 901px;
            height: 12px;
        }
        .style2
        {
            width: 370px;
            height: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblmain" cellspacing="1" cellpadding="1" align="center" border="0" style="width: 900px">
            <tr>
                <td style="width: 100%; height: 464px;">
                    <table id="tblsub" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td style="width: 100%">
                                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" height="10" style="width: 668px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ew:CalendarPopup ID="todate" runat="server" AllowArbitraryText="False" AutoPostBack="True"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01" OnDateChanged="todate_DateChanged">
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                </ew:CalendarPopup>
                                &nbsp;<asp:CheckBox ID="cb_sleelctall" runat="server" CssClass="clssubhead" Text="Select All"
                                    OnCheckedChanged="cb_sleelctall_CheckedChanged" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr bgcolor="#eeeeee">
                            <td style="height: 10px">
                                <table style="height: 10px">
                                    <tr>
                                        <td valign="middle" align="left" colspan="2" class="style1">
                                        </td>
                                        <td align="right" class="style2">
                                            <asp:ImageButton ID="imgbtn_trialprint" runat="server" ImageUrl="../Images/PrintNew1.jpg"
                                                ToolTip="Print Trial Letter" OnClick="imgbtn_trialprint_Click"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" height="10" style="width: 668px">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="Left" style="height: 34px">
                                <table class="style1">
                                    <tr>
                                        <td class="clssubhead">
                                            &nbsp;Delivery Labels
                                        </td>
                                        <td class="clssubhead" align="right">
                                            <%--  <uc3:PagingControl ID="Pagingctrl" runat="server" />--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="900px"
                                    CellPadding="3" CssClass="clsleftpaddingtable">                                    
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name">
                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lbl_name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                    ToolTip='<%# DataBinder.Eval(Container, "DataItem.name") %>' CssClass="label"
                                                    NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'>
                                                </asp:HyperLink>
                                                <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                                                <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Coutdatetime" HeaderText="Crt Status Trial Date & Room"
                                            HeaderStyle-CssClass="clssubhead" ControlStyle-CssClass="clsLabel">
                                            <ControlStyle CssClass="label"></ControlStyle>
                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Batchdate" HeaderText="Batch Date&amp; Rep" HeaderStyle-CssClass="clssubhead"
                                            ControlStyle-CssClass="clsLabel">
                                            <ControlStyle CssClass="label"></ControlStyle>
                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="printdatenew" HeaderText="Print Date &amp; Rep" HeaderStyle-CssClass="clssubhead"
                                            ControlStyle-CssClass="clsLabel">
                                            <ControlStyle CssClass="label"></ControlStyle>
                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TrackingNumber" HeaderText="Tracking Number" HeaderStyle-CssClass="clssubhead"
                                            ControlStyle-CssClass="clsLabel">
                                            <ControlStyle CssClass="label"></ControlStyle>
                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        </asp:BoundField>
                                        <mbrsc:SelectorField SelectionMode="Multiple" AllowSelectAll="true">
                                        </mbrsc:SelectorField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<asp:DataGrid ID="dg_batchtrial" runat="server" Width="900px" AutoGenerateColumns="False"
                                    CellPadding="2" CellSpacing="0" AllowSorting="True">
                                    <%--OnSortCommand="dg_batchtrial_SortCommand"
                                        OnItemDataBound="dg_batchtrial_ItemDataBound">
                                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                    <ItemStyle BackColor="#EEEEEE"></ItemStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Name" SortExpression="Name">
                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lbl_name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                    ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>' CssClass="label"
                                                    NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'>
                                                </asp:HyperLink>
                                                <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Court" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'></asp:Label>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                <asp:Label ID="lbl_trialdate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                <asp:Label ID="lbl_RoomNo" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="btn_Court" runat="server" OnClick="btn_Court_Click">Crt</asp:LinkButton>
                                                <asp:LinkButton ID="btn_Status" runat="server" OnClick="btn_Status_Click">Status</asp:LinkButton>
                                                &nbsp;<asp:LinkButton ID="lbtnTrialTrialdate" runat="server" OnClick="lbtnTrialTrialdate_Click">Trial Date </asp:LinkButton><asp:HyperLink
                                                    ID="HyperLink1" runat="server">&</asp:HyperLink><asp:LinkButton ID="lbtnTrialTrialRoom"
                                                        runat="server" OnClick="lbtnTrialTrialRoom_Click"> Room</asp:LinkButton>
                                                &nbsp;&nbsp;
                                            </HeaderTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Batch Date&amp; Rep" SortExpression="batchdate">
                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_batchdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'
                                                    CssClass="label"></asp:Label>
                                                <asp:Label ID="lbl_BatchRep" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbtnTrialBatchDate" runat="server" OnClick="lbtnTrialBatchDate_Click">Batch Date</asp:LinkButton>
                                                <asp:HyperLink ID="HyperLink2" runat="server">&</asp:HyperLink>
                                                <asp:LinkButton ID="lbtnTrialBatchRep" runat="server" OnClick="lbtnTrialBatchRep_Click">Rep</asp:LinkButton>
                                            </HeaderTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Print Date &amp; Rep" SortExpression="printdate">
                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_printdate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbtnTrialPrintDate" runat="server" OnClick="lbtnTrialPrintDate_Click">Print Date</asp:LinkButton>
                                                <asp:HyperLink ID="HyperLink3" runat="server">&</asp:HyperLink>
                                                <asp:LinkButton ID="lbtnTrialPrintRep" runat="server" OnClick="lbtnTrialPrintRep_Click">Rep</asp:LinkButton>
                                            </HeaderTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Tracking Number">
                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_trackno" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TrackingNumber") %>'></asp:Label>
                                                <asp:Label ID="lblResponse" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container, "DataItem.Response") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <mbrsc:RowSelectorColumn AllowSelectAll="True" SelectionMode="Multiple" />
                                    </Columns>
                                </asp:DataGrid>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

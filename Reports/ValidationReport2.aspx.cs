using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using lntechNew.Components;

namespace HTP.Reports
{
    
    public partial class ValidationReport2 : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 5557 02/25/2008  grouping Auto Generated Code
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_submit.Click += new System.EventHandler(this.Button1_Click);
            this.dg_valrep.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_valrep_ItemCommand);
            this.dg_valrep.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_valrep_PageIndexChanged);
            this.dg_valrep.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_valrep_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        // Fahad Muhammad Qureshi 5557 02/25/2008  grouping Page Control
        #region Controls

        protected eWorld.UI.CalendarPopup calQueryFrom;
        protected System.Web.UI.WebControls.DataGrid dg_valrep;
        protected System.Web.UI.WebControls.Button btn_submit;
        protected eWorld.UI.CalendarPopup calQueryTo;
        protected System.Web.UI.WebControls.Label lbl_message;
        protected System.Web.UI.WebControls.Label lblCurrPage;
        protected System.Web.UI.WebControls.CheckBox chk_showall;

        #endregion

        // Fahad Muhammad Qureshi 5557 02/25/2008  grouping Variables
        #region Variables

        DataSet ds_val;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        ValidationReports objValidation = new ValidationReports();
        clsSession ClsSession = new clsSession();
        static int counterv;

        #endregion

        // Fahad Muhammad Qureshi 5557 02/25/2008  grouping Events
        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Validating session
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {

                    lbl_message.Text = "";

                    if (!IsPostBack)
                    {
                        calQueryFrom.SelectedDate = DateTime.Now.Date;
                        calQueryTo.SelectedDate = DateTime.Now.Date;
                    }
                    if (Convert.ToString(Session["FromUpdate"]) == "true")
                    {
                        FillGrid();
                        Session["FromUpdate"] = "false";
                    }
                   
                }
                UpdateViolationSRV1.PageMethod += new lntechNew.WebControls.PageMethodHandler(UpdateViolationSRV1_PageMethod);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.DataGrid = dg_valrep;
                PagingControl.grdType = GridType.DataGrid;
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);

            }

        }

        //When search button is clicked
        private void Button1_Click(object sender, System.EventArgs e)
        {
            try
            {
                dg_valrep.CurrentPageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);

            }
        }

        //In order to redirect to violation fees page
        private void dg_valrep_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (drv == null)
                return;

            try
            {
                LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkb_manverify");
                lnkBtn.Attributes.Add("onClick", "return showpopup('" + UpdateViolationSRV1.ClientID + "','" + e.Item.ClientID + "',0);");
 
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);

            }

        }
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {

                if (pageSize > 0)
                {
                    dg_valrep.CurrentPageIndex = 0;
                    dg_valrep.PageSize = pageSize;
                    dg_valrep.AllowPaging = true;
                }
                else
                {
                    dg_valrep.AllowPaging = false;
                }
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);

            }
        }

        private void dg_valrep_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_valrep.CurrentPageIndex = e.NewPageIndex;
                    FillGrid();
                   
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);

            }
            

        }

        private void dg_valrep_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Clicked")
                {	//To avoid event firing when popup windows is closed
                    if (Convert.ToString(Session["UP"]) != counterv.ToString())
                    {
                        
                            string ticketid = ((Label)e.Item.FindControl("lbl_ticketid")).Text;
                            int violationid = 0;
                            string search = ((Label)e.Item.FindControl("lbl_search")).Text;
                            ClsSession.CreateCookie("sTicketID", ticketid, this.Request, this.Response);
                            violationid = Convert.ToInt32(((Label)(e.Item.FindControl("lbl_violationid"))).Text);
                            HttpContext.Current.Response.Write("<script language='javascript'>window.open('../ClientInfo/UpdateViolation.aspx?ticketid=" + ticketid + "&search=" + search + "&ticketsViolationID=" + violationid + "','','status=yes,left=20,top=20, width=850,height=600,scrollbars=yes'); </script>");
                            counterv += 1;
                            Session["UP"] = counterv;
                       
                    }
                    else
                    {
                        counterv += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.ToString();
                lbl_message.Visible = true;
                clsLogger.ErrorLog(ex);

            }

        }

        #endregion

        // Fahad Muhammad Qureshi 5557 02/25/2008  grouping Methods
        #region Methods


        /// <summary>
        /// FAHAD 5557 03/09/2009
        /// Mthod to maintain the view or records of the page after update the Violations
        /// </summary>
        void UpdateViolationSRV1_PageMethod()
        {
            lbl_message.Text = UpdateViolationSRV1.ErrorMessage;
            FillGrid();
        }

        /// <summary>
        /// FAHAD 5557 03/09/2009
        /// Maintain the Page index for Paging Control
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            dg_valrep.CurrentPageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }


        /// <summary>
        /// FAHAD 5557 03/09/2009
        /// Method to fill grid
        /// </summary>
        private void FillGrid()
        {
            tbl_plzwait1.Style.Clear();
            tbl_plzwait1.Style.Add(HtmlTextWriterStyle.Display, "none");
            int showall = 0;
            int showDISP = 0;
            int notShowAWorBW = 0;
            if (chk_showall.Checked)
            {
                showall = 1;
            }
            if (chkb_DISP.Checked)
            {
                showDISP = 1;
            }

            if (cb_noaworbw.Checked)
            {
                notShowAWorBW = 1;
            }

            ds_val = objValidation.GetValidationReport2(calQueryFrom.SelectedDate, calQueryTo.SelectedDate, showall, showDISP, notShowAWorBW);

            if (ds_val.Tables[0].Rows.Count < 1)
            {
                lbl_message.Text = "No Records Found";
                lbl_message.Visible = true;
                dg_valrep.Visible = false;
                Pagingctrl.Visible = false;
            }
            else
            {
                Pagingctrl.Visible = true;
                dg_valrep.Visible = true;
                BindReport(ds_val);
                dg_valrep.DataSource = ds_val;
                dg_valrep.DataBind();
                if (dg_valrep.PageSize >= ds_val.Tables[0].Rows.Count)
                {
                    dg_valrep.PagerStyle.Visible = false;

                }
                else
                {
                    dg_valrep.PagerStyle.Visible = true;
                }
                Pagingctrl.PageCount = dg_valrep.PageCount;
                Pagingctrl.PageIndex = dg_valrep.CurrentPageIndex;
                Pagingctrl.SetPageIndex();
            }

        }

        /// <summary>
        /// FAHAD 5557 03/09/2009
        /// Method To Generate Serial No on the basis of TicketId
        /// </summary>
        /// <param name="dtRecords"></param>
        public void BindReport(DataSet dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Tables[0].Columns.Contains("sno"))
                dtRecords.Tables[0].Columns.Add("sno");


            if (dtRecords.Tables[0].Rows.Count >= 1)
                dtRecords.Tables[0].Rows[0]["sno"] = 1;

            if (dtRecords.Tables[0].Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Tables[0].Rows.Count; i++)
                {
                    if (dtRecords.Tables[0].Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Tables[0].Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Tables[0].Rows[i]["sno"] = ++sno;
                    }
                }
            }
            
        }

        
        #endregion

       

    }
}

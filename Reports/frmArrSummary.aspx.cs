using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using HTP.Components;

namespace HTP.Reports
{
    public partial class frmArrSummary : System.Web.UI.Page
    {
        #region Variables

        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
        clsLogger clog = new clsLogger();
        clsSession uSession = new clsSession();
        clsENationWebComponents clsDB = new clsENationWebComponents();
        //ozair 4073 05/21/2008
        clsDocumentTracking clsDT = new clsDocumentTracking();
        int documentBatchID = 0;
        //end ozair 4073

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
               
                Create_ArriagnmentSummary_Report();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sTVIDS"></param>
        /// <returns>int</returns>
        private int GetDocumentID()
        {
            //ozair 3643 on 05/01/2008 implemented the logic for document ID.
            clsDT.BatchDate = DateTime.Now;
            clsDT.DocumentID = 2; // 2: Arr Settings            
            clsDT.EmpID = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
            //get the document id for this report
            return clsDT.GetDBIDByInsertingBatch();
        }

        /// <summary>
        /// ozair 4073 05/21/2008
        /// </summary>
        public void Create_ArriagnmentSummary_Report()
        {
            try
            {
                string sTVIDS = "";

                if (Session["ARRTVIDS"] != null)
                {
                    sTVIDS = Session["ARRTVIDS"].ToString();
                    Session["ARRTVIDS"] = null;
                }

                string FilePath1 = ViewState["FilePath"].ToString() + "ARR-Temp1" + ".pdf";
                string FilePath2 = ViewState["FilePath"].ToString() + "ARR-Temp2" + ".pdf";
                string TempPath = ViewState["FilePath"].ToString() + "ARR" + "-Temp1" + ".pdf";
                string TempPath2 = ViewState["FilePath"].ToString() + "ARR" + "-Temp2" + ".pdf";
                string filename = Server.MapPath("") + "\\Arraignment.rpt";
                string Filename = Server.MapPath("") + "\\ARRPleaNew2.rpt";

                if (Request.QueryString["Flag"] == "1")//for generating document ID..
                {
                    documentBatchID = GetDocumentID();

                    string Path = ViewState["FilePath"].ToString() + "ARR" + "-" + documentBatchID.ToString() + ".pdf";

                    string[] key2 = { "@TVIDS", "@DocumentBatchID" };
                    object[] value2 = { sTVIDS, documentBatchID };

                    string[] key3 = { "@TVIDS", "@DocumentBatchID", "@UpdateFlag", "@employee" };
                    object[] value3 = { sTVIDS, documentBatchID, 1, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    ClsCr.CreateReport(filename, "USP_HTS_ARRAIGNMENT_REPORT", key2, value2, this.Session, this.Response, TempPath);

                    ClsCr.CreateReport(Filename, "usp_hts_arraignment_PleaNotGuilyReport", key3, value3, this.Session, this.Response, TempPath2);

                    string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "ARR-" + documentBatchID.ToString() + ".pdf";

                    if (File.Exists(FilePath1))
                    {
                        if (File.Exists(FilePath2))
                        {
                            FilePath1 = FilePath1.Replace("\\\\", "\\");
                            FilePath2 = FilePath2.Replace("\\\\", "\\");
                            Path = Path.Replace("\\\\", "\\");
                            Doc doc1 = new Doc();
                            Doc doc2 = new Doc();
                            doc1.Read(FilePath1);
                            doc2.Read(FilePath2);
                            doc1.Append(doc2);
                            doc1.Save(Path);
                            doc2.Dispose();
                            doc1.Dispose();
                            File.Delete(FilePath1);
                            File.Delete(FilePath2);
                            //Response.Redirect("c:\\Docstorage" + fPath, false);

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/pdf";
                            Response.WriteFile("C:\\Docstorage" + fPath);
                            Response.Flush();
                            Response.Close();
                        }
                    }
                    // Afaq 8293 09/20/2010 Maintain print history by inserting the printed record in datbase.
                    string[] key = { "@PrintDate", "PrintType", "@EmpID", "@BatchID" };
                    object[] value1 = { DateTime.Now, "ARR", Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)), documentBatchID.ToString() };
                    clsDB.ExecuteSP("usp_hts_InsertInPrintSummary", key, value1);
                    //string Path = ViewState["FilePath"].ToString() + "ARR" + "-" + PrintID.ToString() + ".pdf";
                }
                else if (Request.QueryString["Flag"] == "2")//Only for preview..
                {
                    string[] key2 = { "@TVIDS", "@DocumentBatchID" };
                    object[] value2 = { sTVIDS, documentBatchID };
                    ClsCr.CreateReport(filename, "USP_HTS_ARRAIGNMENT_REPORT", key2, value2, this.Session, this.Response);
                }
                else if (Request.QueryString["Flag"] == "3")//print all with docid
                {
                    documentBatchID = GetDocumentID();

                    string Path = ViewState["FilePath"].ToString() + "ARR" + "-" + documentBatchID.ToString() + ".pdf";

                    string[] key2 = { "@TVIDS", "@DocumentBatchID" };
                    object[] value2 = { "", documentBatchID };

                    ClsCr.CreateReport(filename, "USP_HTS_ARRAIGNMENT_REPORT", key2, value2, this.Session, this.Response, TempPath);
                    string[] key3 = { "@TVIDS", "@DocumentBatchID", "@UpdateFlag", "@employee" };
                    object[] value3 = { sTVIDS, documentBatchID, 1, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    ClsCr.CreateReport(Filename, "usp_hts_arraignment_PleaNotGuilyReport", key3, value3, this.Session, this.Response, TempPath2);

                    string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "ARR-" + documentBatchID.ToString() + ".pdf";

                    if (File.Exists(FilePath1))
                    {
                        if (File.Exists(FilePath2))
                        {
                            FilePath1 = FilePath1.Replace("\\\\", "\\");
                            FilePath2 = FilePath2.Replace("\\\\", "\\");
                            Path = Path.Replace("\\\\", "\\");
                            WebSupergoo.ABCpdf6.Doc doc1 = new Doc();
                            WebSupergoo.ABCpdf6.Doc doc2 = new Doc();
                            doc1.Read(FilePath1);
                            doc2.Read(FilePath2);
                            doc1.Append(doc2);
                            doc1.Save(Path);
                            doc2.Dispose();
                            doc1.Dispose();
                            File.Delete(FilePath1);
                            File.Delete(FilePath2);
                            //Response.Redirect("C:\\Docstorage" + fPath, false);

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/pdf";
                            Response.WriteFile("C:\\Docstorage" + fPath);
                            Response.Flush();
                            Response.Close();
                        }
                    }
                    // Afaq 8293 09/20/2010 Maintain print history by inserting the printed record in datbase.
                    string[] key = { "@PrintDate", "PrintType", "@EmpID", "@BatchID" };
                    object[] value1 = { DateTime.Now, "ARR", Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)), documentBatchID.ToString() };
                    clsDB.ExecuteSP("usp_hts_InsertInPrintSummary", key, value1);
                }
                else if (Request.QueryString["Flag"] == "4")//print all with out doc id
                {
                    string[] key2 = { "@TVIDS", "@DocumentBatchID" };
                    object[] value2 = { "", documentBatchID };
                    ClsCr.CreateReport(filename, "USP_HTS_ARRAIGNMENT_REPORT", key2, value2, this.Session, this.Response);
                }
                else if (Request.QueryString["Flag"] == "5")//for generating document ID..
                {
                    documentBatchID = GetDocumentID();

                    string Path = ViewState["FilePath"].ToString() + "ARR_WAITING" + "-" + documentBatchID.ToString() + ".pdf";

                    string[] key2 = { "@TVIDS", "@DocumentBatchID" };
                    object[] value2 = { sTVIDS, documentBatchID };

                    string[] key3 = { "@TVIDS", "@DocumentBatchID", "@UpdateFlag", "@employee" };
                    object[] value3 = { sTVIDS, documentBatchID, 0, Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request)) };

                    ClsCr.CreateReport(filename, "USP_HTS_ARRAIGNMENT_REPORT_MAIN", key2, value2, this.Session, this.Response, TempPath);

                    ClsCr.CreateReport(Filename, "usp_hts_arraignment_PleaNotGuiltyReport_MAIN", key3, value3, this.Session, this.Response, TempPath2);

                    string fPath = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "ARR_WAITING-" + documentBatchID.ToString() + ".pdf";

                    if (File.Exists(FilePath1))
                    {
                        if (File.Exists(FilePath2))
                        {
                            FilePath1 = FilePath1.Replace("\\\\", "\\");
                            FilePath2 = FilePath2.Replace("\\\\", "\\");
                            Path = Path.Replace("\\\\", "\\");
                            WebSupergoo.ABCpdf6.Doc doc1 = new Doc();
                            WebSupergoo.ABCpdf6.Doc doc2 = new Doc();
                            doc1.Read(FilePath1);
                            doc2.Read(FilePath2);
                            doc1.Append(doc2);
                            doc1.Save(Path);
                            doc2.Dispose();
                            doc1.Dispose();
                            File.Delete(FilePath1);
                            File.Delete(FilePath2);

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/pdf";
                            Response.WriteFile("C:\\Docstorage" + fPath);
                            Response.Flush();
                            Response.Close();

                            //Response.Redirect("C:\\Docstorage" + fPath, false);
                        }
                    }
                }
                else if (Request.QueryString["Flag"] == "6")//Only for preview..
                {
                    string[] key2 = { "@TVIDS", "@DocumentBatchID" };
                    object[] value2 = { sTVIDS, documentBatchID };
                    ClsCr.CreateReport(filename, "USP_HTS_ARRAIGNMENT_REPORT_MAIN", key2, value2, this.Session, this.Response);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion
    }
}

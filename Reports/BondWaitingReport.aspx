<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BondWaitingReport.aspx.cs" Inherits="HTP.Reports.BondWaitingReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bond Waiting Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tr>
                    <td style="width: 896px; height: 118px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                        
                    </td>
                </tr>
                <tr>
                    <td style="width: 896px; height: 22px" align="center">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                        &nbsp; &nbsp; &nbsp;</td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                        <asp:Label ID="Label2" runat="server" CssClass="clsLabel" Text="Current Page : "></asp:Label>
                        <asp:Label ID="LB_curr" runat="server" CssClass="clsLabel" Width="41px"></asp:Label>
                        <asp:Label ID="totlb" runat="server" CssClass="clsLabel" Text="Go To :     "></asp:Label>
                        <asp:DropDownList ID="DL_pages" runat="server" OnSelectedIndexChanged="DL_pages_SelectedIndexChanged"
                            Width="61px" CssClass="clsInputCombo" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" style="width: 896px; height: 11px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 896px; height: 126px;">
                        &nbsp;<asp:GridView ID="DG_pastcases" runat="server" AutoGenerateColumns="False"
                            CssClass="clsleftpaddingtable" Width="100%" OnSelectedIndexChanged="DG_pastcases_SelectedIndexChanged"
                            AllowPaging="True" PageSize="90" OnPageIndexChanging="DG_pastcases_PageIndexChanging" HorizontalAlign="Center" AllowSorting="True" OnSorting="gv_Records_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="S.No" SortExpression="sno">
                                    
                                 
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        &nbsp;
                                        <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket No" SortExpression= "ticketnumber">
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_CaseNumber" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Last Name" SortExpression= "lname">
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.lname") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="First Name" SortExpression= "fname">
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.fname") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" Crt DateTime" SortExpression= "courtdatemain">
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:g}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Crt No" SortExpression= "courtnumber">
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_courtnumber" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumber") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Crt. Loc" SortExpression= "shortname">
                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trial Category"  SortExpression= "status">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_status" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="AutoCourtDate" SortExpression="CourtDate">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_crtdate" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings NextPageText="Next" PreviousPageText="Previous"  />
                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" height="11px" style="width: 896px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 896px; height: 57px;">
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

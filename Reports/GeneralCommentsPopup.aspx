﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="GeneralCommentsPopup.aspx.cs"
    Inherits="HTP.Reports.GeneralCommentsPopup" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Service Ticket Window</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /><link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png" />
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png" />
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png" />
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png" />
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->


    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/GeneralValidations.js" type="text/javascript">
    </script>

    <style type="text/css">
        .style1
        {
            width: 80px;
        }
        .clsLeftPaddingTable TD
        {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
        }
    </style>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script src="../Scripts/jsDate.js" type="text/javascript"></script>
    <link href="../Styles.css" type="text/css" rel="stylesheet"/>

    <script language="javascript" type="text/javascript">
        function Validate() {
            if (document.getElementById("ddl_per").value == -1) {
                alert("Please select percentage completion ");
                return false;

            }
            //added by kahlid  task 2595
            if (document.getElementById("drpCategory").value == -1) {
                alert("Please select a category");
                return false;
            }

            var generalCheck = document.getElementById("cb_gencomments");
            var serviceCheck = document.getElementById("cb_serinstruction");
            var assignto = document.getElementById("ddl_assignto");
            var showdocket = document.getElementById("cb_showondocket");
            var btnSubmit = document.getElementById("btnSubmit");
            var tblwait = document.getElementById("tblwait");

            //Sabir Khan 5738 06/01/2009 Check business days for follow up date date...
            //Yasir Kamal 7412 02/16/2010 do not check follow up date for 100% tickets.
            if (document.getElementById("ddl_per").value != "100") {
                var IsValidFollowupdate = CheckDate(document.getElementById("calfollowupdate").value, "calfollowupdate");
                if (IsValidFollowupdate == false) {
                    return false;
                }
            }

            if (showdocket.checked == true) {

                //If Attorney is assigned than user must select atleat one comment to be display on docket report                        
                if (assignto.selectedIndex != 0 && (generalCheck.checked == false && serviceCheck.checked == false)) {
                    alert("Please select general comment or service ticket instruction to be display on the docket report.");
                    return false;
                }
                else {
                    btnSubmit.style.display = "none";
                    tblwait.style.display = "block";
                    return true;
                }
            }
            else {
                btnSubmit.style.display = "none";
                tblwait.style.display = "block";
                return true;
            }



            if (serviceCheck.checked && document.getElementById("tb_serinstruction").value == "") {
                alert("Please Enter Service Ticket Instructions");
                return false;
            }

            //            if(document.getElementById("txtGC").value=="")
            //            {
            //                alert("Please Enter General Comments");
            //                return false;
            //            }   
            btnSubmit.style.display = "none";
            tblwait.style.display = "block";


        }

        function ShowHide() {

            var showdocket = document.getElementById("cb_showondocket");
            var scomment = document.getElementById("cb_serinstruction");
            var gcomment = document.getElementById("cb_gencomments");

            if (showdocket.checked == false) {
                gcomment.parentElement.setAttribute('disabled', 'true');
                gcomment.disabled = true;
                scomment.parentElement.setAttribute('disabled', 'true');
                scomment.disabled = true;

            }
            if (showdocket.checked == true) {
                gcomment.parentElement.removeAttribute('disabled');
                scomment.parentElement.removeAttribute('disabled');
                scomment.disabled = false;
                gcomment.disabled = false;

            }
        }
        //Zahoor 4770 09/12/08
        function SetPercentValue() {
            var option = document.getElementById("ddl_ContinuanceOption").value
            var ddl_Percent = document.getElementById("ddl_per");
            var pervalue;
            //ddl_Percent.disabled = false;
            //  alert(option);
            if (option == "1") {
                pervalue = "0";
            }
                //else if (option == "Contract Sent" || option == "Contract Signed")
            else if (option == "2" || option == "3") {
                pervalue = "25";
            }
            else if (option == "4" || option == "5") {
                pervalue = "50";
            }
            else {
                pervalue = "100";
            }

            ddl_Percent.value = pervalue;
        }

        //Sabir Khan 5711 04/01/2009 Scripts used for edit service Ticket comments...
        //---------------------------------
        function showhideservice() {
            var text = document.getElementById("tb_serinstruction");
            var btnEdt = document.getElementById("btnEditService");
            if (btnEdt.value == "Edit") {
                text.style.display = "block";
                btnEdt.value = "Cancel";
            }
            else {
                text.style.display = "none";
                btnEdt.value = "Edit";
            }
        }
        //----------------------------------

        //Sabir Khan 5738 06/01/2009 Logic for selecting valid business days for follow update...
        //---------------------------------------------------------------------------------    
        function CheckDate(seldate, tbID) {


            //Yasir Kamal 7412 02/16/2010 do not check follow up date for 100% tickets.
            if (document.getElementById("ddl_per").value != "100") {

                var w = document.Form1.drpCategory.selectedIndex;
                var selected_text = document.Form1.drpCategory.options[w].text;
                today = new Date();
                var diff = Math.ceil(DateDiff(seldate, today));
                var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
                if (seldate != '') {
                    var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth() + 1) + "/" + Date.parseInvariant(seldate).getDate() + "/" + Date.parseInvariant(seldate).getFullYear(), "MM/dd/yyyy");
                    newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
                    var datediff = Math.ceil(DateDiff(newseldate, today));
                    if (datediff >= 0) {

                        //Sabir Khan 6245 08/03/2009 Get courtdate and check with week in future date...
                        var futuredate = dateAdd("d", 30, today);
                        var courtdate = document.getElementById("hfcourtdate").value;
                        var courtdatediff = Math.ceil(DateDiff(courtdate, futuredate));
                        if (courtdatediff < 0) {
                            var i = 0, countbusinessdays = 0, countHolidays = 0;
                            for (i = 1; i <= datediff; i++) {
                                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                                    countbusinessdays++;
                                }
                                else if ((weekday[today.getDay()] == "Saturday") && (i == 1)) {
                                    countbusinessdays++;
                                }
                                today = dateAdd("d", 1, today);
                            }

                            //Asad Ali 8492 11/03/2010 if courtbusiness days greater then 90 and cateory is Bond Forfeiture restrict user
                            if (selected_text == 'Bond Forfeiture' && (datediff > 90 || countbusinessdays == 0) && Querystring() == false) {
                                alert("Please select Follow Up Date within next 90 days from today's Date for Bond Forfeiture Category");
                                return false;
                            }
                                //Asad Ali 8492 11/03/2010 if courtbusiness days greater then 90 and cateory is Bond Forfeiture restrict user
                            else if (((countbusinessdays > 5) || (countbusinessdays == 0)) && selected_text != 'Bond Forfeiture') {
                                //Yasir Kamal 6936 12/09/2009 allow current followupdate for credit card dispute.
                                if ((Querystring() || selected_text == "Credit Card Dispute") && countbusinessdays == 0) {
                                    return true;
                                }
                                else {
                                    alert("Please select Follow Up Date within next 5 business days from today's Date");
                                    return false;
                                }
                            }


                        }
                    }
                    else {
                        var olddate = document.getElementById("lblCurrentFollowupdate").innerText;
                        if (seldate != olddate) {
                            alert("Please select future date");
                            return false;
                        }
                    }
                    if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {
                        alert("Please select any business day");
                        return false;
                    }
                }
            }
        }
        function DateDiff(date1, date2) {
            var one_day = 1000 * 60 * 60 * 24;
            var objDate1 = new Date(date1);
            var objDate2 = new Date(date2);
            var curr = (objDate1.getTime() - objDate2.getTime()) / one_day;
            return (objDate1.getTime() - objDate2.getTime()) / one_day;

        }
        //Sabir Khan 6082 07/03/2009 Check Querystring for Open new service ticket...
        function Querystring() {
            this.params = {};
            var qs = location.search.substring(1, location.search.length);
            if (qs.length == 0)
                return false;
            qs = qs.replace(/\+/g, ' ');
            var args = qs.split('&'); // parse out name/value pairs separated via &
            //    // split out each name=value pair
            for (var i = 0; i < args.length; i++) {
                var pair = args[i].split('=');
                var name = decodeURIComponent(pair[0]);
                if (name == "Fid") {
                    return false;
                }
            }
            return true;
        }
        function ShowPopUp_1() {
            today = new Date();
            var dt = today;
            var tst1;
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 1; i <= 5; i++) {
                tst1 = formatDate(dateAdd("d", i, today), "MM/dd/yyyy");
                if ((weekday[tst1.getDay()] != "Sunday") && (weekday[tst1.getDay()] != "Saturday")) {
                    countbusinessdays += 2;
                }
                else {
                    countbusinessdays += 1
                }
            }
            dt = formatDate(dateAdd("d", countbusinessdays, dt), "MM/dd/yyyy");
            document.getElementById('<%= this.ClientID %>_cal_followupdate').value = dt;
   }
   ///------------------------------------------------------------------------------------    
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="uppnl_main" runat="server">
     <ContentTemplate>
         <section id="main-content-popup class="">
                <section class="wrapper main-wrapper row addnewleadPopupCustom" style="">
                     <div class="col-xs-12">
                         <div class="page-title" style="display: none;>
                
                              <h1 class="title">Add Lead</h1>
                        </div>

                      </div>
                    <div class="clearfix"></div>
                     <div class="col-xs-12">
                         <section class="box ">
                        <header class="panel_header" style="text-align: center">
                            
                                <h2 class="title pull-center"><asp:Label ID="lblDivision" runat="server" CssClass="form-label" ForeColor="Red" Font-Bold="True"></asp:Label></h2>
                                <div class="actions panel_actions remove-absolute pull-right">
                	                <a class="box_toggle fa fa-chevron-down"></a>
                                </div>
                            
                       </header>
                      
                        <div class="content-body">

                             <div class="row">
                                <div class ="col-md-6">
                                    <h3 class="title pull-left"><asp:Label ID="lbl_ClientName" runat="server"></asp:Label>, <asp:Label ID="lbl_fisrtname" runat="server"></asp:Label></h3>
                                </div>
                                <div class ="col-md-6 text-right">
                                    <h3 class="title pull-right"><asp:Label ID="Label1" runat="server" Text="Service Ticket Window"></asp:Label></h3>
                                </div>
                                 </div>
                            <hr style="border-top: 1px solid #11a2cf;"/>
                            <div class="clearfix"></div>

                            <div class="row">
                             <div class="col-md-2">
                                <div class="form-group">
                                    <label class="form-label">% Completion</label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                        <asp:DropDownList ID="ddl_per" runat="server" CssClass="form-control" > 
                                        <asp:ListItem Value="0">0%</asp:ListItem>
                                        <asp:ListItem Value="25">25%</asp:ListItem>
                                        <asp:ListItem Value="50">50%</asp:ListItem>
                                        <asp:ListItem Value="75">75%</asp:ListItem>
                                        <asp:ListItem Value="100">100%</asp:ListItem>
                                        </asp:DropDownList>     
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" runat="server" visible="false">
                                <div class="form-group">
                                    <label class="form-label">Priority</label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                <asp:DropDownList ID="drpPriority" runat="server" CssClass="clsinputadministration">
                                    <asp:ListItem Value="2">High</asp:ListItem>
                                    <asp:ListItem Value="0">Low</asp:ListItem>
                                    <asp:ListItem Value="1">Medium</asp:ListItem>
                                   
                                </asp:DropDownList>  
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Category</label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                        <asp:DropDownList ID="drpCategory" runat="server" CssClass="form-control"
                                            Width="" AutoPostBack="True" DataTextField="Description" DataValueField="ID"
                                            OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                                    
                                </asp:DropDownList>    
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Assign To</label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                          <asp:DropDownList ID="ddl_assignto" runat="server" CssClass="form-control">
                                </asp:DropDownList>  
                                    </div>
                                </div>
                            </div>

                            </div>


                            <div class="row">
                                <hr style="border-top: 1px solid #11a2cf;"/>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lbl_ContinuanceOption" CssClass="form-label" runat="server" Text="Continuance Option" Visible="False"></asp:Label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                      <asp:DropDownList ID="ddl_ContinuanceOption" runat="server" CssClass="form-control"
                                    Width="135px" DataTextField="Description" DataValueField="ID"  
                                      Onchange ="javascript:SetPercentValue()" Visible="False">
                            </asp:DropDownList>               
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label class="form-label">Current Follow Up date</label>
                                    <div class="controls">
                                        <asp:Label ID="lblCurrentFollowupdate" runat="server" Text=""> </asp:Label>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label class="form-label">Next Follow Up date</label>

                                        <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                                                            <asp:TextBox ID="calfollowupdate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>

                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                                     


                                         <%--<ew:CalendarPopup ID="calfollowupdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="True"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                Width="90px" JavascriptOnChangeFunction="CheckDate" >
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                            </ew:CalendarPopup>--%>
                                                             
                                    </div>
                                </div>
                            </div>
                            </div>
                            
                            
                             <div class="clearfix"></div>
                            <hr style="border-top: 1px solid #11a2cf;"/>
                             
                        <div class="col-md-12">
                                <div class="form-group">
                                    <div class="controls">
                                    <asp:CheckBox runat="server" ID="cb_showondocket" CssClass="form-label" Text="Show case on trial docket" />
                                                              
                                    </div>
                                </div>
                            </div>
                               <hr style="border-top: 1px solid #11a2cf;"/>
                           
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label"></label>
                                    <div class="controls">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            

                            <div class="clearfix"></div>

                             <div class="col-md-8">
                                 <div class="row">
                                     <div class="col-md-8 col-sm-8 col-xs-8">
                                         <div class="form-group">
                                    <label class="form-label"><b>General Comments</b></label>
                                </div>
                                     </div>
                                     <div class="col-md-4 col-sm-4 col-xs-4">
                                          <div class="form-group">
                                    <asp:CheckBox runat="server" ID="cb_gencomments" CssClass="form-label" Text="Show On Docket" />
                                </div>
                                     </div>
                                 </div>
                                
                            </div>
                            <div class="col-md-4">
                               
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label"></label>
                                    <span class="desc"></span>
                                    <div class="controls" id="generalcomments2">
                                    <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Height="55px" Width="545px">
                                    </cc2:WCtl_Comments>
                                                              
                                    </div>
                                </div>
                            </div>

                             <hr style="border-top: 1px solid #11a2cf;"/>

                            <div class="clearfix"></div>
                            <div class="row-fluid">
                             <div class="col-md-8 col-sm-8 col-xs-8">
                                <div class="form-group">
                                    <label class="form-label" id="lbl_serviceticket"><b>Service Ticket Comments</b></label>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-4" id="serviceticket2">
                                <div class="form-group">
                                    <asp:CheckBox runat="server" ID="cb_serinstruction" CssClass="form-label" Text="Show On Docket" />
                                </div>
                            </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label"></label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                        <asp:TextBox ID="lblServiceComments" runat="server"  CssClass="form-control" TextMode="MultiLine" style="resize:none" ReadOnly="true" ></asp:TextBox> 
                                        <div class="well-lg"></div>
                                        <asp:TextBox ID="tb_serinstruction" runat="server" CssClass="form-control" TextMode="MultiLine" style="display:none; resize:none"> </asp:TextBox>
                                        <div class=""></div>
                                             <input ID="btnEditService" onclick="showhideservice();" class="btn btn-primary pull-right" type="button" value="Edit" />                 
                                    </div>
                                </div>
                            </div>
                             <div class="clearfix"></div>
                             <hr style="border-top: 1px solid #11a2cf;"/>

                           <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <label class="form-label"></label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Add Service Ticket" CssClass="btn btn-primary" OnClientClick="return Validate();" OnClick="btnSubmit_Click"></asp:Button>               
                                        &nbsp; <table id="tblwait" border="0" cellpadding="0" cellspacing="0" style="width: 100%;display: none; ">
                                            <tr>
                                                <td align="center" class="clssubhead" style="height: 25px">
                                                       Please wait while system updates service ticket information.</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                               </div>                                                       
                          </div>
                                
                                
                </section>
                     </div>
                 </section>
            </section>

        <table id="TableMain" cellspacing="1" cellpadding="1" border="0" align="center" style="width: 552px;
            border-left-color: navy; border-bottom-color: navy; border-top-color: navy; border-collapse: collapse;
            border-right-color: navy">
            
            <tr>
                
                
            </tr>
           
          
            <tr>
                <td class="clsleftpaddingtable" style="height: 20px; width: 558px;" valign="top">
                    <table class="clsleftpaddingtable" style="width: 100%">
                        <tr>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>

            </tr>
           
        </table>
        <asp:HiddenField ID="hf_defaultassigned" runat="server" />
         <asp:HiddenField ID="hfcourtdate" runat="server" />
        </ContentTemplate>
            <Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" />
                
            </Triggers>
        </aspnew:UpdatePanel>
       
    </form>

    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class ConcentrationReport : System.Web.UI.Page
    {
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        private DataTable dtRecords;
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";
            chk_hidestatus.Visible = false;
            tblGrids.Width = "1100";
            if (!IsPostBack)
            {
                dtp_From.Text = DateTime.Now.ToShortDateString();
                FillCourts();
                try
                {
                    chk_HMCDetail.Checked = true;

                    GetRecords();
                    SetTables();
                    SetGridViewColumns();
                }
                catch (Exception e1)
                {
                    LblSucessdiv.Attributes.Add("class", "alert alert-danger alert-dismissable fade in");
                    LblSucesstext.Text = e1.Message;
                    LblSucessdiv.Visible = true;
                }
            }
        }

        private void FillCourts()
        {
            DataTable dt = clsDb.Get_DT_BySPArr("USP_HTS_GET_SHORTCOURTNAMES");
            ddl_Courts.Items.Add(new ListItem("All courts", "0"));
            ddl_Courts.Items.Add(new ListItem("HMC", "1"));
            ddl_Courts.Items.Add(new ListItem("HCJP Courts", "2"));
            ddl_Courts.Items.Add(new ListItem("All Non-HMC & HCJP", "3"));
            ddl_Courts.Items.Add(new ListItem("All Non HMC", "4"));
            ddl_Courts.Items.Add(new ListItem("Not HMC & HCCC", "5"));


            foreach (DataRow dr in dt.Rows)
            {
                ddl_Courts.Items.Add(new ListItem(Convert.ToString(dr["shortname"]), Convert.ToString(dr["courtid"])));
            }
        }

        private void GetRecords()
        {
            try
            {
                string[] keys = { "@StartDate", "@EndDate", "@Court", "@ShowDetail", "@WithoutStatus" };
                DateTime toDate = Convert.ToDateTime(dtp_From.Text);
                for (int i = 0; i < 10; i++)
                    toDate = AddDay(toDate);
                int showDetail = 0; int withoutstatus = 0;
                if (ddl_Courts.SelectedValue != "0" && ddl_Courts.SelectedValue != "1")
                {
                    showDetail = 1;
                }
                else
                {
                    showDetail = Convert.ToInt16(chk_HMCDetail.Checked);
                    if (chk_HMCDetail.Checked)
                    {
                        chk_hidestatus.Visible = true;
                        withoutstatus = Convert.ToInt16(chk_hidestatus.Checked);
                    }
                    else
                    {
                        withoutstatus = 0;
                        chk_hidestatus.Checked = false;
                    }
                }
                object[] values = { dtp_From.Text, toDate, ddl_Courts.SelectedValue, showDetail, withoutstatus };
                dtRecords = clsDb.Get_DT_BySPArr("USP_HTS_CONCENTRATION_REPORT", keys, values);


            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindGridView(DateTime dtStart, DateTime dtEnd, GridView gv, Label lblMessage)
        {
            DataView dv = new DataView(dtRecords);
            dv.RowFilter = "CourtDateMain >= #" + dtStart.ToString() + " # AND CourtDateMain <= #" + dtEnd.ToString() + "#";

            //Aziz 1711
            if ("1" == ddlSort.SelectedValue.ToString())   
                dv.Sort = "CourtID, CourtDateMain";                
                        

            gv.DataSource = dv;
            gv.DataBind();
            if (dv.Count == 0)
                lblMessage.Text = "No records found for " + dtStart.ToShortDateString() + " " + dtStart.DayOfWeek.ToString().ToUpper().Substring(0, 3);
            else
                lblMessage.Text = "";
        }


        private void SetTables()
        {
            try
            {
                DataView dv_firstdate = new DataView(dtRecords);

                DateTime dtStart = Convert.ToDateTime(Convert.ToDateTime(dtp_From.Text).ToShortDateString() + " 12:00 AM");
                DateTime dtEnd = Convert.ToDateTime(dtStart.ToShortDateString() + " 11:59 PM");
                this.BindGridView(dtStart, dtEnd, gv_11, lbl_Message1);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_12, lbl_Message2);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_13, lbl_Message3);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_14, lbl_Message4);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_15, lbl_Message5);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_21, lbl_Message6);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_22, lbl_Message7);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_23, lbl_Message8);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_24, lbl_Message9);

                dtStart = AddDay(dtStart); dtEnd = AddDay(dtEnd);
                this.BindGridView(dtStart, dtEnd, gv_25, lbl_Message10);


                tblGrids.Border = 1;
            }
            catch (Exception ex)
            {
                LblSucessdiv.Attributes.Add("class", "alert alert-danger alert-dismissable fade in");
                LblSucesstext.Text = ex.Message;
                LblSucessdiv.Visible = true;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords();
                SetTables();
                SetGridViewColumns();
                if (hf_CheckBoxSelected.Value == "1")
                {
                    if (chk_HMCDetail.Checked)
                        tblGrids.Width = "1100";
                    else
                        tblGrids.Width = "780";
                }
                else
                    tblGrids.Width = "1100";  
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_11_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                GridView gv = (GridView)sender;
                //to display time

                if (e.Row.RowType == DataControlRowType.Header)
                    return;

                if (gv.Rows.Count != 0)
                {
                    if (e.Row.RowIndex != 0 && e.Row.RowIndex != -1)
                    {
                        if ("0" == ddlSort.SelectedValue)
                        {

                            Label LastTime = (Label)gv.Rows[e.Row.RowIndex - 1].FindControl("lbl_CourtDate");
                            Label CurrentTime = (Label)e.Row.FindControl("lbl_CourtDate");

                            if (Convert.ToDateTime(LastTime.Text).ToShortTimeString() == Convert.ToDateTime(CurrentTime.Text).ToShortTimeString())
                            {
                                CurrentTime.Style[HtmlTextWriterStyle.Display] = "none";
                            }
                        }
                        else
                        {
                            Label LastCourt = (Label)gv.Rows[e.Row.RowIndex - 1].FindControl("lbl_CourtName");
                            Label CurrentCourt = (Label)e.Row.FindControl("lbl_CourtName");

                            if (LastCourt.Text == CurrentCourt.Text)
                            {
                                CurrentCourt.Style[HtmlTextWriterStyle.Display] = "none";

                                Label LastTime = (Label)gv.Rows[e.Row.RowIndex - 1].FindControl("lbl_CourtDate");
                                Label CurrentTime = (Label)e.Row.FindControl("lbl_CourtDate");

                                if (Convert.ToDateTime(LastTime.Text).ToShortTimeString() == Convert.ToDateTime(CurrentTime.Text).ToShortTimeString())
                                {
                                    CurrentTime.Style[HtmlTextWriterStyle.Display] = "none";
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                }


                //Aziz Task 1711
                this.InsertLineBreakOnCourtChange(e);
                //Aziz End

                if (chk_hidestatus.Checked == true)
                {
                    Label status = (Label)e.Row.FindControl("lbl_Status");
                    if (status != null)
                        status.Text = "";
                }


                LinkButton lbtn_Attorney = (LinkButton)e.Row.FindControl("lbtn_Attorney");
                if (lbtn_Attorney != null)
                {
                    HiddenField CourtDate = (HiddenField)e.Row.FindControl("hf_CourtDate");
                    HiddenField CourtID = (HiddenField)e.Row.FindControl("hf_CourtID");
                    Label CourtRoom = (Label)e.Row.FindControl("lbl_CourtNum");
                    HiddenField ViolationID = (HiddenField)e.Row.FindControl("hf_ViolationID");
                    lbtn_Attorney.Attributes.Add("onClick", "return ShowPopup(\'" + CourtDate.Value + "\',\'" + CourtID.Value + "\',\'" + CourtRoom.Text + "\',\'" + ViolationID.Value + "\')");
                    if (gv.HeaderRow.Cells.Count > 3)
                    {
                        gv.HeaderRow.Cells.RemoveAt(1);
                        gv.HeaderRow.Cells.RemoveAt(1);



                    }
                    gv.HeaderRow.Cells[0].ColumnSpan = 3;
                    gv.HeaderRow.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    //if (chk_HMCDetail.Checked)
                    {
                        Label time = (Label)e.Row.FindControl("lbl_CourtName");
                        //http://newpakhouston.legalhouston.com/quickentry/PrintTrialDocket.aspx?courtloc=None&courtdate=10/24/2007&page=1&courtnumber=&datetype=0&RecdType=0
                        //"<a href=quickentry/trialdockets.aspx?sMenu=15> go totrial docket</a>" + Convert.ToDateTime(CourtDate.Value).ToShortDateString();
                        string crtdate = Convert.ToDateTime(CourtDate.Value).ToShortDateString();

                        gv.HeaderRow.Cells[0].Text = "<a href=http://" + Request.ServerVariables["SERVER_NAME"] + "/Reports/TrialDocketCR.aspx?courtloc=None&courtdate=" + crtdate + "&page=1&courtnumber=&datetype=0&singles=0>" + crtdate;


                    }
                    //else
                    //  gv.HeaderRow.Cells[1].Text = Convert.ToDateTime(CourtDate.Value).ToShortDateString() + " " + Convert.ToDateTime(CourtDate.Value).DayOfWeek.ToString().Substring(0, 3).ToUpper();
                }


            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void gv_11_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GetRecords();
                SetTables();
                SetGridViewColumns();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private DateTime AddDay(DateTime dt)
        {
            dt = dt.AddDays(1);
            if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
            {
                dt = AddDay(dt);
            }
            return dt;
        }

        private void SetGridViewColumns()
        {
            try
            {
                //if (ddl_Courts.SelectedValue != "0" && ddl_Courts.SelectedValue != "1")
                if (hf_CheckBoxSelected.Value == "1")
                {
                    if (td_checkBox.Style["Display"] != null)
                    {
                        if (td_checkBox.Style["Display"].ToUpper() == "BLOCK" && chk_HMCDetail.Checked == false)
                        {
                            HideColumns();

                        }
                        else
                            ShowColumns();
                    }
                    else {
                        ShowColumns();

                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                //lbl_Message.Text = ex.Message;
                LblSucessdiv.Attributes.Add("class", "alert alert-danger alert-dismissable fade in");
                LblSucesstext.Text = ex.Message;
                LblSucessdiv.Visible = true;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void ShowColumns(GridView gv)
        {
            if (0 == gv.Rows.Count)
                return;
            gv.Columns[3].Visible = true;
            gv.Columns[5].Visible = true;
        }

        private void ShowColumns()
        {
            this.ShowColumns(gv_11);
            this.ShowColumns(gv_12);

            this.ShowColumns(gv_13);
            this.ShowColumns(gv_14);

            this.ShowColumns(gv_15);
            this.ShowColumns(gv_21);

            this.ShowColumns(gv_22);
            this.ShowColumns(gv_23);

            this.ShowColumns(gv_24);
            this.ShowColumns(gv_25);
        }

        private void HideColumns(GridView gv)
        {
            if (0 == gv.Rows.Count)
                return;
            gv.Columns[3].Visible = false;
            gv.Columns[5].Visible = false;
        }
        private void HideColumns()
        {
            this.HideColumns(gv_11);
            this.HideColumns(gv_12);

            this.HideColumns(gv_13);
            this.HideColumns(gv_14);

            this.HideColumns(gv_15);
            this.HideColumns(gv_21);

            this.HideColumns(gv_22);
            this.HideColumns(gv_23);

            this.HideColumns(gv_24);
            this.HideColumns(gv_25);
        }

        protected void chk_HMCDetail_CheckedChanged(object sender, EventArgs e)
        {
        }

        //Aziz Task 1711
        private void InsertLineBreakOnCourtChange(GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex < 1)
                return;

            if ("0" == ddlSort.SelectedValue.ToString())
                return;

            DataRowView drv = (DataRowView)e.Row.DataItem;
            DataView dv = drv.DataView;

            int currentCourtId = Convert.ToInt32(dv[e.Row.RowIndex]["CourtID"]);
            int previousCourtId = Convert.ToInt32(dv[e.Row.RowIndex - 1]["CourtID"]);
            if (currentCourtId != previousCourtId)
            {
                Label lblRowSeperator = (Label)e.Row.FindControl("lblRowSeperator");
                if (lblRowSeperator != null)
                    lblRowSeperator.Text = "</td></tr><tr><td height=5px colspan=7  style='background-color: White'>&nbsp;&nbsp;</td></tr><tr><td>";
            }
        }
    }
}

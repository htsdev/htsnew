﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

//Waqas 5770 04/09/2009 Created.

namespace HTP.Reports
{
    public partial class AddressValidationReport : System.Web.UI.Page
    {
        #region Variables
        clsENationWebComponents cDb = new clsENationWebComponents();
        clsLogger cBugTracker = new clsLogger();
        clsCase cCase = new clsCase();
        clsSession cSession = new clsSession();
        #endregion

        #region Properties
        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                if (!IsPostBack)
                {
                    GetRecords();
                }
                
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_Data;

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Data.PageIndex = e.NewPageIndex;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                //    if (desc.Text.Length > 10)
                //    {
                //        desc.ToolTip = desc.Text;
                //        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                //    }
                //    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                //    if (desce.Text.Length > 10)
                //    {
                //        desce.ToolTip = desce.Text;
                //        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                //    }                   
                //}
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// To Fill data into Grid
        /// </summary>
        private void GetRecords()
        {
            try
            {
                DataTable dt = cDb.Get_DT_BySPArr("usp_htp_get_AddressValidation");

                if (dt.Rows.Count == 0)
                {
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lbl_Message.Text = "No Records!";
                }
                else
                {
                    dt.Columns.Add("sno");
                    DataView dv = dt.DefaultView;
                    dt = dv.ToTable();
                    GenerateSerialNo(dt); ;
                    gv_Data.DataSource = dt;
                    gv_Data.DataBind();
                    Pagingctrl.PageCount = gv_Data.PageCount;
                    Pagingctrl.PageIndex = gv_Data.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method when Page index changed
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Data.PageIndex = Pagingctrl.PageIndex - 1;
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Data.PageIndex = 0;
                    gv_Data.PageSize = pageSize;
                    gv_Data.AllowPaging = true;
                }
                else
                {
                    gv_Data.AllowPaging = false;
                }
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        /// <summary>
        /// Method to insert Serial numbers in Gridview
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (!dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                cBugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        
        #endregion
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoSignedContractReport.aspx.cs"
    Inherits="HTP.Reports.NoSignedContractReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No Signed Contract Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                        <table style="width: 100%;">
                            <tr>
                                <td align="left" class="clssubhead">
                                    &nbsp;No Signed Contract Report</td>
                                <td style="text-align: right;">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                            <ProgressTemplate>
                                <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                    Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                            </ProgressTemplate>
                        </aspnew:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                    AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                    PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.Ticketid_pk")+"&search=0" %>'
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CauseNumber" HeaderText="Cause Number" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ticketnumber" HeaderText="Ticket Number" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Status" HeaderText="Status" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CourtDate" HeaderText="Court Date" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CourtTime" HeaderText="Court Time" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CourtRoom" HeaderText="Court Room" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
        </TD></TR></TBODY></TABLE></DIV>
    </form>
</body>
</html>

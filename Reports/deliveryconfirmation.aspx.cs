﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using System.Xml;


namespace HTP.Reports
{
    public partial class deliveryconfirmation : System.Web.UI.Page
    {
        clsLogger clog = new clsLogger();
        HTP.Components.clsEDeliverySettings clsedel = new HTP.Components.clsEDeliverySettings();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Noufil 3650 04/30/2008 Get inforamtion about client whose delivery has been confirm depending on thier status
            try
            {
                if (!IsPostBack)
                {
                    fillgrid();
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            }
   
            
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dd_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                fillgrid();
            }
                
            
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void fillgrid()
        {
            DataTable dt = clsedel.getdelconfirmation(Convert.ToInt32(dd_status.SelectedValue));
            if (dt.Rows.Count > 0)
            {
                lbl_message.Visible = false;
                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();

                foreach (GridViewRow gvr in gv_records.Rows)
                {                   
                    string tresponse = ((HiddenField)gvr.FindControl("hf_response")).Value.ToString();
                    XmlDocument doc = new XmlDocument();
                    if (tresponse != "")
                    {
                        doc.LoadXml(tresponse);
                        if (tresponse.Contains("TrackSummary") == true)
                        {
                            string TrackResponse = doc.InnerText.ToString();
                            ((HiddenField)gvr.FindControl("hf_response")).Value = "Track Summary :" + TrackResponse;
                        }
                    }
                    string ControlName = gvr.FindControl("hf_response").NamingContainer.ClientID;
                    ((Label)gvr.FindControl("lbltrack")).Attributes.Add("OnMouseOver", "StateTracePoupup('" + ControlName + "_hf_response" + "');");
                    ((Label)gvr.FindControl("lbltrack")).Attributes.Add("OnMouseOut", "CursorIcon2()");
                    Label lbl = (Label)gvr.FindControl("lbltrack");
                    lbl.Text = "<img src='../Images/screen.jpg' border='0'></img>";
                }
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();

            }
            else
            {
                lbl_message.Visible = true;
                gv_records.Visible = false;
                lbl_message.Text = "No Records Found";
            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            fillgrid();
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }
    }

}

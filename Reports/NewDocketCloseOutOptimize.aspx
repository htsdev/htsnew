﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewDocketCloseOutOptimize.aspx.cs"
    Inherits="HTP.Reports.testpopupoptimize" EnableEventValidation="false" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Docket Close Out</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
        function setDDL() {
            month = document.getElementById('txt_Reset_MM').value = '';
            day = document.getElementById('txt_Reset_DD').value = '';
            year = document.getElementById('txt_Reset_YY').value = '';
            document.getElementById('ddl_Reset_Time').value = '<>';
            document.getElementById('txt_Reset_CourtNum').value = '';
            document.getElementById('ddl_UpdateSelected').value = '1';

        }


        function ValidateDDLSubmit() {
            ValidateRecords();
            var selection = document.getElementById('ddl_UpdateSelected').value;
            var recordsSelected = document.getElementById('hf_SomeRecordsSelected').value;
            var NotToBeDisposed = document.getElementById('hf_NotToBeDisposedCases').value;
            var IsBondCase = document.getElementById("hfIsBondCase");
            var NoBondCase = document.getElementById("hfNoBond");

            if (recordsSelected == '0') {
                alert('Please select some records first');
                return (false);
            }
            //Zeeshan Ahmed 3486 04/01/2008
            if (selection == '2' && IsBondCase.value == "1")
                return confirm("Some of the clients have bonds. Are you sure you want to update their status to Missed Court – No Action. [Press Ok to Yes or Cancel for No]");

            if (selection == '6' && NoBondCase.value == "1") {
                alert("These clients were pled out. Not all of them have bonds. Please unselect the problem records to update.");
                return false;
            }

            if (selection == '3') {
                if (document.getElementById('hf_InsideCourtSelected').value == '1') {
                    var yesNo = confirm('Some of the selected records are not from outside courts, if you continue, those records will not be updated. Do you want to continue?');
                    if (yesNo == true)
                        return (true);
                    else
                        return (false);
                }
                    //ozair 3458 on 03/18/2008 [return true if outside court]
                else
                    return (true);
            }

            if (NotToBeDisposed == '1' && selection == '1') {
                alert('A case with a snap shot status of Arraignment, Arraignment Waiting, Bond or Bond Waiting cannot be disposed from this page.');
                return (false);
            }
        }

        function ValidateDiv() {
            var month = document.getElementById('txt_Reset_MM');
            var day = document.getElementById('txt_Reset_DD');
            var year = document.getElementById('txt_Reset_YY');

            if (month.value == '') {
                alert('Please enter month in the date field');
                month.focus();
                return (false);
            }
            if (day.value == '') {
                alert('Please enter day in the date field');
                day.focus();
                return (false);
            }
            if (year.value == '') {
                alert('Please enter year in the date field');
                year.focus();
                return (false);
            }

            if (isDate(year.value, month.value, day.value) == false) {
                alert('Please enter a valid date');
                month.focus();
                return (false);
            }

            var ddl = document.getElementById('ddl_Reset_Time');
            if (ddl.value == '<>') {
                alert('Please select court time');
                ddl.focus();
                return (false);
            }

            var room = document.getElementById('txt_Reset_CourtNum');
            //ozair 5105 11/19/2008 set to 0 if empty
            if (room.value.trim() == '') {
                room.value = 0;
                document.getElementById('txt_Reset_CourtNum').value = 0;
                //alert('Please enter court number');
                //room.focus();
                //return(false);
            }
            if (isInteger(room.value.trim()) == false) {
                alert('Please enter a valid value for court room');
                room.focus();
                return (false);
            }

            ValidateRecords();
            var recordsSelected = document.getElementById('hf_SomeRecordsSelected').value;

            if (recordsSelected == '0') {
                alert('Please select some records first');
                return (false);
            }

            return (true);

        }

        function SelectionChanged() {
            var selection = document.getElementById('ddl_UpdateSelected').value;
            var divid = 'div_Update';

            if (selection == '4') {

                var IpopTop = (document.body.clientHeight - document.getElementById(divid).offsetHeight) / 2;
                var IpopLeft = (document.body.clientWidth - document.getElementById(divid).offsetWidth) / 2;

                document.getElementById(divid).style.left = IpopLeft + document.body.scrollLeft;
                document.getElementById(divid).style.top = IpopTop + document.body.scrollTop;

                document.getElementById(divid).style.display = "block";
                document.getElementById('ddl_dockets').style.display = "none";
            }
            else {
                document.getElementById(divid).style.display = "none";
                document.getElementById('ddl_dockets').style.display = "block";
            }

        }

        function hideDiv() {
            document.getElementById('div_Update').style.display = "none";
            document.getElementById('ddl_dockets').style.display = "block";
            return (false);
        }

        function poorman_toggle(id) {
            var tr = document.getElementById(id);
            if (tr == null) { return; }
            var bExpand = tr.style.display == '';
            tr.style.display = (bExpand ? 'none' : '');
        }
        function poorman_changeimage(id, sMinus, sPlus) {
            var img = document.getElementById(id);
            if (img != null) {

                var bExpand = img.src.indexOf(sPlus.substring(3, sPlus.length - 3)) >= 0;
                if (!bExpand)
                    img.src = sPlus;
                else
                    img.src = sMinus;
            }
        }

        function ValidateRecords() {
            var hfIsBondCase = document.getElementById("hfIsBondCase");
            var hfNoBond = document.getElementById("hfNoBond");
            hfIsBondCase.value = "0";
            hfNoBond.value = "0";

            var bondflag = '';
            var grid1rows = parseInt(document.getElementById('hf_Grid1_Rows').value);
            var grid2rows = parseInt(document.getElementById('hf_Grid2_Rows').value);
            grid1rows = grid1rows + 1;
            grid2rows = grid2rows + 1;
            var grd = 'gv_records_';
            var court = '';
            var refcasenum = '';
            var sStatusId = '';
            var cases = '';
            var insidecourtflag = '0';
            var somerecordsselected = 0;
            var NotToBeDisposedCases = '0';
            var chkbox = '';



            if (grid1rows > 1) {
                for (i = 2; i <= grid1rows; i++) {
                    if (i < 9) {
                        court = grd + 'ctl0' + i + '_hf_CourtID';
                        refcasenum = grd + 'ctl0' + i + '_hf_TicketsViolationID';
                        chkbox = grd + 'ctl0' + i + '_cb_view';
                        bondflag = grd + 'ctl0' + i + '_lblBondFlag';
                        sStatusId = grd + 'ctl0' + i + '_lbl_sstatus';
                        var crt = document.getElementById(court);
                        var sStatus = document.getElementById(sStatusId);

                        if (crt != 'undefined' && crt != null) {
                            var chk = document.getElementById(chkbox);
                            if (chk != null && chk != 'undefined') {
                                if (chk.checked == true) {
                                    somerecordsselected = 1;
                                    if (crt.value == '3001' || crt.value == '3002' || crt.value == '3003') {
                                        insidecourtflag = '1';
                                    }
                                    else {
                                        cases = cases + document.getElementById(refcasenum).value + ',';
                                    }

                                    // put logic here......
                                    var stemp = sStatus.innerText;
                                    if (stemp.indexOf('A/W') >= 0 || stemp.indexOf('B/W') >= 0 || stemp.indexOf('ARR') >= 0 || stemp.indexOf('BOND') >= 0)
                                        NotToBeDisposedCases = '1';

                                    //Zeeshan Ahmed 3486 03/31/2008
                                    if (document.getElementById(bondflag).innerText == "B") hfIsBondCase.value = "1";
                                    else hfNoBond.value = "1";

                                }
                            }
                        }
                    }
                    else {
                        court = grd + 'ctl' + i + '_hf_CourtID';
                        refcasenum = grd + 'ctl' + i + '_hf_TicketsViolationID';
                        chkbox = grd + 'ctl' + i + '_cb_view';
                        bondflag = grd + 'ctl' + i + '_lblBondFlag';
                        sStatusId = grd + 'ctl' + i + '_lbl_sstatus';
                        var crt = document.getElementById(court);
                        var sStatus = document.getElementById(sStatusId);
                        if (crt != 'undefined' && crt != null) {
                            var chk = document.getElementById(chkbox);
                            if (chk != null && chk != 'undefined')
                                if (chk.checked == true) {
                                    somerecordsselected = 1;
                                    if (crt.value == '3001' || crt.value == '3002' || crt.value == '3003') {
                                        insidecourtflag = '1';
                                    }
                                    else {
                                        cases = cases + document.getElementById(refcasenum).value + ',';
                                    }
                                    // put logic here...
                                    var stemp = sStatus.innerText;
                                    if (stemp.indexOf('A/W') >= 0 || stemp.indexOf('B/W') >= 0 || stemp.indexOf('ARR') >= 0 || stemp.indexOf('BOND') >= 0)
                                        NotToBeDisposedCases = '1';

                                    //Zeeshan Ahmed 3486 03/31/2008
                                    if (document.getElementById(bondflag).innerText == "B") hfIsBondCase.value = "1";
                                    else hfNoBond.value = "1";
                                }
                        }

                    }
                }
            }
            grd = 'gv_records1_';
            court = '';
            refcasenum = '';
            cases = '';
            chkbox = '';
            sStatusId = '';
            if (grid2rows > 1) {
                for (i = 2; i <= grid2rows; i++) {
                    if (i < 9) {
                        court = grd + 'ctl0' + i + '_hf_CourtID';
                        refcasenum = grd + 'ctl0' + i + '_hf_TicketsViolationID';
                        chkbox = grd + 'ctl0' + i + '_cb_view';
                        bondflag = grd + 'ctl0' + i + '_lblBondFlag';

                        //Kazim 4214 6/23/2008 Replace hf_verifiedstatus with lbl_vstatus b/c hiddenfield not exist on the page

                        sStatusId = grd + 'ctl0' + i + '_lbl_vstatus';
                        var crt = document.getElementById(court);
                        var sStatus = document.getElementById(sStatusId);
                        if (crt != 'undefined' && crt != null) {
                            var chk = document.getElementById(chkbox);
                            if (chk != null && chk != 'undefined')
                                if (chk.checked == true) {
                                    somerecordsselected = 1;
                                    if (crt.value == '3001' || crt.value == '3002' || crt.value == '3003') {
                                        insidecourtflag = '1';
                                    }
                                    else {
                                        cases = cases + document.getElementById(refcasenum).value + ',';
                                    }

                                    // put logic here......
                                    var stemp = sStatus.innerText;
                                    if (stemp.indexOf('A/W') >= 0 || stemp.indexOf('B/W') >= 0 || stemp.indexOf('ARR') >= 0 || stemp.indexOf('BOND') >= 0)
                                        NotToBeDisposedCases = '1';

                                    //Zeeshan Ahmed 3486 03/31/2008
                                    if (document.getElementById(bondflag).innerText == "B") hfIsBondCase.value = "1";
                                    else hfNoBond.value = "1";

                                }
                        }
                    }
                    else {
                        court = grd + 'ctl' + i + '_hf_CourtID';
                        refcasenum = grd + 'ctl' + i + '_hf_TicketsViolationID';
                        bondflag = grd + 'ctl' + i + '_lblBondFlag';
                        chkbox = grd + 'ctl' + i + '_cb_view';
                        sStatusId = grd + 'ctl' + i + '_lbl_vstatus';
                        var crt = document.getElementById(court);
                        var sStatus = document.getElementById(sStatusId);
                        if (crt != 'undefined' && crt != null) {
                            var chk = document.getElementById(chkbox);
                            if (chk != null && chk != 'undefined')
                                if (chk.checked == true) {
                                    somerecordsselected = 1;
                                    if (crt.value == '3001' || crt.value == '3002' || crt.value == '3003') {
                                        insidecourtflag = '1';
                                    }
                                    else {
                                        cases = cases + document.getElementById(refcasenum).value + ',';
                                    }

                                    // put logic here.....
                                    var stemp = sStatus.innerText;
                                    if (stemp.indexOf('A/W') >= 0 || stemp.indexOf('B/W') >= 0 || stemp.indexOf('ARR') >= 0 || stemp.indexOf('BOND') >= 0)
                                        NotToBeDisposedCases = '1';

                                    //Zeeshan Ahmed 3486 03/31/2008
                                    if (document.getElementById(bondflag).innerText == "B") hfIsBondCase.value = "1";
                                    else hfNoBond.value = "1";
                                }
                        }

                    }
                }
            }
            if (insidecourtflag != '0')
                document.getElementById('hf_InsideCourtSelected').value = '1';
            else
                document.getElementById('hf_InsideCourtSelected').value = '0';

            if (somerecordsselected != 0)
                document.getElementById('hf_SomeRecordsSelected').value = '1';
            else
                document.getElementById('hf_SomeRecordsSelected').value = '0';

            if (NotToBeDisposedCases != '0')
                document.getElementById('hf_NotToBeDisposedCases').value = '1';
            else
                document.getElementById('hf_NotToBeDisposedCases').value = '0';
        }

        function dispose_validation() {
            if (document.getElementById('chk_DisposeAll').checked == false) {
                alert('Please select the check box and then click submit to dispose');
                return (false);
            }
            else
                return (true);
        }

        function search_validation() {
            if (document.getElementById('chk_RedX').checked == false
                && document.getElementById('chk_QuestionMark').checked == false
                    && document.getElementById('chk_YellowCheck').checked == false
                        && document.getElementById('chk_GreenCheck').checked == false) {
                alert('Please select at least one flag to retrieve data');
                document.getElementById('chk_RedX').focus();
                return (false);
            }
            else {
                return (true);
            }
        }

        function PopUpCallBack(DocID) {
            window.open("../Activities/processPDF.aspx?docid=" + DocID);
            return false;
        }

        function close_docket_popup() {
            document.getElementById("ddl_dockets").style.visibility = ''
            document.getElementById("ddl_status").style.visibility = ''
            document.getElementById("ddl_courts").style.visibility = ''
            document.getElementById("pnl_Dockets").style.display = 'none';
        }

        function setpopuplocation() {

            var top = 400;
            var left = 400;
            var height = document.body.offsetHeight;
            var width = document.body.offsetWidth

            var resolution = width + "x" + height;

            if (width > 1100 || width <= 1280)
                left = 575

            if (width < 1100)
                left = 500;

            // Setting popup display
            document.getElementById("tbl_updatepanel").style.top = top * 2;
            document.getElementById("tbl_updatepanel").style.left = left * 2;
            document.getElementById("tbl_updatepanel").style.display = 'block'

        }

        function ShowProgress() {

            // document.getElementById("tbl_Data").style.display='none'
            document.getElementById("tbl_image").style.display = 'block'
        }

        //        function HidePanel()
        //        {
        //         document.getElementById('pnl_Update').style.display='none';
        //        }
        
    </script>



    <style type="text/css">
        .treetable {
        }

            .treetable th {
                FONT-SIZE: 17pt;
            }

            .treetable td {
                FONT-SIZE: 7pt;
                FONT-FAMILY: Tahoma;
                border-color: 3366cc;
            }


        a {
            text-decoration: none;
            color: #090;
        }

        div.popup {
            width: 300px;
            height: 100px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -150px;
            margin-top: -50px;
            z-index: 99;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

</head>
<body onload="setDDL();">
    <form id="form1" runat="server">
        <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            </aspnew:ScriptManager>
            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
            <section id="main-content" class="newDocketCloseOutOptimize" id="TableMain">
                
                  <section class="wrapper main-wrapper row" id="" style="">
<div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                <div class="col-xs-12">

                           <asp:CheckBox ID="chk_DisposeAll" runat="server" Text="Dispose 'All Open' HMC Cases"
                            Visible="False" /><asp:Button ID="btn_Dispose" runat="server" Text="Submit" CssClass="clsbutton"
                                OnClick="btn_Dispose_Click" OnClientClick="return dispose_validation()" Visible="False" />
                        &nbsp;&nbsp;
                        <asp:HiddenField ID="hfIsBondCase" runat="server" Value="0" /><asp:HiddenField ID="hfNoBond" runat="server" Value="0" />
                          
                           <div class="alert alert-danger alert-dismissable fade in" id="dvMessagebox" runat="server" Visible="false" >
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                </div>

                                  <div align="right" style="height: 20px; display: none">
                        <asp:HiddenField ID="hf_Grid1_Rows" Value="0" runat="server" />
                        <asp:HiddenField ID="hf_Grid2_Rows" Value="0" runat="server" />
                        <asp:HiddenField ID="hf_InsideCourtSelected" runat="server" Value="0" />
                        <asp:HiddenField ID="hf_SomeRecordsSelected" runat="server" Value="0" />
                        <asp:HiddenField ID="hf_NotToBeDisposedCases" runat="server" Value="0" />
                        <asp:Button ID="btn_DisposeSelected" runat="server" CssClass="clsbutton" OnClick="btn_DisposeSelected_Click"
                            Text="Dispose Selected Cases" Visible="False" />
                        <asp:Button ID="btn_updatestatus" runat="server" CssClass="clsbutton" OnClick="btn_updatestatus_Click"
                            Text="Update Selected Verified Court Date with Auto" Visible="False" /></div>
                           <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="ibtn_Docs"
                            PopupControlID="pnl_Dockets" CancelControlID="lbtn_Docket_Close" OnCancelScript="close_docket_popup()"
                            HideDropDownList="false">
                          </cc1:ModalPopupExtender>
                       </div>
                      <div class="clearfix"></div>
                      <div class="col-xs-12">
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">Docket Close Out</h1><!-- PAGE HEADING TAG - END -->                           

                            </div>
                            </div>

                      </div>
                        <div class="col-xs-12">
                            <section class="box ">
                              <header class="panel_header">
             
                <h2 class="title pull-left">Filters</h2>
                 
               
                <div class="actions panel_actions remove-absolute pull-right">
                
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <%--<a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>--%>
                    <%--<a class="box_close fa fa-times"></a>--%>
                </div>
                    
            </header>
                                <div class="content-body checkBoxesIcons">
                                    <div class="row">
                                         <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label">Date</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <picker:datepicker id="cal_Date" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                       <%--<ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                                    Width="86px" EnableHideDropDown="True">
                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                    <TextboxLabelStyle CssClass="clsinputadministration" />
                                                </ew:CalendarPopup>--%>
                                                    </div>
                                              </div>
                      
                                         </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                    <label class="form-label"></label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:CheckBox ID="cb_showall" runat="server" Checked="True" CssClass="clslabel" Text="Show All"
                                                    Visible="False" />
                                                    </div>
                                              </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                    <label class="form-label"></label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                       <asp:CheckBox ID="chk_RedX" runat="server" Checked="True" Text='<img src="../Images/remove2.gif"/>' CssClass="checkbox-custom"  />
                           
                                                    </div>
                                              </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                    <label class="form-label"></label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:CheckBox ID="chk_QuestionMark" runat="server" Checked="True" Text='<img src="../Images/questionmark.gif" />' CssClass="checkbox-custom"  />
                           
                                                    </div>
                                              </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                    <label class="form-label"></label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:CheckBox ID="chk_YellowCheck" runat="server" Text='<img src="../Images/yellow_check.gif"/>' CssClass="checkbox-custom"  />
                           
                                                    </div>
                                              </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                    <label class="form-label"></label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:CheckBox ID="chk_GreenCheck" runat="server" Text='<img src="../Images/right.gif"/>'  CssClass="checkbox-custom"  />
                           
                                                    </div>
                                              </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr />
                                        <div class="row">
                                        	<div class="col-md-12">
                                            	<div class="pull-right">
                                         <asp:Button ID="btn_Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btn_Submit_Click"
                            OnClientClick="return search_validation()" />                                                
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                            <asp:Panel id="tbl_sort" runat="server" visible="false">
                                <section class="box">
                                  <header class="panel_header">
                
                                    <asp:ImageButton ID="ibtn_Docs" runat="server" ImageUrl="~/Images/head_icon.gif" Visible="false" />
                                    <h2 class="title pull-left"><asp:Label ID="lbl_Title" runat="server" Font-Bold="True" ></asp:Label></h2>
               
                                    <div class="actions panel_actions remove-absolute pull-right">
                                        <asp:LinkButton ID="lnkbtn_showall" runat="server" OnClick="lnkbtn_showall_Click">View All Closed Dockets with Discrepancies</asp:LinkButton></td>
                	                    <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                    
                                </header>
                                   <div class="content-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label">Court Location</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <asp:DropDownList ID="ddl_courts" runat="server" CssClass="form-control"
                                                            AutoPostBack="True" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                              </div>
                      
                                         </div>
                                        <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label"> Auto Statuses</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control"
                                                        AutoPostBack="True" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
                                                        <asp:ListItem Value="-1">ALL</asp:ListItem>
                                                        <asp:ListItem Value="1">DLQ/FTA</asp:ListItem>
                                                        <asp:ListItem Value="2">Appearance</asp:ListItem>
                                                        <asp:ListItem Value="3">Disposed</asp:ListItem>
                                                        <asp:ListItem Value="4">NIR</asp:ListItem>
                                                        <asp:ListItem Value="5">Blank</asp:ListItem>
                                                        </asp:DropDownList>
                                              </div>
                      
                                         </div>
                                    </div>
                                        <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label">Dockets</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <asp:DropDownList ID="ddl_dockets" runat="server" CssClass="form-control"
                                                            Width="150px">
                                                        </asp:DropDownList>
                                              </div>
                      
                                         </div>
                                    </div>
                                   </div>
                              </section>
                             </asp:Panel>
                                <div id="resultsection" runat="server" visible="false">

                              <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">

                            <ContentTemplate>

                                 <table id="tbl_image" style="display: none" runat="server">
                                                    <tr>
                                                        <td align="center" class="clslabel">
                                                            <img src="../Images/plzwait.gif" /><strong> Please wait while your request is being
                                                                processed </strong>
                                                        </td>
                                                    </tr>
                                                </table>
                            <section class="box">
                                  <header class="panel_header">
                
                                    <h2 class="title pull-left">List</h2>
               
                                    <div class="actions panel_actions remove-absolute pull-right">
                                        
                	                    <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                    
                                </header>
                                   <div class="content-body">
                                    <div class="row">
                                       <div class="col-md-12">
                                            <div class="table-responsive" data-pattern="priority-columns">
                                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_records_RowDataBound"
                                                    Width="100%" OnRowCommand="gv_records_RowCommand" OnRowDeleting="gv_records_RowDeleting"
                                                    OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20" CssClass="table table-small-font table-bordered table-striped">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# Eval("SNo") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HyperLink ID="hl_sno" runat="server" Text='<%# Eval("SNo") %>' NavigateUrl='<%# "../ClientInfo/CaseDisposition.aspx?search=0&casenumber=" +Eval("TicketID_pk") %>'></asp:HyperLink>
                                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' />
                                                                <asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' />
                                                                <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                                                <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' />
                                                                <asp:HiddenField ID="hf_discrepency" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.discrepency") %>' />
                                                                <asp:HiddenField ID="hf_showquestionmark" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.showquestionmark") %>' />
                                                                <asp:HiddenField ID="hf_TicketsViolationID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>' />
                                                                <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID") %>' />
                                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:D}")%>' />
                                                                <%--Zeeshan Haider 06/17/2013 11008 Validation with auto status--%>
                                                                <asp:HiddenField ID="hf_AutoCourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "autocourtdate", "{0:D}")%>' />
                                                                <asp:HiddenField ID="hf_AutoStatusID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "autostatusid", "{0:D}")%>' />
                                                                <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HiddenField ID="hf_MM" runat="server" />
                                                                <asp:HiddenField ID="hf_DD" runat="server" />
                                                                <asp:HiddenField ID="hf_YY" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Bond" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBondFlag" runat="server" Text="" Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    
                                                        <asp:TemplateField HeaderText="Last Name ,First Name">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="250px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("LastName") + " , " +  Eval("FirstName")%>'
                                                                    Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause No">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="70px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_causeno" runat="server" Text='<%# Eval("CauseNo") %>' Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cb_view" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" Width="18px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Snap Shot Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Auto Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_astatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "AutoStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Verified Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_vstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Image ID="img_dis" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" Width="18px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                </asp:GridView>
                                                <asp:GridView ID="gv_records1" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_records_RowDataBound"
                                                    Width="100%" OnRowCommand="gv_records_RowCommand" OnRowDeleting="gv_records_RowDeleting"
                                                    OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20" CssClass="table table-small-font table-bordered table-striped">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# Eval("SNo") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HyperLink ID="hl_sno" runat="server" Text='<%# Eval("SNo") %>' NavigateUrl='<%# "../ClientInfo/CaseDisposition.aspx?search=0&casenumber=" +Eval("TicketID_pk") %>'></asp:HyperLink>
                                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtID") %>' />
                                                                <asp:HiddenField ID="hf_BondFlag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "BondFlag") %>' />
                                                                <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                                                <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketID_PK") %>' />
                                                                <asp:HiddenField ID="hf_discrepency" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.discrepency") %>' />
                                                                <asp:HiddenField ID="hf_showquestionmark" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.showquestionmark") %>' />
                                                                <asp:HiddenField ID="hf_TicketsViolationID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>' />
                                                                <asp:HiddenField ID="hf_RecordID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RecordID") %>' />
                                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:D}")%>' />
                                                                <asp:Label ID="lbl_CourtNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_LastUpdate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdate")%>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:HiddenField ID="hf_MM" runat="server" />
                                                                <asp:HiddenField ID="hf_DD" runat="server" />
                                                                <asp:HiddenField ID="hf_YY" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Bond" HeaderStyle-Font-Size="XX-Small">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBondFlag" runat="server" Text="" Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    
                                                        <asp:TemplateField HeaderText="Last Name ,First Name">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="250px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("LastName") + " , " +  Eval("FirstName")%>'
                                                                    Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause No">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="70px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_causeno" runat="server" Text='<%# Eval("CauseNo") %>' Font-Size="XX-Small"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cb_view" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" Width="18px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Snap Shot Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_sstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "SnapShotStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Auto Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_astatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "AutoStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Verified Status">
                                                            <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="200px" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_vstatus" runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "VerifiedStatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Image ID="img_dis" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" Width="18px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                </asp:GridView>    
                                            </div>
                                       </div>
                                   </div>
                              </section>
                                </ContentTemplate>
                       
                        </aspnew:UpdatePanel>
                                    </div>

                        <asp:Panel id="td_Options" runat="server" Visible="false">
                                <div id="div_Update" style="display:block">
                            <section class="box" >
                                  <header class="panel_header">
                
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/head_icon.gif" Visible="false"/>
                                    <h2 class="title pull-left">Bulk Reset</h2>
               
                                    <div class="actions panel_actions remove-absolute pull-right">
                                          <asp:Label ID="Label1" runat="server" CssClass="form-label" Text="Update Selected:"></asp:Label>
                        <asp:DropDownList ID="ddl_UpdateSelected" runat="server" CssClass="form-control inline-textbox"
                            onChange="SelectionChanged()">
                            <asp:ListItem Value="1">Dispose</asp:ListItem>
                            <asp:ListItem Value="2">Missed Court - No Action</asp:ListItem>
                            <asp:ListItem Value="6">Missed Court - Pled Out</asp:ListItem>
                            <asp:ListItem Value="3">Waiting</asp:ListItem>
                            <asp:ListItem Value="4">Reset</asp:ListItem>
                            <asp:ListItem Value="5">Update Verified Court Date with Auto</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btn_UpdateSelected" OnClientClick="return ValidateDDLSubmit();" runat="server"
                            CssClass="btn btn-primary" OnClick="btn_UpdateSelected_Click" Text="Update" />
                        &nbsp;
                	                    <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                    
                                </header>
                                   <div class="content-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label">Court Date</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <asp:TextBox ID="txt_Reset_MM" CssClass="form-control inline-textbox" onkeyup="return autoTab(this, 2, event)"
                                                        runat="server" Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_Reset_DD" CssClass="form-control  inline-textbox"
                                                            onkeyup="return autoTab(this, 2, event)" runat="server" Width="25px"></asp:TextBox>/<asp:TextBox
                                                                ID="txt_Reset_YY" runat="server" CssClass="form-control  inline-textbox" Width="25px"></asp:TextBox>
                                                    </div>
                                              </div>
                      
                                         </div>
                                        <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label">Court Time</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <asp:DropDownList ID="ddl_Reset_Time" runat="server" CssClass="form-control">
                                                        <asp:ListItem>&lt;&gt;</asp:ListItem>
                                                        <asp:ListItem>8:00 AM</asp:ListItem>
                                                        <asp:ListItem>8:15 AM</asp:ListItem>
                                                        <asp:ListItem>8:30 AM</asp:ListItem>
                                                        <asp:ListItem>8:45 AM</asp:ListItem>
                                                        <asp:ListItem>9:00 AM</asp:ListItem>
                                                        <asp:ListItem>9:15 AM</asp:ListItem>
                                                        <asp:ListItem>9:30 AM</asp:ListItem>
                                                        <asp:ListItem>9:45 AM</asp:ListItem>
                                                        <asp:ListItem>10:00 AM</asp:ListItem>
                                                        <asp:ListItem>10:15 AM</asp:ListItem>
                                                        <asp:ListItem>10:30 AM</asp:ListItem>
                                                        <asp:ListItem>10:45 AM</asp:ListItem>
                                                        <asp:ListItem>11:00 AM</asp:ListItem>
                                                        <asp:ListItem>11:15 AM</asp:ListItem>
                                                        <asp:ListItem>11:30 AM</asp:ListItem>
                                                        <asp:ListItem>11:45 AM</asp:ListItem>
                                                        <asp:ListItem>12:00 PM</asp:ListItem>
                                                        <asp:ListItem>12:15 PM</asp:ListItem>
                                                        <asp:ListItem>12:30 PM</asp:ListItem>
                                                        <asp:ListItem>12:45 PM</asp:ListItem>
                                                        <asp:ListItem>1:00 PM</asp:ListItem>
                                                        <asp:ListItem>1:15 PM</asp:ListItem>
                                                        <asp:ListItem>1:30 PM</asp:ListItem>
                                                        <asp:ListItem>1:45 PM</asp:ListItem>
                                                        <asp:ListItem>2:00 PM</asp:ListItem>
                                                        <asp:ListItem>2:15 PM</asp:ListItem>
                                                        <asp:ListItem>2:30 PM</asp:ListItem>
                                                        <asp:ListItem>2:45 PM</asp:ListItem>
                                                        <asp:ListItem>3:00 PM</asp:ListItem>
                                                        <asp:ListItem>3:15 PM</asp:ListItem>
                                                        <asp:ListItem>3:30 PM</asp:ListItem>
                                                        <asp:ListItem>3:45 PM</asp:ListItem>
                                                        <asp:ListItem>4:00 PM</asp:ListItem>
                                                        <asp:ListItem>4:15 PM</asp:ListItem>
                                                        <asp:ListItem>4:30 PM</asp:ListItem>
                                                        <asp:ListItem>4:45 PM</asp:ListItem>
                                                        <asp:ListItem>5:00 PM</asp:ListItem>
                                                        <asp:ListItem>5:15 PM</asp:ListItem>
                                                        <asp:ListItem>5:30 PM</asp:ListItem>
                                                        <asp:ListItem>5:45 PM</asp:ListItem>
                                                        <asp:ListItem>6:00 PM</asp:ListItem>
                                                        <asp:ListItem>6:15 PM</asp:ListItem>
                                                        <asp:ListItem>6:30 PM</asp:ListItem>
                                                        <asp:ListItem>6:45 PM</asp:ListItem>
                                                        <asp:ListItem>7:00 PM</asp:ListItem>
                                                        <asp:ListItem>7:15 PM</asp:ListItem>
                                                        <asp:ListItem>7:30 PM</asp:ListItem>
                                                        <asp:ListItem>7:45 PM</asp:ListItem>
                                                        <asp:ListItem>8:00 PM</asp:ListItem>
                                                        <asp:ListItem>8:15 PM</asp:ListItem>
                                                        <asp:ListItem>8:30 PM</asp:ListItem>
                                                        <asp:ListItem>8:45 PM</asp:ListItem>
                                                        <asp:ListItem>9:00 PM</asp:ListItem>
                                                    </asp:DropDownList>
                                              </div>
                      
                                         </div>
                                    </div>
                                        <div class="col-md-3">
                                              <div class="form-group">
                                                    <label class="form-label">Room</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                         <asp:TextBox ID="txt_Reset_CourtNum" runat="server" Width="28px"></asp:TextBox>

                                              </div>
                      
                                         </div>
                                    </div>
                                        <div class="clearfix"></div>
                                        <hr />
                                        <asp:Button ID="Button2" runat="server" CssClass="btn btn-primary" OnClick="btn_Reset_Update_Click"
                                                        Text="Update" OnClientClick="return ValidateDiv()" />
                                                    <asp:Button ID="Button3" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="return hideDiv()" />
                                   </div>
                              </section>
                                </div>
                            </asp:Panel>
        </div>
        </section>
                  
              </section>
          </div>
        <div>

            <asp:Panel ID="pnl_Dockets" runat="server" Height="50px" Style="display: none">
                <table width="100%" style="left: 400px; position: absolute; top: 400px;" border="1"
                    class="clsLeftPaddingTable">
                    <tr>
                        <td width="100%" align="right" background="../Images/subhead_bg.gif" height="34"
                            class="clssubhead">
                            <asp:LinkButton ID="lbtn_Docket_Close" runat="server">X</asp:LinkButton>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gv_ScannedDockets" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                                OnRowDataBound="gv_ScannedDockets_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Docket Date">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtn_DocketDate" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dateofdoc", "{0:d}") %>'></asp:LinkButton>
                                            <asp:Label ID="lbl_docid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doc_id") %>'
                                                Visible="false">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Scan Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ScanDate" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntered") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Attorney">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Attorney" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Importance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pages">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Pages" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DocNum") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_Popup_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

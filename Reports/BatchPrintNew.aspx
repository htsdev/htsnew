﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="BatchPrintNew.aspx.cs"
    Inherits="HTP.Reports.BatchPrintNew" %>

<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Batch Print</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../ErrorLog/BoxOver.js" type="text/javascript"></script>

    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
    

   function ShowDatesTable()
	    {     	        
	        if(document.getElementById("chk_ShowPrintedLetters").checked ==true)
	                document.getElementById("trdate").style.display ='block';
	        else if(document.getElementById("chk_ShowPrintedLetters").checked ==false)
	                document.getElementById("trdate").style.display ='none';	       	        
	    }
	    
	   
		
		// Noufil 4080 05/20/2008 Dropdown use for check and uncheck
		function checkmake(tp)
		{
		    var dd=document.getElementById("dd_state");
		    var grid=document.getElementById("dg_batchtrial");
            var rows=grid.rows.length;
		    var colIndex = 7;
            
            if(document.getElementById("chk_ShowPrintedLetters").checked ==true)
            {
                colIndex = 8;
            }
            
		    if (dd.value==0)
		    {   
		        for ( i = 0 ; i<rows ; i++)
                {      
                    grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=true;
                }
		    }
		    
		    if (dd.value==3)
		    {
		       for ( i = 0 ; i<rows ; i++)
                {      
                    grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;
                }
		    }
		    
		    if (dd.value==2)
		    {
		    
		         for ( i = 0 ; i<rows ; i++)
                 {                   	               
	                var cell = grid.rows[i].cells[6].innerText;
                   
                   if (cell == 'N ') 
                        grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=true;                
                   else
                        grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;  
                 }  
		    }
		    
		    if (dd.value==1)
		    {
		         for ( i = 0 ; i<rows ; i++)
                 {                   	               
	                var cell = grid.rows[i].cells[6].innerText;
                   
                   if (cell == 'Y ') 
                        grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=true;                
                   else
                        grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;  
                 }  
		    }
		}
		
	    
    
//        function checkboxtemp(tp,rbname)   
//        {
//         var colIndex = 7;
//         if(document.getElementById("chk_ShowPrintedLetters").checked ==true)
//         {
//            colIndex = 8;
//         }
//         else
//         {
//            colIndex = 7;
//         }
//         if (document.getElementById(rbname).selectedvalue==1)
//         {
//            var grid=document.getElementById("dg_batchtrial");
//            var rows=grid.rows.length;
//            for ( i = 0 ; i<rows ; i++)
//            {                   	               
//	                var cell = grid.rows[i].cells[6].innerText;
//                        
//                        if (cell == tp + " ") 
//                         grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=true;                
//                        else
//                           grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;  
//          
//            }
//            
//           }
//           if (document.getElementById(rbname).checked==false)
//         {
//            var grid=document.getElementById("dg_batchtrial");
//            var rows=grid.rows.length;
//            for ( i = 0 ; i<rows ; i++)
//            {                   	               
//	                var cell = grid.rows[i].cells[6].innerText;
//                        
//                        if (cell == tp + " ") 
//                         grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;                
//                        else
//                           grid.rows[i].cells[colIndex].getElementsByTagName("INPUT")[0].checked=false;  
//          
//            }
//            
//           }
//        }

//	    function checkbox(tp,rbname)
//	    {	            
//	            checkboxtemp(tp,rbname);	           
//        }
       
		
		//Print records 
		function PrintLetter(dgname,count,lettertype)
		{
	
	      
			var tkiid;
			var batch;
			var chked;
			var isprint;
			var batchdates;
			var splitflag;
			var splitflagmessage = false;
            //Fahad 5071 11/19/2008//
			var isEmailTrialFlag;
			var isEmailSent=false;
			
			// trial letter.....
			
		     tkiid = "_lbl_tictrial";
		     batch = "_lbl_batchid";
		     //Aziz 3220 02/22/08 control changed
		     chked="_chb";				
		     isprint="_txt_tpnt";
		     batchdates="_lbl_batchdate";
		     splitflag = "_lbl_IsSplit";
		     
		     //Fahad 5071 11/19/2008//
             isEmailTrialFlag="_hfEmailFlag1";				
						
			var cnt = count;
			var idx = 2;
			cnt = cnt + 2;			
			var StrTid = "";
			var StrDate = "";
			var print=0*1;
		    var strticktbatch="";
			
			
			for (idx=2;idx<cnt;idx++)
			{			
			    var chk  = "";
			    var tkid = "";
			    var batchNm = "";
			    var printid="";
			    var bdate="";
			    var split="";
			    //Fahad 5071 11/19/2008//
			    var isETF="";
			    
			    
			    var _ctl = "";    			
			    if( idx < 10 )
			        _ctl= "_ctl0";			    			    
			    else		    
			        _ctl= "_ctl";
			    
			       
			    chk  = dgname+ _ctl + idx + chked;
		        tkid = dgname+ _ctl + idx + tkiid;
		        batchNM = dgname+ _ctl + idx + batch ;
		        printid=dgname+ _ctl + idx +isprint ;
	            bdate=dgname+ _ctl + idx +batchdates ;
	            split = dgname+ _ctl + idx + splitflag;
	            //Fahad 5071 11/19/2008//
    		    isETF = dgname+ _ctl + idx + isEmailTrialFlag;	
    		    		
				var GetChk = document.getElementById(chk);
				if (GetChk.checked == true)
				{
				    var GetTiD = document.getElementById(tkid);
                    var GetBatchid = document.getElementById(batchNM);
				    
				   //Zeeshan Ahmed 3268 Remove Receipt Letter Type Code
			        var GetSplit = document.getElementById(split);
				    if (GetSplit.innerText != "1")
			        {
			            strticktbatch = strticktbatch + GetTiD.innerText + GetBatchid.innerText  +",";
			            StrTid = StrTid  + GetTiD.innerText + ",";
			            //Aziz 3220 02/22/08 no need of date				     
			            var getprint=document.getElementById(printid);
			            print=print+getprint.value*1;				            
			        }
			        else
			        {
			            splitflagmessage = true;
			        }
			         //Fahad 5071 11/19/2008//
			        if(document.getElementById("chk_ShowPrintedLetters").checked ==true && dgname == "dg_batchtrial")
			        {
			            var etf=document.getElementById(isETF);
			            if (etf.value=="1")
			            {
			                isEmailSent=true;
			            }
			        }
				}
			}
			 //Fahad 5071 11/19/2008//
			if (StrTid != "")		
		    {
			    if(print!=0)
			    {
				    doyou=confirm("Selected letter(s) are already printed. Do you want to print them again?"  )	
				    if(doyou==false)
				    {			
					    return false;
				    }
				    else if (isEmailSent)
				    {
				        doyou=confirm("Selected Trial Notification email has already been sent to client do you want to send the email again?"  )
				        if(doyou==false)
				        {			
					        document.getElementById("hfEmailStatus").value=0;   
				        }	
				        else
				        {
				            document.getElementById("hfEmailStatus").value=1;
				        }
				    }
				}   
			
			    if (splitflagmessage)
			    {
			        alert("Some of the selected letters have split setting information and couldn't be printed.\nPlease check these cases on violation fee page.");
			    }

                document.Form1.txt_TicketIDs.value = StrTid;
                document.Form1.txt_TicketIDsbatch.value = strticktbatch;  
                //Aziz 3220 02/22/08 no need of date
                //document.Form1.txt_BatchDates.value  = StrDate;
                document.Form1.txt_LetterType.value = lettertype;
                
                if (StrTid.length > 8000)
                {
                    alert('Too many letters selected to print. Please select at most 1000 letters at a time.');
                    return false;
                }           
		    }
		    else
		    {		
		        if (splitflagmessage)
			    {
			        alert("Selected letter(s) have split setting information and couldn't be printed.\nPlease check these cases on violation fee page.");
			        return false;
			    }
			    else
			    {
        		    alert("Please select at least one record.");
	        	    return false;
	    	    }
		    }	
		  
		    //Aziz 3220 02/22/08 changed to server side handler
		    //alert(StrTid);
		    //alert(strticktbatch);
			//window.open("PreviewBatch.aspx?Ticketid="+StrTid+"&TicketidBatch="+strticktbatch+"&Lettertype="+lettertype,"");
            //return false;
	}

	//When selected Delete is clicked
	function PromptDelete(dgname,count,lettertype)
	{
		var cnt = count;		
		cnt = cnt + 2;
		var idx = 2;
		var run=0;
		var chked;
		
		//Zeeshan Ahmed 3268 Remove Receipt Letter Type Code
		chked="_chb";				 
				
		for (idx=2;idx<cnt;idx++)
			{
			var chk = "";
			if(idx<10)
			{
			     chk = dgname+ "_ctl0" + idx + chked;
			}
			else
			{
			    chk = dgname+ "_ctl" + idx + chked;
			}
			var GetChk = document.getElementById(chk);
				if (GetChk.checked == true)
					{
						run=run+1;						
					}
			}				
		if (run==0)
		{
			alert("Please select at least one record.");
			return false;	
		}
		else
		{
		
		    //$find("AEFadeIn").get_OnClickBehavior().play();                        
             //$find("MPExtConfirm").show();
//			doyou = confirm("Are you sure you want to delete letter(s). Press OK to Yes and Cancel to No"); 
//			if (doyou == true)
//			{
//				return true;
//			}
//			else
//			{
//			return false;		
//			}		
		}
	}
	
	// Abid Ali 5359 12/30/2008
	function ValidateCheckbox(gridName)
    {
    
        var matched = 0;
        var eles;
        var mydiv = $find("MPECertifiedMainNumberPopup");
        var backdiv = document.getElementById("divDisable");
        var gridcheck=null;
        var isHaveRecord = false;
        var grid=document.getElementById(gridName);
        var rows=grid.rows.length;
        var eles = document.forms[0].elements;
        
        for(i=0; i<eles.length; i++)
        { 
        
            if (eles[i].type == "checkbox") 
            {
                if (eles[i].id.substring(0, gridName.length) == gridName)
                {   
                    if (eles[i].checked == 1) 
                    {      
                       
                        $find("AEFadeIn").get_OnClickBehavior().play();                        
                       mydiv.show();
                       return false;
                    }
                }
            }
        }
            
        if ( matched == 0) 
        {
            alert("Please select a letter for Printing.");
            //mydiv.style.display = "none";
            backdiv.style.display = "none";
            return false;
        }
        
        return false;
    }
    
//    function cutPopup() 
//    {
//        var mydiv = document.getElementById("mydiv");
//        var backdiv = document.getElementById("divDisable");
//        
//        mydiv.style.display = "none";
//        backdiv.style.display = "none";
//        return true;    
//        
//        
//    }
    
    function CreateStringOrPromtDelete(dgname,count,lettertype, action) 
    {
        
        var isHaveRecord = false;
        document.getElementById("hfLORIds").value = "";
        document.getElementById("hfLORBatchIds").value="";
        document.getElementById("hfLORCourtId").value="";
        
        var grid=document.getElementById(dgname);
        var rows=grid.rows.length;
        var elementTicket;
        var elementBatch;
        var elementCourtId;
        var jIndex = 1;        
        for ( i = 0 ; i<rows ; i++)
        {                                
                //Ozair 5661 04/01/2009 col length check modified
                 if (grid.rows[i].cells.length >= 7 && grid.rows[i].cells[7].getElementsByTagName("INPUT").length > 0)
                 {
                    jIndex = jIndex + 1;
                    //Ozair 5661 04/01/2009 col length check modified
                     if(grid.rows[i].cells[7].getElementsByTagName("INPUT")[0].checked==true)
                     {
                        elementTicket =null;
                        elementBatch = null;
                        elementCourtId = null;
                        
                        // Yasir Kamal 5794 04/14/2009 Lor bug fixed.
                        ///if( i < 10 )
                        if( jIndex < 10 )
                        //5794 end
                        {
                            elementTicket =  document.getElementById(dgname + "_ctl0" + jIndex + "_lbl_tictrial_2");
                            elementBatch =  document.getElementById(dgname + "_ctl0" + jIndex + "_lbl_batchid");
                            elementCourtId =  document.getElementById(dgname + "_ctl0" + jIndex + "_hdnCourtID");
                        }
                        else
                        {
                          elementBatch=  document.getElementById(dgname + "_ctl" + jIndex + "_lbl_batchid");  
                          elementTicket=  document.getElementById(dgname + "_ctl" + jIndex+ "_lbl_tictrial_2");           
                          elementCourtId =  document.getElementById(dgname + "_ctl" + jIndex + "_hdnCourtID");
                        }
                                 
                        
                        if( elementTicket != null && elementBatch != null )
                        {
                            document.getElementById("hfLORIds").value +=  elementTicket.innerText + ",";     
                            
                            if( action == 2)
                                document.getElementById("hfLORBatchIds").value +=   elementBatch.innerText + ",";
                            else
                                document.getElementById("hfLORBatchIds").value +=   elementBatch.innerText + "=" + elementTicket.innerText + ",";
                                
                            if( document.getElementById("hfLORCourtId").value == "")
                            {
                                document.getElementById("hfLORCourtId").value = elementCourtId.value;
                            }
                        }                                 
                     }
                 }
             //}
        }               
        
        if( action == 2)
        {
            if( document.getElementById("hfLORIds").value == "" )
            {
                alert("Please select at least one record.");
			    return false;	
            }
            
             $find("AEFadeIn").get_OnClickBehavior().play();                        
             $find("MPExtConfirm").show();
             document.getElementById("LetterType").value=6;
            return false;
        }
        
        if (document.getElementById("txtCertifiedMailNumber").value == "") 
        {
            alert("Please enter Certified Mail Number to print");
            return false;
        }
        else
        {
            if (document.getElementById("txtCertifiedMailNumber").value == document.getElementById("txtCertifiedMailNumberRetype").value) 
            {
                if (document.getElementById("txtCertifiedMailNumber").value.length == 20) 
                {
                    PrintLetterLOR(dgname,count,lettertype);
                    return true;
                }
                else  
                {
                    alert("Certified Mail Number Should be 20 characters long");
                    return false;
                }
            }
            else
            {
                alert("Certified Mail Number and Retype Number does not match");
                return false;
            }
        }
        
       
       
    }
    
		
		// Abid Ali 5359 1/27/2009 print report
		function OpenReport(filePath)
        {
            
            window.open( filePath ,"");
            return false;
        }
        
        var oldcheck=0;
        function check(con,gridname)
        {
        
        
         
            var id = con.id;
            var courtId = id.substring(id.lastIndexOf("_")+1);
            var matched = 0;
            var checkAll = 0;
            var groupcheckele = "";
            var matchedCount = 0;
            var checkcount = 0;
            var eles = document.forms[0].elements;
                       
           var idx = 2;
        			
        		var grid=document.getElementById(gridname);
                var rows=grid.rows.length;
                for ( i = 0 ; i<rows ; i++)
                {                   	               
                        
                     if ( grid.rows[i].cells[0].getElementsByTagName("INPUT").length > 0)
                     {
                         if(grid.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked==true)
                         {
                            oldcheck=1;
                         }
                         grid.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked=false;  
                     }
                }
        		
        		if(oldcheck==1)
        		{
        		    alert ('Selection of tickets from two different sections are not allowed, Therefore your prervious selections has been removed.');
        		    oldcheck=0;
        		}
        	
        	// abid 5544 02/17/2009 optimize
        	var elmt;
        	for ( i = 0 ; i<rows ; i++)
            {       
                elmt = null;
                if (grid.rows[i].cells.length == 2 && grid.rows[i].cells[1].getElementsByTagName("INPUT").length > 0)
                {                    
                  elmt = grid.rows[i].cells[1].getElementsByTagName("INPUT")[0];                    
                }
                //Ozair 5661 04/01/2009 
                else if (grid.rows[i].cells.length >= 7 && grid.rows[i].cells[7].getElementsByTagName("INPUT").length > 0)
                 {                    
                    //Ozair 5661 04/01/2009 
                    elmt = grid.rows[i].cells[7].getElementsByTagName("INPUT")[0];
                 }                    
                    
                if( elmt != null )
                { 
                    if (elmt.id.substring(elmt.id.lastIndexOf("_")+1) != courtId)
                    {   
                        if (elmt.checked == 1) 
                        {
                            matched =1;
                            elmt.checked = 0;
                        }
                    }
                    else if (elmt.id.substring(elmt.id.lastIndexOf("_")+1) == courtId) 
                    {
                        var thisGrid = elmt.id.substring(0,elmt.id.indexOf("ctl")-1);
                        if (thisGrid == gridname) 
                        {
                            if (checkAll == 0) 
                            {
                                elmt.checked = 0;
                                groupcheckele = elmt.id;
                                
                                checkAll = 1;
                            }
                            else 
                            {
                                matchedCount++;
                                if (elmt.checked == 1) 
                                {
                                    checkcount++;
                                }
                            }
                        }
                        else 
                        {
                            if (elmt.checked == 1) 
                            {
                                matched = 1;
                                elmt.checked = 0;
                            }
                        }                        
                    }
                }               
            }   
        		
            if (matchedCount == checkcount) 
            {
                if (groupcheckele != "") 
                {
                    document.getElementById(groupcheckele).checked = 1;
                }
            }
            if (matched == 1) 
            {
                alert("Selection of tickets from two different sections are not allowed, Therefore your prervious selections has been removed.");
            }
        }
        
        function checkall(con,gridname)
        {
                        
            var id = con.id;
            var courtId = id.substring(id.lastIndexOf("_")+1);
            var matched = 0;            
            var grid=document.getElementById(gridname);
            var rows=grid.rows.length;
            
            // abid 5544 02/17/2009
            var elmt;
        	for ( i = 0 ; i<rows ; i++)
            {       
                elmt = null;                
                if (grid.rows[i].cells.length == 2 && grid.rows[i].cells[1].getElementsByTagName("INPUT").length > 0)
                {                    
                  elmt = grid.rows[i].cells[1].getElementsByTagName("INPUT")[0];                    
                }
                //Ozair 5661 04/01/2009 col length check modified
                else if (grid.rows[i].cells.length >= 7 && grid.rows[i].cells[7].getElementsByTagName("INPUT").length > 0)
                {
                    //Ozair 5661 04/01/2009 col length check modified
                    elmt = grid.rows[i].cells[7].getElementsByTagName("INPUT")[0];
                }   
                
                if( elmt != null )
                { 
                    if (elmt.id.substring(elmt.id.lastIndexOf("_")+1) != courtId)
                    {   
                        if (elmt.checked == 1) 
                        {
                            matched =1;
                            elmt.checked = 0;
                        }
                    }
                    else if (elmt.id.substring(elmt.id.lastIndexOf("_")+1) == courtId) 
                    {
                        if (elmt.id.indexOf(gridname) > -1) 
                        {
                            elmt.checked = con.checked;
                        }
                        else
                        {
                            if (elmt.checked == 1) 
                            {
                                matched =1;
                                elmt.checked = 0;
                            }
                        }                 
                    }
                }               
            }              
           
            if (matched == 1) 
            {
                    alert("Selection of tickets from two different sections are not allowed, Therefore your prervious selections has been removed. ");
            }
        }
        
        
        function PrintLetterLOR(dgname,count,lettertype)
		{
	
	      
			var tkiid;
			var batch;
			var chked;
			var isprint;
			var batchdates;
			var splitflag;
			var splitflagmessage = false;
			var isEmailTrialFlag;
			var isEmailSent=false;
		     tkiid = "_lbl_tictrial_2";
		     batch = "_lbl_batchid";		
		     isprint="_txt_tpnt";
		     batchdates="_lbl_batchdate";
		     splitflag = "_lbl_IsSplit";
             isEmailTrialFlag="_hfEmailFlag1";				
						
			var cnt = count;
			var idx = 2;
			cnt = cnt + 2;			
			var StrTid = "";
			var StrDate = "";
			var print=0*1;
		    var strticktbatch="";
			
			var grid=document.getElementById(dgname);
            var rows=grid.rows.length;
            var element;
            var alreadyTraverse;
            for ( i = 0 ; i<rows ; i++)
            {                   	               
                 var cellsLengh = grid.rows[i].cells.length;
                 alreadyTraverse = false;
                 for( j=0; j < cellsLengh; j++)
                 {
                     if ( grid.rows[i].cells[j].getElementsByTagName("INPUT").length > 0)
                     {
                         if(grid.rows[i].cells[j].getElementsByTagName("INPUT")[0].checked==true)
                         {
                            var chk  = "";
			                var tkid = "";
			                var batchNm = "";
			                var printid="";
			                var bdate="";
			                var split="";
			                var isETF="";
            			    
            			    
			                var _ctl = "";    			
			                if( idx < 10 )
			                    _ctl= "_ctl0";			    			    
			                else		    
			                    _ctl= "_ctl";
            			    
            			       
		                    tkid = dgname+ _ctl + idx + tkiid;
		                    batchNM = dgname+ _ctl + idx + batch ;
		                    printid=dgname+ _ctl + idx +isprint ;
	                        bdate=dgname+ _ctl + idx +batchdates ;
	                        split = dgname+ _ctl + idx + splitflag;
    		                isETF = dgname+ _ctl + idx + isEmailTrialFlag;	
                		    		
				            var GetTiD = document.getElementById(tkid);
                            var GetBatchid = document.getElementById(batchNM);
        				    
		                    var GetSplit = document.getElementById(split);
			                if (GetSplit.innerText != "1" && alreadyTraverse == false)
		                    {
		                        strticktbatch = strticktbatch + GetTiD.innerText + GetBatchid.innerText  +",";
		                        StrTid = StrTid  + GetTiD.innerText + ",";
		                        var getprint=document.getElementById(printid);
		                        print=print+getprint.value*1;	
		                        alreadyTraverse = true;			            
		                    }
		                    else
		                    {
		                        splitflagmessage = true;
		                    }
		                    if(document.getElementById("chk_ShowPrintedLetters").checked ==true)
		                    {
		                        var etf=document.getElementById(isETF);
		                        if (etf.value=="1")
		                        {
		                            isEmailSent=true;
		                        }
		                    }				                             
                         }
                     }
                 }
            }
            
			if (StrTid != "")		
		    {
			    if(print!=0)
			    {
				    doyou=confirm("Selected letter(s) are already printed. Do you want to print them again?"  )	
				    if(doyou==false)
				    {			
					    return false;
				    }
				    else if (isEmailSent)
				    {
				        doyou=confirm("Selected Trial Notification email has already been sent to client do you want to send the email again?"  )
				        if(doyou==false)
				        {			
					        document.getElementById("hfEmailStatus").value=0;   
				        }	
				        else
				        {
				            document.getElementById("hfEmailStatus").value=1;
				        }
				    }
				}   
			
			    if (splitflagmessage)
			    {
			        alert("Some of the selected letters have split setting information and couldn't be printed.\nPlease check these cases on violation fee page.");
			    }

                document.Form1.txt_TicketIDs.value = StrTid;
                document.Form1.txt_TicketIDsbatch.value = strticktbatch;  
                document.Form1.txt_LetterType.value = lettertype;
                
                if (StrTid.length > 8000)
                {
                    alert('Too many letters selected to print. Please select at most 1000 letters at a time.');
                    return false;
                }           
		    }
		    else
		    {		
		        if (splitflagmessage)
			    {
			        alert("Selected letter(s) have split setting information and couldn't be printed.\nPlease check these cases on violation fee page.");
			        return false;
			    }
			    else
			    {
        		    alert("Please select at least one record.");
	        	    return false;
	    	    }
		    }	
	}
	
	function pageLoad(sender,args){
           
        $find("ColPnlTrialNotification").add_expandComplete( expandHandler );
        $find("CollapsiblePanelExtender1").add_expandComplete( expandHandler );
        $find("CollapsiblePanelExtender2").add_expandComplete( expandHandler );
        $find("CollapsiblePanelExtender3").add_expandComplete( expandHandler );
        $find("CollapsiblePanelExtender4").add_expandComplete( expandHandler );
        $find("CollapsiblePanelExtender5").add_expandComplete( expandHandler );

        //$find("collapsibleBehavior").add_add_collapseComplete( collapseHandler );                

    }
    
function ClosePanel(sender, Args){

        if(sender._postBackSettings.panelID=="ScriptManager1|chk_ShowPrintedLetters" || sender._postBackSettings.panelID=="ScriptManager1|ddl_SalesRep" ||sender._postBackSettings.panelID=="ScriptManager1|ddl_Courts" || sender._postBackSettings.panelID=="ScriptManager1|fromdate" || sender._postBackSettings.panelID=="ScriptManager1|todate")
        {
                $find("ColPnlTrialNotification")._doClose();
                $find("CollapsiblePanelExtender1")._doClose();
                $find("CollapsiblePanelExtender2")._doClose();
                $find("CollapsiblePanelExtender3")._doClose();
                $find("CollapsiblePanelExtender4")._doClose();
                $find("CollapsiblePanelExtender5")._doClose();
        }

        //$find("collapsibleBehavior").add_add_collapseComplete( collapseHandler );

    }
    

    function expandHandler( sender  , args ){
  
    if(sender._collapseControlID == 'PnlTrialNotificationHeader')
        __doPostBack('<%= ImgBtnTrialNotification1.ClientID %>','');
     if(sender._collapseControlID == 'PnlMissedCourtLettersHeader')
        __doPostBack('<%= ImgBtnMissedCourtLetters1.ClientID %>','');
        if(sender._collapseControlID == 'PnlPledOutLetterPrintedHeader')
        __doPostBack('<%= ImgBtnPledOutLetterPrinted1.ClientID %>','');
        if(sender._collapseControlID == 'PnlsetcalldetailHeader')
        __doPostBack('<%= ImgBtnsetcalldetail1.ClientID %>','');
        if(sender._collapseControlID == 'PnlSOLLetterPrintedSOLHeader')
        __doPostBack('<%= ImgBtnSOLLetterPrinted1.ClientID %>','');
        if(sender._collapseControlID == 'PnlLORLetterPrintedHeader')
        __doPostBack('<%= ImgBtnLORLetterPrinted1.ClientID %>','');
    }

    
    </script>

    <style type="text/css">
        .style1
        {
            height: 27px;
            width: 40%;
        }
    </style>
</head>
<body>
    <form id="Form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>

        <script type="text/javascript" language="javascript">
       
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(ClosePanel);
                    // runs each time postback is initiated by any update panel on the page
                    function RunThisAfterEachAsyncPostback(sender, Args)
                    {
                    
                            
                    
                    }
        </script>

        <table id="tblmain" cellspacing="1" cellpadding="1" align="center" border="0" style="width: 800px">
            <tr>
                <td style="width: 100%">
                    <table id="tblsub" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td style="width: 100%">
                                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" height="10" style="width: 668px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%;" valign="middle" colspan="" rowspan="">
                                <strong>
                                    <table id="tblmerg" border="0" cellpadding="0" cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                        <tr>
                                            <td align="center" style="height: 30px" valign="middle">
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="Sales Rep" CssClass="clssubhead"></asp:Label>
                                                </strong>
                                                <asp:DropDownList ID="ddl_SalesRep" runat="server" OnSelectedIndexChanged="ddl_SalesRep_SelectedIndexChanged"
                                                    AutoPostBack="True" BackColor="White" CssClass="clsInputadministration" Width="45px">
                                                    <asp:ListItem Value="%">ALL</asp:ListItem>
                                                </asp:DropDownList>
                                                <strong>&nbsp;<asp:Label ID="Label2" runat="server" Text="Court" CssClass="clssubhead"></asp:Label>&nbsp;</strong><asp:DropDownList
                                                    ID="ddl_Courts" runat="server" OnSelectedIndexChanged="ddl_Courts_SelectedIndexChanged"
                                                    AutoPostBack="True" DataTextField="shortname" DataValueField="courtid" CssClass="clsInputadministration">
                                                </asp:DropDownList>
                                                <strong>
                                                    <asp:CheckBox ID="chk_ShowPrintedLetters" runat="server" Text="Show Printed" OnCheckedChanged="chk_ShowPrintedLetters_CheckedChanged"
                                                        AutoPostBack="True" CssClass="clssubhead" /></strong>&nbsp;
                                            </td>
                                        </tr>
                                        <tr id="trdate" style="display:none">
                                            <td align="center" valign="middle" class="clsLeftPaddingTable" style="height: 34px">
                                                <table id="tbldate" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="height: 21px">
                                                            <strong><span class="clssubhead">From&nbsp; </span></strong>
                                                        </td>
                                                        <td align="left" style="height: 21px">
                                                            <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" AutoPostBack="True"
                                                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                                ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                                                OnDateChanged="fromdate_DateChanged" LowerBoundDate="1900-01-01">
                                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="White"></WeekdayStyle>
                                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="LightGray"></WeekendStyle>
                                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                    BackColor="White"></HolidayStyle>
                                                            </ew:CalendarPopup>
                                                        </td>
                                                        <td align="left" style="height: 21px">
                                                            <strong><span class="clssubhead">To&nbsp; </span></strong>
                                                        </td>
                                                        <td align="left" style="height: 21px">
                                                            <ew:CalendarPopup ID="todate" runat="server" AllowArbitraryText="False" AutoPostBack="True"
                                                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                                ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                                                OnDateChanged="todate_DateChanged" LowerBoundDate="1900-01-01">
                                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                    ForeColor="Black" />
                                                            </ew:CalendarPopup>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../Images/separator_repeat.gif" height="10" style="width: 668px">
                                            </td>
                                        </tr>
                                    </table>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="ColPnlTrialNotification" runat="server"
                                            TargetControlID="PnlTrialNotification" CollapseControlID="PnlTrialNotificationHeader"
                                            ExpandControlID="PnlTrialNotificationHeader" Collapsed="true" ImageControlID="ImgBtnTrialNotification"
                                            CollapsedImage="../Images/folder.gif" ExpandedImage="../Images/folderopen.gif">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                            <tr bgcolor="#eeeeee">
                                                <td style="width: 70%">
                                                    <asp:Panel ID="PnlTrialNotificationHeader" runat="server" Style="width: 100%">
                                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                            <tr>
                                                                <td valign="bottom" width="3%" style="height: 27px">
                                                                    <asp:ImageButton ID="ImgBtnTrialNotification1" runat="server" Style="display: none"
                                                                        OnClick="ImgBtnTrialNotification_Click" />
                                                                    <asp:Image ID="ImgBtnTrialNotification" runat=server />                                                                    
                                                                </td>
                                                                <td valign="bottom" align="left" class="style1" width="67%">
                                                                    <strong>Trial Notification Letters</strong><asp:HiddenField ID="hfEmailStatus" runat="server" />
                                                                    <asp:HiddenField ID="hfEmailAlreadySentStatus" runat="server" />
                                                                </td>
                                                                <td valign="bottom" style="height: 27px;" width="30%">
                                                                    &nbsp;&nbsp;<asp:Label ID="lbl_trialprintedcpunt" runat="server" Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td valign="top" align="right" style="height: 27px; width: 30%">
                                                    <asp:ImageButton ID="imgbtn_delete" runat="server" ImageUrl="../Images/remove.gif"
                                                        ToolTip="Delete" CommandArgument="2"></asp:ImageButton>
                                                    <asp:ImageButton ID="imgbtn_trialdeletedprint" runat="server" ImageUrl="../Images/DelPrnt.jpg"
                                                        ToolTip="Deleted Print" Visible="False"></asp:ImageButton>
                                                    <asp:ImageButton ID="imgbtn_trialprint" runat="server" ImageUrl="../Images/PrintNew1.jpg"
                                                        ToolTip="Print Trial Letter" CommandArgument="2" OnClick="imgbtn_trialprint_Click">
                                                    </asp:ImageButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="PnlTrialNotification" runat="server" Style="width: 100%">
                                            <table style="width: 100%">
                                                <tr id="rowtrialdetail">
                                                    <td colspan="6" style="width: 900px;" valign="top" align="center">
                                                        <br />
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <strong class="clssubhead">Type of Cases :</strong>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="dd_state" runat="server" CssClass="clsInputCombo" onchange="return checkmake()">
                                                                        <asp:ListItem Value="0">Select All</asp:ListItem>
                                                                        <asp:ListItem Value="1">Bond</asp:ListItem>
                                                                        <asp:ListItem Value="2">Regular</asp:ListItem>
                                                                        <asp:ListItem Value="3" Selected="True">Clear All</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <strong class="clssubhead">Type of Delivery :</strong>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbregmail" runat="server" GroupName="mail" Text="Regular Mail"
                                                                        CssClass="clssubhead" />&nbsp;
                                                                    <asp:RadioButton ID="rbemail" runat="server" GroupName="mail" Text="Electrnoic Mail"
                                                                        CssClass="clssubhead" Width="112px" />&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <tr id="trdg_batchtrial">
                                                            <td>
                                                                <asp:DataGrid ID="dg_batchtrial" runat="server" Width="800px" AutoGenerateColumns="False"
                                                                    CellPadding="0" AllowSorting="True" OnSortCommand="dg_batchtrial_SortCommand"
                                                                    OnItemDataBound="dg_batchtrial_ItemDataBound">
                                                                    <AlternatingItemStyle BackColor="White" HorizontalAlign="Left"></AlternatingItemStyle>
                                                                    <ItemStyle BackColor="#EEEEEE" HorizontalAlign="Left"></ItemStyle>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn>
                                                                            <HeaderTemplate>
                                                                                <asp:Image ID="Image4" ImageUrl="../Images/T.gif" runat="server"></asp:Image>&nbsp;
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                        </td>
                                                                                        <td style="display: none" colspan="1">
                                                                                            <asp:Label ID="lbl_tictrial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                            <asp:Label ID="lbl_batchid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchid") %>'>
                                                                                            </asp:Label><asp:Label ID="lbl_IsSplit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsSplit") %>'></asp:Label>
                                                                                            <asp:TextBox ID="txt_tpnt" runat="server" Width="22px" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>'>
                                                                                            </asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Name" SortExpression="Name">
                                                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="lbl_name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                                                    ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>' CssClass="Label"
                                                                                    NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'>
                                                                                </asp:HyperLink>
                                                                                <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                                                                <asp:HiddenField ID="hf_fullname" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn>
                                                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_Court" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_RoomNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="btn_Court" runat="server" OnClick="btn_Court_Click">Crt</asp:LinkButton>
                                                                                <asp:LinkButton ID="btn_Status" runat="server" OnClick="btn_Status_Click">Status</asp:LinkButton>
                                                                                &nbsp;<asp:LinkButton ID="lbtnTrialTrialdate" runat="server" OnClick="lbtnTrialTrialdate_Click">Trial Date </asp:LinkButton><asp:HyperLink
                                                                                    ID="HyperLink1" runat="server"><span class="clssubhead"><span class="clssubhead">&</span></span></asp:HyperLink><asp:LinkButton
                                                                                        ID="lbtnTrialTrialRoom" runat="server" OnClick="lbtnTrialTrialRoom_Click"> Room</asp:LinkButton>
                                                                                &nbsp;&nbsp;
                                                                            </HeaderTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Batch Date&amp; Rep" SortExpression="batchdate">
                                                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_batchdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'
                                                                                    CssClass="Label"></asp:Label>
                                                                                <asp:Label ID="lbl_BatchRep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lbtnTrialBatchDate" runat="server" OnClick="lbtnTrialBatchDate_Click">Batch Date</asp:LinkButton>
                                                                                <asp:HyperLink ID="HyperLink2" runat="server"><span class="clssubhead"><span class="clssubhead">&</span></span></asp:HyperLink>
                                                                                <asp:LinkButton ID="lbtnTrialBatchRep" runat="server" OnClick="lbtnTrialBatchRep_Click">Rep</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Print Date &amp; Rep" SortExpression="printdate">
                                                                            <HeaderStyle CssClass="clsaspcolumnheaderblack"></HeaderStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_printdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lbtnTrialPrintDate" runat="server" OnClick="lbtnTrialPrintDate_Click">Print Date</asp:LinkButton>
                                                                                <asp:HyperLink ID="HyperLink3" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                                <asp:LinkButton ID="lbtnTrialPrintRep" runat="server" OnClick="lbtnTrialPrintRep_Click">Rep</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Email">
                                                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_email" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Bond">
                                                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_bond" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.bond") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Tracking Number" Visible="false">
                                                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_Trackno" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.trackingnumber") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn Visible="true">
                                                                        <HeaderStyle HorizontalAlign="Right" />                                                                        
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chbTNAll" runat="server" EnableViewState="true" OnCheckedChanged="ChkAll_CheckedChanged"
                                                                                    AutoPostBack="True" />
                                                                            </HeaderTemplate>
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chb" runat="server" EnableViewState="true" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="TrialDate" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbltrialdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdate") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="TrialRoom" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbltrialroom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trilroom") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="BatchDate" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblbatchdae1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate1") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                &nbsp;
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="BatchEmp" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblbatchemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchemp") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="PrintDate" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblprintdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printdate1") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="PrintEmp" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblrintemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printemp") %>'
                                                                                    Visible="False"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%-- Fahad 5071 11/19/2008--%>
                                                                        <asp:TemplateColumn>
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="img_status1" runat="server" ImageUrl="~/Images/right.gif" ToolTip="Email already Sent" />
                                                                                <asp:HiddenField ID="hfEmailFlag1" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.IsemailFlag") %>' />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                                                            </td>
                                                        </tr>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" background="../Images/separator_repeat.gif" style="width: 900px;
                                                        height: 10px;">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <aspnew:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1"
                                            runat="server">
                                            <ProgressTemplate>
                                                <img alt="Please wait" src="../Images/plzwait.gif" />
                                                Please wait work in progress.......
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="ImgBtnTrialNotification1" />
                                        <aspnew:AsyncPostBackTrigger ControlID="chk_ShowPrintedLetters" />
                                        <aspnew:AsyncPostBackTrigger ControlID="fromdate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="todate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_SalesRep" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_Courts" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                            Collapsed="true" TargetControlID="PnltarMissedCourtLetters" CollapseControlID="PnlMissedCourtLettersHeader"
                                            ExpandControlID="PnlMissedCourtLettersHeader" ImageControlID="ImgBtnMissedCourtLetters"
                                            CollapsedImage="../Images/folder.gif" ExpandedImage="../Images/folderopen.gif">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                            <tr bgcolor="#eeeeee">
                                                <td style="width: 70%">
                                                    <asp:Panel ID="PnlMissedCourtLettersHeader" runat="server" Style="width: 100%">
                                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                            <tr bgcolor="#eeeeee">
                                                                <td valign="bottom" width="3%" style="height: 27px">
                                                                    <asp:ImageButton ID="ImgBtnMissedCourtLetters1" runat="server" Style="display: none"
                                                                        OnClick="ImgBtnMissedCourtLetters_Click" />
                                                                    <asp:Image ID="ImgBtnMissedCourtLetters" runat="server" />
                                                                </td>
                                                                <td valign="bottom" align="left" width="67%" class="style1">
                                                                    <strong>Missed Court Letters</strong>
                                                                </td>
                                                                <td valign="bottom" style="height: 27px" width="30%">
                                                                    &nbsp;&nbsp;<asp:Label ID="lblMissedCourtLetterPrinted" runat="server" Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td valign="top" align="right" style="height: 27px; width: 30%">
                                                    <asp:ImageButton ID="imgbtnMissedLetterDelete" runat="server" ImageUrl="../Images/remove.gif"
                                                        ToolTip="Delete" CommandArgument="11" /><asp:ImageButton ID="imgbtnMissedLetterDeletedPrint"
                                                            runat="server" ImageUrl="../Images/DelPrnt.jpg" ToolTip="Deleted Print" Visible="False" /><asp:ImageButton
                                                                ID="imgbtnMissedLetterPrint" runat="server" ImageUrl="../Images/PrintNew1.jpg"
                                                                OnClick="imgbtn_trialprint_Click" ToolTip="Print Missed Letter" CommandArgument="11" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="PnltarMissedCourtLetters" runat="server" Style="width: 100%">
                                            <table style="width: 100%">
                                                <tr id="rowmisseddetail">
                                                    <td colspan="6" style="width: 900px;" valign="top">
                                                        <asp:DataGrid ID="dgMissedCourt" runat="server" Width="800px" AutoGenerateColumns="False"
                                                            CellPadding="0" AllowSorting="True" OnItemDataBound="dg_batchtrial_ItemDataBound"
                                                            OnSortCommand="dg_Result_SortCommand" OnItemCommand="dg_Result_ItemCommand">
                                                            <AlternatingItemStyle BackColor="White" />
                                                            <ItemStyle BackColor="#EEEEEE" />
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    <HeaderTemplate>
                                                                        <asp:Image ID="Image4" runat="server" ImageUrl="../Images/T.gif" />&nbsp;
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="imgLink" runat="server" ImageUrl="../Images/T.gif" />
                                                                                </td>
                                                                                <td style="display: none" colspan="1">
                                                                                    <asp:Label ID="lbl_tictrial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_batchid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchid") %>'>
                                                                                    </asp:Label><asp:Label ID="lbl_IsSplit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsSplit") %>'></asp:Label>
                                                                                    <asp:TextBox ID="txt_tpnt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>'
                                                                                        Width="22px">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Name" SortExpression="Name">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="lbl_name" runat="server" CssClass="Label" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>'>
                                                                        </asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Court" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_RoomNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="btn_Court" CommandName="court" runat="server" OnClick="btn_Court_Click">Crt</asp:LinkButton>
                                                                        <asp:LinkButton ID="btn_Status" CommandName="status" runat="server" OnClick="btn_Status_Click">Status</asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="lbtnTrialTrialdate" CommandName="courtdate" runat="server"
                                                                            OnClick="lbtnTrialTrialdate_Click">Court Date </asp:LinkButton><asp:HyperLink ID="HyperLink1"
                                                                                runat="server"><span class="clssubhead">&</span></asp:HyperLink><asp:LinkButton ID="lbtnTrialTrialRoom"
                                                                                    runat="server" CommandName="courtroom" OnClick="lbtnTrialTrialRoom_Click"> Room</asp:LinkButton>
                                                                        &nbsp;&nbsp;
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Batch Date&amp; Rep" SortExpression="batchdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_batchdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_BatchRep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialBatchDate" runat="server" OnClick="lbtnTrialBatchDate_Click"
                                                                            CommandName="batchdate">Batch Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink2" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialBatchRep" runat="server" OnClick="lbtnTrialBatchRep_Click"
                                                                            CommandName="batchrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Print Date &amp; Rep" SortExpression="printdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_printdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialPrintDate" runat="server" OnClick="lbtnTrialPrintDate_Click"
                                                                            CommandName="printdate">Print Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink3" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialPrintRep" runat="server" OnClick="lbtnTrialPrintRep_Click"
                                                                            CommandName="printrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Email">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_email" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn Visible="true">
                                                                <HeaderStyle HorizontalAlign="Right" />                                                                
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chbMCAll" runat="server" EnableViewState="true" OnCheckedChanged="ChkAll_CheckedChanged"
                                                                            AutoPostBack="True" />
                                                                    </HeaderTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chb" runat="server" EnableViewState="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialRoom" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialroom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trilroom") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchdae1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        &nbsp;
                                                                    </EditItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchemp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblprintdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblrintemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printemp") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:HiddenField ID="HiddenField2" runat="server" Value="0" />
                                                        <asp:HiddenField ID="hfmissedcourt" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" style="width: 900px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" background="../Images/separator_repeat.gif" height="10" style="width: 900px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <aspnew:UpdateProgress ID="UpdateProgress6" AssociatedUpdatePanelID="UpdatePanel2"
                                            runat="server">
                                            <ProgressTemplate>
                                                <img alt="Please wait" src="../Images/plzwait.gif" />
                                                Please wait work in progress.......
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="chk_ShowPrintedLetters" />
                                        <aspnew:AsyncPostBackTrigger ControlID="fromdate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="todate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_SalesRep" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_Courts" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                            TargetControlID="PnlPledOutLetterPrinted" CollapseControlID="PnlPledOutLetterPrintedHeader"
                                            ExpandControlID="PnlPledOutLetterPrintedHeader" ImageControlID="ImgBtnPledOutLetterPrinted"
                                            Collapsed="true" CollapsedImage="../Images/folder.gif" ExpandedImage="../Images/folderopen.gif">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                            <tr bgcolor="#eeeeee">
                                                <td style="width: 70%">
                                                    <asp:Panel ID="PnlPledOutLetterPrintedHeader" runat="server" Style="width: 100%">
                                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                            <tr bgcolor="#eeeeee">
                                                                <td valign="bottom" width="3%" style="height: 27px">
                                                                    <asp:ImageButton ID="ImgBtnPledOutLetterPrinted1" runat="server" Style="display: none"
                                                                        OnClick="ImgBtnPledOutLetterPrinted_Click" />
                                                                    <asp:Image ID="ImgBtnPledOutLetterPrinted" runat="server" />
                                                                </td>
                                                                <td valign="bottom" align="left" width="67%" class="style1" >
                                                                    <strong>Pled Out Letters</strong>
                                                                </td>
                                                                <td valign="bottom" style="height: 27px" width="30%">
                                                                    &nbsp;&nbsp;<asp:Label ID="lblPledOutLetterPrinted" runat="server" Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td valign="top" align="right" style="height: 27px; width: 30%">
                                                    &nbsp; &nbsp;<asp:ImageButton ID="imgbtnPledOutDeleted" runat="server" ImageUrl="../Images/remove.gif"
                                                        ToolTip="Delete" CommandArgument="12" /><asp:ImageButton ID="imgbtnPledOutDeletedPrint"
                                                            runat="server" ImageUrl="../Images/DelPrnt.jpg" ToolTip="Deleted Print" Visible="False" /><asp:ImageButton
                                                                ID="imgPledOutPrint" runat="server" ImageUrl="../Images/PrintNew1.jpg" OnClick="imgbtn_trialprint_Click"
                                                                ToolTip="Print PledOut Letter" CommandArgument="12" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="PnlPledOutLetterPrinted" runat="server" Style="width: 100%">
                                            <table style="width: 100%">
                                                <tr id="rowpledoutdetail">
                                                    <td colspan="6" style="width: 900px;" valign="top">
                                                        <asp:DataGrid ID="dgPledOut" runat="server" Width="800px" AutoGenerateColumns="False"
                                                            CellPadding="0" AllowSorting="True" OnSortCommand="dg_Result_SortCommand" OnItemDataBound="dg_batchtrial_ItemDataBound"
                                                            OnItemCommand="dg_Result_ItemCommand">
                                                            <AlternatingItemStyle BackColor="White" />
                                                            <ItemStyle BackColor="#EEEEEE" />
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    <HeaderTemplate>
                                                                        <asp:Image ID="Image4" runat="server" ImageUrl="../Images/T.gif" />&nbsp;
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="imgLink" runat="server" ImageUrl="../Images/T.gif" />
                                                                                </td>
                                                                                <td style="display: none" colspan="1">
                                                                                    <asp:Label ID="lbl_tictrial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_batchid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchid") %>'>
                                                                                    </asp:Label><asp:Label ID="lbl_IsSplit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsSplit") %>'></asp:Label>
                                                                                    <asp:TextBox ID="txt_tpnt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>'
                                                                                        Width="22px">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Name" SortExpression="Name">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="lbl_name" runat="server" CssClass="Label" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>'>
                                                                        </asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Court" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_RoomNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="btn_Court" CommandName="court" runat="server" OnClick="btn_Court_Click">Crt</asp:LinkButton>
                                                                        <asp:LinkButton ID="btn_Status" CommandName="status" runat="server" OnClick="btn_Status_Click">Status</asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="lbtnTrialTrialdate" CommandName="courtdate" runat="server"
                                                                            OnClick="lbtnTrialTrialdate_Click">Court Date </asp:LinkButton><asp:HyperLink ID="HyperLink1"
                                                                                runat="server"><span class="clssubhead">&</span></asp:HyperLink><asp:LinkButton ID="lbtnTrialTrialRoom"
                                                                                    runat="server" CommandName="courtroom" OnClick="lbtnTrialTrialRoom_Click"> Room</asp:LinkButton>
                                                                        &nbsp;&nbsp;
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Batch Date&amp; Rep" SortExpression="batchdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_batchdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_BatchRep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialBatchDate" runat="server" OnClick="lbtnTrialBatchDate_Click"
                                                                            CommandName="batchdate">Batch Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink2" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialBatchRep" runat="server" OnClick="lbtnTrialBatchRep_Click"
                                                                            CommandName="batchrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Print Date &amp; Rep" SortExpression="printdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_printdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialPrintDate" runat="server" OnClick="lbtnTrialPrintDate_Click"
                                                                            CommandName="printdate">Print Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink3" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialPrintRep" runat="server" OnClick="lbtnTrialPrintRep_Click"
                                                                            CommandName="printrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Email">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_email" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn Visible="true">
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chbPLOAll" runat="server" EnableViewState="true" OnCheckedChanged="ChkAll_CheckedChanged"
                                                                            AutoPostBack="True" />
                                                                    </HeaderTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chb" runat="server" EnableViewState="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialRoom" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialroom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trilroom") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchdae1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        &nbsp;
                                                                    </EditItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchemp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblprintdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblrintemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printemp") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:HiddenField ID="HiddenField3" runat="server" Value="0" />
                                                        <asp:HiddenField ID="hfpledout" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" style="width: 900px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" background="../Images/separator_repeat.gif" height="10" style="width: 900px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <aspnew:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanel3"
                                            runat="server">
                                            <ProgressTemplate>
                                                <img alt="Please wait" src="../Images/plzwait.gif" />
                                                Please wait work in progress.......
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="chk_ShowPrintedLetters" />
                                        <aspnew:AsyncPostBackTrigger ControlID="fromdate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="todate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_SalesRep" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_Courts" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <aspnew:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server"
                                            TargetControlID="Pnlsetcalldetail" CollapseControlID="PnlsetcalldetailHeader"
                                            Collapsed="true" ExpandControlID="PnlsetcalldetailHeader" ImageControlID="ImgBtnsetcalldetail"
                                            CollapsedImage="../Images/folder.gif" ExpandedImage="../Images/folderopen.gif">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                            <tr bgcolor="#eeeeee">
                                                <td style="width: 70%">
                                                    <asp:Panel ID="PnlsetcalldetailHeader" runat="server" Style="width: 100%">
                                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                            <tr bgcolor="#eeeeee">
                                                                <td valign="bottom" width="3%" style="height: 27px">
                                                                    <asp:ImageButton ID="ImgBtnsetcalldetail1" runat="server" Style="display: none" OnClick="ImgBtnsetcalldetail_Click" />
                                                                    <asp:Image ID="ImgBtnsetcalldetail" runat="server" />
                                                                </td>
                                                                <td valign="bottom" align="left" width="67%" class="style1" >
                                                                    <strong>Set Call Letters</strong>
                                                                </td>
                                                                <td valign="bottom" style="height: 27px" width="30%">
                                                                    &nbsp;&nbsp;<asp:Label ID="lblsetcallLetterPrinted" runat="server" Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td valign="top" align="right" style="height: 27px; width: 30%">
                                                    &nbsp; &nbsp;<asp:ImageButton ID="imgbtnsetcalldelete" runat="server" ImageUrl="../Images/remove.gif"
                                                        ToolTip="Delete" CommandArgument="13" />
                                                    <asp:ImageButton ID="imgbtnsetcallprintdelete" runat="server" ImageUrl="../Images/DelPrnt.jpg"
                                                        ToolTip="Deleted Print" Visible="False" />
                                                    <asp:ImageButton ID="imgbtnsetcallprint" runat="server" ImageUrl="../Images/PrintNew1.jpg"
                                                        OnClick="imgbtn_trialprint_Click" ToolTip="Print Set Call Letter" CommandArgument="13" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="Pnlsetcalldetail" runat="server" Style="width: 100%">
                                            <table>
                                                <tr id="rowsetcalldetail" style="width: 100%">
                                                    <td colspan="6" style="width: 900px;" valign="top">
                                                        <asp:DataGrid ID="dgsetcall" runat="server" Width="800px" AutoGenerateColumns="False"
                                                            CellPadding="0" AllowSorting="True" OnSortCommand="dg_Result_SortCommand" OnItemDataBound="dg_batchtrial_ItemDataBound"
                                                            OnItemCommand="dg_Result_ItemCommand">
                                                            <AlternatingItemStyle BackColor="White" />
                                                            <ItemStyle BackColor="#EEEEEE" />
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    <HeaderTemplate>
                                                                        <asp:Image ID="Image4" runat="server" ImageUrl="../Images/T.gif" />&nbsp;
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="imgLink" runat="server" ImageUrl="../Images/T.gif" />
                                                                                </td>
                                                                                <td style="display: none" colspan="1">
                                                                                    <asp:Label ID="lbl_tictrial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_batchid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchid") %>'>
                                                                                    </asp:Label><asp:Label ID="lbl_IsSplit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsSplit") %>'></asp:Label>
                                                                                    <asp:TextBox ID="txt_tpnt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>'
                                                                                        Width="22px">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Name" SortExpression="Name">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="lbl_name" runat="server" CssClass="Label" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>'>
                                                                        </asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Court" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_RoomNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="btn_Court" CommandName="court" runat="server" OnClick="btn_Court_Click">Crt</asp:LinkButton>
                                                                        <asp:LinkButton ID="btn_Status" CommandName="status" runat="server" OnClick="btn_Status_Click">Status</asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="lbtnTrialTrialdate" CommandName="courtdate" runat="server"
                                                                            OnClick="lbtnTrialTrialdate_Click">Court Date </asp:LinkButton><asp:HyperLink ID="HyperLink1"
                                                                                runat="server"><span class="clssubhead">&</span></asp:HyperLink><asp:LinkButton ID="lbtnTrialTrialRoom"
                                                                                    runat="server" CommandName="courtroom" OnClick="lbtnTrialTrialRoom_Click"> Room</asp:LinkButton>
                                                                        &nbsp;&nbsp;
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Batch Date&amp; Rep" SortExpression="batchdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_batchdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_BatchRep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialBatchDate" runat="server" OnClick="lbtnTrialBatchDate_Click"
                                                                            CommandName="batchdate">Batch Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink2" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialBatchRep" runat="server" OnClick="lbtnTrialBatchRep_Click"
                                                                            CommandName="batchrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Print Date &amp; Rep" SortExpression="printdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_printdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialPrintDate" runat="server" OnClick="lbtnTrialPrintDate_Click"
                                                                            CommandName="printdate">Print Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink3" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialPrintRep" runat="server" OnClick="lbtnTrialPrintRep_Click"
                                                                            CommandName="printrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Email">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_email" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn Visible="true">
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chbSCAll" runat="server" EnableViewState="true" OnCheckedChanged="ChkAll_CheckedChanged"
                                                                            AutoPostBack="True" />
                                                                    </HeaderTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chb" runat="server" EnableViewState="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialRoom" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialroom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trilroom") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchdae1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        &nbsp;
                                                                    </EditItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchemp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblprintdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblrintemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printemp") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:HiddenField ID="HiddenField4" runat="server" Value="0" />
                                                        <asp:HiddenField ID="hfsetcall" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" style="width: 900px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" background="../Images/separator_repeat.gif" height="10" style="width: 900px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <aspnew:UpdateProgress ID="UpdateProgress3" AssociatedUpdatePanelID="UpdatePanel4"
                                            runat="server">
                                            <ProgressTemplate>
                                                <img alt="Please wait" src="../Images/plzwait.gif" />
                                                Please wait work in progress.......
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="chk_ShowPrintedLetters" />
                                        <aspnew:AsyncPostBackTrigger ControlID="fromdate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="todate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_SalesRep" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_Courts" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <aspnew:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server"
                                            TargetControlID="PnlSOLLetterPrinted" CollapseControlID="PnlSOLLetterPrintedSOLHeader"
                                            ExpandControlID="PnlSOLLetterPrintedSOLHeader" ImageControlID="ImgBtnSOLLetterPrinted"
                                            Collapsed="true" CollapsedImage="../Images/folder.gif" ExpandedImage="../Images/folderopen.gif">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                            <tr bgcolor="#eeeeee">
                                                <td style="width: 70%">
                                                    <asp:Panel ID="PnlSOLLetterPrintedSOLHeader" runat="server" Style="width: 100%">
                                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                            <tr bgcolor="#eeeeee">
                                                                <td valign="bottom" width="3%" style="height: 27px">
                                                                    <asp:ImageButton ID="ImgBtnSOLLetterPrinted1" runat="server" Style="display: none"
                                                                        OnClick="ImgBtnSOLLetterPrinted_Click" />
                                                                    <asp:Image ID="ImgBtnSOLLetterPrinted" runat="server" />
                                                                </td>
                                                                <td valign="bottom" align="left" width="67%" class="style1">
                                                                    <strong>SOL Letters</strong>
                                                                </td>
                                                                <td valign="bottom" style="height: 27px" width="30%">
                                                                    &nbsp;&nbsp;<asp:Label ID="lblSOLLetterPrinted" runat="server" Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td valign="top" align="right" style="height: 27px; width: 30%">
                                                    &nbsp; &nbsp;
                                                    <asp:ImageButton ID="imgbtnSOLdelete" runat="server" ImageUrl="../Images/remove.gif"
                                                        ToolTip="Delete" CommandArgument="15" />
                                                    <asp:ImageButton ID="imgbtnSOLprintdelete" runat="server" ImageUrl="../Images/DelPrnt.jpg"
                                                        ToolTip="Deleted Print" Visible="False" />
                                                    <asp:ImageButton ID="imgbtnSOLprint" runat="server" ImageUrl="../Images/PrintNew1.jpg"
                                                        OnClick="imgbtn_trialprint_Click" ToolTip="Print SOL Letter" CommandArgument="15" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="PnlSOLLetterPrinted" runat="server" Style="width: 100%">
                                            <table style="width: 100%">
                                                <tr id="rowSOLdetail">
                                                    <td colspan="6" style="width: 900px;" valign="top">
                                                        <asp:DataGrid ID="dgSOL" runat="server" Width="800px" AutoGenerateColumns="False"
                                                            CellPadding="0" AllowSorting="True" OnSortCommand="dg_Result_SortCommand" OnItemDataBound="dg_batchtrial_ItemDataBound"
                                                            OnItemCommand="dg_Result_ItemCommand">
                                                            <AlternatingItemStyle BackColor="White" />
                                                            <ItemStyle BackColor="#EEEEEE" />
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    <HeaderTemplate>
                                                                        <asp:Image ID="Image4" runat="server" ImageUrl="../Images/T.gif" />&nbsp;
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="imgLink" runat="server" ImageUrl="../Images/T.gif" />
                                                                                </td>
                                                                                <td style="display: none" colspan="1">
                                                                                    <asp:Label ID="lbl_tictrial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_batchid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchid") %>'>
                                                                                    </asp:Label><asp:Label ID="lbl_IsSplit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsSplit") %>'></asp:Label>
                                                                                    <asp:TextBox ID="txt_tpnt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>'
                                                                                        Width="22px">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Name" SortExpression="Name">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="lbl_name" runat="server" CssClass="Label" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>'>
                                                                        </asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Court" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_RoomNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="btn_Court" CommandName="court" runat="server" OnClick="btn_Court_Click">Crt</asp:LinkButton>
                                                                        <asp:LinkButton ID="btn_Status" CommandName="status" runat="server" OnClick="btn_Status_Click">Status</asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="lbtnTrialTrialdate" CommandName="courtdate" runat="server"
                                                                            OnClick="lbtnTrialTrialdate_Click">Court Date </asp:LinkButton><asp:HyperLink ID="HyperLink1"
                                                                                runat="server"><span class="clssubhead">&</span></asp:HyperLink><asp:LinkButton ID="lbtnTrialTrialRoom"
                                                                                    runat="server" CommandName="courtroom" OnClick="lbtnTrialTrialRoom_Click"> Room</asp:LinkButton>
                                                                        &nbsp;&nbsp;
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Batch Date&amp; Rep" SortExpression="batchdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_batchdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_BatchRep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialBatchDate" runat="server" OnClick="lbtnTrialBatchDate_Click"
                                                                            CommandName="batchdate">Batch Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink2" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialBatchRep" runat="server" OnClick="lbtnTrialBatchRep_Click"
                                                                            CommandName="batchrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Print Date &amp; Rep" SortExpression="printdate">
                                                                    <HeaderStyle CssClass="clsaspcolumnheaderblack" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_printdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="lbtnTrialPrintDate" runat="server" OnClick="lbtnTrialPrintDate_Click"
                                                                            CommandName="printdate">Print Date</asp:LinkButton>
                                                                        <asp:HyperLink ID="HyperLink3" runat="server"><span class="clssubhead">&</span></asp:HyperLink>
                                                                        <asp:LinkButton ID="lbtnTrialPrintRep" runat="server" OnClick="lbtnTrialPrintRep_Click"
                                                                            CommandName="printrep">Rep</asp:LinkButton>
                                                                    </HeaderTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Email">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_email" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn Visible="true">
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chbSOLAll" runat="server" EnableViewState="true" OnCheckedChanged="ChkAll_CheckedChanged"
                                                                            AutoPostBack="True" />
                                                                    </HeaderTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chb" runat="server" EnableViewState="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="TrialRoom" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialroom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trilroom") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchdae1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        &nbsp;
                                                                    </EditItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="BatchEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchemp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblprintdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="PrintEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblrintemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printemp") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:HiddenField ID="HiddenField5" runat="server" Value="0" />
                                                        <asp:HiddenField ID="hfSOL" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" style="width: 900px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" background="../Images/separator_repeat.gif" height="10" style="width: 900px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <aspnew:UpdateProgress ID="UpdateProgress4" AssociatedUpdatePanelID="UpdatePanel5"
                                            runat="server">
                                            <ProgressTemplate>
                                                <img alt="Please wait" src="../Images/plzwait.gif" />
                                                Please wait work in progress.......
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="chk_ShowPrintedLetters" />
                                        <aspnew:AsyncPostBackTrigger ControlID="fromdate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="todate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_SalesRep" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_Courts" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <aspnew:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="server"
                                            TargetControlID="PnlLORLetterPrinted" CollapseControlID="PnlLORLetterPrintedHeader"
                                            Collapsed="true" ExpandControlID="PnlLORLetterPrintedHeader" ImageControlID="ImgBtnLORLetterPrinted"
                                            CollapsedImage="../Images/folder.gif" ExpandedImage="../Images/folderopen.gif">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                            <tr bgcolor="#eeeeee">
                                                <td style="width: 70%">
                                                    <asp:Panel ID="PnlLORLetterPrintedHeader" runat="server" Style="width: 100%">
                                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                            <tr bgcolor="#eeeeee">
                                                                <td valign="bottom" width="3%" style="height: 27px">
                                                                    <asp:ImageButton ID="ImgBtnLORLetterPrinted1" runat="server" Style="display: none"
                                                                        OnClick="ImgBtnLORLetterPrinted_Click" />
                                                                    <asp:Image ID="ImgBtnLORLetterPrinted" runat="server" />
                                                                </td>
                                                                <td class="style1" width="67%" valign="bottom" align="left">
                                                                    <strong>LOR Letters</strong>
                                                                </td>
                                                                <td valign="bottom" style="height: 27px" width="30%">
                                                                    &nbsp;&nbsp;<asp:Label ID="lblLORLetterPrinted" runat="server" Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td valign="top" align="right" style="height: 27px; width: 30%">
                                                    &nbsp; &nbsp;
                                                    <asp:ImageButton ID="imgbtnLORDelete" runat="server" ImageUrl="../Images/remove.gif"
                                                        ToolTip="Delete" CommandArgument="15" />
                                                    <asp:ImageButton ID="imgbtnLORPrintDelete" runat="server" ImageUrl="../Images/DelPrnt.jpg"
                                                        ToolTip="Deleted Print" Visible="False" />
                                                    <asp:ImageButton ID="imgbtnLORPrint" runat="server" ImageUrl="../Images/PrintNew1.jpg"
                                                        OnClientClick="return ValidateCheckbox('gv_LOR');" ToolTip="Print LOR Letter" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="PnlLORLetterPrinted" runat="server" Style="width: 100%">
                                            <table style="width: 100%">
                                                <tr id="rowLORdetail">
                                                    <td colspan="6" style="width: 900px;" valign="top">
                                                        <asp:GridView ID="gv_LOR" runat="server" AutoGenerateColumns="false" PagerSettings-Visible="true"
                                                            Width="100%" AllowPaging="false">
                                                            <AlternatingRowStyle BackColor="#EEEEEE"></AlternatingRowStyle>
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader" VerticalAlign="Middle">
                                                            </HeaderStyle>
                                                            <RowStyle HorizontalAlign="Left" />
                                                            <AlternatingRowStyle HorizontalAlign="Left" />
                                                            <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr style="display: none">
                                                                                <td>
                                                                                    <asp:Label ID="lbl_tictrial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_tictrial_2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_batchid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchid") %>'>
                                                                                    </asp:Label><asp:Label ID="lbl_IsSplit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsSplit") %>'></asp:Label>
                                                                                    <asp:TextBox ID="txt_tpnt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isprinted") %>'
                                                                                        Width="22px">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HyperLink ID="lbl_name" runat="server" CssClass="Label" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' ToolTip='<%# DataBinder.Eval(Container, "DataItem.Tooltip") %>'>
                                                                                    </asp:HyperLink>
                                                                                    <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>' />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText=" Crt Status Court Date & Room ">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hdnCourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Courtid") %>'>
                                                                        </asp:HiddenField>
                                                                        <asp:HiddenField ID="hdnCourtName" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Courtname") %>'>
                                                                        </asp:HiddenField>
                                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_RoomNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>'></asp:Label>&nbsp;
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Batch Date&amp; Rep">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_batchdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_BatchRep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.b_emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Print Date &amp; Rep">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_printdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.printdate","{0:M-dd-yyyy}") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_PrintEmp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.p_Emp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_email" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_BondClient" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.bond") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TrialDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TrialRoom" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltrialroom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trilroom") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="BatchDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchdae1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="BatchEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblbatchemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.batchemp") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="PrintDate" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblprintdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printdate1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="PrintEmp" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblrintemp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.printemp") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="img_status1" runat="server" Visible="False" ImageUrl="~/Images/right.gif"
                                                                            ToolTip="Email already Sent" />
                                                                        <asp:HiddenField ID="hfEmailFlag1" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.isprinted") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="true">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chb" runat="server" onClick="check(this,'gv_LOR')" EnableViewState="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:HiddenField ID="hfLOR" runat="server" Value="0" />
                                                        <asp:GridView ID="gvLORPrinter" Visible="false" runat="server" AutoGenerateColumns="False"
                                                            Width="100%" CssClass="clsLeftPaddingTable" AllowSorting="false" AllowPaging="false">
                                                            <Columns>
                                                                <asp:BoundField HeaderText="Client's Full Name" HeaderStyle-CssClass="clssubhead"
                                                                    DataField="FullName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Court & Court Date" ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Court" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtShortName") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_trialdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.currentdateset") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket No" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                                    ControlStyle-Width="160px" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnkTicketId" CssClass="Label" runat="server" Text='<%#Eval("refcasenumber") %>'
                                                                            NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_TicketId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ControlStyle-Width="40px" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Bond" runat="server" CssClass="Label" Text=""></asp:Label>
                                                                        <asp:HiddenField ID="hf_Bond" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.IsBond")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Batch Date & Time" HeaderStyle-CssClass="clssubhead"
                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DocPath") %>' Style="display: none"></asp:Label>
                                                                        <asp:Label ID="lblCmail" runat="server" Text='<%#Eval("USPSTrackingNumber") %>' Style="display: none"></asp:Label>
                                                                        <asp:Label ID="lblPrintDate" runat="server" Text='<%#Eval("PrintDate") %>' Style="display: none"></asp:Label>
                                                                        <asp:Label ID="lblRep" runat="server" Text='<%#Eval("Rep") %>' Style="display: none"></asp:Label>
                                                                        <asp:Label ID="lblBatchDate" runat="server" Text='<%#Eval("BatchDate") %>' Style="display: none"></asp:Label>
                                                                        <asp:Label ID="lblPrintRep" runat="server" Text='<%#Eval("PrintRep") %>' Style="display: none"></asp:Label>
                                                                        <asp:Label ID="lblEDeliveryFlag" runat="server" Text='<%#Eval("EDeliveryFlag") %>'
                                                                            Style="display: none"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Rep" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Printed Date & Time" HeaderStyle-CssClass="clssubhead"
                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Rep" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Printed" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" style="width: 900px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" background="../Images/separator_repeat.gif" height="10" style="width: 900px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <aspnew:UpdateProgress ID="UpdateProgress5" AssociatedUpdatePanelID="UpdatePanel6"
                                            runat="server">
                                            <ProgressTemplate>
                                                <img alt="Please wait" src="../Images/plzwait.gif" />
                                                Please wait work in progress.......
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="chk_ShowPrintedLetters" />
                                        <aspnew:AsyncPostBackTrigger ControlID="fromdate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="todate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_SalesRep" />
                                        <aspnew:AsyncPostBackTrigger ControlID="ddl_Courts" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none; width: 100%;">
                                <asp:TextBox ID="txt_trialset" runat="server" Width="24px">0</asp:TextBox><asp:TextBox
                                    ID="txt_contiset" runat="server" Width="32px">0</asp:TextBox><asp:TextBox ID="txt_repset"
                                        runat="server" Width="32px">0</asp:TextBox><asp:TextBox ID="txt_repprintcount" runat="server"
                                            Width="32px"></asp:TextBox><asp:TextBox ID="txt_receiptset" runat="server" Width="32px">0</asp:TextBox>
                                <asp:TextBox ID="txt_sat" runat="server" Width="32px"></asp:TextBox>
                                <asp:TextBox ID="txt_repseldel" runat="server" Width="16px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%" align="left" colspan="2">
                                <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" width="100%" align="left" style="display: none">
                                <asp:TextBox ID="txt_TicketIDs" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txt_TicketIDsbatch" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txt_BatchDates" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txt_LetterType" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtStopBubbling" runat="server">0</asp:TextBox>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BatchID") %>'></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <aspnew:UpdatePanel ID="UpdatePanel8" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlCertifiedMailNumber" Style="display: none;" runat="server" Width="425px">
                    <table border="2" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
                        border-top-color: navy; border-collapse: collapse; border-right-color: navy"
                        width="425">
                        <tr>
                            <td background="../Images/subhead_bg.gif" valign="bottom">
                                <table border="0" width="425" style="height: 26px">
                                    <tr>
                                        <td class="clssubhead" style="height: 26px">
                                            Certified Mail Number Box
                                        </td>
                                        <td align="right">
                                            <asp:LinkButton ID="lbtn_cmn_close" runat="server">X</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="425" border="0" class="clsLeftPaddingTable ">
                                    <tr>
                                        <td class="clsLabelNew">
                                            Certified Mail Number
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCertifiedMailNumber" runat="server" TextMode="Password" Width="250px"
                                                MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabelNew">
                                            Verify Certified Mail Number
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCertifiedMailNumberRetype" runat="server" TextMode="Password"
                                                Width="250px" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabelNew">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btnSubmit_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none; width: 668px;">
                                <asp:TextBox ID="txtLORFiled" runat="server" Width="24px">0</asp:TextBox>
                                <asp:HiddenField ID="hfLORIds" runat="server" Value="test" />
                                <asp:HiddenField ID="LetterType" runat="server" Value="0" />
                                <asp:HiddenField ID="hfLORBatchIds" runat="server" Value="test" />
                                <asp:HiddenField ID="hfLORCourtId" runat="server" Value="test" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MPECertifiedMainNumberPopup" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="lbtn_cmn_close" PopupControlID="pnlCertifiedMailNumber" TargetControlID="btndummynone">
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:AnimationExtender ID="AEFadeIn" runat="server" Enabled="True" TargetControlID="hfFadeIn">
                    <Animations>
                <OnClick>
                    <%-- We need set the AnimationTarget with the control which needs to make animation --%>
                    <Sequence AnimationTarget="pnlCertifiedMailNumber">
                        <%--The FadeIn and Display animation.--%>                     
                        <FadeIn Duration=".3" MinimumOpacity="0" MaximumOpacity="1" />
                        <ReSize AnimationTarget="pnlCertifiedMailNumber" height="124px" width="430px" duration ="0" fps="0" unit="px" />
                        
                    </Sequence>
                </OnClick>
                    </Animations>
                </ajaxToolkit:AnimationExtender>
                <asp:HiddenField runat="server" ID="hfFadeIn" />
                <asp:Button ID="btndummynone" runat="server" />
            </ContentTemplate>
        </aspnew:UpdatePanel>
        <asp:Panel ID="divDisable" runat="server" Style="z-index: 1; display: none; position: absolute;
            left: 1; top: 1; height: 1px; background-color: Silver; filter: alpha(opacity=50)">
        </asp:Panel>
        <aspnew:UpdatePanel ID="UpdatePanel7" runat="server">
            <ContentTemplate>
                <asp:Button ID="Button1" runat="server" Text="Cancel" Style="display: none;" />
                <ajaxToolkit:ModalPopupExtender ID="MPExtConfirm" runat="server" TargetControlID="Button1"
                    PopupControlID="tablepopup" CancelControlID="btncancel" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="panel1" runat="server" BackColor="#eff4fb">
                    <table id="tablepopup" style="border-top: black thin solid; border-left: black thin solid;
                        border-bottom: black thin solid; border-right: black thin solid" cellpadding="0"
                        cellspacing="0">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                                style="width: 353px">
                                <table style="width: 407px" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td class="clssubhead" align="left" style="height: 16px">
                                                &nbsp;Confirmation Box
                                            </td>
                                            <td align="right" style="height: 16px">
                                                &nbsp;<asp:LinkButton ID="lnkbtncancelpopup" runat="server">X</asp:LinkButton>&nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td align="center">
                                <table width="353px" cellspacing="1" cellpadding="0">
                                    <tr>
                                        <td>
                                            &nbsp;<img src="/images/QuestionIcon.png" border="0" width="50" height="50" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblmessageshow" runat="server" BackColor="#eff4fb" ForeColor="#3366CC"
                                                Text="Are you sure you want to activate this Flag and send a letter to the client requesting for updated contact information?"
                                                Width="353px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnok" runat="server" Text="Yes" Style="background: url(../Images/btnOk.ico) no-repeat 2px 0px;"
                                                CssClass="clsbutton" Height="27px" Width="91px" OnClick="btnok_Click" />&nbsp;
                                            <asp:Button ID="btncancel" runat="server" Text="No" Style="background: url(../Images/btnClose.gif) no-repeat 2px 0px;"
                                                CssClass="clsbutton" Height="27px" Width="91px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 10px">
                            <td bgcolor="#eff4fb">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>

<script language="javascript" type="text/javascript">
		function ShowMsg()
        {
            document.getElementById("txt_StateTraceMsg").value=err;
        }
</script>

</html>

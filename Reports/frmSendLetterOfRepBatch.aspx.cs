﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;


namespace HTP.Reports
{
    public partial class frmSendLetterOfRepBatch : System.Web.UI.Page
    {
        int ticketno;
        int empid;
        int lettertype;
        bool isBatch;
        clsSession uSession = new clsSession();
        clsLogger clog = new clsLogger();

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (uSession.GetCookie("sEmpID", this.Request) != "")
                {
                    ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
                    empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                    lettertype = Convert.ToInt32(Request.QueryString["lettertype"]);
                    isBatch = (Request.QueryString["batch"] == null ? false : Convert.ToBoolean(Request.QueryString["batch"].ToString()));

                    SendToBatch();
                    //Response.Write("<script language='javascript'> window.opener = 'simpleLORbatch'; window.close();   </script>");        
                    //Ozair 5359 02/13/2009 Resolve the Window Close information msg
                    //Nasir 6181 07/27/2009 alert message
                    HttpContext.Current.Response.Write("<script language='javascript'> alert('Letter of Rep sent to batch successfully'); </script>");
                    HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");

                }
                else
                {
                    Response.Redirect("../frmlogin.aspx");
                }
            }
        }

        private void SendToBatch()
        {
            try
            {
                if (lettertype == 6 && isBatch)
                {
                    //string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath", "@PageCount" };
                    //object[] values = { ticketno, System.DateTime.Now, "1/1/1900", 0, lettertype, empid, "batchLOR.pdf", 1 };
                    //clsENationWebComponents clsDB = new clsENationWebComponents();
                    //clsDB.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);
                    //Fahad 9022 03/14/2011 Method Added
                    clsBatch objClsBatch = new clsBatch();
                    objClsBatch.SendLORToBatch(ticketno, empid, lettertype, isBatch);
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

    }
}

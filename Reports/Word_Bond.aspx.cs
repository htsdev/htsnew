using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for Word_Bond.
	/// </summary>
	public partial class Word_Bond : System.Web.UI.Page
	{
		int ticketno;
		int empid;
		int CourtType;
		protected System.Web.UI.WebControls.ImageButton IBtn;
		clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsSession uSession = new clsSession();
        clsLogger log = new clsLogger();
        int LetterType = 4;			

		private void Page_Load(object sender, System.EventArgs e)
		{
		
            // tahir 4477 07/28/2008
            // replaced the session variable with query string..
            CourtType = Convert.ToInt32(Request.QueryString["CourtType"]);
            ViewState["CourtType"] = CourtType;
            // end 4474

			ticketno =Convert.ToInt32(Request.QueryString["casenumber"]);
			ViewState["vTicketId"]=ticketno;

			empid =Convert.ToInt32(uSession.GetCookie("sEmpID",this.Request));
			 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.IBtn.Click += new System.Web.UI.ImageClickEventHandler(this.IBtn_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void IBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (CourtType == 0)
				CreateBondReport();
			else
				CreateBondSuretyReport();
		
		}

        public void CreateBondReport()
        {
            string[] keyMain = { "@TicketIDList", "@empid" };
            object[] valueMain = { ticketno, empid };
            string[] keySub = { "@TicketID" };
            object[] valueSub = { ticketno };

            string filename = Server.MapPath("") + "\\Bond.rpt";
            Cr.CreateReportWord(filename, "sp_BondLetter", "USP_HTS_LOR", keyMain, valueMain, keySub, valueSub, "false", LetterType, this.Session, this.Response);
            //Fahad 5978 06/02/2009 History Notes Added
            log.AddNote(empid, "Bond Printed with Changes", "Bond Printed with Changes", Convert.ToInt32(ViewState["vTicketId"]));
        }

        public void CreateBondSuretyReport()
        {
            string[] key = { "@TicketIDList", "@empid" };
            object[] value1 = { ticketno, empid };
            string filename = Server.MapPath("") + "\\Bond_Surety.rpt";
            Cr.CreateReportWord(filename, "sp_BondLetter", key, value1, "false", LetterType, this.Session, this.Response);
            //Fahad 5978 06/02/2009 History Notes Added
            log.AddNote(empid, "Bond Surety Printed with Changes", "Bond Surety Printed with Changes", Convert.ToInt32(ViewState["vTicketId"]));
        }
	}
}

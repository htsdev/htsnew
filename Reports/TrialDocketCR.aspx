<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrialDocketCR.aspx.cs" Inherits="lntechNew.Reports.TrialDocketCR" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Crystal Report Version</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
    <tr id="trpdf" height=100%>
    
 
					<td colSpan="3">&nbsp; <iframe tabIndex="0" src="RptTrialDocket.aspx?courtloc=<%=ViewState["Courtloc"]%>&courtdate=<%=ViewState["Courtdate"]%>&page=<%=ViewState["Page"]%>&courtnumber=<%=ViewState["Courtnumber"]%>&datetype=<%=ViewState["Datetype"]%>&singles=<%=ViewState["singles"]%>&showowesdetail=<%=ViewState["showdetail"] %> " frameBorder="1" width="98%" scrolling="auto"
							height="98%"></iframe>
					</td>
				</tr>
    </table>
    </div>
    </form>
</body>
</html>

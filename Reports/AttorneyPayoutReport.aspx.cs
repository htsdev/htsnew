﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using HTP.Components.Services;
using FrameWorkEnation.Components;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using WebSupergoo.ABCpdf6;


namespace HTP.Reports
{
    public partial class AttorneyPayoutReport : System.Web.UI.Page
    {
        #region variables

        clsLogger bugTracker = new clsLogger();
        DataView dvCom = new DataView();
        bool IsSearch;
        bool ishistorysearch = false;
        int Empid = 0;
        clsSession cSession = new clsSession();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsFirms cfirms = new clsFirms();
        string URL = string.Empty;

        #endregion

        #region Properites

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                       (!int.TryParse(cSession.GetCookie("sEmpID", this.Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        cal_FromDateFilter.SelectedDate = DateTime.Today;
                        cal_ToDateFilter.SelectedDate = DateTime.Today;
                        FillddlFirms();
                        ViewState["URL"] = Request.Url.Authority;
                    }

                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;

                    PagingctrlHistory.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingctrlHistory_PageIndexChanged);
                    PagingctrlHistory.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(PagingctrlHistory_PageSizeChanged);
                    PagingctrlHistory.GridView = gv_history;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                BindGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                BindGrid();
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_history_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_history.PageIndex = e.NewPageIndex;
                BindHistoryGrid();
                PagingctrlHistory.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                IsSearch = true;
                GridViewSortExpression = "dispositiondate";
                ViewState["firmid"] = ddlAttorneys.SelectedValue;
                ViewState["startdate"] = cal_FromDateFilter.SelectedDate;
                ViewState["enddate"] = cal_ToDateFilter.SelectedDate;
                BindGrid();
                IsSearch = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                IsSearch = true;
                DataView dvreport = (DataView)Session["dvCom"];
                if (dvreport != null && dvreport.Count > 0)
                {
                    dvreport.RowStateFilter = DataViewRowState.CurrentRows;
                    dvreport.RowFilter = "TicketsViolationID in (" + hf_ticketviolationids.Value.Substring(0, hf_ticketviolationids.Value.LastIndexOf(",")) + ")";
                    dvreport.Sort = "dispositiondate desc";
                    DataTable dtreport = dvreport.ToTable();
                    //Yasir Kamal 7725 05/20/2010 display current date instead of disposition
                    //Sabir Khan 7725 06/07/2010 Variable name has been changed into proper name.
                    DateTime currDate = DateTime.Now; 
                    string historynote = "Case Coverage through " + currDate.ToString("MMMM") + " " + currDate.ToString("yyyy");


                    DateTime printdate = DateTime.Now;

                    if (dtreport != null && dtreport.Rows.Count > 0)
                    {
                        string filename = Server.MapPath("") + "\\AttorneyPayoutReport.rpt";
                        string serverpath = Convert.ToString(ConfigurationManager.AppSettings["NTPATHBatchPrint"]);
                        string returnfile = CreateReport(dtreport, filename);
                        string datetime = DateTime.Now.ToFileTime().ToString();
                        string newfilepath = serverpath + datetime + " AttorneyPayout.pdf";
                        File.Copy(returnfile, newfilepath);
                        int ticketid = 0;
                        foreach (DataRow dr in dtreport.Rows)
                        {
                            if (ticketid != Convert.ToInt32(dr["ticketid_pk"]))
                            {
                                cfirms.InsertIntoHTSNotes(0, Convert.ToInt32(dr["ticketid_pk"]), printdate, 32, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), historynote, Path.GetFileName(newfilepath), historynote);
                            }
                            ticketid = Convert.ToInt32(dr["ticketid_pk"]);
                            cfirms.UpdateAttorneyPaidAmount(1, Convert.ToDouble(dr["Attorneyfee"]), printdate, Convert.ToInt32(dr["TicketsViolationID"]));
                        }

                        cfirms.InsertAttorneyPaidHistory(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), Convert.ToInt32(ViewState["firmid"]), printdate, Path.GetFileName(newfilepath));
                        BindGrid();

                        if (File.Exists(returnfile))
                            File.Delete(returnfile);

                        HttpContext.Current.Response.Write("<script language='JavaScript' type='text/javascript'>window.open('../DOCSTORAGE/BatchPrint/" + Path.GetFileName(newfilepath) + "');</script>");
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_history_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //hf_filename
                string filename = ((HiddenField)e.Row.FindControl("hf_filename")).Value;
                ((ImageButton)e.Row.FindControl("imgPreviewDoc")).Attributes.Add("OnClick", "javascript:return showHistoryPDF('" + filename + "');");
            }
        }

        protected void lnk_history_Click(object sender, EventArgs e)
        {
            try
            {
                gv_Records.Visible = false;
                gv_history.Visible = false;
                btnUpdate.Visible = false;
                btnSearch.Visible = false;
                btnSearchhistory.Visible = true;
                lbl_Message.Visible = false;
                lnk_back.Visible = true;
                lnk_history.Visible = false;
                Pagingctrl.Visible = false;
                PagingctrlHistory.Visible = false;
                resetcontrol();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnk_back_Click(object sender, EventArgs e)
        {
            try
            {
                gv_Records.Visible = false;
                gv_history.Visible = false;
                btnUpdate.Visible = false;
                btnSearch.Visible = true;
                btnSearchhistory.Visible = false;
                lbl_Message.Visible = false;
                lnk_back.Visible = false;
                lnk_history.Visible = true;
                Pagingctrl.Visible = false;
                PagingctrlHistory.Visible = false;
                resetcontrol();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSearchhistory_Click(object sender, EventArgs e)
        {
            try
            {
                ishistorysearch = true;
                Pagingctrl.Visible = false;
                BindHistoryGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// Fill firm drop down of attorneys
        /// </summary>
        private void FillddlFirms()
        {
            try
            {
                //DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_GET_CriminalCase_Attorney");
                DataTable dt = cfirms.GetCriminalAttorney();
                if (dt.Rows.Count > 0)
                {
                    ddlAttorneys.DataSource = dt;
                    ddlAttorneys.DataBind();
                }
                ddlAttorneys.Items.Insert(0, new ListItem("---- Choose ----", "-1"));
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                if (Session["dvCom"] != null)
                {
                    gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void PagingctrlHistory_PageIndexChanged()
        {
            try
            {
                ishistorysearch = false;

                if (Session["dthistory"] != null)
                {
                    gv_history.PageIndex = PagingctrlHistory.PageIndex - 1;
                    BindHistoryGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void PagingctrlHistory_PageSizeChanged(int pageSize)
        {
            try
            {
                ishistorysearch = false;

                if (pageSize > 0)
                {
                    gv_history.PageIndex = 0;
                    gv_history.PageSize = pageSize;
                    gv_history.AllowPaging = true;
                }
                else
                {
                    gv_history.AllowPaging = false;
                }
                BindHistoryGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// get data and bind grid for comment report
        /// </summary>
        public void BindGrid()
        {
            DataTable dt = new DataTable();
            if (IsSearch)
            {
                dt = cfirms.GetAttorneyPayoutReport(Convert.ToDateTime(ViewState["startdate"]), Convert.ToDateTime(ViewState["enddate"]), Convert.ToInt32(ViewState["firmid"]), Convert.ToString(ViewState["URL"]));
                Session["dvCom"] = dt.DefaultView;
                gv_Records.PageSize = 20;
                gv_Records.PageIndex = 0;
                IsSearch = false;
            }

            if (Session["dvCom"] != null && dt.Rows.Count == 0)
            {
                dvCom = (DataView)Session["dvCom"];
                dt = dvCom.ToTable();

            }
            if (dt.Rows.Count > 0)
            {
                btnUpdate.Visible = true;
                dvCom = dt.DefaultView;
                dvCom.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                dt = dvCom.ToTable();
                GenerateSerialNo(dt);
                dvCom = dt.DefaultView;
                Session["dvCom"] = dvCom;
                gv_Records.DataSource = dvCom;
                gv_Records.DataBind();

                Pagingctrl.Visible = true;
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                gv_Records.Visible = true;
                lbl_Message.Text = "";
            }
            else
            {
                btnUpdate.Visible = false;
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Visible = true;
                lbl_Message.Text = "No Records Found";
                gv_Records.Visible = false;
            }

        }

        /// <summary>
        /// Generate serial number in given datatable ticket wise
        /// </summary>
        /// <param name="dtRecords"></param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Remove("sno");

                dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        private string CreateReport(DataTable dv, string filename)
        {
            string myExportFile = null;
            try
            {
                if (!(Directory.Exists("C:\\Temp")))
                {
                    Directory.CreateDirectory("C:\\Temp");
                }

                ReportDocument ReportFile = new ReportDocument();

                ReportFile.Load(filename);
                ReportFile.SetDataSource(dv);

                CrystalDecisions.Shared.ExportOptions myExportOptions;
                CrystalDecisions.Shared.DiskFileDestinationOptions myDiskFileDestinationOptions;

                myExportFile = "C:\\Temp\\" + Session.SessionID.ToString() + ".pdf";

                myDiskFileDestinationOptions = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                myDiskFileDestinationOptions.DiskFileName = myExportFile;
                myExportOptions = ReportFile.ExportOptions;

                myExportOptions.DestinationOptions = myDiskFileDestinationOptions;
                myExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                myExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                ReportFile.Export();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                myExportFile = null;
            }


            // Write file to a page.
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.ContentType = "application/pdf";
            //Response.WriteFile(myExportFile);
            //Response.Flush();
            //Response.Close();
            return myExportFile;
        }

        private void BindHistoryGrid()
        {
            ViewState["firmidhistory"] = ddlAttorneys.SelectedValue;
            ViewState["startdatehistory"] = cal_FromDateFilter.SelectedDate;
            ViewState["enddatehistory"] = cal_ToDateFilter.SelectedDate;
            DataTable dthistory = null;
            if (Session["dthistory"] == null || ishistorysearch == true)
            {
                dthistory = GetAttorneyPaidHistory(Convert.ToDateTime(ViewState["startdatehistory"]), Convert.ToDateTime(ViewState["enddatehistory"]), Convert.ToInt32(ViewState["firmidhistory"]));
                Session["dthistory"] = dthistory;
            }
            else
                dthistory = (DataTable)Session["dthistory"];

            if (dthistory.Rows.Count > 0)
            {
                gv_history.Visible = true;
                gv_history.DataSource = dthistory;
                gv_history.DataBind();

                PagingctrlHistory.Visible = true;
                PagingctrlHistory.PageCount = gv_history.PageCount;
                PagingctrlHistory.PageIndex = gv_history.PageIndex;
                PagingctrlHistory.SetPageIndex();
                lbl_Message.Text = "";
            }
            else
            {
                PagingctrlHistory.PageCount = 0;
                PagingctrlHistory.PageIndex = 0;
                PagingctrlHistory.SetPageIndex();
                lbl_Message.Visible = true;
                lbl_Message.Text = "No Records Found";
                gv_history.Visible = false;
            }
        }

        private DataTable GetAttorneyPaidHistory(DateTime startdate, DateTime enddate, int attorneyid)
        {
            DataTable dthistory = null;
            string[] key = { "@startdate", "@enddate", "@attorney" };
            object[] value = { startdate, enddate, attorneyid };
            dthistory = ClsDb.Get_DT_BySPArr("USP_HTP_GetAttorneyPaidHistory", key, value);
            return dthistory;
        }

        private void resetcontrol()
        {
            ddlAttorneys.SelectedIndex = 0;
            cal_FromDateFilter.SelectedDate = DateTime.Now;
            cal_ToDateFilter.SelectedDate = DateTime.Now;
        }

        #endregion
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class HCCCriminalPopup : System.Web.UI.Page
    {
        #region Variables
        public HccCriminalReport HCCReport = new HccCriminalReport();
        DataSet ds = new DataSet();
        clsLogger bugTracker = new clsLogger();
        string BookingNumber = string.Empty;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Display();
                }

            }
            catch (Exception ex)
            {
                
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        private void Display()
        {
            try
            {
                BookingNumber = Request.QueryString["bookingnumber"].ToString();
                string CaseNumber = Request.QueryString["casenumber"];
                ds = new DataSet();
                ds = HCCReport.CaseDetail(BookingNumber,CaseNumber);

                if (ds.Tables.Count > 0)
                {

                    DisplayGrid(gvSection1, ds.Tables[0], lblError1);
                    DisplayGrid(gvSection2, ds.Tables[1], lblerror2);
                    DisplayGrid(gvSection3, ds.Tables[2], lblerror3);
                    DisplayGrid(gvSection5, ds.Tables[3], lblerror5);
                    DisplayGrid(gvSection6, ds.Tables[4], lblerror6);
                    getConnectionInformation();
                    lblmain.Visible = false;
                    TableMain.Visible = true;

                }
                else
                {
                    lblmain.Visible = true;
                    TableMain.Visible = false;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private string FormatPhoneNo(string str)
        {
            string res = "";
            if (str.Length == 10)
            {
                res += "(";
                res += str.Substring(0, 3);
                res += ") ";
                res += str.Substring(3, 3);
                res += "-";
                res += str.Substring(6, 4);
                return res;
            }
            else
                return str;
        }


        private void getConnectionInformation()
        {

            try
            {

                if (Request.QueryString["casenumber"] != null)
                {

                    DataSet ds = HCCReport.GetConnectedPersons(Request.QueryString["casenumber"]);
                    DisplayGrid(gv_qcoc, ds.Tables[0], lblerror7);
                }

            }
            catch (Exception ex)
            {
                ShowConnectedPerson(false);
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void ShowConnectedPerson(bool visible)
        {
           lblerror7.Visible =true;
            gv_qcoc.Visible = visible;

        }


        private void DisplayGrid(GridView gvResult, DataTable dt, Label error)
        {

            try
            {

                if (dt.Rows.Count > 0)
                {
                    gvResult.DataSource = dt;
                    gvResult.DataBind();

                    error.Visible = false;
                    gvResult.Visible = true;

                }
                else
                {
                    error.Visible = true;
                    gvResult.Visible = false;
                }
            }
            catch (Exception ex)
            {
                error.Visible = true;
                gvResult.Visible = false;
            }

        }

        protected void gvSection6_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            {

                ((Label)e.Row.FindControl("lblBaliffNo")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblBaliffNo")).Text);
                ((Label)e.Row.FindControl("lblCoordinatorNo")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblCoordinatorNo")).Text);
                ((Label)e.Row.FindControl("lblClerkANo")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblClerkANo")).Text);
                ((Label)e.Row.FindControl("lblCourtClerkNo")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblCourtClerkNo")).Text);
                ((Label)e.Row.FindControl("lblCourtPhone")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblCourtPhone")).Text);
                ((Label)e.Row.FindControl("lblReporterNo")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblReporterNo")).Text);
                ((Label)e.Row.FindControl("lblServerNo")).Text = FormatPhoneNo(((Label)e.Row.FindControl("lblServerNo")).Text);
            }
        }

        protected void gvSection3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            {
                DataRowView drv =  (DataRowView)e.Row.DataItem;
                hlSection3And4.NavigateUrl = "HccCriminalCase.aspx?section=3&spn=" + drv["spn"];
                hlSection7.NavigateUrl = "HccCriminalCase.aspx?section=7&caseno=" + drv["casenumber"];
                int court= int.Parse(drv["ccr"].ToString());
                hlSection5.NavigateUrl = "HccCriminalCase.aspx?section=5&court=" + court;    
            }
        }        
    }       
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using LNHelper;

namespace HTP.Reports
{
    public partial class ReminderCallValidation : System.Web.UI.Page
    {
        DBComponent ClsDb = new DBComponent();
        clsLogger bugTracker = new clsLogger();


        //Yasir Kamal 6050 06/17/2009 pagging control and showsetting control added.

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    bindData();
                }
                Pagingctrl.GridView = gv_Records;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            bindData();

        }

        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lblMessage.Text = ErrorMsg;

        }


        protected void bindData()
        {
            try
            {
                lblMessage.Text = "";
                // checked  by khalid for bug 2210  not time out expire
                DataTable dt = ClsDb.GetDTBySPArr("USP_HTS_reminderCalls_For_ValidationReport");

                DataColumn dc = new DataColumn("SNo");
                dc.DataType = typeof (Int32);
                dt.Columns.Add(dc);
                //Yasir Kamal 6050 07/01/2009 Serial No bug fixed.

                //Farrukh 9332 08/27/2011 Exception handling if no record found
                if (dt.Rows.Count > 0)
                {
                    int sno = 1;
                    dt.Rows[0]["SNo"] = 1;

                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        // Sabir Khan 10012 02/24/2012 Fixed record count issue...
                        //if (dt.Rows[i - 1]["ticketid"].ToString() != dt.Rows[i]["ticketid"].ToString())
                        //{
                            dt.Rows[i]["Sno"] = ++sno;
                        //}
                    }



                    ////Sabir 4272 07/16/2008 Convert datatable to dataview for sorting..
                    ////Start Here
                    //DataView dv = new DataView(dt);
                    //if (ViewState["sortexp"] != null || ViewState["sortdir"] != null)
                    //{
                    //    dv.Sort = ViewState["sortexp"] + " " + ViewState["sortdir"];
                    //}
                    gv_Records.Visible = true;
                    gv_Records.DataSource = dt;
                    gv_Records.DataBind();
                    Pagingctrl.PageCount = gv_Records.PageCount;
                    Pagingctrl.PageIndex = gv_Records.PageIndex;
                    Pagingctrl.SetPageIndex();
                    //End Here
                }
                else
                {
                    gv_Records.Visible = false;
                    lblMessage.Text = "No records found";
                }

            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        
        
        /// <summary>
        /// handling pageIndexChange  of pagging control
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                bindData();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
        }
        /// <summary>
        /// handling GridView page index changing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                bindData();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Handling PaggingControl page Size Change event
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;

                }
                else
                {
                    gv_Records.AllowPaging = false;
                }

                bindData();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
        }


        ////Sabir 4272 07/16/2008  Implement sorting
        //protected void Gv_records_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        string sortExpression = string.Empty;
        //        string sortDirection = string.Empty;

        //        if (ViewState["sortexp"] == null && ViewState["sortdir"] == null)
        //        {
        //            ViewState["sortexp"] = "";
        //            ViewState["sortdir"] = "";
        //        }


        //        if (ViewState["sortexp"].ToString() == e.SortExpression)
        //        {
        //            sortExpression = e.SortExpression;

        //            if (ViewState["sortdir"].ToString() == "ASC")
        //            {
        //                sortDirection = "DESC";
        //            }
        //            else
        //            {
        //                sortDirection = "ASC";
        //            }
        //        }
        //        else
        //        {
        //            sortExpression = e.SortExpression;
        //            sortDirection = "ASC";
        //        }

        //        ViewState["sortexp"] = sortExpression;
        //        ViewState["sortdir"] = sortDirection;

        //        bindData();


        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Text = ex.Message;
        //        clsLogger.ErrorLog(ex);
        //    }
        //}
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewHireReport.aspx.cs"
    Inherits="HTP.Reports.NewHireReport" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Traffic New Hire Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function closeModalPopup(ticketid)
        {           
            var modalPopupBehavior = $find('mp_Showcomments');
            modalPopupBehavior.hide();            
            return false;
        }
        
        function ValidateComments()
        {   
            if (document.getElementById("txt_Gcomments").value.trim()=="")
            {
                alert("Please enter general comments.");
                return false;
            }
        }
        
    </script>

    <style type="text/css">
        .modalbackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                    <tr>
                        <td align="center">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 9px; background-image: url(../images/separator_repeat.gif);
                            width: 100%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="clsLeftPaddingTable" runat="server" id="tr_showsetting">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" style="height: 25px">
                                        <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of Business Days after case reset:"
                                            runat="server" Attribute_Key="Days" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="tr_showsettingseperator" runat="server">
                        <td align="center" style="height: 9px; background-image: url(../images/separator_repeat.gif)">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" align="left" style="height: 34px; background-image: url(../Images/subhead_bg.gif);">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="clssubhead">
                                        Traffic New Hire Report
                                    </td>
                                    <td align="right" class="clssubhead">
                                        <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-image: url(../images/separator_repeat.gif);
                            height: 11">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="Label" ForeColor="Red" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="center">
                            <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="100%" OnPageIndexChanging="gv_Data_PageIndexChanging" AllowPaging="True"
                                AllowSorting="False" PageSize="20" OnRowDataBound="gv_Data_RowDataBound" OnRowCommand="gv_Data_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("TicketID_PK") %>'
                                                Text='<%# Eval("SNo") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Lastname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="First Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# Eval("FirstName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sign up Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("paymentdate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sign up Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtTime" runat="server" CssClass="GridItemStyle" Text='<%# Eval("paymentdate","{0:hh:mm tt}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Source">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtNumber" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Source") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%--<asp:ImageButton ID="imgbtn_cancel" runat="server" ImageUrl="~/Images/cross.gif"
                                                ToolTip="Remove record from report" />--%>
                                            <asp:ImageButton ID="imgbtn_cancel" runat="server" ImageUrl="~/Images/cross.gif"
                                                ToolTip="Remove record from report" CommandName="remove" CommandArgument='<%# Eval("TicketID_PK") %>'
                                                Style="width: 16px" />
                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("TicketID_PK") %>' />
                                            <asp:HiddenField ID="hf_courtname" runat="server" Value='<%# Eval("CourtName") %>' />
                                            <asp:HiddenField ID="hf_gcomments" runat="server" Value='<%# Eval("GeneralComments") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-image: url(../images/separator_repeat.gif);
                            height: 11px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc2:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:ModalPopupExtender ID="mp_Showcomments" runat="server" CancelControlID="btncancel"
                    TargetControlID="button12" PopupControlID="pnl_msg" BackgroundCssClass="modalbackground"
                    HideDropDownList="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="button12" runat="server" Style="display: none;" />
                <asp:Panel ID="pnl_msg" runat="server">
                    <table id="tblsub" width="300" class="clsLeftPaddingTable" align="center" cellpadding="0"
                        cellspacing="1" bgcolor="white" border="0" style="border-top: black thin solid;
                        border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                        height: 60">
                        <tr>
                            <td class="clssubhead" align="center" style="height: 34px; background-image: url(../images/subhead_bg.gif)">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="clssubhead">
                                            <strong>General Comments</strong>
                                        </td>
                                        <td align="right">
                                            <asp:LinkButton ID="linkbutton1" OnClientClick="return closeModalPopup()" runat="server">x</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div style="height: 100px; overflow: scroll;">
                                    <asp:Label ID="lbl_comments" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:TextBox ID="txt_Gcomments" runat="server" TextMode="MultiLine" CssClass="clsInputadministration"
                                    Width="300px" Height="100px"></asp:TextBox>
                                <%--<cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Width="900px" Height="128px" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnupdatecomments" runat="server" Text="Update" CssClass="clsbutton"
                                    OnClientClick="return ValidateComments();" OnClick="btnupdatecomments_Click">
                                </asp:Button>
                                <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="clsbutton" OnClientClick="return closeModalPopup()">
                                </asp:Button>
                                <asp:HiddenField ID="hf_ticketidtoupdate" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HTP.Components;
using LNHelper;

namespace HTP.Reports
{
    public partial class ChildCustodyReport : System.Web.UI.Page
    {
        //Zeeshan Ahmed 4152 06/02/2008
        ChildCustodyCase childCustodyCase = new ChildCustodyCase();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FillControls();

                    calLoadDateFrom.SelectedDate = DateTime.Now;
                    calLoadDateTo.SelectedDate = DateTime.Now;
                    BindDataGrid();
                }
                PagingControl1.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(PagingControl1_PageIndexChanged);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }

        }

        protected void btn_save_cdi_Click(object sender, EventArgs e)
        {
            gvResult.PageIndex = 0;
            BindDataGrid();
        }


        private void FillControls()
        {
            ListItem item = new ListItem("--- All ---", "0");
            //Fill Court List
            ddlCourts.Items.Clear();
            ddlCourts.DataSource = childCustodyCase.GetChildCustodyCourt();
            ddlCourts.DataBind();
            ddlCourts.Items.Insert(0, item);

            //Fill Setting Type
            ddlSettingType.Items.Clear();
            ddlSettingType.DataSource = childCustodyCase.GetSettingType();
            ddlSettingType.DataBind();
            ddlSettingType.Items.Insert(0, item);

            //Fill Case Status
            ddlCaseStatus.Items.Clear();
            ddlCaseStatus.DataSource = childCustodyCase.GetChildCustodyStatus();
            ddlCaseStatus.DataBind();
            ddlCaseStatus.Items.Insert(0, item);
        }

        public void BindDataGrid()
        {

            try
            {
                lbl_Message.Text = "";
                DataTable dt = null;



                string scalDocketDateFrom = "", scalDocketDateTo = "";
                string scalLoadDateFrom = "", scalLoadDateTo = "";

                if (calDocketDateFrom.PostedDate != string.Empty || calDocketDateTo.PostedDate != string.Empty)
                {
                    scalDocketDateFrom = calDocketDateFrom.SelectedDate.ToShortDateString();
                    scalDocketDateTo = calDocketDateTo.SelectedDate.ToShortDateString();
                }

                if (calLoadDateFrom.PostedDate != string.Empty || calLoadDateTo.PostedDate != string.Empty)
                {
                    scalLoadDateFrom = calLoadDateFrom.SelectedDate.ToShortDateString();
                    scalLoadDateTo = calLoadDateTo.SelectedDate.ToShortDateString();
                }

                dt = childCustodyCase.GetChildCustodyReports(Convert.ToInt32(ddlCourts.SelectedValue), Convert.ToInt32(ddlSettingType.SelectedValue), Convert.ToInt32(ddlCaseStatus.SelectedValue), scalDocketDateFrom, scalDocketDateTo, scalLoadDateFrom, scalLoadDateTo);


                if (dt.Rows.Count > 0)
                {
                    dt = GenerateSeialNo(dt);
                    gvResult.AllowPaging = !cbshowall.Checked;
                    gvResult.DataSource = dt;
                    gvResult.DataBind();
                    tblresult.Visible = gvResult.Visible = true;
                    PagingControl1.PageIndex = gvResult.PageIndex;
                    PagingControl1.PageCount = gvResult.PageCount;
                    PagingControl1.SetPageIndex();

                    //Kazim 4262 6/24/2008 Set the visibilty of lines  

                    trHeaderImage.Style[HtmlTextWriterStyle.Display] = "block";
                    trImageFooter.Style[HtmlTextWriterStyle.Display] = "block";

                }
                else
                {
                    trHeaderImage.Style[HtmlTextWriterStyle.Display] = "none";
                    trImageFooter.Style[HtmlTextWriterStyle.Display] = "none";
                    lbl_Message.Text = "No Records Found ";
                    tblresult.Visible = gvResult.Visible = false;
                }

                // Display Active Tab                
            }
            catch (Exception ex)
            {
                tblresult.Visible = false;
                Logger.Instance.LogError(ex);
            }
        }

        private DataTable GenerateSeialNo(DataTable dt)
        {
            int count = 0;
            if (dt != null && dt.Rows.Count > 0 && !dt.Columns.Contains("SNo"))
            {
                dt.Columns.Add("SNo");
                for (int a = 0; a < dt.Rows.Count; a++)
                {
                    dt.Rows[a]["SNo"] = ++count;

                }
            }
            return dt;
        }

        protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvResult.PageIndex = e.NewPageIndex;
            BindDataGrid();
        }

        void PagingControl1_PageIndexChanged()
        {
            gvResult.PageIndex = PagingControl1.PageIndex - 1;
            BindDataGrid();
        }

        protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[8].Text == "1/1/1900" || e.Row.Cells[8].Text == "01/01/1900")
                    e.Row.Cells[8].Text = "N/A";
                if (e.Row.Cells[6].Text == "1/1/1900" || e.Row.Cells[5].Text == "01/01/1900")
                    e.Row.Cells[6].Text = "N/A";


            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using FrameWorkEnation.Components;
namespace HTP.Reports
{
    public partial class OtherPayments : System.Web.UI.Page
    {
        readonly clsSession _clsSession = new clsSession();
        readonly clsCase _cCase = new clsCase();
        readonly clsLogger _bugTracker = new clsLogger();
        DataSet _ds;
        readonly ValidationReports _reports = new ValidationReports();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_clsSession.IsValidSession(Request, Response, Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    chkPaymentsShowAll.Checked = false;


                    chkPaymentShowPast.Checked = true;
                    cal_FromDateFilter.SelectedDate = DateTime.Now;
                    
                    cal_ToDateFilter.SelectedDate = DateTime.Now;
                  
                    FillGrid();
                }
                
                Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                Pagingctrl.GridView = gv_Records;
                const string javascript = "EnabledPaymentsFilterControls('{0}','{1}','{2}','{3}',{4});";
                chkPaymentsShowAll.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkPaymentsShowAll.ClientID, chkPaymentShowPast.ClientID, 2));
                chkPaymentShowPast.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkPaymentsShowAll.ClientID, chkPaymentShowPast.ClientID, 3));

                if (chkPaymentsShowAll.Checked)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledPaymentsFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkPaymentsShowAll.ClientID, chkPaymentShowPast.ClientID, 2));
                }
                else if (chkPaymentShowPast.Checked)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledPaymentsFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkPaymentsShowAll.ClientID, chkPaymentShowPast.ClientID, 3));
                }
                
            }

        }
        
       
        private void FillGrid()
        {
            gv_Records.Visible = true;
            string[] keys = { "@ShowAllRecords", "@FromDate", "@ToDate", "@ShowPending" };
            object[] values = { chkPaymentsShowAll.Checked, cal_FromDateFilter.SelectedDate, cal_ToDateFilter.SelectedDate, chkPaymentShowPast.Checked };
            _reports.getRecords("usp_HTP_Get_OtherPayments", keys, values);
            _ds = _reports.records;
            if (_ds.Tables[0].Rows.Count > 0)
            {
                GenerateSerialNo(_ds.Tables[0]); 
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSourceID = null;
                gv_Records.DataSource = _ds.Tables[0];
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "";
            }
            else
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "No Records Found";
                gv_Records.Visible = false;
            }

        }
        
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();

        }
        private void GenerateSerialNo(DataTable dtRecords)
        {
            var sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (var i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["TicketID_PK"].ToString() != dtRecords.Rows[i]["TicketID_PK"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }
        // Row command event handler of gv_records.
        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Afaq 8213 04/20/2011 Add command name
            if (e.CommandName == "Deleted")
            {
                try
                {
                    clsENationWebComponents cclsENationWebComponents = new clsENationWebComponents();
                    if (cclsENationWebComponents.DeleteBySPByOneParmameter("usp_htp_delete_otherPayments", "@id", e.CommandArgument))
                    {
                        FillGrid();
                    }


                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    clsLogger.ErrorLog(ex);
                }
            }
        }

        // Page index changing event handler of gv_records.
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        protected void btnPaymentsSubmit_Click(object sender, EventArgs e)
        {
            try
            {
              
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }
        // Remove delete button from already deleted record.
        public bool isRemovedEnabled(object isRemove)
        {
            
            if (Convert.ToBoolean(isRemove) == false)
                return true;
            else
                return false;
        }
    }
}

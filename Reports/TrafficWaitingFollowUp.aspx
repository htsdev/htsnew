﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrafficWaitingFollowUp.aspx.cs"
    Inherits="HTP.Reports.TrafficWaitingFollowUp" %>

<%@ Register Assembly="ReportSettingControl" Namespace="ReportSettingControl" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Abid Ali 5018 12/28/2008 Enabled Disable family fiters controls -->
    <title>Traffic Waiting Follow Up</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <style type="text/css">
        #pnl_popup {
            left: auto !important;
            top: auto !important;
            position: relative !important;
            padding: 0 !important;
        }

        .modalBackground {
            position: relative !important;
            top: auto !important;
            height: auto !important;
        }

        #ReportSetting1__gvReportSetting tbody tr td {
            background: #fff !important;
            color: black !important;
        }
        #ReportSetting1__gvReportSetting tbody tr td  span{
            color: black !important;
        }
         #ReportSetting1__gvReportSetting tbody tr td  a{
            color: #3366cc !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        //Zeeshan 10286 08/15/2012 Traffic Waiting Follow up report Show Setting control
        function CloseReportSettingPopup() {
            $find("ModalBehaviour").hide();
            return true;
        }

        // Abid Ali 5018 12/28/2008 Enabled Disable family fiters controls
        function EnabledFamilyFilterControls(cal_FromDateFilterClientId, cal_ToDateFilterClientId, chkFamilyShowAllClientId, chkFamilyShowPastClinetId, actionNumber) {
            var calFromCtrl = document.getElementById(cal_FromDateFilterClientId);
            var calToCtrl = document.getElementById(cal_ToDateFilterClientId);
            var chkShowAll = document.getElementById(chkFamilyShowAllClientId);
            var chkShowPast = document.getElementById(chkFamilyShowPastClinetId);

            // check following actionNumber value and do the appropriate task
            // 1 --> For calendar control or date control
            // 2 --> For show all check box
            // 3 or Else --> For show past records
            if (actionNumber == 1) {
                chkShowAll.checked = false;
                chkShowAll.disabled = true;

                chkShowPast.checked = false;
                chkShowPast.disabled = true;
            }
            else if (actionNumber == 2) {
                chkShowPast.checked = false;
                chkShowPast.disabled = chkShowAll.checked;
                calFromCtrl.disabled = chkShowAll.checked;
                calToCtrl.disabled = chkShowAll.checked;
            }
            else {
                chkShowAll.checked = false;
                //chkShowPast.checked = false;
                chkShowAll.disabled = chkShowPast.checked;
                calFromCtrl.disabled = chkShowPast.checked;
                calToCtrl.disabled = chkShowPast.checked;
            }
        } // end of EnabledFamilyFilterControls

        function CheckDateValidation() {
            // abid ali 5387 1/15/2009 date comparision
            debugger;
            if (IsDatesEqualOrGrater(document.form1.cal_FromDateFilternew_txtDate.value, 'MM/dd/yyyy', '01/01/1900', 'MM/dd/yyyy') == false) {
                //alert("Please enter valid date, From Date must be grater then or equal to 1/1/1900");
                $("#txtErrorMessage").text("Please enter valid date, From Date must be grater then or equal to 1/1/1900");
                $("#errorAlert").modal();
                // Fahad 4354 1/16/2009 comment out
                //document.form1.cal_FromDateFilter.focus(); 
                return false;
            }

            if (IsDatesEqualOrGrater(cal_ToDateFilternew_txtDate, 'MM/dd/yyyy', '01/01/1900', 'MM/dd/yyyy') == false) {
                /// alert("Please enter valid date, To Date must be grater then or equal to 1/1/1900");
                $("#txtErrorMessage").text("Please enter valid date, To Date must be grater then or equal to 1/1/1900");
                $("#errorAlert").modal();
                // Fahad 4354 1/16/2009 comment out
                //document.form1.cal_ToDateFilter.focus(); 
                return false;
            }
            if (IsDatesEqualOrGrater(cal_ToDateFilternew_txtDate, 'MM/dd/yyyy', document.form1.cal_FromDateFilternew_txtDate.value, 'MM/dd/yyyy') == false) {
                //alert("Please enter valid date, To Date must be grater then or equal to From Date");
                $("#txtErrorMessage").text("Please enter valid date, To Date must be grater then or equal to From Date");
                $("#errorAlert").modal();
                // Fahad 4354 1/16/2009 comment out
                //document.form1.cal_ToDateFilter.focus(); 
                return false;
            }
            else {
                return true;
            }
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <aspnew:ScriptManager ID="ScriptManager1" runat="server" />

            <div class="page-container row-fluid container-fluid">
                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>
                <section id="main-content" class="" id="">
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                 </div>
                 </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Waiting Follow Up</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>
            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>

                                                           <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Waiting Follow Up</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="fa fa-chevron-down" id="iconarrow"></a>
                    
                </div>
            </header>

                 <div class="content-body" id="followupsection">
                <div class="row">




                    

                     <div class="col-md-2">
                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:LinkButton data-toggle="modal" data-target="#myModal" ID="lnkbtn_ShowSetting" runat="server">Show Settings</asp:LinkButton>




                                    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Report Setting Model</h4>
      </div>
      <div class="modal-body" style="padding:5px;">
        <asp:Panel ID="pnl_popup" runat="server" Style="text-align: center; display: block;">
                                                        <table border="0" width="100%;">
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <cc1:ReportSetting ID="ReportSetting1" runat="server" ReportId="222" ReportTitle="Report Attribute"
                                                                        CurrentUserId="3991" ConnectionStringKey="HoustonTraffic" HeaderCssClass="panel_header"
                                                                        LabelCssClass="form-label" GridviewCssClass="table table-small-font table-bordered table-striped"
                                                                        CellBackgroundImageUrl="" TextboxCssClass="form-control"
                                                                        ModalPopupExtenderId = "mpeReportSetting" DropdownCssClass="form-control" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpeReportSetting" runat="server" TargetControlID="lnkbtn_ShowSetting"
                                                        PopupControlID="pnl_popup" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        BehaviorID="ModalBehaviour">
                                                    </ajaxToolkit:ModalPopupExtender>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




                                    


                                                  
                                    </div>
                            </div>
                         </div>

                     

                      <div class="col-md-3">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Follow Up Date Range :</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <span class="form-label">Follow Up Date From :</span>
                                <%--<ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>--%>
                                    <picker:datepicker id="cal_FromDateFilternew"  runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                    
                                    </div>
                                                                </div>
                         </div>

                        <div class="col-md-3">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Follow Up Date Range :</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <span class="form-label">Follow Up Date To :</span>
                               <%--<ew:CalendarPopup Visible="true" ID="cal_ToDateFilter"
                                    runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                    Culture="(Default)" EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                    ShowGoToToday="True" Text=" " ToolTip="Date" UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>--%>
                                    <picker:datepicker id="cal_ToDateFilternew"  runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                    
                                    </div>
                                                                </div>
                         </div>
                    

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <label class="form-label"></label>
                                    <div class="controls">
                                     <asp:CheckBox ID="chkFamilyShowAll" CssClass="checkbox-custom" runat="server" Text="Show All" />
                                    <asp:CheckBox ID="chkFamilyShowPast" CssClass="checkbox-custom" runat="server" Text="Display Past Records" />
                                        </div>
                                    </div>
                                                                </div>
                         </div>
                     

                     <div class="col-md-12">
                           <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:Button ID="btnFamilySubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                                    OnClick="btnFamilySubmit_Click" OnClientClick="return CheckDateValidation();" />
                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                                                               </section>

                                                    </ContentTemplate>
                </aspnew:UpdatePanel>
    <asp:Label ID="lbl_Message" runat="server"></asp:Label>
             <section class="box" id="TableGrid" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Traffic Waiting Follow Up</h2>
                     <div class="actions panel_actions pull-right">
                          <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                                                
                                                                 <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div class="table-responsive" data-pattern="priority-columns">
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-small-font table-bordered table-striped" AllowSorting="True" OnRowDataBound="gv_Records_RowDataBound"
                                            AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="30"
                                            OnRowCommand="gv_Records_RowCommand" OnSorting="gv_Records_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lastname" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="firstname" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_TiketNo" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Cause No</u>" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Causenumber" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <%-- Abid Ali 5018 12/24/2008 Criminal Report Addition --%>
                                                <asp:TemplateField HeaderText="<u>LOC</u>" SortExpression="shortname" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LOC" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Cov</u>" SortExpression="Cov" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Cov" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Cov") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="verifiedcourtstatus">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_verstatus" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.verifiedcourtstatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>CrtDate</u>" SortExpression="sortcourtdate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_crtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Past Due</u>" SortExpression="sortedPastDue">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_pastDue" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.pastdue") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="TrafficWaitingFollowUp">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" Width="90px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TrafficWaitingFollowUp") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                        <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                        <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                        <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("CourtID") %>' />
                                                        <asp:HiddenField ID="hf_TrafficFollowUpDate" runat="server" Value='<%#Eval("TrafficFollowUpDate") %>' />
                                                        <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                        <%--Zeeshan 10286 08/15/2012 Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses--%>
                                                        <asp:HiddenField ID="hf_shortname" runat="server" Value='<%#Eval("shortname") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>

                                            </div>

                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                   
                                    </div>
                                                             
                         </div>
                         </div>
                    </div>
                     </div>
                 </section>

             <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            <%--Sabir Khan 5188 11/21/2008 To exclude HMC and HCJP...--%>
                                            <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Traffic Waiting Follow Up (Non HMC and HCJP)" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <%-- <asp:Panel ID="pnlFollowup" runat="server">
                                    <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Bond Waiting Follow Up Date" />
                                </asp:Panel>--%>
                                <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                                    <ContentTemplate>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
       
            <table>

            
             <tr>
                            <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>
                            </td>
                            
                        </tr>


            </table>


            </section>
                     </section>
            </div>

        </div>
    </form>
    <div id="errorAlert" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Error message</h4>
                </div>
                <div class="modal-body" style="min-height: 93px !important; max-height: 162px;">
                    <p id="txtErrorMessage">Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>

    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <%-- Noufil 4980 10/30/2008 Calender Z-index addded --%>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;

        function pageLoad() {
            $(".panel_actions").click(_BoxToggle);
        }
        function _BoxToggle() {
            $("#followupsection").toggle();
            if ($('#followupsection').css('display') == 'none') {
                $('#iconarrow').removeClass('fa fa-chevron-down');
                $('#iconarrow').addClass('fa fa-chevron-up');
            }
            else {
                $('#iconarrow').removeClass('fa fa-chevron-up');
                $('#iconarrow').addClass('fa fa-chevron-down');
            }

        }
    </script>

</body>
</html>

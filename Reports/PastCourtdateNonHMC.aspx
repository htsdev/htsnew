﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PastCourtdateNonHMC.aspx.cs"
    Inherits="HTP.Reports.PastCourtdateNonHMC" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Past Court Date (Other) Alert</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body class=" ">

    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <%-- <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>--%>
        </aspnew:ScriptManager>
              
        
        <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <!-- START CONTAINER -->
                <div class="page-container row-fluid container-fluid">
                    
                    <asp:Panel ID="pnl" runat="server" UpdateMode="Always">
                    </asp:Panel>
                    
                    <!-- START CONTENT -->
                    <section id="main-content" class=" ">
                        <section class="wrapper main-wrapper row" style=''>
                            
<div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                            <div class='col-xs-12'>
                                <div class="page-title">

                                    <div class="pull-left">
                                        <!-- PAGE HEADING TAG - START -->
                                        <h1 class="title">Past Court Date Alert</h1>
                                        <!-- PAGE HEADING TAG - END -->
                                    </div>
                        
                                </div>
                            </div>
                    
                            <div class="clearfix"></div>
                    
                            <!-- MAIN CONTENT AREA STARTS -->
    
                            <div class="col-xs-12">
                                <section class="box ">
                                    <div class="content-body">

                                        <div class="row">

                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of Business Days after case reset:" runat="server" Attribute_Key="Days" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:CheckBox ID="cb_ShowAll" runat="server" Text="Show All" CssClass="form-label pull-right" OnCheckedChanged="cb_ShowAll_CheckedChanged" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red" Text=""></asp:Label>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </section>

                            </div>

                            <div class="col-lg-12">
                                <section class="box ">
                                    <header class="panel_header">
                                        <div class="actions panel_actions pull-right">
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </div>
                                    </header>
                                    <div class="content-body">
                                        <div class="row">
                                            <div class="col-xs-12">

                                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                                    <ProgressTemplate>
                                                        <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                            CssClass="form-label" Text="Please Wait ......"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <asp:GridView ID="DG_pastcases" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table" AllowPaging="True" PageSize="20" OnPageIndexChanging="DG_pastcases_PageIndexChanging"
                                                    AllowSorting="True" OnSorting="gv_Records_Sorting" CellPadding="0" CellSpacing="0"
                                                    OnRowCommand="DG_pastcases_RowCommand" OnRowDataBound="DG_pastcases_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="<u>S.No</u>" SortExpression="sno">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                                    Text='<%# Eval("sno") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="ticketnumber">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_CaseNumber" runat="server" Text='<%# Eval("ticketnumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="fname">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_FirstName" runat="server" Text='<%# Eval("fname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lname">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_LastName" runat="server" Text='<%# Eval("lname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Crt DateTime</u>" SortExpression="CourtDate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# Eval("courtdatemain","{0:g}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Crt No</u>" SortExpression="courtnumber">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_courtnumber" runat="server" Text='<%# Eval("courtnumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Crt. Loc</u>" SortExpression="shortname">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_CourtNum" runat="server" Text='<%# Eval("shortname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Trial Category</u>" SortExpression="status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("status") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Follow Update</u>" SortExpression="PastCourtDateFollowUpDate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("PastCourtDateFollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add" CommandName="ShowFolloupdate"
                                                                    runat="server" />
                                                                <%--<asp:HiddenField ID="hf_Courtid" runat="server" Value='<%#Eval("CourtID") %>' />--%>
                                                                <%--<asp:HiddenField ID="hf_causenumber" runat="server" Value='<%#Eval("causenumber") %>' />--%>
                                                                <%--<asp:HiddenField ID="hf_ticketid" runat="server" Value='<%#Eval("ticketid_pk") %>' />--%>

                                                                <asp:Label ID="hf_Courtid" runat="server" Text='<%# Eval("CourtID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="hf_causenumber" runat="server" Text='<%# Eval("causenumber") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="hf_ticketid" runat="server" Text='<%# Eval("ticketid_pk") %>' Visible="false"></asp:Label>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                </asp:GridView>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <asp:Panel ID="pnlFollowup" runat="server" CssClass="col-lg-12">
                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Past Court Date HMC Follow Up Date" />
                            </asp:Panel>

                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                                        PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                                </ContentTemplate>
                            </aspnew:UpdatePanel>
                    
                            <!-- MAIN CONTENT AREA ENDS -->
                        </section>
                    </section>
                    <!-- END CONTENT -->

                </div>
                <!-- END CONTAINER -->

            </ContentTemplate>
            <Triggers>
            </Triggers>
        </aspnew:UpdatePanel>

    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->





<%--    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
  <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>



    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>


    <script src="../assets/js/scripts.js" type="text/javascript"></script>--%>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>





















<%--<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="800" align="center"
                    border="0">
                    <tr>
                        <td style="width: 896px; height: 118px">
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr class="clsLeftPaddingTable">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" style="height: 25px">
                                        <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of Business Days after case reset:"
                                            runat="server" Attribute_Key="Days" />
                                    </td>
                                    <td align="right" style="height: 25px">
                                        <asp:CheckBox ID="cb_ShowAll" runat="server" Text="Show All" CssClass="clssubhead"
                                            OnCheckedChanged="cb_ShowAll_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                            <table>
                                <tr>
                                    <td colspan="2" style="text-align: right;" valign="middle">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                        CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="DG_pastcases" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="DG_pastcases_PageIndexChanging"
                                AllowSorting="True" OnSorting="gv_Records_Sorting" CellPadding="0" CellSpacing="0"
                                OnRowCommand="DG_pastcases_RowCommand" OnRowDataBound="DG_pastcases_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="<u>S.No</u>" SortExpression="sno">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                Text='<%# Eval("sno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="ticketnumber">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CaseNumber" runat="server" Text='<%# Eval("ticketnumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="fname">
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" Text='<%# Eval("fname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lname">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" Text='<%# Eval("lname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Crt DateTime</u>" SortExpression="CourtDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# Eval("courtdatemain","{0:g}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Crt No</u>" SortExpression="courtnumber">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_courtnumber" runat="server" Text='<%# Eval("courtnumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Crt. Loc</u>" SortExpression="shortname">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtNum" runat="server" Text='<%# Eval("shortname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Trial Category</u>" SortExpression="status">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="center" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Follow Update</u>" SortExpression="PastCourtDateFollowUpDate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("PastCourtDateFollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add" CommandName="ShowFolloupdate"
                                                runat="server" />
                                            <asp:HiddenField ID="hf_Courtid" runat="server" Value='<%#Eval("CourtID") %>' />
                                            <asp:HiddenField ID="hf_causenumber" runat="server" Value='<%#Eval("causenumber") %>' />
                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" height="11px" style="width: 896px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Past Court Date HMC Follow Up Date" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                            PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                    </ContentTemplate>
                </aspnew:UpdatePanel>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </aspnew:UpdatePanel>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>--%>
</html>

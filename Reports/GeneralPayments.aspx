﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralPayments.aspx.cs"
    Inherits="HTP.Reports.GeneralPayments" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>General Payments</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
        function EnabledPaymentsFilterControls( cal_FromDateFilterClientId, cal_ToDateFilterClientId, chkPaymentsShowAllClientId, chkPaymentsShowPastClinetId, actionNumber)
        {
            var calFromCtrl = document.getElementById( cal_FromDateFilterClientId );
            var calToCtrl = document.getElementById( cal_ToDateFilterClientId );
            var chkShowAll = document.getElementById( chkPaymentsShowAllClientId );
            var chkShowPast = document.getElementById( chkPaymentsShowPastClinetId );            
           
            // check following actionNumber value and do the appropriate task
            // 1 --> For calendar control or date control
            // 2 --> For show all check box
            // 3 or Else --> For show past records
            if( actionNumber == 1 )
            {
                chkShowAll.checked = !chkShowAll.disabled;            
                chkShowPast.checked = !chkShowPast.disabled;
            }
            else if( actionNumber == 2 )
            {                               
                chkShowPast.checked = false;
                chkShowPast.disabled = chkShowAll.checked;
                calFromCtrl.disabled = chkShowAll.checked;
                calToCtrl.disabled = chkShowAll.checked;
            }
            else
            {                
                chkShowAll.checked = false;
                chkShowAll.disabled = chkShowPast.checked;
                calFromCtrl.disabled = chkShowPast.checked;
                calToCtrl.disabled = chkShowPast.checked;
            }            
        } 
        
        function CheckDateValidation()
        {
        var chkShowAll = document.getElementById( '<%=chkPaymentsShowAll.ClientID%>' );
        var chkShowPast = document.getElementById( '<%=chkPaymentsShowAll.ClientID%>' ); 
        if ( !chkShowPast.checked && !chkShowAll.checked)
        {
                var from = document.form1.cal_FromDateFilter.value;
                var To  = document.form1.cal_ToDateFilter.value;
            
            if (IsDatesEqualOrGrater(from,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, From Date must be grater then or equal to 1/1/1900");
			    return false;
			}
			
			if (IsDatesEqualOrGrater(To,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, To Date must be grater then or equal to 1/1/1900");
				
				return false;
			}
            if (IsDatesEqualOrGrater(To,'MM/dd/yyyy',from,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, To Date must be grater then or equal to From Date");
			     
				return false;
			}
			else
			{
			    return true;
			}
		}
		else
		{
		    return true;
		}
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <span class="clssubhead">Date Range :</span>
                            </td>
                            <td align="left" style="width: 130px">
                                <span class="clsLabel">From :</span>
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 120px">
                                <span class="clsLabel">To :&nbsp;</span><ew:CalendarPopup Visible="true" ID="cal_ToDateFilter"
                                    runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                    Culture="(Default)" EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                    ShowGoToToday="True" Text=" " ToolTip="Date" UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 70px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkPaymentsShowAll" runat="server" Text="Show All" />
                                </span>
                            </td>
                            <td align="left" style="width: 120px">
                            <span style="font-size: XX-Small;">
                            <asp:CheckBox ID="chkPaymentShowPast" runat="server" Text="Show Pending Records" />
                             </span>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnPaymentsSubmit" runat="server" Text="Submit" CssClass="clsbutton"
                                    OnClientClick="return CheckDateValidation();" OnClick="btnPaymentsSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                General Payments
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="True" OnRowCommand="gv_Records_RowCommand" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                            AllowPaging="True" PageSize="30">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name"  HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Payment" 
                                                    HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ChargeAmount" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last 5 days of General Comments" 
                                                    HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_GeneralComments" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.generalComments") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgBtn_Delete" Visible='<%# isRemovedEnabled(DataBinder.Eval(Container, "DataItem.isRemoved"))%>' ImageUrl="~/Images/btnClose.gif" runat="server"
                                                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.InvoiceNumber_PK") %>' CommandName="Deleted" OnClientClick="return confirm('Are you sure you want to delete?');" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                               
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>
                            </td>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
    
</body>
</html>

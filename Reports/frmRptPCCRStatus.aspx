<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.frmRptPCCRStatus"
    CodeBehind="frmRptPCCRStatus.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>PCCR Status History</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/dates.js" type="text/javascript"></script>

    <script runat="server">
       
    </script>

    <script language="javascript">				
		function validate()
		{
			//a;
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
			var dtfrom=document.getElementById("dtFrom").value;
			var dtTo=document.getElementById("dtTo").value;
			
			if(dtfrom > dtTo)
			{ 
				alert("Please Specify Valid Future Date!");
				return  false;
			}
		}
		 function GrdviewPCCRSumdisplay()
        {

            document.getElementById("tr_SumHead").style.display = "";
        }
		
    </script>

    <style type="text/css">
        .style1
        {
            width: 125px;
        }
    </style>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
        <tbody>
            <tr>
                <td style="width: 790px">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 790px">
                    <table id="tblSelectionCriteria" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="width: 67px">
                                <asp:Label ID="lblFromDate" runat="server" Height="16px" CssClass="frmtd" Width="65px"> From:</asp:Label>
                            </td>
                            <td class="style1">
                                <ew:CalendarPopup ID="dtFrom" runat="server" Width="80px" EnableHideDropDown="True"
                                    Font-Names="Tahoma" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                    AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                    PadSingleDigits="True" ToolTip="Select start date" ImageUrl="../images/calendar.gif"
                                    Font-Size="8pt">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 80px">
                                <asp:Label ID="lblToDate" runat="server" Height="16px" CssClass="frmtd" Width="60px">To:</asp:Label>
                            </td>
                            <td style="width: 131px">
                                <ew:CalendarPopup ID="dtTo" runat="server" Width="80px" EnableHideDropDown="True"
                                    Font-Names="Tahoma" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                    AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                    PadSingleDigits="True" ToolTip="Select end date" ImageUrl="../images/calendar.gif"
                                    Font-Size="8pt">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkHiredHistory" runat="server" CssClass="frmtd" Text="Show only Hired History">
                                </asp:CheckBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkPCCRSumary" runat="server" CssClass="frmtd" Text="Display Summary">
                                </asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 67px">
                                <asp:Label ID="lblPCCR" runat="server" CssClass="frmtd" Width="85px">PCCR Status:</asp:Label>
                            </td>
                            <td class="style1">
                                <asp:DropDownList ID="ddlPCCR" runat="server" CssClass="label" Width="172px">
                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 80px">
                                <asp:Label ID="Label1" runat="server" CssClass="frmtd" Width="64px">User:</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlUser" runat="server" CssClass="label" Width="112px">
                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                &nbsp;<asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="../Images/grdsearch.gif"
                                    AlternateText="Search"></asp:ImageButton>&nbsp;&nbsp;<asp:ImageButton ID="btn_Reset"
                                        runat="server" ImageUrl="../Images/grdreset.gif"></asp:ImageButton>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <tr>
                        <td style="width: 790px" align="center">
                            <asp:Label ID="Label2" runat="server" ForeColor="White">1</asp:Label><asp:Label ID="lblMessage"
                                runat="server" Font-Size="X-Small" ForeColor="Red">Label</asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_SumHead" runat="server">
                        <td class="producthead">
                            <font color="#ffffff">PCCR Status Summary</font>
                        </td>
                    </tr>
                    <tr id="tr_SumGrd" runat="server">
                        <td>
                            <asp:GridView ID="GrdPCCRStaSumm" Visible="False" runat="server" Width="781px" AutoGenerateColumns="False"
                                OnRowDataBound="GrdPCCRStaSumm_RowDataBound" ShowFooter="True">
                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                <Columns>
                                    <asp:BoundField DataField="StatusDescription" HeaderText="Alert Status">
                                        <HeaderStyle HorizontalAlign="Left" Width="70%" CssClass="GrdHeader"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Counts" HeaderText="Count">
                                        <HeaderStyle CssClass="grdheader" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../images/separator_repeat.gif">
                            &nbsp;
                        </td>
                    </tr>
            </tr>
            <tr>
                <td style="width: 790px">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="producthead" width="155" height="20">
                                <font color="#ffffff">&nbsp;&nbsp;PCCR Status History</font>
                            </td>
                            <td class="TDHeading" align="right" width="20%">
                                <table id="tblPageNavigation" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td align="right" width="28%">
                                            <asp:Label ID="lblCurrPage" runat="server" Width="97px" Font-Names="Verdana" Font-Size="8.5pt"
                                                ForeColor="White" Visible="False" Font-Bold="True">Current Page :</asp:Label>
                                        </td>
                                        <td align="left" width="27%">
                                            <asp:Label ID="lblPNo" runat="server" Width="48px" Font-Names="Verdana" Font-Size="8.5pt"
                                                ForeColor="White" Visible="False" Font-Bold="True"></asp:Label>
                                        </td>
                                        <td align="right" width="28%">
                                            <asp:Label ID="lblGoto" runat="server" Width="16px" Font-Names="Verdana" Font-Size="8.5pt"
                                                ForeColor="White" Visible="False" Font-Bold="True">Goto</asp:Label>
                                        </td>
                                        <td align="right" width="17%">
                                            <asp:DropDownList ID="cmbPageNo" runat="server" CssClass="frmtd" Font-Size="Smaller"
                                                Visible="False" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 790px">
                    <asp:DataGrid ID="dgPCCR" runat="server" Width="781px" AutoGenerateColumns="False"
                        AllowPaging="True" PageSize="15">
                        <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="S.No">
                                <HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSerial" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Case No.">
                                <HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnCaseNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.casenumber") %>'
                                        CommandName="cmdCaseNo">
                                    </asp:LinkButton>
                                    <asp:Label ID="lblCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.casenumber") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Client" HeaderText="Client">
                                <HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="username" HeaderText="User">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="insertdate" HeaderText="Date">
                                <HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="pccrstatus" HeaderText="Status">
                                <HeaderStyle HorizontalAlign="Left" Width="15%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="comments" HeaderText="Comments">
                                <HeaderStyle HorizontalAlign="Left" Width="38%" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle NextPageText="Next &gt;" PrevPageText="&lt; Previous" HorizontalAlign="Center"
                            PageButtonCount="5"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>

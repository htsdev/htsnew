﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;


//Waqas 5864 07/02/2009 ALR Letters
namespace HTP.Reports
{
    public partial class ALRLetters : System.Web.UI.Page
    {

        #region Variables
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger cLogger = new clsLogger();        
        
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    int ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
                    ViewState["vTicketId"] = ticketno;
                    Session["ticketid"] = ticketno;

                    if (cSession.GetCookie("sEmpID", this.Request) != "")
                    {
                        int empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                        ViewState["vEmpId"] = empid;


                        int lettertype = Convert.ToInt32(Request.QueryString["lettertype"]);
                        ViewState["vLetterType"] = lettertype;

                        if (lettertype == 26)
                        {
                            ViewState["Title"] = "ALR Check Request";
                        }

                        if (lettertype == 27)
                        {

                            ViewState["Title"] = "ALR Continuance";
                        }

                        if (lettertype == 28)
                        {
                            ViewState["Title"] = "ALR Hearing Request";
                        }

                        if (lettertype == 29)
                        {
                            ViewState["Title"] = "ALR Mandatory Continuance";
                        }

                        if (lettertype == 30)
                        {
                            ViewState["Title"] = "ALR Motion to Request";
                        }

                        if (lettertype == 31)
                        {
                            ViewState["Title"] = "ALR Subpoena";
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'> alert('Session has been expired. please login Again');   </script>");
                        HttpContext.Current.Response.Write("<script language='javascript'> window.close();   </script>");
                    }
                }
            }
            catch (Exception ex)
            {
                cLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// This method is used to generate and print ALR letters
        /// </summary>
        public void CreateALRWordReport()
        {
            try
            {


                clsCrsytalComponent Cr = new clsCrsytalComponent();

                //string[] key = { "@ticketid", "@empid" };
                //object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ViewState["vEmpId"]) };

                string[] key = { "@ticketid" };
                object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]) };

                int lettertype = Convert.ToInt32(ViewState["vLetterType"]);
                int empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));

                string note = "";
                string filename = "";
                string docfilename = "";
                if (lettertype == 26) //ALR Check Request
                {
                    filename = Server.MapPath("") + "\\ALR_Check_Request.rpt";
                    docfilename = "ALR_Check_Request";

                    string[] keys = { "@ticketid", "@EmpID" };
                    object[] values = { Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ViewState["vEmpId"]) };

                    Cr.CreateReportWordWithName(docfilename, filename, "USP_HTP_GET_ALR_Check_Request_Letter", keys, values, "false", lettertype, empid, this.Session, this.Response);

                    note = "ALR Request Check Printed with changes";
                    cLogger.AddNote(Convert.ToInt32(ViewState["vEmpId"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }

                if (lettertype == 27) //ALR Continuance
                {
                    filename = Server.MapPath("") + "\\ALR_Continuance.rpt";
                    docfilename = "ALR_Continuance";
                    Cr.CreateReportWordWithName(docfilename, filename, "USP_HTP_Get_ALR_Mandatory_Continuance_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);
                    
                    note = "ALR Continuance Printed with changes";
                    cLogger.AddNote(Convert.ToInt32(ViewState["vEmpId"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }

                if (lettertype == 28) //ALR Hearing Request
                {
                    filename = Server.MapPath("") + "\\ALR_Hearing_Request.rpt";
                    docfilename = "ALR_Hearing_Request";
                    Cr.CreateReportWordWithName(docfilename, filename, "USP_HTP_Get_ALR_Hearing_Request_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);

                    note = "ALR Hearing Request Printed with changes";
                    cLogger.AddNote(Convert.ToInt32(ViewState["vEmpId"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }

                if (lettertype == 29) //ALR Mandatory Continuance
                {
                    filename = Server.MapPath("") + "\\ALR_Mandatory_Continuance.rpt";
                    docfilename = "ALR_Mandatory_Continuance";
                    Cr.CreateReportWordWithName(docfilename, filename, "USP_HTP_Get_ALR_Mandatory_Continuance_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);

                    note = "ALR Mandatory Continuance Printed with changes";
                    cLogger.AddNote(Convert.ToInt32(ViewState["vEmpId"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }


                if (lettertype == 30) //ALR Motion To Request
                {
                    filename = Server.MapPath("") + "\\ALR_Motion_To_Request.rpt";
                    docfilename = "ALR_Motion_To_Request";
                    Cr.CreateReportWordWithName(docfilename, filename, "USP_HTP_Get_ALR_Motion_To_Request_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);

                    note = "ALR Motion To Request Printed with changes";
                    cLogger.AddNote(Convert.ToInt32(ViewState["vEmpId"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }

                if (lettertype == 31) //ALR Subpoena
                {
                    filename = Server.MapPath("") + "\\ALR_Subpeona.rpt";
                    docfilename = "ALR_Subpoena";
                    Cr.CreateReportWordWithName(docfilename, filename, "USP_HTP_Get_ALR_Subpeona_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);

                    note = "ALR Subpoena Printed with changes";
                    cLogger.AddNote(Convert.ToInt32(ViewState["vEmpId"]), note, note, Convert.ToInt32(ViewState["vTicketId"]));
                }


            }
            catch (Exception ex)
            {
                cLogger.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }
        #endregion

        protected void IBtn_Click1(object sender, ImageClickEventArgs e)
        {
            CreateALRWordReport();
        }

        
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
    public partial class PaymentDiscrepancyEmail : System.Web.UI.Page
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        DateTime dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        dt = Convert.ToDateTime(Request.QueryString["tDate"]);
                       // dt = Convert.ToDateTime("05/01/2002");
                        BindGrid(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void BindGrid(DateTime dt)
        {
            try
            {
                string key = "@Tdate";
                object value = dt;
                DataSet DS = clsDb.Get_DS_BySPByOneParmameter("usp_hts_get_PaymentDiscrepancyService", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gv.DataSource = DS;
                    gv.DataBind();
                    SerialNumber();
                }
                else
                {

                    lblMessage.Text = "No Record Found";
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region SerialNumber
        private void SerialNumber()
        {

            try
            {
                long sNo = (gv.PageIndex) * (gv.PageSize);
                foreach (GridViewRow row in gv.Rows)
                {
                    sNo += 1;
                    ((Label)(row.FindControl("lblSNO"))).Text = sNo.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string contact1 = ((Label)e.Row.FindControl("lblContact1")).Text.ToString();
                    string contact2 = ((Label)e.Row.FindControl("lblcontact2")).Text.ToString();
                    string contact3 = ((Label)e.Row.FindControl("lblcontact3")).Text.ToString();

                    if (contact1.StartsWith("("))
                    {
                        ((Label)e.Row.FindControl("lblContact1")).Visible = false;
                    }
                    if (contact2.StartsWith("("))
                    {
                        ((Label)e.Row.FindControl("lblcontact2")).Visible = false;
                    }
                    if (contact3.StartsWith("("))
                    {
                        ((Label)e.Row.FindControl("lblcontact3")).Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Data.SqlClient;





//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.IDScreen
{
    /// <summary>
    /// Summary description for CauseNumberReport.
    /// </summary>
    /// 
    /// 

    public partial class CauseNumberReport : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.DataGrid DG_CauseReport;
        protected eWorld.UI.CalendarPopup cal_todate;
        protected eWorld.UI.CalendarPopup cal_fromDate;

        clsENationWebComponents ClsDb = new clsENationWebComponents();

        clsSession ClsSession = new clsSession();


        protected System.Web.UI.WebControls.Button btnSearch;

        DataView DV;

        string StrExp = String.Empty;
        protected System.Web.UI.WebControls.Label lblCurrPage;
        protected System.Web.UI.WebControls.Label lblPNo;
        protected System.Web.UI.WebControls.Label lblGoto;
        protected System.Web.UI.WebControls.DropDownList cmbPageNo;
        protected System.Web.UI.WebControls.DropDownList ddl_CourtLocation;
        protected System.Web.UI.WebControls.Label lbl_Msg;
        string StrAcsDec = String.Empty;



        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Put user code to initialize the page here
                if (!IsPostBack)
                {
                    //Waqas 5057 03/17/2009 Checking employee info in session
                    if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                    {
                        Response.Redirect("../frmlogin.aspx", false);
                    }

                    lbl_Msg.Text = "";
                    lbl_Msg.Visible = false;
                    cal_fromDate.SelectedDate = DateTime.Today;
                    cal_todate.SelectedDate = DateTime.Today;





                    //check for Datagrid Items if not exist then disable controls

                    if (DG_CauseReport.Items.Count < 1)
                    {
                        lblCurrPage.Visible = false;
                        lblGoto.Visible = false;
                        lblPNo.Visible = false;
                        cmbPageNo.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
            this.DG_CauseReport.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DG_CauseReport_PageIndexChanged);
            this.DG_CauseReport.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_CauseReport_CancelCommand);
            this.DG_CauseReport.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_CauseReport_EditCommand);
            this.DG_CauseReport.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.DG_CauseReport_SortCommand);
            this.DG_CauseReport.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DG_CauseReport_UpdateCommand);
            this.DG_CauseReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DG_CauseReport_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion



        //Method for filling pagenumber in dropdown.
        private void FillPageList()
        {
            Int16 idx;

            cmbPageNo.Items.Clear();
            for (idx = 1; idx <= DG_CauseReport.PageCount; idx++)
            {
                cmbPageNo.Items.Add("Page - " + idx.ToString());
                cmbPageNo.Items[idx - 1].Value = idx.ToString();
            }
        }
        private void SetNavigation()
        {
            try
            {
                // setting current page number
                lblPNo.Text = Convert.ToString(DG_CauseReport.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;
                //FillPageList();

                cmbPageNo.SelectedIndex = DG_CauseReport.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void bindGrid()
        {
            try
            {
                //Search Prcocedure, searching with toDate and FromDate.
                if (cal_todate.SelectedDate.ToShortDateString() != "1/1/0001" && cal_fromDate.SelectedDate.ToShortDateString() != "1/1/0001")
                {

                    string[] key = { "@startDate", "@EndDate", "@courtType" };
                    object[] value1 = { cal_todate.SelectedDate.ToString(), cal_fromDate.SelectedDate.ToString(), ddl_CourtLocation.SelectedValue.ToString() };
                    DataSet DsCauseReport = ClsDb.Get_DS_BySPArr("Usp_HTS_GET_ID_CAUSENUMBER", key, value1);
                    if (DsCauseReport.Tables[0].Rows.Count > 0)
                    {

                        DG_CauseReport.Visible = true;
                        DG_CauseReport.DataSource = DsCauseReport;
                        DV = new DataView(DsCauseReport.Tables[0]);//data view paging
                        Session["sDV"] = DV;
                        DG_CauseReport.DataBind();
                        FillPageList();//fill dropdown with page numbers
                        SetNavigation();//navigation through page numbers
                    }
                    else
                    {

                        lbl_Msg.Visible = true;
                        lbl_Msg.Text = "No Record Found";
                        DG_CauseReport.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }


        private void DG_CauseReport_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {

                DG_CauseReport.EditItemIndex = e.Item.ItemIndex;
                bindGrid();
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void DG_CauseReport_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                DG_CauseReport.EditItemIndex = -1;
                bindGrid();
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void DG_CauseReport_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {//Calling Update Procedure to Update Cause number in database.

                int Vticketid = Convert.ToInt32(((HyperLink)e.Item.FindControl("HlkTktKey")).Text);
                string CauseNumber = ((TextBox)e.Item.FindControl("txtcauseno")).Text.ToString();
                int ticketId = Convert.ToInt32(((HyperLink)e.Item.FindControl("HLkTicketID")).Text);
                int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                int courtid = Convert.ToInt32(((Label)e.Item.FindControl("lbl_courtid")).Text);

                string[] key = { "@courtid", "@causenumber" };
                object[] value1 = { courtid, CauseNumber };

                DataSet DS = ClsDb.Get_DS_BySPArr("USP_HTS_CauseNumberReport_Get_CauseNo_validity", key, value1);

                if (DS.Tables[0].Rows[0][0].ToString() != "0")
                {
                    lbl_Msg.Visible = true;
                    lbl_Msg.Text = "Cause Number Already Exists";

                }
                else
                {
                    string[] key1 = { "@ViolationticketId", "@CauseNumber", "@empId", "@ticketId" };
                    object[] value2 = { Vticketid, CauseNumber, ticketId, empid };
                    ClsDb.ExecuteSP("USP_HTS_UPDATE_ID_CAUSENUMBER", key1, value2);
                    DG_CauseReport.EditItemIndex = -1;
                    bindGrid();
                }








            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }		//Method for sorting in grid.
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                DG_CauseReport.DataSource = DV;
                DG_CauseReport.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }
        //Method for bind serial number and tooltip.
        private void BindReport()
        {
            long sNo = (DG_CauseReport.CurrentPageIndex) * (DG_CauseReport.PageSize);
            try
            {
                foreach (DataGridItem ItemX in DG_CauseReport.Items)
                {
                    sNo += 1;

                    ((HyperLink)(ItemX.FindControl("hp_sno"))).Text = sNo.ToString();
                    ((Label)(ItemX.FindControl("Label3"))).ToolTip = "Full Name : " + ((HyperLink)(ItemX.FindControl("hlkFulnm"))).Text + "\nPhone No : " + ((HyperLink)(ItemX.FindControl("hlkPhone"))).Text;
                }
            }

            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void DG_CauseReport_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            try
            {
                DG_CauseReport.CurrentPageIndex = e.NewPageIndex;
                DG_CauseReport.EditItemIndex = -1;
                SetNavigation();
                DV = (DataView)Session["sDV"];
                DG_CauseReport.DataSource = DV;
                DG_CauseReport.DataBind();


                //cmbPageNo.SelectedIndex=DG_CauseReport.CurrentPageIndex;

            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void DG_CauseReport_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            try
            {
                DG_CauseReport.CurrentPageIndex = 0;
                DG_CauseReport.EditItemIndex = -1;

                lbl_Msg.Text = "";
                lbl_Msg.Visible = false;
                bindGrid();
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void DG_CauseReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            BindReport();
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (drv == null)
                return;

            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ((HyperLink)e.Item.FindControl("hp_sno")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber=" + (int)drv["TicketID_PK"];
                }


            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

        private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                lbl_Msg.Text = "";
                lbl_Msg.Visible = false;
                DG_CauseReport.CurrentPageIndex = cmbPageNo.SelectedIndex;
                DV = (DataView)Session["sDV"];
                DG_CauseReport.DataSource = DV;
                DG_CauseReport.DataBind();
                SetNavigation();
                DG_CauseReport.EditItemIndex = -1;
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }

    }
}

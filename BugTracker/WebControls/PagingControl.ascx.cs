using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Errorlog.WebControls
{
    #region Enum

    public enum GridType
    {
        /// <summary>
        /// DataGrid
        /// </summary>
        DataGrid,
        /// <summary>
        /// GridView
        /// </summary>
        GridView
    }

    /// <summary>
    /// All
    /// 10
    /// 20
    /// 30
    /// 40
    /// 50 
    /// 100 
    /// </summary>
    public enum RecordsPerPage
    {
        All,
        Ten = 10,
        Twenty = 20,
        Thirty = 30,
        Fourty = 40,
        Fifty = 50,
        Hundred = 100
    }

    #endregion
    public delegate void PageSizeChangedMethodHandler(int pageSize);
    public partial class PagingControl : UserControl
    {
        #region Variables
        GridView _gvRecords = null;
        DataGrid _dgRecords = null;
        public static GridType GrdType = GridType.GridView;

        #endregion

        #region Properties

        /// <summary>
        ///     Get or set the page count    
        /// </summary>
        public int PageCount
        {
            set
            {
                ViewState["PageCount"] = value;
                IsPagingVisible = (value > 1);
            }
            get
            {
                if (ViewState["PageCount"] == null)
                    ViewState["PageCount"] = 0;
                return Convert.ToInt32(ViewState["PageCount"]);
            }
        }

        /// <summary>
        ///     Set grid view paging
        /// </summary>
        public GridView GridView
        {
            set
            {
                _gvRecords = value;
                if (_gvRecords == null)
                    IsRecordsSizeVisible = false;

                else if (_gvRecords.AllowPaging && Convert.ToInt32(DL_Records.SelectedValue) > 0)
                {
                    if (_gvRecords != value)
                    {
                        _gvRecords.PageSize = 20;
                        _gvRecords.PageIndex = 0;
                    }
                    else
                        _gvRecords.PageSize = Convert.ToInt32(DL_Records.SelectedValue);
                }
            }
        }

        /// <summary>
        ///     Set Data grid paging
        /// </summary>
        public DataGrid DataGrid
        {
            set
            {
                _dgRecords = value;

                if (_dgRecords == null)
                {
                    IsRecordsSizeVisible = false;
                }
                else if (_dgRecords.AllowPaging && Convert.ToInt32(DL_Records.SelectedValue) > 0)
                {
                    if (_dgRecords != value)
                    {
                        _dgRecords.PageSize = 20;
                        _dgRecords.CurrentPageIndex = 0;
                    }
                    else
                    {
                        _dgRecords.PageSize = Convert.ToInt32(DL_Records.SelectedValue);
                    }
                }
            }
        }

        /// <summary>
        ///     Get or set Grid page index
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (ViewState["PageIndex"] == null)
                    ViewState["PageIndex"] = 0;
                return Convert.ToInt32(ViewState["PageIndex"]);
            }
            set
            {
                ViewState["PageIndex"] = value;
            }
        }

        
        /// <summary>
        ///     Get or Set Paging Visible Property
        /// </summary>
        protected bool IsPagingVisible
        {
            get
            {
                if (ViewState["IsPagingVisible"] == null)
                    ViewState["IsPagingVisible"] = true;
                return Convert.ToBoolean(ViewState["IsPagingVisible"]);
            }
            set
            {
                ViewState["IsPagingVisible"] = value;
            }
        }

        
        /// <summary>
        ///     Get or Set number of records per page Visible Property
        /// </summary>
        protected bool IsRecordsSizeVisible
        {
            get
            {
                if (ViewState["IsRecordsSizeVisible"] == null)
                    ViewState["IsRecordsSizeVisible"] = false;

                return Convert.ToBoolean(ViewState["IsRecordsSizeVisible"]);
            }
            set
            {
                ViewState["IsRecordsSizeVisible"] = value;
            }
        }

        #endregion

        #region Events

        /// <summary>
        ///     Page method handler event handler
        /// </summary>
        public event LoggingReport.PageMethodHandler PageIndexChanged;

        /// <summary>
        ///     Page size changed event handler
        /// </summary>
        public event PageSizeChangedMethodHandler PageSizeChanged;

        /// <summary>
        ///     This event fires when ever control get load or reload.
        /// </summary>
        /// <param name="sender">Control which is sending the request</param>
        /// <param name="e">Event Argument</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (GrdType == GridType.DataGrid)
            {
                IsPagingVisible = (_dgRecords != null && _dgRecords.PageCount > 1);
                IsRecordsSizeVisible = (_dgRecords != null && PageSizeChanged != null && _dgRecords.PageCount > 0);
            }
            else
            {
                IsPagingVisible = (_gvRecords != null && _gvRecords.PageCount > 1);
                IsRecordsSizeVisible = (_gvRecords != null && PageSizeChanged != null && _gvRecords.PageCount > 0);
            }

            if (!IsPostBack)
            {
                if (PageCount > 0)
                {
                    FillDdlist();
                    LB_curr.Text = (PageIndex + 1).ToString();
                }
                GrdType = GridType.GridView;
            }
        }

        /// <summary>
        ///     Update page index size
        /// </summary>
        /// <param name="sender">Control which is sending the request</param>
        /// <param name="e">Event Argument</param>
        protected void DL_pages_SelectedIndexChanged(object sender, EventArgs e)
        {
            int x = Convert.ToInt32(this.DL_pages.SelectedValue);
            PageIndex = x;
            LB_curr.Text = x.ToString();

            if (PageIndexChanged != null)
                PageIndexChanged();
        }

        /// <summary>
        ///     Sets the Size of Records Per Page
        /// </summary>
        public RecordsPerPage Size
        {
            set
            {
                DL_Records.ClearSelection();
                DL_Records.SelectedValue = Convert.ToInt32(value).ToString();
            }
        }

        /// <summary>
        ///     Page size change selected index event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DL_Records_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GrdType == GridType.DataGrid)
            {
                if (_dgRecords != null)
                {
                    if (PageSizeChanged != null)
                        PageSizeChanged(Convert.ToInt32(DL_Records.SelectedValue));
                }
            }
            else
            {
                if (_gvRecords != null)
                {
                    if (PageSizeChanged != null)
                        PageSizeChanged(Convert.ToInt32(DL_Records.SelectedValue));
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Fill page index dropdown
        /// </summary>
        private void FillDdlist()
        {
            DL_pages.Items.Clear();

            for (int i = 1; i <= PageCount; i++)
                DL_pages.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));

            if (DL_Records.SelectedValue == "0")
                PageIndex = 0;
            DL_pages.SelectedValue = (PageIndex + 1).ToString();
        }

        /// <summary>
        ///     Set page index and gird page size
        /// </summary>
        public void SetPageIndex()
        {
            IsPagingVisible = (PageCount > 1);
            if (GrdType == GridType.DataGrid)
            {
                IsRecordsSizeVisible = (_dgRecords != null && PageSizeChanged != null && PageCount > 0);
                if (IsRecordsSizeVisible)
                {
                    DL_Records.ClearSelection();
                    if (_dgRecords.AllowPaging)
                        DL_Records.Items.FindByText(_dgRecords.PageSize.ToString()).Selected = true;
                    else
                        DL_Records.Items[0].Selected = true;
                }
            }
            else
            {
                IsRecordsSizeVisible = (_gvRecords != null && PageSizeChanged != null && PageCount > 0);
                if (IsRecordsSizeVisible)
                {
                    DL_Records.ClearSelection();
                    if (_gvRecords.AllowPaging)
                        DL_Records.Items.FindByText(_gvRecords.PageSize.ToString()).Selected = true;
                    else
                        DL_Records.Items[0].Selected = true;
                }
            }
            FillDdlist();
            LB_curr.Text = (PageIndex + 1).ToString();
            DL_pages.SelectedValue = (PageIndex + 1).ToString();
        }


        #endregion
    }
}
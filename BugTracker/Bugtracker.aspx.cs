using System;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

//Waqas 7067 12/17/2009 namespace changed
namespace HTP.Errorlog
{
    /// <summary>
    /// Summary description for Bugtracker.
    /// </summary>
    public partial class Bugtracker : System.Web.UI.Page
    {
        //Ozair 6766  04/29/2010 obsolete method removed along with variable namming
        readonly string _conn1 = ConfigurationManager.AppSettings["connString1"];
        readonly string _conn2 = ConfigurationManager.AppSettings["connString2"];
        readonly string _conn3 = ConfigurationManager.AppSettings["connString3"];
        readonly string _conn4 = ConfigurationManager.AppSettings["connString4"];
        readonly string _conn5 = ConfigurationManager.AppSettings["connString5"];
        static string _conn = ConfigurationManager.AppSettings["connString1"];

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";

                if (!IsPostBack)
                {
                    dtp_dtFrom.Reset(DateTime.Today);
                    dtp_DtTo.Reset(DateTime.Today);
                    btn_Submit.Attributes.Add("onclick", "return validate();");
                    FillGrid(_conn);
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        private void FillGrid(string conn)
        {
            try
            {
                //added by khalid 
                var type = "";
                switch (ddl_Options.SelectedValue)
                {
                    case "LoaderSservice":
                        type = "2";
                        break;
                    case "E-Signature":
                        type = "1";
                        break;
                    case "Outlook Addin":
                        type = "3"; // Added By Agha Usman Task id 3506 on 04/04/2008
                        break;
                    case "POLM": //Ozair 6766 04/29/2010 Added POLM Option
                        type = "4";
                        break;
                    case "Product Defects": //Afaq 8739 01/31/2011 Added Product Defects Option
                        type = "5";
                        break;

                    case "Product Defects Staging": // Kashif Jawed 9286 09/06/2011 Add Product Defects Staging option
                        type = "6";
                        break;

                    default:
                        type = "0";
                        break;
                }
                //end
                if (type == "0")
                {
                    object[] val = { dtp_dtFrom.SelectedDate.ToString(), dtp_DtTo.SelectedDate.ToString() };
                    dg_bug.DataSource = SqlHelper.ExecuteDataset(conn, "usp_hts_get_errorlog_with_date_range", val);
                    dg_bug.DataBind();
                }
                else
                {
                    object[] values = { dtp_dtFrom.SelectedDate.ToString(), dtp_DtTo.SelectedDate.ToString(), type };
                    dg_bug.DataSource = SqlHelper.ExecuteDataset(conn, "usp_hts_get_errorlog_Esig_Loader_range", values);
                    dg_bug.DataBind();
                }
                FillPageList();
                SetNavigation();
                //cmbPageNo.SelectedIndex=0;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }


        #region Navigation logic
        //----------------------------------Navigational LOGIC-----------------------------------------//

        // Procedure for filling page numbers in page number combo........
        private void FillPageList()
        {
            Int16 idx;

            cmbPageNo.Items.Clear();
            for (idx = 1; idx <= dg_bug.PageCount; idx++)
            {
                cmbPageNo.Items.Add("Page - " + idx.ToString());
                cmbPageNo.Items[idx - 1].Value = idx.ToString();
            }
            cmbPageNo.SelectedIndex = 0;
        }

        // Procedure for setting data grid's current  page number on header ........
        private void SetNavigation()
        {
            try
            {
                // setting current page number
                lblPNo.Text = Convert.ToString(dg_bug.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;
                //FillPageList();

                cmbPageNo.SelectedIndex = dg_bug.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        //Event fired When page changed from drop down list
        protected void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                dg_bug.CurrentPageIndex = cmbPageNo.SelectedIndex;
                FillGrid(_conn);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg_bug.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_bug_PageIndexChanged);

        }
        #endregion

        private void dg_bug_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dg_bug.CurrentPageIndex = e.NewPageIndex;
            FillGrid(_conn);
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            dg_bug.CurrentPageIndex = 0;
            FillGrid(_conn);

        }


        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dtp_dtFrom.SelectedDate <= dtp_DtTo.SelectedDate)
            {
                dg_bug.CurrentPageIndex = 0;
                //Ozair 6766 04/29/2010 code refactored
                switch (ddl_Options.SelectedValue)
                {
                    case "Houston Traffic System":
                        _conn = _conn1;
                        FillGrid(_conn);
                        break;
                    case "Dallas Traffic System":
                        _conn = _conn2;
                        FillGrid(_conn);
                        break;
                    case "Public Site":
                        _conn = _conn3;
                        FillGrid(_conn);
                        break;
                    case "QuickBooks":
                        _conn = _conn4;
                        FillGrid(_conn);
                        break;
                    case "SulloLaw":
                        _conn = _conn5;
                        FillGrid(_conn);
                        break;
                    case "POLM"://Ozair 6766 04/29/2010 Added POLM Option
                    case "Product Defects"://Afaq 8739 01/31/2010 Added Product Defects Option
                    case "Product Defects Staging": // Kashif Jawed 9286 09/06/2011 Add Product Defects Staging option
                    case "Outlook Addin":
                    case "E-Signature":
                    case "LoaderSservice":
                        _conn = _conn1; //houston traffictickets database
                        FillGrid(_conn);
                        break;
                }
            }
            else
            {
                dg_bug.Visible = false;
                lblMessage.Text = "Invalid Date Range";
                FillPageList();
                SetNavigation();
            }

        }

        protected void dg_bug_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //Ozair 6766 04/29/2010 code refactored
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) return;
            var controlName = e.Item.FindControl("hf_statetrace").NamingContainer.ClientID;
            ((Label)e.Item.FindControl("lbl_statetrace")).Attributes.Add("OnMouseOver", "StateTracePoupup('" + controlName + "_hf_statetrace" + "');");
            ((Label)e.Item.FindControl("lbl_statetrace")).Attributes.Add("OnMouseOut", "CursorIcon2()");
        }
    }
}

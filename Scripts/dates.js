function Len(Expression)
{
	return Expression.toString().length;
}

function isleapyear(year)
{
	return ((year % 4) == 0) && (((year % 100) != 0) || ((year % 400) == 0));
}

function mostdaysinmonth(month, year)
{
	if ((month == 4) || (month == 6) || (month == 9) || (month == 11))
		return 30;
	else if (month == 2)
		if (isleapyear(year))
			return 29;
		else
			return 28;
	else
		return 31;
}

function isvaliddate(year, month, day)
{
	
	//Modified by kazim task id:2536
	//Change the existing validation because it not validate days greater than 12
	
	//Kazim 3648 4/8/2008 Correct the dob validation   
	//return (month >= 1) && (month <= 12) && (day >= 1) && (day <= mostdaysinmonth(month, year));
	return (day >= 1) && (day <= 12) && (month >= 1) && (month <= mostdaysinmonth(day, year));
}


function goMonths(date, months)
{
	var newDay = date.getDate();
	var newMonth = date.getMonth();
	var newYear = date.getFullYear();   //Saeed 8101 09/28/2010 replace getYear() depricated method with getFullYear() to run script in both IE & FF.

	for (var i = 0; i < months; i++)
	{
		newMonth++;

		if (newMonth == 0)
		{
			newYear++;
		}
	}

	while ((newDay >= 29) && !isvaliddate(newYear, newMonth + 1, newDay))
	{
		newDay--;
	}

	return new Date(newYear, newMonth, newDay);	
}
    
function goDays(date, Days, Type)
{
	var newDay = date.getDate();
	var newMonth = date.getMonth();
	var newYear = date.getFullYear();   //Saeed 8101 09/28/2010 replace getYear() depricated method with getFullYear() to run script in both IE & FF.

	for (var i = 0; i < Days; i++)
	{
		newDay++;

		if (newDay == 0)
		{
			newMonth++;
		}

		if (newMonth == 0)
		{
			newYear++;
		}

	}

	if (Type == 'Events')
	{
		return new Date(newYear, newMonth, newDay-1);
	}
	else
	{
		return new Date(newYear, newMonth, newDay);
	}
}

function validate_date(inputwidget)
{
	var datestring = inputwidget.value;
	var first = datestring.indexOf('/');
	var last = datestring.lastIndexOf('/');

	var daypart = datestring.substring(0, first);
	var monthpart = datestring.substring(first + 1, last);
	var yearpart = datestring.substring(last + 1, datestring.length);

	if ((datestring.length > 0) && 
	     ((yearpart.length < 4) || 
	      !isvaliddate(parseInt(yearpart, 10), parseInt(monthpart, 10), parseInt(daypart, 10))))
	{
		inputwidget.value = "";
		alert("The date entered is not valid, dates must be in the format dd/mm/yyyy.");
		inputwidget.focus();
		return false;
	}
	else
	{
		return true;
	}
}





















var MONTH_NAMES = new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');










function isDate(val,format) {
	var date = getDateFromFormat(val,format);
	if (date == 0) { return false; }
	return true;
	}






function IsDatesEqualOrGrater(date1,dateformat1,date2,dateformat2) {
	var d1 = getDateFromFormat(date1,dateformat1);
	var d2 = getDateFromFormat(date2,dateformat2);
	if (d1==0 || d2==0) {
		return -1;
		}
	else if (d1 >= d2) {
		return 1;
		}
	return 0;
	}



function compareDates(date1,dateformat1,date2,dateformat2) {
	var d1 = getDateFromFormat(date1,dateformat1);
	var d2 = getDateFromFormat(date2,dateformat2);
	if (d1==0 || d2==0) {
		return -1;
		}
	else if (d1 > d2) {
		return 1;
		}
	return 0;
	}







function formatDate(date,format) {
	format = format+"";
	var result = "";
	var i_format = 0;
	var c = "";
	var token = "";
	var date=new Date(date); 
	var y = date.getFullYear()+"";  //Saeed 8101 09/28/2010 replace getYear() depricated method with getFullYear() to run script in both IE & FF.
	var M = date.getMonth()+1;
	var d = date.getDate();
	var H = date.getHours();
	var m = date.getMinutes();
	var s = date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	
	
	if (y.length < 4) {
		y = y-0+1900;
		}
	y = ""+y;
	yyyy = y;
	yy = y.substring(2,4);
	
	if (M < 10) { MM = "0"+M; }
		else { MM = M; }
	MMM = MONTH_NAMES[M-1];
	
	if (d < 10) { dd = "0"+d; }
		else { dd = d; }
	
	h=H+1;
	K=H;
	k=H+1;
	if (h > 12) { h-=12; }
	if (h == 0) { h=12; }
	if (h < 10) { hh = "0"+h; }
		else { hh = h; }
	if (H < 10) { HH = "0"+K; }
		else { HH = H; }
	if (K > 11) { K-=12; }
	if (K < 10) { KK = "0"+K; }
		else { KK = K; }
	if (k < 10) { kk = "0"+k; }
		else { kk = k; }
	
	if (H > 11) { ampm="PM"; }
	else { ampm="AM"; }
	
	if (m < 10) { mm = "0"+m; }
		else { mm = m; }
	
	if (s < 10) { ss = "0"+s; }
		else { ss = s; }
	
	var value = new Object();
	value["yyyy"] = yyyy;
	value["yy"] = yy;
	value["y"] = y;
	value["MMM"] = MMM;
	value["MM"] = MM;
	value["M"] = M;
	value["dd"] = dd;
	value["d"] = d;
	value["hh"] = hh;
	value["h"] = h;
	value["HH"] = HH;
	value["H"] = H;
	value["KK"] = KK;
	value["K"] = K;
	value["kk"] = kk;
	value["k"] = k;
	value["mm"] = mm;
	value["m"] = m;
	value["ss"] = ss;
	value["s"] = s;
	value["a"] = ampm;
	while (i_format < format.length) {
		
		c = format.charAt(i_format);
		token = "";
		while ((format.charAt(i_format) == c) && (i_format < format.length)) {
			token += format.charAt(i_format);
			i_format++;
			}
		if (value[token] != null) {
			result = result + value[token];
			}
		else {
			result = result + token;
			}
		}
	return result;
	}
	
	



function _isInteger(val) {
	var digits = "1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i)) == -1) { return false; }
		}
	return true;
	}
function _getInt(str,i,minlength,maxlength) {
	for (x=maxlength; x>=minlength; x--) {
		var token = str.substring(i,i+x);
		if (token.length < minlength) {
			return null;
			}
		if (_isInteger(token)) { 
			return token;
			}
		}
	return null;
	}



	

































function getDateFromFormat(val,format) {
	val = val+"";
	format = format+"";
	var i_val = 0;
	var i_format = 0;
	var c = "";
	var token = "";
	var token2= "";
	var x,y;
	var now   = new Date();
	var year  = now.getFullYear();  //Saeed 8101 09/28/2010 replace getYear() depricated method with getFullYear() to run script in both IE & FF.
	var month = now.getMonth()+1;
	var date  = now.getDate();
	var hh    = now.getHours();
	var mm    = now.getMinutes();
	var ss    = now.getSeconds();
	var ampm  = "";
	
	while (i_format < format.length) {
		
		c = format.charAt(i_format);
		token = "";
		while ((format.charAt(i_format) == c) && (i_format < format.length)) {
			token += format.charAt(i_format);
			i_format++;
			}
		
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year = _getInt(val,i_val,x,y);
			if (year == null) { return 0; }
			i_val += year.length;
			if (year.length == 2) {
				if (year > 70) {
					year = 1900+(year-0);
					}
				else {
					year = 2000+(year-0);
					}
				}
			}
		else if (token=="MMM"){
			month = 0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name = MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase() == month_name.toLowerCase()) {
					month = i+1;
					if (month>12) { month -= 12; }
					i_val += month_name.length;
					break;
					}
				}
			if (month == 0) { return 0; }
			if ((month < 1) || (month>12)) { return 0; }
			
			}
		else if (token=="MM" || token=="M") {
			x=token.length; y=2;
			month = _getInt(val,i_val,x,y);
			if (month == null) { return 0; }
			if ((month < 1) || (month > 12)) { return 0; }
			i_val += month.length;
			}
		else if (token=="dd" || token=="d") {
			x=token.length; y=2;
			date = _getInt(val,i_val,x,y);
			if (date == null) { return 0; }
			if ((date < 1) || (date>31)) { return 0; }
			i_val += date.length;
			}
		else if (token=="hh" || token=="h") {
			x=token.length; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 1) || (hh > 12)) { return 0; }
			i_val += hh.length;
			hh--;
			}
		else if (token=="HH" || token=="H") {
			x=token.length; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 0) || (hh > 23)) { return 0; }
			i_val += hh.length;
			}
		else if (token=="KK" || token=="K") {
			x=token.length; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 0) || (hh > 11)) { return 0; }
			i_val += hh.length;
			}
		else if (token=="kk" || token=="k") {
			x=token.length; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 1) || (hh > 24)) { return 0; }
			i_val += hh.length;
			h--;
			}
		else if (token=="mm" || token=="m") {
			x=token.length; y=2;
			mm = _getInt(val,i_val,x,y);
			if (mm == null) { return 0; }
			if ((mm < 0) || (mm > 59)) { return 0; }
			i_val += mm.length;
			}
		else if (token=="ss" || token=="s") {
			x=token.length; y=2;
			ss = _getInt(val,i_val,x,y);
			if (ss == null) { return 0; }
			if ((ss < 0) || (ss > 59)) { return 0; }
			i_val += ss.length;
			}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase() == "am") {
				ampm = "AM";
				}
			else if (val.substring(i_val,i_val+2).toLowerCase() == "pm") {
				ampm = "PM";
				}
			else {
				return 0;
				}
			}
		else {
			if (val.substring(i_val,i_val+token.length) != token) {
				return 0;
				}
			else {
				i_val += token.length;
				}
			}
		}
	
	if (i_val != val.length) {
		return 0;
		}
	
	if (month == 2) {
		
		if ( ( (year%4 == 0)&&(year%100 != 0) ) || (year%400 == 0) ) { 
			if (date > 29){ return false; }
			}
		else {
			if (date > 28) { return false; }
			}
		}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return false; }
		}
	
	if (hh<12 && ampm=="PM") {
		hh+=12;
		}
	else if (hh>11 && ampm=="AM") {
		hh-=12;
		}
	var newdate = new Date(year,month-1,date,hh,mm,ss);
	return newdate.getTime();
	}

function daysbetween(from, until)
{
	if (from && until)
	{
		
		var datestring = from;
		var first  = datestring.indexOf('/');
		var last   = datestring.lastIndexOf('/');

		var daypart   = datestring.substring(0, first);
		var monthpart = datestring.substring(first + 1, last);
		var yearpart  = datestring.substring(last + 1, datestring.length);

		var fromparam =  new Date(yearpart, monthpart - 1, daypart, 12, 0, 0, 0);

		
		datestring = until;
		first  = datestring.indexOf('/');
		last   = datestring.lastIndexOf('/');

		daypart   = datestring.substring(0, first);
		monthpart = datestring.substring(first + 1, last);
		yearpart  = datestring.substring(last + 1, datestring.length);

		var untilparam =  new Date(yearpart, monthpart - 1, daypart, 12, 0, 0, 0);
        	
		var diff = untilparam - fromparam; 
		var dsli = diff/1000/3600/24; 
		dsli = Math.round(dsli);

		
		return dsli;
	}
}


﻿/*---------------------------------------------------------------------//
//-----------------------------AUTO SUGGEST----------------------------//
//---------------------------------------------------------------------//
*/
var key;
function AutoComplete(aStr, oText, oDiv, nMaxSize, nControlWidth, nListSize) {
    // initialize member variables
    this.oText = oText; // the text box
    this.oDiv = oDiv; // a hidden <div> for the popup auto-complete
    this.nMaxSize = nMaxSize;
    this.nControlWidth = nControlWidth;
    this.nListSize = nListSize;


    // preprocess the texts for fast access
    this.db = new AutoCompleteDB();
    var i, n = aStr.length;
    for (i = 0; i < n; i++) {
        this.db.add(aStr[i]);
    }

    // attach handlers to the text-box 
    oText.AutoComplete = this;
    oText.onkeyup = AutoComplete.prototype.onKeyUp;
    oText.onkeydown = keygetter;
    oText.onblur = AutoComplete.prototype.onTextBlur;
}

function keygetter(event) {
    if (!event && window.event) event = window.event;
    if (event) key = event.keyCode;
    else key = event.which;
}

AutoComplete.prototype.onKeyUp = function() {

    this.AutoComplete.onchange();

    if (this.AutoComplete.oDiv.style.visibility == "visible") {
        // When DOWN 
        if (key == 40) {
            var divv = this.AutoComplete.oDiv;
            if (divv.childNodes.length != 0) {
                var index = divv.title;
                if (divv.childNodes.length <= index) {
                    divv.title = ""; index = divv.title;
                }

                if (index == "") {
                    divv.firstChild.className = "AutoCompleteHighlight";
                    divv.title = "0";
                }
                else {
                    if (divv.childNodes.length > index) {
                        if (parseInt(index) < divv.childNodes.length - 1) {
                            divv.childNodes[index].className = "AutoCompleteBackground";
                            index++;
                            divv.childNodes[index].className = "AutoCompleteHighlight";
                            divv.title = index;

                        }
                        else if (parseInt(index) == divv.childNodes.length - 1) {
                            divv.childNodes[index].className = "AutoCompleteHighlight";
                            divv.title = index;
                        }

                    }
                }
            }


        }
        // When UP
        else if (key == 38) {
            var divv = this.AutoComplete.oDiv;
            if (divv.childNodes.length != 0) {
                var index = divv.title;
                if (divv.childNodes.length <= index) {
                    divv.title = ""; index = divv.title;
                }
                if (index == "") {
                    divv.lastChild.className = "AutoCompleteHighlight";
                    divv.title = (divv.childNodes.length - 1).toString();
                }
                else {

                    if (divv.childNodes.length > index) {
                        if (parseInt(index) > 0) {
                            divv.childNodes[index].className = "AutoCompleteBackground";
                            index--;
                            divv.childNodes[index].className = "AutoCompleteHighlight";
                            divv.title = index;
                        }
                        else if (parseInt(index) == 0) {
                            divv.childNodes[index].className = "AutoCompleteHighlight";
                            divv.title = index;
                        }
                        //document.title = divv.title+"-"+divv.childNodes.length.toString()+"DOWN";
                    }
                }
            }
        }
        // When ENTER
        else if (key == 13) {
            var divv = this.AutoComplete.oDiv;
            if (divv.title != "") {
                this.AutoComplete.oText.value = Encoder.htmlDecode(divv.childNodes[divv.title].innerHTML);
                divv.style.visibility = "hidden";
            }
            return false;
        }
        //When ESC
        else if (key == 27) {
            this.AutoComplete.oDiv.style.visibility = "hidden";
            this.AutoComplete.oText.value = "";
            this.AutoComplete.oDiv.title = "";
            return false;
        }
    }
    else {
        this.AutoComplete.onchange();
        this.AutoComplete.oDiv.title = "";
    }

}

AutoComplete.prototype.onTextBlur = function() {
    this.AutoComplete.onblur();
}

AutoComplete.prototype.onblur = function() {
    this.oDiv.style.visibility = "hidden";
    this.oDiv.title = "";
}


AutoComplete.prototype.onTextChange = function() {
    this.AutoComplete.onchange();
}

AutoComplete.prototype.onchange = function() {
    var size = this.nControlWidth;
    var txt = this.oText.value;
    var ListSize = this.nListSize;

    // count the number of strings that match the text-box value.
    var nCount = this.db.getCount(txt);

    // if a suitable number then show the popup-div
    if ((this.nMaxSize == -1) || ((nCount < this.nMaxSize) && (nCount > 0))) {
        //Backup of old Div Length
        var countLen = this.oDiv.childNodes.length;


        // clear the popup div.              
        while (this.oDiv.hasChildNodes())
            this.oDiv.removeChild(this.oDiv.firstChild);

        // get all the matching strings from the AutoCompleteDB
        var aStr = new Array();
        this.db.getStrings(txt, "", aStr);

        // add each string to the popup-div
        var i, n = aStr.length;
        if (n > parseInt(ListSize)) {
            n = parseInt(ListSize);
        }
        for (i = 0; i < n; i++) {
            var oDiv = document.createElement('div');
            this.oDiv.appendChild(oDiv);
            oDiv.innerHTML = aStr[i];
            var str = aStr[i];
            if (size < str.toString().length * 7) {
                size = str.toString().length * 7;
            }
            oDiv.onmousedown = AutoComplete.prototype.onDivMouseDown;
            oDiv.onmouseover = AutoComplete.prototype.onDivMouseOver;
            oDiv.onmouseout = AutoComplete.prototype.onDivMouseOut;
            oDiv.AutoComplete = this;
        }
        if (countLen != this.oDiv.childNodes.length) {
            this.oDiv.title = "";
        }
        this.oDiv.style.visibility = "visible";
    }
    else // hide the popup-div
    {
        this.oDiv.innerHTML = "";
        this.oDiv.style.visibility = "hidden";
        this.oDiv.title = "";
    }
    this.oDiv.style.width = size.toString();


}

function AutoCompleteDB() {
    // set initial values.
    this.bEnd = false;
    this.nCount = 0;
    this.aStr = new Object;
}


AutoCompleteDB.prototype.getCount = function(str, bExact) {
    // if end of search string, return number
    if (str == "")
        if (this.bEnd && bExact && (this.nCount == 1)) return 0;
    else return this.nCount;

    // otherwise, pull the first letter off the string
    var letter = str.substring(0, 1);
    var rest = str.substring(1, str.length);

    // and look for case-insensitive matches
    var nCount = 0;
    var lLetter = letter.toLowerCase();
    if (this.aStr[lLetter])
        nCount += this.aStr[lLetter].getCount(rest, bExact && (letter == lLetter));

    var uLetter = letter.toUpperCase();
    if (this.aStr[uLetter])
        nCount += this.aStr[uLetter].getCount(rest, bExact && (letter == uLetter));

    return nCount;
}

AutoCompleteDB.prototype.add = function(str) {
    // increment the count value.
    this.nCount++;

    // if at the end of the string, flag this node as an end point.
    if (str == "")
        this.bEnd = true;
    else {
        // otherwise, pull the first letter off the string
        var letter = str.substring(0, 1);
        var rest = str.substring(1, str.length);

        // and either create a child node for it or reuse an old one.
        if (!this.aStr[letter]) this.aStr[letter] = new AutoCompleteDB();
        this.aStr[letter].add(rest);
    }
}

AutoComplete.prototype.onDivMouseDown = function() {
    // set the text-box value to the word
this.AutoComplete.oText.value = Encoder.htmlDecode(this.innerHTML);
}

AutoComplete.prototype.onDivMouseOver = function() {
    // assumes the existence of a CSS style called AutoCompleteHighlight
    this.className = "AutoCompleteHighlight";
    this.AutoComplete.oText.value = Encoder.htmlDecode(this.innerHTML);
}

AutoComplete.prototype.onDivMouseOut = function() {
    // assumes the existence of a CSS style called AutoCompleteBackground
    this.className = "AutoCompleteBackground";
}

AutoCompleteDB.prototype.getStrings = function(str1, str2, outStr) {
    if (str1 == "") {
        // add matching strings to the array
        if (this.bEnd)
            outStr.push(str2);

        // get strings for each child node
        for (var i in this.aStr)
            this.aStr[i].getStrings(str1, str2 + i, outStr);
    }
    else {
        // pull the first letter off the string
        var letter = str1.substring(0, 1);
        var rest = str1.substring(1, str1.length);

        // and get the case-insensitive matches.
        var lLetter = letter.toLowerCase();
        if (this.aStr[lLetter])
            this.aStr[lLetter].getStrings(rest, str2 + lLetter, outStr);

        var uLetter = letter.toUpperCase();
        if (this.aStr[uLetter])
            this.aStr[uLetter].getStrings(rest, str2 + uLetter, outStr);
    }
}


<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectEmails.aspx.cs" Inherits="lntechNew.QuickEntryNew.SelectEmails" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>Select Email Address</title>
<link href="../Styles.css" rel="stylesheet" type="text/css" />
<base target="_self" /> 
<script type="text/javascript">







</script>


</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="300">
            <tr>
                <td style="height: 32px;" background="../images/subhead_bg.gif">
                    &nbsp;<asp:Label ID="lbl_head" runat="server" CssClass="clssubhead" Text="Select Attorneys Email Addresses"></asp:Label></td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:GridView ID="gv_emails" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="5%" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_email" runat="server" Checked="True" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Address">
                                <HeaderStyle CssClass="clssubhead" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_email" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.EmailAddress") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td align="right" style="width: 50%; height: 22px;">
                                <asp:Button ID="btn_send" runat="server" CssClass="clsbutton" Text="Send" Width="75px" OnClientClick="returnvalues(1);return false;" /></td>
                            <td style="width: 100px; height: 22px;">
                                <asp:Button ID="btn_cancel" runat="server" CssClass="clsbutton" Text="Cancel" Width="75px" OnClientClick="returnvalues(0);return false;" /></td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hf_totalrec" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.QuickEntryNew.ArraignmentSnapShot" enableViewState="True" Codebehind="ArraignmentSnapShot.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ArraignmentSnapShot</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="950" align="left" border="0">
				<tr><td colspan="5" align="center">
                    <asp:Label ID="lblPrintDate" runat="server" Font-Bold="True" Font-Size="Larger"></asp:Label></td></tr>
				<tr>
					<td style="HEIGHT: 21px"><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MONDAY</FONT></STRONG>
					</td>
					<td style="HEIGHT: 21px"><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TUESDAY</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WEDNESDAY</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THURSDAY</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FRIDAY</FONT></STRONG>
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="left" width="20%"><asp:datagrid id="dg_Mon" runat="server" AutoGenerateColumns="False" CellPadding="0" BorderStyle="Solid"
							BorderColor="Black" Width="80%" EnableViewState="False">
							<Columns>
								<asp:TemplateColumn HeaderText="No">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNoM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketidM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Com">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblTrialM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialDateTime") %>'>
										</asp:Label>
										<asp:Label id=lblBondM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_BondFlag") %>'>
										</asp:Label>
										<asp:Label id=lblTrialCM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialComments") %>'>
										</asp:Label>
										<asp:Label id="lblPlusM" runat="server"></asp:Label>
										<asp:Label id=lblPreM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_pretrialstatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Arr">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblArrM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_ViolationDate") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id=hlnkNameM runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.MON_TicketID_PK"),"&amp;search=0") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top" width="20%"><asp:datagrid id="dg_Tue" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							Width="80%" EnableViewState="False">
							<Columns>
								<asp:TemplateColumn HeaderText="No">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNoT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketidT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Com">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblTrialT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TrialDateTime") %>'>
										</asp:Label>
										<asp:Label id=lblBondT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_BondFlag") %>'>
										</asp:Label>
										<asp:Label id=lblTrialCT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TrialComments") %>'>
										</asp:Label>
										<asp:Label id="lblPlusT" runat="server"></asp:Label>
										<asp:Label id=lblPreT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_pretrialstatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Arr">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblArrT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_ViolationDate") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id=hlnkNameT runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK"),"&amp;search=0") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top" width="20%"><asp:datagrid id="dg_Wed" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							Width="80%" EnableViewState="False">
							<Columns>
								<asp:TemplateColumn HeaderText="No">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNoW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketidW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Com">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblTrialW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialDateTime") %>'>
										</asp:Label>
										<asp:Label id=lblBondW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_BondFlag") %>'>
										</asp:Label>
										<asp:Label id=lblTrialCW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialComments") %>'>
										</asp:Label>
										<asp:Label id="lblPlusW" runat="server"></asp:Label>
										<asp:Label id=lblPreW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_pretrialstatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Arr">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblArrW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_ViolationDate") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id=hlnkNameW runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.WED_TicketID_PK"),"&amp;search=0") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top" width="20%"><asp:datagrid id="dg_Thu" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							Width="80%" EnableViewState="False">
							<Columns>
								<asp:TemplateColumn HeaderText="No">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNoTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketidTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Com">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblTrialTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialDateTime") %>'>
										</asp:Label>
										<asp:Label id=lblBondTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_BondFlag") %>'>
										</asp:Label>
										<asp:Label id=lblTrialCTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialComments") %>'>
										</asp:Label>
										<asp:Label id="lblPlusTh" runat="server"></asp:Label>
										<asp:Label id=lblPreTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_pretrialstatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Arr">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblArrTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_ViolationDate") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id=hlnkNameTh runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.THR_TicketID_PK"),"&amp;search=0") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top" width="20%"><asp:datagrid id="dg_Fri" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black"
							Width="80%" EnableViewState="False">
							<Columns>
								<asp:TemplateColumn HeaderText="No">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNoF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketidF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Com">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblTrialF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TrialDateTime") %>'>
										</asp:Label>
										<asp:Label id=lblBondF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_BondFlag") %>'>
										</asp:Label>
										<asp:Label id=lblTrialCF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TrialComments") %>'>
										</asp:Label>
										<asp:Label id="lblPlusF" runat="server"></asp:Label>
										<asp:Label id=lblPreF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_pretrialstatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Arr">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblArrF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_ViolationDate") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id=hlnkNameF runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_Name") %>' Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK"),"&amp;search=0") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					</td>
					<td><STRONG><FONT size="2">Comments:</FONT></STRONG>
					</td>
				</tr>
				<tr>
					<td vAlign="top"><asp:datagrid id="dg_MonC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							ShowHeader="False" GridLines="None">
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNo1M runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketid1M runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblCommM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialComments") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top"><asp:datagrid id="dg_TueC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							ShowHeader="False" GridLines="None">
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNo1T runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketid1T runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblCommT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tue_TrialComments") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top"><asp:datagrid id="dg_WedC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							ShowHeader="False" GridLines="None">
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNo1W runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketid1W runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblCommW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialComments") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top"><asp:datagrid id="dg_ThuC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							ShowHeader="False" GridLines="None">
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNo1Th runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketid1Th runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblCommTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialComments") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
					<td vAlign="top"><asp:datagrid id="dg_FriC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
							ShowHeader="False" GridLines="None">
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblNo1F runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
										</asp:Label>
										<asp:Label id=lblTicketid1F runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblCommF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fri_TrialComments") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

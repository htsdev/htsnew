﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SullowlawRemindercall.aspx.cs"
    Inherits="HTP.QuickEntryNew.SullowlawRemindercall" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Online Failed Transaction</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="javascript" type="text/javascript">      
       
        // Noufil 4462 09/01/2008 function to show or hide grid
        function checkradio()
        {
                    
            if (document.getElementById("rb_report_0").checked==true)
            {        
            document.getElementById("tr_image").style.display = 'none';
            document.getElementById("tr_image2").style.display = 'block';        
            }
            if (document.getElementById("rb_report_1").checked==true)
            {        
            document.getElementById("tr_image2").style.display = 'none';
            document.getElementById("tr_image").style.display = 'block';        
            }
            
        } 
        
        function checkcomments(type)
        {
            var datelenght;
            if (type == 1)
            {
                var comm=document.getElementById("txt_onlinecomment").value;
                var comm_lenght = document.getElementById("txt_onlinecomment").value.length;
                var oldcomm_lenght = document.getElementById("hf_onlineoldcomments").value.length;
            }
            else if( type ==0)
            {
                var comm=document.getElementById("txt_comment").value;
                var comm_lenght = document.getElementById("txt_comment").value.length;
                var oldcomm_lenght = document.getElementById("hf_oldcomments").value.length;
            }
            else if( type == 2)
            {
                var comm=document.getElementById("tbContactRepComments").value;
                var comm_lenght = document.getElementById("tbContactRepComments").value.length;
                var oldcomm_lenght = document.getElementById("hfContactOldComments").value.length;
            }
            
            if (comm_lenght == 0 )
		    {
		        alert("Please Enter Comments")
			    return false;	
		    }
            
            if (CheckName(comm)==false)
            {
                alert("Please enter valid comments.");
                return false;
            }
            if(comm_lenght > 0)
                dateLenght =  27
            else
                dateLenght = 0
            
            if ((oldcomm_lenght + comm_lenght) > 5000 - dateLenght)//-27 bcoz to show last time partconcatenated with comments
		    {
		        alert("Sorry You cannot type in more than 5000 characters in contact comments")
			    return false;		  
		    }
		    
		    
		    
        }
        // Noufil 4947 11/10/2008 Check comments
        function CheckName(name)
        {       
            for ( i = 0 ; i < name.length ; i++)
            {
                var asciicode =  name.charCodeAt(i)
                //If not valid alphabet 
                if ((asciicode == 60) || (asciicode ==62))
                return false;
           }            
        }
        
        //Zeeshan 4709 09/26/2008 Add Javascript Function To Display Date Range For Contact Us Report.
        function showHideDateRange(chkPending)
        {
         var dateRange = document.getElementById("tblContactUs");
         
         if ( document.getElementById("rb_report_1").checked )
         {
             dateRange.style.display = "block";
             document.getElementById("cal_EffectiveFrom").disabled= true;
             
             if( chkPending == 1)
                document.getElementById("dd_callback").value = "0";
         }
         else
         {
            dateRange.style.display = "none";
            document.getElementById("cal_EffectiveFrom").disabled= false;
         }
         
         
        
        }
       
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<%--onload="showHideDateRange(0);"--%>
<body>
    <form id="Form2" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="820" align="center"
            border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="height: 9px" width="780" background="../images/separator_repeat.gif" height="9">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%-- <tr>
                <td style="height: 9px" colspan="4">
                    <table style="width: 100%" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable">
                        <tr>
                            <td style="width: 130px">
                                &nbsp; Visit Date:&nbsp;&nbsp;
                            </td>
                            <td class="style8">
                                <ew:CalendarPopup ID="cal_EffectiveFrom" runat="server" EnableHideDropDown="True"
                                    ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                    AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
                                    PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Text=" " Width="80px">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" rowspan="1" valign="middle" width="90px">
                                <asp:CheckBox ID="cb_showall" runat="server" Text="Show All" CssClass="clssubhead"
                                    Checked="True" />
                            </td>
                            <td style="width: 172px">
                                Call Back Status:&nbsp;
                            </td>
                            <td>
                                <asp:DropDownList ID="dd_callback" runat="server" CssClass="clsInputadministration"
                                    Width="160px">
                                </asp:DropDownList>
                            </td>
                            <td align="center" valign="middle" rowspan="1" style="width: 412px">
                                <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_update1_Click1">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr>
                <td style="height: 9px">
                    <table style="width: 100%" cellpadding="0" cellspacing="0" class="clsLeftPaddingTable">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="left" class="clssubhead" valign="middle" style="width: 70px;">
                                Start Date:
                            </td>
                            <td class="clssubhead" style="width: 110px">
                                <ew:CalendarPopup ID="calStartDate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                    CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                    ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                    ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                    Text=" " Width="80px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clssubhead" style="width: 65px;">
                                End Date:
                            </td>
                            <td class="clssubhead" style="width: 110px">
                                <ew:CalendarPopup ID="calenddate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                    CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                    ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                    ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                    Text=" " Width="80px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clssubhead" style="width: 80px">
                                <asp:CheckBox ID="cb_showall" runat="server" Text="All Dates" CssClass="clssubhead"
                                    Checked="True" />
                            </td>
                            <td class="clssubhead" style="width: 107px">
                                Call Back Status:
                            </td>
                            <td style="width: 160px">
                                <asp:DropDownList ID="dd_callback" runat="server" CssClass="clsInputadministration"
                                    Width="155px">
                                </asp:DropDownList>
                            </td>
                            <td align="right" valign="middle">
                                <asp:Button ID="Button2" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_update1_Click1">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 11px" width="100%" background="../images/separator_repeat.gif"
                    colspan="4">
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="upnlSullolawVisitors" runat="server">
                        <ContentTemplate>
                            <table id="TableGrid" cellspacing="0" cellpadding="0" bgcolor="white" border="0"
                                width="100%">
                                <tr id="tr_image" runat="server">
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                        <table width="100%">
                                            <tr>
                                                <td class="clssubhead">
                                                    &nbsp;Online Follow up
                                                </td>
                                                <td align="right">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 11px" width="780" background="../images/separator_repeat.gif"
                                        colspan="4" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdData" runat="server" valign="top" align="center">
                                        <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red" EnableViewState="False"></asp:Label>
                                        <asp:GridView ID="dg_ReminderCalls" runat="server" CssClass="clsLeftPaddingTable"
                                            AutoGenerateColumns="False" BorderStyle="Solid" Width="820px" OnRowCommand="dg_ReminderCalls_RowCommand"
                                            OnRowDataBound="dg_ReminderCalls_RowDataBound" ShowHeader="False" OnPageIndexChanging="dg_ReminderCalls_PageIndexChanging"
                                            AllowPaging="true" PageSize="20">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <table class="clsLeftPaddingTable" id="Table1" cellspacing="1" cellpadding="0" width="820px"
                                                            align="center" border="0">
                                                            <tr>
                                                                <td valign="middle" align="left" bgcolor="#eff4fb" rowspan="5" style="width: 60px">
                                                                    <asp:HyperLink ID="hl_SerialNumber" runat="server" Text='<%# Eval("Sno") %>' CssClass="clssubhead"></asp:HyperLink>
                                                                </td>
                                                                <td valign="middle" align="left" bgcolor="#eff4fb" height="20" style="width: 317px">
                                                                    <asp:Label ID="Label11" runat="server" CssClass="clssubhead">Name:</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 544px">
                                                                    <asp:LinkButton ID="lnkName" runat="server" PostBackUrl='<%# "../DOCSTORAGE/ScanDoc/images/C-" + DataBinder.Eval(Container, "DataItem.customerid")+".pdf" %>'
                                                                        CommandArgument='<%# "../DOCSTORAGE/ScanDoc/images/C-" + DataBinder.Eval(Container, "DataItem.customerid")+".pdf" %>'
                                                                        Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.lastname"),", ",(DataBinder.Eval(Container, "DataItem.firstname") )) %>'>
                                                                    </asp:LinkButton>
                                                                    <asp:Label ID="lbl_Customerid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.customerid") %>'
                                                                        CssClass="Label" ForeColor="#123160" Visible="False">
                                                                    </asp:Label>
                                                                </td>
                                                                <td valign="top" bgcolor="#eff4fb" height="20" rowspan="4" align="left" style="width: 117px">
                                                                    <p>
                                                                        <asp:Label ID="lbl_contact1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Contact2" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Contact3" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'
                                                                            CssClass="Label">
                                                                        </asp:Label><br>
                                                                        <asp:LinkButton ID="lnk_Comments" runat="server" Text="Add Comments" CommandName="Add_comments"
                                                                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.comments") %>'></asp:LinkButton>
                                                                    </p>
                                                                </td>
                                                                <td valign="top" width="205" bgcolor="#eff4fb" height="20" rowspan="4" align="left">
                                                                    <div style="width: 280px; height: 83px; overflow: auto;">
                                                                        <asp:Label ID="lbl_comments" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.comments") %>'
                                                                            CssClass="clsLeftPaddingTable">
                                                                        </asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#eff4fb" height="20" align="left" style="width: 317px" valign="middle">
                                                                    <asp:Label ID="Label12" runat="server" CssClass="clssubhead">Court Date:</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 544px">
                                                                    <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialdesc") %>'
                                                                        CssClass="Label" ForeColor="#123160" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" bgcolor="#eff4fb" align="left" style="width: 317px; height: 20px;">
                                                                    <asp:Label ID="Label13" runat="server" CssClass="clssubhead">Location:</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                    width: 544px">
                                                                    <asp:Label ID="lbl_Location" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtAddress") %>'
                                                                        CssClass="Label" ForeColor="#123160">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" bgcolor="#eff4fb" align="left" style="width: 317px; height: 20px;">
                                                                    <asp:Label ID="Label5" runat="server" CssClass="clssubhead">Email Received  
                                                                    Date:</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                    width: 544px">
                                                                    <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recdate") %>'
                                                                        CssClass="Label" ForeColor="#123160">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" bgcolor="#eff4fb" align="left" style="width: 317px; height: 20px;">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="clssubhead">Call Back Status :</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                    width: 544px">
                                                                    <asp:Label ID="lbl_callback" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.calldescription") %>'
                                                                        CssClass="Label" ForeColor="#123160">
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="hf_callback" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.callback") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 11px" width="820px" background="../../images/separator_repeat.gif"
                                                                    colspan="5">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&#160;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&#160;&#160;&#160;" LastPageText="&#160;&#160;&#160;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btn"
                                                        PopupControlID="panel1" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        CancelControlID="btn_cancel">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <%--<ajaxToolkit:ModalPopupExtender ID="Modal_gvrecords" runat="server" TargetControlID="Button1"
                                                        PopupControlID="pnl_online" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        CancelControlID="btn_onlinecancel">
                                                    </ajaxToolkit:ModalPopupExtender>--%>
                                                    <asp:Button ID="btn" runat="server" Style="display: none" />
                                                    <asp:Button ID="Button1" runat="server" Style="display: none" />
                                                    <%--  <asp:Panel ID="pnl_online" runat="server" Height="100px" Width="430px" Style="display: none;">
                                                        <table id="Table2" bgcolor="white" border="1" style="border-top: black thin solid;
                                                            border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                                                            height: 200px; width: 388px;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px"
                                                                    width="450" colspan="2">
                                                                    &nbsp;Update Comments
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="width: 100px; height: 28px;">
                                                                    <font color="#3366cc">&nbsp;Name:</font>
                                                                </td>
                                                                <td width="520" colspan="2" style="height: 28px; text-align: left;">
                                                                    &nbsp;<asp:Label ID="lbl_onlinename" runat="server" CssClass="Label"></asp:Label><asp:Label
                                                                        ID="Label3" runat="server" Visible="False" CssClass="Label"></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="width: 100px; height: 28px;">
                                                                    <font color="#3366cc">&nbsp;Email Address:</font>
                                                                </td>
                                                                <td width="520" colspan="2" style="height: 28px; text-align: left">
                                                                    &nbsp;<asp:Label ID="lbl_onlineemail" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="height: 28px;" align="left">
                                                                    &nbsp;<asp:Label ID="Label4" runat="server" Width="104px" ForeColor="#3366CC">Contact Number:</asp:Label>
                                                                </td>
                                                                <td colspan="2" style="text-align: left; height: 28px;">
                                                                    &nbsp;<asp:Label ID="lbl_ph" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="width: 100px; height: 28px;">
                                                                    <font color="#3366cc">&nbsp;Question:</font>
                                                                </td>
                                                                <td colspan="2" style="text-align: left;">
                                                                    <div style="width: 280px; height: 48px; overflow: auto; vertical-align: middle" runat="server"
                                                                        id="divQuestion">
                                                                        &nbsp;<asp:Label ID="lbl_onlinequestion" runat="server" CssClass="Label"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="width: 100px; height: 28px;">
                                                                    <span style="color: #3366cc">&nbsp;Call Back:</span>
                                                                </td>
                                                                <td colspan="2" style="text-align: left; height: 28px;">
                                                                    &nbsp;<asp:DropDownList ID="ddl_onlinecallback" runat="server" CssClass="clsInputCombo"
                                                                        Width="180px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td style="width: 100px">
                                                                    <span class="clssubhead">&nbsp;Comments:</span>
                                                                </td>
                                                                <td align="left">
                                                                    <%-- Yasir Kamal 5401 13/01/2009 Legal Consultation Update comment bug 
                                                    <div style="width: 280px; height: 83px; overflow: auto;" runat="server" id="divCommentOnline">
                                                        <%-- Yasir Kamal 5401 13/01/2009 END 
                                                        <asp:Label ID="lblOldOnlineComments" runat="server" CssClass="Label" Width="275px"></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txt_onlinecomment" runat="server" Height="64px" Text="" TextMode="MultiLine"
                                                        Width="275px" CssClass="clsInputadministration"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#eff4fb">
                                                <td style="width: 100px; height: 28px;">
                                                    &nbsp;
                                                </td>
                                                <td align="left" style="height: 28px">
                                                    &nbsp;<asp:Button ID="btn_onlineupdate" runat="server" CssClass="clsbutton" Text="Update"
                                                        OnClick="btn_onlineupdate_Click" OnClientClick="return checkcomments(1);" Width="60px" />
                                                    <asp:Button ID="btn_onlinecancel" runat="server" CssClass="clsbutton" Text="Cancel"
                                                        Width="60px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:HiddenField ID="hf_onlineoldcomments" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                        </asp:Panel>--%>
                                                    <asp:Panel ID="panel1" runat="server" Height="100px" Width="400px" Style="display: none;">
                                                        <table id="Table1" bgcolor="white" border="1" style="border-top: black thin solid;
                                                            border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                                                            height: 200px;" cellpadding="0" cellspacing="0" width="375">
                                                            <tr>
                                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px"
                                                                    width="300" colspan="2">
                                                                &nbsp;Update Comments
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="height: 28px" align="left">
                                                                    <font color="#3366cc">&nbsp;Name:</font>
                                                                </td>
                                                                <td width="520" colspan="2" style="height: 28px; text-align: left;">
                                                                    &nbsp;<asp:Label ID="lbl_Name" runat="server" CssClass="Label"></asp:Label><asp:Label
                                                                        ID="lbl_rid" runat="server" Visible="False" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="height: 28px" align="left">
                                                                    <font color="#3366cc">&nbsp;Court Date:</font>
                                                                </td>
                                                                <td width="520" colspan="2" style="height: 28px; text-align: left">
                                                                    &nbsp;<asp:Label ID="lbl_Status" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="height: 28px" align="left">
                                                                    <font color="#3366cc">&nbsp;Location:</font>
                                                                </td>
                                                                <td colspan="2" style="text-align: left; height: 28px;">
                                                                    &nbsp;<asp:Label ID="lbl_location" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="height: 28px" align="left">
                                                                    <span style="color: #3366cc">&nbsp;Call Back:</span>
                                                                </td>
                                                                <td colspan="2" style="text-align: left; height: 28px;">
                                                                    &nbsp;<asp:DropDownList ID="ddl_rStatus" runat="server" CssClass="clsInputCombo"
                                                                        Width="180px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" class="clssubhead" style="height: 60px" align="left">
                                                                    &nbsp;<asp:Label ID="lbl_contacttext" runat="server" Width="104px" ForeColor="#3366CC">Contact Number:</asp:Label>
                                                                </td>
                                                                <td colspan="2" style="text-align: left; height: 60px;">
                                                                    &nbsp;<asp:Label ID="lbl_contact1" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'></asp:Label><br />
                                                                    &nbsp;<asp:Label ID="lbl_Contact2" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'></asp:Label><br />
                                                                    &nbsp;<asp:Label ID="lbl_Contact3" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td style="width: 70px" align="left">
                                                                    <span class="clssubhead">&nbsp;Comments:</span>
                                                                </td>
                                                                <td align="left">
                                                                    <div style="width: 100%; height: 60px; overflow: auto;" runat="server" id="divComment">
                                                                        <asp:Label ID="lblOldComments" runat="server" CssClass="Label" Width="100%"></asp:Label>
                                                                    </div>
                                                                    <asp:TextBox ID="txt_comment" runat="server" Height="64px" Text="" TextMode="MultiLine"
                                                                        Width="275px" CssClass="clsInputadministration"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td style="width: 70px; height: 28px;">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" style="height: 28px">
                                                                    &nbsp;<asp:Button ID="btn_updatecomments" runat="server" CssClass="clsbutton" OnClick="btn_updatecomments_Click"
                                                                        Text="Update" OnClientClick="return checkcomments(0);" Width="60px" />
                                                                    <asp:Button ID="btn_cancel" runat="server" CssClass="clsbutton" Text="Cancel" Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:HiddenField ID="hf_oldcomments" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" style="height: 9px;">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

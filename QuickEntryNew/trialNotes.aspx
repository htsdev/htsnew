<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.QuickEntryNew.trialNotes" Codebehind="trialNotes.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Date</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
	   function closewin()
	    {
	      opener.location.reload();	      	     
	      self.close();	   
	    }	    
		</script>
	</HEAD>
	<body onload="" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 108; LEFT: 16px; POSITION: absolute; TOP: 40px" cellSpacing="0"
				cellPadding="0" width="350" align="center" border="0">
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE class="clsLeftPaddingTable" id="tblsub" height="20" cellSpacing="1" cellPadding="0"
							width="350" align="center" border="0">
							<TR>
							</TR>
							<TR>
								<TD class="clsaspcolumnheader" height="22">Ticket Number</TD>
								<TD height="22">
									<asp:label id="lbl_DispTicketNo" runat="server" Width="167px"></asp:label></TD>
							</TR>
							<TR>
								<TD class="clsaspcolumnheader" height="22">Name</TD>
								<TD height="22">
									<asp:label id="Lbl_DispFLName" runat="server" Width="176px"></asp:label></TD>
							</TR>
							<TR>
								<TD class="clsaspcolumnheader" height="22">Trial Comments&nbsp;</TD>
								<TD height="22">
									<P>&nbsp;</P>
									<P>
										<asp:textbox id="Txt_DispTrialComments" runat="server" Width="184px" Height="56px" TextMode="MultiLine"></asp:textbox></P>
								</TD>
							</TR>
							<TR>
								<TD align="right">
									<asp:TextBox id="txtbefore" runat="server" Width="10px" Visible="False"></asp:TextBox></TD>
								<TD align="right">
									<asp:button id="Btn_TrialNotes_Submit" runat="server" Width="80px" Text="Submit" CssClass="clsbutton"></asp:button></TD>
							</TR>
						</TABLE>
						<asp:Label id="lbl_Message" runat="server" Visible="False" ForeColor="Red"></asp:Label>
					</TD>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="22" style="HEIGHT: 22px"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewPrintHistory.aspx.cs" Inherits="lntechNew.QuickEntryNew.PreviewPrintHistory" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Preview Print History</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <iframe src="frmPreviewPrintHistory.aspx?filename=<%=ViewState["filename"]%>" width="100%" height="100%"></iframe>
    </div>
    </form>
</body>
</html>

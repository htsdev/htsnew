<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrialDocketNew.aspx.cs" Inherits="lntechNew.QuickEntryNew.TrialDocketNew" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>New Trial Docket</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
<form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="height: 13px">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                 <tr>
            <td width="100%" background="../Images/separator_repeat.gif" style="height: 9px"  ></td>
            </tr>
                <tr>
                    <td style="height: 18px">
                        &nbsp;<cr:crystalreportviewer id="CRV1" runat="server" autodatabind="True" height="1054px"
                            reportsourceid="CRSource" width="935px"></cr:crystalreportviewer><cr:crystalreportsource
                                id="CRSource" runat="server">
<Report FileName="D:\SourceCode\HTS\Reports\TrialDocket.rpt"></Report>
</cr:crystalreportsource>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using MetaBuilders.WebControls;
// Yasir Kamal 5427 02/06/2009 Pagging added for dataGrid.
using lntechNew.WebControls;


namespace HTP.QuickEntryNew
{
    // Noufil 5061 11/04/2008 Remove Set call and FTA call from this report.

    /// <summary>
    /// Summary description for RemingerCalls.
    /// </summary>
    public partial class ReminderCalls : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clscalls clscall = new clscalls();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        string SessionRecid;
        private DataTable dtReminderStatus;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Sabir 4272 07/22/2008. Add attibutes for bond client and reguler client check box. 
                chkBondClient.Attributes.Add("onclick", "IsCheck(1)");
                chkRegulerClient.Attributes.Add("onclick", "IsCheck(2)");

                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    SessionRecid = Convert.ToString(Session["ReminID"]);

                    if (Page.IsPostBack != true)
                    {
                        cal_EffectiveFrom.SelectedDate = DateTime.Today; //setting the date
                        if (SessionRecid == "")					 //If not redirecting from Notes Page
                            Session["ReminID"] = "1";		 //initialize Session RecID	
                        GetReminderStatus(); //fills reminder status drop down list

                    }
                    //Yasir Kamal 5427 01/23/2009 Pagging functionality Added.
                    PagingControl.grdType = GridType.GridView;
                    Pagingctrl.GridView = dg_ReminderCalls;
                    // Yasir Kamal 5427  end.
                    FillGrid(); //filling the grid
                    SetUrl();	//	putting validations on grid
                }
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            //Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            //Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);

            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddl_rStatus.SelectedIndexChanged += new System.EventHandler(this.ddl_rStatus_SelectedIndexChanged);
            this.btn_update1.Click += new System.EventHandler(this.btn_update1_Click);
            this.lbRefresh.Click += new EventHandler(this.btn_update1_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void FillGrid() // Filling the Grid
        {
            try
            {	//"@employeeid"  ClsSession.GetSessionVariable("sEmpID",this.Session)

                // Noufil 3498 04/05/2008 radiobutton value and show all value are set in database as parameter
                // Noufil 3949 05/06/2008 Language Dropdown added for language filter
                //Sabir Khan 4272 07/22/2008 flag for bond,Reguler and All Clients.
                int Mode = 0;
                if (this.chkBondClient.Checked && (!(this.chkRegulerClient.Checked)))
                    Mode = 1;  //1 for only bond clients.
                else if (this.chkRegulerClient.Checked && (!(this.chkBondClient.Checked)))
                    Mode = 2;  //2 for only reguler clients.
                else if (this.chkBondClient.Checked && this.chkRegulerClient.Checked)
                    Mode = 3; //3 for all clients.                               
                DataSet ds = clscall.remindercallgrid(cal_EffectiveFrom.SelectedDate, Convert.ToInt32(ddl_rStatus.SelectedValue.ToString()), cb_showall.Checked, 0, Convert.ToInt32(dd_language.SelectedValue.ToString()), Mode);

                //Yasir Kamal 5427 01/23/2009 Pagging functionality Added.

                DataColumn dc = new DataColumn("SNo");
                dc.DataType = typeof(Int32);
                ds.Tables[0].Columns.Add(dc);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["Sno"] = i + 1;
                }
                dg_ReminderCalls.DataSource = ds;

                try
                {
                    dg_ReminderCalls.DataBind();
                }
                catch
                {

                    if (dg_ReminderCalls.SelectedIndex > dg_ReminderCalls.PageCount - 1)
                    {
                        dg_ReminderCalls.SelectedIndex = dg_ReminderCalls.PageCount - 1;
                        dg_ReminderCalls.DataBind();

                    }

                }

                Pagingctrl.PageCount = dg_ReminderCalls.PageCount;
                Pagingctrl.PageIndex = dg_ReminderCalls.SelectedIndex;
                Pagingctrl.SetPageIndex();


                //S No
                BindReport();

                if (dg_ReminderCalls.Rows.Count== 0)
                {
                    lblMessage.Text = "No Records";
                    dg_ReminderCalls.Visible = false;
                }
                else
                {
                    dg_ReminderCalls.Visible = true;

                    if (dg_ReminderCalls.PageSize >= ds.Tables[0].Rows.Count)
                    {
                        //dg_ReminderCalls.PagerStyle.Visible = false;

                    }
                    else
                    {
                        //dg_ReminderCalls.PagerStyle.Visible = true;
                    }

                }
                if (ddl_rStatus.SelectedValue == "0")
                {
                    //this.chkHeaderFlag.Visible = true;
                    //this.chkHeaderFlag.Attributes.Add("onclick", "DGSelectOrUnselectAll('dg_ReminderCalls', this, 'chkFlag')");
                    //this.btnActivateFlag.Visible = true;
                }
                else
                {
                    //this.chkHeaderFlag.Visible = false;
                    //this.chkHeaderFlag.Attributes.Remove("onclick");
                    //this.btnActivateFlag.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Method To Generate Serial Number
        private void BindReport()
        {
            long sNo = (dg_ReminderCalls.SelectedIndex) * (dg_ReminderCalls.PageSize);
            try
            {
                foreach (GridView ItemX in dg_ReminderCalls.Rows)
                {
                    sNo += 1;

                    ((TextBox)(ItemX.FindControl("txt_sno"))).Text = sNo.ToString();
                }
                //To assign total num of records
                // Yasir Kamal 5427 01/31/2009 Pagging functionality Added.
                txt_totalrecords.Text = dg_ReminderCalls.Rows.Count.ToString();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //

        private void SetUrl() // setting the grid
        {
            try
            {
                int rowId = 0;
                foreach (GridViewRow  items in dg_ReminderCalls.Rows)
                {
                    // setting hyperlink
                    ((HyperLink)(items.FindControl("lnkName"))).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" + ((Label)(items.FindControl("lbl_TicketID"))).Text;

                    //setting Callbacks
                    Label lblCalls = (Label)(items.FindControl("lbl_CallBacks"));
                    GetAllReminderStatuses();
                    //Nasir 6899 16/11/2009 check if null or empty
                    // Afaq 7145 09/28/2010 replace empty or null check with int.TryParse 
                    int.TryParse(lblCalls.Text, out rowId);
                    DataRow dr = dtReminderStatus.Rows.Find(rowId);
                    if (dr != null)
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = dr["Description"].ToString();

                    //Sabir Khan 5751 04/03/2009 Scrolling has been added to reminder notes control...
                    ((HyperLink)(items.FindControl("lnk_Comments"))).NavigateUrl = "javascript:window.open('remindernotes.aspx?casenumber=" + ((Label)(items.FindControl("lbl_TicketID"))).Text + "&violationID=" + ((Label)(items.FindControl("lbl_TicketViolationID"))).Text + "&searchdate=" + ((Label)(items.FindControl("lbl_CourtDate"))).Text + "&Recid=" + ((TextBox)(items.FindControl("txt_sno"))).Text + "&calltype=" + "0" + "&genComments=" + (HiddenField)(items.FindControl("hf_GeneralComments")) + "','','status=yes,width=530,height=410,scrollbars=yes');void('');";

                    // Comments setting			 
                    if (((Label)(items.FindControl("lbl_Comments"))).Text == "")
                    {
                        ((Label)(items.FindControl("lbl_Comments"))).Text = "No Comments";
                    }

                    // Firm setting
                    if (((Label)(items.FindControl("lbl_Firm"))).Text != "SULL")
                    {
                        ((Label)(items.FindControl("lbl_contact1"))).Text = ((Label)(items.FindControl("lbl_Firm"))).Text;
                        ((Label)(items.FindControl("lbl_contact1"))).Font.Bold = true;
                        ((Label)(items.FindControl("lbl_contact2"))).Visible = false; ;
                        ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                    }
                    // Contact no setting
                    else if
                        (((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("(") &&
                        ((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("(") &&
                        ((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("("))
                    {
                        ((Label)(items.FindControl("lbl_contact1"))).Text = "No contact";
                        ((Label)(items.FindControl("lbl_contact2"))).Visible = false; ;
                        ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                    }
                    else
                    {
                        if (((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact1"))).Visible = false;
                        }
                        if (((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact2"))).Visible = false;
                        }
                        if (((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                        }
                    }
                    // Bond flag setting
                    if (((Label)items.FindControl("lbl_BondFlag")).Text == "1")
                    {
                        ((Label)items.FindControl("lbl_Bond")).Visible = true;
                    }
                    // Owes setting
                    if (((Label)(items.FindControl("lbl_owes"))).Text.StartsWith("$0") == false)
                    {
                        ((Label)(items.FindControl("lbl_owes"))).Text = "(" + ((Label)(items.FindControl("lbl_owes"))).Text + ")";
                        ((Label)(items.FindControl("lbl_owes"))).Visible = true;
                    }
                    //Insurance settings
                    if (((Label)items.FindControl("lbl_Insurance")).Text.Length > 0)
                    {
                        ((Label)items.FindControl("lbl_Insurance")).Visible = true;
                    }
                    //child belt settings
                    if (((Label)items.FindControl("lbl_Child")).Text.Length > 0)
                    {
                        ((Label)items.FindControl("lbl_Child")).Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void ddl_rStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                //	if (SessionRecid=="")					 
                //	Session["ReminID"]="1";              //initialize Session RecID	
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void btn_update1_Click(object sender, System.EventArgs e)
        {
            try
            {
                //	if (SessionRecid=="")					 
                //	Session["ReminID"]="1";			 //initialize Session RecID	

                //Yasir Kamal 5427 02/06/2009 Pagging functionality Added.
                if (sender == btn_update1)
                {
                    dg_ReminderCalls.SelectedIndex = 0;
                }
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
                tdData.Style.Add("display", "block");
                //tdWait.Style.Add("display", "none");
                // Yasir 5427 end

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetReminderStatus()
        {
            DataTable dtStatus = clscall.GetReminderStatus();

            if (dtStatus.Rows.Count > 0)
            {
                ddl_rStatus.Items.Clear();
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                    ddl_rStatus.Items.Add(item);
                }
                ListItem itm = new ListItem("All", "5");
                ddl_rStatus.Items.Add(itm);
            }
        }

        private void GetAllReminderStatuses()
        {
            dtReminderStatus = clscall.GetReminderStatuses();
            dtReminderStatus.Constraints.Add("PrimaryKeyConstraint", dtReminderStatus.Columns[0], true);
        }

        protected void ddl_rStatus_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                //	if (SessionRecid=="")					 
                //	Session["ReminID"]="1";			 //initialize Session RecID	
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Yasir Kamal 5427 01/23/2009 Pagging functionality Added.
        /// <summary>
        /// handling pagging control pageIndex change event
        /// </summary>

        void Pagingctrl_PageIndexChanged()
        {
            dg_ReminderCalls.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
            SetUrl();

            //dg_ReminderCalls.PageIndex = Pagingctrl.PageIndex - 1;
            //FillGrid();
            //SetUrl();
        }

        /// <summary>
        /// handling dataGrid pageIndexChange event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dg_ReminderCalls_PageIndexChanged(object source, GridViewPageEventArgs e)
        {

            dg_ReminderCalls.PageIndex = e.NewPageIndex;
            FillGrid();
            SetUrl();
        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="pageSize"></param>

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_ReminderCalls.SelectedIndex = 0;
                dg_ReminderCalls.PageSize = pageSize;
                dg_ReminderCalls.AllowPaging = true;

            }
            else
            {
                dg_ReminderCalls.AllowPaging = false;
            }

            FillGrid();
            SetUrl();

        }

        // 5427 end
    }
}

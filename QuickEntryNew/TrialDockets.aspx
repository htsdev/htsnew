﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Page Language="c#" CodeBehind="TrialDockets.aspx.cs" AutoEventWireup="false"
    Inherits="HTP.QuickEntryNew.TrialDockets" %>

<%@ Register Src="~/WebControls/AttorneyScheduler.ascx" TagName="AttorneyScheduler"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>HTP Trial Docket</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <style type="text/css">
        #gv_emails tbody tr td span {
            font-size: 14px !important;
            color: black !important;
        }

        #pnlShowSettingPopup {
            width: 94.5% !important;
            margin-left: 29px;
        }
        table {
    background: white;
}
        tbody td{
    background: white;
}
        td{
    background: white !important;
}
    </style>
    <script type="text/javascript">




        function selectemail() {
            debugger;
            //var WinSettings = "'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=350"
            ////var WinSettings = "center:yes;resizable:no;dialogHeight:300px;dialogwidth:310px;scroll:yes"
            //var Arg = "Select";
            ////titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,
            ////var val = window.showModalDialog("SelectEmails.aspx", Arg, WinSettings);
            //var val = window.open("SelectEmails.aspx", Arg, WinSettings);

            //if (val == "") {
            //    return false;
            //}
            //else {
            //    document.getElementById("hf_emails").value = val;
            //    return true;

            //}
            //document.getElementById("hf_emails").value = val;
            $('#mailerModal').modal('show');

           // return false;
        }
        function OpenSingleCR() {

            var Courtloc = document.getElementById("DDLCourts").value;
            var Courtdate = document.getElementById("dtpCourtDate").value;
            var Courtnumber = document.getElementById("TxtCourtNo").value;
            var Datetype = document.getElementById("LstCaseStatus").value;
            window.open('../Reports/TrialDocketCR.aspx?Courtloc=' + Courtloc + '&Courtdate=' + Courtdate + '&Page=1&Courtnumber=' + Courtnumber + '&Datetype=' + Datetype + '&singles=1', '', 'fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
            return (false);
        }

        function OpenCRAll() {

            var Courtloc = document.getElementById("DDLCourts").value;
            var Courtdate = document.getElementById("dtpCourtDate").value;
            var Courtnumber = document.getElementById("TxtCourtNo").value;
            var Datetype = document.getElementById("LstCaseStatus").value;
            var showsdetail = document.getElementById("cbowesdetail").checked;
            window.open('../Reports/TrialDocketCR.aspx?Courtloc=' + Courtloc + '&Courtdate=' + Courtdate + '&Page=1&Courtnumber=' + Courtnumber + '&Datetype=' + Datetype + '&singles=0&showowesdetail=' + showsdetail, '', 'fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
            return (false);
        }




        function OpenPopUp(path, Val)  //(var path,var name)
        {
            window.open(path, '', "height=300,width=400,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
            //return true;
        }
        //  Noufil 5772 04/14/2009 Validate Court Number
        function CheckName(name) {
            for (i = 0 ; i < name.length ; i++) {
                var asciicode = name.charCodeAt(i)
                //If not valid alphabet 
                if (!((asciicode >= 48 && asciicode <= 57)))
                    return false;
            }
            return true;
        }
        function CheckCourtNumber() {
            if (CheckName(document.getElementById("TxtCourtNo").value) == false) {
                alert("Invalid Court Number : Please use number");
                return false;
            }
        }


        //Nasir 6968 12/14/2009 add new method ShowProgressOnUpdate display loading on show setting click 
        function ShowProgressOnUpdate() {

            //document.getElementById(tbl.id).style.display = 'block';
            //document.getElementById("btn_Update").style.display = 'none';                
            $find("MPELoding").show();
            //$get('td_iframe').style.display = 'none';
            return false;
        }
        $('#btn_sendmail').click(function () {
            //a//lert();
            returnvalues('1');
        });
        function returnvalues(opt) {
            debugger;
           // opt = 1;
            if (opt == 0) {

                window.returnValue = "";
                window.close();

            }

            if (opt == 1) {


                var emailaddress = "";
                var length = parseInt(document.getElementById("hf_totalrec").value);

                if (length > 0) {

                    for (i = 2 ; i <= length + 1; i++) {

                        var controlid = "";


                        if (i < 10) {
                            controlid = "gv_emails_ctl0" + i;
                        }
                        else {
                            controlid = "gv_emails_ctl" + i
                        }


                        var checkbox = document.getElementById(controlid + "_cb_email").checked;
                        var email = document.getElementById(controlid + "_lbl_email").innerText;

                        if (checkbox == true) {
                            emailaddress = emailaddress + email + ",";
                        }

                    }


                    if (emailaddress == "") {
                        alert("Please Select atleast one email address");
                        return false;
                    }
                    else {

                       // window.returnValue = emailaddress;
                        document.getElementById("<%= hf_emails.ClientID%>").value = emailaddress;
                        $('#hf_emails').val(emailaddress)
                        //$('#mailerModal').modal('hide');
                        $('#btnsendmail').trigger('click');
                        //window.close();
                    }
                }
                else {
                    //window.returnValue = "";
                   // window.close();
                }

            }



        }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div class="page-container row-fluid container-fluid">

            <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
            <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
            <section id="main-content" class="trialDockePage" id="TableSub">
            <section class="wrapper main-wrapper row" style="">
                         <div class="col-xs-12">
                             <div class="alert alert-danger alert-dismissable fade in" id="dvMessage" runat="server" visible="false">
                                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <asp:Label ID="lblRecStatus" runat="server" Visible="False"></asp:Label>
                                 </div>
                         </div> 
                <div class="col-xs-12">
                        <div class="page-title">
                             <div class="pull-left">
                                 <!-- PAGE HEADING TAG - START --><h1 class="title">Trial Docket</h1><!-- PAGE HEADING TAG - END -->                           
                                 </div>

                             
                            </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Filters</h2>

                            <div class="actions panel_actions pull-right">
                    
                	            <a class="box_toggle fa fa-chevron-down"></a>
                    
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-3">
                                   <div class="form-group">
                                      <label class="form-label">Court Location</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <asp:DropDownList ID="DDLCourts" runat="server" CssClass="form-control" Width="220px"
                                                ToolTip="Court Location">
                                                <asp:ListItem Value="None">--Choose--</asp:ListItem>
                                            </asp:DropDownList>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-2">
                                   <div class="form-group">
                                      <label class="form-label">Court Date</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <%--<ew:CalendarPopup ID="dtpCourtDate" runat="server" Text=" " Width="80px" ImageUrl="../images/calendar.gif"
                                    SelectedDate="2006-03-14" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                    CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                    Nullable="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Trial Date"
                                    Font-Names="Tahoma" Font-Size="8pt">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>--%>
                                               <div class="input-group date" id="">
                                     <asp:TextBox ID="dtpCourtDate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                    <label class="input-group-addon btn" for="testdate">
                                       <span class="fa fa-calendar"></span>
                                    </label> </div>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-2">
                                   <div class="form-group">
                                      <label class="form-label">Court No</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <asp:TextBox ID="TxtCourtNo" runat="server" CssClass="form-control"
                                    ToolTip="Court Room No"></asp:TextBox>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-2">
                                   <div class="form-group">
                                      <label class="form-label">Court Status</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <asp:ListBox ID="LstCaseStatus" runat="server" CssClass="clsInputCombo" Height="48px"
                                    ToolTip="Court Status">
                                    <asp:ListItem Value="0" Selected="True">---Choose----</asp:ListItem>
                                    <asp:ListItem Value="2">Arraignement</asp:ListItem>
                                    <asp:ListItem Value="3">Pre Trial Setting</asp:ListItem>
                                    <asp:ListItem Value="5">JudgeTrial</asp:ListItem>
                                    <asp:ListItem Value="4">Jury Trail Setting</asp:ListItem>
                                    <asp:ListItem Value="52">Non Issue</asp:ListItem>
                                    <asp:ListItem Value="57">Scire</asp:ListItem>
                                    <asp:ListItem Value="51">Other</asp:ListItem>
                                </asp:ListBox>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-2">
                                   <div class="form-group">
                                   <label class="form-label">&nbsp;</label>
                                   <%--   <label class="form-label">Court Status</label>--%>
                                       <span class="desc"></span>
                                           <div class="controls">
                                              <asp:CheckBox ID="cbowesdetail" runat="server" CssClass="checkbox-custom" Text="$ Detail"
                                    Checked="true" />
                                           </div>
                                    </div>
                               </div>
                                <div class="clearfix"></div>
                                <hr />
                                <asp:Button ID="Button_Submit" runat="server" CssClass="btn btn-primary" Text="Submit"
                                    OnClientClick="return CheckCourtNumber();" ></asp:Button>
                                <asp:Button  ID="btnsendmail" runat="server" OnClick="SendMailTo" CssClass="btn btn btn-primary pull-left" Text="Send" style="display:none;"/>
                            <asp:HiddenField ID="hf_emails" runat="server" />
                            </div>
                        </div>
                    </section> 
                    <div class="clearfix"></div>
                    <section class="box ">
                        <header class="panel_header">
                             <asp:LinkButton ID="lnk_ShowSetting" Text="Show Setting" runat="server" CssClass="title pull-left"
                        OnClick="lnk_ShowSetting_Click" OnClientClick="ShowProgressOnUpdate();"></asp:LinkButton>
                            <%--<h2 class="title pull-left">Show Setting</h2>--%>

                            <div class="actions panel_actions pull-right">
                    
                	            <a class="box_toggle fa fa-chevron-down"></a>
                    
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12">
                                   <table width="100%" cellspacing="1" cellpadding="1" border="1" style="border-collapse: collapse">
                                    <tr>
                                        <td align="center" style="font-size: 10pt; width: 34%;">
                                            <img id="ImageButton2"  src="../Images/Email.jpg" alt="" data-toggle="modal" data-target="#mailerModal"
                                                ToolTip="Email" /><br />
                                            <font face="Arial" size="1">EMAIL</font>
                                        </td>
                                        <td align="center" style="font-size: 10pt; display: none">
                                            <asp:ImageButton ID="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg"
                                                ToolTip="Print"></asp:ImageButton><br />
                                            <font face="Arial" size="1">HTML PRINT</font>
                                        </td>
                                        <%--<td align="center" style="font-size: 10pt">
                                            <asp:ImageButton ID="imgExp_Ds_to_Excel" runat="server" ImageUrl="../Images/excel_icon.gif"
                                                Width="81px" Height="36px" ToolTip="Excel"></asp:ImageButton><br />
                                            <font face="Arial" size="1">EXCEL</font>
                                        </td>--%>
                                        <td align="center" style="font-size: 10pt; width: 33%">
                                            <asp:ImageButton ID="btnPDFCourts" runat="server" ImageUrl="../Images/pdfall.jpg"
                                                OnClick="img_PDFCourt" ToolTip="Print All Court" Width="39px" />
                                            <asp:DropDownList ID="ddl_Copies" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem Selected="True">5</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <font face="Arial" size="1">MULTIPLES</font>
                                        </td>
                                        <td align="center" style="font-size: 10pt; width: 33%">
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/pdfRM.JPG" ToolTip="Court Room"
                                                OnClick="Single_Click" />
                                            <br />
                                            <font face="Arial" size="1">SINGLES</font>
                                        </td>
                                        <%--<td align="center" style="font-size: 10pt; width: 90px;">
                                            <asp:ImageButton ID="Button_CR" runat="server" ImageUrl="~/Images/Cr.jpg" ToolTip="CrystalReport Version"
                                                Height="41px" OnClientClick="return OpenCRAll()" />&nbsp;&nbsp;<br />
                                            <font face="Arial" size="1">PDF</font>
                                        </td>--%>
                                    </tr>
                                </table>
                               </div>
                                <hr />
                                <asp:Panel id="td_iframe" runat="server"  Visible="false">
                                <div class="col-md-12" >
                                <iframe id="reportFrame" runat="server" tabindex="0" frameborder="1" scrolling="auto" width="98%" height="800px"></iframe>
                                    </div>
                                    </asp:Panel>
                            </div>
                        </div>
                    </section>
                 
                </div>
            </section>



                <aspnew:UpdatePanel ID="upnlcourt" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="MPEShowSettingPopup" runat="server" BackgroundCssClass="modalBackground"
                    PopupControlID="pnlShowSettingPopup" TargetControlID="Button2">
                </ajaxToolkit:ModalPopupExtender>
                <%--CancelControlID="lnkbtnCancelPopUp"--%>
                <asp:Button ID="Button2" runat="server" Style="display: none" Text="Button" />
                <div class="row-fluid" >
<asp:Panel ID="pnlShowSettingPopup" runat="server">
                    <uc1:AttorneyScheduler ID="AttorneySchedulerID" runat="server" />
                </asp:Panel>
                </div>
                
            </ContentTemplate>
            <Triggers>
                <asp35:AsyncPostBackTrigger ControlID="lnk_ShowSetting" EventName="Click" />
            </Triggers>
        </aspnew:UpdatePanel>

       
       


       </section>

        </div>
        <%--<ajaxToolkit:ModalPopupExtender ID="MPELoding" runat="server" BackgroundCssClass="modalBackground"
            PopupControlID="Loading" TargetControlID="Button1">
        </ajaxToolkit:ModalPopupExtender>--%>
        <asp:Button ID="Button1" runat="server" Style="display: none" />
        <div id="Loading">
            <img alt="Please wait" src="../Images/page_loading_ani.gif" />
        </div>

        <!-- CORE JS FRAMEWORK - START -->

        <!-- END CORE TEMPLATE JS - END -->
        <div id="mailerModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #11a2cf; color: #fff;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="color: #fff;">Select Attorneys Email Addresses</h4>
                    </div>
                    <div class="modal-body">

                        <asp:GridView ID="gv_emails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                            Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle Width="5%" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cb_email" runat="server" Checked="True" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <HeaderStyle CssClass="" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_email" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.EmailAddress") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:HiddenField ID="hf_totalrec" runat="server" />

                    </div>
                    <div class="modal-footer">
                        <%--<asp:ImageButton ID="ImageButton3" runat="server" OnClick="SendMail" ImageUrl="btn btn btn-primary pull-left"
                                                ToolTip="Email" OnClientClick="return selectemail();" /><br />--%>
                        <input type="button" id="btn_sendmail" class="btn btn btn-primary pull-left" value="Send" onclick="return returnvalues('1')" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </form>


</body>
</html>

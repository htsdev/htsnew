using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;

namespace lntechNew.QuickEntryNew
{
    public partial class TrialDocketNew : System.Web.UI.Page
    {
        private string filepath;

        public string Filepath
        {
            get { return filepath; }
            set { filepath = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
            filepath = @"\Reports\TrialDocket.rpt";
            CRV1.ReportSource = filepath;
            
            
        }
    }
}

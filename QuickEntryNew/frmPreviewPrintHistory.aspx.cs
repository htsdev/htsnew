using System;
using System.Configuration;
using System.IO;
using lntechNew.Components.ClientInfo;

namespace HTP.QuickEntryNew
{
    public partial class frmPreviewPrintHistory : System.Web.UI.Page
    {
        readonly clsLogger _bugTracker = new clsLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ViewState["FilePath"] = ConfigurationManager.AppSettings["NPathSummaryReports"];
                    ViewState["vNTPATH"] = ConfigurationManager.AppSettings["NTPath"];
                    //ozair 8039 07/20/2010 implemented Arr/Bond Summary links in Documents page
                    var path = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1);
                    var fileName = Request.QueryString["filename"] + ".pdf";
                    //checking for file name having * which will be used to search for files and getting the file name
                    if (fileName.Contains("*"))
                    {
                        var files = Directory.GetFiles(ViewState["FilePath"].ToString(), fileName);
                        //ozair 8039 07/20/2010 remove variable i to remove warining becasue i was not using inside the loop and code was considered as unrecheable.
                        for (var i = 0; i < files.Length;)
                        {
                            path = path + files[0].Substring(files[0].LastIndexOf("\\") + 1);
                            break;
                        }
                    }
                    else
                    {
                        path = path + fileName;
                    }

                    Response.Redirect("../Docstorage" + path, false);
                }
            }
            catch (Exception ex)
            {
                _bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

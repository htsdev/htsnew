﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArrCombined.aspx.cs" Inherits="HTP.QuickEntryNew.ArrCombined" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Scheduled Arraignments</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->



     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
  
    <script language="javascript" type="text/javascript">
		
		function ArraignmentDocketPopUp(flag)
		{
		    var day=document.getElementById("HF_ArraignmentDocket").value;
		    window.open("../Reports/ArraignmentDocket.aspx?day="+day+"&Flag="+flag,'',"toolbar=no,status=no,menubar=no,resizable=yes");
		    return false;
		}
		
	    function PrintAllBondValidate()
	    {
	    	    if(ValidateBondWaiting()==false)
                {
                    alert("Please Select Row for Print Bond.");
		            return false;
                }
	    }
	    
	       function PopUpShowPreviewPDF(TDocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("../Paperless/PreviewMain.aspx?DocID=0&TDocID="+ TDocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				return false;
			}
		
	 function ValidateArraignmentWaiting()
		{ 
		  
		  var rec_count =document.getElementById("hf_Count").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="dg_AWR_ctl02_chk_Select";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="dg_AWR_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk_Select";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk_Select";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
	    
	    
	     function ValidateBondWaiting()
		{ 
		  
		  var rec_count =document.getElementById("hf_Count").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="dg_BondWaiting_ctl02_chk";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="dg_BondWaiting_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
		
		function ShowBndWaitingPopup()
		{
		
		if(ValidateBondWaiting()==false)
		{
		alert('No record Selected');
		return(false);
		}
		var res = confirm('A document ID Number will be generated for this report.');
		if(res==true)
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 1;
		 return(true);
		}
		else
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 0;
		 return(false);// Afaq 8039 07/21/2010 report will show on ok button only.			
		}
		
		}
		
		
		function ShowArrWaitingPopup()
		{
		
		if (ValidateArraignmentWaiting()==false)
		{
		alert('No record Selected');
		return(false);
		}
		var res = confirm('A document ID Number will be generated for this report.');
		if(res==true)
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 1;
		 return(true);
		}
		else
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 0;
		 return(false);// Afaq 8039 07/21/2010 report will show on ok button only.			
		}
		
		}
		
		function Hide()
		{
		
			if( document.getElementById("ddl_WeekDay").value=="-1")
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
		    //Yasir Kamal 5822 04/21/2009 Extra space Removed.				
			//	document.getElementById("a1").style.display='none';
				//document.getElementById("a2").style.display='none';				
				document.getElementById("bond").style.display='none';					
				document.getElementById("bondTD").style.display='none';					
			}
			else if(document.getElementById("ddl_WeekDay").value==100)
			{
				document.getElementById("jt").style.display='';
				document.getElementById("jtgrd").style.display='';				
			//	document.getElementById("a1").style.display='none';
				//document.getElementById("a2").style.display='none';				
				document.getElementById("bond").style.display='none';					
				document.getElementById("bondTD").style.display='none';	
			}
			//code Added by Azwer... for hiding and visible bond report..
			else if(document.getElementById("ddl_WeekDay").value==150)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';				
				//document.getElementById("a1").style.display='none';
				//document.getElementById("a2").style.display='none';				
				document.getElementById("bond").style.display='';					
				document.getElementById("bondTD").style.display='';	
			}
			else if(document.getElementById("ddl_WeekDay").value!=99)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';			
			//	document.getElementById("a1").style.display='none';
				//document.getElementById("a2").style.display='none';				
				document.getElementById("bond").style.display='none';					
				document.getElementById("bondTD").style.display='none';	
			}
			
			else
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='';				
			//	document.getElementById("a1").style.display='';
				//document.getElementById("a2").style.display='';								
			}
			
			
		}
		function OpenBondSummary(path)
		{
		    
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
		    
		}
		 function Validate()
		{ 
		  
		  var rec_count =document.getElementById("txtARRSummCount").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="ASdg_ctl02_chk1";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="ASdg_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk1";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk1";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
	     function select_deselectAll(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,10)=='dgbond$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
		
		 function select_deselectAll_ArrWait(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,10)=='dg_AWR$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
			 function select_deselectAll_BondWait(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,18)=='dg_BondWaiting$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
		
		function Prompt(val)
		{
		    
		    if(val== "1")
		    {
		        if(Validate() == false)
		        {
		            alert("Please Select Row for Print");
		            return false;
		        }
		         doyou = confirm("Selected Cases will be changed to waiting status and document ID will be generated? (OK = Yes   Cancel = No)"); 
                 if (doyou == true)
                 {
                    document.getElementById("HD").value="1";//for generating docs
			        return true;
                 }
                 else
                 {
                    document.getElementById("HD").value="2";//for Preview
                    return false; // Afaq 8039 07/21/2010 report will show on ok button only.			
                 }
             }
             if(val=="2")    
             {
                doyou = confirm("Selected Cases will be changed to waiting status and document ID will be generated? (OK = Yes   Cancel = No)"); 
                if (doyou == true)
                {
                    OpenBondSummary("../Reports/ArrSummary.aspx?Flag=3"); //print all with docid
                    window.location.reload();
                    return false;
                }
                else
                {
                    OpenBondSummary("../Reports/ArrSummary.aspx?Flag=4");   //print all with out docid
                    return false;
                }
             
             }
             if(val=="3")
             {
                if(ValidateforBond()==false)
                {
                    alert("Please Select Row for Print");
		            return false;
                }
                doyou = confirm("Selected Cases will be changed to waiting status and document ID Number will be generated? (OK = Yes   Cancel = No)"); 
                if (doyou == true)
                {
                     //for generating docs
                     document.getElementById("HD").value="1";
                    return true;
                }
                else
                {
                    document.getElementById("HD").value="2";      //for without docs
                    return false; // Afaq 8039 07/21/2010 report will show on ok button only.			
                }  
             
             }
             if(val=="4")
             {
                doyou = confirm("Selected Cases will be changed to waiting status and document ID Number will be generated? (OK = Yes   Cancel = No)"); 
                if (doyou == true)
                {
                     //for generating docs
                     OpenBondSummary("../Reports/BondSummary.aspx?Flag=3");//PRINTAAL WITH DOCS
                     window.location.reload();
                    return false;
                }
                else
                {
                    OpenBondSummary("../Reports/BondSummary.aspx?Flag=4");     //print all  without docs
                    return false;
                }  
             }
	   }
	    function ValidateforBond()
		{ 
		  
		  var rec_count =document.getElementById("txtRowCount").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="dgbond_ctl02_chk";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="dgbond_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
	    
	    function select_deselectAll2(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,8)=='ASdg$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAlll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
		
		function PopUp(ticketid)
		{
		    var path="PopUp.aspx?ticketid="+ticketid;
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes,width=450,height=200");
		    return false;
		}
		function DocValidate()
		{
		        
		    var txtdocid=document.getElementById("txtDocid").value;
		    if(txtdocid=="")
		    {
		        alert("Please Enter the DocumentID");
		        document.getElementById("txtDocid").focus();
		        return false;
		    }
		    
		        if(isNaN(txtdocid)==true)
		        {
		            alert("Please Enter Valid Document ID");
		            document.getElementById("txtDocid").focus();
		            return false;
		        }
		        if(spaceValidation(txtdocid)==false)
		        {
		            alert("Please Enter Valid Document ID");
		            document.getElementById("txtDocid").focus();
		            return false;
		        }
		   
		    
		    
		}
		 function spaceValidation(str)
        { 
            
           str = (str.replace(/^\W+/,'')).replace(/\W+$/,'');

            if(str == "")
            { 
                return false;
             }
        }
		
    </script>

</head>
<body onload="javascript: Hide();" ms_positioning="GridLayout">
    <form id="Form2" method="post" runat="server">
         <div class="page-container row-fluid container-fluid">
             <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>
              <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>
                    <div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                     <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Docket Settings</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                    
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <asp:Label ID="lbl_Message" runat="server" CssClass="Label" Font-Bold="True" ForeColor="Green"
                                Visible="False">There is currently no clients arraigned for arraignments</asp:Label>
                    <div class="col-lg-12">
                        <section class="box ">
                             <header class="panel_header">
                                <%--<h2 class="title">Basic Table</h2>--%>
                                <div class="col-md-8 col-sm-9 col-xs-10 pull-left">
                                    <div class="form-group">
                                        <div class="controls">
                                           <asp:DropDownList ID="ddl_WeekDay" runat="server" AutoPostBack="True" CssClass="clsinputcombo"
                                OnSelectedIndexChanged="ddl_WeekDay_SelectedIndexChanged1">
                                <asp:ListItem Value="-1">Select a Day</asp:ListItem>
                                <asp:ListItem Value="160">Arraignment Settings</asp:ListItem>
                                <asp:ListItem Value="999">Arraignment Waiting</asp:ListItem>
                                <asp:ListItem Value="150">Bond Settings</asp:ListItem>
                                <asp:ListItem Value="998">Bond Waiting</asp:ListItem>
                                <asp:ListItem Value="100">Judge Trial / PR</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lbl_CurrDateTime" runat="server"  Font-Bold="True"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="actions panel_actions pull-right">
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </div>
                            </header>
                            <div class="content-body">
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <asp:HyperLink ID="hp" runat="server" NavigateUrl="~/QuickEntryNew/PrintHistory.aspx"
                                Visible="False">Show Print History</asp:HyperLink>
                            <asp:Label ID="lblSep" runat="server" ForeColor="Blue" Text="|" Visible="False"></asp:Label>&nbsp;
                            <asp:HyperLink ID="HPARRSumm" runat="server" NavigateUrl="#" Visible="False">Print All</asp:HyperLink>
                            <asp:HyperLink ID="lnk_bsumm" runat="server" NavigateUrl="#" Visible="False">Print ALL</asp:HyperLink>
                            <asp:ImageButton ID="ImgCrystalSingle" runat="server" ImageUrl="../Images/pdf.jpg"
                                ToolTip="Print Single" Visible="False" OnClientClick="return ArraignmentDocketPopUp(1);" />
                                        </div>
                                         
                                    </div>
                                    <div class="col-xs-12">
        <table cellpadding="0" cellspacing="0" border="0" align="center">
        <tr>
            <td >
                <table id="TableMain" cellspacing="0" cellpadding="0" align="center"  border="0">                   
                    
                    <tr>
                        <td align="right">
                           
                        </td>
                    </tr>
                    <tr>
                        <td id="td_Title" runat="server"  style="display: none">
                            From:
                             <picker:datepicker id="cal_From" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                            <%--<ew:CalendarPopup ID="cal_From" runat="server" Width="86px" ImageUrl="../Images/calendar.gif"
                                ControlDisplay="TextBoxImage">
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                            </ew:CalendarPopup>--%>
                            To:
                             <picker:datepicker id="cal_To" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                            <%--<ew:CalendarPopup ID="cal_To" runat="server" Width="86px" ImageUrl="../Images/calendar.gif"
                                ControlDisplay="TextBoxImage">
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                            </ew:CalendarPopup>--%>
                            <asp:CheckBox ID="chk_ShowAllRec" runat="server" Checked="True" Text="Show all" />
                            <asp:Button ID="btn_SearchRec" runat="server" CssClass="clsbutton" Text="Search"
                                OnClick="btn_Search_BondWaiting_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 9px" colspan="5">
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="5">
                            <asp:Label ID="lblmsg" runat="server"></asp:Label>
                        </td>
                    </tr>
                   
                    <tr>
                        <td id="longshot" runat="server" style="display: none">
                            <table id="TableGrid" cellspacing="0" cellpadding="0" bgcolor="white"
                                border="0" class="table table-small-font table-bordered table-striped">
                             <%--   <tr>
                                    <td style="height: 16px">
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td id="jt" style="height: 14px">
                                        <strong>Judge Trials</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="a1" style="height: 14px">
                                    </td>
                                </tr>
                                <tr>
                                    <td id="jtgrd" style="height: 9px"  colspan="5" height="9">
                                        <asp:DataGrid ID="dg_ReportJT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                                            BorderColor="Black" BorderStyle="Solid" >
                                            <ItemStyle ></ItemStyle>
                                            <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="No:">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_sno1" runat="server">0</asp:Label>
                                                        <asp:Label ID="lbl_Ticketid1" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Flags">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label141" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Case #">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle  Font-Bold="True"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnkCaseNo1" runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                                            Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>'
                                                            Font-Underline="True">
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Seq" Visible="false">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label151" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                                        </asp:Label>&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Last Name">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label161" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                        </asp:Label>&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="First Name">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label171" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                        </asp:Label>&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="DL">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label181" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                                        </asp:Label>&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="DOB">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Court Information">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CCDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
                                                        </asp:Label>
                                                        <asp:Label ID="lbl_CCNum" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Officer Day">
                                                    <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle ></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_OfficerDay" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerDay") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                        <table width="100%" id="tbl_PR_Report" runat="server" class="table table-small-font table-bordered table-striped">
                                            <tr>
                                                <td style="height: 14px">
                                                    <strong>Probation Request Report</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="Td4" style="height: 14px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dg_ProbReq" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                                        BorderStyle="Solid" Width="100%" OnItemDataBound="dg_ProbReq_ItemDataBound">
                                                        <ItemStyle ></ItemStyle>
                                                        <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="No">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Last Name" SortExpression="lastname">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_LastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="First Name">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Cause No">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="lbl_CauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Ticket No">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_TicketNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Violation Desc">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_VioDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Crt Date">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Crt Time">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:t}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Room">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtRoom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtRoom") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Status">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Flags">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Flags" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Flag") %>'>
                                                                    </asp:Label>
                                                                   
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Trial Comments">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle  Font-Size="XX-Small"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_TrialComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialComments") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" id="tbl_Cont_Report" runat="server" class="table table-small-font table-bordered table-striped">
                                            <tr>
                                                <td style="height: 14px">
                                                    <strong>Continuance Report</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="Td3" style="height: 14px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dg_Cont" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                                        BorderStyle="Solid" Width="100%" OnItemDataBound="dg_Cont_ItemDataBound" >
                                                        <ItemStyle ></ItemStyle>
                                                        <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="No">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Last Name" SortExpression="lastname">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_LastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="First Name">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Cause No">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="lbl_CauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Ticket No">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_TicketNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Violation Desc">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_VioDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Crt Date">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Crt Time">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:t}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Room">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtRoom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtRoom") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Status">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Flags">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle ></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Flags" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Flag") %>'>
                                                                    </asp:Label>
                                                                   
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Continuance Comments">
                                                                <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                                                <ItemStyle  Font-Size="XX-Small"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_ContinuanceComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContinuanceComments") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="100%" style="display: block" id="BforBond" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td id="bond">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table table-small-font table-bordered table-striped">
                                <tr>
                                    <td>
                                        <strong>Bond Settings</strong>
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="btnBoncSumm" runat="server" Text="Print Selected" OnClick="btnBondSumm_Click"
                                            CssClass="clsButton"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="5">
                        </td>
                    </tr>
                    <tr>
                        <td id="bondTD">
                            <asp:DataGrid ID="dgbond" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                BorderStyle="Solid" Width="100%" OnItemDataBound="dgbond_ItemDataBound" AllowSorting="True"
                                OnSortCommand="dgbond_SortCommand">
                                <ItemStyle ></ItemStyle>
                                <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="btvids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="bticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="serialNo" runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="CheckAll" runat="server" onclick="return select_deselectAll(this.checked, this.id);"
                                                Checked="false" />
                                        </HeaderTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CAUSE NO">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="bCauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.causeno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FLAG">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblArrFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TICKET NO">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bTicketNO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="blastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FIRST NAME">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DL">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bDL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DOB">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="X-REF">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Status">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcrtdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BOND D">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblbonddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bonddate") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="B Time" SortExpression="Time">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBond" runat="server" ImageUrl="~/Images/preview.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="td_BondWaiting" runat="server" style="display: block">
                <table>
                    <tr>
                        <td align="left">
                            <asp:Button ID="btn_PrintReport_Bond" runat="server" CssClass="clsbutton" OnClick="btn_PrintReport_Bond_Click"
                                OnClientClick="return ShowBndWaitingPopup()" Text="Print Summary Report" Visible="False" />
                        </td>
                        <td align="right">
                            <asp:LinkButton ID="PrintAllBonds" runat="server" Visible="False" OnClientClick="return PrintAllBondValidate();"
                                OnClick="PrintAllBonds_OnClick">Print All Bonds</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dg_BondWaiting" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                BorderStyle="Solid" Width="100%" OnItemDataBound="Bondwaiting_ItemDataBound"
                                AllowSorting="True" OnSortCommand="dgbondwaiting_sortcommand">
                                <ItemStyle ></ItemStyle>
                                <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="btvids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="bticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="serialNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="CheckAll" runat="server" onclick="return select_deselectAll_BondWait(this.checked, this.id);"
                                                Checked="false" />
                                        </HeaderTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CAUSE NO" SortExpression="causeno">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="bCauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.causeno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FLAG" SortExpression="BondFlag">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_bond" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TICKET NO" SortExpression="TICKETNO">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bTicketNO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="blastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FIRST NAME" SortExpression="firstname">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DL" SortExpression="dlnumber">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bDL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DOB" SortExpression="DOB">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="X-REF" SortExpression="midnum">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Status" SortExpression="status">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="bstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID" SortExpression="doccount">
                                        <HeaderStyle  Font-Bold="True" Width="20%"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <sup>
                                                <asp:Label ID="lblSc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doccount") %>'
                                                    Font-Size="8">
                                                </asp:Label></sup><asp:ImageButton ID="ImgDoc1" Style="display: inline" runat="server"
                                                    ImageUrl="~/Images/folder.gif" Height="20px" />
                                            <asp:Label ID="lblshowscanB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.showscan") %>'
                                                Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Court Date" SortExpression="CourtDate">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="B DATE" SortExpression="BondDate">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_BondDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondDate","{0:d}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="WAIT D" SortExpression="WaitingDate">
                                        <HeaderStyle Font-Bold="True"  />
                                        <ItemStyle  />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_WaitTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WaitingDate","{0:d}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="W Time" SortExpression="Time">
                                        <HeaderStyle  Font-Bold="True"></HeaderStyle>
                                        <ItemStyle ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="display: none" id="AforArraignment" runat="server" class="table-responsive">
                <div class="row" id="Td1">
                                <div class="col-lg-12">
                                    <div class="pull-left">
                                        <strong>Arraignment Settings</strong>
                                    </div>
                                     <div class="pull-right">
                                        <asp:Button ID="btnARR" runat="server" Text="Print Selected" CssClass="btn btn-primary pull-right"
                                            OnClick="btnARR_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
            </td>
        </tr>
        <tr>
            <td height="5">
            </td>
        </tr>
        <tr>
            <td id="Td2"  runat="server" style="height: 165px">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table table-small-font table-bordered table-striped">
                            <asp:DataGrid ID="ASdg" runat="server" AutoGenerateColumns="False" BorderColor="Black" class="table table-small-font table-bordered table-striped"
                    BorderStyle="Solid"  OnItemDataBound="ASdg_ItemDataBound" OnSortCommand="ASdg_SortCommand"
                    AllowSorting="True">
                    <ItemStyle ></ItemStyle>
                    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="ASTVIDS" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="ASTIDS" runat="server"   Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lblSNo" runat="server"  CssClass="form-label" NavigateUrl="#"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk1" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="CheckAlll" runat="server" onclick="return select_deselectAll2(this.checked, this.id);"
                                    Checked="false" />
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CAUSE NO">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="AScauseno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="FLAG">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblArrFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLAGID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TICKET NO">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Asticketno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="ASlastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="FIRST NAME">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="ASfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DL">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDLNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOB">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="ASDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="X-REF">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="ASmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Status">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="ASStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="ASCourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Time">
                            <HeaderStyle  Font-Bold="True"></HeaderStyle>
                            <ItemStyle ></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                        </div>
                        
                    </div>

                </div>
                
            </td>
        </tr>
    <%--    <tr>
            <td height="15">
            </td>
        </tr>--%>
        <tr>
            <td>
                <div class="col-lg-12" id="tbl_ArrWaiting" runat="server" style="display:none;">
                     <section class="box ">
                         <header class="panel_header">
                                <%--<h2 class="title">Basic Table</h2>--%>
                                <div class="col-md-8 col-sm-9 col-xs-10 pull-left">
                                    <div class="form-group">
                                        <div class="controls">
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="actions panel_actions pull-right">
                                   <asp:Button ID="btn_PrintReport" runat="server" CssClass="clsbutton" OnClick="btn_PrintReport_Click"
                                            OnClientClick="return ShowArrWaitingPopup()" Text="Print Summary Report" Visible="false" />
                                        <asp:HiddenField ID="hf_ArrWaiting_GenIDs" runat="server" Value="0" />
                                        <asp:HiddenField ID="hf_BondWaiting_GenIDs" runat="server" Value="0" />
                                </div>
                            </header>
                          <div class="content-body">
                               <div class="row">
                                    <div class="col-xs-12">
                                        <asp:DataGrid ID="dg_AWR" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                            BorderStyle="Solid" OnItemDataBound="AWRWaiting_ItemDataBound"
                                            AllowSorting="True" OnSortCommand="dg_AWR_SortCommand">
                                            <ItemStyle  />
                                            <HeaderStyle BackColor="#FFCC00" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASTVIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASTIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="NO">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="CheckAll" runat="server" onclick="return select_deselectAll_ArrWait(this.checked, this.id);"
                                                            Checked="false" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chk_Select" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="CAUSE NO" SortExpression="CAUSENO">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="AScauseno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="FLAG" SortExpression="FLAGID">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArrWFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLAGID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="TICKET NO" SortExpression="TICKETNO">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Asticketno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASlastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="FIRST NAME" SortExpression="firstname">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="DL" SortExpression="dlnumber">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_DL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="DOB" SortExpression="DOB">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="X-REF" SortExpression="midnum">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Status" SortExpression="status">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="ID" SortExpression="doccount">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <sup>
                                                            <asp:Label ID="lblScripts" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doccount") %>'
                                                                Font-Size="8"></asp:Label></sup><asp:ImageButton ID="ImgDoc" Style="display: inline"
                                                                    runat="server" ImageUrl="~/Images/folder.gif" />
                                                        <asp:Label ID="lblshowscan" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.showscan") %>'
                                                            Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASCourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="WAIT D" SortExpression="WaitingDate">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_WaitTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WaitingDate","{0:d}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Time" SortExpression="Time">
                                                    <HeaderStyle Font-Bold="True"  />
                                                    <ItemStyle  />
                                                    <ItemTemplate>
                                                        <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                        <asp:TextBox ID="txtRowCount" runat="server"></asp:TextBox><asp:TextBox ID="txtARRSummCount"
                                runat="server"></asp:TextBox>
                                    </div>
                                   </div>
                          </div>
                     </section>
                </div>
             
            </td>
        </tr>
    </table>
                                        </div>
                                </div>
                            </div>
                            </section>
                    </div>
                    </section>
                </section>
         </div>
    
    <asp:HiddenField ID="HD" runat="server" />
    <asp:HiddenField ID="HF_ArraignmentDocket" runat="server" />
    <asp:HiddenField ID="hf_Count" runat="server" Value="0" />
    </form>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#mainmenu li, #limatter").attr("class", "");
            $("#ulmatter").css("display", "none");
            $("#lireport").attr("class", "open");
            $("#ulreport").css("display", "block");
            $("#ActiveMenu1_A52").addClass("active");
            $("#ActiveMenu1_A52 > a li > span:nth-child(2)").addClass("arrow open");
            
        });
    </script>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Configuration.Helper;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Data.SqlClient;
using lntechNew.Components;
using System.Configuration;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using System.Threading;
using System.IO;
//Nasir 6968 12/14/2009 add name spance
using HTP.Components.Services;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.QuickEntryNew
{
    /// <summary>
    /// Summary description for TrialDockets.
    /// </summary>
    public partial class TrialDockets : System.Web.UI.Page
    {
        #region Variables
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        protected System.Web.UI.WebControls.DropDownList DDLCourts;
        protected System.Web.UI.WebControls.TextBox dtpCourtDate;
        protected System.Web.UI.WebControls.TextBox TxtCourtNo;
        protected System.Web.UI.WebControls.DropDownList DropDownList2;
        protected System.Web.UI.WebControls.ListBox LstCaseStatus;
        //protected System.Web.UI.WebControls.Table Table_OutPut;
        clsCourts ClsCourt = new clsCourts();
        protected System.Web.UI.WebControls.Button Button_Submit;
        clsSession ClsSession = new clsSession();
        protected System.Web.UI.WebControls.Label lblRecStatus;
        protected System.Web.UI.WebControls.ImageButton imgPrint_Ds_to_Printer;
        protected System.Web.UI.WebControls.ImageButton imgExp_Ds_to_Excel;
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsLogger bugTracker = new clsLogger();
        //Ozair 5057 03/24/2009 Warnings Removed
        DataSet Ds_FillTable;
        string servername = string.Empty;

        #endregion

        clsENationWebComponents ClsDB = new clsENationWebComponents();
        private void Page_Load(object sender, System.EventArgs e)
        {
            // Check for valid Session if not then redirect to login page	
            try
            {
                //Kazim 3984 5/26/2008 Set endresponse property to false

                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                    goto SearchPage;
                }
                ClsSession.SetSessionVariable("sMoved", "False", this.Session);
                //if (TxtCourtNo.Text == "")
                //    TxtCourtNo.Text = "-1";


                try
                {
                    DataSet ds_emails = ClsDB.Get_DS_BySP("USP_HTS_GetAttorneyAddress");

                    if (ds_emails.Tables[0].Rows.Count > 0)
                    {
                        hf_totalrec.Value = ds_emails.Tables[0].Rows.Count.ToString();
                        gv_emails.DataSource = ds_emails.Tables[0];
                        gv_emails.DataBind();
                    }
                    else
                    {
                        hf_totalrec.Value = "0";
                    }
                }
                catch (Exception)
                {

                }
                if (!Page.IsPostBack)
                {
                   
                   
            //Set network path
            ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                    ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH1"].ToString();
                    //

                    FillCourts();
                    //FillGrid();

                    //DDLCourts.SelectedValue.ToString(), dtpCourtDate.SelectedDate, "1", TxtCourtNo.Text, LstCaseStatus.SelectedValue.ToString() };
                    //	window.open('../Reports/TrialDocketCR.aspx?Courtloc='+Courtloc+'&Courtdate=' + Courtdate + '&Page=1&Courtnumber='+ Courtnumber + '&Datetype=' + Datetype + '&singles=0','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');


                    //Aziz 1711                    
                    //Nasir 6968 12/03/2009 not add frame when page load first time 
                    //this.reportFrame.Attributes.Remove("src");
                    ViewState["SelectedDate"] = dtpCourtDate.Text;
                    //Modified By Zeeshan Ahmed On 1/1/2008
                    //Add showowesdetail parameter in the query string
                    ViewState["CourtNumber"] = TxtCourtNo.Text;
                    ViewState["datetype"] = LstCaseStatus.SelectedValue;
                    ViewState["Courts"] = this.DDLCourts.SelectedValue;
                    //Nasir 6968 12/03/2009 not display when page load first time
                    //this.reportFrame.Attributes.Add("src", "../Reports/RptTrialDocket.aspx?courtloc=" + this.DDLCourts.SelectedValue +
                    //        "&courtdate=" + dtpCourtDate.SelectedDate + "&page=1&courtnumber=" + TxtCourtNo.Text + "&datetype=" +
                    //        LstCaseStatus.SelectedValue + "&singles=0&showowesdetail=" + Convert.ToString(Convert.ToInt32(cbowesdetail.Checked)));

                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Cannot find table 0.")
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                this.lblRecStatus.Visible = true;
                dvMessage.Visible = true;
                this.lblRecStatus.Text = "Record Not Found";
            }

        SearchPage:
            { }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Submit.Click += new System.EventHandler(this.Button_Submit_Click);
            this.imgPrint_Ds_to_Printer.Click += new System.Web.UI.ImageClickEventHandler(this.imgPrint_Ds_to_Printer_Click);
            //this.imgExp_Ds_to_Excel.Click += new System.Web.UI.ImageClickEventHandler(this.imgExp_Ds_to_Excel_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Unload += new System.EventHandler(this.page_unload);
        }
        #endregion

        #region HelperMethods

        private void FillCourts()
        {
            try
            {
                IDataReader DrCourt = ClsCourt.GetAllCourtName();
                DDLCourts.DataSource = DrCourt;
                DDLCourts.DataTextField = "shortcourtname";
                DDLCourts.DataValueField = "courtid";
                DDLCourts.DataBind();
                //Sabir 4527 08/25/2008  Adding Case Type "Traffic","Criminal" and "Civil"
                //-------------------------------------------------------
                ListItem IstNewItem3 = new ListItem("Traffic", "Traffic");
                DDLCourts.Items.Insert(1, IstNewItem3);

                ListItem IstNewItem4 = new ListItem("Criminal", "Criminal");
                DDLCourts.Items.Insert(2, IstNewItem4);

                ListItem IstNewItem5 = new ListItem("Civil", "Civil");
                DDLCourts.Items.Insert(3, IstNewItem5);
                //--------------------------------------------------------
                ListItem lstNewItem = new ListItem("All Locations", "None");
                DDLCourts.Items.Insert(4, lstNewItem);

                ListItem lstNewItem1 = new ListItem("All Inside Courts", "OU");
                DDLCourts.Items.Insert(5, lstNewItem1);

                ListItem lstNewItem2 = new ListItem("All Outside Courts", "JP"); // Afaq 7154 07/28/2010 Correct the spelling of outside.
                DDLCourts.Items.Insert(6, lstNewItem2);


                DDLCourts.Items[4].Selected = true;
                this.dtpCourtDate.Text = DateTime.Now.AddDays(1).ToShortDateString();
            }
            catch (Exception ex)
            {
                this.lblRecStatus.Visible = true;
                this.lblRecStatus.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetCourtLocAndCourtNumbers()
        {
            //Yasir Kamal 6033 06/19/2009 can't fine table 1. bug fixed.
            try
            {
                string[] key = { "@courtloc", "@courtdate", "@page", "@courtnumber", "@datetype" };
                object[] value1 = { "None", dtpCourtDate.Text, "1", "-1", "0" };
                Ds_FillTable = ClsDb.Get_DS_BySPArr("usp_hts_get_trial_docket_report_new_Update", key, value1);

                string[] courts;
                //ozair code
                if (Ds_FillTable.Tables.Count > 0)
                {
                    if (Ds_FillTable.Tables[1].Rows.Count > 0)
                    {
                        string courtloc = string.Empty;
                        for (int i = 0; i < Ds_FillTable.Tables[1].Rows.Count; i++)
                        {
                            if (courtloc.Length == 0)
                            {
                                courtloc = Ds_FillTable.Tables[1].Rows[i]["currentcourtloc"].ToString();
                            }
                            else
                            {
                                courtloc = courtloc + "," + Ds_FillTable.Tables[1].Rows[i]["currentcourtloc"].ToString();
                            }
                        }
                        courts = courtloc.Split(',');
                        ViewState["vCourts"] = courts;
                    } //ozair code end
                    else
                    {
                        ViewState["vCourts"] = "";
                    }
                    // Code for CourtNumber -- Sarim
                    if (Ds_FillTable.Tables[2].Rows.Count > 0)
                    {
                        string courtloc = string.Empty;
                        for (int i = 0; i < Ds_FillTable.Tables[2].Rows.Count; i++)
                        {
                            if (courtloc.Length == 0)
                            {
                                courtloc = Ds_FillTable.Tables[2].Rows[i]["currentcourtloc"].ToString();
                                courtloc = courtloc + "|" + Ds_FillTable.Tables[2].Rows[i]["currentcourtnum"].ToString();
                            }
                            else
                            {
                                courtloc = courtloc + "," + Ds_FillTable.Tables[2].Rows[i]["currentcourtloc"].ToString();
                                courtloc = courtloc + "|" + Ds_FillTable.Tables[2].Rows[i]["currentcourtnum"].ToString();
                            }
                        }
                        courts = courtloc.Split(',');
                        ViewState["vCourtNum"] = courts;
                    }
                    else
                    {
                        ViewState["vCourtNum"] = "";
                    }
                }
            }
            catch (Exception ex)
            {
                this.lblRecStatus.Visible = true;
                dvMessage.Visible = true;
                this.lblRecStatus.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // This method will Generate PDF report by using ABC PDF.
        private void GeneratePDF(string path)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(0, 0);

            theDoc.Page = theDoc.AddPage();
            theDoc.Rect.Bottom = 15.0;
            theDoc.Rect.Top = 785.0;
            //theDoc.HtmlOptions.BrowserWidth = 800;
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }
            string name = Session.SessionID + ".pdf";
            //for website
            ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
            //website end
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();
        }

        // This method will Generate PDF report by using ABC PDF in a serial.
        private void GeneratePDF(string path, int seq)
        {
            try
            {
                if (seq == 0)
                {
                    Doc theDoc1 = new Doc();
                    theDoc1.Rect.Inset(0, 0);
                    theDoc1.Page = theDoc1.AddPage();
                    theDoc1.Rect.Bottom = 15.0;
                    theDoc1.Rect.Top = 785.0;
                    //theDoc1.HtmlOptions.BrowserWidth = 800;
                    int theID;
                    theID = theDoc1.AddImageUrl(path);
                    while (true)
                    {
                        theDoc1.FrameRect(); // add a black border
                        if (!theDoc1.Chainable(theID))
                            break;
                        theDoc1.Page = theDoc1.AddPage();
                        theID = theDoc1.AddImageToChain(theID);
                    }
                    for (int i = 1; i <= theDoc1.PageCount; i++)
                    {
                        theDoc1.PageNumber = i;
                        theDoc1.Flatten();
                    }
                    string name = Session.SessionID + ".pdf";
                    //for website
                    ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                    ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                    //website end
                    //theDoc1.Save(ViewState["vFullPath"].ToString());
                    Session["vdoc1"] = theDoc1;
                    //theDoc1.Clear();
                }
                if (seq > 0)
                {
                    Doc theDoc = new Doc();
                    theDoc.Rect.Inset(0, 0);
                    theDoc.Page = theDoc.AddPage();
                    //theDoc.HtmlOptions.BrowserWidth = 800;
                    int theID;
                    theID = theDoc.AddImageUrl(path);
                    while (true)
                    {
                        theDoc.FrameRect(); // add a black border
                        if (!theDoc.Chainable(theID))
                            break;
                        theDoc.Page = theDoc.AddPage();
                        theID = theDoc.AddImageToChain(theID);
                    }
                    for (int i = 1; i <= theDoc.PageCount; i++)
                    {
                        theDoc.PageNumber = i;
                        theDoc.Flatten();
                    }
                    Doc theDoc1 = (Doc)Session["vdoc1"];
                    theDoc1.Append(theDoc);
                    Session["vdoc1"] = theDoc1;
                    theDoc.Clear();
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            //theDoc1.Clear();
        }

        // Display PDF file generated by ABC PDF in a browser. 
        private void RenderPDF()
        {
            //for website
            string vpth = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH1"].ToString().Length) - 1);
            string link = "../DOCSTORAGE" + vpth;
            link = link + Session.SessionID + ".pdf";
            //website end
            Response.Redirect(link, false);
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();

            // delete all files that are one day old. 
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
            for (int i = 0; i < files.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);

                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files[i]);
                }
            }
        }

        //send Report to attorney's in  PDF format file.
        protected void SendMailTo(object sender, EventArgs e)
        {
            servername = Request.ServerVariables["SERVER_NAME"].ToString();
            ThreadStart ts = new ThreadStart(ThreadEmail);
            Session["ServerName"] = Request.ServerVariables["SERVER_NAME"].ToString();
            Session["users"] = hf_emails.Value;
            Thread th = new Thread(ts);
            th.Start();

        }

        //send Report to attorney's in PDF format file in a separate thread.
        private void ThreadEmail()
        {
            //clsGenerateTrialDocket clsGTD = new clsGenerateTrialDocket("None", Convert.ToDateTime(ViewState["SelectedDate"]), "1", "", "0", "0");
            string path = Server.MapPath("/reports");
            //string trialDocketPath = clsGTD.GenerateTrialDocketUsingCR(path, this.Request);
            string trialDocketPath = GenerateTrialDocketUsingCR(path);
            //string trialDocketPath = clsGTD.GenerateTrialDocketUsingCR(path, Request);
            lntechNew.Components.MailClass.SendMailToAttorneys("Trail Docket Report", "Trail Docket Report", trialDocketPath.ToString(), Session["users"].ToString());
            FileInfo fi = new FileInfo(trialDocketPath.ToString());
            fi.Delete();
        }


        public string GenerateTrialDocketUsingCR(string Serverpath)
        {
            clsENationWebComponents ClsDb = new clsENationWebComponents();
            clsCrsytalComponent Cr = new clsCrsytalComponent();

            DataSet ds;
            string name = "TrialDocket-" + DateTime.Now.ToFileTime() + ".pdf";
            //Nasir 5817 05/01/2009 New sub report added Officer Summary
            String[] reportname = new String[] { "ArraignmentWaiting.rpt", "Judge.rpt", "OfficerSummary.rpt", "Report1.rpt" };
            string[] keySub = { "@URL", "@Courtloc", "@courtdate" };
            object[] valueSub = { servername, this.DDLCourts.SelectedValue, dtpCourtDate.Text };
            ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ST_TrialDocket", keySub, valueSub);
            string path = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
            string filename = null;

            string[] key = { "@courtloc", "@courtdate", "@page", "@courtnumber", "@datetype", "@singles", "@URL" };
            object[] value1 = { this.DDLCourts.SelectedValue, dtpCourtDate.Text, 1, TxtCourtNo.Text, LstCaseStatus.SelectedValue, 1, servername };
            // Afaq 7154 07/28/2010 Get report name from configuration service.
            //filename = Serverpath + "\\TrialDocket.rpt";
            ConfigurationService ConfigurationService = new ConfigurationService();
            filename = ConfigurationService.GetFilePath(SubModule.GENERAL, Key.REPORT_FILE_PATH) + "\\TrialDocket.rpt"; 
            return Cr.CreateSubReports(filename, "usp_hts_get_trial_docket_report_CR", key, value1, reportname, ds, 1, path, name.ToString());


        }
        #endregion

        #region EventHandlers

        private void page_unload(object sender, System.EventArgs e)
        {
            //if (TxtCourtNo.Text == "-1")
            //TxtCourtNo.Text = "";
            //Response.Write("");
        }


        private void Button_Submit_Click(object sender, System.EventArgs e)
        {
            try
            {
                ViewState["SelectedDate"] = dtpCourtDate.Text;
                ViewState["CourtNumber"] = TxtCourtNo.Text;
                ViewState["datetype"] = LstCaseStatus.SelectedValue;
                ViewState["Courts"] = this.DDLCourts.SelectedValue;
                //Nasir 6968 12/03/2009 Add frame on button click                
                this.td_iframe.Visible = true;
                this.reportFrame.Attributes.Remove("src");
                this.reportFrame.Attributes.Add("src", "../Reports/RptTrialDocket.aspx?courtloc=" + this.DDLCourts.SelectedValue +
                        "&courtdate=" + dtpCourtDate.Text + "&page=1&courtnumber=" + TxtCourtNo.Text + "&datetype=" +
                        LstCaseStatus.SelectedValue + "&singles=0&showowesdetail=" + Convert.ToString(Convert.ToInt32(cbowesdetail.Checked)));
                //FillGrid();

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        // Export the report into Excel file and open into Browser. 
        private void imgExp_Ds_to_Excel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            DataSet Exp_DataSet = (DataSet)Session["PassDG"];
            if (Exp_DataSet != null)
            {
                Session["ds"] = Exp_DataSet;
                Response.Redirect("FrmTableExportToExcel.aspx");
            }
        }

        // Show the Print preview of the document. 
        private void imgPrint_Ds_to_Printer_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            DataSet expDataSet = (DataSet)Session["PassDG"];
            if (expDataSet != null)
            {
                Response.Redirect("PrintTrialDocket.aspx?courtloc=" + DDLCourts.SelectedValue.ToString() + "&courtdate=" + dtpCourtDate.Text + "&page=" + "1" + "&courtnumber=" + TxtCourtNo.Text + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 0);
            }
            else
            {
                Response.Write("No DataSet Found");
            }
        }

        //Export Report in to PDF.
        protected void img_pdf_Click(object sender, ImageClickEventArgs e)
        {
            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/PrintTrialDocket.aspx?singles=1&DT=" + DateTime.Now.ToString() + "&courtloc=" + DDLCourts.SelectedValue.ToString() + "&courtdate=" + dtpCourtDate.Text + "&page=" + "1" + "&courtnumber=" + TxtCourtNo.Text + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 1;
            //website end
            GeneratePDF(path);
            RenderPDF();
        }

        //Export all Reports in to PDF.
        protected void img_PdfAll_Click(object sender, ImageClickEventArgs e)
        {
            SetCourtLocAndCourtNumbers();

            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/PrintTrialDocket.aspx?DT=" + DateTime.Now.ToString() + "&courtloc=None" + "&courtdate=" + dtpCourtDate.Text + "&page=" + "1" + "&courtnumber=-1&datetype=0&RecdType=" + 1;
            //website end
            GeneratePDF(path, 0);

            try
            {
                string[] courtid = (string[])ViewState["vCourts"];
                for (int i = 1; i <= courtid.Length; i++)
                {
                    path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/PrintTrialDocket.aspx?DT=" + DateTime.Now.ToString() + "&courtloc=" + courtid[i - 1] + "&courtdate=" + dtpCourtDate.Text + "&page=1&courtnumber=" + TxtCourtNo.Text + "&datetype=0&RecdType=" + 1;
                    GeneratePDF(path, i);
                }

                Doc theDoc1 = (Doc)Session["vdoc1"];
                theDoc1.Save(ViewState["vFullPath"].ToString());
                theDoc1.Clear();
                Session["vdoc1"] = null;
                RenderPDF();
            }
            catch
            {

            }
        }



        //Export Report in to PDF.
        protected void img_PDFCourt(object sender, ImageClickEventArgs e)
        {
            SetCourtLocAndCourtNumbers();
            int intCopies = Convert.ToInt32(ddl_Copies.SelectedValue);
            // clsGenerateTrialDocket clsGTD = new clsGenerateTrialDocket("None", DateTime.Now.AddDays(1), "1", "", "0", "0");
            // Noufil 5772 04/21/2009 Replace tommorrow date with selected date
            clsGenerateTrialDocket clsGTD = new clsGenerateTrialDocket(Convert.ToString(ViewState["Courts"]), Convert.ToDateTime(ViewState["SelectedDate"]), "1", Convert.ToString(ViewState["CourtNumber"]), Convert.ToString(ViewState["datetype"]), "0");
            string path = Server.MapPath("/reports");
            string trialDocketPath = clsGTD.GenerateTrialDocketUsingCR(path, this.Request, intCopies);
            // Afaq 7154 08/11/2010 Add date time parameter in the link because it retrieve report from the cache instead of new report.
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('TrialDocketPopup.aspx?path={0}&datetime="+DateTime.Now.ToFileTime().ToString()+"','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');;</script>", HttpUtility.UrlEncode(trialDocketPath, System.Text.Encoding.UTF8)));
        }

        protected void Single_Click(object sender, ImageClickEventArgs e)
        {

            //clsGenerateTrialDocket clsGTD = new clsGenerateTrialDocket("None", DateTime.Now.AddDays(1), "1", "", "0", "0");
            // Noufil 5772 04/21/2009 Replace tommorrow date with selected date
            clsGenerateTrialDocket clsGTD = new clsGenerateTrialDocket(Convert.ToString(ViewState["Courts"]), Convert.ToDateTime(ViewState["SelectedDate"]), "1", Convert.ToString(ViewState["CourtNumber"]), Convert.ToString(ViewState["datetype"]), "0");
            string path = Server.MapPath("/reports");
            string trialDocketPath = clsGTD.GenerateTrialDocketUsingCR(path, this.Request);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('TrialDocketPopup.aspx?path={0}','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');;</script>", HttpUtility.UrlEncode(trialDocketPath, System.Text.Encoding.UTF8)));
        }

        //Nasir 6968 12/14/2009 add new Show Setting event show ShowSetting control popup
        protected void lnk_ShowSetting_Click(object sender, EventArgs e)
        {
            try
            {
                //Bind record in show setting control 
                AttorneySchedulerID.BindRepeaterControl();
                //set MPEShowSettingPopup property
                AttorneySchedulerID.ModalPopUpID = MPEShowSettingPopup.ClientID;
                //set ModalPopUpLoadingID property
                //AttorneySchedulerID.ModalPopUpLoadingID = MPELoding.ClientID;
                MPEShowSettingPopup.Show();
                //MPELoding.Hide();
                
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }





        #endregion

    }
}
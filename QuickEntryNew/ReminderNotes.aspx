<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.QuickEntryNew.ReminderNotes"
    CodeBehind="ReminderNotes.aspx.cs" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReminderNotes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        
    
	function popup()
	{
	    this.ModalPopupExtender1.hide();
	    return false;
		
  	}
	function ValidateControls()
	{
	     //Fahad 5296 12/12/2008  the most outer if included for FTACall
	    if(document.getElementById("hfCallType").value=="FTACall")
	    {
	        if(document.getElementById("td_comments").style.display=='block')
	        {
	            if(document.Form1.txt_Notes.value.length==0)
	            {
	                alert('Please enter genaral comments');
	                document.Form1.txt_Notes.focus();
	                return false;	
	            }
	        }
    	    
	        if(document.getElementById("ddl_rStatus").value=="4")
            {
                document.getElementById("hdnFlag").value="1";          
            }                             
            else
            { 
                document.getElementById("hdnFlag").value="0";
              
            }      
    	    
	    }
	    else
	    {
	            var cmts =document.getElementById("txt_RComments").value;
                var dateLenght = 0;
	            var newLenght = document.getElementById("txt_RComments").value.length
        	
	            // Added Zahoor 3977
	            if(document.getElementById("td_comments").style.display=='block')
	            {
	                if(document.Form1.txt_Notes.value.length==0)
	                {
	                    alert('Please enter genaral comments');
	                    document.Form1.txt_Notes.focus();
	                    return false;	
	                }
	            }
	            // end 3977
            	
            	
	            if(document.getElementById("txt_RComments").style.display=='block')
	            {
	                if(document.getElementById("txt_RComments").value.length == 0)
	                {
	                    alert('Please enter contact comments');
	                    // added zahoor: for setting focus on control
	                    document.getElementById("txt_RComments").focus(); 
	                    return false;	
	                }
	            }
        	
        	    
	            if(document.getElementById("ddl_rStatus").value=="4")
                {
                    document.getElementById("hdnFlag").value="1";          
                }                             
                else
                { 
                    document.getElementById("hdnFlag").value="0";
                  
                }      
               
                if (cmts.length > 5000)
		        {
		            event.returnValue=false;
                    event.cancel = true;
		        }
                  
	            //Comments Textboxes
        		  
                if(newLenght > 0)
                    dateLenght =  27
                else
                    dateLenght = 0
                  
                  
		        if (document.getElementById("txt_RComments").value.length + document.getElementById("lblComment").innerText.length > 5000 - dateLenght)//-27 bcoz to show last time partconcatenated with comments
		        {
		            alert("Sorry You cannot type in more than 5000 characters in contact comments")
			        return false;		  
		        }
    		  
	    }
}	

    function MaxLength()
    {
        if(document.getElementById("txt_RComments").value.length >500)
	    {
	        alert("The length of character is not in the specified Range");
	        document.getElementById("txt_RComments").focus();
			
		}
				
	}
		
	function ValidateLenght()
	{ 
	    var cmts = document.getElementById("txt_RComments").value;
		if (cmts.length > 500)
		{
		    event.returnValue=false;
               event.cancel = true;
		}
	}

    </script>

    
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div class="page-container row-fluid container-fluid">
              <section id="main-content" style="margin-left:0px !important;">
                <section class="wrapper main-wrapper row" style=''>
                     <div class='col-xs-12'>
                         <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="form-label"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="hfCallType" runat="server" />
                     </div>

                    <div class='col-xs-12'>
                        <div class="col-xs-4">
                            <asp:Label ID="Label1" runat="server" CssClass="form-label">Name:</asp:Label>                            
                        </div>
                        <div class="col-xs-8">
                             <asp:Label ID="lbl_Name" runat="server" CssClass="form-label"></asp:Label>
                            <asp:Label ID="lbl_rid" runat="server" Visible="False" CssClass="form-label"></asp:Label>
                        </div>                       
                    </div>
                    <div class='col-xs-12'>
                        <div class="col-xs-4">
                            <asp:Label ID="Label2" runat="server" CssClass="form-label">Case Status:</asp:Label>                            
                        </div>
                        <div class="col-xs-8">
                             <asp:Label ID="lbl_Status" runat="server" CssClass="form-label"></asp:Label>                           
                        </div>                       
                    </div>
                     <div class='col-xs-12'>
                        <div class="col-xs-4">
                            <asp:Label ID="Label3" runat="server" CssClass="form-label">Call Back:</asp:Label>                            
                        </div>
                        <div class="col-xs-8">
                              <asp:DropDownList ID="ddl_rStatus" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddl_rStatus_SelectedIndexChanged">
                                </asp:DropDownList>                           
                        </div>                       
                    </div>

                    <div class='col-xs-12'>
                        <div class="col-xs-4">
                            <asp:Label ID="lbl_contacttext" runat="server" CssClass="form-label">Contact Number:</asp:Label>                          
                        </div>
                        <div class="col-xs-8">
                              <asp:Label ID="lbl_contact1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'>
                                </asp:Label><br />
                                <asp:Label ID="lbl_Contact2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'>
                                </asp:Label><br />
                                <asp:Label ID="lbl_Contact3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'>
                                </asp:Label>                          
                        </div>                       
                    </div>


                    <div class='col-xs-12'>
                        <div class="col-xs-4">
                            <asp:Label ID="lbllanguage" runat="server" Text="Language:" CssClass="form-label"></asp:Label>                            
                        </div>
                        <div class="col-xs-8">
                             <asp:Label ID="lbl_Language" runat="server" CssClass="form-label"></asp:Label>                           
                        </div>                       
                    </div>

                     <div class='col-xs-12' id="td_comments" runat="server">
                        <div class="col-xs-4">
                             <asp:Label ID="lblgen_comments" runat="server" CssClass="form-label" Text="Gen. Comments:" Width="113px"></asp:Label>                            
                        </div>
                        <div class="col-xs-8"  runat="server" id="divNotes">
                             <asp:Label ID="lblNotes" runat="server"  CssClass="from-label"></asp:Label> 
                            <br />                                             
                            <asp:TextBox ID="txt_Notes" runat="server" Width="325px" TextMode="MultiLine" Height="50px"
                                MaxLength="5000" CssClass="form-control"></asp:TextBox>                         
                        </div>                       
                    </div>
                    <div class='col-xs-12' id="tr_Ccomments" runat="server">
                        <div class="col-xs-4">
                            <asp:Label ID="lblcomments" runat="server" CssClass="form-label" Text="Contact Comments:"></asp:Label>                            
                        </div>
                        <div class="col-xs-8" runat="server" id="div1">
                            <asp:Label ID="lblComment" runat="server" Width="306px" CssClass="label"></asp:Label>    
                            <br />
                                <asp:TextBox ID="txt_RComments" runat="server" Width="325px" TextMode="MultiLine"
                                    Height="50px" MaxLength="5000" CssClass="form-control"></asp:TextBox>                      
                        </div>                       
                    </div>

                     <div class='col-xs-12'>
                        <div class="col-xs-4">
                           <asp:Label ID="lbl_outsidefirm" runat="server" CssClass="form-label"  Visible="False">Outside attorney</asp:Label>                           
                        </div>
                        <div class="col-xs-8">
                              <asp:Label ID="lbl_Firm" runat="server" CssClass="form-label" Visible="False" Font-Bold="true"></asp:Label>                          
                        </div>                       
                    </div>

                    <div class='col-xs-12'>
                        <div class="col-xs-4">
                           <asp:Label ID="lbl_Bond" runat="server" Visible="False" CssClass="form-label" Font-Bold="true">Bond</asp:Label>                         
                        </div>
                        <div class="col-xs-8">
                              <asp:Literal ID="litScript" runat="server"></asp:Literal>
                                <asp:Button ID="btn_update" runat="server" CssClass="btn btn-primary" Text="Update" OnClientClick="return ValidateControls();"
                                    OnClick="btn_update_Click"></asp:Button>   
                            
                            <input id="hdnFlag" type="hidden" value="0" runat="server" />   
                            <input id="hdnStatus" type="hidden" runat="server" />
                    <input id="hdnStatusName" type="hidden" runat="server" />                   
                        </div>                       
                    </div>
                    </section>
                  </section>
        </div>
    <div style="left: 0px; width: 0px; position: absolute; top: 0px; height: 0px">
       
        <aspnew:ScriptManager ID="scriptmanager1" runat="server">
        </aspnew:ScriptManager>
        <table>
            <%--<tr>
                    <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34px"
                        style="width: 353px">
                    </td>
                </tr>--%>
            <tr>
                <td>
                    <asp:Panel ID="panel1" runat="server" BackColor="#eff4fb">
                        <table id="tablepopup" style="border-top: black thin solid; border-left: black thin solid;
                            border-bottom: black thin solid; border-right: black thin solid" cellpadding="0"
                            cellspacing="0">
                            <tr>
                                <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                                    style="width: 353px">
                                    <table style="width: 407px" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td class="clssubhead" align="left" style="height: 16px">
                                                    &nbsp;Confirmation Box
                                                </td>
                                                <td align="right" style="height: 16px">
                                                    &nbsp;<asp:LinkButton ID="lnkbtncancelpopup" runat="server">X</asp:LinkButton>&nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr bgcolor="#eff4fb">
                                <td align="center">
                                    <table width="353px" cellspacing="1" cellpadding="0">
                                        <tr>
                                            <td>
                                                &nbsp;<img src="/images/QuestionIcon.png" border="0" width="50" height="50" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblmessageshow" runat="server" BackColor="#eff4fb" ForeColor="#3366CC"
                                                    Text="Are you sure you want to activate this Flag and send a letter to the client requesting for updated contact information?"
                                                    Width="353px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnok" runat="server" Text="Yes" Style="background: url(../Images/btnOk.ico) no-repeat 2px 0px;"
                                                    CssClass="clsbutton" Height="27px" Width="91px" OnClick="btnok_Click" />&nbsp;
                                                <asp:Button ID="btncancel" runat="server" Text="No" Style="background: url(../Images/btnClose.gif) no-repeat 2px 0px;"
                                                    CssClass="clsbutton" Height="27px" Width="91px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td bgcolor="#eff4fb">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <asp:Button ID="Button1" runat="server" Text="Cancel" Style="display: none;" />
        <AjaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
            PopupControlID="tablepopup" CancelControlID="btncancel" BackgroundCssClass="modalBackground">
        </AjaxToolkit:ModalPopupExtender>
    </div>
    </form>
    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 
</body>
</html>

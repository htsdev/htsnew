﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using System.IO;
using HTP.Components;

namespace HTP.DocumentTracking
{
    public partial class UpdateBatch : System.Web.UI.Page
    {
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        PendingClass NewPendInsert = new PendingClass();
        PendingClass Verified = new PendingClass();
        clsSession clsSessn = new clsSession();
        clsDocumentTracking clsDT = new clsDocumentTracking();

        string PicName = "";
        string path = "";
        string dpath = "";
        int pageno = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (clsSessn.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["sMenu"] != "" & Request.QueryString["sMenu"] != null)
                    {
                        ViewState["vSMenuID"] = Request.QueryString["sMenu"].ToString();
                    }
                    if (Request.QueryString["SBID"] != "" & Request.QueryString["SBID"] != null)
                    {
                        ViewState["SBID"] = Request.QueryString["SBID"].ToString();
                        hlk_Back.NavigateUrl = "~/DocumentTracking/SearchBatch.aspx?sMenu=" + ViewState["vSMenuID"];
                        ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString();//get path from web config..                        
                        BindData();
                    }
                }
            }

        }

        protected void ddlImgsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Width = 0;
            int Height = 0;
            int FixWidth = 798;
            int FixHeight = 840;
            int ddlsize = 0;
            try
            {

                ddlsize = Convert.ToInt32(ddlImgsize.SelectedValue);
                ddlsize = ddlsize - 150;

                Width = FixWidth / 100 * ddlsize;
                Width = Width + FixWidth;

                Height = FixHeight / 100 * ddlsize;
                Height = Height + FixHeight;

                img_docs.Width = Width;
                img_docs.Height = Height;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        protected void ImgMoveFirst_Click(object sender, ImageClickEventArgs e)
        {
            lbl_Message.Text = "";

            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet DS_MoveFirst = clsDT.GetUpdateBatchDetail();

                if (DS_MoveFirst.Tables[0].Rows.Count > 0)
                {
                    ViewState["PicName"] = DS_MoveFirst.Tables[0].Rows[0]["PicName"].ToString();
                    ViewState["SBDID"] = DS_MoveFirst.Tables[0].Rows[0]["SBDID"].ToString();

                    dpath = ViewState["vNTPATHScanImage"].ToString() + DS_MoveFirst.Tables[0].Rows[0]["PicName"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                    }
                    lblpageno.Text = "1";
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;

            }
        }

        protected void ImgMoveLast_Click(object sender, ImageClickEventArgs e)
        {
            lbl_Message.Text = "";

            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet DS_MoveLast = clsDT.GetUpdateBatchDetail();

                if (DS_MoveLast.Tables[0].Rows.Count > 0)
                {
                    int cnt = Convert.ToInt32(DS_MoveLast.Tables[0].Rows.Count);
                    ViewState["PicName"] = DS_MoveLast.Tables[0].Rows[cnt - 1]["PicName"].ToString();
                    ViewState["SBDID"] = DS_MoveLast.Tables[0].Rows[0]["SBDID"].ToString();

                    dpath = ViewState["vNTPATHScanImage"].ToString() + DS_MoveLast.Tables[0].Rows[cnt - 1]["PicName"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                    }
                    lblpageno.Text = cnt.ToString();
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;

            }
        }

        protected void ImgMovePrev_Click(object sender, ImageClickEventArgs e)
        {
            lbl_Message.Text = "";

            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet DS_MovePrevious = clsDT.GetUpdateBatchDetail();

                if (DS_MovePrevious.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MovePrevious.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MovePrevious.Tables[0].Rows[i]["PicName"].ToString();

                        if (pid == ViewState["PicName"].ToString())
                        {
                            ViewState["PicName"] = DS_MovePrevious.Tables[0].Rows[i - 1]["PicName"].ToString();
                            ViewState["SBDID"] = DS_MovePrevious.Tables[0].Rows[0]["SBDID"].ToString();

                            dpath = ViewState["vNTPATHScanImage"].ToString() + DS_MovePrevious.Tables[0].Rows[i - 1]["PicName"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno - 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                            }
                            break;
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;

            }
        }

        protected void ImgMoveNext_Click(object sender, ImageClickEventArgs e)
        {
            lbl_Message.Text = "";

            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet DS_MoveNext = clsDT.GetUpdateBatchDetail();

                if (DS_MoveNext.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MoveNext.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MoveNext.Tables[0].Rows[i]["PicName"].ToString();
                        if (ViewState["PicName"] == null)
                        {
                            ViewState["PicName"] = DS_MoveNext.Tables[0].Rows[i + 1]["PicName"].ToString();
                            ViewState["SBDID"] = DS_MoveNext.Tables[0].Rows[0]["SBDID"].ToString();

                            dpath = ViewState["vNTPATHScanImage"].ToString() + DS_MoveNext.Tables[0].Rows[i + 1]["PicName"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno + 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                            }
                            break;

                        }
                        else
                        {
                            if (pid == ViewState["PicName"].ToString())
                            {
                                ViewState["PicName"] = DS_MoveNext.Tables[0].Rows[i + 1]["PicName"].ToString();
                                ViewState["SBDID"] = DS_MoveNext.Tables[0].Rows[0]["SBDID"].ToString();

                                dpath = ViewState["vNTPATHScanImage"].ToString() + DS_MoveNext.Tables[0].Rows[i + 1]["PicName"].ToString() + ".jpg";
                                pageno = Convert.ToInt32(lblpageno.Text);
                                pageno = pageno + 1;
                                lblpageno.Text = pageno.ToString();
                                if (!File.Exists(dpath))
                                {
                                    img_docs.ImageUrl = "~/Images/not-available.gif";
                                }
                                else
                                {
                                    img_docs.ImageUrl = dpath;
                                }
                                break;
                            }
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                clsDT.ScanBatchDetailID = Convert.ToInt32(ViewState["SBDID"]);
                clsDT.DocumentBatchID = Convert.ToInt32(txt_LetterID.Text.Trim());
                clsDT.DocumentBatchPageCount = Convert.ToInt32(txt_PageCount.Text.Trim());
                clsDT.DocumentBatchPageNo = Convert.ToInt32(txt_PageNo.Text.Trim());
                clsDT.EmpID = Convert.ToInt32(clsSessn.GetCookie("sEmpID", this.Request));

                clsDT.UpdateUpdateBatch();

                string searchpath = clsDT.ScanBatchID.ToString() + "-" + clsDT.ScanBatchDetailID.ToString() + ".jpg";
                string[] filename = Directory.GetFiles(ViewState["vNTPATHScanImage"].ToString(), searchpath);//get scann images
                
                if (filename.Length >= 1)
                {
                    for (int i = 0; i < filename.Length; i++)
                    {
                        string picName = clsDT.ScanBatchID.ToString() + "-" + clsDT.ScanBatchDetailID.ToString() + "-" + clsDT.DocumentBatchID.ToString();
                        string picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg";
                        File.Copy(filename[i].ToString(), picDestination);//save images with batchid and picid like 1-1.jpg.
                        File.Delete(filename[i].ToString());//delete from intial position.
                    }
                }
                ResetControls();
                BindData();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        protected void img_flip_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string fPath = ViewState["vNTPATHScanImage"].ToString() + ViewState["PicName"].ToString() + ".jpg";
                string dpath = fPath;
                fPath = fPath.Replace("\\\\", "\\");
                System.Drawing.Image img;
                img = System.Drawing.Image.FromFile(fPath);

                img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipXY);

                if (File.Exists(dpath))
                {
                    File.Delete(dpath);
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                img_docs.ImageUrl = fPath;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        #endregion

        #region Methods

        private void ResetControls()
        {
            txt_LetterID.Text = "";
            txt_PageCount.Text = "";
            txt_PageNo.Text = "";
        }

        private void BindData()
        {
            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet ds_UpdateBatch = clsDT.GetUpdateBatch();

                if (ds_UpdateBatch.Tables[0].Rows.Count > 0)
                {
                    lbl_BatchID.Text = ds_UpdateBatch.Tables[0].Rows[0]["BatchID"].ToString();
                    lbl_PageCount.Text = ds_UpdateBatch.Tables[0].Rows[0]["PageCount"].ToString();
                    lbl_ScanDate.Text = ds_UpdateBatch.Tables[0].Rows[0]["ScanDate"].ToString();
                    lbl_NoLetterID.Text = ds_UpdateBatch.Tables[0].Rows[0]["NoLetterID"].ToString();
                    if (lbl_NoLetterID.Text == "0")
                    {
                        Response.Redirect("SearchBatch.aspx?sMenu=" + ViewState["vSMenuID"],false);
                    }
                    else
                    {
                        DisplayImage();
                        btnUpdate.Enabled = true;
                    }
                }
                else
                {
                    lbl_Message.Text = "No record found! Please go back.";
                    lbl_Message.Visible = true;
                    btnUpdate.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        private void DisplayImage()
        {
            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet DS_Display = clsDT.GetUpdateBatchDetail();
                if (DS_Display.Tables[0].Rows.Count > 0)
                {
                    PicName = DS_Display.Tables[0].Rows[0]["PicName"].ToString();
                    path = ViewState["vNTPATHScanImage"].ToString() + PicName + ".jpg";
                    ViewState["PicName"] = PicName;
                    ViewState["SBDID"] = DS_Display.Tables[0].Rows[0]["SBDID"].ToString();
                    if (!File.Exists(path))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = path;
                    }
                    lblpageno.Text = "1";
                    lblCount.Text = DS_Display.Tables[0].Rows.Count.ToString();
                    DisabledImageButtons();
                }
                else
                {
                    lblpageno.Text = "0";
                    lblCount.Text = "0";
                    DisabledImageButtons();
                    img_docs.Visible = false;
                    ddlImgsize.Enabled = false;
                    img_flip.Enabled = false;
                }


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }

        private void DisabledImageButtons()
        {
            if (lblpageno.Text == lblCount.Text)
            {
                ImgMoveLast.Enabled = false;
                ImgMoveNext.Enabled = false;
            }
            else
            {
                ImgMoveLast.Enabled = true;
                ImgMoveNext.Enabled = true;
            }
            if (lblpageno.Text == "1")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else if (lblpageno.Text == "0")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else
            {
                ImgMoveFirst.Enabled = true;
                ImgMovePrev.Enabled = true;
            }
        }

        #endregion
    }
}

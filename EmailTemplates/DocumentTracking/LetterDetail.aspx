<%@ Page Language="C#" AutoEventWireup="true" Codebehind="LetterDetail.aspx.cs" Inherits="HTP.DocumentTracking.LetterDetail" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Datail</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="400" align="center"
                border="0">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                    &nbsp;Document Detail</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="clsLeftPaddingTable">
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" class="clsLeftPaddingTable">
                        <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td width="100%">
                                    <asp:GridView ID="gv_LetterDetail" runat="server" Width="100%" AutoGenerateColumns="False"
                                        AllowPaging="True" PageSize="15" OnPageIndexChanging="gv_Scan_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Document ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_LetterID" runat="server" Text='<%# bind("LetterID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Pages">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PageCount" CssClass="label" runat="server" Text='<%# bind("PageCount") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Page No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PageNo" CssClass="label" runat="server" Text='<%# bind("PageNo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/separator_repeat.gif" style="height: 11px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

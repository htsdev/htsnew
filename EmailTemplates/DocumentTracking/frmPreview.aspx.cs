using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace HTP.DocumentTracking
{
    /// <summary>
    /// Preview Scanned Documents
    /// </summary>
    public partial class frmPreview : System.Web.UI.Page
    {
        #region Variables

        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger cLog = new clsLogger();

        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Put user code to initialize the page here
                int documentBatchID = Convert.ToInt32(Request.QueryString["DBID"].Trim());
                int scanBatchID = Convert.ToInt32(Request.QueryString["SBID"].Trim());
                ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString();
                //To get the address of file
                ShowPDFN(documentBatchID, scanBatchID);
            }
            catch (Exception ex)
            {
                cLog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        #endregion

        #region Methods

        private void ShowPDFN(int DBID, int SBID)
        {
            Document objdocument = new Document();
            DataSet ds;
            DataTable dt;
            string strFile;
            string strFileName = Server.MapPath("../Temp/");
            strFile = Session.SessionID + SBID.ToString() + DBID.ToString() + ".pdf";
            strFileName += strFile;
            PdfWriter.getInstance(objdocument, new FileStream(strFileName, FileMode.Create));
            objdocument.Open();

            string[] key = { "@ScanBatchID", "@DocumentBatchID" };
            object[] value1 = { SBID, DBID };

            ds = ClsDb.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_ScannedImages", key, value1);
            dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                objdocument.setMargins(1, 0, 0, 0);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string fn;

                    fn = dt.Rows[i]["SBID"].ToString() + "-" + dt.Rows[i]["SBDID"].ToString() + "-" + dt.Rows[i]["DBID"].ToString() + ".jpg";
                    fn = ViewState["vNTPATHScanImage"].ToString() + fn;

                    try
                    {
                        iTextSharp.text.Image pic = iTextSharp.text.Image.getInstance(fn);
                        pic.scaleToFit(PageSize.A4.Width - 50, PageSize.A4.Height - 50);
                        objdocument.Add(pic);
                    }
                    catch
                    {
                    }
                }
                objdocument.Close();
            }
            Session["sDTFilePath"] = strFileName;
            Response.Redirect("../Temp/" + strFile, false);
        }

        #endregion
    }
}

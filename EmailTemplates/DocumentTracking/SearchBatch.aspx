<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchBatch.aspx.cs" Inherits="HTP.DocumentTracking.SearchBatch"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Batch</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function DeleteConfirm()
    {
        var isDelete=confirm("Are you sure you want to delete this batch?");
        if(isDelete)
        {
            //Fahad 5167/5168 02/20/2009 return true added
            return true;
           
        }
        
        else
        {
            return false;
        }
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table id="TableMain" cellspacing="0" cellpadding="0" width="880" align="center"
            border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td style="height: 9px" background="../images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable" colspan="2" width="100%">
                    <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td class="clsLeftPaddingTable" width="25%">
                                <strong><span style="font-size: 9pt">Search Scanned Batch Dates:</span></strong>
                            </td>
                            <td width="16%">
                                <ew:CalendarPopup ID="cal_StartDate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clsLeftPaddingTable" width="4%">
                                <strong>To</strong>
                            </td>
                            <td width="16%">
                                <ew:CalendarPopup ID="cal_EndDate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td width="25%">
                                <asp:CheckBox ID="chk" runat="server" CssClass="clsLeftPaddingTable" Text="Show Batches With Non Verified"
                                    Checked="True" />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 9px" background="../images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                &nbsp;Search Documents
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                                <td class="clssubhead" background="../Images/subhead_bg.gif" height="34" align="right">
                                    &nbsp;<asp:HyperLink ID="hlk_Scan" runat="server">Scan</asp:HyperLink>&nbsp;&nbsp;
                                </td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="clsLeftPaddingTable" id="tdProcess" style="display: none">
                    <asp:Label ID="lblProess" runat="server" Text="Please Wait Scanning and OCR is in Process..."
                        ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 9px" background="../images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td align="center" class="clsLeftPaddingTable">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" colspan="2" style="height: 144px">
                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                CssClass="clsLabel"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress>
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td align="center" width="100%">
                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_SearchBatch" runat="server" AutoGenerateColumns="False" Width="100%"
                                            AllowPaging="True" CssClass="clsLeftPaddingTable" AllowSorting="True" OnPageIndexChanging="gv_SearchBatch_PageIndexChanging"
                                            PageSize="20" OnSorting="gv_SearchBatch_Sorting" OnRowDataBound="gv_SearchBatch_RowDataBound">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="<u>Batch ID</u>" SortExpression="BatchID" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlk_BatchID" runat="server" Text='<%# bind("BatchID") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Images</u>" SortExpression="Count" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Images" CssClass="Label" runat="server" Text='<%# bind("Count") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Scan Date</u>" SortExpression="ScanDate" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ScanDate" CssClass="Label" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ScanDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Verified</u>" SortExpression="Verified" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Verified" CssClass="Label" runat="server" Text='<%# bind("Verified") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>No Document ID</u>" SortExpression="NoLeterID"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlk_NoLetterID" runat="server" Text='<%# bind("NoLeterID") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Missing Doc</u>" SortExpression="Missing" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle CssClass="clssubhead" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_MissingDoc" CssClass="Label" runat="server" Text='<%# bind("Missing") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_delete" runat="server" OnClientClick="return DeleteConfirm();"
                                                            ImageUrl="../Images/cross.gif" CommandArgument='<%# bind("BatchID") %>' CommandName="delete"
                                                            OnCommand="img_delete_Command" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px" background="../images/separator_repeat.gif">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td style="visibility: hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="display: none">
                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server" Text="Label"></asp:Label><asp:Label
                        ID="lbl_IsSplit" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>


using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using HTP.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;



namespace HTP.DocumentTracking
{
    public partial class BatchDetail : System.Web.UI.Page
    {
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsDocumentTracking clsDT = new clsDocumentTracking();

        DataView DV;
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;
        string PicID = String.Empty;
        //Ozair 5057 03/24/2009 Warnings Removed

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lbl_Message.Visible = false;
                    if (Request.QueryString["sMenu"] != "" & Request.QueryString["sMenu"] != null)
                    {
                        ViewState["vSMenuID"] = Request.QueryString["sMenu"].ToString();
                        hlk_Search.NavigateUrl = "~/DocumentTracking/SearchBatch.aspx?sMenu=" + ViewState["vSMenuID"];
                    }

                    if (Request.QueryString["SBID"] != "" & Request.QueryString["SBID"] != null)
                    {
                        ViewState["SBID"] = Request.QueryString["SBID"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString();//get path from web config..
                        BindGrid();
                    }

                }
            }
        }

        protected void gv_BatchDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string DBID = String.Empty;
            string SBID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lbl_BatchID = (Label)e.Row.FindControl("lbl_BatchID");
                Label lbl_Verified = (Label)e.Row.FindControl("lbl_Verified");
                Label lbl_Missing = (Label)e.Row.FindControl("lbl_Missing");
                HyperLink hlk_LetterID = (HyperLink)e.Row.FindControl("hlk_LetterID");
                HyperLink hlk_View = (HyperLink)e.Row.FindControl("hlk_View");
                HyperLink hlk_Scan = (HyperLink)e.Row.FindControl("hlk_Scan");
                LinkButton lnkbtn_verify = (LinkButton)e.Row.FindControl("lnkbtn_verify");

                SBID = lbl_BatchID.Text;
                DBID = hlk_LetterID.Text;
                //Sabir Khan 6950 11/09/2009 Allow verify document when more pages scaned then required....
                //if (lbl_Missing.Text != "0")
                if (Convert.ToInt32(lbl_Missing.Text.ToString()) > 0)
                {
                    hlk_LetterID.NavigateUrl = "#";
                    hlk_LetterID.Attributes.Add("onClick", "OpenLetterDetail(" + DBID + "," + SBID + ")");
                    hlk_View.NavigateUrl = "#";
                    hlk_View.Enabled = false;
                    hlk_Scan.NavigateUrl = "#";
                    hlk_Scan.Attributes.Add("onClick", "OpenScanMissing(" + DBID + "," + SBID + ")");
                    hlk_Scan.Enabled = true;
                }
                else
                {
                    hlk_View.NavigateUrl = "~/DocumentTracking/ViewBatchLetters.aspx?sMenu=" + ViewState["vSMenuID"] + "&DBID=" + DBID + "&SBID=" + SBID;
                    hlk_View.Enabled = true;
                    hlk_Scan.Enabled = false;
                }
                //Sabir Khan 6950 11/09/2009 Allow verify document when more pages scaned then required....
                //if (lbl_Verified.Text == "No" && lbl_Missing.Text == "0")
                if (lbl_Verified.Text == "No" && Convert.ToInt32(lbl_Missing.Text.ToString()) <= 0)
                {
                    lnkbtn_verify.Enabled = true;
                    lnkbtn_verify.CommandArgument = SBID + "," + DBID;
                }
                else
                {
                    lnkbtn_verify.Enabled = false;
                }
            }
        }

        protected void gv_BatchDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_BatchDetail.PageIndex = e.NewPageIndex;//paging
                DV = (DataView)Session["DV"];
                gv_BatchDetail.DataSource = DV;
                gv_BatchDetail.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;

            }
        }

        protected void gv_BatchDetail_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);//sorting
        }

        protected void lnkbtn_verify_Command(object sender, CommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = "";

                string[] cArg = e.CommandArgument.ToString().Split(',');
                int scanBatchID = Convert.ToInt32(cArg[0]);
                int documentBatchID = Convert.ToInt32(cArg[1]);

                clsDT.ScanBatchID = scanBatchID;
                clsDT.DocumentBatchID = documentBatchID;
                clsDT.VerifiedDate = DateTime.Now;
                clsDT.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                if (clsDT.CheckIsAlreadyVerified() == 0)
                {
                    clsDT.UpdateVerifyBatchDetail();
                }
                else
                {
                    //ozair 4308 06/27/2008 Letter ID changed to Document ID
                    lbl_Message.Text = "Document ID " + documentBatchID+" is already scanned and verified.";
                    lbl_Message.Visible = true;
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
            finally
            {
                BindGrid();
            }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
                DataSet DS_GetBatch = clsDT.GetBatchDetail();

                if (DS_GetBatch.Tables[0].Rows.Count > 0)
                {
                    gv_BatchDetail.DataSource = DS_GetBatch;
                    DV = new DataView(DS_GetBatch.Tables[0]);
                    Session["DV"] = DV;
                    gv_BatchDetail.DataBind();
                }
                else
                {
                    lbl_Message.Text = "No Record Found. The document id may not be in our system.";
                    lbl_Message.Visible = true;
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }

        }

        #region SortingLogic

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gv_BatchDetail.DataSource = DV;
                gv_BatchDetail.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        #endregion

        #endregion

    }
}

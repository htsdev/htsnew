<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanMissing.aspx.cs" Inherits="HTP.DocumentTracking.ScanMissing" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Scan Missing</title>
    <base target="_self" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function CheckValid()
        { 
            var pn=document.getElementById("txt_PageNo").value;
       
            if(pn.length==0)
           {
                alert("Please enter page no.");
                document.getElementById("txt_PageNo").focus();
                return false;
           } 
             if(isNaN(pn))
           { 
                 alert("Invalid page no, Please enter again.");
                document.getElementById("txt_PageNo").focus();
                return false;
            }
       
       		document.getElementById("tbl_plzwait1").style.display = 'block';
		    document.getElementById("tbl_plzwait1").focus();
		    return true;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </aspnew:ScriptManager>
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="400" align="center"
            border="0">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                &nbsp;Scan Missing
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="clsLeftPaddingTable">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable" colspan="2" width="100%">
                    <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0" class="clsLeftPaddingTable">
                        <tr>
                            <td class="clsLeftPaddingTable">
                                <strong>Batch ID</strong>
                            </td>
                            <td class="clsLeftPaddingTable">
                                <strong>Document ID</strong>
                            </td>
                            <td class="clsLeftPaddingTable">
                                <strong>Total Pages</strong>
                            </td>
                            <td class="clsLeftPaddingTable">
                                <strong>Page No</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbl_BatchID" runat="server" CssClass="Label"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_LetterID" runat="server" CssClass="Label"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_PageCount" runat="server" CssClass="Label"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_PageNo" runat="server" Width="64px" CssClass="clstextarea"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button ID="btn_Scan" runat="server" CssClass="clsbutton" Text="Scan" OnClick="btn_Scan_Click"
                                    Width="58px" />&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" width="400">
                    <table runat="server" id="tbl_plzwait1" style="display: none" width="400">
                        <tr>
                            <td class="clssubhead" valign="middle" align="center" width="100%">
                                <img src="../Images/plzwait.gif" />
                                Please wait while scanning is in process...
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="height: 144px">
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="visibility: hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript">
        function StartScan()
        {
            if(CheckValid())
            {
                var sid=document.getElementById("txtsessionid").value;
                var eid=document.getElementById("txtempid").value;
                var sSrv=document.getElementById("txtSrv").value;
                var type='network';
                var path = '<%=ViewState["vNTPATHScanTemp"]%>';
            
                var sel=OZTwain1.SelectScanner();
	            if(sel=="Success")
	            {
                    document.getElementById("tbl_plzwait1").style.display = "block";
		            document.getElementById("tbl_plzwait1").focus();
                    OZTwain1.Acquire(sid,eid,path,type,-1);
                }
                else if(sel=="Cancel")
                {
                    alert("Operation canceled by user!");
                    return false;
                }
                else 
                {
                    alert("Scanner not installed.");
	                return false;
                }
            }
            else
            {
                return false;
            }
        }
    </script>

    <%=Session["objTwain"].ToString()%>
</body>
</html>

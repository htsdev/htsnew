<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanBatch.aspx.cs" Inherits="HTP.DocumentTracking.ScanBatch" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Scan Batch</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function hide()
    { 
        var td=document.getElementById("tdProcess");
        td.style.display='none';
        
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </aspnew:ScriptManager>
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td style="height: 50px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                &nbsp;Scan/Search Documents
                            </td>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34" align="right">
                                <asp:HyperLink ID="hlk_Search" runat="server">Search</asp:HyperLink>&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="clsLeftPaddingTable">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable" colspan="2" width="100%">
                    <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td class="clsLeftPaddingTable" width="13%">
                                <strong>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;Scan Date:</strong>
                            </td>
                            <td width="13%">
                                &nbsp;<asp:Label ID="cal_ScanDate" runat="server" CssClass="Label"></asp:Label>
                            </td>
                            <td>
                                <asp:Button ID="btnScan" runat="server" CssClass="clsbutton" Text="Scan " OnClick="btnScan_Click"
                                    Width="58px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable" style="height: 13px">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table id="tbl_plzwait1" style="display: none" width="800">
                                <tr>
                                    <td class="clssubhead" valign="middle" align="center">
                                        <img src="../Images/plzwait.gif" />
                                        Please wait while scanning and OCR is in process..
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                        </Triggers>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="height: 144px">
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="100%">
                                <aspnew:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Scan" runat="server" Width="100%" AutoGenerateColumns="False"
                                            AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_Scan_PageIndexChanging"
                                            OnSorting="gv_Scan_Sorting" PageSize="15" OnRowDataBound="gv_Scan_RowDataBound">
                                            <AlternatingRowStyle BackColor="#EEEEEE" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Batch ID" SortExpression="BatchID">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlk_BatchID" runat="server" Text='<%# bind("BatchID") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle ForeColor="#006699" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Images" SortExpression="Count">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Images" CssClass="Label" runat="server" Text='<%# bind("Count") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle ForeColor="#006699" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Scan Date" SortExpression="ScanDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ScanDate" CssClass="Label" runat="server" Text='<%# bind("ScanDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle ForeColor="#006699" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Verified" SortExpression="Verified">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Verified" CssClass="Label" runat="server" Text='<%# bind("Verified") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No Document ID" SortExpression="NoLeterID">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlk_NoLetterID" runat="server" Text='<%# bind("NoLeterID") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Missing Doc" SortExpression="Missing">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_MissingDoc" CssClass="Label" runat="server" Text='<%# bind("Missing") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td style="visibility: hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript">
        function StartScan()
        {
            var sid=document.getElementById("txtsessionid").value;
            var eid=document.getElementById("txtempid").value;
            var sSrv=document.getElementById("txtSrv").value;
            var type='network';
            var path = '<%=ViewState["vNTPATHScanTemp"]%>';
            
            var sel=OZTwain1.SelectScanner();
	        if(sel=="Success")
	        {
               document.getElementById("tbl_plzwait1").style.display = "block";
		       document.getElementById("tbl_plzwait1").focus();
                OZTwain1.Acquire(sid,eid,path,type,-1);
            }
            else if(sel=="Cancel")
            {
                alert("Operation canceled by user!");
                return false;
            }
            else 
            {
                alert("Scanner not installed.");
	            return false;
            }
        }
    </script>

    <%=Session["objTwain"].ToString()%>
</body>
</html>

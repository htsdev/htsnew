using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.WebScanII
{
    public partial class popup : System.Web.UI.Page
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblBatch.Text = Request.QueryString["batchid"].ToString();
            lblTotal.Text = Request.QueryString["total"].ToString();
            lblstime.Text = Request.QueryString["starttime"].ToString();
            string[] key ={ "@Batchid","@sentdate" };
            object[] value1 ={ Convert.ToInt32(Request.QueryString["batchid"]), Convert.ToDateTime(Request.QueryString["starttime"]) };
            DataSet ds = clsDb.Get_DS_BySPArr("usp_webscan_get_msmqlog_NewII", key, value1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblProcessed.Text = ds.Tables[0].Rows[0]["processed"].ToString();
                lblNoCause.Text = ds.Tables[0].Rows[0]["NoCause"].ToString();
                lblQuoteClient.Text = ds.Tables[0].Rows[0]["QuoteClient"].ToString();
                gv_Scan.DataSource = ds;
                gv_Scan.DataBind();
            }
        }
    }
}

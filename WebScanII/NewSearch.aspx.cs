using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;
using System.Messaging;

namespace HTP.WebScanII
{
    public partial class NewSearch : System.Web.UI.Page
    {
        #region Variables
        
        Batch NewBatch = new Batch();
        clsLogger BugTracker = new clsLogger();
        DataView DV;
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;
        string PicID = String.Empty;
        string path = "";

        #endregion 

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    
                    lblMessage.Visible = false;
                    ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();
                    BindGrid();

                }
            }
        }
        
         protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchResult(); 
        }

        protected void gv_Scan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";
           
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();
                if (((HyperLink)e.Row.FindControl("verified")).Text == "0")
                { 
                    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                    ((ImageButton)e.Row.FindControl("img_delete")).Enabled = true;
                }
                else
                { 
                    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "NewVerified.aspx?batchid=" + batchid;
                    ((ImageButton)e.Row.FindControl("img_delete")).Enabled = false;
                }
                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { 
                    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                    ((LinkButton)e.Row.FindControl("lnkbtn_verify")).Enabled = false;
                }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("discrepancy")).Text == "0")
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = "NewDiscrepancy.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("pending")).Text == "0")
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = "NewPending.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                //if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{

                //    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "";
                //}
                
                ((ImageButton)e.Row.FindControl("img_delete")).Attributes.Add("onclick","return DeleteConfirm();");
            }
        }

        protected void gv_Scan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Scan.PageIndex = e.NewPageIndex;//paging
                DV = (DataView)Session["DV"];
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;

            }
        }
         
        protected void gv_Scan_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);//sorting
        }
        
        protected void delete(object sender, CommandEventArgs e)
        {
            try
            {
                int BatchID = Convert.ToInt32(e.CommandArgument.ToString());

                lblMessage.Text = "";
                string[] key ={ "@BatchID" };
                object[] value ={ BatchID };
                
                DataSet DS_DeletePics = clsDb.Get_DS_BySPArr("Usp_WebScan_DeletePics",key,value);

                if (DS_DeletePics.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_DeletePics.Tables[0].Rows.Count; i++)
                    {
                        PicID = DS_DeletePics.Tables[0].Rows[i]["picid"].ToString();
                        path = ViewState["vNTPATHScanImage"].ToString() + BatchID + "-" + PicID + ".jpg";
                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }

                    }
                    string[] key1 ={ "@BatchID"};
                    object[] value1 ={ BatchID };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteBatch",key1,value1);
                    
                        lblMessage.Text = "Batch Deleted Successfuly";
                        lblMessage.Visible = true;
                    

                }


                BindGrid();

                


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void lnkbtn_verify_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int BatchID = Convert.ToInt32(e.CommandArgument.ToString());

                lblMessage.Text = "";
                string[] keyn ={ "@BatchID", "@EmpID" };
                object[] valuen ={ BatchID, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)) };

                DataSet dsn=clsDb.Get_DS_BySPArr ("Usp_WebScan_autoverify_new", keyn, valuen);
                if (dsn.Tables.Count > 0)
                {
                    if (dsn.Tables[0].Rows.Count > 0)
                    {
                        MessageQueue MyMessageQ;
                        Message MyMessage;

                        try
                        {
                            MyMessageQ = new MessageQueue(ConfigurationSettings.AppSettings["QueuePath"].ToString());
                            MyMessage = new Message(dsn.Tables[0]);
                            DateTime dt = DateTime.Now;
                            MyMessageQ.Send(MyMessage);
                            //lblMessage.Text = "Batch verified and sent to queue for further process.";
                            HttpContext.Current.Response.Write("<script language=javascript>window.open('popup.aspx?batchid=" + BatchID.ToString() + "&total=" + dsn.Tables[0].Rows.Count.ToString() + "&starttime="+dt.ToString()+"','','height=240, width=650,scrollbars=yes');</script>");
                        }

                        catch (Exception ex)
                        {
                            //lblMessage.Text = "Batch verified but not sent to queuefor further process.";
                            BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                    }
                }

                BindGrid();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        
        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                DataSet DS_GetBatch = NewBatch.GetBatchSearchII();//Display Records of current date when page loaded.
               
                if (DS_GetBatch.Tables[0].Rows.Count > 0)
                {
                    gv_Scan.DataSource = DS_GetBatch;
                    DV = new DataView(DS_GetBatch.Tables[0]);
                    Session["DV"] = DV;
                    gv_Scan.DataBind();
                }
                else
                {
                    lblMessage.Text = "No Record Found";

                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }
        
        private void SearchResult()
        {
            lblMessage.Visible = false;
            gv_Scan.Visible = true;
            int checkstatus = 0;
            if (chk.Checked == true)
            { checkstatus = 1; }
            try
            {
                if (fromdate.SelectedDate.ToShortDateString() != "1/1/0001" && enddate.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    string[] key ={ "@StarDate", "@EndDate","@checkStatus" };
                    //khalid bug 3113 2/19/08 to fix varchar to datetime conversion bug
                    object[] value1 ={ fromdate.SelectedDate, enddate.SelectedDate,checkstatus };
                    DataSet Ds_Result = clsDb.Get_DS_BySPArr("usp_WebScan_NewGetSearchII", key, value1);
                    if (Ds_Result.Tables[0].Rows.Count > 0)
                    {


                        gv_Scan.DataSource = Ds_Result;
                        DV = new DataView(Ds_Result.Tables[0]);
                        Session["DV"] = DV;
                        gv_Scan.DataBind();

                    }
                    else
                    {
                        gv_Scan.Visible = false;

                        lblMessage.Visible = true;
                        lblMessage.Text = "No Record Found";

                    }
                }

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        } 

        #region SortingLogic
        
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }
        #endregion 

        #endregion                
    }
}

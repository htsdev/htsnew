using System;
using System.Collections;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTextSharp.text; 
using iTextSharp.text.pdf; 
using ICSharpCode; 
using System.IO;
using FrameWorkEnation.Components;
using BaxterSoft.Graphics;
using System.Configuration;
//Nasir 5437 01/26/2009 use this for DeleteRootTempFile method in class clsGeneralMethods
using lntechNew.Components;
//Nasir 5437 01/26/2009 to log exception
using lntechNew.Components.ClientInfo;

namespace lntechNew.WebScanII
{
	/// <summary>
	/// Summary description for PreviewPDF.
	/// </summary>
	/// 
	

	public partial class PreviewPDF : System.Web.UI.Page
	{
		//clsWeb objDS = new clsWeb(clsWeb.enmDBServer.TrafficDoc); 
		clsENationWebComponents ClsDb = new clsENationWebComponents();
        
        
		//clsDB.structResult structResult; 
        
		string strFileName;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!(Page.IsPostBack)) 
			{
                ShowPDFN(@""+Session["sBSDApath"].ToString()); 	
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion



        private void ShowPDFN(string path)
        {
            try
            {
                Document objdocument = new Document();
                //Nasir 5437 01/26/2009 use this class for DeleteRootTempFile
                clsGeneralMethods GM = new clsGeneralMethods();
                string strFile;
                strFileName = Server.MapPath("../Temp/");
                strFile = Session.SessionID + DateTime.Now.ToFileTime() + ".pdf";
                strFileName += strFile;
                PdfWriter.getInstance(objdocument, new FileStream(strFileName, FileMode.Create));
                objdocument.Open();


                if (File.Exists(path))
                {

                    objdocument.setMargins(1, 0, 0, 0);
                    string fn;
                    
                    fn = path;

                   
                    try
                    {
                        
                        iTextSharp.text.Image pic = iTextSharp.text.Image.getInstance(fn);

                        //iTextSharp.text.Image jpeg = iTextSharp.text.Image.getInstance((byte[]) ds.GetValue(1));
                        pic.scaleToFit(PageSize.A4.Width - 50, PageSize.A4.Height - 50);
                        
                        //pic.scalePercent(36, 32); //for 200 dpi
                        //pic.scalePercent(21, 25); //for 300 dpi

                        objdocument.Add(pic);
                        //					Bread.Close();	`
                    }
                    catch
                    {

                    }

                   
                }
                //Nasir 5437 02/02/2009 take out from if condition to close document
                 objdocument.Close();
                //
                //Nasir 5437 02/02/2009 to handle Exeception thread aborted
                Response.Redirect("../Temp/" + strFile,false);
                
                //Nasir 5437 01/26/2009 removing the files from upload folder which are left behind
                GM.DeleteTempFiles(Server.MapPath("../Temp/"), "*.pdf");
                
            }
            //Nasir 5437 01/26/2009 to log exception
            catch(Exception ex)
            {
               clsLogger.ErrorLog(ex);
            }
        }

        

	}
}

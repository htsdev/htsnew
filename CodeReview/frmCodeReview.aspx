<%@ Page Language="C#" AutoEventWireup="true" Codebehind="frmCodeReview.aspx.cs" Inherits="lntechNew.frmCodeReview" %>

<%@ Register Src="Controls/UCAddComments.ascx" TagName="UCAddComments" TagPrefix="uc2" %>
<%@ Register Src="Controls/UCaddClass.ascx" TagName="UCaddClass" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <table width="780" align="center">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td style="height: 32px">
                                &nbsp;Code Review Comments Page</td>
                            <td style="width: 100px; height: 32px">
                                <asp:HyperLink ID="hl_AddClass" runat="server">Add Class</asp:HyperLink>
                                <asp:HyperLink ID="HyperLink1" runat="server">Add Comments</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 21px" id="AddComments">
                    &nbsp;<aspnew:ScriptManager ID="ScriptManager1" runat="server">
                    </aspnew:ScriptManager>
                    <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc2:UCAddComments id="UCAddComments1" runat="server">
                            </uc2:UCAddComments>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 100px">
                                     
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" id="addClass">
                                        <uc3:UCaddClass id="UCaddClass1" runat="server">
                                        </uc3:UCaddClass></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                    <div style="text-align: center">
                        <br />
                        &nbsp;</div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;<table>
                        <tr>
                            <td style="width: 100px">
                                &nbsp;<ew:CalendarPopup ID="Calendarpopup2" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="-130"
                                    Enabled="False" EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                    ShowGoToToday="True" ToolTip="Payment Process Date" UpperBoundDate="12/31/9999 23:59:00"
                                    Width="90px">
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 100px">
                                <ew:CalendarPopup ID="Calendarpopup1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="-130" Enabled="False"
                                    EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="images/calendar.gif"
                                    Nullable="True" PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Payment Process Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 100px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="clsrlist">
                                </asp:DropDownList></td>
                            <td style="width: 100px">
                                <asp:Button ID="Button3" runat="server" CssClass="clsbutton" Text="Search" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;<asp:GridView ID="GridView1" runat="server">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="CaseSummaryNew.aspx.cs"
    Inherits="lntechNew.ClientInfo.CaseSummaryNew" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Summary Crystal Report Version</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tbody>
                <tr>
                    <td bgcolor="white" colspan="5">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#eff4fb">
                        <table id="TableNote" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td align="center" bgcolor="#eff4fb" style="height: 6px">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    </td>
                                    <td style="height: 6px">
                                        &nbsp;<br />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td class="clsleftpaddingtable" style="height: 90px">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%" class="clsleftpaddingtable">
                                            <tr>
                                                <td align="center">
                                                    <asp:CheckBox ID="Contact" runat="server" Font-Bold="True" Text="Contact" CssClass="clsleftpaddingtable" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Matter" runat="server" Font-Bold="True" Text="Matter" CssClass="clsleftpaddingtable" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Flags" runat="server" Font-Bold="True" Text="Flags" CssClass="clsleftpaddingtable" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Comments" runat="server" Font-Bold="True" Text="Comments" CssClass="clsleftpaddingtable" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="History" runat="server" Font-Bold="True" Text="History" Width="83px"
                                                        CssClass="clsleftpaddingtable" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Billing" runat="server" Font-Bold="True" Text="Billing" Width="83px"
                                                        CssClass="clsleftpaddingtable" /></td>
                                                <td align="center">
                                                    <asp:ImageButton ID="Button_CR" runat="server" OnClick="Button_CR_Click" ImageUrl="~/Images/Cr.jpg"
                                                        Height="43px" Width="100px" CssClass="clsleftpaddingtable" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" style="height: 2px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <td>
                                </td>
                    <tr>
                    </tr>
            </tbody>
        </table>
    </form>
</body>
</html>

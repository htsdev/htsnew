﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="HTP.PaperLess.Documents" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register Assembly="Microsoft.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/WebControls/faxcontrol.ascx" TagName="Faxcontrol" TagPrefix="Fc" %>
<%@ Register Src="../WebControls/ReadNotes.ascx" TagName="ReadNotes" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Documents</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        //ozair 3643 on 05/12/2008 method to show verified scanned document from document tracking system
        function PopUpShowPreviewDT(DBID,SBID)
        {
            window.open ("../documenttracking/PreviewMain.aspx?DBID="+ DBID +"&SBID="+ SBID);
            return false;
        }
        //end ozair 3643
    
        function checkGComments()
        {
            //		   var dateLenght = 0;
            //		  var newLenght = 0;
            //		  
            //          newLenght = document.frmgeneralinfo.WCC_GeneralComments_txt_Comments.value.length
            //		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

            //		  if (document.frmgeneralinfo.WCC_GeneralComments_txt_Comments.value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerText.length > (2000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
            //		  {
            //		    alert("Sorry You cannot type in more than 2000 characters in General comments box")
            //			return false;		  
            //		  }

            //		    //General Comments Check
            //			if (document.getElementById("txtGeneralComments").value.length > 2000-27) //-27 bcoz to show last time partconcatenated with comments
            //			{
            //				alert("You Cannot type more than 2000 characters ");
            //				document.getElementById("txtGeneralComments").focus();
            //				return false;
            //			}
            //		
        }
		
		
		
		
        function OpenPopUpNew(path)  //(var path,var name)
        {
            window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
            return false;
        }
        
        function HideNShow()
        {
            var ddlOperation=document.getElementById("ddlOperation").value;
            
            var tdfile=document.getElementById("fpupload");
            //Nasir 6181 07/31/2009 remove for above td
            var tdScan=document.getElementById("Scan");
            //Nasir 6181 07/31/2009 hide button
            var tdsubmit=document.getElementById("btnSubmit");
            var ddldocType=document.getElementById("ddlDocType").value;
            debugger;
            if(ddlOperation=="3")
            {
                document.getElementById("nonDoc").style.display='none';
                document.getElementById("GeDoc").style.display='block';
            }
            else 
            {
                document.getElementById("nonDoc").style.display='block';
                document.getElementById("GeDoc").style.display='none';
                if(ddlOperation== "2")
                {
                
                    tdfile.style.display='block';
                    //Nasir 6181 07/31/2009 remove for above td
                    tdScan.style.display='none'   ;
                    tdsubmit.style.display='block';    
                    
                }
                else
                {
                
                    tdfile.style.display='none';   
                    //Nasir 6181 07/31/2009 remove for above td
                    tdScan.style.display='block'   ;
                    tdsubmit.style.display='none';    
                }
            }
            
        }
        function check()
        {   
            var ddldocType=document.getElementById("ddlDocType").value;
            var ddlOperation=document.getElementById("ddlOperation").value;
            
            var tdfile=document.getElementById("fpupload");
            
            if(tdfile.style.display=="block")
            {
                if(ddldocType=="1")
                {
                    alert("Please Select the Document Type");
                    document.getElementById("ddlDocType").focus();
                    return false;
                }
                    // Noufil 6358 08/13/2009 IF ALR case is not dispose then Display javascript message
                else if (ddldocType == "20" && document.getElementById("ddl_SubDocType").value == "13" && document.getElementById("hf_IsALRDispoaseCase").value == "0")
                {
                    alert("Please dispose the ALR case.");
                    return false;
                }
            }
            if (ValidateSubDoctype()==false)
                return false;
           
        }
        function hide()
        {
            
            var tdfile=document.getElementById("fpupload");
            //Nasir 6181 07/31/2009 remove for above td and btnSubmit
            var tdsubmit=document.getElementById("btnSubmit");
            
            tdfile.style.display='none';
            //Nasir 6181 07/31/2009 remove for above td and 
            tdsubmit.style.display='none';
        }
        function PopUpShowPreviewMSG(DocID,refType,DocNum,DocExt)
        {
            //window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
            window.open("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt,'',"height=10,width=10,resizable=no, status=no,toolbar=no,scrollbars=no,menubar=no,location=no");
            return false;
        }
        function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
        {
            //Ozair 8039 07/20/2010 check implemented for ARR/Bond Summary report view (reftype 4)
            debugger;
            if(refType==4)
            {
                window.open ("../QuickEntryNew/PreviewPrintHistory.aspx?filename="+ DocID);
            }
            else
            {
                window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
            }
            return false;
        }
        function DeleteCheck(id)
        {
            //Noufil 5819 07/05/5009 Don't allow depenedent document to delete
            var SubDocTypeValue = document.getElementById(id).name;
            var SubdoctypeInGrid = document.getElementById("hf_Subdoctype").value; 
            var IsALRDPSRecieptDoc = "0";
            var IsALRSubpoenaPickupDOC = "0";
            var IsALRContRequestResetManDOC = "0";
            var IsALRContRequestResetDISDOC = "0";
    		    
            SubdoctypeInGrid = SubdoctypeInGrid.split(',');
    		    
            for ( i = 0 ; i < SubdoctypeInGrid.length; i++)
            {
                if (SubdoctypeInGrid[i] == "6")
                    IsALRDPSRecieptDoc = "1";
                if (SubdoctypeInGrid[i] == "10")
                    IsALRSubpoenaPickupDOC = "1";
                // Noufil 6358 08/24/2009 Code Comment
                //	if (SubdoctypeInGrid[i] == "16")
                //	    IsALRContRequestResetManDOC = "1";
                //	if (SubdoctypeInGrid[i] == "18")
                //	   IsALRContRequestResetDISDOC = "1";
            }
		    
            if (SubDocTypeValue == 5 && IsALRDPSRecieptDoc == "1")
            {
                alert("Please delete ALR DPS RECIEPT document first then delete ALR HEARING REQUEST Document.");
                return false;
            }		        
            if (SubDocTypeValue == 9 && IsALRSubpoenaPickupDOC == "1")
            {
                alert("Please delete ALR SUBPOENA PICKUP ALERT document first then delete ALR SUBPOENA APPLICATION document.");
                return false;
            }		        
		        
            // Noufil 6358 08/24/2009 Code Comment
            // if (SubDocTypeValue == 15 && IsALRContRequestResetManDOC == "1")
            // {
            //		            alert("Please delete ALR CONTINUANCE REQUEST RESET (MANDATORY) document first then delete ALR CONTINUANCE REQUEST (MANDATORY) document.");
            //		            return false;
            //		        }		       
            //		        if (SubDocTypeValue == 17 && IsALRContRequestResetDISDOC == "1")
            //		        {
            //		            alert("Please delete ALR CONTINUANCE REQUEST RESET (DISCRETIONARY) document first then delete ALR CONTINUANCE REQUEST (DISCRETIONARY) document.");
            //		            return false;
            //		        }
			    
            var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
            if(x)
            {
            }
            else
            {
                return false;
            }
        }
		
        //added by khalid bug 2526  	
        function OpenPopUpSmall(path)  //(var path,var name)
        {
            window.open(path,'',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");
        }
		
        // Abid Ali 5397 1/16/2009 for LOR certified
        function OpenReport(filePath)
        {
            
            window.open( filePath ,"");
            return false;
        }
					
        function PrintLetter( flag)
        {   
            //var flag=document.getElementById("ddlLetterType").value;
            //a;
            if(flag== "-1")
            {
                alert("PLease Select Letter Type!");
                document.getElementById("ddlLetterType").focus();
                return false;
            }
            /*if(flag=="1")
            {
              //alert(flag)			
              OpenPopUp("../ClientInfo/frmCaseSummary.aspx?casenumber=<%=ViewState["vTicketId"]%>&search=<%=ViewState["vSearch"]%>");	
            return false;	
        }*/
        //for EmailTrial leter..
        if(flag == "9")
        { 
            var retBatch=false; 
		    
            OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
		    return false;
				
        }
        if(flag=="2")
        {	 
            //var retBatch=false;
            var retBatch=false;
            var option = confirm("Trial Noticiation Letter generated. Would you like to 'PRINT NOW' or 'SEND TO BATCH'?\n Click [OK] to print now or click [CANCEL] to send to batch."); 
            if (option)
                retBatch =false;
            else
                retBatch=true;
            var courtid = document.getElementById("lbl_court").innerText;
            var flg_split = document.getElementById("lbl_IsSplit").innerText;
            var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerText;
            //changes made by khalid  bug 2526 date 22-1-08
            var PrintContinue;
            if (retBatch == true)
            {  
                if(flg_print==1)
                {
                    PrintContinue = confirm("Case number "+courtid +" already exists in batch print queue. Would you like to override the existing letter with this new letter");
                }
                if(PrintContinue==false)
                    // Afaq 8105 08/16/2010 Add date parameter in url.
                    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=0"+"&Date=<%=DateTime.Now.ToFileTime().ToString()%>");	
                else
                    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=1"+"&Date=<%=DateTime.Now.ToFileTime().ToString()%>");	
				 
            }
            else 
            {
                //	alert("here");
                OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");	
                //location.reload(true);
				
            }			 
		   
		  
            //end 2526
            //OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&CourtID="+courtid+"&flg_split="+flg_split+"&flg_print="+flg_print );	
		  
            return false;
        }						
        if(flag=="3")
        {	
			
            window.open("../Reports/word_receipt.aspx?casenumber=<%=ViewState["vTicketId"]%>",'',"left=150,top=50,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");	
				

			
		      return false;	
		  }		
		  if(flag=="4")
		  {	
		      //Nasir 6483 10/06/2009 logic for bond report for sec user
		      var paymenttype=document.getElementById("hf_CheckBondReportForSecUser").value;
        	        
		      if(paymenttype!="0")
		      {
		          if(paymenttype == 7)
		          {
		              alert("Bonds can only be printed within 2 months of last payment.");
		          }
		          else
		          {
		              alert("Bonds can only be printed within 3 Weeks of last payment.");
		          }
		          return false;
		      }
        			
		      var courtid=document.getElementById("lbl_courtid").innerText;
		      if (courtid==3001 ||courtid==3002 || courtid==3003)
		      {
		          OpenPopUp("../Reports/Word_Bond.aspx?CourtType=0" + "&casenumber=<%=ViewState["vTicketId"]%>");	
			    }
			    else
			    {
			        OpenPopUp("../Reports/Word_Bond.aspx?CourtType=1" + "&casenumber=<%=ViewState["vTicketId"]%>");	
			    }		
                return false;	
            }			
            if(flag=="5")
            {	 
                OpenPopUp("../Reports/FrmMainCONTINUANCE.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>");	
				
                        return false;	
                    }
			

                    if(flag=="6")
                    {	 
		       
		        
                        //Nasir 6181 07/22/2009 if all cases disposed.Dont Allow to print LOR 
                        var isnotDisposed=document.getElementById("hf_IsnotDisposed").value;
        
                        if(isnotDisposed==0)
                        {
                            alert("All cases disposed. LOR not available");
                            return false;
                        }
                        // Sabir Khan 5763 05/01/2009 Check for LOR Subpoena Letter and LOR Motion of Discovery...
                        //----------------------------------------------------------------------------------------
                        if(document.getElementById("hf_IsLORSubpoena").value == "1")
                        {
                            if(document.getElementById("hf_HasSameCourtdate").value == "0")
                            {
                                alert("The LOR for this court house contains Subpeona Letter and it requires to have same court date/time for all violations.\n Please first fix this issue and then print LOR.");
                                return false;
                            }
                        }
                        //Zeeshan Haider 11598 12/26/2013 Removed single speeding violation check for LOR MOD.
                        /*if(document.getElementById("hf_IsLORMOD").value == "1")
                        {
                            if(document.getElementById("hf_HasMoreSpeedingviol").value == "0")
                            {
                                alert("The LOR for this court house contains Motion For Discovery and it requires to have only one speeing violation.\n This client has more than one speeding violation. Please first fix this issue and then print LOR.");
                                return false;
                            }
                        }*/
                        //----------------------------------------------------------------------------------------  
		      
                        //Nasir 6181 07/22/2009 if LOR method is fax
                        //Ozair 6460 08/25/2009 included check for bond flag
                        if(document.getElementById("hfLORMethodCount").value != '0' && document.getElementById("hf_IsBond").value=="0") 
                        {               
                            return true;            
                        }
		        
                        //Nasir 6013 07/08/2009 remove message
		       
			    
                        var flg_print = document.getElementById("lbl_IsLORAlreadyInBatchPrint").innerHTML;
                        //Nasir 6181 07/27/2009 remove retbatch
                        if ( flg_print == 1)
                        {
                            if(!confirm("Case number <%=CauseNumber%> already exists in batch print queue. Would you like to override the existing letter with this new letter"))
			        {
			            return false;
			        }			    		    
			    }
                                                    
			    //Nasir 6181 07/27/2009 remove retbatch condition and print letter url            
			    //Ozair 6460 08/25/2009 small popup method called
                OpenPopUpSmall("../Reports/frmSendLetterOfRepBatch.aspx?Batch=true&lettertype=6&casenumber=<%=ViewState["vTicketId"]%>");					    	           
                                           	  
			    //Nasir 6013 07/10/2009 replace doyou to false
			    return false;	
			}
			
			if(flag=="8")
			{				    		    
			    OpenPopUp("../../reports/Payment_Receipt_Contract.asp?TicketID=<%=ViewState["vTicketid"]%>");	
			    return false;	
			}
			  
			}
			function OpenPopUp(path)  //(var path,var name)
			{
		
			    window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		 
			}
		
			function ValidateSubDoctype()
			{		
			    //Noufil 5819 07/05/5009 Don't allow child document to upload without parent document
			    var SubDocTypeValue = document.getElementById("ddl_SubDocType").value;
			    var SubdoctypeInGrid = document.getElementById("hf_Subdoctype").value; 
			    var IsALRHearingDoc = "0";
			    var IsALRSubpoenaApplicationDOC = "0";
			    var IsALRContRequestManDOC = "0";
			    var IsALRContRequestDISDOC = "0";
			    var IsALRhearingNotDOC = "0";
		    
			    SubdoctypeInGrid = SubdoctypeInGrid.split(',');
		    
			    for ( i = 0 ; i < SubdoctypeInGrid.length; i++)
			    {
			        if (SubdoctypeInGrid[i] == "5")
			        {
			            IsALRHearingDoc = "1";
			        }   
		        
			        if (SubdoctypeInGrid[i] == "9")
			        {
			            IsALRSubpoenaApplicationDOC = "1";
			        }
		            
			        if (SubdoctypeInGrid[i] == "15")
			        {
			            IsALRContRequestManDOC = "1";
			        }		        
			        else if (SubdoctypeInGrid[i] == "17")
			        {
			            IsALRContRequestDISDOC = "1";
			        }		        
			        else if (SubdoctypeInGrid[i] == "7")
			        {
			            IsALRhearingNotDOC = "1";
			        }
			    }		    
		   
			    if (SubDocTypeValue == 6 && IsALRHearingDoc == "0")
			    {
			        alert("Please add ALR HEARING REQUEST document first then Add ALR DPS RECIEPT document.");
			        return false;
			    }
		    
			    if (SubDocTypeValue == 10 && IsALRSubpoenaApplicationDOC == "0")
			    {
			        alert("Please add ALR SUBPOENA APPLICATION document first then Add ALR SUBPOENA PICKUP ALERT document.");
			        return false;
			    }
		    
		    
			    if (IsALRContRequestManDOC =="1" || IsALRContRequestDISDOC=="1")
			    {
			        // Noufil 6358 08/24/2009 ALR CONTINUANCE REQUEST (MANDATORY) document can only be added after ALR HEARING NOTIFICATION or no ALR CONTINUANCE REQUEST (MANDATORY) and ALR CONTINUANCE REQUEST (DISCRETIONARY) document present.
			        if (SubDocTypeValue == 15)
			        {
			            alert("Please add ALR HEARING NOTIFICATION document first then Add ALR CONTINUANCE REQUEST (MANDATORY)");
			            return false;
			        }
			            // Noufil 6358 08/24/2009 ALR CONTINUANCE REQUEST (DISCRETIONARY) document can only be added after ALR HEARING NOTIFICATION or no ALR CONTINUANCE REQUEST (MANDATORY) and ALR CONTINUANCE REQUEST (DISCRETIONARY) document present.
			        else if(SubDocTypeValue == 17)
			        {
			            alert("Please add ALR HEARING NOTIFICATION document first then Add ALR CONTINUANCE REQUEST (DISCRETIONARY)");
			            return false;
			        }		        
			    }		    
			}
		
			function DateDiff(date1, date2)
			{
			    var objDate1=new Date(date1);
			    var objDate2=new Date(date2);
			    return (objDate1.getTime()-objDate2.getTime())/1000;
			}
		
    </script>

    <!-- Do Not Delete This. initialozing the scannig object. -->
    <%--<%=Session["objTwain"].ToString()%>--%>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function AcquireImage() {
            var DocId = document.getElementById("ddlDocType").value;
            if (DocId == "1") {
                alert("Plese Select valid document type to scan");
                document.getElementById("ddlDocType").focus();
                return false;
            }
            if (ValidateSubDoctype() == false)
                return false;
            debugger;
            var ddlOperation = document.getElementById("ddlOperation").value;
            //Ozair 5546 02/17/2009 included ticketid with session id
            var namefile = document.getElementById("txtsessionid").value + document.getElementById("hf_TicketID").value;
            var sid = document.getElementById("hf_TicketID").value;
            var eid = document.getElementById("txtempid").value;
            //            if (eid == null || !eid.length > 0)
            //                eid = '0';

            var txtbID = document.getElementById("txtbID").value;
            //            if (txtbID == null || !txtbID.length > 0)
            //                txtbID = 0;
            var Desc = document.getElementById("txtDescription").value;
            //            if (Desc == null || !Desc.length > 0)
            //                Desc = "empty";
            var subDoc = document.getElementById("ddl_SubDocType").value;
            var operationType = document.getElementById("ddlOperation").value;
            
            
            //var path = document.getElementById("NTPATHScanTo").value;
            //var path = "C:\DOCSTORAGE\ScanDoc\tempimages\\";
            //var vCode = document.getElementById("NTPATHScanTo");
            //path = vCode.value;
            var Type = 'network';
            var AutoFeed = 1;
            //window.open("PgScanner.htm", '', "height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
            //window.open("PgScanner.htm");

            window.open("ScanData.aspx?DocID=" + DocId + "&txtbID=" + txtbID + "&empId=" + eid + "&sid=" + sid + "&Desc=" + Desc + "&subDoc=" + subDoc + "&Operation=" + operationType +"&name="+namefile, '', "height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
            
            //window.open("PreviewMain.aspx?DocID=" + DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt, '', "height=10,width=10,resizable=no, status=no,toolbar=no,scrollbars=no,menubar=no,location=no");
            //var sel=OZTwain1.SelectScanner();
            //            var DWObject = Dynamsoft.WebTwainEnv.GetWebTwain('dwtcontrolContainer');
            //            if (DWObject != null) {
            //                if (ddlOperation == "0") {
            //                    //AutoFeed=-1;
            //                    DWObject.IfDisableSourceAfterAcquire = true;
            //                    DWObject.SelectSource();
            //                    DWObject.OpenSource();
            //                    DWObject.AcquireImage();
            //                }
                
            //OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
            //
            //                if (ddlOperation == "1") {
            //                    var x = confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
            //                    if (x) {
            //                        //OZTwain1.Acquire(sid,eid,path,Type);
            //                        AcquireImage();
            //                    }
            //                    else {
            //                        document.getElementById("txtbID").value = "";
            //                        //document.Form1.Adf.checked = true;
            //                    }
            //                }
            //            }
            //            else if (DWObject == "Cancel") {
            //                alert("Operation canceled by user!");
            //                return false;
            //            }
            //            else {
            //                alert("Scanner not installed.");
            //                return false;
            //            }

        }
    </script>
</head>
<body onload="hide();">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div class="page-container row-fluid container-fluid">

            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            </aspnew:ScriptManager>

            <aspnew:UpdatePanel ID="UpdPnlActiveMenu" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <%--msg--%>
                    <%--<aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                    <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                    <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />--%>
                </Triggers>
            </aspnew:UpdatePanel>

            <section id="main-content" class="">
    <section class="wrapper main-wrapper row" style="">
        <div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" style="margin-top: 8px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
        <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">MATTER --> Documents</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            <div class="pull-right hidden-xs">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                
                                                <tr>
                                                    <td style="height: 35px">
                                                        &nbsp;
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="form-label"></asp:Label>,
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="form-label"></asp:Label>&nbsp;(
                                                        <asp:Label ID="lbl_CaseCount" runat="server" CssClass="form-label"></asp:Label>),
                                                        <asp:HyperLink ID="hlnk_MidNo" runat="server" CssClass="form-label"></asp:HyperLink>
                                                        
                                                    </td>
                                                   
                                                </tr>
                    <tr>
                                <td>
                                    <asp:Label ID="lblMessage" runat="server" Font-Names="Verdana" Font-Size="X-Small" CssClass="form-label"
                                        ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td id="read" runat="server" style="width: 100%;" colspan="2">
                                    <uc2:ReadNotes ID="ReadNotes1" runat="server" />
                                </td>
                            </tr>
                                            </table>

                </div>

            </div>
            </div>

        <div class="clearfix"></div>

        <div class="col-xs-12">

             <section class="box" id="tbl_contact">
                 <header class="panel_header">
                     <h2 class="title pull-left">Scan/Upload</h2>
                     <div class="actions panel_actions pull-right">
                      <asp:DropDownList ID="ddlOperation" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True" Value="0">Scan-Feeder</asp:ListItem>
                                                    <asp:ListItem Value="1">Scan-Flatbed</asp:ListItem>
                                                    <asp:ListItem Value="2">Upload</asp:ListItem>
                                                </asp:DropDownList>
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>
                 <div class="content-body">
            <div class="row">

                 <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">Document Type</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:DropDownList ID="ddlDocType" runat="server" CssClass="form-control" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                            </asp:DropDownList>

                                </div>
                                                            </div>
                     </div>

                <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">Sub Type</label>
                            <span class="desc"></span>
                            <%--<div class="controls" id="sdt1" runat="server"> Ammar check this --%> 
                                                            <div class="controls" id="sdt1" >
                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" EnableViewState="true">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddl_SubDocType" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <aspnew:AsyncPostBackTrigger ControlID="ddlDocType" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </aspnew:UpdatePanel>

                                </div>
                                                            </div>
                     </div>

                <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">Description</label>
                            <span class="desc"></span>
                            <div class="controls">
                                 <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"
                                                                 MaxLength="100">
                                     </asp:TextBox>

                                </div>
                                                            </div>
                     </div>



                <div class="col-md-3">
                    <div class="form-group">
                          <label style="color:white;">.</label>
                            <span class="desc"></span>
                            <div class="controls">
                                  <table class="clsLeftPaddingTable">
                                                                <tr>
                                                                    <td class="clsLeftPaddingTable" id="fpupload">
                                                                        <asp:FileUpload ID="FPUpload" runat="server" CssClass="btn btn-primary" />
                                                                    </td>
                                                                </tr>
                                                            </table>

                                </div>
                                                            </div>

                </div>

                </div>
                     <div class="row">
                         <div class="col-md-3">
                <div class="clearfix"></div>
                 <div class="col-md-3" >
                   <div class="form-group">
                          
                            <span class="desc"></span>
                            <div class="controls">
                                  <table class="clsLeftPaddingTable">
                                                                <tr>
                                                                    
                                                                    <td id="Scan" class="clsLeftPaddingTable" style="height: 20px; width: 60px;">
                                                                        <asp:Button ID="btnScan" runat="server" CssClass="btn btn-primary" Text="Scan" 
                                                                             />
                                                                    </td>
                                                                    <td class="clsLeftPaddingTable" id="submit" style="height: 20px; ">
                                                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>

                                </div>
                                                            </div>
                     </div>

                
                <div class="clearfix"></div>
                 <%--Invisible portion start--%>
                 <div class="col-md-3" >
                                                        <div class="form-group">
                            <label class="form-label"></label>
                            <span class="desc"></span>
                            
                                 <table>
                           <tr id="nonDoc">
                               <td valign="top" id="GeDoc" style="display: none">
                                                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                                                    <tr>
                                                        <td class="" height="20">
                                                            &nbsp;<asp:DropDownList ID="ddlLetterType" runat="server" CssClass="clsInputCombo">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="" align="right">
                                                            &nbsp;
                                                            <asp:Button ID="btnGenerateDocs" runat="server" Text="Generate Documents" CssClass="btn btn-primary"
                                                                OnClick="btnGenerateDocs_Click" />&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                           </tr>
                       </table>

                                </div>
                                                            </div>
                     </div>

                     </div>
                     
                <%--Invisible portion End--%>


                      </div>




                     
                 </section>
            

            <div class="clearfix"></div>

            <section class="box" id="">
                 <header class="panel_header">
                     <h2 class="title pull-left">Generate Documents</h2>
                     <div class="actions panel_actions pull-right">
                      
                                              
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                <div class="content-body">
            <div class="row">

                                <div class="table-responsive" data-pattern="priority-columns">
                                 <asp:DataList ID="dl_generatedocs" runat="server" RepeatDirection="Horizontal" Style="position: relative" class="table table-small-font table-bordered table-striped"
                                                     OnItemDataBound="dl_generatedocs_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lb_docs" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.value") %>'
                                                            OnClick="btnGenerateDocs_Click" Style="position: relative; left: 0px;" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                            OnCommand="lb_docs_Command"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                    </div>
                   
                </div>
                    </div>






                </section>

            <div class="clearfix"></div>

            <section class="box" id="">
                 <header class="panel_header">
                     <h2 class="title pull-left">Document History</h2>
                     <div class="actions panel_actions pull-right">
                      
                                              
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                <div class="content-body">
            <div class="row">

                 <div class="table-responsive" data-pattern="priority-columns">
                                
                                                            <asp:DataGrid ID="dgrdDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False" class="table table-small-font table-bordered table-striped"
                                                                Font-Names="Verdana" Font-Size="2px" PageSize="20" Width="100%" OnItemCommand="dgrdDoc_ItemCommand">
                                                                <FooterStyle CssClass="GrdFooter" />
                                                                <PagerStyle CssClass="GrdFooter" Font-Names="Arial" HorizontalAlign="Center" Mode="NumericPages"
                                                                    NextPageText="" PageButtonCount="50" PrevPageText="" />
                                                                <AlternatingItemStyle BackColor="#EEEEEE" />
                                                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Date">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDateTime" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATEDATETIME") %>'
                                                                                Font-Size="Smaller"></asp:Label>&nbsp; &nbsp;
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Event">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEvent" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Events") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Document Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocType" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.DOCREF") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lbldid" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblocrid" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Document Description">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocDescription" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ResetDesc") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocExtension" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Rep">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAbb1" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="View">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif"
                                                                                Width="19" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Pages / Docs">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ImageCount") %>'
                                                                                Width="32px" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblRecType" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>'
                                                                                Visible="False" Width="10px" Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Options">
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkbtn_Delete" Name='<%#Eval("subdoctype") %>' runat="server"
                                                                                CommandName="Delete" CommandArgument='<%#Eval("subdoctype") %>' Font-Size="Smaller">Delete,</asp:LinkButton>&nbsp;
                                                                            <asp:LinkButton ID="lnkbtn_OCR" runat="server" CommandName="OCR" Font-Size="Smaller">OCR</asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkbtn_VOCR" runat="server" CommandName="View" Visible="False"
                                                                                Font-Size="Smaller">View OCR</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                       
                                            

                                </div>
                </div>
                    </div>






                </section>

            <div class="clearfix"></div>

             <section class="box" id="">
                 <header class="panel_header" id="FlagsE">
                     <h2 class="title pull-left">General Comments</h2>
                     <div class="actions panel_actions pull-right">
                      
                                              
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                <div class="content-body">
            <div class="row">

                 <div class="col-md-12">
                                                        <div class="form-group">
                            <label class="form-label"></label>
                            <span class="desc"></span>
                            <div class="controls">
                             
                                                <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server"  />
                                           

                                </div>
                                                            </div>
                     </div>
                </div>
                    </div>


                </section>

            <div class="clearfix"></div>

            <section class="box">
            
        <div class="content-body">
            <div class="row">
                 <asp:Button ID="btnupdate" runat="server" CssClass="btn btn-primary" OnClick="btnupdate_Click"
                                        Text="Update" />
            </div>
        </div>
        </section>

             <div class="clearfix"></div>

            <%--invisible portion Start MSG--%>
             <section class="box" >
            
        <div class="content-body" style="display:none">
            <div class="row">
                <table>
                    <tr>
                                <td colspan="5">
                                    <uc1:Footer ID="Footer1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="visibility: hidden" colspan="2">
                                    <asp:TextBox ID="txtImageCount" runat="server"></asp:TextBox><asp:TextBox ID="txtQuery"
                                        runat="server"></asp:TextBox><asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox
                                            ID="txtsessionid" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtbID" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox>
                                    <asp:Label ID="lbl_courtid" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsSplit" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_court" runat="server"></asp:Label>
                                    <asp:HiddenField ID="hf_TicketID" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORSubpoena" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORMOD" runat="server" />
                                    <asp:HiddenField ID="hf_HasSameCourtdate" runat="server" />
                                    <asp:HiddenField ID="hf_HasMoreSpeedingviol" runat="server" />
                                    <asp:HiddenField ID="hf_IsnotDisposed" runat="server" />
                                    <asp:HiddenField ID="hfLORMethodCount" runat="server" />
                                    <asp:HiddenField ID="hf_Subdoctype" runat="server" />
                                    <asp:HiddenField ID="hf_IsALRDispoaseCase" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_IsBond" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_CheckBondReportForSecUser" Value="0" runat="server" />
                                </td>
                            </tr>

                    <asp:Label ID="lbl_IsLORAlreadyInBatchPrint" Style="display: none;" runat="server"></asp:Label>
        <asp:HiddenField ID="hf_letterofrap" runat="server" />
        <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                    PopupControlID="panel1" BackgroundCssClass="modalBackground" HideDropDownList="false"
                    CancelControlID="img1">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="panel1" runat="server" Width="483px" Height="400px">
                    <table id="Table1" bgcolor="white" border="0" style="border-top: black thin solid;
                        border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                        height: 400px;">
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                <table>
                                    <tr>
                                        <td style="width: 500px">
                                            <span class="clssubhead">Compose Fax</span>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="img1" runat="server" ImageUrl="../Images/remove2.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Fc:Faxcontrol ID="Faxcontrol1" runat="server" Recall="true" />
                                <asp:Button ID="Button1" runat="server" Text="Cancel" Style="display: none;" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
                </table>
            </div>
        </div>
        </section>
            <%--invisible portion End MSG--%>






           </div>
       

            


           








       </section>
           </section>
        </div>






    </form>
    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">
        document.getElementById('FPUpload').style = 'margin-top:7px;Width:226px;';
        //document.getElementById('FileUpload1').style = 'margin-top:7px;Width="226px;';
    </script>
</body>
</html>

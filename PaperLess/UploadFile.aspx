<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaperLess.UploadFile" Codebehind="UploadFile.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UploadFile</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			function Check()
			{	
				if(Form1.cmbDocType.value=="-1")
				{
					alert("Please Select Document Type");
					Form1.cmbDocType.focus();
					document.Form1.File1.filters
					return false;
				}
			}
		</script>
		<!--<script language="C#" runat="Server">
		//Event Handler for the upload button
		void UploadFile(object Sender,EventArgs E)
		{
			if (File1.PostedFile !=null) //Checking for valid file
			{		
				// Since the PostedFile.FileNameFileName gives the entire path we use Substring function to rip of the filename alone.
				string StrFileName = File1.PostedFile.FileName.Substring(File1.PostedFile.FileName.LastIndexOf("\\") + 1) ;
				string StrFileType = File1.PostedFile.ContentType ;
				int IntFileSize =File1.PostedFile.ContentLength;
				//Checking for the length of the file. If length is 0 then file is not uploaded.
				if (IntFileSize <=0)
					Response.Write(" <font color='Red' size='2'>Uploading of file " + StrFileName + " failed </font>");
				else
				{
					string sid= txtsessionid.Text;
					string eid= txtempid.Text;
					string StrFileName1=sid+eid+StrFileName;
					File1.PostedFile.SaveAs(Server.MapPath(".\\tempimages\\" + StrFileName1));
					//Response.Write( "<font color='green' size='2'>Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully</font>");
					lblMessage.Text="Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully";
				}
			}
		}
		</script>-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" encType="multipart/form-data" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; WIDTH: 780px" cellSpacing="0" cellPadding="0"
				width="780" align="center" border="0">
				<tbody>
				<TR>
					<TD style="WIDTH: 780px; HEIGHT: 5px" ><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
			<tr>
			<td>
			    <table id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="4" height="11"></td>
				</tr>
				<TR>
					<TD class="clssubhead" style="WIDTH: 780px; HEIGHT: 35px" background="../Images/subhead_bg.gif"
						colSpan="4">&nbsp;Upload File</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 11px" colSpan="4">&nbsp;</TD>
				</TR>
				<TR id="first">
					<TD class="clsLeftPaddingTable" vAlign="middle" width="15%">Document Type
					</TD>
					<td class="clsLeftPaddingTable" vAlign="middle" width="30%"><asp:dropdownlist id="cmbDocType" runat="server" CssClass="clsInputCombo" Width="184px"></asp:dropdownlist></td>
					<TD class="clsLeftPaddingTable" align="left" width="10%">Description
					</TD>
					<TD class="clsLeftPaddingTable" align="left"><asp:textbox id="txtDesc" runat="server" CssClass="clsinputadministration" Width="272px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" align="left">Select a file to upload
					</TD>
					<td class="clsLeftPaddingTable" align="left"><INPUT class="clsinputadministration" id="File1" style="WIDTH: 298px; HEIGHT: 17px" type="file"
							size="30" name="File1" runat="server">
					</td>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 11px"><asp:button id="CmdUpload" onclick="UploadFile" runat="server" CssClass="clsbutton" Text="UpLoad"
							value="Upload"></asp:button>
					</TD>
					<td class="clsLeftPaddingTable" align="right">
						<asp:LinkButton id="LinkButton1" runat="server">Back to Scan Main Screen</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</TR>
				<tr>
					<td class="clsLeftPaddingTable" align="center" colSpan="4"><asp:label id="lblMessage" runat="server" CssClass="label" Height="16px"></asp:label></td>
				</tr>
				<tr>
					<td style="VISIBILITY: hidden" colSpan="4"><asp:textbox id="txtempid" runat="server"></asp:textbox><asp:textbox id="txtsessionid" runat="server"></asp:textbox></td>
				</tr>
				</TABLE>
				</td>
				</tr>
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="4" height="11"></td>
				</tr>
				<TR>
					<TD style="WIDTH: 760px" align="left" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
				</tbody>
			</table>
			
		</form>
	</body>
</HTML>

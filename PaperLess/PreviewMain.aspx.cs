using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.IO;
using System.Configuration;

namespace lntechNew.PaperLess
{
    /// <summary>
    /// Summary description for PreviewMain.
    /// </summary>
    public partial class PreviewMain : System.Web.UI.Page
    {
        clsENationWebComponents ClsDB = new clsENationWebComponents();

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                //ShowPDF(Session["DocID"]);
                //ShowPDF(22);
                try
                {


                    if (Request.QueryString["TDocID"] != null)
                    {
                        DataSet ds = ClsDB.Get_DS_BySPByOneParmameter("USP_HTS_GET_DOCID_BY_TICKETID", "@TicketID", Request.QueryString["TDocID"]);
                        ViewState["DocID"] = ds.Tables[0].Rows[0]["Doc_ID"].ToString();
                        ViewState["RecType"] = ds.Tables[0].Rows[0]["RecordType"].ToString();
                        ViewState["DocNum"] = ds.Tables[0].Rows[0]["doc_num"].ToString();
                        ViewState["DocExt"] = ds.Tables[0].Rows[0]["DocExtension"].ToString();

                    }
                    else
                    {

                        //Session["DocID"] = Request.QueryString["DocID"]; 
                        ViewState["DocID"] = Request.QueryString["DocID"];
                        //Session["RecType"] = Request.QueryString["RecType"]; 
                        ViewState["RecType"] = Request.QueryString["RecType"];
                        //Session["DocNum"] = Request.QueryString["DocNum"]; 
                        ViewState["DocNum"] = Request.QueryString["DocNum"];
                        //Session["DocExt"] = Request.QueryString["DocExt"]; 
                        ViewState["DocExt"] = Request.QueryString["DocExt"];
                        //if(Session["DocExt"].ToString()!="000")

                    }
                    if (String.Compare(ViewState["DocExt"].ToString(), "msg", true) == 0)
                    {

                        string folderPath = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                        string fileName = string.Empty;
                        string[] key = { "@DocID" };
                        //object[] value1={Convert.ToInt32(Session["DocID"].ToString())};
                        object[] value1 = { Convert.ToInt32(ViewState["DocID"].ToString()) };
                        DataTable dt = ClsDB.Get_DS_BySPArr("usp_dts_getScanByDocId", key, value1).Tables[0];
                        if (dt.Rows.Count != 0)
                        {
                            fileName = dt.Rows[0]["ScanBookId"] + "-" + dt.Rows[0]["DOCID"] + ".msg";
                            fileName = Path.Combine(folderPath, fileName);
                            //-- if something was passed to the file querystring 
                            if (fileName != "")
                            {
                                System.IO.FileInfo file = new System.IO.FileInfo(fileName);
                                //-- if the file exists on the server 
                                if (file.Exists)
                                {
                                    //set appropriate headers 
                                    Response.Clear();
                                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                                    Response.AddHeader("Content-Length", file.Length.ToString());
                                    Response.ContentType = "application/octet-stream";
                                    Response.WriteFile(file.FullName);
                                    Response.Write("javascript:CloseWindow();");
                                    Response.End();
                                    //if file does not exist 
                                }
                                else
                                {
                                    Response.Write("This file does not exist.");
                                }
                                //nothing in the URL as HTTP GET 
                            }
                            else
                            {
                                Response.Write("Please provide a file to download.");
                            }

                        }
                        //HttpContext.Current.Response.WriteFile(
                    }

                    //Adil 5419 1/29/2009 adding support for EML files.
                    else if (String.Compare(ViewState["DocExt"].ToString(), "eml", true) == 0)
                    {

                        string folderPath = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                        string fileName = Path.Combine(folderPath, Convert.ToString(Request.QueryString["Fname"]) + ".eml");
                        System.IO.FileInfo file = new System.IO.FileInfo(fileName);
                        //-- if the file exists on the server 
                        if (file.Exists)
                        {
                            //set appropriate headers 
                            Response.Clear();
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                            Response.AddHeader("Content-Length", file.Length.ToString());
                            Response.ContentType = "application/octet-stream";
                            Response.WriteFile(fileName);
                        }
                        else
                        {
                            Response.Write("This file does not exist.");
                        }
                    }

                    else if (ViewState["DocExt"].ToString() != "000")
                    {
                        string[] key = { "@DocID" };
                        //object[] value1={Convert.ToInt32(Session["DocID"].ToString())};
                        object[] value1 = { Convert.ToInt32(ViewState["DocID"].ToString()) };
                        DataSet ds = ClsDB.Get_DS_BySPArr("usp_hts_get_scandoccheck", key, value1);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            //HttpContext.Current.Response.Write("<script language='javascript'> alert('Documnet Not Found.'); window.location='../clientinfo/casehistory.aspx' </script>");
                            HttpContext.Current.Response.Write("<script language='javascript'> alert('Documnet Not Found.'); self.close();</script>");
                            Response.End();
                        }

                    }

                    //HttpContext.Current.Response.WriteFile(



                }
                //Adil 5419 1/29/2009 commenting "(Exception ex)" to avoid warning for ex.
                catch //(Exception ex)
                {
                    //HttpContext.Current.Response.Write("<script language='javascript'> alert('Documnet Not Found.'); self.close();</script>");
                    Response.End();
                }

                //ShowPDF(Request.QueryString["DocID"]);

            }

            //Response.WriteFile(Session["PDFFileName"]);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}

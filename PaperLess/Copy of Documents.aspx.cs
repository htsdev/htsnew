using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.WebControls;
using System.IO;
using WebSupergoo.ABCpdf6;


namespace lntechNew.PaperLess
{
    public partial class Documents : System.Web.UI.Page
    {

        #region Variables


        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsCase ClsCase = new clsCase();
        
        bool Bond = false;
        bool LetterOfRep = false;
        bool Continuance = false;
        bool TrialLetter = false;
        
        //Added by Khalid On 14/11/2007
        DataTable LetterType = new DataTable();

        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx");
                }
                else //To stop page further execution
                {

                    lblMessage.Text = "";
                    if (!IsPostBack)
                    {
                        LetterType.Columns.Add("name");
                        LetterType.Columns.Add("value");
                        if (Request.QueryString.Count >= 2)
                        {
                            ViewState["vTicketId"] = Request.QueryString["casenumber"];
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }

                        ViewState["vNTPATHCaseSummary"] = ConfigurationSettings.AppSettings["NTPATHCaseSummary"].ToString();
                        ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();


                        txtempid.Text = ClsSession.GetCookie("sEmpID", this.Request);
                        txtsessionid.Text = Session.SessionID;
                        int tID = Convert.ToInt32(ViewState["vTicketId"]);
                        if (tID == 0)
                        {
                            Response.Redirect("../ClientInfo/ViolationFeeold.aspx?newt=1");
                        }
                        BindControls();
                        ShowHideLettersType();
                        DisplayInfo();
                        BindGrid();
                        IsAlreadyInBatchPrint();
                        CheckSpilitCases();
                        #region ForScanningPurPose

                        string strServer;
                        strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                        Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.
                        btnScan.Attributes.Add("onclick", "return StartScan();");
                        #endregion


                    }
                }
                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");
                txt1.Text = ViewState["vTicketId"].ToString();
                TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                txt2.Text = ViewState["vSearch"].ToString();
            SearchPage:
                { }

                ddlOperation.Attributes.Add("onclick", "HideNShow();");
                btnSubmit.Attributes.Add("onclick", "return check();");
                btnGenerateDocs.Attributes.Add("onclick", "return PrintLetter();");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                SetValuesForGC();
                DataSet ds_GC = ClsCase.UpdateGeneralComments();
                txtGeneralComments.Text = ds_GC.Tables[0].Rows[0]["generalcomments"].ToString();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
       
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (FPUpload.PostedFile != null) //Checking for valid file
                {
                    // Since the PostedFile.FileNameFileName gives the entire path we use Substring function to rip of the filename alone.
                    string StrFileName = FPUpload.PostedFile.FileName.Substring(FPUpload.PostedFile.FileName.LastIndexOf("\\") + 1);
                    string StrFileType = FPUpload.PostedFile.ContentType;
                    int IntFileSize = FPUpload.PostedFile.ContentLength;
                    //Checking for the length of the file. If length is 0 then file is not uploaded.
                    if (IntFileSize <= 0)
                        lblMessage.Text = " <font color='Red' size='2'>Uploading of file " + StrFileName + " failed </font>";
                    else
                    {
                        string sid = txtsessionid.Text;
                        string eid = txtempid.Text;
                        string StrFileName1 = sid + eid + StrFileName;
                        //File1.PostedFile.SaveAs(Server.MapPath(".\\tempimages\\" + StrFileName1));
                        FPUpload.PostedFile.SaveAs(ViewState["vNTPATHScanTemp"].ToString() + StrFileName1);
                        //Response.Write( "<font color='green' size='2'>Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully</font>");
                        lblMessage.Text = "Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully";

                        //Session["ScanDocType"] = cmbDocType.SelectedItem.Text;
                        ViewState["ScanDocType"] = ddlDocType.SelectedItem.Text;
                        //Session["BookID"]= 0;
                        ViewState["BookID"] = 0;
                        //Session["DocCount"]= 0;
                        ViewState["DocCount"] = 0;
                        ViewState["Events"] = "Upload";

                        //Perform database Uploading Task

                        //string searchpat="*"+ Session.SessionID + Session["sEmpID"].ToString() +"*.*";
                        string searchpat = "*" + Session.SessionID + eid + "*.*";
                        //string[] fileName=Directory.GetFiles(Server.MapPath("tempimages/"),searchpat);	
                        string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);

                        //string Ticket = Session["sTicketID"].ToString();
                        //int empid= (int) Convert.ChangeType(Session["sEmpID"].ToString() ,typeof(int)); //
                        //int TicketID = (int) Convert.ChangeType(Session["sTicketID"].ToString() ,typeof(int));
                        string Ticket = ViewState["vTicketId"].ToString();//ClsSession.GetCookie("sTicketID",this.Request);
                        int empid = Convert.ToInt32(eid);
                        int TicketID = Convert.ToInt32(Ticket);
                        //string bType=Session["ScanDocType"].ToString().Trim(); 
                        string bType = ViewState["ScanDocType"].ToString().Trim();
                        int subdoctypeid = 0;
                        if (ddl_SubDocType.Items.Count > 0)
                        {
                            subdoctypeid = Convert.ToInt32(ddl_SubDocType.SelectedValue);
                        }
                        int BookId = 0, picID = 0;
                        string picName, picDestination;
                        int DocCount = 1;

                        for (int i = 0; i < fileName.Length; i++)
                        {
                            string exten = fileName[i].Substring(fileName[i].LastIndexOf(".") + 1);
                            string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                            object[] value1 = { DateTime.Now, exten.ToUpper(), "(Uploaded File) " + txtDescription.Text.Trim().ToUpper(), bType, subdoctypeid, empid, TicketID, DocCount, BookId, ViewState["Events"].ToString(), "" };
                            //call sP and get the book ID back from sP
                            picName = ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
                            string BookI = picName.Split('-')[0];
                            string picI = picName.Split('-')[1];
                            BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                            picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                            //Move file
                            //picDestination= Server.MapPath("images/")+ picName + "." + exten; //DestinationImage
                            picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + "." + exten; //DestinationImage

                            System.IO.File.Copy(fileName[i].ToString(), picDestination);

                            System.IO.File.Delete(fileName[i].ToString());
                        }

                        BindGrid();
                        RefreshControls();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnScan_Click(object sender, EventArgs e)
        {
            try
            {

                ViewState["ScanDocType"] = ddlDocType.SelectedItem.Text;
                ViewState["BookID"] = 0;
                ViewState["DocCount"] = 0;
                ViewState["Events"] = "Scan";

                if (txtbID.Text.Length > 0)
                {
                    ViewState["sSDDocCount"] = ViewState["sSDDocCount"].ToString();
                }
                else
                {
                    ViewState["sSDDocCount"] = 1;
                }


                ViewState["sSDDesc"] = txtDescription.Text.Trim().ToUpper();

                //Perform database Uploading Task

                string searchpat = "*" + Session.SessionID + txtempid.Text + "*.jpg";
                //string[] fileName = Directory.GetFiles(Server.MapPath("tempimages/"), searchpat);
                string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);
                //
                if (fileName.Length > 1)
                {
                    fileName = sortfilesbydate(fileName);
                }
                //
                string Ticket = ViewState["vTicketId"].ToString();

                int empid = Convert.ToInt32(txtempid.Text); //
                int TicketID = (int)Convert.ChangeType(ViewState["vTicketId"].ToString().ToString(), typeof(int));

                string bType = ViewState["ScanDocType"].ToString().Trim();
                int subdoctypeid = 0;
                if (ddl_SubDocType.Items.Count > 0)
                {
                    subdoctypeid = Convert.ToInt32(ddl_SubDocType.SelectedValue);
                }

                int BookId = 0, picID = 0;
                if (txtbID.Text.Length > 0)
                {
                    BookId = Convert.ToInt32(txtbID.Text);
                }
                string picName, picDestination;

                int DocCount = Convert.ToInt32(ViewState["sSDDocCount"].ToString());

                string description = ViewState["sSDDesc"].ToString().ToUpper();

                for (int i = 0; i < fileName.Length; i++)
                {
                    string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                    object[] value1 = { DateTime.Now, "JPG", description, bType, subdoctypeid, empid, TicketID, DocCount, BookId, ViewState["Events"].ToString(), "" };
                    //call sP and get the book ID back from sP
                    picName = ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
                    string BookI = picName.Split('-')[0];
                    string picI = picName.Split('-')[1];
                    BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                    picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                    if (ddlDocType.SelectedValue.ToString() != "0")//if (Adf.Checked == false)
                    {
                        txtbID.Text = BookId.ToString();
                    }
                    DocCount = DocCount + 1;
                    ViewState["sSDDocCount"] = Convert.ToString(DocCount);

                    //Move file
                    //picDestination = Server.MapPath("images/") + picName + ".jpg"; //DestinationImage
                    picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage

                    System.IO.File.Copy(fileName[i].ToString(), picDestination);

                    System.IO.File.Delete(fileName[i].ToString());
                }
                //if (ddlDocType.SelectedValue.ToString() == "0")//if (Adf.Checked)
                if (ddlOperation.SelectedValue == "0")//if (Adf.Checked)
                {

                    txtbID.Text = "";
                    txtDescription.Text = "";
                    //Adf.Checked = false;
                }
                //BindGrid(ViewState["vTicketId"].ToString());

                BindGrid();
                RefreshControls();
                //
                HttpContext.Current.Response.Write("<script>window.open('PreviewMain.aspx?DocID=" + BookId + "&RecType=1&DocNum=0&DocExt=JPG');</script>");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dgrdDoc_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            string lngDocID;
            string lngRecordType;
            string strURL;
            string lngDocNum;
            string lngDocExt;
            try
            {
                if (e.CommandName == "Delete")//for delete operation..
                {
                    try
                    {
                        lngDocID = ((Label)(e.Item.FindControl("lblDocID"))).Text;
                        lngDocNum = ((Label)(e.Item.FindControl("lblDocNum"))).Text;
                        lngRecordType = ((Label)(e.Item.FindControl("lblRecType"))).Text;
                        lngDocExt = ((Label)(e.Item.FindControl("lblDocExtension"))).Text;
                        int picid = Convert.ToInt32(((Label)(e.Item.FindControl("lbldid"))).Text);
                        string[] key = { "@docid", "@RecType", "@DocNum", "@DocExt" };
                        object[] value1 = { Convert.ToInt32(lngDocID), Convert.ToInt32(lngRecordType), Convert.ToInt32(lngDocNum), lngDocExt };
                        ClsDb.ExecuteSP("usp_HTS_Delete_ScanDoc", key, value1);

                        BindGrid();
                    }
                    catch (Exception)
                    {
                        lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
                    }
                }
                if (e.CommandName == "OCR")
                {
                    try
                    {
                        lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                        string did = (((Label)(e.Item.FindControl("lbldid"))).Text);
                        string picDestination;
                        string[] key3 ={ "@sbookid" };
                        object[] value3 ={ Convert.ToInt32(lngDocID) };
                        DataSet ds_bid = ClsDb.Get_DS_BySPArr("usp_hts_get_totalpicsbybookid", key3, value3);
                        for (int i = 0; i < ds_bid.Tables[0].Rows.Count; i++)
                        {
                            did = ds_bid.Tables[0].Rows[i]["docid"].ToString();
                            string picName = lngDocID + "-" + did;
                            //picDestination= Server.MapPath("images/")+ picName + ".jpg"  ; //DestinationImage


                            picDestination = ConfigurationSettings.AppSettings["NTPATHScanImage1"].ToString() + picName + ".jpg"; //DestinationImage
                            clsGeneralMethods CGeneral = new clsGeneralMethods();
                            string ocr_data = CGeneral.OcrIt(picDestination);
                            string[] key2 = { "@doc_id", "@docnum_2", "@data_3" };
                            object[] value2 = { Convert.ToInt32(lngDocID), Convert.ToInt32(did), ocr_data };
                            //call sP and get the book ID back from sP
                            ClsDb.InsertBySPArr("usp_scan_insert_tblscandata", key2, value2);
                        }
                        BindGrid();
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
                    }
                }
                if (e.CommandName == "View")
                {
                    ViewState["sdoc_id"] = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                    ViewState["sdocnum"] = (((Label)(e.Item.FindControl("lbldid"))).Text);
                    ViewState["sdoctypedesc"] = (((Label)(e.Item.FindControl("lblDocType"))).Text);
                    ViewState["sdocdesc"] = (((Label)(e.Item.FindControl("lblDocDescription"))).Text);
                    Response.Redirect("ViewOCR.aspx?search=" + ViewState["vSearch"].ToString() + "&casenumber=" + ViewState["vTicketId"].ToString() + "&sdoc_id=" + ViewState["sdoc_id"].ToString() + "&sdocnum=" + ViewState["sdocnum"].ToString() + "&sdoctypedesc=" + ViewState["sdoctypedesc"].ToString() + "&sdocdesc=" + ViewState["sdocdesc"].ToString());
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnGenerateDocs_Click(object sender, EventArgs e)
        {
            try
            {

                ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketId"]);

                // Save Case Summary Page
                string name = ViewState["vTicketId"].ToString() + "-Matter Summary-" + DateTime.Now.ToFileTime() + ".pdf";
                string path = ViewState["vNTPATHCaseSummary"].ToString();// +name;
                ClsCase.CreateCaseSummary(path, name);

                //Comment By Khalid
                //string[] key_2 ={ "@ticketid", "@subject", "@notes", "@employeeid" };
                //object[] value_2 ={ Convert.ToInt32(ViewState["vTicketId"]), "<a href=\"javascript:window.open('../DOCSTORAGE/CaseSummary/" + name + "');void('');\">Case Summary</a>", "Case Summary Report", ClsPayments.EmpID.ToString() };
                //ClsDb.InsertBySPArr("sp_Add_Notes", key_2, value_2);

                int TicketID = Convert.ToInt32(ViewState["vTicketId"]);

                string[] keys ={ "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath" };
                object[] values ={ TicketID, System.DateTime.Now.Date, System.DateTime.Now.Date, 1, 9, Convert.ToInt32(txtempid.Text), name };
                ClsDb.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                //Open Summary Page
                string path2 = "../ClientInfo/CaseSummaryNew.aspx?casenumber=" + ViewState["vTicketId"].ToString() + "&search=" + ViewState["vSearch"].ToString();
                string prop = "height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no";
                Response.Write("<script>window.open('" + path2 + "','" + "','" + prop + "');</script>");

                ViewState["vTicketId"] = TicketID;
                ViewState["vEmpID"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));

                BindGrid();
            }
            catch (Exception ex)
            {
                // lblMessage.Text = ex.Message + "----" +ViewState["vFullPath"].ToString();
                lblMessage.Text = "unable to get the document";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddl_SubDocType.Items.Clear();
                string[] key ={ "@DocTypeID" };
                object[] value1 ={ Convert.ToInt32(ddlDocType.SelectedValue) };
                DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_Get_SubDocTypeBYDocTypeID", key, value1);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ddl_SubDocType.Items.Add(new ListItem("-----", "0"));
                }
                else
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string Description = ds.Tables[0].Rows[i]["SubDocType"].ToString().Trim();
                        string ID = ds.Tables[0].Rows[i]["SubDocTypeID"].ToString().Trim();
                        ddl_SubDocType.Items.Add(new ListItem(Description, ID));
                    }
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dl_generatedocs_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            LinkButton lb = (LinkButton)e.Item.FindControl("lb_docs");
            if (lb != null)
            {
                lb.Attributes.Add("onclick", "return PrintLetter(" + lb.CommandArgument + ")");
            }
        }

        protected void lb_docs_Command(object sender, CommandEventArgs e)
        {
            //Useless Code
            //string st = e.CommandArgument.ToString();
        }

        #endregion

        #region Methods

        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }

        void BindControls()
        {
            try
            {
                //ClsDb.FetchValuesInWebControlBysp(cmbDocType,"usp_Get_All_DocType","DocType","DocTypeID");
                ddlDocType.Items.Clear();

                //cmbDocType.Items.Add(new ListItem("Select","-1"));
                DataSet ds = ClsDb.Get_DS_BySP("usp_Get_All_DocType");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string Description = ds.Tables[0].Rows[i]["DocType"].ToString().Trim();
                    string ID = ds.Tables[0].Rows[i]["DocTypeID"].ToString().Trim();
                    ddlDocType.Items.Add(new ListItem(Description, ID));

                }
                //khalid 14
                //dl_generatedocs.DataSource = ds;
                //dl_generatedocs.DataBind();
                //ddl_SubDocType.Items.Clear();
                ddl_SubDocType.Items.Add(new ListItem("-----", "0"));
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        public void BindGrid()
        {
            try
            {
                string[] key ={ "@TicketID" };
                object[] value ={ ViewState["vTicketId"].ToString() };
                DataSet DS = ClsDb.Get_DS_BySPArr("usp_hts_GetAllScanDocs", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    dgrdDoc.DataSource = DS;
                    dgrdDoc.DataBind();
                    BindLinks();
                }
                else
                {
                    dgrdDoc.DataSource = DS;
                    dgrdDoc.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetValuesForGC()
        {               
            ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
            ClsCase.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
            ClsCase.GeneralComments = txtGeneralComments.Text.ToString();
        }
        
        private void DisplayInfo()
        {
            try
            {
                string[] key ={ "@TicketID_PK" };
                object[] value ={ Convert.ToInt32(ViewState["vTicketId"]) };
                DataSet DS = ClsDb.Get_DS_BySPArr("USP_HTS_Get_ClientInfo", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lbl_FirstName.Text = DS.Tables[0].Rows[0]["Firstname"].ToString();
                    lbl_LastName.Text = DS.Tables[0].Rows[0]["Lastname"].ToString();
                    lbl_CaseCount.Text = DS.Tables[0].Rows[0]["CaseCount"].ToString();
                    hlnk_MidNo.Text = DS.Tables[0].Rows[0]["Midnum"].ToString();
                    txtGeneralComments.Text = DS.Tables[0].Rows[0]["GeneralComments"].ToString();
                    string casetype = ViewState["vSearch"].ToString();

                    switch (casetype)
                    {
                        case "0":
                            hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            break;
                        case "1":
                            hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
               
        private void BindLinks()
        {
            long lngDocID;
            long RecType;
            long lngDocNum;
            string DocExt;
            string Event = "";
            try
            {
                foreach (DataGridItem dgItem in dgrdDoc.Items)
                {
                    lngDocID = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocID"))).Text);
                    lngDocNum = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocNum"))).Text);
                    RecType = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblRecType"))).Text);
                    DocExt = System.Convert.ToString(((Label)(dgItem.FindControl("lblDocExtension"))).Text);
                    Event = System.Convert.ToString(((Label)(dgItem.FindControl("lblEvent"))).Text);

                    if (lngDocNum == -1)
                    {
                        string lnk = "javascript: return OpenPopUpNew('../ClientInfo/frmPreview.aspx?RecordID=" + lngDocID + "')";
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", lnk);
                    }
                    else
                    {
                        ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + lngDocID.ToString() + "," + RecType + "," + lngDocNum.ToString() + ",'" + DocExt + "'" + ");");
                    }




                    //if (ClsSession.GetSessionVariable("sAccessType",this.Session).ToString()!="2")
                    {
                        if (((Label)dgItem.FindControl("lblDocType")).Text.Trim() == "Resets")
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                        }
                        if (Event == "Print" || Event == "")
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                            ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                        }

                    }

                    //Allow Only Primary User To Delete Document
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "2")
                    {

                        DateTime recDate = Convert.ToDateTime(((Label)dgItem.FindControl("lblDateTime")).Text);
                        DateTime CurDate = DateTime.Now;
                        int a = CurDate.Day - recDate.Day;
                        if (a > 1)
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;
                        }
                        else
                        {
                            ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Attributes.Add("OnClick", "return DeleteCheck();");
                        }
                    }
                    else
                        ((LinkButton)dgItem.FindControl("lnkbtn_Delete")).Visible = false;

                    if (((Label)dgItem.FindControl("lblocrid")).Text != "0")
                    {
                        ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                        ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = true;
                    }
                    if (((Label)dgItem.FindControl("lblDocExtension")).Text != "000" && ((Label)dgItem.FindControl("lblDocExtension")).Text != "JPG")
                    {
                        ((LinkButton)dgItem.FindControl("lnkbtn_OCR")).Visible = false;
                        ((LinkButton)dgItem.FindControl("lnkbtn_VOCR")).Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        
        private void ShowHideLettersType()
        {
            try
            {
                // ddlLetterType.Visible = true;
                // btnGenerateDocs.Visible = true;
                string[] key ={ "@ticketid" };
                object[] value ={ Convert.ToInt32(ViewState["vTicketId"]) };
                DataSet DS = ClsDb.Get_DS_BySPArr("USP_HTS_GetCaseInfo", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {

                    if (Convert.ToInt32(DS.Tables[0].Rows[0]["ActiveFlag"]) == 1)
                    {

                        //Comment By Zeeshan Ahmed No Need of this because nothing Is Modified in the Condition                           
                        //  if (ddlOperation.Items.Count == 3)
                        //   {
                        //Comment By Khalid
                        // ddlOperation.Items.Add(new ListItem("Generate", "3"));
                        //   }

                        ddlLetterType.Items.Clear();

                        //Comment by Khalid
                        //ddlLetterType.Items.Add(new ListItem("--Choose--", "-1"));
                        //ddlLetterType.Items.Add(new ListItem("Matter Summary", "1"));
                        //ddlLetterType.Items.Add(new ListItem("Payment Receipt", "3"));

                        //Adding Rows To Letter Type Table
                        Addrows("Matter Summary", "1");
                        Addrows("Payment Receipt", "3");


                        string[] key1 ={ "@TicketID" };
                        object[] value1 ={ Convert.ToInt32(ViewState["vTicketId"]) };
                        DataSet DS_Underlying = ClsDb.Get_DS_BySPArr("USP_HTS_Get_PaymentInfo_ButtonStatus", key1, value1);
                      
                        if (DS_Underlying.Tables[0].Rows.Count > 0)
                        {
                              # region previous visibility logic
                            for (int i = 0; i < DS_Underlying.Tables[0].Rows.Count; i++)
                            {//payment info if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2))

                                if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 2))
                                {

                                    //ddlLetterType.Items.Add(new ListItem("Bond", "4"));   
                                    Bond = true;
                                    lbl_courtid.Text = DS_Underlying.Tables[0].Rows[i]["CourtId"].ToString();
                                }
                                else
                                {//p-info  if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 12)
                                    if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 12)
                                    {
                                        // ddlLetterType.Items.Add(new ListItem("Bond", "4"));   
                                        Bond = true;
                                        lbl_courtid.Text = DS_Underlying.Tables[0].Rows[i]["CourtId"].ToString();
                                    }

                                }

                                DateTime dcourt = Convert.ToDateTime(DS_Underlying.Tables[0].Rows[i]["CourtDateMain"]);
                                if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 3 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 4 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3003))
                                {//p-info     if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 3 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 4 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3003))


                                    //ddlLetterType.Items.Add(new ListItem("Trial Letter", "2")); 
                                    TrialLetter = true;
                                    if (DS.Tables[0].Rows[0]["email"].ToString() != "")
                                    {
                                        // Following line was commented by Farhan Sabir to address bug#1721
                                        //btn_EmailTrial.Visible = true;
                                        ViewState["emailid"] = DS.Tables[0].Rows[0]["email"].ToString();

                                    }
                                    else
                                    {
                                        ViewState["emailid"] = "";

                                    }
                                }
                                else
                                    //Inside court status in Arr
                                    if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 4 && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtId"]) == 3001 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtId"]) == 3002 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtId"]) == 3003))
                                    {

                                        //ddlLetterType.Items.Add(new ListItem("Trial Letter", "2"));
                                        TrialLetter = true;
                                        if (DS.Tables[0].Rows[0]["email"].ToString() != "")
                                        {
                                            // Following line was commented by Farhan Sabir to address bug#1721
                                            //btn_EmailTrial.Visible = true;
                                            ViewState["emailid"] = DS.Tables[0].Rows[0]["email"].ToString();

                                        }
                                        else
                                        {
                                            ViewState["emailid"] = "";

                                        }
                                    }
                                // Checking for LETTER OF REP //status in Waiting,Arr,Waiting2nd,Bond and court not to be Lubbok/Mykawa

                                //original if ((Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 11 || Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["categoryid"]) == 12) && (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3003))
                          if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["CourtID"]) != 3003)
                          
                                {//p-info  if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3003)
                          

                                    //ddlLetterType.Items.Add(new ListItem("Letter Of Rep (LOR)", "6"));
                                    LetterOfRep = true;
                                }

                                if (Convert.ToInt32(DS_Underlying.Tables[0].Rows[i]["ContinuanceAmount"]) > 0)
                                {
                                    //   ddlLetterType.Items.Add(new ListItem("Continuance", "5"));
                                    Continuance = true;
                                }


                            }
                        }
                              #endregion




                    }
                }
                else
                {
                    ddlLetterType.Visible = false;
                    btnGenerateDocs.Visible = false;
                    if (ddlOperation.Items.Count > 3)
                    {
                        ddlOperation.Items.Remove("Generate");
                    }
                }
                if (Bond == true)
                {
                    //Comment by Khalid
                    // ddlLetterType.Items.Add(new ListItem("Bond", "4"));
                    Addrows("Bond", "4");
                }
                if (TrialLetter == true)
                {
                    //Comment by Khalid
                    //ddlLetterType.Items.Add(new ListItem("Trial Letter", "2"));
                    Addrows("Trial Letter", "2");
                }
                if (LetterOfRep == true)
                {
                    //Comment by Khalid
                    //ddlLetterType.Items.Add(new ListItem("Letter Of Rep (LOR)", "6"));
                    Addrows("Letter Of Rep (LOR)", "6");
                }
                if (Continuance == true)
                {
                    //Comment by Khalid
                    //ddlLetterType.Items.Add(new ListItem("Continuance", "5"));
                    Addrows("Continuance", "5");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void GeneratePDF(string path, string name)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();
            //theDoc.HtmlOptions.BrowserWidth = 800;
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                //theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }


            ViewState["vFullPath"] = ViewState["vNTPATHCaseSummary"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHCaseSummary"].ToString();
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();

        }

        private void CheckSpilitCases()
        {
            try
            {
                string[] keys = { "@ticketid" };
                object[] values = { Request.QueryString["casenumber"] };
                DataTable dtSplit = new DataTable();
                dtSplit = ClsDb.Get_DT_BySPArr("USP_HTS_PAYMENTINFO_GET_SPLITCaseCount", keys, values);

                if (Convert.ToInt32(dtSplit.Rows[0][0].ToString()) > 0)
                    lbl_IsSplit.Text = "1";
                else
                    lbl_IsSplit.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void IsAlreadyInBatchPrint()
        {
            try
            {
                lbl_court.Text = Request.QueryString["casenumber"].ToString();
                string[] keys = { "@TicketID" };
                object[] values = { Request.QueryString["casenumber"] };
                DataTable dtBatchPrint = new DataTable();
                dtBatchPrint = ClsDb.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
                if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                    lbl_IsAlreadyInBatchPrint.Text = "1";
                else
                    lbl_IsAlreadyInBatchPrint.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void RefreshControls()
        {
            try
            {
                ddlDocType.SelectedValue = "1";
                ddl_SubDocType.Items.Clear();
                ddl_SubDocType.Items.Add(new ListItem("-----", "0"));
                ddlLetterType.SelectedValue = "-1";
                txtDescription.Text = "";
                ddlOperation.SelectedValue = "0";

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        //Missing Khalid Comments
        public void Addrows(string name, string val)
        {
            DataRow dr;
            dr = LetterType.NewRow();
            dr["name"] = name;
            dr["value"] = val;
            LetterType.Rows.Add(dr);
            dl_generatedocs.DataSource = LetterType;
            dl_generatedocs.DataBind();


        }

        #endregion

    }
}

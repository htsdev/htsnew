<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" Codebehind="ScanDocument.aspx.cs" AutoEventWireup="false" Inherits="lntechNew.PaperLess.ScanDocument" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Scan Document</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../Styles.css" rel="stylesheet" type="text/css" />
		<SCRIPT src="../Scripts/Dates.js" type="text/javascript"></SCRIPT>
		<style type="text/css">A:link { COLOR: #0066cc; TEXT-DECORATION: none }
	A:visited { COLOR: #0066cc; TEXT-DECORATION: none }
	A:active { TEXT-DECORATION: none }
	A:hover { COLOR: #0066cc; TEXT-DECORATION: underline }
	.style1 { COLOR: #ffffff }
	.style2 { FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-STYLE: normal; FONT-FAMILY: Tahoma }
	.style4 { FONT-SIZE: 8pt }
		</style>
		<script>
						
			function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				return false;
			}
			
			function openUpload()
			{
				window.open ('uploadFile.aspx', null, 'status=yes,left=20,top=20,height=600, width=850,scrollbars=no'); 
				return false;
			}
			function DeleteCheck()
			{
				var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
				if(x)
				{
				}
				else
				{
					return false;
				}
			}
					
			function ShowHide()
			{
				window.open ("ScanOnly.aspx?casenumber=<%=ViewState["vTicketId"]%>&search=<%=ViewState["vSearch"]%>",'', 'status=yes,left=20,top=20,height=600, width=850,scrollbars=yes');
				return false;
			/*
				document.getElementById("first").style.display='none';
				document.getElementById("first1").style.display='none';
				document.getElementById("new").style.display='block';
				document.getElementById("cmbDocType1").selectedIndex=document.getElementById("cmbDocType").selectedIndex;
				document.getElementById("txtcmb").value="2";
			*/	
			}
			
			function HideShow()
			{
				document.getElementById("first").style.display='block';
				document.getElementById("first1").style.display='block';
				document.getElementById("new").style.display='none';
				document.getElementById("txtcmb").value="1";
			}
			
			function onSelectionChanged()
			{
				document.getElementById("txtbID").value="";
				document.getElementById("txtDescription").value="";
			}
			
			function check()
			{
				if(document.getElementById("txtcmb").value=="1" || document.getElementById("txtcmb").value=="0")
				{
					document.getElementById("first").style.display='block';
					document.getElementById("first1").style.display='block';
					document.getElementById("new").style.display='none';
				}
				else
				{
					document.getElementById("new").style.display='block';
					document.getElementById("first").style.display='none';
					document.getElementById("first1").style.display='none';
				}				
			}
			
		</script>

        

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />

        
	</HEAD>
	<body onload="javascript: check();">
		<form id="Form1" method="post" runat="server">
			
			<TABLE id="TableMain" width=780 cellSpacing="0" cellPadding="0"
				align="center" border="0">
			 <tbody>
			    <tr>
						<td style="HEIGHT: 14px" colSpan="4">
                            &nbsp;<uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
				</tr>
			<tr>
			<td>
			<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">	
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
				<TR>
					<TD class="clssubhead" style="WIDTH: 780px; HEIGHT: 35px" background="../Images/subhead_bg.gif"
						colSpan="2">&nbsp;&nbsp;Scan Document</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 11px" colSpan="2">&nbsp;</TD>
				</TR>
				<TR id="first">
					<TD class="clsLeftPaddingTable" width="55%" align="right">
                        &nbsp;<asp:button id="btnScan" runat="server" CssClass="clsbutton" Width="144px" Height="21px" Text="Scan New Document"></asp:button>&nbsp;
					</TD>
					<TD class="clsLeftPaddingTable" width="45%">&nbsp;
						<asp:button id="btnUpload" runat="server" Width="96px" CssClass="clsbutton" Text="Upload File"
							Height="21px"></asp:button></TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" align="left" height="11" colSpan="2">&nbsp;</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" align="left" height="11" colSpan="2">&nbsp;</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" align="left" height="11" colSpan="2">&nbsp;</TD>
				</TR>
				<TR id="first1">
					<TD class="clsLeftPaddingTable" colSpan="2">
						Show Document&nbsp;<asp:dropdownlist id="cmbDocType" runat="server" CssClass="clsInputCombo" Width="200px" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" align="left" height="11" colSpan="2">&nbsp;</TD>
				</TR>
				<tr>
					<td id="new" colSpan="2">
						<table width="780">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 909px; HEIGHT: 22px">Document Type
									<asp:dropdownlist id="cmbDocType1" runat="server" CssClass="clsInputCombo" Width="200px" AutoPostBack="True"></asp:dropdownlist>&nbsp; 
									Description
									<asp:textbox id="txtDescription" runat="server" CssClass="clsinputadministration" Width="192px"
										MaxLength="50"></asp:textbox><br>
									&nbsp;
								</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 380px; HEIGHT: 22px" align="left"><INPUT class="clsbutton" id="btnScn" style="WIDTH: 72px; HEIGHT: 20px" onclick="JavaScript: StartScan();"
										type="button" value="Scan Now" name="Button1" runat="server">&nbsp;<INPUT class="clsbutton" id="btncancel" onclick="javascript: HideShow();" type="button"
										value="Cancel" name="btncancel" runat="server">&nbsp;&nbsp; <INPUT id="Adf" type="checkbox" runat="server">Use 
									Feeder
								</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="left" colSpan="2">&nbsp;</TD>
							</TR>
						</table>
					</td>
				</tr>
				<TR>
					<TD align="left" colSpan="2"><asp:datagrid id="dgrdDoc" runat="server" Width="780px" PageSize="20" AutoGenerateColumns="False"
							AllowSorting="True" OnItemCommand="DoGetQueryString" Font-Size="2px" Font-Names="Verdana">
							<FooterStyle CssClass="GrdFooter"></FooterStyle>
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Document Type">
									<HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblDocID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>' Visible="false">
										</asp:Label>
										<asp:Label id=lblDocType runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOCREF") %>' Visible="true">
										</asp:Label>
										<asp:Label id=lbldid runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblocrid runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>' Visible="False">
										</asp:Label>
										<asp:Label id="lblDocNum" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>' Visible="false">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Document Description">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblDocDescription runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ResetDesc") %>' CssClass="GrdLbl">
										</asp:Label>
										<asp:Label id=lblDocExtension runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>' CssClass="GrdLbl" Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Update Date Time">
									<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblDateTime runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATEDATETIME") %>' CssClass="GrdLbl" Visible="true">
										</asp:Label>&nbsp;
										<asp:Label id=lblAbb runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>' CssClass="GrdLbl">
										</asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="User Name">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblAbb1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>' CssClass="GrdLbl">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Preview">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="imgPreviewDoc" Width="19" CommandName="DoGetScanDoc" ImageUrl="../images/Preview.gif"
											Runat="server"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Pages">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Label id=lblDocCount runat="server" CssClass="GrdLbl" Width="32px" Text='<%# DataBinder.Eval(Container, "DataItem.ImageCount") %>'>
										</asp:Label>
										<asp:Label id=lblRecType runat="server" CssClass="GrdLbl" Width="10px" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Options">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle Font-Size="X-Small" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:LinkButton id="lnkbtn_Delete" runat="server" CommandName="Delete">Delete,</asp:LinkButton>&nbsp;
										<asp:LinkButton id="lnkbtn_OCR" runat="server" CommandName="OCR">OCR</asp:LinkButton>
										<asp:LinkButton id="lnkbtn_VOCR" runat="server" CommandName="View" Visible="False">View OCR</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="" Font-Names="Arial" PrevPageText="" HorizontalAlign="Center" PageButtonCount="50"
								CssClass="GrdFooter" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="2" height="11">
					</td>
				</tr>
				<TR>
					<TD align="center" colSpan="2">&nbsp;
						<asp:label id="lblMessage" runat="server" Width="421px" Height="16px" ForeColor="Red"></asp:label></TD>
				</TR>
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="2" height="11">
					</td>
				</tr>
				<TR>
					<TD style="WIDTH: 760px" align="center" colSpan="2"><uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
				</TR>
				<tr>
					<td style="VISIBILITY: hidden" colspan="2"><asp:textbox id="txtImageCount" runat="server"></asp:textbox><asp:textbox id="txtQuery" runat="server"></asp:textbox><asp:textbox id="txtShow" runat="server">0</asp:textbox><asp:textbox id="txtempid" runat="server"></asp:textbox><asp:textbox id="txtsessionid" runat="server"></asp:textbox>
						<asp:TextBox id="txtcmb" runat="server">0</asp:TextBox>
						<asp:TextBox id="txtSrv" runat="server"></asp:TextBox>
						<asp:TextBox id="txtbID" runat="server"></asp:TextBox></td>
				
				</tr>
				</TABLE>
				</td>
				</tr>
				</tbody>
			</TABLE>
		</form>
		<DIV id="ErrorString"></DIV>
		<IMG height="1" src="" width="1" name="Img1">
		<script language="JavaScript">
		function StartScan()
		{
			if(document.getElementById("cmbDocType1").value=="-1" || document.getElementById("cmbDocType1").value=="1")
			{
				alert("Plese Select valid document type to scan");
				document.getElementById("cmbDocType1").focus();
				return false;
			}
			else
			{
				var adf=document.Form1.Adf.checked;
				var desc=document.Form1.txtDescription.value;
				var doctype=document.Form1.cmbDocType1.options[document.Form1.cmbDocType1.selectedIndex].text;				
				window.open ("ScanPopup.aspx?ticketid=<%=ViewState["vTicketId"]%>&doctype=" + doctype + "&desc="+ desc + "&adf=" + adf,'', 'status=yes,left=20,top=20,height=50, width=50,scrollbars=no');
				return false;
			}
		}
		</script>
	</body>
</HTML>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaperLess.ScanOnly" Codebehind="ScanOnly.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ScanOnly</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Dates.js" type="text/javascript"></SCRIPT>
		<style type="text/css">A:link { COLOR: #0066cc; TEXT-DECORATION: none }
	A:visited { COLOR: #0066cc; TEXT-DECORATION: none }
	A:active { TEXT-DECORATION: none }
	A:hover { COLOR: #0066cc; TEXT-DECORATION: underline }
	.style1 { COLOR: #ffffff }
	.style2 { FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-STYLE: normal; FONT-FAMILY: Tahoma }
	.style4 { FONT-SIZE: 8pt }
		</style>
		<%=Session["objTwain"].ToString()%>
		<SCRIPT LANGUAGE="JavaScript">

		var User
		var Server
		var RegCode
	
		User = "Greg Sullo"
		Server = "ln.legalhouston.com"
		//RegCode = "Gt6Rir4P+Tu5QJYENT4oXnY/ASuT0Z+Bt4GCnPuoONvd83dWHe+Pv9ntZOT/p4nwm1ZMs6b6SLudWGV+V8Tr1LqOnrLV9mluDdsZbgpCM6S+vwKTmBhPkfrh5h6TsLe621OpRZWBRyervZ70zu2+0yEOdEbv4mR/Dw+dh8Ne/NH0"
		RegCode = "b+iPpr0fScoAl8xeD4XKbDD4xp1IRZCu3QADNYyZxBunmboA7HtIPr/6/16McLUm5b/wNqhNfLJWpwEliBEqGt4Dz/A2gwTPtdUtIraEUi/1zTmFSeOr4ELTO1LUo4iWbT1ShRfo76RDN+kCLmTkt/5wb690DHA9O7lReQTIK7nc"
		//alert(User)
		VSTwain1.Register(User,Server,RegCode)
		
		</SCRIPT>
		<script>
						
			function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				return false;
			}
			
			
			function DeleteCheck()
			{
				var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
				if(x)
				{
				}
				else
				{
					return false;
				}
			}
					
			
			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; WIDTH: 780px" cellSpacing="0" cellPadding="0"
				width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 780px; HEIGHT: 5px" colSpan="2"></TD>
				</TR>
			</TABLE>
			<table id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
				<TR>
					<TD class="clssubhead" style="WIDTH: 780px; HEIGHT: 35px" background="../Images/subhead_bg.gif"
						colSpan="2">&nbsp;&nbsp;Scan Document</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" align="left" height="11" colSpan="2">&nbsp;</TD>
				</TR>
				<tr>
					<td id="new" colSpan="2">
						<table width="780">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 909px; HEIGHT: 22px">Document Type
									<asp:dropdownlist id="cmbDocType1" runat="server" CssClass="clsInputCombo" Width="200px" AutoPostBack="True"></asp:dropdownlist>&nbsp; 
									Description
									<asp:textbox id="txtDescription" runat="server" CssClass="clsinputadministration" Width="192px"
										MaxLength="50"></asp:textbox><br>
									&nbsp;
								</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 380px; HEIGHT: 22px" align="left"><INPUT class="clsbutton" id="btnScn" style="WIDTH: 72px; HEIGHT: 20px" onclick="JavaScript: StartScan();"
										type="button" value="Scan Now" name="Button1" runat="server" onserverclick="btnScn_ServerClick1" >&nbsp;&nbsp;&nbsp;
									<INPUT id="Adf" type="checkbox" runat="server" NAME="Adf" checked="CHECKED">Use Feeder
								</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" align="left" colSpan="2">&nbsp;</TD>
							</TR>
						</table>
					</td>
				</tr>
				<TR>
					<TD align="left" colSpan="2"><asp:datagrid id="dgrdDoc" runat="server" Width="780px" PageSize="20" AutoGenerateColumns="False"
							AllowSorting="True" OnItemCommand="DoGetQueryString" Font-Size="2px" Font-Names="Verdana">
							<FooterStyle CssClass="GrdFooter"></FooterStyle>
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Document Type">
									<HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblDocID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>' Visible="false">
										</asp:Label>
										<asp:Label id=lblDocType runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOCREF") %>' Visible="true">
										</asp:Label>
										<asp:Label id=lbldid runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblocrid runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>' Visible="False">
										</asp:Label>
										<asp:Label id="lblDocNum" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>' Visible="false">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Document Description">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblDocDescription runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ResetDesc") %>'>
										</asp:Label>
										<asp:Label id=lblDocExtension runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Update Date Time">
									<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblDateTime runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATEDATETIME") %>' CssClass="GrdLbl" Visible="true">
										</asp:Label>&nbsp;
										<asp:Label id=lblAbb runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>' CssClass="GrdLbl">
										</asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="User Name">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>' CssClass="GrdLbl">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Preview">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="imgPreviewDoc" Width="19" CommandName="DoGetScanDoc" ImageUrl="../images/Preview.gif"
											Runat="server"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Pages">
									<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Label id=lblDocCount runat="server" CssClass="GrdLbl" Width="32px" Text='<%# DataBinder.Eval(Container, "DataItem.ImageCount") %>'>
										</asp:Label>
										<asp:Label id=lblRecType runat="server" CssClass="GrdLbl" Width="10px" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="" Font-Names="Arial" PrevPageText="" HorizontalAlign="Center" PageButtonCount="50"
								CssClass="GrdFooter" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="2" height="11">
					</td>
				</tr>
				<TR>
					<TD align="center" colSpan="2">&nbsp;
						<asp:label id="lblMessage" runat="server" Width="421px" Height="16px" ForeColor="Red"></asp:label></TD>
				</TR>
				<tr>
					<td width="780" background="../Images/separator_repeat.gif" colSpan="2" height="11">
					</td>
				</tr>
				<TR>
					<TD style="WIDTH: 760px" align="left" colSpan="2"><uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
				</TR>
				<tr>
					<td style="VISIBILITY: hidden" colspan="2"><asp:textbox id="txtImageCount" runat="server"></asp:textbox><asp:textbox id="txtQuery" runat="server"></asp:textbox><asp:textbox id="txtempid" runat="server"></asp:textbox><asp:textbox id="txtsessionid" runat="server"></asp:textbox>
						<asp:TextBox id="txtSrv" runat="server"></asp:TextBox>
						<asp:TextBox id="txtbID" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox></td>
				</tr>
			</table>
		</form>
		<DIV id="ErrorString"></DIV>
		<IMG height="1" src="" width="1" name="Img1"> 
		<!-- added by ozair-->
		<SCRIPT language="JavaScript">
function StartScan()
{
	if(document.getElementById("cmbDocType1").value=="-1" || document.getElementById("cmbDocType1").value=="1")
	{
		alert("Plese Select valid document type to scan");
		document.getElementById("cmbDocType1").focus();
		return false;
	}	
		
	if (VSTwain1.maxImages == null)
	{
	  document.getElementById('ErrorString').innerHTML = "<a href=http://www.vintasoft.com/vstwain-dotnet-faq.html#faq17>Click here to see how to set the .NET Framework Security settings</a>";
	  alert("Your .NET Framework Security settings must be configured to run the components in your browser.");
	  return
	}	
	imagesCounter = 0
	VSTwain1.StartDevice()
	VSTwain1.showUI = false
	VSTwain1.maxImages = 1
	if (VSTwain1.SelectSource())
	{
	  VSTwain1.OpenDataSource()
	  //VSTwain1.unitOfMeasure=0;
	  VSTwain1.pixelType = 1;//PixelType.RGB;
      VSTwain1.resolution = 100;
	  if (document.Form1.Adf.checked == false)
	    VSTwain1.xferCount = 1
	  else
	    VSTwain1.xferCount = -1
	  VSTwain1.feederEnabled = document.Form1.Adf.checked
	  //
	  //var ii=1
	 
	  //
	  while (VSTwain1.AcquireModal())
	  {
	    //imagesCounter = imagesCounter + UploadToHttpServer()
	    var sid= document.getElementById("txtsessionid").value;
		var eid= document.getElementById("txtempid").value;
		var fpat=document.getElementById("txtnoofscandoc").value+sid+eid+"img.jpg"
		
		imgPath = "<%=ViewState["vNTPATHScanTemp"]%>"+fpat
	    if (VSTwain1.SaveImage(0,imgPath))
	    {
	      document.Img1.src = imgPath
	    }
	    else
	    {
	      alert(VSTwain1.errorString)
	    }
  	    var ii=Number(document.getElementById("txtnoofscandoc").value)+1
  	    document.getElementById("txtnoofscandoc").value=ii;	    
	  }
	  
	  	  
	  if (document.Form1.Adf.checked == false)
	  {
		var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		if(x)
		{			
			StartScan();
		}		
		else
		{
			document.getElementById("txtbID").value="";
			document.getElementById("txtnoofscandoc").value="1";
			document.Form1.Adf.checked = true;				
		}
	  }
	  VSTwain1.CloseDataSource()
	  document.getElementById("txtnoofscandoc").value="1";
	}
}
		</SCRIPT>
		<!-- added by ozair-->
	</body>
</HTML>

﻿using System;
using System.Text;

namespace HtpPolmTests
{
    internal class TestHelper
    {
        public static string AddedBdText { get; set; }
        public static string AddedBdText2 { get; set; }
        public static int AddedBdId { get; set; }
        public static int AddedBdId2 { get; set; }

        public static string AddedCsText { get; set; }
        public static string AddedCsText2 { get; set; }
        public static int AddedCsId { get; set; }
        public static int AddedCsId2 { get; set; }

        public static string AddedCtText { get; set; }
        public static string AddedCtText2 { get; set; }
        public static int AddedCtId { get; set; }
        public static int AddedCtId2 { get; set; }

        public static string AddedDvText { get; set; }
        public static string AddedDvText2 { get; set; }
        public static int AddedDvId { get; set; }
        public static int AddedDvId2 { get; set; }

        public static string AddedLgText { get; set; }
        public static string AddedLgText2 { get; set; }
        public static int AddedLgId { get; set; }
        public static int AddedLgId2 { get; set; }

        public static string AddedSeText { get; set; }
        public static string AddedSeText2 { get; set; }
        public static int AddedSeId { get; set; }
        public static int AddedSeId2 { get; set; }

        public static string AddedQlmdText { get; set; }
        public static string AddedQlmdText2 { get; set; }
        public static int AddedQlmdId { get; set; }
        public static int AddedQlmdId2 { get; set; }


        /// <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <param name="lowerCase">If true, generate lowercase string</param>
        /// <returns>Random string</returns>
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < size; i++)
            {
                char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
    }
}
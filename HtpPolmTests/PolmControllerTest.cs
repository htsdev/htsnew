﻿using HTP.ClientController;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Collections.Generic;
using POLM.DataTransferObjects;
using System.Data;

namespace HtpPolmTests
{


    /// <summary>
    ///This is a test class for PolmControllerTest and is intended
    ///to contain all PolmControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PolmControllerTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #region BodilyDamage

        #region AddBodilyDamage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddBodilyDamage(null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddBodilyDamage(string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddBodilyDamageValidValueTest()
        {
            TestHelper.AddedBdText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddBodilyDamage(TestHelper.AddedBdText, true);
            if (actual)
                TestHelper.AddedBdId = target.GetBodilyDamageByName(TestHelper.AddedBdText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddBodilyDamageValidValueFalseTest()
        {
            TestHelper.AddedBdText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddBodilyDamage(TestHelper.AddedBdText2, false);
            if (actual)
                TestHelper.AddedBdId2 = target.GetBodilyDamageByName(TestHelper.AddedBdText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddBodilyDamage(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateBodilyDamage


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageMaxLengthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(1, TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(TestHelper.AddedBdId, "asdwe", true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateBodilyDamage(TestHelper.AddedBdId2, "asdwe", true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteBodilyDamage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteBodilyDamageLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteBodilyDamage(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteBodilyDamageZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteBodilyDamage(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteBodilyDamageGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteBodilyDamage(TestHelper.AddedBdId);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteBodilyDamageGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteBodilyDamage(TestHelper.AddedBdId2);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region GetAllBodilyDamage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllBodilyDamage(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllBodilyDamage(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllBodilyDamage(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetBodilyDamageByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameNullTest()
        {
            PolmController target = new PolmController();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetBodilyDamageByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetBodilyDamageByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetBodilyDamageByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetBodilyDamageByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameValidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetBodilyDamageByName(TestHelper.AddedBdText);
            Assert.AreEqual(true, (polmDto != null));
        }

        #endregion

        #region GetAllBodilyDamageOfSource

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetAllBodilyDamageOfSourceLessThanTest()
        {
            PolmController target = new PolmController();
            target.GetAllBodilyDamageOfSource(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetAllBodilyDamageOfSourceZeroTest()
        {
            PolmController target = new PolmController();
            target.GetAllBodilyDamageOfSource(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageOfSourceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            List<PolmDivisionDto> list = target.GetAllBodilyDamageOfSource(1);
            Assert.IsTrue(true);
        }


        #endregion

        #region AddBodilyDamageOfSOurce

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageOfSOurceLessThanZeroTest1()
        {
            PolmController target = new PolmController();
            target.AddBodilyDamageOfSOurce(-1, 1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageOfSOurceLessThanZeroTest2()
        {
            PolmController target = new PolmController();
            target.AddBodilyDamageOfSOurce(1, -1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageOfSOurceZeroTest1()
        {
            PolmController target = new PolmController();
            target.AddBodilyDamageOfSOurce(0, 1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageOfSOurceZeroTest2()
        {
            PolmController target = new PolmController();
            target.AddBodilyDamageOfSOurce(1, 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddBodilyDamageOfSOurceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddBodilyDamageOfSOurce(1, 1);
            Assert.IsTrue(actual);
        }


        #endregion

        #region DeleteBodilyDamageOfSOurce

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteBodilyDamageOfSOurceLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteBodilyDamageOfSOurce(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteBodilyDamageOfSOurceZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteBodilyDamageOfSOurce(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteBodilyDamageOfSOurceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteBodilyDamageOfSOurce(210);
            Assert.AreEqual(actual, actual);
        }


        #endregion

        #endregion

        #region Case Status

        #region AddCaseStatus

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCaseStatusNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddCaseStatus(null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCaseStatusEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddCaseStatus(string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddCaseStatusValidValueTest()
        {
            TestHelper.AddedCsText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddCaseStatus(TestHelper.AddedCsText, true);
            if (actual)
                TestHelper.AddedCsId = target.GetCaseStatusByName(TestHelper.AddedCsText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddCaseStatusValidValueFalseTest()
        {
            TestHelper.AddedCsText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddCaseStatus(TestHelper.AddedCsText2, false);
            if (actual)
                TestHelper.AddedCsId2 = target.GetCaseStatusByName(TestHelper.AddedCsText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCaseStatusMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddCaseStatus(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateCaseStatus


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCaseStatusNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateCaseStatus(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCaseStatusEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateCaseStatus(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCaseStatusMaxLengthTest()
        {
            PolmController target = new PolmController();
            target.UpdateCaseStatus(1, TestHelper.RandomString(65, false), true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCaseStatusLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateCaseStatus(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCaseStatusZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateCaseStatus(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateCaseStatusGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateCaseStatus(TestHelper.AddedCsId, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateCaseStatusGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateCaseStatus(TestHelper.AddedCsId2, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteCaseStatus

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteCaseStatusLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteCaseStatus(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteCaseStatusZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteCaseStatus(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteCaseStatusGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteCaseStatus(TestHelper.AddedCsId);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteCaseStatusGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteCaseStatus(TestHelper.AddedCsId2);
            Assert.IsTrue(actual);
        }

        #endregion

        #region GetAllCaseStatus

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllCaseStatusActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllCaseStatus(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllCaseStatusInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllCaseStatus(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllCaseStatusNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllCaseStatus(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetCaseStatusByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetCaseStatusByNameNullTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetCaseStatusByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetCaseStatusByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetCaseStatusByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetCaseStatusByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetCaseStatusByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetCaseStatusByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetCaseStatusByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetCaseStatusByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetCaseStatusByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetCaseStatusByNameValidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetCaseStatusByName(TestHelper.AddedCsText);
            Assert.AreEqual(true, (polmDto == null));
        }

        #endregion

        #endregion

        #region Contact Type


        #region AddContactType

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddContactTypeNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddContactType(null, true);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddContactTypeEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddContactType(string.Empty, true);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddContactTypeValidValueTest()
        {
            TestHelper.AddedCtText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddContactType(TestHelper.AddedCtText, true);
            if (actual)
                TestHelper.AddedCtId = target.GetContactTypeByName(TestHelper.AddedCtText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddContactTypeValidValueFalseTest()
        {
            TestHelper.AddedCtText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddContactType(TestHelper.AddedCtText2, false);
            if (actual)
                TestHelper.AddedCtId2 = target.GetContactTypeByName(TestHelper.AddedCtText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddContactTypeMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddContactType(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateContactType


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateContactTypeNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateContactType(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateContactTypeEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateContactType(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateContactTypeMaxLengthTest()
        {
            PolmController target = new PolmController();
            target.UpdateContactType(1, TestHelper.RandomString(65, false), true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateContactTypeLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateContactType(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateContactTypeZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateContactType(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateContactTypeGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateContactType(TestHelper.AddedCtId, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateContactTypeGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateContactType(TestHelper.AddedCtId2, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteContactType

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteContactTypeLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteContactType(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteContactTypeZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteContactType(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteContactTypeGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteContactType(TestHelper.AddedCtId);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteContactTypeGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteContactType(TestHelper.AddedCtId2);
            Assert.IsTrue(actual);
        }

        #endregion

        #region GetAllContactType

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllContactTypeActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllContactType(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllContactTypeInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllContactType(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllContactTypeNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllContactType(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetContactTypeByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetContactTypeByNameNullTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetContactTypeByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetContactTypeByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetContactTypeByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetContactTypeByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetContactTypeByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetContactTypeByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetContactTypeByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetContactTypeByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetContactTypeByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetContactTypeByNameValidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetContactTypeByName(TestHelper.AddedCtText);
            Assert.AreEqual(true, (polmDto == null));
        }

        #endregion

        #endregion

        #region Damage Value


        #region AddDamageValue

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddDamageValueNullValueTest()
        {
            PolmController target = new PolmController();
            target.AddDamageValue(null, true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddDamageValueEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddDamageValue(string.Empty, true);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddDamageValueValidValueTest()
        {
            TestHelper.AddedDvText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddDamageValue(TestHelper.AddedDvText, true);
            if (actual)
                TestHelper.AddedDvId = target.GetDamageValueByName(TestHelper.AddedDvText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddDamageValueValidValueFalseTest()
        {
            TestHelper.AddedDvText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddDamageValue(TestHelper.AddedDvText2, false);
            if (actual)
                TestHelper.AddedDvId2 = target.GetDamageValueByName(TestHelper.AddedDvText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddDamageValueMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddDamageValue(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateContactType


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateDamageValueNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateDamageValue(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateDamageValueEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateDamageValue(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateDamageValueMaxLengthTest()
        {
            PolmController target = new PolmController();
            target.UpdateDamageValue(1, TestHelper.RandomString(65, false), true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateDamageValueLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateDamageValue(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateDamageValueZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateDamageValue(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateDamageValueGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateDamageValue(TestHelper.AddedDvId, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateDamageValueGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateDamageValue(TestHelper.AddedDvId2, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteDamageValue

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteDamageValueLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteDamageValue(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteDamageValueZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteDamageValue(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteDamageValueGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteDamageValue(TestHelper.AddedDvId);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteDamageValueGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteDamageValue(TestHelper.AddedDvId2);
            Assert.IsTrue(actual);
        }

        #endregion

        #region GetAllDamageValue

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllDamageValueActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllDamageValue(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllDamageValueInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllDamageValue(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllDamageValueNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllDamageValue(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetDamageValueByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetDamageValueByNameNullTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetDamageValueByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetDamageValueByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetDamageValueByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetDamageValueByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetDamageValueByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetDamageValueByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetDamageValueByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetDamageValueByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetDamageValueByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetDamageValueByNameValidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetDamageValueByName(TestHelper.AddedDvText);
            Assert.AreEqual(true, (polmDto == null));
        }

        #endregion

        #endregion

        #region Language

        #region AddLanguage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddLanguageNullValueTest()
        {
            PolmController target = new PolmController();
            target.AddLanguage(null, true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddLanguageEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddLanguage(string.Empty, true);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddLanguageValidValueTest()
        {
            TestHelper.AddedLgText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddLanguage(TestHelper.AddedLgText, true);
            if (actual)
                TestHelper.AddedLgId = target.GetLanguageByName(TestHelper.AddedLgText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddLanguageValidValueFalseTest()
        {
            TestHelper.AddedLgText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddLanguage(TestHelper.AddedLgText2, false);
            if (actual)
                TestHelper.AddedLgId2 = target.GetLanguageByName(TestHelper.AddedLgText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddLanguageMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddLanguage(TestHelper.RandomString(210, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateLanguage


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateLanguageNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateLanguage(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateLanguageEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateLanguage(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateLanguageMaxLengthTest()
        {
            PolmController target = new PolmController();
            target.UpdateLanguage(1, TestHelper.RandomString(205, false), true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateLanguageLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateLanguage(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateLanguageZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateLanguage(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateLanguageGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateLanguage(TestHelper.AddedLgId, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateLanguageGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateLanguage(TestHelper.AddedLgId2, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteLanguage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteLanguageLessThanZeroTest()
        {
            PolmController target = new PolmController();
            Assert.IsFalse(target.DeleteLanguage(-1));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteLanguageZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteLanguage(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteLanguageGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteLanguage(TestHelper.AddedLgId);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteLanguageGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteLanguage(TestHelper.AddedLgId2);
            Assert.IsTrue(actual);
        }

        #endregion

        #region GetAllLanguage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllLanguageActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllLanguage(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllLanguageInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllLanguage(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllLanguageNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllLanguage(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetLanguageByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetLanguageByNameNullTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetLanguageByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetLanguageByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetLanguageByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetLanguageByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetLanguageByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetLanguageByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetLanguageByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetLanguageByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetLanguageByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetLanguageByNameValidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetLanguageByName(TestHelper.AddedLgText);
            Assert.AreEqual(true, (polmDto == null));
        }

        #endregion

        #endregion

        #region Source


        #region AddSource

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddSourceNullValueTest()
        {
            PolmController target = new PolmController();
            target.AddSource(null, true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddSourceEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddSource(string.Empty, true);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddSourceValidValueTest()
        {
            TestHelper.AddedSeText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddSource(TestHelper.AddedSeText, true);
            if (actual)
                TestHelper.AddedSeId = target.GetSourceByName(TestHelper.AddedSeText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddSourceValidValueFalseTest()
        {
            TestHelper.AddedSeText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddSource(TestHelper.AddedSeText2, false);
            if (actual)
                TestHelper.AddedSeId2 = target.GetSourceByName(TestHelper.AddedSeText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddSourceMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddSource(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateSource


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateSourceNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateSource(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateSourceEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateSource(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateSourceMaxLengthTest()
        {
            PolmController target = new PolmController();
            target.UpdateSource(1, TestHelper.RandomString(65, false), true);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateSourceLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateSource(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateSourceZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateSource(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateSourceGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateSource(TestHelper.AddedSeId, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void UpdateSourceGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateSource(TestHelper.AddedSeId2, TestHelper.RandomString(5, true), true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteSource

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteSourceLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteSource(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteSourceZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteSource(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteSourceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteSource(TestHelper.AddedSeId);
            Assert.IsTrue(actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteSourceGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteSource(TestHelper.AddedSeId2);
            Assert.IsTrue(actual);
        }

        #endregion

        #region GetAllSource

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllSourceActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllSource(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllSourceInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllSource(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllSourceNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllSource(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetSourceByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetSourceByNameNullTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetSourceByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetSourceByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetSourceByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetSourceByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetSourceByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetSourceByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetSourceByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetSourceByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetSourceByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetSourceByNameValidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetSourceByName(TestHelper.AddedSeText);
            Assert.AreEqual(true, (polmDto == null));
        }

        #endregion

        #endregion

        #region QLMD

        #region AddQuickLegalMatterDescription

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQuickLegalMatterDescriptionNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddQuickLegalMatterDescription(null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQuickLegalMatterDescriptionEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddQuickLegalMatterDescription(string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddQuickLegalMatterDescriptionValidValueTest()
        {
            TestHelper.AddedQlmdText = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddQuickLegalMatterDescription(TestHelper.AddedQlmdText, true);
            if (actual)
                TestHelper.AddedQlmdId = target.GetQlmdByName(TestHelper.AddedQlmdText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddQuickLegalMatterDescriptionValidValueFalseTest()
        {
            TestHelper.AddedQlmdText2 = TestHelper.RandomString(5, false);
            PolmController target = new PolmController();
            bool actual = target.AddQuickLegalMatterDescription(TestHelper.AddedQlmdText2, false);
            if (actual)
                TestHelper.AddedQlmdId2 = target.GetQlmdByName(TestHelper.AddedQlmdText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQuickLegalMatterDescriptionMaxLangthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddQuickLegalMatterDescription(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateQuickLegalMatterDescription


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionNullValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionEmptyValueTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionMaxLengthTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(1, TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionLessThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionGreaterThanZeroActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(TestHelper.AddedQlmdId, "asdwe", true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateQuickLegalMatterDescriptionGreaterThanZeroInActiveTest()
        {
            PolmController target = new PolmController();
            bool actual = target.UpdateQuickLegalMatterDescription(TestHelper.AddedQlmdId2, "asdwe", true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteQuickLegalMatterDescription

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteQuickLegalMatterDescriptionLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteQuickLegalMatterDescription(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteQuickLegalMatterDescriptionZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteQuickLegalMatterDescription(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteQuickLegalMatterDescriptionGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteQuickLegalMatterDescription(TestHelper.AddedQlmdId);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteQuickLegalMatterDescriptionGreaterThanZeroTest2()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteQuickLegalMatterDescription(TestHelper.AddedQlmdId2);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region GetAllQuickLegalMatterDescription

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllQuickLegalMatterDescriptionActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllQuickLegalMatterDescription(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllQuickLegalMatterDescriptionInActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllQuickLegalMatterDescription(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllQuickLegalMatterDescriptionNullActiveTest()
        {
            PolmController target = new PolmController();
            List<PolmDto> list = target.GetAllQuickLegalMatterDescription(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetQlmdByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetQlmdByNameNullTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetQlmdByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetQlmdByNameEmptyTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetQlmdByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetQlmdByNameNumberTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetQlmdByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetQlmdByNameCharactersTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetQlmdByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetQlmdByNameInvalidBdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetQlmdByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetQlmdByNameValidQlmdTest()
        {
            PolmController target = new PolmController();
            PolmDto polmDto = target.GetQlmdByName(TestHelper.AddedQlmdText);
            Assert.AreEqual(true, (polmDto != null));
        }

        #endregion

        #region GetAllQuickLegalMatterDescriptionOfSource

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetAllQuickLegalMatterDescriptionOfSourceLessThanTest()
        {
            PolmController target = new PolmController();
            target.GetAllQuickLegalMatterDescriptionOfSource(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetAllQuickLegalMatterDescriptionOfSourceZeroTest()
        {
            PolmController target = new PolmController();
            target.GetAllQuickLegalMatterDescriptionOfSource(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllQuickLegalMatterDescriptionOfSourceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            List<PolmDivisionDto> list = target.GetAllQuickLegalMatterDescriptionOfSource(1);
            Assert.IsTrue(list.Count >= 0);
        }


        #endregion

        #region AddQlmdOfSOurce

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQlmdOfSOurceLessThanZeroTest1()
        {
            PolmController target = new PolmController();
            target.AddQlmdOfSOurce(-1, 1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQlmdOfSOurceLessThanZeroTest2()
        {
            PolmController target = new PolmController();
            target.AddQlmdOfSOurce(1, -1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQlmdOfSOurceZeroTest1()
        {
            PolmController target = new PolmController();
            target.AddQlmdOfSOurce(0, 1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddQlmdOfSOurceZeroTest2()
        {
            PolmController target = new PolmController();
            target.AddQlmdOfSOurce(1, 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddQlmdOfSOurceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddQlmdOfSOurce(1, 1);
            Assert.IsTrue(actual);
        }


        #endregion

        #region DeleteQlmdOfSOurce

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteQlmdOfSOurceLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteQlmdOfSOurce(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteQlmdOfSOurceZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteQlmdOfSOurce(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteQlmdOfSOurceGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteQlmdOfSOurce(210);
            Assert.AreEqual(actual, actual);
        }


        #endregion

        #endregion

        #region GetProspect

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetProspectZeroTest()
        {
            PolmController target = new PolmController();
            target.GetProspect(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetProspectLessthanZeroTest()
        {
            PolmController target = new PolmController();
            target.GetProspect(-1);
        }

        [TestMethod]
        public void GetProspectGreaterthanZeroTest()
        {
            PolmController target = new PolmController();
            ProspectDto prospectDto = target.GetProspect(10);
            Assert.IsTrue(prospectDto != null);
        }


        #endregion

        #region GetAttachments

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetAttachmentsZeroTest()
        {
            PolmController target = new PolmController();
            target.GetAttachments(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetAttachmentsLessthanZeroTest()
        {
            PolmController target = new PolmController();
            target.GetAttachments(-1);
        }

        [TestMethod]
        public void GetAttachmentsGreaterthanZeroTest()
        {
            PolmController target = new PolmController();
            List<AttachmentDto> attachmentDto = target.GetAttachments(10);
            Assert.IsTrue(attachmentDto != null);
        }


        #endregion

        #region GetProspectReport

        [TestMethod]
        public void GetProspectReportNullTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = null,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1

                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportDateRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = Convert.ToDateTime("01/01/1900"),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = null,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1

                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportInvalidDateRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = null,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1

                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportLastNameEmptyRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = string.Empty,
                                                                      ProspectCaseAssignAttorney = null,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1

                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseAssignAttorneyEmptyRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = string.Empty,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1

                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseAssignAttorneyIdZeroRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = string.Empty,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = 0,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseAssignAttorneyIdLessThanZeroRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = string.Empty,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = -1,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseAssignAttorneyIdGreaterThanZeroRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = string.Empty,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = -1,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectProspectCaseAssignDateRangeTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = DateTime.Today.AddDays(1),
                                                                      AssignStartDate = DateTime.Today,
                                                                      LastName = string.Empty,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = DateTime.Today,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = false,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseDivisionNullTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseDivisionTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = "Sullo",
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseFileDateTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = DateTime.Today,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseFollowUpDateTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = DateTime.Today,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseReffAttorneyNameEmptyTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = string.Empty,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseReffAttorneyNameTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = "at",
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseSourceEmptyTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = string.Empty,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseSourceTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = "Houston",
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseStatusIdZeroTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = 0,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseStatusIdLessThanZeroTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = -1,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectCaseStatusIdGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = 2,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectFullNameEmptyTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = string.Empty,
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectFullNameTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = "Test",
                                                                      ProspectLastCaseStatusUpdateDate = null,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportProspectLastCaseStatusUpdateDateTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = DateTime.Today,
                                                                      ShowAll = true,
                                                                      UserId = 1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportUserIdZeroTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = DateTime.Today,
                                                                      ShowAll = true,
                                                                      UserId = 0
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetProspectReportUserIdLessThanZeroTest()
        {
            PolmController target = new PolmController();
            ProspectReportSearchDto prospectReportSearchDto = new ProspectReportSearchDto
                                                                  {
                                                                      AssignEndDate = null,
                                                                      AssignStartDate = null,
                                                                      LastName = null,
                                                                      ProspectCaseAssignAttorney = string.Empty,
                                                                      ProspectCaseAssignAttorneyId = null,
                                                                      ProspectCaseAssignDate = null,
                                                                      ProspectCaseDivision = null,
                                                                      ProspectCaseFileDate = null,
                                                                      ProspectCaseFollowUpDate = null,
                                                                      ProspectCaseReffAttorneyName = null,
                                                                      ProspectCaseSource = null,
                                                                      ProspectCaseStatusId = null,
                                                                      ProspectFullName = null,
                                                                      ProspectLastCaseStatusUpdateDate = DateTime.Today,
                                                                      ShowAll = true,
                                                                      UserId = -1
                                                                  };
            List<ProspectReportDto> list = target.GetProspectReport(prospectReportSearchDto);
            Assert.IsTrue(list != null);
        }
        #endregion

        #region AddComments

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsNullDtoTest()
        {
            PolmController target = new PolmController();
            target.AddComments(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsNullClientInfoTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto { ClientInfo = null });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsNullCaseInfoTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto { ClientInfo = new ClientInfoDto(), CaseInfo = null });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsClientIdZeroTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 0 },
                                       CaseInfo = new CaseInfoDto { SourceId = 2 },
                                       UserComments = "test",
                                       InsertedCommentsUserId = "2",

                                   });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsClientIdLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = -1 },
                                       CaseInfo = new CaseInfoDto { SourceId = 2 },
                                       UserComments = "test",
                                       InsertedCommentsUserId = "2",

                                   });
        }

        [TestMethod]
        public void AddCommentsClientIdGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            Assert.IsTrue(target.AddComments(new ProspectDto
                                                 {
                                                     ClientInfo = new ClientInfoDto { ClientId = 100 },
                                                     CaseInfo = new CaseInfoDto { SourceId = 3 },
                                                     UserComments = "test",
                                                     InsertedCommentsUserId = "73",

                                                 }));

        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsSourceIdLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 2 },
                                       CaseInfo = new CaseInfoDto { SourceId = -1 },
                                       UserComments = "test",
                                       InsertedCommentsUserId = "2",

                                   });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsSourceIdZeroTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 2 },
                                       CaseInfo = new CaseInfoDto { SourceId = 0 },
                                       UserComments = "test",
                                       InsertedCommentsUserId = "2",

                                   });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsInsertedCommentsUserIdZeroTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 2 },
                                       CaseInfo = new CaseInfoDto { SourceId = 0 },
                                       UserComments = "test",
                                       InsertedCommentsUserId = "0",

                                   });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsInsertedCommentsUserIdLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 2 },
                                       CaseInfo = new CaseInfoDto { SourceId = 0 },
                                       UserComments = "test",
                                       InsertedCommentsUserId = "-1",

                                   });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsUserCommentsNullTestTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 2 },
                                       CaseInfo = new CaseInfoDto { SourceId = 0 },
                                       UserComments = null,
                                       InsertedCommentsUserId = "-1",

                                   });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCommentsUserCommentsEmptyTestTest()
        {
            PolmController target = new PolmController();
            target.AddComments(new ProspectDto
                                   {
                                       ClientInfo = new ClientInfoDto { ClientId = 2 },
                                       CaseInfo = new CaseInfoDto { SourceId = 0 },
                                       UserComments = string.Empty,
                                       InsertedCommentsUserId = "-1",

                                   });
        }

        #endregion

        #region GetUserComments

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetUserCommentsZeroTest()
        {
            PolmController polmController = new PolmController();
            polmController.GetUserComments(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetUserCommentsLessThanZeroTest()
        {
            PolmController polmController = new PolmController();
            polmController.GetUserComments(-1);
        }

        [TestMethod]
        public void GetUserCommentsGreaterThanZeroTest()
        {
            PolmController polmController = new PolmController();
            List<CommentsHistoryDto> list = polmController.GetUserComments(3);
            Assert.IsTrue(list != null);

        }

        #endregion

        #region DeleteAttachments

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 0,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 1,
                                                                FiledBy = 1,
                                                            }
                                         });
        }


        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = -1,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 1,
                                                                FiledBy = 1,
                                                            }
                                         });
        }


        [TestMethod]
        public void DeleteAttachmentsGreaterThanZeroTest()
        {
            PolmController target = new PolmController();
            bool actual = target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 1,
                                                                FiledBy = 1,
                                                            }
                                         });
            Assert.AreEqual(actual, actual);
        }


        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsSourceIdNullTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = null,
                                                                FiledBy = 1,
                                                            }
                                         });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsSourceIdZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 0,
                                                                FiledBy = 1,
                                                            }
                                         });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsSourceIdLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = -1,
                                                                FiledBy = 1,
                                                            }
                                         });
        }


        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsFiledByNullTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 2,
                                                                FiledBy = null,
                                                            }
                                         });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsFiledByZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 0,
                                                                FiledBy = 0,
                                                            }
                                         });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteAttachmentsFiledByLessThanZeroTest()
        {
            PolmController target = new PolmController();
            target.DeleteAttachments(new ProspectDto
                                         {
                                             AttachmentId = 2,
                                             CaseInfo = new CaseInfoDto
                                                            {
                                                                SourceId = 1,
                                                                FiledBy = -1,
                                                            }
                                         });
        }




        #endregion

        #region AddAttachments

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsNullTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsNullClientInfoTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = null });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsNullCaseInfoInfoTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto(), CaseInfo = null });
        }


        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsNullFileOriginalNameTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto { ClientId = 3 }, FileOriginalName = null });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsEmptyFileOriginalNameTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto { ClientId = 3 }, FileOriginalName = string.Empty });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsNullFileStoredNameTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto { ClientId = 3 }, FileOriginalName = "asdsad", FileStoredName = null });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsEmptyFileStoredNameTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto { ClientId = 3 }, FileOriginalName = "asdsd", FileStoredName = string.Empty });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsClientIdZeroTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto { ClientId = 0 }, FileOriginalName = "asdasd" });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddAttachmentsClientIdLessthanZeroTest()
        {
            PolmController target = new PolmController();
            target.AddAttachments(new ProspectDto { ClientInfo = new ClientInfoDto { ClientId = -1 }, FileOriginalName = "asdasd" });
        }

        [TestMethod]
        public void AddAttachmentsTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddAttachments(new ProspectDto
                                                    {
                                                        CaseInfo = new CaseInfoDto { SourceId = 1 },
                                                        ClientInfo = new ClientInfoDto { ClientId = 397 },
                                                        FileOriginalName = "asdasd",
                                                        FileStoredName = "saddadsadad"
                                                    });
            Assert.AreEqual(actual, true);
        }



        #endregion

        #region GetAllPolmConsultationHistory

        [TestMethod]
        public void GetAllPolmConsultationHistoryZerotestTest()
        {
            PolmController target = new PolmController();
            DataTable dtHistory = target.GetAllPolmConsultationHistory(0);
            Assert.IsTrue(dtHistory != null);

        }

        [TestMethod]
        public void GetAllPolmConsultationHistoryLessThanZerotestTest()
        {
            PolmController target = new PolmController();
            DataTable dtHistory = target.GetAllPolmConsultationHistory(-1);
            Assert.IsTrue(dtHistory != null);

        }

        [TestMethod]
        public void GetAllPolmConsultationHistoryGreaterThanZerotestTest()
        {
            PolmController target = new PolmController();
            DataTable dtHistory = target.GetAllPolmConsultationHistory(1234);
            Assert.IsTrue(dtHistory != null);

        }

        #endregion

        #region AddPolmConsultationHistory

        [TestMethod]
        public void AddPolmConsultationHistoryZeroTicketIdTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(0, "asd", "asdsadada", "asdasd", "adasdsad", 2, 379);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryLessThanZeroTicketIdTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(-1, "asd", "asdsadada", "asdasd", "adasdsad", 2, 379);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryZeroCaseIdTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(1234, "asd", "asdsadada", "asdasd", "adasdsad", 2, 0);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryLessThanZeroCaseidTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(1244, "asd", "asdsadada", "asdasd", "adasdsad", 2, -1);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryEmptyQlmdTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(1244, string.Empty, "asdsadada", "asdasd", "adasdsad", 2, 397);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryEmptyLmdTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(1244, "asdsadada", string.Empty, "asdasd", "adasdsad", 2, 397);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryEmptyDamageTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(1244, "asdsadada", "asdasd", string.Empty, "adasdsad", 2, 397);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPolmConsultationHistoryEmptyBdTest()
        {
            PolmController target = new PolmController();
            bool actual = target.AddPolmConsultationHistory(1244, "asdsadada", "asdasd", "adasdsad", string.Empty, 2, 397);
            Assert.IsTrue(actual);
        }





        #endregion

        #region Prospect

        #region AddProspect

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProspectNullTest()
        {
            PolmController polmController = new PolmController();
            polmController.AddProspect(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProspectNullCaseInfoDtoTest()
        {
            PolmController polmController = new PolmController();
            polmController.AddProspect(new ProspectDto { CaseInfo = null });
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProspectNullClientInfoDtoTest()
        {
            PolmController polmController = new PolmController();
            polmController.AddProspect(new ProspectDto { CaseInfo = new CaseInfoDto(), ClientInfo = null });
        }

        [TestMethod]
        public void AddProspectTest()
        {
            PolmController polmController = new PolmController();
            int id = polmController.AddProspect(new ProspectDto
                                           {
                                               CaseInfo = new CaseInfoDto
                                                              {
                                                                  FiledBy = 12,
                                                                  FiledDate = DateTime.Now,
                                                                  DivisionId = 2,
                                                                  QuickLegalMatterDescriptionId = 3,
                                                                  LegalMetterDescription = "asdasdasd",
                                                                  SourceId = 1
                                                              },
                                               ClientInfo = new ClientInfoDto
                                                                {
                                                                    FirstName = "test",
                                                                    LastName = "test",
                                                                    Address = "asd",
                                                                    ContactNumber1 = "1231231233",
                                                                    ContactType1 = 2,
                                                                    Dob = DateTime.Now.AddDays(-10),
                                                                    LanguageId = 3
                                                                }
                                           });
            Assert.IsTrue(id > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProspectDobTest()
        {
            PolmController polmController = new PolmController();
            int id = polmController.AddProspect(new ProspectDto
                                                    {
                                                        CaseInfo = new CaseInfoDto
                                                                       {
                                                                           FiledBy = 12,
                                                                           FiledDate = DateTime.Now,
                                                                           DivisionId = 2,
                                                                           QuickLegalMatterDescriptionId = 3,
                                                                           LegalMetterDescription = "asdasdasd",
                                                                           SourceId = 1
                                                                       },
                                                        ClientInfo = new ClientInfoDto
                                                                         {
                                                                             FirstName = "test",
                                                                             LastName = "test",
                                                                             Address = "asd",
                                                                             ContactNumber1 = "1231231233",
                                                                             ContactType1 = 2,
                                                                             Dob = Convert.ToDateTime("01/01/1900"),
                                                                             LanguageId = 3
                                                                         }
                                                    });
            Assert.IsTrue(id > 0);
        }

        #endregion

        #region UpdateProspect

        [TestMethod]
        public void UpdateProspectTest()
        {
            PolmController polmController = new PolmController();
            bool id = polmController.UpdateProspect(new ProspectDto
                                                        {
                                                            CaseInfo = new CaseInfoDto
                                                                           {
                                                                               LastUpdatedBy = 2,
                                                                               LastCaseStatusUpdateDate = DateTime.Now,
                                                                               DivisionId = 2,
                                                                               QuickLegalMatterDescriptionId = 3,
                                                                               LegalMetterDescription = "asdasdasd",
                                                                               SourceId = 1
                                                                           },
                                                            ClientInfo = new ClientInfoDto
                                                                             {
                                                                                 FirstName = "test",
                                                                                 LastName = "test",
                                                                                 Address = "asd",
                                                                                 ContactNumber1 = "1231231233",
                                                                                 ContactType1 = 2,
                                                                                 Dob = DateTime.Now.AddDays(-10),
                                                                                 LanguageId = 3,
                                                                                 ClientId = 397

                                                                             }
                                                        });
            Assert.IsTrue(id);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProspectDobTest()
        {
            PolmController polmController = new PolmController();
            polmController.UpdateProspect(new ProspectDto
                                                        {
                                                            CaseInfo = new CaseInfoDto
                                                                           {
                                                                               LastUpdatedBy = 2,
                                                                               LastCaseStatusUpdateDate = DateTime.Now,
                                                                               DivisionId = 2,
                                                                               QuickLegalMatterDescriptionId = 3,
                                                                               LegalMetterDescription = "asdasdasd",
                                                                               SourceId = 1
                                                                           },
                                                            ClientInfo = new ClientInfoDto
                                                                             {
                                                                                 FirstName = "test",
                                                                                 LastName = "test",
                                                                                 Address = "asd",
                                                                                 ContactNumber1 = "1231231233",
                                                                                 ContactType1 = 2,
                                                                                 Dob = Convert.ToDateTime("01/01/1900"),
                                                                                 LanguageId = 3,
                                                                                 ClientId = 397

                                                                             }
                                                        });
        }

        #endregion

        #endregion


    }
}

﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for ClsUpdateFollowUpInfoTest and is intended
    ///to contain all ClsUpdateFollowUpInfoTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ClsUpdateFollowUpInfoTest
    {

        private TestContext testContextInstance;
        private int ticketId;
        private int empId;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestInitialize]
        public void InitiliazeValues()
        {
            ticketId = 64685;
            empId = 3992;
        }

        /// <summary>
        ///A test for UpdateTrafficWiatingFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateTrafficWiatingFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update traffic waiting follow up date when both dates are different";
            bool expected = true;
            bool actual;
            actual = target.UpdateTrafficWiatingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateTrafficWiatingFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateTrafficWiatingFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update traffic waiting follow up date when both dates are same";
            bool expected = true;
            bool actual;
            actual = target.UpdateTrafficWiatingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateTrafficWiatingFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateTrafficWiatingFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update traffic waiting follow up date when date is not set yet";
            bool expected = true;
            bool actual;
            actual = target.UpdateTrafficWiatingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFamilyWiatingFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateFamilyWiatingFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update family waiting follow up date when both dates are different";
            bool expected = true;
            bool actual;
            actual = target.UpdateTrafficWiatingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFamilyWiatingFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateFamilyWiatingFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update fFamily waiting follow up date when both dates are same";
            bool expected = true;
            bool actual;
            actual = target.UpdateTrafficWiatingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFamilyWiatingFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateFamilyWiatingFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update family waiting follow up date when date is not set yet";
            bool expected = true;
            bool actual;
            actual = target.UpdateTrafficWiatingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateTrafficTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateTrafficTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.TrafficWaitingFollowUpDate.ToString();
            string comments = "Unit testing to update traffic waiting follow up date by UpdateFollowUpDate method";
            string freestate = string.Empty;
            string ticketNo = string.Empty;
            bool isUpdateAll = false;
            bool isRemoved = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateFamilyTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateFamilyTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.FamilyWaitingFollowUpDate.ToString();
            string comments = "Unit testing to update family waiting follow up date by UpdateFollowUpDate method";
            string freestate = string.Empty;
            string ticketNo = string.Empty;
            bool isUpdateAll = false;
            bool expected = true;
            bool isRemoved = false;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateContractTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateContractTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.ContractFollowUpDate.ToString();
            string comments = "Unit testing to update contract follow up date by UpdateFollowUpDate method";
            string freestate = string.Empty;
            string ticketNo = string.Empty;
            bool isUpdateAll = false;
            bool isRemoved = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateBondWaitingTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateBondWaitingTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.BondWaitingFollowUpDate.ToString();
            string comments = "Unit testing to update bond waiting follow up date by UpdateFollowUpDate method";
            string freestate = string.Empty;
            string ticketNo = string.Empty;
            bool isUpdateAll = false;
            bool isRemoved = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateBondTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateBondTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.BondFollowUpDate.ToString();
            string comments = "Unit testing to update bond follow up date by UpdateFollowUpDate method";
            string freestate = string.Empty;
            string ticketNo = string.Empty;
            bool isUpdateAll = false;
            bool expected = true;
            bool isRemoved = false;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateArrWaitingTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateArrWaitingTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.ArrWaitingFollowUpDate.ToString();
            string comments = "Unit testing to update waiting follow up date by UpdateFollowUpDate method";
            string freestate = string.Empty;
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool isRemoved = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateArrWaitingTest
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateCriminalTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.CriminalFollowUpDate.ToString();
            string comments = "Unit testing to update waiting follow up date by UpdateFollowUpDate method";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool isRemoved = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateFollowUpDateArrWaitingTest
        ///Waqas 5653 03/21/2009 Test case
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateNOLORTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.LORFollowUpDate.ToString();
            string comments = "Unit testing to update No LOR follow up date by UpdateFollowUpDate method";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool isRemoved = false;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }
        //Nasir 5938 05/27/2009
        /// <summary>
        ///A test for UpdateFollowUpForNOS        
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateNOSTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.NOSFollowUpDate.ToString();
            string comments = "Unit testing to update NOS follow up date by UpdateFollowUpDate method";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool isRemoved = false;
            bool actual;
            actual = target.UpdateFollowUpDate(ticketId.ToString(), empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        //Nasir 5938 05/27/2009
        /// <summary>
        ///A test for UpdateFollowUpForNOS for zero ticket id       
        ///</summary>
        [TestMethod()]
        public void UpdateFollowUpDateNOSTestForZero()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string followUpType = FollowUpType.NOSFollowUpDate.ToString();
            string comments = "Unit testing to update NOS follow up date by UpdateFollowUpDate method";
            string freestate = true.ToString();
            string ticketNo = "0";
            bool isUpdateAll = false;
            bool expected = false;
            bool isRemoved = false;
            bool actual;
            actual = target.UpdateFollowUpDate("0", empId, currentFollowUpDate, followUpType, followUpDate, comments, freestate, ticketNo, isUpdateAll, isRemoved);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateContractFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateContractFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update contract follow up date when both dates are different";
            bool expected = true;
            bool actual;
            actual = target.UpdateContractFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateContractFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateContractFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update contract follow up date when both dates are same";
            bool expected = true;
            bool actual;
            actual = target.UpdateContractFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateContractFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateContractFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update contract follow up date when date is not set yet";
            bool expected = true;
            bool actual;
            actual = target.UpdateContractFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateBondWaitingFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateBondWaitingFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update bond waiting follow up date when both dates are different";
            bool expected = true;
            bool actual;
            actual = target.UpdateBondWaitingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateBondWaitingFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateBondWaitingFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update bond waiting follow up date when both dates are same";
            bool expected = true;
            bool actual;
            actual = target.UpdateBondWaitingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateBondWaitingFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateBondWaitingFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update bond waiting follow up date when date is not set yet";
            bool expected = true;
            bool actual;
            actual = target.UpdateBondWaitingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateBondFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateBondFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update bond follow up date when both dates are change";
            bool expected = true;
            bool actual;
            actual = target.UpdateBondFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateBondFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateBondFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update bond follow up date when both dates are same";
            bool expected = true;
            bool actual;
            actual = target.UpdateBondFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateBondFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateBondFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update bond follow up date when date is not set yet";
            bool expected = true;
            bool actual;
            actual = target.UpdateBondFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateArrWaitingFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateArrWaitingFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update waiting follow up date when both dates are change";
            string freestate = string.Empty;
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateArrWaitingFollowUpDate(ticketNo, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateArrWaitingFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateArrWaitingFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update waiting follow up date when both dates are same";
            string freestate = string.Empty;
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateArrWaitingFollowUpDate(ticketNo, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateArrWaitingFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateArrWaitingFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update waiting follow up date when date is not set yet";
            string freestate = string.Empty;
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateArrWaitingFollowUpDate(ticketNo, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateCriminalFollowUpDateDifferenceTest
        ///</summary>
        [TestMethod()]
        public void UpdateCriminalFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update criminal follow up date when both dates are change";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateArrWaitingFollowUpDate(ticketNo, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateCriminalFollowUpDateSameTest
        ///</summary>
        [TestMethod()]
        public void UpdateCriminalFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update criminal follow up date when both dates are same";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateArrWaitingFollowUpDate(ticketNo, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateCriminalFollowUpDateNullTest
        ///</summary>
        [TestMethod()]
        public void UpdateCriminalFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update criminal follow up date when date is not set yet";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            bool isUpdateAll = false;
            bool expected = true;
            bool actual;
            actual = target.UpdateArrWaitingFollowUpDate(ticketNo, empId, currentFollowUpDate, followUpDate, comments, freestate, ticketNo, isUpdateAll);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateNoLORFollowUpDateDifferenceTest
        ///Waqas 5653 03/21/2009 Test case
        ///</summary>
        [TestMethod()]
        public void UpdateNoLORFollowUpDateDifferenceTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update No LOR follow up date when both dates are change";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            
            bool expected = true;
            bool actual;
            actual = target.UpdateNoLORFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateNoLORFollowUpDateSameTest
        ///Waqas 5653 03/21/2009 Test case
        ///</summary>
        [TestMethod()]
        public void UpdateNoLORFollowUpDateSameTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime followUpDate = DateTime.Now;
            string comments = "Unit testing to update No LOR follow up date when both dates are same";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            
            bool expected = true;
            bool actual;
            actual = target.UpdateNoLORFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UpdateNoLORFollowUpDateNullTest
        ///Waqas 5653 03/21/2009 Test case
        ///</summary>
        [TestMethod()]
        public void UpdateNoLORFollowUpDateNullTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = DateTime.Now.AddDays(10);
            string comments = "Unit testing to update No LOR follow up date when date is not set yet";
            string freestate = true.ToString();
            string ticketNo = ticketId.ToString();
            
            bool expected = true;
            bool actual;
            actual = target.UpdateNoLORFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CanUpdateFollowUpDateEqualTest
        ///</summary>
        [TestMethod()]
        public void CanUpdateFollowUpDateEqualTest()
        {
            ClsUpdateFollowUpInfo_Accessor target = new ClsUpdateFollowUpInfo_Accessor();
            string oldFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime newFollowUpDate = DateTime.Now;
            bool expected = false;
            bool actual;
            actual = target.CanUpdateFollowUpDate(oldFollowUpDate, newFollowUpDate);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CanUpdateFollowUpDateNotEqualTest
        ///</summary>
        [TestMethod()]
        public void CanUpdateFollowUpDateNotEqualTest()
        {
            ClsUpdateFollowUpInfo_Accessor target = new ClsUpdateFollowUpInfo_Accessor();
            string oldFollowUpDate = DateTime.Now.ToShortDateString();
            DateTime newFollowUpDate = DateTime.Now.AddDays(10);
            bool expected = true;
            bool actual;
            actual = target.CanUpdateFollowUpDate(oldFollowUpDate, newFollowUpDate);
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        ///     A test for UpdatePastCourtdateFollowUpDate
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void UpdatePastCourtdateFollowUpDateForNullParameterTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            int ticketId = 0;
            int empId = 0;
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = new DateTime();
            string comments = string.Empty;
            FollowUpType ftype = new FollowUpType();
            ftype = FollowUpType.PastCourtDateHMCFollowupdate;
            target.UpdatePastCourtdateFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments, ftype);
        }

        /// <summary>
        ///     A test for UpdatePastCourtdateFollowUpDate
        ///</summary>
        [TestMethod()]
        public void UpdatePastCourtdateFollowUpDateForUpdationTest()
        {
            bool expected = true;
            bool actual = false;
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo();
            int ticketId = 14938;
            int empId = 3991;
            string currentFollowUpDate = "04/08/2009";
            DateTime followUpDate = new DateTime(2009, 04, 09);
            string comments = "Testing";
            FollowUpType ftype = new FollowUpType();
            ftype = FollowUpType.PastCourtDateHMCFollowupdate;
            actual = target.UpdatePastCourtdateFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments, ftype);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///     A test for UpdateALRHearingFollowUpDate
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void UpdateALRHearingFollowUpDateForNullParameterTest()
        {
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo(); // TODO: Initialize to an appropriate value
            int ticketId = 0;
            int empId = 0;
            string currentFollowUpDate = string.Empty;
            DateTime followUpDate = new DateTime();
            string comments = string.Empty;
            string subdoctype = string.Empty;
            target.UpdateALRHearingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments, subdoctype);
        }

        /// <summary>
        ///     A test for UpdateALRHearingFollowUpDate
        ///</summary>
        [TestMethod()]
        public void UpdateALRHearingFollowUpDateForUpdationTest()
        {
            bool expected = true;
            bool actual = false;
            ClsUpdateFollowUpInfo target = new ClsUpdateFollowUpInfo(); // TODO: Initialize to an appropriate value
            int ticketId = 163843;
            int empId = 3991;
            string currentFollowUpDate = DateTime.Today.ToShortDateString();
            DateTime followUpDate = DateTime.Today;
            string comments = "TestClassAttribute";
            string subdoctype = "12";
            actual = target.UpdateALRHearingFollowUpDate(ticketId, empId, currentFollowUpDate, followUpDate, comments, subdoctype);
            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using HTP.Components.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using lntechNew.Components.Services;

namespace HTPUnitTests
{
    /// <summary>
    /// Summary description for ShowSettingHistoryServiceTest
    /// </summary>
    [TestClass]
    public class ShowSettingHistoryServiceTest
    {
        public ShowSettingHistoryServiceTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


            /// <summary>
            ///A test for GetShowSettingHistory
            ///</summary>
            [TestMethod()]            
            public void GetShowSettingHistoryTestForNull()
            {
            ShowSettingHistoryService target = new ShowSettingHistoryService(); // TODO: Initialize to an appropriate value
            DateTime StartDate = new DateTime(2009,12,10); // TODO: Initialize to an appropriate value
            DateTime EndDate = new DateTime(2009,12,12); // TODO: Initialize to an appropriate value
            int ReportID = 135; // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.GetShowSettingHistory(StartDate, EndDate, ReportID,true);
            Assert.IsNotNull(actual);
            }

            /// <summary>
            ///A test for GetShowSettingHistory
            ///</summary>
            [TestMethod()]            
            public void GetShowSettingHistoryTest()
            {
            ShowSettingHistoryService target = new ShowSettingHistoryService(); // TODO: Initialize to an appropriate value
            DateTime StartDate = new DateTime(2009,11,10); // TODO: Initialize to an appropriate value
            DateTime EndDate = new DateTime(2009,12,17); // TODO: Initialize to an appropriate value
            int ReportID = 135; // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.GetShowSettingHistory(StartDate, EndDate, ReportID,false);
            Assert.AreEqual(actual.Columns.Count,3);
            }

            /// <summary>
            ///A test for InsertShowSettingHistory
            ///</summary>
            [TestMethod()]            
            public void InsertShowSettingHistoryTestOne()
            {
                ShowSettingHistoryService target = new ShowSettingHistoryService(); // TODO: Initialize to an appropriate value
                int ReportID = 97; // TODO: Initialize to an appropriate value
                string Notes = "unit Test"; // TODO: Initialize to an appropriate value
                int EmployeeID = 3992; // TODO: Initialize to an appropriate value
                bool expected = true; // TODO: Initialize to an appropriate value
                bool actual;
                actual = target.InsertShowSettingHistory(ReportID, Notes, EmployeeID);
                Assert.AreEqual(expected, actual);                
            }

            /// <summary>
            ///A test for InsertShowSettingHistory
            ///</summary>
            [TestMethod()]
            public void InsertShowSettingHistoryTestTwo()
            {
                ShowSettingHistoryService target = new ShowSettingHistoryService(); // TODO: Initialize to an appropriate value
                int ReportID = 197; // TODO: Initialize to an appropriate value
                string Notes = "unit Test 2"; // TODO: Initialize to an appropriate value
                int EmployeeID = 3992; // TODO: Initialize to an appropriate value
                bool expected = true; // TODO: Initialize to an appropriate value
                bool actual;
                actual = target.InsertShowSettingHistory(ReportID, Notes, EmployeeID);
                Assert.AreEqual(expected, actual);
            }


            
    }
}

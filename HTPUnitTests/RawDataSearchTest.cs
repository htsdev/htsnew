﻿using HTP.backroom;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for RawDataSearchTest and is intended
    ///to contain all RawDataSearchTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RawDataSearchTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetData
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Fadi\\Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:24919/")]
        [DeploymentItem("lntechNew.dll")]
        public void GetDataTest()
        {
            RawDataSearch_Accessor target = new RawDataSearch_Accessor(); // TODO: Initialize to an appropriate value
            target.GetData();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}

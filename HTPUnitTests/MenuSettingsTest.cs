﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{
    /// <summary>
    /// Summary description for MenuSettingsTest
    /// </summary>
    [TestClass]
    public class MenuSettingsTest
    {
        public MenuSettingsTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion



        /// <summary>
        ///A test for GetMenuItems
        ///</summary>
        [TestMethod()]
        public void GetMenuItemsTest()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            int LevelID = 1; // TODO: Initialize to an appropriate value
            int MenuID = 12; // TODO: Initialize to an appropriate value
            DataSet expected = new DataSet(); // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetMenuItems(LevelID, MenuID);
            Assert.AreNotEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for UpdateFirstLevelMenuItem
        ///</summary>
        [TestMethod()]
        public void UpdateSecondLevelMenuItemTest1()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            int MenuID = 234; // TODO: Initialize to an appropriate value
            int LevelID = 2; // TODO: Initialize to an appropriate value
            string Title = "Menu Configuration"; // TODO: Initialize to an appropriate value
            int IsActive = 1; // TODO: Initialize to an appropriate value
            string OrderedIDs = ""; // TODO: Initialize to an appropriate value
            string URL = "/Configuration/MenuConfiguration.aspx"; // TODO: Initialize to an appropriate value
            int IsSelected = 0; // TODO: Initialize to an appropriate value
            int IsLogging = 0; // TODO: Initialize to an appropriate value
            target.UpdateSecondLevelMenuItem(MenuID, LevelID, Title, IsActive, OrderedIDs, URL, IsSelected, IsLogging);
            
        }

        /// <summary>
        ///A test for UpdateFirstLevelMenuItem
        ///</summary>
        [TestMethod()]
         public void UpdateFirstLevelMenuItemTest()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            int MenuID = 16; // TODO: Initialize to an appropriate value
            int LevelID = 1; // TODO: Initialize to an appropriate value
            string Title = "SEARCH"; // TODO: Initialize to an appropriate value
            int IsActive = 1; // TODO: Initialize to an appropriate value
            string OrderedIDs = ""; // TODO: Initialize to an appropriate value
            target.UpdateFirstLevelMenuItem(MenuID, LevelID, Title, IsActive, OrderedIDs);            
        }

        /// <summary>
        ///A test for GetMenuItems
        ///</summary>
        [TestMethod()]
        public void GetMenuItemsTest1()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            int LevelID = 2; // TODO: Initialize to an appropriate value
            int MenuID = 42; // TODO: Initialize to an appropriate value
            DataSet expected = new DataSet(); // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetMenuItems(LevelID, MenuID);
            Assert.AreNotEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for AddSecondLevelMenuItems
        ///</summary>
        [TestMethod()]
        public void AddSecondLevelMenuItemsTest()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            int LevelID = 2; // TODO: Initialize to an appropriate value
            string Title = "Test1"; // TODO: Initialize to an appropriate value
            int IsActive = 0; // TODO: Initialize to an appropriate value
            int ParentMenuID = 19; // TODO: Initialize to an appropriate value
            string URL = "/Configuration/MenuConfiguration.aspx"; // TODO: Initialize to an appropriate value
            int IsSelected = 0; // TODO: Initialize to an appropriate value
            int IsLogging = 0; // TODO: Initialize to an appropriate value
            target.AddSecondLevelMenuItems(LevelID, Title, IsActive, ParentMenuID, URL, IsSelected, IsLogging);
            
        }

        /// <summary>
        ///A test for AddFirstLevelMenuItems
        ///</summary>
        [TestMethod()]
        public void AddFirstLevelMenuItemsTest()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            int LevelID = 2; // TODO: Initialize to an appropriate value
            string Title = "Test2"; // TODO: Initialize to an appropriate value
            int IsActive = 0; // TODO: Initialize to an appropriate value
            target.AddFirstLevelMenuItems(LevelID, Title, IsActive);
            
        }

        /// <summary>
        ///A test for GetMenuData
        ///</summary>
        [TestMethod()]
        public void GetMenuDataTest()
        {
            MenuSettings target = new MenuSettings(); // TODO: Initialize to an appropriate value
            DataSet expected = new DataSet(); // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetMenuData();
            Assert.AreNotEqual(expected, actual);
            
        }
    }
}

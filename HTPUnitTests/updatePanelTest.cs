﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for updatePanelTest and is intended
    ///to contain all updatePanelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class updatePanelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ManageSharepointViolationEvents
        ///</summary>  
        [TestMethod()]
        public void ManageSharepointViolationEventsTest_ForZero()
        {
            updatePanel target = new updatePanel();
            int TicketID = 0;
            int TicketViolationID = 0;
            ArrayList objArrList = new ArrayList();
            objArrList.Add(TicketID);
            objArrList.Add(TicketViolationID);
            target.ManageSharepointViolationEvents(objArrList);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for ManageSharepointViolationEvents
        ///</summary>
        [TestMethod()]
        public void ManageSharepointViolationEventsTest_ForNagative()
        {
            updatePanel target = new updatePanel();
            int TicketID = -10;
            int TicketViolationID = -10;
            ArrayList objArrList = new ArrayList();
            objArrList.Add(TicketID);
            objArrList.Add(TicketViolationID);
            target.ManageSharepointViolationEvents(objArrList);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for ManageSharepointViolation for Nagative value.
        ///</summary>
        [TestMethod()]
        public void ManageSharepointViolationTestForNagative()
        {


            updatePanel_Accessor target = new updatePanel_Accessor();
            int iTicketID = -10;
            int iNewTicketsViolationID = -10;
            target.ManageSharepointViolation(iTicketID, iNewTicketsViolationID);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for ManageSharepointViolation for Zero value.
        ///</summary>
        [TestMethod()]
        public void ManageSharepointViolationTestForZero()
        {


            updatePanel_Accessor target = new updatePanel_Accessor();
            int iTicketID = 0;
            int iNewTicketsViolationID = 0;
            target.ManageSharepointViolation(iTicketID, iNewTicketsViolationID);
            Assert.IsNotNull(target);
        }
    }
}



   
            
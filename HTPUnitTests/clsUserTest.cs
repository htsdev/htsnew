﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///Waqas 5057 03/19/2009
    ///clsUserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsUserTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Created Test Case for GetSystemUserInfo(int)
        ///Waqas 5057 03/19/2009 Test case
        ///</summary>
        [TestMethod()]
        public void GetSystemUserInfo_NullTest()
        {
            clsUser target = new clsUser(); 
            int empId = 3991;
            target.LoginID = "fahim";
            target.Password = "iqbal";
            clsUser expected = new clsUser();
            clsUser actual;

            actual = target.GetSystemUserInfo(empId);
            if (actual == null)
            {
                Assert.Fail();
            }
            
        }

        /// <summary>
        ///Created Test Case for GetSystemUserInfo(int)
        ///Waqas 5057 03/19/2009 Test case
        ///</summary>
        [TestMethod()]
        public void GetSystemUserInfo_AnyPropertyNullTest()
        {
            clsUser target = new clsUser();
            int empId = 3991;
            target.LoginID = "fahim";
            target.Password = "iqbal";
            clsUser expected = new clsUser();
            clsUser actual;

            actual = target.GetSystemUserInfo(empId);
            if (actual.EmpID == null || actual.LoginID == null || actual.Password == null || actual.LastName == null || actual.FirstName == null || actual.Abbreviation == null ||
                actual.AccessType == null || actual.canUpdateCloseOut == null || actual.IsSPNUser == null || actual.UserNameSPN == null || actual.PasswordSPN == null || actual.UserLocation == null)
            {
                Assert.Fail();
            }

        }

        /// <summary>
        ///Created Test Case for GetSystemUserInfo(int)
        ///Waqas 5057 03/19/2009 Test case
        ///</summary>
        [TestMethod()]
        public void GetSystemUserInfo_TypeTest()
        {
            clsUser target = new clsUser();
            int empId = 3991;
            target.LoginID = "fahim";
            target.Password = "iqbal";
            clsUser expected = new clsUser();
            clsUser actual;

            actual = target.GetSystemUserInfo(empId);

            Assert.AreEqual(actual.GetType(), target.GetType());

        }

        ///// <summary>
        /////A test for Adduser
        /////</summary>
        //[TestMethod()]        
        //public void AdduserTest()
        //{
        //    clsUser target = new clsUser(); // TODO: Initialize to an appropriate value
        //    string fname = "Test"; // TODO: Initialize to an appropriate value
        //    string lname = "For"; // TODO: Initialize to an appropriate value
        //    string abbrev = "TF"; // TODO: Initialize to an appropriate value
        //    string uname = "TFU"; // TODO: Initialize to an appropriate value
        //    string passwrd = "Test"; // TODO: Initialize to an appropriate value
        //    string accesstype = "1"; // TODO: Initialize to an appropriate value
        //    string closeout = "out"; // TODO: Initialize to an appropriate value
        //    string empid = "3992"; // TODO: Initialize to an appropriate value
        //    string email = "Test@test.com"; // TODO: Initialize to an appropriate value
        //    bool canspnsearch = false; // TODO: Initialize to an appropriate value
        //    string SPNUserName = "Test"; // TODO: Initialize to an appropriate value
        //    string SPNPassword = "Test"; // TODO: Initialize to an appropriate value
        //    string status = "Tur"; // TODO: Initialize to an appropriate value
        //    string NTUserID = "system113"; // TODO: Initialize to an appropriate value
        //    bool check = true; // TODO: Initialize to an appropriate value
        //    bool stadmin = false; // TODO: Initialize to an appropriate value
        //    string noofdays = "4"; // TODO: Initialize to an appropriate value
        //    string FirmId = "4020"; // TODO: Initialize to an appropriate value
        //    string ContactNumber = "459632114"; // TODO: Initialize to an appropriate value
        //    target.Adduser(fname, lname, abbrev, uname, passwrd, accesstype, closeout, empid, email, canspnsearch, SPNUserName, SPNPassword, status, NTUserID, check, stadmin, noofdays, FirmId, ContactNumber);
        //    Assert.IsTrue(target.CheckUser("Test","3992").Tables[0].Rows.Count > 0);
        //}
    }
}

﻿using lntechNew.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for TrialLetterTest and is intended
    ///to contain all TrialLetterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TrialLetterTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetThankEmail
        ///</summary>
        [TestMethod()]
        public void GetThankEmailTest()
        {
            TrialLetter target = new TrialLetter(); // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetThankEmail();
            Assert.AreNotEqual(expected, actual);

        }

        /// <summary>
        ///A test for GetTrialLetterEmail
        ///</summary>
        [TestMethod()]
        public void GetTrialLetterEmailTest()
        {
            TrialLetter target = new TrialLetter(); // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetTrialLetterEmail();
            Assert.AreNotEqual(expected, actual);
        }
    }
}

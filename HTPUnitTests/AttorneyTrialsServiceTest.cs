﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using HTP.Components.Entities;
using HTP.Components.Services;
using System.Data;

namespace HTPUnitTests
{
    /// <summary>
    ///This is a test class for AttorneyTrialsService and is intended
    ///to contain all its Unit Tests
    ///</summary>
    ///

    [TestClass]
   public class AttorneyTrialsServiceTest
   {      
       /// <summary>
       ///This is a Region for Method GetCourtName which  returns DataSet.
       ///This Region is intended to contain all its Unit Tests.
       ///</summary>

       #region GetCourtName

       /// <summary>
       /// This Test method will Check if the Dataset is Null or Not . 
       /// </summary>
        [TestMethod]
        public void GetCourtNameNull()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            DataSet actual = new DataSet();
            actual = target.GetCourtName();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test Method will check if the Type is DataSet or Not
        /// </summary>
        [TestMethod]
        public void GetCourtNameType()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            DataSet actual = new DataSet();
            DataSet expected = new DataSet();
            actual = target.GetCourtName();
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        /// <summary>
        /// This Test Method will check if the No: of rows are greater than or equal to 0.
        /// </summary>
        [TestMethod]
        public void GetCourtNameRows()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            bool expected, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.GetCourtName().Tables[0].Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
         }

        /// <summary>
        /// This Test Method will check if the No: of Colums are equal to 3.
        /// </summary>
        [TestMethod]
        public void GetCourtNameColumnCount()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            int expected = 0, actual = 0;
            expected = 3;
            actual = (target.GetCourtName().Tables[0].Columns.Count);
            Assert.AreEqual(expected, actual);

        }
       #endregion


        /// <summary>
        ///This is a Region for Method GetAttorney which  returns DataSet.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>

       #region GetAttorney

        /// <summary>
        /// This Test method will Check if the Dataset is Null or Not . 
        /// </summary>

        [TestMethod]
        public void GetAttorneyNull()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            DataSet actual = new DataSet();
            actual = target.GetAttorney();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test Method will check if the Type is DataSet or Not
        /// </summary>
        [TestMethod]
        public void GetAttorneyType()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            DataSet actual = new DataSet();
            DataSet expected = new DataSet();
            actual = target.GetAttorney();
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }


        /// <summary>
        /// This Test Method will check if the No: of rows are greater than or equal to 0 or Not.
        /// </summary>
        [TestMethod]
        public void GetAttorneyRows()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            bool expected, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.GetAttorney().Tables[0].Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        /// This Test Method will check if the No: of Colums are equal to 2 or Not.
        /// </summary>
        [TestMethod]
        public void GetAttorneyColumnCount()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            int expected = 0, actual = 0;
            expected = 2;
            actual = (target.GetAttorney().Tables[0].Columns.Count);
            Assert.AreEqual(expected, actual);
        }
        #endregion
        

        /// <summary>
        ///This is a Region for Method GetJuryTrialById which  returns DataTable.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>

       #region GetJuryTrialById

        /// <summary>
        /// This Test method will Check if the DataTable is Null or Not . 
        /// </summary>
        [TestMethod]
        public void GetJuryTrialByIdNull()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            AttorneyTrials AtrnyTrail = new AttorneyTrials();
            AtrnyTrail.RowId = Convert.ToInt64(200);
            DataTable actual = new DataTable();
            actual = target.GetJuryTrialById(AtrnyTrail);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// This Test Method will check if the Type is DataTable or Not
        /// </summary>
        [TestMethod]
        public void GetJuryTrialByIdType()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            AttorneyTrials AtrnyTrail = new AttorneyTrials();
            DataTable actual = new DataTable();
            DataTable expected = new DataTable();
            AtrnyTrail.RowId = Convert.ToInt64(200);
            actual = target.GetJuryTrialById(AtrnyTrail);
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        /// <summary>
        /// This Test Method will check if the No: of rows are greater than or equal to 0 or Not.
        /// </summary>
        [TestMethod]
        public void GetJuryTrialByIdRows()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            AttorneyTrials AtrnyTrail = new AttorneyTrials();
            bool expected, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = (target.GetJuryTrialById(AtrnyTrail).Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// This Test Method will check if the No: of Colums are equal to 16 or Not.
        /// </summary>
        [TestMethod]
        public void GetJuryTrialByIdColumnCount()
        {
            AttorneyTrialsService target = new AttorneyTrialsService();
            AttorneyTrials AtrnyTrail = new AttorneyTrials();
            int expected = 0, actual = 0;
            expected = 16;
            actual = (target.GetJuryTrialById(AtrnyTrail).Columns.Count);
            Assert.AreEqual(expected, actual);
        }    
       #endregion
             
   }
}

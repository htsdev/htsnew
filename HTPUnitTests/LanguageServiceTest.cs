﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for LanguageServiceTest and is intended
    ///to contain all LanguageServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LanguageServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #region GetAllLanguages()

        /// <summary>
        ///     A test for GetAllLanguages to Check Datatype
        ///</summary>
        [TestMethod()]
        public void GetAllLanguagesTypeTest()
        {
            LanguageService target = new LanguageService();
            DataTable expected = new DataTable();
            DataTable actual;
            actual = target.GetAllLanguages();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///     A test for GetAllLanguages to check rows count
        ///</summary>
        [TestMethod()]
        public void GetAllLanguagesRowsCountTest()
        {
            LanguageService target = new LanguageService();
            bool expected = true;
            bool actual = target.GetAllLanguages().Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///     A test for GetAllLanguages to check Null DataTable
        ///</summary>
        [TestMethod()]
        public void GetAllLanguagesNullDataTableTest()
        {
            LanguageService target = new LanguageService();
            Assert.IsNotNull(target.GetAllLanguages());
        }

        #endregion

        #region InsertLanguage

        /// <summary>
        ///     A test for InsertLanguage with null parameter
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void InsertLanguageTestForNullParameter()
        {
            LanguageService target = new LanguageService();
            string languageName = string.Empty;
            target.InsertLanguage(languageName);
        }

        /// <summary>
        ///     A test for InsertLanguage with null parameter
        ///</summary>
        [TestMethod()]
        public void InsertLanguageTest()
        {
            LanguageService target = new LanguageService();
            string languageName = "ENGLISH";
            int expected = 0;
            int actual = target.InsertLanguage(languageName);
            Assert.AreEqual(expected, actual);
        }


        #endregion

    }
}

﻿using HTP.Components.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Configuration.Helper;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for ConfigurationServiceTest and is intended
    ///to contain all ConfigurationServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ConfigurationServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetFilePath
        ///</summary>
        [TestMethod()]        
        public void GetFilePathTestNotNull()
        {
            ConfigurationService target = new ConfigurationService(); // TODO: Initialize to an appropriate value
            SubModule SubModule = Configuration.Helper.SubModule.GENERAL; // TODO: Initialize to an appropriate value
            Key Path = Configuration.Helper.Key.REPORT_FILE_PATH; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetFilePath(SubModule, Path);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetFilePath
        ///</summary>
        [TestMethod()]
        public void GetFilePathTest()
        {
            ConfigurationService target = new ConfigurationService(); // TODO: Initialize to an appropriate value
            SubModule SubModule = Configuration.Helper.SubModule.GENERAL; // TODO: Initialize to an appropriate value
            Key Path = Configuration.Helper.Key.REPORT_FILE_PATH; // TODO: Initialize to an appropriate value
            string expected = @"\\srv-web\DOCSTORAGE\ReportFiles"; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetFilePath(SubModule, Path);
            Assert.AreEqual(actual, expected);
        }
    }
}

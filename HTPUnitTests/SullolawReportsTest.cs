﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for SullolawReportsTest and is intended
    ///to contain all SullolawReportsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SullolawReportsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion




        /// <summary>
        ///     A test for Online Tracking Report with Datatable
        ///</summary>
        [TestMethod()]
        public void GetOnlineTrackingReportTest()
        {
            int siteid = 1;
            int showall = 1;
            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            int callstat = 0;
            DataTable expected = new DataTable();
            DataTable actual;
            actual = SullolawReports.GetOnlineTrackingReport(siteid, showall, startdate, enddate, callstat);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///     A test for GetOnlineTrackingReport with rows count test
        ///</summary>
        [TestMethod()]
        public void GetOnlineTrackingReportTestForRowsCount()
        {
            int siteid = 1;
            int showall = 1;
            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            int callstat = 0;
            bool actual = false;
            DataTable dt = new DataTable();
            dt = SullolawReports.GetOnlineTrackingReport(siteid, showall, startdate, enddate, callstat);
            if (dt.Rows.Count >= 0)
                actual = true;
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void GetOnlineTrackingReportTestForException()
        {
            int siteid = 0;
            int showall = -1;
            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            int callstat = -1;
            DataTable actual = SullolawReports.GetOnlineTrackingReport(siteid, showall, startdate, enddate, callstat);
        }

        /// <summary>
        ///A test for GetLegalConsultationReport
        ///</summary>
        [TestMethod()]
        public void GetLegalConsultationReportTest()
        {
            int siteid = 1;
            int showall = 1;
            DateTime date = DateTime.Now;
            int callstat = 1;
            DateTime enddate = DateTime.Now;
            DataTable expected = new DataTable();
            DataTable actual;
            actual = SullolawReports.GetLegalConsultationReport(siteid, showall, date, callstat, enddate);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }


        /// <summary>
        ///A test for GetLegalConsultationReport
        ///</summary>
        [TestMethod()]
        public void GetLegalConsultationReportTestForROwCount()
        {
            int siteid = 1;
            int showall = 1;
            DateTime date = DateTime.Now;
            int callstat = 1;
            DateTime enddate = DateTime.Now;
            bool actual = false;
            DataTable dt;
            dt = SullolawReports.GetLegalConsultationReport(siteid, showall, date, callstat, enddate);
            if (dt.Rows.Count >= 0)
                actual = true;
            Assert.AreEqual(true, actual);
        }
    }
}

﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for clsCaseDetailTest and is intended
    ///to contain all clsCaseDetailTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsCaseDetailTest
    {


        private TestContext testContextInstance;
        clsCaseDetail target = new clsCaseDetail();
        clsCaseDetail goal = new clsCaseDetail();
        bool expected, actual ;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


       
        [TestMethod()]
        public void UpdateCaseDetailTestForNullAndZeroValues()
        {
            goal.TicketViolationID = 0;
            goal.RefCaseNumber = null;
            goal.CauseNumber = null;
            goal.ViolationNumber = 0;
            goal.PlanID = 0;
            goal.CourtViolationStatusID = 0;
            goal.CourtNumber = null;
            goal.CourtID = 0;
            goal.EmpID = 0;
            goal.ChargeLevel = 0;
            goal.CDI = null;
            actual = false;
            expected = goal.UpdateCaseDetail();
            Assert.AreEqual(expected, actual);
           
        }


        [TestMethod()]
        public void UpdateCaseDetailTestForNegativeValues()
        {
            //clsCaseDetail target = new clsCaseDetail(); // TODO: Initialize to an appropriate value
            // TODO: Initialize to an appropriate value
            goal.TicketViolationID = -10;
            goal.RefCaseNumber = null;
            goal.CauseNumber = null;
            goal.ViolationNumber = -100;
            goal.PlanID = -100;
            goal.CourtViolationStatusID = -10;
            goal.CourtNumber = null;
            goal.CourtID = -10;
            goal.EmpID = -10;
            goal.ChargeLevel = -10;
            goal.CDI = null;
            actual = false;
            expected = goal.UpdateCaseDetail();
            Assert.AreEqual(expected, actual);

        }
        
        [TestMethod()]
        public void UpdateCaseDetailContainsRow()
        {
            goal.TicketViolationID = 5289;
            goal.RefCaseNumber = "id345";
            goal.CauseNumber = "";
            goal.ViolationNumber = 100;
            goal.PlanID = 100;
            goal.CourtViolationStatusID = 10;
            goal.CourtNumber = "381";
            goal.CourtID = 10;
            goal.EmpID = 10;
            goal.ChargeLevel = -10;
            goal.CDI = "pak";
            int expnumber = 1;
            expected = (expnumber >= 0) ? true : false;
            actual = target.UpdateCaseDetail();
            Assert.AreNotEqual(expected, actual);

            
        }
        /// <summary>
        /// This Test method will Check the type of return value. 
        /// </summary> 
        [TestMethod()]
        public void HasSameCourtDateTest()
        {

            clsCaseDetail target = new clsCaseDetail();
            bool expected = true;
            Assert.AreEqual(expected.GetType(), target.HasSameCourtDate(4468).GetType());
        }

        /// <summary>
        /// This Test method will Check the type of return value. 
        /// </summary> 
        [TestMethod()]
        public void HasMoreSpeedingViolationTest()
        {

            clsCaseDetail target = new clsCaseDetail();
            int expected = 0;
            Assert.AreEqual(expected.GetType(), target.HasMoreSpeedingViolation(4468).GetType());
        }

        //Waqas 5864 07/07/2009 ALR Changes
        /// <summary>
        ///A test for GetAttorneysByTickets
        ///</summary>
        [TestMethod()]
        public void GetAttorneysByTicketsTest_ForDataTable()
        {
            clsCaseDetail target = new clsCaseDetail(); // TODO: Initialize to an appropriate value
            int ticketid = 12345; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetAttorneysByTickets(ticketid);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetAttorneysByTickets
        ///</summary>
        [TestMethod()]
        public void GetAttorneysByTicketsTest_ForRows()
        {
            clsCaseDetail target = new clsCaseDetail(); // TODO: Initialize to an appropriate value
            int ticketid = 12345; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetAttorneysByTickets(ticketid);

            bool expected = true;
            bool actualResult = (actual.Rows.Count >= 0) ? true : false;
            Assert.AreEqual(expected, actualResult);
            
        }


        /// <summary>
        ///A test for SetForBondReport
        ///</summary>
        [TestMethod()]
        public void SetForBondReportTestForNull()
        {
            clsCaseDetail target = new clsCaseDetail(); // TODO: Initialize to an appropriate value
            int TicketID = 90379; // TODO: Initialize to an appropriate value            
            string actual;
            actual = target.SetForBondReport(TicketID);
            Assert.IsNotNull(actual);

        }


        /// <summary>
        ///A test for SetForBondReport
        ///</summary>
        [TestMethod()]        
        public void SetForBondReportTest()
        {
            clsCaseDetail target = new clsCaseDetail(); // TODO: Initialize to an appropriate value
            int TicketID = 90379; // TODO: Initialize to an appropriate value
            string expected = "0"; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.SetForBondReport(TicketID);
            Assert.AreEqual(expected, actual);
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using System.Data;

namespace HTPUnitTests
{
    
    /// <summary>
    ///This is a test class for clsJuryOutcome and is intended
    ///to contain all clsJuryOutcome Unit Tests
    ///</summary>
    [TestClass()]
   public class ClsJuryOutComeTest
    {
        /// <summary>
        ///This is a Region for Method GetJuryOutComeReport which  returns DataSet.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///
        #region
        //Yasir Kamal 5859 05/12/2009 Test Cases for Jury Outcome report.  
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void GetJuryOutComeReport_Null()
        {
            ClsJuryOutCome target = new ClsJuryOutCome();
            DateTime docketDate = Convert.ToDateTime("04/15/2009");
            DataSet actual;
            actual = target.GetJuryOutComeReport(docketDate);
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void GetJuryOutComeReport_Type()
        {
            ClsJuryOutCome target = new ClsJuryOutCome();
            DateTime docketDate = Convert.ToDateTime("04/15/2009");
            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;
            actual = target.GetJuryOutComeReport(docketDate);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void GetJuryOutComeReport_Rows()
        {
            ClsJuryOutCome target = new ClsJuryOutCome();
            DateTime docketDate = Convert.ToDateTime("04/15/2009");
            bool expected = false, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = target.GetJuryOutComeReport(docketDate).Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
     
        #endregion


    }
}

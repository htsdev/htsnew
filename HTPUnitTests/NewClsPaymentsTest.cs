﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for NewClsPaymentsTest and is intended
    ///to contain all NewClsPaymentsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class NewClsPaymentsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        //waqas 5864 07/07/2009 ALR
        /// <summary>
        ///A test for GetMaxSchedulePaymentDate
        ///</summary>
        [TestMethod()]
        public void GetMaxSchedulePaymentDateTest()
        {
            NewClsPayments target = new NewClsPayments(); // TODO: Initialize to an appropriate value
            int TicketId = 123456; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetMaxSchedulePaymentDate(TicketId);
            Assert.IsNotNull(actual);
        }
    }
}

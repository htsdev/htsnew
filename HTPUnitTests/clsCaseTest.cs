﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for clsCaseTest and is intended
    ///to contain all clsCaseTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsCaseTest
    {


        private TestContext testContextInstance;
        clsCase target = new clsCase(); // TODO: Initialize to an appropriate value

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Fahad 5722 04/07/2009
        ///A test for UpdateNonHMCFollowUpDate foe zero values
        ///</summary>
        [TestMethod()]
        public void UpdateNonHMCFollowUpDateTestForZero()
        {

            int TicketId = 0; // TODO: Initialize to an appropriate value
            DateTime FollowUpDate = new DateTime(); // TODO: Initialize to an appropriate value
            FollowUpDate = DateTime.Today;
            int empid = 0; // TODO: Initialize to an appropriate value
            target.UpdateNonHMCFollowUpDate(TicketId, FollowUpDate, empid);
            Assert.IsNotNull(target);

        }
        /// <summary>
        ///Fahad 5722 04/07/2009
        ///A test for UpdateNonHMCFollowUpDate For negative Values
        ///</summary>
        [TestMethod()]
        public void UpdateNonHMCFollowUpDateTestForNegativeValues()
        {

            int TicketId = -10; // TODO: Initialize to an appropriate value
            DateTime FollowUpDate = new DateTime(); // TODO: Initialize to an appropriate value
            FollowUpDate = DateTime.Today;
            int empid = -100; // TODO: Initialize to an appropriate value
            target.UpdateNonHMCFollowUpDate(TicketId, FollowUpDate, empid);
            Assert.IsNotNull(target);

        }



        /// <summary>
        ///Fahad 5753 04/07/2009
        ///A test for UpdateSplitCaseFollowUpDate foe zero values
        ///</summary>
        [TestMethod()]
        public void UpdateSplitCaseFollowUpDateTestForZero()
        {

            int TicketId = 0; // TODO: Initialize to an appropriate value
            DateTime FollowUpDate = new DateTime(); // TODO: Initialize to an appropriate value
            FollowUpDate = DateTime.Today;
            int empid = 0; // TODO: Initialize to an appropriate value
            target.UpdateSplitCaseFollowUpDate(TicketId, FollowUpDate, empid);
            Assert.IsNotNull(target);

        }
        /// <summary>
        ///Fahad 5753 04/07/2009
        ///A test for UpdateSplitCaseFollowUpDate For negative Values
        ///</summary>
        [TestMethod()]
        public void UpdateSplitCaseFollowUpDateTestForNegativeValues()
        {

            int TicketId = -10; // TODO: Initialize to an appropriate value
            DateTime FollowUpDate = new DateTime(); // TODO: Initialize to an appropriate value
            FollowUpDate = DateTime.Today;
            int empid = -100; // TODO: Initialize to an appropriate value
            target.UpdateSplitCaseFollowUpDate(TicketId, FollowUpDate, empid);
            Assert.IsNotNull(target);

        }

        //Nasir 5771 04/22/2009
        #region GetClientInfo

        /// <summary>
        ///A test for GetClientInfo
        ///</summary>
        [TestMethod()]
        public void GetClientInfoTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.GetClientInfo(TicketID);
            Assert.AreEqual(expected, actual);

        }
        /// <summary>
        /// A test for GetClientInfo
        /// </summary>
        [TestMethod()]
        public void GetClientInfoForVerseCaseTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int TicketID = 5895236; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.GetClientInfo(TicketID);
            Assert.AreEqual(expected, actual);

        }
        /// <summary>
        /// A test for GetClientInfo
        /// </summary>
        [TestMethod()]
        public void GetClientInfoForNegativeTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int TicketID = -456455; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.GetClientInfo(TicketID);
            Assert.AreEqual(expected, actual);

        }
        #endregion


        #region ALR NULL AREST DATE TEST

        /// <summary>
        ///     A test for CheckALRWithNullArrestDate
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void CheckALRWithNullArrestDateNullParameterTest()
        {
            clsCase target = new clsCase();
            int ticketid = 0;
            target.CheckALRWithNullArrestDate(ticketid);
        }

        /// <summary>
        ///     A test for CheckALRWithNullArrestDate
        ///</summary>
        [TestMethod()]
        public void CheckALRWithNullArrestDateClientCheckTest()
        {
            bool expected = false;
            bool actual = false;
            clsCase target = new clsCase();
            int ticketid = 163843;
            actual = target.CheckALRWithNullArrestDate(ticketid);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///     A test for CheckALRWithNullArrestDate
        ///</summary>
        [TestMethod()]
        public void CheckALRWithNullArrestDateQUoteCLientCheckTest()
        {
            bool expected = true;
            bool actual = false;
            clsCase target = new clsCase();
            int ticketid = 183603;
            actual = target.CheckALRWithNullArrestDate(ticketid);
            Assert.AreEqual(expected, actual);
        }

        #endregion

        /// Fahad 5807 05/20/2009
        ///A test for IsBondFlagUpdated
        ///</summary>
        [TestMethod()]
        public void IsBondFlagUpdatedTestForZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsBondFlagUpdated(ticketid);
            Assert.AreEqual(expected, actual);

        }
        /// Fahad 5807 05/20/2009
        ///A test for IsBondFlagUpdated
        ///</summary>
        [TestMethod()]
        public void IsBondFlagUpdatedTestForNonZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 10; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsBondFlagUpdated(ticketid);
            Assert.AreEqual(expected, actual);

        }
        /// Fahad 5807 05/20/2009
        ///A test for IsBondFlagUpdated
        ///</summary>
        [TestMethod()]
        public void IsBondFlagUpdatedTestForNegative()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = -10; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsBondFlagUpdated(ticketid);
            Assert.AreEqual(expected, actual);

        }

        /// Fahad 5807 05/20/2009
        ///A test for IsMatterDescriptionUpdated
        ///</summary>
        [TestMethod()]
        public void IsMatterDescriptionUpdatedTestForZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsMatterDescriptionUpdated(ticketid);
            Assert.AreEqual(expected, actual);

        }
        /// Fahad 5807 05/20/2009
        ///A test for IsMatterDescriptionUpdated
        ///</summary>
        [TestMethod()]
        public void IsMatterDescriptionUpdatedTestForNonZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 10; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsMatterDescriptionUpdated(ticketid);
            Assert.AreEqual(expected, actual);

        }
        /// Fahad 5807 05/20/2009
        ///A test for IsMatterDescriptionUpdated
        ///</summary>
        [TestMethod()]
        public void IsMatterDescriptionUpdatedTestForNegative()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = -10; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsMatterDescriptionUpdated(ticketid);
            Assert.AreEqual(expected, actual);

        }

        /// Fahad 5807 05/20/2009
        ///A test for AddFTAViolationsNonClient
        ///</summary>
        [TestMethod()]
        public void AddFTAViolationsNonClientTestForZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int Recordid = 0; // TODO: Initialize to an appropriate value
            string MidNumber = string.Empty; // TODO: Initialize to an appropriate value
            string NewCauseNo = string.Empty; // TODO: Initialize to an appropriate value
            int ticketid = 0; // TODO: Initialize to an appropriate value
            int PlanId = 0; // TODO: Initialize to an appropriate value
            CourtViolationStatusType status = new CourtViolationStatusType(); // TODO: Initialize to an appropriate value
            int empId = 0; // TODO: Initialize to an appropriate value
            target.AddFTAViolationsNonClient(Recordid, MidNumber, NewCauseNo, ticketid, PlanId, status, empId);
            Assert.IsNotNull(target);


        }
        /// Fahad 5807 05/20/2009
        ///A test for AddFTAViolationsNonClient
        ///</summary>
        [TestMethod()]
        public void AddFTAViolationsNonClientTestForNonZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int Recordid = 10; // TODO: Initialize to an appropriate value
            string MidNumber = string.Empty; // TODO: Initialize to an appropriate value
            string NewCauseNo = string.Empty; // TODO: Initialize to an appropriate value
            int ticketid = 10; // TODO: Initialize to an appropriate value
            int PlanId = 10; // TODO: Initialize to an appropriate value
            CourtViolationStatusType status = new CourtViolationStatusType(); // TODO: Initialize to an appropriate value
            int empId = 10; // TODO: Initialize to an appropriate value
            target.AddFTAViolationsNonClient(Recordid, MidNumber, NewCauseNo, ticketid, PlanId, status, empId);
            Assert.IsNotNull(target);


        }

        /// Fahad 5807 05/20/2009
        ///A test for UpdateBondforHMC
        ///</summary>
        [TestMethod()]
        public void UpdateBondforHMCTestForZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 0; // TODO: Initialize to an appropriate value
            target.UpdateBondforHMC(ticketid);

        }
        /// Fahad 5807 05/20/2009
        ///A test for UpdateBondforHMC
        ///</summary>
        [TestMethod()]
        public void UpdateBondforHMCTestForNonZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 0; // TODO: Initialize to an appropriate value
            target.UpdateBondforHMC(ticketid);

        }
        /// Fahad 5807 05/20/2009
        ///A test for UpdateBondforHMC
        ///</summary>
        [TestMethod()]
        public void UpdateBondforHMCTestForNegative()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 0; // TODO: Initialize to an appropriate value
            target.UpdateBondforHMC(ticketid);

        }



        /// <summary>
        /// Yasir Kamal 5948 06/04/2009 Test cases.
        ///This is a Region for Method HasDisposedViolations which  returns Boolean Value.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///

        #region HasDisposedViolations

        /// <summary>
        /// this method will check if method returns null or not
        /// </summary>
        [TestMethod()]
        public void HasDisposedViolation_NotNull()
        {
            bool isDisposed;
            clsCase target = new clsCase();
            int TicketID = 0;
            isDisposed = target.HasDisposedViolations(TicketID);

            Assert.IsNotNull(isDisposed);
        }


        /// <summary>
        ///this method will check type of reutrn value.
        ///</summary>
        [TestMethod()]
        public void HasDisposedViolation_type()
        {
            clsCase target = new clsCase();
            int TicketID = 0;
            bool expected = false;
            bool actual;
            actual = target.HasDisposedViolations(TicketID);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }


        #endregion

        /// Fahad 6036 06/16/2009
        ///A test for HasViolationByCourID
        ///</summary>
        [TestMethod()]
        public void HasViolationByCourIDTestForZero()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int Ticketid = 0; // TODO: Initialize to an appropriate value
            int CourtId = 0; // TODO: Initialize to an appropriate value
            int Status = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.HasViolationByCourID(Ticketid, CourtId, Status);
            Assert.AreEqual(expected, actual);
        }


        /// Fahad 6036 06/16/2009
        ///A test for HasViolationByCourID
        ///</summary>
        [TestMethod()]
        public void HasViolationByCourIDTestForNegative()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int Ticketid = -100; // TODO: Initialize to an appropriate value
            int CourtId = -100; // TODO: Initialize to an appropriate value
            int Status = -100; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.HasViolationByCourID(Ticketid, CourtId, Status);
            Assert.AreEqual(expected, actual);
        }

        //Waqas 5864 07/07/2009 ALR

        /// <summary>
        ///A test for GetALRCaseQuestionsInfo
        ///</summary>
        [TestMethod()]
        public void GetALRCaseQuestionsInfoTestForNull()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int TicketID = 12345; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetALRCaseQuestionsInfo(TicketID);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetALRCaseQuestionsInfo
        ///</summary>
        [TestMethod()]
        public void GetALRCaseQuestionsInfoTest_ForTable()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int TicketID = 12345; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetALRCaseQuestionsInfo(TicketID);
            Assert.IsNotNull(actual.Tables[0]);
        }

        /// <summary>
        ///A test for CheckALRWithArrestDateInDaysRange
        ///</summary>
        [TestMethod()]
        public void CheckALRWithArrestDateInDaysRangeTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 12345; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.CheckALRWithArrestDateInDaysRange(ticketid);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for UpdateEmailSentFlag
        ///</summary>
        [TestMethod()]
        public void UpdateEmailSentFlagTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            // Refactoring ticket id should not be Zero.
            //int ticketid = 0; 
            int ticketid = 12345;
            target.UpdateEmailSentFlag(ticketid);
            Assert.AreNotEqual(ticketid, 0);
        }

        #region UpdateClientInfo

        /// <summary>
        ///A test for UpdateClientInfo
        ///</summary>
        [TestMethod()]
        public void UpdateClientInfoTest()
        {
            clsCase target = new clsCase();
            target.TicketID = 67371;
            target.EmpID = 3992;
            target.FirstName = "HERMAN";
            target.LastName = "NATANAEL";
            target.MiddleName = "";
            target.Address1 = "North nazimabad";
            target.Address2 = "";
            target.Contact1 = "123456778";
            target.ContactType1 = 1;
            target.Contact2 = "12312313";
            target.ContactType2 = 2;
            target.Contact3 = "2132131311";
            target.ContactType3 = 3;
            target.DLNumber = "0";
            target.DLStateID = 1;
            target.DOB = "01/01/1900";
            target.SSN = "";
            target.Email = "";
            target.City = "Houston";
            target.StateID = 45;
            target.Zip = "12345";
            target.Race = "Asian";
            target.Gender = "M";
            target.Height = "5'10";
            target.Weight = "123";
            target.HairColor = "";
            target.EyeColor = "";
            target.YDS = "";
            target.AddressConfirmFlag = 0;
            target.EmailNotAvailable = true;
            target.NoDL = 1;
            target.ContinuanceStatus = 0;
            target.ContinuanceDate = Convert.ToDateTime("01/01/1900");
            target.FirmID = 123;
            target.ContinuanceFlag = false;
            target.WalkInClientFlag = "0";
            target.IsPR = 0;
            target.HasConsultationComments = 0;
            target.DP2 = "";
            target.DPC = "";
            target.FLAG1 = "";
            target.UpdateBarcodeInfo = false;
            target.SPN = "";
            target.SmsFlag1 = 0;
            target.SmsFlag2 = 0;
            target.SmsFlag3 = 0;
            bool expected = true;
            bool actual;
            actual = target.UpdateClientInfo();
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        ///A test for UpdateClientInfo
        ///</summary>
        [TestMethod()]        
        public void UpdateClientInfoTestForException()
        {
            clsCase target = new clsCase();
            target.TicketID = 67371;
            target.EmpID = 3992;
            target.FirstName = "HERMAN";
            target.LastName = "NATANAEL";
            target.MiddleName = "";
            target.Address1 = "North nazimabad";
            target.Address2 = "";
            target.Contact1 = "123456778";
            target.ContactType1 = 1;
            target.Contact2 = "12312313";
            target.ContactType2 = 2;
            target.Contact3 = "2132131311";
            target.ContactType3 = 3;
            target.DLNumber = "0";
            target.DLStateID = 1;
            target.DOB = "abcdserf";
            target.SSN = "";
            target.Email = "";
            target.City = "Houston";
            target.StateID = 45;
            target.Zip = "12345";
            target.Race = "Asian";
            target.Gender = "M";
            target.Height = "5'10";
            target.Weight = "123";
            target.HairColor = "";
            target.EyeColor = "";
            target.YDS = "";
            target.AddressConfirmFlag = 0;
            target.EmailNotAvailable = true;
            target.NoDL = 1;
            target.ContinuanceStatus = 0;
            target.ContinuanceDate = Convert.ToDateTime("01/01/1900");
            target.FirmID = 123;
            target.ContinuanceFlag = false;
            target.WalkInClientFlag = "0";
            target.IsPR = 0;
            target.HasConsultationComments = 0;
            target.DP2 = "";
            target.DPC = "";
            target.FLAG1 = "";
            target.UpdateBarcodeInfo = false;
            target.SPN = "";
            target.SmsFlag1 = 0;
            target.SmsFlag2 = 0;
            target.SmsFlag3 = 0;
            bool expected = false;
            bool actual;
            actual = target.UpdateClientInfo();
            Assert.AreEqual(expected, actual);
        }

        #endregion


        /// <summary>
        /// Yasir Kamal 6109 07/17/2009 Test cases.
        ///This is a Region for Method IsNoCourtAssigned which  returns Boolean Value.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///

        #region IsNoCourtAssigned

        /// <summary>
        /// this method will check if method returns null or not
        /// </summary>
        [TestMethod()]
        public void IsNoCourtAssigned_NotNull()
        {
            bool IsNoCourtAssigned;
            clsCase target = new clsCase();
            int TicketID = 0;
            IsNoCourtAssigned = target.IsNoCourtAssigned(TicketID);

            Assert.IsNotNull(IsNoCourtAssigned);
        }


        /// <summary>
        ///this method will check type of reutrn value.
        ///</summary>
        [TestMethod()]
        public void IsNoCourtAssigned_type()
        {
            clsCase target = new clsCase();
            int TicketID = 0;
            bool expected = false;
            bool actual;
            actual = target.IsNoCourtAssigned(TicketID);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }


        #endregion

        #region UpdateClientNewHireStatus

        /// <summary>
        ///A test for UpdateClientNewHireStatus
        ///</summary>
        [TestMethod()]        
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void UpdateClientNewHireStatusForNullTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 0; 
            int empid = 0; 
            int newhirestatus = 0; 
            bool updatewithbusinessdays = false; // TODO: Initialize to an appropriate value
            target.UpdateClientNewHireStatus(ticketid, empid, newhirestatus, updatewithbusinessdays);            
        }

        /// <summary>
        ///A test for UpdateClientNewHireStatus
        ///</summary>
        [TestMethod()]
        public void UpdateClientNewHireStatusTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 79303;
            int empid = 3992;
            int newhirestatus = 0;
            bool updatewithbusinessdays = false; 
            target.UpdateClientNewHireStatus(ticketid, empid, newhirestatus, updatewithbusinessdays);
            Assert.Inconclusive();
        }

        #endregion

        //Waqas 6342 08/26/2009 ALR Criminal
        /// <summary>
        ///To test updating an email of the case.
        ///</summary>
        [TestMethod()]
        public void UpdateCaseTest()
        {
            clsCase target = new clsCase(); 
            bool expected = true; 
            bool actual;
            target.TicketID = 5065;
            target.Email = "abc@abc.com";
            actual = target.UpdateCase();
            Assert.AreEqual(expected, actual);
            
        }


        /// <summary>
        ///Testing of the observing information of the client.
        ///</summary>
        [TestMethod()]
        public void UpdateCaseObservingTest()
        {
            clsCase target = new clsCase();
            bool expected = true;
            bool actual;
            target.TicketID = 5065;
            target.ALROBSArrestingAgency = "MyAgency";
            target.ALROBSContactNumber = "11111111";
            target.ALROBSOfficerCity= "dallas";
            actual = target.UpdateCase();
            Assert.AreEqual(expected, actual);

        }
        //Waqas  6599 10/02/2009 
        /// <summary>
        ///A test for GetCaseEmployer
        ///</summary>
        [TestMethod()]
        public void GetCaseEmployerTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetCaseEmployer();
            Assert.AreEqual(expected, "");
            
        }

        /// <summary>
        ///A test for GetCaseOccupation
        ///</summary>
        [TestMethod()]
        public void GetCaseOccupationTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetCaseOccupation();
            Assert.AreEqual(expected, "");
            
        }


        /// <summary>
        ///A test for CheckForBondReport
        ///</summary>
        [TestMethod()]
        public void CheckForBondReportTestForNull()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 90379; // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.CheckForBondReport(ticketid);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for CheckForBondReport
        ///</summary>
        [TestMethod()]        
        public void CheckForBondReportTest()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 90379; // TODO: Initialize to an appropriate value
            DataTable expected = new DataTable(); // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.CheckForBondReport(ticketid);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for CheckForBondReport
        ///</summary>
        [TestMethod()]
        public void CheckForBondReportTestForCol()
        {
            clsCase target = new clsCase(); // TODO: Initialize to an appropriate value
            int ticketid = 90379; // TODO: Initialize to an appropriate value            
            DataTable actual;
            actual = target.CheckForBondReport(ticketid);
            Assert.AreEqual(1, actual.Rows.Count);
        }
    }
}




﻿using HTP.ClientController;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using RoleBasedSecurity.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for RoleBasedSecurityControllerTest and is intended
    ///to contain all RoleBasedSecurityControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RoleBasedSecurityControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        #region Application

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForNullApplicationDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = null; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForNullApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = null, InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForEmptyApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = " ", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForNumbersApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "123456", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForSpecailCharactersApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "###$%@%<>", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Test", InsertedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Test", InsertedBy = -10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Test", InsertedBy = 10, IsActive = null }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddApplicationTestForExistingApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "POLM", InsertedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        public void AddApplicationTestForValidApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Dallas Added", InsertedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddApplication(applicationDto);

        }

        /// <summary>
        ///A test for UpdateApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForNullApplicationDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = null; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForNullApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = null, ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForEmptyApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = " ", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForNumbersApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "123456", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForSpecailCharactersApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "###$%@%<>", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForZeroUpdatedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Test", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForNegativeUpdatedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Test", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = -10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Test", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 10, IsActive = null }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateApplicationTestForExistingApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDtoTest = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(applicationDtoTest);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "POLM", ApplicationId = Convert.ToInt32(aList[0].ApplicationId), LastUpdatedBy = 10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        public void UpdateApplicationTestForValidApplicationName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto testDto = new ApplicationDto { ApplicationName = "Dallas Added" };
            List<ApplicationDto> aList = target.GetApplicationByName(testDto);
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Dallas Updated", ApplicationId = aList[0].ApplicationId, LastUpdatedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateApplication(applicationDto);

        }

        /// <summary>
        ///A test for DeleteApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteApplicationTestForNullApplicationDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = null; // TODO: Initialize to an appropriate value
            target.DeleteApplication(applicationDto);

        }

        /// <summary>
        ///A test for DeleteApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteApplicationTestForZeroApplicationId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationId = 0 }; // TODO: Initialize to an appropriate value
            target.DeleteApplication(applicationDto);

        }

        /// <summary>
        ///A test for DeleteApplication
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteApplicationTestForNegativeApplicationId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationId = -10 }; // TODO: Initialize to an appropriate value
            target.DeleteApplication(applicationDto);

        }

        /// <summary>
        ///A test for DeleteApplication
        ///</summary>
        [TestMethod()]
        public void DeleteApplicationTestForValidApplication()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationName = "Dallas Updated" };
            List<ApplicationDto> objList = target.GetApplicationByName(applicationDto);
            if (objList != null || objList.Count > 0)
            {
                ApplicationDto aDto = new ApplicationDto { ApplicationId = Convert.ToInt32(objList[0]) };
                target.DeleteApplication(aDto);
            }


        }

        /// <summary>
        ///A test for GetAllApplications
        ///</summary>
        [TestMethod()]
        //[ExpectedException(typeof(ApplicationException))]
        public void GetAllApplicationsTestForTrueIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { IsActive = true }; // TODO: Initialize to an appropriate value
            target.GetAllApplications(applicationDto);
        }

        /// <summary>
        ///A test for GetAllApplications
        ///</summary>
        [TestMethod()]
        //[ExpectedException(typeof(ApplicationException))]
        public void GetAllApplicationsTestForFalseIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { IsActive = false }; // TODO: Initialize to an appropriate value
            target.GetAllApplications(applicationDto);
        }


        /// <summary>
        ///A test for GetApplicationByName
        ///</summary>
        [TestMethod()]
        public void GetApplicationByNameNullTest()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { IsActive = true }; // TODO: Initialize to an appropriate value
            List<ApplicationDto> expected = null; // TODO: Initialize to an appropriate value
            List<ApplicationDto> actual;
            actual = target.GetApplicationByName(applicationDto);
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        ///A test for GetApplicationByName
        ///</summary>
        [TestMethod()]
        public void GetApplicationByNameValidTest()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationId = 1, ApplicationName = "Traffic Program" }; // TODO: Initialize to an appropriate value
            List<ApplicationDto> expected = null; // TODO: Initialize to an appropriate value
            List<ApplicationDto> actual;
            actual = target.GetApplicationByName(applicationDto);

        }

        /// <summary>
        ///A test for GetApplicationById
        ///</summary>
        [TestMethod()]
        public void GetApplicationByIdTest()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ApplicationDto applicationDto = new ApplicationDto { ApplicationId = 1 }; // TODO: Initialize to an appropriate value
            List<ApplicationDto> expected = null; // TODO: Initialize to an appropriate value
            List<ApplicationDto> actual;
            actual = target.GetApplicationById(applicationDto);
            Assert.AreNotEqual(expected, actual);
        }

        #endregion

        #region Company

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForNullCompanyDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = null; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForNullCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = null, InsertedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForEmptyCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = " ", InsertedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForNumbersCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "123456", InsertedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForSpecailCharactersCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "###$%@%<>", InsertedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "Test", InsertedBy = 0, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "Test", InsertedBy = -10, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "Test", InsertedBy = 3991, IsActive = null, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddCompanyTestForExistingCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "Sullo & Sullo", InsertedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" };  // TODO: Initialize to an appropriate value
            target.AddCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        public void AddCompanyTestForValidName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas", InsertedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.AddCompany(companyDto);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for UpdateCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForNullCompanyDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = null; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForNullCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = null, CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForEmptyCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = " ", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForNumbersCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyName = "123456", CompanyId = 21, LastUpdatedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForSpecailCharactersCompanyName()
        {

            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = "###$%@%<>", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 3991, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForZeroUpdatedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = "Test", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 0, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForNegativeUpdatedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = "Test", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = -10, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = "Test", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 10, IsActive = null, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);

        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateCompanyTestForExistingCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = "Sullo & Sullo", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 10, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);
        }

        /// <summary>
        ///A test for AddCompany
        ///</summary>
        [TestMethod()]
        public void UpdateCompanyTestForValidCompanyName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo For Dallas" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyName = "Sullo & Sullo Updated", CompanyId = Convert.ToInt32(cList[0].CompanyId), LastUpdatedBy = 10, IsActive = true, PhoneNumber = "1234567", FaxNumber = "1234567", URL = "http://www.google.com.pk/", HeaderFilePath = "http://www.google.com.pk/", FooterFilePath = "http://www.google.com.pk/" }; // TODO: Initialize to an appropriate value
            target.UpdateCompany(companyDto);
        }


        /// <summary>
        ///A test for DeleteCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteCompanyTestForNullCompanyDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = null; // TODO: Initialize to an appropriate value
            target.DeleteCompany(companyDto);

        }

        /// <summary>
        ///A test for DeleteCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteCompanyTestForZeroCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyId = 0 }; // TODO: Initialize to an appropriate value
            target.DeleteCompany(companyDto);

        }

        /// <summary>
        ///A test for DeleteCompany
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteCompanyTestForNegativeCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDto = new CompanyDto { CompanyId = -10 }; // TODO: Initialize to an appropriate value
            target.DeleteCompany(companyDto);

        }

        /// <summary>
        ///A test for DeleteCompany
        ///</summary>
        [TestMethod()]
        public void DeleteCompanyTestForValidCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            CompanyDto companyDtoTest = new CompanyDto { CompanyName = "Sullo & Sullo Updated" };
            List<CompanyDto> cList = target.GetCompanyByName(companyDtoTest);
            CompanyDto companyDto = new CompanyDto { CompanyId = Convert.ToInt32(cList[0].CompanyId) }; // TODO: Initialize to an appropriate value
            target.DeleteCompany(companyDto);

        }

        #endregion

        #region Role

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForNullRoleDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = null; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForNullRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = null, InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForEmptyRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = " ", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForNumbersRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "123456", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForSpecailCharactersRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "###$%@%<>", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Test", InsertedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Test", InsertedBy = -10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Test", InsertedBy = 10, IsActive = null }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddRoleTestForExistingRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Primary User", InsertedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddRole(roleDto);

        }

        /// <summary>
        ///A test for UpdateRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForNullRoleDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = null; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForNullRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = null, LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForEmptyRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = " ", LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForNumbersRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "123456", LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForSpecailCharactersRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "###$%@%<>", LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Test", LastUpdatedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Test", LastUpdatedBy = -10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Test", LastUpdatedBy = 10, IsActive = null }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }

        /// <summary>
        ///A test for AddRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateRoleTestForExistingRoleName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleName = "Primary User", LastUpdatedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateRole(roleDto);

        }


        /// <summary>
        ///A test for DeleteRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteRoleTestForNullRoleDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = null; // TODO: Initialize to an appropriate value
            target.DeleteRole(roleDto);

        }

        /// <summary>
        ///A test for DeleteRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteRoleTestForZeroRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleId = 0 }; // TODO: Initialize to an appropriate value
            target.DeleteRole(roleDto);

        }

        /// <summary>
        ///A test for DeleteRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteRoleTestForNegativeRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            RoleDto roleDto = new RoleDto { RoleId = -10 }; // TODO: Initialize to an appropriate value
            target.DeleteRole(roleDto);

        }

        #endregion

        #region Process

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForNullProcessDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = null; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForNullProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = null, InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForEmptyProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = " ", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForNumbersProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "123456", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForSpecailCharactersProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "###$%@%<>", InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Test", InsertedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Test", InsertedBy = -10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Test", InsertedBy = 10, IsActive = null }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddProcessTestForExistingProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Can Edit Prospect Case Status", InsertedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddProcess(processDto);

        }

        /// <summary>
        ///A test for UpdateProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForNullProcessDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = null; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForNullProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = null, LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForEmptyProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = " ", LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForNumbersProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "123456", LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForSpecailCharactersProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "###$%@%<>", LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Test", LastUpdatedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Test", LastUpdatedBy = -10, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Test", LastUpdatedBy = 10, IsActive = null }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessTestForExistingProcessName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessName = "Can Edit Prospect Case Status", LastUpdatedBy = 25, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateProcess(processDto);

        }


        /// <summary>
        ///A test for DeleteRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteProcessTestForNullProcessDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = null; // TODO: Initialize to an appropriate value
            target.DeleteProcess(processDto);

        }

        /// <summary>
        ///A test for DeleteProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteProcessTestForZeroProcessId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessId = 0 }; // TODO: Initialize to an appropriate value
            target.DeleteProcess(processDto);

        }

        /// <summary>
        ///A test for DeleteProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteProcessTestForNegativeProcessId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            ProcessDto processDto = new ProcessDto { ProcessId = -10 }; // TODO: Initialize to an appropriate value
            target.DeleteProcess(processDto);

        }

        #endregion

        #region UserInRole

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForNullUserInRoleDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = null; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForZeroUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 0, CompanyId = 12, RoleId = 2, InsertedBy = 3991, IsActive = true };// TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForNegativeUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = -10, CompanyId = 12, RoleId = 2, InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }


        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForZeroRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 12, RoleId = 0, InsertedBy = 3991, IsActive = true };// TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForNegativeRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 12, RoleId = -2, InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForZeroCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 0, RoleId = 10, InsertedBy = 3991, IsActive = true };// TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForNegativeCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = -12, RoleId = 2, InsertedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 12, RoleId = 2, InsertedBy = 3991, IsActive = null }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = -12, RoleId = 2, InsertedBy = -91, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = -12, RoleId = 2, InsertedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserInRoleTestForExistingUserInRole()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 1, CompanyId = 1, RoleId = 2, InsertedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.AddUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForNullUserInRoleDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = null; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForZeroUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 0, CompanyId = 12, RoleId = 2, LastUpdatedBy = 3991, IsActive = true };// TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForNegativeUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = -10, CompanyId = 12, RoleId = 2, LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }


        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForZeroRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 12, RoleId = 0, LastUpdatedBy = 3991, IsActive = true };// TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForNegativeRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 12, RoleId = -2, LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for AddProcess
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForZeroCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 0, RoleId = 10, LastUpdatedBy = 3991, IsActive = true };// TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForNegativeCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = -12, RoleId = 2, LastUpdatedBy = 3991, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForNullIsActive()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = 12, RoleId = 2, LastUpdatedBy = 3991, IsActive = null }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForNegativeLastUpdatedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = -12, RoleId = 2, LastUpdatedBy = -91, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForZeroLastUpdatedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 10, CompanyId = -12, RoleId = 2, LastUpdatedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for UpdateUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserInRoleTestForExistingUserInRole()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto processDto = new UserInRolesDto { UserId = 1, CompanyId = 1, RoleId = 2, LastUpdatedBy = 0, IsActive = true }; // TODO: Initialize to an appropriate value
            target.UpdateUserInRole(processDto);

        }

        /// <summary>
        ///A test for DeleteUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteUserInRoleTestForZeroUserRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto userRoleDto = new UserInRolesDto { UserRoleId = 0 }; // TODO: Initialize to an appropriate value
            target.DeleteUserInRole(userRoleDto);

        }

        /// <summary>
        ///A test for DeleteUserInRole
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteUserInRoleTestForNegativeValues()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserInRolesDto userRoleDto = new UserInRolesDto { UserRoleId = -10 }; // TODO: Initialize to an appropriate value
            target.DeleteUserInRole(userRoleDto);

        }

        #endregion

        #region UserAccessRights


        /// <summary>
        ///A test for UpdateMenuRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateMenuRoleRightsTestForNullUserAccessRightsDto()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto(); // TODO: Initialize to an appropriate value
            target.UpdateMenuRoleRights(userAccessRightsDto);

        }
        /// <summary>
        ///A test for UpdateMenuRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateMenuRoleRightsTestForZeroMenuId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanView = true, CanAdd = true, CanEdit = true, CanDelete = true, RightsId = 10, MenuId = 0 }; // TODO: Initialize to an appropriate value
            target.UpdateMenuRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateMenuRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateMenuRoleRightsTestForNegativeMenuId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanView = true, CanAdd = true, CanEdit = true, CanDelete = true, RightsId = 10, MenuId = -10 }; // TODO: Initialize to an appropriate value
            target.UpdateMenuRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateMenuRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateMenuRoleRightsTestForZeroRoghtsId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanView = true, CanAdd = true, CanEdit = true, CanDelete = true, RightsId = 0, MenuId = 10 }; // TODO: Initialize to an appropriate value
            target.UpdateMenuRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateMenuRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateMenuRoleRightsTestForNegativeRightsId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanView = true, CanAdd = true, CanEdit = true, CanDelete = true, RightsId = -10, MenuId = 10 }; // TODO: Initialize to an appropriate value
            target.UpdateMenuRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateProcessRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessRoleRightsTestForNullUserAccessRightsDto()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = null; // TODO: Initialize to an appropriate value
            target.UpdateProcessRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateProcessRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessRoleRightsTestForZeroRightsId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanExecute = true, RightsId = 0, ProcessId = 10 }; // TODO: Initialize to an appropriate value
            target.UpdateProcessRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateProcessRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessRoleRightsTestForNegativeRightsId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanExecute = true, RightsId = -10, ProcessId = 10 }; // TODO: Initialize to an appropriate value
            target.UpdateProcessRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateProcessRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessRoleRightsTestForZeroProcessId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanExecute = true, RightsId = 10, ProcessId = 0 }; // TODO: Initialize to an appropriate value
            target.UpdateProcessRoleRights(userAccessRightsDto);

        }

        /// <summary>
        ///A test for UpdateProcessRoleRights
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateProcessRoleRightsTestForNegativeProcessId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserAccessRightsDto userAccessRightsDto = new UserAccessRightsDto { CanExecute = true, RightsId = 10, ProcessId = -10 }; // TODO: Initialize to an appropriate value
            target.UpdateProcessRoleRights(userAccessRightsDto);

        }

        #endregion

        #region User

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNullUserDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = null; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }


        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNullUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForEmptyUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = " ", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "<>###<> ", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidLengthUserName()
        {
            StringBuilder sb = new StringBuilder(60);
            sb.Insert(0, "a", 60);
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = sb.ToString(), Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForExistingUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNullFirstName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = null, LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForEmptyFirstName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = " ", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidFirstName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "<>###<>", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidLengthFirstName()
        {
            StringBuilder sb = new StringBuilder(60);
            sb.Insert(0, "a", 60);
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = sb.ToString(), LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNullLastName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Fahim", LastName = null, IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForEmptyLastName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Fahim", LastName = " ", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidLastName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Fahim", LastName = "<>###<>", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidLengthLastName()
        {
            StringBuilder sb = new StringBuilder(60);
            sb.Insert(0, "a", 60);
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = sb.ToString(), IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForInvalidEmail()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "InvalidEmail", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNullEmail()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = -10, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 0, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForEmptyPassword()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = " ", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 0, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNegativeRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 10, RoleId = -2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForZeroRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 10, RoleId = 0, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNegativeCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 10, RoleId = 2, CompanyId = -2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForZeroCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 10, RoleId = 10, CompanyId = 0, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForNegativeUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 10, RoleId = 2, CompanyId = 2, UserId = -2 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for AddUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void AddUserTestForZeroUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = 10, CompanyId = 10, UserId = 0 }; // TODO: Initialize to an appropriate value
            target.AddUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNullUserDTO()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = null; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }


        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNullUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForEmptyUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = " ", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "<>###<> ", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidLengthUserName()
        {
            StringBuilder sb = new StringBuilder(60);
            sb.Insert(0, "a", 60);
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = sb.ToString(), Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForExistingUserName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "UnitTest", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNullFirstName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = null, LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForEmptyFirstName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = " ", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidFirstName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "<>###<>", LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidLengthFirstName()
        {
            StringBuilder sb = new StringBuilder(60);
            sb.Insert(0, "a", 60);
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = sb.ToString(), LastName = "UnitTest", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNullLastName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Fahim", LastName = null, IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForEmptyLastName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Fahim", LastName = " ", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidLastName()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Fahim", LastName = "<>###<>", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidLengthLastName()
        {
            StringBuilder sb = new StringBuilder(60);
            sb.Insert(0, "a", 60);
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = sb.ToString(), IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForInvalidEmail()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "InvalidEmail", NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNullEmail()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = null, NtUserId = "123", InsertedBy = 3991, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNegativeInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = -10, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForZeroInsertedBy()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 0, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForEmptyPassword()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = " ", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 0, RoleId = 2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNegativeRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = -2, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForZeroRoleId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = 0, CompanyId = 2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNegativeCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = 2, CompanyId = -2, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForZeroCompanyId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = 10, CompanyId = 0, UserId = 2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForNegativeUserId()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = 2, CompanyId = 2, UserId = -2 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }

        /// <summary>
        ///A test for UpdateUser
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateUserTestForZeroUserd()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            UserDto userDto = new UserDto { UserName = "Fahim", Password = "12345", FirstName = "Test", LastName = "Test", IsActive = true, Abbreviation = "Try", Email = "Jhon@abc.com", NtUserId = "123", InsertedBy = 10, RoleId = 10, CompanyId = 10, UserId = 0 }; // TODO: Initialize to an appropriate value
            target.UpdateUser(userDto);
        }


        /// <summary>
        ///A test for GetAllAttorney
        ///</summary>
        [TestMethod()]
        public void GetAllAttorneyNullTest()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            List<UserDto> expected = null; // TODO: Initialize to an appropriate value
            List<UserDto> actual;
            actual = target.GetAllAttorney();
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetAllAttorney
        ///</summary>
        [TestMethod()]
        public void GetAllAttorneyValidTest()
        {
            RoleBasedSecurityController target = new RoleBasedSecurityController(); // TODO: Initialize to an appropriate value
            target.GetAllAttorney();
        }

        #endregion






    }
}





﻿using lntechNew.Components.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for AttorneySchedulerServiceTest and is intended
    ///to contain all AttorneySchedulerServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AttorneySchedulerServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for InsertAttorneySchedule
        ///</summary>
        [TestMethod()]
        public void InsertAttorneyScheduleTestOne()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            string DocketDate = "12/13/2009"; // TODO: Initialize to an appropriate value
            int AttorneyID = 4030; // TODO: Initialize to an appropriate value
            string ContactNumber = "456421354"; // TODO: Initialize to an appropriate value
            int EmployeeID = 3992; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.InsertAttorneySchedule(DocketDate, AttorneyID, ContactNumber, EmployeeID);
            Assert.AreEqual(expected, actual);

        }


        /// <summary>
        ///A test for InsertAttorneySchedule
        ///</summary>
        [TestMethod()]
        public void InsertAttorneyScheduleTestTwo()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            string DocketDate = "12/01/2010"; // TODO: Initialize to an appropriate value
            int AttorneyID = 4020; // TODO: Initialize to an appropriate value
            string ContactNumber = "77421354"; // TODO: Initialize to an appropriate value
            int EmployeeID = 3992; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.InsertAttorneySchedule(DocketDate, AttorneyID, ContactNumber, EmployeeID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for UpdateAttorneySchedule
        ///</summary>
        [TestMethod()]

        public void UpdateAttorneyScheduleTestOne()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            int AttorneyScheduleID = 4; // TODO: Initialize to an appropriate value
            string DocketDate = "12/12/2009"; // TODO: Initialize to an appropriate value
            int AttorneyID = 4036; // TODO: Initialize to an appropriate value
            string ContactNumber = "789644233"; // TODO: Initialize to an appropriate value
            int EmployeeID = 3992; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UpdateAttorneySchedule(AttorneyScheduleID, DocketDate, AttorneyID, ContactNumber, EmployeeID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for UpdateAttorneySchedule
        ///</summary>
        [TestMethod()]

        public void UpdateAttorneyScheduleTestTwo()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            int AttorneyScheduleID = 9; // TODO: Initialize to an appropriate value
            string DocketDate = "12/10/2010"; // TODO: Initialize to an appropriate value
            int AttorneyID = 4016; // TODO: Initialize to an appropriate value
            string ContactNumber = "789644233"; // TODO: Initialize to an appropriate value
            int EmployeeID = 3992; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UpdateAttorneySchedule(AttorneyScheduleID, DocketDate, AttorneyID, ContactNumber, EmployeeID);
            Assert.AreEqual(expected, actual);

        }



        /// <summary>
        ///A test for GetShowSettingWeeks
        ///</summary>
        [TestMethod()]
        public void GetShowSettingWeeksTestromwscount()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetShowSettingWeeks();
            Assert.AreEqual(9, actual.Rows.Count);

        }



        /// <summary>
        ///A test for GetShowSettingWeeks
        ///</summary>
        [TestMethod()]
        public void GetShowSettingWeeksTestisnotnull()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value                
            DataTable actual;
            actual = target.GetShowSettingWeeks();
            Assert.IsNotNull(actual);

        }

        /// <summary>
        ///A test for DeleteAttorneySchedule
        ///</summary>
        [TestMethod()]
        public void DeleteAttorneyScheduleTest()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            int AttorneyScheduleID = 8; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.DeleteAttorneySchedule(AttorneyScheduleID);
            Assert.AreEqual(expected, actual);

        }


        /// <summary>
        ///A test for DeleteAttorneySchedule
        ///</summary>
        [TestMethod()]
        public void DeleteAttorneyScheduleTestIsNotNull()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            int AttorneyScheduleID = 8; // TODO: Initialize to an appropriate value                
            bool actual;
            actual = target.DeleteAttorneySchedule(AttorneyScheduleID);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetAttorneysWeeklySettings
        ///</summary>
        [TestMethod()]
        public void GetAttorneysWeeklySettingsTestIsNotNull()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            string StartDate = "10/10/2009"; // TODO: Initialize to an appropriate value
            string EndDate = "12/10/2009"; // TODO: Initialize to an appropriate value                
            DataTable actual;
            actual = target.GetAttorneysWeeklySettings(StartDate, EndDate);
            Assert.IsNotNull(actual);

        }

        /// <summary>
        ///A test for GetAttorneysWeeklySettings
        ///</summary>
        [TestMethod()]
        public void GetAttorneysWeeklySettingsTestRowsCount()
        {
            AttorneySchedulerService target = new AttorneySchedulerService(); // TODO: Initialize to an appropriate value
            string StartDate = "10/10/2009"; // TODO: Initialize to an appropriate value
            string EndDate = "12/10/2009"; // TODO: Initialize to an appropriate value                
            DataTable actual;
            actual = target.GetAttorneysWeeklySettings(StartDate, EndDate);
            Assert.AreEqual(true, actual.Rows.Count > 0);

        }
    }
}

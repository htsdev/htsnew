﻿using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for FinancialReportsTest and is intended
    ///to contain all FinancialReportsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FinancialReportsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///This test method will check not null of resulted Dataset
        ///</summary>
        [TestMethod()]
        public void GetPaymentDetailByDateByRepTest_NotNull()
        {
            FinancialReports target = new FinancialReports(); 
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/26/2009");
            DataSet actual;
            actual = target.GetPaymentDetailByDateByRep(QueryDate, QueryDateTo);
            Assert.IsNotNull(actual);
           
        }

        /// <summary>
        ///This test method will check type of resulted Dataset
        ///</summary>
        [TestMethod()]
        public void GetPaymentDetailByDateByRepTest_Type()
        {
            FinancialReports target = new FinancialReports();
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/26/2009");
            DataSet expected = new DataSet();
            DataSet actual;
            actual = target.GetPaymentDetailByDateByRep(QueryDate, QueryDateTo);
            Assert.AreEqual(expected.GetType(), actual.GetType());
            
        }

        /// <summary>
        ///This test is for checking the dataset is not null or not
        ///</summary>
        [TestMethod()]
        public void GetWeeklyPaymentDetailByDateByRepTest_NotNull()
        {
            FinancialReports target = new FinancialReports();
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DataSet actual;
            actual = target.GetWeeklyPaymentDetailByDateByRep(QueryDate);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///This test is for checking the dataset type
        ///</summary>
        [TestMethod()]
        public void GetWeeklyPaymentDetailByDateByRepTest_Type()
        {
            FinancialReports target = new FinancialReports();
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DataSet expected = new DataSet();
            DataSet actual;
            actual = target.GetWeeklyPaymentDetailByDateByRep(QueryDate);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for GetPaymentSummaryByDate
        ///</summary>
        [TestMethod()]
        public void GetPaymentSummaryByDateTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DataSet actual;
            actual = target.GetPaymentSummaryByDate(QueryDate);
            Assert.IsNotNull(actual);
            
        }

        /// <summary>
        ///A test for GetPaymentSummaryByDate
        ///</summary>
        [TestMethod()]
        public void GetPaymentSummaryByDateTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DataSet expected = new DataSet();
            DataSet actual;
            actual = target.GetPaymentSummaryByDate(QueryDate);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for GetPaymentDetailsForCreditCards
        ///</summary>
        [TestMethod()]
        public void GetPaymentDetailsForCreditCardsTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            int RepID = 0;  
            int PayID = 0;  
            int CourtID = 0;  
            DataSet actual;
            actual = target.GetPaymentDetailsForCreditCards(QueryDate, QueryDateTo, RepID, PayID, CourtID);
            Assert.IsNotNull(actual);
            
        }

        /// <summary>
        ///A test for GetPaymentDetailsForCreditCards
        ///</summary>
        [TestMethod()]
        public void GetPaymentDetailsForCreditCardsTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            int RepID = 0;  
            int PayID = 0;  
            int CourtID = 0;  
            DataSet expected = new DataSet();
            DataSet actual;
            actual = target.GetPaymentDetailsForCreditCards(QueryDate, QueryDateTo, RepID, PayID, CourtID);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for GetPaymentDetailsForByCriteriaRange
        ///</summary>
        [TestMethod()]
        public void GetPaymentDetailsForByCriteriaRangeTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            int RepID = 0;  
            int PayID = 0;  
            int CourtID = 0;  
            DataSet actual;
            actual = target.GetPaymentDetailsForByCriteriaRange(QueryDate, QueryDateTo, RepID, PayID, CourtID);
            Assert.IsNotNull(actual);
            
        }

        /// <summary>
        ///A test for GetPaymentDetailsForByCriteriaRange
        ///</summary>
        [TestMethod()]
        public void GetPaymentDetailsForByCriteriaRangeTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            int RepID = 0;  
            int PayID = 0;  
            int CourtID = 0;  
            DataSet expected = new DataSet();  
            DataSet actual;
            actual = target.GetPaymentDetailsForByCriteriaRange(QueryDate, QueryDateTo, RepID, PayID, CourtID);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for GetAllPaymentTypeSumByDate
        ///</summary>
        [TestMethod()]
        public void GetAllPaymentTypeSumByDateTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            DataSet actual;
            actual = target.GetAllPaymentTypeSumByDate(QueryDate, QueryDateTo);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetAllPaymentTypeSumByDate
        ///</summary>
        [TestMethod()]
        public void GetAllPaymentTypeSumByDateTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            DataSet expected = new DataSet();  
            DataSet actual;
            actual = target.GetAllPaymentTypeSumByDate(QueryDate, QueryDateTo);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for GetAllCourtsInfoByDateRange
        ///</summary>
        [TestMethod()]
        public void GetAllCourtsInfoByDateRangeTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            DataSet actual;
            actual = target.GetAllCourtsInfoByDateRange(QueryDate, QueryDateTo);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetAllCourtsInfoByDateRange
        ///</summary>
        [TestMethod()]
        public void GetAllCourtsInfoByDateRangeTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            DataSet expected = new DataSet();  
            DataSet actual;
            actual = target.GetAllCourtsInfoByDateRange(QueryDate, QueryDateTo);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for GetAllMailerSummary
        ///</summary>
        [TestMethod()]
        public void GetAllMailerSummaryTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            int RepID = 0;  
            int PayID = 0;  
            int CourtID = 0;  
            DataSet actual;
            actual = target.GetAllMailerSummary(QueryDate, QueryDateTo, RepID, PayID, CourtID);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetAllMailerSummary
        ///</summary>
        [TestMethod()]
        public void GetAllMailerSummaryTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            int RepID = 0;  
            int PayID = 0;  
            int CourtID = 0;  
            DataSet expected = new DataSet();  
            DataSet actual;
            actual = target.GetAllMailerSummary(QueryDate, QueryDateTo, RepID, PayID, CourtID);
            Assert.AreEqual(expected.GetType(), actual.GetType());
            
        }

        /// <summary>
        ///A test for GetAllCourtsForCloseOut
        ///</summary>
        [TestMethod()]
        public void GetAllCourtsForCloseOutTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DataSet actual;
            actual = target.GetAllCourtsForCloseOut();
            Assert.IsNotNull(actual);            
        }

        /// <summary>
        ///A test for GetAllCourtsForCloseOut
        ///</summary>
        [TestMethod()]
        public void GetAllCourtsForCloseOutTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DataSet expected = new DataSet();              
            DataSet actual;
            actual = target.GetAllCourtsForCloseOut();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for GetAllRepList
        ///</summary>
        [TestMethod()]
        public void GetAllRepListTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            DataSet actual;
            actual = target.GetAllRepList(QueryDate, QueryDateTo);
            Assert.IsNotNull(actual);            
        }

        /// <summary>
        ///A test for GetAllRepList
        ///</summary>
        [TestMethod()]
        public void GetAllRepListTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DateTime QueryDate = Convert.ToDateTime("01/25/2009");
            DateTime QueryDateTo = Convert.ToDateTime("01/25/2009");
            DataSet expected = new DataSet();  
            DataSet actual;
            actual = target.GetAllRepList(QueryDate, QueryDateTo);
            Assert.AreEqual(expected.GetType(), actual.GetType());            
        }

        /// <summary>
        ///A test for GetPaymentTypeForCloseOut
        ///</summary>
        [TestMethod()]
        public void GetPaymentTypeForCloseOutTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DataSet actual;
            actual = target.GetPaymentTypeForCloseOut();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetPaymentTypeForCloseOut
        ///</summary>
        [TestMethod()]
        public void GetPaymentTypeForCloseOutTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DataSet expected = new DataSet();  
            DataSet actual;
            actual = target.GetPaymentTypeForCloseOut();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for DeleteWeeklyPaymentDetailsByDate
        ///</summary>
        [TestMethod()]
        public void DeleteWeeklyPaymentDetailsByDateTest()
        {
            FinancialReports target = new FinancialReports();  
            DateTime TransDate = Convert.ToDateTime("01/25/2009");
            bool expected = false;  
            bool actual;
            actual = target.DeleteWeeklyPaymentDetailsByDate(TransDate);
            Assert.AreNotEqual(expected, actual);            
        }

        /// <summary>
        ///A test for InsertWeeklyPaymentDetails
        ///</summary>
        [TestMethod()]
        public void InsertWeeklyPaymentDetailsTest()
        {
            FinancialReports target = new FinancialReports();  
            int EmployeeID = 0;  
            DateTime TransDate = Convert.ToDateTime("01/25/2009");
            Decimal Cash = 0;
            Decimal Check = 0;
            string Notes = "";
            bool expected = false;  
            bool actual;
            actual = target.InsertWeeklyPaymentDetails(EmployeeID, TransDate, Cash, Check, Notes);
            Assert.AreNotEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for GetEmailSetting
        ///</summary>
        [TestMethod()]
        public void GetEmailSettingTest_NotNull()
        {
            FinancialReports target = new FinancialReports();  
            DataSet actual;
            actual = target.GetEmailSetting();
            Assert.IsNotNull(actual);            
        }

        /// <summary>
        ///A test for GetEmailSetting
        ///</summary>
        [TestMethod()]
        public void GetEmailSettingTest_Type()
        {
            FinancialReports target = new FinancialReports();  
            DataSet expected = new DataSet();
            DataSet actual;
            actual = target.GetEmailSetting();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        #region FillCategoryType
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void FillCategoryType_Null()
        {
            object[] value1 = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();

            DataSet actual;

            actual = target.FillCategoryType();
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void FillCategoryType_Type()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.FillCategoryType();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void FillCategoryType_Rows()
        {
            FinancialReports target = new FinancialReports();
            bool expected = false, actual = false;
            int expnumber = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = (expnumber >= 0) ? true : false;
            actual = target.FillCategoryType().Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 5  . 
        /// </summary>
        [TestMethod()]
        public void FillCategoryType_NoOfCol()
        {
            FinancialReports target = new FinancialReports();

            int expected = 0, actual = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = 4;
            actual = target.FillCategoryType().Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);


        }
        /// <summary>
        /// This Test method will Check that the No of parameter pass to stored procedure . 
        /// </summary>          
        [TestMethod()]
        public void FillCategoryType_NoOfParam()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            target.Values = value;
            int expected = 3;
            int actual = 0;

            actual = target.Values.Length;
            Assert.AreEqual(expected, actual);
        }
        #endregion
        #region FillCreditType
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void FillCreditType_Null()
        {
            object[] value1 = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            DataSet actual;

            actual = target.FillCreditType();
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void FillCreditType_Type()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.FillCreditType();
            Assert.AreEqual(expected.GetType(), actual.GetType());


        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        [TestMethod()]
        public void FillCreditType_Rows()
        {
            FinancialReports target = new FinancialReports();
            bool expected = false, actual = false;
            int expnumber = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = (expnumber >= 0) ? true : false;
            actual = target.FillCreditType().Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 3  . 
        /// </summary>
        [TestMethod()]
        public void FillCreditType_NoOfCol()
        {
            FinancialReports target = new FinancialReports();

            int expected = 0, actual = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = 3;

            actual = target.FillCreditType().Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);


        }
        /// <summary>
        /// This Test method will Check that the No of parameter pass to stored procedure . 
        /// </summary> 
        [TestMethod()]
        public void FillCreditType_NoOfParam()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            target.Values = value;
            int expected = 3;
            int actual = 0;
            actual = target.Values.Length;
            Assert.AreEqual(expected, actual);
        }
        #endregion
        #region GetPaymentTypeSumByDate
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void GetPaymentTypeSumByDate_Null()
        {
            object[] value1 = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            DataSet actual;
            actual = target.GetPaymentTypeSumByDate();
            Assert.IsNotNull(actual);
        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void GetPaymentTypeSumByDate_Type()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.GetPaymentTypeSumByDate();
            Assert.AreEqual(expected.GetType(), actual.GetType());


        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        [TestMethod()]
        public void GetPaymentTypeSumByDate_Rows()
        {
            FinancialReports target = new FinancialReports();
            bool expected = false, actual = false;
            int expnumber = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = (expnumber >= 0) ? true : false;

            actual = target.GetPaymentTypeSumByDate().Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 5  . 
        /// </summary>
        [TestMethod()]
        public void GetPaymentTypeSumByDate_NoOfCol()
        {
            FinancialReports target = new FinancialReports();

            int expected = 0, actual = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = 5;

            actual = target.GetPaymentTypeSumByDate().Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);

        }
        /// <summary>
        /// This Test method will Check that the No of parameter pass to stored procedure . 
        /// </summary> 
        [TestMethod()]
        public void GetPaymentTypeSumByDate_NoOfParam()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            target.Values = value;
            int expected = 3;
            int actual = 0;
            actual = target.Values.Length;
            Assert.AreEqual(expected, actual);
        }
        #endregion
        #region GetCourtSummary
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void GetCourtSummary_Null()
        {
            object[] value1 = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            DataSet actual;
            actual = target.GetPaymentTypeSumByDate();
            Assert.IsNotNull(actual);

        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void GetCourtSummary_Type()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.GetCourtSummary();
            Assert.AreEqual(expected.GetType(), actual.GetType());


        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        [TestMethod()]
        public void GetCourtSummary_Rows()
        {
            FinancialReports target = new FinancialReports();
            bool expected = false, actual = false;
            int expnumber = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = (expnumber >= 0) ? true : false;

            actual = target.GetCourtSummary().Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);


        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 7  . 
        /// </summary>
        [TestMethod()]
        public void GetCourtSummary_NoOfCol()
        {
            FinancialReports target = new FinancialReports();

            int expected = 0, actual = 0;
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            target.Values = value;
            expected = 7;

            actual = target.GetCourtSummary().Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);


        }
        /// <summary>
        /// This Test method will Check that the No of parameter pass to stored procedure . 
        /// </summary> 
        [TestMethod()]
        public void GetCourtSummary_NoOfParam()
        {
            object[] value = { "01/19/2009", "01/19/2009", 3991 };
            FinancialReports target = new FinancialReports();
            target.Values = value;
            int expected = 3;
            int actual = 0;

            actual = target.Values.Length;
            Assert.AreEqual(expected, actual);

        }
        #endregion
    }
}

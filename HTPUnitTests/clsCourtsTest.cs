﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data.Common;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for clsCourtsTest and is intended
    ///to contain all clsCourtsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsCourtsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AddCourt
        ///</summary>
        [TestMethod()]      
        public void AddCourtTest()
        {
            clsCourts target = new clsCourts(); // TODO: Initialize to an appropriate value
            DbTransaction trans = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.AddCourt(trans);
            Assert.AreEqual(expected, actual);
           
        }
        /// <summary>
        ///A test for AddCourt return type
        ///</summary>
        [TestMethod()]
        public void AddCourtByTypeTest()
        {
            clsCourts target = new clsCourts(); // TODO: Initialize to an appropriate value
            DbTransaction trans = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.AddCourt(trans);
            Assert.AreEqual(expected.GetType(), actual.GetType());

        }

        /// <summary>
        ///A test for UpdateCourt
        ///</summary>
        [TestMethod()]        
        public void UpdateCourtTest()
        {
            clsCourts target = new clsCourts(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UpdateCourt();
            Assert.AreEqual(expected, actual);            
        }
        /// <summary>
        ///A test for UpdateCourt return type
        ///</summary>
        [TestMethod()]
        public void UpdateCourtByTypeTest()
        {
            clsCourts target = new clsCourts(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UpdateCourt();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
    }
}

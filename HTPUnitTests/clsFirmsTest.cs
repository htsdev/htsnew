﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using System;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for clsFirmsTest and is intended
    ///to contain all clsFirmsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsFirmsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        //Fahad 6054 07/25/2009 
        /// <summary>
        ///A test for GetAllOutsideFirmsInfo
        ///</summary>
        [TestMethod()]
        public void GetAllOutsideFirmsInfoTest()
        {
            clsFirms target = new clsFirms(); // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.GetAllOutsideFirmsInfo();
            Assert.AreNotEqual(expected, actual);

        }
        //Fahad 6054 07/25/2009 
        /// <summary>
        ///A test for GetFirmByTicketId
        ///</summary>
        [TestMethod()]
        public void GetFirmByTicketIdTest()
        {
            clsFirms target = new clsFirms(); // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetFirmByTicketId(TicketID);
            Assert.AreEqual(expected, actual);
        }
        //Fahad 6054 07/25/2009 
        /// <summary>
        ///A test for GetFirmByTicketId
        ///</summary>
        [TestMethod()]
        public void GetFirmByTicketIdTestForNegative()
        {
            clsFirms target = new clsFirms(); // TODO: Initialize to an appropriate value
            int TicketID = -10; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetFirmByTicketId(TicketID);
            Assert.AreEqual(expected, actual);
        }
        //Fahad 6054 07/25/2009 
        /// <summary>
        ///A test for GetFirmByTicketId
        ///</summary>
        [TestMethod()]
        public void GetFirmByTicketIdTestForPositive()
        {
            clsFirms target = new clsFirms(); // TODO: Initialize to an appropriate value
            int TicketID = 124546; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetFirmByTicketId(TicketID);
            Assert.AreNotEqual(expected, actual);
        }


        #region GetFirmChargeLevelAmount

        /// <summary>
        ///     A test for GetFirmChargeLevelAmount
        ///</summary>
        [TestMethod()]
        public void GetFirmChargeLevelAmountTest()
        {
            clsFirms target = new clsFirms();
            int firmID = 3000;
            DataTable expected = new DataTable();
            DataTable actual = new DataTable();
            actual = target.GetFirmChargeLevelAmount(firmID);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///     A test for GetFirmChargeLevelAmount
        ///</summary>
        [TestMethod()]
        public void GetFirmChargeLevelAmountNullTest()
        {
            clsFirms target = new clsFirms();
            int firmID = 3000;
            DataTable expected = new DataTable();
            expected = target.GetFirmChargeLevelAmount(firmID);
            Assert.IsNotNull(expected);
        }

        /// <summary>
        ///     A test for GetFirmChargeLevelAmount
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void GetFirmChargeLevelAmountExceptionTest()
        {
            clsFirms target = new clsFirms();
            int firmID = 0;
            DataTable expected = new DataTable();
            expected = target.GetFirmChargeLevelAmount(firmID);
        }

        #endregion

        #region GetCriminalAttorney

        /// <summary>
        ///     A test for GetCriminalAttorney
        ///</summary>
        [TestMethod()]
        public void GetCriminalAttorneyNullTest()
        {
            clsFirms target = new clsFirms();
            DataTable actual;
            actual = target.GetCriminalAttorney();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///     A test for GetCriminalAttorney
        ///</summary>
        [TestMethod()]
        public void GetCriminalAttorneyTest()
        {
            clsFirms target = new clsFirms();
            DataTable actual = null;
            DataTable expected = new DataTable();
            actual = target.GetCriminalAttorney();
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }

        #endregion

        #region UpdateChargeAmount

        /// <summary>
        ///A test for UpdateChargeAmount
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void UpdateChargeAmountExceptionTest()
        {
            clsFirms target = new clsFirms();
            int FirmID = 0;
            double amount = 0;
            int chargelevelid = 0;
            target.UpdateChargeAmount(FirmID, amount, chargelevelid);
        }

        /// <summary>
        ///A test for UpdateChargeAmount
        ///</summary>
        [TestMethod()]
        public void UpdateChargeAmountTest()
        {
            clsFirms target = new clsFirms();
            int FirmID = 3000;
            double amount = 0;
            int chargelevelid = 1;
            target.UpdateChargeAmount(FirmID, amount, chargelevelid);
            Assert.Inconclusive();
        }

        #endregion

        #region GetAttorneyPayoutReport

        /// <summary>
        ///A test for GetAttorneyPayoutReport
        ///</summary>
        [TestMethod()]
        public void GetAttorneyPayoutReportTypeTest()
        {
            clsFirms target = new clsFirms();
            DataTable expected = new DataTable();
            DataTable actual;

            DateTime startdate = Convert.ToDateTime("01/01/2009");
            DateTime enddate = DateTime.Now;
            int attorney = 3000;
            string URL = string.Empty;
            actual = target.GetAttorneyPayoutReport(startdate, enddate, attorney, URL);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        /// <summary>
        ///A test for GetAttorneyPayoutReport
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void GetAttorneyPayoutReportNullTest()
        {
            clsFirms target = new clsFirms();
            DataTable expected = new DataTable();
            DataTable actual;

            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            //DateTime startdate = null;
            //DateTime enddate = null;
            int attorney = 0;
            string URL = string.Empty;
            target.GetAttorneyPayoutReport(startdate, enddate, attorney, URL);            
        }

        /// <summary>
        ///A test for GetAttorneyPayoutReport
        ///</summary>
        [TestMethod()]
        public void GetAttorneyPayoutReportNullDataTableTest()
        {
            clsFirms target = new clsFirms();
            DataTable actual = null;
            DateTime startdate = Convert.ToDateTime("01/01/2009");
            DateTime enddate = DateTime.Now;
            int attorney = 3000;
            string URL = string.Empty;
            actual = target.GetAttorneyPayoutReport(startdate, enddate, attorney, URL);
            Assert.IsNotNull(actual);
        }

        #endregion
    }
}

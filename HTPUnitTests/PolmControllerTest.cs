﻿using System;
using System.Collections.Generic;
using HTP.ClientController;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HTPUnitTests
{
    /// <summary>
    ///This is a test class for PolmControllerTest and is intended
    ///to contain all PolmControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PolmControllerTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region BodilyDamage

        private string _addedBdText, _addedBdText2;
        private int _addedBdId, _addedBdId2;

        #region AddBodilyDamage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageNullValueTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.AddBodilyDamage(null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageEmptyValueTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.AddBodilyDamage(string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddBodilyDamageValidValueTest()
        {
            _addedBdText = TestHelper.RandomString(5, false);
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.AddBodilyDamage(_addedBdText, true);
            if (actual)
                _addedBdId = target.GetBodilyDamageByName(_addedBdText).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void AddBodilyDamageValidValueFalseTest()
        {
            _addedBdText2 = TestHelper.RandomString(5, false);
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.AddBodilyDamage(_addedBdText2, false);
            if (actual)
                _addedBdId2 = target.GetBodilyDamageByName(_addedBdText2).Id;
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AddBodilyDamageMaxLangthTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.AddBodilyDamage(TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        #endregion

        #region UpdateBodilyDamage


        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageNullValueTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(1, null, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageEmptyValueTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(1, string.Empty, true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageMaxLengthTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(1, TestHelper.RandomString(65, false), true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageLessThanZeroTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(-1, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageZeroTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(0, "asdwe", true);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageGreaterThanZeroActiveTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(_addedBdId, "asdwe", true);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateBodilyDamageGreaterThanZeroInActiveTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.UpdateBodilyDamage(_addedBdId2, "asdwe", true);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region DeleteBodilyDamage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteBodilyDamageLessThanZeroTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            target.DeleteBodilyDamage(-1);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void DeleteBodilyDamageZeroTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            target.DeleteBodilyDamage(0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteBodilyDamageGreaterThanZeroTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.DeleteBodilyDamage(_addedBdId);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void DeleteBodilyDamageGreaterThanZeroTest2()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            bool actual = target.DeleteBodilyDamage(_addedBdId2);
            Assert.AreEqual(true, actual);
        }

        #endregion

        #region GetAllBodilyDamage

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageActiveTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            List<POLM.DataTransferObjects.PolmDto> list = target.GetAllBodilyDamage(true);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageInActiveTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            List<POLM.DataTransferObjects.PolmDto> list = target.GetAllBodilyDamage(false);
            Assert.IsTrue(list.Count >= 0);
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetAllBodilyDamageNullActiveTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            List<POLM.DataTransferObjects.PolmDto> list = target.GetAllBodilyDamage(null);
            Assert.IsTrue(list.Count >= 0);
        }

        #endregion

        #region GetBodilyDamageByName

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameNullTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName(null);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameEmptyTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName(string.Empty);
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameNumberTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName("123456");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameCharactersTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName("!@#$%^&");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameInvalidBdTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName("T for test");
            Assert.AreEqual(true, (polmDto == null));
        }

        /// <summary>
        ///A test for AddBodilyDamage
        ///</summary>
        [TestMethod]
        public void GetBodilyDamageByNameValidBdTest()
        {
            PolmController_Accessor target = new PolmController_Accessor();
            POLM.DataTransferObjects.PolmDto polmDto = target.GetBodilyDamageByName(_addedBdText);
            Assert.AreEqual(false, (polmDto == null));
        }


        #endregion



        #endregion
    }
}

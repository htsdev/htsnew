﻿using HTP.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace HTPUnitTests
{


    /// <summary>
    ///This is a test class for clsContactTest and is intended
    ///to contain all clsContactTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsContactTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        

        /// <summary>
        ///A test for GetContactLookUp
        ///</summary>
        [TestMethod()]
        public void GetContactLookUpTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            string LastName = "ALLEN"; // TODO: Initialize to an appropriate value
            string FirstInitial = "LISA"; // TODO: Initialize to an appropriate value
            DateTime DOB = new DateTime(); // TODO: Initialize to an appropriate value            
            DOB = Convert.ToDateTime("05/30/1964");                
            DataTable actual;
            actual = target.GetContactLookUp(LastName, FirstInitial, DOB);
            Assert.AreEqual(8, actual.Columns.Count);
        }

        /// <summary>
        ///A test for GetContactLookUp
        ///</summary>
        [TestMethod()]
        public void GetContactLookUpTest2()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            string LastName = "Test1234"; // TODO: Initialize to an appropriate value
            string FirstInitial = "4"; // TODO: Initialize to an appropriate value
            DateTime DOB = new DateTime(); // TODO: Initialize to an appropriate value            
            DOB = Convert.ToDateTime("05/30/1964");
            DataTable actual;
            actual = target.GetContactLookUp(LastName, FirstInitial, DOB);
            Assert.AreEqual(8, actual.Columns.Count);
        }


        /// <summary>
        ///A test for AssociateContactID
        ///</summary>
        [TestMethod()]        
        public void AssociateContactIDTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int ContactID = 5896664; // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value
            int EmpID = 0; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.AssociateContactID(ContactID, TicketID, EmpID);
            Assert.AreEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for AssociateContactID
        ///</summary>
        [TestMethod()]  
        public void AssociateContactIDBondaryAnalysisTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int ContactID = 78979; // TODO: Initialize to an appropriate value
            int TicketID = -5; // TODO: Initialize to an appropriate value
            int EmpID = -1; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.AssociateContactID(ContactID, TicketID, EmpID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for GetContactInfo
        ///</summary>
        [TestMethod()]        
        public void GetContactInfoTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int ContactID = 4563; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.GetContactInfo(ContactID);
            Assert.AreEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for GetContactInfo
        ///</summary>
        [TestMethod()]
        public void GetContactInfoTestForNegative()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int ContactID = -534543; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.GetContactInfo(ContactID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for UnlinkContactFromNonClient
        ///</summary>
        [TestMethod()]        
        public void UnlinkContactFromNonClientTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int RecordID = 45632; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UnlinkContactFromNonClient(RecordID,1);
            Assert.AreEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for UnlinkContactFromNonClient
        ///</summary>
        [TestMethod()]
        public void UnlinkContactFromNonClientBoundryAnalysisTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int RecordID = 456632; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UnlinkContactFromNonClient(RecordID,4);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for AssociatedMatters
        ///</summary>
        [TestMethod()]        
        public void AssociatedMattersTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            string TicketID = "-5984"; // TODO: Initialize to an appropriate value            
            DataSet actual;
            actual = target.AssociatedMatters(TicketID);
            Assert.AreEqual(5, actual.Tables.Count);
            
        }

        /// <summary>
        ///A test for AssociatedMatters
        ///</summary>
        [TestMethod()]        
        public void AssociatedMattersGetTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            string TicketID = "46544569"; // TODO: Initialize to an appropriate value            
            DataSet actual;
            actual = target.AssociatedMatters(TicketID);
            Assert.AreEqual(5, actual.Tables.Count);

        }

        /// <summary>
        ///A test for UpdateCIDConfirmationFlag
        ///</summary>
        [TestMethod()]        
        public void UpdateCIDConfirmationFlagTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value
            int IsContactIDConfirmed = 0; // TODO: Initialize to an appropriate value
            int EmpID = 0; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UpdateCIDConfirmationFlag(TicketID, IsContactIDConfirmed, EmpID);
            Assert.AreEqual(expected, actual);
           
        }

        /// <summary>
        ///A test for UpdateCIDConfirmationFlag
        ///</summary>
        [TestMethod()]
        public void UpdateCIDConfirmationFlagProperlyTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int TicketID = 41764; // TODO: Initialize to an appropriate value
            int IsContactIDConfirmed = -5; // TODO: Initialize to an appropriate value
            int EmpID = 0; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.UpdateCIDConfirmationFlag(TicketID, IsContactIDConfirmed, EmpID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for DisassociateContactID
        ///</summary>
        [TestMethod()]       
        public void DisassociateContactIDTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int TicketID = 0; // TODO: Initialize to an appropriate value
            int EmpID = 0; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.DisassociateContactID(TicketID, EmpID);
            Assert.AreEqual(expected, actual);
            
        }
        /// <summary>
        ///A test for DisassociateContactID
        ///</summary>
        [TestMethod()]       
        public void DisassociateContactIDProperlyTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int TicketID = -41; // TODO: Initialize to an appropriate value
            int EmpID = 0; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.DisassociateContactID(TicketID, EmpID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for CreateNewContact
        ///</summary>
        [TestMethod()]        
        public void CreateNewContactTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            string Lastname = "Test1"; // TODO: Initialize to an appropriate value
            string Firstname = "Test2"; // TODO: Initialize to an appropriate value
            string DOB = "02/04/1985"; // TODO: Initialize to an appropriate value
            int EmpID = 3012; // TODO: Initialize to an appropriate value
            int TicketID = 46562; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.CreateNewContact(Lastname, Firstname, DOB, EmpID, TicketID);
            Assert.AreEqual(expected, actual>0);
            
        }
        /// <summary>
        ///A test for CreateNewContact
        ///</summary>
        [TestMethod()]
        public void CreateNewContactForZeroTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            string Lastname = "Test4"; // TODO: Initialize to an appropriate value
            string Firstname = "Test5"; // TODO: Initialize to an appropriate value
            string DOB = "01/06/1982"; // TODO: Initialize to an appropriate value
            int EmpID = 3012; // TODO: Initialize to an appropriate value
            int TicketID = 46562; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.CreateNewContact(Lastname, Firstname, DOB, EmpID, TicketID);
            Assert.AreEqual(expected, actual == 0);

        }

        /// <summary>
        ///A test for GetTicketForCID
        ///</summary>
        [TestMethod()]        
        public void GetTicketForCIDTestForNull()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int ContactID = 6532; // TODO: Initialize to an appropriate value
            string expected = null; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetTicketForCID(ContactID);
            Assert.AreNotEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for GetTicketForCID
        ///</summary>
        [TestMethod()]
        public void GetTicketForCIDTest()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int ContactID = -6532; // TODO: Initialize to an appropriate value
            string expected = "0"; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetTicketForCID(ContactID);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for GetContactIDByTicketID
        ///</summary>
        [TestMethod()]        
        public void GetContactIDByTicketIDTestForNegative()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int TicketID = 4563; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetContactIDByTicketID(TicketID);
            Assert.AreEqual(expected, actual>-1);
            
        }

        /// <summary>
        ///A test for GetContactIDByTicketID
        ///</summary>
        [TestMethod()]
        public void GetContactIDByTicketIDTestForZero()
        {
            clsContact target = new clsContact(); // TODO: Initialize to an appropriate value
            int TicketID = -83; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetContactIDByTicketID(TicketID);
            Assert.AreEqual(expected, actual);

        }
    }
}

﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using System;

namespace HTPUnitTests
{
    
    
    /// <summary>
    ///This is a test class for clscallsTest and is intended
    ///to contain all clscallsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clscallsTest
    {


        private TestContext testContextInstance;
        clscalls target = new clscalls();

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///This is a Region for Method remindercallgrid() which  returns DataSet.
        ///This Region is intended to contain all its Unit Tests.
        ///</summary>
        ///
        #region remindercallgrid
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void remindercallgrid_SetCall_Null()
        {
            clscalls target = new clscalls();
            DataSet actual;
            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"),0,true,1,-1,1);
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void remindercallgrid_SetCall_Type()
        {
            clscalls target = new clscalls();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 1, -1, 1);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void remindercallgrid_SetCall_Rows()
        {
            clscalls target = new clscalls();
            bool expected = false, actual = false;
            int expnumber = 0;            
            expected = (expnumber >= 0) ? true : false;
            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 1, -1, 1).Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 24  . 
        /// </summary>
        [TestMethod()]
        public void remindercallgrid_SetCall_NoOfCol()
        {
            clscalls target = new clscalls();

            int expected = 0, actual = 0;
            expected = 24;
            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 1, -1, 1).Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);

        }      
       
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void remindercallgrid_ReminderCall_Null()
        {
            clscalls target = new clscalls();
            DataSet actual;
            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 0, -1, 1);
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void remindercallgrid_ReminderCall_Type()
        {
            clscalls target = new clscalls();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 0, -1, 1);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void remindercallgrid_ReminderCall_Rows()
        {
            clscalls target = new clscalls();
            bool expected = false, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 0, -1, 1).Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 24  . 
        /// </summary>
        [TestMethod()]
        public void remindercallgrid_ReminderCall_NoOfCol()
        {
            clscalls target = new clscalls();

            int expected = 0, actual = 0;
            expected = 24;
            actual = target.remindercallgrid(Convert.ToDateTime("02/06/2009"), 0, true, 0, -1, 1).Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);

        }
       
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void remindercallgrid_MissedCourtDate_Null()
        {
            clscalls target = new clscalls();
            DataSet actual;
            actual = target.remindercallgrid(Convert.ToDateTime("03/05/2009"), 0, true, 2, -1, 0);
            Assert.IsNotNull(actual);


        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void remindercallgrid_MissedCourtDate_Type()
        {
            clscalls target = new clscalls();

            DataSet ds = new DataSet();
            DataSet expected = ds;
            DataSet actual;

            actual = target.remindercallgrid(Convert.ToDateTime("03/05/2009"), 0, true, 2, -1, 0);
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void remindercallgrid_MissedCourtDate_Rows()
        {
            clscalls target = new clscalls();
            bool expected = false, actual = false;
            int expnumber = 0;
            expected = (expnumber >= 0) ? true : false;
            actual = target.remindercallgrid(Convert.ToDateTime("03/05/2009"), 0, true, 2, -1, 0).Tables[0].Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 24  . 
        /// </summary>
        [TestMethod()]
        public void remindercallgrid_MissedCourtDate_NoOfCol()
        {
            clscalls target = new clscalls();

            int expected = 0, actual = 0;
            expected = 24;
            actual = target.remindercallgrid(Convert.ToDateTime("03/05/2009"), 0, true, 2, -1, 0).Tables[0].Columns.Count;
            Assert.AreEqual(expected, actual);

        }

        #endregion

        #region getsetcallreport
        /// <summary>
        ///This Test method will Check if the Dataset is Null or Not . ()
        ///</summary>
        [TestMethod()]
        public void getsetcallreport_Null()
        {
            clscalls target = new clscalls();
            DataTable actual;
            actual = target.getsetcallreport();
            Assert.IsNotNull(actual);
        }
        /// <summary>
        ///This Test method will Check Type of the Dataset.
        ///</summary>
        [TestMethod()]
        public void getsetcallreport_Type()
        {
            clscalls target = new clscalls();
            DataTable ds = new DataTable();
            DataTable expected = ds;
            DataTable actual;
            actual = target.getsetcallreport();
            Assert.AreEqual(expected.GetType(), actual.GetType());
        }
        /// <summary>
        /// This Test method will Check No of Rows Return by dataset is equal or greater than Zero . 
        /// </summary>
        /// 
        [TestMethod()]
        public void getsetcallreport_Rows()
        {
            clscalls target = new clscalls();
            bool expected = false, actual = false;
            int expnumber = 0;            
            expected = (expnumber >= 0) ? true : false;
            actual = target.getsetcallreport().Rows.Count >= 0 ? true : false;
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This Test method will Check that the No of columns are equal to 16  . 
        /// </summary>
        [TestMethod()]
        public void getsetcallreport_NoOfCol()
        {
            clscalls target = new clscalls();
            int expected = 0, actual = 0;            
            expected = 16;
            actual = target.getsetcallreport().Columns.Count;
            Assert.AreEqual(expected, actual);
        }     
        #endregion

       

        /// Fahd 5533 02/25/2009
        ///A test for GetReminderStatus
        ///</summary>
        [TestMethod()]
        
        public void GetReminderStatusTest()
        {
            
            DataTable expected=null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetReminderStatus();
            Assert.AreNotEqual(expected, actual);
            
        }

        /// Fahd 5533 02/25/2009
        ///A test for GetLegalConsultationData
        ///</summary>
        [TestMethod()]
       
        public void GetLegalConsultationDataTest()
        {
            
            DateTime sdate = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime edate = new DateTime(); // TODO: Initialize to an appropriate value
            int status = 0; // TODO: Initialize to an appropriate value
            sdate=Convert.ToDateTime("2/25/2004");
            edate = Convert.ToDateTime("2/25/2009");
            bool showall = true; // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetLegalConsultationData(sdate, edate, status, showall);
            Assert.AreNotEqual(expected, actual);
            
        }



        /// Fahd 5533 02/25/2009
        ///A test for UpdateLegalConsultationComments
        ///</summary>
        [TestMethod()]
        
        public void UpdateLegalConsultationCommentsTest1()
        {
            int TicketID = 0; // TODO: Initialize to an appropriate value
            int empid = 0; // TODO: Initialize to an appropriate value
            string ConsultaionComments = string.Empty; // TODO: Initialize to an appropriate value
            int ConsultationStatusID = 0; // TODO: Initialize to an appropriate value
            target.UpdateLegalConsultationComments(TicketID, empid, ConsultaionComments, ConsultationStatusID);
            Assert.IsNotNull(target);
            
        }

        /// Fahad 5503 03/03/2009 
        ///A test for remindercallgrid
        ///</summary>
        [TestMethod()]
        public void remindercallgridTestForNull()
        {
            clscalls target = new clscalls(); // TODO: Initialize to an appropriate value
            DateTime date = new DateTime(); // TODO: Initialize to an appropriate value
            int status = -5; // TODO: Initialize to an appropriate value
            bool check = true; // TODO: Initialize to an appropriate value
            int reporttype = 9; // TODO: Initialize to an appropriate value
            int lang = 6; // TODO: Initialize to an appropriate value
            int Mode = 3; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.remindercallgrid(date, status, check, reporttype, lang, Mode);
            Assert.AreNotEqual(expected, actual);
           
        }

        /// Fahad 5503 03/03/2009 
        ///A test for remindercallgrid
        ///</summary>
        [TestMethod()]
        public void remindercallgridTest()
        {
            clscalls target = new clscalls(); // TODO: Initialize to an appropriate value
            DateTime date = new DateTime(); // TODO: Initialize to an appropriate value
            int status = 5; // TODO: Initialize to an appropriate value
            bool check = true; // TODO: Initialize to an appropriate value
            int reporttype = 1; // TODO: Initialize to an appropriate value
            int lang = 1; // TODO: Initialize to an appropriate value
            int Mode = 3; // TODO: Initialize to an appropriate value
            DataSet expected = null; // TODO: Initialize to an appropriate value
            DataSet actual;
            actual = target.remindercallgrid(date, status, check, reporttype, lang, Mode);
            Assert.AreNotEqual(expected, actual);

        }
    }
}

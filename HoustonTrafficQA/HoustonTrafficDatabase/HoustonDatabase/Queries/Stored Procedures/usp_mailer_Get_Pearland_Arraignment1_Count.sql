﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Get_Pearland_Arraignment1_Count]    Script Date: 08/02/2013 10:34:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by:		Rab Nawaz Khan
Task ID	: 11108
Date :	08/02/2013
Business Logic:	The procedure is used by LMS application to get the number of prINTable letter
				count for the Pearland Municipal Court Appearance letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, IF prINTing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category IF prINTing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies IF searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

-- usp_mailer_Get_Pearland_Arraignment1_Count 38, 143,38, '08/02/2013', '08/02/2013', 0
ALTER PROCEDURE [dbo].[usp_mailer_Get_Pearland_Arraignment1_Count]
	(
	@catnum		INT,
	@LetterType 	INT,
	@crtid 		INT,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     INT                                             
	)

AS                                                              
                          
-- DECLARING LOCAL VARIABLES FOR FILTERS.....                                  
DECLARE	@officernum VARCHAR(50), 
	@officeropr VARCHAR(50), 
	@tikcetnumberopr VARCHAR(50), 
	@ticket_no VARCHAR(50), 
	@zipcode VARCHAR(50), 
	@zipcodeopr VARCHAR(50), 
	@fineamount MONEY,
	@fineamountOpr VARCHAR(50) ,
	@fineamountRelop VARCHAR(50),
	@singleviolation MONEY, 
	@singlevoilationOpr VARCHAR(50) , 
	@singleviolationrelop VARCHAR(50), 
	@doubleviolation MONEY,
	@doublevoilationOpr VARCHAR(50) , 
	@doubleviolationrelop VARCHAR(50), 
	@count_Letters INT,
	@violadesc VARCHAR(500),
	@violaop VARCHAR(50)

-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL.
DECLARE @sqlquery NVARCHAR(MAX), @sqlquery2 NVARCHAR(MAX), @sqlParam NVARCHAR(1000)
SELECT 	@sqlquery ='',@sqlquery2 = ''

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
SELECT @sqlparam = '@catnum INT, @LetterType INT, @crtid INT, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType INT'
                                                      
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                                              
SELECT	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
FROM 	tblmailer_letters_to_sendfilters  
WHERE 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                                                      
-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY 
-- INSERTING VALUES IN A TEMP TABLE....
SET 	@sqlquery=@sqlquery+'                                          
SELECT distinct  
CONVERT(datetime , CONVERT(VARCHAR(10),ta.listdate,101)) AS mdate,   
flag1 = isnull(ta.Flag1,''N'') ,                                                      
ta.donotmailflag, ta.recordid,                                                    
count(tva.ViolationNumber_PK) AS ViolCount,                                                      
sum(tva.fineamount)AS Total_Fineamount
INTo #temp                                                            
FROM dbo.tblTicketsViolationsArchive TVA INNER JOIN dbo.tblTicketsArchive TA ON TVA.recordid = TA.recordid  
INNER JOIN tblstate S ON ta.stateid_fk = S.stateID INNER JOIN dbo.tblCourtViolationStatus tcvs ON tva.violationstatusid = tcvs.CourtViolationStatusID

-- ONLY NON-CLIENTS....
WHERE ta.clientflag=0 

-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
and isnull(ta.ncoa48flag,0) = 0

-- FOR THE SELECTED LIST DATE RANGE ONLY...
and datediff(day,  ta.listdate , @startListdate)<=0
and datediff(day,  ta.listdate , @endlistdate)>=0 

--Only Appearance status
and tcvs.CategoryID=2 

-- only future court date
and datediff(day, tva.courtdate, getdate()) < 0

-- First Name and Last Name should not be empty . . 
AND LEN(ISNULL(ta.LastName, '''')) > 0 AND LEN(ISNULL(ta.FirstName, '''')) > 0
	 
-- Attorney should not be assigned. . . 
AND LEN(ISNULL(tva.AttorneyName, '''')) = 0

' 
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
IF(@crtid = @catnum ) 
	SET @sqlquery=@sqlquery+' 
	and tva.CourtLocation In (SELECT courtid FROM tblcourts WHERE courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE 
	SET @sqlquery =@sqlquery +' 
	and tva.CourtLocation = @crtid' 
                             
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                           
IF(@officernum<>'')                                                        
	SET @sqlquery =@sqlquery + '  
	and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
IF(@ticket_no<>'')                        
	SET @sqlquery=@sqlquery + ' 
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              

-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                    
IF(@zipcode<>'')                                                                              
	SET @sqlquery=@sqlquery+ ' 
	and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION...
IF(@violadesc<>'')            
	SET @sqlquery=@sqlquery+ ' 
	and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
IF(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	SET @sqlquery =@sqlquery+ '
        and  tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
IF(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	SET @sqlquery =@sqlquery+ '
        and not tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                                       
                                                      
-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
--Sabir Khan 7601 03/22/2010 Exclude clients whose DLQ letter is sent in past 5 business days.
SET @sqlquery =@sqlquery+ ' 

group by                                                       
CONVERT(datetime , CONVERT(VARCHAR(10),ta.listdate,101)) ,
ta.Flag1,      
ta.donotmailflag ,ta.recordid    
-- no letter of other mailer type will be send in the next 5 working days FROM th previous sent date. 
 Delete FROM #temp
 FROM #temp a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
 INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
 WHERE 	lettertype =@LetterType 
 or (
		l.courtcategory = 38
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
	)

                                                  

-- GETTING THE RESULT SET IN TEMP TABLE.....
SELECT distinct  ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag, recordid INTo #temp1  FROM #temp  WHERE 1=1                                                        
'                                  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
IF(@singleviolation<>0 and @doubleviolation=0)                                  
	SET @sqlquery =@sqlquery+ '
	and '+ @singlevoilationOpr+ '  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') and ViolCount=1) or violcount>3
    '                                  
                                  
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                       
IF(@doubleviolation<>0 and @singleviolation=0 )                                  
	SET @sqlquery =@sqlquery + '
	and'+ @doublevoilationOpr+ '   (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')and ViolCount=2)  or violcount>3 
	'

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
IF(@doubleviolation<>0 and @singleviolation<>0)                                  
	SET @sqlquery =@sqlquery+ '
	and'+ @singlevoilationOpr+ '  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') and ViolCount=1)
	or' + @doublevoilationOpr+'   (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')and ViolCount=2) or violcount>3 
	'

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
SET @sqlquery2=@sqlquery2 +'
SELECT count(distinct recordid) AS Total_Count, mdate 
INTo #temp2 
FROM #temp1 
group by mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
SELECT count(distinct recordid) AS Good_Count, mdate 
INTo #temp3
FROM #temp1 
WHERE Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS 
SELECT count(distinct recordid) AS Bad_Count, mdate  
INTo #temp4  
FROM #temp1 
WHERE Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
SELECT count(distinct recordid) AS DonotMail_Count, mdate  
INTo #temp5   
FROM #temp1 
WHERE donotmailflag=1 
group by mdate
	

-- OUTPUTTING THE REQURIED INFORMATION........         
SELECT   #temp2.mdate AS listdate,
        Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
FROM #Temp2 left outer join #temp3   
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
WHERE Good_Count > 0 order by #temp2.mdate desc

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5 
'                                                      
                                                      
PRINT @sqlquery  + @sqlquery2                                       

-- CONCATENATING THE QUERY ....
SET @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL QUERY...
EXEC sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType

GO
GRANT EXECUTE ON [dbo].[usp_mailer_Get_Pearland_Arraignment1_Count] TO dbr_webuser
GO


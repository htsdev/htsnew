/****** 
Created by:		Tahir Ahmed
Business Logic:	The stored procedure is used by LMS/Reports/Misc Reports/Printing Grid report.
				It is used to get the Court category/Court Name  and letter types printed in 
				past 2 months for the provided date.
				
List of Parameters:
	@SelectedDate:	Mailer printing date.
	@UserLocation:	To display only Houston (1), Dallas (2), or both (0) the locations.

List of Columns:	
	categoryid:		ID of the court category...
	courtid:		ID of the individual court house.
	courtname:		Court name ...
	lettername:		Name of the mailer type..
	lettertype:		ID of the mailer type..

******/

-- USP_Mailer_Get_PrintingGridCourts '8/25/08',1
ALTER procedure [dbo].[USP_Mailer_Get_PrintingGridCourts]
	(
	@selectedDate datetime ,
	
	-- TAHIR 3634 06/14/2008
	-- FOR DALLAS USER....
	@UserLocation int = 0
	)

as


declare @courts table (categoryid int, categoryname varchar(200), courtid int, courtname varchar(200) )
declare @letters table (categoryid int, courtid int, lettertype int,  lettername varchar(200))
declare @temp table (lettertype int, courtid int, categoryid int)

if @Userlocation in (0, 2)
	begin
		insert into @temp 
		select distinct n.lettertype, n.courtid, b.courtid
		from tblletternotes n inner join tblbatchletter b
		on n.batchid_fk = b.batchid
		--Yasir Kamal 7218 01/13/2010 lms structure changed. 
		INNER JOIN tblCourtCategories tcc ON tcc.CourtCategorynum = b.CourtID
		and datediff(day,n.recordloaddate, @selecteddate) between 0 and 60
		--and b.courtid in (6,11,16,25,27,28) --Sabir Khan 6888 10/29/2009 Added category id of fortworth and arlington....
		AND tcc.LocationId_FK = 2
	end

if @Userlocation in (0, 1)
	begin
		insert into @temp 
		select distinct n.lettertype, n.courtid, b.courtid
		from tblletternotes n inner join tblbatchletter b
		on n.batchid_fk = b.batchid
		--Yasir Kamal 7218 01/13/2010 lms structure changed. 
		INNER JOIN tblCourtCategories tcc ON tcc.CourtCategorynum = b.CourtID
		and datediff(day,n.recordloaddate, @selecteddate) between 0 and 60
		--and b.courtid not in (6,11,16,25,27,28) --Sabir Khan 6888 10/29/2009 Added category id of fortworth and arlington....
		AND tcc.LocationId_FK <> 2
	end	



if @userlocation in (0, 1)
	begin

		--HMC COURTS....
		insert into @courts 
		select	distinct	
				m.courtcategorynum, 
				m.courtcategoryname, 
				m.courtcategorynum, 
				m.courtcategoryname 
		from tblcourts c inner join tblcourtcategories m
		on c.courtcategorynum  = m.courtcategorynum
		--where c.courtid in (3001,3002,3003)
		where c.courtcategorynum <> 2

		-- HARRIS COUNTY JP COURTS
		insert into @courts 
		select	distinct	
				m.courtcategorynum, 
				m.courtcategoryname, 
				c.courtid, 
				c.shortname + ' ('+c.address+')' 
		from tblcourts c inner join tblcourtcategories m
		on c.courtcategorynum  = m.courtcategorynum
		where m.courtcategorynum = 2

	end
	
if @userlocation in (0,2)	
	begin
	
		-- DALAS MUNICIPAL....
		insert into @courts 
		select	distinct	
				m.courtcategorynum, 
				m.courtcategoryname, 
				m.courtcategorynum, 
				m.courtcategoryname 
		from dallastraffictickets.dbo.tblcourts c inner join tblcourtcategories m
		on c.courtcategorynum  = m.courtcategorynum
		-- tahir 4674 08/26/2008 fixed the DCJP letter count issue...
		--where c.courtid in (  3049, 3110)
		where c.courtcategorynum <> 11 -- (6,16,25)
		-- end 4674
		UNION 
		select	distinct	
				m.courtcategorynum, 
				m.courtcategoryname, 
				c.courtid, 
				c.shortcourtname 
		from dallastraffictickets.dbo.tblcourts c inner join tblcourtcategories m
		on c.courtcategorynum  = m.courtcategorynum
		where m.courtcategorynum = 11

	end

-- GETTING LETTERS....
insert into @letters (categoryid ,  lettertype ,  lettername )
select distinct l.courtcategory, l.letterid_pk, l.lettername
from tblletter l where isactive =1 

select distinct c.categoryid, c.courtid, c.courtname , l.lettername, l.lettertype
--into #tmp
from @courts c inner join @letters l on l.categoryid = c.categoryid
inner join @temp t on t.lettertype = l.lettertype 
and (t.courtid = c.courtid or t.categoryid = c.courtid)

order by categoryid, courtid




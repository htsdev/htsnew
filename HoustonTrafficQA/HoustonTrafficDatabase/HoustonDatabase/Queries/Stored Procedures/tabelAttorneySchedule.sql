USE [TrafficTickets]
GO
/****** Object:  Table [dbo].[AttorneysSchedule]    Script Date: 12/18/2009 12:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AttorneysSchedule](
	[AttorneyScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[DocketDate] [datetime] NOT NULL,
	[RecDate] [datetime] NOT NULL,
	[ContactNumber] [varchar](14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AttorneyID] [int] NULL,
	[UpdatedByEmployeeID] [int] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_AttorneysSchedule_LastUpdatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_AttorneysSchedule] PRIMARY KEY CLUSTERED 
(
	[AttorneyScheduleID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [TrafficTickets]
GO
ALTER TABLE [dbo].[AttorneysSchedule]  WITH CHECK ADD  CONSTRAINT [FK_AttorneysSchedule_tblUsers1] FOREIGN KEY([UpdatedByEmployeeID])
REFERENCES [dbo].[tblUsers] ([EmployeeID])

GO 
GRANT SELECT ON [dbo].[AttorneysSchedule] TO dbr_webuser
GO
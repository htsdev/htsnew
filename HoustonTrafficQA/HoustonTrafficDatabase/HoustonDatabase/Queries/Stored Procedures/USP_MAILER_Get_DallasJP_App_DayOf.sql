

/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Dallas County JP Appearance Day Of letter for the selected date range.

				-Excludes Letters if already Printed, or DCJP Appearance letter printed in past 1 week.  --Yasir Kamal 5593 03/04/2009 
				
List of Parameters:
	@startdate:	Starting list date for Appearance Day Of letter.
	@enddate:	Ending list date for Appearance Day Of letter.
	@CrtId:		Court id of the selected court category if printing for individual court hosue.
	@bondflag:	Flag if need to print bond cases.
	@catnum:	Category number of the selected letter type, if printing for all courts of the selected court category

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--  USP_MAILER_Get_DallasJP_App_DayOf '3/26/09', '3/26/10', 11, 0, 11  
alter procedure [dbo].[USP_MAILER_Get_DallasJP_App_DayOf]  
 (  
 @startdate datetime,  
 @enddate datetime,  
 @courtid int = 6,  
 @bondflag tinyint = 0,  
 @catnum int=6  
 )  
  
as  

-- DECLARING LOCAL VARIABLES FOR FILTERS...
declare @officernum varchar(50),                                                      
 @officeropr varchar(50),                                                      
 @tikcetnumberopr varchar(50),                                                      
 @ticket_no varchar(50),                                                                                                    
 @zipcode varchar(50),                                                               
 @zipcodeopr varchar(50),                                                      
 @fineamount money,                                                              
 @fineamountOpr varchar(50) ,                                                              
 @fineamountRelop varchar(50),                                     
 @singleviolation money,                                                              
 @singlevoilationOpr varchar(50) ,                                                              
 @singleviolationrelop varchar(50),                                                      
 @doubleviolation money,                                                              
 @doublevoilationOpr varchar(50) ,                                                              
 @doubleviolationrelop varchar(50),                                                    
 @count_Letters int,                                    
 @violadesc varchar(500),                                    
 @violaop varchar(50)  

-- DECLARING &  INITIALIZING THE LOCAL VARIABLE FOR DYNAMIC SQL                    
DECLARE @sqlquery varchar(max)
set @sqlquery = ''                                                   
  
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......
Select  @officernum=officernum,                                                      
 @officeropr=oficernumOpr,                                                      
 @tikcetnumberopr=tikcetnumberopr,                                                      
 @ticket_no=ticket_no,                                                      
 @zipcode=zipcode,                                                      
 @zipcodeopr=zipcodeLikeopr,                                              
 @singleviolation =singleviolation,                                                 
 @singlevoilationOpr =singlevoilationOpr,                                                      
 @singleviolationrelop =singleviolationrelop,                                                      
 @doubleviolation = doubleviolation,                                                      
 @doublevoilationOpr=doubleviolationOpr,                                                              
 @doubleviolationrelop=doubleviolationrelop,                                    
 @violadesc=violationdescription,                                    
 @violaop=violationdescriptionOpr ,                                
 @fineamount=fineamount ,                   
 @fineamountOpr=fineamountOpr ,                                                   
 @fineamountRelop=fineamountRelop                   
from  tblmailer_letters_to_sendfilters                                 
where  courtcategorynum=@catnum    
and lettertype =  38                                                  
       
-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY 
-- INSERTING VALUES IN A TEMP TABLE....      
set @sqlquery=@sqlquery+'                                            
Select distinct tva.recordid,     
--convert(datetime, convert(varchar(10),ta.recloaddate,101))as mdate,      
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date.
case when datediff(day, tva.courtdate, getdate()) in (0,1,2) then convert(datetime, convert(varchar(10),tva.courtdate,101)) else convert(datetime, convert(varchar(10),ta.recloaddate,101)) end as mdate  ,
flag1 = isnull(ta.Flag1,''N'') ,                                                        
isnull(ta.donotmailflag,0) as donotmailflag,              
count(tva.ViolationNumber_PK) as ViolCount,                                                        
isnull(sum(isnull(tva.fineamount,0)),0)as Total_Fineamount    
into #temp                                                              
FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA                         
INNER JOIN                         
 dallastraffictickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid    

-- PERSON NAME SHOULD NOT BE EMPTY...
where 1=1 

--TAHIR 4374 07/09/2008 EXLCUDE DIFFICULT JP COURT CASES..
--Yasir Kamal 6520 09/03/2009 include difficult JP court cases.
--and tva.courtlocation not in (3085,3086,3088,3090)


-- tahir 3796 4/18/08
-- ONLY CONST & SHERIFF COMPLAINANT AGENCIES
and 
-- Abbas Shahid Khwaja 9296 05/18/2011 Screen out all cases with Offense Date more than 1 year ago and also having DISD in complainant agency
--(charindex(''CONST'', isnull(tva.complainant_agency,'''')) > 0   or charindex(''SHERIFF'', isnull(tva.complainant_agency,'''')) > 0  )
charindex(''DISD'', isnull(tva.complainant_agency,'''')) = 0 
and [dbo].[fn_CalculateOffenseDate](tva.ticketviolationdate,getdate()) <= 0

-- Haris Ahmed 10412 09/05/2012 Screen out all cases having DCHD in complainant agency
and charindex(''DCHD'', isnull(tva.complainant_agency,'''')) = 0 

-- EXCLUDE FAILURE TO PAY TOLL TICKETS..
and charindex(''Failure to Pay toll'', isnull(tva.violationdescription,'''')) = 0

-- ONLY TODAY OR  PAST COURT COURT DATES....
and datediff(day,tva.courtdate,getdate())>= 0

-- FOR THE SELECTED DATE RANGE ONLY...
-- TAHIR 5593 03/24/2009  also print for todays court date.
and (
	(datediff(day, tva.courtdate, getdate()) in (0,1,2) )
	OR
	-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date.
	(datediff(day, ta.recloaddate, ''' + convert(varchar,@startDate,101)  + ''') <= 0  and datediff(day, ta.recloaddate, ''' + convert(varchar,@endDate , 101) + ''') >= 0   )
	)

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0 

-- Rab Nawaz Khan 8754 01/21/2011 Exclude Disposed Violations
and tva.violationstatusid <> 80
-- End 8754
 
'              

-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...    
if(@courtid=@catnum)  
	set @sqlquery=@sqlquery+'   
	and tva.courtlocation In (select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = '+ convert(varchar,@catnum) +')'            

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else  
	set @sqlquery=@sqlquery+'   
	and tva.courtlocation=' + convert(varchar,@courtid)  
  
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                      
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '    
	and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'                                                         
                                  
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                                       
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + '   
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ '   
	and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'  
                                 
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....  
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '  
    and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....   
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '  
    and  not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
  
set @sqlquery =@sqlquery + '   
   
 group by 
 -- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date.                                                      
 case when datediff(day, tva.courtdate, getdate()) in (0,1,2) then convert(datetime, convert(varchar(10),tva.courtdate,101)) else convert(datetime, convert(varchar(10),ta.recloaddate,101)) end,  
 ta.flag1,                                                      
 ta.donotmailflag,                                                      
 tva.recordid  
having 1=1   
'  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT  
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '  
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
      '                                  

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + '  
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )  
      '                                                      
    
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                              
if(@doubleviolation<>0 and @singleviolation<>0)                                  
	set @sqlquery =@sqlquery+ '  
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))  
        '                                    

set @sqlquery =@sqlquery+ ' 
delete from #temp
from #temp a inner join dallastraffictickets.dbo.tblticketsarchive ta 
on ta.recordid = a.recordid
where len(isnull(ta.firstname,'''') +isnull(ta.lastname,'''')  ) = 0 
or charindex(''law office'', isnull(ta.lastname,'''')+isnull(ta.firstname,'''') ) > 0  
or charindex(''law firm'', isnull(ta.lastname,'''')+isnull(ta.firstname,'''') ) > 0  
or charindex(''attorney'', isnull(ta.lastname,'''')+isnull(ta.firstname,'''') ) > 0  
'
  
-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED....
-- APPEARANCE OR DAY OF LETTER...
set @sqlquery =@sqlquery+ '  
-- Yasir Kamal 5593 03/04/2009 DCJP Appearance letter sent in past 1 week
create table #tempvar (recordid int) 
--select distinct recordid into #tempvar from tblletternotes
insert into #tempvar select recordid  from tblletternotes where lettertype = 38  
insert into #tempvar select recordid  from tblletternotes where lettertype = 39 and datediff(day, recordloaddate, getdate()) <= 7 
--5593 end    

-- TAHIR 4374 07/09/2008
-- where  lettertype  IN (38,39)    
  
select T.recordid into #tempvarexist from #temp T , #tempvar V  
where T.recordid = V.recordid  
  
delete from #temp where recordid in (select recordid from #tempvarexist)  


'                                  

-- GETTING THE RESULT SET IN TEMP TABLE.....    
set @sqlquery=@sqlquery+                                         
'Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag   
into #temp1  from #temp a, dallastraffictickets.dbo.tblticketsviolationsarchive tva where a.recordid = tva.recordid   

-- Abbas Shahid Khwaja 9296 05/18/2011 Screen out all cases with Offense Date more than 1 year ago and also having DISD in complainant agency
and charindex(''DISD'', isnull(tva.complainant_agency,'''')) = 0 
and [dbo].[fn_CalculateOffenseDate](tva.ticketviolationdate,getdate()) <= 0 
-- Haris Ahmed 10412 09/05/2012 Screen out all cases having DCHD in complainant agency
and charindex(''DCHD'', isnull(tva.complainant_agency,'''')) = 0 
 '                                  
  
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')  
begin  
	-- NOT LIKE FILTER.......  
	if(charindex('not',@violaop)<> 0 )                
		set @sqlquery=@sqlquery+'   
		and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
	  
	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )                
		set @sqlquery=@sqlquery+'   
		and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
end  


--Sabir Khan 6784 10/16/2009 
--EXCULDE CASES HAVING ANY OF THE FOLLOWING STATUS....
set @sqlquery=@sqlquery+                                         
'delete from #temp1 
from #temp1 a inner join dallastraffictickets.dbo.tblticketsviolationsarchive tva on tva.recordid = a.recordid
inner join dallastraffictickets.dbo.WebSiteCourtStatus w on w.id  = tva.WebSiteCourtStatus
-- Muneer Sheikh 8302 09/06/2010 "DISPOSED CASE", "FULL OR PARTIAL PREPAYMENT", "UNDER 17 YEARS OF AGE", "ZERO FINE AMOUNT" Excluded from mailing.
 where w.MailAllowed = 1 and w.WebSite=''DCJP''
--Sabir Khan 6947 11/06/2009 Removed warrant check....
--or charindex(''Warrant'', w.CourtStatus) > 0
' 
 
--Sabir Khan 6898 10/30/2009 EXCLUDE CLIENT CASES...
set @sqlquery=@sqlquery +'  
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)'
  
  
-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....  
set @sqlquery=@sqlquery+'   
  
Select count(distinct recordid) as Total_Count, mDate                                                    
into #temp2                                                  
from #temp1                                                  
group by mDate                                                       
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mDate                                                                                                
into #temp3                                                  
from #temp1                                                      
where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag =  0                                                   
group by mDate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mDate                                                    
into #temp4                                                      
from #temp1                                                  
where Flag1  not in (''Y'',''D'',''S'')                                                      
and donotmailflag = 0                                                   
group by mDate   
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mDate                                                   
 into #temp5                                                      
from #temp1          
where donotmailflag = 1    
group by mDate   
         
-- OUTPUTTING THE REQURIED INFORMATION........                                                      
Select   listdate = #temp2.mDate ,  
        Total_Count,  
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #Temp2 left outer join #temp3   
on #temp2.mDate = #temp3.mDate  
                                                     
left outer join #Temp4  
on #temp2.mDate = #Temp4.mDate  
    
left outer join #Temp5  
on #temp2.mDate = #Temp5.mDate                                                   
    
where Good_Count > 0                
order by #temp2.mDate desc                                                       
            
-- DROPPING THE TEMPORARY TABLES USED ABOVE.....                                                
drop table #temp                                                  
drop table #temp1                                                  
drop table #temp2                                                      
drop table #temp3                                                  
drop table #temp4  
drop table #temp5  
'                                                      
                                                     
--print @sqlquery  

-- EXECUTING THE DYNAMIC SQL QUERY...
exec(@sqlquery)  
  



/*    
Alter By: Muhammad Nasir    
Date:   09/11/2008    
    
Business Logic:    
 The stored procedure is used to get Case info.    
    
List of Input Parameters:    
 @TicketId: Case identification for the client.    
    
List of Output Columns:    
 TicketID_PK,                                                               
 Firstname,                                                               
 MiddleName,                                                               
 Lastname,                                                                   
 Midnum,                                                               
 Contact1,                                                               
 ContactType1,                                                                                  
 DOB,                                                             
 CurrentCourtNum,    
 CurrentCourtloc,    
 Datetypeflag,    
 LanguageSpeak,                                                                   
 GeneralComments,                                                               
 BondFlag,                                                               
 BondsRequiredflag,                                                  
 AccidentFlag,                                                               
 CDLFlag,                                                               
 BaseFeeCharge,                                                               
 FastTrackFee,                                                               
 ContinuanceAmount,                                                               
 RerapAmount,                                                               
 AS Adjustment,                                                               
 Addition,                                                               
 Extraadditonalcharge,                                                               
 TotalFeeCharged,                                                               
 Activeflag,      
 calculatedtotalfee,                                                               
 FirmID,                                                                   
 CaseCount,                                                               
 LockFlag,    
 email,                             
 Paid,    
 SpeedingID,        
 InitialAdjustment,                               
 InitialAdjustmentInitial  ,                                                        
 Addressconfirmflag         ,                                          
 YDS ,                                
 dlstate,                              
 email,                      
 Recordid   ,
--Waqas 5771 04/19/2009 Contact Id and IsContactIDConfirmed Flag added
 ContactId , 
 IsContactIdConfirmed                   
 LateFlag,                  
 ContinuanceFlag  ,                
 ContinuanceAmount,            
 paymentstatus ,          
 Followupflag,          
 Callbackdate,      
 Zip,      
 CaseTypeID,    
 IsTrafficAlert,    
 caseTypeName ,      
 LastGerenalcommentsUpdatedate ,
 LanguageID  
    
*/      
    
-- exec USP_HTS_Get_ClientInfo 73898      
Alter PROCEDURE [dbo].[USP_HTS_Get_ClientInfo] --78091
	@TicketID_PK INT
AS
	DECLARE @ticketnumber VARCHAR(200)
	SET @ticketnumber = ''
	
	SELECT @ticketnumber = @ticketnumber + ttv.RefCaseNumber + ', '
	FROM   tblTicketsViolations ttv
	WHERE  ttv.TicketID_PK = @TicketID_PK
	
	
	
	SELECT TOP 1 
	       T.TicketID_PK,
	       Firstname,
	       Lastname,
	       MiddleName,
	       Address1,
	       Address2,
	       Contact1,
	       ISNULL(ContactType1, 0) AS ContactType1,
	       Contact2,
	       ISNULL(ContactType2, 0) AS ContactType2,
	       Contact3,
	       ISNULL(ContactType3, 0) AS ContactType3,
	       ISNULL(LanguageSpeak, '') AS LanguageSpeak,
	       ISNULL(BondFlag, 0) AS BondFlag,
	       AccidentFlag,
	       CDLFlag,
	       AdsourceID,
	       Activeflag,
	       calculatedtotalfee,
	       DLNumber,
	       --Sabir khan 5160 11/20/2008 set TX as default state...    
	       ISNULL(DLState, 45) AS DLState,
	       CONVERT(VARCHAR(10), dob, 101) AS dob,
	       SSNumber,
	       Email,
	       Midnum,
	       City,
	       ISNULL(Stateid_FK, 0) AS Stateid_FK,
	       Zip,
	       Race,
	       ISNULL(gender, '--Choose-- ') AS gender,
	       Height,
	       WEIGHT,
	       HairColor,
	       Eyes AS EyeColor,
	       GeneralComments,
	       TTV.ReminderComments AS ContactComments,
	       SettingComments,
	       --PymtComments, -- Sarim 2664 06/04/2008         
	       TrialComments,
	       --Waqas 5771 04/15/2009 for contact matters    
	       Recordid = (
	           SELECT TOP 1 ISNULL(recordid, 0)
	           FROM   tblticketsviolations
	           WHERE  ticketid_pk = t.ticketid_pk
	       ),
	       ISNULL(T.ContactID_FK, '') AS ContactID,
	       ISNULL(t.IsContactIDConfirmed, 0) AS IsContactIDConfirmed,
	       0 AS OccupationId,	-- Sarim 2664 06/04/2008                          
	       dbo.fn_HTS_Get_CustomerInfoByMidnumber(5632) AS CaseCount,
	       YDS,
	       Addressconfirmflag,
	       ISNULL(emailnotavailable, 0) AS emailnotavailable,
	       ISNULL(NoDL, 0) AS NoDL,
	       ISNULL(ContinuanceAmount, 0) AS ContinuanceAmount,
	       ContinuanceComments,
	       ISNULL(Continuancestatus, 1) AS ContinuanceStatus,
	       ISNULL(FirmID, 3000) AS FirmID,
	       ISNULL(ContinuanceFlag, 0) AS ContinuanceFlag,
	       ISNULL(ContinuanceDate, '1/1/1900') AS ContinuanceDate,
	       ISNULL(ActiveFlag, 0) AS ActiveFlag,
	       ISNULL(ClientCourtNumber, '0') AS ClientCourtNumber,   --Sabir Khan 7979 07/07/2010 type changed   
	       WalkInClientFlag,
	       ISNULL(T.isPR, 2) AS isPR,
	       ISNULL(T.hasConsultationComments, 2) AS hasConsultationComments,
	       --added khalid        
	       (
	           SELECT COUNT(*)
	           FROM   tblticketsviolations v
	                  JOIN tblcourts c
	                       ON  v.courtid = c.courtid
	           WHERE  c.iscriminalcourt = 1
	                  AND v.ticketid_pk = T.ticketid_pk
	       ) AS Court,
	       ISNULL(midnum, '') AS spn,
	       ISNULL(ttv.setCallComments, '') AS setcallcomments,	-- noufil 4172 0613/2008 Setcall comments                       
	       ISNULL(ttv.ftacallcomments, '') AS ftacallcomments,	-- Zahoor 3997 9/19/8 FTA Call Comments
	                                                          	--Nasir 5381 1/15/2009 add select value general comments update date     
	       ISNULL(T.LastGerenalcommentsUpdatedate, '1/1/1900') AS 
	       LastGerenalcommentsUpdatedate,	--Fahad 5916 08/03/2009 add "ISNull" Check in general comments update date
	                                     	-- Noufil 5884 07/02/2009 SMS Flags Added
	       ISNULL(t.SendSMSFlag1, 0) AS SendSMSFlag1,
	       ISNULL(t.SendSMSFlag2, 0) AS SendSMSFlag2,
	       ISNULL(t.SendSMSFlag3, 0) AS SendSMSFlag3,
	       -- Ozair 7673 02/11/2009 Null check implemented on Language ID	       
	       isnull(l.LanguageID,0) AS LanguageID,
	       tc1.[Description] AS contactTypeDesc1,
	       tc2.[Description] AS contactTypeDesc2,
	       tc3.[Description] AS contactTypeDesc3,
	       @ticketnumber AS TicketNumber,
	       -- Noufil 6766 04/05/2010 State Description Name added
	       ts.[State] AS stateDescription,
	       T.isEmailVerified, -- Afaq 7937 06/29/2010 Add isEmailVerified flag.
	     -- Abbas Qamar 10114 04-03-2012 Checking Fortbend,Montgoemry , HCCC criminal Courts
	       (
	           SELECT COUNT(*)
	           FROM   tblticketsviolations v
	                  JOIN tblcourts c
	                       ON  v.courtid = c.courtid
	           WHERE  c.iscriminalcourt = 1 AND c.Courtid IN (3037,3043,3036) AND c.InActiveFlag=0
	                  AND v.ticketid_pk = T.ticketid_pk
	       ) AS IsCriminalCourt,
	       T.CaseTypeId, --Abbas Qamar 10093 04/10/2012 Case Type ID field added.
	       ISNULL(cl.isLeadsDetailsSubmitted, 0) isLeadsDetailsSubmitted
	FROM   tblTickets T
	       INNER JOIN tblticketsviolations TTV
	            ON  t.TicketID_PK = ttv.TicketID_PK
	                -- Noufil 6766 02/11/2009 join added to get Language ID
	                
	       LEFT OUTER JOIN [Language] l
	            ON  LOWER(LTRIM(RTRIM(l.LanguageName))) = LOWER(LTRIM(RTRIM(t.LanguageSpeak)))
	       LEFT OUTER JOIN tblContactstype tc1
	            ON  tc1.ContactType_PK = t.ContactType1
	       LEFT OUTER JOIN tblContactstype tc2
	            ON  tc2.ContactType_PK = t.ContactType2
	       LEFT OUTER JOIN tblContactstype tc3
	            ON  tc3.ContactType_PK = t.ContactType3
	            LEFT OUTER JOIN tblState ts ON ts.StateID = t.Stateid_FK
	       -- Rab Nawaz Khan 11473 10/30/2013 join the table for Leads. . . 
	       LEFT OUTER JOIN tblCallerIdsFromClients cl ON cl.ticketId = t.TicketID_PK
	WHERE  (T.TicketID_PK = @TicketID_PK)       
        
    
    
    
    
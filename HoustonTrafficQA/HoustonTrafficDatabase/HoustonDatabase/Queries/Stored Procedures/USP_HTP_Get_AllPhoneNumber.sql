USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Get_AllPhoneNumber]    Script Date: 04/11/2012 16:00:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Created By : Abbas Qamar
Task Id : 10093 
Creation Date : 03-28-2012
Business Logic : This procedure returns phone numbers from Non client tables


*/
--[dbo].[USP_HTP_Get_AllPhoneNumber]  6258753
ALTER PROCEDURE [dbo].[USP_HTP_Get_AllPhoneNumber]  

@RecordId INT,
@CaseType INT

AS  
IF @CaseType IN (1,2)
BEGIN
SELECT     TOP 1 ISNULL(tblTicketsArchive.PhoneNumber,'') AS PhoneNumber, ISNULL(tblTicketsArchive.WorkPhoneNumber,'') AS WorkPhoneNumber, 1 AS NotinFamily
FROM         tblTickets AS tt INNER JOIN
                      tblTicketsArchive ON tt.Recordid = tblTicketsArchive.RecordID 
                      INNER JOIN tblTicketsViolationsArchive tva ON tva.RecordID = tblTicketsArchive.RecordID

WHERE     (tt.Recordid = @RecordId)
	
END
ELSE
	IF @CaseType = 4
	BEGIN
		SELECT CASE isnull(PhoneNumber,'') WHEN 'N/A' THEN '' ELSE PhoneNumber END AS PhoneNumber, '' AS WorkPhoneNumber,0 AS NotinFamily 
		FROM civil.dbo.ChildCustodyParties WHERE ChildCustodyPartyId = @RecordId
	END


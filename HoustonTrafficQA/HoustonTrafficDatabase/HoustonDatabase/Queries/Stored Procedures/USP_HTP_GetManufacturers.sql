﻿ /******  
* Created By :	  Saeed Ahmed
* Create Date :   12/02/2010 
* Task ID :		  8585
* Business Logic : This procedure returns all available manufacturers in our system.
* List of Parameter :
* Column Return :     
******/
CREATE PROCEDURE [dbo].[USP_HTP_GetManufacturers]
AS
SELECT id,m.[Name] FROM Manufacturer m


GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetManufacturers] TO dbr_webuser
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DeleteAttorneyAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DeleteAttorneyAddress]
GO


Create Procedure USP_HTS_DeleteAttorneyAddress 
(
@Id int 
)
as 

Delete from attorneyaddress where Id = @Id




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


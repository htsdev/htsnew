
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetCriminalCourts]    Script Date: 05/15/2008 11:50:02 ******/

/*  
Created By : Noufil Khan  
Business Logic : This procedure display all Criminal Courts

*/

CREATE procedure [dbo].[USP_HTP_GetCriminalCourts]
as
select courtid,Courtname from tblcourts where iscriminalcourt=1


go
GRANT EXEC ON [dbo].[USP_HTP_GetCriminalCourts] to dbr_webuser
go





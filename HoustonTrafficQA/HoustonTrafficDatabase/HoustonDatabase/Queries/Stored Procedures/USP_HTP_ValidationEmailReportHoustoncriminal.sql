﻿/*
* Business Logic : This procedure displays two reports 
*					1. Service ticket report.
*					2. ALR hearing. (Displays report who follow update is in past or today)
*/
--Noufil 4641 08/19/2008 default email addresses add for SQL job
 
-- Noufil 4332 08/12/2008 Procedure updated... New method use for validation email.
   
-- [USP_HTP_ValidationEmailReportHoustoncriminal] 'noufil@lntechnologies.com',0,''
  
  
ALTER procedure [dbo].[USP_HTP_ValidationEmailReportHoustoncriminal]--'agha@lntechnologies.com',1  
(        
 @EmailAddress varchar(500) = '',  
 @DisplayReport bit = 0 ,
 @ThisResultSet varchar(max)='' output              
)                                
AS   

-------------------------------------------------------------------------------------------------------------------------------------------
if ( @EmailAddress = '')
begin
	declare @ServerIP varchar(100)
	CREATE TABLE #tempIP (Results varchar(1000) null) 
	BULK INSERT #tempIP
	FROM 'C:\Program Files\Microsoft SQL Server\SQLServerIP.txt'

	select @ServerIP= results from #tempIP

	drop table #tempIP
	
	if @ServerIP='66.195.101.51'	--Live Settings    
		select @EmailAddress =  'hvalidation@sullolaw.com'   
	else if @ServerIP='192.168.168.220'		--Developer Settings
		select @EmailAddress = 'adil@lntechnologies.com'
	else -- QA Settings
		select @EmailAddress = 'QA@lntechnologies.com'
end
--------------------------------------------------------------------------------------------------------------------------------------------------  



declare @c_ticketid varchar(50)                                          
declare @SerialNo int             
declare @counter int                                          
declare @tid varchar(50)                                          
set @counter = 1                          
Declare @SNO int                                            
set @SNO =1
Declare @HTML varchar(max)
declare @ticketid int
set @html = ''
DECLARE @startDate VARCHAR(20)
SET @startDate = convert(varchar(20),getdate(), 101)

DECLARE @endDate VARCHAR(20)
SET @endDate = convert(varchar(20),getdate(), 101)


------------------------------------------------------------- Header CSS -------------------------------------------------------------------

DECLARE @html1 nvarchar(max) 
set  @html1= '<style type="text/css">.clsLeftPaddingTable                                            
   {                                            
    PADDING-LEFT: 5px;                                            
    FONT-SIZE: 8pt;                                            
    COLOR: #123160;                    
    FONT-FAMILY: Tahoma;                                            
    background-color:EFF4FB;                                            
    border-collapse:collapse;                                            
    border-width:1px;                                            
   }                                            
                                   
   </style>                                            
   <H2 align="center"><font color="#3366cc"><STRONG>' 


--------------------------------------------------------- Service TIcket -----------------------------------------------------------------
declare @ServiceticketTitle varchar(50)
Declare @ServiceticketHeader nvarchar(max) 
declare @Serviceticket varchar(max)

set @ServiceticketTitle = 'Criminal - Unsigned Contracts'                                          
set @ServiceticketHeader= '</strong></font></H2>                                            
   <table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black">
   <tr> 
   <td><font color="#3366cc"><STRONG>S.NO</strong></font></td>      
   <td><font color="#3366cc"><STRONG>Division</strong></font></td>
   <td><font color="#3366cc"><STRONG>Category</strong></font></td>      
   <td><font color="#3366cc"><STRONG>Name</strong></font></td>      
   <td><font color="#3366cc"><STRONG>Crt Date</strong></font></td>       
   <td><font color="#3366cc"><STRONG>Crt Time/Time</strong></font></td> 
   <td><font color="#3366cc"><STRONG>Status</strong></font></td>         
   <td><font color="#3366cc"><STRONG>Loc</strong></font></td>                                            
   <td><font color="#3366cc"><STRONG>Open D</strong></font></td>
   <td><font color="#3366cc"><STRONG>Open By</strong></font></td>
   <td><font color="#3366cc"><STRONG>Update D</strong></font></td>
   <td><font color="#3366cc"><STRONG>Priority</strong></font></td>
   <td><font color="#3366cc"><STRONG>Comp.%</strong></font></td> 
   <td><font color="#3366cc"><STRONG>Follow up date</strong></font></td>
   </tr>'  

create table #Servicetickettable(                                          
sno int identity,                                          
sno2 varchar(5) null,                                          
ticketid_pk varchar(50),
Lastname VARCHAR(200),
ClientName varchar(200),
openDate varchar(100),
sortopenDate DATETIME,
Rep varchar(100),
GeneralComments VARCHAR(MAX),
Priority VARCHAR(100),
DESCRIPTION VARCHAR(200),
COMPLETION VARCHAR(200),
FlagID INT,
FID INT,
courtdate VARCHAR(100),
courttime VARCHAR(100),
courtnumbermain INT,
shortdescription VARCHAR(100),
settinginformation VARCHAR(200),
recsortdate VARCHAR(200),
sortrecsortdate DATETIME,
Category VARCHAR(200),
AssignedTo VARCHAR(200),
closeddate VARCHAR(100),
RecDate DATETIME,
courtdatemain VARCHAR(200),
CaseTypeName VARCHAR(100),
ContinuanceOption VARCHAR(500),
CaseTypeId INT,
rec VARCHAR(200),
ServiceTicketFollowupdate VARCHAR(100), --Sabir Khan 5738 06/03/2009 Service Ticket follow update date...
SortServiceTicketFollowupdate DATETIME
)                                          
                              
insert into #Servicetickettable                                          
(                                          
ticketid_pk,
Lastname,
ClientName,
openDate,
sortopenDate,
Rep,
GeneralComments,
Priority,
DESCRIPTION,
COMPLETION,
FlagID,
FID,
courtdate,
courttime,
courtnumbermain,
shortdescription,
settinginformation,
recsortdate,
sortrecsortdate,
Category,
AssignedTo,
closeddate,
RecDate,
courtdatemain,
CaseTypeName,
ContinuanceOption,
CaseTypeId,
rec,
ServiceTicketFollowupdate,          --Sabir Khan 5738 06/03/2009 Service Ticket follow update date...
SortServiceTicketFollowupdate 
)
exec traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords @startDate,@endDate,3992,1,1,2,1

set @counter =1                                                                                    
declare cur_Serviceticket cursor                                          
for select ticketid_pk,sno from #Servicetickettable                                          
open cur_Serviceticket                                          
                                            
FETCH next from cur_Serviceticket into @c_ticketid,@SerialNo                                          
while @@fetch_status = 0                                          
begin                                          
 if(@counter = 1)                                          
 begin                                          
  update #Servicetickettable  set Sno2 = @counter where Sno=@SerialNo                                          
  set @tid = @c_ticketid                                          
  set @counter = @counter + 1                                          
 end                                          
 else                                          
 begin                                          
  if(@c_ticketid <> @tid)                                          
   begin                     
    update #Servicetickettable  set Sno2 = @counter where Sno = @SerialNo                                   
    set @tid = @c_ticketid                                        
 set @counter = @counter + 1                                          
end                                          
 end                                          
FETCH next from cur_Serviceticket into @c_ticketid,@SerialNo                                          
end                          
CLOSE cur_Serviceticket                                         
DEALLOCATE cur_Serviceticket     
                                    
set @Serviceticket  =@html1 + @ServiceticketTitle + @ServiceticketHeader +                                            
                                            
Cast ( (  select                                                                                             
td= '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+ticketid_pk+'" >'+isnull(sno2,'.')+' </a> ', '',                                          
td = isnull(casetypename,'') ,'',   
td = isnull(category,'') ,'',   
td = isnull(Lastname,'') ,'',   
td = isnull(courtdate,'') ,'',   
td = isnull(courttime,'') ,'',   
td = isnull(shortdescription,'') ,'',   
td = isnull(settinginformation,'') ,'',   
td = isnull(recsortdate,'') ,'',
td = isnull(assignedto,'') ,'',
td = isnull(rec,'') ,'',
td = isnull(priority,'') ,'',
td = isnull(completion,'') ,'',
td = ISNULL(ServiceTicketFollowupdate,''),''  --Sabir Khan 5738 06/03/2009 Service Ticket follow update date...

     
from   #Servicetickettable order by sno FOR XML PATH ('tr'),Type) as NVarchar(max) )+'</table><br><br>'                                            
                               
set @Serviceticket  = replace(@Serviceticket , '&lt;','<')                                           
set @Serviceticket = replace(@Serviceticket , '&gt;','>')   


if((select Count(*) from #Servicetickettable) > 0)                                          
 begin                               
  set @HTML =@HTML + @Serviceticket
    END                                        
if((select Count(*) from #Servicetickettable) <= 0)                     
 begin                                          
  set @HTML =@HTML + @html1 + @ServiceticketTitle  +'<table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black"><tr><td align="center"><font color="#3366cc"><STRONG>NO RECORD FOUND</STRONG></font> </td></tr></table>'                                      
END
drop table #Servicetickettable 

------------------------------------------------------------ End Service Ticket -----------------------------------------------------------------

-- Noufil 5819 05/14/2009 ALR hearing report added
---------------------------------------------------------------- ALRHearing  ---------------------------------------------------------------------
declare @ALRHearingTitle varchar(50)
Declare @ALRHearingHeader nvarchar(max) 
declare @ALRHearing varchar(max)

set @ALRHearingTitle = 'ALR Alerts'                                          
set @ALRHearingHeader= '</strong></font></H2>                                            
   <table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black">
   <tr> 
   <td style="width: 3%"><font color="#3366cc" align="left"><STRONG>S#</strong></font></td>      
   <td style="width: 10%"><font color="#3366cc"><STRONG>Cause Number</strong></font></td>
   <td style="width: 13%"><font color="#3366cc"><STRONG>First Name</strong></font></td>      
   <td style="width: 13%"><font color="#3366cc"><STRONG>Last Name</strong></font></td>      
   <td style="width: 17%"><font color="#3366cc"><STRONG>Court Date & Time</strong></font></td>          
   <td style="width: 8%"><font color="#3366cc"><STRONG>Room #</strong></font></td>         
   <td style="width: 22%"><font color="#3366cc"><STRONG>Alert Type</strong></font></td>                                            
   <td style="width: 9%"><font color="#3366cc" ><STRONG>Follow-up</strong></font></td>
   </tr>'  
-- style="width: 20%"
create table #ALRHearingtable(                                          
sno int identity,                                          
sno2 varchar(5) null,                                          
ticketid_pk varchar(50),
FirstName varchar(200),
Lastname VARCHAR(200),
casenumberaasignbycourt varchar(100),
refcasenumber VARCHAR(100),
courtdatemain VARCHAR(20),
courttime VARCHAR(10),
courtnumbermain INT,
hiredate DATETIME,
title VARCHAR(200),
folloupdate DATETIME,
subdoctype INT,
Maxbusinesdays INT
)                                          
                              
insert into #ALRHearingtable                                          
(                                          
ticketid_pk,
FirstName,
Lastname,
casenumberaasignbycourt,
refcasenumber,
courtdatemain,
courttime,
courtnumbermain,
hiredate,
title,
folloupdate,
subdoctype,
Maxbusinesdays
)
exec [dbo].[USP_HTP_GET_ALRHEARINGREPORT] 1

set @counter =1                                                                                    
declare cur_ALRHearing cursor                                          
for select ticketid_pk,sno from #ALRHearingtable                                          
open cur_ALRHearing                                          
                                            
FETCH next from cur_ALRHearing into @c_ticketid,@SerialNo                                          
while @@fetch_status = 0                                          
begin                                          
 if(@counter = 1)                                          
 begin                                          
  update #ALRHearingtable  set Sno2 = @counter where Sno=@SerialNo                                          
  set @tid = @c_ticketid                                          
  set @counter = @counter + 1                                          
 end                                          
 else                                          
 begin                                          
  if(@c_ticketid <> @tid)                                          
   begin                     
    update #ALRHearingtable  set Sno2 = @counter where Sno = @SerialNo                                   
    set @tid = @c_ticketid                                        
 set @counter = @counter + 1                                          
end                                          
 end                                          
FETCH next from cur_ALRHearing into @c_ticketid,@SerialNo                                          
end                          
CLOSE cur_ALRHearing                                         
DEALLOCATE cur_ALRHearing     
                                    
set @ALRHearing  =@html1 + @ALRHearingTitle + @ALRHearingHeader +                                            
                                            
Cast ( (  select                                                                                             
td= '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+ticketid_pk+'" >'+isnull(sno2,'.')+' </a> ', '',                                          
td = isnull(casenumberaasignbycourt,'') ,'',   
td = isnull(FirstName,'') ,'',   
td = isnull(Lastname,'') ,'',   
td = isnull(courtdatemain,'') ,'',   
td = isnull(courtnumbermain,'') ,'',
td = isnull(title,'') ,'',   
td = isnull(CONVERT (VARCHAR(20),folloupdate,101),'') ,''


from   #ALRHearingtable order by sno FOR XML PATH ('tr'),Type) as NVarchar(max) )+'</table><br><br>'                                            
                               
set @ALRHearing  = replace(@ALRHearing , '&lt;','<')                                           
set @ALRHearing = replace(@ALRHearing , '&gt;','>')   


if((select Count(*) from #ALRHearingtable) > 0)                                          
 begin                               
  set @HTML =@HTML + @ALRHearing
    END                                        
if((select Count(*) from #ALRHearingtable) <= 0)                     
 begin                                          
  set @HTML =@HTML + @html1 + @ALRHearingTitle  +'<table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black"><tr><td align="center"><font color="#3366cc"><STRONG>NO RECORD FOUND</STRONG></font> </td></tr></table>'                                      
END
drop table #ALRHearingtable

---------------------------------------------------------------- ALRHearing  ---------------------------------------------------------------------
--declare @ThisResultidServicecrim varchar(max)
--declare @ThisPendingCount varchar(5)
--declare @ThisAlertCount varchar(5)
--declare @ThisTotalCount varchar(5)
--
--declare @Param0 varchar(200)
--Set @Param0 = '''''' + convert(varchar(20),getdate(), 101) + ''''', ''''' + convert(varchar(20),getdate(), 101) + ''''', 3992,1,1,0,1,2,1'
--
--execute USP_HTP_Generics_Get_XMLSet_For_Validation 
--			'traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords ', 
--			 @Param0, 
--			'Service Ticket Report (criminal)',
--			'TicketID_PK,casetypename as [Division],category as [Category],clientname as [Name],courtdate as [Crt Date],courttime as [Crt Time],shortdescription as [Status],settinginformation as [Loc],recsortdate as [Open D],assignedto as [Open By],rec as [Update D],priority as [Priority],completion as [Comp.%]',
--			1,@ResultSet = @ThisResultidServicecrim output, @PendingCount = @ThisPendingCount OutPut,@AlertCount =@ThisAlertCount output  ,@TotalCount=@ThisTotalCount output
--
--set @ThisResultSet= isnull(@ThisResultidServicecrim,'') 
--set @ThisResultSet = replace(@ThisResultSet, '&lt;','<')                               
--set @ThisResultSet= replace(@ThisResultSet, '&gt;','>')  
--select (@ThisResultSet)


--if @EmailAddress <> ''
--
--BEGIN
--PRINT ('Email start')
--declare @subject varchar(max)
----ozair 4891 10/03/2008 subject modified
--set @subject= 'Validation Report (Criminal) '+ convert(varchar(20),getdate(),101)
--
--EXEC msdb.dbo.sp_send_dbmail
--    @profile_name = 'Traffic System',
--    @recipients = @EmailAddress,
--    @subject = @subject,
--    @body = @ThisResultSet,
--    @body_format = 'HTML';
--    
--PRINT ('Email Done')
--End

Declare @Subject varchar(50)                                            
set @Subject ='Validation Report (Criminal) '+ convert(varchar(20),getdate(),101)

if len(isnull(@html,'')) > 0       
	if @DisplayReport = 1    
		Select @HTML as ValidationReport    
	Else  
		EXEC msdb.dbo.sp_send_dbmail                                       
		@profile_name = 'Traffic System',                                       
		@recipients =  @EmailAddress ,                          
		@subject = @Subject,                                       
		@body = @HTML,                                      
		@body_format = 'HTML' ;                                      
else     
	RAISERROR ('An error occured in Validation Email Report.', 16,1);
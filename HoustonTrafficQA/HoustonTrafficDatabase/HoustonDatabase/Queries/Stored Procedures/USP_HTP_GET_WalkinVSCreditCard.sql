/*******************  
  
Alter By: Sabir Khan  
Date:  06/25/2010  
Task Id: 7951  
  
Business Logic:  
 This procedure is used to get total number of walkin clients, total number of credit card clients and also thier revenue for spacified date range  
  
Input Parameters:  
 @sdate: Start Date  
 @edate: End Date  
 @location: Location: 1 for houston and 2 for dallas.  
  
Output Columns:  
 Month: Name of month of spacified date range  
 Walkin Client: total number of walkin client  
 Credit Card: total number of credit card client.  
 Revenue: total reveune.  
  
*****************************/  
-- USP_HTP_GET_WalkinVSCreditCard '06/01/2010','06/01/2010',2  
ALTER PROCEDURE [dbo].[USP_HTP_GET_WalkinVSCreditCard]  
  
  
@sdate DATETIME,  
@edate DATETIME,  
@location INT  
AS  
  
IF @location = 2  
BEGIN  
  select distinct t.ticketid_pk,tp.recdate as date,sum(ChargeAmount) as chargeamount  
  into #tempdallas  
   FROM DallasTrafficTickets.dbo.tblTicketsPayment tp inner JOIN DallasTrafficTickets.dbo.tbltickets t 
   --inner join tblticketsviolations tv on tv.ticketid_pk  = t.ticketid_pk 
  on t.ticketid_pk = tp.ticketid   
  where --isnull(t.activeflag ,0) = 1  
 -- AND 
ISNULL(tp.PaymentVoid,0) = 0  
  and datediff(month,tp.recdate,@sdate) <=0  
  and datediff(month,tp.recdate,@edate) >=0  
  and datediff(year,tp.recdate,@sdate)<=0  
  and datediff(year,tp.recdate,@edate)>=0  
  and tp.paymenttype not in (99,100)--Muhammad Ali 7951 07/16/2010 Remove Extra Payment Type 9,11,8,103..becouse in financial Report we have 99,100 payment type cataria..  

  group by t.ticketid_pk, tp.recdate  
 --Muhammad Ali 7951 07/16/2010 Comment this line for extra payments records....  
 -- having sum(isnull(tp.chargeamount,0)) >0  
  
  ALTER TABLE #tempdallas add walkinclient INT  
  
  update t  
  set walkinclient = 1  
  from #tempdallas t inner join DallasTrafficTickets.dbo.tbltickets tt on tt.ticketid_pk = t.ticketid_pk  
  where isnull(tt.WalkinClient,0) = 1 and isnull(tt.activeflag,0) = 1  
  
  alter table #tempdallas add CreditCard int  
  
  update t  
  set CreditCard = 1  
  from #tempdallas t inner join DallasTrafficTickets.dbo.tbltickets tt on tt.ticketid_pk = t.ticketid_pk  
  where isnull(tt.WalkinClient,0) = 0 and isnull(tt.activeflag,0) = 1  
  
  select month(date) as date1,count(walkinclient) as walkin ,count(CreditCard) as credit,round(sum(chargeamount),2) as revenue into #temp1dallas  
  from #tempdallas group by month(date) order by month(date)  
  
  select DateName(mm,DATEADD(mm,date1,-1))  [Month],walkin [Walk In Client],credit [Credit Card Client],revenue [Revenue] from #temp1dallas  
  
  drop table #tempdallas  
  drop table #temp1dallas  
 END  
ELSE  
 BEGIN  
  select distinct t.ticketid_pk,tp.recdate as date,sum(chargeamount) as chargeamount  
  into #temphouston  
  from tblTicketsPayment tp inner JOIN tbltickets t  
  on t.ticketid_pk = tp.ticketid   
  where --isnull(t.activeflag ,0) = 1  
 -- AND 
ISNULL(tp.PaymentVoid,0) = 0  
  and datediff(month,tp.recdate,@sdate) <=0  
  and datediff(month,tp.recdate,@edate) >=0  
  and datediff(year,tp.recdate,@sdate)<=0  
  and datediff(year,tp.recdate,@edate)>=0  
  and tp.paymenttype not in (99,100,9,11,8,103)  
  group by t.ticketid_pk, tp.recdate  
  having sum(isnull(tp.chargeamount,0)) >0  
  
  ALTER TABLE #temphouston add walkinclient INT  
  
  update t  
  set walkinclient = 1  
  from #temphouston t inner join tbltickets tt on tt.ticketid_pk = t.ticketid_pk  
  where isnull(tt.WalkInClientFlag,0) = 1 and isnull(tt.activeflag,0) = 1  
  
  alter table #temphouston add CreditCard int  
  
  update t  
  set CreditCard = 1  
  from #temphouston t inner join tbltickets tt on tt.ticketid_pk = t.ticketid_pk  
  where isnull(tt.WalkInClientFlag,0) = 0 and isnull(tt.activeflag,0) = 1  
  
  select month(date) as date1,count(walkinclient) as walkin ,count(CreditCard) as credit,round(sum(chargeamount),2) as revenue into #temp1houston  
  from #temphouston group by month(date) order by month(date)  
  
  select DateName(mm,DATEADD(mm,date1,-1))  [Month],walkin [Walk In Client],credit [Credit Card Client],revenue [Revenue] from #temp1houston  
  
  drop table #temphouston  
  drop table #temp1houston  
 END  
  
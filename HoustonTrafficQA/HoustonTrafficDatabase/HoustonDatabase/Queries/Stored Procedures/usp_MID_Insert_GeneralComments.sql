SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Insert_GeneralComments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Insert_GeneralComments]
GO




create procedure [dbo].[usp_MID_Insert_GeneralComments]
	(
		@TicketNumber	varchar(25)
	)

as


declare	@str	varchar(1000)

select 	@str=''

select	@str =@str + 
	upper(t.InsertDate)+' - '+ 
	upper(u.Abbreviation)+' - '+
	upper(m.StatusDescription)+' - '+
	upper(t.Comments)+' , '
FROM    dbo.tblTicketsArchivePCCR t 
INNER JOIN
        dbo.tblUsers u
ON 	t.EmployeeID = u.EmployeeID 
INNER JOIN
        dbo.tblPCCRStatus m
ON 	t.PCCRStatusID = m.StatusID
where	t.recordid_fk = (
			select top 1 recordid from tblticketsarchive
			where	ticketnumber = @ticketnumber
			)

if len(@str) > 0 
begin	
	update 	tbltickets
	set	generalcomments = generalcomments + ' ' + @str
	where 	ticketnumber_pk = @ticketnumber
end
	






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


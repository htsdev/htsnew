SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_Comments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_Comments]
GO



 
  
  
--USP_HTS_Delete_Comments 9003, 3991,1
--grant execute on dbo.USP_HTS_Delete_Comments to dbr_webuser  
  
create procedure [dbo].[USP_HTS_Delete_Comments]
@TicketID as int,  
@EmployeeID as int,  
@CommentID as int
As  
  
Declare @SQLQry as varchar(max)  
  
 If @CommentID = 1 -- GeneralComments  
  Begin  
   -- UPDATING TBLTICKETS FOR NEW COMMENTS  
   Set @SQLQry = ' Update TblTickets  Set GeneralComments = '''', EmployeeIDUpdate = ' + Convert(Varchar(10),@EmployeeID) + ' Where TicketID_PK = ' + Convert(varchar(15),@TicketID)  
   -- INSERTING NOTES FOR CASE HISTORY  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''General Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else IF @CommentID = 2 -- SettingComments  
  Begin  
   -- UPDATING TBLTICKETS FOR NEW COMMENTS  
   Set @SQLQry = ' Update TblTickets  Set SettingComments = '''', EmployeeIDUpdate = ' + Convert(Varchar(10),@EmployeeID) + ' Where TicketID_PK = ' + Convert(varchar(15),@TicketID)  
   -- INSERTING NOTES FOR CASE HISTORY  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''Setting Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else If @CommentID = 3 -- ReminderComments  
  Begin  
   -- UPDATING TBLTICKETS FOR NEW COMMENTS  
   Set @SQLQry = ' Update TblTicketsViolations  Set ReminderComments = '''', ReminderCallEmpID = ' + Convert(Varchar(10),@EmployeeID) + ' Where TicketID_PK = ' + Convert(varchar(15),@TicketID)  
   -- INSERTING NOTES FOR CASE HISTORY  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''Contact Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else If @CommentID = 4 -- TrialComments  
  Begin  
   -- UPDATING TBLTICKETS FOR NEW COMMENTS  
   Set @SQLQry = ' Update TblTickets  Set TrialComments = '''', EmployeeIDUpdate = ' + Convert(Varchar(10),@EmployeeID) + ' Where TicketID_PK = ' + Convert(varchar(15),@TicketID)  
   -- INSERTING NOTES FOR CASE HISTORY  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''Trial Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else If @CommentID = 5 -- PreTrialComments  
  Begin  
   -- UPDATING TBLTICKETS FOR NEW COMMENTS  
   Set @SQLQry = ' Update TblTickets  Set PreTrialComments = '''', EmployeeIDUpdate = ' + Convert(Varchar(10),@EmployeeID) + ' Where TicketID_PK = ' + Convert(varchar(15),@TicketID)  
   -- INSERTING NOTES FOR CASE HISTORY  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''Pre-Trial Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else If @CommentID = 6 -- ContinuanceComments  
  Begin  
   -- UPDATING TBLTICKETS FOR NEW COMMENTS  
   Set @SQLQry = ' Update TblTickets  Set ContinuanceComments = '''', EmployeeIDUpdate = ' + Convert(Varchar(10),@EmployeeID) + ' Where TicketID_PK = ' + Convert(varchar(15),@TicketID)  
   -- INSERTING NOTES FOR CASE HISTORY  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''Continuance Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else If @CommentID = 7 -- ConsultationComments  
  Begin  
   -- UPDATING tbl_hts_consultationcomments FOR NEW COMMENTS  
   Set @SQLQry = ' Update tbl_hts_consultationcomments  Set Comments = '''', CommentsDate = GetDate() Where TicketID_FK = ' + Convert(varchar(15),@TicketID)  
   Set @SQLQry = @SQLQry + ' Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(' + Convert(Varchar(15),@TicketID) + ', ''Consultation Notes Deleted!'', ' + Convert(Varchar(10),@EmployeeID) + ') '  
  End  
 Else   
 Set @SQLQry = @SQLQry  
  
Exec (@SQLQry)  
--Select @SQLQry  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


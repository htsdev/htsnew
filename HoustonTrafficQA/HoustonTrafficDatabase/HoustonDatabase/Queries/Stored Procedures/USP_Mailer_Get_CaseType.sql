/****** 

Business Logic:	The procedure is used by LMS application to display print date letter type of select city during selected date range ...
				
List of Parameters:
	@startdate: Start Date		
	@enddate:	End Date
	@City:	City.
	
	
List of Columns:	
	Print Date: Print Date.
	Letter Type: Type of Letter.	

******/
--  USP_Mailer_Get_CaseType '7/21/2006','7/21/2007'      
ALTER procedure [dbo].[USP_Mailer_Get_CaseType]       
      
@startdate datetime,      
@enddate datetime,
@city	int=1	-- 1=Houston, 2=Dallas, 3=Both
      
as      
      
      
      
if @city=1
begin  
SELECT distinct      
case c.courtcategorynum
when 2 then 
	ct.CourtId
else
	c.courtcategorynum
end as courtid,      
  
 l.letterid_pk,
case c.courtcategorynum
when 2 then
(ct.courtname +'-'+ l.LetterName) 
else
(c.courtcategoryname+'-'+ l.LetterName) 
end as LetterType,
convert(datetime,  convert(varchar(10), ln.recordloaddate,101)) as printdate,ln.batchId_fk
FROM   dbo.tblLetterNotes ln


INNER JOIN
dbo.tblCourts ct
ON ln.CourtId = ct.CourtId 

INNER JOIN      
 dbo.tblLetter l       
ON  ln.LetterType = l.LetterID_PK   
inner join tblbatchletter b  
on b.batchid = ln.batchid_fk      
INNER JOIN      
 dbo.tblcourtcategories c       
ON  b.courtid =  c.courtcategorynum 
and b.lettertype = l.letterid_pk
and c.courtcategorynum = l.courtcategory 
  
--WHERE   (ln.RecordLoadDate BETWEEN @startdate AND @enddate)      
where datediff(day, ln.recordloaddate, @startdate)<=0 
and datediff(day, ln.recordloaddate, @enddate)>=0
--Sabir Khan 5726 03/30/2009 Fort Worth Municipal Court has been excluded from houston category. (27) is for Fort Worth Municipal Court.
-- tahir 6741 10/15/09 excluded arlington court....
--Yasir Kamal 7218 01/13/2010 lms structure changed. 
--and c.courtcategorynum not in(6,11,16,25,27,28)
AND c.LocationId_FK <> 2
  
order by  convert(datetime,  convert(varchar(10), ln.recordloaddate,101)),    
 case c.courtcategorynum
when 2 then 
	ct.CourtId
else
	c.courtcategorynum
end 
, l.letterid_pk      
end

else if @city=2
begin  
SELECT distinct      
 c.courtcategorynum as courtid,      
 l.letterid_pk,      
 (c.courtcategoryname+'-'+ l.LetterName)as LetterType  ,  
convert(datetime,  convert(varchar(10), ln.recordloaddate,101)) as printdate  
FROM   dbo.tblLetterNotes ln       
    
INNER JOIN      
 dbo.tblLetter l       
ON  ln.LetterType = l.LetterID_PK   
inner join tblbatchletter b  
on b.batchid = ln.batchid_fk      
INNER JOIN      
 dbo.tblcourtcategories c       
ON  b.courtid =  c.courtcategorynum  
and b.lettertype = l.letterid_pk
and c.courtcategorynum = l.courtcategory
  
WHERE   datediff(day, ln.RecordLoadDate , @startdate) <= 0
AND datediff(day, ln.recordloaddate,  @enddate) >= 0 
--Sabir Khan 5726 03/30/2009 Fort Worth Municipal Court has been included in Dallas category. (27) is for Fort Worth Municipal Court.   
-- tahir 6741 10/15/09 added arlington court....
--and c.courtcategorynum in(6,11,16,25,27,28)
AND c.LocationId_FK = 2
  
order by  convert(datetime,  convert(varchar(10), ln.recordloaddate,101)),    
 c.courtcategorynum, l.letterid_pk      
end

else 
begin  
SELECT distinct      
 c.courtcategorynum as courtid,      
 l.letterid_pk,      
 (c.courtcategoryname+'-'+ l.LetterName)as LetterType  ,  
convert(datetime,  convert(varchar(10), ln.recordloaddate,101)) as printdate  
FROM   dbo.tblLetterNotes ln       
    
INNER JOIN      
 dbo.tblLetter l       
ON  ln.LetterType = l.LetterID_PK   
inner join tblbatchletter b  
on b.batchid = ln.batchid_fk      
INNER JOIN      
 dbo.tblcourtcategories c       
ON  b.courtid =  c.courtcategorynum  
and b.lettertype = l.letterid_pk
and c.courtcategorynum = l.courtcategory 
  
WHERE   datediff(day, ln.RecordLoadDate , @startdate) <= 0
AND datediff(day, ln.recordloaddate,  @enddate) >= 0   
  
order by  convert(datetime,  convert(varchar(10), ln.recordloaddate,101)),    
 c.courtcategorynum, l.letterid_pk      

end



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_bondflagdiscrepancy]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_bondflagdiscrepancy]
GO
       
CREATE procedure dbo.usp_hts_bondflagdiscrepancy      
      
as      
      
declare @tempdesc table (                              
 rowid int identity(1,1),                              
 TicketID_PK int,             
 CauseNumber varchar(30),            
 LastName varchar(50),            
 firstname varchar(50),
 status	varchar(20),            
 courtinfo varchar(100),             
 BondFlag varchar(20),            
 BondDocuments varchar(20),            
 activeflag int              
 )                              
                              
select  distinct                                    
 TT.TicketID_PK,            
 isnull(ttv.casenumassignedbycourt,'') as  CauseNumber,                                
 tt.lastname,            
 tt.firstname,
 isnull(c1.shortdescription,'N/A') as status,            
 convert(varchar(11),TTV.courtdatemain,101)+' @ '+ replace(substring(convert(varchar(20),TTV.courtdatemain),13,18),'12:00AM','0:00AM') +' # ' + convert(varchar,convert(int, isnull(TTV.courtnumbermain,0))) as courtinfo,              
 case when isnull(tt.Bondflag,0) = 0 then 'No' else 'Yes' end as bondflag,            
 case when (select count(sb.ticketid) from tblscanbook sb where sb.ticketid=tt.TicketId_Pk and sb.doctypeid in (8,12)) > 0 then 'Yes' else             
 case when (select count(hn.ticketid_fk) from tblHTSNotes hn where hn.ticketid_fk=tt.TicketId_Pk and hn.LetterID_FK =4)> 0 then 'Yes' else         
 case when (select count(tn.ticketid) from tblticketsnotes tn where tn.ticketid=tt.TicketId_Pk and tn.subject='bond letter printed')> 0 then 'Yes' else 'No' end end end as BondDocuments,        
 tt.activeflag            
 --dbo.Fn_CheckDiscrepency(ttv.Ticketsviolationid) as Vld,             
into #tempdesc         
FROM                                    
 tbltickets tt,                               
 tblticketsviolations ttv,            
 tblCourtViolationStatus c1,            
 tblticketspayment p            
            
                             
where --1=1                              
                    
-- JOIN CLAUSES.........                              
--and  
  
 tt.ticketid_pk = ttv.ticketid_pk                              
and ttv.courtviolationstatusidmain = c1.courtviolationstatusid            
and tt.ticketid_pk=p.ticketid            
            
                             
-- FILTER CLAUSES......                  
and p.paymentvoid<>1            
and p.paymenttype <> 8          
-- exclude tickets having no bond discrepancy flag          
and tt.ticketid_pk not in (select distinct ticketid_pk from tblticketsflag where flagid=32)       
--Exclude Disposed Cases            
and c1.categoryid <> 50    
      
and tt.activeflag = 1                                     
                             
order by tt.TicketID_PK              
            
insert into @tempdesc (TicketID_PK, CauseNumber, LastName, firstname, status, courtinfo, BondFlag, BondDocuments, activeflag)            
(select distinct * from #tempdesc where bondflag<>bonddocuments)            
            
drop table #tempdesc            
            
select distinct * from @tempdesc


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



grant execute on usp_hts_bondflagdiscrepancy to dbr_webuser


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
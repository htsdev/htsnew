ALTER PROCEDURE USP_HTS_GET_MAILER_INFO          
@TicketID int          
AS          
SELECT  distinct   CONVERT(varchar, ta.ListDate, 101) AS UploadDate, t.TicketID_PK, ISNULL(t.MailerID, 0) AS MailerID, o.LastName, o.FirstName,         
                      o.LastName + ', ' + o.FirstName AS OfficerName, l.LetterName,CONVERT(varchar, ln.recordloaddate, 101) as MailDate       
FROM         tblTickets AS t INNER JOIN    
    tblticketsviolations v on t.ticketid_pk=v.ticketid_pk left outer join           
                      tblTicketsArchive AS ta ON v.RecordId = ta.RecordID left outer JOIN        
                      tblOfficer AS o ON v.ticketOfficerNumber = o.OfficerNumber_PK left outer JOIN        
                      tblLetterNotes AS ln ON t.MailerID = ln.NoteId left outer JOIN        
                      tblLetter AS l ON ln.LetterType = l.LetterID_PK           
WHERE  t.TicketID_PK = @TicketID 
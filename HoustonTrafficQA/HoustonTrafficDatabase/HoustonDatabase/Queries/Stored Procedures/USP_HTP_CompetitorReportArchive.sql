/**************************
Created By:		Tahir Ahmed
Create Date:	9/12/2008

Business Logic:
	The stored procude is used by HTP /Backroom /Competitor Report. 
	It displays the attorneys/bond companies hired for HMC Jury Trial cases

Input Parameters:
	@startdate:		Start date to search attorneys/bonding companies hired on 
	@enddate:		End date to search attorneys/bonding companies hired on 
	@Type:			Type of the report (Attorney tracker, bond tracker, Attorney archive)
	@NoOfRecords:	Number of attornyes to be displayed

Output Columns:
name:			Name of the attorney or bonding company
noofcases:		Number of cases for which the attorney/bonding company hired
sumBond_clients:number of distinct people hired for bonds
AmountSum:		sum of bond amount
percentage:		share of the attorney in total cases
perc_clients:	share of attorney in total clients
ArrCount:		number of arraignment cases
JuryCount:		number of jury trial cases
NonJuryCount:	number on non-jury trial cases

******************************/
 
  
-- grant execute on dbo.[usp_hts_CompetitorReport_Archive] to dbr_webuser   
alter procedure [dbo].[USP_HTP_CompetitorReportArchive]              
              
@startdate datetime,              
@enddate datetime,              
@Type int,   
@NoOfRecords int          
              
as      
declare @totalclients float   
declare @totalcasess float     
  
  
  
select distinct eet.associatedcasenumber as midnumber,eet.causenumber, Attorney=p.name,c.categoryid  
into #tempp                    
FROM	
-- tahir 6671 12/16/209 removed non-client reference to get informaton from competitor archive table
--tblTicketsViolationsArchive AS tv 
--INNER JOIN  
--	tblTicketsArchive AS t 
--ON	t.RecordID = tv.RecordID 
--INNER JOIN  
	LoaderFilesArchive.dbo.tblEventExtractTemp_competitor AS eet 
--ON	REPLACE(eet.causenumber, ' ', '') = ISNULL(tv.CauseNumber, '') 
INNER JOIN  
	tblCompetitors AS p 
ON	ISNULL(eet.attorneyname, '') = p.Name 
INNER JOIN  
	tblCourtViolationStatus c ON eet.status = c.Description AND	c.CategoryID = 4
--AND tv.violationstatusid = c.CourtViolationStatusID  
WHERE	(ISNULL(eet.barnumber, '') <> '' OR LEN(ISNULL(eet.attorneyname, '')) <> 0 ) 
AND		(ISNULL(eet.bondingnumber, '') = '') 
AND		(ISNULL(eet.bondingcompanyname, '') = '') 
--AND		(tv.CauseNumber IS NOT NULL) 
and datediff(day, eet.updatedate, @startdate)<=0 
and datediff(day, eet.updatedate, @enddate)>=0 
--AND		(tv.CourtLocation IN (3001, 3002, 3003)) 



--Sabir Khan 6280 08/04/2009 Get count of No Attorney...
--------------------------------------------------------
INSERT INTO #tempp(midnumber,causenumber,Attorney,categoryid)
select distinct eet.associatedcasenumber as midnumber, eet.causenumber,
	'NO ATTORNEY' AS Attorney ,c.categoryid  
                    
FROM	
-- tahir 6671 12/16/209 removed non-client reference to get informaton from competitor archive table
--tblTicketsViolationsArchive AS tv 
--INNER JOIN  tblTicketsArchive AS t ON	t.RecordID = tv.RecordID 
--INNER JOIN  
LoaderFilesArchive.dbo.tblEventExtractTemp_competitor AS eet 
--ON	REPLACE(eet.causenumber, ' ', '') = ISNULL(tv.CauseNumber, '') 
INNER JOIN  tblCourtViolationStatus c ON eet.status = c.Description AND c.CategoryID = 4    
--AND tv.violationstatusid = c.CourtViolationStatusID  
WHERE	ISNULL(eet.barnumber, '') = '' 
AND     LEN(ISNULL(eet.attorneyname, '')) = 0 
--AND		(tv.CauseNumber IS NOT NULL) 
and datediff(day, eet.updatedate, @startdate)<=0 
and datediff(day, eet.updatedate, @enddate)>=0 
--AND		(tv.CourtLocation IN (3001, 3002, 3003)) 

------------------------------------------------------------

select @totalcasess=count(distinct causenumber) from #tempp     
select count(distinct midnumber) as sumBond_clients  
into #SumBond from #tempp              
group by Attorney       
  
Set @totalclients = (select sum(sumBond_clients) from #SumBond)  
drop table #SumBond


-- Faique 6505 12/12/2012 assaign variable values as 1 if it is zero, preventing for divide by zero exception
IF @totalcasess = 0
  BEGIN
  	SET @totalcasess = 1
  END
  
  IF @totalclients = 0
  BEGIN
  	SET @totalclients = 1
  END  
 
 -- tahir 6504 09/01/2009 fixed the union clause issue...
 
select top (@NoOfRecords) 
	Attorney as name,
	count(distinct causenumber) as noofcases, 
	count(distinct midnumber) as sumBond_clients,  
	0 as AmountSum,
	round(((count(distinct causenumber)/@totalcasess)*100),2) as percentage,  
	(round((count(distinct midnumber) *100 / @totalclients),2)) as perc_clients,  
	0 as ArrCount,
	count(case when categoryid=4 then categoryid end)as JuryCount,
	0 as NonJuryCount          
into #tempp1              
from #tempp WHERE attorney <> 'NO ATTORNEY'      
group by Attorney       
order by noofcases desc

--Sabir Khan 6280 08/04/2009 Get count of No Attorney...
insert into #tempp1
	select 'NO ATTORNEY' as name,
	count(distinct causenumber) as noofcases, 
	count(distinct midnumber) as sumBond_clients,  
	0 as AmountSum,
	round(((count(distinct causenumber)/@totalcasess)*100),2) as percentage,  
	(round((count(distinct midnumber) *100 / @totalclients),2)) as perc_clients,  
	0 as ArrCount,
	count(case when categoryid=4 then categoryid end)as JuryCount,
	0 as NonJuryCount          
from #tempp        WHERE attorney = 'NO ATTORNEY'      

select * from #tempp1 order by noofcases desc
  
drop table #tempp         
drop table #tempp1         

go

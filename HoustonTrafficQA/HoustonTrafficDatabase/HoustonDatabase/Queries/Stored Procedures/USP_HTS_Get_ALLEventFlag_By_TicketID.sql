  
alter procedure [dbo].[USP_HTS_Get_ALLEventFlag_By_TicketID]      
    
@TicketID as int      
      
as      
      
select * from tblEventflags        
where flagid_pk        
not in       
(select flagid from  tblticketsflag where ticketid_pk =@TicketID and flagid != 9 and flagid !=23)      
and ActiveStatus = 1  
order by Description
  
  
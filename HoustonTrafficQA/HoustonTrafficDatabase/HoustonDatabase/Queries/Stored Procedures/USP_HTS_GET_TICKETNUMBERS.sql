SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_TICKETNUMBERS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_TICKETNUMBERS]
GO


CREATE PROCEDURE USP_HTS_GET_TICKETNUMBERS
@TicketID_pk int
as
Select refcasenumber as TicketNumber from tblticketsviolations where TicketID_PK = @TicketID_pk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


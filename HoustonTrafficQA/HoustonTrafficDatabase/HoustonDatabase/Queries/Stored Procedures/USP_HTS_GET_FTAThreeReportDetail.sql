ALTER procedure USP_HTS_GET_FTAThreeReportDetail   
 (  
 -- INPUT PARAMETER FOR MAIL DATE.....  
 @MailDate datetime,  
  
 -- TYPE OF DETAILS TO FETCH (EITHER CLIENT OR QOUTE)  
 @DetailType varchar(20)  
 )  
as  
  
-- ADDING TIME PART TO THE INPUT MAIL DATE.....  
set @MailDate = @MailDate + '23:59:59'  
  
-- CREATING TEMPORARY TABLE........  
declare @Archive TABLE  
(  
 [midnumber] [varchar] (20)  ,  
 [ticketnumber] [varchar] (20)  ,  
 [courtid] [int] NULL ,  
 [maildatefta_three] [datetime] NULL   
)   
  
  
-- FIRST INSERTING RAW DATA FOR THE GIVEN DATE RANGE INTO TEMPORARY TABLE  
insert into @Archive  
select   
 -- DISTINCT MID NUMBER.....   
 midnumber,  
  
 -- MIN. OF RELATED INFORMATION, IF MORE THAN ONE CASE EXISTS FOR THE SAME MAIL DATE.....  
 min(ticketnumber),  
 min(courtid),  
 min(convert(varchar(10),maildateftarm,101))  
from tblticketsarchive  
  
-- RECORDS RELATED TO THE GIVEN DATE RANGE......  
where datediff(day,maildateftarm,@maildate) = 0  
  
-- AND RELATED TO INSIDE COURTS ONLY......  
and courtid in (3001,3002,3003)  
group by  
 midnumber  
  
  
-- IF DETAILS OF HIRED CLIENTS REQUIRE....................  
if @DetailType = 'hired'  
 begin  
  SELECT  t.TicketID_PK,   
   t.lastname+' '+t.firstname as client,  
  
   -- GETTING MAX. OF TICKET NUMBER FROM UNDERLYING VIOLATIONS.......  
   RefCaseNumber = (  
    select top 1 refcasenumber from tblticketsviolations v  
    where v.ticketid_pk = t.ticketid_pk order by ticketsviolationid  
    ),  
  
   -- GETTING PAID AMOUNT RELATED TO THE CASE.........  
   Amount = (   
    select  isnull(sum(isnull(p.chargeamount,0)),0) from tblticketspayment p  
    where p.ticketid = t.ticketid_pk   
    and p.paymenttype not in (99,100)  
    and p.paymentvoid = 0  
    )  
  
  -- FROM TEMP. TABLE AND TBLTICKETS BY RELATING THE MID NUMBER, COURT ID,   
  -- UNDERLYING TICKET NUMBER. AND PROFILE CREATE DATE SHOULD BE AT LEAST 3 DAYS  
  -- GREATER THAN THE MAILER DATE.....  
  FROM @Archive ta   
  left OUTER JOIN  
   dbo.tblTickets t   
  ON t.ticketid_pk in (  
    select min(b.ticketid_pk) from tbltickets b inner join tblticketsviolations v  
    on b.ticketid_pk = v.ticketid_pk  
    where b.Midnum = ta.MidNumber   
    AND  V.CourtId = ta.CourtID   
    and datediff(day,ta.maildatefta_three,b.recdate) >=3   
    and v.refcasenumber = ta.ticketnumber  
    )  
  
  -- FILTERING RECORDS FOR HIRED CLIENTS ONLY.........  
  where t.activeflag =1  
  order by  
   [client], t.ticketid_pk  
  
 end  
  
  
-- ELSE IF DETAILS OF QUOTED CLIENTS REQUIRED..........  
else if @DetailType = 'quote'  
 begin  
  SELECT  t.TicketID_PK,   
   t.lastname+' '+t.firstname as client,  
  
   -- GETTING MAX. OF TICKET NUMBER FROM UNDERLYING VIOLATIONS.......  
   RefCaseNumber = (  
    select top 1 refcasenumber from tblticketsviolations v  
    where v.ticketid_pk = t.ticketid_pk order by ticketsviolationid  
    ),  
  
   -- GETTING QUOTE AMOUNT RELATED TO THE CASE.........  
   t.calculatedtotalfee as Amount   
  
  -- FROM TEMP. TABLE AND TBLTICKETS BY RELATING THE MID NUMBER, COURT ID,   
  -- UNDERLYING TICKET NUMBER. AND PROFILE CREATE DATE SHOULD BE AT LEAST 3 DAYS  
  -- GREATER THAN THE MAILER DATE.....  
  FROM @Archive ta   
  left OUTER JOIN  
   dbo.tblTickets t   
  ON t.ticketid_pk in (  
    select min(b.ticketid_pk) from tbltickets b inner join tblticketsviolations v  
    on b.ticketid_pk = v.ticketid_pk  
    where b.Midnum = ta.MidNumber   
    AND  V.CourtId = ta.CourtID   
    and datediff(day,ta.maildatefta_three,b.recdate) >=3   
    and v.refcasenumber = ta.ticketnumber      
    )  
  
  -- FILTERING RECORDS FOR QUOTED CLIENTS ONLY.........  
  where t.activeflag =0  
  order by  
   [client], t.ticketid_pk  
  
 end  
go 
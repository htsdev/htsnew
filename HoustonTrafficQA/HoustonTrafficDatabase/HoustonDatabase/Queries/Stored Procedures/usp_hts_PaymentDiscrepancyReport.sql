﻿/*
* Modified by: Ozair
* Task ID : 7791
* Business logic : This procedure is used to get all cases having payment discrepencies
* Parameter :     
*	@psdate
*	@pedate  
*	@showall
* */  
  
ALTER PROCEDURE [dbo].[usp_hts_PaymentDiscrepancyReport]
	@psdate DATETIME,
	@pedate DATETIME,
	@showall INT
AS
	SELECT DISTINCT 
	       
	       t.ticketid_pk,
	       (
	           SELECT TOP 1 tv.refcasenumber
	           FROM   tblticketsviolations tv
	           WHERE  tv.ticketid_pk = t.ticketid_pk
	       ) AS TicketNo,
	       t.Firstname,
	       t.Lastname,
	       (CONVERT(VARCHAR(12), tp.recdate, 101)) AS recdate,
	       tp.recdate AS TransactionDate,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact1), '')
	       ) + '(' + ISNULL(LEFT(tblContactstype_1.Description, 1), '') + ')' AS 
	       contact1,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact2), '')
	       ) + '(' + ISNULL(LEFT(dbo.tblContactstype.Description, 1), '') + ')' AS 
	       contact2,
	       CONVERT(
	           VARCHAR(20),
	           ISNULL(dbo.formatphonenumbers(T.Contact3), '')
	       ) + '(' + ISNULL(LEFT(tblContactstype_2.Description, 1), '') + ')' AS 
	       contact3,
	       '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='
	       + CONVERT(VARCHAR(30), t.ticketid_pk) + '" >' + CONVERT(VARCHAR(30), ROW_NUMBER() OVER(ORDER BY t.ticketid_pk ASC))
	       + '</a>' AS link
	FROM   tbltickets t
	       LEFT OUTER JOIN dbo.tblContactstype AS tblContactstype_2
	            ON  t.ContactType3 = tblContactstype_2.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype
	            ON  t.ContactType2 = dbo.tblContactstype.ContactType_PK
	       LEFT OUTER JOIN dbo.tblContactstype AS tblContactstype_1
	            ON  t.ContactType1 = tblContactstype_1.ContactType_PK
	       INNER JOIN tblticketspayment tp
	            ON  t.ticketid_pk = tp.ticketid
	            AND tp.recdate = (
	                    SELECT MAX(recdate)
	                    FROM   tblticketspayment
	                    WHERE  ticketid = tp.ticketid
	                )
	WHERE  --Ozair 7791 07/26/2010  where clause optimized    
	       tp.InvoiceNumber_PK = (
	           SELECT TOP 1 invoicenumber_pk
	           FROM   tblTicketsPayment
	           WHERE  ticketid = t.TicketID_PK
	       ) 
	       --Sabir Khan 5391 01/09/2009 Sum of paid amount not equal to zero and also remove duplications of records...
	       AND tp.ticketid IN (SELECT ticketid
	                           FROM   tblTicketsPayment
	                           GROUP BY
	                                  ticketid
	                           HAVING ISNULL(SUM(ISNULL(chargeamount, 0)), 0) <>
	                                  0)
	       AND (
	               (@showall = 1)
	               OR (
	                      (DATEDIFF(DAY, @psdate, tp.recdate) > 0)
	                      AND (DATEDIFF(DAY, @pedate, tp.recdate) < 0)
	                  )
	           )
	       AND tp.invoicenumber_pk > 0
	       AND tp.paymentvoid <> 1
	       AND t.activeflag <> 1            
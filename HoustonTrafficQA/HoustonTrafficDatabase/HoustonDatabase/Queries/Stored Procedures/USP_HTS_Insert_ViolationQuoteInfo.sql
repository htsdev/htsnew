SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_ViolationQuoteInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_ViolationQuoteInfo]
GO










CREATE procedure USP_HTS_Insert_ViolationQuoteInfo      
 (      
@TicketID_FK int,      
@QuoteResultID int,  
@QuoteResultDate varchar(12),      
@FollowUPID int,      
@FollowUPDate varchar(12),      
@FollowUpYN int,      
@QuoteComments varchar (1000),      
@CallBackDate datetime,      
@AppointmentDate datetime,      
@AppointmentTime varchar (12)      
)      
      
      
      
as      
      
      
insert into tblViolationQuote      
 (      
      
TicketID_FK,      
QuoteResultID,  
QuoteResultDate,      
FollowUPID,      
FollowUPDate,      
FollowUpYN,      
QuoteComments,      
CallBackDate,      
AppointmentDate,      
AppointmentTime      
      
 )      
values      
 (      
@TicketID_FK,      
@QuoteResultID,  
@QuoteResultDate,      
@FollowUPID,      
@FollowUPDate,      
@FollowUpYN,      
@QuoteComments,      
@CallBackDate,      
@AppointmentDate,      
@AppointmentTime      
)  
  







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_MANUALLYSIGNED_CLIENTS_DAILY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_MANUALLYSIGNED_CLIENTS_DAILY]
GO

      
-- USP_HTS_GET_MANUALLYSIGNED_CLIENTS_DAILY 6,'09/14/2007','09/18/2007',2        
CREATE procedure [dbo].[USP_HTS_GET_MANUALLYSIGNED_CLIENTS_DAILY]          
 (          
 @CatNum  int,                          
 @sDate  datetime,                          
 @eDate   datetime,        
 @DataBase int          
 )          
          
as          
          
set nocount on         
          
declare @tblcourts table (courtid int)          
          
----------------------------------------------------------------------------------------------------          
--  DALLAS DATABASE....          
----------------------------------------------------------------------------------------------------          
if @DataBase = 2          
 if @catnum < 1000          
  insert into @tblcourts           
  select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = @catnum          
 else          
  insert into @tblcourts select @catnum          
          
          
----------------------------------------------------------------------------------------------------          
--  TRAFFIC TICKETS DATABASE....          
----------------------------------------------------------------------------------------------------          
if @DataBase =1        
 if @catnum < 1000          
  insert into @tblcourts           
  select courtid from dbo.tblcourts where courtcategorynum = @catnum          
 else          
  insert into @tblcourts select @catnum          
----------------------------------------------------------------------------------------------------            
          
-- GETTING INFORMATION FROM CLIENTS/QUOTES......          
declare @QuoteClient table (          
 ticketid int,           
 activeflag int,          
 hiredate datetime,          
 quotedate datetime,          
 chargeamount money           
 )          
          
if @DataBase = 2          
 insert into @QuoteClient          
 select         
 t.ticketid_pk,         
 t.activeflag,           
 min(p.recdate),         
 t.recdate,         
 isnull(sum(isnull(p.chargeamount,0)),0)          
 from dallastraffictickets.dbo.tbltickets t           
 left outer join dallastraffictickets.dbo.tblticketspayment p           
 on p.ticketid = t.ticketid_pk and p.paymentvoid = 0        
 inner join dallastraffictickets.dbo.tblticketsviolations tv on tv.ticketid_pk=t.ticketid_pk         
 inner join @tblcourts c on c.courtid=tv.courtid        
 group by t.ticketid_pk, t.activeflag, t.recdate, t.MailerID,p.recdate       
 having datediff(day, p.recdate, @sdate)<= 0          
 and datediff(day, p.recdate, @edate)>= 0         
 and isnull(t.MailerID,0)=0
else          
 insert into @QuoteClient          
 select         
 t.ticketid_pk,        
 t.activeflag,           
 min(p.recdate),         
 t.recdate,         
 isnull(sum(isnull(p.chargeamount,0)),0)          
 from tbltickets t left outer join tblticketspayment p           
 on p.ticketid = t.ticketid_pk and p.paymentvoid = 0         
 inner join tblticketsviolations tv on tv.ticketid_pk=t.ticketid_pk         
 inner join @tblcourts c on c.courtid=tv.courtid        
 group by t.ticketid_pk, t.activeflag, t.recdate, t.MailerID,p.recdate        
 having datediff(day, p.recdate, @sdate)<= 0          
 and datediff(day, p.recdate, @edate)>= 0          
 and isnull(t.MailerID,0)=0
         
        
        
---- SUMMARIZING THE INFORMATION....          
declare @temp2 table          
  (        
  recdate datetime,        
  nacount int,          
  Client  int,          
  Quote  int,          
  Revenue  money,          
  expense money          
  )          
          
INSERT into @temp2 (recdate,nacount,client, Quote, revenue, expense)          
select        
 convert(varchar,q.hiredate,101),        
 count(distinct q.ticketid),         
 sum(case q.activeflag when 1 then 1 end),           
 sum(case q.activeflag when 0 then 1 end),            
sum(q.chargeamount),        
 count(distinct q.ticketid)* 0          
from @QuoteClient q         
group by convert(varchar,q.hiredate,101)        
        
          
select recdate,        
  nacount,         
  isnull(client,0) as client,          
  isnull(quote,0) as quote,          
  isnull(revenue,0) as revenue,          
  expense,          
  isnull(revenue,0) - expense as profit ,          
  NPM = convert(numeric(10,3),case when isnull(revenue,0) = 0 then 0 else (isnull(revenue,0) - expense)/revenue end) ,          
  ROP = convert(numeric(10,3),case when isnull(expense,0) = 0 then 0 else (isnull(revenue,0) - expense)/expense end),          
  QPMP = convert(numeric(10,3),cast(isnull(quote,0)/cast(nacount as float) as float)),          
  HPMP = convert(numeric(10,3),cast(isnull(client,0)/cast(nacount as float) as float)),          
  PPMP = convert(numeric(10,3),(isnull(revenue,0) - expense)/cast(nacount as float))          
from @temp2          
order by recdate desc 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


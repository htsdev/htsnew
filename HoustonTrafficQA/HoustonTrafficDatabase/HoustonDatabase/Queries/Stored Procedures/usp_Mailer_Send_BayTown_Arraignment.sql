
/****** 
Created by:		Rab Nawaz Khan
Task	  :		10436
Date	  :		09/27/2012
Business Logic:	The procedure is used by LMS application to get data for Baytown Appearance letter
				AND to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, IF printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category IF printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee id for the currently logged in user
	@PrintType:		Flag to just preview the letter or to mark letters AS sent. IF 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies IF searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	IF @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	CourtDate:		Court date associated with the violation
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber:	Case number for the violation
	ZipCode:		Person's Home Zip code.
	ZipMid:			First five characters  of zip code & midnumber.....
	Abb:			Short abbreviation of employee who is printing the letter

******/

-- usp_Mailer_Send_BayTown_Arraignment 36, 135,36, '9/20/2012,' ,3992,0,0,0
ALTER PROCEDURE [dbo].[usp_Mailer_Send_BayTown_Arraignment]
@catnum			INT	= 36,                                    
@LetterType		INT	= 135,                                    
@crtid			INT = 36,                                    
@startListdate	VARCHAR (500) ='01/04/2006',                                    
@empid			INT = 3992,                              
@printtype		INT = 0 ,
@searchtype		INT,
@isprinted		bit                             
                                    
                                    
AS                                    
                              
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....            
DECLARE @officernum VARCHAR(50),                                    
@officeropr VARCHAR(50),                                    
@tikcetnumberopr VARCHAR(50),                                    
@ticket_no VARCHAR(50),                                                                    
@zipcode VARCHAR(50),                                             
@zipcodeopr VARCHAR(50),                                    
                                    
@fineamount MONEY,                                            
@fineamountOpr VARCHAR(50) ,                                            
@fineamountRelop VARCHAR(50),                   
              
@singleviolation MONEY,                                            
@singlevoilationOpr VARCHAR(50) ,                                            
@singleviolationrelop VARCHAR(50),                                    
                                    
@doubleviolation MONEY,                                            
@doublevoilationOpr VARCHAR(50) ,                                            
@doubleviolationrelop VARCHAR(50),                                  
@count_Letters INT,                  
@violadesc VARCHAR(500),                  
@violaop VARCHAR(50)                                     
                                   
-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL... 
DECLARE @sqlquery VARCHAR(MAX), @sqlquery2 VARCHAR(MAX),
@lettername VARCHAR(50) 
SET @lettername = 'ARRAIGNMENT' 
SELECT @sqlquery =''  , @sqlquery2 = ''

                                  
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....          
SELECT @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
FROM tblmailer_letters_to_sendfilters                                    
WHERE courtcategorynum=@catnum                                    
AND Lettertype=@LetterType                                  


-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
DECLARE @user VARCHAR(10)
SELECT @user = UPPER(abbreviation) FROM tblusers WHERE employeeid = @empid

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...                                    
SET @sqlquery=@sqlquery+'                                    
SELECT	DISTINCT ta.recordid, 
	CONVERT(VARCHAR(10),ta.listdate,101) AS listdate, 
	tva.courtdate,
	ta.courtid, 
	flag1 = ISNULL(ta.Flag1,''N'') ,
	ta.officerNumber_Fk, 
	tva.ticketnumber_pk AS ticketnumber,
	ta.donotmailflag,
	ta.clientflag,
	UPPER(firstname) AS firstname,
	UPPER(lastname) AS lastname,
	UPPER(address1) + '''' + ISNULL(ta.address2,'''') AS address,                      
	UPPER(ta.city) AS city,
	s.state,
	zipcode,
	LEFT(zipcode,5) + rtrim(ltrim(midnumber)) AS zipmid,
	midnumber,
	dp2 AS dptwo,
	dpc, 
	violationdate,
	violationstatusid,	
	ViolationDescription, 
	ISNULL(tva.fineamount,0) AS FineAmount, 
	COUNT(tva.ViolationNumber_PK) AS ViolCount,
	SUM(ISNULL(tva.fineamount,0))AS Total_Fineamount 
INTO #MainBayTownArrQuery 
FROM dbo.tblTicketsViolationsArchive tva WITH(NOLOCK) 
INNER JOIN dbo.tblTicketsArchive ta WITH(NOLOCK) ON tva.RecordID = ta.RecordID
INNER JOIN tblstate S WITH(NOLOCK) ON ta.stateid_fk = S.stateID
INNER JOIN dbo.tblCourtViolationStatus tcvs WITH(NOLOCK) ON tva.violationstatusid = tcvs.CourtViolationStatusID

-- First Name and Last Name Should not be Empty. . . 
WHERE ISNULL(ta.FirstName, '''') <> ''''
AND ISNULL(ta.LastName, '''') <> ''''

-- ONLY VERIFIED AND VALID ADDRESSES
AND Flag1 in (''Y'',''D'',''S'')

-- SHOULD BE NON-CLIENT
AND donotmailflag = 0 

-- NOT MARKED AS STOP MAILING...
AND clientflag = 0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
AND ISNULL(ta.ncoa48flag,0) = 0 

--Only Appearance status
AND tcvs.CategoryID=2 

-- only future court date
AND datediff(day, tva.courtdate, getdate()) < 0
'
          
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY....
IF( @crtid = @catnum)
	SET @sqlquery=@sqlquery+' 
	AND tva.CourtLocation In (SELECT courtid FROM tblcourts WHERE courtcategorynum = '+CONVERT(VARCHAR(10), @crtid)+' )'

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE
	SET @sqlquery =@sqlquery +'                                  
	AND tva.CourtLocation =  ' +CONVERT(VARCHAR(10), @crtid)                                 

-- FOR THE SELECTED DATE RANGE ONLY.....
IF(@startListdate<>'')                                    
	SET @sqlquery=@sqlquery+'
	AND ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 


-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                    
IF(@officernum<>'')                                    
	SET @sqlquery =@sqlquery +'  
	AND 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
              
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
IF(@ticket_no<>'')                                    
	SET @sqlquery=@sqlquery+ '
	AND  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
IF(@zipcode<>'')                                                          
	SET @sqlquery=@sqlquery+ ' 
	AND	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'LEFT(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'               

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION
IF(@violadesc<>'')              
	SET @sqlquery=@sqlquery+' 
	AND	('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'           

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
IF(@fineamount<>0 AND @fineamountOpr<>'not'  )              
	SET @sqlquery =@sqlquery+ '  
	AND  tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                   

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
IF(@fineamount<>0 AND @fineamountOpr  ='not'  )              
	SET @sqlquery =@sqlquery+ '
    AND not tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                   

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED.....
SET @sqlquery =@sqlquery+ '  

GROUP BY  
	ta.recordid, CONVERT(VARCHAR(10),ta.listdate,101), tva.courtdate, ta.courtid, ta.Flag1, ta.officerNumber_Fk,
	tva.TicketNumber_PK, ta.donotmailflag, ta.clientflag, UPPER(firstname) , UPPER(lastname) , UPPER(address1) + '''' + ISNULL(ta.address2,'''') ,
	UPPER(address1), ISNULL(ta.address2,''''), UPPER(ta.city), s.state,zipcode,LEFT(zipcode,5) + rtrim(ltrim(midnumber)), midnumber, dp2,
	dpc, violationstatusid,	violationdate, ViolationDescription, FineAmount 

-- Delete Already Sended Letters of Arraignment or and other Letter of Baytown Court sended in last 5 days. . . 
DELETE FROM #MainBayTownArrQuery
FROM #MainBayTownArrQuery a INNER JOIN tblletternotes n WITH(NOLOCK) ON n.RecordID = a.recordid 
INNER JOIN tblletter l WITH(NOLOCK) ON l.LetterID_PK = n.LetterType 
WHERE 	lettertype ='+CONVERT(VARCHAR(10),@LetterType)+'  
or (
		l.courtcategory = 36
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
	)
-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
SELECT DISTINCT recordid,courtdate, violationstatusid,ViolCount,Total_Fineamount,  listdate AS mdate,FirstName,LastName,address,                              
city,state,FineAmount,violationdescription,dpc,dptwo,Flag1,officerNumber_Fk,ticketnumber,midnumber,                                     
courtid,zipmid,donotmailflag,clientflag, zipcode INTO #FinalQueryForArrRecs  FROM #MainBayTownArrQuery  WHERE 1=1                                  
'              

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
IF(@singleviolation<>0 AND @doubleviolation=0)              
	SET @sqlquery =@sqlquery+ '
	AND'+ @singlevoilationOpr+'  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND ViolCount=1) or violcount>3
    '              

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
IF(@doubleviolation<>0 AND @singleviolation=0 )              
	SET @sqlquery =@sqlquery + '
	AND'+ @doublevoilationOpr+'   (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND ViolCount=2) or violcount>3  
	'

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER... 
IF(@doubleviolation<>0 AND @singleviolation<>0)              
	SET @sqlquery =@sqlquery+ '
	AND '+ @singlevoilationOpr+'  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND ViolCount=1)
	or' +  @doublevoilationOpr+'  (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND ViolCount=2) or violcount>3 ' 
	
SET @sqlquery2 =@sqlquery2 + '
 	
 -- Hide the entire columm in mailer IF fine amount is zero agains any violation of a recored . . . 
 ALTER TABLE #FinalQueryForArrRecs 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 SELECT recordid INTO #fine FROM #FinalQueryForArrRecs   WHERE ISNULL(FineAmount,0) = 0  
 
 UPDATE t   
 SET t.isfineAmount = 0  
 FROM #FinalQueryForArrRecs t INNER JOIN #fine f on t.recordid = f.recordid   
	
	'
             
-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....         
IF( @printtype = 1)    
	begin     
		SET @sqlquery2 =@sqlquery2 +                                    
		'
		DECLARE @ListdateVal DateTime,                                  
		@totalrecs INT,                                
		@COUNT INT,                                
		@p_EachLetter MONEY,                              
		@recordid VARCHAR(50),                              
		@zipcode   VARCHAR(12),                              
		@maxbatch INT  ,
		@lCourtId INT  ,
		@dptwo VARCHAR(10),
		@dpc VARCHAR(10)
		DECLARE @tempBatchIDs table (batchid INT)

		-- GETTING TOTAL LETTERS AND COSTING ......
		SELECT @totalrecs =COUNT(*) FROM #FinalQueryForArrRecs WHERE mdate = @ListdateVal  
		SET @p_EachLetter=CONVERT(MONEY,0.45) 


		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		DECLARE ListdateCur Cursor for                                  
		SELECT DISTINCT mdate FROM #FinalQueryForArrRecs                                
		open ListdateCur                                   
		Fetch Next FROM ListdateCur INTO @ListdateVal                                                      
		while (@@Fetch_Status=0)                              
			begin                                

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE...
				SELECT @totalrecs =COUNT(DISTINCT recordid) FROM #FinalQueryForArrRecs WHERE mdate = @ListdateVal                                 

				-- INSERTING RECORD IN BATCH TABLE......
				insert INTO tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
				( '+CONVERT(VARCHAR(10), @empid)+'  , '+CONVERT(VARCHAR(10),@lettertype)+' , @ListdateVal, '+CONVERT(VARCHAR(10),@crtid)+' , 0, @totalrecs, @p_EachLetter)                                   


				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				SELECT @maxbatch=MAX(BatchId) FROM tblBatchLetter  
				insert INTO @tempBatchIDs SELECT @maxbatch                                                     

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				DECLARE RecordidCur Cursor for                               
				SELECT DISTINCT recordid,zipcode, courtid, dptwo, dpc FROM #FinalQueryForArrRecs WHERE mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next FROM  RecordidCur INTO @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
					begin                              

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert INTO tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+CONVERT(VARCHAR(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next FROM  RecordidCur INTO @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
				Fetch Next FROM ListdateCur INTO @ListdateVal                              
			end                                            

		close ListdateCur  
		deallocate ListdateCur 

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....		 
		SELECT DISTINCT CONVERT(VARCHAR(20),n.noteid) AS letterid, t.recordid,t.courtdate, t.FirstName,                              
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription, t.dpc, 
		dptwo, t.ticketnumber AS TicketNumber_PK, t.zipcode,t.zipmid ,'''+@user+''' AS Abb , isfineamount,
		''Baytown'' AS CourtName, '''+@lettername +''' AS lettername, 0 AS promotemplate,  t.midnumber, t.midnumber AS midnum, t.courtid,
		CAST(0 AS BIT) AS isSpanish 
		FROM #FinalQueryForArrRecs t , tblletternotes n, @tempBatchIDs tb
		WHERE t.recordid = n.recordid AND t.courtid = n.courtid AND n.batchid_fk = tb.batchid AND n.lettertype = '+CONVERT(VARCHAR(10),@lettertype)+' 
		order by T.zipmid


		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........		
		DECLARE @lettercount INT, @subject VARCHAR(200), @body VARCHAR(400) , @sql VARCHAR(500)
		SELECT @lettercount = COUNT(DISTINCT n.noteid) FROM tblletternotes n, @tempBatchIDs b WHERE n.batchid_fk = b.batchid
		SELECT @subject = CONVERT(VARCHAR(20), @lettercount) +  '' Baytown Municipal Court Arraignment Letters Printed''
		SELECT @body = CONVERT(VARCHAR(20), @lettercount) + '' LETTERS ('' + CONVERT(VARCHAR(12), GETDATE()) + '') - ''+ UPPER( '''+@user+''')
		exec usp_mailer_send_Email @subject, @body


		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		DECLARE @batchIDs_filname VARCHAR(500)
		SET @batchIDs_filname = ''''
		SELECT @batchIDs_filname = @batchIDs_filname + CONVERT(VARCHAR,batchid) + '',''  FROM @tempBatchIDs
		SELECT ISNULL(@batchIDs_filname,'''') AS batchid

		'
	end


else IF( @printtype = 0) 

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	begin
		SET @sqlquery2 = @sqlquery2 + '
		SELECT DISTINCT ''NOT PRINTABLE'' AS letterId,  recordid,courtdate, FirstName,                              
		LastName,address,city, state,FineAmount,violationdescription,dpc, 
		dptwo, ticketnumber AS TicketNumber_PK, zipcode,zipmid,'''+@user+''' AS Abb , isfineamount ,
		''Sugarland'' AS CourtName, '''+@lettername +''' AS lettername, 0 AS promotemplate,  midnumber, midnumber AS midnum, courtid,
		CAST(0 AS BIT) AS isSpanish
		FROM #FinalQueryForArrRecs 
		order by zipmid
		'
	end

-- DROPPING TEMPORARY TABLES ....
SET @sqlquery2 = @sqlquery2 + '

drop table #MainBayTownArrQuery 
drop table #FinalQueryForArrRecs'
                                    
--print @sqlquery + @sqlquery2

-- CONCATENATING THE DYNAMIC SQL...
SET @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)

GO
GRANT EXECUTE ON usp_Mailer_Send_BayTown_Arraignment TO webuser
GO
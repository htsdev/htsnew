/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to update all event state
				also a note in auto dialer setting history is made. 
				
				
List of Parameters:
	@EventState:
	@EmployeeID:

	
*******/

CREATE procedure dbo.usp_AD_Update_AllEventState

@EventState	bit, 
@EmployeeID	int

as

declare @Notes varchar(500)

set @Notes=''

update AutoDialerEventConfigSettings
set EventState=@EventState,
	LastUpdateDateTime=GetDate(),
	LastUpdatedBy=@EmployeeID

set @Notes='Event State Set to '+case when @EventState=1 then '"Start"' else '"Stop"' end 

insert into AutoDialerConfigSettingsHistory  
(  
EventTypeID_Fk,  
Notes,  
EmployeeID  
)  
(  
select eventtypeid,@Notes,@EmployeeID from AutoDialerEventConfigSettings
)  

go

grant exec on dbo.usp_AD_Update_AllEventState to dbr_webuser
go
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/*
* This stored procedure is used to get delivery information of letter. 
*/

ALTER procedure [dbo].[usp_MailerUSPS_Get_LetterInfo]
@LetterID int
as

SELECT t.LetterID,
       t.ZipCode,
       t.DP2,
       t.BarCodeID,
       t.ServiceTypeID,
       t.ConfirmServiceSubscriberID,
       t.FCLT_ID,
       t.OperationCode,
       t.OperationDateTime 
       INTO #temp
FROM   mailerUSPSInfo.dbo.uspspackagedata t
where Convert(int,t.letterid) = @LetterID --Sabir Khan 8679 12/20/2010 convert into int for comparing...
GROUP BY
       t.LetterID,
       t.ZipCode,
       t.DP2,
       t.BarCodeID,
       t.ServiceTypeID,
       t.ConfirmServiceSubscriberID,
       t.FCLT_ID,
       t.OperationCode,
       t.OperationDateTime
ORDER BY
       t.LetterID,
       t.OperationDateTime
        

--Sabir Khan 8679 12/20/2010 Get site ID based on letter id
declare @DbInfo int
select @DbInfo = tc.LocationId_fk from tblcourtcategories tc inner join tblletter l on l.courtcategory = tc.courtcategorynum
inner join tblletternotes tn on tn.lettertype = l.letterID_pk
where tn.noteid = @LetterID

if @DbInfo =1
begin
SELECT row_number() over(order by t.letterid,t.operationdatetime) as SNo,t.letterid,tta.lastname, tta.firstname,traffictickets.dbo.fn_hts_get_TicketNumberCommasForRecID(tln.recordid) as TicketNumber,
t.zipcode,t.dp2,t.barcodeid,t.servicetypeid,t.confirmservicesubscriberID,t.FCLT_ID,u.FCLT_Name,u.Area_ID,u2.AreaName,
u.District,u3.DistrictName,u.FCLT_Type,t.operationDatetime,t.operationcode,o.operationdescription,o.mailtype,
ot.operationcodetypeName+' - '+ ot.operationcodetypedescription as OperationCodeType,
--case when convert(int,t.operationcode)=93 then 'Returned' 
case when convert(int,t.operationcode)BETWEEN 46 AND 99 then 'Returned' 
else case when convert(int,t.operationcode) in (905,911,913,915,919) then 'Delivered' else 'Pending' end
 end as DeliveredStatus
into #temp2
FROM   #temp t
       INNER JOIN mailerUSPSInfo.dbo.USPSFCLT u
            ON  t.FCLT_ID = u.FCLT_ID inner join TrafficTickets.dbo.tblLetterNotes tln
on  CONVERT(INT, t.letterid) = tln.NoteId inner join TrafficTickets.dbo.tblticketsarchive tta 
on tln.recordid=tta.recordid inner join mailerUSPSInfo.dbo.uspsoperationcode o 
on t.operationcode=o.operationcode inner join mailerUSPSInfo.dbo.uspsoperationcodetype ot
on o.operationcodetypeID_FK=ot.operationcodetypeID INNER JOIN mailerUSPSInfo.dbo.USPSArea u2
ON u.AREA_ID=u2.AreaID INNER JOIN mailerUSPSInfo.dbo.USPSDistrict u3
ON u.DISTRICT=u3.DistrictID
order by t.letterid,t.operationdatetime
END

if @DbInfo =2
begin
SELECT row_number() over(order by t.letterid,t.operationdatetime) as SNo,t.letterid,tta.lastname, tta.firstname,traffictickets.dbo.fn_hts_get_TicketNumberCommasForRecID(tln.recordid) as TicketNumber,
t.zipcode,t.dp2,t.barcodeid,t.servicetypeid,t.confirmservicesubscriberID,t.FCLT_ID,u.FCLT_Name,u.Area_ID,u2.AreaName,
u.District,u3.DistrictName,u.FCLT_Type,t.operationDatetime,t.operationcode,o.operationdescription,o.mailtype,
ot.operationcodetypeName+' - '+ ot.operationcodetypedescription as OperationCodeType,
--case when convert(int,t.operationcode)=93 then 'Returned' 
case when convert(int,t.operationcode)BETWEEN 46 AND 99 then 'Returned' 
else case when convert(int,t.operationcode) in (905,911,913,915,919) then 'Delivered' else 'Pending' end
 end as DeliveredStatus
into #tempdallas
FROM   #temp t
       INNER JOIN mailerUSPSInfo.dbo.USPSFCLT u
            ON  t.FCLT_ID = u.FCLT_ID inner join TrafficTickets.dbo.tblLetterNotes tln
on  CONVERT(INT, t.letterid) = tln.NoteId inner join DallasTrafficTickets.dbo.tblticketsarchive tta 
on tln.recordid=tta.recordid inner join mailerUSPSInfo.dbo.uspsoperationcode o 
on t.operationcode=o.operationcode inner join mailerUSPSInfo.dbo.uspsoperationcodetype ot
on o.operationcodetypeID_FK=ot.operationcodetypeID INNER JOIN mailerUSPSInfo.dbo.USPSArea u2
ON u.AREA_ID=u2.AreaID INNER JOIN mailerUSPSInfo.dbo.USPSDistrict u3
ON u.DISTRICT=u3.DistrictID
order by t.letterid,t.operationdatetime
end

drop table #temp



declare @table table
(
SNo varchar(10),
LetterID	varchar(9),
lastname varchar(50), 
firstname varchar(50),
TicketNumber varchar(max),
ZipCode	varchar(9),
DP2	varchar(2),
BarCodeID	varchar(2),
ServiceTypeID	varchar(3),
ConfirmServiceSubscriberID	varchar(6),
FCLT_ID	varchar(5),
FCLT_Name varchar(100),
Area_ID varchar(5),
Area_name varchar(100),
District varchar(3),
District_name varchar(100),
FCLT_Type varchar(3),
OperationDateTime	datetime,
OperationCode	varchar(5)	,
operationdescription varchar(200),
mailtype varchar(100),
OperationCodeType varchar(100),
DeliveredStatus varchar(50)	
)

declare @rowcount int
declare @rowno	int
declare @LID varchar(9)
declare @GroupCount int
set @GroupCount =1
set @rowno = 1
--Sabir Khan 8679 12/20/2010 For Houston mailers...
if @DbInfo = 1
begin
select @rowcount=count(letterid) from #temp2

while(@rowno<=@rowcount)
begin
	if(@rowno=1)
	begin
		select @LID=letterid from #temp2 where SNo=@rowno
		insert into @table
		
			Select @GroupCount, letterid,lastname, firstname,TicketNumber,
			zipcode,dp2,barcodeid,servicetypeid,confirmservicesubscriberID,FCLT_ID,FCLT_Name,Area_ID,Areaname,
			District,Districtname,FCLT_Type,operationDatetime,operationcode,operationdescription,mailtype,
			OperationCodeType,
			DeliveredStatus
			from #temp2
			where SNo=@rowno
		
	end
	else
	begin
		if @LID<>(select letterid from #temp2 where SNo=@rowno)
		begin
			set @GroupCount=@GroupCount+1
			insert into @table
			
				Select @GroupCount, letterid,lastname, firstname,TicketNumber,
				zipcode,dp2,barcodeid,servicetypeid,confirmservicesubscriberID,FCLT_ID,FCLT_Name,Area_ID,Areaname,
				District,Districtname,FCLT_Type,operationDatetime,operationcode,operationdescription,mailtype,
				OperationCodeType,
				DeliveredStatus
				from #temp2
				where SNo=@rowno
			
			select @LID=letterid from #temp2 where SNo=@rowno
		end
		else
		begin
			insert into @table
				Select '', letterid,lastname, firstname,TicketNumber,
				zipcode,dp2,barcodeid,servicetypeid,confirmservicesubscriberID,FCLT_ID,FCLT_Name,Area_ID,Areaname,
				District,Districtname,FCLT_Type,operationDatetime,operationcode,operationdescription,mailtype,
				OperationCodeType,
				DeliveredStatus
				from #temp2
				where SNo=@rowno					
		end		
	end
	set @rowno=@rowno+1	
end
drop table #temp2
END
--Sabir Khan 8679 12/20/2010 For Dallas Mailers....
if @DbInfo = 2
begin
select @rowcount=count(letterid) from #tempdallas
while(@rowno<=@rowcount)
begin
	if(@rowno=1)
	begin
		select @LID=letterid from #tempdallas where SNo=@rowno
		insert into @table
		
			Select @GroupCount, letterid,lastname, firstname,TicketNumber,
			zipcode,dp2,barcodeid,servicetypeid,confirmservicesubscriberID,FCLT_ID,FCLT_Name,Area_ID,Areaname,
			District,Districtname,FCLT_Type,operationDatetime,operationcode,operationdescription,mailtype,
			OperationCodeType,
			DeliveredStatus
			from #tempdallas
			where SNo=@rowno
		
	end
	else
	begin
		if @LID<>(select letterid from #tempdallas where SNo=@rowno)
		begin
			set @GroupCount=@GroupCount+1
			insert into @table
			
				Select @GroupCount, letterid,lastname, firstname,TicketNumber,
				zipcode,dp2,barcodeid,servicetypeid,confirmservicesubscriberID,FCLT_ID,FCLT_Name,Area_ID,Areaname,
				District,Districtname,FCLT_Type,operationDatetime,operationcode,operationdescription,mailtype,
				OperationCodeType,
				DeliveredStatus
				from #tempdallas
				where SNo=@rowno
			
			select @LID=letterid from #tempdallas where SNo=@rowno
		end
		else
		begin
			insert into @table
				Select '', letterid,lastname, firstname,TicketNumber,
				zipcode,dp2,barcodeid,servicetypeid,confirmservicesubscriberID,FCLT_ID,FCLT_Name,Area_ID,Areaname,
				District,Districtname,FCLT_Type,operationDatetime,operationcode,operationdescription,mailtype,
				OperationCodeType,
				DeliveredStatus
				from #tempdallas
				where SNo=@rowno					
		end		
	end
	set @rowno=@rowno+1	
end
drop table #tempdallas
end


select * from @table




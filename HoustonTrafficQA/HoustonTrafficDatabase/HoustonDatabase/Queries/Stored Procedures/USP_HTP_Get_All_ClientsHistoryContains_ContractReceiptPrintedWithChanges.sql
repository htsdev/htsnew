USE TrafficTickets
-- ================================================
-- Author:		Rab Nawaz Khan
-- Create date: 01/16/2013
-- Task Id: 10672
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Business Logic:	This Procedure is use to get the records of all clients who has "Contract/Receipt Printed with Changes" Text found in there History. . .
-- =============================================
-- USP_HTP_Get_All_ClientsHistoryContains_ContractReceiptPrintedWithChanges 'Contract/Receipt Printed with Changes'
CREATE PROCEDURE USP_HTP_Get_All_ClientsHistoryContains_ContractReceiptPrintedWithChanges 
@Description VARCHAR(1000) = 'Contract/Receipt Printed with Changes'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(v.casenumassignedbycourt, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(v.casenumassignedbycourt, 'N/A')))) ELSE 'N/A' END AS [Cause Number],
	CONVERT(VARCHAR(100), CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(v.RefCaseNumber, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(v.RefCaseNumber, 'N/A')))) ELSE 'N/A' END) AS [Ticket Number],
	UPPER(u.UserName) AS [User Name],
	Convert(varchar(100), ttn.Recdate, 101) + ' @ ' +  RIGHT(CONVERT(VARCHAR, ttn.Recdate, 100), 7)  AS [History Comments RecDate],
	t.Firstname AS [First Name], t.Lastname AS [Last Name], tp.[Description] AS [Payment Type], 
	'$' + CONVERT(VARCHAR(10), CONVERT(MONEY, ISNULL(ttp.ChargeAmount, 0))) AS [Charge Amount]
	--'http://ln.legalhouston.com/clientinfo/CaseHistory.aspx?sMenu=64&casenumber='+CONVERT(VARCHAR(20), t.TicketID_PK)+ '&search=0'
	FROM tblTicketsNotes ttn INNER JOIN tbltickets t ON ttn.TicketID = t.TicketID_PK
		INNER JOIN tblticketsviolations v ON t.TicketID_PK = v.TicketID_PK	
		INNER JOIN tblUsers u ON ttn.EmployeeID = u.EmployeeID
		INNER JOIN tblTicketsPayment ttp ON ttp.TicketID = t.TicketID_PK
		INNER JOIN tblPaymenttype tp ON ttp.PaymentType = tp.Paymenttype_PK
	WHERE ttn.[Subject] LIKE '%' +@Description+'%'
	AND ISNULL(t.Activeflag, 0) = 1
	AND t.Firstname NOT LIKE '%Test%' 
	AND t.Lastname NOT LIKE '%test%'
	ORDER BY t.Firstname, t.Lastname
END
GO
GRANT EXECUTE ON [USP_HTP_Get_All_ClientsHistoryContains_ContractReceiptPrintedWithChanges] TO dbr_webuser
GO

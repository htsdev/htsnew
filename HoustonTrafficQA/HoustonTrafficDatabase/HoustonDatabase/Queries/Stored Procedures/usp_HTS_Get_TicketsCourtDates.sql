SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_TicketsCourtDates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_TicketsCourtDates]
GO

  
  
CREATE PROCEDURE [dbo].[usp_HTS_Get_TicketsCourtDates]        
@TicketID_PK varchar(20)        
AS        
        
SELECT DISTINCT         
 dbo.tblTicketsViolations.TicketID_PK,       
 dbo.tblTicketsViolations.CourtViolationStatusIDmain,       
 CONVERT(Varchar(20), dbo.tblTicketsViolations.CourtDateMain,100) + '  ' + dbo.tblCourtViolationStatus.Description AS CourtDatenStatus,    
dbo.tblTicketsViolations.CourtDateMain as CourtDate,    
dbo.tblCourtViolationStatus.Description as ViolationDescription    
FROM dbo.tblTicketsViolations       
LEFT OUTER JOIN        
 dbo.tblCourtViolationStatus       
ON  dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID        
WHERE   dbo.tblTicketsViolations.TicketID_PK = @TicketID_PK        

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


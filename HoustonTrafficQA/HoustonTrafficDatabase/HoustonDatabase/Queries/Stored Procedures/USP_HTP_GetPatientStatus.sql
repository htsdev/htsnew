﻿/******  
* Created By :	  Fahad Muhammad Qureshi.
* Create Date :   10/15/2010 7:22:45 PM
* Task ID :		  8422
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return :     
******/
ALTER PROCEDURE [dbo].[USP_HTP_GetPatientStatus]
AS
	SELECT ps.Id,
	       ps.Description
	FROM   PatientStatus ps
GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetPatientStatus] TO dbr_webuser
GO 	    
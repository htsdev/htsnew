﻿/******   
Created by:  Noufil Khan  
Business Logic : this procedure is used to get the information about client before creating the batch print report. because this information will be use in batch print  
  
List of Parameters:   
 @Type : if 2 then search by docket date, if 1 then search by scan/upload date   
 @DateFrom : start date to search  
 @DateTo : end date to search  
 @AttorneyID : attorney id to whom docket belongs to   
 @IsDCJP : if 1 then search for JP Appeal docket  
  
List of Columns:    
 bookid : id used for viewing updating and deleting records.  
 ScanDate : scan/upload date of docket  
 DocketDate : Docket Date of docket  
 Attorney : Attorney Name    
 DocCount : page count   
 Abbreviation : employee abbreviation  
  
******/    
  
  
  
--[USP_HTP_SetCall_Batch_Letter] 3991,15,'1721343782,729043781,'  
create procedure [dbo].[USP_HTP_SOL_Batch_Letter]   
@empid  int,                  
@LetterType int,                  
@TicketIDList varchar(8000)                  
                  
as                      
begin                      
                 
                    
  
  
  
declare @tickets table (                                                
 ticketid numeric                                               
 )                       
                      
insert into @tickets                                             
 select * from dbo.Sap_String_Param_New(@TicketIDList)                                
               
      
select T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber, T.languageSpeak,  CVS.ShortDescription as Status , C.Address + ', ' + C.City + ', '+S2
.State +' ' + C.Zip as Location , BP.PrintDate , TV.casenumassignedbycourt , v.Description    from
tblHTSBatchPrintLetter   BP                  
join tbltickets T      
on T.TicketID_Pk = BP.Ticketid_FK      
join tblticketsviolations TV      
On T.TicketID_Pk = TV.Ticketid_pk  --and T.TicketID_Pk in (select top 1 ticketId_pk from tblticketsviolations where ticketId_Pk = T.TicketID_Pk)  
join tblviolations v 
on tv.ViolationNumber_PK = v.ViolationNumber_PK  
join tblcourtviolationstatus CVS      
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain      
join tblcourts C      
on C.courtID = TV.CourtID      
join tblState S1       
on S1.Stateid = t.StateID_fk      
join tblState S2      
on  S2.Stateid = C.State      
where       
cast(convert(varchar(12), BP.ticketid_fk) + convert(varchar(12), BP.BatchId_Pk) as numeric)         
        
IN         
        
(select ticketid from @tickets)  
and letterid_fk = @LetterType       
group by   
  
T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, T.languageSpeak,  CVS.ShortDescription , C.Address , C.City ,S2.State,C.Zip,BP.BatchID_pk ,TV.RefcaseNumber,BP.PrintDate , TV.casenumassignedbycourt , v.Description
        
order by BP.BatchID_pk  
end         

go
GRANT EXEC ON [dbo].[USP_HTP_SOL_Batch_Letter] to dbr_webuser
go
  
  
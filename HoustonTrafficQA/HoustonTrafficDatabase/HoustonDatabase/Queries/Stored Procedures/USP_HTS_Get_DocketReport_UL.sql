  
  
 -- USP_HTS_Get_DocketReport_UL '3077', 'OPEN', '5/11/09', '5/18/09',0
ALTER procedure [dbo].[USP_HTS_Get_DocketReport_UL]       
 (      
 @courtlocation varchar(4) = 'NONE',      
 @casestatus varchar(4)= 'NONE',      
 @startcourtdate datetime = null,      
 @endcourtdate datetime = null,
 @firmid int      
 )      
as      

SET NOCOUNT ON ;      
declare @sqlquery nvarchar(4000),      
 @sqlquery1 nvarchar(500),      
 @sqlquery2 nvarchar(500),      
 @strParam  nvarchar(1000)      
      
set @strParam ='      
 @courtlocation varchar(4) ,      
 @casestatus varchar(4) ,      
 @startcourtdate datetime ,      
 @endcourtdate datetime '      
      
-- tahir 5899 5/11/2009 fixed the date range bug...
--if @endcourtdate is not null      
 --set @endcourtdate = @endcourtdate + '23:59:59.998'      
      
-- Agha Usman 2664 06/04/2007      
set @sqlquery = '      
 select t.ticketid_pk,      
  T.lastname,      
  T.firstname,
 --Sabir Khan 5521 02/13/2009 subqeury has been used for chargeamount...
  t.totalfeecharged - (select SUM(ChargeAmount) from tblticketspayment WITH(NOLOCK) where   ticketid = t.ticketid_pk and paymentvoid = 0 group by ticketid) as dueamount,
  (select min(recdate) from tblticketspayment WITH(NOLOCK) where t.ticketid_pk = ticketid and paymentvoid = 0 group by ticketid) as sortentry,
  --Nasir 5716 05/28/2009 for date format string 
  (select convert(varchar,min(recdate),101) from tblticketspayment WITH(NOLOCK) where t.ticketid_pk = ticketid and paymentvoid = 0 group by ticketid) as entry,
  --ISNULL( t.totalfeecharged - SUM( p.ChargeAmount), 0) as dueamount,     --Yasir Kamal 5406 01/16/2009  Owes Column added       
  --min(p.recdate) as entry, 
  -- Sabir Khan 5975 10/22/2009 Removed the fixed length of court name...       
  isnull(C.shortcourtname,''N/A'') as court,   
  --Sabir Khan 5831 04/22/2009 Get most recent payment date...     
  --Nasir 5716 06/04/2009 format time
  (select convert(varchar(10), Max(recdate), 101) from tblticketspayment WITH(NOLOCK) where t.ticketid_pk = ticketid and paymentvoid = 0 group by ticketid)  + ''&nbsp;&nbsp;'' +
  (select dbo.Fn_FormateTime(Max(recdate)) from tblticketspayment WITH(NOLOCK) where t.ticketid_pk = ticketid and paymentvoid = 0 group by ticketid) as paydate,
  (select Max(recdate) from tblticketspayment WITH(NOLOCK) where t.ticketid_pk = ticketid and paymentvoid = 0 group by ticketid) as sortpaydate,
  --null as ContactDate ,      
  --datediff(day, jpfaxdate, getdate()) as datedifference,      
  --null as datedifference,      
  --(convert(varchar(10), isnull(courtdatemain,''01/01/1900''), 101)  + ''&nbsp;&nbsp;'' +     
  --right(convert(varchar(25), isnull(courtdatemain,''01/01/1900''),100),7)  + ''&nbsp;&nbsp;&nbsp;'' +      
  --isnull(courtnumbermain,0))  as courtdatemain, 
  --Nasir 5716 06/04/2009 format date
  dbo.fn_DateFormat(courtdatemain, 24, ''/'','':'',1) +  isnull('' #'' + courtnumbermain,0) as courtdatemain,
  courtdatemain as sortcourtdate,  
  --Sabir Khan 6581 09/16/2009 Getting sub status for criminal courts...    
  CASE WHEN isnull(c.IsCriminalCourt,0) = 1 THEN 
      	    s.ShortDescription
      	ELSE 
      		D.description
      		END AS DESCRIPTION,      
  FirmAbbreviation = ( case when t.CoveringFirmID <> 3000 then FirmAbbreviation      
     else  ''''       
         end       
       ),      
  bondflag = case bondflag when 1 then ''B'' else '''' end,  
  V.ticketofficernumber as officernumber        
      
 FROM    tblTickets T WITH(NOLOCK)       
  LEFT OUTER JOIN      
  tblOfficer O WITH(NOLOCK)       
 ON  officernumber = O.OfficerNumber_PK       
 INNER JOIN      
  tblticketsviolations V WITH(NOLOCK)       
 on  T.ticketid_pk = V.ticketid_pk       
 left outer JOIN    -- tahir 5919 05/18/2009 changed the join clause  
  tblFirm F WITH(NOLOCK)       
 ON  v.CoveringFirmID = F.FirmID
 INNER JOIN      
  tblCourts C WITH(NOLOCK)       
 ON  v.courtid = C.Courtid       
 INNER JOIN       
  tblcourtviolationstatus S WITH(NOLOCK)       
 On  V.courtviolationstatusidmain = S.courtviolationstatusid         
 INNER JOIN     
  tbldatetype D WITH(NOLOCK)       
 on  S.categoryid = D.typeid  '      
  
if @casestatus <> '50'      
 set @sqlquery = @sqlquery + 'and  d.typeid <> 50 '      
        
--------    
set  @sqlquery = @sqlquery + ' where T.activeflag=1 '    
--------      
    
select  @sqlquery1 =       
 case       
  when isnumeric(@courtlocation) = 1  then  'and v.courtid =  @courtlocation '      
  when @courtlocation = 'JP' then 'and v.courtid not in (0,3003,3002,3001) '       
  when @courtlocation = 'IN' then 'and v.courtid in (3003,3002,3001) '
  
  --Yasir Kamal 5025 01/02/2009  �Non HMC/HCJP Cases� under Courts drop down box
  
  WHEN @courtlocation = 'LL' THEN 'and ISNULL(c.courtcategorynum,0)  NOT IN (1,2) '
  
  else  ' and v.courtid  not in (0)'      
 end      
set  @sqlquery = @sqlquery + isnull(@sqlquery1,' ' )      
      
select  @sqlquery1 =       
 case       
  when isnumeric(@casestatus) = 1  then  ' and d.typeid =  @casestatus'      
  when @casestatus = 'WAIT' then ' and d.typeid  in (1,11)'      
--  when @casestatus = 'APP'  then ' and d.typeid  in (3,4,5,9)'      
  when @casestatus = 'APP'  then ' and d.typeid  in (3,4,5)'      
  when @casestatus not in ('WAIT','APP') and isnumeric(@casestatus) <> 1 then ' and d.typeid not in (0,6,7,8,10,50)'      
        
 end      
set  @sqlquery = @sqlquery + isnull(@sqlquery1,' ')      
      
if ( @endcourtdate is not null  and @startcourtdate is not null )      
begin      
 -- tahir 5899 5/11/2009 fixed the date range bug...
 --set @sqlquery1 = ' and (courtdatemain  between @startcourtdate and @endcourtdate)'      
 set  @sqlquery1 = ' and datediff(day, courtdatemain, @startcourtdate) <= 0 and datediff(day, courtdatemain, @endcourtdate) >= 0'
end 
--Sabir Khan 4990 10/18/2008 check for Out side clients
-----------------
if @firmid != 0  
begin  
--Sabir Khan 5215 11/26/2008 
set @sqlquery1 = @sqlquery1 + '  AND F.FirmID = ' + Convert(varchar,@firmid)  
end           
-----------------      
set  @sqlquery = @sqlquery + isnull(@sqlquery1,' ')      
set @sqlquery = @sqlquery + '
 --Sabir Khan 5521 02/16/2009 
 --group by t.ticketid_pk,      
  --T.lastname,      
  --T.firstname,
  --totalfeecharged,      --Yasir Kamal 5406 01/16/2009  Owes Column added
  --C.shortcourtname,      
  
  --isnull(courtdatemain,''01/01/1900'') ,      
  --right(convert(varchar(25), isnull(courtdatemain,''01/01/1900''),100),7) ,      
  --courtnumbermain,       
  --D.description,      
  --FirmAbbreviation,      
  --t.CoveringFirmID,      
  --V.ticketofficernumber,  
  --case bondflag when 1 then ''B'' else '''' end      
      
 --  order by       
--   isnull(courtdatemain,''01/01/1900''),      
--   D.description,      
--   right(convert(varchar(25), isnull(courtdatemain,''01/01/1900''),100),7),      
--   C.shortcourtname,      
--   courtnumbermain,      
--   T.lastname,      
--   T.firstname'      
      
      
set  @sqlquery = @sqlquery + isnull(@sqlquery2,' ')      
--print @sqlquery      
--exec (@sqlquery)      
exec sp_executesql @sqlquery, @strparam, @courtlocation , @casestatus , @startcourtdate  , @endcourtdate      
  
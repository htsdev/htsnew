SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Remove_BondFlagConfirmedFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Remove_BondFlagConfirmedFlag]
GO

create procedure USP_HTS_Remove_BondFlagConfirmedFlag

@Ticketid_pk int

as 

delete from tblticketsflag where ticketid_pk = @Ticketid_pk and flagid = 30
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿ALTER  PROCEDURE USP_HTS_Get_DL_State      
      
 AS      
  
declare @temp table (  
 stateid int,  
 [state] varchar(10)    ,  
 rowid int identity(1,1)  
)
 
 ---- Faique Ali 10235 12/08/2012 avoid select All by specifying selected column..
 
  --SELECT * FROM TrafficTickets.dbo.tblState ts
  
insert into @temp (stateid, [state]) select stateID,[state]from tblstate where stateid = 45  
insert into @temp (stateid, [state]) select stateID,[state]from tblstate where stateid = 14  
insert into @temp (stateid, [state]) select stateID,[state]from tblstate where stateid = 55  
insert into @temp (stateid, [state]) select stateID,[state]from tblstate  where stateid not in (14,45,  55,0) order by [state]  
  
select * from @temp order by rowid 
 
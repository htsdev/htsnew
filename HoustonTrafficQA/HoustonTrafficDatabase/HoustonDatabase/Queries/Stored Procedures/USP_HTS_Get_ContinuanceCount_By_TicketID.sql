SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_ContinuanceCount_By_TicketID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_ContinuanceCount_By_TicketID]
GO



Create Procedure [dbo].[USP_HTS_Get_ContinuanceCount_By_TicketID]
(
@TicketID int
)
as


Select Count(*) as ContinuanceCount   from tblticketsflag
where ticketid_pk = @TicketID
and flagid = 9

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[Usp_Hts_Get_OpenServiceTicketsRecords]    Script Date: 11/05/2008 14:56:20 

Business Logic: This stored procedure is used by HTP/Validations/Service Ticket Report 
It displays the cases of the Clients and Quote Clients whose service ticket flag has either added or closed.

List of Parameters:  
@StartDate = starting date 
@EndDate = ending date 
@EmpID = EmpId associated with user
@TicketType = eg: traffic ticket
@ShowAllMyTickets = If Show all My Ticket is checked then records will be displayed of currently login user without restriction of given dates.
@ShowAllUserTickets = if Show All User Ticket is checked then records will be displayed from all users without restriction of given dates.
@CaseTypeId = id associated with case.
@isdaterange = If "Use Date Range" check box is checked then user can search records within the date range. If unchecked, then user can search records without date range. 

by default User will be able to see all tickets opened by them or assigned to them.

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- [Usp_Hts_Get_OpenServiceTicketsRecords] '11/04/2008', '11/04/2008', 3991, 1, 1, -1,1
ALTER PROCEDURE [dbo].[Usp_Hts_Get_OpenServiceTicketsRecords]
	@StartDate DATETIME,
	@EndDate DATETIME,
	@EmpID INT,
	@TicketType INT ,
	@ShowOnlyIncomplete INT = 0, 
	 --Sabir Khan  5738 05/25/2009 code commented...
	 --@ShowAllMyTickets int =0 ,
	 --@ShowAllUserTickets int =0,
	@CaseTypeId INT = -1, -- Agha Usman 4271 06/25/2008
	                      --@isdaterange int,  -- ozair   5069   11/06/2008 new   parameter added for using date range for show all check
	@showAllfollowup INT, --sabir Khan 5738 05/21/2009 Show all followupdate...
	@ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	DECLARE @sqlString VARCHAR(MAX)                                    
	
	SET @StartDate = CONVERT(VARCHAR, @StartDate, 101) + ' 00:00:00'                 
	SET @EndDate = CONVERT(VARCHAR, @EndDate, 101) + ' 23:59:59'    
	
	SELECT DISTINCT 
	       T.TicketID_PK,
	       T.Lastname + '' AS ClientName,
	       '' + T.Firstname AS FirstName ,
	       dbo.fn_DateFormat(
	           ISNULL(MIN(TF.RecDate), CONVERT(DATETIME, '1/1/1900')),
	           28,
	           '/',
	           ':',
	           1
	       ) AS openDate,
	       --Yasir Kamal 5759 04/10/2009 using Recdate For sorting
	       TF.RecDate AS sortopenDate,
	       -- 5759 end   			  
	       U.Abbreviation AS Rep,
	       T.GeneralComments,
	       CASE 
	            WHEN TF.Priority = 0 THEN 'Low'
	            WHEN TF.Priority = 1 THEN 'Medium'
	            WHEN TF.Priority = 2 THEN 'High'
	            ELSE 'N/A'
	       END AS Priority,
	       stc.Description,
	       ISNULL(TF.ContinuanceStatus, 0) AS [COMPLETION],
	       TF.FlagID,
	       TF.FID,
	       dbo.fn_DateFormat(
	           ISNULL(tv.courtdatemain, CONVERT(DATETIME, '1/1/1900')),
	           27,
	           '/',
	           ':',
	           1
	       ) AS courtdate,
	       dbo.formattime(tv.courtdatemain) AS courttime,
	       courtnumbermain,
	       cvs.shortdescription,
	       c.shortname AS settinginformation,
	       dbo.fn_DateFormat(
	           ISNULL(lastUpdatedate, CONVERT(DATETIME, '1/1/1900')),
	           28,
	           '/',
	           ':',
	           1
	       ) AS recsortdate,
	       --Yasir Kamal 5759 04/10/2009 using Recdate For sorting
	       lastUpdatedate AS sortrecsortdate,
	       --5759 end
	       ISNULL(stc.Description, 'N/A') AS Category,
	       ISNULL(U2.Abbreviation, 'N/A') AS AssignedTo,
	       dbo.fn_DateFormat(
	           ISNULL(TF.ClosedDate, CONVERT(DATETIME, '1/1/1900')),
	           28,
	           '/',
	           ':',
	           1
	       ) AS closeddate,
	       TF.RecDate,
	       dbo.fn_DateFormat(
	           ISNULL(tv.courtdatemain, CONVERT(DATETIME, '1/1/1900')),
	           27,
	           '/',
	           ':',
	           1
	       ),
	       ct.CaseTypeName,
	       -- Zahoor 4770 09/16/08
	       CO.Description AS ContinuanceOption,
	       ct.CaseTypeId,
	       dbo.fn_DateFormat(
	           ISNULL(TF.RecDate, CONVERT(DATETIME, '1/1/1900')),
	           27,
	           '/',
	           ':',
	           1
	       ) AS rec,
	       --Sabir Khan 5738 06/01/2009 get follow up date...
	       dbo.fn_dateFormat(
	           TF.ServiceTicketFollowupDate,
	           27,
	           '/',
	           ':',
	           1
	       ) AS followupdate,
	       TF.ServiceTicketFollowupDate
	FROM   dbo.tblTicketsFlag TF
	       INNER JOIN dbo.tblTickets AS T
	            ON  TF.TicketID_PK = T.TicketID_PK
	       INNER JOIN casetype ct
	            ON  T.CaseTypeId = ct.CaseTypeId
	       INNER JOIN tblusers AS U
	            ON  tf.empid = u.employeeID
	       INNER JOIN tblusers u2
	            ON  u2.employeeid = tf.AssignedTo
	       --Sabir Khan 5738 05/25/2009 join for employee id...
	       INNER JOIN tblusers u3
	            ON  u3.employeeid = @EmpID
	       LEFT OUTER JOIN tblServiceTicketCategories stc
	            ON  TF.ServiceTicketCategory = stc.ID
	       INNER JOIN tblticketsviolations tv
	            ON  tv.ticketid_pk = t.ticketid_pk
	       INNER JOIN tblCourts C
	            ON  c.courtid = tv.courtid
	       INNER JOIN tblCourtviolationstatus Cvs
	            ON  cvs.courtviolationstatusid = tv.courtviolationstatusidmain
	       LEFT OUTER JOIN ContinuanceOptions CO
	            ON  CO.OptionID = tf.ContinuanceOption
	WHERE  Flagid = 23
	       AND Tv.ticketsviolationid IN (SELECT TOP 1 ticketsviolationid
	                                     FROM   tblticketsviolations
	                                     WHERE  ticketid_pk = t.ticketid_pk) 
	           -- Noufil 5069 11/04/2008 Show close ticket records to..
	           --Sabir Khan 5738 05/25/2009 Code commented
	           --//////////////////////////////////////////////
	           -- and ((@showAllfollowup = 1) or (
	           		   
	          		--(@TicketType=1 and @isdaterange=1 and tf.ServiceTicketFollowupDate between  convert(varchar(20), @StartDate) and convert(varchar(20), @EndDate))
	           		--or (@TicketType=0 and @isdaterange=1 and tf.ServiceTicketFollowupDate between  convert(varchar(20), @StartDate) and convert(varchar(20), @EndDate))
	         		--)
	           --)
	           --//////////////////////////////////
	       AND (
	               -- Sabir Khan 5738 05/25/2009 if called from validation .. show only incomplete tickets having follow update on today, past null, 1/1/1900
	               (
	                   @ShowOnlyIncomplete = 1
	                   
	                   AND (--Yasir Kamal 6732 10/30/2009 display 100% records in open tickets
	                   		(DATEDIFF(DAY, ISNULL(tf.ServiceTicketFollowupDate, '1/1/1900'),GETDATE()) >= 0 AND ISNULL(tf.continuancestatus, 0) <= 100) 
	                   		OR (ISNULL(tf.continuancestatus, 0)=0))
	                   AND ISNULL(TF.deleteflag, 0) = 0
	               )
	               OR (
	                      @ShowOnlyIncomplete = 0
	                      AND (
	                              (@showAllfollowup = 1 OR @ValidationCategory=2)  -- Saeed 7791 07/09/2010 display all records if @showAllfollowup=1 or validation category is 'Report'
	                              OR (
	                              		(@showAllfollowup = 0  OR @ValidationCategory=1) -- Saeed 7791 07/09/2010 display  if @showAllfollowup = 0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	                                     AND (
											-- Sabir Khan 5738 05/25/2009 for open service ticket...
	                                         @TicketType = 1
	                                         AND DATEDIFF(DAY, tf.ServiceTicketFollowupDate, @StartDate) <= 0
	                                         AND DATEDIFF(DAY, tf.ServiceTicketFollowupDate, @EndDate)   >= 0
	                                     )
	                                     OR 
	                                     (
												-- Sabir Khan 5738 05/25/2009 for close service ticket...
	                                            @TicketType = 0
	                                            AND DATEDIFF(DAY, tf.ServiceTicketFollowupDate, @StartDate) <= 0
												AND DATEDIFF(DAY, tf.ServiceTicketFollowupDate, @EndDate)   >= 0
	                                     )
	                                 )
	                          )
	                  )
	           )
	           
	           
	           
	           --and (@ShowOnlyIncomplete = 0 or isnull(tf.continuancestatus,0) < 100  )
	           --Yasir Kamal 5740 04/01/2009 allow user to see tickets open by then or assigned to them
	           -- Sabir Khan 5738 05/25/2009 If follow update is checked...
	       AND (
	               u3.Accesstype = 2
	               OR u3.isServiceTicketAdmin = 1
	               OR tf.assignedto = @EmpID
	               OR tf.EmpID = @EmpID
	           )
	           
	           
	           --end 5740
	       AND (--Yasir Kamal 6732 10/30/2009 display 100% records in open tickets
				(@TicketType = 1 and ISNULL(tf.continuancestatus, 0) <= 100 and ISNULL(TF.deleteflag, 0) = 0)
				 OR 
				(@TicketType = 0 and ISNULL(tf.continuancestatus, 0) = 100 and ISNULL(TF.deleteflag, 0) = 1)
				)
	       --AND (@TicketType <> 1 OR ISNULL(TF.deleteflag, 0) = 0)
	       AND (
	               @CaseTypeId = -1
	               OR ct.CaseTypeId = CONVERT(VARCHAR(10), @CaseTypeId)

	       )
	       
	GROUP BY
	       T.TicketID_PK,
	       T.Lastname,
	       T.Firstname,
	       TF.RecDate,
	       U.Abbreviation,
	       T.GeneralComments,
	       TF.Priority,
	       stc.Description,
	       TF.ContinuanceStatus,
	       TF.FlagID,
	       TF.FID,
	       tv.courtdatemain,
	       cvs.shortdescription,
	       c.shortname,
	       lastUpdatedate,
	       stc.Description,
	       U2.Abbreviation,
	       TF.ClosedDate,
	       TF.RecDate,
	       ct.CaseTypeName,
	       ct.CaseTypeId,
	       courtnumbermain,
	       CO.Description,
	       TF.ServiceTicketFollowupDate
	       
	       --Sabir Khan 5738 05/25/2009 sort by followupdate date
	       --order by TF.RecDate desc
	ORDER BY
	       TF.ServiceTicketFollowupDate
      
                                    
                                    


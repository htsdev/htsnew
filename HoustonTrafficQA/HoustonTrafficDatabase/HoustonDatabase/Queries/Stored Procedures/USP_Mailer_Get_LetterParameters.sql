if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_LetterParameters]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_LetterParameters]
GO

-- USP_Mailer_Get_LetterParameters 29,0
CREATE procedure [dbo].[USP_Mailer_Get_LetterParameters]
	(
	@LetterType int,
	@SPType int
	)


as

--select param_name from tblletter_parameters where lettertype = @lettertype and sp_type = @sptype

select [name] as param_name from sys.parameters  p
inner join tblletter l
on p.object_id = case @sptype 
					when 0 then object_id(l.count_spname) 
					when 1 then object_id(l.data_spname)
				end
where l.letterid_pk = @lettertype 
order by p.parameter_id




GO
grant execute on [dbo].[USP_Mailer_Get_LetterParameters] to dbr_webuser
GO

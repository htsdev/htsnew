/******   
Alter by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to Insert the information in document tracking batch tickets table and
				 also insert the entry in case history for the related cases. it also gets the required information 
				 that will be displayed under Arr summary report.			  
  
List of Parameters:   
 @TVIDS : ticketsviilationids
 @DocumentBatchID : docuemnt id 
 @updateflag : 
 @employee : employee id of current logged in user  
  
List of Columns:    
 ticketid :
 ticketno : 
 lastname : 
 firstname :
 dob : 
 midnum: 
 causeno : 
 courtdate :
 description :  
 status :  
 courdate :
 roomno :
 dlnumber : 
 CRTDATE :  
 ticketsviolationid :
 docid : document id 
   
******/

ALTER procedure [dbo].[usp_hts_arraignment_PleaNotGuilyReport]          
@TVIDS as varchar(max),          
@DocumentBatchID as int,          
@UpdateFlag as int,     
@employee as int     

as                      
          
declare @temp_ticketid int


DECLARE @TBLTEMPTVID TABLE (TICKETVIOLATIONID INT)             
declare @temp table          
(ticketid int,ticketno varchar(20), lastname varchar(20), firstname varchar(20),  dob varchar(12),midnum varchar(20), causeno varchar(20),         
description varchar(200),status varchar(20),courtdate datetime,roomno varchar(3),dlnumber varchar(30),crtdate varchar(12), ticketsviolationid int,  -- Haris Ahmed 10327 08/01/2012 Change description length to VarChar(200)           
docid int)           
          
if  @TVIDS=''                      
begin             
	insert into @temp                
      
	Select   distinct                           
	t.ticketid_pk as TicketID,                        
	tV.REFCASENUMBER AS TICKETNO,                              
	upper(T.LASTNAME) as lastname,                            
	upper(T.FIRSTNAME) as firstname,                            
	convert(varchar(12),T.DOB,101) as DOB,                          
	T.MIDNUM,                            
	TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,                      
	upper(v.description) as description  ,                    
	TCV.SHORTDESCRIPTION as status,                    
	TV.COURTDATEMAIN  as courtdate ,                    
	tv.courtnumbermain as roomno,                    
	t.dlnumber,                    
	CONVERT(VARCHAR(12),TV.COURTDATEMAIN,101) AS CRTDATE,
	tv.ticketsviolationid,          
	@DocumentBatchID  as DOCID          
    FROM                               
	TBLTICKETS T INNER JOIN                             
	TBLTICKETSVIOLATIONS TV ON                             
	T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                             
	TBLCOURTVIOLATIONSTATUS TCV ON                             
	TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                       
	tblviolations v on                       
	tv.violationnumber_pk =v.violationnumber_pk  left outer join              
	tblticketsviolationlog tvl on                 
	tv.ticketsviolationid=tvl.ticketviolationid and                
	tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid)                            
	WHERE                            
	TCV.CATEGORYID=2 
	AND T.ticketid_pk IN ( Select distinct                        
							t.ticketid_pk                             
							FROM                           
							TBLTICKETS T INNER JOIN                             
							TBLTICKETSVIOLATIONS TV ON                             
							T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                             
							TBLCOURTVIOLATIONSTATUS TCV ON                             
							TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                       
							tblviolations v on                       
							tv.violationnumber_pk =v.violationnumber_pk inner join                           
							tblcourts tc on                          
							tv.courtid=tc.courtid left outer join                   
							tblticketsviolationlog tvl on                 
							tv.ticketsviolationid=tvl.ticketviolationid and                
							tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid)                                                
							WHERE                            
							tV.courtviolationstatusidmain in (select courtviolationstatusid from tblcourtviolationstatus where CATEGORYID IN (2))
							and t.activeflag=1  and tv.courtid in (3001,3002,3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .
							and tv.courtdatemain is not null                                  
							AND tV.courtviolationstatusidmain <> 80                              
							AND tV.courtviolationstatusidmain <> 162                          
						)                             
	and t.activeflag=1  
	and tv.courtid in (3001,3002,3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .)                                 
	order by  courtdate,lastname,firstname                    
      
	if @UpdateFlag =1          
	begin    
		declare @temp2 table             
		(              
		counter int IDENTITY(1,1) NOT NULL,              
		ticketsviolationid int    
		)    

		set @temp_ticketid = 0 
		
		insert into @temp2 select ticketsviolationid from @temp    
		declare @loopcounter as int            
		set @loopcounter=(SELECT count(*) FROM @temp2)    
		while @loopcounter >=1     
		begin    
			-- tahir 4702 08/28/2008 need to update arraignment wait date & A/W follow up date....
			
			update tblticketsviolations 
			set courtviolationstatusidmain=201, 
			ArrignmentWaitingdate = getdate(),  -- set the A/W date
			vemployeeid = @employee 
			where ticketsviolationid = (SELECT ticketsviolationid FROM @temp2 where counter=@loopcounter)          

			select @temp_ticketid = ticketid_pk from tblticketsviolations 
			where ticketsviolationid = (SELECT ticketsviolationid FROM @temp2 where counter=@loopcounter)  

			-- update A/W follow up date......
			--Sabir Khan 5676 03/19/2009 FollowUpDate logic has been changed from 5 business days to 6 business days...
			if exists (select ticketid_pk from dbo.tblticketsextensions where ticketid_pk = @temp_ticketid)
				begin
					update tblticketsextensions set arrwaitingfollowupdate = dbo.fn_getnextbusinessday(getdate(),6)
					where ticketid_pk = @temp_ticketid
				end
			else
				begin
					insert into tblticketsextensions (ticketid_pk, arrwaitingfollowupdate)
					select @temp_ticketid, dbo.fn_getnextbusinessday(getdate(),6)
				end
			
			-- end 4702

			set @loopcounter=@loopcounter-1    
			set @temp_ticketid = 0
		end          
	end        
    
	select * from @temp               
end       
else   
begin           
	insert into @TBLTEMPTVID select * from dbo.sap_string_param(@TVIDS)          
    
    insert into @temp      
	Select   distinct                           
    t.ticketid_pk as TicketID,                        
	tV.REFCASENUMBER AS TICKETNO,                              
	upper(T.LASTNAME) as lastname,                            
	upper(T.FIRSTNAME) as firstname,                            
	convert(varchar(12),T.DOB,101) as DOB,                          
	T.MIDNUM,                            
	TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,                      
	upper(v.description) as description  ,                    
	TCV.SHORTDESCRIPTION as status,                    
	TV.COURTDATEMAIN  as courtdate ,                    
	tv.courtnumbermain as roomno,                    
	t.dlnumber,                    
	CONVERT(VARCHAR(12),TV.COURTDATEMAIN,101) AS CRTDATE,
	tv.ticketsviolationid,          
	@DocumentBatchID   as DOCID                  
    FROM                                 
	TBLTICKETS T INNER JOIN                             
	TBLTICKETSVIOLATIONS TV ON                             
	T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                             
	TBLCOURTVIOLATIONSTATUS TCV ON                             
	TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                       
	tblviolations v on                       
	tv.violationnumber_pk =v.violationnumber_pk  left outer join                      
	tblticketsviolationlog tvl on                 
	tv.ticketsviolationid=tvl.ticketviolationid and                
	tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid)                            
	WHERE                            
	TCV.CATEGORYID=2 
	AND TV.Ticketsviolationid in(	Select distinct                        
									tv.Ticketsviolationid  
									FROM                           
									TBLTICKETS T INNER JOIN			                       
								    TBLTICKETSVIOLATIONS TV ON                             
								    T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                             
									TBLCOURTVIOLATIONSTATUS TCV ON                             
									TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                       
									tblviolations v on                       
									tv.violationnumber_pk =v.violationnumber_pk inner join                           
									tblcourts tc on                          
									tv.courtid=tc.courtid left outer join                    
									tblticketsviolationlog tvl on                 
									tv.ticketsviolationid=tvl.ticketviolationid and                
									tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid)                                                
									WHERE                                  
									TV.TICKETSVIOLATIONID IN (SELECT TICKETVIOLATIONID FROM @TBLTEMPTVID)  and                                     
									tV.courtviolationstatusidmain --in (1,2,3,5,8)                                 
									in (select courtviolationstatusid from tblcourtviolationstatus where CATEGORYID IN (2)) --AND TCV.STATUSTYPE=1                            
									and t.activeflag=1  and tv.courtid in (3001,3002,3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . . 
									and tv.courtdatemain is not null                                  
									AND tV.courtviolationstatusidmain <> 80                              
									AND tV.courtviolationstatusidmain <> 162                    
								)                             
	and t.activeflag=1  
	and tv.courtid in (3001,3002,3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .                                 
	order by  courtdate,lastname,firstname             
      
	if @UpdateFlag =1          
	begin     
		declare @temp1          table    
		(              
		counter int IDENTITY(1,1) NOT NULL,              
		TICKETVIOLATIONID int    
		)     
		
		set @temp_ticketid = 0 

		insert into @temp1 SELECT TICKETVIOLATIONID FROM @TBLTEMPTVID    
		declare @loopcounter1 as int            
		set @loopcounter1=(SELECT count(*) FROM @temp1)    
		while @loopcounter1 >=1     
		begin    
			-- tahir 4702 08/28/2008 need to update arraignment wait date & A/W follow up date....
			update tblticketsviolations 
			set courtviolationstatusidmain=201,  
			ArrignmentWaitingdate = getdate(),  -- set the A/W date
			vemployeeid = @employee 
			where ticketsviolationid = (SELECT TICKETVIOLATIONID FROM @temp1 where counter=@loopcounter1)          

			select @temp_ticketid = ticketid_pk from tblticketsviolations 
			where ticketsviolationid = (SELECT TICKETVIOLATIONID FROM @temp1 where counter=@loopcounter1)

			-- update A/W follow up date......
			--Sabir Khan 5676 03/19/2009 FollowUpDate logic has been changed from 5 business days to 6 business days...
			if exists (select ticketid_pk from dbo.tblticketsextensions where ticketid_pk = @temp_ticketid)
				begin
					update tblticketsextensions set arrwaitingfollowupdate = dbo.fn_getnextbusinessday(getdate(),6)
					where ticketid_pk = @temp_ticketid
				end
			else
				begin
					insert into tblticketsextensions (ticketid_pk, arrwaitingfollowupdate)
					select @temp_ticketid, dbo.fn_getnextbusinessday(getdate(),6)
				end
			
			-- end 4702   

			set @loopcounter1=@loopcounter1-1 
			set @temp_ticketid = 0
		end    
	end            
         
	select * from @temp          
end                           
      
IF @DocumentBatchID <> 0          
begin        
	declare @docid as varchar(20)      
	set @docid=convert(varchar(20),@DocumentBatchID,101)      
    
	Insert into tbl_htp_documenttracking_batchTickets (BatchID_FK, TicketID_FK)
	select distinct DOCID, ticketid from @temp
	
	insert into tblTicketsNotes (TicketID,Subject,EmployeeID)      
	select TicketID_FK, '<a href="javascript:window.open(''../quickentrynew/PreviewPrintHistory.aspx?filename='+'ARR-'+@docid+''');void('''');"''>Arraignment Setting Summary DOC ID : ' + @docid+'</a>', @employee      
	from tbl_htp_documenttracking_batchTickets 
	where BatchID_FK=@DocumentBatchID        
end        
    
  
ALTER procedure [dbo].[USP_HTS_RM_GetAllCleintswithoutOfficerInfo]    
    
as     
    
select '<a href="javascript:window.open(''../clientinfo/violationfeeold.aspx?search=0&casenumber='+convert(varchar(10),t.ticketid_pk)+''');void('''');"''>' + refcasenumber+'</a>' as [Ticket Number],   
casenumassignedbycourt as  [Cause Number],lastName as  [Last Name], FirstName as [First Name], courtdatemain as [Court Date]     
from tbltickets t , tblticketsviolations tv    
where t.ticketid_pk = tv.ticketid_pk     
and datediff(day, tv.courtdatemain, getdate()) <=0     
and tv.ticketofficernumber not in (select officernumber_pk from tblofficer )    
and activeflag = 1 and tv.courtid in (3001,3002,3003)  
and tv.courtviolationstatusidmain not in (select courtviolationstatusid from tblcourtviolationstatus where categoryid = 50)   
union  
select '<a href="javascript:window.open(''../clientinfo/violationfeeold.aspx?search=0&casenumber='+convert(varchar(10),t.ticketid_pk)+''');void('''');"''>' + refcasenumber+'</a>' as [Ticket Number],   
casenumassignedbycourt as  [Cause Number],lastName as  [Last Name], FirstName as [First Name], courtdatemain as [Court Date]     
from tbltickets t, tblticketsviolations tv    
where t.ticketid_pk = tv.ticketid_pk     
and datediff(day, tv.courtdatemain, getdate()) <=0     
and isnull(tv.ticketofficernumber ,0) = 0  
and activeflag = 1 and tv.courtid in (3001,3002,3003)  
and tv.courtviolationstatusidmain not in (select courtviolationstatusid from tblcourtviolationstatus where categoryid = 50)  
  
order by courtdatemain    
go

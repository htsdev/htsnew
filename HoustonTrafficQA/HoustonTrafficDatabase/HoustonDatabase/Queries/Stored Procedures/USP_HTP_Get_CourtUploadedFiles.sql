set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** 
Create by:  Muhammad Kazim
Business Logic : this procedure is used to get the uploaded files information of particular court.
 
List of Parameters:     
      @Courtid : represnts Courtid of a court 
 
List of Columns: 
      id :          this field will display the fileid of a file
      courtid:	    this field will display the court id of uploaded file
      Documentname: this field will display the documentname of uploaded file.
      DocumentPath: this field will display the DocumentPath of uploaded file.
      Rep:	    this field will display the rep name who has uploaded the file.	
 
******/



--USP_HTP_Get_CourtUploadedFiles 3069

Create PROCEDURE [dbo].[USP_HTP_Get_CourtUploadedFiles]
@CourtId int

AS

select id,Courtfiles.CourtId,[Uploaded Date],DocumentName,DocumentPath,username as rep from Courtfiles 
join tblusers on Courtfiles.rep=tblusers.employeeid
where Courtfiles.courtid=@courtid  
go


grant execute on dbo.[USP_HTP_Get_CourtUploadedFiles] to [dbr_webuser]
go



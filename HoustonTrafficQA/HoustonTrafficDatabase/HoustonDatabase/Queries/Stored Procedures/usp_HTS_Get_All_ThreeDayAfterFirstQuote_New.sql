﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 1/14/2010 11:44:37 AM
 ************************************************************/

/******  
Alterd by:  Yasir kamal
Business Logic : This procedure is used By Quote Call Back Report.
         
List of Parameters:   
    @CallDate : Call Back Date.
	@casetypeid : -1 - All
	               1 - Traffic
	               2 - Criminal
	               3 - Civil
	               4 - Family
******/

-- Noufil 4487 08/05/2008 courtlocation field added
-- [usp_HTS_Get_All_ThreeDayAfterFirstQuote_New] '01/01/1900', 1
ALTER PROCEDURE [dbo].[usp_HTS_Get_All_ThreeDayAfterFirstQuote_New]
	@CallDate DATETIME,
	@casetypeid INT -- Noufil 4487 08/04/2008 Case type parameter added
AS
--ozair 6934 01/22/2010 Removed Extra columns
	SELECT DISTINCT 
	       dbo.tblViolationQuote.QuoteID,
	       CONVERT(VARCHAR(12), dbo.tblViolationQuote.LastContactDate, 101) AS 
	       LastContactDate,
	       CONVERT(INT, GETDATE() - dbo.tblViolationQuote.LastContactDate) AS 
	       Days,
	       --Ozair 6934 01/26/2010 first/last name swaped 
	       dbo.tblTickets.Firstname + ' ' + dbo.tblTickets.Lastname AS Customer,	
	       CONVERT(DECIMAL(8, 0), dbo.tblTickets.calculatedtotalfee) AS 
	       calculatedtotalfee,	       
	       dbo.tblViolationQuote.FollowUpYN,
	       dbo.tblTickets.TicketID_PK,
	       REPLACE(
	           tblQuoteResult_1.QuoteResultDescription,
	           '---------------------',
	           ' '
	       ) AS FollowUpStatus,
	       CONVERT(
	           VARCHAR(12),
	           --Yasir kamal 5744 04/14/2009 auto court date. 
	           dbo.tblTicketsViolations.CourtDate,
	           101
	       ) AS CourtDate,
	       --Fahad 6934 01/13/2010 removed some unused columns 
	       dbo.tblViolationQuote.CallbackDate,
	       tblTickets.casetypeid,
	       c.shortname AS crt
	       --Fahad 6934 01/13/2010 Temp Table Created in order to maintain Tracking
	       INTO #tempquote
	FROM   dbo.tblViolationQuote
	       LEFT OUTER JOIN dbo.tblQuoteResult tblQuoteResult_1
	            ON  dbo.tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID
	       RIGHT OUTER JOIN dbo.tblTicketsViolations
	       RIGHT OUTER JOIN dbo.tblTickets
	            ON  dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK
	            ON  dbo.tblViolationQuote.TicketID_FK = dbo.tblTickets.TicketID_PK
	       LEFT OUTER JOIN dbo.tblUsers
	            ON  dbo.tblTickets.EmployeeIDUpdate = dbo.tblUsers.EmployeeID	       
	       INNER JOIN dbo.tblCourts c
	            ON  tblTicketsViolations.CourtID = c.Courtid 
	                --Yasir Kamal 5744 04/02/2009 cases having primary auto status ARRAIGNMENT only
	                
	       INNER JOIN dbo.tblCourtViolationStatus
	            ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
	WHERE  (dbo.tblTickets.Activeflag = 0)
	       AND (dbo.tblViolationQuote.FollowUpYN = 1)
	       AND (
	               DATEDIFF(DAY, dbo.tblViolationQuote.CallbackDate, @CallDate)
	           ) = 0
	           
	           -- AND (dbo.tblViolationQuote.CallbackDate between @CallDate +'00:00:00' and @CallDate +'23:59:59')
	           -- And (dbo.tblTicketsViolations.CourtDateMain IN
	           --  (SELECT MAX(dbo.tblTicketsViolations.CourtDateMain)FROM dbo.tblTicketsViolations
	           --    WHERE dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK))
	       AND (@casetypeid = -1 OR tblTickets.casetypeid = @casetypeid)
	           -- Noufil 5894 06/17/2009 code comment as per QA request in Task history
	           -- tahir 6513 09/02/09 need to list only arraignment cases
	           --Sabir Khan 6966 11/10/2009 Allow Criminal, Civil and Family Law cases on Quote call back report...
	       AND (
	               (
	                   tblTickets.casetypeid = 1
	                   AND dbo.tblCourtViolationStatus.CategoryID = 2
	               )
	               OR (
	                      tblTickets.casetypeid IN (2, 3, 4)
	                      AND dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5)
	                  )
	           )
	       -- Babar Ahmad 9654 09/28/2011 Excluded records with Problem Client (No Hire/Allow Hire) flag.   
	       AND dbo.tblViolationQuote.TicketID_FK NOT IN (SELECT TicketID_PK FROM tblTicketsFlag ttf WHERE ISNULL(ttf.FlagID,0) IN (13,36))     
	           -- Noufil 5894 06/17/2009 Add order by clause
	ORDER BY --Fahad 7496 04/08/2010 Append the Order By clause QuoteID Added
	       dbo.tblViolationQuote.CallBackDate,dbo.tblViolationQuote.QuoteID 
--Fahad 6934 01/13/2010 Inser clause added and also inserted through Temporaray table.
	INSERT INTO QuoteCallbackTracking
	  (
	    TicketID,
	    QuoteID
	  )
	SELECT DISTINCT tvq.TicketID_PK,
	       tvq.QuoteID
	FROM   #tempquote tvq
	WHERE  tvq.QuoteID NOT IN (SELECT qct.QuoteID
	                           FROM   QuoteCallbackTracking qct)
	
	SELECT *
	FROM   #tempquote
	
DROP TABLE #tempquote --Droped the table to remove reference
	

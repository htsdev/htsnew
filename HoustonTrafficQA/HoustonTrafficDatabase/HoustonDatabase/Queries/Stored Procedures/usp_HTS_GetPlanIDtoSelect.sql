SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetPlanIDtoSelect]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetPlanIDtoSelect]
GO










--Get Plan ID from  tblPricePlans where courtid= ddl_courtlocation.selected value  
--and Date is between effectivedate and end date to be selected in pricing plan   
-- on violation update screen  
CREATE PROCEDURE  usp_HTS_GetPlanIDtoSelect  
@courtid  int,  
@RecordLoadDate varchar(12),
@Old	int  
AS      

if @Old=0
begin      
select planid from tblpriceplans  
where courtid=@courtid and   
@RecordLoadDate between EffectiveDate and enddate  
end

if @Old=1
begin      
select planid from tblpriceplans  
where courtid=@courtid and
getdate() between EffectiveDate and enddate  
end  
  
  
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


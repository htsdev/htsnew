SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
/*
Altered By     : Fahad Muhammad Qureshi.
Altered Date   : 07/31/2009  
TasK		   : 6054          
Business Logic : This procedure Retrieve information about all Credit types i-e
							1)-Partener Firm Credit.
							2)-Attorney Credit.
							3)-Friend Credit.
List of Input Parameters:
							@sDate:	Start Date.
							@eDate: End Date.
							@FirmId: Firm ID
*/ 



ALTER procedure [dbo].[usp_Get_All_AttorneyCreditPayments]           
                  
@RecDate DateTime,              
@RecToDate datetime,  
@firmId int =NULL,               
@BranchId INT =1 -- Sabir Khan 10920 05/27/2013 Branch ID added.              
                  
AS                  
                  
  set @RecToDate = @RecToDate + '23:59:59.998'                        
            
            
SELECT p.ChargeAmount AS ChargeAmount,                     
p.EmployeeID,                           
p.PaymentType,                     
CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,                     
u.Lastname,                           
CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                          
ELSE t.Lastname + ', ' + t.Firstname                          
END AS CustomerName,                          
c.ShortName AS Court,                          
pt.Description AS CCType,                      
p.TicketID,                    
case t.bondflag when 1 then 'B' else '' end AS BondFlag,                           
-- Added By Zeeshan Ahmed On 6/20/2007------                  
CASE WHEN isnull(t.initialadjustment,0) <> 0 then 'YES'  ELSE '' END as Adj1,                    
CASE WHEN ISNULL(t.adjustment,0) <> 0 THEN 'YES' ELSE '' END as Adj2,                     
--------------------------------------------                  
p.CardType,                    
             
dbo.Fn_FormateTime(p.RecDate) As RecTime  ,                    
p.PaymentType ,    
p.recdate as sorttime           
 , IPAddress	-- Sabir Khan 10920 05/27/2013 Getting IP Address.                
FROM dbo.tblTickets t                     
INNER JOIN                    
dbo.tblCourts c                     
INNER JOIN                    
dbo.tblTicketsViolations v                     
ON  c.Courtid = v.CourtID                     
and v.ticketsviolationid = (                    
select top 1 ticketsviolationid from tblticketsviolations                    
where courtid is not null and ticketid_pk = v.ticketid_pk                    
)                    
ON  t.TicketID_PK = v.TicketID_PK                     
RIGHT OUTER JOIN                    
dbo.tblPaymenttype pt                     
RIGHT OUTER JOIN                    
dbo.tblTicketsPayment p                     
ON  pt.Paymenttype_PK = p.PaymentType 
ON  t.TicketID_PK = p.TicketID                     
LEFT OUTER JOIN                    
dbo.tblUsers u                     
ON  p.EmployeeID = u.EmployeeID             
Where        p.RecDate  Between  @RecDate  AND  @RecToDate                    
And   p.paymenttype IN (9,11,104) --Fahad 6054 07/30/2009 Payment Type Added 9-Attorney Credit,11-Friend Credit and 104-Partner Firm Credit
AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))  
AND p.BranchID IN (SELECT branchid FROM fn_get_branchIDs(@BranchId))   -- sabir khan 10920 05/27/2013 Branch Id check added.                                
ORDER by              
p.RecDate desc              










GO
grant execute on [dbo].[usp_Get_All_AttorneyCreditPayments] to dbr_webuser
go

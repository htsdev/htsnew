SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PricePlanDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PricePlanDetail]
GO










--Getting Price plan Detail by passing plan ID
CREATE procedure USP_HTS_GET_PricePlanDetail
	(
	@PlanId		int
	)
as
/*
SELECT	p.PriceId, 
	p.planid,
	p.CategoryId, 
	c.CategoryName, 
	p.BasePrice, 
	p.SecondaryPrice, 
	p.BasePercentage, 
	p.SecondaryPercentage, 
	p.BondBase, 
	p.BondSecondary, 
	p.BondBasePercentage, 
	p.BondSecondaryPercentage, 
	p.BondAssumption, 
	p.FineAssumption

FROM    dbo.tblPricePlanDetail p 
INNER JOIN
	dbo.tblViolationCategory c 
ON 	p.CategoryId = c.CategoryId
where 	p.planid = @planid
*/

SELECT     
	ISNULL(p.PriceId, 0) AS PriceId, 
	ISNULL(p.PlanId, @planid) AS PlanId, 
	ISNULL(c.CategoryName,0) AS CategoryName, 
	ISNULL(p.BasePrice,0) AS BasePrice, 
	ISNULL(p.SecondaryPrice,0) AS SecondaryPrice , 
	ISNULL(p.BasePercentage,0) AS BasePercentage , 
             ISNULL(p.SecondaryPercentage,0) AS SecondaryPercentage, 	
	ISNULL(p.BondBase,0) AS BondBase, 
	ISNULL(p.BondSecondary,0) AS BondSecondary, 
	ISNULL(p.BondBasePercentage,0) AS BondBasePercentage , 
	ISNULL(p.BondSecondaryPercentage,0) AS BondSecondaryPercentage,
	ISNULL(p.BondAssumption,0) AS BondAssumption, 
	ISNULL(p.FineAssumption,0) AS FineAssumption , 
	c.CategoryId
FROM         dbo.tblPricePlanDetail p RIGHT OUTER JOIN
                      dbo.tblViolationCategory c ON p.CategoryId = c.CategoryId
and     (ISNULL(p.PlanId, @planid) = @planid)





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/*  
* Created By		: Saeed Ahmed
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to delete the record in the ReportAttribute table 
*                     when the attribute id is not present in ReportSetting table
* Parameter List  
* @attributeid 		: This parameter contains the AttributeID
*
*/

CREATE PROCEDURE [dbo].[USP_ReportSetting_Delete_ReportAttribute]
(@attributeid INT)
AS
BEGIN
    IF EXISTS(
           SELECT AttributeID
           FROM   ReportSetting rs
           WHERE  rs.AttributeId = @attributeid
       )
    BEGIN
        SELECT 'true' AS isExist
    END
    ELSE
    BEGIN
        DELETE 
        FROM   ReportAttribute
        WHERE  AttributeID = @attributeid
        SELECT 'false' AS isExist
    END
END 

GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_Delete_ReportAttribute] TO dbr_webuser
GO	
	

  
  
/******   
Alter by:  Syed Muhammad Ozair for Task 3502 on 03/21/2008  
Business Logic : this procedure is used to get the ALR request report and format it in html and Email at specified address.  
     also it checks for the server at which it is running from, to get the proper email address to avoid confusion.  
  
List of Parameters:   
 @EmailAddress : email address at which email should be sent  
  
List of Columns:    
 S.NO :  serial no according to the number of clients  
 TICKET.NO : ticket number of the violation  
 L.NAME : last name of the client  
 F.NAME : first name of the client  
 CRT.DATE : verified court date of that violation  
 CRT.NO : verified court date of that violation  
 CRT.LOC : verified court location of that violation  
 TRIAL.CATEGORY : It would be ALR.  
******/  
  
  
--[USP_HTS_ALR_Request_Alert_Email] 'ozair@lntechnologies.com'    
    
alter procedure [dbo].[USP_HTS_ALR_Request_Alert_Email]    
(          
--- ozair 3 on 03/21/2008 ---  
 @EmailAddress varchar(60) = ''            
)      
                              
AS       
  
--- ozair 3502 on 03/21/2008 ---  
  
if ( @EmailAddress = '')    
begin    
 declare @ServerIP varchar(100)    
 CREATE TABLE #tempIP (Results varchar(1000) null)     
 BULK INSERT #tempIP    
 FROM 'C:\Program Files\Microsoft SQL Server\SQLServerIP.txt'    
    
 select @ServerIP= results from #tempIP    
    
 drop table #tempIP    
     
 if @ServerIP='66.195.101.51' --Live Settings        
  select @EmailAddress =  'ALRAlert@sullolaw.com'       
 else if @ServerIP='192.168.168.220'  --Developer Settings    
  select @EmailAddress = 'ozair@lntechnologies.com'    
 else -- QA Settings    
  select @EmailAddress = 'faraz@lntechnologies.com'    
end     
  
--- END ozair 3502 on 03/21/2008 ---  
  
  
----------------------khalid for ALR Request Confirmation  
DECLARE @html1 nvarchar(max)  
declare @htmlAlrAlert nvarchar(max)  
declare @AlrRequest varchar(max)   
declare @AlrRequestTitle varchar(50)  
set @AlrrequestTitle ='ALR Request Alert'      
                          
  
                                
declare @c_ticketid varchar(50)                              
declare @SerialNo int                              
declare @counter int                              
declare @tid varchar(50)                              
set @counter = 1                              
                              
                              
Declare @SNO int                                
set @SNO =1                                
                                
Declare @Subject varchar(50)                                
set @Subject ='ALR Request Alert Email('+convert(varchar,getdate(),101) +')'                                
                                
set  @html1= '<style type="text/css">.clsLeftPaddingTable                                
   {                                
    PADDING-LEFT: 5px;                                
    FONT-SIZE: 8pt;                                
    COLOR: #123160;                                
    FONT-FAMILY: Tahoma;                                
    background-color:EFF4FB;                                
    border-collapse:collapse;                                
    border-width:1px;                                
   }                                
                                   
   </style>                                
   <H2 align="center"><font color="#3366cc"><STRONG>'                   
                  
                  
  
----------------------------------------------------------ALR REQUEST ALERT------------------------  
  
set  @htmlAlrAlert= '</strong></font></H2>                                
   <table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black">                                
   <tr>                                
   <td><font color="#3366cc"><STRONG>S.NO</strong></font>                                
   </td>                           
   <td><font color="#3366cc"><STRONG>TICKET.NO</strong></font>                                
   </td>                                     
   <td><font color="#3366cc"><STRONG>L.NAME</strong></font>                                
   </td>                                
   <td><font color="#3366cc"><STRONG>F.NAME</strong></font>                                
   </td>                            
   <td><font color="#3366cc"><STRONG>CRT.DATE</strong></font>                                
   </td>                                
   <td><font color="#3366cc"><STRONG>CRT.NO</strong></font>                                
   </td>                                                
   <td><font color="#3366cc"><STRONG>CRT.LOC</strong></font>                                
   </td>  
   <td><font color="#3366cc"><STRONG>TRIAL.CATEGORY</strong></font>                                
   </td>                                                                       
   </tr>'   
------------------------------------------------------------ALR request Alert ----------end html ----------  
  
        
                              
                             
                              
------------------------------------ALR request Alert ------------start---------------------------------------------------------------  
create table #tblALRrequest                      
(                  
 sno int identity,                              
 sno2 varchar(5) null,                      
 ticketid varchar(50),                      
 ticketno varchar(50),                      
 firstname varchar(50),                      
 lastname varchar(50),                      
 courtdate varchar(20),                  
 courtnumber varchar(10),                      
 status varchar(10),                      
 shortname varchar(50),  
 courtdate2 varchar(50),                      
 )                   
                  
insert into #tblALRrequest                  
(                  
 ticketid ,                      
 ticketno ,                      
 firstname,                      
 lastname ,                      
 courtdate,                  
 courtnumber,                      
 status ,                      
 shortname,  
 courtdate2                 
)              
exec usp_ALR_Request_Alert       
                  
                   
                  
set @counter =1                              
                              
declare cur_alr cursor                              
for select TicketID,sno from #tblALRrequest                             
open cur_alr                             
                                
FETCH next from cur_alr into @c_ticketid,@SerialNo                              
while @@fetch_status = 0                              
begin                             
 if(@counter = 1)                              
 begin                              
  update #tblALRrequest set Sno2 = @counter where Sno=@SerialNo                              
  set @tid = @c_ticketid                              
  set @counter = @counter + 1                            
 end                              
 else                              
 begin                              
  if(@c_ticketid <> @tid)                              
   begin                              
    update #tblALRrequest set Sno2 = @counter where Sno = @SerialNo          
    set @tid = @c_ticketid                              
    set @counter = @counter + 1                         
   end                              
 end                              
FETCH next from cur_alr into @c_ticketid,@SerialNo                              
end               
CLOSE cur_alr                             
DEALLOCATE cur_alr                              
                      
                      
                  
                      
                      
   set @AlrRequest = @html1 + @AlrRequestTitle + @htmlAlrAlert +                  
   Cast ( (                              
   select                              
   td= '<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+ticketid+'" >'+convert(varchar,isnull(sno2,'.'))+' </a> ', '',                           
                             
--td= '<a href="http://newpakhouston.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber='+ticketid+'" >'+isnull(sno2,'.')+' </a> ', '',                              
   td= isnull(ticketno,''),'',  
   td=isnull(lastname,''),'',                      
   td= isnull(firstname,''),'',                      
   td=isnull(courtdate,''),'',                      
   td=isnull(courtnumber,''),'',                                          
   td=isnull( shortname,''),'',  
   td=isnull( status,''),''  
                       
                       
  from #tblALRrequest                       
                                    
order by sno      
                             
FOR XML PATH ('tr'),Type) as NVarchar(max) )+'</table><br><br>'                                
                              
set @AlrRequest = replace(@AlrRequest, '&lt;','<')                               
set @AlrRequest= replace(@AlrRequest, '&gt;','>')  
  
  
-----------------------------------------------------------------------------------ALR request ---------------------end----------------------  
  
-------- Sending Mail ---------------------                                
Declare @HTML varchar(max)                                
declare @ticketid int                              
                               
-------------------------------------------------ALR request Alert--------------------  
  
set @HTML = ''  
  
if((select Count(*) from #tblALRrequest) > 0)                                      
 begin                                      
  set @HTML =@HTML + @AlrRequest                                  
    END                           
if((select Count(*) from #tblALRrequest) <= 0)                                      
 begin                                      
  set @HTML =@HTML + @html1 + @AlrRequestTitle  +'<table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black"><tr><td align="center"><font color="#3366cc"><STRONG>NO RECORDS FOUND</STRON
G></font> </td></tr></table>'                                  
    END   
  
---------------------------------------------------------------------ALR request end  
--new job and profile is required to be created ,consult Mr Fahim to varify  
--  Create a new email group titled ALRAlert@sullolaw.com.   
-- This email needs to go to Natisha, Greg, Sandra, Penelope, Beverly and Cathia.   
-- This alert needs to go out daily at 3:30pm and 4:45pm  
  
if ( select Count(*) from #tblALRrequest ) > 0  
Begin  
  
EXEC msdb.dbo.sp_send_dbmail                                   
  @profile_name = 'ALR Alerts',                                    
  @recipients =@EmailAddress ,                                  
  @subject = @Subject,                                   
  @body = @HTML,                                  
  @body_format = 'HTML' ;    
  
End                                
--------                               
--END     
--  
--EXEC msdb.dbo.sp_send_dbmail                                   
--      @profile_name = 'faraz',                                   
--      @recipients =   @EmailAddress,                                         
--      @subject = @Subject,                                   
--      @body = @HTML,                                  
--      @body_format = 'HTML' ;                                          
                              
  
--print (@HTML)    
--print(@html1)  
        
---select * from #tblALRrequest  
                  
drop table #tblALRrequest  
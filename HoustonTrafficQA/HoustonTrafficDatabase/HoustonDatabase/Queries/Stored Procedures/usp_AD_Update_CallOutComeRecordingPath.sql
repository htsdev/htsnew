﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to update recording file path location by call id. 
				
List of Parameters:	
	@CallID		
	@RecordingPath
	
*******/

CREATE procedure dbo.usp_AD_Update_CallOutComeRecordingPath

@CallID int,
@RecordingPath varchar(200)

as

update AutoDialerCallOutcome
set RecordingPath=@RecordingPath
where CallID=@CallID

go

grant exec on dbo.usp_AD_Update_CallOutComeRecordingPath to dbr_webuser
go
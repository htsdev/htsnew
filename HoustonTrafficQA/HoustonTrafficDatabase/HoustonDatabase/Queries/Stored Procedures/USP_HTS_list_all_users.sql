
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_HTS_list_all_users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_HTS_list_all_users]
go
--khalid 3188 2/20/08 service ticket admin
create procedure [dbo].[USP_HTS_list_all_users]   
  
@status varchar(1),  
@orderby varchar(20)  
  
  
as  
  
declare @temp varchar(2000)  
  
set @temp = 'select firstname,lastname,abbreviation,username,password,access=          
case accesstype when 1 then ''Secondary'' when 2 then ''Primary'' else ''Read Only''end  
,employeeid  ,         
case canupdatecloseoutlog when 0 then ''No'' else ''Yes'' end  as canupdatecloseoutlog, email,       
case flagCanSPNSearch       
when 1 then ''Yes''      
else ''No''      
end as CanSPNSearch  ,    
    
case status when 1 then ''Active'' else ''Inactive'' end  as status,  
isnull(NTUserID,'''') as NTUserID, 
case IsAttorney when 1 then ''Yes'' else ''No'' end  as IsAttorney,


case isServiceTicketAdmin when 1 then ''Yes'' else ''No'' end  as AccessST
  
from tblusers          
    
where status = ' + @status   + '  
    
order by '+ @orderby   
    
  
exec (@temp)  
go


GO
GRANT EXEC ON [dbo].[USP_HTS_list_all_users] TO [dbr_webuser]
GO  
  


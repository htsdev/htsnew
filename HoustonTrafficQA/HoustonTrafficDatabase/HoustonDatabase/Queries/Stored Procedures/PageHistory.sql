USE [TrafficTickets]
GO
/****** Object:  Table [dbo].[PageHistory]    Script Date: 06/17/2008 19:24:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageHistory](
	[PageHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[SubMenuId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[ViewDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PageHistory] PRIMARY KEY CLUSTERED 
(
	[PageHistoryId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
go


grant select on [PageHistory] to [dbr_webuser]
go
 
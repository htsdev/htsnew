SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ADDAttorneyAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ADDAttorneyAddress]
GO


CREATE Procedure USP_HTS_ADDAttorneyAddress  
  
(  
  
@Email varchar(50)  
  
  
)  
as  
  
declare @i int 

Select @i=0 



if not exists( select Emailaddress from attorneyaddress where EmailAddress = @Email)
Begin 
Insert into attorneyaddress values ( @Email)
Select @i = 1 
end


select @i





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


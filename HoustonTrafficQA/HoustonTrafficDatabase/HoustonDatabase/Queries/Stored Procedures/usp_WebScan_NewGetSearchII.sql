SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_NewGetSearchII]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_NewGetSearchII]
GO


 -- usp_WebScan_NewGetSearch '03/09/2007','03/09/2007 23:59:59:998',0         
CREATE procedure [dbo].[usp_WebScan_NewGetSearchII]           
          
@StarDate as datetime,              
@EndDate as datetime,        
@checkStatus as int            
          
as          
          
declare @main table( batchid int , ImagesCount int )                  
declare @Verified table(batchid int, verifiedCount int)          
declare @Queued table(batchid int, QueuedCount int)          
declare @Discrepancy table(batchid int, DiscrepancyCount int)          
declare @Pending table(batchid int, PendingCount int)    
declare @NoCause table(batchid int, NoCauseCount int)          
        
if @checkStatus = 0        
begin          
Insert into @main Select batchid , count(id) as ImagesCount from tbl_WebScan_Pic group by batchid                  
          
          
insert into @Verified select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=4 group by p.batchid                  
          
          
insert into @Queued select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=3 group by p.batchid                  
          
          
insert into @Discrepancy select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=2 group by p.batchid                  
          
          
insert into @Pending select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=1 group by p.batchid                  
    
insert into @NoCause select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=5 group by p.batchid         
          
          
select distinct b.batchid,          
    isnull(v.verifiedCount,0) as verifiedCount,          
    isnull(m.ImagesCount,0) as ImagesCount,          
    isnull(q.QueuedCount,0)as QueuedCount,          
    isnull(d.DiscrepancyCount,0) as DiscrepancyCount,          
    isnull(p.PendingCount,0) as PendingCount,          
 isnull(c.NoCauseCount,0) as NoCauseCount,     
    b.scantype,          
    convert(varchar(12),b.scandate,101) as scandate          
          
from tbl_WebScan_Batch b left outer  join           
  @main m on           
  b.batchid=m.batchid left outer  join           
  @Verified v on           
  b.batchid=v.batchid left outer  join           
  @Queued q on           
  b.batchid=q.batchid left outer join           
  @Discrepancy d on           
  b.batchid=d.batchid left outer  join          
  @Pending p on           
  b.batchid=p.batchid left outer  join          
  @NoCause c on           
  b.batchid=c.batchid           
          
where
b.scantype='Resets'       
and
(               
 b.scandate between @StarDate and Convert(datetime,@EndDate+' 23:59:59:998')  
)
order by b.batchid desc           
end        
         
else if @checkStatus=1  --for non verified Batches        
begin        
           
Insert into @main Select batchid , count(id) as ImagesCount from tbl_WebScan_Pic group by batchid                  
          
          
insert into @Verified select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=4 group by p.batchid                  
          
          
insert into @Queued select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=3 group by p.batchid                  
          
          
insert into @Discrepancy select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=2 group by p.batchid                  
          
          
insert into @Pending select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=1 group by p.batchid                  
     
insert into @NoCause select p.batchid,count(p.id) from tbl_webscan_Pic p inner join           
 tbl_Webscan_ocr o on p.id=o.picid where o.checkstatus=5 group by p.batchid     
          
select distinct b.batchid,          
    isnull(v.verifiedCount,0) as verifiedCount,          
    isnull(m.ImagesCount,0) as ImagesCount,          
    isnull(q.QueuedCount,0)as QueuedCount,          
    isnull(d.DiscrepancyCount,0) as DiscrepancyCount,          
    isnull(p.PendingCount,0) as PendingCount,    
 isnull(c.NoCauseCount,0) as NoCauseCount,          
    b.scantype,          
    convert(varchar(12),b.scandate,101) as scandate          
          
from tbl_WebScan_Batch b left outer  join           
  @main m on           
  b.batchid=m.batchid left outer  join           
  @Verified v on           
  m.batchid=v.batchid left outer  join           
  @Queued q on           
  m.batchid=q.batchid left outer join           
  @Discrepancy d on           
  m.batchid=d.batchid left outer  join          
  @Pending p on           
  m.batchid=p.batchid left outer  join          
  @NoCause c on           
  m.batchid=c.batchid           
          
where               
b.scantype='Resets'       
and
(
	m.imagescount>v.verifiedCount      
	or      
	q.QueuedCount>0      
	or      
	d.DiscrepancyCount>0      
	or      
	p.PendingCount>0      
	or      
	c.NoCauseCount>0      
)
order by b.batchid desc      
        
end      


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/************************************************************
Created by:		Syed Muhammad Ozair
Business Logic:	this procedure is used to verify (updates setting info from Resets updated from BSDA, BSDAII)
				records/case and then returns a result set for records which are updated through this process.
				Also IsCourtSettingInfoChangedDuringBSDA is reset for provided cases and then it is set in 
				case of any change in court date and court number from previous record

List of Parameters:				
	@PicID:	 
	@CauseNo:
	@CourtDate:
	@CourtTime:
	@CourtNo:
	@Status:
	@Location:
	@CourtDateScan:
	@EmployeeID:
	@Msg: out put variable
	
 ************************************************************/

ALTER PROCEDURE [dbo].[usp_webscan_verified]
	@PicID INT, 
	 --ozair 4330 07/02/2008 causeno length increased to 200 from 50
	@CauseNo VARCHAR(200),
	@CourtDate AS DATETIME, 
	@CourtTime AS VARCHAR(15), 
	@CourtNo AS INT, 
	@Status AS VARCHAR(50), 
	@Location AS VARCHAR(50), 
	@CourtDateScan AS DATETIME, 
	@EmployeeID AS INT, 
	@Msg AS INT OUTPUT 
	
	
	
	AS 
	
	--ozair 4330 07/2/2008 modified to implement more then one cause no in one reset
	SET @Msg = 0 
	DECLARE @temp TABLE
	        (id INT IDENTITY(1, 1) NOT NULL, causeno VARCHAR(50) NULL)
	
	INSERT INTO @temp
	  (
	    causeno
	  )(
	       SELECT sValue
	       FROM   dbo.splitstring(@CauseNo, ',')
	   )
	
	DECLARE @count INT
	DECLARE @item INT 
	
	SET @item = 1
	SELECT @count = COUNT(*)
	FROM   @temp
	
	--Ozair 5579 04/14/2009 ReSetting IsCourtSettingInfoChangedDuringBSDA flag
	DECLARE @count_reset INT
	DECLARE @item_reset INT
	SET @item_reset = 1
	SELECT @count_reset = COUNT(*)
	FROM   @temp
	
	WHILE @count_reset >= @item_reset
	BEGIN
	    UPDATE tblTickets
	    SET    IsCourtSettingInfoChangedDuringBSDA = 0
	    WHERE  ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                           FROM   tbltickets t
	                                  INNER JOIN tblticketsviolations 
	                                       tv
	                                       ON  t.ticketid_pk = tv.ticketid_pk
	                           WHERE  t.activeflag = 1
	                                  AND tv.casenumassignedbycourt = (
	                                          SELECT causeno
	                                          FROM   @temp
	                                          WHERE  id = @item_reset
	                                      ))
	    
	    SET @item_reset = @item_reset + 1
	END
	--end Ozair 5579
	
	WHILE @count >= @item
	BEGIN
	    DECLARE @TempCauseNo AS VARCHAR(50)             
	    DECLARE @TempCauseNoCount INT       
	    DECLARE @ProblemMsg VARCHAR(100) 
	    
	    --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	    DECLARE @OldVerifiedStatus INT
	    DECLARE @OldVerifiedCourtDate DATETIME
	    DECLARE @OldVerifiedCourtNumber VARCHAR(3)
	    
	    IF @Location = 'HMC'
	    BEGIN
	        SELECT @TempCauseNo = tv.casenumassignedbycourt,
	               --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	               @OldVerifiedStatus = tv.CourtViolationStatusIDmain,
	               @OldVerifiedCourtDate = tv.CourtDateMain,
	               @OldVerifiedCourtNumber = LTRIM(RTRIM(ISNULL(tv.CourtNumbermain, '')))
	        FROM   tblticketsviolations tv
	               INNER JOIN tbltickets t
	                    ON  t.ticketid_pk = tv.ticketid_pk
	        WHERE  tv.casenumassignedbycourt = (
	                   SELECT causeno
	                   FROM   @temp
	                   WHERE  id = @item
	               )
	               AND tv.courtid IN (3001, 3002, 3003)
	               AND t.activeflag = 1        
	        
	        SELECT @TempCauseNoCount = COUNT(tv.casenumassignedbycourt)
	        FROM   tblticketsviolations tv
	               INNER JOIN tbltickets t
	                    ON  t.ticketid_pk = tv.ticketid_pk
	        WHERE  tv.casenumassignedbycourt = (
	                   SELECT causeno
	                   FROM   @temp
	                   WHERE  id = @item
	               )
	               AND tv.courtid IN (3001, 3002, 3003)
	               AND t.activeflag = 1
	    END
	    ELSE 
	    IF @Location <> 'HMC'
	    BEGIN
	        SELECT @TempCauseNo = tv.casenumassignedbycourt,
	               --Ozair 5799 04/14/2009 Getting Old Settings Info before update
	               @OldVerifiedStatus = tv.CourtViolationStatusIDmain,
	               @OldVerifiedCourtDate = tv.CourtDateMain,
	               @OldVerifiedCourtNumber = LTRIM(RTRIM(ISNULL(tv.CourtNumbermain, '')))
	        FROM   tblticketsviolations tv
	               INNER JOIN tbltickets t
	                    ON  t.ticketid_pk = tv.ticketid_pk
	        WHERE  tv.casenumassignedbycourt = (
	                   SELECT causeno
	                   FROM   @temp
	                   WHERE  id = @item
	               )
	               AND tv.courtid NOT IN (3001, 3002, 3003)
	               AND t.activeflag = 1           
	        
	        SELECT @TempCauseNoCount = COUNT(tv.casenumassignedbycourt)
	        FROM   tblticketsviolations tv
	               INNER JOIN tbltickets t
	                    ON  t.ticketid_pk = tv.ticketid_pk
	        WHERE  tv.casenumassignedbycourt = (
	                   SELECT causeno
	                   FROM   @temp
	                   WHERE  id = @item
	               )
	               AND tv.courtid NOT IN (3001, 3002, 3003)
	               AND t.activeflag = 1
	    END            
	    
	    IF @TempCauseNoCount > 1
	    BEGIN
	        SET @ProblemMsg = 'Duplicate Cause No'      
	        SET @Msg = 1
	    END
	    ELSE 
	    IF @TempCauseNoCount = 0
	    BEGIN
	        SET @ProblemMsg = 'Not in System'       
	        SET @Msg = 1
	    END
	    
	    IF @TempCauseNo <> 'NULL'
	       AND @TempCauseNoCount = 1
	    BEGIN
	        DECLARE @TempCID AS VARCHAR(3)                                    
	        DECLARE @CourtNumScan AS VARCHAR(50)                                    
	        
	        SET @TempCID = @CourtNo                                    
	        
	        IF @TempCID = '13'
	           OR @TempCID = '14'
	        BEGIN
	            SET @CourtNumScan = '3002'
	        END
	        ELSE 
	        IF @TempCID = '18'
	        BEGIN
	            SET @CourtNumScan = '3003'
	        END
	        ELSE
	        BEGIN
	            SET @CourtNumScan = '3001'
	        END                                    
	        
	        DECLARE @tempCVID INT 
	        
	        --Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status                                    
	        IF @Status = 'JURY TRIAL'
	            SET @tempCVID = 26
	        ELSE 
	        IF @Status = 'JUDGE'
	            SET @tempCVID = 103
	        ELSE 
	        IF @Status = 'PRETRIAL'
	            SET @tempCVID = 101
	        ELSE 
	        IF @Status = 'ARRAIGNMENT'
	            SET @tempCVID = 3
	        ELSE 
	        IF @Status = 'WAITING'
	            SET @tempCVID = 104                  
	        
	        DECLARE @TodayDate AS DATETIME                                    
	        SET @TodayDate = GETDATE()                            
	        
	        BEGIN
	            --ozair 5799 04/14/2009	setting IsCourtSettingInfoChanged field	            
	            IF (
	                   @OldVerifiedStatus <> @tempCVID
	                   OR @OldVerifiedCourtDate <> @CourtDateScan
	                   OR @OldVerifiedCourtNumber <> CONVERT(VARCHAR(3), @CourtNo)
	               )
	            BEGIN
	                UPDATE tblTickets
	                SET    IsCourtSettingInfoChangedDuringBSDA = 1
	                WHERE  ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                       FROM   tbltickets t
	                                              INNER JOIN 
	                                                   tblticketsviolations 
	                                                   tv
	                                                   ON  t.ticketid_pk = tv.ticketid_pk
	                                       WHERE  t.activeflag = 1
	                                              AND tv.casenumassignedbycourt = (
	                                                      SELECT causeno
	                                                      FROM   @temp
	                                                      WHERE  id = @item
	                                                  ))
	            END
	            
	            IF @Location = 'HMC'
	            BEGIN
	                UPDATE tblticketsviolations
	                SET    CourtDateMain = @CourtDateScan,
	                       CourtNumberMain = @CourtNo,
	                       CourtViolationStatusIDmain = @tempCVID,
	                       VerifiedStatusUpdateDate = @TodayDate,
	                       CourtID = @CourtNumScan,
	                       CourtDate = @CourtDateScan,
	                       CourtNumber = @CourtNo,
	                       CourtViolationStatusID = @tempCVID,
	                       CourtDateScan = @CourtDateScan,
	                       CourtViolationStatusIDScan = @tempCVID,
	                       CourtNumberScan = @CourtNo,
	                       ScanUpdatedate = @TodayDate,
	                       CourtIDScan = @CourtNumScan,
	                       Updateddate = @TodayDate,
	                       vemployeeid = @EmployeeID,
	                       BSDAUpdateDate = GETDATE()
	                WHERE  casenumassignedbycourt = (
	                           SELECT causeno
	                           FROM   @temp
	                           WHERE  id = @item
	                       )
	                       AND CourtID IN (3001, 3002, 3003)
	                       AND ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                           FROM   tbltickets t
	                                                  INNER JOIN 
	                                                       tblticketsviolations 
	                                                       tv
	                                                       ON  t.ticketid_pk = 
	                                                           tv.ticketid_pk
	                                           WHERE  t.activeflag = 1
	                                                  AND tv.casenumassignedbycourt = (
	                                                          SELECT causeno
	                                                          FROM   @temp
	                                                          WHERE  id = @item
	                                                      ))
	            END
	            ELSE 
	            IF @Location <> 'HMC'
	            BEGIN
	                IF @tempCVID <> 104
	                BEGIN
	                    UPDATE tblticketsviolations
	                    SET    CourtDateMain = @CourtDateScan,
	                           CourtViolationStatusIDmain = @tempCVID,
	                           VerifiedStatusUpdateDate = @TodayDate,
	                           CourtDate = @CourtDateScan,
	                           CourtViolationStatusID = @tempCVID,
	                           CourtDateScan = @CourtDateScan,
	                           CourtViolationStatusIDScan = @tempCVID,
	                           ScanUpdatedate = @TodayDate,
	                           Updateddate = @TodayDate,
	                           vemployeeid = @EmployeeID,
	                           BSDAUpdateDate = GETDATE()
	                    WHERE  casenumassignedbycourt = (
	                               SELECT causeno
	                               FROM   @temp
	                               WHERE  id = @item
	                           )
	                           AND CourtID NOT IN (3001, 3002, 3003)
	                           AND ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                               FROM   tbltickets t
	                                                      INNER JOIN 
	                                                           tblticketsviolations 
	                                                           tv
	                                                           ON  t.ticketid_pk = 
	                                                               tv.ticketid_pk
	                                               WHERE  t.activeflag = 1
	                                                      AND tv.casenumassignedbycourt = (
	                                                              SELECT causeno
	                                                              FROM   @temp
	                                                              WHERE  id = @item
	                                                          ))
	                END
	                ELSE
	                BEGIN
	                    UPDATE tblticketsviolations
	                    SET    CourtViolationStatusIDmain = @tempCVID,
	                           CourtViolationStatusID = @tempCVID,
	                           CourtViolationStatusIDScan = @tempCVID,
	                           ScanUpdatedate = @TodayDate,
	                           Updateddate = @TodayDate,
	                           vemployeeid = @EmployeeID,
	                           BSDAUpdateDate = GETDATE()
	                    WHERE  casenumassignedbycourt = (
	                               SELECT causeno
	                               FROM   @temp
	                               WHERE  id = @item
	                           )
	                           AND CourtID NOT IN (3001, 3002, 3003)
	                           AND ticketid_pk IN (SELECT DISTINCT t.ticketid_pk
	                                               FROM   tbltickets t
	                                                      INNER JOIN 
	                                                           tblticketsviolations 
	                                                           tv
	                                                           ON  t.ticketid_pk = 
	                                                               tv.ticketid_pk
	                                               WHERE  t.activeflag = 1
	                                                      AND tv.casenumassignedbycourt = (
	                                                              SELECT causeno
	                                                              FROM   @temp
	                                                              WHERE  id = @item
	                                                          ))
	                END
	            END            
	            
	            IF @Msg = 0
	            BEGIN
	                UPDATE tbl_webscan_ocr
	                SET    causeno = @CauseNo,
	                       STATUS = @Status,
	                       NewCourtDate = CONVERT(VARCHAR(12), @CourtDate, 101),
	                       CourtNo = @CourtNo,
	                       Location = @Location,
	                       TIME = @CourtTime,
	                       checkstatus = 4,
	                       ProblemMsg = ''
	                WHERE  picid = @PicID                            
	                
	                UPDATE tbl_webscan_data
	                SET    CauseNo = @CauseNo,
	                       CourtDate = @CourtDate,
	                       CourtTime = @CourtTime,
	                       CourtRoomNo = @CourtNo,
	                       STATUS = @Status,
	                       CourtLocation = @Location
	                WHERE  PicID = @PicID                             
	                
	                SET @Msg = 2
	            END
	        END
	    END                        
	    
	    IF @Msg = 1
	    BEGIN
	        UPDATE tbl_webscan_ocr
	        SET    causeno = @CauseNo,
	               STATUS = @Status,
	               NewCourtDate = CONVERT(VARCHAR(12), @CourtDate, 101),
	               CourtNo = @CourtNo,
	               Location = @Location,
	               TIME = @CourtTime,
	               checkstatus = 5,
	               ProblemMsg = @ProblemMsg
	        WHERE  picid = @PicID                            
	        
	        UPDATE tbl_webscan_data
	        SET    CauseNo = @CauseNo,
	               CourtDate = @CourtDate,
	               CourtTime = @CourtTime,
	               CourtRoomNo = @CourtNo,
	               STATUS = @Status,
	               CourtLocation = @Location
	        WHERE  PicID = @PicID
	    END
	    ELSE
	    BEGIN
	        SET @Msg = 0
	    END
	    
	    SET @item = @item + 1
	END      
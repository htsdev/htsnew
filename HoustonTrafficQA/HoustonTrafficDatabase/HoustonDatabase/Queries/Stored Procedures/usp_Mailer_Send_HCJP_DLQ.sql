
/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for HCJP DLQ letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@empid:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	ZipMid:			Zip Code's first five characters & mid number.
	CourtDate:		Court date for the violation.
	CourtId:		Court Identification associated with the violation.
	Abb:			Short abbreviation of employee who is printing the letter
	Language:		Flag to print Sinlge sided (English) or Double sided (Both English & Spanish) letters.
	ChkGroup:		Record ID + language flag. used to group the records on report file.

******/

-- usp_Mailer_Send_HCJP_DLQ 2, 53,  2, '5/9/2008,' , 3991, 0 ,0,0,0
      
ALTER proc [dbo].[usp_Mailer_Send_HCJP_DLQ]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int  ,
@language int = 0,
@isprinted bit                               
                                    
                                    
as                                    
                                          
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     

-- INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
declare @sqlquery varchar(max), @sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''
                                            
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  


-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...                                    
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	convert(varchar(10),tva.bonddate,101) as listdate,
	--tva.courtdate,
	ta.violationdate as courtdate,
	ta.courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	case when len(isnull(tva.causenumber,''''))>0 then tva.causenumber else tva.TicketNumber_PK end as TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      	upper(ta.city) as city,
	s.state,
	dbo.tblCourts.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,                                 
	count(tva.ViolationNumber_PK) as ViolCount,
	sum(isnull(tva.fineamount,0))as Total_Fineamount 
into #temp                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
	dbo.tblCourts 
ON 	ta.CourtID = dbo.tblCourts.Courtid 
INNER JOIN
tblstate S ON
ta.stateid_fk = S.stateID

-- ONLY DLQ CASES......
where	tva.violationstatusid = 146  

-- NOT A JUVENILE CASE...
and		isnull(tva.juvenileflag,0) =0 

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and		isnull(ta.ncoa48flag,0) = 0 

-- ONLY VERIFIED AND VALID ADDRESSES
and 	Flag1 in (''Y'',''D'',''S'')

-- NOT MARKED AS STOP MAILING...
and 	donotmailflag = 0 
'               
          
-- IF PRINTING LETTERS FOR ALL COURTS FOR SELECTED CATEGORY....
if( @crtid= @catnum ) -- Adil 8928 02/17/2011 HCJP 7-1(3019) Excluded from Printing.          
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = '+convert(varchar, @catnum)+' AND InActiveFlag = 0  and courtid <> 3019)' --Sabir Khan 8208 09/22/2010 Filter out Inactive courts

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else 
	set @sqlquery =@sqlquery +'                                  
	and 	tva.courtlocation = ' + convert(varchar(10),@crtid) + ' and tva.courtlocation <> 3019 '                                
                                  
-- GET RECORDS RELATED ONLY TO THE SELECTED DATE RANGE.....
if(@startListdate<>'')                                    
	set @sqlquery=@sqlquery+'
	and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.bonddate' ) 


-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                    
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery + '  
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                    
if(@ticket_no<>'')                                    
	set @sqlquery=@sqlquery+ '
	and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          

-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                    
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'               if(@violadesc<>'')              

-- VIOLATION DESCRIPTION FILTER...
-- EXCLUDE VIOLATIONS SPECIFIED IN THE FILTER...
if(@violadesc<>'')  
	set @sqlquery=@sqlquery+' 
	and	('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'           

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
	set @sqlquery =@sqlquery+ '
	and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   


-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )              
	set @sqlquery =@sqlquery+ '
	and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   


-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (                                                                    
	select	n.recordid from tblletternotes n inner join tblticketsarchive t on t.recordid  = n.recordid
	where 	n.lettertype ='+convert(varchar(10),@lettertype)+'   
	)
	
group by  
	ta.recordid, convert(varchar(10),tva.bonddate,101), ta.violationdate, ta.courtid, ta.Flag1, ta.officerNumber_Fk,
	case when len(isnull(tva.causenumber,''''))>0 then tva.causenumber else tva.TicketNumber_PK end, ta.donotmailflag, ta.clientflag, violationstatusid, upper(firstname), upper(lastname), upper(address1) + '''' + isnull(ta.address2,'''') ,
	upper(address1), isnull(ta.address2,''''), upper(ta.city), s.state,  tblCourts.CourtName, zipcode, midnumber, dp2,
	dpc, violationstatusid,	violationdate, left(zipcode,5) + rtrim(ltrim(midnumber)), ViolationDescription, FineAmount '              
                            
-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
set @sqlquery=@sqlquery+'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                              
city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
courtid,zipmid,donotmailflag,clientflag, zipcode, courtdate into #temp1  from #temp where 1=1                                    

-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624

'              

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)              
	set @sqlquery =@sqlquery+ '
	and'+ @singlevoilationOpr+ '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1) or violcount>3
	'

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                   
if(@doubleviolation<>0 and @singleviolation=0 )              
	set @sqlquery =@sqlquery + '
	and'+ @doublevoilationOpr+ '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)  or violcount>3  
	'

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...      
if(@doubleviolation<>0 and @singleviolation<>0)              
	set @sqlquery =@sqlquery+ '
	and'+ @singlevoilationOpr+ '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)
	or '+ @doublevoilationOpr+'   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)  or violcount>3  
	'

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....         
if( @printtype =  1)   
	begin      
		set @sqlquery2 =@sqlquery2 +                                    
		'
		Declare @ListdateVal DateTime,                                  
		@totalrecs int,                                
		@count int,                                
		@p_EachLetter money,                              
		@recordid varchar(50),                              
		@zipcode   varchar(12),                              
		@maxbatch int  ,
		@lCourtId int  ,
		@dptwo varchar(10),
		@dpc varchar(10)
		declare @tempBatchIDs table (batchid int)

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
		Select @count=Count(*) from #temp1                                

		if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                

		if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                  
		Select distinct mdate  from #temp1                                
		open ListdateCur                                   
		Fetch Next from ListdateCur into @ListdateVal                                                      
		while (@@Fetch_Status=0)                              
			begin                                
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
				('+convert(varchar(10),@empid)+' , '+convert(varchar(10),@lettertype)+' , @ListdateVal, '+convert(varchar(10),@catnum)+' , 0, @totalrecs, @p_EachLetter)                                   

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter   
				insert into @tempBatchIDs select @maxbatch                                                    

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
					begin     

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...                         
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
				Fetch Next from ListdateCur into @ListdateVal                              
			end                                            

		close ListdateCur  
		deallocate ListdateCur 

		-- GETTING RECORDS IN TEMP TABLE......
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
		dptwo, TicketNumber_PK, t.zipcode  , t.zipmid , courtdate  , t.courtid, '''+@user+''' as Abb
		into #tempResult1                            
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by [zipmid]
		'

		-- FOR SINGLE SIDED LETTERS.....	
		if @language = 0
			begin
				-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....	
				set @sqlquery2 = @sqlquery2 + '
				select *, 0 as language, convert(varchar(15), recordid) + ''0'' as ChkGroup
				from #tempResult1 
				'
			end

		-- FOR DOUBLE SIDED LETTERS...
		else if @language = 1
			begin
				-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
				set @sqlquery2 = @sqlquery2 + '
				select *, 0 as language, convert(varchar(15), recordid) + ''0'' as ChkGroup
				from #tempResult1 
				union all
				select *, 1 as language, convert(varchar(15), recordid) + ''1'' as ChkGroup
				from #tempResult1 
				order by  [zipmid]
				'
			end


		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		set @sqlquery2 = @sqlquery2 + '

		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500), @str varchar(2000)
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
		set @str = char(13)+char(13)
		select  @str = @str +  c.shortcourtname + '' - '' + convert(varchar(20),  count(distinct n.noteid)) + '' Letters''+char(13)
		from tblletternotes n, @tempBatchIDs b , tblcourts c
		where n.batchid_fk = b.batchid and c.courtid = n.courtid group by c.shortcourtname
		select @subject = convert(varchar(20), @lettercount) + '' LMS Harris County DLQ Letters Printed''
		select @body = ''TOTAL ''+ convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')+ @STR
		exec usp_mailer_send_Email @subject, @body

		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'
	end

-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
-- TO DISPLAY THE LETTER...
else if( @printtype =  0) 
	begin
		-- FOR SINGLE SIDED LETTERS.....
		if @language = 0 
			begin
				set @sqlquery2 = @sqlquery2 + '
				Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
				LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              				dptwo, TicketNumber_PK, zipcode , zipmid  , courtdate , courtid, '''+@user+''' as Abb ,
				0 as language, convert(varchar(15), recordid) + ''0'' as ChkGroup                            
				from #temp1 
				order by zipmid 
				'
			end
		
		-- FOR DOUBLE SIDED LETTERS.....
		else if @language = 1
			begin
				set @sqlquery2 = @sqlquery2 + '
				Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
				LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              				dptwo, TicketNumber_PK, zipcode , zipmid  , courtdate , courtid, '''+@user+''' as Abb  ,
				0 as language, convert(varchar(15), recordid) + ''0'' as ChkGroup                           
				from #temp1 
				union all
				Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
				LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              				dptwo, TicketNumber_PK, zipcode , zipmid  , courtdate , courtid, '''+@user+''' as Abb,
				1 as language, convert(varchar(15), recordid) + ''1'' as ChkGroup                             
				from #temp1 
				order by zipmid 
				'
			end
	end

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '
drop table #temp 
drop table #temp1'
                                    
--print @sqlquery + @sqlquery2
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)


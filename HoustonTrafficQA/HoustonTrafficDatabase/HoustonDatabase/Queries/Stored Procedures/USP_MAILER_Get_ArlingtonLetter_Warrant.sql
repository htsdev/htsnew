set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Created by:		Sabir Khan  6610 10/01/2009
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Arlington Warrant letter for the selected date range.
				
List of Parameters:
	@startdate:	Starting list date for Arraignment letter.
	@enddate:	Ending list date for Arraignment letter.
	@courtid:	Court id of the selected court category if printing for individual court hosue.
	@bondflag:	Flag if printing bond letters else appearance letter.
	@catnum:	Category number of the selected letter type, if printing for 
				all courts of the selected court category

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/


--  USP_MAILER_Get_ArlingtonLetter_Warrant '10/02/2009', '10/02/2010', 3068, 1, 28
ALTER procedure [dbo].[USP_MAILER_Get_ArlingtonLetter_Warrant]
 (  
 @startdate datetime,  
 @enddate datetime,  
 @courtid int = 6,  
 @bondflag tinyint = 0,  
 @catnum int=6  
 )  
  
as  
  

-- DECLARING LOCAL VARIABLES FOR FILTERS...
declare @officernum varchar(50),                                                      
 @officeropr varchar(50),                                                      
 @tikcetnumberopr varchar(50),                                                      
 @ticket_no varchar(50),                                                                                                    
 @zipcode varchar(50),                                                               
 @zipcodeopr varchar(50),                                                      
 @fineamount money,                                                              
 @fineamountOpr varchar(50) ,                                                              
 @fineamountRelop varchar(50),                                     
 @singleviolation money,                                                              
 @singlevoilationOpr varchar(50) ,                                                              
 @singleviolationrelop varchar(50),                                                      
 @doubleviolation money,                                                              
 @doublevoilationOpr varchar(50) ,                                                              
 @doubleviolationrelop varchar(50),                                                    
 @count_Letters int,                                    
 @violadesc varchar(500),                                    
 @violaop varchar(50),   
 @sqlquery varchar(max) ,   
 @lettertype int                                                   
  
-- INITIALIZING THE LOCAL VARIABLES
set @sqlquery = ''                                                   
set @lettertype = 71  

-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......  
Select  @officernum=officernum,                                                      
 @officeropr=oficernumOpr,                                                      
 @tikcetnumberopr=tikcetnumberopr,                                                      
 @ticket_no=ticket_no,                                                      
 @zipcode=zipcode,                                                      
 @zipcodeopr=zipcodeLikeopr,                                              
 @singleviolation =singleviolation,                                                 
 @singlevoilationOpr =singlevoilationOpr,                                                      
 @singleviolationrelop =singleviolationrelop,                                                      
 @doubleviolation = doubleviolation,                                                      
 @doublevoilationOpr=doubleviolationOpr,                                                              
 @doubleviolationrelop=doubleviolationrelop,                                    
 @violadesc=violationdescription,                                    
 @violaop=violationdescriptionOpr ,                                
 @fineamount=fineamount ,                   
 @fineamountOpr=fineamountOpr ,                                                   
 @fineamountRelop=fineamountRelop                   
from  tblmailer_letters_to_sendfilters                                 
where  courtcategorynum=@catnum      
and   lettertype =   @lettertype                                                
       

-- MAIN QUERY....
-- BOND DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY ARRAIGNMENT (PRIMARY STATUS) CASES
-- INSERTING VALUES IN A TEMP TABLE....  
set @sqlquery=@sqlquery+'                                          
Select distinct tva.recordid,   
     convert(datetime,convert(varchar(10),tva.violationbonddate,101)) as mdate,                       
  flag1 = isnull(ta.Flag1,''N'') ,                                                      
  isnull(ta.donotmailflag,0) as donotmailflag,                                          
  count(tva.ViolationNumber_PK) as ViolCount,                                                      
  isnull(sum(isnull(tva.bondamount,0)),0)as Total_Fineamount  
into #temp                                                            
FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA                       
INNER JOIN                       
 dallastraffictickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid  

-- PERSON NAME SHOULD NOT BE EMPTY..
where ta.lastname is not null   and ta.firstname is not null  

-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
and isnull(ta.ncoa48flag,0) = 0  


-- ONLY BOND CASES....
and tva.violationbonddate is not null   

-- FOR THE SELECTED DATE RANGE ONLY...
and datediff(day, tva.violationbonddate , ''' + convert(varchar(10),@startDate,101)  + ''') <= 0  
and datediff(day, tva.violationbonddate , ''' + convert(varchar(10),@endDate,101)  + ''' )>= 0  
  
  -- Rab Nawaz Khan 8754 02/23/2011 Exclude Disposed Violations
and tva.violationstatusid <> 80
-- End 8754

  
'              
  
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@courtid=@catnum)  
	set @sqlquery=@sqlquery+'   
	and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum ='+convert(varchar,@catnum ) +')'            

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else  
	set @sqlquery=@sqlquery+'   
	and tva.courtlocation =' + convert(varchar,@courtid)  
  
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                      
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '    
	and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'                                                         
                                  
                                                       
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + '   
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ '   
	and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'  
                                 

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '  
    and tva.bondamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
  

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '  
    and not tva.bondamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       


-----------------------------------------------------------------------------------------------------------------------------------------------------

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR APP DAY OF LETTER PRINTED IN PAST TWO WEEKS FOR THE SAME RECORD..  
set @sqlquery =@sqlquery + '                                                
and ta.recordid  not in   
 (                                                                    
select recordid FROM dbo.tblLetterNotes   
where lettertype = 71  
 union  
 -- Babar Ahmad 8597 07/15/2011 Changed logic to 5 business days.
 select recordid from dbo.tblletternotes where lettertype = 70   
 and datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0 
 --Asad Ali 8542 11/23/2010 Exclude cases whose Appearance is Printed in past 5 days
union  
-- Babar Ahmad 8597	07/15/2011 Changed logic to 5 business days.
 select recordid from dbo.tblletternotes where lettertype = 69  
 and datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
 )   
 
 group by                                                       
 convert(datetime,convert(varchar(10),tva.violationbonddate,101)),  
 ta.flag1,  ta.donotmailflag,  tva.recordid  
 having  1=1   
'                                  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT  
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '  
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.bondamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    '                                  

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + '  
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.bondamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )  
      '                                                      
    
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                
if(@doubleviolation<>0 and @singleviolation<>0)                                  
	set @sqlquery =@sqlquery+ '  
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.bondamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.bondamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))  
    '                                    
                                                      
  
-- GETTING THE RESULT SET IN TEMP TABLE.....  
set @sqlquery=@sqlquery+                                         
'Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag   
into #temp1  from #temp a, dallastraffictickets.dbo.tblticketsviolationsarchive tva where a.recordid = tva.recordid   

-- Rab Nawaz Khan 8754 01/21/2011 Exclude Disposed Violations
and tva.violationstatusid <> 80
-- End 8754

 '                                  
  
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')  
	begin  
	-- NOT LIKE FILTER.......  
	if(charindex('not',@violaop)<> 0 )                
		set @sqlquery=@sqlquery+'   
		and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
	  
	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )                
		set @sqlquery=@sqlquery+'   
		and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
end  
  


-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery=@sqlquery+'   
-- Babar Ahmad 9348 08/19/2011 Excluded client cases
--Yasir Kamal 7119 12/10/2009 send warrant letters to existing clients.
--exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)


Select count(distinct recordid) as Total_Count, mDate                                                    
into #temp2                                                  
from #temp1                                                  
group by mDate                                                       
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mDate                                                                                                
into #temp3                                                  
from #temp1                                                      
where Flag1 in (''Y'',''D'',''S'')     
and donotmailflag =  0                                                   
group by mDate   

-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS                                                      
Select count(distinct recordid) as Bad_Count, mDate                                                    
into #temp4                                                      
from #temp1                                                  
where Flag1  not in (''Y'',''D'',''S'')                                                      
and donotmailflag = 0                                                   
group by mDate   
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mDate                                                   
 into #temp5                                                      
from #temp1          
where donotmailflag = 1    
--and flag1  in  (''Y'',''D'',''S'')                                                      
group by mDate   
         

-- OUTPUTTING THE REQURIED INFORMATION........                                                      
Select   listdate = #temp2.mDate ,  
        Total_Count,  
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #Temp2 left outer join #temp3   
on #temp2.mDate = #temp3.mDate  
                                                     
left outer join #Temp4  
on #temp2.mDate = #Temp4.mDate  
    
left outer join #Temp5  
on #temp2.mDate = #Temp5.mDate                                                   
    
where Good_Count > 0                
order by #temp2.mDate desc                                                       
            
-- DROPPING THE TEMPORARY TABLES USED ABOVE.....                                                
drop table #temp                                                  
drop table #temp1                                                  
drop table #temp2                                                   
drop table #temp3                                                  
drop table #temp4  
drop table #temp5  
'                                                      
PRINT @sqlquery

-- EXECUTING THE DYNAMIC SQL QUERY...
exec(@sqlquery)  




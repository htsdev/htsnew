SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Update_LetterPrinter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Update_LetterPrinter]
GO






CREATE PROCEDURE [dbo].[USP_Mailer_Update_LetterPrinter] 
	-- Add the parameters for the stored procedure here
	@LetterId_PK tinyint,
	@printerName varchar(500)
	
AS
BEGIN
	
	Update tblLetter set [printerName] = @printerName, [isAutomated] = 1 where  LetterId_PK = @LetterId_PK
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CreditCardInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CreditCardInfo]
GO

        
  
  
  
CREATE procedure [dbo].[USP_HTS_GET_CreditCardInfo] --111965    --sp to get credit card info  
@ticketid int          
          
as          
          
SELECT         
--  dbo.tblTicketsCybercash.TicketID,       
--dbo.tblTicketsCybercash.amount,      
 dbo.tblTicketsCybercash.ccnum,      
 dbo.tblTicketsCybercash.expdate,         
 UPPER(dbo.tblTicketsCybercash.name) AS NAME,      
 dbo.tblTicketsCybercash.cin,      
-- dbo.tblTicketsCybercash.cardtype,       
 dbo.tblCCType.CCType,         
 dbo.tblTicketsCybercash.auth_code,       
dbo.tblTicketsCybercash.transnum,  
dbo.tblTicketsCybercash.response_reason_text       
FROM         dbo.tblTicketsCybercash left outer JOIN        
                      dbo.tblCCType ON dbo.tblTicketsCybercash.cardtype = dbo.tblCCType.CCTypeID        
WHERE     (dbo.tblTicketsCybercash.TicketID = @ticketid)       
order by recdate desc  
  
----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


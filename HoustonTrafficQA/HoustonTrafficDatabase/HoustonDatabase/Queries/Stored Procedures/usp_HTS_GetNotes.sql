SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetNotes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetNotes]
GO











CREATE PROCEDURE  usp_HTS_GetNotes  
  
@TicketID int  
  
 AS  
  
SELECT    n.Subject,   
      n.Recdate,  
 upper(   u.Abbreviation) as abbreviation,  
 upper(   u.lastname ) as lastname 
FROM          
    dbo.tblTicketsNotes n  
INNER JOIN  
           dbo.tblUsers u   
ON n.EmployeeID = u.EmployeeID  
where   
    TicketID=@TicketID 
and isnull(notetype,0) = 0
order by   
    n.Recdate desc, n.notesid_pk desc  
  
SELECT     
 ISNULL(T.Firstname, '') AS Firstname,                           
 ISNULL(T.Lastname, '') AS Lastname,                               
 ISNULL(T.Midnum, '') AS Midnum,                           
 dbo.fn_HTS_Get_CustomerInfoByMidnumber(T.TicketID_PK) AS CaseCount                           
                          
FROM     tblTickets T                   
where                    
(T.TicketID_PK = @TicketID)

SELECT    n.Subject,   
      n.Recdate,  
 upper(   u.Abbreviation) as abbreviation,  
 upper(   u.lastname ) as lastname 
FROM          
    dbo.tblTicketsNotes n  
INNER JOIN  
           dbo.tblUsers u   
ON n.EmployeeID = u.EmployeeID  
where   
    TicketID=@TicketID 
and notetype = 1
order by   
    n.Recdate desc, n.notesid_pk desc  







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_SET_OSCARMAILFLAG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_SET_OSCARMAILFLAG]
GO


CREATE PROCEDURE [dbo].[USP_HTS_SET_OSCARMAILFLAG]  --164638 
@TicketID int,
@OscarMailFlag int  
as  
Update tbltickets set OscarMailFlag = @OscarMailFlag where ticketid_pk = @TicketID and ActiveFlag = 1 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get get all sources for patients related to Hip Recall.
* List of Parameter :
* Column Return :     
******/
create PROCEDURE [dbo].[USP_HTP_GetPatientSource]
AS
	SELECT s.Id,
	       s.Description
	FROM   Source  s
GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetPatientSource] TO dbr_webuser
GO 	    
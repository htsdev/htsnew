
/****** 
Altered by:		Sabir Khan
Business Logic:	The procedure is used by LMS application to get data for Business Startup letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.	
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	Business Name:	Business Name that will appear on the letter
	
	BusinessAddress:Business address that will appear on the letter
	OwnerAddress:   Owner address that will appear on the letter
	City:			Business and Owner City that will appear on the letter
	State:			Business and Owner State that will appear on the letter	
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter	
	ZipCode:		Person's Home Zip code.
	ZipMid:			First five characters of zip code + mid number.	
	Abb:			Short abbreviation of employee who is printing the letter
*******/

--SELECT * FROM AssumedNames.dbo.Business WHERE BusinessID = 247306
--SELECT * FROM AssumedNames.dbo.BusinessOwner WHERE BusinessID = 247306

-- usp_Mailer_Send_Businesses 24, 47, 24, '04/30/2012,', 3992,0, 0, 0

CREATE procedure [dbo].[usp_Mailer_Send_Businesses] 
@catnum int=24, 
@LetterType int=47, 
@crtid int=24, 
@startListdate varchar (500) ='01/04/2006', 
@empid int=2006, 
@printtype int =0 , 
@searchtype int , 
@isprinted bit 
 
as 
 
 
declare @officernum varchar(50), 
@officeropr varchar(50), 
@tikcetnumberopr varchar(50), 
@ticket_no varchar(50), 
@zipcode varchar(50), 
@zipcodeopr varchar(50), 
 
@fineamount money, 
@fineamountOpr varchar(50) , 
@fineamountRelop varchar(50), 
 
@singleviolation money, 
@singlevoilationOpr varchar(50) , 
@singleviolationrelop varchar(50), 
 
@doubleviolation money, 
@doublevoilationOpr varchar(50) , 
@doubleviolationrelop varchar(50), 
@count_Letters int, 
@violadesc varchar(500), 
@violaop varchar(50) 
 
declare @sqlquery varchar(max), 
 @sqlquery2 varchar(max) 
 
Select @officernum=officernum, 
@officeropr=oficernumOpr, 
@tikcetnumberopr=tikcetnumberopr, 
@ticket_no=ticket_no, 
@zipcode=zipcode, 
@zipcodeopr=zipcodeLikeopr, 
@singleviolation =singleviolation, 
@singlevoilationOpr =singlevoilationOpr, 
@singleviolationrelop =singleviolationrelop, 
@doubleviolation = doubleviolation, 
@doublevoilationOpr=doubleviolationOpr, 
@doubleviolationrelop=doubleviolationrelop, 
@violadesc=violationdescription, 
@violaop=violationdescriptionOpr , 
@fineamount=fineamount , 
@fineamountOpr=fineamountOpr , 
@fineamountRelop=fineamountRelop 
 
from tblmailer_letters_to_sendfilters 
where courtcategorynum=@catnum 
and Lettertype=@LetterType 
 
Select @sqlquery ='' , @sqlquery2 = '' 
 
declare @user varchar(10) 
select @user = upper(abbreviation) from tblusers where employeeid = @empid 
 
set @sqlquery=@sqlquery+' 
Select distinct ta.businessid as recordid,' 
if @searchtype = 0 
 set @sqlquery = @sqlquery + ' 
 convert(varchar(10),ta.registrationdate,101) as listdate, ' 
else 
 set @sqlquery = @sqlquery + ' 
 convert(varchar(10),tva.courtdate,101) as listdate, ' 
 
set @sqlquery = @sqlquery + ' 
count(tva.ownernumber) as ViolCount, 
0 as Total_Fineamount 
into #temp2 
FROM assumednames.dbo.businessowner tva 
INNER JOIN 
 assumednames.dbo.business ta 
ON tva.businessid = ta.businessid
where tva.Flags in (''Y'',''D'',''S'')
and ta.Flags in (''Y'',''D'',''S'') -- Sabir Khan 9948 12/28/2011 Fixed barcode issue.  
and ta.businessid not in (Select businessid from assumednames.dbo.businessowner Where DP2=''21'' AND dpc=''3'' AND ta.FLAGS=''Y'')
and ta.nomailflag = 0 and tva.nomailflag = 0 
and ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+ @startlistdate + '', 'ta.registrationdate' )
 
set @sqlquery =@sqlquery+ ' 
and not exists ( 
 select recordid from tblletternotes 
 where lettertype ='+convert(varchar(10),@lettertype)+' and recordid = ta.businessid
 ) 
group by 
 ta.businessid, convert(varchar(10),ta.registrationdate,101) ' 
 
set @sqlquery=@sqlquery+' 
 
Select * into #temp From (Select distinct a.recordid, 
a.listdate, 
flag1 = isnull(ta.Flags,''N'') , 
case 
when isnull(ta.nomailflag, 0) = 1 or isnull(tva.nomailflag, 0) = 1 then 1 
when isnull(ltrim(rtrim(ta.Address)),'''') = '''' or isnull(ltrim(rtrim(tva.OwnerAddress)),'''') = '''' then 1 else 0 end as donotmailflag, --9921 Farrukh 11/23/2011 do not mail when address is empty
upper(ta.BusinessName) as businessname, 
upper(ta.Address) as address, 
upper(ta.City) as city, 
upper(ta.State) as state, 
ta.Zip as zipcode, 
tva.dp2 as dptwo, 
tva.dpc ,
upper(tva.OwnerAddress) as OwnerAddress, 
upper(tva.OwnerCity) as OwnerCity, 
upper(tva.OwnerState) as OwnerState, 
tva.OwnerZip as OwnerZip, 
tva.dp2 as Ownerdptwo, 
tva.dpc as Ownerdpc,
tva.ownername,
ta.filenumber,
ta.err_stat ,
a.total_fineamount,
a.violcount  
from assumednames.dbo.business ta, assumednames.dbo.businessowner tva, #temp2 a
where ta.businessid = tva.businessid
and ta.businessid = a.recordid
and tva.Flags in (''Y'',''D'',''S'') -- Sabir Khan 9948 12/28/2011 Fixed barcode issue.
and ta.nomailflag = 0 and tva.nomailflag = 0
and ta.Flags in (''Y'',''D'',''S'')  
and ta.businessid not in (Select businessid from assumednames.dbo.businessowner Where DP2=''21'' AND dpc=''3'' AND ta.FLAGS=''Y'')
and ISNULL(LTRIM(RTRIM(ta.Address)),'''') <> ''''
and ISNULL(LTRIM(RTRIM(tva.OwnerAddress)),'''') <> ''''

Union All

Select distinct a.recordid, 
a.listdate, 
flag1 = isnull(ta.Flags,''N'') , 
case 
when isnull(ta.nomailflag, 0) = 1 or isnull(tva.nomailflag, 0) = 1 then 1 
when isnull(ltrim(rtrim(ta.Address)),'''') = '''' or isnull(ltrim(rtrim(tva.OwnerAddress)),'''') = '''' then 1 else 0 end as donotmailflag, --9921 Farrukh 11/23/2011 do not mail when address is empty
upper(ta.BusinessName) as businessname, 
upper(ta.Address) as address, 
upper(ta.City) as city, 
upper(ta.State) as state, 
ta.Zip as zipcode, 
ta.dp2 as dptwo, 
ta.dpc ,
upper(ta.Address) as OwnerAddress, 
upper(ta.City) as OwnerCity, 
upper(ta.State) as OwnerState, 
ta.Zip as OwnerZip, 
ta.dp2 as Ownerdptwo, 
ta.dpc as Ownerdpc,
(Select Top 1 OwnerName From assumednames.dbo.BusinessOwner Where BusinessId = ta.BusinessId) as ownername,
ta.filenumber,
ta.err_stat ,
a.total_fineamount,
a.violcount 
from assumednames.dbo.business ta, assumednames.dbo.businessowner tva, #temp2 a
where ta.businessid = tva.businessid and ta.Address <> tva.OwnerAddress
and ta.businessid = a.recordid
and tva.Flags in (''Y'',''D'',''S'') -- Sabir Khan 9948 12/28/2011 Fixed barcode issue.
and ta.nomailflag = 0 and tva.nomailflag = 0
and ta.Flags in (''Y'',''D'',''S'')  
and ta.businessid not in (Select businessid from assumednames.dbo.businessowner Where DP2=''21'' AND dpc=''3'' AND ta.FLAGS=''Y'')
and ISNULL(LTRIM(RTRIM(ta.Address)),'''') <> ''''
and ISNULL(LTRIM(RTRIM(tva.OwnerAddress)),'''') <> '''') Main

 ' 
 
set @sqlquery=@sqlquery+' 
--Sabir Khan 9975 01/09/2012 Need to send only one mailer if multiple companies have same address.
---------------------------------------------------------------------------------------------------
Select recordid, listdate,AssumedNames.[dbo].[Fn_AN_Get_AllCompanyNameWithComma](ADDRESS) as businessName,address, 
city,state,dpc,dptwo,Flag1, OwnerAddress, OwnerCity, OwnerState, OwnerZip, Ownerdptwo, Ownerdpc, ownername, filenumber, err_stat, 
donotmailflag, zipcode, 3097 as courtid into #temp6 from #temp
group by listdate,AssumedNames.[dbo].[Fn_AN_Get_AllCompanyNameWithComma](ADDRESS),address, 
city,state,dpc,dptwo,Flag1, OwnerAddress, OwnerCity, OwnerState, OwnerZip, Ownerdptwo, 
Ownerdpc, ownername, filenumber, err_stat,donotmailflag, zipcode,recordid


select distinct listdate as mdate,OwnerAddress,address,BusinessName, city,state,zipcode,dpc,dptwo,Flag1,OwnerCity, OwnerState, 
OwnerZip,err_stat,donotmailflag,3107 as courtid  into #temp7 from #temp6 

alter table #temp7 add recordid int,Ownerdptwo varchar(3), Ownerdpc varchar(3), ownername varchar(max), filenumber varchar(21),IsAddressNull int NOT NULL DEFAULT(0), IsOwnerAddress int Not Null Default(0)

update t7 set t7.recordid = t6.recordid, t7.Ownerdptwo = t6.Ownerdptwo, t7.Ownerdpc = t6.Ownerdpc, t7.ownername = t6.ownername,
t7.filenumber = t6.filenumber from #temp7 t7 inner join #temp6 t6
on t7.OwnerAddress = t6.OwnerAddress and t7.address = t6.address and t7.BusinessName = t6.BusinessName and t7.city = t6.city 
and t7.state = t6.state and t7.zipcode = t6.zipcode and t7.dpc = t6.dpc and t7.dptwo = t6.dptwo and t7.Flag1 = t6.Flag1 
and t7.OwnerCity = t6.OwnerCity and t7.OwnerState = t6.OwnerState and t7.OwnerZip= t6.OwnerZip and isnull(t7.err_stat,'''') = isnull(t6.err_stat,'''')
and t7.donotmailflag  = t6.donotmailflag;

WITH duplicates  AS (SELECT  ROW_NUMBER() OVER (PARTITION BY [Address],city,state,zipcode,businessname 
ORDER BY [Address],city,state,zipcode,businessname) AS duplicate_id,*
FROM  #temp7 )  UPDATE   duplicates  SET   address = '''' WHERE   duplicate_id > 1 

DECLARE @BusinessName NVARCHAR(MAX)
DECLARE @OwnerAddress varchar(max)
DECLARE @BusinessAddress varchar(max)
DECLARE @COUNT int
DECLARE CUR_DELETE CURSOR FOR
SELECT BusinessName,COUNT(Address) FROM #temp7
GROUP BY BusinessName HAVING COUNT(Address) > 1 
OPEN CUR_DELETE 
FETCH NEXT FROM CUR_DELETE INTO @BusinessName,@COUNT
WHILE @@FETCH_STATUS = 0
BEGIN 
select top 1  @BusinessAddress= address,@OwnerAddress = OwnerAddress from #temp7 where BusinessName = @BusinessName and len(address)>1 order by [Address],city,state,zipcode,businessname desc
--update #temp7 set Address = '''' where BusinessName = @BusinessName and OwnerAddress = @BusinessAddress
update #temp7 set Address = '''' where BusinessName = @BusinessName and OwnerAddress <> @BusinessAddress and len(isnull(Address,'''')) > 0

FETCH NEXT FROM CUR_DELETE INTO @BusinessName,@COUNT
END 
CLOSE CUR_DELETE
DEALLOCATE CUR_DELETE
update #temp7 set ISAddressNull = 1 where len(isnull(Address,'''')) = 0

select * into #temp1 from #temp7

' -- Sabir Khan 9921 12/13/2011 Court id has been changed.
 
if( @printtype<>0) 
set @sqlquery2 =@sqlquery2 + 
' 
 Declare @ListdateVal DateTime, 
 @totalrecs int, 
 @p_EachLetter money, 
 @recordid varchar(50), 
 @zipcode varchar(12), 
 @maxbatch int , 
 @lCourtId int , 
 @dptwo varchar(10), 
 @dpc varchar(10),
 @PreRecordid varchar(50)
 
 declare @tempBatchIDs table (batchid int) 
 Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal 
 -- Rab Nawaz Khan 10067 02/28/2012 Per mailer cost changes to 45 cents regardless of mailer count . . . 
 -- Select @count=Count(*) from #temp1 
 -- if @count>=500 
 -- set @p_EachLetter=convert(money,0.3) 
 -- if @count<500 
 set @p_EachLetter=convert(money,0.45) 
 
 
 Declare ListdateCur Cursor for 
 Select distinct mdate from #temp1 
 open ListdateCur 
 Fetch Next from ListdateCur into @ListdateVal 
 while (@@Fetch_Status=0) 
 begin 
 Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal 

 insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values 
 ('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter) 
 
 Select @maxbatch=Max(BatchId) from tblBatchLetter 
 insert into @tempBatchIDs select @maxbatch 
 Declare RecordidCur Cursor for 
 Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal 
 open RecordidCur 
 Fetch Next from RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc 
 while(@@Fetch_Status=0) 
 begin 
 if(@PreRecordid <>  @recordid)
 BEGIN
 insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc) 
 values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)
 END 
 set @PreRecordid =  @recordid
 Fetch Next from RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc 

 end 
 close RecordidCur 
 deallocate RecordidCur 
 Fetch Next from ListdateCur into @ListdateVal 
 end 
 
 close ListdateCur 
 deallocate ListdateCur
 
-------- Select distinct ''NOT PRINTABLE'' as letterId, recordid as businessid, 
-------- CASE  WHEN LTRIM(RTRIM(businessName)) = '''' THEN ''BUSINESS STARTUP'' ELSE businessName END as businessName, --9921 Farrukh 11/23/2011 do not mail when address is empty
-------- OwnerAddress, OwnerCity, OwnerState, OwnerZip, Ownerdptwo, Ownerdpc, ownerName,address,city, state, dpc, dptwo as dp2, zipcode as zip ,err_stat, filenumber , '''+@user+''' as Abb,ISAddressNull ,IsOwnerAddress
-------- from #temp1 
-------- order by zip 
 
 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid as businessid, t.businessname, 
 t.ownerName,t.address,t.city, t.state, t.dpc, 
 t.dptwo as dp2, OwnerAddress, OwnerCity, OwnerState, OwnerZip, Ownerdptwo, Ownerdpc, t.zipcode as zip, err_stat, filenumber, '''+@user+''' as Abb, ISAddressNull, IsOwnerAddress
 from #temp1 t , tblletternotes n, @tempBatchIDs tb 
 where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
 order by t.zipcode
 
declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500) 
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid 
select @subject = convert(varchar(20), @lettercount) + '' LMS Businesses Letters Printed'' 
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''') 
exec usp_mailer_send_Email @subject, @body 

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '','' from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid
 
' 
 
else 
 
 set @sqlquery2 = @sqlquery2 + ' 
 Select distinct ''NOT PRINTABLE'' as letterId, recordid as businessid, 
 CASE  WHEN LTRIM(RTRIM(businessName)) = '''' THEN ''BUSINESS STARTUP'' ELSE businessName END as businessName, --9921 Farrukh 11/23/2011 do not mail when address is empty
 OwnerAddress, OwnerCity, OwnerState, OwnerZip, Ownerdptwo, Ownerdpc, ownerName,address,city, state, dpc, dptwo as dp2, zipcode as zip ,err_stat, filenumber , '''+@user+''' as Abb,ISAddressNull ,IsOwnerAddress
 from #temp1 
 order by zip
' 

set @sqlquery2 = @sqlquery2 + ' 
 
drop table #temp
drop table #temp1
drop table #temp2

' 
 
--print @sqlquery + @sqlquery2 
set @sqlquery = @sqlquery + @sqlquery2 
exec (@sqlquery)


  


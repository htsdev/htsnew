﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 1/20/2010 7:14:30 PM
 ************************************************************/

/****** 
Created by:		Fahad Muhammad Qureshi.
TaskID:			6934
Created Date:	12/08/2009
Business Logic:	The procedure is used by Dallas Traffic Program for validation to get information about those all Quote Clients having Today's Call Back Date. 
				
List of Parameters:
	@DateFrom :Starting Date of Date range.
	@DateTo : Ending Date of Date range.
	@IsValidation : Parameter which shows that identify the report is displaying in Validation email or not.

******/
-- USP_HTP_Get_All_TodayQouteCallDate '1/1/2008','12/31/2010',1,2
ALTER PROCEDURE [dbo].[USP_HTP_Get_All_TodayQouteCallDate]
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@IsValidation BIT = 0,
	@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	SELECT DISTINCT	       
	       CONVERT(VARCHAR(12), tvq.CallBackDate, 101) AS CallBackDate,	       
	       CONVERT(VARCHAR(12), tvq.LastContactDate, 101) AS LastContactDate,
	       CONVERT(INT, GETDATE() - tvq.LastContactDate) AS Days,
	       tt.TicketID_PK,
	       tt.Firstname + ' ' + tt.Lastname AS Customer,
	       tt.calculatedtotalfee,
	       CONVERT(VARCHAR(12), ttv.CourtDateMain, 101) AS CourtDateMain,
	       tcvs.ShortDescription AS [STATUS],
	       [CaseType] = (
	           SELECT ct.CaseTypeName
	           FROM   CaseType ct
	           WHERE  ct.CaseTypeId = tc.CaseTypeID
	       ),
	       tc.ShortName AS shortcourtname,
	       REPLACE(tqr.QuoteResultDescription, '---------------------', ' ') AS 
	       QuoteResultDescription
	FROM   tblViolationQuote tvq
	       LEFT OUTER JOIN tblQuoteResult tqr
	            ON  tqr.QuoteResultID = tvq.FollowUPID
	       LEFT OUTER JOIN tblTickets tt
	            ON  tvq.TicketID_FK = tt.TicketID_PK
	       INNER JOIN tblTicketsViolations ttv
	            ON  ttv.TicketID_PK = tt.TicketID_PK
	       INNER JOIN tblCourts tc
	            ON  tc.CourtID = ttv.CourtID
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusID
	WHERE  -- All Quote Cients
	       tt.Activeflag = 0
	       --Follow up should be Yes
	       AND tvq.FollowUpYN = 1
	           --Case Status Category Checks implemented according to Case Type
	       AND (
	               (tt.casetypeid = 1 AND tcvs.CategoryID = 2)
	               OR (
	                      tt.casetypeid IN (2, 3, 4)
	                      AND tcvs.CategoryID IN (3, 4, 5)
	                  )
	           )
	           --When Called from Report check for Date Range
	       AND (
	               (@IsValidation <> 1 OR @ValidationCategory=2)  -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
	               AND 
	               (
	               	((DATEDIFF(DAY, tvq.CallBackDate, @DateFrom) <= 0) AND (DATEDIFF(DAY, tvq.CallBackDate, @DateTo) >= 0))
					)
	           --When Called from validation report email exclude date range
			OR  ((@IsValidation = 1 OR @ValidationCategory=1) AND DATEDIFF(DAY, tvq.CallBackDate, GETDATE()) = 0)	-- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	       )

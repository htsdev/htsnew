﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure is used to get nine weeks with week first day and week last day.		
******/
  
Create PROCEDURE dbo.USP_HTP_Get_ShowSetting_NeenWeeks  
AS  
BEGIN  
    DECLARE @FirstDay DATETIME  
    DECLARE @LastDay DATETIME  
      
    CREATE TABLE #WeekDay  
    (  
     startDate  DATETIME  
       ,EndDate    DATETIME  
    )  
      
    SET @FirstDay = DATEADD(wk ,DATEDIFF(wk ,6 ,GETDATE()) ,7)  
    SET @LastDay = DATEADD(wk ,DATEDIFF(wk ,5 ,GETDATE()) ,5)  
      
    INSERT INTO #WeekDay  
      (  
        startDate  
       ,EndDate  
      )  
    VALUES  
      (  
        @FirstDay  
       ,@LastDay  
      )  
      
    DECLARE @count INT  
    SET @count = 1   
    WHILE @count<9  
    BEGIN  
        SET @FirstDay = (  
                SELECT DATEADD(wk ,1 ,DATEADD(wk ,DATEDIFF(wk ,6 ,@FirstDay) ,7))  
            )  
          
        SET @LastDay = (  
                SELECT DATEADD(wk ,1 ,DATEADD(wk ,DATEDIFF(wk ,5 ,@LastDay) ,5))  
            )  
          
        INSERT INTO #WeekDay  
          (  
            startDate  
           ,EndDate  
          )  
        VALUES  
          (  
            @FirstDay  
           ,@LastDay  
          )  
          
          
          
        SET @count = @count+1  
    END  
      
    SELECT CONVERT(VARCHAR(10) ,startDate ,101) AS StartDate  
          ,CONVERT(VARCHAR(10) ,EndDate ,101) AS EndDate  
    FROM   #WeekDay  
      
    DROP TABLE #WeekDay  
END  
   
   
   GO 
   
   GRANT EXECUTE on dbo.USP_HTP_Get_ShowSetting_NeenWeeks TO dbr_webuser
   
   
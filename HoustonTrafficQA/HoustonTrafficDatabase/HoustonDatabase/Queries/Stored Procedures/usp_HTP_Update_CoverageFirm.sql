USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTP_Update_CoverageFirm]    Script Date: 11/11/2008 07:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Noufil 4747
/******   
Create by:  Noufil Khan
Business Logic : This procedure update the Coverage firm for the given ticket id.
  
List of Parameters:   
	@CoveringFirmID : Firm ID which will be updated
	@ticketid_pk : Ticket ID of the Case.
******/  

ALTER Procedure [dbo].[usp_HTP_Update_CoverageFirm] -- 3058,196643

@CoveringFirmID int,
@ticketid_pk int
as

declare @PreviousFirmName varchar(30)
declare @refcasenumber varchar(30)
declare @EmployeeId int
declare @command varchar(300)
declare @CoveringFirmName varchar(20)

set @refcasenumber=(select max(refcasenumber) from tblticketsviolations where ticketid_pk=@ticketid_pk)

set @PreviousFirmName=(
						select top 1 FirmAbbreviation from tblfirm
							inner join tblticketsviolations on tblticketsviolations.CoveringFirmID=tblfirm.firmid 
							where ticketid_pk=@ticketid_pk
					   )

set @EmployeeId = (select EmployeeID from tbltickets where ticketid_pk=@ticketid_pk )

update tblticketsviolations
	set CoveringFirmID = @CoveringFirmID -- 3058
where ticketid_pk=@ticketid_pk
--Sabir Khan 4662 11/10/2008 To eleminate the exception on newpaymentinfo page...
set @CoveringFirmName=(
						select top 1 FirmAbbreviation from tblfirm
							inner join tblticketsviolations on tblticketsviolations.CoveringFirmID=tblfirm.firmid 
							where ticketid_pk=@ticketid_pk
					   )

set @command='Firm Changed - ' + @refcasenumber + ' - ' + isnull(@PreviousFirmName,'N/A') + ' to ' + @CoveringFirmName        
exec usp_HTS_Insert_CaseHistoryInfo  @ticketid_pk,@command ,@EmployeeId,''       


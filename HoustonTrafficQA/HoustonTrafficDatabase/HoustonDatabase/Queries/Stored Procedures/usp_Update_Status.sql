SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Status]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Status]
GO

CREATE PROCEDURE usp_Update_Status     
@Status int,    
@TicketNumber varchar(20)
AS    
BEGIN    
if(@status<>0)    
begin    
update tblticketsviolationsarchive set violationstatusid=@Status    
where ticketnumber_pk=@TicketNumber    
end    
END 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


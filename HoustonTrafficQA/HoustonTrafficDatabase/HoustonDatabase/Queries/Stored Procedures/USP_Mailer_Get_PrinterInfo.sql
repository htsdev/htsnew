SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_PrinterInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_PrinterInfo]
GO


CREATE PROCEDURE [dbo].[USP_Mailer_Get_PrinterInfo]
	@LetterID_PK tinyint
AS
BEGIN
	
	Select LetterID_PK,printerName from [tblLetter] where [LetterID_PK]=@LetterID_PK
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


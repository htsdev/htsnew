SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PricingPlans_OnActiveFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PricingPlans_OnActiveFlag]
GO










CREATE PROCEDURE [USP_HTS_GET_PricingPlans_OnActiveFlag]       
      
(      
@DefaultDate DateTime,     
@CourtID int,     
@ShowAll  bit  -- Plan Status         
)      
      
as 
if (@ShowAll <> 0)

Begin
   
if (@CourtID <> 0)    
    
	begin    
 		SELECT pp.PlanId,  
  		pp.PlanShortName,         
 		pp.PlanDescription,         
 		pp.EffectiveDate,         
  		pp.EndDate,         
 		 c.ShortName,         
  		pp.IsActivePlan        
 		FROM    dbo.tblPricePlans pp         
 		INNER JOIN        
 		dbo.tblCourts c         
 		ON  pp.CourtId = c.Courtid        
 		where       
 		 pp.IsActivePlan  = @ShowAll     
 		and c.Courtid = @CourtID    
 		and pp.effectivedate <= @DefaultDate      
 		and pp.enddate >= @DefaultDate    
	End    
    
	else    
    
	begin    
 		SELECT pp.PlanId,   
  		pp.PlanShortName,         
  		pp.PlanDescription,         
  		pp.EffectiveDate,         
  		pp.EndDate,         
 		 c.ShortName,         
  		pp.IsActivePlan        
		 FROM    dbo.tblPricePlans pp         
 		INNER JOIN        
 		dbo.tblCourts c         
 		ON  pp.CourtId = c.Courtid        
 		where       
  		pp.IsActivePlan  = @ShowAll     
 		and pp.effectivedate <= @DefaultDate      
 		and pp.enddate >= @DefaultDate    
	End    
  End
else if (@ShowAll = 0)
   Begin
	if (@CourtID <> 0)    
    
	begin    
 	SELECT pp.PlanId,  
  	pp.PlanShortName,         
 	pp.PlanDescription,         
  	pp.EffectiveDate,         
  	pp.EndDate,         
  	c.ShortName,         
  	pp.IsActivePlan        
 	FROM    dbo.tblPricePlans pp         
 	INNER JOIN        
 	dbo.tblCourts c         
 	ON  pp.CourtId = c.Courtid        
 	where       
     
 	 c.Courtid = @CourtID    
 	and pp.effectivedate <= @DefaultDate      
 	and pp.enddate >= @DefaultDate    
	End    
    
	else    
    
	begin    
 	SELECT pp.PlanId,   
  	pp.PlanShortName,         
  	pp.PlanDescription,         
	pp.EffectiveDate,         
	pp.EndDate,         
  	c.ShortName,         
  	pp.IsActivePlan        
 	FROM    dbo.tblPricePlans pp         
 	INNER JOIN        
 	dbo.tblCourts c         
 	ON  pp.CourtId = c.Courtid        
 	where       
    
 	 pp.effectivedate <= @DefaultDate      
 	and pp.enddate >= @DefaultDate   
 
	End    


End






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:27:05 PM
 ************************************************************/

USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_ReportSetting_Get_CourtList]    Script Date: 09/27/2010 12:26:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to get the list of active courts. 
*					  as an input parameter.
* Parameter List  
* InActiveFlag		: Returns list of courts based on this parameter.
* Example			: USP_ReportSetting_Get_CourtList 0
*
*/

CREATE PROC [dbo].[USP_ReportSetting_Get_CourtList]
@InActiveFlag BIT
AS
SELECT DISTINCT tc.Courtid,
       tc.CourtName
FROM   tblCourts AS tc
WHERE  tc.Courtid <> 0
       AND InActiveFlag = @InActiveFlag
ORDER BY
       tc.CourtName 
GO

GRANT EXECUTE ON [dbo].[USP_ReportSetting_Get_CourtList] TO dbr_webuser
GO


﻿
/*
* Business Logic : This procedure updates client personal information and some information.
* Parameter  : 
*				@TicketID_PK,
				@EmpID
				@Firstname
				@Lastname
				@MiddleName
				@Address1
				@Address2
				@Contact1
				@ContactType1
				@Contact2
				@ContactType2
				@Contact3
				@ContactType3
				@DLNumber
				@DLState
				@DOB
				@SSNumber
				@Email
				@City
				@Stateid_FK
				@Zip
				@Race
				@gender
				@Height
				@Weight
				@HairColor
				@Eyes
				@YDS
				@Addressconfirmflag
				@EmailNotAvailable
				@DLNotAvailable
				@ContinuanceStatus
				@ContinuanceDate
				@FirmID
				@ContinuanceFlag
				@WalkInClientFlag
				@isPR
				@hasConsultationComments
				@dp2
				@dpc
				@flag1
				@updatebarcodeinfo
				@midnum
				@SmsFlag1
				@SmsFlag2
				@SmsFlag3

*/

ALTER  procedure [dbo].[USP_HTS_Update_ClientInfo]       --Updates General info of client                                    
@TicketID_PK as int,                                         
@EmpID as int,                                        
@Firstname as varchar(20),                                         
@Lastname as varchar(20),                                         
@MiddleName as varchar(6),                                         
@Address1 as varchar(50),                                         
@Address2 as varchar(20),                                        
@Contact1 as varchar(15),                                         
@ContactType1 as int,                                        
@Contact2 as varchar(15),                                         
@ContactType2 as int,                                         
@Contact3 as varchar(15),                                         
@ContactType3 as int,                                         
@DLNumber as varchar(30),                                        
@DLState as int,                                        
@DOB as datetime,                                        
@SSNumber as varchar(12),                                         
@Email as varchar(255),                                         
@City as varchar(50),                                        
@Stateid_FK  as int,                                         
@Zip as varchar(20),                                        
@Race as varchar(20),                                        
@gender  as varchar(10),                                        
@Height  as varchar(10),                                        
@Weight as varchar(10),                                        
@HairColor as varchar(10),                                        
@Eyes as varchar(10),                                          
--@GeneralComments as varchar(2000),                                         
--@ContactComments as varchar(500),                                         
--@SettingComments as varchar(100),                                         
--@TrialComments as varchar(1000) ,                         
@YDS as varchar(5),                          
@Addressconfirmflag as tinyint ,                  
@EmailNotAvailable as bit,                
@DLNotAvailable int,              
--@ContinuanceComments varchar(2000),              
@ContinuanceStatus tinyint,              
@ContinuanceDate datetime,              
@FirmID int,            
@ContinuanceFlag int,    
@WalkInClientFlag varchar(15),  
@isPR int,  
@hasConsultationComments int ,  
@dp2 varchar(50),
@dpc varchar(50),
@flag1 varchar(1),
@updatebarcodeinfo bit ,
@midnum as varchar(20),
@SmsFlag1 BIT,
@SmsFlag2 BIT,
@SmsFlag3 BIT

as                                        
          
-- Added By Zeeshan Ahmed --        
declare @ContinuanceComments varchar(2000)          
declare @prevcontinuancestatus int                            
declare @PrevFirm int          
declare @dFirstname varchar(20)                                         
declare @dLastname varchar(20)                                         
declare @dMiddleName  varchar(6)                                          
declare @dDOB datetime
declare @isManual bit

Select @PrevFirm = firmid ,@prevcontinuancestatus = ContinuanceStatus from tbltickets where ticketid_pk =  @TicketID_PK                   
          
          
             
  if @ContinuanceFlag = 0            
 begin            
              
  Select @ContinuanceComments = '', @ContinuanceDate='1/1/1900' ,@ContinuanceStatus = 1            
             
 end 

-- Agha Usman 4347 07/21/2008 - Saving old values to be used in future
select  @isManual =isProblemClientManual,@dFirstName = FirstName , @dLastname = LastName , @dMiddleName = MiddleName, @dDOB = DOB from tbltickets where ticketId_Pk =  @TicketID_PK 
                        
                                     
                                        
  UPDATE    tblTickets                                        
  SET 
                                            
  Firstname = Upper(@Firstname),                                         
  Lastname = Upper(@Lastname),                                         
  MiddleName =Upper(@MiddleName),                                         
  Contact1 = @Contact1,                                         
  ContactType1 = @ContactType1,                                         
  Contact2 = @Contact2,                                         
  ContactType2 = @ContactType2,                                         
  Contact3 = @Contact3 ,                       
  ContactType3 = @ContactType3 ,                                         
  DLNumber = @DLNumber ,                                         
  DLState = @DLState ,                                         
  DOB = @DOB ,                                         
  SSNumber = @SSNumber ,                                         
  Address1 = Upper(@Address1),                                         
  Address2 = Upper(@Address2),                                        
  Email = @Email ,                                         
  City = Upper(@City),                                        
  Stateid_FK = @Stateid_FK ,                                         
  Zip = @Zip ,                                        
  Race = Upper(@Race),                                         
  gender = @gender,                                         
  Height = @Height ,                                         
  Weight =@Weight  ,                                         
  HairColor = @HairColor ,                                         
  Eyes = @Eyes,                                        
  EmployeeidUpdate=@Empid,                            
  YDS=@YDS,                          
  Addressconfirmflag=@Addressconfirmflag   ,                   
  EmailNotAvailable = @EmailNotAvailable,                
  NoDL=@DLNotAvailable,              
  ContinuanceStatus=@ContinuanceStatus ,              
  ContinuanceDate=@ContinuanceDate ,             
  ContinuanceFlag=@ContinuanceFlag,             
  firmid = @FirmID,        
  --ContinuanceComments = @ContinuanceComments,    
  WalkInClientFlag=@WalkInClientFlag,  
  isPR = @isPR,  
  hasConsultationComments = @hasConsultationComments,
  dp2   = Case when @updatebarcodeinfo = 1 then @dp2   else   dp2 end,
  dpc   = Case when @updatebarcodeinfo = 1 then @dpc   else   dpc end,
  flag1 = Case when @updatebarcodeinfo = 1 then @flag1 else   flag1 end,
  Midnum= Case when @midnum !='' then @midnum else midnum END,
  -- Noufil 5884 07/02/2009 Set sms flag values with parameter to update.
  SendSMSFlag1 = @SmsFlag1,
  SendSMSFlag2 = @SmsFlag2,
 SendSMSFlag3 = @SmsFlag3
  
WHERE (TicketID_PK =@TicketID_PK)                                        
                
             
                

-- Agha Usman 4347 07/21/2008 - Problem client managment
exec USP_HTP_ProblemClient_Manager @TicketID_PK,@Firstname,@Lastname,@MiddleName,@DOB,@isManual,@dFirstname,@dLastname,@dMiddleName ,@dDOB 

---------------Added By Zeeshan ---------------------          
      
      
update tblticketsflag      
Set       
ContinuanceDate = @ContinuanceDate ,      
ContinuanceStatus = @ContinuanceStatus      
      
where ticketid_pk = @TicketID_PK and flagid = 9       
and RecDate = ( Select max(Recdate) from tblticketsflag where ticketid_pk = @TicketID_PK and flagid = 9)      
      
--------Adding Continuance Notes---------------------        
if @prevcontinuancestatus <> @ContinuanceStatus         
Begin        
declare @contStatusDescription varchar(50)        
select @contStatusDescription = Description from tblcontinousstatus where statusid = @ContinuanceStatus        
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid_pk,'Continuance Flag - '+ @contStatusDescription  ,@empid,null)        
End        
-----------------------------------------------------         
        
        
if exists ( Select ticketid_pk from tblticketsflag where ticketid_pk =  @TicketID_PK and flagid = 8 )           
begin          
          
 if @FirmID <> @PrevFirm  and @FirmID <> 3000          
    begin          
          
 declare @firmName varchar(50)          
 select @firmName=FirmName  from tblfirm where  firmid = @FirmID           
 insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid_pk,'Flag -'+ @firmName + ' Client',@empid,null)               
 end          
--    else          
--          
-- begin          
--    delete from tblticketsflag where ticketid_pk =  @TicketID_PK and flagid = 8          
-- end          
          
End          
----------------------------------------------------------------------------------                                    
/* Remarked by Adil
--if (right(@GeneralComments,1) <> ')' and left(@GeneralComments,1) <> '')     --if general comments are updated                                      
if (right(@GeneralComments,1) <> ')' )                       
 begin                                           
  declare @empnm1 as varchar(12)                                  
                                             
  select @empnm1 = abbreviation from tblusers where employeeid = @EmpID                                          
                  
  if len( @GeneralComments) > 0                                      
  set @GeneralComments = @GeneralComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm1 + ')' + ' '                                          
                                             
  update tbltickets set  GeneralComments = @GeneralComments                                        
  where ticketid_pk = @TicketID_PK                                            
 end                    
--Update Reminder comments                                
                    
--if( right(@ContactComments,1) <> ')' and left(@ContactComments,1) <> '')   --if Contact comments are updated                                      
if( right(@ContactComments,1) <> ')' )   --if Contact comments are updated                                      
 begin          
  declare @empnm as varchar(12)                                          
                                             
  select @empnm = abbreviation from tblusers where employeeid = @empid                                          
                                       
  if len( @ContactComments) > 0                       
  set @ContactComments = @ContactComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' '                                          
                                             
  update tblticketsviolations  set  ReminderComments = @ContactComments,                     
        ReminderCallEmpID=@empid                                                 
 where ticketid_pk = @TicketID_PK                                 
 end                  
                  
                                
                                  
--if( right(@SettingComments,1) <> ')'and left(@SettingComments,1) <> '')    --if setting comments are updated                                      
if( right(@SettingComments,1) <> ')'  )    --if setting comments are updated                                      
 begin                                           
  declare @empnm2 as varchar(12)                                          
                                             
  select @empnm2 = abbreviation from tblusers where employeeid = @empid                                          
                                       
  if (len( @SettingComments) <> 0     )                  
  set @SettingComments = @SettingComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm2 + ')'     + ' '                                       
                                             
  update tbltickets set  SettingComments = @SettingComments                                        
 where ticketid_pk = @TicketID_PK                                            
 end                                   
                                    
--if( right(@TrialComments,1) <> ')' and left(@TrialComments,1) <> '')    --if trial comments are updated                                      
if( right(@TrialComments,1) <> ')'  )    --if trial comments are updated                                      
 begin                                           
  declare @empnm3 as varchar(12)                                          
                                             
  select @empnm3 = abbreviation from tblusers where employeeid = @empid                                          
                                       
  if len( @TrialComments) > 0                       
   set @TrialComments = @TrialComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm3 + ')' + ' '                                          
                         
  update tbltickets set  TrialComments = @TrialComments                                        
 where ticketid_pk = @TicketID_PK                                            
 end                  */
                  
   /*           
--if( right(@TrialComments,1) <> ')' and left(@TrialComments,1) <> '')    --if trial comments are updated                                      
if( right(@ContinuanceComments,1) <> ')'  )    --if trial comments are updated                                      
 begin                                           
  declare @empnm4 as varchar(12)                                          
                          
  select @empnm4 = abbreviation from tblusers where employeeid = @empid                                          
                                       
  if len( @ContinuanceComments) > 0                       
   set @ContinuanceComments = @ContinuanceComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm4 + ')' + ' '                                          
                         
  update tbltickets set  ContinuanceComments = @ContinuanceComments                                        
 where ticketid_pk = @TicketID_PK                                            
 end         
        
*/
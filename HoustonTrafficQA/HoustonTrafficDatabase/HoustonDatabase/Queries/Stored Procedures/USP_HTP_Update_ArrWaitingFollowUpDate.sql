
 
--USP_HTP_Save_ArrWaitingFollowUpDate 195704 ,'4/30/2008'  
Alter PROCEDURE [dbo].[USP_HTP_Update_ArrWaitingFollowUpDate]  
  
@TicketID_PK int,  
@ArrWaitingFollowUpDate datetime=NULL  ,
@EmployeeId as int = 3992
AS  
  
BEGIN  
 
  

declare @L_Exist int , 
		@oldArrWaitingFollowUpDate as Datetime,
	    @note as varchar(100)


set @L_Exist =0  

select  @L_Exist = Count(TicketID_PK) from tblTicketsExtensions  
where TicketID_PK = @TicketID_PK  


if(@L_Exist=0)  
 begin  
 insert into tblTicketsExtensions(TicketID_PK ,ArrWaitingFollowUpDate)  
 values(@TicketID_PK ,@ArrWaitingFollowUpDate)  
    --Zeeshan Ahmed 4629 04/09/2008 Add Note In History For A/W Followup date
	Set @note = 'A/W Follow Up Date : Follow Up Date has changed from N/A to '  +Convert(varchar,@ArrWaitingFollowUpDate,101)
	exec usp_HTS_Insert_CaseHistoryInfo @TicketID_PK ,@note,@EmployeeId,'' 
  
 end  
else  
 begin  


	--Getting Old Date
	select @oldArrWaitingFollowUpDate = ArrWaitingFollowUpDate  from tblTicketsExtensions  
	where TicketID_PK = @TicketID_PK  
	

	update tblTicketsExtensions  
	set ArrWaitingFollowUpDate = @ArrWaitingFollowUpDate  
	where TicketID_PK = @TicketID_PK  

	--Zeeshan Ahmed 4629 04/09/2008 Add Note In History For A/W Followup date
	if( datediff(day,@ArrWaitingFollowUpDate , 	@oldArrWaitingFollowUpDate) <> 0 )
	Begin
		Set @note = 'A/W Follow Up Date : Follow Up Date has changed from ' + Convert(varchar,@oldArrWaitingFollowUpDate,101)   + ' to '  +Convert(varchar,@ArrWaitingFollowUpDate,101)
		exec usp_HTS_Insert_CaseHistoryInfo @TicketID_PK ,@note,@EmployeeId,'' 
	End

 end  
  
   
END  
  
  
/*************************************************************************
* Created By	: Anonymous
* Created Date	: N/A
* 
* Business Logic : Insert letter into batch print
* 
* Parameter List:
*				@TicketID_FK		: Ticket ID for the record
*				@BatchDate			: Batch Date
*				@PrintDate			: Printed Date
*				@IsPrinted			: Is Printed or not
*				@LetterID_FK		: Letter Type ID
*				@EmpID				: Employee ID
*				@DocPath			: Document Path
*				@FromBSDA			: From BSDA
*				@TrialLetterFlag	: Trial Letter Flag
*				@PageCount			: Page Count
*				@DocumentBatchID	: Document Batch ID
 *************************************************************************/

ALTER PROCEDURE [dbo].[USP_HTS_Insert_BatchPrintLetter] 
(
    @TicketID_FK      [int],
    @BatchDate        [datetime],
    @PrintDate        [datetime],
    @IsPrinted        [tinyint],
    @LetterID_FK      [int],
    @EmpID            [int],
    @DocPath          [varchar](120),
    @FromBSDA         [int] = 0,
    @TrialLetterFlag  TINYINT = 0,
    @PageCount        INT = 1,
    --ozair 3643 on 05/01/2008
    @DocumentBatchID  INT = 0 
    --end ozair 3643
)
AS
	DECLARE @LetterName VARCHAR(100)                                  
	DECLARE @BatchID INT                                  
	DECLARE @RecordID VARCHAR(25)                        
	DECLARE @lnk VARCHAR(500)                                 
	
	SELECT @BatchID = 0                         
	SELECT @LetterName = (
	           SELECT LetterName
	           FROM   tblHTSLetters
	           WHERE  LetterID_pk = @LetterID_FK
	       )                                    
	
	
	IF (@IsPrinted = 0) --Batch Print
	BEGIN
	    -- Abid Ali 5359 1/1/2009 Delete already exists LOR records for the ticket which are not printed yet	                   
	    IF @LetterID_FK = 6
	    BEGIN
	    	-- Abid Ali 5563 02/21/2009 Get batch id
	    	DECLARE @LORBatchId INT
	    	SELECT @LORBatchId = BatchID_PK
	    	FROM tblHTSBatchPrintLetter
	    	WHERE  TicketID_FK = @TicketID_FK
	               AND [LetterID_FK] = @LetterID_FK
	               AND IsPrinted = 0
	               AND DeleteFlag <> 1
	        
	        IF @LORBatchId IS NOT NULL
	        BEGIN
				UPDATE  tblHTSBatchPrintLetter
				SET DeleteFlag = 1
				WHERE  BatchID_PK = @LORBatchId
	        END
	    END
	    
	    INSERT INTO [tblHTSBatchPrintLetter]
	      (
	        [TicketID_FK],
	        [BatchDate],
	        [PrintDate],
	        [IsPrinted],
	        [LetterID_FK],
	        [EmpID],
	        [DocPath],
	        [TrialLetterFlag]
	      )
	    VALUES
	      (
	        @TicketID_FK,
	        @BatchDate,
	        NULL,
	        @IsPrinted,
	        @LetterID_FK,
	        @EmpID,
	        @DocPath,
	        @TrialLetterFlag
	      )                        
	    
	    SET @RecordID = CONVERT(VARCHAR(12), SCOPE_IDENTITY())                         
	    
	    IF @LetterID_FK = 2
	    BEGIN
	        EXEC usp_hts_batch_trialletter @TicketID_FK,
	             @RecordID,
	             @EmpID
	    END                  
	    
	    IF @LetterID_FK = 3
	    BEGIN
	        EXEC usp_hts_batch_receipt @TicketID_FK,
	             @RecordID,
	             @EmpID
	    END 
	    
	    -- Abid 5563 02/21/2009 LOR insert into batch
	    IF @LetterID_FK = 6
	    BEGIN
	        EXEC USP_HTP_INSERT_LetterOfRepBatch @TicketID_FK,
	             @EmpID,
	             @RecordID
	             
	    END
	    
	    -- ADDING NOTE IN CASE HISTORY FOR LETTER SENT TO BATCH RPINT.......
	    --set @lnk = '<a href="javascript:window.open(''frmPreview.aspx?RecordID='+ @RecordID  +''');void('''');">' +@LetterName+ ' sent to batch print</a>'                        
	    -- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
	    IF (@FromBSDA = 0 AND @LetterID_FK <> 34)
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	        VALUES
	          (
	            @TicketID_FK,
	            @LetterName + ' Sent to Batch Print',
	            @EmpID
	          )
	    ELSE IF @LetterID_FK <> 34
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	        VALUES
	          (
	            @TicketID_FK,
	            @LetterName + ' Sent to Batch Print from BSDA',
	            @EmpID
	          )
	END
	ELSE 
	IF @isprinted = 1
	BEGIN
	    INSERT INTO [tblHTSNotes]
	      (
	        [BatchID_PK],
	        [TicketID_FK],
	        [PrintDate],
	        [LetterID_FK],
	        [EmpID],
	        [Note],
	        [DocPath],
	        [PageCount]
	      )
	    VALUES
	      (
	        @BatchID,
	        @TicketID_FK,
	        GETDATE(),	--@PrintDate,                                      
	        @LetterID_FK,
	        @EmpID,
	        CASE 
	             WHEN @LetterID_FK = 9 THEN @LetterName + ' sent to File Server'
	             ELSE -- ozair 3643 on 05/01/2008 
	                  CASE @DocumentBatchID
	                       WHEN 0 THEN @LetterName + ' Printed'
	                       ELSE @LetterName + ' Printed ID: ' + CONVERT(VARCHAR, @DocumentBatchID)
	                  END
	        END,
	        --end ozair 3643
	        @DocPath,
	        @PageCount
	      )                                      
	    
	    SET @RecordID = CONVERT(VARCHAR(12), SCOPE_IDENTITY())                        
	    
	    IF (@IsPrinted = 0)
	        SET @lnk = '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + @RecordID + ''');void('''');">' + 
	            @LetterName + ' Printed from Batch Print- </a>'
	    ELSE
	        --ozair 3643 on 05/01/2008
	    BEGIN
	        IF @DocumentBatchID = 0
	            SET @lnk = '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + @RecordID + ''');void('''');">' + 
							CASE WHEN @LetterID_FK = 9 THEN @LetterName +
	                                   ' sent to File Server </a>'
	                              ELSE @LetterName + ' Printed Now </a>'
	                         END
	        ELSE
	            SET @lnk = '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + @RecordID + ''');void('''');">' + 
							CASE WHEN @LetterID_FK = 9 THEN @LetterName +
	                                   ' sent to File Server </a>'
	                              ELSE @LetterName + ' Printed Now ID: ' +
	                                   CONVERT(VARCHAR, @DocumentBatchID) +
	                                   ' </a>'
	                         END
	    END 
	    --end ozair 6343
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    VALUES
	      (
	        @TicketID_FK,
	        @lnk,
	        @EmpID
	      )
	END                    
	
	IF @LetterID_FK = 11
	BEGIN
		UPDATE PendingTicket
	        SET IsActive = 1 WHERE TicketId=@TicketID_FK
	END
	
	IF @LetterID_FK = 2
	BEGIN
		IF (@IsPrinted = 0)
	        UPDATE tbltickets
	        SET    jurystatus = 2
	        WHERE  ticketid_pk = @TicketID_FK
	    
	    IF (@IsPrinted = 1)
	        UPDATE tbltickets
	        SET    jurystatus = 1
	        WHERE  ticketid_pk = @TicketID_FK	        
	END 
	
	--Updated by Ozair at 01/12/2008//Task 2598    
	
	IF (@LetterID_FK = 4 AND @IsPrinted = 1)
	BEGIN
	    DECLARE @fid INT    
	    SET @fid = 0    
	    SELECT @fid = fid
	    FROM   tblticketsflag
	    WHERE  ticketid_pk = @TicketID_FK
	           AND flagid = 32    
	    
	    IF @fid > 0
	    BEGIN
	        -- removing no bond discrepancy flag    
	        DELETE 
	        FROM   tblticketsflag
	        WHERE  ticketid_pk = @TicketID_FK
	               AND flagid = 32
	               AND fid = @fid 
	        
	        -- inserting notes    
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid,
	            Notes
	          )
	        VALUES
	          (
	            @TicketID_FK,
	            'Flag - No Bond Discrepancy - Removed',
	            @EmpID,
	            NULL
	          )
	    END
	END--end ozair      
	   --  
	--Ozair 5767 05/06/2009 setting IsLORPrinted Flag to 1
	IF (@LetterID_FK = 6 AND @IsPrinted = 1)
	BEGIN	    
	        UPDATE tbltickets
	        SET    IsLORPrinted = 1
	        WHERE  ticketid_pk = @TicketID_FK
	END 
	
	   
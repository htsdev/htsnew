/****************

Alter by:	Tahir Ahmed
Date:		10/08/2008

Business Logic:
	The stored procedure is used by Houtson Traffic Program to add/update 
	the price calculation steps in DB after price calculations. The information 
	then can be retreived on Calculation summary page.

Input Parameters:
	@TicketId_pk:
	@TicketsViolationId:
	@PriceType:
	@CalculationDescription:
	@Amount:

Output Columns:
	N/A

***************/ 

  
alter procedure USP_HTS_Insert_PriceCalculationSteps  
 (  
 @TicketId_pk  int,  
 @TicketsViolationId int,  
 @PriceType  varchar(50),  
 @CalculationDescription varchar(400),  
 @Amount   money  
 )  
as   
  
declare @RecCount int,  
 @isAdditional bit  
  
select @isAdditional = 0  
  
if (@TicketsViolationId is not null)  
 select  @RecCount = count(*)   
 from  tblpricingcalculator   
 where  ticketsviolationid = @TicketsViolationId  
  
else if (   @CalculationDescription = 'Speeding'    
  or @CalculationDescription = 'Accident'    
  or @CalculationDescription = 'CDL'   
  or @CalculationDescription = 'Round Off'  
  or @CalculationDescription = 'Continuance'  
  or @CalculationDescription = 'InitialAdj'  
  or @CalculationDescription = 'FinalAdj' 
  or @CalculationDescription = 'Payment plan charges' -- tahir 4786 10/08/2008
  OR @CalculationDescription = 'Day Of Arraignment' -- Haris Ahmed 10330 10/04/2012 Add Day Of Arraignment
 )  
 begin  
  select  @RecCount = count(*) from tblpricingcalculator where CalculationDescription = @CalculationDescription  
  and  ticketid_pk = @TicketId_pk  
    
  select @isAdditional = 1  
 end  
  
  
if @RecCount > 0   
 begin  
  if (@isAdditional = 0 )  
   begin   
    update  tblpricingcalculator  
    set pricetype = @PriceType,  
     CalculationDescription  = @CalculationDescription,  
     Amount = @Amount  
    where ticketsviolationid = @TicketsViolationId  
   end  
  else  
   begin   
    update  tblpricingcalculator  
    set Amount = @Amount  
    where CalculationDescription  = @CalculationDescription  
    and  ticketid_pk  = @ticketid_pk  
   end  
 end  
else  
 begin  
  insert into tblPricingCalculator (  
   Ticketid_pk,   
   TicketsviolationId,  
   PriceType,  
   CalculationDescription,  
   Amount  
   )  
  values  
   (  
   @TicketId_pk,  
   @TicketsViolationId,  
   @PriceType,  
   @CalculationDescription,  
   @Amount  
   )  
 end  
  
  
  
  
  
  
  
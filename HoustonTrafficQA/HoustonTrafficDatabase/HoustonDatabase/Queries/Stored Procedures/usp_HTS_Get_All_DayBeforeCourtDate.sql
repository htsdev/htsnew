﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 1/20/2010 7:00:02 PM
 ************************************************************/

/******  
Alterd by:  Yasir kamal
Business Logic : This procedure is used By Quote Call Back Report.
         
List of Parameters:   
    @DateFrom : Start court date.
	@DateTo : End Court date.
	@casetypeid : -1 - All
	               1 - Traffic
	               2 - Criminal
	               3 - Civil
	               4 - Family
******/

-- Noufil 4487 08/05/2008 courtlocation field added

-- usp_HTS_Get_All_DayBeforeCourtDate '04/04/2008','08/08/2008',1
Alter PROCEDURE [dbo].[usp_HTS_Get_All_DayBeforeCourtDate] 
--@TicketID_PK varchar(12)
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@casetypeid INT -- Noufil 4487 08/04/2008 Case type id added to display Civil client or traffic client or all of them
AS
--ozair 6934 01/22/2010 Removed Extra columns
	SELECT DISTINCT 			
	       dbo.tblViolationQuote.QuoteID,
	       CONVERT(VARCHAR(12), dbo.tblViolationQuote.LastContactDate, 101) AS 
	       LastContactDate,
	       CONVERT(INT, GETDATE() - dbo.tblViolationQuote.LastContactDate) AS 
	       Days,
	       dbo.tblUsers.Firstname + ' ' + dbo.tblUsers.Lastname AS EmpName,
	       --Ozair 6934 01/26/2010 first/last name swaped 
	       dbo.tblTickets.Firstname + ' ' + dbo.tblTickets.Lastname AS Customer,	/* CONVERT(varchar(12), dbo.tblTickets.ContactDate, 101) AS ContactDate, Agha Usman 2664 06/17/2008 */ 
	       CONVERT(DECIMAL(8, 0), dbo.tblTickets.calculatedtotalfee) AS 
	       calculatedtotalfee,
	       dbo.tbltickets.GeneralComments,
	       dbo.tblViolationQuote.FollowUpYN,
	       dbo.tblTickets.TicketID_PK,
	       REPLACE(
	           tblQuoteResult_1.QuoteResultDescription,
	           '---------------------',
	           ' '
	       ) AS FollowUpStatus,
	       CONVERT(
	           VARCHAR(12),
	           --Yasir Kamal 5744 04/02/2009 need to search on auto court date
	           dbo.tblTicketsViolations.courtdate,
	           101
	       ) AS CourtDate,
	       --5744 end          
	       dbo.tblViolationQuote.CallBackDate,
	       tblTickets.casetypeid,
	       c.shortname AS crt
	       --Fahad 6934 01/13/2010 Temp Table Created in order to maintain Tracking
	       INTO #tempquote
	FROM   dbo.tblViolationQuote
	       LEFT OUTER JOIN dbo.tblQuoteResult tblQuoteResult_1
	            ON  dbo.tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID
	       LEFT OUTER JOIN dbo.tblTicketsViolations
	       RIGHT OUTER JOIN dbo.tblTickets
	            ON  dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK
	            ON  dbo.tblViolationQuote.TicketID_FK = dbo.tblTickets.TicketID_PK
	       LEFT OUTER JOIN dbo.tblUsers
	            ON  dbo.tblTickets.EmployeeIDUpdate = dbo.tblUsers.EmployeeID	       
	       INNER JOIN dbo.tblCourts c
	            ON  tblTicketsViolations.CourtID = c.Courtid 
	                --Yasir Kamal 5744 04/02/2009 cases having primary auto status ARRAIGNMENT only
	       INNER JOIN dbo.tblCourtViolationStatus
	            ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
	WHERE  (dbo.tblTickets.Activeflag = 0)
	       AND (dbo.tblViolationQuote.FollowUpYN = 1)
	       AND DATEDIFF(DAY, dbo.tblTicketsViolations.courtdate, @datefrom) <= 0
	       AND DATEDIFF(DAY, dbo.tblTicketsViolations.courtdate, @dateto) >= 0
	       AND (@casetypeid = -1 OR tblTickets.casetypeid = @casetypeid)
	       -- Abbas Qamar 10093 29-03-2012 checking the contact no
	       AND LEN(dbo.tblTickets.Contact1) > 0
	           -- Noufil 5894 06/17/2009 code comment as per QA request in Task history
	           -- tahir 6513 09/02/09 need to list only arraignment cases
	           --Sabir Khan 6966 11/10/2009 Allow Criminal, Civil and Family Law cases on Quote call back report...
	       AND (
	               (
	                   tblTickets.casetypeid = 1
	                   AND dbo.tblCourtViolationStatus.CategoryID = 2
	               )
	               OR (
	                      tblTickets.casetypeid IN (2, 3, 4)
	                      AND dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5)
	                  )
	       )
	       -- Babar Ahmad 9654 09/28/2011 Excluded records with Problem Client (No Hire/Allow Hire) flag.   
	       AND dbo.tblViolationQuote.TicketID_FK NOT IN (SELECT TicketID_PK FROM tblTicketsFlag ttf WHERE ISNULL(ttf.FlagID,0) IN (13,36))     
	       
	           -- Noufil 5894 06/17/2009 Add order by clause
	ORDER BY   --Fahad 7496 04/08/2010 Append the Order By clause QuoteID Added
	       dbo.tblViolationQuote.CallBackDate,dbo.tblViolationQuote.QuoteID
	--Fahad 6934 01/13/2010 Temp Table Created in order to maintain Tracking				
	INSERT INTO QuoteCallbackTracking
	  (
	    TicketID,
	    QuoteID
	  )
	SELECT DISTINCT tvq.TicketID_PK,
	       tvq.QuoteID
	FROM   #tempquote tvq
	WHERE  tvq.QuoteID NOT IN (SELECT qct.QuoteID
	                           FROM   QuoteCallbackTracking qct)
	
	SELECT *
	FROM   #tempquote
	
	DROP TABLE #tempquote --Droped the table to remove reference
	

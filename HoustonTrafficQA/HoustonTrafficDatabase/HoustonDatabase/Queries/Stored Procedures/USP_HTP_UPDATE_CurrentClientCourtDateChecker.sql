

/**  
* Abbas 9223 05/06/2011  
* Business Logic :  This Store procedure Update the list of all current clients with FUTURE Verified dates who have a different court date or time or court location or status for the AUTO DATE. 
System shall look at the PREVIOUS auto date IF the autodate was change with respect to Verifed court date update by the system.
If there is NO FUTURE AUTODATE (EITHER BLANK OR IT IS A PAST AUTO DATE) then do not list the case on the report. 
Make sure that Auto Date is in Future
*/

CREATE PROCEDURE [dbo].[USP_HTP_UPDATE_CurrentClientCourtDateChecker]        
          
@ticketid as int ,       --ticket number      
@TicketsViolationId as int
   
          
as          
BEGIN
	update tblTicketsViolations
	set NoChangeInCourtSetting=1
	where TicketID_PK=@ticketId and TicketsViolationId=@TicketsViolationId
END 



GO
GRANT EXECUTE ON dbo.USP_HTP_UPDATE_CurrentClientCourtDateChecker TO dbr_webuser
GO


﻿use TrafficTickets
go


/*    
* Updated By: Muhammad Nasir   
* Task: 10071  
* Date: 12/10/2012    
*     
* Business Logic: Get reminder status for bad email    
*     
*/    
ALTER Procedure [DBO].[USP_HTP_Get_Confirmation_Status]    
as    
select 
reminderid_pk as [Confirmationid_PK]
,[description] as [Description] 
from tblReminderStatus
where reminderid_pk not in (7,8)

GO 
GRANT EXECUTE ON [DBO].[USP_HTP_Get_Confirmation_Status] TO dbr_webuser
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Get_LetterOfRep]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Get_LetterOfRep]
GO

  
--USP_HTS_ESignature_Get_LetterOfRep 124546, 3991, 'polka'  
CREATE procedure [dbo].[USP_HTS_ESignature_Get_LetterOfRep]       
@ticketid int ,  
@empid   int,  
@SessionID varchar(200)  
as  
  
select distinct upper(firstname) as firstname,upper(lastname) as lastname, dbo.formatdate(dob) as dob,dlnumber,               
isnull(casenumassignedbycourt,'N/A') as casenumassignedbycourt,refcasenumber,languagespeak                         --added on 03/29/2006    zee            
, dbo.formatdateandtime_with_at_sign(V.courtdatemain) as currentdateset,  upper(firstname + ' ' + lastname) as fullname,              
'ATTN: ' + upper(C.courtcontact) as attn ,upper(C.judgename) as judgename ,                
'TEL: ' + dbo.formatphonenumbers(convert(varchar(20),C.phone)) as phone,upper(C.courtname) as courtname,               
upper(C.address)+ ' ' + upper(C.address2) as address ,upper(C.city) + ', ' + upper(E.state) + ' ' + C.zip as cityzip ,               
'FAX: ' + dbo.formatphonenumbers(convert(varchar(20),C.fax)) as FAXNO  ,upper(S.description) as description,                  
UPPER(F.address) as firmaddress, UPPER(F.address2) as firmaddress2,UPPER(F.city) as firmcity,'TX' as firmstate,              
UPPER(F.zip) as firmzip,'TEL: ' + dbo.formatphonenumbers(convert(varchar(20),F.phone)) as firmphone,                 
'FAX: ' + dbo.formatphonenumbers(convert(varchar(20),F.fax)) as firmfax,  
condition1flag ,Attorneyname ,firmsubtitle,(              
SELECT Top 1 dbo.tblCourtSettingRequest.Description              
FROM         dbo.tblCourts INNER JOIN              
              dbo.tblCourtSettingRequest ON dbo.tblCourts.SettingRequestType = dbo.tblCourtSettingRequest.SettingType INNER JOIN              
              dbo.tblTicketsViolations ON dbo.tblCourts.Courtid = dbo.tblTicketsViolations.CourtID              
WHERE     (dbo.tblTicketsViolations.TicketID_PK = t.TicketID_PK)) as courtDesc ,  
(SELECT     MAX(dbo.tblTicketsViolations.CourtDateMain) AS Expr1              
FROM         dbo.tblTicketsViolations INNER JOIN              
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID              
WHERE     (dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5)) --AND (dbo.tblTicketsViolations.CourtID = T.currentcourtloc)              
HAVING      (MAX(dbo.tblTicketsViolations.CourtDateMain) > GETDATE() + 21)) as NewCourtDate,
Convert(varchar(12),C.RepDate) as RepDate,  
V.courtid,  
(select s.description   
from tblCourtSettingRequest s,tblcourts co  
where co.settingrequesttype=s.settingtype  
and co.courtid=V.courtid  
) as SettingRequest  
  
into #HTS_LetterOfRep  
from tbltickets T,tblcourts C,tblticketsviolations V,tblcourtviolationstatus U,              
tblcourtsettingrequest S, tblfirm F,tblstate E              
where V.courtid = C.courtid              
and T.ticketid_pk = V.ticketid_pk              
and C.settingrequesttype = S.settingtype              
and c.state = E.stateid              
and T.firmid = F.firmid              
and V.courtviolationstatusidmain = U.courtviolationstatusid              
and activeflag = 1              
and categoryid in (1,2,11,12,0)              
--and c.courttype = 1                  
and settingrequesttype not in (0)                  
and V.courtid not in (3001,3002)      --zee            
and T.ticketid_pk = @ticketid                 
and V.courtviolationstatusid<>80 --newly added so no refcasenumber having 'Disposed' can come  zee            
order by isnull(casenumassignedbycourt,'N/A') desc                  
              
              
--select * from tbldatetype              
              
select *, (Select ImageStream From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_LetterOfRep_Attorney1' and TicketID = @ticketID and SessionID = @SessionID) as AttorneySign1 from #HTS_LetterOfRep  
drop table #HTS_LetterOfRep  
  
if @@error = 0 and @@rowcount > 0                   
begin                 
               
 update tblticketsViolations              
 set courtviolationstatusidmain = 104,              
 courtviolationstatusid = 104,              
 courtviolationstatusidscan = 104 ,    
 vemployeeid = @empid             
  from tbltickets T, tblticketsViolations V, tblcourtviolationstatus S              
 where T.ticketid_pk = V.ticketid_pk              
 and T.ticketid_pk = @ticketid              
 and V.courtviolationstatusidmain = S.courtviolationstatusid              
 and categoryid in (1,2,11,12,0)              
 and V.courtid not in (3004,3003) --Exclude Pasadena Court    zee          
               
 --update tbltickets set datetypeflag = 1 where ticketid_pk = @ticketid and currentcourtloc not in (3004,3003) and datetypeflag <> 1              
 update tbltickets set jpfaxdate = getdate(), employeeidupdate = @empid where ticketid_pk = @ticketid               
 return 0                  
end                
              
                
else                  
 return @@error      
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


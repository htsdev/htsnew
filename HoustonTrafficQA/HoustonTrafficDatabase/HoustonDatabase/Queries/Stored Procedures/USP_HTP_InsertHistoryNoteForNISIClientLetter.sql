USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_InsertHistoryNoteForNISIClientLetter]    Script Date: 05/15/2013 08:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************

Created By: Aziz-ur_Rehman
BuisnessLogic: This procedure is used to insert information like (opendate,username) that open the ticketdesk site  
 
List of Parameters:

@Submenuid: represents the menuid from wher user open this report. 
@Employeeid: reprensent employeeid who open this report

List of Columns:

ViewDate: this is the date on which user open the ticketdesk site.

*************************************************/
  
CREATE PROCEDURE [dbo].[USP_HTP_InsertHistoryNoteForNISIClientLetter]  
(  
 @TicketID int,  
 @EmployeeId INT,
 @FilePath VARCHAR(MAX)
)  
 
as  
  
INSERT INTO TrafficTickets.dbo.tblTicketsNotes (TicketId, [SUBJECT], RecDate, EmployeeId)
VALUES(@TicketId,'<a href="javascript:window.open(''../Reports/frmNisiLetters.aspx?ClientLatter='+CONVERT(VARCHAR,@FilePath)+''');void('''');"''>'+'Bond Forfeiture Letter' + '</a>' + ' Sent to Client. ', GETDATE(),@employeeId)


 



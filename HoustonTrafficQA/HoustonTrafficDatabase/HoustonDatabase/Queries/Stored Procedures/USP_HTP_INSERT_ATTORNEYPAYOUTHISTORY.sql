/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This procedure insert the attorney pay out printed records into the history table.
* Parameter : 
*			@EmpID
			@FirmID
			@PrintDatetime
			@Filepath
  
*/

CREATE PROCEDURE [dbo].[USP_HTP_INSERT_ATTORNEYPAYOUTHISTORY]
	@EmpID INT,
	@FirmID INT,
	@PrintDatetime DATETIME,
	@Filepath VARCHAR(200)
AS
	INSERT INTO AttorneyPayoutHistory
	  (
	    EmpID,
	    FirmID,
	    PrintDatetime,
	    Filepath
	  )
	VALUES
	  (
	    @EmpID,
	    @FirmID,
	    @PrintDatetime,
	    @Filepath
	  )
GO

	  GRANT EXECUTE ON [dbo].[USP_HTP_INSERT_ATTORNEYPAYOUTHISTORY] TO 
dbr_webuser
USE [TrafficTickets]
GO
/****** 
* Business Logic: This procedure is used to get client having bonding number 01818976 and auto status in 'NISI REVIEW','FTA WARRANT','SURETY NISI CONTESTED','NISI NO BOND ATTACHED'
*  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
ALTER procedure [dbo].[USP_RPT_Get_BondingCases]  
 (  
 @sDate datetime,  
 @eDate datetime  
 )  
  
  
as   
  
select   
replace(CauseNumber, ' ','') CauseNumber,  
TicketNumber,  
Status,  
CourtNumber  ,  
CourtDate,  
CourtTime,  
UpdateDate,  
ViolationStatusID,  
assocmidnumber,  
BarNumber,  
BondingNumber,  
AttorneyName,  
BondingCompanyName  
into #tempNisi  
from tbleventdataanlysis where barnumber in ('24026218')  
and causenumber like '%NISI%'  
  
  
  
select distinct ticketid_pk,   
nisi.causenumber, nisi.ticketnumber,    
 nisi.status [Status],          
 case when isnumeric(nisi.courtnumber)  = 0 then 'N/A' else nisi.courtnumber end  [Crt #],          
 convert(varchar(10),nisi.courtdate,101) as [Crt Date],         
 upper(dbo.formattime(nisi.courtdate)) as [Crt Time],        
 dbo.formatdateandtimeintoshortdateandtime(nisi.updatedate) as [Update Date],         
 nisi.bondingnumber [Bonding Number],  
nisi.barnumber [Bar Number],  
nisi.updatedate  
into #tempnisi1  
 from #tempNisi nisi  
	left outer join tblticketsviolations tv on 
--charindex( nisi.ticketnumber, tv.refcasenumber)>0
  --tv.refcasenumber like nisi.ticketnumber +'%'
dbo.RemoveNonNumericChar(tv.refcasenumber) = nisi.ticketnumber -- Sabir Khan 8215 03/16/2011 Matching ticket number.
 --where isnull(tv.courtviolationstatusidmain,0) <>80  
order by nisi.updatedate desc  
-- Sabir Khan 8215 03/16/2011 Update Ticket ID
update N
set N.ticketid_pk = tv.ticketid_pk
from #tempNisi1 N
left outer join tblticketsviolations tv on 
tv.refcasenumber like N.ticketnumber + '_'
where N.Ticketid_pk is null
-- Sabir Khan 8215 03/16/2011 Delete cases having null ticket number
delete from #tempNisi1 where ticketnumber is null
-- Sabir Khan 8215 03/16/2011 Update Ticket ID
update N
set N.ticketid_pk = tv.ticketid_pk
from #tempNisi1 N
left outer join tblticketsviolations tv on 
tv.refcasenumber =substring(N.Ticketnumber,0,len(N.Ticketnumber))
where N.Ticketid_pk is null

-- Sabir Khan 8215 03/16/2011 Update Ticket ID
update N
set N.ticketid_pk = tv.ticketid_pk
from #tempNisi1 N
left outer join tblticketsviolations tv on 
substring(tv.refcasenumber,0,len(tv.refcasenumber)) =substring(N.Ticketnumber,0,len(N.Ticketnumber))
where N.Ticketid_pk is null
-- Sabir Khan 8215 03/16/2011 Delete disposed cases
delete from #tempNisi1 where ticketnumber in (select dbo.RemoveNonNumericChar(refcasenumber) from tblticketsviolations where CourtviolationstatusIDmain = 80)
-- Sabir Khan 8215 03/16/2011 Delete null ticket ID cases...
delete from #tempNisi1 where ticketid_pk is null

select   
 nisi.causenumber,  
 nisi.ticketnumber,    
 isnull('<a class="label" href="../ClientInfo/ViolationFeeold.aspx?search=0&casenumber='+convert(varchar,t.ticketid_pk)+'">'+t.lastname + ', '+ t.firstname+'</a>', 'N/A') as [Defendant],      
 [Status],          
 [Crt #],          
 [Crt Date],         
 [Crt Time],        
 [Update Date],         
 [Bonding Number],  
 [Bar Number] 
from #tempnisi1 nisi  
 left outer join tbltickets t on nisi.ticketid_pk = t.ticketid_pk


--  
  
drop table #tempNisi  
drop table #tempNisi1
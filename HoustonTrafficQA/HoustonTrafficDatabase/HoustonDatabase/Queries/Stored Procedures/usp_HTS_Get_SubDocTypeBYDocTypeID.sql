/******
Alter by:  Rab Nawaz Khan  
  
Business Logic : this procedure is used to get the subdoctype for ALR(20).			  
  
List of Parameters:   
 @DocTypeID : document Type e.g ALR
  
List of Columns:    
 subdoctypeid:
 subdoctype:
   
******/


ALTER PROCEDURE [dbo].[usp_HTS_Get_SubDocTypeBYDocTypeID]
	@DocTypeID INT
AS
	SELECT subdoctypeid,
	       subdoctype
	FROM   tblsubdoctype
	WHERE  doctypeid = @doctypeid
	       --Hafiz Khalid 10296  07/04/2012 remove the alr hearing apperance witness subdoctype
	       AND subdoctypeid <> 14
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Get_GroupID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Get_GroupID]
GO



CREATE PROCEDURE USP_HTS_LA_Get_GroupID
@Load_Date As DateTime,
@List_Date As DateTime,
@GroupID Numeric Output
AS  
Insert Into Tbl_HTS_LA_Group (Load_Date, List_Date) Values(@Load_Date, @List_Date)
Set @GroupID = Scope_Identity()
Return @GroupID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/************************************************************  
 * Code formatted by SoftTree SQL Assistant © v4.0.34  
 * Time: 4/17/2009 3:51:48 PM  
 ************************************************************/  
  
/**  
* Waqas 5771 04/17/2009  
* Business Logic: This procedure will add new contact  
* Last Name, First Name, Date Of Birth will be provided.   
* NoDL, CDLFlag, EmailNotAvailable will be insert as its default values.  
**/  
  
Create PROCEDURE usp_htp_add_new_contact
	@FirstName VARCHAR(MAX),
	@LastName VARCHAR(MAX),
	@DOB DATETIME,
	@EmpID INT = null,
	@TicketID INT = null,
	@ContactID INT OUTPUT
	
AS
BEGIN
    
    DECLARE @Name AS VARCHAR(30)
    IF(@EMPID IS NOT NULL)
    begin
    SELECT @Name = (  
               SELECT ISNULL(tu.Firstname, '') + ' ' + ISNULL(tu.Lastname, '')  
               FROM   tblUsers tu  
               WHERE  tu.EmployeeID = @EMPID )
    END
    
    INSERT INTO Contact
      (
        FirstName,
        LastName,
        DOB
      )
    VALUES
      (
        @FirstName,
        @LastName,
        @DOB
      )  
    
    SELECT @ContactID = SCOPE_IDENTITY() 
    
    IF(@TicketID IS NOT NULL)
    begin
    INSERT INTO tblticketsnotes  
      (  
        ticketid,  
        SUBJECT,  
        employeeid  
      )  
    VALUES  
      (  
        @TicketID,  
        'New Contact ID <a href="javascript:window.open(' +  
        '''/ClientInfo/AssociatedMatters.aspx?cid=' + CONVERT(VARCHAR(20), @ContactID)   
        + ''');void('''');">' + CONVERT(VARCHAR(20), @ContactID) +  
        '</a> Created - ' + @Name + ' ',  
        @EMPID  
      )
      END  
      
    RETURN
END
GO

GRANT EXECUTE ON [dbo].[usp_htp_add_new_contact] TO dbr_webuser
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_ALLFLAGS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_ALLFLAGS]
GO


-- Author:		<Author,,Asghar Ali>
-- Create date: <Create Date,12/06/2007,>
-- Description:	<Description,This is used to get questions and all flags,>
-- =============================================

--		USP_HTS_GET_ALLFLAGS 125352	

CREATE PROCEDURE [dbo].[USP_HTS_GET_ALLFLAGS] 
 
	@TicketId_pk	int
AS

BEGIN

create table #temp1 (Flags varchar(1000))

declare @F as varchar(50)
	
-- Geting Bond Flag
select 
@F =case when bondflag =1 then 'BOND' else '0' end
from tbltickets 
where ticketid_pk=@TicketId_pk

if (@F <> '0')
begin
insert into #temp1 values (@F) 
end

--- Geting AccidentFlag Flag
select 
@F =case when AccidentFlag =1 then 'ACC' else '0' end
from tbltickets 
where ticketid_pk=@TicketId_pk
if (@F <> '0')
begin
insert into #temp1 values (@F) 
end

-- Geting CDLFlag Flag
select 
@F =case when CDLFlag =1 then 'CDL' else '0' end
from tbltickets 
where ticketid_pk=@TicketId_pk
if (@F <> '0')
begin
insert into #temp1 values (@F) 
end

--- Geting all Flags which selected to genral info 
insert into #temp1
select   
	te.Description as Flags
	from tblticketsflag tf inner join 
	  tbleventflags te on tf.flagid = te.flagid_pk
	where tf.ticketid_pk=@TicketId_pk order by Flags


select * from #temp1

drop table #temp1
END

------------------------------------------------------------------------------------------------------------------------------



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


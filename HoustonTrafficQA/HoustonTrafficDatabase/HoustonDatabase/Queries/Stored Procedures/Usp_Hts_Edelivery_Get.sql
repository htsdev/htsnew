GO
/****** Object:  StoredProcedure [dbo].[Usp_Hts_Edelivery_Get]    Script Date: 04/30/2008 17:21:40 ******/
/*  
Created By : Noufil Khan  
Business Logic : This procedure gives us the images of label that is generated through USPS
Parameter:@BatchId
*/

--[Usp_Hts_Edelivery_Get] '1,2,3,88,43597,43606,'
--SELECT * FROM tblhtsbatchprintletter where DeliveryConfirmationLabel is not null

CREATE procedure [dbo].[Usp_Hts_Edelivery_Get]  
@BatchId varchar(max)
as

declare @tblletters  table(id int IDENTITY PRIMARY KEY,BatchId_Pk int)            
insert into @tblletters       

select MainCat from dbo.sap_dates(@BatchId)               
select DeliveryConfirmationLabel  from tblhtsbatchprintletter  tbpl 
inner join @tblletters tl 
	on tbpl.BatchId_pk  = tl.BatchId_Pk 

where DeliveryConfirmationLabel is not null

go
GRANT EXEC ON [dbo].[Usp_Hts_Edelivery_Get] to dbr_webuser
go 







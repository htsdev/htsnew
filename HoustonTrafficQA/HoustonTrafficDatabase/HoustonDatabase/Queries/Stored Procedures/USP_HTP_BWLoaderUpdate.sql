USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_BWLoaderUpdate]    Script Date: 04/05/2010 15:30:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************
* 
Altered By: Sabir Khan
Dated:		03/30/2010

Business Logic:
	This procedure is used to update all B/W cases having auto future jury and auto future arriagment.

Input Parameters:
	N/A


**********************/
-- exec  USP_HTP_BWLoaderUpdate
ALTER procedure [dbo].[USP_HTP_BWLoaderUpdate]

as

declare @LoaderID int, 
		@LoaderName varchar(100)

select @Loaderid = LoaderId, @LoaderName = LoaderDescription
from tblloaders where Loaderid = 7


-- get all B/W cases having future Auto jury trials for HMC courts...
select distinct  v.ticketid_pk, v.ticketsviolationid, v.courtid,
	v.courtdate, v.courtnumber, v.courtviolationstatusid,
	v.courtdatemain, v.courtnumbermain, v.courtviolationstatusidmain
INTO #Events
from tblticketsviolations v inner join tbltickets t on t.ticketid_pk = v.ticketid_pk
inner join tblcourtviolationstatus sa on sa.courtviolationstatusid = v.courtviolationstatusid 

-- only active clients..
where t.activeflag = 1

-- only cases having verified status bond waiting..
and v.courtviolationstatusidmain = 202

-- only cases having auto primary status JURY TRIAL..
--Sabir Khan 7616 03/29/2010 added Arraigment Category...
and sa.categoryid IN (2,4)

-- ONLY CASES HAVING AUTO COURT DATE IN FUTURE..
and datediff(day, v.courtdate, getdate()) < 0

-- ONLY HMC CASES..
and v.courtid in (3001,3002,3003)

---- EXCLUDE CLIENT IF THERE ARE OTHER CASES IN PROFILE OTHER THEN B/W OR DISP..
--and NOT EXISTS  (
--		select ticketid_pk from tblticketsviolations 
--		where ticketid_pk = t.ticketid_pk and courtviolationstatusidmain not in (80,202,)
--		)

-- EXCLUDE CLIENTS IF THERE IS A SPLIT AUTO SETTING INFORMATION...
and not exists (
		select a.ticketid_pk from tblticketsviolations a inner join tblcourtviolationstatus c2
		on c2.courtviolationstatusid = a.courtviolationstatusid
		where a.ticketid_pk = t.ticketid_pk 
		and v.CourtViolationStatusIDmain NOT IN (80,236)  --Sabir Khan 7616 03/29/2010 Excluding disposed and no hire cases...
		and 
			(
			a.courtdate!= v.courtdate or
			convert(int,a.courtnumber) != convert(int, v.courtnumber) or
			c2.categoryid != sa.categoryid
			)
		)
order by v.ticketid_pk 


 --sabir Khan 7616 03/29/2010 Status has been added for differences of arriagment and jury trial....
alter table #Events add violcount INT,STATUS INT DEFAULT NULL 

--ADD 1 for Arrignment cases....
UPDATE #Events
SET STATUS = 1 WHERE courtviolationstatusid IN (SELECT CourtViolationStatusID  FROM tblCourtViolationStatus WHERE CategoryID = 2)

--ADD 2 for Jury cases....
UPDATE #Events
SET STATUS = 2 WHERE STATUS IS NULL AND courtviolationstatusid IN (SELECT CourtViolationStatusID  FROM tblCourtViolationStatus WHERE CategoryID = 4)

--Sabir Khan 7616 04/05/2010 Remove the cases having verified court date is greater then auto court date.
DELETE E
FROM #Events E INNER JOIN tblticketsviolations v on v.ticketsviolationid = E.ticketsviolationid
WHERE DATEDIFF(DAY,v.CourtDateMain,E.courtdate) <=0 

alter table #Events add rowid int identity(1,1)

-- getting violation count for each client...
select ticketid_pk, count(ticketsviolationid) violcount
into #violcount from #Events group by ticketid_pk

update b set b.violcount= a.violcount
from #Events b, #violcount a where a.ticketid_pk = b.ticketid_pk

--select * from #events

declare @idx int, @reccount int,  @ticketid int , @ticketsviolationid int,  @empid int  ,@senddate datetime,
		@IsTrial int , @ViolCount int, @IdxViol int, @TempTicketId int, @SendTrialLetter bit, @lettercount INT,@STATUS INT

DECLARE @Client table(ticketsviolationid int, ticketid_pk int);

select @ViolCount = 0 , @IdxViol = 0, @TempTicketId = 0, @SendTrialLetter = 0 ,@senddate = getdate(), @empid = 3992
select @idx = 1, @reccount = count(rowid), @ticketid = 0, @ticketsviolationid = 0, @lettercount = 0
from #Events

While @idx <= @reccount
begin
	select	@ticketid = ticketid_pk, @ticketsviolationid = ticketsviolationid , 
			@ViolCount = violcount, @STATUS = STATUS 
	from #Events where rowid = @idx

	if @ticketid != @TempTicketId
		begin
			set @TempTicketId = 0
			set @IdxViol = 1
			set @SendTrialLetter = 0
		end

	-- UPDATE CASE SETTING INFORMATION IN CLIENTS....
	update V    
	set v.courtviolationstatusidmain = c.courtviolationstatusid,
	v.courtdatemain = c.courtdate,
	v.courtnumbermain = c.courtnumber,
	v.courtid = case c.courtnumber     
					 when '13' then 3002    
					 when '14' then 3002    
					 when '18' then 3003    
					 else 3001    
				 end ,
	V.updateddate = getdate(),
	v.vemployeeid = @empid
	OUTPUT INSERTED.ticketsviolationid, inserted.ticketid_pk  into @Client 
	from tblticketsviolations v inner join #Events c on v.ticketsviolationid = c.ticketsviolationid
	and c.ticketsviolationid = @ticketsviolationid

	-- add note in case history..
	if @ViolCount =  @IdxViol 
		begin
			insert into tblticketsnotes (ticketid, subject , employeeid)
			select @ticketid, 'Bond Waiting Event Loader', @empid
		end

	-- SENDING TRIAL LETTER IN BATCH PRINT....
	--Sabir Khan 7616 03/29/2010 added check for sending trial letter to batch only for jury trial
	IF(@STATUS = 2)
	BEGIN
		-- validating the conditions required to send a trial letter....
		create table #validate (reccount int)
		insert into #validate exec usp_hts_trialletter_validation @ticketid
		select @IsTrial = isnull(sum(isnull(reccount,0)),0) from #validate
		delete from #validate drop table #validate

		-- if trial letter logic is satisfied, NO LETTER flag is not set 
		if (@IsTrial  = 0 and dbo.fn_HTP_HasFlag(@ticketid, 7) = 0)
			begin
				set @SendTrialLetter = 1
			end

		-- if it is last violation of a client then send trial letter in batch...
		-- to send the trial letter only once in batch print if client has multiple violations...
		if @ViolCount =  @IdxViol and @SendTrialLetter = 1
			begin
				-- delete existing letter in batch print....
				exec USP_HTS_BATCHLETTERS_DELETE_EXISTING @ticketid

				-- sending letter in batch print...
				exec USP_HTS_Insert_BatchPrintLetter 
					@ticketid_fk = @ticketid,
					@batchdate = @senddate,
					@printdate = '1/1/1900',
					@isprinted = 0, 
					@letterid_fk = 2,
					@FromBSDA = 0,
					@empid = @empid,
					@docpath = null

				set @lettercount = @lettercount + 1
			end
    END

	select	@TempTicketId = @ticketid,
			@ticketid = 0 , 
			@ticketsviolationid = 0, 
			@IsTrial = null,
			@ViolCount = null

	set @IdxViol = @IdxViol + 1
	set @idx = @idx + 1
End

-- dump data in A/W loader update history table...

insert into LoaderResetHistory 
	(
	ticketid_pk, ticketsviolationid, courtid, 
	courtdate, courtnumber,	courtviolationstatusid, 
	courtdatemain, courtnumbermain, courtviolationstatusidmain, 
	vemployeeid, LoaderUpdateDate, loaderid
	)
	
select ticketid_pk, ticketsviolationid, courtid,
	courtdate, courtnumber, courtviolationstatusid,
	courtdatemain, courtnumbermain, courtviolationstatusidmain, 
	@empid, getdate(), @LoaderId
from #events 

-- sending email to management with the statistics...
--Sabir Khan 7616 03/29/2010 modified for seperating total count of arriagment and jury trial...
declare @message varchar(MAX), @subject varchar(100), @totalclientsArr int, @totalviolationsArr int, @totalclientsJury int, @totalviolationsJury int ,
		@compTime varchar(50) 
select	@compTime = dbo.fn_DateFormat(getdate(),28,'/',':',1),
		@totalclientsArr = count(distinct E.ticketid_pk) ,
		@totalviolationsArr = count(distinct E.ticketsviolationid)
from #Events E inner join @Client C on E.ticketsviolationid =C.ticketsviolationid and E.TicketID_pk =C.TicketId_pk WHERE E.STATUS = 1

select	@totalclientsJury = count(distinct E.ticketid_pk) ,@totalviolationsJury = count(distinct E.ticketsviolationid)
from #Events E inner join @Client C on E.ticketsviolationid =C.ticketsviolationid and E.TicketID_pk =C.TicketId_pk WHERE E.STATUS = 2

--Yasir Kamal 7616 04/02/2010 duplicate violations issue fixed.
SELECT DISTINCT C.Ticketid_pk,tv.refcasenumber AS ticketnumber,t.firstname,t.lastname,tsv.description,tv.TicketsViolationID INTO #result
FROM  @Client C INNER JOIN #events E ON e.ticketid_pk = c.ticketid_pk
INNER JOIN tbltickets t ON t.TicketID_PK = c.ticketid_pk
INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = c.ticketid_pk AND tv.TicketsViolationID = C.ticketsviolationid
INNER JOIN tblCourtViolationStatus tsv ON tsv.CourtViolationStatusID = tv.CourtViolationStatusIDmain

set @subject = @LoaderName +  ' Completed - ' + convert(varchar(10), getdate(),101)

set @message  = 
@LoaderName +  ' completed at: ' +  @compTime  + '.<br>
Following are the details<br><br>
<table width="300px" border="1" cellpading="0" cellspacing="0" style="bordercolor: black">
<tr><td width="60%"></td><td align="left" width="20%">Arraignment</td><td align="left" width="20%">Jury Trial</td></tr>
<tr><td width="60%">Total number of clients updated</td><td align="right" width="20%">' + convert(varchar,@totalclientsArr)       + '</td><td align="right" width="20%">' + convert(varchar,@totalclientsJury)       + '</td></tr>
<tr><td width="60%">Total number of violations updated</td><td align="right" width="20%">' + convert(varchar,@totalviolationsArr)       + '</td><td align="right" width="20%">' + convert(varchar,@totalviolationsJury)       + '</td></tr>
<tr><td width="60%">Total number of trial letters sent to batch print</td><td align="right" width="20%"></td><td align="right" width="20%">' + convert(varchar,@lettercount) +  '</td></tr>
</table><br><br>


Following are the detail of updated cases:<br><br>
<table width="500px" border="1" cellpading="0" cellspacing="0" style="bordercolor: black">
<tr><td width="20%"> Ticket Number </td><td width="30%">First Name</td><td width="30%"> Last Name</td><td width="20%"> Updated Status</td></tr>' 

SELECT  @message  = @message  + '<tr><td width="20%"><a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&caseNumber='+ convert(varchar(20),ticketid_pk) + '")>' + Ticketnumber + '</a></td><td width = "30%">'+ FirstName + '</td><td width = "30%">' + LastName + '</td><td width = "20%">' + Convert(VARCHAR(MAX),Description) +'</td></tr>
'
FROM #result

SET  @message  = @message  +'</table><br><br>
This is a system generated email!<br>' 

EXEC msdb.dbo.sp_send_dbmail                           
    @profile_name = 'Data Loader',                           
    @recipients =   'dataloaders@sullolaw.com',            
	@copy_recipients  = 'tahir@lntechnologies.com',            
    @subject = @Subject,                           
    @body = @Message,                         
    @body_format = 'HTML' ;

drop table #events
drop table #violcount
DROP TABLE #result


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_Get_BondingCases_bkp_11SEP07]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_Get_BondingCases_bkp_11SEP07]
GO

  
  
  
  
CREATE procedure [dbo].[USP_RPT_Get_BondingCases_bkp_11SEP07]  
 (  
 @sDate datetime,  
 @eDate datetime  
 )  
  
  
as   
  
select   
 replace(v.causenumber,' ','') [Cause Number],   
 v.ticketnumber [Case Number],   
-- t.lastname + ', '+ t.firstname [Defendant],  
 v.status [Status],    
 case when isnumeric(v.courtnumber)  = 0 then 'N/A' else v.courtnumber end  [Crt #],    
 convert(varchar(10),v.courtdate,101) as [Crt Date],   
 upper(dbo.formattime(v.courtdate)) as [Crt Time],  
 dbo.formatdateandtimeintoshortdateandtime(v.updatedate) as [Update Date],   
 v.bondingnumber [Bonding Number]  
  
  
into #temp   
from tbleventdataanlysis v--,  tblcourtviolationstatus c--, tblticketsarchive t  
where  v.bondingnumber like '01818976'  
--and   t.recordid  = v.recordid   
--and v.violationstatusid = c.courtviolationstatusid  
--and v.violationstatusid in (173,186,194,229)  
and v.status in ('NISI REVIEW','FTA WARRANT','SURETY NISI CONTESTED','NISI NO BOND ATTACHED')  
--and datediff(day, v.updateddate, @sdate)<=0  
--and datediff(day, v.updateddate, @edate)>=0  
order by v.courtdate desc  
  
select distinct * from #temp  
drop table #temp
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿--Business Logic : This procedure displays one reports 
--*					1. Service ticket report.
--*					
--Noufil 4641 08/19/2008 default email addresses add for SQL job
-- [USP_HTp_ValidationEmailReportHoustoncivil] 'agha@lntechnologies.com', 1,''
-- Noufil 4432 08/12/2008 Procedure updated... New method use for validation email.
-- Noufil 4746 09/05/2008 Email subject Change from "ALR Subpeona Alert" to "Validation Report (Civil)"

ALTER procedure [dbo].[USP_HTP_ValidationEmailReportHoustoncivil]
(      
	@EmailAddress varchar(500) = '',
	@DisplayReport bit = 0 ,
	@ThisResultSet varchar(max)='' output           
)                              
AS   

-------------------------------------------------------------------------------------------------------------------------------------------
if ( @EmailAddress = '')
BEGIN
	-- Noufil 6107 07/06/2009 Declare Parameter for Hyperlink
	declare @SerialHyperlink varchar(1000)
	set @SerialHyperlink = 'ln.legalhouston.com'

	declare @ServerIP varchar(100)
	CREATE TABLE #tempIP (Results varchar(1000) null) 
	BULK INSERT #tempIP
	FROM 'C:\Program Files\Microsoft SQL Server\SQLServerIP.txt'

	select @ServerIP= results from #tempIP

	drop table #tempIP
	
	-- Noufil 6107 07/06/2009 Set Server name for URL with respect to server IP
	if @ServerIP='66.195.101.51' --Live Settings
	 begin
		select @EmailAddress =  'hvalidation@sullolaw.com'
		set @SerialHyperlink = 'ln.legalhouston.com'
	 end
	 else if @ServerIP='192.168.168.220'  --Developer Settings
	 begin
		select @EmailAddress = 'dev@lntechnologies.com'
		set @SerialHyperlink = 'newpakhouston.legalhouston.com'
	 end
	 else -- QA Settings
	 begin
		select @EmailAddress = 'QA@lntechnologies.com'
		set @SerialHyperlink = 'qa.houston.com'
	 end
end
--------------------------------------------------------------------------------------------------------------------------------------------------

declare @ThisResultidServicecivil varchar(max)
declare @ThisPendingCount varchar(5)
declare @ThisAlertCount varchar(5)
declare @ThisTotalCount varchar(5)

declare @Param varchar(200)
Set @Param = '''''' + convert(varchar(20),getdate(), 101) + ''''', ''''' + convert(varchar(20),getdate(), 101) + ''''', 3992,1,1,4,1'
-- Zeeshan Ahmed 4861 Rename Service Ticket Report And Set Case Type 4 To Display Only Family Law Cases
--Sabir Khan 5738 06/03/2009 Service Ticket follow update date...
delete from summaryreport
execute USP_HTP_Generics_Get_XMLSet_For_Validation 
			'traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords ', 
			 @Param, 
			'Service Ticket Report (Family)',
			--Ozair 6829 10/21/2009 FID column added
			'FID,TicketID_PK,casetypename as [Division],category as [Category],ClientName as [Name],courtdate as [Crt Date],courttime as [Crt Time],shortdescription as [Status],settinginformation as [Loc],recsortdate as [Open D],assignedto as [Open By],rec as [Update D],priority as [Priority],completion as [Comp.%], followupdate as [Follow Up Date]',
			-- Noufil 6107 07/06/2009 Two new paramter '[Follow Up Date]',@SerialHyperlink added.
			1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultidServicecivil output, @PendingCount = @ThisPendingCount OutPut,@AlertCount =@ThisAlertCount output  ,@TotalCount=@ThisTotalCount output

set @ThisResultSet= isnull(@ThisResultidServicecivil,'') 

set @ThisResultSet = replace(@ThisResultSet, '&lt;','<')                               
set @ThisResultSet= replace(@ThisResultSet, '&gt;','>')  
select (@ThisResultSet)

if isnull(@ThisResultSet,'') <> '' and @EmailAddress <> ''
Begin
declare @subject varchar(max)
set @subject= 'Validation Report (Civil) '+ convert(varchar(20),getdate(),101)

EXEC msdb.dbo.sp_send_dbmail
    @profile_name = 'Traffic System',
    @recipients = @EmailAddress,
    @subject = @subject,
    @body = @ThisResultSet,
    @body_format = 'HTML';
End






﻿/****** 
Created by:		Fahad Muhammad Qureshi.
TaskID:			6934
Created Date:	11/20/2009
Business Logic:	The procedure is used by Houston Traffic Program for validation to get information about those all Quote Clients having Court date in future.		
List of Parameters: N/A
******/
ALTER PROCEDURE [dbo].[USP_HTP_Get_BeforeQouteCallDate]
AS
	DECLARE @businessDay   INT
	SELECT @businessDay = (
	           SELECT CONVERT(INT, attribute_value)
	           FROM   Tbl_BusinessLogicDay tbld
	           WHERE  tbld.Report_Id = (
	                      SELECT report_id
	                      FROM   tbl_Report tr
	                      WHERE  tr.Report_Url = 
	                             '/Reports/BeforeQouteCallBack.aspx'
	                  )
	       )
	--Ozair 7496 03/05/2010 next business date saved in variable to use in where caluse to reduce query time
	DECLARE @businessDate  DATETIME
	SELECT @businessDate = dbo.fn_getnextbusinessday (GETDATE(), CONVERT(INT, @businessDay)) 
	
	SELECT DISTINCT
	       CONVERT(VARCHAR(12), tvq.CallBackDate, 101) AS CallBackDate,
	       CONVERT(VARCHAR(12), tvq.LastContactDate, 101) AS LastContactDate,
	       CONVERT(INT, GETDATE() - tvq.LastContactDate) AS Days,
	       tt.TicketID_PK,
	       tt.Firstname + ' ' + tt.Lastname AS Customer,
	       tt.calculatedtotalfee,
	       CONVERT(VARCHAR(12), ttv.CourtDateMain, 101) AS CourtDateMain,
	       tcvs.ShortDescription AS [STATUS],
	       [CaseType] = (
	           SELECT ct.CaseTypeName
	           FROM   CaseType ct
	           WHERE  ct.CaseTypeId = tt.CaseTypeId
	       ),
	       tc.ShortName AS shortcourtname,
	       REPLACE(tqr.QuoteResultDescription, '---------------------', ' ') AS 
	       QuoteResultDescription
	FROM   tblViolationQuote tvq
	       LEFT OUTER JOIN tblQuoteResult tqr
	            ON  tqr.QuoteResultID = tvq.FollowUPID
	       LEFT OUTER JOIN tblTickets tt
	            ON  tvq.TicketID_FK = tt.TicketID_PK
	       INNER JOIN tblTicketsViolations ttv
	            ON  ttv.TicketID_PK = tt.TicketID_PK
	       INNER JOIN tblCourts tc
	            ON  tc.CourtID = ttv.CourtID
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusID
	WHERE  -- All Quote Cients
	       tt.Activeflag = 0
	       --Follow up should be Yes
	       AND tvq.FollowUpYN = 1 
	           --Case Status Category Checks implemented according to Case Type
	       AND (
	               (tt.casetypeid = 1 AND tcvs.CategoryID = 2)
	               OR (
	                      tt.casetypeid IN (2, 3, 4)
	                      AND tcvs.CategoryID IN (3, 4, 5)
	                  )
	           )
	           -- Check for before business day check from court date
	           --Ozair 7496 03/05/2010 next business date variable used
	       AND DATEDIFF(DAY, ttv.CourtDateMain, @businessDate) = 0
GO


  
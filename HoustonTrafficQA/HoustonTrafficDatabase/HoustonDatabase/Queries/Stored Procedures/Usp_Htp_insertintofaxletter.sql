
/****** Object:  StoredProcedure [dbo].[Usp_Htp_insertintofaxletter]    Script Date: 05/22/2008 14:21:43 ******/
/*  
Created By : Noufil Khan  
Business Logic : This procedure insert records in fax Letter Table

*/

-- Noufil 3999 06/19/2008 This procedure insert records in fax Letter Table
CREATE procedure [dbo].[Usp_Htp_insertintofaxletter]  
@ticketid int,  
@empid int ,  
@ename varchar(200),  
@eemail varchar(200),  
@faxno varchar(50),  
@subject varchar(200),  
@body varchar(500),  
@status varchar(500),  
@faxdate datetime,  
@docpath varchar(100)  
  
as  
  
if not exists   
  (select * from faxletter   
   where Ticketid=@ticketid and Employeeid=@empid and EmployeeName=@ename and EmployeeEmail=@eemail and  
      FaxNumber=@faxno and Subject=@subject and Body=@body and status=@status and FaxDate=@faxdate  
     and Docpath=@docpath)  
begin  
insert into faxletter  
(Ticketid,Employeeid,EmployeeName,EmployeeEmail,FaxNumber,Subject,Body,status,FaxDate,Docpath)  
values  
(@ticketid,@empid,@ename,@eemail,@faxno,@subject,@body,@status,@faxdate,@docpath)  
end  

go
GRANT EXEC ON [dbo].[Usp_Htp_insertintofaxletter] to dbr_webuser
go 
--Fahad 5753 04/13/2009 Business Logic added

/*        
    
Business Logic  : This procedure Retrive all records having split setting information of clients which are not from Criminal.      
           
Parameter:       
   @ShowAll     : if checked then all records having all type of follow Up date/ 
				  else records having follow Update in past and present only.   
   
     
      
*/      
    
-- Noufil Temp table code comment    
-- Noufil 4432 08/05/2008 html serial number added added for validation report    
-- exec [dbo].[usp_splitreport_new] 0,1 
ALTER procedure [dbo].[usp_splitreport_new]    
@ShowAll BIT,
@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
as                     
    
    
Select         
  ttv.TicketID_PK, 
  tt.Firstname, 
  tt.Lastname,
  ISNULL(CONVERT(VARCHAR(20), tt.SplitCaseFollowupDate, 101), '') AS FollowUpDate, --Fahad 5753 04/10/2009 add follow update 
  dbo.fn_getnextbusinessday(GETDATE(), 5) AS NextFollowUpDate,	--Fahad 5753 04/10/2009 add next five business days to get next follow update
  -- Noufil 5753 04/17/2009 COurt Date added according to our standard, comment the old one.
  --dbo.fn_DateFormat(ttv.CourtDateMain,25,'/',':',1) AS VerifiedCourtInfo,
 --Sabir Khan 7979 07/02/2010 No need to convert into int. 
  ( dbo.fn_DateFormat(ttv.CourtDateMain,25,'/',':',1) + '  #  ') + (dbo.Formatcourtnumbers(CONVERT(VARCHAR(3),ISNULL(dbo.Formatcourtnumbers(ttv.CourtNumbermain), '0'))) + '  ' + cvs.ShortDescription) AS VerifiedCourtInfo,--Fahad 5753 04/10/2009 Concatenate all setting info of the client
  ISNULL(tt.Midnum, '') AS Midnum, 
  ISNULL(ttv.casenumassignedbycourt, '') AS casenumassignedbycourt,
  ttv.RefCaseNumber AS RefCaseNumberseq,
  ttv.CourtDateMain, tc.ShortName, 
  ttv.CourtID, 
  ttv.CourtNumbermain  
      

from tblTicketsViolations ttv           
inner join tblcourtviolationstatus cvs on cvs.courtviolationstatusid = ttv.courtviolationstatusidmain           
inner join  dbo.tblTickets TT on TT.ticketId_pk = ttv.ticketId_pk    
inner join tblcourts TC on TC.courtid = ttv.courtid          
where TT.ticketId_Pk in     
      (    
      select  DISTINCT tt.TicketId_Pk    
      from tblticketsviolations v          
      inner join tblcourtviolationstatus cvs on cvs.courtviolationstatusid = v.courtviolationstatusidmain    
      inner join  dbo.tblTickets TT on TT.ticketId_pk = v.ticketId_pk    
      inner join tblcourts TC on TC.courtid = v.courtid    
       where     
        tt.ticketid_Pk in    
          (          
          Select ticketId_Pk from     
            (    
               select distinct     
              v.ticketid_pk,     
              v.courtdatemain,    
              v.courtnumbermain,    
              cvs.categoryid,    
              cvs.courtviolationstatusid    
               from tblticketsviolations v     
              inner join tbltickets t on t.ticketid_pk = v.ticketid_pk           
              inner join tblcourts tc on tc.courtid = v.courtid    
              inner join tblcourtviolationstatus cvs on v.courtviolationstatusidmain = cvs.courtviolationstatusid    
               where t.activeflag = 1    
                 and tc.iscriminalcourt = 0    
                 and cvs.categoryid in (3,4,5)    
                 and datediff(day, v.courtdatemain, getdate()) < 0    
            ) as a    
    
          where  a.courtdatemain <> v.courtdatemain  or convert(VARCHAR(3),a.courtnumbermain) <> convert(VARCHAR(3),v.courtnumbermain)  or a.categoryid<> cvs.categoryid       
          )    
       and cvs.categoryid in (3,4,5)     
       AND V.RefCaseNumber <>'0'    
       )    
    
and TT.ticketId_Pk not in ( select ticketid_pk from tblticketsflag  where ticketid_pk =  ttv.ticketid_pk  and flagid = 12) 
AND 
(
	(@ShowAll = 1 OR @ValidationCategory=2)  -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
	OR 
	((@ShowAll = 0 OR @ValidationCategory=1) AND DATEDIFF(dd, ISNULL(tt.SplitCaseFollowupDate, '01/01/1900'), GETDATE()) >= 0) -- Saeed 7791 07/10/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
) --Fahad 5753 04/10/2009 set the records fetching according to checked or unchecked status.
ORDER BY ttv.TicketID_PK




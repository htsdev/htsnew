SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_Update]
GO


--usp_WebScan_Update 171,'2006TR0287649','1/12/2006','1/12/2006','8:00 AM','JURY TRIAL','Lubbock','6','5/2/2006','5/2/2006 10:00 AM'    
CREATE procedure [dbo].[usp_WebScan_Update]     
        
@PicID int,        
@CauseNo varchar(50),        
@NewCourtDate varchar(50),        
@TodayDate varchar(50),    
@Time varchar(50),        
@Status varchar(50),        
@Location varchar(50) ,      
@CourtNo varchar(3),    
@ScanUpdateDate datetime,    
@CourtDateScan datetime ,  
@Flag int=0 output    
       
        
as        
        
    
begin     
    
Update tbl_WebScan_OCR        
        
set        
 CauseNo=@CauseNo,        
 NewCourtDate=@NewCourtDate,      
 CourtNo=@CourtNo,         
 TodayDate=@TodayDate,        
 Time=@Time,        
 Location=@Location,        
 Status=@Status        
        
where        
 PicID=@PicID     
end    
    
  
declare @TempCauseNo as varchar(50)  
select @TempCauseNo =casenumassignedbycourt from tblticketsviolations where casenumassignedbycourt=@CauseNo  
  
if    @TempCauseNo <> 'NULL'  
begin  
  
declare @TempCID as varchar(3)    
declare @CourtNumScan as varchar(50)    
    
set @TempCID=@CourtNo    
    
if @TempCID='13' or @TempCID='14'    
 begin    
  set @CourtNumScan='3002'    
 end    
else if @TempCID='18'    
 begin    
  set @CourtNumScan='3003'    
 end    
 -- Abbas Qamar 05-12-2012 10088 Setting HMC-W courtId for room no 20
 else if @TempCID='20'    
 begin    
  set @CourtNumScan='3075'    
 end    
else    
 begin    
  set @CourtNumScan='3001'    
 end    
    
declare @tempCVID int    
Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status    
    
    
    
update tblticketsviolations    
    
set     
 courtviolationstatusidscan=@tempCVID,    
 courtnumberscan=@CourtNo,    
 scanupdatedate=@ScanUpdateDate,    
 courtdatescan=@CourtDateScan,    
 CourtIDScan=@CourtNumScan,    
 vemployeeid=3991    
     
where     
-- Abbas Qamar 05-12-2012 10088 Setting HMC-W courtId
casenumassignedbycourt= @CauseNo and CourtID in (3001,3002,3003,3075)  

	update tbl_webscan_ocr
	set scanverified=1
	where  PicID=@PicID     
	
	set @Flag=1  
end  
 else  
begin  
 set @Flag=2  
end  
  
return @Flag  
    
    
--select courtviolationstatusidscan,courtnumberscan,scanupdatedate,courtdatescan,CourtIDScan from tblticketsviolations where casenumassignedbycourt='2006TR0287649'


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


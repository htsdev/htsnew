﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 9:11:54 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 

******/




ALTER PROCEDURE dbo.USP_HTP_GetPatientIdbyTicketId
	@TicketId INT
AS
	DECLARE @PatientId INT
	SET @PatientId = 0
	
	SELECT @PatientId = P.PatientId
	FROM   Patient p
	WHERE  p.TicketId = @TicketId
	
	SELECT @PatientId
GO

GRANT EXECUTE ON dbo.USP_HTP_GetPatientIdbyTicketId TO dbr_webuser
GO 	      
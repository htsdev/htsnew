/*******  
  
Created By : Zeeshan Ahmed  
  
Business Logic : This procedure is used to get case type.  
  
List of Columns :   
  
  CaseTypeID : Case Type Id  
  CaseTypeName : Case Type Name  
  
*******/  
  
CREATE PROCEDURE dbo.USP_HTP_GET_ALL_CASETYPE  
AS  
BEGIN  
  
Select CaseTypeId , CaseTypeName From CaseType  
  
END   

GO

Grant execute on dbo.USP_HTP_GET_ALL_CASETYPE   to dbr_webuser

GO
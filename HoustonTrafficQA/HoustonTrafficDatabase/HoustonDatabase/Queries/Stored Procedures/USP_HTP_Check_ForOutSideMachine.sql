USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Check_ForOutSideMachine]    Script Date: 06/03/2013 17:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
* This stored procedure is used to get all company name
*/

CREATE Procedure [dbo].[USP_HTP_Check_ForOutSideMachine]
@ClientIPAddress varchar(100)
as
select * from branch 
WHERE SUBSTRING(@ClientIPAddress,0,11) = substring(Branchstartwith,0,11)



GO

GRANT EXECUTE ON USP_HTP_Check_ForOutSideMachine TO dbr_webuser

GO


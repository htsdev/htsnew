﻿USE [TrafficTickets] 
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Send_Pearland_DayBeforeArraignment_Letters]    Script Date: 08/05/2013 12:28:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
* Created By: Rab Nawaz Khan
* Task ID:	11108
* Date	:	08/16/2013	
Business Logic:	The procedure is used by LMS application to get data for Pearland Appearance Day of letter
				AND to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, IF prINTing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category IF prINTing for individual court hosue.
	@startListdate:	Comma separated dates selected to prINT letter.
	@EmpId:			Employee id for the currently logged in user
	@PrINTType:		Flag to just preview the letter or to mark letters as sent. IF 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies IF searching by court date or by list date. 0= list date, 1 = court date
	@isPrINTed:		Flag to include already prINTed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	IF @PrINTType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	CourtDate:		Court date associated with the violation
	Abb:			Short abbreviation of employee who is prINTing the letter


******/

-- usp_mailer_Send_Pearland_DayBeforeArraignment_Letters 38,144,38,'08/20/2013,',3992,0,0,0
ALTER PROCEDURE [dbo].[usp_mailer_Send_Pearland_DayBeforeArraignment_Letters]
	@catnum INT=38,
	@LetterType INT=144,                                    
	@crtid INT=38,
	@startListdate VARCHAR (500) ='08/13/2013',                                    
	@empid INT=3992,
	@prINTtype INT =0,
	@searchtype INT = 0,
	@isprinted BIT = 0
AS
BEGIN                                                       
	-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                
	DECLARE @officernum VARCHAR(50),                                    
	@officeropr VARCHAR(50),                                    
	@tikcetnumberopr VARCHAR(50),                                    
	@ticket_no VARCHAR(50),                                                                    
	@zipcode VARCHAR(50),                                             
	@zipcodeopr VARCHAR(50),                                                                       
	@fineamount MONEY,                                            
	@fineamountOpr VARCHAR(50) ,                                            
	@fineamountRelop VARCHAR(50),                   
	@singleviolation MONEY,                                            
	@singlevoilationOpr VARCHAR(50) ,                                            
	@singleviolationrelop VARCHAR(50),                                    
	@doubleviolation MONEY,                                            
	@doublevoilationOpr VARCHAR(50) ,                                            
	@doubleviolationrelop VARCHAR(50),                                  
	@count_Letters INT,                  
	@violadesc VARCHAR(500),                  
	@violaop VARCHAR(50),
	@endlistdate VARCHAR (500)                                     

	-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL...                                    
	DECLARE @sqlquery VARCHAR(MAX),@sqlquery2 VARCHAR(MAX), @lettername VARCHAR(50) 
	SET @lettername = 'DAY BEFORE ARRAIGNMENT' 
	SELECT @sqlquery =''  , @sqlquery2 = ''
	                                            
	-- ASSIGNING VALUES TO THE VARIABLES 
	-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
	-- FOR THIS LETTER TYPE....
	SELECT @officernum=officernum,                                    
	@officeropr=oficernumOpr,                                    
	@tikcetnumberopr=tikcetnumberopr,                                    
	@ticket_no=ticket_no,                                                                      
	@zipcode=zipcode,                                    
	@zipcodeopr=zipcodeLikeopr,                                    
	@singleviolation =singleviolation,                                    
	@singlevoilationOpr =singlevoilationOpr,                                    
	@singleviolationrelop =singleviolationrelop,                                    
	@doubleviolation = doubleviolation,                                    
	@doublevoilationOpr=doubleviolationOpr,                                            
	@doubleviolationrelop=doubleviolationrelop,                  
	@violadesc=violationdescription,                  
	@violaop=violationdescriptionOpr ,              
	@fineamount=fineamount ,                                            
	@fineamountOpr=fineamountOpr ,                                            
	@fineamountRelop=fineamountRelop              
	                                 
	FROM tblmailer_letters_to_sendfilters                                    
	WHERE courtcategorynum=@catnum                                    
	AND Lettertype=@LetterType                                  


	-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
	-- EMPLOYEE IN THE LOCAL VARIABLE....
	DECLARE @user VARCHAR(10)
	SELECT @user = upper(abbreviation) FROM tblusers WHERE employeeid = @empid
	                                    
	-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
	SET @sqlquery=@sqlquery+'
	SELECT DISTINCT ta.recordid,
		   CONVERT(VARCHAR(10), tva.courtdate, 101) AS listdate,
		   CONVERT(VARCHAR(10), tva.TicketViolationDate, 101) AS OffenceDate,
		   tva.courtdate,
		   tva.courtlocation courtid,
		   flag1 = ISNULL(ta.Flag1, ''N''),
		   ta.officerNumber_Fk,                                        
		   tva.TicketNumber_PK as ticketnumber_pk,
		   ta.donotmailflag,
		   ta.clientflag,
		   UPPER(firstname) AS firstname,
		   UPPER(lastname) AS lastname,
		   UPPER(address1) + '''' + ISNULL(ta.address2, '''') AS ADDRESS,
		   UPPER(ta.city) AS city,
		   s.state,
		   dbo.tblCourts.CourtName,
		   zipcode,
		   midnumber,
		   dp2 AS dptwo,
		   dpc,
		   violationdate,
		   violationstatusid,
		   LEFT(zipcode, 5) + RTRIM(LTRIM(midnumber)) AS zipmid,
		   ViolationDescription,
		   ISNULL(tva.fineamount, 0) AS FineAmount, 
		   COUNT(tva.ViolationNumber_PK) AS ViolCount,
		   SUM(ISNULL(tva.fineamount, 0)) AS Total_Fineamount
		   INTO #temp
	FROM   dbo.tblTicketsViolationsArchive tva
		   INNER JOIN dbo.tblTicketsArchive ta ON  tva.RecordID = ta.RecordID
		   INNER JOIN dbo.tblCourts ON  tva.courtlocation = dbo.tblCourts.Courtid
		   INNER JOIN tblstate S ON  ta.stateid_fk = S.stateID
		   INNER JOIN tblCourtViolationStatus tcvs ON tcvs.CourtViolationStatusID = tva.violationstatusid
	                
		   -- ONLY VERIFIED AND VALID ADDRESSES
	WHERE  Flag1 IN (''Y'', ''D'', ''S'') 

			   -- NOT MARKED AS STOP MAILING...
		   AND donotmailflag = 0 
	           
			   -- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
		   AND ISNULL(ta.ncoa48flag, 0) = 0
	       
		   -- show court violation category of type 2 eg: ARRAIGNMENT ,APPEARANCE etc
		   AND tcvs.CategoryID = 2
	       
		   -- First Name AND Last Name should not be empty . . 
			AND LEN(ISNULL(ta.LastName, '''')) > 0 AND LEN(ISNULL(ta.FirstName, '''')) > 0
		 
			-- Attorney should not be assigned. . . 
			AND LEN(ISNULL(tva.AttorneyName, '''')) = 0
	' 
	          
	-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY....
	IF(@crtid = @catnum)              
		SET @sqlquery=@sqlquery+' 
		AND 	tva.courtlocation In (SELECT courtid FROM tblcourts WHERE courtcategorynum = '+ CONVERT(VARCHAR(10),@crtid) + ' )'                  

	-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
	ELSE 
		SET @sqlquery =@sqlquery +' 
		AND 	tva.courtlocation = '+ CONVERT(VARCHAR(10),@crtid)
	                                  
	-- FOR THE SELECTED DATE RANGE ONLY.....
	-- Afaq 8168 08/19/2010 replace tva.DLQUpdateDate with tva.courtdate 
	                                 
	IF(@startListdate<>'')                                    
		SET @sqlquery=@sqlquery+'
		AND   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.CourtDate' ) 

	                                   
	-- LMS FILTERS SECTION:
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
	IF(@officernum<>'')                                    
		SET @sqlquery =@sqlquery + ' 
		AND 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
	              
	              
	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                    
	IF(@ticket_no<>'')                                    
		SET @sqlquery=@sqlquery+ ' 
		AND  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
	                         
	                                    
	-- ZIP CODE FILTER......
	-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
	IF(@zipcode<>'')                                                          
		SET @sqlquery=@sqlquery+ ' 
		AND	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'               

	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION
	IF(@violadesc<>'')              
		SET @sqlquery=@sqlquery+' 
		AND	('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'           

	-- FINE AMOUNT FILTER.....
	-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
	IF(@fineamount<>0 AND @fineamountOpr<>'not'  )              
		SET @sqlquery =@sqlquery+ '  
		AND tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                   

	-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
	IF(@fineamount<>0 AND @fineamountOpr = 'not'  )              
		SET @sqlquery =@sqlquery+ '  
		AND not tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                   

	-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
	SET @sqlquery =@sqlquery+ ' 
	group by ta.recordid, CONVERT(VARCHAR(10),tva.courtdate,101), tva.courtdate, tva.courtlocation, ta.Flag1, ta.officerNumber_Fk,
	tva.TicketNumber_PK, ta.donotmailflag, ta.clientflag, violationstatusid, upper(firstname) , upper(lastname) , upper(address1) + '''' + isnull(ta.address2,'''') ,
	upper(address1), isnull(ta.address2,''''), upper(ta.city),s.state,  tblCourts.CourtName, zipcode, midnumber, dp2,
	dpc, violationstatusid,	violationdate, left(zipcode,5) + rtrim(ltrim(midnumber)), ViolationDescription, FineAmount,tva.DLQUpdateDate,tva.TicketViolationDate  
	'              
	 
	-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....	                           
	SET @sqlquery=@sqlquery+' 

	SELECT distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate,OffenceDate, FirstName,LastName,address,                              
	city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
	courtid,zipmid,donotmailflag,clientflag, zipcode, courtdate, midnumber INTo #temp1  FROM #temp WHERE 1=1                                   
	'              

	SET @sqlquery=@sqlquery+' 
	-- no letter of other mailer type will be send in the next 5 working days FROM th previous sent date. 
	Delete FROM #temp1
	FROM #temp1 a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
	INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
	WHERE 	lettertype ='+CONVERT(VARCHAR(10),@lettertype)+' 
	or (
			l.courtcategory = 38
			AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
		)
			
			
	   
	   -- EXCLUDE CLIENT CASES
		delete FROM #temp1 WHERE recordid in (
		SELECT v.recordid FROM tblticketsviolations v inner join tbltickets t 
		on t.ticketid_pk = v.ticketid_pk AND t.activeflag = 1 
		inner join #temp1 a on a.recordid = v.recordid
		)

	'

	-- SINGLE VIOLATION FINE AMOUNT FILTER....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
	IF(@singleviolation<>0 AND @doubleviolation=0)              
		SET @sqlquery =@sqlquery+ '
		AND  '+ @singlevoilationOpr+ '  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND ViolCount=1) or violcount>3                                            
		'              
	              
	-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF(@doubleviolation<>0 AND @singleviolation=0 )              
		SET @sqlquery =@sqlquery + '
		AND'+ @doublevoilationOpr+ '   (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND ViolCount=2)  or violcount>3  
		'

	-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...              
	IF(@doubleviolation<>0 AND @singleviolation<>0)              
		SET @sqlquery =@sqlquery+ '
		AND'+ @singlevoilationOpr+ '  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND ViolCount=1)
		OR '+ @doublevoilationOpr+ '  (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND ViolCount=2) or violcount>3  '
	    
	    
	 -- hide the entire columm in mailer IF fine amount is zero agains any violation of a recored . . . 
	 SET @sqlquery2 =@sqlquery2 +  '
	 alter table #temp1 
	 ADD isfineamount BIT NOT NULL DEFAULT 1  
	 
	 SELECT recordid INTo #fine FROM #temp1   WHERE ISNULL(FineAmount,0) = 0  
	 
	 update t   
	 SET t.isfineAmount = 0  
	 FROM #temp1 t inner join #fine f on t.recordid = f.recordid   
	    
		'
	  
	-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
	-- CREATE LETTER HISTORY FOR EACH PERSON....         
	IF( @prINTtype  = 1)         
		BEGIN
			SET @sqlquery2 =@sqlquery2 +                                    
			'
			DECLARE @ListdateVal DateTime, @totalrecs INT, @count INT,                                
			@p_EachLetter MONEY, @recordid VARCHAR(50),
			@zipcode   VARCHAR(12),	@maxbatch INT  ,@lCourtId INT  ,
			@dptwo VARCHAR(10),@dpc VARCHAR(10)
			DECLARE @tempBatchIDs table (batchid INT)

			-- GETTING TOTAL LETTERS AND COSTING ......
			SELECT @totalrecs =count(*) FROM #temp1 WHERE listdate = @ListdateVal 
			SET @p_EachLetter=CONVERT(MONEY,0.45) 

			-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
			-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
			DECLARE ListdateCur Cursor for                                  
			SELECT distinct listdate  FROM #temp1                                
			open ListdateCur                                   
			Fetch Next FROM ListdateCur INTo @ListdateVal                                                      
			while (@@Fetch_Status=0)                              
				begin                                

					-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE...
					SELECT @totalrecs =count(distinct recordid) FROM #temp1 WHERE listdate = @ListdateVal                                 

					-- INSERTING RECORD IN BATCH TABLE......                               
					insert INTo tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) 
					values ('+CONVERT(VARCHAR(10),@empid)+' ,'+CONVERT(VARCHAR(10),@lettertype)+', @ListdateVal, '+CONVERT(VARCHAR(10),@catnum)+'  , 0, @totalrecs, @p_EachLetter)                                   

					-- GETTING BATCH ID OF THE INSERTED RECORD.....
					SELECT @maxbatch=MAX(BatchId) FROM tblBatchLetter  
					insert INTo @tempBatchIDs SELECT @maxbatch                                                    

					-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
					DECLARE RecordidCur Cursor for                               
					SELECT distinct recordid,zipcode, courtid, dptwo, dpc FROM #temp1 WHERE listdate = @ListdateVal                               
					open RecordidCur                                                         
					Fetch Next FROM  RecordidCur INTo @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
					while(@@Fetch_Status=0)                              
						begin                              

							-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
							insert INTo tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
							values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+CONVERT(VARCHAR(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
							Fetch Next FROM  RecordidCur INTo @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
						end                              
					close RecordidCur 
					deallocate RecordidCur                                                               
					Fetch Next FROM ListdateCur INTo @ListdateVal                              
				end                                            

			close ListdateCur  
			deallocate ListdateCur

			-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....		 
			SELECT distinct CONVERT(VARCHAR(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
			t.LastName,t.address,t.city, t.state,FineAmount,OffenceDate, violationdescription,CourtName, t.dpc,                              
			dptwo, TicketNumber_PK, t.zipcode, courtdate , '''+@user+''' as Abb, isfineamount, '''+@lettername +''' AS lettername,
			t.zipmid, ''Pearland'' AS CourtName, 0 AS promotemplate,  t.midnumber, t.midnumber AS midnum, t.courtid
			FROM #temp1 t , tblletternotes n, @tempBatchIDs tb
			WHERE t.recordid = n.recordid AND t.courtid = n.courtid AND n.batchid_fk = tb.batchid AND n.lettertype = '+CONVERT(VARCHAR(10),@lettertype)+' 
			order by T.recordid 
			-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........		
			DECLARE @lettercount INT, @subject VARCHAR(200), @body VARCHAR(400) , @sql VARCHAR(500)
			SELECT @lettercount = count(distinct n.noteid) FROM tblletternotes n, @tempBatchIDs b WHERE n.batchid_fk = b.batchid
			SELECT @subject = CONVERT(VARCHAR(20), @lettercount) +  '' LMS Pearland Arrignment Day Before Letters PrINTed''
			SELECT @body = CONVERT(VARCHAR(20), @lettercount) + '' LETTERS ('' + CONVERT(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
			EXEC usp_mailer_send_Email @subject, @body

			-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
			DECLARE @batchIDs_filname VARCHAR(500)
			SET @batchIDs_filname = ''''
			SELECT @batchIDs_filname = @batchIDs_filname + CONVERT(VARCHAR,batchid) + '',''  FROM @tempBatchIDs
			SELECT isnull(@batchIDs_filname,'''') as batchid

			'
		END
	ELSE IF( @prINTtype  = 0)         

		-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
		-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
		-- TO DISPLAY THE LETTER...
		BEGIN
			SET @sqlquery2 = @sqlquery2 + '
			SELECT distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
			LastName,address,city, state,FineAmount,OffenceDate, violationdescription,CourtName, dpc,                              
			dptwo, TicketNumber_PK, zipcode , courtdate , '''+@user+''' as Abb, isfineamount, '''+@lettername +''' AS lettername,
			zipmid, ''Pearland'' AS CourtName, 0 AS promotemplate,  midnumber, midnumber AS midnum, courtid 
			FROM #temp1 
			order by recordid 
			'
		END

	-- DROPPING TEMPORARY TABLES ....
	SET @sqlquery2 = @sqlquery2 + '

	drop table #temp 
	drop table #temp1'

	-- PRINT @sqlquery + @sqlquery2
	-- EXECUTING THE DYNAMIC SQL ....
	EXEC(@sqlquery + @sqlquery2)

END

GO
GRANT EXECUTE ON dbo.usp_mailer_Send_Pearland_DayBeforeArraignment_Letters TO dbr_webuser
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_NewViolation_bak]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_NewViolation_bak]
GO









create procedure USP_HTS_Insert_NewViolation_bak
 (        
 @TicketId_pk  int,        
 @TicketsViolationIDs  varchar(100),      
 @RefCaseNumber  varchar(20),        
 @SequenceNumber  int,        
 @CauseNumber varchar(20),  
 @ViolationNumber_pk int,        
 @CourtNumberAuto int,        
 @FineAmount  money,        
 @BondAmount  money,        
 @BondFlag  bit,        
 @PlanId   int,        
 @CaseStatusId  int,        
 @CourtDateAuto  datetime,        
 @CourtId  int,        
 @EmployeeId  int,      
 @UpdateMain bit
 )        
       
as        
       
insert into tblticketsviolations        
 (        
 ticketid_pk,        
 refcasenumber,        
 sequencenumber,  
 casenumassignedbycourt,        
 violationnumber_pk,        
 courtnumber,        
 fineamount,        
 bondamount,        
 underlyingbondflag,        
 planid,        
 courtviolationstatusid,        
 courtdate,        
 courtid,        
 courtviolationstatusidmain,      
 courtnumbermain,      
 courtdatemain,      
 courtviolationstatusidscan,      
 courtnumberscan,      
 courtdatescan,      
 vemployeeid        
 )        
values        
 (        
 @TicketId_pk,        
 @RefCaseNumber,        
 @SequenceNumber,  
 @CauseNumber,        
 @ViolationNumber_pk,        
 @CourtNumberAuto,        
 @FineAmount,        
 @BondAmount,        
 @BondFlag,        
 @PlanId ,        
 @CaseStatusId,        
 @CourtDateAuto,        
 @CourtId,       
 @CaseStatusId,      
 @CourtNumberAuto,      
 @CourtDateAuto,      
 @CaseStatusId,      
 @CourtNumberAuto,      
 @CourtDateAuto,      
 @EmployeeId        
 )      
      
    
-- RESETTING LOCK FLAG.....  
update tbltickets set lockflag = 0 where ticketid_pk  = @TicketID_PK  
  
--- Inserting new violation info in tblticketsnotes -------      
 insert into tblticketsnotes(ticketid,subject,employeeid)      
values (@TicketID_PK,'New Violation Added: Case # '+@RefCaseNumber+'-'+convert(varchar(5),@SequenceNumber) ,@EmployeeId)    
    
    
-- UPDATING OTHER SELECTED VIOLATIONS...............      
if len(@TicketsViolationIDs) > 0      
 begin      
  update tblticketsviolations            
  set  courtid = @CourtId,            
   courtdatemain = @CourtDateAuto,            
   CourtNumberMain = @CourtNumberAuto,            
   courtviolationstatusidmain = @CaseStatusId            
  where  ticketsviolationid in ( select * from dbo.Sap_String_Param(@TicketsViolationIDs)  )            
 end      
      
     
      
-- UPDATING TBLTICKETS IF 'UPDATE MAIN' IS SELECTED......      
if @UpdateMain = 1             
 begin            
  update tbltickets            
  set currentdateset = @CourtDateAuto,            
   currentcourtloc = @courtid,            
   currentcourtnum = @CourtNumberAuto,            
   datetypeflag = (            
   select  typeid             
   from  tbldatetype d, tblcourtviolationstatus c            
   where c.categoryid = d.typeid            
   and  c.courtviolationstatusid = @CaseStatusId            
   )            
  where  ticketid_pk = @TicketId_pk            
 end  
  







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


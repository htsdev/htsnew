SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_SAVE_CONSULTATION_COMMENTS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_SAVE_CONSULTATION_COMMENTS]
GO

CREATE PROCEDURE USP_HTS_SAVE_CONSULTATION_COMMENTS  
@TicketID int,  
@Comments varchar(4999),  
@EmpID int  
  
as  
  
declare @count int, @strabbr varchar(10), @strComments varchar(4999)  
  
SELECT @strabbr = Abbreviation FROM TBLUSERS where employeeid = @EmpID   
  
set @count = (select count(TicketID_FK) from tbl_hts_ConsultationComments where TicketID_FK=@TicketID)  
  
set @strComments = @Comments + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) +' - ' + @strabbr +  ')'  
  
if(@count>0)  
begin  
 update tbl_hts_ConsultationComments  
 set  
  CommentsDate = getdate(),  
  Comments = @strComments  
 WHERE  
  TicketID_FK = @TicketID  
end  
  
else  
begin  
  
INSERT INTO tbl_hts_ConsultationComments(TicketID_FK,CommentsDate, Comments)  
VALUES (@TicketID, getdate(), @strComments)  
  
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


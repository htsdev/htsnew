
  
   
/******   
  
Create by:  Zeeshan Ahmed  
Business Logic : this procedure is used to get the Transaction Detail within the specific date of range.  
   
  
List of Parameters :   
  
 @RecDate     : From Search Date                           
 @RecDateTo   : To Search Date                            
 @EmployeeID  : Employee ID                             
 @PaymentType : Payment Type                            
 @CourtID  : COurt Location      
 @firmId  : Firm ID  
  
List of Columns:   
     
      TotalRev    : Display the total revenue generated with respect to letter type.  
      TotalClients  : Display the total clients hire with respect to letter type.  
      CourtCategoryName  : Display Court Name  
      MailType   : Display Mailer Type  
*****/  
--Sabir Khan 6047 06/29/2009 re-design this procedure for to speed up financial report... 
--khalid 3309 3/11/08 to add firms compatablity  
--  [usp_Get_All_PaymentDetailByCriteriaRange] '7/30/09','7/30/09',3991,-1,0 
alter procedure [dbo].[usp_Get_All_PaymentDetailByCriteriaRange]      
						
                            
@RecDate DateTime,                            
@RecDateTo DateTime,                            
@EmployeeID int,                            
@PaymentType Int,                            
@CourtID int = 0,      
@firmId int = NULL,
@BranchID INT=1 -- Sabir Khan 10920 05/27/2013 branch id added.                            
                            
AS      
                      
--ozair 4132 05/27/2008 set the lenght to max from 4000 as dynamic sql is trimming/cutting.
declare @SQL  nvarchar(max),                  
--end ozair 4132
 @Parm nvarchar(200)                  
select @SQL = '', @parm = '@RecDate DateTime, @RecDateTo DateTime, @EmployeeID int, @PaymentType Int, @CourtID int,@firmId int, @BranchID int'                  
  
                  
select @SQL = @SQL + ' SELECT DISTINCT p.ChargeAmount AS ChargeAmount,
       p.EmployeeID,
       p.PaymentType,
       CONVERT(VARCHAR(12), p.RecDate, 101) AS RecDate,
       u.Lastname,
       CASE 
            WHEN (LEN(UPPER(t.Lastname) + '', '' + UPPER(t.Firstname)) > 17) THEN LEFT(UPPER(t.Lastname) + '', '' + UPPER(t.Firstname), 14) 
                 + ''...''
            ELSE UPPER(t.Lastname) + '', '' + UPPER(t.Firstname)  --Sabir khan 6467 10/14/2009 client name should be in upper case...
       END AS CustomerName,
       (
           SELECT ShortName
           FROM   TblCourts
           WHERE  CourtID = (
                      SELECT TOP 1 CourtID
                      FROM   tblTicketsViolations
                      WHERE  ticketid_pk = v.ticketid_pk
                      ORDER BY
                             courtdate DESC
                  )
       ) AS Court,
       pt.Description AS CCType,
       p.TicketID,
       CASE t.bondflag
            WHEN 1 THEN ''B''
            ELSE ''''
       END AS BondFlag,
       p.CardType,
       dbo.Fn_FormateTime(p.RecDate) AS RecTime,
        case when isnull(t.initialadjustment,0) > 0 then ''YES''  ELSE '''' END as Adj1,                  
		CASE WHEN ISNULL(t.adjustment,0) > 0 THEN ''YES'' ELSE '''' END as Adj2,
       p.RecDate AS sorttime,
       ''N/A'' AS courtname,
	   t.mailerid , 	   
	   	ISNULL(v.RecordID,0) as RecordID,
	   	IPAddress	-- sabir Khan 10920 05/27/2013 IP Address is included in the select
into #tempTransDetail               
 FROM dbo.tblCourts c      
 INNER JOIN   dbo.tblTicketsViolations v  ON  c.Courtid = v.CourtID                                 
 INNER JOIN    dbo.tblTickets t  ON  v.TicketID_PK = t.TicketID_PK                   
 and v.ticketsviolationid in(  
 select top 1  ticketsviolationid from tblticketsviolations         
    where ticketid_pk  = v.ticketid_pk and (courtid is not null and courtid <> 0)AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))                 
      )                   
 RIGHT OUTER JOIN                  
  dbo.tblPaymenttype pt                   
 RIGHT OUTER JOIN                  
  dbo.tblTicketsPayment p                   
 ON  pt.Paymenttype_PK = p.PaymentType                   
 ON  t.TicketID_PK = p.TicketID                   
 LEFT OUTER JOIN                  
  dbo.tblUsers u                   
 ON  p.EmployeeID = u.EmployeeID left outer join              
 tblletternotes ln on t.MailerID = ln.NoteID left outer join              
 tblletter tl on tl.LetterID_PK = ln.LetterType   left outer join          
 tblticketsarchive ta on ln.RecordID = ta.RecordID                
 where datediff(day, p.recdate, @recdate) <= 0 and datediff(day, p.recdate, @recdateto) >= 0
 and  p.paymentvoid = 0 AND (v.coveringFirmId = coalesce(@firmId  ,v.coveringFirmId))       
'                  
                  
If @EmployeeID <>  0                   
select @SQL = @SQL + '                         
 and p.employeeid = @employeeid                  
 '                  


-- tahir 4225 7/16/2008 
-- fixing bug related to selected court.
If @CourtID = 1                   
select @SQL = @SQL + '                         
 and c.courtcategorynum  =1                
'                 
                  
If @CourtID = 2                   
select @SQL = @SQL + '                         
 and c.courtcategorynum  = 2                
'                
              
If @CourtID = 100                   
select @SQL = @SQL + '                        
 and isnull(c.courtcategorynum,0) not in (1,2,8)
 '                  

If @CourtID > 3000                   
select @SQL = @SQL + '                        
 and c.courtid = @CourtId                  
 '                  

-- end 4225
                
-- IF 'Show All (Inc. Atty/Freind Credit)' IS SELECTED AS PAYMENT TYPE......                          
IF (@PaymentType= 1)  --Fahad 6054 07/30/2009 Partener Firm Credit-104 Added                           
select @SQL = @SQL + '                         
    AND p.PaymentType IN (1,2,3,4,5,6,8,7,9,11,104)                           
'                  
  --Fahad 6054 07/30/2009 Comment the following Check                          
--else IF (@PaymentType=200)                            
--select @SQL = @SQL + '                         
--   AND  p.PaymentType IN (1,2,4,5,6,8,7,11)                             
--'                  
                             
-- IF   'Show All (Exc. Atty/Freind Credit)' IS SELECTED AS PAYMENT TYPE....                          
else IF (@PaymentType= 200)                            
select @SQL = @SQL + '                         
   AND  p.PaymentType IN (1,2,3,4,5,6,8,7)                             
'                  
                         
else IF (@PaymentType=300)                            
select @SQL = @SQL + '                         
   And  p.PaymentType IN (5,6)                            
'                  
                             
else IF (@PaymentType=301)                            
select @SQL = @SQL + '                         
   And  p.PaymentType IN (5)                            
'                  
                           
ELSE                            
select @SQL = @SQL + ' And  p.PaymentType = @PaymentType                            
'                  
          
--ozair 4111 05/23/2008 removed date from selection and group by clause          
select @SQL = @SQL + '  AND p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID)) -- Sabir Khan 10920 05/27/2013 branch id check added.
                       order by p.recdate 

ALTER TABLE #tempTransDetail ADD MailDate varchar(10) NOT NULL DEFAULT ''1/1/1900'' ,mailtype VARCHAR(500) NOT NULL DEFAULT ''N/A'',
listdate varchar(10),CourtCategoryNum int 


--Sabir Khan 6047 06/29/2009 update mailer id...
UPDATE a
SET    MailDate = CONVERT(VARCHAR, ISNULL(ln.RecordLoadDate, ''1/1/1900''), 101),
       mailtype = CASE 
                       WHEN ISNULL(t.IsTrafficAlertSignup, 0) = 1 THEN 
                            ''Traffic Alert''
                       ELSE ISNULL(TL.SHORTNAME, ''N/A'') + (
                                CASE ISNULL(ta.NCOA18Flag, 0)
                                     WHEN 1 THEN ''-(18)''
                                     ELSE ''''
                                END
                            )
                  END
FROM   #tempTransDetail a LEFT OUTER JOIN tblletternotes ln ON a.mailerid = ln.NoteId
INNER JOIN tblletter tl ON tl.LetterID_PK  = ln.LetterType
INNER JOIN tbltickets t ON t.TicketID_PK = a.ticketid
LEFT OUTER JOIN tblticketsarchive ta ON ta.RecordID = a.recordid


--Sabir Khan 6047 06/29/2009 update list date
SELECT a.recordid, MIN(ta.listdate) listdate
INTO #listdate
FROM #tempTransDetail a INNER JOIN tblticketsarchive ta ON ta.RecordID  = a.recordid
GROUP  BY a.recordid ORDER BY a.recordid

UPDATE a
SET    a.listdate = convert(varchar(10),b.listdate,101) 
FROM   #tempTransDetail a INNER JOIN #listdate b ON a.recordid = b.recordid 


--Sabir Khan 6047 06/29/2009 update court category by letter batch...

UPDATE a 
SET a.courtcategorynum = ISNULL(b.courtid,0)
FROM #tempTransDetail a INNER JOIN tbltickets t ON t.TicketID_PK = a.ticketid
LEFT OUTER JOIN tblletternotes n ON n.noteid = t.MailerID
INNER JOIN tblbatchletter b ON b.BatchID = n.BatchId_Fk
WHERE a.courtcategorynum IS NULL

UPDATE a 
SET a.courtcategorynum = c.courtcategorynum
FROM #tempTransDetail a INNER JOIN tblticketsviolations v ON v.TicketID_PK = a.ticketid
LEFT OUTER JOIN tblcourts c ON c.courtid = v.courtid 
WHERE a.courtcategorynum IS NULL

--Sabir Khan 11560 New Promo Logic has been implemented...
Update a
set a.Adj1 = Case tn.PromoAttorney when ''%Kubosh%'' then ''PK'' when ''%Colderon%'' then ''PR'' when ''%Furentes%'' then ''PR'' when ''%Cantu%'' then ''PCA'' when ''%Citizen%'' then ''PWC'' when ''%Prince%'' then ''PP'' else ''POR'' end
from #tempTransDetail a inner join tbltickets t on t.ticketid_pk = a.Ticketid
inner join tblticketsviolations tv on tv.ticketid_pk = t.ticketid_pk
inner join tblLetterNotes tn on tn.recordid = tv.recordid and tn.LetterType in (9,12)
where isnull(tn.isPromo,0) = 1
and isnull(tn.PromoID,0) <> 0
and Len(isnull(tn.PromoAttorney,''''))>0


select * from #tempTransDetail  
						
'

--end ozair 4111        

--print @SQL                  
exec sp_executesql @SQL, @parm, @RecDate , @RecDateTo , @EmployeeID , @PaymentType , @CourtID,@firmId,@BranchID      
  
  
  
  
  

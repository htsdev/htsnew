﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get Auto Dialer call status for set/reminder call events. 
				
				
List of Parameters:
	@TicketsViolationIDs

List of Columns:	
	ADReminderCallStatus:
	ADSetCallStatus:
	
*******/

Create procedure dbo.usp_AD_Get_CallStatus

@TicketsViolationIDs varchar(max)

as

select autodialerremindercallstatus as ADReminderCallStatus, autodialersetcallstatus as ADSetCallStatus
from tblticketsviolations
where ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs)) 

go

grant exec on dbo.usp_AD_Get_CallStatus to dbr_webuser
go
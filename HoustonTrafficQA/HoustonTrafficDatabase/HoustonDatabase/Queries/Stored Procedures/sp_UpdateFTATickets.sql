USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateFTATickets]    Script Date: 04/27/2012 02:11:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Create by		: Sabir Khan
Created Date	: 17/03/2009
Task ID			: 4413

Business Logic : This procedure is used to insert new tickets and also update all records of Non Clients and Quote clients 
				 -- By Matching Cause Number 
				 -- By Matching Ticket Number
				 -- By Matching Ticket Number & Violation Number
				 -- By Matching Ticket Number & Sequence Number
		
******/
    
ALTER procedure [dbo].[sp_UpdateFTATickets]    
    
as    
    
set nocount on     
    
--------------------------------------------------------------------------------------------------    
-- DECLARATIONS....................    
--------------------------------------------------------------------------------------------------    
declare @idx int,    
 @rec int,    
 @linkid int,    
 @causenumber varchar(30),    
 @ticketnumber varchar(20),    
 @updatedate datetime,     
 @Recordid int,    
 @midnumber varchar(20),    
 @LoaderId tinyint,    
 @RecFound int,    
 @TicketFound int,    
 @CurrDate  DATETIME,
 @isNewTicket bit 
 
 SElect @LoaderId = 4, @RecFound = 0, @currdate = getdate()   
 
 
 --GET ALL RECORDS IN TEMP TABLe HAVING FTA IN CAUSE NUMBER...
 select linkid, causenumber, midnumber, UpdateDate, ticketnumber + isnull(sequencenumber,'') AS ticketnumber   
 INTO #temp from  tbllubbockfta    
-- where charindex('fta',causenumber,0) <> 0

--Sabir Khan 4413 05/03/2009 add column into temp table...
ALTER TABLE #temp 
	ADD RowId INT PRIMARY KEY identity(1,1),
		lookupid INT NOT NULL DEFAULT(0),
		ticketviolationid INT NOT NULL DEFAULT(0),
		isNewTicket bit not null default(0),
		lastrecordid INT,
		RecordID INT,
		IncorrectMid INT NOT NULL DEFAULT(0)
		
--update lookupid and recordid if cause number and mid number is exists in database...
UPDATE a SET  a.lookupid = v.rowid, a.recordid = v.recordid
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.CauseNumber = a.causenumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber = a.midnumber
WHERE ISNULL(a.lookupid,0) = 0
AND courtlocation IN (3001,3002,3003)

--update lookupid and recordid if Ticket number and mid number is exists in database...
UPDATE a SET  a.lookupid = v.rowid, a.recordid = v.recordid
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.TicketNumber_PK = a.ticketnumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber = a.midnumber
where ISNULL(a.lookupid,0) = 0
AND courtlocation IN (3001,3002,3003)

--update lookupid and recordid if ticket number + ViolationNumber_pk and mid number is exists in database...
UPDATE a SET a.lookupid = v.rowid, a.recordid = v.recordid
FROM #temp a INNER JOIN tblticketsviolationsarchive V ON a.ticketnumber =  v.TicketNumber_PK + CONVERT(VARCHAR, v.ViolationNumber_PK)
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber = a.midnumber
WHERE ISNULL(a.lookupid,0) = 0
AND courtlocation IN (3001,3002,3003)

-- Update IncorrectMid for all those records if cuase number is exists and mid number is different in database
UPDATE a SET  a.IncorrectMid = 1
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.CauseNumber = a.causenumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber <> a.midnumber
WHERE ISNULL(a.lookupid,0) = 0
AND courtlocation IN (3001,3002,3003)

-- Update IncorrectMid for all those records if Ticket number is exists and mid number is different in database
UPDATE a SET  a.IncorrectMid = 1
FROM  #temp a INNER JOIN dbo.tblticketsviolationsarchive v ON v.TicketNumber_PK = a.ticketnumber
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber <> a.midnumber
where ISNULL(a.lookupid,0) = 0
AND a.IncorrectMid = 0
AND courtlocation IN (3001,3002,3003)

-- Update IncorrectMid for all those records if ticket number + violation number is exists and mid number is different in database
UPDATE a SET  a.IncorrectMid = 1
FROM #temp a INNER JOIN tblticketsviolationsarchive V ON a.ticketnumber =  v.TicketNumber_PK + CONVERT(VARCHAR, v.ViolationNumber_PK)
inner join tblticketsarchive t on t.recordid = v.recordid AND t.MidNumber <> a.midnumber
WHERE ISNULL(a.lookupid,0) = 0
AND a.IncorrectMid = 0
AND courtlocation IN (3001,3002,3003)

SELECT * INTO #tempMid FROM #temp WHERE IncorrectMid = 1

UPDATE t SET t.IncorrectMid = 1
FROM #temp t INNER JOIN #tempMid tm ON tm.linkid = t.linkid
AND t.IncorrectMid = 0
DROP TABLE #tempMid

--insert all incorrect mid number records into a mid number discrepancy table.
INSERT INTO Mid_No_Discrepancy(linkid,CauseNumber,MidNumber,TicketNumber_Seq,UpdateDate)
SELECT linkid,CauseNumber,MidNumber,ticketnumber,UpdateDate FROM #temp WHERE IncorrectMid = 1


ALTER TABLE #temp ADD ROID INT NOT NULL DEFAULT(0)

SELECT linkid,causenumber,midnumber,updatedate,ticketnumber,
lookupid,ticketviolationid,isNewTicket,Recordid,IncorrectMid
INTO #temp2 FROM #temp WHERE IncorrectMid = 0

ALTER TABLE #temp2 ADD ROWID INT NOT NULL IDENTITY(1,1)

UPDATE t SET ROID = t1.ROWID 
FROM #temp t INNER JOIN #temp2 t1 
ON t.linkid = t1.linkid
AND t.causenumber = t1.causenumber
AND t.midnumber = t1.midnumber
AND t.updatedate = t1.updatedate
AND t.ticketnumber = t1.ticketnumber

DROP TABLE #temp2

--update isNewTicket column for new tickets...
UPDATE #temp 
SET isNewTicket = 1 
WHERE ISNULL(lookupid,0) = 0
AND CHARINDEX('fta',causenumber) > 0
AND IncorrectMid = 0

--update newly added column if cause number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.causenumber = v.casenumassignedbycourt
where V.courtid in (3001,3002,3003)    
AND ISNULL(a.ticketviolationid,0) = 0  

--update newly added column if cause number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.ticketnumber = v.RefCaseNumber
where V.courtid in (3001,3002,3003)    
AND ISNULL(a.ticketviolationid,0) = 0  

--update newly added column if cause number is exists in Quote client...
UPDATE a SET  a.ticketviolationid = v.TicketsViolationID
FROM  #temp a INNER JOIN tblticketsviolations v ON a.ticketnumber = v.RefCaseNumber + CONVERT(VARCHAR, v.SequenceNumber)
where V.courtid in (3001,3002,3003)    
AND ISNULL(a.ticketviolationid,0) = 0  
 
declare @reccount INT
DECLARE @addedrec int, @updatedrec int, @tempupdate INT, @ftalink BIT, @lastrecordid int
select @idx = 1, @recordid = 0, @RecordID = 0,  @reccount = count(rowid) from #temp WHERE IncorrectMid = 0 
select @addedrec = 0, @updatedrec = 0, @tempupdate = 0, @TicketFound = 0, @ftalink = 0, @lastrecordid  = 0

--Sabir Khan 4413 03/11/2009 Create temp table table for getting updated records rowid and violationstatusid...
DECLARE @NonClient table(RowId int);
SET @causenumber = NULL
While @idx <= @reccount
	BEGIN
		select  @causenumber = causenumber,
				@linkid = linkid,
				@updatedate = updatedate, 
				@midnumber = midnumber,
				@ticketnumber = ticketnumber,    				
				@RecordID = RecordID,
				@isNewTicket = isNewTicket,
				@lastrecordid = lastrecordid
		from #temp  where ROID = @idx 	     
		-- INSERTING NEW  FTA TICKETS.....
		IF @isNewTicket = 0	
		BEGIN	
			--------------------------------------------------------------------------------------------------    
			-- FIRST UPDATE THE CAUSE NUMBER IN NON CLIENTS BY MATCHING TICKET NUMBER    
			--------------------------------------------------------------------------------------------------    
			update v    
			set v.causenumber = c.causenumber    
			from  dbo.tblticketsviolationsarchive v    
			inner join    
			#temp c    
			on v.rowid = c.lookupid   
			where len(isnull(v.causenumber,'')) = 0 
			AND c.ROID = @idx  AND ISNULL(c.lookupid,0) > 0	
			--------------------------------------------------------------------------------------------------    
			-- NOW UPDATING LINKID, FTAISSUEDATE, BODNDATE, BONDFLAG, UPDATEDDATE, BONDAMOUNT .................    
			--------------------------------------------------------------------------------------------------    
			update v    
			set v.ftalinkid = l.linkid,    
			v.ftaissuedate = l.updatedate,    
			v.updateddate = l.updatedate,    
			v.bonddate = l.updatedate,    
			v.courtdate = case when charindex('fta',l.causenumber,0) > 0 then l.updatedate else v.courtdate end,    
			v.ticketviolationdate = case when charindex('fta',l.causenumber,0) > 0 then l.updatedate else v.ticketviolationdate end,    
			v.violationstatusid = 186,    
			v.UpdationLoaderId  = @LoaderId 
			OUTPUT INSERTED.rowid into @NonClient     
			from   dbo.tblticketsviolationsarchive v INNER JOIN  #temp l ON v.rowid = l.lookupid   
			where (datediff(DAY,isnull(v.updateddate,'01/01/1900'),l.updatedate) >= 0 or (v.violationstatusid = 146))
			AND ISNULL(l.lookupid,0) > 0
			AND l.ROID = @idx	
		END		
		select	@causenumber = null,
				@linkid = null,
				@updatedate = null, 
				@midnumber = null,
				@ticketnumber = null,    				
				@RecordID = null,
				@isNewTicket = null,
				@lastrecordid = null			
	SET @RecFound = 0   
    set @idx = @idx + 1 
	END

--------------------------------------------------------------------------------------------------------
-- UPDATE STATUS, COURTDATE, COURT NUMBER FOR EXISTING TICKETS IN QUOTE/CLIENT    
-- BY MATCHING CAUSE NUMBER 
--------------------------------------------------------------------------------------------------------
select @idx = 1, @recordid = 0, @RecordID = 0,  @reccount = count(rowid) from #temp 
select @addedrec = 0, @updatedrec = 0, @tempupdate = 0, @TicketFound = 0, @ftalink = 0, @lastrecordid  = 0
DECLARE @QuoteClient table(ViolationstatusId int);
SET @causenumber = NULL
While @idx <= @reccount
	BEGIN
		select  @causenumber = causenumber,
				@linkid = linkid,
				@updatedate = updatedate, 
				@midnumber = midnumber,
				@ticketnumber = ticketnumber,    				
				@RecordID = RecordID,
				@isNewTicket = isNewTicket,
				@lastrecordid = lastrecordid
		from #temp  where ROWID = @idx  	     
		-- INSERTING NEW  FTA TICKETS.....
		IF @isNewTicket = 0	
		BEGIN	
			-----------------------------------------------------------------------------------------------------------------------    
			-- UPDATE STATUS, COURTDATE, COURT NUMBER FOR EXISTING TICKETS IN QUOTE/CLIENT    
			-- BY MATCHING CAUSE NUMBER    
			-----------------------------------------------------------------------------------------------------------------------    
			update V    
			set courtviolationstatusid = 186,    
			 v.courtdate = case when charindex('fta',l.causenumber,0) <> 0 then l.updatedate else v.courtdate end,    
			 V.updateddate = l.updatedate,    
			v.ticketviolationdate = case when charindex('fta',l.causenumber,0)<> 0 then l.updatedate else v.ticketviolationdate end,
			--Sabir 4640 08/21/2008 set bondreminderdate to today's date +2 business days when verified status is  "A/W" or "Arraignment" otherwise nothing to do with Bondreminderdate
			V.BondReminderDate=case when V.CourtViolationStatusIDmain in (3,201) 
			then 
			convert(varchar(20), dbo.fn_getnextbusinessday(getdate(),2), 101)
			else 
			v.bondreminderdate 
			end     
			OUTPUT INSERTED.TicketsViolationID into @QuoteClient
			from    
			tblticketsviolations V INNER JOIN  #temp l   ON V.TicketsViolationID = l.ticketviolationid
			where (datediff(DAY,isnull(v.updateddate,'01/01/1900'), l.updatedate) >= 0 or (v.courtviolationstatusid = 146))    
			AND ISNULL(l.ticketviolationid,0) > 0
			AND l.ROWID = @idx 
		END
		select	@causenumber = null,
				@linkid = null,
				@updatedate = null, 
				@midnumber = null,
				@ticketnumber = null,    				
				@RecordID = null,
				@isNewTicket = null,
				@lastrecordid = null			
	SET @RecFound = 0   
    set @idx = @idx + 1 
END
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--Insert Max recordid into temp table by matching linkid and recordid ...
SELECT a.linkid, MAX(V.recordid) AS lastrecordid 
INTO #tmplink 
FROM #temp a INNER  JOIN tblticketsviolationsarchive v ON v.ftalinkid = a.linkid
INNER JOIN tblticketsarchive t ON t.recordid = v.recordid 
where v.courtlocation IN (3001,3002,3003)
GROUP BY a.linkid

--update lastrecordid from #tmplink table ...
UPDATE a SET a.lastrecordid = b.lastrecordid
FROM #temp a INNER JOIN #tmplink b ON a.linkid = b.linkid
WHERE ISNULL(a.lastrecordid,0) =0 
AND isnewticket = 1	
 	
select @idx = 1, @recordid = 0, @RecordID = 0,  @reccount = count(rowid) from #temp  WHERE IncorrectMid = 0 
select @addedrec = 0, @updatedrec = 0, @tempupdate = 0, @TicketFound = 0, @ftalink = 0, @lastrecordid  = 0
SET @RecFound = 0
While @idx <= @reccount
	BEGIN
		select  @causenumber = causenumber,
				@linkid = linkid,
				@updatedate = updatedate, 
				@midnumber = midnumber,
				@ticketnumber = ticketnumber,    				
				@RecordID = RecordID,
				@isNewTicket = isNewTicket,
				@lastrecordid = lastrecordid
		from #temp  where ROID = @idx
	     
		-- INSERTING NEW  FTA TICKETS.....
		IF @isNewTicket = 1 
		BEGIN

			-- FIRST INSERT RECORD IN TBLTICKETSARCHIVE........    
			-- BY GETTING CLIENT INFORMATION FROM TBLTICKETSARCHIVE USING RECORD ID OF ASSOCIATED CAUSE NUMBER.........    
			insert into  dbo.tblticketsArchive(TicketNumber, MidNumber, FirstName, LastName,     
			Initial, Address1, City, StateID_FK, 
			--PhoneNumber, 10196 Fahad 05/09/2012 Removed PhoneNumber from the Non-clinet Db to insert Or Update
			ZipCode, DOB, DLNumber,     
			ViolationDate,  OfficerName,Race,Gender,Height ,listdate,    
			DP2 ,DPC,CRRT,courtid,flag1,OfficerNumber_FK, recloaddate, insertionloaderid)    

			select top 1 @ticketnumber, t.midnumber, t.firstname, t.lastname, t.initial, t.address1, t.city, t.stateid_fk,    
			--t.phonenumber,10196 Fahad 05/09/2012 Removed PhoneNumber from the Non-clinet Db to insert Or Update 
			t.zipcode, t.dob, t.dlnumber, @updatedate,  t.officername, t.race, t.gender,    
			t.height, @updatedate, t.dp2, t.dpc, t.crrt, 3001, t.flag1, t.officernumber_fk, getdate(),  @LoaderId   
			from  tblticketsarchive t INNER JOIN tblticketsviolationsarchive v 
			ON t.RecordID = v.RecordID
			where t.recordid = @lastrecordid
			
			set @RecFound = scope_identity()
			

			-- IF CLIENT INFORMATION NOT FOUND BY ASSOCIATED CAUSE NUMBER THEN FIND BY MID NUMBER.......    
			IF isnull(@RecFound,0) > 0     
			begin    
			
				
				INSERT INTO  dbo.tblticketsViolationsArchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,    
				ViolationDescription,ViolationCode,violationstatusid,courtdate,courtnumber,courtlocation,    
				bondamount,ticketviolationdate,ticketofficernumber,RecordID,CauseNumber,    
				bonddate,updateddate,Ticketnumber_la, ftaissuedate, InsertionLoaderId, FTALinkId, updationloaderid )    

				SELECT @ticketnumber, 0, 200, 'FAILURE TO APPEAR (CUSTODY)', 'FTA', 186,@updatedate , 0,3001,    
				200, @updatedate, null, 0, @causenumber, @updatedate, @updatedate, @causenumber, @updatedate, @LoaderId , @linkid, @LoaderId
				
				SET @addedrec = @addedrec + 1 						
			end 
		END
		select	@causenumber = null,
				@linkid = null,
				@updatedate = null, 
				@midnumber = null,
				@ticketnumber = null,    				
				@RecordID = null,
				@isNewTicket = null,
				@lastrecordid = null
			
	SET @RecFound = 0   
    set @idx = @idx + 1 
END
  
DROP TABLE #Temp
DROP TABLE #tmplink
--DROP TABLE #tmpmid


--Sabir Khan 4413 03/11/2009 get total count of updated records...
SELECT @updatedrec = 
		ISNULL((select COUNT(Distinct rowid) from @NonClient ),0) 
		+ 
		ISNULL((select COUNT(Distinct ViolationstatusId) from @QuoteClient),0)
		
-----------------------------------------------------------------------------------------------------------------------    
-- UPDATE DO NOT MAIL FLAG FOR NEWLY INSERTED RECORDS..    
-- IF PERSON'S NAME & ADDRESS MARKED AS DO NOT MAIL...    
-----------------------------------------------------------------------------------------------------------------------    
EXEC USP_HTS_UPDATE_NOMAILFLAG @CurrDate, '3001,3002,3003,'  

-- SENDING UPLOADED NO OF RECORDS AND UPDATED NO OF RECORDS THROUGH EMAIL   
  
EXEC usp_sendemailafterloaderexecutes  @addedrec,@LoaderId,'houston','',@updatedrec
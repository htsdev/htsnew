USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_CheckAll_NOMAILFLAG_ChkByLetterID]    Script Date: 12/16/2011 17:55:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tahir Ahmed
-- Create date: 07/21/2008
-- Business Logic:	The stored procedure is used by HTP /Activies /Donot Mail Update application.
--				It is used to search record by mailer ID. User can mark the records appearing
--				in the results to remove the person from our mailing lists 
--
-- List Of Parameters:
--	@LetterID:		mailer ID to search for the person

-- List of output columns:
--	dbid:			Database ID related to the record
--	recordid:		unique identification of record in the above specified database
--	casetype:		description for the matter type (HTP, DTP, Civil, Criminal)
--	ticketnumber:	Ticket number associated with the case
--	firstname:		first name of the person
--	lastname:		last name of the person
--	address1:		mailing address of the person 
--	city:			city associated with the mailing address
--	state:			name of the state associated with the mailing address
--	zipcode:		zipcode associated with the mailing address
--	nomailflag		flag if already marked as do not mail flag
--  Insertdate:     date when case was marked for do not mail

-- Dependent Procedure : 
-- Procedure usp_htp_update_usps_donotmailflag from USPSTracking service is using this procedure.

--
--
-- =============================================
                
ALTER procedure [dbo].[usp_hts_CheckAll_NOMAILFLAG_ChkByLetterID] --25622      
                  
@LetterID int                    
                  
as                  

--Yasir Kamal 7218 01/13/2010 mailer structure changes                  
declare @DBiD int -- 1-TrafficTickets, 2-DallasTrafficTickets
declare @Recordid int 

select	@Recordid  = n.recordid,
		@dbid = CASE ISNULL(b.CourtID,0) WHEN 26 THEN 5 WHEN 24 THEN 6 ELSE ISNULL(tcc.LocationId_FK ,0) END
from tblletternotes n inner join tblbatchletter b 
on b.batchid = n.batchid_fk
INNER JOIN tblCourtCategories tcc ON tcc.CourtCategorynum = b.courtid
where n.noteid = @letterid

SELECT @dbid= ISNULL(@dbid,0) , @Recordid = ISNULL(@Recordid,0)


--Yasir Kamal 6144 07/14/2009 Insertdate column added.
--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
--Yasir Kamal 6170 08/13/2009 add DoNotMailUpdateDate column
declare @DataSet table(Sno int identity, dbid int,DoNotMailUpdateDate VARCHAR(10),recordid int, casetype varchar(50), FirstName varchar(20),lastname varchar(20),Address1 varchar(50),city varchar(20),state varchar(15),ZipCode varchar(12), nomailflag bit)               

-- IF LETTER IS RELATED TO TRAFFICTICKETS DATABASE......
if @dbid = 1
BEGIN
	--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
	insert into @dataset (dbid ,DoNotMailUpdateDate, recordid, casetype ,FirstName ,lastname ,Address1 ,city ,state ,ZipCode, nomailflag )     
	select distinct @dbid as dbid,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.recordid, 'Houston Traffic' as casetype, t.firstname, t.lastname, t.address1 + isnull(' '+t.address2,'') as address1, t.city, s.state, t.zipcode , isnull(t.donotmailflag,0)
	from tblticketsarchive t inner join tblstate s on t.stateid_fk = s.stateid 
	inner join tblticketsviolationsarchive v on v.recordid = t.recordid
	where t.recordid =  @recordid
end

-- IF LETTER IS RELATED TO DALLASTRAFFICTICKETS DATABASE......
if @dbid = 2
BEGIN
	--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
	--Sabir Khan 6232 07/23/2009 Select distinct records...
	insert into @dataset (dbid ,DoNotMailUpdateDate, recordid, casetype ,  FirstName ,lastname ,Address1 ,city ,state ,ZipCode, nomailflag )     
	select DISTINCT @dbid as dbid,ISNULL(CONVERT(VARCHAR(10),t.DoNotMailUpdateDate,101),'Not Set'), t.recordid, 'Dallas Traffic' as casetype, t.firstname, t.lastname, t.address1 + isnull(' '+t.address2,'') as address1, t.city, s.state, t.zipcode , isnull(t.donotmailflag,0)
	from dallastraffictickets.dbo.tblticketsarchive t inner join dallastraffictickets.dbo.tblstate s 
	on t.stateid_fk = s.stateid 
	inner join dallastraffictickets.dbo.tblticketsviolationsarchive v on v.recordid = t.recordid
	where t.recordid =  @recordid
end


-- IF LETTER IS RELATED TO CIVIL DATABASE......
-- tahir 4394 7/10/2008  added civil cases...
-- tahir 4418 07/26/2008 added civil database....
if @dbid = 5
BEGIN
	--Sabir Khan 6167 07/17/2009 remove ticketnumber field for removing duplication in clients...
	--Sabir Khan 6232 07/23/2009 Select distinct records...
	insert into @dataset (dbid ,DoNotMailUpdateDate ,recordid, casetype ,FirstName ,lastname ,Address1 ,city ,state ,ZipCode, nomailflag )     
	select DISTINCT @dbid as dbid,ISNULL(CONVERT(VARCHAR(10),ta.DoNotMailUpdateDate,101),'Not Set'), ta.childcustodypartyid, 'Family Law' as casetype, ta.FirstName, ta.LastName, ta.Address AS address1 , ta.City, st.State, case when len(ta.ZipCode)>5 then substring(ta.ZipCode,1,5) else ta.ZipCode end as ZipCode, isnull(donotmailflag,0) 
	from civil.dbo.childcustodyparties ta                      
	join                      
	civil.dbo.tblstate st                       
	on ta.StateID = st.StateID 
	inner join civil.dbo.childcustody v on v.childcustodyid = ta.childcustodyid
	where ta.childcustodypartyid = @recordid                    
end

--Sabir Khan 9921 12/08/2011 Assumed Name database has been added.

if @dbid = 6
BEGIN
insert into @dataset (dbid ,DoNotMailUpdateDate, recordid, casetype,FirstName,Address1,city ,state ,ZipCode, nomailflag  )     
select distinct @dbid as dbid,'1/1/1900', t.BusinessID AS recordid, 'N/A' as casetype,
case when len(isnull(t.BusinessName,''))>0 then t.BusinessName else v.ownername end AS firstname, 
case when len(isnull(t.[Address],''))>0 then t.[Address] else v.ownerADdress end AS address1,
case when len(isnull( t.City,''))>0 then  t.City else v.ownerCity end AS City,s.state, 
case when len(isnull( t.Zip,''))>0 then  t.Zip else v.ownerZip end AS Zip,
case when len(isnull( t.NoMailflag,''))>0 then  t.NoMailflag else v.NoMailflag end AS NoMailflag
FROM AssumedNames.dbo.Business t 
inner JOIN AssumedNames.dbo.BusinessOwner v on v.BusinessID = t.BusinessID
inner join tblstate s on V.[OwnerState] = s.[State]
where t.BusinessID = @recordid
end

-- out put the resutls..        
select * from @dataset   order by dbid     
         

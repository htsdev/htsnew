SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/******        
Created by:  Sabir Khan 5941 07/31/2009
Business Logic : This procedure is used all active or inactive counties  ... 
List of Parameters:         
    @isactive : boolean value.   
******/

CREATE PROCEDURE [dbo].[USP_HTP_GET_Counties]
	@isactive BIT = 1
AS
	SELECT ISNULL(CountyID,0) AS CountyID,
	       CountyName
	FROM   tbl_Mailer_County
	WHERE  IsActive = @isactive
GO
GRANT EXECUTE ON [dbo].[USP_HTP_GET_Counties] TO [dbr_webuser]


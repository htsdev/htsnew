/*
* Business Logic : This procedure returns all the avalable languages that exist in our system.
*/

create PROCEDURE [dbo].[USP_HTP_GET_LANGUAGES]
AS
	SELECT LanguageID, UPPER(LanguageName) as LanguageName
	FROM   [LANGUAGE] lng	
	order by LanguageName asc
GO

GRANT EXECUTE ON [dbo].[USP_HTP_GET_LANGUAGES] TO dbr_webuser
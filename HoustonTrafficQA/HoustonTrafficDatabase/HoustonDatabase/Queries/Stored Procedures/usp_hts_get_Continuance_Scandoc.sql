SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_Continuance_Scandoc]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_Continuance_Scandoc]
GO


--- [usp_hts_get_Continuance_Scandoc]  '0874829720'
  
CREATE procedure [dbo].[usp_hts_get_Continuance_Scandoc]  
  
@TicketID int  
  
as  
  
SELECT distinct sb.ScanBookID AS DOC_ID,                           
  dt.DocType,                           
  sb.UPDATEDATETIME,  '0' as doc_num,                               
  ResetDesc, ' '  AS RefCaseNumber,                           
  '1' AS RecordType,                          
  sb.doccount AS ImageCount,                                
  0 as did,                            
  isnull(sd.DOC_ID,0) as ocrid,                          
  sp.DocExtension as DocExtension,                        
  u.Abbreviation,              
  Events,
  sdt.subdoctype                             
 FROM         dbo.tblScanBook sb                         
 INNER JOIN                        
              dbo.tblDocType dt                         
 ON  sb.DocTypeID = dt.DocTypeID                         
 INNER JOIN                        
        dbo.tblScanPIC sp                         
ON  sb.ScanBookID = sp.ScanBookID                         
INNER JOIN                        
         dbo.tblUsers u                         
ON  sb.EmployeeID = u.EmployeeID                         
LEFT OUTER JOIN                        
        dbo.tblscandata sd                         
ON  sp.DOCID = sd.docnum AND sb.ScanBookID = sd.doc_id  
LEFT OUTER JOIN   
  dbo.tblSubDocType sdt  
ON sdt.subdoctypeid=sb.subdoctypeid                       
 where   sb.TicketID = @TicketID                     
and isnull(sb.isdeleted,0) = 0  and dt.doctypeid=2                   
order by sb.UPDATEDATETIME desc
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


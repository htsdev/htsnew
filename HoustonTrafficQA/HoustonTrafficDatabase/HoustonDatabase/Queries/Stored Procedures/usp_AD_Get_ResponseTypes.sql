﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get Auto Dialer Response Types 
				
List of Columns:	
	responseid:		
	responsetype:
	
*******/

Create Procedure dbo.usp_AD_Get_ResponseTypes

as

select 
	responseid,	
	responsetype 
from 
	autodialerresponse

go

grant exec on dbo.usp_AD_Get_ResponseTypes to dbr_webuser
go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_PAYMENT_CHECK_AMOUNT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_PAYMENT_CHECK_AMOUNT]
GO

 
CREATE PROCEDURE dbo.USP_HTS_PAYMENT_CHECK_AMOUNT      
@TicketID int,      
@Amount money      
as      
      
declare @sum money, @amt money, @total money, @count int      
      
set @count = (select count(chargeamount) from tblTicketsPayment where ticketid= @TicketID AND PaymentVoid = 0)    
    
if(@count = 0)    
 set @sum = 0    
else    
 set @sum = (select sum(chargeamount) from tblTicketsPayment where ticketid= @TicketID AND PaymentVoid = 0)    
    
set @amt =  (select calculatedtotalfee from tbltickets where ticketid_pk = @TicketID)      
    
    
set @total = @sum + @Amount      
    
if(@amt - @total < 0)      
 select 0 as ProcessPayment      
else      
 select 1 as ProcessPayment      
      
      
--print @sum      
      
--USP_HTS_PAYMENT_CHECK_AMOUNT 125290,76  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


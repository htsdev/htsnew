set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
Author: Muhammad Adil Aleem
Description: To get XML Schema of any SQL procedure
Parameters: 
	@ProcedureName : Source procedure name
	@Parameters : Source procedure's parameter values [Comma separated]
	@Subject : Title of XML report
	@ResultSet : Output parameter for generated XML schema
Type : Generic
Created date : Jul-1st-2008
*/
Create procedure [dbo].[USP_HTP_Generics_Get_XMLSet]
@ProcedureName varchar(150),
@Parameters varchar(500),
@Subject varchar(200),
@ResultSet varchar(max) output

as
begin
declare @execProc varchar(max)
declare @execResultSet varchar(max)
declare @ColumnHeader varchar(max)
declare @sqlString varchar(max)
declare @Session varchar(40)

Set @Session = NewID()

Set @Session = replace(@Session,'-','')

Set @execProc = @ProcedureName + ' ' + @Parameters

create table #resultSet_Outer(TempCol varchar(1))
create table #columnNames_Outer(column_name varchar(100), data_type varchar(20), character_maximum_length varchar(20), RowID int)

--////////////////////////// Getting Column Header /////////////////////////////////////////////////////

Set @execResultSet = '
SELECT * into #resultSet_Inner' + @Session + ' FROM OPENQUERY(LinkSrvData,''' + @execProc +''')
select column_name, data_type, character_maximum_length into #columnNames_Inner from tempdb.information_schema.columns where table_name like ''#resultSet_Inner' + @Session + '__%''
Alter Table #columnNames_Inner add RowID int identity(1,1)
truncate table #columnNames_Outer
insert into #columnNames_Outer select * from #columnNames_Inner
declare @ColumnCount int, @LoopCounter int
declare @sqlString varchar(max)
select @ColumnCount = count(column_name) from #columnNames_Inner
Set @LoopCounter = 1
set @sqlString = ''Alter table #resultSet_Outer  add ''
while @LoopCounter <= @ColumnCount
	begin
		select @sqlString = @sqlString + '', ['' + column_name + ''] '' + data_type + (case when isnull(character_maximum_length,'''') <> '''' then ''('' + convert(varchar(20),character_maximum_length) + '')'' else '''' end) from #columnNames_Inner where RowID = @LoopCounter
		Set @LoopCounter = @LoopCounter + 1
	end
set @sqlString = replace(@sqlString,''add ,'',''add '')
exec (@sqlString)
Alter Table #resultSet_Outer drop column TempCol
insert into #resultSet_Outer select * from #resultSet_Inner' + @Session + '
drop table #resultSet_Inner' + @Session + '
drop table #columnNames_Inner
'
Set @execResultSet = Replace(@execResultSet,'[[','[')
Set @execResultSet = Replace(@execResultSet,']]',']')

exec(@execResultSet)

set @ColumnHeader = '<style type="text/css">.clsLeftPaddingTable
			{
				PADDING-LEFT: 5px;
				FONT-SIZE: 8pt;
				COLOR: #123160;
				FONT-FAMILY: Tahoma;
				background-color:EFF4FB;
				border-collapse:collapse;
				border-width:1px;
			}
			
			</style>
			<H2 align="center"><font color="#3366cc"><STRONG>' + @Subject + '</strong></font></H2>
			<table border="1" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EFF4FB" class="clsLeftPaddingTable" BorderColor="black">
			<tr>'
--////////////////////////// Getting ResultSet /////////////////////////////////////////////////////

declare @ColumnCount int, @LoopCounter int
select @ColumnCount = count(column_name) from #columnNames_Outer
Set @LoopCounter = 1
while @LoopCounter <= @ColumnCount
begin
select @ColumnHeader = @ColumnHeader + '<td ><font color="#3366cc"><STRONG>' + column_name + '</strong></font></td>' from #columnNames_Outer where RowID = @LoopCounter
Set @LoopCounter = @LoopCounter + 1
end

create table #XML_Outer (XML_String varchar(max))
select @sqlString = 'declare @XMLStr varchar(max); Set @XMLStr = Cast ( (  select '
Set @LoopCounter = 1
while @LoopCounter <= @ColumnCount
begin
select @sqlString = @sqlString + 'td= (Case isnull([' + column_name + '],'''') when '''' then '' '' else [' + column_name + '] end) ,'''',' from #columnNames_Outer where RowID = @LoopCounter
Set @LoopCounter = @LoopCounter + 1
end

Set @sqlString = @sqlString + ' from #resultSet_Outer FOR XML PATH (''tr''), Type) as varchar(max) )+ ''</table>''; insert into #XML_Outer values(@XMLStr)'

Set @sqlString = Replace(@sqlString,', from',' from')
Set @sqlString = Replace(@sqlString,'[[','[')
Set @sqlString = Replace(@sqlString,']]',']')


exec(@sqlString)
-- /////////////////////////  ResultSet Returned  //////////////////////////////
Set @ResultSet = @ColumnHeader + (Select XML_String from #XML_Outer)

Drop Table #resultSet_Outer
Drop Table #columnNames_Outer
end

Go


grant exec on dbo.USP_HTP_Generics_Get_XMLSet to dbr_webuser

Go






SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_GetSignAll_ImageFileNames]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_GetSignAll_ImageFileNames]
GO



--[USP_HTS_ESignature_GetSignAll_ImageFileNames] 'BondDoc', 'Client', 'ENGLISH', 2, '129578'
CREATE procedure [dbo].[USP_HTS_ESignature_GetSignAll_ImageFileNames]
@DocumentName varchar(30),
@Entity varchar(20),
@ClientLanguage varchar(20),
@PageCount int,
@SessionID varchar(200),
@TicketID int
as
Declare @KeyWord as varchar(30)
Set @KeyWord = @DocumentName
If @KeyWord = 'BondDoc'
Begin
	Set @KeyWord = 'Bond'
End

select distinct 
(Convert(varchar,Locs.SelectionOrder) + 'RunTimeSign' + @DocumentName + '_Main' + 
Case When @PageCount > 1 Then replicate('0', 4 - Len(Imgs.PageNumber)) + Convert(varchar,Imgs.PageNumber) Else '' End +  
'Top' + Convert(varchar,Locs.Pnl_Top) + 'Left' + Convert(varchar,Locs.Pnl_Left) + '.GIF') as ImageFileName

from Tbl_HTS_ESignature_Location Locs
Right join Tbl_HTS_ESignature_Images Imgs
on Locs.ImageTrace = Imgs.ImageTrace
Where Locs.ImageTrace Like '%' + @KeyWord + '%' And Locs.ImageTrace Like '%' + @Entity + '%' And (Locs.Language = @ClientLanguage Or Locs.Language = '') And Imgs.SessionID = @SessionID And Imgs.TicketID = @TicketID
--delete from Tbl_HTS_ESignature_Images



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_PAYMENTINFO_GETBUTTONSTATUS_bak]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_PAYMENTINFO_GETBUTTONSTATUS_bak]
GO



create  procedure USP_PAYMENTINFO_GETBUTTONSTATUS_bak
@TicketIDList  int,
@courtid int
 
as 
--declare @TicketIDList  int
--declare @empid int  
--declare @courtid int
--set @TicketIDList = 4367
--set @empid = 3001
--set @courtID = 3005

if @courtid in (3001,3002,3003)
	begin
		SELECT     count(*) as CountViolations
		FROM         dbo.tblTickets INNER JOIN  
	                     dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN  
        	              dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID INNER JOIN  
	                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK  
		WHERE     
			(dbo.tblCourtViolationStatus.CategoryID in (4)) AND 
	                      (dbo.tblTickets.TicketID_PK = @ticketidlist)  
				and (dbo.tblTickets.Activeflag = 1) 
					AND (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)   
	 
	end
else 
	begin
		SELECT count(*) as CountViolations
			FROM dbo.tblTickets INNER JOIN  
	                      dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN  
	                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID INNER JOIN  
	                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK  
		WHERE 
			(dbo.tblCourtViolationStatus.CategoryID in (2,3,4,5)) 	     AND 
		                      (dbo.tblTickets.TicketID_PK = @ticketidlist) 
				and (dbo.tblTickets.Activeflag = 1) 
			AND (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)   

	end




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


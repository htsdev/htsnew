SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_Hts_UpdateNoTrialLetter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_Hts_UpdateNoTrialLetter]
GO



CREATE procedure Usp_Hts_UpdateNoTrialLetter    
    
@TicketID  int ,  
@EmpID int   
    
as    
    
    
update tbltickets set jurystatus=3 where ticketid_pk=@TicketID  
  
 insert into tblTicketsNotes (TicketID,Subject,EmployeeID)   
 values(@TicketID,'Removed from No Trial Letter',@EmpID)     







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** 
Alter by:  Noufil khan
Business Logic : this procedure is used to get Client Information when a single letter printin batch print

List of Parameters:	
	@TicketID : By ticketID we can seearch our desire result.

******/

--[dbo].[USP_HTP_SetCall_Letter] 145299
CREATE Procedure [dbo].[USP_HTP_SetCall_Letter]
(    
@TicketID int    
)    
as    
    
 select Distinct T.TicketID_PK ,  T.FirstName , T.LastName , T.Address1 ,T.City , T.Zip ,S1.State, TV.RefcaseNumber , T.languageSpeak,  TV.CourtDateMain , TV.CourtNumberMain , CVS.ShortDescription as Status ,
  C.Address + ', ' + C.City + ', '+S2.State +' ' + C.Zip as Location from tbltickets T   
join tblticketsviolations TV    
On T.TicketID_Pk = TV.Ticketid_pk     
join tblcourtviolationstatus CVS    
On CVS.courtviolationstatusid = TV.CourtViolationStatusidmain    
join tblcourts C    
on C.courtID = TV.CourtID    
join tblState S1     
on S1.Stateid = t.StateID_fk    
join tblState S2    
on  S2.Stateid = C.State    
where     
T.Ticketid_pk = @TicketID      

go

GRANT EXEC ON [USP_HTP_SetCall_Letter] to dbr_webuser
go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_call_back_report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_call_back_report]
GO









CREATE procedure USP_HTS_GET_call_back_report --'01/01/1900','01/01/1900','01/01/1900','01/01/1900','0','0','3'     
@stCdate varchar(12) ,         --Appearance SDate          
@edCdate varchar(12) ,                
@stDate varchar(12)  ,          --Contact SDate           
@edDate varchar(12) ,            
@casestatus varchar(2)= 0,        --pending               
@userID int = 0,          
@dateOrder int = 0              --on which date to "Search"          
            
as                           
---------------------------------------    
/*    
declare @stCdate as  varchar(12)    
declare @edCdate as  varchar(12)    
declare @stDate  as varchar(12)    
declare @edDate as  varchar(12)    
declare @casestatus as varchar(2)    
declare @userID as int    
declare @dateOrder as int     
    
set @stCdate='03/27/2006'    
set @edCdate='06/18/2008'    
set @stDate='03/27/2006'    
set @edDate='03/27/2006'    
set @casestatus='1'    
set @userID=0    
set @dateOrder=3    
*/               
declare @sqlquery varchar(3000)               
      
--newly added          
          
set @sqlquery ='SELECT distinct      
 T.TicketID_PK,      
 T.Firstname + '' '' + T.Lastname AS ClientName,       
 TTV.CourtID,      
 TTV.CourtDateMain AS appeardate,      
 DATEDIFF(day,TTV.CourtDateMain, GETDATE()) AS appeardiff,       
 T.ContactDate AS contactdate,       
 DATEDIFF(day, T.ContactDate, GETDATE()) AS contactdiff,      
  (''$''+convert(varchar(12),basefeecharge+initialadjustment+ContinuanceAmount)) as Quote,      
 ISNULL(T.CallStatusforQuote, 0) AS callstatus,       
 CONVERT(varchar(12), dbo.formatphonenumbers(ISNULL(T.Contact1, ''0000000000''))) AS contactnumber,       
 T.ContactType1 AS contacttype,       
 T.GeneralComments,       
 U.Abbreviation,       
 T.EmployeeID--,      
-- T.Bondrequiredflag        
            
  FROM         dbo.tblTickets T INNER JOIN      
                      dbo.tblUsers U ON T.EmployeeID = U.EmployeeID INNER JOIN      
                      dbo.tblTicketsViolations TTV ON T.TicketID_PK = TTV.TicketID_PK INNER JOIN      
                      dbo.tblCourtViolationStatus ON TTV.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID LEFT OUTER JOIN      
                      dbo.tblTicketsContact C ON T.TicketID_PK = C.TicketID_PK      
where       
 (TTV.CourtID <>0) and (T.Activeflag <> 1) and      
ttv.TicketsViolationID  = (      
    select top 1 b.TicketsViolationID from tblTicketsViolations  b       
    where  b.ticketid_pk = ttv.ticketid_pk       
    and  b.courtdatemain  = ( select min(courtdatemain) from tblticketsviolations a      
               where a.ticketid_pk = b.ticketid_pk      
               and   a.courtdatemain > = getdate() -convert ( varchar(12),getdate(),108)      
          )      
               ) '        
      
if @dateOrder = 0                 
 begin                
 set @sqlquery = @sqlquery + 'and datediff(day,TTV.CourtDateMain,getdate()) between -7 and -1 '                
 set @sqlquery = @sqlquery + 'and datediff(day,contactdate,getdate()) > 2 '                
 end                 
else if @dateOrder = 1  --Searching on Contact date              
 begin      
 set @edDate = convert(varchar(12),convert(datetime, @edDate,101)+1,101)               
 set @sqlquery = @sqlquery + 'and datediff(day,TTV.CourtDateMain,getdate()) between -7 and -1 '                 
 set @sqlquery = @sqlquery + 'and t.contactdate between '''+@stDate+''' and '''+@edDate+''' '                
 end                
else if @dateOrder = 2    --searching on appear date            
 begin                
 set @edCDate = convert(varchar(12),convert(datetime, @edCDate,101)+1,101)    
 set @sqlquery = @sqlquery + 'and TTV.CourtDateMain between '''+@stCDate+''' and '''+@edCDate+''''                
 set @sqlquery = @sqlquery + 'and datediff(day,contactdate,getdate()) > 2 '                
 end                
else if @dateOrder = 3       --searching on both dates         
 begin                
 set @edCDate = convert(varchar(12),convert(datetime, @edCDate,101)+1,101)    
 set @edDate = convert(varchar(12),convert(datetime, @edDate,101)+1,101)    
     
 set @sqlquery = @sqlquery + 'and TTV.CourtDateMain between '''+@stCDate+''' and '''+@edCDate+''' '                 
 set @sqlquery = @sqlquery + 'and t.contactdate between '''+@stDate+''' and '''+@edDate+''' '                
    
 end                 
if @userID <> 0                
 Begin                 
 set @sqlquery = @sqlquery + 'and T.EmployeeId = ' +  convert(varchar(20),@userID) + ' '                
 end                
      
set @sqlquery = @sqlquery + 'and callstatusforquote = ' +  @casestatus + ' '                         
              
set @sqlquery = @sqlquery + ' AND ( dbo.tblCourtViolationStatus.CategoryID in (2,5))'                         
      
if @casestatus = 0                 
 begin                
  set @sqlquery = @sqlquery + ' or datediff(day,T.Callbackdate,Getdate()) = 0 '                
 end                    
      
--print @sqlquery                
exec (@sqlquery)    





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


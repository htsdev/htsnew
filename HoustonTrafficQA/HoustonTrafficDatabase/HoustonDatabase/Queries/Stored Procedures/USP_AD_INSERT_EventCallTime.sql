﻿ set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/******************************************************  
	Created By	   : Farrukh Iftikhar
	Created Date   : 07/28/2011 
	Task Id		   : 9332
	Business Logic : This Procedure Inserts Event Call Time
	Parameter Used : 
						@eventtypeid   Event id
						@dialtime  Event Dial Time					

 ******************************************************/



CREATE PROCEDURE [dbo].[USP_AD_INSERT_EventCallTime]
@eventtypeid TINYINT,
@dialtime DATETIME
AS

INSERT INTO autodialerEventdialtime (EventTypeID,DialTime)
VALUES (@eventtypeid,@dialtime)





GO
GRANT EXECUTE ON [USP_AD_INSERT_EventCallTime] TO dbr_webuser




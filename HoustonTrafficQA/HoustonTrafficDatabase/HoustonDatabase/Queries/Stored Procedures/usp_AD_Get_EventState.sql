﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get the event state from settings table. 
				
List of Parameters:	
	@EventTypeID		
	
List of Columns:
	eventstate:		
*******/

CREATE procedure dbo.usp_AD_Get_EventState

@EventTypeID tinyint

as

select eventstate from autodialereventconfigsettings
where eventtypeid=@EventTypeID

go

grant exec on dbo.usp_AD_Get_EventState to dbr_webuser
go 
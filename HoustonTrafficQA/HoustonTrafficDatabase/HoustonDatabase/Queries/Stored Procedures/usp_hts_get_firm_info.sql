/*
// Waqas 5864 07/20/2009
Business Logic : This procedure is used to get Firm information

Input Parameter : 
@firmid

Output Parameter :
FirmName,
FirmAbbreviation,
FirmID,
Address,
Address2,      
City,
State,
Zip,
Phone,
Fax,
AttorneyName,
Firmsubtitle,
basefee,
daybeforefee
judgefee
cancoverby
cancoverfrom
sendtextmessage
textmessagenumber
SenderEmail
noticeperioddays
AttorenyFirstName, 
AttorneyLastName, 
AttorneyBarCardNumber, 
AttorneyEmailAddress
AttorneySignatureImage
*/

  

alter procedure [dbo].[usp_hts_get_firm_info]      
@firmid int      

as      


select FirmName,FirmAbbreviation,FirmID,Address,Address2,      
City,State,Zip,Phone,Fax,AttorneyName,Firmsubtitle,basefee,daybeforefee,      
judgefee ,isnull(cancoverby,0) as cancoverby , isnull(cancoverfrom,0) as  cancoverfrom ,
-- tahir 5084 10/06/2008 added send text message info..
--Sabir Khan 5298 12/03/2008 get Sender Email...
isnull(sendtextmessage,0) as sendtextmessage, isnull(textmessagenumber,'') as textmessagenumber,isnull(SenderEmail,'') as SenderEmail, 
isnull(noticeperioddays, 0) as noticeperioddays,
--Waqas 5864 06/30/2009 for alr process
--Yasir Kamal 7150 01/01/2010 get attorney signature image.
AttorenyFirstName, AttorneyLastName, AttorneyBarCardNumber, AttorneyEmailAddress,AttorneySignatureImage
from tblfirm      
where firmid = @firmid      
    
    
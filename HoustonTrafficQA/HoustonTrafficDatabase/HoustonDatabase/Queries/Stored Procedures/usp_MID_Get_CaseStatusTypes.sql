
/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by HTP /Activities /Traffic Alert application to get 
				case statuses in selection criteria list.
				
List of Parameters:
	None

List of Columns:
	Column1:	Case status id
	Column2:	description for case status 	
	
	
*******/


-- PROCEDURE USED TO POPULATE CASE STATUS DROP DOWN.......  
ALTER  procedure [dbo].[usp_MID_Get_CaseStatusTypes]  
as
select convert(varchar(10),typeid),  
 [description]  
from dbo.tbldatetype  
  
-- REMOVING GARBAGE DATA......  
where [description] not like '---%'  
and [description] not like 'arraignm%'  
and  typeid in (5)

-- TAHIR 4136 05/29/2008
-- ADDED FTA  CASES FOR CASE STATUS SELECTION CRITERIA
union
select convert(Varchar(10),courtviolationstatusid), description
from tblcourtviolationstatus
where courtviolationstatusid = 186



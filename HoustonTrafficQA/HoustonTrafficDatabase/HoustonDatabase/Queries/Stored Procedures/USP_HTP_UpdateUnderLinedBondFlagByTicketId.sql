﻿/****** Object:  StoredProcedure [dbo].[USP_HTP_UpdateUnderLinedBondFlagByTicketId]  ******/     
/*        
 Created By     : Fahad Muhammad Qureshi.
 Created Date   : 05/18/2009  
 TasK		    : 5807        
 Business Logic : This procedure Set the Bond Flag 
				
Parameter:       
  @ticketid_pk	: Ticket id      
  
*/ 

CREATE PROCEDURE [dbo].[USP_HTP_UpdateUnderLinedBondFlagByTicketId]
@TicketID INT
AS
UPDATE tblTicketsViolations
SET UnderlyingBondFlag = 1
	
WHERE   
     TicketID_PK=@TicketID  
GO
GRANT EXEC ON [dbo].[USP_HTP_UpdateUnderLinedBondFlagByTicketId] TO dbr_webuser
GO  
 
﻿
/******              
Created by:  Muhammad Nasir 6393 09/15/2009      
Business Logic : This procedure is used to get all HMC clients cases whose officer name is       
     missing and court date is with in a week and status is not disposed.      
           
List of Columns: ticketid_pk      
     [Cause Number]: Cause number assigned by court       
     [Last Name]:last name of client      
     [First Name]:first name of client      
     [Court Date]:court date of client      
         
******/      
       
ALTER PROCEDURE dbo.USP_HTP_Get_HMC_MissingOfficerName
AS
	DECLARE @TrialDate VARCHAR(50)      
	
	--Nasir 6971 11/13/2009 date with in a week
	SELECT @TrialDate = CONVERT(VARCHAR,DATEADD(DAY ,1, GETDATE()),101) + ' to '  + CONVERT(VARCHAR,DATEADD(DAY ,7, GETDATE()),101)     
	
	
	
	SELECT t.ticketid_pk
	      ,casenumassignedbycourt AS [Cause Number]
	      ,lastName AS [Last Name]
	      ,FirstName AS [First Name]
	      ,courtdatemain AS [Court Date]
	FROM   tbltickets t
	      ,tblticketsviolations tv
	WHERE  t.ticketid_pk = tv.ticketid_pk
			--Nasir 6971 11/13/2009 date with in a week
	       AND DATEDIFF(DAY ,DATEADD(DAY ,1, GETDATE()) ,tv.courtdatemain)< 7
	       AND DATEDIFF(DAY ,DATEADD(DAY ,1, GETDATE()) ,tv.courtdatemain)>= 0
	       AND (
	       			--Fahad 8401 10/14/2010 Get only those clients having UNKNOWN, UNK, -UNK, NA, N/A,Unkown, Unknow as officer last or first name.
	       			--Afaq 8608 12/28/2010 Get only those clients having UNKNOWN, UNK, -UNK, NA, N/A,Unkown, Unknow as officer last or first name.
	       			-- Babar Ahmad 9184 05/09/2011 Added four more keywords to firstname and lastname ('Unknwn', 'unknon', '??', '?')
	               ISNULL(tv.ticketofficernumber, 0) IN (select officernumber_pk from tblofficer where firstname in ('Unknown','N/A','UNK','NA','-UNK','Unkown', 'Unknow', 'Unknwn', 'unknon', '??', '?') or lastname in ('Unknown','N/A','UNK','NA','-UNK','Unkown', 'Unknow', 'Unknwn', 'unknon', '??', '?'))
	               OR tv.ticketofficernumber NOT IN (SELECT officernumber_pk FROM   tblofficer)	           
	           )
	       AND activeflag = 1
	       AND tv.courtid IN (3001 ,3002 ,3003)
	       AND tv.courtviolationstatusidmain NOT IN (SELECT 
	                                                        courtviolationstatusid
	                                                 FROM   
	                                                        tblcourtviolationstatus
	                                                 WHERE  categoryid = 50)         
	
	SELECT @TrialDate 
 

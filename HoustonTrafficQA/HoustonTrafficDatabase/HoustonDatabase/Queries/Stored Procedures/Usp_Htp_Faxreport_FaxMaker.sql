USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[Usp_Htp_Faxreport]    Script Date: 11/16/2011 19:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 7/20/2009 3:36:54 PM
 ************************************************************/

--exec Usp_Htp_Faxreport @SystemId=1,@statusid=-1,@fromdate=N'7/1/2008',@todate=N'8/11/2008'  
    
/****** Object:  StoredProcedure [dbo].[Usp_Htp_Faxreport]    Script Date: 05/23/2008 16:48:40 ******/    
/*      
Created By : Noufil Khan      
Business Logic : This Procedure return those client whose fax letter had been sent.    
Parametr:    
   @SystemId : For 1 Houston or 2 Dallas Traffic Program
   @statusid : fax status (-1 all,0 pending and decline,2 success,3 canceled)
   @fromdate : Date from which searching starts    
   @todate   : Date to which searching ends    
   @alldates :  If 1 then not apply date criteria   
    
column : ticketid,faxid,faxdate,faxstatus,firstname,lastname,causenumber,courtinfo,status,courtShortname,docpath    
    
*/    
  
    
  
    
CREATE PROCEDURE [dbo].[Usp_Htp_Faxreport_FaxMaker]
	@SystemId INT,
	@statusid INT,
	@fromdate DATETIME,
	@todate DATETIME,
	 --Waqas 5436 02/09/2009 For show all dates
	@alldates BIT = 0,
	@IsValidation BIT = 0
AS
	--declare @sqlquery as varchar(max)  
	DECLARE @dbName VARCHAR(50)   
	SELECT @dbName = DbName
	FROM   SYSTEM
	WHERE  SystemId = @SystemId 
	--set @dbName = @dbName + '.dbo.'  
	
	
	IF @dbName = 'TrafficTickets'
	    SELECT DISTINCT 
	           f.ticketid AS ticketid,
	           f.faxletterid AS faxid,
	           dbo.fn_DateFormat(
	               ISNULL(f.faxdate, CONVERT(DATETIME, '1/1/1900')),
	               30,
	               '/',
	               ':',
	               1
	           ) AS faxdate,
	           
	           --Nasir 6013 07/10/2009 replace decline to f.status 
	           CASE f.status
	                WHEN 'SUCCESS' THEN 'Confirmed'
	                WHEN 'PENDING' THEN 'Queued'
	                ELSE f.status
	           END AS faxstatus,
	           t.firstname AS fname,
	           t.lastname AS lname,
	           tv.casenumassignedbycourt AS causenumber,
	           dbo.fn_DateFormat(
	               ISNULL(tv.CourtDateMain, CONVERT(DATETIME, '1/1/1900')),
	               30,
	               '/',
	               ':',
	               1
	           ) + ' #' + tv.courtnumber AS courtinfo,
	           cvs.shortdescription AS STATUS,
	           c.ShortName AS CRT,
	           f.docpath AS docpath,
	           f.ResentCount,	           
	           CASE 
	           WHEN CONVERT(VARCHAR,ISNULL(f.LastResentFaxdate,'01/01/1900'),101)='01/01/1900' THEN '' ELSE 
	           dbo.fn_DateFormat(
	               ISNULL(f.LastResentFaxdate,  ''),
	               30,
	               '/',
	               ':',
	               1
	           ) END AS LastResentFaxdate
	           
	           
	    FROM   faxletter f
	           LEFT OUTER JOIN TrafficTickets.dbo.tbltickets t
	                ON  t.ticketid_pk = f.ticketid
	           LEFT OUTER JOIN TrafficTickets.dbo.tblticketsviolations tv
	                ON  t.ticketid_pk = tv.ticketid_pk
	           LEFT OUTER JOIN TrafficTickets.dbo.tblCourtViolationStatus cvs
	                ON  tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
	           LEFT OUTER JOIN TrafficTickets.dbo.tblcourts c
	                ON  tv.CourtID = c.Courtid
	    WHERE  --((@status=1 and f.status in ('PENDING','DECLINE')) or (@status=0 and f.status= (case @statusid when 0 then 'PENDING' when 1 then 'SUCCESS' when 2 then 'DECLINE' else f.status  end  )))
	           --and ((@status=1 and datediff(day,f.faxdate,getdate()) = 0) or (@status=0 and (f.faxdate between @fromdate + ' 00:00:00'  and @todate+ ' 23:59:59')))
	           --Waqas 5436 02/09/2009																									
	           (
	               (
	                   --Nasir 6013 07/20/2009 if not call from validation email
	                   (@IsValidation = 0)
	                   AND (
	                           @statusid = -1
	                           OR (@statusid = 0 AND f.status IN ('pending', 'DECLINE'))
	                           OR (@statusid = 1 AND f.status = 'SUCCESS')
	                              --Nasir 6013 07/01/2009 check for canceled
	                           OR (@statusid = 2 AND f.[status] = 'Cancelled')
	                       )
	               )
	               OR (
	                      --Nasir 6013 07/20/2009 if call from validation email
	                      @IsValidation = 1
	                      AND f.[status] NOT IN ('Cancelled', 'SUCCESS')
	                  )
	           )
	           AND (
	                   @alldates = 1
	                   OR (
	                          DATEDIFF(DAY, f.faxdate, @fromdate) <= 0
	                          AND DATEDIFF(DAY, f.faxdate, @todate) >= 0
	                      )
	           )
	           --Nasir 6013 07/20/2009 check App category and waiting status
	           AND (tv.CourtViolationStatusIDmain=104 OR cvs.CategoryID =2)	           
	           AND tv.CourtID NOT IN (3001, 3002, 3003)
	    ORDER BY
	           f.ticketid
	ELSE 
	IF @dbName = 'DallasTrafficTickets'
	    SELECT DISTINCT 
	           f.ticketid AS ticketid,
	           f.faxletterid AS faxid,
	           dbo.fn_DateFormat(
	               ISNULL(f.faxdate, CONVERT(DATETIME, '1/1/1900')),
	               30,
	               '/',
	               ':',
	               1
	           ) AS faxdate,
	           CASE f.status
	                WHEN 'SUCCESS' THEN 'Confirmed'
	                WHEN 'PENDING' THEN 'Queued'
	                ELSE 'Declined'
	           END AS faxstatus,
	           t.firstname AS fname,
	           t.lastname AS lname,
	           tv.casenumassignedbycourt AS causenumber,
	           dbo.fn_DateFormat(
	               ISNULL(tv.CourtDateMain, CONVERT(DATETIME, '1/1/1900')),
	               30,
	               '/',
	               ':',
	               1
	           ) + ' ' + tv.courtnumber AS courtinfo,
	           cvs.shortdescription AS STATUS,
	           c.ShortName AS CRT,
	           f.docpath AS docpath
	    FROM   faxletter f
	           LEFT OUTER JOIN DallasTrafficTickets.dbo.tbltickets t
	                ON  t.ticketid_pk = f.ticketid
	           LEFT OUTER JOIN DallasTrafficTickets.dbo.tblticketsviolations tv
	                ON  t.ticketid_pk = tv.ticketid_pk
	           LEFT OUTER JOIN DallasTrafficTickets.dbo.tblCourtViolationStatus 
	                cvs
	                ON  tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
	           LEFT OUTER JOIN DallasTrafficTickets.dbo.tblcourts c
	                ON  tv.CourtID = c.Courtid
	    WHERE  --((@status=-1 and f.status in ('PENDING','DECLINE')) or (@status=0 and f.status= (case @statusid when 0 then 'PENDING' when 1 then 'SUCCESS' when 2 then 'DECLINE' else f.status  end  )))
	           --and ((@status=1 and datediff(day,f.faxdate,getdate()) = 0) or (@status=0 and (f.faxdate between @fromdate + ' 00:00:00' and @todate+ ' 23:59:59')))
	           --Waqas 5436 02/09/2009
	           (
	               @statusid = -1
	               OR (@statusid = 0 AND f.status IN ('pending', 'DECLINE'))
	               OR (@statusid = 1 AND f.status = 'SUCCESS')
	           )
	           AND (
	                   @alldates = 1
	                   OR (
	                          DATEDIFF(DAY, f.faxdate, @fromdate) <= 0
	                          AND DATEDIFF(DAY, f.faxdate, @todate) >= 0
	                      )
	               )
	           AND tv.CourtViolationStatusIDMain IN (104, 3)
	           AND tv.CourtID NOT IN (3001, 3002, 3003)  
  
go 

GRANT EXECUTE ON [dbo].[Usp_Htp_Faxreport_FaxMaker] TO dbr_webuser
GO

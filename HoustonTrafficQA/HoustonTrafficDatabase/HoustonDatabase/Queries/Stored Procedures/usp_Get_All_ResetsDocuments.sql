SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_ResetsDocuments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_ResetsDocuments]
GO















CREATE  PROCEDURE [dbo].[usp_Get_All_ResetsDocuments] 

@TicketID varchar(30)
--@DocType varchar(50)

AS
SELECT     dbo.tblTicketsViolations.ScanDocID AS DOC_ID, dbo.tblTicketsViolations.ScanDocNum AS DOC_Num, 'Resets' AS DOCREF, 
                      dbo.tblTicketsViolations.ScanUpdateDate AS UPDATEDATETIME, LTRIM(dbo.tblTicketsViolations.RefCaseNumber)
                      + ' - ' + LTRIM(STR(dbo.tblTicketsViolations.SequenceNumber)) + '  - ' + dbo.tblCourtViolationStatus.ShortDescription + '  - ' + CONVERT(varchar(20), 
                      dbo.tblTicketsViolations.CourtDate, 101) + ' ' +  SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 1, 5) 
                      + ' ' + SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 13, 2) 
                      + '  - #' + dbo.tblTicketsViolations.CourtNumber + '  - ' + ISNULL(dbo.tblViolations.Description, '') AS ResetDesc
FROM         dbo.tblTicketsViolations LEFT OUTER JOIN
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
WHERE     (dbo.tblTicketsViolations.ScanDocID IS NOT NULL) And ticketid_pk = @ticketid
Order By dbo.tblTicketsViolations.RefCaseNumber






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/***********
Business Logic: This stored procedure is used to get Letter count for Cass Report.

*/
-- USP_Mailer_Get_ZipLetterCount '10/01/2009,','11,','38,'
ALTER procedure [dbo].[USP_Mailer_Get_ZipLetterCount] 
--declare 
@printdates varchar(8000),
@court  varchar(2000),
@letterid  varchar(2000)

as



--select @printdates = '5/15/2007,', @court = '3,4,8,9,10,', @letterid = '22,24,27,28,'

declare @tblCourt table(id int IDENTITY PRIMARY KEY,courtid int)
insert into @tblcourt select * from dbo.sap_string_param(@court) 

declare @tblletter  table(id int IDENTITY PRIMARY KEY,letterid int)
insert into @tblletter select * from dbo.sap_string_param(@letterid) 

declare @tbldates  table(id int IDENTITY PRIMARY KEY,printdate datetime)
insert into @tbldates select * from dbo.sap_dates(@printdates) 


declare	@temp table(
	shortname varchar(50),
	courtid int,
	lettername varchar(50),
	zipcode varchar(50),
	noteid int,
	printdate datetime
	)


insert into @temp --(shortname, courtid, lettername, zipcode, noteid)

select distinct
	
	case 
	when 	c.courtcategorynum = 2 then
		ct.courtName
	else
		c.courtcategoryname
	end,
	bl.courtid,
	l.lettername,
	n.zipcode,
	n.noteid,
	convert(varchar(10), n.recordloaddate, 101)

from	tblcourtcategories c, tblletter l, tblletternotes n , tblbatchletter bl,tblcourts ct,  @tblcourt a, @tblletter b, @tbldates d
where	c.courtcategorynum = bl.courtid
and 	
case 
when c.courtcategorynum = 2 then 
n.courtid
else
bl.courtid 
end = a.courtid
and 	n.batchid_fk = bl.batchid
and	l.letterid_pk = n.lettertype
and	n.lettertype = b.letterid

and n.courtid = ct.courtid
and 	datediff(Day, n.recordloaddate, d.printdate) = 0 
and 	a.id=b.id
and 	a.id=d.id


--Yasir Kamal 6741 10/14/2009 dallas cass report count bug fixed.  
-- GET RECORDS FROM DALLAS TRAFFIC TICKETS DATABASE....
UNION

select distinct
	
	case 
	when 	c.courtcategorynum = 2 then
		ct.courtName
	else
		c.courtcategoryname
	end,
	bl.courtid,
	l.lettername,
	n.zipcode,
	n.noteid,
	convert(varchar(10), n.recordloaddate, 101)

from	tblcourtcategories c, tblletter l, tblletternotes n , tblbatchletter bl, DallasTrafficTickets.dbo.tblcourts ct,  @tblcourt a, @tblletter b, @tbldates d
where	c.courtcategorynum = bl.courtid
and 	
case 
when c.courtcategorynum = 2 then 
n.courtid
else
bl.courtid 
end = a.courtid
and 	n.batchid_fk = bl.batchid
and	l.letterid_pk = n.lettertype
and	n.lettertype = b.letterid

and n.courtid = ct.courtid
and 	datediff(Day, n.recordloaddate, d.printdate) = 0 
and 	a.id=b.id
and 	a.id=d.id


select 	shortname + '-' + lettername as LetterType, printdate,
	left(zipcode,3) as zip,
	count(noteid) as lettercount

from @temp

group by 
	shortname + '-' + lettername, printdate,
	left(zipcode,3) 
order by
	[printdate],[lettertype], [zip]




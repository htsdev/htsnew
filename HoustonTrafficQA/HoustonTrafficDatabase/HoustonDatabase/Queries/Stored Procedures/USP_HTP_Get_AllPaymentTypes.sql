/*  
Created By : Abbas Qamar
Task Id : 9957 
Creation Date : 03-01-2012
Business Logic : This procedure returns payment types


*/

CREATE PROCEDURE [dbo].[USP_HTP_Get_AllPaymentTypes]  
AS  
SELECT * FROM  tblPaymenttype tp WHERE tp.Paymenttype_PK NOT IN (0,10)


GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_AllPaymentTypes] TO dbr_webuser
GO 
	



﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/10/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to get list of all docuemnt sub-types.
* List of Parameter : @DocumentTypeId
* Column Return :     
******/
CREATE PROCEDURE dbo.USP_HTP_GetDocumentSubType
@DocumentTypeId INT
AS
SELECT Id,DocumentTypeId,Description FROM DocumentSubType WHERE DocumentTypeId=@DocumentTypeId
	
GO
GRANT EXECUTE ON dbo.USP_HTP_GetDocumentSubType TO dbr_webuser
GO



/************************************************************
 * Created By : Afaq Ahmed
 * Create Date : 7/6/2010 11:34:24 AM
 * Business Logic :Update the isEmailVerified field when verify button on the contact page will click.Update to 0 if email is verified else 1.
 * Parameter :@ticketId = Primary Key TIcketid_pk of client.,@isEmailVerified = email is verified or not; 1 if verified, 0 if not
 * Column Return : 
 ************************************************************/

CREATE PROCEDURE [dbo].[USP_HTP_IsEmailVerified]
-- Add the parameters for the stored procedure here
	@ticketId
AS
	INT,
	@isEmailVerified AS BIT
	
	
	AS
BEGIN
    UPDATE tblTickets
    SET    isEmailVerified = @isEmailVerified
    WHERE  TicketID_PK = @ticketId
END
GO

GRANT EXECUTE ON [dbo].USP_HTP_IsEmailVerified TO dbr_webuser

﻿ 
 
 /*
Create By: Muhammad Nasir
Date:	  09/11/2008

Business Logic:
	The stored procedure is used to get Case info.

List of Input Parameters:
	@TicketId:	Case identification for the client.
	@LastGeneralCommentsDate: Last General Comments Date
	
List of Output Columns:
	true or false
*/
 
alter PROCEDURE dbo.USP_HTP_IsLastGeneralCommentsDateUpdated 
@TicketID_PK AS INT,
@LastGeneralCommentsDate DATETIME
 
 AS
 
	    DECLARE @LastGerenalcommentsUpdatedate DATETIME	   
	    SELECT @LastGerenalcommentsUpdatedate=LastGerenalcommentsUpdatedate FROM tblTickets WHERE TicketID_PK=@TicketID_PK	    
	    
	    
	    --Waqas 6895 11/02/2009 for new case   
		if(@TicketID_PK = 0 and @LastGeneralCommentsDate = '')
		begin
				SELECT @LastGerenalcommentsUpdatedate = '01/01/1900' 
				SELECT @LastGeneralCommentsDate= '01/01/1900' 
		end
 
	    IF DATEDIFF(ss,@LastGerenalcommentsUpdatedate,@LastGeneralCommentsDate) = 0
			SELECT 1
		ELSE
			SELECT 0
			
			
		
			
			
			
			
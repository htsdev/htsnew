﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 12:44:00 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 
      
******/
CREATE PROCEDURE dbo.USP_HTP_GetDoctorByCriteria 
@Criteria VARCHAR(MAX) = ''
AS      
      
BEGIN
    SELECT DISTINCT TOP 15 d.LastName AS Doctor
    FROM   Doctor d
    WHERE  d.LastName LIKE @criteria + '%'
           AND d.LastName IS NOT NULL
           AND LEN(d.LastName) > 0
    ORDER BY
           d.LastName
END 
GO
GRANT EXECUTE ON dbo.USP_HTP_GetDoctorByCriteria  TO dbr_webuser
GO 	 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_LETTERS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_LETTERS]
GO


CREATE PROCEDURE [USP_HTS_GET_LETTERS] AS    
   
select  l.letterid_pk as LetterID,l.LetterName, count(distinct ticketid_fk) as BatchCount,sum(isprinted) as printed    
from tblhtsletters l     
LEFT OUTER  join    
 tblhtsbatchprintletter b    
on  l.letterid_pk = b.letterid_fk    
where  --b.ISPrinted = 0 
deleteflag<>1   
group by    
 l.letterid_pk,l.LetterName    



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


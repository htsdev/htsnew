/****** 
Altered by:		Sabir Khan
Business Logic:	This procedure is used to get limited access for dallaslms user.

List of parameters: @EmpID: Employee ID of currently logged in users...
*******/

-- usp_Mailer_Reports_GetShortCourtCategories 1,1

alter   PROCEDURE  [dbo].[usp_Mailer_Reports_GetShortCourtCategories]  
	(
	--Sabir Khan 7048 11/24/2009 parameter for dallaslms user added...
	  @UserLocation int  =1, 
--Yasir Kamal 7218 01/19/2010 lms structure changes.
--	  @EmpID INT = 0,
	  @AccessType INT = 0
	)    
      
AS      
     
declare @temp table
(
	id int IDENTITY(1,1) NOT NULL,
	countyid int,
	countyname varchar(100) NULL,
	url varchar(50) NULL,
	courtcategorynum int NULL,
	courtcategoryname varchar(50) NULL
)

declare @temp1 table
(	
	countyid int,
	countyname varchar(100) NULL,
	url varchar(50) NULL,
	courtcategorynum int NULL,
	courtcategoryname varchar(50) NULL
) 

Select 	url,
	courtcategorynum,  
	CourtCategoryName ,
	userlocation,
	countyid
into #temp
From 	tblmailerreports      
WHERE  isActive = 1
AND countyid IN (91,92)
AND ( @AccessType = 2 OR accesstype = @AccessType )
AND	( @UserLocation in (0, 1) OR userlocation = @UserLocation )

INSERT INTO #temp
SELECT 'lettersoptions_newreports.aspx', courtcategorynum,  courtcategoryname, locationid_fk, countyid_fk 
FROM tblcourtcategories 
WHERE isactive= 1 AND (@UserLocation in (0, 1) OR locationid_Fk = @UserLocation) 
AND @AccessType = 2


insert into @temp(countyid, countyname,url,courtcategorynum,courtcategoryname)
select a.countyid, c.countyname, a.url, a.courtcategorynum, a.courtcategoryname 
from #temp  a left outer join dbo.tbl_mailer_county c
on c.countyid = a.countyid

update @temp set countyname = 'Tools' where countyid = 91
update @temp set countyname = 'Misc Reports' where countyid = 92

insert into @temp1 (countyid, countyname, courtcategorynum)
select distinct countyid, countyname, 0 from @temp

insert into @temp1 (countyid, countyname,url, courtcategorynum,  courtcategoryname)
select countyid, countyname, url, courtcategorynum, courtcategoryname
from @Temp

update @temp1 set url = '', courtcategoryname = ''
where courtcategorynum = 0

update @temp1 set countyname = ''
where courtcategorynum <> 0

select countyname, url, courtcategorynum, courtcategoryname
from @temp1 order by countyid, courtcategoryname

drop table #temp





        
  
      
--****************************************************************************
--Business Logic : Use for implement print history for Arraignment settings 
--and Bond settings under 'HTP/Reports/Docket Settings' report
--****************************************************************************  
Alter procedure [dbo].[usp_hts_InsertInPrintSummary]        
        
@PrintDate as datetime,        
@PrintType as varchar(15),        
@EmpID as int,        
@BatchID as  int     
as        
-- Afaq 8293 09/20/2010 remove the statements use to insert data in tblBatchSummary.      
begin      
        
Insert into tblprintsummary        
(        
 PrintDate,        
 EmpID,        
 PrintType  ,      
 BatchID       
)        
values        
(        
 @PrintDate ,        
         
 @EmpID,        
 @PrintType   ,      
 @BatchID      
      
         
)        
      
end      
      

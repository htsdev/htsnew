SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_TrialLetterBatchReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_TrialLetterBatchReport]
GO


--	USP_HTS_GET_TrialLetterBatchReport	'07/1/2007', '07/25/2007'

CREATE PROCEDURE [dbo].[USP_HTS_GET_TrialLetterBatchReport]

@stBatchDate datetime,
@endBatchDate datetime

As

set @stBatchDate =@stBatchDate +'00:00:00'
set @endBatchDate  =@endBatchDate + '23:59:59'

SELECT DISTINCT t.ticketid_pk,
                      (isnull(t.Firstname,'')+','+isnull(t.lastname,'')) as ClientName, t.Email, t.TrialLetterEmailedDate, tblCourtViolationStatus.Description AS CourtStatus, tblCourts.shortname as CourtLoc, 
                      tv.CourtDateMain,tv.RefCaseNumber,tbp.BatchDate
FROM			tblTicketsViolations AS tv INNER JOIN
                      tblTickets AS t ON t.TicketID_PK = tv.TicketID_PK INNER JOIN
                      tblCourts ON tv.CourtID = tblCourts.Courtid INNER JOIN
                      tblCourtViolationStatus ON tv.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
					  inner join tblHTSBatchPrintLetter tbp on t.ticketid_pk = tbp.ticketid_fk	
where tv.TicketsViolationID = (select min(TicketsViolationID) from tblticketsviolations tv2 
									where tv2.ticketid_pk = tv.ticketid_pk 	
								) 

and isnull(tbp.TrialLetterFlag,0) =1
and tbp.BatchDate between @stBatchDate and @endBatchDate


                 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


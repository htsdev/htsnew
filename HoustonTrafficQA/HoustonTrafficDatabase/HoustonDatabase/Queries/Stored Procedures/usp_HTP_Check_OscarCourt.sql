/****** 
Create by:  Syed Muhammad Ozair
Business Logic : this procedure is used to check for any Oscar court in the given case excluding disposed and no hire violations. 

List of Parameters:	
	@Ticketid : Ticket Id of the case

List of Columns: 	
	isfound : if found then 1 else 0

******/
CREATE Procedure dbo.usp_HTP_Check_OscarCourt

@Ticketid int

as

declare @count int


select @count=count(*) from tblticketsviolations
where ticketid_pk=@Ticketid
and courtid=3061
and courtviolationstatusidmain not in (80,236)

if @count>0
	select 1 as isfound
else
	select 0 as isfound

go

grant exec on dbo.usp_HTP_Check_OscarCourt to dbr_webuser
go
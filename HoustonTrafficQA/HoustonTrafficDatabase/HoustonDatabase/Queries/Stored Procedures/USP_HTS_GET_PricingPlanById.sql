SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PricingPlanById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PricingPlanById]
GO










CREATE procedure USP_HTS_GET_PricingPlanById  
 (  
 @PlanId  int  
 )  
  
  
  
as  
  
  
  
SELECT PlanId,
 PlanShortName,   
 PlanDescription,   
 CourtId,   
 EffectiveDate,   
 EndDate,   
 IsActivePlan  
FROM    dbo.tblPricePlans  
where planid = @planid







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


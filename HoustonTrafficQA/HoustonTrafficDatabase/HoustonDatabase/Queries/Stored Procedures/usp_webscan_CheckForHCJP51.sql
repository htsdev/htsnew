﻿/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to check for HCJP 5-1 court against the cause number if found then returns 1 else 0

List of Parameters:	
	@CauseNo : 	
	@Msg : output type parameter

Returns @Msg int 0 or 1

******/ 

ALTER procedure dbo.usp_webscan_CheckForHCJP51
                
@CauseNo as varchar(200),       
@Msg as int output            
   
as                

declare @CourtID int
set @CourtID=0
--ozair 4474 08/05/2008 setting the parameter value that is not get by procedure somehow at live
set @Msg=0
--end ozair 08/05/2008
                
declare @temp table  
(  
 id int IDENTITY(1,1) NOT NULL,  
 causeno varchar(50) NULL  
)  
insert into @temp(causeno)  
(select sValue from dbo.splitstring(@CauseNo,',') )

declare @count int  
declare @item int   
  
set @item=1  
select @count=count(*) from @temp  
  
while @count>=@item  
begin
	select @CourtID=isnull(tv.courtid,0) 
	from tblticketsviolations tv 	
	where tv.casenumassignedbycourt=(select causeno from @temp where id= @item)  
	
	if @CourtID=3015
	begin
		set @Msg=1
	end
	set @item=@item+1
end                           
          
return @Msg        

go

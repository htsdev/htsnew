/****** 
Alter by		: Sabir Khan
Created Date	: 07/31/2009
TasK ID			: 5941

Business Logic : This procesdure will check plain id if already exist then it will get and return
				 and if not exists then it will insert new price plan and return newly added price plan id.
				 
				
Column Return : 
				@updatedrec: Number of updated records.
			
******/
ALTER PROCEDURE USP_HTS_INSERT_PricingPlan
(
    @PlanShortName    VARCHAR(100)
   ,@plandescription  VARCHAR(100)
   ,@CourtId          INT
   ,@EffectiveDate    DATETIME
   ,@EndDate          DATETIME
   ,@EmployeeId       INT
   ,@IsActivePlan     BIT
   ,@PlanId           INT=0
   ,@PlanIdNew        INT=0 OUTPUT
)
AS
	--Checking any duplicate values exsists, and existance of plan related to the specified court for given date range.        
	SELECT @PlanIdNew = ISNULL(PlanID ,0) --Sabir Khan 5941 07/31/2009 Getting price plan id if already exists...
	FROM   tblpriceplans
	WHERE  (
	           plandescription=@plandescription
	           OR (
	                  DATEDIFF(DAY,ISNULL(effectivedate,'1/1/1900'),@EffectiveDate) <= 0 
	                  AND DATEDIFF(DAY,ISNULL(enddate,'1/1/1900'),@EndDate) >=0
	                  AND IsActivePlan=1
	                  AND courtid=@CourtId
	              )
	       )
	
	SELECT @PlanIdNew = ISNULL(@PlanIdNew,0) -- Sabir Khan 5941 08/17/2009 Set 0 as plan id...
	
	IF @PlanIdNew=0   --Sabir Khan 5941 07/31/2009 If Not then exists...
	BEGIN
	    INSERT INTO tblpriceplans
	      (
	        PlanShortName
	       ,plandescription
	       ,CourtId
	       ,EffectiveDate
	       ,EndDate
	       ,EmployeeId
	       ,IsActivePlan
	      )
	    VALUES
	      (
	        @PlanShortName
	       ,@plandescription
	       ,@CourtId
	       ,@EffectiveDate
	       ,@EndDate
	       ,@EmployeeId
	       ,@IsActivePlan
	      )        
	    
	    SELECT @PlanIdNew = SCOPE_IDENTITY() -- @@identity
	END
	
	RETURN      
      
    
  







/******             
Created by:  Yasir Kamal          
Business Logic : This Procedure selects all HMC clients who are missing mid numbers (x-ref) with arr, jud, or jury settings in the future.
 

List of Columns:    
 TicketID_PK : Tickets  
 ClientName  
 Date of Birth
 Ticket Number 
 Hire Date    
           
******/              
    
  --Yasir Kamal 5363 12/30/2008  HMC Missing X-Ref 



alter procedure dbo.USP_HTP_Get_MissingMidNumbers    
    
as    
    
select a.ticketid_pk, a.[client name],   
a.[date of birth],  
a.ticketnumber,  
case when a.[hire date] is null then 'N/A' else convert(varchar(10), a.[hire date], 101) end as [hire date] ,convert(varchar(10), a.courtdate, 101) AS courtdate, a.courtdatemain -- Sabir Khan 5809 04/21/2009 Court date field included... 
from   
 (  
  
select      
--'<a href="javascript:window.open(''../clientinfo/violationfeeold.aspx?search=0&casenumber='+convert(varchar(10),t.ticketid_pk)+''');void('''');"''>' + t.lastname + ', ' +  t.firstname+'</a>' as [Client Name],        
t.ticketid_pk,  t.lastname + ', ' +  t.firstname  as [Client Name],        
  case when datediff(Day, '1/1/1900',isnull(t.dob,'1/1/1900')  ) = 0 then 'N/A' else convert(varchar(10), t.dob, 101) end as [Date of birth],     
 [Hire Date] = (  
  select min(recdate) from tblticketspayment where paymentvoid = 0 and ticketid = t.ticketid_pk  
   ),     
 min(v.refcasenumber) TicketNumber, min(v.CourtDateMain) AS courtdate , v.courtdatemain     
from tbltickets t inner join tblticketsviolations v    
on  v.ticketid_pk = t.ticketid_pk    
-- yasir 5432 01/21/2009 fixed the garbage data issue..  
--where len(isnull(t.midnum,'')) = 0      
WHERE ((len(REPLACE(isnull(t.midnum,''),'''','')) = 0) or (LTRIM(RTRIM(t.Midnum)) LIKE '%[^0-9]%'))  -- Sabir Khan 5809 04/21/2009 Get all cases having null or alphanumeric midnum...
and datediff(Day, courtdatemain, getdate())<=0    
and v.courtid in (3001,3002,3003)    
and t.activeflag = 1
--Sabir Khan 5868 05/04/2009 Exclude disposed cases...
and v.CourtViolationStatusIDmain <> 80  
group by t.lastname , t.firstname ,    
 t.dob, t.ticketid_pk ,v.CourtDateMain   
  
) as a   
--Sabir Khan 5809 04/21/2009 Sort by court date...
--order by [client name]    
order by courtdatemain    
  


  
  
/******  
  
Create by:  Agha Usman  
  
Business Logic : It is used by Edelivery service to update the tracking status of the letter by the help USPS Delivery Confirmation API.  
  
   
  
List of Parameters:      
  
  @BatchId_Pk : For unique value of letter  
  @DeliveryStatus : For updating status of Edelivery  
  @USPSResponseDelivery : For updating response  
  
   
  
List of Columns:  
  
  No Columns  
******/  
  
  
ALTER PROCEDURE [dbo].[usp_hts_UpdateEdeliveryLetterTracking]   
@BatchId_Pk int,  
@DeliveryStatus smallint,  
@USPSResponseDelivery xml  
AS  
BEGIN  
declare @TicketId int  
declare @OldStatus bit  
Select @TicketId = TicketID_FK ,@OldStatus = DeliveryStatus from tblHtsbatchprintletter where BatchID_PK = @BatchId_Pk  
  
 update tblHtsbatchprintletter   
 set  DeliveryStatus = @DeliveryStatus,  
 USPSResponseDelivery = @USPSResponseDelivery   
 where BatchID_PK = @BatchId_Pk  
  
 if @DeliveryStatus = 0   
	exec usp_HTS_Insert_CaseHistoryInfo @TicketId,'Delivery status updated to :  Pending',3992,null
 else  
	exec usp_HTS_Insert_CaseHistoryInfo @TicketId,'Delivery status updated to :  Delivered',3992,null
 
end  
  

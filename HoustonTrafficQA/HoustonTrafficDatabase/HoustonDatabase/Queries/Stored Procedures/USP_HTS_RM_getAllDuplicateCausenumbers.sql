SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_RM_getAllDuplicateCausenumbers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_RM_getAllDuplicateCausenumbers]
GO

CREATE procedure USP_HTS_RM_getAllDuplicateCausenumbers

as

select casenumassignedbycourt as [Cause Number], count(casenumassignedbycourt) as Counts from tbltickets t
	inner join tblticketsviolations tv on t.ticketid_pk = tv.ticketid_pk 
where courtviolationstatusidmain not in (80, 236)
	and courtid not in (3001, 3002, 3003)
group by casenumassignedbycourt 
having count(casenumassignedbycourt) > 1
order by Counts desc




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


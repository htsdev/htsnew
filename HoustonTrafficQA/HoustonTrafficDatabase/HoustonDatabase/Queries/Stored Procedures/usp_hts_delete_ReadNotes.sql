SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_delete_ReadNotes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_delete_ReadNotes]
GO


Create Procedure usp_hts_delete_ReadNotes    
------DELETE FLAG (FAHAD-12/24/2007)    
@ticketid as int,
@empid as int     
    
as    
    
delete  from tblticketsflag where ticketid_pk = @ticketid and flagid = 31  
  
update tbltickets set readcomments = null where ticketid_pk = @ticketid

declare @desc varchar(20)                      
select @desc=description from tbleventflags where flagid_pk=31                    
insert into tblticketsnotes(ticketid,subject,employeeid,Notes) values(@ticketid,'Flag Removed -'+ @desc,@empid,null)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_ScanReport_bak]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_ScanReport_bak]
GO



-- USP_HTS_GET_ScanReport '12/11/2004', 0

create      procedure USP_HTS_GET_ScanReport_bak            
        
@sdate datetime,      --date to match with recdate        
@requesttype int = 0  -- Either want to see all records or selected records          
as            
if @requesttype = 0     --when search by date        
	begin        
		select 	ticketnumber,        
			s.courtdate,        
			s.courtno,        
			ViolationStatus,        
			Updateflag = case updateflag            
						when 0 then 'Pending'             
						when 2 then 'Exist'            
						else 'Updated' 
					end,        
			s.RecDate,        
			doc_id,        
			docnum,        
			c.description,            
			sortorder= case updateflag            
					when 0 then 0             
					when 2 then 2            
					else 1 
				end ,
			isnull(v.ticketid_pk , 0)   as ticketid  ,
			isnull(t.activeflag, 0) as activeflag      
		FROM	dbo.tblTickets t
		INNER JOIN
			dbo.tblTicketsViolations v 
		ON 	t.TicketID_PK = v.TicketID_PK 
		RIGHT OUTER JOIN
			dbo.tblScannedDocketCourtStatus S 
		INNER JOIN
			dbo.tblCourtViolationStatus c 
		ON 	S.ViolationStatus = c.CourtViolationStatusID 
		ON 	v.RefCaseNumber = S.ticketnumber
		where 	convert(varchar(10),s.recdate,101)=@sdate        
		order by 
			sortorder asc,
			ticketnumber            
	end            






else   --When viewing all records         

	begin        
		select        
		ticketnumber,        
		s.courtdate,        
		courtno,        
		ViolationStatus,        
		Updateflag = case updateflag            
		when 0 then 'Pending'             
		when 2 then 'Exist'            
		else 'Updated' end,        
		s.RecDate,        
		doc_id,        
		docnum,        
		c.description,            
		sortorder= case updateflag            
		when 0 then 0             
		when 2 then 2            
		else 1 end ,
		isnull(v.ticketid_pk , 0) as ticketid ,
			isnull(t.activeflag, 0) as activeflag      
		FROM	dbo.tblTickets t
		INNER JOIN
			dbo.tblTicketsViolations v 
		ON 	t.TicketID_PK = v.TicketID_PK 
		RIGHT OUTER JOIN
			dbo.tblScannedDocketCourtStatus S 
		INNER JOIN
			dbo.tblCourtViolationStatus c 
		ON 	S.ViolationStatus = c.CourtViolationStatusID 
		ON 	v.RefCaseNumber = S.ticketnumber     
		where updateflag = 0    
		and datediff(day,s.recdate,getdate()) < = 31              
		order by sortorder asc,ticketnumber            
	end   
	





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


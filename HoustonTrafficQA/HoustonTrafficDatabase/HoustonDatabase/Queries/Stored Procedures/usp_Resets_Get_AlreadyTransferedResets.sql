SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Resets_Get_AlreadyTransferedResets]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Resets_Get_AlreadyTransferedResets]
GO


-- usp_Resets_Get_AlreadyTransferedResets 464,126

CREATE  PROCEDURE [dbo].[usp_Resets_Get_AlreadyTransferedResets]                   
@olddocid int,  
@olddocnum int                 
  
AS                  
  
Select count(*) as transfered   
from tblScanPIC   
Where olddocid=@olddocid and olddocnum=@olddocnum  
  
  
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


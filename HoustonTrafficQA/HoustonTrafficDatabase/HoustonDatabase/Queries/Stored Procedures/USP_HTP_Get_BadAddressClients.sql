set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****
Created By : Sabir Khan     
    
Business Logic : This stored procedure is used get all client whose email address is not verified by zp4. 
 --
 *****/
-- USP_HTP_Get_BadAddressClients 1    
ALTER PROCEDURE [dbo].[USP_HTP_Get_BadAddressClients]  

@ShowAll INT =0
AS  

 -- sameeullah  1/4/2011 8488 No need to convert Followup date to 1/1/19100 

SELECT DISTINCT t.TicketID_PK, 
t.lastname, t.Address1 + ' ' + ISNULL(t.Address2,'') AS ADDRESS, -- Haris Ahmed 08/13/2012 9641 Add space between address1 and address2
Convert(varchar,max(tv.CourtDateMain),101) CourtDateMain,
Convert(varchar,MIN(tp.RecDate),101) AS Hiredate,
ISNULL(CONVERT(VARCHAR,e.BadAddressFollowUpDate,101),'') AS FollowUpDate
FROM tblTickets t 
INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
INNER JOIN tblTicketsPayment tp ON tp.TicketID = t.TicketID_PK
INNER JOIN tblCourtViolationStatus tvs ON tvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain
INNER JOIN tblcourts c ON c.Courtid = tv.CourtID
LEFT OUTER JOIN tbltickets e ON e.Ticketid_pk = t.TicketID_PK
WHERE ISNULL(t.Activeflag,0) = 1 
AND t.YDS = 'N'
AND tvs.CategoryID <> 50
AND tvs.CourtViolationStatusID <> 105 --Farrukh 9945 12/19/2011 Excluded cases having Case Status Missed Court
AND (@ShowAll = 1 OR DATEDIFF(DAY,isnull(e.BadAddressFollowUpDate,'1/1/1900'),GETDATE())>=0) 
AND ISNULL(t.NoBadAddressFlag, 0) = 0 --Haris Ahmed 10003 01/20/2012 Excluded cases having No Bad Address Flag is true
GROUP BY t.TicketID_PK,
t.Lastname, 
t.Address1 + ' ' + ISNULL(t.Address2,''),
e.BadAddressFollowUpDate











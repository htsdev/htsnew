IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_HTS_UPDATE_PartialPayNotes')
	BEGIN
		DROP  Procedure   [dbo].[USP_HTS_UPDATE_PartialPayNotes]
	END

GO

GO




--Fahad 2804 (2-7-08)  
--Now General Comments should not update from this procedure   
CREATE procedure [dbo].[USP_HTS_UPDATE_PartialPayNotes]            
        
@ScheduleID as int,       --Schedule Record ID          
@ticketid as int ,       --ticket number          
@empid as int ,          --current employee/user                  
@DueDate as datetime,        --Payment due date        
@amount as money  
  
    --Status either pending , contacted,message           
              
as              
if (@ScheduleID<>0)   --Update existing record in tblscchedule    
 begin           
      
    update tblSchedulePayment set PaymentDate=@DueDate        
  where scheduleid=@Scheduleid and        
        ticketid_pk = @ticketid            
  end    
    
else --Insert into tblschedulepayment owes amount    
    
 begin    
     insert into tblSchedulePayment values (@ticketid,@amount,@DueDate,getdate(),1,@empid)       
          end    



GO

GO


GRANT EXECUTE ON [dbo].[USP_HTS_UPDATE_PartialPayNotes] TO [dbr_webuser]

GO
 
  
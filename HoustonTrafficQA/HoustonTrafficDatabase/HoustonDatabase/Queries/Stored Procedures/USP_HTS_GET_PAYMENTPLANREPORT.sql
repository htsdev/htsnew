﻿/**
* Business Logic : This procedure returns client payment information base on parameter criterea
* parameter : 
			* @dsdata datetime : Start Date
			  @dedate datetime : ENd date
			  @critaria : Plan criterea  1 For Payment Reminder Calls, 2 for Payment Defaults, 3 for Waived Payments, 4 for Past Dues Calls,
			  @chk int : 1 for Show all , 0 for not showing all records 
			  @CourtType : 0 for All Courts , 1 for All Criminal Courts
			  
Column selected 
		*	TicketID_PK
		*	ClientName
		*	crt
		*	CourtDate
		*	TotalFeeCharged
		*	Owes
		*	PaymentDate
		*	PaymentFollowUpdate
		*	Remainingdays
		*	CrtType
**/
 
 --	USP_HTS_GET_PAYMENTPLANREPORT  '03/24/2009', '03/24/2009', 1,1,0
ALTER procedure [dbo].[USP_HTS_GET_PAYMENTPLANREPORT]          
          
@dsdata datetime ,           
@dedate datetime ,           
--@cSdate datetime ,           
--@cEdate datetime ,           
@critaria int,          
@chk int,          
@CourtType int          
          
AS

/***        
--select @dedate = @dedate + '23:59:59.000'
--Declare @Cstatus varchar(12)
--set @Cstatus = @critaria
--declare @strParamDef nvarchar(200)
--set @strParamDef = '@dsdata datetime , @dedate datetime ,          
--@critaria int, @chk int,  @CourtType int, @critaria varchar(12)'          
***/

declare @strQuery nvarchar(4000)
          
set @strQuery = '          
SELECT      t.paymentstatus,
 t.Firstname + '' '' + t.Lastname AS ClientName,          
 t.TicketID_PK,          
 max(CONVERT(VARCHAR(200),sp.PaymentDate,101)) as  PaymentDate ,       
 --sp.Amount ,      
 --'''' as PmytComments, --Agha Usman 2664 06/17/2008
 --sp.scheduleid,           
 --t.Activeflag,          
 --sp.ScheduleFlag, 
 isnull(t.TotalFeeCharged,0) -           
  (          
   select sum(isnull(chargeamount,0)) from tblticketspayment p          
    where p.ticketid = t.ticketid_pk          
     and p.paymentvoid = 0          
     and p.paymenttype not in (99,100)          
  ) as Owes,          
 t.TotalFeeCharged,          
--  (select isnull(sum(isnull(chargeAmount,0)) ,0)           
--  from tblticketspayment where ticketid =  t.ticketid_pk
--  and paymentvoid = 0      
--  and paymenttype not in (0,98,99,100)) as Paidamount,  

 (isnull((select  min(tv.courtdatemain) from tblticketsviolations tv          
    where tv.ticketid_pk = t.ticketid_pk          
     and datediff(day, getdate(), tv.courtdatemain) >=0),          
    (select  max(tv1.courtdate) from tblticketsviolations tv1 where tv1.ticketid_pk = t.ticketid_pk))) as CourtDate,           

 (select  top 1 c.shortname from tblticketsviolations tv ,tblcourts c       
 where tv.courtid = c.courtid and tv.ticketid_pk = t.ticketid_pk) as crt,
 
 -- Noufil 5618 03/31/2009 Followupdate and days difference column added.
 CONVERT(VARCHAR(200),t.PaymentDueFollowUpDate,101) AS PaymentFollowUpDate, datediff(day,getdate(),t.PaymentDueFollowUpDate) as Pastdays
 --Nasir 6049 07/10/2009 add case Type
,(SELECT ct.CaseTypeName FROM CaseType ct WHERE ct.CaseTypeId=t.CaseTypeId) as CrtType
FROM      dbo.tblTickets AS t INNER JOIN          
                      dbo.tblSchedulePayment AS sp ON t.TicketID_PK = sp.TicketID_PK 

WHERE (t.Activeflag = 1) 
-- noufil 5618 04/09/2009 Show all client either owes or not.
--AND (sp.ScheduleFlag = 1)    
 
and (select top 1  tv.courtid from tblticketsviolations tv where      
 tv.ticketid_pk =t.ticketid_pk) in (select distinct(CourtID) from tblcourts where courtid <> 0 and InActiveFlag = 0) 

'       
if(@critaria= 4)--ozair 5618 05/08/2009 only past follow up dates 
				set @strQuery = @strQuery + 'AND (datediff(day, isnull(t.PaymentDueFollowUpDate,''01/01/1900''), Getdate()) > 0 ) AND isnull(t.paymentstatus,1) not in (2,3) '
			else
				set @strQuery = @strQuery + 'AND isnull(t.paymentstatus,1) = ' + CONVERT(VARCHAR(5), @critaria )
				
if @CourtType <> 0 -- Search with case type if @courtType <> 0
	begin           
			set @strQuery = @strQuery + ' AND t.CaseTypeId='+ CONVERT(VARCHAR,@CourtType) -- Noufil 06/01/2009 5965 search iwth case type.
	end  
	
	
IF @chk=0
	BEGIN		
		set @strQuery = @strQuery + ' AND (datediff(day,convert(datetime,''' + CONVERT(VARCHAR(20),@dsdata, 101) + '''),isnull(t.PaymentDueFollowUpDate,''1/1/1900''))>=0 and datediff(day,isnull(t.PaymentDueFollowUpDate,''1/1/1900''),convert(datetime,''' + CONVERT(VARCHAR(20),@dedate, 101) + '''))>=0)'
	END

set @strQuery = @strQuery + '	
	 AND (ISNULL(t.TotalFeeCharged, 0) -(
               SELECT SUM(ISNULL(chargeamount, 0))
               FROM   tblticketspayment p
               WHERE  p.ticketid = t.ticketid_pk
                      AND p.paymentvoid = 0
                      AND p.paymenttype NOT IN (99, 100)
           )) > 0 '
																												--Nasir 6049 07/10/2009 add case Type
set @strQuery = @strQuery + 'GROUP BY t.paymentstatus,t.Firstname,t.Lastname,t.TicketID_PK,t.TotalFeeCharged,t.PaymentDueFollowUpDate,t.CaseTypeId'
set @strQuery = @strQuery + ' ORDER BY t.PaymentDueFollowUpDate DESC'


PRINT(@strQuery)
EXEC(@strQuery)




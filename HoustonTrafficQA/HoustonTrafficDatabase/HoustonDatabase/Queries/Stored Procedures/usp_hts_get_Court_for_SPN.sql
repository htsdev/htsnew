SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_Court_for_SPN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_Court_for_SPN]
GO


CREATE  PROCEDURE dbo.usp_hts_get_Court_for_SPN      
@ticketid as numeric      
      
as      
      
SELECT     COUNT(CourtID) AS Courts      
FROM         tblTicketsViolations      
WHERE    CourtID = 3037 --(CourtID not in (3001,3002,3003))       
AND (TicketID_PK = @ticketid)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


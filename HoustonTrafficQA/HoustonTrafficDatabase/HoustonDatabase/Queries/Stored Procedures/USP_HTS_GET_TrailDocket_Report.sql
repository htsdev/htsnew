SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_TrailDocket_Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_TrailDocket_Report]
GO













--exec usp_hts_update_trialcomments_for_Ticket_ID 53191,'Hello HI'
---exec USP_HTS_GET_TrailDocket_Report 'None','3/16/2005'

CREATE           procedure USP_HTS_GET_TrailDocket_Report   
  
@courtloc varchar(10) = 'None',    
@courtdate varchar(12) = '01/01/1900',    
@page varchar(12) = 'TRIAL',    
@courtnumber varchar(6) = '0',    
@datetype varchar(20) = '0'    
as     
  
/*  
declare @courtloc varchar(10),   
@courtdate varchar(12),    
@page varchar(12) ,    
@courtnumber varchar(6) ,    
@datetype varchar(20)     
    
set @courtloc = 'None'    
set @courtdate='03/04/2005'    
set @page ='TRIAL'    
set @courtnumber ='0'    
set @datetype ='0'    
  */  
set nocount on     
SELECT  dbo.tblTickets.TicketID_PK,dbo.tblTickets.Firstname,dbo.tblTickets.MiddleName, dbo.tblTickets.Lastname,       
          isnull(dbo.tblOfficer.FirstName,'N/A') as Ofirstname, isnull(dbo.tblOfficer.LastName,'N/A') as Olastname, dbo.tblViolations.ShortDesc, dbo.tblTickets.CourtDate,       
           dbo.tblTicketsViolations.courtdatemain as currentdateset,dbo.tblTicketsViolations.courtid as currentcourtloc,convert(int,dbo.tblTicketsViolations.courtnumbermain) as currentcourtnum,    
  dbo.tblcourtviolationstatus.ShortDescription as description,activeflag,dbo.tblcourts.courtname, dbo.tblcourts.Address,      
trialcomments=      
case     
 left(dbo.tblcourtviolationstatus.Description,3)      
  when 'PRE' then dbo.tblTickets.pretrialcomments      
  else dbo.tblTickets.trialcomments end      
      
,bondflag,firmabbreviation,courttype,      
case dbo.tblTicketsViolations.violationnumber_pk      
 when 11 then 1      
 else 0      
end as accflag,      
speeding = dbo.getviolationspeed(sequencenumber,refcasenumber,fineamount),      
convert(varchar(2),month(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(2),day(dbo.tblTicketsViolations.courtdatemain)) + '/' + convert(varchar(4),year(dbo.tblTicketsViolations.courtdatemain)) as courtarrdate,      
datepart(hour,dbo.tblTicketsViolations.courtdatemain) as courttime,dbo.tblCaseDispositionstatus.description as ViolationStatusID,      
dbo.tblTicketsViolations.violationnumber_pk as violationnumber,address1,midnum,violationtype,dbo.tblCourts.Address as courtaddress,dbo.tblcourtviolationstatus.categoryid as courtviolationstatusid       
into #temp      
FROM  dbo.tblTickets INNER JOIN      
                    dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN      
  dbo.tblCaseDispositionstatus ON dbo.tblTicketsViolations.violationstatusid = dbo.tblCaseDispositionstatus.casestatusid_pk INNER JOIN      
   dbo.tblfirm ON dbo.tblTickets.firmid = dbo.tblfirm.firmid INNER JOIN      
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN      
                      dbo.tblcourtviolationstatus ON dbo.tblTicketsviolations.courtviolationstatusidmain = dbo.tblcourtviolationstatus.courtviolationstatusID INNER JOIN      
                      dbo.tblCourts ON dbo.tblTicketsViolations.courtid = dbo.tblCourts.Courtid LEFT OUTER JOIN      
                      dbo.tblOfficer ON dbo.tblTickets.OfficerNumber = dbo.tblOfficer.OfficerNumber_PK      
WHERE     (dbo.tblViolations.Violationtype not IN (1) or dbo.tblViolations.violationnumber_pk in (11,12) ) AND (dbo.tblcourtviolationstatus.categoryid IN (select distinct categoryid from tblcourtviolationstatus   
where violationcategory in (0,2) and categoryid not in (50) ))       
AND (dbo.tblTickets.Activeflag = 1)       
and currentcourtloc <> 0      
and datediff(DAY,dbo.tblTicketsViolations.courtdatemain,@courtdate) = 0    
    
--select * from #temp    
    
if @@rowcount > 0       
begin      
--select * from tblticketsactivity      
--select * from tblactivitystatus      
/*    
select ticketid,activitytypeid --into #temp1      
from tblticketsactivity A,tbltickets T where       
T.ticketid_pk = A.ticketid      
and activitytypeid = 4      
and activeflag = 1      
and ActivityStatusID <> 100      
*/    
    
select ticketid,totalfeecharged-sum(ChargeAmount) as dueamount      
into #temp2      
from tblticketspayment A,tbltickets T where       
T.ticketid_pk = A.ticketid      
and activeflag = 1      
and paymenttype not in (99,100)      
group by ticketid,totalfeecharged      
    
    
declare @sqlquery varchar(6000)      
set @sqlquery = ' '      
    
select     
T.*,    
--isnull(V.activitytypeid,0) as continuance,     
dueamount     
into #temp3      
FROM #temp T     
--LEFT OUTER JOIN #temp1 V ON T.TicketID_PK = V.TicketID       
left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid       
    
SELECT     #temp3.*,count(*) as clientcount into #temp4      
--TicketID_PK, dbo.tblTickets.Firstname, dbo.tblTickets.Lastname, tblTickets_1.Address1      
FROM         #temp3 RIGHT OUTER JOIN      
                      tblTickets T ON #temp3.Firstname = T.Firstname AND #temp3.Lastname = T.Lastname AND       
                     -- #temp3.Address1 = T.Address1       
   (#temp3.Address1 = T.Address1 or #temp3.midnum = T.midnum)      
GROUP BY       
#temp3.TicketID_PK,#temp3.Firstname,#temp3.MiddleName,#temp3.Lastname,#temp3.Ofirstname,#temp3.Olastname,#temp3.ShortDesc,#temp3.CourtDate,#temp3.currentdateset,#temp3.currentcourtloc,#temp3.currentcourtnum,#temp3.Description,#temp3.activeflag,    
#temp3.courtname,#temp3.Address,#temp3.trialcomments,#temp3.bondflag,#temp3.firmabbreviation,#temp3.courttype,#temp3.accflag,#temp3.speeding,    
--#temp3.continuance,    
#temp3.dueamount,#temp3.courtarrdate,#temp3.courttime,#temp3.ViolationStatusID,#temp3.violationnumber,    
#temp3.address1,#temp3.midnum,#temp3.violationtype,#temp3.courtaddress,#temp3.courtviolationstatusid      
set @sqlquery = 'select P.TicketID_PK,P.Firstname,P.MiddleName,P.Lastname,P.Ofirstname,P.Olastname,replace(P.ShortDesc,''None'',''Other'') as shortdesc,P.CourtDate,P.currentdateset,P.currentcourtloc,P.currentcourtnum,P.Description,P.activeflag,P.courtname
  
    
,P.Address,P.trialcomments,P.bondflag,P.firmabbreviation,P.courttype,P.accflag,P.speeding,P.dueamount,P.courtarrdate,P.courttime,P.ViolationStatusID,P.violationnumber,P.clientcount,T.coveringfirmid,F.FirmAbbreviation as coveringfirm,    
P.violationtype,courtaddress,cdlflag,courtviolationstatusid,convert(varchar(20),P.TicketID_PK) + isnull(convert(varchar(4),courtviolationstatusid),0) as ticketstatusid,       
pretrialstatus =  case      
when pretrialstatus = 1 and T.currentcourtloc between 3001 and 3022 and left(Description,3) <> ''JUR'' then ''RFT''      
when pretrialstatus =2 and  left(Description,3) <> ''JUR'' then ''WBT''      
when pretrialstatus =4 and  left(Description,3) <> ''JUR'' then ''DA''      
when pretrialstatus =5 and  left(Description,3) <> ''JUR'' then ''DSC''      
else '' '' end    ,AccidentFlag  
      
into #temp5 from #temp4 P,dbo.tbltickets T,dbo.tblfirm F  where P.ticketid_pk = T.ticketid_pk and T.coveringfirmid = F.firmid and P.ticketid_pk is not null '      
if @courtloc is not null and @courtloc <> 'None'        
begin      
 if @courtloc = 'JP' or @courtloc = 'OU'      
  if @courtloc = 'JP'      
   begin      
    set @sqlquery = @sqlquery + ' and P.currentcourtloc not in (3001,3002,3003)'      
   end      
  else      
   begin      
    set @sqlquery = @sqlquery + ' and P.currentcourtloc in (3001,3002,3003)'      
   end      
 ELSE      
        
        
  begin      
   set @sqlquery = @sqlquery + ' and P.currentcourtloc = ' + @courtloc      
  end      
end      
if @courtdate is not null and @courtdate <> '01/01/1900'      
begin      
 set @sqlquery = @sqlquery +  ' and datediff(day,P.currentdateset,'''  + @courtdate + ''') = 0'      
end      
if @courtnumber <> '0'      
begin      
 set @sqlquery = @sqlquery +  ' and P.currentcourtnum = ' + @courtnumber      
end      
if @datetype <> '0'      
begin      
 set @sqlquery = @sqlquery +  ' and courtviolationstatusid in (' + @datetype + ')'      
end      
set @sqlquery = @sqlquery +  ' and P.ShortDesc not in (''CDL'',''ACC'',''PROB'') order by P.courtarrdate,P.currentcourtloc,P.Address,convert(int,P.currentcourtnum) asc,P.courttime,P.lastname,P.firstname,P.middlename '      
      
set @sqlquery = @sqlquery +  'select * into #violations from tblviolations where description like ''%speeding%'' and violationtype <> 0 '      
      
set @sqlquery = @sqlquery +  'select T.*, isnull(V.violationnumber_pk,0) as speedingvnum   

into #temp6  
from #temp5 T left outer join #violations V on T.violationnumber = V.violationnumber_pk order by case T.courtviolationstatusid      
when 9 then 9      
when 0 then 9      
else 1 end      
,T.courtarrdate,T.currentcourtloc,T.Address,convert(int,T.currentcourtnum) asc,T.courttime,T.lastname,T.firstname,T.middlename,T.courtviolationstatusid    
  
drop table #temp5  
  
  
declare @ticketID int  
  
declare @tbl1 table ( ticketid int,  shortdesc1 varchar(1000))  
  
declare @strVaue varchar(200)  
  
declare Crtemp cursor for   
select distinct ticketID_pk from #temp6  
  
open Crtemp  
fetch next from Crtemp  
 into @ticketid  
  
WHILE @@FETCH_STATUS = 0  
 BEGIN  
  set @strVaue = ''''  
  select @strVaue = @strVaue + shortdesc + '', '' from #temp6 where ticketID_pk = @ticketid  
--  select @strVaue = @strVaue    from #temp6 where ticketID_pk = @ticketid  
  select distinct @strVaue = @strVaue + case speeding when '' '' then '' '' else ''(''+speeding+ '')'' end from #temp6 where ticketID_pk = @ticketid  
  insert into @tbl1 values(@ticketid, @strVaue)  
  
  fetch next from Crtemp  
  into @ticketid  
 END  
  
close Crtemp  
DEALLOCATE Crtemp  
  
SELECT TicketId_Pk as CaseNumber,
   '':''+lastName Client_LastName , t2.shortdesc1, TicketID_PK ,  
         Firstname+'',''+MiddleName  as FMname,   
   case Olastname+'',''+Ofirstname when ''N/A,N/A'' then '''' else Olastname+'',''+Ofirstname end  OLFNAME , currentcourtloc,   
 currentcourtnum, Description, courtname+''-''+Address+''-''+convert(varchar(10),currentdateset,101)CourtName_Address_DateSet,  
       bondflag , firmabbreviation, courttime,   
       case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end ClientCount,  
      ltrim ( case clientcount when 1 then '''' else ''*''+convert(varchar(5),clientcount) end +'' ''+  
       ltrim( case AccidentFlag 
              when 0 then '''' else ''ACC'' end +'' ''+
              
              case cdlflag 
              when 0 then '''' else ''CDL'' end +'' ''+
              case pretrialstatus 
              when ''RFT'' then ''(RFT)'' else      pretrialstatus end  ))  
       as ClientCount_PreTrialStatus_CDLFLAG, dbo.formattime(t1.currentdateset) as currentdateset,''    '' as space,
trialcomments
into #temp7  
FROM #temp6 t1, @tbl1 t2   
 where t2.ticketid = t1.TicketID_PK  
  
  
  
SELECT distinct    casenumber,
        count(ticketid_pk) as violationCount ,Client_LastName, isnull(shortdesc1,'''') shortdesc1, TicketID_PK ,  
         FMname,   
  OLFNAME , currentcourtloc,   
  case currentcourtnum when '''' then '''' else ''#''+convert(varchar(5),currentcourtnum) end currentcourtnum, Description, CourtName_Address_DateSet, case bondflag when 0 then '''' else ''B'' end BondFlag, firmabbreviation, courttime,   
  min(clientcount) as clientcount,ClientCount_PreTrialStatus_CDLFLAG  ,currentdateset,space,trialcomments

FROM #temp7  
group by   
      TicketID_PK, shortdesc1,FMname,Client_LastName,OLFNAME,  
         currentcourtloc,currentcourtnum,   
 Description,CourtName_Address_DateSet,   bondflag, firmabbreviation,   
 courttime,ClientCount_PreTrialStatus_CDLFLAG,currentdateset,casenumber,space,
trialcomments  

order by currentcourtloc  
  
drop table #temp6  
drop table #temp7   
' 
--print @sqlquery      
exec(@sqlquery)      

drop table #temp2      
drop table #temp3      
drop table #temp4      
end
drop table #temp 
  
  













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


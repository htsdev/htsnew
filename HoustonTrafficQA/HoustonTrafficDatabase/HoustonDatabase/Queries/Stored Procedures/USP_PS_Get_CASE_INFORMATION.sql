/****************
Alter By : Zeeshan Ahmed

Business Logic : This procedure is used to get detail information about Quote or Non Client Cases
				 Set Texas State as default state for the cases which have null stateid.
List Of Input Parameters :

@RecordId : Record Id of the case                         
@TicketID : Ticket Id of the case       
@Letterid : Letter Id of the case     

List Of Columns :

TicketNumber
midnumber                                           
DLNumber,                                           
violationNumber_pk,
ViolationCode,                                           
ViolationDescription,                                           
FineAmount ,                                            
FirstName,                                           
LastName,                                           
Address1,                                            
Address2,                                           
courtid,                      
Gender,                                           
City,                                           
StateID_FK,                                      
State,                                            
ZipCode,                                          
PhoneNumber,                                           
DOB,                                           
ViolationDate,                                           
CourtDate ,                  
Race,                                           
CourtNumber,                                           
Courttime,                                      
RecordID ,                                     
PlanId,                                    
BondFlag,                            
BondAmount  ,                    
TypeID      ,      
Fee ,    
AccidentFlag,    
SchoolFlag,    
CDLFlag,             
DLstate,
fTALinkID


***************/


              
                
Alter procedure [dbo].[USP_PS_Get_CASE_INFORMATION]   --0,5132,0       
      
                                       
@RecordId int ,                         
@TicketID int ,      
@Letterid int                           
as                                       
        
        
declare @Amount numeric         
declare @isHCJPWarrentLetter as int 

Set @isHCJPWarrentLetter =0     

if ( @Letterid <> 0 )
Begin
	
	if (Select top 1 LetterType from tblletternotes where noteid = @Letterid )= 21
		set @isHCJPWarrentLetter = 1
	
End

        
if ( @TicketID = 0 )        
Set @Amount= 0         
        
else        
select top 1  @Amount = isnull(CalculatedTotalFee,0)  from tbltickets where ticketid_pk = @TicketID        
      
      
if  @TicketID = 0       
Begin      
        
Select        
  case when T.courtlocation between 3007 and 3022 then isnull(T.causenumber,T.ticketnumber_pk) else T.ticketnumber_pk end as TicketNumber ,                                   
    TV.midnumber,                                           
    TV.DLNumber,                                           
    T.ViolationNumber_pk,
    T.ViolationCode,                                           
    T.ViolationDescription,                                           
    case when @isHCJPWarrentLetter = 1 then T.BondAmount else  T.FineAmount end as FineAmount ,                                           
    TV.FirstName,                                           
    TV.LastName,                                           
    TV.Address1,                                            
    TV.Address2,                                           
    t.courtlocation as courtid,                      
    TV.Gender,                                           
    TV.City,                                           
    isnull(TV.StateID_FK,45) as Stateid_fk,                                      
    S.State,                                            
    TV.ZipCode,                                          
    TV.PhoneNumber,                                           
    TV.DOB,                                           
    TV.ViolationDate,                                           
    isnull(T.CourtDate,'1/1/1900') as CourtDate ,                  
 TV.Race,                                           
    T.CourtNumber,                                           
    TV.Courttime,                                      
   isnull( TV.RecordID,0) as RecordID ,                                     
   PlanId = (                                      
   select top 1 planid from tblpriceplans p                                       
   where tv.listdate between p.effectivedate and p.enddate                                      
   and p.isactiveplan = 1                                       
   and p.courtid = tv.courtid                                      
   ) ,                                    
    BondFlag = ( case when t.courtdate < getdate() then 1 else 0 end ),                            
 isnull(T.BondAmount,0) as BondAmount  ,                    
 dbo.tblDateType.TypeID      ,      
 0 as Fee ,    
 0 as AccidentFlag,    
 0 as SchoolFlag,    
 0 as CDLFlag,             
 0 as DLstate,
 fTALinkID

                       
 into #temp                                    
    from                     
   dbo.tblState AS S RIGHT OUTER JOIN                    
         dbo.tblTicketsViolationsArchive AS T INNER JOIN                    
         dbo.tblTicketsArchive AS TV ON TV.RecordID = T.RecordID INNER JOIN                    
         dbo.tblDateType INNER JOIN                    
         dbo.tblCourtViolationStatus AS CVS ON dbo.tblDateType.TypeID = CVS.CategoryID ON T.violationstatusid = CVS.CourtViolationStatusID ON                     
         S.StateID = isnull(TV.StateID_FK,45)                
 where t.recordid = @recordid 
 and T.violationstatusid != 80
 and ClientFlag = 0                                



------------------------------------------------                              
-- Finding Other FTA Cases Associated with the current case --    
    
    
Declare @FTALinkID as int     
Set @FTALinkID=0    
    
Select Top 1 @FTALinkID = fTALinkID  from #temp where ftalinkid > 0  
    
    
if @FTALinkID > 0     
Begin    
 -- Find Associated FTA Tickets     
     
insert into  #temp     
    
 Select 
 
     case when T.courtlocation between 3007 and 3022 then isnull(T.causenumber,T.ticketnumber_pk) else T.ticketnumber_pk end as TicketNumber ,                                   
    TV.midnumber,                                           
    TV.DLNumber,                                           
	T.violationNumber_pk,
    T.ViolationCode,                                           
    T.ViolationDescription,                                           
     case when @isHCJPWarrentLetter = 1 then T.BondAmount else  T.FineAmount end as FineAmount ,                                            
    TV.FirstName,                                           
    TV.LastName,                                           
    TV.Address1,                                            
    TV.Address2,                                           
    t.courtlocation as courtid,                      
    TV.Gender,                                           
    TV.City,                                           
    isnull(TV.StateID_FK,45) as Stateid_fk,                                     
    S.State,                                            
    TV.ZipCode,                                          
    TV.PhoneNumber,                                           
    TV.DOB,                                           
    TV.ViolationDate,                                           
    isnull(T.CourtDate,'1/1/1900') as CourtDate ,                  
 TV.Race,                                           
    T.CourtNumber,                                           
    TV.Courttime,                                      
   isnull( TV.RecordID,0) as RecordID ,                                     
   PlanId = (                                      
   select top 1 planid from tblpriceplans p                                       
   where tv.listdate between p.effectivedate and p.enddate                                      
   and p.isactiveplan = 1                                       
   and p.courtid = tv.courtid                                      
   ) ,                                    
    BondFlag = ( case when t.courtdate < getdate() then 1 else 0 end ),                            
 isnull(T.BondAmount,0) as BondAmount  ,                    
 dbo.tblDateType.TypeID      ,      
 0 as Fee ,    
 0 as AccidentFlag,    
 0 as SchoolFlag,    
 0 as CDLFlag,             
 0 as DLstate,
 fTALinkID
             
from               
 dbo.tblState AS S RIGHT OUTER JOIN              
    dbo.tblTicketsViolationsArchive AS T INNER JOIN              
 dbo.tblTicketsArchive AS TV ON TV.RecordID = T.RecordID INNER JOIN              
 dbo.tblDateType INNER JOIN              
 dbo.tblCourtViolationStatus AS CVS ON dbo.tblDateType.TypeID = CVS.CategoryID ON T.violationstatusid = CVS.CourtViolationStatusID ON               
 S.StateID = isnull(TV.StateID_FK,45)              
where t.recordid <>  @recordid  and FTALinkID = @FTALinkID  and  TV.ClientFlag = 0   

End


select * , @Amount as Amount from #temp                                    
drop table #temp       
end      
      
Else      
begin      
      
      
Select        
  case when tv.courtid between 3007 and 3022 then  tv.casenumassignedbycourt  else tv.refcaseNumber  end  as TicketNumber ,                                   
    T.midnum as midnumber,                                           
    T.DLNumber,                                           
	TV.ViolationNumber_pk  ,                                          
    VS.Description as ViolationDescription,         
    case when @isHCJPWarrentLetter = 1 then TV.BondAmount else  TV.FineAmount end as FineAmount ,                                         
    T.FirstName,                                           
    T.LastName,                                           
    T.Address1,                                
    T.Address2,                                           
    TV.courtid,                      
    T.Gender,                                           
    T.City,                                          
    isnull(T.StateID_FK,45) as Stateid_fk,                                     
    S.State,                                            
    T.Zip as ZipCode,                                          
    T.contact1 as  PhoneNumber,                                           
    T.DOB,                                           
     isnull(TicketViolationDate,'1/1/1900') as  ViolationDate,                                                   
    isnull(TV.CourtDate,'1/1/1900') as CourtDate ,                  
 T.Race,                                           
    TV.CourtNumber as CourtNumber,                                           
     isnull(TV.RecordID,0) as Recordid,                                     
 PlanId ,                                    
    BondFlag = ( case when tv.courtdate < getdate() then 1 else 0 end ),                            
 isnull(TV.BondAmount,0) as BondAmount  ,                    
 dbo.tblDateType.TypeID  ,                  
    T.CalculatedTotalFee as Fee ,    
 isnull(T.AccidentFlag,0) as AccidentFlag,    
 0 as SchoolFlag,    
 isnull(T.CDLFlag,0) as CDLFlag,
 isnull(t.dlstate,0) as dlstate
                        
 into #temp2                
 from                     
    dbo.tblTickets AS T       
   join dbo.tblTicketsViolations AS TV       
   on t.ticketid_pk = tv.ticketid_pk       
   join  tblcourtviolationstatus tcv      
   on tv.courtviolationstatusid = tcv.courtviolationstatusid      
   Join tbldatetype        
 on tcv.categoryid = tbldatetype.typeid       
 join tblstate S           
   on  S.StateID = isnull(T.StateID_FK,45)            
 join tblviolations vs      
 on tv.violationnumber_pk = vs.violationnumber_pk      
where t.ticketid_pk  = @TicketID      
and ActiveFlag = 0 
and tv.courtviolationstatusid != 80 


select * , 0 as Amount from #temp2                                    
drop table #temp2       
      
End      
                  
  


  

go

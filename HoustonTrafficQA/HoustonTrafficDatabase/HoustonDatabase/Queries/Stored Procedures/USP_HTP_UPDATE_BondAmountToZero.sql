﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 5/18/2009 12:30:20 PM
 ************************************************************/

/****** Object:  StoredProcedure [dbo].[USP_HTP_UPDATE_BondAmountToZero]  ******/     
/*        
 Created By     : Fahad Muhammad Qureshi.
 Created Date   : 05/18/2009  
 TasK		    : 5807        
 Business Logic : This procedure Set the Bond Amount zero for HMC
				
Parameter:       
  @ticketid_pk	: Ticket id      
  
*/ 
CREATE PROCEDURE [dbo].[USP_HTP_UPDATE_BondAmountToZero]
	@ticketid_pk INT
AS
	UPDATE tblTicketsViolations
	SET    BondAmount = 0.00
	WHERE  TicketID_PK = @ticketid_pk
GO


GRANT EXEC ON [dbo].[USP_HTP_UPDATE_BondAmountToZero] TO dbr_webuser
GO  
 
USE TrafficTickets
GO
/******  
* Created By :	Sameeullah Daris.
* Create Date :   2/3/2011 4:22:15 AM
* Task ID :				8882
* Business Logic :  This procedure is used to insert  Records for Attorney Cases .
* List of Parameter :
* @AttorneyCasesFilesID
* @Court
* @ClientBirthDate
* @StatusReason
* @CaseNumber
* @Customer
* @TicketNumber
* @OfficersName
* @CourtDate
* @ClientDLNumber
* @DLType
* @NewId: Output param.      
******/

ALTER PROCEDURE [dbo].[USP_HTP_Insert_AttorneyCases]
			@AttorneyCasesFilesID bigint,
           @Court varchar(300),
           @ClientBirthDate smalldatetime,
           @StatusReason varchar(100),
           @CaseNumber varchar(100),
           @Customer varchar(300),
           @TicketNumber varchar(100),
           @OfficersName varchar(100),           
           @CourtDate smalldatetime,
           @ClientDLNumber varchar(100),
           @DLType varchar(100),
			@NewId INT OUTPUT
AS
	--Sabir Khan 9063 03/30/2011 removed un-neccessory columns
	INSERT INTO [dbo].[OutsideAttorneyCases]
           ([AttorneyCasesFileID]
           ,[Court]
           ,[ClientBirthDate]
           ,[StatusReason]
           ,[CauseNumber]
           ,[Customer]
           ,[TicketNumber]
           ,[OfficersName]          
           ,[CourtDate]
           ,[ClientDLNumber]
           ,[DLType]
           ,[IsProcessed]
           ,[ProcessedDate])
     VALUES
           (@AttorneyCasesFilesID ,
           @Court ,
           @ClientBirthDate ,
           @StatusReason ,
           @CaseNumber ,
           @Customer ,
           @TicketNumber ,
           @OfficersName ,          
           @CourtDate ,
           @ClientDLNumber ,
           @DLType ,
           'FALSE',
           null)
	
	
	
	SET @NewId = SCOPE_IDENTITY()
	SELECT @NewId




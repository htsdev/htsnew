set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

    
/*        
Created By     : Muhammad Ali.
Created Date   : 06/23/2010  
TasK		   : 7747	        
Business Logic  : This procedure Updates Missing Cause Follow Up Date.      
           
Parameter:       
   @TicketID     : Updating criteria with respect to TicketId      
   @FollowUpDate : Date of follow Up which has to be set     
     
      
*/      
--USP_HTP_Update_MissingCauseFollowUpdate 4343,'2010-06-24'
Alter PROCEDURE [dbo].[USP_HTP_Update_MissingCauseFollowUpdate]
(
	@TicketID INT, 
	@FollowUpDate DATETIME
)
AS
BEGIN
	
IF EXISTS(SELECT * FROM tblticketsextensions WHERE Ticketid_pk=@TicketID)
BEGIN
	UPDATE tblticketsextensions
SET
	
	MissingCauseFollowUpdate = @FollowUpDate
	WHERE TicketID_PK=@TicketID
END
ELSE
	BEGIN
		INSERT INTO tblticketsextensions
		(
			Ticketid_pk,
			
			MissingCauseFollowUpdate
		)
		VALUES
		(
			@TicketID,@FollowUpDate
		)
	END

	
END
GO
grant exec on dbo.USP_HTP_Update_MissingCauseFollowUpdate to dbr_webuser 
go 



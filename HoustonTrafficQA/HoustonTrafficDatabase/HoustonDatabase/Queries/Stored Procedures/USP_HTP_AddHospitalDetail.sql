﻿/******    
* Created By :   Saeed Ahmed.  
* Create Date :   11/05/2010   
* Task ID :    8501  
* Business Logic :  This procedure is used to add hosptial detial.  
* List of Parameter : @Name,  
     : @City,  
     : @State,  
     : @ZipCode  
* Column Return :    Auto generated id of the table.   
******/  
alter PROC  dbo.USP_HTP_AddHospitalDetail  
 @name VARCHAR(100),  
 @City VARCHAR(50),  
 @State INT,  
 @ZipCode VARCHAR(10),
 @address VARCHAR(200),
 @id INT OUTPUT  
as  
  
INSERT INTO Hospital  
(  
 [Name],  
 City,  
 [State],  
 ZipCode,
 address  
)  
VALUES  
(  
 @Name,  
 @City,  
 @State,  
 @ZipCode ,
 @address 
)  
  
  
set @id= SCOPE_IDENTITY() 
SELECT @id

go
grant execute on dbo.USP_HTP_AddHospitalDetail to dbr_webuser
go
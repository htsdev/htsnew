/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This procedure returns all criminal case type attorney
* Column returns : 
*			CoveringFirmID
			FirmName
*/

CREATE PROCEDURE [dbo].[USP_HTP_GET_CriminalCase_Attorney]
AS
SELECT DISTINCT ttv.CoveringFirmID,tf.FirmName
FROM   tblTicketsViolations ttv
       INNER JOIN tbltickets tt
            ON  tt.TicketID_PK = ttv.TicketID_PK
       INNER JOIN tblFirm tf
            ON  tf.FirmID = ttv.CoveringFirmID
WHERE  tt.Activeflag = 1
       AND tt.CaseTypeId = 2       
ORDER BY tf.FirmName

GO
 GRANT EXECUTE ON [dbo].[USP_HTP_GET_CriminalCase_Attorney] TO dbr_webuser
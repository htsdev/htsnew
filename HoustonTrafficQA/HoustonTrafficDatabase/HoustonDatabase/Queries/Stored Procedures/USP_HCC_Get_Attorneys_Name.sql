SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HCC_Get_Attorneys_Name]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HCC_Get_Attorneys_Name]
GO



  
    
CREATE procedure USP_HCC_Get_Attorneys_Name    
    
as    
    
Select *  from  tblattorneys  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


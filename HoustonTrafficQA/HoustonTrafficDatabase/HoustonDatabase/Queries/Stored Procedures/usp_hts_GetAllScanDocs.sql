﻿/****** 
ALTER by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get the details of documents that were printed,
				 scanned and uploaded which will be displayed on Documents page under matter section.

List of Parameters:	
	@TicketID : ticke id of the case

List of Columns:
	doc_id :
	docref :
	updatedatetime :
	doc_num :
	resetdesc :
	refcasenumber :
	recordtype :
	imagecount :
	did :
	ocrid :
	docextension :
	abbreviation :
	Events :

******/   
    
ALTER PROCEDURE [dbo].[usp_hts_GetAllScanDocs]
	@TicketID VARCHAR(30)
AS
	DECLARE @temp TABLE (
	            doc_id INT,
	            docref VARCHAR(200),
	            updatedatetime DATETIME,
	            doc_num INT,
	            resetdesc VARCHAR(250),
	            refcasenumber VARCHAR(30),
	            recordtype VARCHAR(10),
	            imagecount INT,
	            did INT,
	            ocrid INT,
	            docextension VARCHAR(60),	-- Abid Ali 5359 1/15/2009 to store letter of rep name                     
	            abbreviation VARCHAR(10),
	            Events VARCHAR(50),
	            -- Noufil 5819 05/07/2009 Subdoctype added
	            subdoctype INT
	        )                      
	
	
	
	
	INSERT INTO @temp
	SELECT DISTINCT sb.ScanBookID AS DOC_ID,
	       (
	           CASE 
	                WHEN ISNULL(sdt.subdoctype, '') = '' THEN dt.DocType
	                ELSE dt.DocType + ' - ' + sdt.subdoctype
	           END
	       ) AS DocType,
	       sb.UPDATEDATETIME,
	       '0' AS doc_num,
	       ResetDesc,
	       ' ' AS RefCaseNumber,
	       '1' AS RecordType,
	       sb.doccount AS ImageCount,
	       0 AS did,
	       ISNULL(sd.DOC_ID, 0) AS ocrid,
	       sp.DocExtension AS DocExtension,
	       u.Abbreviation,
	       Events,
	       -- Noufil 5819 05/07/2009 Subdoctype added
	       ISNULL(sb.SubDocTypeID, -1) AS Subdoctype
	FROM   dbo.tblScanBook sb
	       INNER JOIN dbo.tblDocType dt
	            ON  sb.DocTypeID = dt.DocTypeID
	       INNER JOIN dbo.tblScanPIC sp
	            ON  sb.ScanBookID = sp.ScanBookID
	       INNER JOIN dbo.tblUsers u
	            ON  sb.EmployeeID = u.EmployeeID
	       LEFT OUTER JOIN dbo.tblscandata sd
	            ON  sp.DOCID = sd.docnum
	            AND sb.ScanBookID = sd.doc_id
	       LEFT OUTER JOIN dbo.tblSubDocType sdt
	            ON  sdt.subdoctypeid = sb.subdoctypeid
	WHERE  sb.TicketID = @TicketID
	       AND ISNULL(sb.isdeleted, 0) = 0 
	
	
	-- Insert into #ScanDoc                                    
	INSERT INTO @temp
	SELECT N.recordid AS DOC_ID,
	       L.lettername AS docref,
	       N.Printdate AS UPDATEDATETIME,
	       '-1' AS doc_num,
	       N.note AS ResetDesc,
	       ' ' AS RefCaseNumber,
	       '0' AS RecordType,
	       N.PageCount AS ImageCount,
	       0 AS did,
	       0 AS ocrid,
	       '000' AS DocExtension,
	       dbo.tblUsers.Abbreviation AS Abbreviation,	-- ref bug id 1428                             
	       'Print' AS Events,
	       -- Noufil 5819 05/07/2009 Subdoctype added
	       -1 AS Subdoctype
	FROM   dbo.tblHTSNotes N
	       INNER JOIN dbo.tblHTSLetters L
	            ON  N.LetterID_FK = L.LetterID_pk
	       INNER JOIN dbo.tblUsers
	            ON  N.EmpID = dbo.tblUsers.EmployeeID
	WHERE  (N.TicketID_FK = @TicketID) 
	
	--ozair 3643 on 05/12/2008 getting records from Document Tracking 
	INSERT INTO @temp
	SELECT bt.batchid_fk AS DOC_ID,
	       d.documentname AS docref,
	       bt.verifieddate AS UPDATEDATETIME,
	       '-2' AS doc_num,
	       d.documentname + ' Scanned ID: ' + CONVERT(VARCHAR, bt.batchid_fk) AS ResetDesc,
	       ' ' AS RefCaseNumber,
	       '2' AS RecordType,
	       COUNT(sbd.scanbatchdetailid) AS ImageCount,
	       sbd.scanbatchid_fk AS did,
	       0 AS ocrid,
	       '000' AS DocExtension,
	       u.abbreviation AS Abbreviation,
	       'Scan' AS Events,
	       -- Noufil 5819 05/07/2009 Subdoctype added
	       -1 AS Subdoctype
	FROM   tbl_htp_documenttracking_batch b
	       INNER JOIN tbl_htp_documenttracking_batchtickets bt
	            ON  b.batchid = bt.batchid_fk
	       INNER JOIN tbl_htp_documenttracking_scanbatch_detail sbd
	            ON  bt.batchid_fk = sbd.documentbatchid
	       INNER JOIN tbl_htp_documenttracking_documents d
	            ON  b.documentid_fk = d.documentid
	       INNER JOIN tblusers u
	            ON  sbd.employeeid = u.employeeid
	WHERE  bt.ticketid_fk = @TicketID
	       AND bt.verifieddate IS NOT NULL
	GROUP BY
	       sbd.scanbatchid_fk,
	       bt.batchid_fk,
	       d.documentname,
	       u.abbreviation,
	       bt.verifieddate
	--end ozair 3643
	
	-- Abid Ali 5359 1/16/2009 Letter of Rep Certified Mail Confirmation
	INSERT INTO @temp
	SELECT DISTINCT dbo.tblLORBatchHistory.BatchID_FK AS DOC_ID,
	       'Letter of Rep' AS docref,
	       dbo.tblHTSBatchPrintLetter.PrintDate AS UPDATEDATETIME,
	       '-2' AS doc_num,
	       'Batch Print' AS ResetDesc,
	       ' ' AS RefCaseNumber,
	       '3' AS RecordType,
	       1 AS ImageCount,
	       0 AS did,
	       0 AS ocrid,
	       dbo.tblHTSBatchPrintLetter.USPSTrackingNumber + '_Printed.pdf' AS 
	       DocExtension,
	       printUser.abbreviation AS Abbreviation,
	       'Print' AS Events,
	       -- Noufil 5819 05/07/2009 Subdoctype added
	       -1 AS Subdoctype
	FROM   dbo.tblLORBatchHistory
	       INNER JOIN dbo.tblHTSBatchPrintLetter
	            ON  dbo.tblLORBatchHistory.BatchID_FK = dbo.tblHTSBatchPrintLetter.BatchID_PK
	       LEFT OUTER JOIN tblUsers printUser
	            ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = printUser.EmployeeID
	WHERE  dbo.tblLORBatchHistory.TicketID_PK = @TicketID
	       --Ozair 5359 02/13/2009 included isnull check
	       -- abid 5563 02/21/2009 removed because printed should show in doc scan
	       --AND isnull(dbo.tblHTSBatchPrintLetter.EDeliveryFlag,0) = 0
	       AND dbo.tblHTSBatchPrintLetter.IsPrinted = 1
	
	INSERT INTO @temp
	SELECT DISTINCT dbo.tblLORBatchHistory.BatchID_FK AS DOC_ID,
	       'LOR Confirmation' AS docref,
	       -- abid 5563 02/21/2009 proper date of confirmation
	       dbo.tblHTSBatchPrintLetter.USPSConfirmDate AS UPDATEDATETIME,
	       '-2' AS doc_num,
	       'Letter of Rep confirmed' AS ResetDesc,
	       ' ' AS RefCaseNumber,
	       '3' AS RecordType,
	       1 AS ImageCount,
	       0 AS did,
	       0 AS ocrid,
	       dbo.tblHTSBatchPrintLetter.USPSTrackingNumber + '_Confirmed.pdf' AS 
	       DocExtension,
	       printUser.abbreviation AS Abbreviation,
	       'Certified Mail' AS Events,
	       -- Noufil 5819 05/07/2009 Subdoctype added
	       -1 AS Subdoctype
	FROM   dbo.tblLORBatchHistory
	       INNER JOIN dbo.tblHTSBatchPrintLetter
	            ON  dbo.tblLORBatchHistory.BatchID_FK = dbo.tblHTSBatchPrintLetter.BatchID_PK
	       LEFT OUTER JOIN tblUsers printUser
	            ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = printUser.EmployeeID
	WHERE  dbo.tblLORBatchHistory.TicketID_PK = @TicketID
	       --ozair 5665 03/27/2009
	       AND dbo.tblHTSBatchPrintLetter.USPSResponse IS NOT NULL
	       AND dbo.tblHTSBatchPrintLetter.EDeliveryFlag = 1
	       AND dbo.tblHTSBatchPrintLetter.IsPrinted = 1
	
	--Ozair 8039 07/20/2010 Arr/Bond Summary Reports added in Documents history
	INSERT INTO @temp
	SELECT bt.batchid_fk AS DOC_ID,
	       d.documentname AS docref,
	       b.BatchDate AS UPDATEDATETIME,
	       '-2' AS doc_num,
	       d.documentname + ' Summary DOC ID: ' + CONVERT(VARCHAR, bt.batchid_fk) AS ResetDesc,
	       ' ' AS RefCaseNumber,
	       '4' AS RecordType,
	       1 AS ImageCount,
	       -1 AS did,
	       0 AS ocrid,
	       '000' AS DocExtension,
	       u.abbreviation AS Abbreviation,
	       'Print' AS Events,
	       -1 AS Subdoctype
	FROM   tbl_htp_documenttracking_batch b
	       INNER JOIN tbl_htp_documenttracking_batchtickets bt
	            ON  b.batchid = bt.batchid_fk
	       INNER JOIN tbl_htp_documenttracking_documents d
	            ON  b.documentid_fk = d.documentid
	       INNER JOIN tblusers u
	            ON  b.employeeid = u.employeeid
	WHERE  bt.ticketid_fk = @TicketID
	GROUP BY
	       bt.batchid_fk,
	       d.documentname,
	       u.abbreviation,
	       b.BatchDate
	
	SELECT *
	FROM   @temp
	ORDER BY
	       UPDATEDATETIME DESC,
	       docref                                
                                    
          
  
   
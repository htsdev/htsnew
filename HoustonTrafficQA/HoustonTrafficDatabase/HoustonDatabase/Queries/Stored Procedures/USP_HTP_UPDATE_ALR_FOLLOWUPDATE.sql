/**
* Created By : Noufil Khan 05/15/2209
* Business Logic : This procedure Updates follow-up date for the following ALR Alerts. and then insert history notes. FOllowing are the alert type
*					5.  ALR Hearing Request
*					6.  ALR DPS Receipt
*  				    7.  ALR Hearing Notification
*  				    8.  ALR Discovery
*  				    9.  ALR Subpoena Application
*  				    10. ALR Subpoena Pick up Alert
*  				    11. ALR Subpoena Return
*  				    12. ALR Hearing Disposition
*  				    14. ALR Request for Appearance of Witness
*  				    15. ALR Continuance Request(Mandatory)
*  				    17. ALR Continuance Request(Discretionary)
**/

--  [dbo].[USP_HTP_UPDATE_ALR_FOLLOWUPDATE] 13446, 20, 4058,'02/03/2010'

alter  PROCEDURE [dbo].[USP_HTP_UPDATE_ALR_FOLLOWUPDATE]
@ticketid INT,
@SubDocTypeID INT,
@empid INT,
@followupdate DATETIME

AS

-- tahir 7335 01/28/2010 code optimization.....

DECLARE @Subdocname VARCHAR(50)
SELECT @Subdocname = tsdt.SubDocType FROM tblSubDocType tsdt WHERE tsdt.SubDocTypeID= @SubDocTypeID

DECLARE @SqlRes VARCHAR(10)
 
IF @SubDocTypeID = 5
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRHearingFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRHearingFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 6
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRDPSReceiptFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRDPSReceiptFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 7
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRHearingNtfFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRHearingNtfFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 8
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRDiscoveryFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRDiscoveryFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 9
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRSubpAppFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRSubpAppFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 10
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRSubpAlertFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRSubpAlertFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 11
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRSubpRetFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	-- tahir 7335 01/28/2010 fixed deadlock issue.....
	--Update tblTickets SET ALRSubpRetFollowUpDate = @followupdate WHERE TicketID_PK= TicketID_PK	
	Update tblTickets SET ALRSubpRetFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 12
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRHearingDispFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRHearingDispFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 14
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRRAWFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRRAWFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 15
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRContReqMFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRContReqMFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END

ELSE IF @SubDocTypeID = 17
BEGIN
	select @SqlRes =  CONVERT (VARCHAR(10),ALRContReqDFollowUpDate,101) FROM tblTickets WHERE TicketID_PK = @ticketid
	Update tblTickets SET ALRContReqDFollowUpDate = @followupdate WHERE TicketID_PK= @ticketid	
END


IF DATEDIFF(DAY,ISNULL(@SqlRes,'01/01/1900'),@followupdate) <> 0
BEGIN
	INSERT INTO tblTicketsNotes (	TicketID, 	[Subject],	Notes,	Recdate,	EmployeeID	)
	values 	( 
			@ticketid,
			isnull(@Subdocname,'ALR') + ' Follow-up date has been changed from ' + isnull(@SqlRes,'N/A') + ' to ' + CONVERT(VARCHAR(10),@followupdate,101),
			'', 	GETDATE(), 	@empid
			)	
END

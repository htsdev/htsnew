USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_GetCaseinfo_Fax]    Script Date: 11/20/2009 19:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_HTS_GetLetterOfRep 124164
/*  
Created By : Noufil Khan  
Business Logic : This procedure return information about client regarding fax
Parameter :  @ticketid  display records with respect to ticket id entered
Column :firstname,lastname,refcasenumber,email,fax

*/
--[USP_HTS_GetCaseinfo_Fax] 61982,3991
ALTER procedure [dbo].[USP_HTS_GetCaseinfo_Fax]
@ticketid int ,
@empid int  

as

select Top 1    
	 tc.firstname as firstname,
	 tc.lastname as lastname, 
	 tv.refcasenumber, 
	 t.email,  	 
	 C.fax as FAXNO
	  
from tblusers t  
	 INNER JOIN tblTicketsViolations tv   
	  ON tv.VEmployeeID = t.employeeid
	 INNER JOIN tblcourts C  
	  On C.Courtid = tv.CourtID 	 
	 inner join tbltickets tc
	  on tv.ticketid_pk=tc.ticketid_pk

Where tv.TicketID_PK=@ticketid


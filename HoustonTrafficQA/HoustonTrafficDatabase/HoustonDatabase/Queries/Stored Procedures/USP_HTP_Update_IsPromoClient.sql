﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Update_IsPromoClient]    Script Date: 11/26/2013 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Sabir Khan 11460 10/01/2013: this procedure is used to update promo field for promo clients.
*******/

ALTER PROCEDURE [dbo].[USP_HTP_Update_IsPromoClient]
@LetterType INT
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @temp TABLE(RecordID int, CauseNumber VARCHAR(30), MidNumber VARCHAR(30))

INSERT INTO @temp(RecordID,CauseNumber,MidNumber)
SELECT ta.RecordID, tva.CauseNumber, ta.MidNumber 
FROM tblTicketsArchive ta INNER JOIN tblTicketsViolationsArchive tva ON ta.RecordID = tva.RecordID
INNER JOIN HMCCurrentMailerRecords h ON h.midnumber = ta.MidNumber

DECLARE @tempEvent TABLE(CauseNumber VARCHAR(30), InsertDate DATETIME, attorneyname VARCHAR(100), barnumber VARCHAR(100))

INSERT INTO @tempEvent(CauseNumber,InsertDate,attorneyname,barnumber)
SELECT t.causenumber,t.InsertDate,t.attorneyname,t.barnumber
FROM LoaderFilesArchive.dbo.tblEventExtractTemp t 
inner join @temp tm on tm.CauseNumber = replace(t.causenumber,' ','') 
WHERE DATEDIFF(DAY,t.InsertDate,DATEADD(YEAR,-3,GETDATE()))<=0
AND (LEN(ISNULL(t.attorneyname,''))>0 OR LEN(ISNULL(t.barnumber,''))>0)


-- Sabir Khan 11560 11/26/2013 new promo logic has been implemented.

if((select count(*) from @tempEvent)>0)
begin
	UPDATE h SET h.IsPromo = 1, h.PromoAttorney = te.attorneyname,h.PromoAttorneyBarCard = te.barnumber 
	FROM HMCCurrentMailerRecords h INNER JOIN @temp t ON h.midnumber = t.midnumber	
	INNER JOIN @tempEvent te ON REPLACE(te.causenumber,' ','') = t.CauseNumber 
	AND te.attorneyname NOT LIKE '%SULLO%'
	AND te.barnumber NOT LIKE '24026218'
	WHERE  (LEN(ISNULL(te.attorneyname,''))>0 OR LEN(ISNULL(te.barnumber,''))>0)
	AND DATEDIFF(DAY,te.InsertDate,DATEADD(YEAR,-3,GETDATE()))<=0 
end


update t set t.PromoID = a.PromoID,t.PromoMessage = a.PromoMessageForLatter 
from HMCCurrentMailerRecords t 
inner join AttorneyPromoTemplates a on a.MailerID = @LetterType 
AND (a.AttorneyName = t.PromoAttorney or a.AttorneyBarCode = t.PromoAttorneyBarCard) 
AND (a.AttorneyName in ('WILLIAM CITIZEN','Lennon Prince')) and t.IsPromo = 1


update t set t.PromoID = a.PromoID,t.PromoMessage = a.PromoMessageForLatter 
from HMCCurrentMailerRecords t 
inner join AttorneyPromoTemplates a on a.MailerID = @LetterType 
AND (a.AttorneyName = t.PromoAttorney or a.AttorneyBarCode = t.PromoAttorneyBarCard) 
AND (a.AttorneyName in ('Paul Kubosh','Raymond Coldren','Arturo Fuentes')) 
and t.IsPromo = 1 
AND t.PromoMessage IS NULL
AND t.ViolCount = a.ViolationCount

update t set t.PromoID = a.PromoID,t.PromoMessage = a.PromoMessageForLatter 
from HMCCurrentMailerRecords t 
inner join AttorneyPromoTemplates a on a.MailerID = @LetterType 
AND (a.AttorneyName in ('Paul Kubosh')) 
and t.IsPromo = 1 
AND t.PromoMessage IS NULL
AND t.ViolCount = a.ViolationCount

END





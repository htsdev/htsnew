SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DELETE_TBLSPNAME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DELETE_TBLSPNAME]
GO

create procedure [dbo].[USP_HTS_DELETE_TBLSPNAME]

@rptid int

AS

delete from tbl_sp_name
where rptid = @rptid
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- Noufil 4487 08/05/2008 courtlocation field added
--[usp_HTS_Get_All_Quotes] 1,-1
ALTER PROCEDURE [dbo].[usp_HTS_Get_All_Quotes]
 (          
 @TicketID_PK varchar(12),
 @casetypeid int    -- casetype field added to filter record according to casetype       
 )          
AS          
          
SELECT DISTINCT           
			 q.QuoteID,           
			 CONVERT(varchar(12), q.LastContactDate, 101) AS LastContactDate,           
			 CONVERT(Int, GETDATE() - q.LastContactDate) AS Days,           			
			 t.Lastname + ', ' + t.Firstname AS Customer,           			
			 CONVERT(decimal(8, 0), t.calculatedtotalfee) AS calculatedtotalfee,           			
			 r1.QuoteResultDescription,           
			 q.FollowUpYN,           
			 t.TicketID_PK,           
			 REPLACE(r2.QuoteResultDescription, '---------------------', ' ') AS FollowUpStatus,           
			 CONVERT(varchar(12), tv.CourtDateMain, 101) AS CourtDate,           
			 q.CallBackDate,           
			 q.AppointmentDate,           
			 q.CallBackTime,           
			 q.AppointmentTime,t.casetypeid,
			 c.shortname as crt      
	FROM tblTicketsViolations tv           
		RIGHT OUTER JOIN dbo.tblTickets t ON  tv.TicketID_PK = t.TicketID_PK           
					AND     tv.CourtDateMain =           
											(          
											  SELECT MAX(b.CourtDateMain)          
											  FROM tblTicketsViolations b          
											  WHERE   b.TicketID_PK = t.TicketID_PK          
											)          
		RIGHT OUTER JOIN tblViolationQuote q           
				LEFT OUTER JOIN tblQuoteResult r2 ON  q.FollowUPID = r2.QuoteResultID           
					ON  t.TicketID_PK = q.TicketID_FK           
		LEFT OUTER JOIN tblUsers u ON  t.EmployeeIDUpdate = u.EmployeeID           
		LEFT OUTER JOIN tblQuoteResult r1 ON  q.QuoteResultID = r1.QuoteResultID
		inner join dbo.tblCourts c on tv.CourtID = c.Courtid

	WHERE t.Activeflag = 0 AND  q.FollowUpYN = 1 And  ((@casetypeid=-1 and 1=1) or (@casetypeid<> -1 and t.casetypeid= @casetypeid))         
   
order by [customer]  



/****************

Alter by:	Tahir Ahmed
Date:		10/08/2008

Business Logic:
	The stored procedure is used by Houtson Traffic Program to get the amount
	for different types of additional fee.

Input Parameters:
	@SpeedingID:	
	@AccFlag:
	@CDLFlag:
	@LateFlag:
	@HasPaymentPlan:

Output Columns:
	SpeedingAmount:
	AccAmount:
	CDLAmount:
	LateFee:
	PlanAmount:

***************/ 


alter procedure [dbo].[USP_HTS_GET_AdditionalAmount]  
 (  
 @SpeedingID int,  
 @AccFlag bit,  
 @CDLFlag bit,
  -- Added by Zeeshan to Implement Late Fee
 @LateFlag bit  ,
@HasPaymentPlan bit = 0 -- tahir 4786 10/08/2008

 )  
  
as   
  
declare @SpeedingAmount money,  
 @AccAmount money,  
 @CDLAmount money,  
 -- Added by Zeeshan to Implement Late Fee
 @LateFee money,
@PlanAmount MONEY, -- tahir 4786 10/08/2008
@DayOfArrAmount MONEY
   
select  @SpeedingAmount=isnull(amount,0)   
from  tblspeeding   
where speedingid_pk = @speedingid  
and  typeid=1  
  
select  @AccAmount = isnull(amount,0)  
from tblspeeding  
where description like 'AccidentFlag'  
and  typeid = 2  
  
select  @CDLAmount = isnull(amount,0)  
from tblspeeding  
where description like 'CDLFlag'  
and  typeid = 2  
  
select  @LateFee = isnull(amount,0)  
from tblspeeding  
where description like 'Late Fee'  
and  typeid = 2  

-- tahir 4786 10/08/2008
select @planamount = isnull(amount, 0)
from tblspeeding where description like 'Payment plan charges'
and typeid = 2

-- Rab Nawaz Khan 10330 06/29/2012
select @DayOfArrAmount = isnull(amount, 0)
from tblspeeding where description like 'Day Of Arr'
and typeid = 2
-- End 10330

select	isnull(@SpeedingAmount,0)  as SpeedingAmount , 
		isnull(@AccAmount,0) as AccAmount,  
		isnull( @CDLAmount, 0) as CDLAmount  , 
		isnull( @LateFee, 0) as LateFee,
		isnull( @planamount, 0) as PlanAmount,
		isnull( @DayOfArrAmount, 0) as DayOfArrAmount

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


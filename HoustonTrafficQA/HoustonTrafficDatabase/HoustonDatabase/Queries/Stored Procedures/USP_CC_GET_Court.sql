set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*********

Created By		: Zeeshan Ahmed

Business Logic	: This Procedure is used to get list of child custody courts.

List Of Columns :

		ChildCustodyCourtId 
		CourtName
		WebsitreCourtid

*********/
Create PROCEDURE [dbo].[USP_CC_GET_Court]
AS


SELECT * FROM [dbo].[ChildCustodyCourt]
Order by ChildCustodyCourtid desc 



go

grant execute on [dbo].[USP_CC_GET_Court] to dbr_webuser

go
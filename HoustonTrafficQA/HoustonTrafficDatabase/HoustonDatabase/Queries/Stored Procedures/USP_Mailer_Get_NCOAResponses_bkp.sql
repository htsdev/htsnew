SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_NCOAResponses_bkp]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_NCOAResponses_bkp]
GO

-- USP_Mailer_Get_NCOAResponses '8/1/2007', '8/30/07', 0, 0
create procedure [dbo].[USP_Mailer_Get_NCOAResponses_bkp]
	(
	@sDate datetime,
	@eDate datetime,
	@Querytype int,
	@lettertype int
	)

as

--declare 	@sDate datetime,
--	@eDate datetime,
--	@Querytype int,
--	@lettertype int
--
--select @sDate = '8/1/07', @edate = '8/28/07', @querytype = 0 , @lettertype = 9

create table #temp    (recordid int, casetype int, processdate datetime)
create table #letter  (lettertype int, noteid int, mailerdate datetime, recordid int, casetype int)
create table #letter2 (lettertype int, noteid int, mailerdate datetime, recordid int, casetype int)
create table #clients (ticketid int, mailerid int, mailerdate datetime, lettertype int, activeflag int, lettername varchar(200), revenue money, casetype int)
create table #payments(ticketid int, hiredate datetime , revenue money, casetype int)

if @querytype = 0
	begin
		insert into #temp ( recordid , casetype, processdate )
		select distinct  t.recordid, 1, convert(varchar(10), t.accuzipprocessdate,101)
		from tblticketsarchive t 
		where ncoa18flag = 1
		and datediff(day, t.accuzipprocessdate, @sdate)<=0 
		and datediff(day, t.accuzipprocessdate, @edate)>=0

		insert into #temp ( recordid , casetype, processdate )
		select distinct  t.recordid, 2, convert(varchar(10), t.accuzipprocessdate,101)
		from dallastraffictickets.dbo.tblticketsarchive t 
		where ncoa18flag = 1
		and datediff(day, t.accuzipprocessdate, @sdate)<=0 
		and datediff(day, t.accuzipprocessdate, @edate)>=0

		insert into #temp ( recordid , casetype, processdate )
		select distinct  t.bookingnumber, 3, convert(varchar(10), t.accuzipprocessdate,101)
		from jims.dbo.criminalcases  t
		where ncoa18flag = 1
		and datediff(day, t.accuzipprocessdate, @sdate)<=0 
		and datediff(day, t.accuzipprocessdate, @edate)>=0
	end
else
	begin
		insert into #temp ( recordid , casetype, processdate )
		select distinct  t.recordid, 1, convert(varchar(10), t.accuzipprocessdate,101)
		from tblticketsarchive t 
		where ncoa48flag = 1
		and datediff(day, t.accuzipprocessdate, @sdate)<=0 
		and datediff(day, t.accuzipprocessdate, @edate)>=0

		insert into #temp ( recordid , casetype, processdate )
		select distinct  t.recordid, 2, convert(varchar(10), t.accuzipprocessdate,101)
		from dallastraffictickets.dbo.tblticketsarchive t 
		where ncoa48flag = 1
		and datediff(day, t.accuzipprocessdate, @sdate)<=0 
		and datediff(day, t.accuzipprocessdate, @edate)>=0

		insert into #temp ( recordid , casetype, processdate )
		select distinct  t.bookingnumber, 3, convert(varchar(10), t.accuzipprocessdate,101)
		from jims.dbo.criminalcases  t
		where ncoa48flag = 1
		and datediff(day, t.accuzipprocessdate, @sdate)<=0 
		and datediff(day, t.accuzipprocessdate, @edate)>=0
	end

--select * from #temp

if @lettertype = 0
	begin
		insert into #letter (lettertype , noteid , mailerdate , recordid ,  casetype )
		select distinct n.lettertype, n.noteid, 
			convert(Varchar(10), b.batchsentdate,101), n.recordid,
			case when b.courtid in (6,11) then 2
				 when b.courtid = 8 then 3
				else 1 
			end
		from tblletternotes n, tblbatchletter b
		where b.batchid = n.batchid_fk 
		union
		select distinct n.lettertype, n.noteid, 
			convert(Varchar(10), b.batchsentdate,101), d.recordid,
			case when b.courtid in (6,11) then 2
				 when b.courtid = 8 then 3
				else 1 
			end
		from tblletternotes n, tblbatchletter b, tblletternotes_detail d
		where b.batchid = n.batchid_fk 
		and d.noteid = n.noteid
	end
else
	begin
		insert into #letter (lettertype , noteid , mailerdate , recordid ,  casetype )
		select distinct n.lettertype, n.noteid, 
			convert(Varchar(10), b.batchsentdate,101), n.recordid,
			case when b.courtid in (6,11) then 2
				 when b.courtid = 8 then 3
				else 1 
			end
		from tblletternotes n, tblbatchletter b
		where b.batchid = n.batchid_fk and n.lettertype = @lettertype
		union
		select distinct n.lettertype, n.noteid, 
			convert(Varchar(10), b.batchsentdate,101), d.recordid,
			case when b.courtid in (6,11) then 2
				 when b.courtid = 8 then 3
				else 1 
			end
		from tblletternotes n, tblbatchletter b, tblletternotes_detail d
		where b.batchid = n.batchid_fk 
		and d.noteid = n.noteid and n.lettertype = @lettertype
	end


-- select *  from #letter

insert into #letter2 (lettertype , noteid , mailerdate , recordid ,  casetype )
select distinct  l.lettertype, l.noteid, a.processdate, l.recordid, l.casetype
from #letter l , #temp a
where l.recordid = a.recordid and l.casetype = a.casetype 
and datediff(minute, l.mailerdate , a.processdate ) <= 0
		
-- select * from #letter2

insert into #payments (ticketid, hiredate, revenue, casetype)
select p.ticketid, min(p.recdate), isnull(sum(isnull(chargeamount,0)),0), 1
from tbltickets t, tblticketspayment p
where t.ticketid_pk = p.ticketid
and t.activeflag = 1
and p.paymentvoid = 0
and t.ticketid_pk in (select ticketid_pk from tblticketsviolations where courtid <> 37)
group by p.ticketid
	
insert into #payments (ticketid, hiredate, revenue, casetype)
select p.ticketid, min(p.recdate), isnull(sum(isnull(chargeamount,0)),0), 2
from dallastraffictickets.dbo.tbltickets t, dallastraffictickets.dbo.tblticketspayment p
where t.ticketid_pk = p.ticketid
and t.activeflag = 1
and p.paymentvoid = 0
group by p.ticketid

insert into #payments (ticketid, hiredate, revenue, casetype)
select p.ticketid, min(p.recdate), isnull(sum(isnull(chargeamount,0)),0), 3
from tbltickets t, tblticketspayment p
where t.ticketid_pk = p.ticketid
and t.activeflag = 1
and p.paymentvoid = 0
and t.ticketid_pk in (select ticketid_pk from tblticketsviolations where courtid = 3037)
group by p.ticketid


select distinct noteid, lettertype, mailerdate, casetype
into #tmp
from #letter2

insert into #clients (ticketid, mailerid, lettertype, activeflag, mailerdate, casetype)
select distinct t.ticketid_pk, a.noteid,  a.lettertype, t.activeflag, a.mailerdate, a.casetype
from #tmp a left outer join  tbltickets t on a.noteid =  t.mailerid
where a.casetype <> 2

--select * from #clients where activeflag = 1


insert into #clients (ticketid, mailerid, lettertype, activeflag, mailerdate, casetype)
select distinct t.ticketid_pk, a.noteid,  a.lettertype, t.activeflag, a.mailerdate, a.casetype
from #tmp a left outer join  dallastraffictickets.dbo.tbltickets t
on  a.noteid = t.mailerid
where a.casetype = 2

update c
set c.revenue = p.revenue
from #clients c, #payments p
where p.ticketid = c.ticketid
and p.casetype = c.casetype

update c
set c.lettername = a.courtcategoryname + '-'+ l.lettername
from #clients c, tblcourtcategories a, tblletter l
where c.lettertype = l.letterid_pk 
and l.courtcategory = a.courtcategorynum

select distinct mailerid, lettertype, lettername, activeflag, mailerdate,casetype,revenue, ticketid
into #clients2
from #clients

--select * from #clients2 where activeflag =1
select lettername,
		mailerdate, 
		count(distinct mailerid) as lettercount,
		isnull(sum(case activeflag when 0 then 1 end),0) as Quotes,
		isnull(sum(case activeflag when 1 then 1 end),0) as Hires,
		isnull(sum(isnull(revenue,0)),0) as revenue
into #temp2
from #clients2
group by lettername, mailerdate
order by lettername, mailerdate

select lettername
, mailerdate
, lettercount, quotes, hires, revenue ,
	case when lettername like 'Dallas%' then lettercount*.5 
		else case when lettercount <500 then lettercount*.39 
				else lettercount * .30 end
	end as expense
into #temp3
from #temp2

select lettername, mailerdate, 
lettercount, quotes, hires, revenue, expense, revenue - expense as profit
from #temp3 
order by lettername, mailerdate desc

drop table #temp
drop table #letter
drop table #letter2
drop table #clients
drop table #clients2
drop table #temp2
drop table #payments
drop table #temp3
drop table #tmp



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


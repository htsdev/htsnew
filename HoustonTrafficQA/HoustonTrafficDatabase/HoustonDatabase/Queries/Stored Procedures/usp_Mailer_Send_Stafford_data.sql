set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
/****** 

Business Logic:	The procedure is used by LMS application to get data for Stafford Arraignment letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@BondFlag:		Flag that identifies if searching for Arraignment letters or for appearance.
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Abb:			Short abbreviation of employee who is printing the letter
	Language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	ChkGroup:		Recordid plus language flag for grouping of records on the report file.

*******/





-- usp_Mailer_Send_Stafford_data 12,32,12, '6/13/2007,' , 3992, 0 , 0, 0
      
ALTER proc [dbo].[usp_Mailer_Send_Stafford_data]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int ,
@isprinted bit                                
                                    
as                                    

                                          
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
declare @sqlquery varchar(MAX),
	@sqlquery2 varchar(MAX), 
	@lettername varchar(50) -- Rab Nawaz Khan 9682 10/04/2011 Added to make the single template file for Appearance mailers 
SET @lettername = 'ARRAIGNMENT'  
        
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

Select @sqlquery =''  , @sqlquery2 = ''

declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                    
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid,' 
if @searchtype = 0
	set @sqlquery = @sqlquery + '
	convert(varchar(10),ta.listdate,101) as listdate, '
else
	set @sqlquery = @sqlquery + '
	convert(varchar(10),tva.courtdate,101) as listdate, '

set @sqlquery = @sqlquery + '
count(tva.ViolationNumber_PK) as ViolCount,
sum(isnull(tva.fineamount,0))as Total_Fineamount 
into #temp2                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 

INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 
where  	Flag1 in (''Y'',''D'',''S'')
and 	donotmailflag = 0 
and 	ta.clientflag=0  
--Sabir Khan 7483 03/12/2010 Using bond date.
and tva.bonddate is null 
and isnull(ta.ncoa48flag,0) = 0
--Adil 8349 10/30/2010 Only Appearance and furture court date cases..
--Only Appearance status
and tcvs.CategoryID=2
-- only future court date
and datediff(day, tva.courtdate, getdate()) < 0 

'               
          
if(@crtid=12 )              
	set @sqlquery=@sqlquery+' 
and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = 12 )'                  


if @crtid <> 12             
	set @sqlquery =@sqlquery +'                                  
and 	tva.courtlocation =  ' + convert(varchar(10),@crtid)                                                                  
                                  

if @searchtype = 0
begin
	if(@startListdate<>'')                                    
		set @sqlquery=@sqlquery+' 
		--Rab Nawaz Khan 9180 05/17/2011 mailer letters total printable count difference problem fixed 
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 
end
else
begin
	if(@startListdate<>'')                                    
		set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.courtdate' ) 
end

                                    
if(@officernum<>'')                                    
begin                                    
set @sqlquery =@sqlquery +                                     
            '  
and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
end                                    
                                    
if(@ticket_no<>'')                                    
set @sqlquery=@sqlquery+ '
and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                          
if(@zipcode<>'')                                                          
set @sqlquery=@sqlquery+ ' 
and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'               if(@violadesc<>'')              
set @sqlquery=@sqlquery+' 
and	('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'           if(@fineamount<>0 and @fineamountOpr<>'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end    

if(@fineamount<>0 and @fineamountOpr  = 'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end    set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (                                                                    
	select	recordid from tblletternotes 
	where 	lettertype ='+convert(varchar(10),@lettertype)+'   
	)     
group by  
	ta.recordid, '

if @searchtype = 0
	set @sqlquery = @sqlquery + '
	convert(varchar(10),ta.listdate,101)  '
else
	set @sqlquery = @sqlquery + '
	convert(varchar(10),tva.courtdate,101)  '

                            


set @sqlquery=@sqlquery+'                                    

Select	distinct ta.recordid, 
	a.listdate,
	ta.courtdate,
	tva.courtlocation courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	tva.TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	s.state,
	c.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	tva.ticketviolationdate as violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
	-- case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount, 
	isnull(tva.fineamount,0) as FineAmount, 
	a.ViolCount,
	a.Total_Fineamount 
into #temp                                          
from tblticketsarchive ta, tblticketsviolationsarchive tva, #temp2 a, tblcourts c, tblstate s
where ta.recordid = tva.recordid and ta.stateid_fk = s.stateid
and ta.recordid = a.recordid
and tva.courtlocation = c.courtid
--Adil 8349 10/29/2010 Dispose cases should not mail OUT.
and tva.violationstatusid <> 80
'
                                         
set @sqlquery=@sqlquery+'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, violationdate into #temp1  from #temp                                    
 '              
if(@singleviolation<>0 and @doubleviolation=0)              
begin              
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                    
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)              
         or violcount>3                                            
      '              
end              
              
                   
if(@doubleviolation<>0 and @singleviolation=0 )              
begin              
set @sqlquery=@sqlquery+                     
'Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, listdate as mdate,FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, violationdate into #temp1  from #temp                                    
 '              
set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+              
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                    
      or violcount>3  '                                  
end              
              
if(@doubleviolation<>0 and @singleviolation<>0)              
begin              
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                    
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)              
         or   '              
set @sqlquery =@sqlquery +  @doublevoilationOpr+                                     
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                    
      or violcount>3  '                
              
end              
              
 set @sqlquery2 =@sqlquery2 +                                    
'
-- Rab Nawaz Khan 9704 09/28/2011 added to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 

 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1   WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid   
 
'
 
         
if( @printtype<>0)         
set @sqlquery2 =@sqlquery2 +                                    
'
	Declare @ListdateVal DateTime,                                  
		@totalrecs int,                                
		@count int,                                
		@p_EachLetter money,                              
		@recordid varchar(50),                              
		@zipcode   varchar(12),                              
		@maxbatch int  ,
		@lCourtId int  ,
		@dptwo varchar(10),
		@dpc varchar(10)

	declare @tempBatchIDs table (batchid int)
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(*) from #temp1                                

	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                
                              
	
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                   

			Select @maxbatch=Max(BatchId) from tblBatchLetter 
			insert into @tempBatchIDs select @maxbatch                                                     
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            

	close ListdateCur  
	deallocate ListdateCur 

	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,  
	 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
	 dptwo, TicketNumber_PK, t.zipcode, t.midnumber, violationdate as courtdate, zipmid,'''+@user+''' as Abb, isfineamount,
	 '''+@lettername +''' as lettername, 0 as promotemplate, t.courtid, t.midnumber as midnum 
	 from #temp1 t , tblletternotes n, @tempBatchIDs tb
	 where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
	 order by [zipmid]

declare @lettercount int, @subject varchar(200), @body varchar(400) ,  @sql varchar(500)
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
select @subject = convert(varchar(20), @lettercount) +  '' LMS Stafford ARR Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'

else
	
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc, 
	 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  	 dptwo, TicketNumber_PK, zipcode  , midnumber, violationdate as courtdate, zipmid, '''+@user+''' as Abb  , isfineamount,
	 '''+@lettername +''' as lettername, 0 as promotemplate, courtid, midnumber as midnum 
	 from #temp1 
	 order by [zipmid] 
 '

set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'
                                    
--print @sqlquery + @sqlquery2
set @sqlquery = @sqlquery + @sqlquery2
exec (@sqlquery)




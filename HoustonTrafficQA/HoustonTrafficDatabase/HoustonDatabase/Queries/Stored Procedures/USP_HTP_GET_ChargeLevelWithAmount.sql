/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This procedure returns all the charge level and firm charge amount according to selected attorney/firm
* Parameter :
*				@FirmID
*				
* Column returns : 
*				Charge Level Id
*				Level code
*				Charge Amount
*/

CREATE PROCEDURE [dbo].[USP_HTP_GET_ChargeLevelWithAmount]
	@FirmID INT
AS
	SELECT ROW_NUMBER() OVER(ORDER BY tc.ID) AS sno,
	       tc.ID,
	       tc.LevelCode,
	       ISNULL(
	           (
	               SELECT ISNULL(fcl2.ChargeAmount, 0.0)
	               FROM   FirmChargeLevel fcl2
	               WHERE  fcl2.ChargeLevelID = tc.ID
	                      AND fcl2.FirmID = @FirmID
	           ),
	           0.0
	       ) AS amount
	FROM   tblChargelevel tc
GO


GRANT EXECUTE ON [dbo].[USP_HTP_GET_ChargeLevelWithAmount] TO dbr_webuser
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_InsertInOCR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_InsertInOCR]
GO


CREATE procedure [dbo].[usp_WebScan_InsertInOCR]              
              
@PicID as int,              
@CauseNo as varchar(50),              
@Status as varchar(50),              
@NewCourtDate as varchar(50),              
@TodayDate as varchar(50),              
@Location as varchar(50),          
@Time as varchar(50),    
@CourtNo as varchar(50),  
  
@AdminVerified as int      ,  
@ScanVerified as int  ,
@CheckStatus as int              
as              
              
Insert into tbl_WebScan_OCR              
(               
 PicID,              
 CauseNo,              
 Status,              
 NewCourtDate,     
 CourtNo,              
 TodayDate,              
 Location ,    
 Time          ,  
 AdminVerified,  
 ScanVerified   ,
 CheckStatus            
)              
values              
(               
 @PicID ,              
 @CauseNo ,              
 @Status ,              
 @NewCourtDate ,      
 @CourtNo,             
 @TodayDate ,              
 @Location   ,          
 @Time ,  
@AdminVerified,  
@ScanVerified  ,     
@CheckStatus              
)  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


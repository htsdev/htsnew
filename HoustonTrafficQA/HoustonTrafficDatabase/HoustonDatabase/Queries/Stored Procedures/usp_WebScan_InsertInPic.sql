SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_InsertInPic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_InsertInPic]
GO


CREATE procedure [dbo].[usp_WebScan_InsertInPic]    
    

@BatchID as int,  
@PicID as int output
    
as    
    
Insert into tbl_WebScan_Pic    
(     
 
 BatchID    
)    
values    
(     
 
 @BatchID     
)  
set @PicID=scope_identity()


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


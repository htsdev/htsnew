


/****** 
Created by:		Muhammad Ali	
Create Date:	09/23/2010

Business Logic:	The procedure is used by LMS application to get the returns on the selected mailer type.

				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS Reports
	@sDate:			Starting date for search criteria.
	@eDate:			Ending date for search criteria.
	@SearchType:	Flag that identifies if searching by upload date or by mailer date. 0= upload date, 1 = mailer date

List of Columns:	
	mdate:			Maier Date or upload date as selected in the search criteria.
	lettercount:	Number of letters printed
	nonclient:		Number of people not hired/contacted us
	client:			Number of people hired us.
	quote :			Number of people just contact us for information..
	

******/

-- [usp_mailer_weekly_AdvanceReport_data] 1, 25, '01/01/2010', '10/30/2010', 0
CREATE procedure [dbo].[usp_mailer_weekly_AdvanceReport_data]
(	
	@CatNum		INT,                
	@lettertype 	int ,               
	@sDate		datetime ,              
	@eDate 		DATETIME,
	@SearchType	TINYINT
)	
AS
set nocount on 

declare @DBid int
-- 1 = Traffic Tickets
-- 2 = Dallas Traffic Tickets

select @dbid =	 CASE ISNULL(tl.courtcategory,0) WHEN 26 THEN 5 ELSE ISNULL(tcc.LocationId_FK ,0) END
from tblletter tl INNER JOIN tblCourtCategories tcc ON tl.courtcategory = tcc.CourtCategorynum where letterid_pk = @lettertype

SELECT @dbid= ISNULL(@dbid,0)

declare @tblcourts table (courtid int)

----------------------------------------------------------------------------------------------------
--		DALLAS DATABASE....
----------------------------------------------------------------------------------------------------
if @dbid = 2
    BEGIN
		if @catnum < 1000
			insert into @tblcourts 
			select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = @catnum
		else
			insert into @tblcourts select @catnum	
    END
----------------------------------------------------------------------------------------------------
--		TRAFFIC TICKETS DATABASE....
----------------------------------------------------------------------------------------------------
ELSE
	BEGIN
	if @catnum < 1000
		insert into @tblcourts 
		select courtid from dbo.tblcourts where courtcategorynum = @catnum
	else
		insert into @tblcourts select @catnum	
	END
----------------------------------------------------------------------------------------------------		


-- GETTING LETTER HISTORY INTO TEMP TABLE AS PER SELECTION CRITERIA.....
declare @letters table (
	noteid int, 
	mdate datetime
	)

if @searchtype = 0 
	BEGIN
		insert into @letters
		select noteid,  convert(varchar(10), listdate, 101)
		from tblletternotes n, @tblcourts c
		where n.courtid = c.courtid
		and n.lettertype = @lettertype
		and datediff(day, listdate, @sdate)<= 0
		and datediff(day, listdate, @edate)>= 0
	END
else
	BEGIN
		insert into @letters
		select noteid,  convert(varchar(10), recordloaddate, 101)
		from tblletternotes n, @tblcourts c
		where n.courtid = c.courtid
		and n.lettertype = @lettertype
		and datediff(day, recordloaddate, @sdate)<= 0
		and datediff(day, recordloaddate, @edate)>= 0
	END


-- GETTING INFORMATION FROM CLIENTS/QUOTES......
declare @QuoteClient table (
	ticketid int, 
	mailerid int,
	activeflag int,
	hiredate datetime,
	quotedate datetime
	)

if @dbid = 2
	insert into @QuoteClient
	select t.ticketid_pk, t.mailerid, t.activeflag, 
			min(p.recdate), t.recdate
	from dallastraffictickets.dbo.tbltickets t 
	left outer join  
		dallastraffictickets.dbo.tblticketspayment p 
	on	p.ticketid = t.ticketid_pk and p.paymentvoid = 0
	inner join 
		@letters l
	on	l.noteid = t.mailerid
	group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate
else
	insert into @QuoteClient
	select t.ticketid_pk, t.mailerid, t.activeflag, 
			min(p.recdate), t.recdate
	from tbltickets t 
	left outer join  
		tblticketspayment p 
	on	p.ticketid = t.ticketid_pk and p.paymentvoid = 0
	inner join 
		@letters l
	on	l.noteid = t.mailerid
	group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate

-- SUMMARIZING THE INFORMATION....
declare @temp2 table
		(
		mdate	datetime,
		letterCount	int,
		NonClient	int,
		Client		int,
		Quote		int,
		weekno int
		)

INSERT into @temp2 (mdate, lettercount, client, Quote)
select l.mdate,  
	count(distinct l.noteid), 
	sum(case q.activeflag when 1 then 1 end), 
	sum(case q.activeflag when 0 then 1 end) 
	
from @letters l
left outer join
	@QuoteClient q
on  l.noteid = q.mailerid
group  by l.mdate

update t
set nonclient = lettercount - (isnull(quote,0)+isnull(client,0))
from @temp2 t

--Ids concatenates..
CREATE TABLE #temp3
(
	dates VARCHAR(20),
	ClientsIds VARCHAR(MAX)
)
---Client ids...
INSERT INTO #temp3(dates,ClientsIds) 
  SELECT convert(varchar,p1.mdate,101),
       ( SELECT CONVERT(VARCHAR, noteid)+ ','
           FROM  @letters p2
left outer join
	@QuoteClient q
on  p2.noteid = q.mailerid
          WHERE p2.mdate = p1.mdate AND q.activeflag=1
          ORDER BY mdate
            FOR XML PATH('') ) AS ids
      from @letters p1
left outer join
	@QuoteClient q
on  p1.noteid = q.mailerid
  WHERE q.activeflag=1
group  by p1.mdate


------- Non Client ids...

CREATE TABLE #temp4
(
	dates VARCHAR(20),
	nonClientsIds VARCHAR(MAX)
)

INSERT INTO #temp4(dates,nonClientsIds) 
  SELECT convert(varchar,p1.mdate,101),
       ( SELECT CONVERT(VARCHAR, noteid)+ ','
           FROM  @letters p2
left outer join
	@QuoteClient q
on  p2.noteid = q.mailerid
          WHERE p2.mdate = p1.mdate AND q.activeflag=0
          ORDER BY mdate
            FOR XML PATH('') ) AS ids
      from @letters p1
left outer join
	@QuoteClient q
on  p1.noteid = q.mailerid
  WHERE q.activeflag=0
group  by p1.mdate

CREATE TABLE #temp5
(
	date VARCHAR(20),
	clientsids VARCHAR(MAX),
	nonclients VARCHAR(MAX)
)


INSERT INTO #temp5 (date,clientsids,nonclients)
SELECT t.mdate,t3.ClientsIds,t4.nonClientsIds
FROM @temp2 t LEFT OUTER JOIN #temp3 t3 ON t.mdate=t3.dates LEFT OUTER JOIN #temp4 t4 ON t.mdate=t4.dates




declare  @dt1 datetime, @dt2 datetime
declare	@wk int, @t1 int, @dw int

declare @table table (mdate datetime, weekday int, weekno int, sdate datetime, edate datetime, week_range varchar(50))

select @wk = 1,  @dt1 = @sdate 

if (@edate > dateadd(wk,datediff(wk,0,@sdate),5))
begin
	select @dt2 = dateadd(wk,datediff(wk,0,@sdate),5),@t1=0 --Set last day of Week
end
else
begin
	select @dt2 = @edate,@t1=0 --Set end date
end
while datediff(day,@sdate, @edate) >= 0
	begin	
		select @dw = datepart(dw, @sDate)
		if @dw < @t1 
			begin
				if(@edate > @sdate + 6)
				begin
					select @wk = @wk + 1 , @dt1 = @sdate, @dt2 = @sdate + 6
				end
				else
				begin
					select @wk = @wk + 1 , @dt1 = @sdate, @dt2 = @edate
				end				
			end
		insert into @table (mdate, [weekday], weekno, sdate, edate)
		select @sDate, @dw, @wk, @dt1, @dt2
		select @t1 = @dw
		set @sDate = @sDate + 1
	end
--5313
update @table set week_Range = convert(varchar(10), sdate,1) + ' - '  + convert(varchar(10), edate,1)

update	t1
set	t1.weekno = t2.weekno
from	@temp2 t1 inner join @table t2 on t1.mdate = t2.mdate
--Abbas Shahid Khwaja 4900 12/04/2011 Remove the DateRange column(not required).	
SELECT  b.weekno as mdate2,
		b.week_range as mdate,
		isnull(sum(lettercount),0) as lettercount, 
		isnull(sum(isnull(nonclient,0 )),0)  as nonclient, 
		isnull(sum(isnull(client,0)),0) as client,
		isnull(sum(isnull(quote,0)),0) as quote,t5.clientsids,t5.nonclients,
		CASE WHEN @DBid =1 THEN 1 ELSE 2 END AS ProjectType
from @temp2 a INNER JOIN #temp5 t5 ON a.mdate=t5.date
inner join @table b on a.mdate = b.mdate 
group BY  b.weekno, b.week_range,t5.clientsids,t5.nonclients
order by b.weekno desc


GO

GRANT EXECUTE ON [dbo].[usp_mailer_weekly_AdvanceReport_data] TO dbr_webuser

GO



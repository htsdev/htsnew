SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_OUTSIDECOURT_RECORDS_ONETIME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_OUTSIDECOURT_RECORDS_ONETIME]
GO

  
    
CREATE PROCEDURE [dbo].[USP_HTS_GET_OUTSIDECOURT_RECORDS_ONETIME]      
AS      
  
  
select distinct tv.ticketid_pk, tv.refcasenumber as TicketNumber, tv.courtdate as Courtdate      
 from tblticketsviolations tv       
   inner join tbltickets t on    
 t.TicketID_PK = tv.TicketID_PK    
where   
 courtid between 3007 and 3022      
and t.ActiveFlag = 1   
and tv.courtviolationstatusid IN (3,26,101,103)   
and tv.refcasenumber not like('ID%') and len(tv.refcasenumber) = 12     
and tv.courtdate > getdate()   
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


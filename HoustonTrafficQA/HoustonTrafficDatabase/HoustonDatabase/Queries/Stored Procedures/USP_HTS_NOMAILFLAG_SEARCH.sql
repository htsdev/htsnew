/*****************************  
* Business Logic : This Stored procedure is used by HTP/Activites/DoNotMailUpdate/NoMailFlagRecords. to get list of all
*					persons added in our no-mailing list according to search criteria.
*					
* Input Parameters:
* @FirstName = firstname of a person.
* @LastName = lastname of a person.
* @Address = address of a person.
* @Zip = zip code of city.
* @StateID = id of state.
* @City = city of a peroson.
* @TodaysRecord =  if 1 display todays records .
* @startdate = start date in a date range.
* @enddate = end date in a date range.
* 
* Output Columns:
* FIRSTNAME
* LASTNAME
* ADDRESS
* ZIP
* City
* State
*************************/  
--Yasir Kamal  6100 07/02/2009 date range search criteria added.
alter PROCEDURE [dbo].[USP_HTS_NOMAILFLAG_SEARCH] --'','','','','','','1','07/21/2009','07/21/2009'     
@FirstName varchar(20),          
@LastName varchar(20),          
@Address varchar(50),        
@Zip varchar(12),    
@StateID int,    
@City varchar(50),    
@UseDateRange BIT,
@startdate AS DATETIME,
@enddate AS DATETIME    
as      
          
declare @Sql varchar(2000)

--Yasir Kamal 6144 07/13/2009 insert date column added.      
--Yasir Kamal 6185 07/20/2009 Insert update source column
-- Noufil 6151 10/31/2009 Use Top 250
set @Sql ='        
SELECT distinct top 250 convert(varchar,m.insertdate,101) as insertdate,
	m.FIRSTNAME, 
	m.LASTNAME, 
	m.ADDRESS, 
	m.ZIP, 
	m.City, 
	s.State,
Case m.UpdateSource when 1 then ''USPS Tracking Service'' 
					When 0 then ''Upload Returned Mailers'' 
					When 2 then ''Upload Returned Mailers-IMB''
					else ''N/A''
					end as UpdateSource  
FROM tbl_hts_donotmail_records m     
join tblstate s    
on m.StateID_FK = s.StateID    
WHERE 1=1 '      
      
if(@FirstName <> '')      
begin      
set @Sql = @Sql + ' and m.FirstName = '''+@FirstName+''''      
end      
      
if(@LastName <> '')      
begin      
set @Sql = @Sql + ' and m.LastName = '''+@LastName + ''''      
end      
      
if(@Address <> '')      
begin      
set @Sql = @Sql + ' and m.Address = ''' +@Address+ ''''      
end      
      
if(@Zip <> '')      
begin      
set @Sql = @Sql + ' and m.Zip = '''+@Zip +''''      
end      
    
if(@City <> '')    
begin    
set @Sql = @Sql + ' and m.City = '''+@City +''''      
end    
    
if(@StateID <> 0)    
begin    
set @Sql = @Sql + ' and m.StateID_FK = ' +convert(varchar,@StateID) +' '    
end    
    

IF(@UseDateRange = 1)
begin
SET @sql = @sql + 'and datediff(day,insertdate,'''+CONVERT(VARCHAR,@startdate,101)+''') <=0 and datediff(day,insertdate,'''+CONVERT(VARCHAR,@enddate,101)+''') >=0'	
End
    
print @sql    
exec (@Sql)      
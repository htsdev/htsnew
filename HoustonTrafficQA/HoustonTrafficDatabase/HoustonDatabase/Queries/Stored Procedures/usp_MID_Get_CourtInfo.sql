SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_CourtInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_CourtInfo]
GO

-- PROCEDURE FOR FILLING COURT LOCATIONS IN THE DROP DOWN....  
-- WHICH WILL BE USED AS THE SELECTION CRITERIA......  
CREATE  procedure [dbo].[usp_MID_Get_CourtInfo]  
as  
  
  
select convert(varchar(10),courtid),  
 shortname   
from  tblcourts   
-- REMOVING GARBAGE DATA....  
where ( courtid <> 0 and courtid is not null and courtname <> '' )  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


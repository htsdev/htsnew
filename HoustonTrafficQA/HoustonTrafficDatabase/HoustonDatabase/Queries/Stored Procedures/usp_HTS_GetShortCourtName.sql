/***********    
    
Alter By : Zeeshan Ahmed     
Business Login : This Procedure is used to get court list by case type.    
    
List of  Parameters :    
     
 @active   : 1 Active Courts , 0 All courts    
 @FilterCourt : 0 Not Filter Court on Case Type    
 @CaseType    : 1 : Traffic ,2: Criminal , 3: Civil     
    
List Of Columns    
     
 courtid : Court ID    
 shortcourtname : Short Court Name    
 ShortName : Court Name    
     
************/    
    
                    
ALTER PROCEDURE  usp_HTS_GetShortCourtName 
@active int=0,    
@FilterCourt int = 0 ,    
@CaseType int = 1    
    
    
                    
AS                    
                    
if ( @FilterCourt = 0 )                 
Begin    
                     
 if @active=1                  
 begin                  
 Select  courtid,shortcourtname,ShortName   From tblcourts                   
 where inactiveflag=0                   
 end                  
 else                  
 Select  courtid,shortcourtname,ShortName  From tblcourts                   
     
End    
Else     
Begin    
 -- Ozair 4469 07/26/2008 included inactive court check 
   SELECT courtid,shortcourtname,ShortName from tblcourts where inactiveflag =0 and casetypeid=@CaseType  
          
End
﻿      
/****** 
Altered by:		Sabir Khan
Business Logic:	This procedure is used to add entry of trial letter into batch. 
				
				
List of Parameters:
	@TicketIDList
	@BatchID
	@empid
	
*******/     
ALTER Procedure [dbo].[USP_HTS_Batch_Trialletter]          
 (                  
 @TicketIDList  int,         
 @BatchID int,                 
 @empid int                  
 )                  
as        
        
INSERT INTO tbl_HTS_Batch_TrialLetter        
(        
 batchid,        
 TicketID_PK,        
 MiddleName,        
 TicketNumber_PK,        
 FirstName,        
 LastName,        
 Address1,        
 Address2,        
 Ccity,        
 Cstate,        
 Czip,        
 CourtDate,        
 CourtNumber,        
 Address,        
 City,        
 Zip,        
 State,        
 DateTypeFlag,        
 ActiveFlag,        
 Languagespeak,        
 --SequenceNumber, -- Sarim 2664 06/17/2008               
 CategoryID,        
 TicketViolationDate,        
 Description,        
 CaseStatus,        
 courtid,        
 courtviolationstatusid        
)         
        
SELECT         
@BatchID as batchid,        
dbo.tblTickets.TicketID_PK,               
 dbo.tblTickets.MiddleName,               
 dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK,               
 dbo.tblTickets.Firstname,               
 dbo.tblTickets.Lastname,            
             
-- added by asghar          
(case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.Address1 else (select Address from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID)  end) as Address1,          
(case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.Address2 else (select Address2 from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end) as Address2,          
(case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.City else (select City from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end) as Ccity,          
(case when dbo.tblTickets.FirmID = 3000 then dbo.tblState.State else (select dbo.tblState.State where dbo.tblState.StateID =(select state from dbo.tblfirm where dbo.tblfirm.FirmID =dbo.tblTickets.FirmID)) end) as cState,          
(case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.Zip else (select Zip from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end) as Czip,          
       
 --Sabir Khan 5349 12/20/2008 auto court date has been changed into verified court date...       
 --dbo.tblTicketsViolations.CourtDate,                
dbo.tblTicketsViolations.CourtDatemain as CourtDate,  
--Sabir Khan 5349 12/20/2008 auto court number has been changed into verified court number...                
 --dbo.tblTicketsViolations.CourtNumber,               
dbo.tblTicketsViolations.CourtNumbermain as CourtNumber,               
 dbo.tblCourts.Address,               
 dbo.tblCourts.City,               
 dbo.tblCourts.Zip,               
 tblState_1.State,               
 dbo.tblCourtViolationStatus.CategoryID as Datetypeflag,               
 dbo.tblTickets.Activeflag,               
 isnull( dbo.tblTickets.LanguageSpeak, 'ENGLISH') AS Languagespeak,               
 --dbo.tblTicketsViolations.SequenceNumber,   -- Sarim 2664 06/17/2008               
 dbo.tblCourtViolationStatus.CategoryID,               
 --Farrukh 10272 05/10/2012 Replaced violationDate from 1/1/9100 to empty string
 (CASE WHEN DATEDIFF(DAY,ISNULL(dbo.tblTicketsViolations.TicketViolationDate,'1/1/1900'),'1/1/1900') <> 0 THEN CONVERT(VARCHAR(10),dbo.tblTicketsViolations.TicketViolationDate,101) ELSE '' END) AS TicketViolationDate,
 --Farrukh 10148 04/18/2012 Added Spanish Violation Description for Spanish Clients
 (CASE WHEN isnull( dbo.tblTickets.LanguageSpeak, 'ENGLISH') <> 'ENGLISH' AND ISNULL(dbo.tblViolations.SpanishDescription,'') <> '' THEN dbo.tblViolations.SpanishDescription ELSE dbo.tblViolations.[Description] END) AS [Description],
 --dbo.tblViolations.Description,  --Sabir Khan 8862 02/23/2011 get violation description in case of HCJP court and Juvnile Status...             
 CASE WHEN dbo.tblCourts.courtcategorynum = 2 AND dbo.tblTicketsViolations.CourtViolationStatusIDmain IN (139,256,257,258,259) THEN dbo.tblCourtViolationStatus.[Description] ELSE dbo.tblDateType.Description END AS CaseStatus ,                    
 dbo.tblTicketsViolations.courtid as courtid,        
 dbo.tblCourtViolationStatus.courtviolationstatusid          
FROM    dbo.tblTickets               
INNER JOIN              
 dbo.tblTicketsViolations               
ON  dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK               
INNER JOIN              
 dbo.tblCourts               
ON  dbo.tblTicketsviolations.courtid = dbo.tblCourts.Courtid               
INNER JOIN              
 dbo.tblState               
ON  dbo.tblTickets.Stateid_FK = dbo.tblState.StateID               
INNER JOIN              
 dbo.tblState tblState_1               
ON  dbo.tblCourts.State = tblState_1.StateID               
INNER JOIN              
 dbo.tblCourtViolationStatus   
--Sabir Khan 5349 12/20/2008 auto court violation status has been changed into verified court  violation status...              
--ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID               
ON  dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID               
INNER JOIN              
 dbo.tblViolations               
ON  dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK               
INNER JOIN              
 dbo.tblDateType               
ON  dbo.tblCourtViolationStatus.CategoryID = dbo.tblDateType.TypeID              
WHERE   (dbo.tblTickets.Activeflag = 1)   
--Sabir Khan 5349 12/20/2008 auto court date has been changed into verified court date...              
--AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)  
AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDatemain, GETDATE()) <= 0)                            
--Sabir Khan 7074 12/04/2009 No need to print trial letter for arrignment cases...
--Sabir Khan 8862 02/23/2011 Include Juvnile statuses for HCJP court only.
AND     ((dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5) OR (dbo.tblCourts.courtcategorynum = 2 AND dbo.tblTicketsViolations.CourtViolationStatusIDmain IN (139,256,257,258,259) )))                             
AND     (dbo.tblTickets.TicketID_PK = @ticketidlist) 
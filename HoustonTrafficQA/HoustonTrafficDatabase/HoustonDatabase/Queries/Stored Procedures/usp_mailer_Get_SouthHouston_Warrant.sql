/****** 
Created by:		Rab Nawaz Khan
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the South Houston Municipal Court Warrant letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--  usp_mailer_Get_SouthHouston_Warrant  32, 124, 32, '6/20/2011', '6/22/2011', 0
Alter PROCEDURE [dbo].[usp_mailer_Get_SouthHouston_Warrant]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int   ,                                              
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int
	)

as                                                              
          

                                                  
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(max)  ,
	@sqlquery2 nvarchar(max)  ,
	@sqlParam nvarchar(1000)

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

select @sqlquery ='', @sqlquery2 = ''
                                                              
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                 
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                        


-- MAIN QUERY....
-- FTA ISSUE DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY FTA CASES
-- INSERTING VALUES IN A TEMP TABLE....                              
set 	@sqlquery=@sqlquery+'                                          
 Select distinct 
	convert(datetime , convert(varchar(10),tva.ftaissuedate,101)) as mdate,   
	flag1 = isnull(ta.Flag1,''N'') , 
	ta.donotmailflag, 
	TA.recordid AS RECORDID,
	left(TA.zipcode,5) + ta.midnumber as zipmid, 
	isnull(tva.fineamount,0) as fineamount,  
	tva.violationnumber_pk, 
	tva.violationdescription,
	tva.FTALinkId
into #temp                                                            
FROM tblTicketsViolationsArchive tva, tblTicketsArchive ta,  dbo.tblcourts, tblstate s

-- ONLY FTA VIOLATIONS...
where 
tva.RecordID = ta.RecordID 
and dbo.tblcourts.courtid = tva.courtlocation   
and s.stateid =  ta.stateid_Fk
and ta.address1 <> ''11002 HAMMERLY BLVD APT 76''

-- HAVING FTA IN CAUSE NUMBER...
and charindex(''FTA'',tva.causenumber) <> 0

-- SHOULD HAVE FTA LINK ID 
and TVA.FTALinkID is not null

-- PERSON NOT MOVED IN LAST 48 MONTHS (ACCUZIP)
and isnull(ta.ncoa48flag,0) = 0

and TVA.violationstatusid = 186

and ta.lastname IS NOT NULL 
and ta.firstname IS NOT NULL 

-- FTA ISSUE DATE LIES BETWEEN SELECTED DATE RANGE...
and datediff(Day, tva.ftaissuedate, @startlistdate) <= 0 
and datediff(day, tva.ftaissuedate, @endlistdate) >= 0

-- added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
 '
  
-- IF PRINTING FOR ALL COURTS OF THE SELECTED CATEGORY...
if(@crtid = @catnum  )                                    
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE if( @crtid <>  @catnum )                                    
	set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid' 
                                                        
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                                     
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and not  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-----------------------------------------------------------------------------------------------------------------------------------------------------

-- GETTING VIOL COUNT & TOTAL FINE AMOUNT FOR EACH LETTER...
set @sqlquery =@sqlquery+ '

 
Select distinct 
	convert(datetime , convert(varchar(10),tva.ftaissuedate,101)) as mdate,   
	flag1 = isnull(ta.Flag1,''N'') , 
	ta.donotmailflag, 
	TA.recordid AS RECORDID,
	left(TA.zipcode,5) + ta.midnumber as zipmid, 
	isnull(tva.fineamount,0) as fineamount,  
	tva.violationnumber_pk, 
	tva.violationdescription,
	tva.FTALinkId
into #tempT                                                            
FROM tblTicketsViolationsArchive tva, tblTicketsArchive ta,  dbo.tblcourts, tblstate s
where tva.RecordID = ta.RecordID 
and dbo.tblcourts.courtid = tva.courtlocation   
and s.stateid =  ta.stateid_Fk
and ta.address1 <> ''11002 HAMMERLY BLVD APT 76''
and charindex(''FTA'',tva.causenumber) > 0
and TVA.FTALinkID is not null
and isnull(ta.ncoa48flag,0) = 0 
and ta.lastname IS NOT NULL 
and ta.firstname IS NOT NULL 
AND TVA.violationstatusid = 186 
and datediff(Day, ftaissuedate, @startlistdate) <= 0 
and datediff(day, ftaissuedate, @endlistdate) >= 0 

--added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0

'
-- IF PRINTING FOR ALL COURTS OF THE SELECTED CATEGORY...
if(@crtid = @catnum  )                                    
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE if( @crtid <>  @catnum )                                    
	set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid ' 
	
set @sqlquery =@sqlquery+ '
insert into #temp
Select distinct 
	convert(datetime , convert(varchar(10),tva.ftaissuedate,101)) as mdate,   
	flag1 = isnull(ta.Flag1,''N'') , 
	ta.donotmailflag, 
	TA.recordid AS RECORDID,
	left(TA.zipcode,5) + ta.midnumber as zipmid, 
	isnull(tva.fineamount,0) as fineamount,  
	tva.violationnumber_pk, 
	tva.violationdescription,
	tva.FTALinkId
from tblticketsarchive ta, tblticketsviolationsarchive tva, #tempT a, tblcourts 
where tva.recordid = ta.recordid and tblcourts.courtid = tva.courtlocation  
and a.ftalinkid = tva.ftalinkid 
and charindex(''FTA'',tva.causenumber) = 0
and (tva.violationstatusid <> 80) and ta.recordid not in(select distinct recordid from #tempT)

--added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0 

select distinct ftalinkid into #tmp2 from #temp group by ftalinkid having count(recordid) > 1

select zipmid , 
	count(violationnumber_pk) as violcount,
	isnull(sum(isnull(fineamount,0)),0) as total_fineamount
into #temptable
from #temp  
group by zipmid

alter table #temp add violcount int , total_fineamount money 

update a 
set a.violcount = b.violcount, 
	a.total_fineamount = b.total_fineamount
from #temp a, #temptable b where a.zipmid = b.zipmid

'

set @sqlquery =@sqlquery+ '

select distinct * into #temp1 from #temp where 1 = 1
'

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT                                      
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and violcount=2) )
      '                                                    

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (total_fineamount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and violcount=1))                                
    and ('+ @doublevoilationOpr+  '  (total_fineamount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and violcount=2))
        '                                  
                              
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
begin
-- NOT LIKE FILTER.......
if(charindex('not',@violaop)<> 0 )              
	set @sqlquery=@sqlquery+' 
	and	 not ( violcount =1  and  ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'           

-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
-- LIKE FILTER.......
if(charindex('not',@violaop) =  0 )              
	set @sqlquery=@sqlquery+' 
	and	 ( violcount =1  and ( '+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  violationdescription  ' ,+ ''+ 'like')+'))'           
end

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR DLQ LETTER IS PRINTED IN PAST 2 DAYS FOR THE SAME RECORD...
set @sqlquery =@sqlquery+ ' 

delete from #temp1 
from #temp1 a inner join tblletternotes_detail d on d.recordid = a.recordid inner join 
tblletternotes n on n.noteid=  d.noteid
where (n.lettertype = @lettertype or (n.lettertype in (121, 122, 123, 125)  and datediff(day, n.recordloaddate, getdate()) in (0,1) )    )

'


-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery2=@sqlquery2 +'
-- exclude client cases...
select a.mdate,a.flag1,a.donotmailflag,a.recordid,a.zipmid,a.fineamount,a.violationnumber_pk,a.violationdescription,a.violcount,a.total_fineamount, v.ftalinkid into #ftalinks
from tblticketsviolationsarchive v inner join #temp1 a on a.recordid = v.recordid
--added attorney association logic.
Where len(isnull(v.attorneyname, '''')+isnull(v.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(v.BondingCompanyName,''''))=0

-- send warrant letters to existing clients.
--delete from #ftalinks
--from #ftalinks a inner join tblticketsviolationsarchive b on b.ftalinkid = a.ftalinkid inner join 
--tblticketsviolations v on b.recordid = v.recordid  inner join 
--tbltickets t on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 


Select count(distinct zipmid) as Total_Count, mdate 
into #temp2 
from #ftalinks 
group by mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct zipmid) as Good_Count, mdate 
into #temp3
from #ftalinks 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS                                                      
Select count(distinct zipmid) as Bad_Count, mdate  
into #temp4  
from #ftalinks 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct zipmid) as DonotMail_Count, mdate  
into #temp5   
from #ftalinks 
where donotmailflag=1 
group by mdate

-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
-- SELECTED DATE RANGE.......
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
'
	
-- OUTPUTTING THE REQURIED INFORMATION........         
set @sqlquery2 = @sqlquery2 +'                                                        
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate   
order by l.mdate 
'

-- GETTING TOTAL NUMBER OF PRINTIABLE LETTERS FOR ALL THE SELECTED
-- DATE RANGES. TO DETERMINE THE EXISTANCE OF SAME CLIENT IN MULTIPLE LETTERS...
set @sqlquery2 = @sqlquery2 +'        
Select count(distinct zipmid) from #ftalinks 
where Flag1 in (''Y'',''D'',''S'') and donotmailflag = 0  

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5
'

SET @sqlquery  = @sqlquery  + @sqlquery2                                 
--PRINT @sqlquery  

-- CONCATENATING THE QUERY ....

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


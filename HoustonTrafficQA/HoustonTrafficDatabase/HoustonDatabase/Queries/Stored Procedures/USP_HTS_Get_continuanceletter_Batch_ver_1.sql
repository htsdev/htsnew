 ALTER procedure dbo.USP_HTS_Get_continuanceletter_Batch_ver_1                  
                   
 @empid int,                  
-- @chkVal int ,                  
 @letterType int,           
-- @printed int,               
-- @filedate datetime = '01/01/1900',              
 @ticketidlist varchar(400) ='0'              
  as                            
                  
-- CREATING TEMP TABLE TO STORE THE TICKET IDs                  
declare @tickets table (                  
 ticketid int     
                  
 )                
    
--Displaying records sorted when printed    
declare @ticketsort table    
(    
 ticketsort int,    
        batchid int     
)      
                  
-- INSERTING TICKET IDs AS                   
/*  
if @chkVal = 1                  
begin        
 if @printed=2        
  begin                  
   insert into @tickets                  
   SELECT    distinct TicketID_FK                  
   FROM         dbo.tblHTSBatchPrintLetter                  
   WHERE     (LetterID_FK = @letterType) AND (IsPrinted = 0) AND (deleteflag <> 1)    
 insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType AND (IsPrinted = 0)  and deleteflag<>1   )    
 --   where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (IsPrinted = 0)                 
   end         
 else        
  begin                  
   insert into @tickets                  
   SELECT    distinct TicketID_FK                  
   FROM         dbo.tblHTSBatchPrintLetter                  
   WHERE     (LetterID_FK = @letterType) AND (deleteflag <> 1)      
 insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType  and deleteflag<>1   )    
--    where hts.LetterID_Fk = @letterType and deleteflag<>1               
   end         
          
end                   
                  
if @chkVal = 2                  
begin          
 if @printed=2        
  begin                    
   insert into @tickets                  
   SELECT    distinct TicketID_FK                  
   FROM         dbo.tblHTSBatchPrintLetter                  
   WHERE     (LetterID_FK = @letterType)                   
   AND (IsPrinted = 0) AND (deleteflag <> 1)                  
   AND (datediff(day,BatchDate, @filedate) = 0)      
  insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
   where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType AND (IsPrinted = 0)  and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)    )    
 --   where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (IsPrinted = 0) AND (datediff(day,BatchDate, @filedate) = 0)                    
  end                
          
 else        
  begin                    
   insert into @tickets                  
   SELECT    distinct TicketID_FK                  
   FROM         dbo.tblHTSBatchPrintLetter                  
   WHERE     (LetterID_FK = @letterType)                   
   AND (deleteflag <> 1)                   
   AND (datediff(day,BatchDate, @filedate) = 0)    
 insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv    
   where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType   and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)    )    
   -- where hts.LetterID_Fk = @letterType and deleteflag<>1 AND (datediff(day,BatchDate, @filedate) = 0)                    
  end                   
         
         
end                   
                  
if @chkVal = 3                  
*/  
begin                  
 insert into @tickets                  
 select * from dbo.Sap_String_Param(@ticketidlist)     
 insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType and deleteflag<>1   )    
  --  where hts.LetterID_Fk = @letterType and deleteflag<>1                  
end                    
declare @temp table (                  
 [RowId] int identity(1,1),                  
 [ticketid_pk] [int] NOT NULL ,                  
 [CaseDesc] [varchar] (127)  ,                  
 [firstname] [varchar] (20)  ,                  
 [lastname] [varchar] (20)  ,                  
 [currentdateset] [datetime] NULL ,                  
 [currentcourtnum] [varchar] (4)  ,                  
 [Address] [varchar] (50)  ,                  
 [city] [varchar] (50)  ,                  
 [state] [varchar] (50) ,                  
 [zip] [varchar] (12)  ,                  
 [contrequestnotes] [varchar] (300)  NULL ,    
 [batchid] [int]                  
 )                   
                  
declare @temp2 table (                  
 [ticketid_pk] [int] NOT NULL ,                  
 [CaseDesc] [varchar] (1000)  ,                  
 [firstname] [varchar] (20)  ,                  
 [lastname] [varchar] (20)  ,                  
 [currentdateset] [datetime] NULL ,                  
 [currentcourtnum] [varchar] (4)  ,                  
 [Address] [varchar] (50)  ,                  
 [city] [varchar] (50)  ,                  
 [state] [varchar] (50) ,                  
 [zip] [varchar] (12)  ,                  
 [contrequestnotes] [varchar] (300)  NULL  ,                 
 [batchid] [int]    
 )                   
                  
declare @RecCount int,                  
 @Idx int,                  
 @TicketId int,                  
 @TempTicketId int,                  
 @CaseDesc varchar(1000)                  
                  
select @RecCount = 0,                  
 @Idx = 1,                  
 @CaseDesc = ''                  
                  
                  
-- INSERTING RECORDS IN TEMP TABLE.....                  
insert into @temp (                  
 ticketid_pk , casedesc, firstname, lastname, currentdateset, currentcourtnum,                   
 address, city, state, zip , contrequestnotes,batchid                  
 )                   
select              
 T.ticketid_pk,                  
 refcasenumber + ' : ' + O.description as CaseDesc,                  
 firstname,                   
 lastname,                  
 V.CourtDateMain,                  
 V.CourtNumberMain,                  
 Address,                  
 C.city,                  
 S.state,                  
 C.zip,                  
 contrequestnotes ,    
 ts.batchid                   
from  @ticketsort ts,tbltickets T,                   
 tblticketsviolations V,                   
 tblcourts C,                   
 tblstate S,                   
 tblviolations O                    
where T.ticketid_pk = V.ticketid_pk                    
and  V.CourtId = C.courtid                    
and  C.state  = S.stateid                    
and  v.violationnumber_pk = O.violationnumber_pk                    
and  violationtype <> 1                    
and  activeflag = 1                    
and  continuanceamount > 0                    
--and  t.ticketid_pk in (                  
--  select distinct ticketid from @tickets               
--  )                  
and ts.ticketsort=t.ticketid_pk    
order by ts.batchid                  
--order by t.ticketid_pk                  
                  
-- GETTING RECORD COUNT FROM TEMP TABLE THAT WILL BE USED IN LOOPING...                  
select @RecCount = count(*) from @temp                  
                  
-- GETTING FIRST TICKET ID IN A TEMP. VARIABLE.....                  
select @TempTicketId = min(ticketid_pk) from @temp                  
                  
-- LOOPING TO MERGE CASE NUMBER OF A TICKET ID....                  
while @idx  < =   @RecCount                  
begin                  
 -- INSERTING FIRST RECORD .........                  
 if @idx = 1                   
    begin                  
  insert into @temp2                   
  select ticketid_pk ,                  
   CaseDesc   ,                  
   firstname  ,                  
   lastname  ,                  
   currentdateset  ,                  
   currentcourtnum   ,                  
   Address  ,                  
   city   ,                  
   state  ,                  
   zip   ,                  
   contrequestnotes,    
   batchid                   
 from  @temp where rowid = @idx                   
                  
  -- INSERTING A NOTE IN CASE HISTORY.....                  
--  exec sp_Add_Notes @TempTicketId,'Continuance Letter Printed',null,@empid              
     end                  
 else                  
   begin                  
                  
  select @ticketid = ticketid_pk from @temp where rowid = @idx                   
                   
-- IF TICKET ID IS CHANGED THEN INSERT RECORD IN 2ND TEMP TABLE....                    
  if @ticketid  <> @tempticketid                  
   begin                  
    insert into @temp2                   
    select ticketid_pk ,                  
     casedesc   ,                  
     firstname  ,                  
     lastname  ,                  
     currentdateset  ,                  
     currentcourtnum   ,                  
     Address  ,                  
     city   ,                  
     state  ,                  
     zip   ,                  
     contrequestnotes,    
  batchid                   
    from  @temp where rowid = @idx                  
                  
--    exec sp_Add_Notes @ticketid,'Continuance Letter Printed',null,@empid                  
   end                  
                  
  -- OTHERWISE ADD THE CASE NUMBER & VIOL. DESC IN PREVIOUSLY INSERTED TICKET....                  
  else                  
   begin                  
    select @CaseDesc =  char(10) + char(13) + CaseDesc                  
    from @temp where rowid = @idx                  
                      
    update @temp2 set casedesc = casedesc + @casedesc where ticketid_pk = @ticketid                  
   end                    
                   
  select @tempticketid = @ticketid                  
    end                  
                  
 -- INCREMENTING THE COUNTER.....                  
 set @idx = @idx + 1                  
                  
end                  
                  
-- GETTING FINAL RESULTS......                  
select * from @temp2            

go 
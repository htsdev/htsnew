﻿
/**********************
* Created By:	Muhammad Nasir
* Task ID:		6968
* Business Rule:Get show setting history records(reports logs) of given dates range and report
* 	
* List of Parameters:				
			@StartDate		history start date
			@EndDate		history end date
			@ReportID		Report identification
			@ShowAll		disable date range				
* 
**********************/

Create PROCEDURE USP_HTP_Get_ShowSettingHistory
	@StartDate DATETIME,
	@EndDate DATETIME,
	@ReportID INT,
	@ShowAll BIT
AS
BEGIN
    SELECT ssh.RecDate,
           ssh.Note,
           tu.Firstname + ' ' + tu.Lastname AS RepName
    FROM   ShowSettingHistory ssh
           INNER JOIN tblUsers tu
                ON  tu.EmployeeID = ssh.UpdatedByEmployeeID
    WHERE  ssh.ReportID = @ReportID
           AND (
                   @ShowAll = 1
                   OR (
                          DATEDIFF(DAY, ssh.RecDate, @StartDate) < = 0
                          AND DATEDIFF(DAY, ssh.RecDate, @EndDate) > = 0
                      )
               )
END
GO

GRANT EXECUTE ON dbo.USP_HTP_Get_ShowSettingHistory TO dbr_webuser
GO
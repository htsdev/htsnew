﻿
/******   
Altered by:  Syed Muhammad Ozair  
Business Logic : this procedure is used to get the court category names in ascending order grouped by county  
     name which are sorted by their sortorder defiened in against them.   
  
List of Columns:    
 countyname :   
 url :   
 courtcategorynum :  
 courtcategoryname :  
  
******/  

Alter   PROCEDURE  [dbo].[usp_Mailer_GetShortCourtCategories] --1  
 (  
 @UserLocation int = 1  
 )  
         
AS          
--ozair 3898 05/28/2008          
declare @temp table  
(  
 id int IDENTITY(1,1) NOT NULL,  
 countyname varchar(100) NULL,  
 url varchar(50) NULL,  
 courtcategorynum int NULL,  
 courtcategoryname varchar(50) NULL,
 --Yasir Kamal 7218 01/13/2010 lms structure changed. 
 locationid_fk INT NULL  
)  
  
declare @temp1 table  
(   
 countyname varchar(100) NULL,  
 url varchar(50) NULL,  
 courtcategorynum int NULL,  
 courtcategoryname varchar(50) NULL,
 locationid_fk INT NULL    
)  
  
Select  
 c.countyname,    
 t.url,    
 t.courtcategorynum,      
 t.CourtCategoryName,
 t.LocationId_FK  
into   
 #temp     
From  
 tblCourtCategories t inner join tbl_mailer_county c  
 on t.countyid_fk=c.countyid         
Where    
 c.isActive = 1  
 and t.isactive=1    
order by   
 c.sortorder,  
 t.courtcategoryname  
  
-- tahir 5701 03/25/2009 added Fort Worth mailers..  
-- Sarim 6610 09/24/2009 added Arlington mailers ..  

if @UserLocation = 2  
 delete from #temp where LocationId_FK <> 2
  
insert into @temp(countyname,url,courtcategorynum,courtcategoryname,locationid_fk)  
select * from #temp  
  
drop table #temp  
  
declare @count int  
declare @item int   
declare @countyName varchar(100)  
  
set @countyName=''  
set @item=0  
select @count=count(*) from @temp  
  
while @count>=@item  
begin  
 if @item=0  
 begin  
  insert into @temp1(countyname,url,courtcategorynum,courtcategoryname,locationid_fk)  
  (select countyname,'',0,'',0 from @temp where id=1)  
    
  select @countyName=countyname from @temp where id=1  
  
  set @item=1  
 end  
 else if @item > 0  
 begin  
   if(@countyName<>(select countyname from @temp where id=@item))  
   begin  
    insert into @temp1(countyname,url,courtcategorynum,courtcategoryname,locationid_fk)  
    (select countyname,'',0,'',0 from @temp where id=@item)  
      
    select @countyName=countyname from @temp where id=@item  
   end  
   else  
   begin  
    insert into @temp1(countyname,url,courtcategorynum,courtcategoryname,locationid_fk)  
    (select '',url,courtcategorynum,courtcategoryname,locationid_fk from @temp where id=@item)  
      
    set @item=@item + 1  
   end  
 end  
  
end  
  
select * from @temp1    
    
--end ozair 3898 


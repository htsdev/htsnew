SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_PrintingGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_PrintingGrid]
GO


-- USP_Mailer_Get_PrintingGrid '10/4/07,10/3/07,10/2/07,10/1/07,9/30/07,9/29/07,9/28/07,9/27/07,9/26/07,9/25/07,9/24/07,9/23/07,'

create procedure [dbo].[USP_Mailer_Get_PrintingGrid] 
	(
	@SelectedDates varchar(200)
	)


as 


select * from dbo.Sap_Dates(@SelectedDates)





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****************
Alter By : Zeeshan Ahmed

Business Logic : This Procudure is used to add notes in case history if First Name , Last Name , Address Changed from the Court Information. 
		 Modify Procedure to compare 1st 5 digit of zipcode	
List Of Input Parameters :

 @TicketId	 : Ticket Id     
 @cFirstName : First Name
 @cLastName  : Last Name    
 @cAddress   : Address   
 @cState     : State   
 @cCity      : City   
 @cZip       : ZipCode


***************/ 
    
Alter procedure [dbo].[USP_PS_Update_CaseInformation]    
 (    
 @TicketId int,    
 @cFirstName varchar(50),    
 @cLastName varchar(50),    
 @cAddress varchar(100),    
 @cState int,    
 @cCity varchar(100),    
 @cZip varchar(20)  
)    
      
as    
    
declare   
 @tFirstName varchar(50),    
 @tLastName varchar(50),    
 @tAddress varchar(100),    
 @tCity varchar(100),    
 @tState int,    
 @tZip varchar(20),    
 @IsDifferent bit,    
 @IsAddessDifferent bit,    
 @Comments varchar(200),    
 @AddressComments varchar(200),    
 @EmpAbb  varchar(10),    
 @sName varchar(50)    
  
    
select @EmpAbb = abbreviation from tblusers where employeeid = 3992    
    
select @tFirstName =  ltrim(rtrim(t.firstname)),    
 @tLastName = ltrim(rtrim(t.lastname)),    
  
 @tAddress = ltrim(rtrim(t.address1)),    
 @tCity = ltrim(rtrim(t.city)),    
 @tState = t.stateid_fk,    
 @tZip = ltrim(rtrim(left(t.zip,5)))    
from  tbltickets t   
where  t.ticketid_pk = @TicketId    
    
select @sName = state from tblstate where stateid =  @tstate    
    
if @tfirstname <>  @cfirstname    
 set @IsDifferent = 1    
else    
 set @IsDifferent = 0    
    
if @tlastname <> @clastname    
 set @IsDifferent = 1    
else    
 set @IsDifferent = 0    
    
set @comments  =     ' Diff. in client first/last name: Name in court data was ' + @tlastname + ', '+ @tfirstname +' (' +  dbo.formatdateandtimeintoshortdateandtime(getdate())  + '-' +  @empabb +  ')'    
if @IsDifferent = 1    
begin    
 update  t     
 set  t.firstname = @cFirstName,    
  t.lastname = @cLastName,    
  t.generalcomments = isnull(t.generalcomments,'') + @comments,    
  t.trialcomments = isnull(t.trialcomments,'') + @comments    
 from  tbltickets t   
 where  t.ticketid_pk = @ticketid    
   
end    
    
    
if @tAddress <> @cAddress  or   @tCity <> @cCity or left(@tZip,5)  <> left(@cZip,5)   or   @tState <> @cState   
 set @IsAddessDifferent = 1    
else    
 set @IsAddessDifferent = 0    
   
set @AddressComments=' Diff. in client address: Address in court data was '+ @tAddress + ', ' + @tCity + ', ' + @sName + ' ' + @tZip +' (' +  dbo.formatdateandtimeintoshortdateandtime(getdate())  + '-' +  @empabb +  ')'    
if @IsAddessDifferent = 1    
begin    
 update  t     
 set  t.address1 = @cAddress,    
  t.address2 = '',    
  t.city = @ccity,    
  t.stateid_fk = @cstate,    
  t.zip = @czip,    
  t.generalcomments = isnull(t.generalcomments,'') + @AddressComments,    
  t.trialcomments = isnull(t.trialcomments,'') + @AddressComments    
 from  tbltickets t   
 where  t.ticketid_pk = @ticketid    
   
end    
    

Go
  

    
    
    
    
    
    
    
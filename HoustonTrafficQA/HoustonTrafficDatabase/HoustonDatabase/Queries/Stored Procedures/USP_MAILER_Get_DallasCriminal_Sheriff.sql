set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Dallas County Criminal Booking List letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/
  
--  USP_MAILER_Get_DallasCriminal_Sheriff  16,45,16, '5/1/08', '6/30/11',0  
ALTER procedure [dbo].[USP_MAILER_Get_DallasCriminal_Sheriff]    
 (    
 @catnum  int,    
 @LetterType  int,    
 @crtid   int,                                                     
 @startListdate  Datetime,    
 @endListdate  DateTime,                                                          
 @SearchType     int    
 )    
    
as    
    
-- DECLARING LOCAL VARIABLES FOR FILTERS....
declare @officernum varchar(50),                                                        
 @officeropr varchar(50),                                                        
 @tikcetnumberopr varchar(50),                                                        
 @ticket_no varchar(50),                                                                                                      
 @zipcode varchar(50),                                                                 
 @zipcodeopr varchar(50),                                                        
 @fineamount money,                                                                
 @fineamountOpr varchar(50) ,                                                                
 @fineamountRelop varchar(50),                                       
 @singleviolation money,                                                                
 @singlevoilationOpr varchar(50) ,                                                                
 @singleviolationrelop varchar(50),                                                        
 @doubleviolation money,                                                                
 @doublevoilationOpr varchar(50) ,                                                                
 @doubleviolationrelop varchar(50),                                                      
 @count_Letters int,                                      
 @violadesc varchar(500),                                      
 @violaop varchar(50)

    
-- DECLARING & INITIALIZING LOCAL VARIABLE FOR DYNAMIC SQL...            
declare  @sqlquery varchar(max)
set @sqlquery = ''                                                     
    
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......
Select  @officernum=officernum,                                                        
 @officeropr=oficernumOpr,                                                        
 @tikcetnumberopr=tikcetnumberopr,                                                        
 @ticket_no=ticket_no,                                                        
 @zipcode=zipcode,                                                        
 @zipcodeopr=zipcodeLikeopr,                                                
 @singleviolation =singleviolation,                                                   
 @singlevoilationOpr =singlevoilationOpr,                                                        
 @singleviolationrelop =singleviolationrelop,                                                        
 @doubleviolation = doubleviolation,                                                        
 @doublevoilationOpr=doubleviolationOpr,                                                                
 @doubleviolationrelop=doubleviolationrelop,                                      
 @violadesc=violationdescription,                                      
 @violaop=violationdescriptionOpr ,                                  
 @fineamount=fineamount ,                     
 @fineamountOpr=fineamountOpr ,                                                     
 @fineamountRelop=fineamountRelop                     
from  tblmailer_letters_to_sendfilters                                   
where  courtcategorynum=@catnum       
and lettertype = @LetterType                                                  
         
   
-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY 
-- INSERTING VALUES IN A TEMP TABLE....    
set @sqlquery=@sqlquery+'                                            
Select distinct tva.recordid,     
convert(datetime, convert(varchar(10),ta.listdate,101))as mdate,      
flag1 = isnull(ta.Flag1,''N'') ,                                                        
isnull(ta.donotmailflag,0) as donotmailflag,                                                         
count(tva.ViolationNumber_PK) as ViolCount,                                                        
isnull(sum(isnull(tva.fineamount,0)),0)as Total_Fineamount    
into #temp                                                              
FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA                         
INNER JOIN                         
dallastraffictickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid    

-- PERSON NAME MUST NOT BE EMPTY
where len(isnull(ta.firstname,'''') )> 0 and len(isnull(ta.lastname,'''') ) > 0   

--Yasir Kamal 6831 10/21/2009 
-- EXCLUDE RECORDS THAT HAVE BEEN ALREADY PRINTED OR PASS TO HIRE LETTER PRINTED IN PAST 7 DAYS.
-- Babar Ahmad 8597	07/13/2011 Changed number of days from 7 to 5 and included Bonded Out Mailer.
and ta.recordid not in (     
select n.recordid from tblletternotes n  inner join dallastraffictickets.dbo.tblticketsarchive a     
on a.recordid = n.recordid where lettertype = '+convert(varchar, @lettertype)+'    
union 
select l.recordid from tblletternotes l where (l.Lettertype in (46,73) and datediff(day, l.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0)
)    

-- FOR THE SELECTED DATE RANGE ONLY...
and datediff(day, ta.listdate, ''' + convert(varchar,@startlistDate,101)  + ''') <= 0    
and datediff(day, ta.listdate, ''' + convert(varchar,@endlistDate , 101) + ''') >= 0   


-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
and isnull(ta.ncoa48flag,0) = 0  


-- ONLY BOOKING LIST RECORDS...
--and tva.insertionloaderid = 18

--Rab Nawaz Khan 9326 05/30/2011 Only Booking List and felony cases
AND tva.insertionloaderid in (18, 35) 
-- end 9326 



-- Rab Nawaz Khan 8754 01/21/2011 Exclude Disposed Violations
and tva.violationstatusId <> 80
-- END 8754 
'  
    
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY....
if(@crtid=@catnum)    
	set @sqlquery=@sqlquery+'     
	and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum ='+convert(varchar(10),@catnum ) +')'              

-- IF PRINTING FOR A SINGLE COURT OF THE SELECTED COURT CATEGORY....
else    
	set @sqlquery=@sqlquery+'     
	and tva.courtlocation=' + convert(varchar,@crtid)    
    

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                        
if(@officernum<>'')                                                          
	set @sqlquery =@sqlquery + '      
	and  ta.officerNumber_Fk ' +@officeropr  +'('+   @officernum+')'                                                           
                                    
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                                         
if(@ticket_no<>'')                          
	set @sqlquery=@sqlquery + '     
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                                
                                      
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                      
if(@zipcode<>'')                                                                                
	set @sqlquery=@sqlquery+ '     
	and  ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'    
                                   
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....   
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                    
	set @sqlquery =@sqlquery+ '    
    and   tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
    
-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                    
	set @sqlquery =@sqlquery+ '    
    and  not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
    
set @sqlquery =@sqlquery + '     
group by                                                         
convert(datetime, convert(varchar(10),ta.listdate,101)),    
ta.flag1,                                                        
ta.donotmailflag,                                                        
tva.recordid    
having 1=1   
'                                    

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT      
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '  
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
      '                                  

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + '  
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )  
      '                                                      

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                  
if(@doubleviolation<>0 and @singleviolation<>0)                                  
	set @sqlquery =@sqlquery+ '  
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))  
        '                                    
                                                      
  
-- GETTING THE RESULT SET IN TEMP TABLE.....  
set @sqlquery=@sqlquery+                                         
'Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag   
into #temp1  from #temp a, dallastraffictickets.dbo.tblticketsviolationsarchive tva where a.recordid = tva.recordid 


-- Rab Nawaz Khan 8754 01/21/2011 Exclude Disposed Violations
and tva.violationstatusId <> 80
-- END 8754 

 '                                  
  
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')  
begin  
	-- NOT LIKE FILTER.......  
	if(charindex('not',@violaop)<> 0 )                
	 set @sqlquery=@sqlquery+'   
	 and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
	  
	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )                
	 set @sqlquery=@sqlquery+'   
	 and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
end  
  
  
set @sqlquery=@sqlquery+'    
-- tahir 6327 08/07/2009 need to rollback 4456 changes..
-- tahir ahmed 4456 07/23/2008 
-- get only those letters that have DRVIING WHILE INTOXICATED like violations...

--select distinct recordid into #templetter from dallastraffictickets.dbo.tblticketsviolationsarchive
--where violationdescription like ''driving while intoxicated%''

--delete from #temp1 where recordid not in (select recordid from #templetter)

'  
  

-- GETTING THE RESULT SET IN TEMP TABLE.....  
set @sqlquery=@sqlquery+'    
 select distinct recordid into #ViolFilter from dallastraffictickets.dbo.tblticketsviolationsarchive  
 where charindex(''justice of the peace'',violationdescription)>0   
 delete from #temp1 where recordid in (select recordid from #violfilter)  
 '  
 
 set @sqlquery=@sqlquery+                       
' -- Sabir Khan 7099 12/07/2009 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
 ' 

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery=@sqlquery+'     
  
Select count(distinct recordid) as Total_Count, mDate                                                      
into #temp2                                                    
from #temp1                                                    
group by mDate                                                         
    
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mDate                                                                                                  
into #temp3                                                    
from #temp1                                                        
where Flag1 in (''Y'',''D'',''S'')       
and donotmailflag =  0                                                     
group by mDate     
                                                        
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mDate                                                      
into #temp4                                                        
from #temp1                                                    
where Flag1  not in (''Y'',''D'',''S'')                                                        
and donotmailflag = 0                                                     
group by mDate     
                     
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mDate                                                     
 into #temp5                                                        
from #temp1            
where donotmailflag = 1       
--and flag1  in  (''Y'',''D'',''S'')                                                        
group by mDate     
           
-- OUTPUTTING THE REQURIED INFORMATION........                                                        
Select   listdate = #temp2.mDate ,    
        Total_Count,    
        isnull(Good_Count,0) Good_Count,    
        isnull(Bad_Count,0)Bad_Count,    
        isnull(DonotMail_Count,0)DonotMail_Count    
from #Temp2 left outer join #temp3     
on #temp2.mDate = #temp3.mDate    
                                                       
left outer join #Temp4    
on #temp2.mDate = #Temp4.mDate    
      
left outer join #Temp5    
on #temp2.mDate = #Temp5.mDate                                                     
      
where Good_Count > 0                  
order by #temp2.mDate desc                                                         
              
-- DROPPING THE TEMPORARY TABLES USED ABOVE.....                                                  
drop table #temp                                                    
drop table #temp1                                                    
drop table #temp2                                                        
drop table #temp3                                                    
drop table #temp4    
drop table #temp5    
'                                                        
                                                        
-- print @sqlquery    

-- EXECUTING THE DYNAMIC SQL QUERY...
exec(@sqlquery)    

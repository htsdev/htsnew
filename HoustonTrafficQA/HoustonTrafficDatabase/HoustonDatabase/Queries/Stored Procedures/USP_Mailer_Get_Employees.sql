SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_Employees]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_Employees]
GO

CREATE procedure dbo.USP_Mailer_Get_Employees    
    
as    
    
select employeeid, upper( abbreviation) as abbreviation from tblusers    

order by abbreviation    
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


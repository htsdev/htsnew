﻿ /******  
* Created By :	  Syed Muhammad Ozair.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get get patient correspondence.
* List of Parameter :
* Column Return :     
******/
ALTER PROCEDURE dbo.usp_HTP_GetCorrespondence
	@PatientId INT
AS
	SELECT ISNULL(p.ContractSentDate,'01/01/1900') AS ContractSentDate,
	       ISNULL(p.ContractReceivedDate,'01/01/1900') AS ContractReceivedDate,
	       ISNULL(p.ContractSignedSentDate,'01/01/1900') AS ContractSignedSentDate,
	       ISNULL(p.HippaSentDate,'01/01/1900') AS HippaSentDate,
	       ISNULL(p.HippaReceivedDate,'01/01/1900') AS HippaReceivedDate,
	       ISNULL(p.IsRejectionLetterSent,0) AS IsRejectionLetterSent
	FROM   Patient p
	WHERE p.Id=@PatientId
GO

GRANT EXECUTE ON dbo.usp_HTP_GetCorrespondence TO dbr_webuser
GO

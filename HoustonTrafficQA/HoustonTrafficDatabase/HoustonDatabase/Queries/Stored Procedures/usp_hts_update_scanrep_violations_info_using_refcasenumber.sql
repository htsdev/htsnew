ALTER procedure usp_hts_update_scanrep_violations_info_using_refcasenumber      
@refcasenumber varchar(20),      
@sequencenumber varchar(3),       
@courtnumber varchar(3),      
@courtdate datetime,      
@courtviolationstatusid int,      
--@courtid int,      
@docid int,      
@docnum int,      
@employeeid int      
as      
  
--set @refcasenumber  
      
declare @rowcount tinyint,      
@rowcountexist tinyint      
if exists(      
select * from tblticketsviolations      where
refcasenumber=  @refcasenumber      
and (datediff(day,courtdatescan,@courtdate) > 0 or courtdatescan is null)      
)      
 set @rowcount = 1      
else      
 set @rowcount = 0      
      
      
      
if exists(      
select * from tblticketsviolations      
where refcasenumber=  @refcasenumber      
and (datediff(day,courtdatescan,@courtdate) = 0 )      
)      
 set @rowcountexist = 1      
else      
 set @rowcountexist = 0      
      
      
update tblticketsviolations      
set       
--courtid = @courtid,      
--courtnumber = @courtnumber,      
--courtviolationstatusid = @courtviolationstatusid,      
--courtdate= @courtdate,      
courtnumberscan = @courtnumber,      
courtviolationstatusidscan = @courtviolationstatusid,      
courtdatescan= @courtdate,      
--courtviolationstatusidmain=@courtviolationstatusid,    
--courtnumbermain=@courtnumber,    
--courtdatemain=@courtdate,      
ScanDocID = @docid,      
ScanDocnum = @docnum,      
vemployeeid = @employeeid      
where 
refcasenumber=  @refcasenumber      
and (datediff(day,courtdatescan,@courtdate) > 0 or courtdatescan is null)      
      
      
      
      
      
if @rowcount > 0      
begin       
-- if left(@refcasenumber,1) = 'F'      
-- begin      
--  set @sequencenumber = '0'      
-- end      
 update tblScannedDocketCourtStatus set Updateflag = 1,      
 ticketnumber = @refcasenumber,      
 courtdate = @courtdate,      
 courtno = @courtnumber,violationstatus=@courtviolationstatusid,manualupdatedate=getdate()      
 where doc_id = @docid and docnum = @docnum and Updateflag = 0      
end      
else      
begin      
if @rowcountexist = 1      
       
 update tblScannedDocketCourtStatus set Updateflag = 2,      
 ticketnumber = @refcasenumber,      
 courtdate = @courtdate,      
 courtno = @courtnumber,violationstatus=@courtviolationstatusid,manualupdatedate=getdate()      
 where doc_id = @docid and docnum = @docnum and Updateflag = 0      
end    
go
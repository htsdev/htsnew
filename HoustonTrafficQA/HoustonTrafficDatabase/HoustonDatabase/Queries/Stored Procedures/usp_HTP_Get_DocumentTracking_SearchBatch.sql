/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get all the Scan batches detail.

List of Parameters:	
	@StartDate : from date 
	@EndDate : end date
	@CheckStatus : to display all non verified records.

List of Columns: 	
	BatchID : Batch id  
	Count : Image count
	ScanDate : when it is scanned in to system
	Verified : total no of verfied images of document 
	NoLeterID : will display the count of pages/images for which document id is not retrived. 
	Missing : missing pages if any
	

******/

ALTER Procedure dbo.usp_HTP_Get_DocumentTracking_SearchBatch

@StartDate datetime,
@EndDate datetime,
@CheckStatus int = 1

as


if @CheckStatus=0
begin
	select distinct sum(distinct documentbatchpagecount) as TotalPages, documentbatchid,scanbatchid_fk
	into #temp
	from tbl_htp_documenttracking_scanbatch_detail a inner join tbl_htp_documenttracking_scanbatch b
	on a.scanbatchid_fk=b.scanbatchid
	where b.scanbatchdate between @StartDate and Convert(datetime,@EndDate+' 23:59:59:998')   
	group by documentbatchid,scanbatchid_fk


	select sbd.scanbatchid_fk as BatchID,  
	count(sbd.scanbatchid_fk) as Count,  
	dbo.fn_DateFormat(sb.scanbatchdate,25,'/',':',1) as ScanDate,  --FAHAD 5167/5168 02/20/2009 Date format function added 
	(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk) as Verified,  
	(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where documentbatchid=0 and scanbatchid_fk=sbd.scanbatchid_fk) as NoLeterID,  
	case when count(sbd.scanbatchid_fk) - (select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk)=0 then 0 else 
	case when (select distinct sum(TotalPages)+(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk)-count(sbd.scanbatchdetailid) from #temp where scanbatchid_fk=sbd.scanbatchid_fk and documentbatchid<>0) < 0 then 0 
	else (select distinct sum(TotalPages)+(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk)-count(sbd.scanbatchdetailid) from #temp where scanbatchid_fk=sbd.scanbatchid_fk and documentbatchid<>0) end end as Missing  
	from tbl_htp_documenttracking_scanbatch sb inner join tbl_htp_documenttracking_scanbatch_detail sbd  
	on sb.scanbatchid = sbd.scanbatchid_fk  
	where sb.scanbatchdate between @StartDate and Convert(datetime,@EndDate+' 23:59:59:998')   
	group by sbd.scanbatchid_fk, sb.scanbatchdate  
	order by sbd.scanbatchid_fk desc  

drop table #temp
end

else if @CheckStatus=1
begin
	select distinct sum(distinct documentbatchpagecount) as TotalPages, documentbatchid,scanbatchid_fk
	into #temp1
	from tbl_htp_documenttracking_scanbatch_detail 
	where isverified<>1
	group by documentbatchid,scanbatchid_fk

	select sbd.scanbatchid_fk as BatchID,  
	count(sbd.scanbatchid_fk) as Count,  
	dbo.fn_DateFormat(sb.scanbatchdate,25,'/',':',1) as ScanDate,   
	(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk) as Verified,  
	(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where documentbatchid=0 and scanbatchid_fk=sbd.scanbatchid_fk) as NoLeterID,  
	case when (select distinct sum(TotalPages)+(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk)-count(sbd.scanbatchdetailid) from #temp1 where scanbatchid_fk=sbd.scanbatchid_fk and documentbatchid<>0) < 0 then 0 
	else (select distinct sum(TotalPages)+(select count(scanbatchdetailid) from tbl_htp_documenttracking_scanbatch_detail where isverified=1 and scanbatchid_fk=sbd.scanbatchid_fk)-count(sbd.scanbatchdetailid) from #temp1 where scanbatchid_fk=sbd.scanbatchid_fk and documentbatchid<>0) end as Missing  
	from tbl_htp_documenttracking_scanbatch sb inner join tbl_htp_documenttracking_scanbatch_detail sbd  
	on sb.scanbatchid = sbd.scanbatchid_fk  
	where sb.scanbatchid in (select distinct scanbatchid_fk from tbl_htp_documenttracking_scanbatch_detail where isverified=0)  
	group by sbd.scanbatchid_fk, sb.scanbatchdate  
	order by sbd.scanbatchid_fk desc 

drop table #temp1
end


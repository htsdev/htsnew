SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATE_STATUS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATE_STATUS]
GO

    
CREATE PROCEDURE [dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATE_STATUS]      
@TicketsViolationID int,      
@StatusID int,    
@EmpID int,   
@TicketID int,  
@Date datetime,  
@UpdateAllFlag bit  
      
as       
      
  
if @UpdateAllFlag = 1  
begin  
UPDATE tblTicketsViolations      
 set      
  CourtViolationStatusID = @StatusID,      
  CourtViolationStatusIDMain = @StatusID,      
  CourtViolationStatusIDScan = @StatusID,      
  VEmployeeID = @EmpID    
   
WHERE TicketsViolationID IN (select TicketsViolationID from tbl_hts_docketcloseout_snapshot where datediff(day,courtdate,@Date)=0 and TicketID_PK = @TicketID)--@TicketsViolationID    
end  
  
else      
begin  
UPDATE tblTicketsViolations      
 set      
  CourtViolationStatusID = @StatusID,      
  CourtViolationStatusIDMain = @StatusID,      
  CourtViolationStatusIDScan = @StatusID,      
  VEmployeeID = @EmpID    
WHERE TicketsViolationID = @TicketsViolationID    
end  
      
      
     

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


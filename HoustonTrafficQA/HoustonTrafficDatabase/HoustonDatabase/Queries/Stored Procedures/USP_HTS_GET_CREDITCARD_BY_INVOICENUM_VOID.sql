SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CREDITCARD_BY_INVOICENUM_VOID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CREDITCARD_BY_INVOICENUM_VOID]
GO



 CREATE procedure [dbo].[USP_HTS_GET_CREDITCARD_BY_INVOICENUM_VOID]        
@invnumber int      
--@ticketid int    --Getting credit card info by providing invoicenumber      
as          
select        
invnumber,        
amount,        
ccnum,        
expdate,        
cin,        
name,        
C.employeeid,        
address1 + ' ' + address2 as Address,        
city,        
S.state,        
zip,        
firstname,        
lastname  ,        
ticketid  ,     
transnum ,--if to void transaction of credit card    
cardtype
from tblticketscybercash C,tbltickets T,tblstate S          
    
where T.ticketid_pk = C.ticketid  and T.stateid_fk=S.stateid        
and invnumber = @invnumber        
--and rowid = @invnumber    
--and t.ticketid_pk = @ticketid    
    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


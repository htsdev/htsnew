SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Insert_NewOccupation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Insert_NewOccupation]
GO











CREATE  procedure dbo.usp_HTS_Insert_NewOccupation 
	(
	@OccDescription	varchar(50)
	)

as

declare	@IsExist	int,
	@ExistingId	int
select 	@IsExist = (
		select count(*) from tbloccupation 
		where lower(ltrim(rtrim(occdescription))) like lower(ltrim(rtrim(@OccDescription)))
		)

if @isExist = 0 
	begin
		insert	into tbloccupation (occdescription ) values (@occDescription)
		select max(occupationid) from tbloccupation
	end
else
	begin
		select distinct occupationid from tbloccupation 
		where lower(ltrim(rtrim(occdescription))) like lower(ltrim(rtrim(@OccDescription)))
	end











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


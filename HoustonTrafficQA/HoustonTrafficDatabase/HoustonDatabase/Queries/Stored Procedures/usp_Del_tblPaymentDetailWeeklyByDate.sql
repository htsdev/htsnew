USE [TrafficTickets]
GO
/****** Business Logic:
* This procedure is used to get weekly payment detail by spacified transaction date.
* Altered by: Sabir Khan
* Task ID: 10920
* Date: 05/27/2013
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


ALTER PROCEDURE [dbo].[usp_Del_tblPaymentDetailWeeklyByDate]

@TransDate DATETIME,
@branchID INT =0
AS
SET NOCOUNT ON
DELETE [dbo].[tblPaymentDetailWeekly] 

Where Convert(varchar(12),TransDate,101) = Convert(varchar(12), @TransDate,101) AND BranchID=@branchID


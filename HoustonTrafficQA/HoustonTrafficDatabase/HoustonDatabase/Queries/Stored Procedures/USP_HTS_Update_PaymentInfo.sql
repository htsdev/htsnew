SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_PaymentInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_PaymentInfo]
GO

        
         CREATE    procedure [dbo].[USP_HTS_Update_PaymentInfo]  --4343,4,'hello','cm',3992        
@ticketid int,          
@pretrialstatus tinyint,          
@comments varchar(500),          
@certifiedmailnumber varchar(30),    
@empid as int          
as    
    
if (right(@comments,1) <> ')' and left(@comments,1) <> '')       --if comments are updated              
  begin                   
   declare @empnm as varchar(12)                                
   select @empnm = abbreviation from tblusers where employeeid = @empid                          
   set @comments = @comments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' '                                                     
 update tbltickets        
 set          
  pymtcomments = @comments,     
  pretrialstatus = @pretrialstatus,        
 CertifiedMailNumber = @certifiedmailnumber              
 where ticketid_pk = @ticketid          
  end     
else    
  begin    
  update tbltickets        
 set          
 pretrialstatus = @pretrialstatus,        
 CertifiedMailNumber = @certifiedmailnumber              
 where ticketid_pk = @ticketid          
  end    
    
    
    
          
  
  
  
  
  
  
----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_DataSetMovePrev]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_DataSetMovePrev]
GO


CREATE procedure [dbo].[usp_WebScan_DataSetMovePrev]
@BathID int  
as  

Select   
 o.picid,  
 o.causeno,  
 convert(varchar(12),o.newcourtdate,101) as courtdate,  
 convert(varchar(12),o.todaydate,101) as todaydate,  
 o.time,  
 o.status,  
 o.location  
  
from tbl_webscan_pic p inner join   
  tbl_webscan_ocr o on   
  p.id=o.picid  
where   
 p.batchid=@BathID
order by o.picid desc


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


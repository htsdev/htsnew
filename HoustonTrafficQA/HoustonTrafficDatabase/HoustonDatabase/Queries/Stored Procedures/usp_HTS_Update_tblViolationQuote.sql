
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HTS_Update_tblViolationQuote]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HTS_Update_tblViolationQuote]
GO
--general comments updation removed from the scroipt 
 CREATe PROCEDURE [dbo].[usp_HTS_Update_tblViolationQuote]      
 @QuoteID numeric(18, 0),       
 @TicketID_FK varchar(12),       
 @QuoteResultID int,      
 @QuoteResultDate varchar(12),      
 @FollowUPID int,      
 @FollowUPDate datetime,      
 @FollowUpYN int,      
-- @GeneralComments varchar(1000),   khalid-- 2/23/08 2349   
 @CallBackDate varchar(12),      
 --@AppointmentDate varchar(12),      
 --@AppointmentTime varchar(12),      
 @CallBackTime varchar(12),
 @EmpID int     
 --@LastContactDate datetime      
 --@UNID numeric(18,0)=0 OUTPUT      
AS      
      
Declare @LastContactDate datetime      
Set @LastContactDate = GetDate()      
      
SET NOCOUNT ON      
UPDATE [dbo].[tblViolationQuote]      
SET       
 [TicketID_FK] = @TicketID_FK,      
 [QuoteResultID] = @QuoteResultID,      
 [QuoteResultDate] = @QuoteResultDate,      
 [FollowUPID] = @FollowUPID,      
 [FollowUPDate] = @FollowUPDate,      
 [FollowUpYN] = @FollowUpYN,           
 [CallBackDate] = @CallBackDate,      
 --[AppointmentDate] = @AppointmentDate,      
 --[AppointmentTime] = @AppointmentTime,      
 [CallBackTime] = @CallBackTime,      
 [LastContactDate] = @LastContactDate      
WHERE      
 [QuoteID] = @QuoteID    
  
---------------------------------------------------------  
--  khalid  23/2/08 3249 to add general comments procedure
--if (right(@GeneralComments,1) <> ')' )                         
-- begin                                             
--  declare @empnm1 as varchar(12)                                    
--                                               
--  select @empnm1 = abbreviation from tblusers where employeeid = @EmpID                                           
--                    
--  if len( @GeneralComments) > 0                                        
--  set @GeneralComments = @GeneralComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm1 + ')' + ' '                                            
--                                               
--  update tbltickets set  GeneralComments = @GeneralComments                                          
--  where ticketid_pk = @TicketID_FK                                             
-- end   

GO
GRANT EXEC ON [dbo].[usp_HTS_Update_tblViolationQuote] TO [dbr_webuser]
GO
     
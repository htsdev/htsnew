/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to delete the scan batch along with it's details

List of Parameters:	
	@ScanBatchID : scan bact id for which records will be deleted

******/  
  
CREATE procedure dbo.usp_HTP_Delete_DocumentTracking_DeleteBatch
    
@ScanBatchID as int     
    
    
as    
        
Begin    
     
 delete from tbl_HTP_DocumentTracking_ScanBatch_Detail where scanbatchid_fk = @ScanBatchID
     
 delete from tbl_HTP_DocumentTracking_ScanBatch where scanbatchid=@ScanBatchID
    
End    

go


grant exec on dbo.usp_HTP_Delete_DocumentTracking_DeleteBatch to dbr_webuser
go
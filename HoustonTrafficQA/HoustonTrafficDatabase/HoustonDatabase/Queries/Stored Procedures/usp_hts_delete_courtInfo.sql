ALTER procedure usp_HTS_Delete_CourtInfo    
@courtid int,  
@isDeleted int =0 output      
as        
declare @clientassigned int        
select @clientassigned = isnull(count(ticketid_pk),0) from tblticketsviolations where courtid  = @courtid        
if @clientassigned = 0        
begin        
 delete from tblcourts         
 where courtid = @courtid        
 Set @isDeleted=0  
 select @isDeleted  
end        
else        
begin  
Set @isDeleted=1  
select @isDeleted    
end  
GO 
﻿ USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_htp_getSignUpClient_byDate]    Script Date: 02/14/2013 16:10:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Business Logic:	This procedure is used by HTP application in HTP/Backroom that allows user to enter a date range [From] and [To]
				and the SP Fetch SignUp Clients those are hired within selected date range.   
				
List of Parameters:
	@from:			Initial date.
	@to:			Final date.
	
This SP Will show the following columns:	

	Ticket Number	
	CID
	Firstname		
	Lastname		
	DOB			
	CaseNumber				
	Hire Date		 		
	Client CDL
	address
	spenish
	Email Address
	Contact Type1
	Contact1
	Contact Type2
	Contact2
	Contact Type3
	Contact3
	DL Number
	US Citizen
	Vehicle Type
	ClientProfile

*******/

--exec [dbo].[usp_htp_getSignUpClient_byDate] '1/1/2012','2/1/2013'


CREATE PROC [dbo].[usp_htp_getSignUpClient_byDate] 

@from as datetime, 
@to as datetime
AS

BEGIN
	--MAIN QUERY FOR FETCHING RECORDS..
	SELECT distinct convert(varchar, ttp.RecDate,101) as [Hire Date] ,ttv.RefCaseNumber as [Ticket Number],isnull( tt.ContactID_FK,0) as [CID], tt.Firstname,tt.Lastname,convert(varchar, tt.DOB,101) as [DOB],isnull( ttv.casenumassignedbycourt,'') as [CaseNumber], isnull(tt.TicketID_PK,0) as [TicketID],
	Row_number() OVER (order by ttp.RecDate) as ID,
	case ISNULL(tt.CDLFlag, 0) when 0 then 'NO' else 'YES' end as [Client CDL],
	ISNULL(tt.Address1, '')+','+isnull(tt.City,'')+','+isnull(ts.state,'') +','+isnull(tt.Zip,'') as address,
	case tt.LanguageSpeak when 'ENGLISH' then 'NO' else 'YES' end as [spenish],
	isnull(tt.Email ,'N/A') as [Email Address],
	CASE WHEN LEN(ISNULL(tt.Contact1, '')) > 0 THEN
		case ISNUMERIC(SUBSTRING(tt.Contact1, 11,4)) 
			when 1 then
				SUBSTRING(tt.Contact1, 0,4) + '-' + SUBSTRING(tt.Contact1, 4,3) + '-' + SUBSTRING(tt.Contact1, 7,4) + ' X ' + SUBSTRING(tt.Contact1, 11,4)
			when 0 then
				SUBSTRING(tt.Contact1, 0,4) + '-' + SUBSTRING(tt.Contact1, 4,3) + '-' + SUBSTRING(tt.Contact1, 7,4) end
		ElSE 'N/A' END AS Contact1,
	ISNULL(tt.ContactType1, 0) AS ContactType1,
	CASE WHEN LEN(ISNULL(tt.Contact2, '')) > 0 THEN
		case ISNUMERIC(SUBSTRING(tt.Contact2, 11,4)) 
			when 1 then
				SUBSTRING(tt.Contact2, 0,4) + '-' + SUBSTRING(tt.Contact2, 4,3) + '-' + SUBSTRING(tt.Contact2, 7,4) + ' X ' + SUBSTRING(tt.Contact2, 11,4)
			when 0 then
				SUBSTRING(tt.Contact2, 0,4) + '-' + SUBSTRING(tt.Contact2, 4,3) + '-' + SUBSTRING(tt.Contact2, 7,4) end
		ElSE 'N/A' END AS Contact2,
	ISNULL(tt.ContactType2, 0) AS ContactType2,
	CASE WHEN LEN(ISNULL(tt.Contact3, '')) > 0 THEN
		case ISNUMERIC(SUBSTRING(ISNULL(tt.Contact3, 'N/A'), 11,4)) 
			when 1 then
				SUBSTRING(tt.Contact3, 0,4) + '-' + SUBSTRING(tt.Contact3, 4,3) + '-' + SUBSTRING(tt.Contact3, 7,4) + ' X ' + SUBSTRING(tt.Contact3, 11,4)
			when 0 then
				SUBSTRING(tt.Contact3, 0,4) + '-' + SUBSTRING(tt.Contact3, 4,3) + '-' + SUBSTRING(tt.Contact3, 7,4) end
		ElSE 'N/A' END AS Contact3,
	ISNULL(tt.ContactType3, 0) AS ContactType3,
	isnull(tt.DLNumber,'N/A')as [DL Number],
	case tt.isPR when 0 then 'YES'  when 1 then 'NO'   when 3 then 'N/A' else 'N/A' end as [US Citizen],
	case isnull(tt.VehicleType,0) when 1 then 'Commercial Vehicle' when 2 then 'Motor Cycle' when 3 then 'Car' else 'N/A' end as[Vehicle Type]
	into #temp
	from dbo.tblTickets tt inner join dbo.tblTicketsPayment ttp on ttp.TicketID=tt.TicketID_PK
			Inner join tblTicketsViolations ttv on ttv.TicketID_PK = tt.TicketID_PK
			inner join tblState ts  on ts.stateid=tt.stateid_fk
			inner join tblPaymenttype tp on tp.Paymenttype_PK=ttp.PaymentType
			
	where ISNULL(tt.Activeflag, 0) = 1 
	and ttp.PaymentVoid=0 
	and  (DATEDIFF(DAY, ttp.RecDate,@from)<=0 and DATEDIFF(DAY, ttp.RecDate,@to)>=0 )
	and ttp.PaymentType not in (0,8,10,98)
	and tt.Firstname not like 'test'
	and tt.Lastname not like 'test'
	order by [Hire Date] desc, tt.Firstname, tt.Lastname

	select MIN([Hire Date]) as RecDate, FirstName, lastName, address, DOB, MIN(ID) as ID
	into #MinRecDate
	from #temp
	group by FirstName, lastName, address, DOB

	--remove extra records..
	delete from #temp where ID NOT IN (select t.ID from #MinRecDate t)


	--adding contect discription columns 
	ALTER TABLE #temp
	Add [Contact Type1 Description] Varchar(50), [Contact Type2 Description] Varchar(50), [Contact Type3 Description] Varchar(50)

	update #temp
	set [Contact Type1 Description] =  case isnull(tc.Description,'N/A') when '---Choose---' then 'N/A' else tc.Description end
	from #temp Inner Join tblContactstype tc on tc.ContactType_PK = #temp.ContactType1

	update #temp
	set [Contact Type2 Description] =  case isnull(tc.Description,'N/A') when '---Choose---' then 'N/A' else tc.Description end
	from #temp Inner Join tblContactstype tc on tc.ContactType_PK = #temp.ContactType2

	update #temp
	set [Contact Type3 Description] = case isnull(tc.Description,'N/A') when '---Choose---' then 'N/A' else tc.Description end
	from #temp Inner Join tblContactstype tc on tc.ContactType_PK = #temp.ContactType3





	--getting requierd columns only..
	select 
	
	[CID],
	[Firstname] as [FIRSTNAME],
	[Lastname]as [LASTNAME],
	[DOB],
	[Vehicle Type] as [VEHICLE TYPE],
	[Hire Date] as [HIRE DATE], 
	[Client CDL] AS [HAS CDL],
	CASE WHEN LEN([Ticket Number]) = 0 THEN 'N/A' ELSE [Ticket Number] END AS [TICKET NUMBER],
	CASE WHEN LEN([CaseNumber]) = 0 THEN 'N/A' ELSE [CaseNumber] END AS [CASE NUMBER],
	[address] as [ADDRESS],
	[spenish] AS [SPANISH FLAG],
	[Contact Type1 Description] as [CONTACT TYPE1],
	[Contact1] as[CONTACT1],
	[Contact Type2 Description] as [CONTACT TYPE2],
	[Contact2] as [CONTACT2],
	[Contact Type3 Description] as [CONTACT TYPE3],
	[Contact3] as [CONTACT3],
	CASE WHEN LEN([Email Address]) = 0 THEN 'N/A' ELSE [Email Address] END AS [EMAIL ADDRESS],
	CASE WHEN LEN([DL Number]) = 0 THEN 'N/A' ELSE [DL Number] END AS [DL NUMBER], 
	[US Citizen] as [US CITIZEN],
	[TICKETID]

	
	

	into #empFinal1
	from #temp

	--getting final result
	select * from #empFinal1

	--deleting temp tables...
	drop table #MinRecDate 
	drop table #temp  
	drop table #empFinal1

END






GO
GRANT EXECUTE ON [dbo].[usp_htp_getSignUpClient_byDate] TO dbr_webuser
GO




﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 7/18/2012 6:36:26 PM
 ************************************************************/

/****************

Created by:	Hafiz Khalid Nawaz 
Date:		07/18/2012

Business Logic:
	The stored procedure is used by Houtson Traffic Program to update the 
	Cleint's/Prospect's ALR Hearing post hire questions.... 10288 TFS ticket

Input Parameters:
	@TicketID_PK:
	@EmpID:
	@ALRPriorAttorneyType:
	@ALRPriorAttorneyName:
	@CaseSummaryComments
	@ALROfficerName
    @ALROfficerbadge
    @ALRPrecinct
    @ALROfficerAddress
    @ALROfficerCity
    @ALROfficerStateID
    @ALROfficerZip
    @ALROfficerContactNumber
    @ALRIsIntoxilyzerTakenFlag
    @ALRIntoxilyzerResult
    @ALRBTOFirstName
    @ALRBTOLastName
    @ALRBTOBTSSameFlag
    @ALRBTSFirstName
    @ALRBTSLastName
    @ALRArrestingAgency
    @ALRMileage
    @ALRObservingOfficerName,
	@ALRObservingOfficerBadgeNo,
	@ALRObservingOfficerPrecinct,
	@ALRObservingOfficerAddress,
	@ALRObservingOfficerCity,
	@ALRObservingOfficerState,
	@ALRObservingOfficerZip,
	@ALRObservingOfficerContact,
	@ALRObservingOfficerArrestingAgency,
	@ALRObservingOfficerMileage,
	@IsALRHearingRequired,
	@ALRHearingRequiredAnswer
	

Output Columns:
	N/A

***************/    
                  
CREATE PROCEDURE [dbo].[Usp_HTS_UpdateALRPostHireQuestions]
	@TicketID_PK INT,
	@EmpID INT,
	@ALROfficerName VARCHAR(MAX),
	@ALROfficerbadge VARCHAR(MAX),
	@ALRPrecinct VARCHAR(MAX),
	@ALROfficerAddress VARCHAR(MAX),
	@ALROfficerCity VARCHAR(MAX),
	@ALROfficerStateID INT,
	@ALROfficerZip VARCHAR(MAX),
	@ALROfficerContactNumber VARCHAR(MAX),
	@ALRIsIntoxilyzerTakenFlag TINYINT,
	@ALRIntoxilyzerResult VARCHAR(MAX),
	@ALRBTOFirstName VARCHAR(MAX),
	@ALRBTOLastName VARCHAR(MAX),
	@ALRBTOBTSSameFlag BIT = NULL,
	@ALRBTSFirstName VARCHAR(MAX),
	@ALRBTSLastName VARCHAR(MAX),
	@ALRArrestingAgency VARCHAR(MAX),
	@ALRMileage FLOAT,
	@IsALRArrestingObservingSame BIT = NULL,
	@ALRObservingOfficerName VARCHAR(MAX) = NULL,
	@ALRObservingOfficerBadgeNo VARCHAR(MAX) = NULL,
	@ALRObservingOfficerPrecinct VARCHAR(MAX) = NULL,
	@ALRObservingOfficerAddress VARCHAR(MAX) = NULL,
	@ALRObservingOfficerCity VARCHAR(MAX) = NULL,
	@ALRObservingOfficerState INT = NULL,
	@ALRObservingOfficerZip VARCHAR(MAX) = NULL,
	@ALRObservingOfficerContact VARCHAR(MAX) = NULL,
	@ALRObservingOfficerArrestingAgency VARCHAR(MAX) = NULL,
	@ALRObservingOfficerMileage FLOAT = NULL,
	@IsALRHearingRequired BIT = NULL,
	@ALRHearingRequiredAnswer VARCHAR(MAX) = NULL
AS
	
	
	
	UPDATE tblTickets
	SET    
	       EmployeeIDUpdate = @EmpID,
	       ALROfficerName = @ALROfficerName,
	       ALROfficerbadge = @ALROfficerbadge,
	       ALRPrecinct = @ALRPrecinct,
	       ALROfficerAddress = @ALROfficerAddress,
	       ALROfficerCity = @ALROfficerCity,
	       ALROfficerStateID = @ALROfficerStateID,
	       ALROfficerZip = @ALROfficerZip,
	       ALROfficerContactNumber = @ALROfficerContactNumber,
	       ALRIsIntoxilyzerTakenFlag = @ALRIsIntoxilyzerTakenFlag,
	       ALRIntoxilyzerResult = @ALRIntoxilyzerResult,
	       ALRBTOFirstName = @ALRBTOFirstName,
	       ALRBTOLastName = @ALRBTOLastName,
	       ALRBTOBTSSameFlag = @ALRBTOBTSSameFlag,
	       ALRBTSFirstName = @ALRBTSFirstName,
	       ALRBTSLastName = @ALRBTSLastName,
	       ALRMileage = @ALRMileage,
	       
	       IsALRArrestingObservingSame = @IsALRArrestingObservingSame,
	       ALRObservingOfficerName = @ALRObservingOfficerName,
	       ALRObservingOfficerBadgeNo = @ALRObservingOfficerBadgeNo,
	       ALRObservingOfficerPrecinct = @ALRObservingOfficerPrecinct,
	       ALRObservingOfficerAddress = @ALRObservingOfficerAddress,
	       ALRObservingOfficerCity = @ALRObservingOfficerCity,
	       ALRObservingOfficerState = @ALRObservingOfficerState,
	       ALRObservingOfficerZip = @ALRObservingOfficerZip,
	       ALRObservingOfficerContact = @ALRObservingOfficerContact,
	       ALRObservingOfficerArrestingAgency = @ALRObservingOfficerArrestingAgency,
	       ALRObservingOfficerMileage = @ALRObservingOfficerMileage,
	       IsALRHearingRequired = @IsALRHearingRequired,
	       ALRHearingRequiredAnswer = @ALRHearingRequiredAnswer
	      
	WHERE  ticketid_pk = @ticketid_pk 
	
go

grant execute on dbo.[Usp_HTS_UpdateALRPostHireQuestions] to dbr_webuser
go   
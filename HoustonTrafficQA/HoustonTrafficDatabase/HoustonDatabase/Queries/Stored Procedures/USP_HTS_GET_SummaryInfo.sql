    
    
--   [USP_HTS_GET_SummaryInfo]    124164    
    
ALTER procedure [dbo].[USP_HTS_GET_SummaryInfo]     
          
          
          
 (            
 @TicketId_pk int            
 )            
            
as            
            
declare @PaidAmount money,            
 @AmountOwes money            
            
select @PaidAmount = (            
  select isnull(sum(isnull(chargeAmount,0)) ,0)             
  from tblticketspayment where ticketid =  @TicketId_pk          
  and paymentvoid = 0        
  and paymenttype not in (0,98,99,100)        
  )            
select @AmountOwes = calculatedtotalfee - @paidamount             
from  tbltickets where ticketid_pk =  @TicketId_pk          
            
            
 Select           
isnull(t.Lastname,'') as lastName,          
isnull(t.Firstname,'') as FirstName,     
isnull(t.MiddleName,'') as MiddleName,         
isnull(d.Description ,'')AS CaseStatus,          
CONVERT(varchar(20),v.courtdatemain,0) AS CourtDate,          
isnull(v.courtnumbermain,0) as CourtNum,          
isnull(c.CourtName,'') as CourtName,           
isnull(c.Address,'') AS CourtAddress,          
FirmAbb = (case when t.firmid = 3000 then '' else isnull(f.FirmAbbreviation,'' ) end ) ,          
isnull(v.remindercomments,'') as ConComments,           
isnull(t.GeneralComments,'') as GenComments,           
isnull(t.SettingComments,'') as SetComments ,           
isnull(t.TrialComments,'') as TriComments,    
(          
convert(varchar(50),isnull(t.Address1,'')) +  convert(varchar(50),isnull(', '+t.Address2,''))    +', '+     
case when isnull(t.City,'') <> '' then (convert(varchar(50),isnull(t.City,''))+', ') else '' end+     
case when isnull(ts.State,'') <> '' then (convert(varchar(50),isnull(ts.State,''))+', ') else '' end+     
case when isnull(t.zip,'') <> '' then (convert(varchar(50),isnull(t.zip,''))) else '' end    
) as Address,    
    
    
isnull(t.Midnum,'') as MidNumber,          
isnull(t.DLNumber,'') as DLNum,         
     
-- change by asghar for upation of case summary     
convert(varchar(10),isnull(t.DOB,''),101) as DOBirth,          
    
    
Convert(varchar,isnull(t.Contact1,'')) as Contact1,           
Convert(varchar,isnull(c3.Description,'')) as Contactype1,          
    
    
    
Convert(varchar,isnull(t.Contact2,'')) as Contact2,    
Convert(varchar,isnull(c1.Description,'')) as Contactype2,    
    
          
Convert(varchar,isnull(t.Contact3,'')) as Contact3,    
Convert(varchar,isnull(c2.Description,'')) as Contactype3,    
    
isnull(v.casenumassignedbycourt,'')as CauseNo,    
isnull(t.email,'') as email,    
isnull(left(t.LanguageSpeak,3),'') as Language,           
isnull((case t.CDLFlag           
 when 0 then 'No'          
 when 1 then 'Yes'          
end),'') as CDLFlag,           
isnull((case t.AccidentFlag           
 when 0 then 'No'          
 when 1 then 'Yes'          
end),'') as AccidentFlag,           
convert(varchar(20),isnull(t.calculatedtotalfee,0)) as TotFee,          
convert(varchar(20),isnull(t.BaseFeeCharge,0)) as Initial,          
convert(varchar(20),isnull(t.RerapAmount,0)) as Rerap,          
convert(varchar(20),isnull(t.ContinuanceAmount,0))as Continuance,     
convert(varchar(20),isnull(t.InitialAdjustment,0)) as InitialAdjustment,         
convert(varchar(20),isnull(t.Adjustment,0)) as Adjust,           
t.Addition,          
isnull( c3.Description ,'')AS Description1,           
isnull(c1.Description ,'')AS Description2,          
isnull(c2.Description ,'')AS Description3,            
convert(varchar(20),isnull(@PaidAmount,0)) as PaidAmount,            
convert(varchar(20),isnull(@AmountOwes,0)) as AmountOwes,          
isnull(t.City,'') as cities,          
isnull(t.Zip,'') as zipcode,          
isnull(ts.State,'') as states,          
isnull(o.firstname +' '+ o.lastname,'N/A') as OfficerName,          
isnull(o.officertype,'') as officertype,          
bond = ( case when t.bondflag = 1 then 'BOND' else '' end )          
          
FROM         dbo.tblEventFlags tf_1 INNER JOIN        
                      dbo.tblTicketsFlag tf ON tf_1.FlagID_PK = tf.FlagID RIGHT OUTER JOIN        
                      dbo.tblFirm f RIGHT OUTER JOIN        
                      dbo.tblCourts c RIGHT OUTER JOIN        
        dbo.tblTickets t LEFT OUTER JOIN      
        dbo.tblticketsviolations v ON t.ticketid_pk=v.ticketid_pk Left OUTER JOIN       
        dbo.tblContactstype c2 ON t.ContactType3 = c2.ContactType_PK LEFT OUTER JOIN        
                      dbo.tblContactstype c1 ON t.ContactType2 = c1.ContactType_PK LEFT OUTER JOIN        
                      dbo.tblContactstype c3 ON t.ContactType1 = c3.ContactType_PK LEFT OUTER JOIN        
                      dbo.tblOfficer o ON t.OfficerNumber = o.OfficerNumber_PK LEFT OUTER JOIN        
                      dbo.tblDateType d ON V.CourtViolationStatusIdMain = d.TypeID ON c.Courtid = V.CourtId ON f.FirmID = t.FirmID ON         
                      tf.TicketID_PK = t.TicketID_PK LEFT OUTER JOIN        
                      dbo.tblState ts ON t.Stateid_FK = ts.StateID        
             
          
WHERE   t.TicketID_PK =  @TicketId_pk          
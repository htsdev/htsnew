SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Add_tblPaymentDetailWeekly__backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Add_tblPaymentDetailWeekly__backup]
GO



CREATE PROCEDURE [dbo].[usp_Add_tblPaymentDetailWeekly]
	@EmployeeID int,
	@TransDate datetime,
	@ActualCash money,
	@ActualCheck money,
	@Notes varchar(1200)
AS
SET NOCOUNT ON
INSERT [dbo].[tblPaymentDetailWeekly]
(
	[EmployeeID],
	[TransDate],
	[ActualCash],
	[ActualCheck],
	[Notes]
)
VALUES
(
	@EmployeeID,
	@TransDate,
	@ActualCash,
	@ActualCheck,
	@Notes
)

	


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


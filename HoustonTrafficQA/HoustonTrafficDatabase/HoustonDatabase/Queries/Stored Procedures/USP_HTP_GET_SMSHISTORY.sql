
/*
* ALTER BY :- Rab Nawaz Khan 10914 06/04/2013
* Business Logic : This procedure returns information about clients to whom SMS has been send.
* Parameter :
*				@ticketid : Ticket id of client
				@smstype : Type of SMS 0 for Email 1 for SMS
*/

-- [dbo].[USP_HTP_GET_SMSHISTORY] 201229,1
ALTER PROCEDURE [dbo].[USP_HTP_GET_SMSHISTORY] --201229,1
@ticketid INT,
@smstype BIT
AS
SELECT	
	teh.Client,
	teh.Subject,
	teh.SentInfo,
	dbo.fn_DateFormat(teh.recdate,25,'/',':',1) AS recdate	,
	teh.Sentflag,
	teh.TicketID,
	teh.CourtDate,
	teh.CourtNumber,
	teh.resendby,
	teh.Smstype,
	teh.PhoneNumber,
	-- Rab Nawaz Khan 10914 05/28/2013 Alert Type Text has been changed for Set call and Reminder Call. . . 
	CASE WHEN teh.SmsCallType = 'Set Call' THEN 'Set Text'
		 WHEN teh.SmsCallType = 'Reminder Call' THEN 'Reminder Text'
		 ELSE teh.SmsCallType END AS SmsCallType,
	teh.TicketNumber,
	teh.recdate AS recdateforsort,
	-- Rab Nawaz Khan 10914 06/06/2013 Getting Send SMSs to clients and storing in temp table. . . 
	CASE WHEN LEN(LTRIM(RTRIM(ISNULL(teh.resendby, '')))) > 0 AND teh.SmsCallType NOT LIKE 'Manual' THEN CONVERT(VARCHAR(50), 'Resent')
		ELSE CONVERT(VARCHAR(50), 'Sent') END AS SendRecieved  
INTO #tmpHistory 	
FROM tblEmailHistory teh
WHERE ISNULL(teh.Smstype,0)=@smstype
AND teh.TicketID=@ticketid
ORDER BY recdateforsort DESC


-- Rab Nawaz Khan 10914 06/04/2013 Response of the clients has been displayed on SMS history page . . .  
INSERT INTO #tmpHistory
SELECT NULL AS Client,	   NULL AS Subject,       [Text] AS SentInfo,  TrafficTickets.dbo.fn_DateFormat(ReceivedDate,25,'/',':',1) AS recdate ,
       1 AS Sentflag,        TicketID AS TicketID,       NULL AS CourtDate,       NULL AS CourtNumber,       NULL AS resendby,      1 AS Smstype,
        SUBSTRING( FromNumber , 1, 3) + '-'+SUBSTRING(FromNumber, 4, 3)+ '-'+SUBSTRING(FromNumber, 7, 15) AS PhoneNumber,      
        'N/A' AS SmsCallType,    TicketNumber AS TicketNumber,
      ReceivedDate,     'Received' AS SendRecieved 
  FROM [ArticleManagementSystem].[dbo].[SMSDetails]
WHERE TicketID =  @ticketid

SELECT * FROM #tmpHistory ORDER BY recdateforsort DESC

DROP TABLE #tmpHistory
GO
GRANT EXECUTE ON [dbo].[USP_HTP_GET_SMSHISTORY] TO dbr_webuser



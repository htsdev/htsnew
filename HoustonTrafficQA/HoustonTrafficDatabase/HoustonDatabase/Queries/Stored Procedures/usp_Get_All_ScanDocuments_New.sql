SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_ScanDocuments_New]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_ScanDocuments_New]
GO
















--usp_Get_All_ScanDocuments_New 119962,"Continuance"





CREATE                PROCEDURE [dbo].[usp_Get_All_ScanDocuments_New] 

@TicketID varchar(30),
@DocType varchar(50)

AS


IF (@DocType="All")
Begin
/*	
	*/
	
	
	 
	SELECT  dbo.tblTicketsViolations.ScanDocID AS DOC_ID, 'Resets        ' AS DOCREF, 
                      dbo.tblTicketsViolations.UpdatedDate AS UPDATEDATETIME, dbo.tblTicketsViolations.ScanDocNum AS DOC_Num,
	LTRIM(dbo.tblTicketsViolations.RefCaseNumber)
                      + ' - ' + LTRIM(STR(dbo.tblTicketsViolations.SequenceNumber)) + '  - ' + dbo.tblCourtViolationStatus.ShortDescription + '  - ' + CONVERT(varchar(20), 
                      dbo.tblTicketsViolations.CourtDate, 101) + ' ' +  SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 1, 5) 
                      + ' ' + SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 13, 2) 
                      + '  - #' + dbo.tblTicketsViolations.CourtNumber + '  - ' + ISNULL(dbo.tblViolations.Description, '') AS ResetDesc, dbo.tblTicketsViolations.TicketsViolationID AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount
into #ScanDoc 
FROM         dbo.tblTicketsViolations LEFT OUTER JOIN
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
	WHERE     (dbo.tblTicketsViolations.ScanDocID IS NOT NULL)    AND  TicketID_PK = @TicketID
	Order By dbo.tblTicketsViolations.RefCaseNumber

/**  **/
	Insert into #ScanDoc
	SELECT   ScanRepos.dbo.DOCMAIN.DOC_ID, ScanRepos.dbo.DOCMAIN.DOCREF, ScanRepos.dbo.DOCPIC.UPDATEDATETIME,  '0' as DOC_Num, 'No Description' AS ResetDesc, ' ' AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount

	FROM         ScanRepos.dbo.DOCMAIN LEFT OUTER JOIN
             		         ScanRepos.dbo.DOCPIC ON ScanRepos.dbo.DOCMAIN.DOC_ID = ScanRepos.dbo.DOCPIC.DOC_ID
	WHERE     (ScanRepos.dbo.DOCMAIN.DOCSOURCE = @TicketID )
/** Tahir **/
	Insert into #ScanDoc
	SELECT   tblScanBook.ScanBookID AS DOC_ID, tblDocType.DocType, tblScanBook.UPDATEDATETIME,  '0' as DOC_Num, 'No Description'  AS ResetDesc, ' '  AS RefCaseNumber, '1' AS RecordType,tblscanbook.doccount AS ImageCount

	FROM      tblScanBook, tblDocType
	WHERE     tblScanBook.DocTypeID = tblDocType.DocTypeID 
	And  	  tblScanBook.TicketID = @TicketID

	select * from  #ScanDoc
end

ELSE IF (@DocType<> "<choose>")
begin

	SELECT  dbo.tblTicketsViolations.ScanDocID AS DOC_ID, 'Resets        ' AS DOCREF, 
	                      dbo.tblTicketsViolations.UpdatedDate AS UPDATEDATETIME, dbo.tblTicketsViolations.ScanDocNum AS DOC_Num,
		LTRIM(dbo.tblTicketsViolations.RefCaseNumber)
	                      + ' - ' + LTRIM(STR(dbo.tblTicketsViolations.SequenceNumber)) + '  - ' + dbo.tblCourtViolationStatus.ShortDescription + '  - ' + CONVERT(varchar(20), 
	                      dbo.tblTicketsViolations.CourtDate, 101) + ' ' +  SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 1, 5) 
	                      + ' ' + SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 13, 2) 
	                      + '  - #' + dbo.tblTicketsViolations.CourtNumber + '  - ' + ISNULL(dbo.tblViolations.Description, '') AS ResetDesc, dbo.tblTicketsViolations.TicketsViolationID AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount
		into #ScanDoc2 
		FROM         dbo.tblTicketsViolations LEFT OUTER JOIN
	                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK LEFT OUTER JOIN
	                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
		WHERE     (dbo.tblTicketsViolations.ScanDocID IS NOT NULL)    AND  TicketID_PK = 0
		Order By dbo.tblTicketsViolations.RefCaseNumber
	if @DocType = "Resets"
	BEGIN
		insert into #ScanDoc2
		SELECT  dbo.tblTicketsViolations.ScanDocID AS DOC_ID, 'Resets        ' AS DOCREF, 
	                      dbo.tblTicketsViolations.UpdatedDate AS UPDATEDATETIME, dbo.tblTicketsViolations.ScanDocNum AS DOC_Num,
		LTRIM(dbo.tblTicketsViolations.RefCaseNumber)
	                      + ' - ' + LTRIM(STR(dbo.tblTicketsViolations.SequenceNumber)) + '  - ' + dbo.tblCourtViolationStatus.ShortDescription + '  - ' + CONVERT(varchar(20), 
	                      dbo.tblTicketsViolations.CourtDate, 101) + ' ' +  SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 1, 5) 
	                      + ' ' + SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 13, 2) 
	                      + '  - #' + dbo.tblTicketsViolations.CourtNumber + '  - ' + ISNULL(dbo.tblViolations.Description, '') AS ResetDesc, dbo.tblTicketsViolations.TicketsViolationID AS RefCaseNumber, '0' AS RecordType, 1 AS ImageCount
		--into #ScanDoc2 
		FROM         dbo.tblTicketsViolations LEFT OUTER JOIN
	                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK LEFT OUTER JOIN
	                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
		WHERE     (dbo.tblTicketsViolations.ScanDocID IS NOT NULL)    AND  TicketID_PK = @TicketID
		Order By dbo.tblTicketsViolations.RefCaseNumber

	END



/** Tahir **/
	Insert into #ScanDoc2
	SELECT   tblScanBook.ScanBookID AS DOC_ID, tblDocType.DocType, tblScanBook.UPDATEDATETIME,  '0' as DOC_Num,  'No Description'   AS ResetDesc, ' ' AS RefCaseNumber, '1' AS RecordType,tblscanbook.doccount AS ImageCount

	FROM      tblScanBook, tblDocType
	WHERE     tblScanBook.DocTypeID = tblDocType.DocTypeID 
	And  	  tblScanBook.TicketID = @TicketID
	And       Lower(tblDocType.DocType) = Lower(@DocType)

	select * from  #ScanDoc2
end











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


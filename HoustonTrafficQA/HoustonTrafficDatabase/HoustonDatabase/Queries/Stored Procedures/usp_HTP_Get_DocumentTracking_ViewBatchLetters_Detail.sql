/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to fetch the details of a perticular verified scanned batch document

List of Parameters:	
	@DocumentBatchID : document id of the for which records will be fetched
	@ScanBatchID : scan batch id

List of Columns: 	
	PicName : picture name of those scanned docs for which document id is retrived/verifed

******/

CREATE Procedure [dbo].[usp_HTP_Get_DocumentTracking_ViewBatchLetters_Detail]

@DocumentBatchID int,
@ScanBatchID int

as

select convert(varchar,sbd.scanbatchid_fk) +'-'+ convert(varchar,sbd.scanbatchdetailid)  +'-'+ convert(varchar,sbd.documentbatchid) as PicName	
from	tbl_htp_documenttracking_scanbatch_detail sbd inner join tbl_htp_documenttracking_scanbatch sb
on	sbd.scanbatchid_fk=sb.scanbatchid inner join tbl_HTP_DocumentTracking_Batch b
on	sbd.documentbatchid=b.batchid inner join tbl_HTP_DocumentTracking_Documents d
on	b.documentid_fk=d.documentid
where	sbd.documentbatchid=@DocumentBatchID
and	sbd.scanbatchid_fk=@ScanBatchID
order by	sbd.documentbatchpageno

go

grant exec on [dbo].[usp_HTP_Get_DocumentTracking_ViewBatchLetters_Detail] to dbr_webuser
go
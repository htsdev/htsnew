USE [TrafficTickets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
Created By     : Sabir Khan.
Created Date   : 04/23/2013
Work Item ID   : 10729
Business Logic : This procedure is used to display NISI Compititor information. 
           
Parameter:       
    @sDate		: Start date   
    @eDate		: End Date				 
    @breakDown  : Break down of result.
				: possible values
				: Monthly
				: Quarterly
				: Yearly 
    @Top		: Top number of records
				: Possible value
				: 10, 20, 30 and 40  
*/ 
-- [dbo].[USP_HTP_NISICompititor] '01/01/2013','04/30/2013','Montly',10
CREATE PROCEDURE [dbo].[USP_HTP_NISICompititor] 
(  
@sDate DATETIME,
@eDate DATETIME,
@breakDown VARCHAR(20),
@Top INT
)
AS 
BEGIN
-- Get all cause number from Docket Extract that have "BOND AMOUNT TO BE PAID MODIFIED BY JUDGE" in docket entry violation description
SELECT REPLACE([Cause Number],' ','') AS causenumber,[NISI Amount Owed On Particular NISI] AS nisiPaid
into #temp FROM LoaderFilesArchive.dbo.HoustonDocketExtract_DataArchiveTable 
WHERE LTRIM(RTRIM([Docket Entry Violation DESCRIPTION]))  LIKE 'BOND AMOUNT TO BE PAID MODIFIED BY JUDGE'

-- Add index to temp table on cause number for increasing performance
CREATE NONCLUSTERED INDEX IX_TempCauseNo ON #temp(causenumber); 

select min(layout.[NISI Issue Date]) as NISIIssueDate,Replace(layout.[NISI Cause Number],' ','') as NISICauseNumber
into #temp2
from LoaderFilesArchive.dbo.NISI_DataArchiveTable layout
inner join #temp t ON t.causenumber = REPLACE(layout.[NISI Cause Number],' ','')
group by Replace(layout.[NISI Cause Number],' ','')

SELECT t.causenumber as DocketEntryCause, t.nisiPaid
into #temp3
from #temp t 
inner join #temp2 t2 on t2.NISICauseNumber = t.causenumber
where 
-- Filters for start date and End date on NISI issued date
DATEDIFF(DAY,t2.NISIIssueDate,@sDate)<=0 
AND 
DATEDIFF(DAY,t2.NISIIssueDate,@eDate)>=0

---- Cross reference check of cause number from the above result in NISI layout 8 data file.
SELECT distinct t.DocketEntryCause as DocketEntryCause,
t.nisiPaid AS nisiPaid, evnt.bondingcompanyname AS bondingCompanyName 
INTO #temp4 FROM LoaderFilesArchive.dbo.tblEventExtractTemp evnt INNER JOIN #temp3 t
ON t.DocketEntryCause = replace(evnt.causenumber,' ','')
WHERE
-- Records in the event should have bonding number or attorney name. 
(ltrim(rtrim(isnull(evnt.bondingnumber,''))) <> '' OR LTRIM(RTRIM(isnull(evnt.attorneyname,''))) <> '')
-- sort by amount of forfeiture $'s owed from highest to lowest.

-- Getting the final NISI Competitor information
SELECT TOP(@Top) bondingCompanyName,round(SUM(nisiPaid),2) AS nisiPaid, COUNT(DocketEntryCause) AS nisiIssued
FROM #temp4 GROUP BY bondingCompanyName
order by SUM(nisiPaid) desc

-- Drop all temp tables.
DROP TABLE #temp
DROP TABLE #temp2
DROP TABLE #temp3
DROP TABLE #temp4
END
GO
GRANT EXECUTE ON [dbo].[USP_HTP_NISICompititor] TO dbr_webuser
GO



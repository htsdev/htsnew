SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PAYMENTSDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PAYMENTSDetail]
GO

        

         --sp_helptext USP_HTS_GET_PAYMENTSDetail 12421  
  
 --Procedure to get payment details for displaying on payment info page    
CREATE Procedure [dbo].[USP_HTS_GET_PAYMENTSDetail]  --12924                    
@ticketID int                      
as                  
                  
SELECT                     
 TP.InvoiceNumber_PK,                  
 TP.paymentvoid as voidflag,              
 TP.TicketID,                   
 convert(varchar(20),TP.ChargeAmount)AS Paid,              
-- convert(varchar(10),TP.RecDate,101)as recdate,                   
 TP.RecDate,    
 tblPaymenttype.Description,        
 UPPER (tblusers.abbreviation) AS abbreviation,      
 isnull (tblticketscybercash.transnum,'')as transnum          
                   
FROM         tblTicketsPayment TP INNER JOIN                  
                      tblPaymenttype ON TP.PaymentType = tblPaymenttype.Paymenttype_PK         
 inner join tblusers on TP.employeeid=tblusers.employeeid                
 left outer join tblticketscybercash on TP.invoicenumber_pk= tblticketscybercash.invnumber              
         
               
WHERE     (TP.TicketID =@ticketID)   and           
         TP.paymenttype<>99       
order by tp.recdate     
    
   
  
  
  
  
  
  

----------------------------------------------------------------------------------------------      

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


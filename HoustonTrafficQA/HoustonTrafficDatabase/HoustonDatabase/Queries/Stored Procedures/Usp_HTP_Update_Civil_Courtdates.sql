
GO
/****** Object:  StoredProcedure [dbo].[Usp_HTP_Update_Civil_Courtdates]    Script Date: 09/15/2008 10:09:11 ******/
-- Noufil 4747
/******   
Create by:  Noufil Khan  
Business Logic : This Procedure Update the Court Date and Court Violation status to Pre-Trial for the Civil Cases
  
List of Parameters:   
	@courtdatemain : COurt Date which will be update.
	@ticketsviolationid : Ticket Violation ID of the Case
	@ticketid_pk : Ticket id of the Case
  
List of Columns:    
 isfound : if found then 1 else 0  
  
******/  

alter procedure [dbo].[Usp_HTP_Update_Civil_Courtdates]

@courtdatemain datetime,  
@ticketsviolationid int,  
@ticketid_pk int  
as  
	update tblticketsviolations  
		set courtdate =@courtdatemain,  
		courtviolationstatusid=101  
	where ticketsviolationid=@ticketsviolationid  
		and ticketid_pk=@ticketid_pk 

select @@rowcount

go

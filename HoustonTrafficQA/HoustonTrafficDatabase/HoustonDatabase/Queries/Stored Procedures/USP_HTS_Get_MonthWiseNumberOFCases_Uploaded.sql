SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_MonthWiseNumberOFCases_Uploaded]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_MonthWiseNumberOFCases_Uploaded]
GO

-- USP_HTS_Get_MonthWiseNumberOFCases_Uploaded '4/1/06', '3/31/07'
CREATE procedure dbo.USP_HTS_Get_MonthWiseNumberOFCases_Uploaded
	(
	@fromdate datetime,
	@todate datetime
	)
as 

set @todate = @todate + ' 23:59:59.998'

declare @temp table ( midnumber varchar(20), listdate datetime, violcount int)
insert into @temp
select t.midnumber, convert(varchar(10),t.listdate,101),  count(v.ticketnumber_pk+convert(varchar(10), violationnumber_pk))
from tblticketsarchive t, tblticketsviolationsarchive v
where t.recordid = v.recordid
and datediff(day, t.listdate, @fromdate) <= 0
and datediff(day, t.listdate, @todate) >= 0
and v.courtlocation in (3001,3002,3003)
group by t.recordid, t.midnumber, convert(varchar(10),t.listdate,101)

-- MULTIPLE VIOLATIONS......
select	year(listdate) as [year], 
		month(listdate) as [Month] ,
		'Multiple Violation' as [Type], 
		count(distinct midnumber) as [Client Count], 
		sum (violcount) as [Violation Count]
into #temp
from @temp 
where violcount > 1
group by year(listdate), month(listdate)


-- SINGLE VIOLATIONS......
insert into #temp
select	year(listdate) as [year], 
		month(listdate) as [Month] ,
		'Single Violation' as [Type],
		count(distinct midnumber) as [Client Count], 
		sum (violcount) as [Violation Count]
from @temp 
where violcount = 1
group by year(listdate), month(listdate)

alter table #temp add [day] int

update #temp set [day] = 1 

select	datename(month, convert(varchar,[month])+'/'+convert(varchar,[day])+'/'+convert(varchar,[year])) + '-' + 
		datename(YEAR, convert(varchar,[month])+'/'+convert(varchar,[day])+'/'+convert(varchar,[year]) )as  [Upload Month],
		[Type],
		[Client Count],
		[Violation Count]
from	#temp 
order by [Type] desc, [year], [month]
	

/*
select	datename(month, [month]) + '-'+ datename(year, [year]),
		[Type],
		[Client Count],
		[Violation Count]
from	#temp
order by [Type] desc , [month]

drop table #temp
*/
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


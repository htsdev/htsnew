SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_LettersInfoTemp]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_LettersInfoTemp]
GO

CREATE procedure dbo.USP_Mailer_Get_LettersInfoTemp


as

select distinct b.empid, b.lettertype, b.courtid, c.courtcategoryname, l.lettername, e.abbreviation
from tblbatchletter b, tblcourtcategories c, tblletter l, tblusers e
where b.empid = e.employeeid
and b.lettertype = l.letterid_pk
and b.courtid = c.courtcategorynum


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


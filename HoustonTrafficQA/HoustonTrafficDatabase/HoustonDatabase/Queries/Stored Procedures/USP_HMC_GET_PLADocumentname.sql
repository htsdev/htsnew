SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_GET_PLADocumentname]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_GET_PLADocumentname]
GO



CREATE procedure [dbo].[USP_HMC_GET_PLADocumentname]   
  
as  
  
declare @File_name varchar(100)  
declare @Length int   
declare @PlA_filename varchar(100)  
  
set @File_name = Convert(varchar(10),(SELECT MAX( CONVERT( numeric, RIGHT(isnull(PLADocument,'P0000'),4))+1) from tbl_hmc_Efile))  
set @Length = 5 - len(@File_Name)  
set @PlA_filename = 'P' + REPLICATE('0',@Length) + @File_Name  
select @PlA_filename  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


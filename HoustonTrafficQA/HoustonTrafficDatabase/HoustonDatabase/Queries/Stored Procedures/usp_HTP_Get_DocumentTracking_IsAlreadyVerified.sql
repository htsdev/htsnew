﻿/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to check a document id if it is already verified or not.

List of Parameters:	
	@DocumentBatchID : document id of the for which records will be checked

List of Columns: 	
	IsAlreadyVerified : 0: not verified; 1: verified

******/

Create Procedure dbo.usp_HTP_Get_DocumentTracking_IsAlreadyVerified

@DocumentBatchID int

as

declare @IsAlreadyVerified int

set @IsAlreadyVerified=0

if(select count(batchid_fk) from tbl_htp_documenttracking_batchtickets where verifiedDate is not null and batchid_fk=@DocumentBatchID)>=1
  set @IsAlreadyVerified=1

select @IsAlreadyVerified as IsAlreadyVerified
 
go

grant exec on dbo.usp_HTP_Get_DocumentTracking_IsAlreadyVerified to dbr_webuser
go 
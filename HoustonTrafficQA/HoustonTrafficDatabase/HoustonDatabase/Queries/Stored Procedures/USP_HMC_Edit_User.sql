SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Edit_User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Edit_User]
GO



CREATE Procedure [dbo].[USP_HMC_Edit_User]  
 @Uid varchar(20)   
as    
 Select * from tbl_HMC_Users where Uid = @Uid


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


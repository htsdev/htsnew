/******  
Created by:		    Fahad Muhammad Qureshi
Business Logic :    This procedure is used to Set OR Update Thank You Email Sent Date in our DataBase.
TaskId:				6429     
List of Parameters: @TicketId :Id to update the records.
					@SendDate :Date on which ThankYou Email has been sent. 
    
******/

Alter PROCEDURE dbo.USP_HTP_UPDATE_ThankEmailSentDate
(
	@TicketId AS INT,
	@SendDate AS DATETIME
)
as
UPDATE tblTickets

SET
	--Afaq 7821 05/28/2010 Set ThankYouEmailReadDate = NULL for new emails.
	ThankYouEmailSentDate = @SendDate,ThankYouEmailReadDate = NULL

WHERE Ticketid_pk=@TicketId
USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Mailer_Send_DayBeforeArraignment]    Script Date: 10/21/2013 12:29:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for HMC Day Before Arraignment letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Abb:			Short abbreviation of employee who is printing the letter

******/
-- usp_Mailer_Send_DayBeforeArraignment 1, 127, 1, '10/15/2010,10/15/2010,' , 3992, 0 ,1,0
      
ALTER PROCEDURE [dbo].[usp_Mailer_Send_DayBeforeArraignment]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int ,
@isprinted bit                                
                                    
as                                    
                                          
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
declare @sqlquery varchar(max),
	@sqlquery2 varchar(max)  , @lettername varchar(50) -- Rab Nawaz Khan 9682 10/04/2011 Added to make the single template file for Appearance mailers 
SET @lettername = 'DAY BEFORE ARRAIGNMENT'

-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....        
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

-- INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
Select @sqlquery =''  , @sqlquery2 = ''

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                    
-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid,' 
if @searchtype = 0
	set @sqlquery = @sqlquery + '
	convert(varchar(10),ta.listdate,101) as listdate, '
else
	set @sqlquery = @sqlquery + '
	convert(varchar(10),tva.courtdate,101) as listdate, '

set @sqlquery = @sqlquery + '
count(tva.ViolationNumber_PK) as ViolCount,
isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount 
into #temp2                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 

INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 

-- GET ONLY ARRAIGNMENT (PRIMARY STATUS) CASES....
where  	tcvs.CategoryID=2 
-- Haris Ahmed 9952 if Phone Numbers are not null
and (ISNULL(TA.PhoneNumber, '''') != '''' or ISNULL(TA.WorkPhoneNumber, '''') != '''') 
-- ONLY VERIFIED AND VALID ADDRESSES
and 	Flag1 in (''Y'',''D'',''S'')

-- NOT MARKED AS STOP MAILING...
and 	donotmailflag = 0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0

--Muhammad Muneer 8465 10/28/2010 add the new functionality for the firt name and the last name
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0 
'               

-- IF PRINTING FOR ALL COURTS OF THE SELECTED CATEGORY.....          
if(@crtid=1 )              
	set @sqlquery=@sqlquery+' 
and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = 1 )'                  

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
if @crtid <> 1              
	set @sqlquery =@sqlquery +'                                  
and 	tva.courtlocation =  ' + convert(varchar(10),@crtid)                                 
                                  
-- GET RECORDS RELATED ONLY TO THE SELECTED LIST DATE RANGE.....
if @searchtype = 0
begin
	if(@startListdate<>'')                                    
		set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 
end

-- GET RECORDS RELATED ONLY TO THE SELECTED COURT DATE RANGE.....
else
begin
	if(@startListdate<>'')                                    
		set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.courtdate' ) 
end

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                    
begin                                    
set @sqlquery =@sqlquery +                                     
            '  
and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
end                                    
                                    
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                                    
set @sqlquery=@sqlquery+ '
and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                    

-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                          
set @sqlquery=@sqlquery+ ' 
and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                  
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end    

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr='not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end    



if @searchtype = 0
	set @sqlquery = @sqlquery + ' group by  ta.recordid,
	convert(varchar(10),ta.listdate,101) having 1=1  '
else
	set @sqlquery = @sqlquery + ' group by  ta.recordid,
	convert(varchar(10),tva.courtdate,101) having 1=1   '

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT                            
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                  
  
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                                                   


-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR ARRAIGNMENT 1 LETTER PRINTED IN PAST 5 DAYS FOR THE RECORD....
--set @sqlquery =@sqlquery+ ' 
--and ta.recordid not in (                                                                    
--	select	recordid from tblletternotes 
--	where 	lettertype ='+convert(varchar(10),@lettertype)+'  
--	union  
--	select recordid from tblletternotes 
--	where lettertype = 9 and datediff(day, recordloaddate, getdate()) <= 5   
--	)     
-- '

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR ARRAIGNMENT 1 LETTER PRINTED IN PAST 5 DAYS FOR THE SAME RECORD
-- tahir 5611 03/13/2009 fine tunning...
set @sqlquery =@sqlquery + '                                                  

delete from #temp2 
from #temp2 a inner join tblletternotes V 
on a.recordid = V.recordid  
where (v.lettertype = '+convert(varchar,@lettertype)+') or (datediff(day, recordloaddate, getdate()) <= 5)

'   

-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
set @sqlquery=@sqlquery+'                                    

Select	distinct ta.recordid, 
	a.listdate,
	ta.courtdate,
	ta.courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	tva.TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	S.state as state,
	c.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .
	-- case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,  
	isnull(tva.fineamount,0) as FineAmount, 
	a.ViolCount,
	a.Total_Fineamount 
into #temp                                          
from tblticketsarchive ta, tblticketsviolationsarchive tva, #temp2 a, tblcourts c, tblstate s
where ta.recordid = tva.recordid
and ta.recordid = a.recordid
and ta.courtid = c.courtid
and ta.stateid_fk = S.stateID

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0 
'

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
			BEGIN
				set @sqlquery=@sqlquery+'
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
				and	 not (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           	
			END
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )              
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+' 
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
				and	 (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           			
			END
	END


-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....                                         
set @sqlquery=@sqlquery+'

-- Sabir Khan 11460 10/01/2013 Attorney logic has been implemented.
TRUNCATE TABLE HMCCurrentMailerRecords

insert into HMCCurrentMailerRecords(recordid,violationstatusid,ViolCount,Total_Fineamount,mdate,FirstName,LastName,ADDRESS,city,STATE,FineAmount,violationdescription,CourtName,dpc,
dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,courtid,zipmid,donotmailflag,clientflag,zipcode,promotemplate,midnumber)

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription, CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode 
 -- tahir 5449 01/23/2009
 , dbo.GetPromoPriceTemplate(zipcode,'+convert(Varchar,@lettertype)+') AS promotemplate,midnumber  
  from #temp                                    

 
 exec USP_HTP_Update_IsPromoClient
 
select recordid,violationstatusid,ViolCount,Total_Fineamount,mdate,FirstName,LastName,ADDRESS,city,STATE,FineAmount,violationdescription,CourtName,dpc,
dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,courtid,zipmid,donotmailflag,clientflag,zipcode,promotemplate,IsPromo into #temp1 from HMCCurrentMailerRecords
TRUNCATE TABLE HMCCurrentMailerRecords

-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624


-- Rab Nawaz Khan 9704 09/28/2011 added to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 

 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1 WHERE ISNULL(FineAmount,0) = 0   
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid   

 '              
              
-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....         
if( @printtype<>0)         
set @sqlquery2 =@sqlquery2 +                                    
'
	Declare @ListdateVal DateTime,                                  
		@totalrecs int,                                
		@count int,                                
		@p_EachLetter money,                              
		@recordid varchar(50),                              
		@zipcode   varchar(12),                              
		@maxbatch int  ,
		@lCourtId int  ,
		@dptwo varchar(10),
		@dpc varchar(10), @IsPromo bit

	declare @tempBatchIDs table (batchid int)
	
	-- GETTING TOTAL LETTERS AND COSTING ......
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(*) from #temp1                                

	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                
    
	-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
	-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

			-- INSERTING RECORD IN BATCH TABLE......                               
			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                   

			-- GETTING BATCH ID OF THE INSERTED RECORD.....
			Select @maxbatch=Max(BatchId) from tblBatchLetter 
			insert into @tempBatchIDs select @maxbatch     

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....                                                
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc, IsPromo from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc , @IsPromo                                         
				while(@@Fetch_Status=0)                              
				begin                              

					-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc,isPromo)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc,@IsPromo)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc , @IsPromo                         
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            

	close ListdateCur  
	deallocate ListdateCur 

	-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName, 
	 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription, t.courtid, CourtName, t.mdate as courtdate, t.dpc, 
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb  ,promotemplate, '''+@lettername +''' as lettername, isfineamount, t.zipmid,
	 0 as midnumber, 0 as midnum, n.IsPromo 
	 from #temp1 t , tblletternotes n, @tempBatchIDs tb
	 where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
	 order by T.recordid 

-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
select @subject = convert(varchar(20), @lettercount) +  '' LMS HMC Day Before ARR Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'

else
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,  
	 -- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,  
	 dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb  ,promotemplate , '''+@lettername +''' as lettername, isfineamount,
	  0 as midnumber, 0 as midnum, courtid, mdate as courtdate, zipmid, IsPromo  
	 from #temp1 
	 order by recordid 
 '

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'
                                    
--print @sqlquery + @sqlquery2
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)



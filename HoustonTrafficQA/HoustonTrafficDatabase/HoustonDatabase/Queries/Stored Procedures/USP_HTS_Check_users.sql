SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Check_users]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Check_users]
GO


CREATE procedure USP_HTS_Check_users          
 @username varchar(20),      
 @employeeid int      
as              
  
 if @employeeid = 0              
  select  count(*) as exist      
  from  tblusers       
  where  username=@username      
  
 else              
  select  count(*) as exist      
  from  tblusers       
  where  username=@username       
  and employeeid <>  @employeeid      



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



/****** Object:  StoredProcedure [dbo].[USP_HTP_GetContractFollowUp]  ******/    
/*      
Created By	    : Fahad Muhammad Qureshi
Created Date	: 04/01/2008
TasK			: 5098      
Business Logic  : This procedure display information about all those clients (sctive flag 1)whose cases are in 
					AG courts (Family Law Case Type)excluding disposed and no hire cases
				    
Parameter:     
   @courtid  : searching criterea with respect to court    
   @datefrom : Date range that starts from    
   @dateto   : Date range that end to     
    
*/    

ALTER procedure [dbo].[USP_HTP_GetContractFollowUp] 
AS 
SELECT                                     
		t.TicketID_PK,    
		tv.ticketsviolationid,    
		tv.casenumassignedbycourt AS CAUSENUMBER,    
		tv.RefCaseNumber AS TICKETNUMBER,    
		t.Lastname AS LASTNAME,    
		t.Firstname AS FIRSTNAME,
		dbo.fn_dateformat(t.DOB,27,'/',':',1) as DOB,    
		t.DLNumber AS DL,     
		dbo.fn_dateformat(tv.CourtDateMain,30,'/',':',1)AS COURTDATE,    
		tv.CourtnumberMain AS COURTNO,  
		vs.ShortDescription AS STAT,                       
		c.ShortName AS LOC,    
		F.FirmAbbreviation  as COV ,   
		tv.CourtID as courtid,    
		case	                              
			when t.bondflag=1 then 'B'                              
		else ''                              
		end as bflag,        
		dbo.fn_dateformat(t.Contractfollowupdate,27,'/',':',1)  as FollowUpDate, 
		t.Activeflag,
		--Sabir Khan 5492 02/03/2009 Add case type column...
 		CT.CaseTypeName as CaseType

                           
	FROM dbo.tblTickets AS t     
		INNER JOIN dbo.tblTicketsViolations AS tv     
		ON t.TicketID_PK = tv.TicketID_PK     
		INNER JOIN dbo.tblCourtViolationStatus AS vs     
		ON tv.CourtViolationStatusIDmain = vs.CourtViolationStatusID     
		INNER JOIN dbo.tblCourts AS c     
		ON tv.CourtID = c.Courtid                                 
		left outer join  tblfirm F       
		On F.FirmID = Isnull(tv.CoveringFirmID,3000)     
		left outer join tblticketsextensions tx     
		on t.ticketid_pk=tx.ticketid_pk 
		--Sabir Khan 5492 join for Case Type...
		left outer join CaseType CT 
		on c.CaseTypeId =  CT.CaseTypeId       
		
	WHERE  
			t.Activeflag = 1
		AND
			tv.courtviolationstatusidmain not in (80,236)	
		AND
			(select count(DocTypeID) from tblScanBook WHERE TicketID =t.TicketID_PK and doctypeid =19 )= 0     
		AND --Ozair 5473 02/02/2009 uused case type id instead of court id  
		--Sabir Khan 5492 02/03/2009 Include Criminal,Civil and Family Law Cases....  
        c.CaseTypeId in (2,3,4)
        ORDER BY t.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause


			
			
			


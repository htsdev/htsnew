﻿
/******   
Created by:  Noufil Khan  
Business Logic : this procedure is used to set case status
  
List of Parameters:   
	@courtviolationstatus : Case Status Id 
	@ticketid_pk : Case Number 
	@empid : Employee id
******/

ALTER PROCEDURE [dbo].[USp_Htp_UpdateStatus]
	@courtviolationstatus INT,
	@ticketid_pk INT,
	@empid INT
AS
	-- Noufil 5807 06/03/2009 loop added to update record one by one bacuase trigger was not updating notes.
	DECLARE @count INT
	CREATE TABLE #temp
	(
		sno                INT IDENTITY,
		ticketviolationid  INT
	)
	
	
	SET @count = (
	        SELECT COUNT(ttv.TicketsViolationID)
	        FROM   tblTicketsViolations ttv
	        WHERE  ttv.TicketID_PK = @ticketid_pk
	    )
	
	DECLARE @TicketsViolationID INT	
	
	INSERT INTO #temp
	SELECT ttv.TicketsViolationID
	FROM   tblTicketsViolations ttv
	WHERE  ttv.TicketID_PK = @ticketid_pk
	
	WHILE (@count > 0)
	BEGIN	    
	    UPDATE tblticketsviolations
	    SET    courtviolationstatusidmain = @courtviolationstatus,
	           courtviolationstatusid = @courtviolationstatus,
	           courtviolationstatusidscan = @courtviolationstatus,
	           vemployeeid = @empid
	    WHERE  TicketsViolationID = (SELECT t1.ticketviolationid FROM #temp t1 WHERE t1.sno=@count)
	    SET @count = @count -1	    
	END
	DROP TABLE #temp


go

grant execute on [dbo].[USp_Htp_UpdateStatus]  to dbr_webuser

go
ALTER    procedure USP_HTS_Get_pending_trial_sets as      
    
select distinct     
  v.refcasenumber,     
  firstname,    
  lastname,    
  midnum,      
  CourtdateMain,    
  v.courtid,    
  totalfeecharged,      
  diff = case datediff(day,CourtdateMain,getdate())      
  when 0 then 0      
    else 1      
  end,    
  courtname,    
  v.courtnumbermain,    
  T.ticketid_pk,    
  V.violationnumber_pk,    
  Address    
 from       
  tbltickets T, tblticketsviolations V, tblcourts C,tblviolations S ,  
-- added by tahir  ahmed to get case status from underlying violation......  
 tblcourtviolationstatus cvs  
 where     cvs.courtviolationstatusid = v.courtviolationstatusidmain  
 and T.ticketid_pk = V.ticketid_pk    
  and v.courtid = C.courtid    
  and V.violationnumber_pk = S.violationnumber_pk    
--  and datetypeflag = 2    
 and cvs.categoryid = 2  
  and T.activeflag = 1    
  and violationtype in (0,2)    
  and datediff(day,v.CourtdateMain,getdate()) in (0,-1,-2)    
  and v.TicketsViolationID  = (      
      select top 1 b.TicketsViolationID from tblTicketsViolations  b       
       where  b.ticketid_pk = v.ticketid_pk       
       and  b.courtdatemain  = (     
    select min(courtdatemain) from tblticketsviolations a      
                 where a.ticketid_pk = b.ticketid_pk      
             and   a.courtdatemain > = getdate() - convert(varchar(12), getdate(),108)    
          )      
                   )     
  order by CourtdateMain --v.courtid,lastname,firstname,  
  --and A.ticketid_pk not in (select distinct ticketid_pk from tblticketstrialarr where       
  --datetype =2)    
--select getdate() - convert(varchar(12), getdate(),108)    
go 
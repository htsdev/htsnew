SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_ThreeDaysBeforeCourtDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_ThreeDaysBeforeCourtDate]
GO

















CREATE PROCEDURE [dbo].[usp_Get_All_ThreeDaysBeforeCourtDate] 

--@TicketID_PK varchar(12)
@DateFrom datetime,
@DateTo datetime

AS
SELECT DISTINCT 
                      dbo.tblViolationQuote.QuoteID, CONVERT(varchar(12), dbo.tblViolationQuote.LastContactDate, 101) AS LastContactDate, CONVERT(Int, GETDATE() 
                      - dbo.tblViolationQuote.LastContactDate) AS Days, dbo.tblUsers.Firstname + ' ' + dbo.tblUsers.Lastname AS EmpName, 
                      dbo.tblTickets.Firstname + ' ' + dbo.tblTickets.Lastname AS Customer, CONVERT(varchar(12), dbo.tblTickets.ContactDate, 101) AS ContactDate, 
                      CONVERT(decimal(8,2), dbo.tblTickets.calculatedtotalfee) as calculatedtotalfee, dbo.tblViolationQuote.QuoteComments, tblQuoteResult_2.QuoteResultDescription, dbo.tblViolationQuote.FollowUpYN, 
                      dbo.tblTickets.TicketID_PK, REPLACE(tblQuoteResult_1.QuoteResultDescription, '---------------------', ' ') AS FollowUpStatus, CONVERT(varchar(12), 
                      dbo.tblTicketsViolations.CourtDateMain, 101) AS CourtDate,
	         dbo.tblViolationQuote.CallBackDate, dbo.tblViolationQuote.AppointmentDate, 
                      dbo.tblViolationQuote.CallBackTime, dbo.tblViolationQuote.AppointmentTime

FROM         dbo.tblViolationQuote LEFT OUTER JOIN
                      dbo.tblQuoteResult tblQuoteResult_1 ON dbo.tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID RIGHT OUTER JOIN
                      dbo.tblTicketsViolations RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK ON 
                      dbo.tblViolationQuote.TicketID_FK = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
                      dbo.tblUsers ON dbo.tblTickets.EmployeeID = dbo.tblUsers.EmployeeID LEFT OUTER JOIN
                      dbo.tblQuoteResult tblQuoteResult_2 ON dbo.tblViolationQuote.QuoteResultID = tblQuoteResult_2.QuoteResultID
WHERE     (dbo.tblTickets.Activeflag = 0)  AND (dbo.tblViolationQuote.FollowUpYN = 1) 
And  (Convert(datetime, dbo.tblViolationQuote.LastContactDate,101) <= Convert(datetime, (GETDATE() - 3) ,101) )
--AND (dbo.tblTickets.CourtDate BETWEEN GETDATE() AND GETDATE() + 3)
AND ( (CONVERT(datetime, dbo.tblTicketsViolations.CourtDateMain, 101) >= CONVERT(datetime, @DateFrom, 101) )
		And (CONVERT(datetime, dbo.tblTicketsViolations.CourtDateMain, 101) <= CONVERT(datetime, (@DateTo+1), 101)) 
And
	dbo.tblTicketsViolations.CourtDateMain IN
                          (SELECT     MAX(dbo.tblTicketsViolations.CourtDateMain)
                            FROM          dbo.tblTicketsViolations
                            WHERE      dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK))








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/******  
* Created By :	  Farrukh Iftikhar.
* Create Date :   08/10/2011 
* Task ID :		  9584
* Business Logic :  This procedure is used to get case type of a particular ticket
* List of Parameter :
			@TicketId
* Column Return :   
			TicketID_PK
			CaseTypeId  
******/

ALTER PROCEDURE [dbo].[USP_HTP_GetCaseTypeByTicketId]
	@TicketId INT
AS
	SELECT  t.CaseTypeId
	FROM   tblTickets t
	WHERE  t.TicketID_PK = @TicketId
GO

GRANT EXECUTE ON [dbo].[USP_HTP_GetCaseTypeByTicketId] TO dbr_webuser
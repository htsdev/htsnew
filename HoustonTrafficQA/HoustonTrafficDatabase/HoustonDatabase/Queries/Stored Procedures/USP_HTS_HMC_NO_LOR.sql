﻿/**********
Alter By : Waqas Javed 5653 03/25/2009 Add Comments In Procedure  
  
Business Logic : This Report display client cases with following logics
*	Do not belong to HMC-L, HMC-D or HMC-M courts location. 
*	Case status should be in  "WAITING" or "ARRAIGNMENT" status. 
*	"Letter of Rep" should not be printed for the case. 
*	Case should not have "Not On System" flag active. 
*	Case type should be Traffic. 
*	Records will be sorted by follow up date in ascending order. 
  
List Of Parameters :   
  
List Of Columns :  
  
 CauseNumber,       
 LastName,  
 FirstName,  
 StatusID,  
 CourtDate,  
 CourtRoom,  
 TicketNumber,  
 Status,  
 TicketID_PK,  
 CourtID,  
 CourtTime,
 LORFollowUpDate  
  
Last Change Detail : Noufil 4895 10/04/2008 IF not on system flag active then remove it from the report.    
  
**********/  
-- USP_HTS_HMC_NO_LOR 1,0  
ALTER PROCEDURE [dbo].[USP_HTS_HMC_NO_LOR] 
--Waqas 5653 03/21/2009 LOR FollowUp Date in Validation email
	@Validation INT = 0,
	@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	SELECT tv.casenumassignedbycourt AS CauseNumber,
	       t.Lastname AS LastName,
	       t.Firstname AS FirstName,
	       tv.CourtViolationStatusIDmain AS StatusID,
	       tv.CourtDateMain AS CourtDate,
	       tv.CourtNumbermain AS CourtRoom,
	       tv.RefCaseNumber AS TicketNumber,
	       cvs.ShortDescription AS STATUS,
	       t.TicketID_PK,
	       tv.CourtID,
	       dbo.Fn_FormateTime(tv.CourtDateMain) AS CourtTime 
	       --Waqas 5653 03/21/2009 LOR FollowUp Date  
	       ,
	       CASE CONVERT(VARCHAR, ISNULL(t.LORFollowUpDate, ' '), 101)
	            WHEN '01/01/1900' THEN ' '
	            ELSE CONVERT(VARCHAR, t.LORFollowUpDate, 101)
	       END AS LORFollowUpDate
	FROM   dbo.tblTickets AS t
	       INNER JOIN dbo.tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN dbo.tblCourtViolationStatus AS cvs
	            ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID
	                --Ozair 8005 07/22/2010 Query optimized to get data in minimum amount of time	                
	                
	       INNER JOIN tblCourts tc
	            ON  tc.Courtid = tv.CourtID
	WHERE  --Ozair 8005 07/22/2010 Query optimized to get data in minimum amount of time
	       -- NOt on System Flag not Active
	       (
	           SELECT COUNT(*)
	           FROM   tblticketsflag
	           WHERE  ticketid_pk = t.TicketID_PK
	                  AND flagid = 15
	       ) = 0 
	       --Yasir Kamal 6501 09/01/2009  remove cases if Lor Confirmation Type Document is Uploaded.
	       AND (
	               (ISNULL(tv.CoveringFirmID, 3000) <> 3043)
	               OR (
	                      ISNULL(tv.CoveringFirmID, 3000) = 3043
	                      AND NOT EXISTS (
	                              SELECT tsb.TicketID
	                              FROM   tblScanBook tsb
	                              WHERE  tsb.TicketID = t.TicketID_PK
	                                     AND tsb.DocTypeID = 18
	                          )
	                  )
	           )
	           --Waqas 5653 03/21/2009 sort by follow up date...
	       AND (
	               (@Validation = 0 OR @ValidationCategory=2)  -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
	               OR (
	                      (@Validation = 1 OR @ValidationCategory=1) AND DATEDIFF(DAY, GETDATE(), ISNULL(LORFollowUpDate, '')) <= 0 -- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	               )
	           )
	       AND (
	       			-- Rab Nawaz 10005 01/19/2011 Added the Jury Trail and Judge Trail Statuses. (4 for Jury Trail and 5 for Judge Trail) 
	               (cvs.CategoryID IN (1, 2, 3, 12, 4, 5)) -- Haris Ahmed 9931 12/01/2011 Add Pre Trail Id (1 - WAITING, 2 - ARRAIGNMENT, 3 - PRE-TRIAL, 12 - BOND
	               OR tv.CourtViolationStatusIDmain = 135
	           ) 
	           --ozair 5767 05/06/2009 included lor print check (not printed)
	       AND t.IsLORPrinted = 0
	           --Sabir Khan 5202 11/24/2008 To get only traffic cases only
	       AND t.CaseTypeId = 1
	           --only Clients
	       AND t.activeflag = 1
	           --excluding HMC Courts
	           --Fahad 10378 01/09/2013 HMC-W alos excluded along other HMC Courts.
	       AND tc.courtid NOT IN (3001, 3002, 3003, 3075) -- Haris Ahmed 9931 12/01/2011 3001 - Houston Municipal Court, 3002 - Houston Municipal - Mykawa, 3003 - Houston Municipal - Dairy Ash
	           --not a Criminal Court
	       AND tc.IsCriminalCourt = 0
	ORDER BY
	       t.LORFollowUpDate,
	       t.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
	       
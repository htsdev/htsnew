SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_GET_COURT_DETAILS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_GET_COURT_DETAILS]
GO


CREATE PROCEDURE USP_GET_COURT_DETAILS    
@CourtID int    
as    
SELECT     tblCourts.CourtName, tblCourts.Address, tblCourts.City, tblState.State, tblCourts.Zip, tblCourts.Phone   
FROM         tblCourts INNER JOIN    
                      tblState ON tblCourts.State = tblState.StateID    
WHERE tblCourts.CourtID = @CourtID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


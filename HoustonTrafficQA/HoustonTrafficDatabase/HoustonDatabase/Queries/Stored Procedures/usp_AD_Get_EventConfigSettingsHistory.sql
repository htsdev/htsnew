/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get the event config settings history. 
				

List of Columns:
	recorddatetime:
	empname:
	eventtypename:
	notes
			
*******/


Create procedure dbo.usp_AD_Get_EventConfigSettingsHistory

as

select sh.recorddatetime,upper(u.lastname)+', '+upper(firstname) as empname,es.eventtypename,sh.notes
from autodialerconfigsettingshistory sh inner join autodialereventconfigsettings es
on sh.eventtypeid_fk=es.eventtypeid inner join tblusers u
on sh.employeeid=u.employeeid
order by sh.recorddatetime desc

go 

grant exec on dbo.usp_AD_Get_EventConfigSettingsHistory to dbr_webuser
go
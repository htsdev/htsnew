﻿   
  
ALTER  PROCEDURE [dbo].[USP_HTS_GET_SERVICETICKET_CATEGORIES] --3  
@CaseTypeId int = -1        
AS        
      
declare @tbl table       
(      
SNo int identity,      
ID int,      
Description varchar(50)      
)      
      
insert into @tbl      
Select Distinct stc.Id,stc.Description       from tblServiceTicketCategories stc       
inner join CaseTypeServiceCategory ctsc on stc.ID = ctsc.ServiceCategoryId  
inner join CaseType ct  on ctsc.CaseTypeId = ct.CaseTypeId   
-- Generate description with new serial number that are ordered    
where @CaseTypeId = -1 or ct.CaseTypeId = @CaseTypeId  -- Agha Usman 4271 06/27/2008
order by Description asc    
  
      
select * from @tbl      

    
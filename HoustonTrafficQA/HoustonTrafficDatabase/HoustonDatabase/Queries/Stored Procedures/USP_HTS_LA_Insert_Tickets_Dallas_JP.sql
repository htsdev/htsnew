 set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




alter PROCEDURE [dbo].[USP_HTS_LA_Insert_Tickets_Dallas_JP]
@Record_Date datetime,  
@List_Date as smalldatetime,
@Ticket_Number as varchar(20),
@Cause_Number as varchar(20),
@Name_First as varchar(50),  
@Name_Last as varchar(50),  
@Name_Middle as varchar(50),  
@Address as varchar(50),  
@City as varchar(35),  
@State as Varchar(10),
@ZIP as varchar(10),  
@DP2 varchar(2),
@DPC varchar(1),
@PrecinctID varchar(5),
@Violation_Date datetime,  
@Violation_Description as Varchar(2000),
@Violation_Code as Varchar(50),
@FineAmount as Money,
@BondAmount as Money,
@Officer_Name as varchar(50),
@Complainant_Agency as varchar(50),
@GroupID as Int,
@AddressStatus Char(1),
@Flag_SameAsPrevious as bit,
@PreviousRecordID as int,
@LoaderID as int,
@CurrentRecordID int output,
@Flag_Inserted int Output
AS
set nocount on  

Declare @StateID as Int
Declare @ViolationID as int
Declare @IDFound as int
Declare @TicketNum2 as varchar(20)
Declare @Court_Date as datetime
Declare @CourtID as int

Set @Name_First = left(@Name_First,20)
Set @Name_Last = left(@Name_Last,20)
Set @Name_Middle = left(@Name_Middle,1)
Set @Address = Replace(@Address, '  ', '')
Set @Flag_Inserted = 0
Set @List_Date = convert(datetime, convert(varchar(12), @List_Date, 101)) 
Set @CourtID = 0
Set @Court_Date = dateadd(day,14,@Violation_Date)

if datepart(dw,@Court_Date) = 7 -- If Saturday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 1
	end
else if datepart(dw,@Court_Date) = 1 -- If Sunday then make it Friday (Court date should be in working days)
	begin
		set @Court_Date = @Court_Date - 2
	end

Set @CourtID = (Select Top 1 CourtID from DallasTrafficTickets.dbo.TblCourts Where PrecinctID = @PrecinctID)

if len(@Ticket_Number) < 5
	Begin
		Set @Ticket_Number = @Cause_Number
	End

if @Cause_Number = ''
	Begin
		Set @Cause_Number = @Ticket_Number
	End

If @ZIP = '00000'
	Begin
		Set @ZIP = ''
	End
if @Violation_Code = ''
	Begin
		Set @Violation_Code = 'None'
	End

Set @ViolationID = null
Select Top 1 @ViolationID = ViolationNumber_PK, @Violation_Code = ViolationCode From DallasTrafficTickets.dbo.TblViolations Where (Description = @Violation_Description and CourtLoc = @CourtID and ViolationType = 9)

If @ViolationID is null
	Begin
		Insert Into DallasTrafficTickets.dbo.TblViolations (Description, Sequenceorder, IndexKey, IndexKeys, ChargeAmount, BondAmount, Violationtype, ChargeonYesflag, ShortDesc, ViolationCode, AdditionalPrice, Attorneyregularprice, Attorneybondprice, CourtLoc)
		Values(@Violation_Description, null, 2, 160, @FineAmount, @BondAmount, 9, 1, 'None', @Violation_Code, 0, 50, 95, @CourtID)
		Set @ViolationID = scope_identity()
	End

Set @StateID = (Select Top 1 StateID From DallasTrafficTickets.dbo.TblState Where State = @State)

If @Flag_SameAsPrevious = 0
	Begin
		Set @IDFound = null
		SELECT Top 1 @IDFound = RecordID, @TicketNum2 = TicketNumber From DallasTrafficTickets.dbo.tblTicketsArchive Where TicketNumber = @Ticket_Number
		If @IDFound is null
			Begin
				if Len(@Ticket_Number)<5
					Begin
						insert into DallasTrafficTickets.dbo.tblIDGenerator(recdate) values(getdate())
						select @Ticket_Number='DC' + convert(varchar(12),@@identity)
						Set @Cause_Number = @Ticket_Number
					End

				Insert Into DallasTrafficTickets.dbo.tblTicketsArchive (RecLoadDate, ListDate, CourtDate, ViolationDate, TicketNumber, MidNumber, FirstName, LastName, Initial, PhoneNumber, Address1, City, StateID_FK, ZipCode, CourtID, OfficerName, GroupID, Flag1, DP2, DPC, InsertionLoaderID)   
				Values(@Record_Date, @List_Date, @Court_Date, @Violation_Date, @Ticket_Number, @Ticket_Number, @Name_First, @Name_Last, @Name_Middle, '0000000000', @Address, @City, @StateID, @ZIP, @CourtID, @Officer_Name, @GroupID, @AddressStatus, @DP2, @DPC, @LoaderID)

				Set @CurrentRecordID = scope_identity()
			End
		Else
			Begin
				Set @CurrentRecordID = @IDFound
				if Len(@Ticket_Number) < 5
					Begin
						Set @Ticket_Number = @TicketNum2
					End
			End
	End
Else
	Begin
		Set @CurrentRecordID = @PreviousRecordID
	End

-- Adil 4875 27/09/2008
If @CourtID = 3086 -- Dallas JP 3-1
	Begin
		Set @FineAmount = (case When @FineAmount > 2 then @FineAmount - 2 else @FineAmount End)
	End

Set @IDFound = null
Set @IDFound = (Select top 1 RowID From DallasTrafficTickets.dbo.tblTicketsViolationsArchive
			Where 
			(TicketNumber_PK = @Ticket_Number))

If @IDFound is null
	Begin
		Insert Into DallasTrafficTickets.dbo.tblTicketsViolationsArchive (TicketNumber_PK, ViolationNumber_PK, CauseNumber, CourtDate, TicketViolationDate, UpdatedDate, ViolationDescription, ViolationCode, FineAmount, BondAmount, CourtLocation, RecordID, ViolationStatusID, LoaderUpdateDate, InsertionLoaderId, GroupID, Complainant_Agency)
		Values(@Ticket_Number, @ViolationID, @Cause_Number, @Court_Date, @Violation_Date, Getdate(), @Violation_Description, @Violation_Code, @FineAmount, @BondAmount, @CourtID, @CurrentRecordID, 116, getdate(), @LoaderID, @GroupID, @Complainant_Agency)
		Set @Flag_Inserted = 1
	End





/*
* Created By: Sabir Khan
* Task:		  9711
* Date:		  11/17/2011
* 
* Business Logic: This procedure is used to get all promotional scheme with advertisment mailer and promotional zip codes.
* 
*/
CREATE PROCEDURE [dbo].[USP_HTP_GetAllPromoZipCodes]
	
AS
BEGIN
SELECT DISTINCT ps.SchemeDescription AS [Scheme Description],
       ps.MessageForMatterPage AS [Matter Page Message],
       '$ '+ CONVERT(varchar,LEFT(ps.SchemePrice,LEN(ps.SchemePrice))) AS [Scheme Price],
       --'$ '+ CONVERT(varchar,LEFT(ISNULL(ps.BondPrice,''),LEN(ISNULL(ps.BondPrice,'')))) AS [Bond Price],            
       cc.CourtCategoryName + ' - ' + l.LetterName AS Mailer,
       [dbo].[fn_hts_get_AllPromoZipCodes](ps.SchemeId,pm.SchemeMailerId,l.LetterID_PK,cc.CourtCategorynum) AS [Zip Codes],
       CASE ps.IsActiveScheme WHEN 1 THEN 'Yes' ELSE 'No' END [Active Scheme]
FROM   dbo.PromotionalSchemes ps
       INNER JOIN dbo.PromotionalSchemes_MailerType pm
            ON  ps.SchemeId = pm.SchemeId
       INNER JOIN dbo.PromotionalSchemes_ZipCodes pz
            ON  pm.SchemeMailerId = pz.SchemeMailerId
       INNER JOIN dbo.tblLetter l
            ON  pm.MailerType = l.LetterID_PK
       INNER JOIN dbo.tblCourtCategories cc
            ON  l.courtcategory = cc.CourtCategorynum
WHERE  LEN(ISNULL(pz.ZipCode, '')) > 0
--AND ISNULL(ps.IsActiveScheme,0) = 1

END


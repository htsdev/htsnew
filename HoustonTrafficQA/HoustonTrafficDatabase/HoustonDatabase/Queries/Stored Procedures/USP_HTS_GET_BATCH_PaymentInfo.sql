SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_BATCH_PaymentInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_BATCH_PaymentInfo]
GO


   CREATE procedure [dbo].[USP_HTS_GET_BATCH_PaymentInfo]      
      
@TicketID int      
as       
SELECT     N.Printdate,N.note,L.lettername,N.recordid, dbo.tblUsers.Abbreviation AS UserAbb      
FROM         dbo.tblHTSNotes N INNER JOIN      
                      dbo.tblHTSLetters L ON N.LetterID_FK = L.LetterID_pk INNER JOIN      
                      dbo.tblUsers ON N.EmpID = dbo.tblUsers.EmployeeID      
WHERE     (N.TicketID_FK = @TicketID)      
order by n.recordid desc  
    
    
      
      
    
    
  
  
  
          


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_errorlog_with_date_range]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_errorlog_with_date_range]
GO

CREATE proc [dbo].[usp_hts_get_errorlog_with_date_range]    
@dtFrom datetime,    
@dtTo datetime    
    
as    
set @dtFrom=@dtfrom+'12:00:00 AM'  
set @dtTo=@dtto+'11:59:59:998 PM'  
  
SELECT  EventID, [Message], Source, TargetSite, StackTrace, [Timestamp]    
FROM  Tbl_HTS_ErrorLog    
WHERE  ([Timestamp] BETWEEN CONVERT(DATETIME, @dtFrom ) AND CONVERT(DATETIME, @dtTo ))    
ORDER BY EventID DESC    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 

Alter by:  Zeeshan Ahmed

Business Logic : This procedure is used to view credit card transaction processed on Traffic System and Public Site
				 Display Ticket Numbers of Sullolaw Cases
			     		 	
List of Parameters:     

	@ShowTestTrans : Used To Show Only Test Transactions          
	@Database	   : Database ID i.e. Houston Or Dallas        
	@StartDate  : Transaction Start Date
	@EndDate 	: Transaction End Date        
	@Source : Source i.e. Traffic System , Public Site , Manuall CC          
	@TransactionType : Transaction Type 

List of Columns: 

	TicketID : TicketId Of Case
	TranID	 : Transaction Number
	InvoiceNumber : Invoice Number
	TransStatus : Transaction Status
	SubmitDate : Transaction Date
	Customer : Customer Name
	Card : Card Type 
	PaymentMethod : Payment Method
	Amount : Payment Amount
	SalesRep : Sales Rep ID
	IsVoid : Is Void Transaction
	IsApproved : Is Transaction Approved
	Source  : Source i.e. Public Site / Traffic System / Manual CC
	TicketNumber  : Ticket Number 
	
	
******/

 
    
-- USP_HTS_AUTHORIZE_NET_REPORT  1,1,'7/2/2007 12:00:00 AM','8/2/2007 12:00:00 AM','-1','-1'    
--USP_HTS_AUTHORIZE_NET_REPORT  1,1,'7/28/07',-1,-1      
      
    
-- USP_HTS_AUTHORIZE_NET_REPORT  1,2,'7/2/2007 12:00:00 AM','8/2/2007 12:00:00 AM','-1','-1'    
--USP_HTS_AUTHORIZE_NET_REPORT  1,1,'7/28/07',-1,-1      
      
    
Alter PROCEDURE [dbo].[USP_HTS_AUTHORIZE_NET_REPORT]            
@ShowTestTrans bit,          
@Database int,          
@StartDate datetime,          
@EndDate datetime,        
@Source int,          
@TransactionType int          
AS            
          
set @EndDate = @EndDate + '23:59:59'         
          
declare @sql nvarchar(MAX),
@sql2 nvarchar(max)           

declare @strParamDef nvarchar(MAX)           

set @strParamDef = '@ShowTestTrans bit,@Database int,@StartDate datetime,@EndDate datetime,@Source int,@TransactionType int   '
set @sql2 = ''

set @sql = '          
DECLARE @Records table            
(            
 TicketID int,            
 TransID varchar(15),            
 InvoiceNumber varchar(100),            
 TransStatus varchar(100),            
 SubmitDate datetime,             
 Customer varchar(50),            
 Card varchar(5),            
 PaymentMethod varchar(100),            
 Amount money,            
 SalesRep varchar(5),            
 TicketNumber varchar(20),            
 isVoid char,            
 isApproved char,            
 Source varchar(15),        
 transtype int           
)            
    '        
        
if @Database = 1        
begin        
            
set @sql = @sql + '        
-- Getting transaction records from Traffic system & Manual CC            
insert into @Records             
(TicketID, TransID, InvoiceNumber, TransStatus,SubmitDate,Customer,Card,PaymentMethod,Amount,SalesRep,TicketNumber,isVoid,isApproved,Source,transtype )            
            
select            
t.TicketID_PK,            
cc.transnum,            
cc.invnumber,            
cc.response_reason_text,            
cc.recdate,            
cc.name,         
case cc.Cardtype             
 when 1 then ''V''            
 when 2 then ''M''            
 when 3 then ''A''            
 when 4 then ''D''            
end,            
cc.cardnumber,            
cc.amount,            
u.Abbreviation,            
tv.RefCaseNumber,            
case cc.isVoid            
 when 1 then ''Y''            
 when 0 then ''N''            
end,            
case cc.response_reason_code            
 when ''1'' then ''Y''            
 else ''N''            
end,            
case cc.TransType            
 when 0 then ''Public Site''            
 when 1 then ''Traffic System''            
 when 2 then ''Manual CC''            
end,        
cc.transtype          
FROM             
--       dbo.tblTickets AS t RIGHT OUTER JOIN        
--                      dbo.tblTicketsCybercashLog AS cc ON t.TicketID_PK = cc.TicketID INNER JOIN        
--                      dbo.tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK LEFT OUTER JOIN        
--                      dbo.tblUsers AS u ON cc.EmployeeID = u.EmployeeID          
    
       dbo.tblTicketsCybercashLog AS cc      
       left OUTER JOIN   dbo.tblTickets AS t     
                      ON t.TicketID_PK = cc.TicketID left outer JOIN      
                      dbo.tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK LEFT OUTER JOIN      
                      dbo.tblUsers AS u ON cc.EmployeeID = u.EmployeeID       
    
where cc.transtype Not in (  0 , 3 ) and (cc.recdate between @StartDate and @EndDate )        
             
-- Getting transaction records from Legal Houston            
insert into @Records             
(TicketID, TransID, InvoiceNumber, TransStatus,SubmitDate,Customer,Card,PaymentMethod,Amount,SalesRep,TicketNumber,isVoid,isApproved,Source,transtype)            
            
             
select distinct      
         
t.TicketID_PK,            
cc.transnum,            
case when cc.invnumber = '''' then  '' ('' + cc.reftransnum + '')''   else cc.invnumber + '' ('' + isnull((Select top 1 invnumber from legalhouston.dbo.tblticketscybercash where id_customer = cc.ticketid),cc.invnumber)  + '')'' end as invnumber ,         
  
   
cc.response_reason_text,            
cc.recdate,            
cc.name,         
case cc.Cardtype             
 when 1 then ''V''            
 when 2 then ''M''            
 when 3 then ''A''            
 when 4 then ''D''            
end,            
cc.cardnumber,            
cc.amount,            
u.Abbreviation,            
tv.RefCaseNumber,            
case cc.isVoid            
 when 1 then ''Y''            
 when 0 then ''N''            
end,            
case cc.response_reason_code            
 when ''1'' then ''Y''            
 else ''N''            
end,            
case cc.TransType            
 when 0 then ''Public Site''          
 when 1 then ''Traffic System''      
 when 2 then ''Manual CC''          
end,        
cc.transtype        
FROM         dbo.tblTicketsViolations AS tv INNER JOIN        
             dbo.tblTickets AS t ON tv.TicketID_PK = t.TicketID_PK RIGHT OUTER JOIN        
             dbo.tblTicketsCybercashLog AS cc Left outer JOIN        
             legalhouston.dbo.tblcustmer AS cu ON cc.TicketID = cu.id_customer ON t.TicketID_PK = cu.Ticketid LEFT OUTER JOIN        
             dbo.tblUsers AS u ON cc.EmployeeID = u.EmployeeID        
where cc.TransType=0   and  (cc.recdate between @StartDate and @EndDate ) 

'


Set @sql2 = 
'
-- Getting transaction records from Sullolaw            
insert into @Records             
(TicketID, TransID, InvoiceNumber, TransStatus,SubmitDate,Customer,Card,PaymentMethod,Amount,SalesRep,TicketNumber,isVoid,isApproved,Source,transtype)            
            
             
select distinct      
         
t.TicketID_PK,            
cc.transnum,            
case when cc.invnumber = '''' then  '' ('' + cc.reftransnum + '')''   else cc.invnumber + '' ('' + isnull((cc.invnumber),cc.invnumber)  + '')'' end as invnumber ,         
  
   
cc.response_reason_text,            
cc.recdate,            
cc.name,         
case cc.Cardtype             
 when 1 then ''V''            
 when 2 then ''M''            
 when 3 then ''A''            
 when 4 then ''D''            
end,            
cc.cardnumber,            
cc.amount,            
u.Abbreviation,            
tv.RefCaseNumber,            
case cc.isVoid            
 when 1 then ''Y''            
 when 0 then ''N''            
end,            
case cc.response_reason_code            
 when ''1'' then ''Y''            
 else ''N''            
end,            
case cc.TransType            
 when 0 then ''Public Site''          
 when 1 then ''Traffic System''      
 when 2 then ''Manual CC'' 
 when 3 then ''Public Site''          
end,        
cc.transtype        
FROM         dbo.tblTicketsViolations AS tv INNER JOIN        
             dbo.tblTickets AS t ON tv.TicketID_PK = t.TicketID_PK RIGHT OUTER JOIN        
             dbo.tblTicketsCybercashLog AS cc LEFT OUTER JOIN        
             Sullolaw.dbo.tblcustomer AS cu ON cc.TicketID = cu.Ticketid ON t.TicketID_PK = cu.Ticketid LEFT OUTER JOIN        
             dbo.tblUsers AS u ON cc.EmployeeID = u.EmployeeID        
where cc.TransType=3  and  (cc.recdate between @StartDate and @EndDate ) 
     
 '          




end          
        
else if @Database = 2        
begin        
        
set @sql2 = @sql2 + '        
insert into @Records           
(TicketID, TransID, InvoiceNumber, TransStatus,SubmitDate,Customer,Card,PaymentMethod,Amount,SalesRep,TicketNumber,isVoid,isApproved,Source)          
          
select          
t.TicketID_PK,          
cc.transnum,          
cc.invnumber,          
cc.response_reason_text,          
cc.recdate,          
cc.name,         
case cc.Cardtype           
 when 1 then ''V''          
 when 2 then ''M''          
 when 3 then ''A''          
 when 4 then ''D''          
end,          
cc.cardnumber,          
cc.amount,          
u.Abbreviation,          
tv.RefCaseNumber,          
case cc.isVoid          
 when 1 then ''Y''          
 when 0 then ''N''          
end,          
case cc.response_reason_code          
 when ''1'' then ''Y''          
 else ''N''          
end,          
case cc.TransType          
 when 0 then ''Public Site''          
 when 1 then ''Traffic System''          
 when 2 then ''Manual CC''          
end          
FROM         DallasTrafficTickets.dbo.tblTickets AS t INNER JOIN        
                      DallasTrafficTickets.dbo.tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK RIGHT OUTER JOIN        
                      DallasTrafficTickets.dbo.tblTicketsCybercashLog AS cc ON t.TicketID_PK = cc.TicketID LEFT OUTER JOIN        
                      DallasTrafficTickets.dbo.tblUsers AS u ON cc.EmployeeID = u.EmployeeID        
          
where cc.transtype = 0  and (cc.recdate between @StartDate and @EndDate )               
         
'        
        
end        
        
--set @sql = @sql + ' Select * from @Records where DateDiff(day,submitdate,'''+convert(varchar,@StartDate) +''')=0 '          
set @sql2 = @sql2+ ' Select distinct TicketID ,        
TransID,InvoiceNumber,TransStatus,SubmitDate,Customer,Card,PaymentMethod, Amount, SalesRep,        
isVoid, isApproved, Source,        
min(TicketNumber) as TicketNumber        
 from @Records   where 1 = 1 
        
'        
          
if @Source <> -1          
begin          
 if @source = 0          
  set @sql2 = @sql2 + ' and Source = ''Public Site'''          
 else if @source = 1          
  set @sql2 = @sql2 + ' and Source = ''Traffic System'''          
 else          
  set @sql2 = @sql2 + ' and Source = ''Manual CC'''          
end          
          
if @TransactionType <> -1          
begin          
 if @TransactionType = 1          
  set @sql2 = @sql2 + 'and isApproved = ''Y'''          
 if @TransactionType = 2          
  set @sql2 = @sql2 + 'and isVoid = ''Y'''           
end          
        
if @ShowTestTrans = 0        
 set @sql2 = @sql2 + ' and TransID <> ''0'' ' --Sabir Khan 6672 10/06/2009 Fixed bug.       
           
        
        
set @sql2 = @sql2 + ' group by         
TicketID,TransID,InvoiceNumber, TransStatus,SubmitDate, Customer, Card,         
PaymentMethod,Amount,SalesRep, isVoid,isApproved, Source          
order by TicketID   '          
        
   
Set @sql = @sql + isnull(@sql2,'')

EXECUTE sp_executesql @sql,@strParamDef ,@ShowTestTrans,@Database,@StartDate,@EndDate,@Source,@TransactionType
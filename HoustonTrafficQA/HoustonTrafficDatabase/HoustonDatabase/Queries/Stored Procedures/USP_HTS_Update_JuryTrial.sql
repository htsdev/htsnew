-- =============================================
-- Author:	Author-Asghar
-- Create date: 03/03/2007
-- Business Logic: This stored procedure is used to update attorney trial information in tbljurytrial.
-- =============================================
--   USP_HTS_Insert_JuryTrial '01/03/2007',111,'asghar','khalid','nahid','as876','a234','234','judge','breif','444','verdict','45','sd'


alter PROCEDURE [dbo].[USP_HTS_Update_JuryTrial] 
	
@rowid		bigint,
@date		datetime,
@attorney	int,
@prosecutor	varchar(50),
@officer	varchar(50),
@defendant	varchar(50),
@caseno		varchar(30),
@ticketno	varchar(30),
@Courtid	int,
@judge		varchar(50),
--Yasir Kamal 6279 08/05/2009 field length increased.
@brieffacts	varchar(MAX),
@verdictid	int,
@verdict	varchar(50),
@fine		money,
@courtreport varchar(50)		
	

AS
BEGIN

update tbljurytrial

	set 
	date=@date,
	attorney = @attorney,
	prosecutor= @prosecutor,
	officer = @officer,
	defendant = @defendant,
	caseno = @caseno,
	ticketno = @ticketno,
	Courtid = @Courtid,
	judge = @judge,
	brieffacts = @brieffacts,
	verdictid =@verdictid,
	verdict = @verdict,
	fine = @fine,
	courtreport =@courtreport
    

where rowid = @rowid

END


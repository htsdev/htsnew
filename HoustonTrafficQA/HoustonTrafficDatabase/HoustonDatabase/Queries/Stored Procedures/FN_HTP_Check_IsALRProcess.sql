﻿ /****** 
Created by:		Muhammad Nasir
Business Logic:	The function is used in to check the court is ALR Process or not
				
				
List of Parameters:
	@CourtID:	Call ID 	

List of Columns:	
	@IsALR: if court is ALR Process return 1 else 0
*******/
Create FUNCTION dbo.FN_HTP_Check_IsALRProcess(@CourtID INT)
RETURNs BIT
AS
BEGIN
	DECLARE @IsALR BIT
	
		IF(EXISTS(SELECT * FROM tblCourts WHERE Courtid=@CourtID AND ALRProcess=1))
			SET @IsALR= 1
		ELSE
			SET @IsALR= 0 
	RETURN @IsALR
END

GO
GRANT EXECUTE ON [dbo].[FN_HTP_Check_IsALRProcess] TO [dbr_webuser]
GO 
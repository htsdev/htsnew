 /******************    
    
Alter BY : Sabir Khan    
    
BUSINESS LOGIC : This procedure is used to add new case into Traffic Ticket Database    
      
LIST OF INPUT PARAMETERS :    
    
 @firstname :	First Name.
 @MiddleName : Middle Name.
 @lastname  : Last Name.
 @DOB : Date of Birth.
 @FirmID  : Associated Firm.
 @Contact1  : first Contact Number.
 @ContactType1  : Contact Type.
 @LanguageSpeak : Language of Speek.
 @bondflag  : Bond Flag.
 @AccidentFlag : Accident Flag.
 @CDLFlag : CDL Flag.
 @BondsRequiredflag : Bond Required Flag.
 @AdSourceId  : Ad Source ID.
 @GeneralComments : General Comments.
 @employeeid  : Employee ID.
 @SpeedingID : Speeding ID.
 @CaseTypeID : Case Type.
 @Occupation : It represents occupation of the client
 @Employer : It represents the employer of the client
 @IsUnemployed : If client is unemployed
 @VehicleType : Vehicle Type of the client for only traffic cases. 
				1 for Commercial Vehicle, 
				2 for Motor Cycle and 
				3 for Car else 0 or null.
 @TicketId : Ticket ID
    
List of Columns:  
Ticket ID: Return ticket ID which is newly generated. 
  
*******************/   
                  
alter procedure [dbo].[USP_HTS_Insert_NewCase]                    
 (                    
 @firstname  varchar(20),         
 @MiddleName varchar(1),                 
 @lastname  varchar(20)='',    
 @DOB datetime,                   
 @FirmID   int,                    
 @Contact1  varchar(15),                    
 @ContactType1  int=100,                    
 @LanguageSpeak  varchar(15)='English',                    
 @bondflag  tinyint=0,                    
 @AccidentFlag  tinyint=0,                    
 @CDLFlag  tinyint=0,                    
 @BondsRequiredflag tinyint=0,                    
 @AdSourceId  tinyint=0,                    
 @GeneralComments varchar(max)='', -- Adil - For 2585                   
 @employeeid  int ,                
 @SpeedingID int,  
 @CaseTypeID int = 1 ,
 --Ozair 6599 09/19/2009 occupation , employer, isemployed, VehicleType
 @Occupation varchar(100) = null,
 @Employer varchar(50) = null,
 @IsUnemployed bit = null,                  
 @VehicleType tinyint = null,
 @TicketId int=0 output                     
 )                    
                    
as                    
                    
declare @TicketNumber varchar(25),                    
                  
 @ErrFlag int                    
                  
select @errflag = 0                  
                    
begin tran                    
                    
 insert into tblIDGenerator(recdate) values(getdate())                    
 select @TicketNumber='ID' + convert(varchar(12),@@identity)                    
                     
 if @@ERROR <> 0 set @ErrFlag = @@ERROR  
 
 --Waqas 6894 10/30/2009 Inserting Occupation and employer.
DECLARE @OccID INT
IF EXISTS(SELECT * FROM Occupation WHERE Occupation = @Occupation)
BEGIN
	SET @OccID = (SELECT OccupationID FROM Occupation WHERE Occupation = @Occupation )
END
ELSE
	BEGIN
		INSERT INTO Occupation
		(Occupation)
		VALUES
		(@Occupation)
		SET @OccID = (SELECT OccupationID FROM Occupation WHERE Occupation = @Occupation )
	END
	
DECLARE @EmplrID INT
IF EXISTS(SELECT * FROM Employer WHERE Employer = @Employer)
BEGIN
	SET @EmplrID = (SELECT EmployerID FROM Employer WHERE Employer = @Employer )
END
ELSE
	BEGIN
		INSERT INTO Employer
		(Employer)
		VALUES
		(@Employer)
		SET @EmplrID = (SELECT EmployerID FROM Employer WHERE Employer = @Employer )
	END
                  
                     
 insert into tbltickets                    
  (                    
  firstname,      
  middlename,                    
  lastname,    
  DOB,                  
  firmid,                    
  Contact1,                    
  ContactType1,                    
  LanguageSpeak,                    
  bondflag,                    
  BondsRequiredflag,                    
  AdSourceId,                    
  employeeid,                
  SpeedingID,            
  EmployeeIDUpdate  ,    
  stateid_fk   ,  
  CaseTypeID,
  --Ozair 6599 09/19/2009 occupation , employer, isemployed
  --Waqas 6894 10/30/2009 Inserting Occupation and employer.
  OccupationID ,
  EmployerID,
  IsUnemployed,
  VehicleType
  )                    
 values                    
  (                    
  @firstname,      
  @MiddleName,      
  @lastname,    
  @DOB,                     
  @FirmID,                    
  @Contact1,                    
  @ContactType1,                    
  @LanguageSpeak,                    
  @bondflag,                    
  @BondsRequiredflag,                    
  @AdSourceId,                    
  @employeeid,                
  @SpeedingID,            
  @employeeid ,      
  45,  -- Default state Texas..... 
  @CaseTypeID,                   
  --Ozair 6599 09/19/2009 occupation , employer, isemployed
  --Waqas 6894 10/30/2009 Inserting Occupation and employer.
  @OccID, 
  @EmplrID, 
  @IsUnemployed,
  @VehicleType
  )                    
                     
 if @@ERROR <> 0 set @ErrFlag = @@ERROR                    
 set @TicketId = @@identity                    
                     
 if @TicketId is not null                    
  begin                    
          
--UPDATING CDL & ACCIDENT INFO.....          
 update tbltickets set cdlflag = @CDLFlag, accidentflag = @AccidentFlag  where ticketid_pk = @ticketid          
          
  --  INSERTING A DUMMY DATA ROW IN TBLTICKETS VIOLATIONS...............                    
   insert into tblticketsviolations                    
    (                    
    TicketID_PK,                    
    ViolationNumber_PK,                    
    refcasenumber,  
 casenumassignedbycourt,  
    courtid,  
    planid,                   
    ticketofficernumber,                    
    ticketviolationdate,
	--Sabir Khan 5010 10/21/2008 Insert PRE TRAIL as a status for Family Law Cases Only...
    CourtViolationStatusIDmain,
    CourtDateMain,                             
    vemployeeid  ,      
    underlyingbondflag       
--    casenumassignedbycourt              
    )                    
                      
    select  @TicketID,                    
     0,                    
     @TicketNumber,  
  @TicketNumber,  
    case when @CaseTypeID= 1 then 3001 when @CaseTypeID = 2 then 3037 when @CaseTypeID= 3 then  3071  when @CaseTypeID= 4 then  3077 end , -- Noufil 4782 09/23/2008 Family Law Case type case added     
     45,      
     962528,                    
     '01/01/1900',
	--Sabir Khan 5010 10/21/2008 set Pre Trail as a status and 09:00AM as a court time when case type is family law...
	--Sabir Khan 6264 08/12/2009 Court time has been changed from 09:00AM to 08:00AM
     case when @CaseTypeID = 4 then 101 else null end ,
	 case when @CaseTypeID = 4 then '1900-01-01 08:00:00.000' else null end,                     
     @employeeid,      
     @bondflag      
--     @ticketnumber                
   if @@ERROR <> 0 set @ErrFlag = @@ERROR                    
                    
   --insert into tblTicketsContact(TicketID_PK,ContactType_PK) values (@TicketID,0 )                    
   --if @@ERROR <> 0 set @ErrFlag = @@ERROR                    
                    
   --insert into tblticketstrialarr(TicketID_pk,DateSet_PK,datetype) values (@TicketID,'01/01/1900',1 )                    
   --if @@ERROR <> 0 set @ErrFlag = @@ERROR                    
             
   insert into tblticketsnotes(ticketid,subject,employeeid) values(@TicketID,'Initial Inquiry',@employeeid)                    
   if @@ERROR <> 0 set @ErrFlag = @@ERROR                    
                    
  -- insert into tblviolationquote(ticketid_fk) values (@ticketid)                    
  -- if @@ERROR <> 0 set @ErrFlag = @@ERROR                    
                
 --for general comments            
 
  
-- Agha Usman - 4347  - Problem Client Integration
exec USP_HTP_ProblemClient_Manager @TicketId,@firstname,@lastname,@MiddleName,@DOB,null 

 if ((right(@GeneralComments,1) <> ')')  and (left(@GeneralComments,1) <> ''))    --if comments are inserted                
  begin                     
    declare @empnm as varchar(12)                    
                       
    select @empnm = abbreviation from tblusers where employeeid = @employeeid                    
                 
    set @GeneralComments = @GeneralComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' '                    
                    
    update tbltickets set  generalcomments = @GeneralComments                       
                          
    where ticketid_pk = @TicketId                    
  end                   
  end                    
                  
                    
if @ErrFlag = 0                    
 begin                    
  commit tran                  
                    
 end                    
else                    
 begin                    
  rollback tran                    
 end    
    
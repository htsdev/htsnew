ALTER procedure [dbo].[USP_HTS_Get_DocketReport_UL_update]   
 (  
 @courtlocation varchar(4) = 'NONE',  
 @casestatus varchar(4)= 'NONE',  
 @startcourtdate datetime = null,  
 @endcourtdate datetime = null  
 )  
as  
  
declare @sqlquery nvarchar(4000),  
 @sqlquery1 nvarchar(500),  
 @sqlquery2 nvarchar(500),  
 @strParam  nvarchar(1000)  
  
set @strParam ='  
 @courtlocation varchar(4) ,  
 @casestatus varchar(4) ,  
 @startcourtdate datetime ,  
 @endcourtdate datetime '  
  
  
if @endcourtdate is not null  
 set @endcourtdate = @endcourtdate + '23:59:59.998'  
  
  
set @sqlquery = '  
 select t.ticketid_pk,  
  T.lastname,  
  T.firstname,  
  min(p.recdate) as entry,  
  left(C.shortcourtname,18) as court,  
  jpfaxdate as ContactDate ,  
  datediff(day, jpfaxdate, getdate()) as datedifference,  
  isnull(courtdatemain,''01/01/1900'') as courtdatemain ,  
  right(convert(varchar(25), isnull(courtdatemain,''01/01/1900''),100),7) as CourtTime,  
  courtnumbermain,   
  D.description,  
  FirmAbbreviation = ( case when t.CoveringFirmID <> 3000 then FirmAbbreviation  
     else  ''''   
         end   
       ),  
  bondflag = case bondflag when 1 then ''B'' else '''' end,
  V.ticketofficernumber officernumber    
  
 FROM    tblTickets T   
 INNER JOIN  
  tblTicketsPayment P   
 ON  T.TicketID_PK = P.TicketID   
 INNER JOIN  
  tblFirm F   
 ON  T.CoveringFirmID = F.FirmID LEFT   
 OUTER JOIN  
  tblOfficer O   
 ON  officernumber = O.OfficerNumber_PK   
 INNER JOIN  
  tblticketsviolations V   
 on  T.ticketid_pk = V.ticketid_pk   
 INNER JOIN  
  tblCourts C   
 ON  v.courtid = C.Courtid   
 INNER JOIN   
  tblcourtviolationstatus S   
 On  V.courtviolationstatusidmain = S.courtviolationstatusid   
 inner join   
  tbldatetype D   
 on  S.categoryid = D.typeid  '  
if @casestatus <> '50'  
 set @sqlquery = @sqlquery + 'and  d.typeid <> 50 '  
    
  
  
select  @sqlquery1 =   
 case   
  when isnumeric(@courtlocation) = 1  then  'where v.courtid =  @courtlocation '  
  when @courtlocation = 'JP' then 'where v.courtid not in (0,3003,3002,3001) '   
  when @courtlocation = 'IN' then 'where v.courtid in (3003,3002,3001) '   
  else  'where v.courtid  not in (0)'  
 end  
set  @sqlquery = @sqlquery + isnull(@sqlquery1,' ' )  
  
select  @sqlquery1 =   
 case   
  when isnumeric(@casestatus) = 1  then  ' and d.typeid =  @casestatus'  
  when @casestatus = 'WAIT' then ' and d.typeid  in (1,11)'  
--  when @casestatus = 'APP'  then ' and d.typeid  in (3,4,5,9)'  
  when @casestatus = 'APP'  then ' and d.typeid  in (3,4,5)'  
  when @casestatus not in ('WAIT','APP') and isnumeric(@casestatus) <> 1 then ' and d.typeid not in (0,6,7,8,10,50)'  
    
 end  
set  @sqlquery = @sqlquery + isnull(@sqlquery1,' ')  
  
if ( @endcourtdate is not null  and @startcourtdate is not null )  
begin  
 set @sqlquery1 = ' and courtdatemain  between @startcourtdate and @endcourtdate'  
end  
set  @sqlquery = @sqlquery + isnull(@sqlquery1,' ')  
set @sqlquery = @sqlquery + ' group by t.ticketid_pk,  
  T.lastname,  
  T.firstname,  
  C.shortcourtname,  
  jpfaxdate  ,  
  datediff(day,jpfaxdate,getdate()) ,  
  isnull(courtdatemain,''01/01/1900'') ,  
  right(convert(varchar(25), isnull(courtdatemain,''01/01/1900''),100),7) ,  
  courtnumbermain,   
  D.description,  
  FirmAbbreviation,  
  t.CoveringFirmID,  
  case bondflag when 1 then ''B'' else '''' end,
 V.ticketofficernumber	
  
  order by   
   isnull(courtdatemain,''01/01/1900''),  
   D.description,  
   right(convert(varchar(25), isnull(courtdatemain,''01/01/1900''),100),7),  
   C.shortcourtname,  
   courtnumbermain,  
   T.lastname,  
   T.firstname'  
  
  
set  @sqlquery = @sqlquery + isnull(@sqlquery2,' ')  
--print @sqlquery  
--exec (@sqlquery)  
exec sp_executesql @sqlquery, @strparam, @courtlocation , @casestatus , @startcourtdate  , @endcourtdate  
go 
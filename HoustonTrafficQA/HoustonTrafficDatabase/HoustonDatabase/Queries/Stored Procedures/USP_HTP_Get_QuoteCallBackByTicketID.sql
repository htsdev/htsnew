﻿/******  
Created by:  Fahad Muhammad Qureshi
Business Logic : This procedure is used to get Quote Call Back By QuoteID
Task ID: 6934 
Created Date: 12/16/2009         
List of Parameters: 
		TicketID 
    
******/
CREATE PROCEDURE [dbo].[USP_HTP_Get_QuoteCallBackByTicketID]
	@TicketID INT
AS
	SELECT DISTINCT tt.Firstname + ' ' + tt.Lastname AS Customer,
       (
           CASE ISNULL(tvq.FollowUPID, 0)
                WHEN 0 THEN 1
                ELSE tvq.FollowUPID
           END
       ) AS Followupid,
       tvq.FollowUpYN,
       ISNULL(tvq.CallBackDate, '01/01/1900') AS CallBackDate,
       tt.calculatedtotalfee,
       --Yasir Kamal 7355 01/29/2010 get client language.
       tt.LanguageSpeak
FROM   tblTickets tt
       INNER JOIN tblViolationQuote tvq
            ON  tt.TicketID_PK = tvq.TicketID_FK 
	WHERE  (tvq.TicketID_FK = @TicketID)

GO

GRANT EXEC ON [dbo].[USP_HTP_Get_QuoteCallBackByTicketID] to dbr_webuser
GO
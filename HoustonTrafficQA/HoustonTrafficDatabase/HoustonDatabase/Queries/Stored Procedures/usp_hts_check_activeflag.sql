  
/****** 
Alter by	: Sabir Khan
Altered Date	: 11/29/2011
TasK		: 9243

Business Logic : This procedure is used to return client information

List of Parameters:	
	@TicketID			: id against which records are fetched


Column Return : 
		activeflag,
		lockflag,
		courtid,
		casetypeid,
		inactiveflag
		
******/

alter procedure usp_hts_check_activeflag      
            
@ticketid int            
            
as            
            
SELECT     TOP (1) ISNULL(t.Activeflag, 0) AS activeflag, ISNULL(t.LockFlag, 0) AS lockflag, tv.CourtID AS courtid, t.casetypeid, c.InActiveFlag  --Sabir Khan 9243 11/29/2011 inactiveflag column has been added.       
FROM         tblTickets AS t INNER JOIN    
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK  
          left outer join tblcourts c on c.courtid = tv.courtid    
WHERE     (t.TicketID_PK = @ticketid)      
        
        
        
---- Added by Asghar for checking the 'No Check' status during payment process        
select flagid from tblticketsflag         
where ticketid_pk =@ticketid        
and flagid=17       
    
    
﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 8/10/2009 1:47:51 PM
 ************************************************************/

/******   
Created by:  Muhammad Nasir  
Task ID :  6098  
Business Logic: The procedure is used by houston to get the Comments by given search criteria its should be complaint and not reviewed.  
      
List of Parameters:   
 @startdate:  Starting date of complaint flag add  
 @endListDAte: Ending date of complaint flag add  
 @CaseType  Civil, Criminal, Traffic, Family  
 @Attorney  Attorney who was covering the case  
 @ShowAll  By default all non reviewed cases will be displayed  
  
List of Columns:   
 TicketID_PK: Case number.  
 FullName:  FullName of client  
 Area    case type Civil, Criminal, Traffic, Family  
 AttorneyName: Attorney who was covering the case  
 CDate:   complaint date and time  
 Complaint:  system shows the full complaint comment  
  
******/  
  
  
Create PROCEDURE dbo.USP_HTP_GET_Comments_Report
	@StartDate DATETIME,
	@EndDate DATETIME,
	@CaseType INT,
	@Attorney INT,
	@CommenType INT, --1 General, 3  Reminder call,4 Complaint
	@ShowAll BIT
AS
BEGIN
    SELECT DISTINCT top 2000 tt.TicketID_PK,
           tt.Firstname + ' ' + tt.Lastname AS FullName,
           ct.CaseTypeName AS Area,
           tf.FirmName as AttorneyName,
           --dbo.fn_DateFormat(ttn.RecDate,25,'/',':',1) AS CDate,
           ttn.[Subject] AS Comments,
           CASE ttn.Fk_CommentID
                WHEN 1 THEN tt.GeneralComments
                WHEN 3 THEN ttv.ReminderComments
           END AS FullComments,
           ttn.Fk_CommentID
           ,ttn.Recdate
    FROM   tblTickets tt
           INNER JOIN tblTicketsNotes ttn
                ON  tt.TicketID_PK = ttn.TicketID           
           INNER JOIN tblTicketsViolations ttv
                ON  tt.TicketID_PK = ttv.TicketID_PK
           INNER JOIN tblFirm tf
                ON  ttv.CoveringFirmID = tf.FirmID
           Inner JOIN CaseType ct
				ON tt.CaseTypeId=ct.CaseTypeId
    WHERE  (
               (@ShowAll = 1)
               OR (
                      DATEDIFF(DAY, ttn.RecDate, @StartDate) <= 0
                      AND DATEDIFF(DAY, ttn.RecDate, @EndDate) >= 0
                  )
           )
           AND (
                   (@CommenType = 1 AND ttn.Fk_CommentID = 1)
                   OR (@CommenType = 3 AND ttn.Fk_CommentID = 3)
                   OR (
                          @CommenType = 4
                          AND ttn.Fk_CommentID IN (1, 3)
                          AND ttn.IsComplaint = 1
                      )
               )
           AND (@CaseType = -1 OR tt.CaseTypeId = @CaseType)
           AND (@Attorney = -1 OR ttv.CoveringFirmID = @Attorney)
            ORDER BY ttn.Recdate DESC
END

   
go

GRANT EXECUTE ON dbo.USP_HTP_GET_Comments_Report TO dbr_webuser
 GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_CaseDispositionStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_CaseDispositionStatus]
GO











CREATE PROCEDURE [usp_HTS_Get_CaseDispositionStatus] AS

Select CaseStatusID_Pk,[Description] as Dcr,orderfield from tblCaseDispositionstatus order by orderfield






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This procedure Update attorney paid amount.
* Parameter : 
*			@isattorneypayoutprinted
			@attorneypaidamount
			@attorneypaiddate
			@ticketsviolationID				
  
*/
CREATE PROCEDURE [dbo].[USP_HTP_UpdateAttorneyPaid]
	@isattorneypayoutprinted BIT,
	@attorneypaidamount MONEY,
	@attorneypaiddate DATETIME,
	@ticketsviolationID INT
AS
	UPDATE tblTicketsViolations
	SET    IsAttorneyPayoutPrinted = @isattorneypayoutprinted,
	       AttorneyPaidAmount = @attorneypaidamount,
	       AttorneyPaidDate = @attorneypaiddate
	WHERE  TicketsViolationID = @ticketsviolationID
	
	GO
	GRANT EXECUTE ON [dbo].[USP_HTP_UpdateAttorneyPaid] TO dbr_webuser
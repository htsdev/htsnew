SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_SelectedViolation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_SelectedViolation]
GO


CREATE procedure [dbo].[USP_HTS_GET_SelectedViolation]
	(
	@ViolId int
	)


as

select violationnumber_pk, description,ShortDesc, isnull(categoryid,0) as categoryid
from tblviolations where violationnumber_pk = @violid





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


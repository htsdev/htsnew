﻿
/******     
    
Created by  : Muhammad Nasir    
TaskID   : 10525    
Created Date  : 11/29/2012    
Business Logic : This procedure gets all HTP clients who have been hired on last Saturday and next Arraignment Date is on Monday.     
It also gets the attorneys to whom text message will be sent.    
        
******/    
ALTER Proc [dbo].[USP_HTP_GET_TodayHireClientWithNextARRIAGNMENTDATEonMonday]    
    
As    
    
declare @dateOffset INT    
declare @lastSaturdayDate DATETIME    
declare @todayDate DATETIME    
declare @totalClientHiredOnLastSaturday int    
    
select    
@dateOffset = case    
when DATENAME(weekday,getDate()) = 'Monday' then 5    
when DATENAME(weekday,getDate()) = 'Tuesday' then 4    
when DATENAME(weekday,getDate()) = 'Wednesday' then 3    
when DATENAME(weekday,getDate()) = 'Thursday' then 2    
when DATENAME(weekday,getDate()) = 'Friday' then 1    
when DATENAME(weekday,getDate()) = 'Saturday' then 0    
when DATENAME(weekday,getDate()) = 'Sunday' then -1    
end     
    
select     
@todayDate = getDate()    
, @lastSaturdayDate = (@todayDate-7)+@dateOffset    
    
SELECT    
*    
From    
(    
SELECT    
 ROW_NUMBER () OVER (PARTITION BY tt.TicketID_PK ORDER BY tt.TicketID_PK) as ROWID
, tt.TicketID_PK as ticketID     
, CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(TV.RefCaseNumber, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(TV.RefCaseNumber, 'N/A'))))  
  ELSE 'N/A' END AS TicketID_PK    
, LOWER(tt.Email) AS Email    
, tt.Firstname    
, tt.Lastname    
, case when isnull(tt.Firstname,'') = '' then 'N/A' else tt.Firstname + ',' end +     
  case when isnull(tt.Lastname,'') = '' then 'N/A' else tt.Lastname end as clientName    
, tt.LanguageSpeak,tv.CourtID    
, (select case when count(*) = 1 then '(1)'
	 when count(*) = 2 then '(+1)'
	 when count(*) = 3 then '(+2)'
	 when count(*) = 4 then '(+3)'
	 when count(*) = 5 then '(+4)'
	else '(' + cast(count(*) as varchar(3)) + ')' end
from tblTicketsViolations where ticketID_PK = tt.ticketID_PK) as totalViolations
, CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(casenumassignedbycourt, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(casenumassignedbycourt, 'N/A'))))  
  ELSE 'N/A' END as CauseNumber    
, tv.courtNumber    
, replace(CONVERT(VARCHAR(5), tv.courtDateMain, 108) + SUBSTRING(CONVERT(VARCHAR(19), tv.courtDateMain, 100),18,1), ':','') as courtTime    
, tu.Abbreviation as RepsAcronym  
--Muhammad Nasir 10525 17/12/2012 Added courtDate, courtShortDesc, cvs.Description
, tv.courtDateMain as courtDate  
, tc.shortcourtname as courtShortDesc    
, cvs.shortDescription as violationStatus  
, case when tt.contactType1 = 4 then isnull(replace(dbo.fn_FormatContactNumber(tt.contact1, tt.contactType1), '(mobile)', ''), '')  
 when tt.contactType2 = 4 then isnull(replace(dbo.fn_FormatContactNumber(tt.contact2, tt.contactType2), '(mobile)', ''), '')  
 when tt.contactType3 = 4 then isnull(replace(dbo.fn_FormatContactNumber(tt.contact3, tt.contactType3), '(mobile)', ''), '')  
 ELSE '' end as mobileNumber  
FROM
Traffictickets.dbo.tblTickets tt    
inner join tblTicketsViolations tv on tv.ticketID_PK = tt.TicketID_PK
--Muhammad Nasir 10525 17/12/2012 Added join for tblCourtViolationStatus
inner join tblCourtViolationStatus cvs on cvs.courtViolationStatusID = tv.ViolationStatusID  
inner join tblCourts tc on tc.courtID = tv.courtID    
inner join tblTicketsPayment tp on tp.ticketID = tt.TicketID_PK    
inner join tblUsers tu on tu.employeeID = tp.employeeID    
WHERE    
tt.TicketID_PK IN     
 (SELECT ttp.TicketID    
  FROM Traffictickets.dbo.tblTicketsPayment ttp    
  WHERE ttp.PaymentVoid = 0    
  GROUP BY    
  ttp.PaymentVoid,    
  ttp.TicketID    
  HAVING DATEDIFF(DAY, MIN(ttp.RecDate), @lastSaturdayDate) = 0)    
AND tv.courtID IN (select courtID from tblCourts where courtID in (3001, 3002, 3003, 3075))
--Farrukh 11266 07/09/2013 Changed "ViolationStatusID" to "CourtViolationStatusIDmain" to exclude Disposed Cases
AND tv.CourtViolationStatusIDmain IN (Select courtViolationStatusID from tblCourtViolationStatus where [Description] like '%ARRAIGNMENT%')
AND tv.CourtViolationStatusIDmain NOT IN (SELECT courtviolationstatusid FROM tblCourtViolationStatus tcvs WHERE  ISNULL(tcvs.CategoryID, 0) = 50)
Group by     
tt.TicketID_PK    
, LOWER(tt.Email)    
, tt.Firstname    
, tt.Lastname    
, tt.LanguageSpeak    
, tv.CourtID    
, tv.caseNumAssignedByCourt    
, tv.RefCaseNumber    
, tv.courtNumber    
, tv.courtDateMain    
, tu.Abbreviation  
--Muhammad Nasir 10525 17/12/2012 Added following parameters---------  
, tv.courtDateMain  
, tc.shortcourtname    
, cvs.shortDescription  
, tt.contact1  
, tt.contact2    
, tt.contact3  
, tt.contactType1    
, tt.contactType2  
, tt.contactType3  
--10525 changes ended-------------------------------------------------  
HAVING DATEDIFF(DAY, MIN(tv.courtDateMain), @todayDate) = 0    
) client    
where ROWID = 1    
--Add specific courts clause to SP...    
    
SELECT distinct        
es.id      
, es.[Name]      
, es.Email as phoneNumber      
FROM EmailSettings es
LEFT JOIN tblEmailHistory eh on eh.emailTo = es.Name AND DATEDIFF(DAY,eh.recDate,getDate()) <> 0
WHERE es.defaultflag = 1    
    



SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentDetailByRecDateRange__backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentDetailByRecDateRange__backup]
GO








CREATE PROCEDURE [dbo].[usp_Get_All_PaymentDetailByRecDateRange] 

@RecDate DateTime,
@RecDateTo DateTime

AS
--Drop Table #TempFinReportA
--Drop Table #TempFinReportB

CREATE TABLE #TempFinReportA
(
   EmployeeID  int NOT NULL CONSTRAINT PK1_EmployeeID PRIMARY KEY,
   CashAmount      money     Not NULL DEFAULT(0),
   --CheckAmount     money     Not NULL DEFAULT(0)
)
CREATE TABLE #TempFinReportB
(
   EmployeeID  int NOT NULL CONSTRAINT PK2_EmployeeID PRIMARY KEY,
   --CashAmount      money     Not NULL DEFAULT(0),
   CheckAmount     money     Not NULL DEFAULT(0)
)

INSERT INTO #TempFinReportA  (EmployeeID, CashAmount)
SELECT  EmployeeID, Sum(ChargeAmount) As Amount 
FROM         dbo.tblTicketsPayment 
Where 
/*
	( CONVERT(varchar(12),RecDate, 101)   >=  CONVERT(varchar(12), @RecDate, 101)  AND
		CONVERT(varchar(12),RecDate, 101)   <=  CONVERT(varchar(12), @RecDateTo, 101)  )
--Where CONVERT(varchar(12),RecDate, 101)   =  '05/25/2005'
*/
( RecDate Between  @RecDate   AND   @RecDateTo+1)
And PaymentType = 1  
AND  PaymentVoid <> 1 
Group By EmployeeID 
Order By EmployeeID 


INSERT INTO #TempFinReportB  (EmployeeID, CheckAmount)
SELECT  EmployeeID, Sum(ChargeAmount) As Amount 
FROM         dbo.tblTicketsPayment 
Where 
/*	( CONVERT(varchar(12),RecDate, 101)   >=  CONVERT(varchar(12), @RecDate, 101)  AND
		CONVERT(varchar(12),RecDate, 101)   <=  CONVERT(varchar(12), @RecDateTo, 101)  )
--Where CONVERT(varchar(12),RecDate, 101)   =  '05/25/2005'
*/

( RecDate Between  @RecDate   AND   @RecDateTo+1)
--And PaymentType IN (2,5,6,7,9)  
And PaymentType = 2
AND  PaymentVoid <> 1 
Group By EmployeeID 
Order By EmployeeID 


SELECT     ISNULL(dbo.#TempFinReportA.EmployeeID, dbo.#TempFinReportB.EmployeeID) AS EmpID, ISNULL(CashAmount,0)as CashAmount, ISNULL(CheckAmount,0) as CheckAmount
FROM         dbo.#TempFinReportA FULL OUTER JOIN
                      dbo.#TempFinReportB ON dbo.#TempFinReportA.EmployeeID = dbo.#TempFinReportB.EmployeeID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


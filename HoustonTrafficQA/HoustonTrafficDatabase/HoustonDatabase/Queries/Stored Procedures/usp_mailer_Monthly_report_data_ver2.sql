/****** 
Created by:		Tahir Ahmed
Create Date:	18/06/2007

Business Logic:	The procedure is used by LMS application to get the returns on the selected mailer type.

				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS Reports
	@sDate:			Starting date for search criteria.
	@eDate:			Ending date for search criteria.
	@SearchType:	Flag that identifies if searching by upload date or by mailer date. 0= upload date, 1 = mailer date

List of Columns:	
	mdate:			Maier Date or upload date as selected in the search criteria.
	lettercount:	Number of letters printed
	nonclient:		Number of people not hired/contacted us
	client:			Number of people hired us.
	revenue:		Revenue generated from the hired people.
	expense:		Expenses occured on mailing.
	profit:			Profit earned by the maiing.
	NPM:			The profit margin tells you how much profit a company makes for every $1 in generates in revenue.  In our case, the net profit margin tells us how much revenue is generated per dollar of postage spent. NPM = Profits / Revenue
	ROP:			Return on postage tells us how much profit is earned per $1 of postage spent.  ROP = Profit / Expense
	QPMP:			Quotes per Mail Piece tells us how many quotes are given per mail piece sent.  QPMP = Quote / Mailer Count
	HPMP:			Hires per Mail Piece tells us how many clients hire per mail piece sent.  HPMP = Hire / Mailer Count
	PPMP:			Profit per Mail Piece “PPMP” tells us how much money is earned per mail piece sent.  PPMP = Profits / Mailer Count
	CPMP:			It is ratio of CALLS to mailer count and tells us how many people contacting us per mail piece. CPMP = (Hires + Calls) / Mailer Count
	HPQ:			It is ratio of HIRES to CALLS and tells us how many people hired us per call. HPQ = Hires / (Hires + Quote)

	

******/

-- usp_mailer_Monthly_report_data_ver2 16, 45, '01/07/2005', '01/27/2012', 0
ALTER procedure [dbo].[usp_mailer_Monthly_report_data_ver2]
	(
	@CatNum		int,                
	@lettertype 	int,                
	@sDate		datetime,                
	@eDate 		datetime,
	@SearchType	tinyint
	)

as


set nocount on 

-- Waqas Javed 5313 12/30/08 Setting start date from First day of Month and end date to last day of Month
--Sets First day of the current month of start date @sDate
Select @sDate = dateadd(mm,datediff(mm,0,@sDate),0)
--Sets last day of the current month of end date @eDate
Select @eDate = dateadd(ms,- 3,dateadd(mm,0,dateadd(mm,datediff(mm,0,@eDate)+1,0))) 
--Yasir Kamal 7218 01/13/2010 lms structure changed. 		
declare @DBid int
-- 1 = Traffic Tickets
-- 2 = Dallas Traffic Tickets

select @dbid =	 CASE ISNULL(tl.courtcategory,0) WHEN 26 THEN 5 ELSE ISNULL(tcc.LocationId_FK ,0) END
from tblletter tl INNER JOIN tblCourtCategories tcc ON tl.courtcategory = tcc.CourtCategorynum where letterid_pk = @lettertype

SELECT @dbid= ISNULL(@dbid,0)

declare @tblcourts table (courtid int)

----------------------------------------------------------------------------------------------------
--		DALLAS DATABASE....
----------------------------------------------------------------------------------------------------
if @dbid = 2
	BEGIN
		if @catnum < 1000
			insert into @tblcourts 
			select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = @catnum
		else
			insert into @tblcourts select @catnum
	END
----------------------------------------------------------------------------------------------------
--		TRAFFIC TICKETS DATABASE....
----------------------------------------------------------------------------------------------------
ELSE
    BEGIN
		if @catnum < 1000
			insert into @tblcourts 
			select courtid from dbo.tblcourts where courtcategorynum = @catnum
		else
			insert into @tblcourts select @catnum	
    END
	
----------------------------------------------------------------------------------------------------		
--7218 end. 

-- GETTING LETTER HISTORY INTO TEMP TABLE AS PER SELECTION CRITERIA.....
declare @letters table (
	noteid int, 
	mdate datetime
	)

if @searchtype = 0 
	BEGIN
		insert into @letters
		select noteid,  convert(varchar(10), listdate, 101)
		from tblletternotes n, @tblcourts c
		where n.courtid = c.courtid
		and n.lettertype = @lettertype
		and datediff(day, listdate, @sdate)<= 0
		and datediff(day, listdate, @edate)>= 0
	END
else
	BEGIN
		insert into @letters
		select noteid,  convert(varchar(10), recordloaddate, 101)
		from tblletternotes n, @tblcourts c
		where n.courtid = c.courtid
		and n.lettertype = @lettertype
		and datediff(day, recordloaddate, @sdate)<= 0
		and datediff(day, recordloaddate, @edate)>= 0
	END

-- GETTING COST FOR EACH MAILER/LIST DATE....
declare @cost table (
	mdate datetime,
	pcost money
	)

	insert into @cost
	select l.mdate, sum(n.pcost) from tblletternotes n inner join  @letters l
	on l.noteid = n.noteid 
	group by l.mdate



-- GETTING INFORMATION FROM CLIENTS/QUOTES......
declare @QuoteClient table (
	ticketid int, 
	mailerid int,
	activeflag int,
	hiredate datetime,
	quotedate datetime,
	chargeamount money
	)

if @dbid = 2
	insert into @QuoteClient
	select t.ticketid_pk, t.mailerid, t.activeflag, 
			min(p.recdate), t.recdate, isnull(sum(isnull(p.chargeamount,0)),0)
	from dallastraffictickets.dbo.tbltickets t 
	left outer join  
		dallastraffictickets.dbo.tblticketspayment p 
	on	p.ticketid = t.ticketid_pk and p.paymentvoid = 0
	inner join 
		@letters l
	on	l.noteid = t.mailerid
	group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate
else
	insert into @QuoteClient
	select t.ticketid_pk, t.mailerid, t.activeflag, 
			min(p.recdate), t.recdate, isnull(sum(isnull(p.chargeamount,0)),0)
	from tbltickets t 
	left outer join  
		tblticketspayment p 
	on	p.ticketid = t.ticketid_pk and p.paymentvoid = 0
	inner join 
		@letters l
	on	l.noteid = t.mailerid
	group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate

-- SUMMARIZING THE INFORMATION....
declare @temp2 table
		(
		mdate	datetime,
		letterCount	int,
		NonClient	int,
		Client		int,
		Quote		int,
		Revenue		money,
		expense money,
		weekno int
		)

INSERT into @temp2 (mdate, lettercount, client, Quote, revenue)
select l.mdate,  
	count(distinct l.noteid), 
	sum(case q.activeflag when 1 then 1 end), 
	sum(case q.activeflag when 0 then 1 end),  
	sum(q.chargeamount)
from @letters l
left outer join
	@QuoteClient q
on  l.noteid = q.mailerid
group  by l.mdate

 --Modify by Mohammad Ali  9997 01/17/2012 to get Cliend and non Client IDS    
CREATE TABLE #temp3
(
	dates VARCHAR(20),
	ClientsIds VARCHAR(MAX)
)
INSERT INTO #temp3(dates,ClientsIds) 
  SELECT convert(varchar,p1.mdate,101),
       ( SELECT CONVERT(VARCHAR, noteid)+ ','
           FROM  @letters p2
left outer join
	@QuoteClient q
on  p2.noteid = q.mailerid
          WHERE p2.mdate = p1.mdate AND q.activeflag=1
          ORDER BY mdate
            FOR XML PATH('') ) AS ids
      from @letters p1
left outer join
	@QuoteClient q
on  p1.noteid = q.mailerid
  WHERE q.activeflag=1
group  by p1.mdate


------- Non Client ids...

CREATE TABLE #temp4
(
	dates VARCHAR(20),
	nonClientsIds VARCHAR(MAX)
)


INSERT INTO #temp4(dates,nonClientsIds) 
  SELECT convert(varchar,p1.mdate,101),
       ( SELECT CONVERT(VARCHAR, noteid)+ ','
           FROM  @letters p2
left outer join
	@QuoteClient q
on  p2.noteid = q.mailerid
          WHERE p2.mdate = p1.mdate AND q.activeflag=0
          ORDER BY mdate
            FOR XML PATH('') ) AS ids
      from @letters p1
	left outer join
		@QuoteClient q
	on  p1.noteid = q.mailerid
	WHERE q.activeflag=0
	group  by p1.mdate

	CREATE TABLE #temp5
	(
		date VARCHAR(20),
		clientsids VARCHAR(MAX),
		nonclients VARCHAR(MAX)
	)
	INSERT INTO #temp5 (date,clientsids,nonclients)
	SELECT t.mdate, t3.ClientsIds,t4.nonClientsIds
	FROM @temp2 t LEFT OUTER JOIN #temp3 t3 ON t3.dates = t.mdate
	LEFT OUTER JOIN #temp4 t4 ON t4.dates = t.mdate
	
--End of 9997 getting IDs 

update t
set t.expense = p.pcost,
	nonclient = lettercount - (isnull(quote,0)+isnull(client,0))
from @temp2 t, @cost p
where datediff(day, t.mdate, p.mdate ) = 0

	select year(mdate) as ydate, 
		MONTH(mdate) as mdate,
		isnull(sum(nonclient),0) as nonclient, 
		isnull(sum(client),0) as client, 
		isnull(sum(quote),0) as quote,
		isnull(sum(revenue),0) as revenue, 
		isnull(sum(lettercount),0) as lettercount, 
		isnull(sum(expense),0) as expense, 
		isnull(sum(isnull(revenue,0) - expense),0) as profit,
		--Mohammad Ali 9997 01-17-2012 For grouping sum of the following fields according to monthly search report
		NPM = SUM(convert(numeric(10,3),case when isnull(revenue,0) = 0 then 0 else (isnull(revenue,0) - expense)/revenue end)) ,
		ROP = SUM(convert(numeric(10,3),case when isnull(expense,0) = 0 then 0 else (isnull(revenue,0) - expense)/expense end)),
		QPMP =SUM(convert(numeric(10,3),cast(isnull(quote,0)/cast(lettercount as float) as float))),
		HPMP = SUM(convert(numeric(10,3),cast(isnull(client,0)/cast(lettercount as float) as float))),
		PPMP =SUM(convert(numeric(10,3),(isnull(revenue,0) - expense)/cast(lettercount as float))),
		--tahir 4465 07/31/2008 added two new columns to the reports...
		cpmp =SUM(convert(numeric(10,3), cast( (isnull(client,0) + isnull(quote,0)) as float) / cast(lettercount as float))),
		hpq =SUM(convert(numeric(10,3), case when (isnull(client,0) + isnull(quote,0)) = 0 then 0 else cast( isnull(client,0) as float) / cast((isnull(client,0) + isnull(quote,0)) as float) end  ))		
		--End of 9997		
	into #temp
	from @temp2 a 
	group by year(mdate), month(mdate)	

	SELECT  convert(datetime, convert(varchar,mdate)+'/1/'+convert(varchar,ydate),101 ) as HidenDate ,convert(datetime, convert(varchar,mdate)+'/1/'+convert(varchar,ydate),101 ) as mdate,
	nonclient, client , quote, revenue, lettercount, expense, profit,NPM,ROP,QPMP,HPMP,PPMP,
	--tahir 4465 07/31/2008 added two new columns to the reports...
	cpmp,hpq
	-- end 4465
	into #temp2
	from #temp 	
	
	select HidenDate,left( datename( month, mdate ), 3 )+ '-' + right( datepart( year, mdate ), 2 )as mdate,
	SUM(nonclient) AS nonclient, SUM(client) AS client, SUM(quote) AS quote, SUM(revenue) AS revenue, 
	SUM(lettercount) AS lettercount, SUM(expense) AS expense, SUM(profit) AS profit
	,SUM(npm) AS npm, SUM(rop) AS rop, SUM(qpmp) AS qpmp, SUM(hpmp) hpmp, SUM(ppmp) AS ppmp, SUM(cpmp) AS cpmp, SUM(hpq) AS hpq
	,CASE WHEN @DBid =1 THEN 1 ELSE 2 END AS ProjectType
	INTO #tempFinal from #temp2 
	GROUP BY left( datename( month, mdate ), 3 )+ '-' + right( datepart( year, mdate ), 2 ),HidenDate
	ORDER BY HidenDate DESC
	
	
	ALTER TABLE #tempFinal ADD clientsids VARCHAR(MAX), nonclients VARCHAR(MAX)
	
	ALTER TABLE #temp5 ADD rowid INT IDENTITY(1,1)
	
	DECLARE @idx INT
	SET @idx = 1
	DECLARE @TotCount INT
	
	SELECT @TotCount = COUNT(DISTINCT Rowid) FROM #temp5

	WHILE @idx<=@TotCount
	BEGIN
	
	UPDATE TF
		SET TF.clientsids = ISNULL(TF.clientsids,'') + ISNULL(t2.clientsids,''), TF.nonclients = ISNULL(TF.nonclients,'') + ISNULL(t2.nonclients,'')	
		FROM #tempFinal TF INNER JOIN #temp5 t2 ON TF.mdate = left( datename( month, t2.date ), 3 )+ '-' + right( datepart( year, t2.date ), 2 )
	WHERE t2.Rowid = @idx
		
		SET @idx = @idx + 1	
	END
	
	
	UPDATE #tempFinal SET clientsids =  NULL WHERE LEN(clientsids) = 0
	UPDATE #tempFinal SET nonclients =  NULL WHERE LEN(nonclients) = 0
	
	SELECT mdate,nonclient, client , quote, revenue, lettercount, expense, profit,NPM,ROP,QPMP,HPMP,PPMP,	
	cpmp,hpq,ProjectType,clientsids,nonclients  FROM #tempFinal
	
	

drop table #temp
drop table #temp2
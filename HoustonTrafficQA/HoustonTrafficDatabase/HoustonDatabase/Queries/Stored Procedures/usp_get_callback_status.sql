SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_get_callback_status]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_get_callback_status]
GO





create PROCEDURE [dbo].[usp_get_callback_status] --125587    
@TicketID int    
as    
    
declare @rec_count int, @client int    
    
    
    
set @rec_count = (Select count(QuoteID) from tblviolationquote where ticketid_fk = @TicketID)    
set @client = (select activeflag from tbltickets where ticketid_pk = @TicketID)    
    
if(@rec_count>0 or @client = 1)    
 select 1 as ViolationQuote, isnull(@client,0) as ClientFlag    
else    
 select 0 as ViolationQuote, isnull(@client,0) as ClientFlag    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


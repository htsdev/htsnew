SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_Quotes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_Quotes]
GO











CREATE PROCEDURE [dbo].[usp_Get_All_Quotes] 

@TicketID_PK varchar(12)

AS

SELECT DISTINCT 
                      dbo.tblViolationQuote.QuoteID, CONVERT(varchar(12), dbo.tblViolationQuote.LastContactDate, 101) AS LastContactDate, CONVERT(Int, GETDATE() 
                      - dbo.tblViolationQuote.LastContactDate) AS Days, dbo.tblUsers.Firstname + ' ' + dbo.tblUsers.Lastname AS EmpName, 
                      dbo.tblTickets.Firstname + ' ' + dbo.tblTickets.Lastname AS Customer, CONVERT(varchar(12), dbo.tblTickets.ContactDate, 101) AS ContactDate, 
                      CONVERT(decimal(8,2), dbo.tblTickets.calculatedtotalfee) as calculatedtotalfee, dbo.tblViolationQuote.QuoteComments, tblQuoteResult_2.QuoteResultDescription, dbo.tblViolationQuote.FollowUpYN, 
                      dbo.tblTickets.TicketID_PK, REPLACE(tblQuoteResult_1.QuoteResultDescription, '---------------------', ' ') AS FollowUpStatus, CONVERT(varchar(12), 
                      dbo.tblTicketsViolations.CourtDateMain, 101) AS CourtDate,
	         dbo.tblViolationQuote.CallBackDate, dbo.tblViolationQuote.AppointmentDate, 
                      dbo.tblViolationQuote.CallBackTime, dbo.tblViolationQuote.AppointmentTime
FROM         dbo.tblTicketsViolations RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK RIGHT OUTER JOIN
                      dbo.tblViolationQuote LEFT OUTER JOIN
                      dbo.tblQuoteResult tblQuoteResult_1 ON dbo.tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID ON 
                      dbo.tblTickets.TicketID_PK = dbo.tblViolationQuote.TicketID_FK LEFT OUTER JOIN
                      dbo.tblUsers ON dbo.tblTickets.EmployeeID = dbo.tblUsers.EmployeeID LEFT OUTER JOIN
                      dbo.tblQuoteResult tblQuoteResult_2 ON dbo.tblViolationQuote.QuoteResultID = tblQuoteResult_2.QuoteResultID
WHERE     
	(dbo.tblTickets.Activeflag = 0) And (dbo.tblViolationQuote.FollowUpYN = 1) 
--AND 	 Convert(datetime, dbo.tblTicketsViolations.CourtDateMain,101) >= Convert(datetime, GETDATE(),101)
And 	dbo.tblTicketsViolations.CourtDateMain 
IN (Select Max(dbo.tblTicketsViolations.CourtDateMain) 
		from dbo.tblTicketsViolations
			Where dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK )








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_QUOTEREPORT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_QUOTEREPORT]
GO

--[USP_HTS_GET_QUOTEREPORT] '09/12/2007','09/12/2007'

CREATE procedure [dbo].[USP_HTS_GET_QUOTEREPORT]
--declare
@fromDate datetime, 
@toDate datetime

as 

--set @fromDate = '08/01/2007' + ' 00:00:00'
--set @toDate = '09/10/2007' + ' 23:59:59'


set @fromDate = @fromDate + ' 00:00:00'
set @toDate = @toDate + ' 23:59:59'


select distinct ticketid_fk, convert(varchar(12), q.callbackdate, 101) as LastContactDate, followupid
	into #temp
	from tblViolationQuote Q 
				left outer join tbltickets t on t.ticketid_pk = q.ticketid_fk
				left outer join tblticketsviolations tv on q.ticketid_fk = tv.ticketid_pk
				left outer join tblticketspayment p on t.ticketid_pk = p.ticketid 
	where q.callbackdate between @fromDate and @toDate 
		and followupYN = 1 
				--and  activeflag = 0 
				 and (q.recdate > p.recdate or  p.recdate is null)

				--and tv.CourtDateMain  > GETDATE()

--select  * from tblQuoteResult tblViolationQuote order by quoteid desc

select t.LastContactDate [Contact Date] ,
count(distinct t.ticketid_fk)  as [Total Quote], 
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 2 and WCIn.LastContactDate = t.LastContactDate) as [Will Come In],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 3 and WCIn.LastContactDate = t.LastContactDate) as [Thinking About It],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 4 and WCIn.LastContactDate = t.LastContactDate) as [Price Shopping],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 6 and WCIn.LastContactDate = t.LastContactDate) as [No Money],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 7 and WCIn.LastContactDate = t.LastContactDate) as [Too Expensive],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 8 and WCIn.LastContactDate = t.LastContactDate) as [Will Take DSC],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 9 and WCIn.LastContactDate = t.LastContactDate) as [Not Interested],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 10 and WCIn.LastContactDate = t.LastContactDate) as [Handle Myself],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 11 and WCIn.LastContactDate = t.LastContactDate) as [Hired Other Attorney],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 12 and WCIn.LastContactDate = t.LastContactDate) as [No Direct Quote],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid = 13 and WCIn.LastContactDate = t.LastContactDate) as [Other],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid in (14, 5, 1)  and WCIn.LastContactDate = t.LastContactDate) as [No Selection found],
(select count(distinct ticketid_fk) from #temp WCIn where WCIn.followupid is null  and WCIn.LastContactDate = t.LastContactDate) as [Null Value]

	into #temp1	
		from #temp t
	group by t.LastContactDate -- , followupid	


select LastContactDate, count(ticketid) as [Total Hire]
	into #tempSameDay
		from tblticketspayment p, #temp q
	where p.ticketid = q.ticketid_fk
and paymenttype not in (99, 100)
and  p.PaymentVoid = 0
group by LastContactDate

 


select t.*, t1.[Total Hire]  from #temp1 t left outer join #tempSameDay t1 on t.[Contact Date] = t1.LastContactDate

--
--select * from #temp1
--
--
drop table #temp1
drop table #tempSameDay
drop table #temp
--


--
--
--
--select LastContactDate, count(ticketid) as [Same Day]
--	into #tempSameDay
--		from tblticketspayment p, #temp q
--	where p.ticketid = q.ticketid_fk
--and paymenttype not in (99, 100)
--and  p.PaymentVoid = 0
--
--group by LastContactDate
--
--
--select LastContactDate, count(ticketid) as [Next Day], followupid
--	into #tempNextDay
--		from tblticketspayment p, #temp q
--	where p.ticketid = q.ticketid_fk
--and paymenttype not in (99, 100)
--and  p.PaymentVoid = 0
--and datediff(day, LastContactDate, recdate )  = 1
--group by LastContactDate, followupid
--
--
--select LastContactDate, count(ticketid) as [other], q.followupid
--	into #tempRemaining
--		from tblticketspayment p, #temp q
--	where p.ticketid = q.ticketid_fk
--and paymenttype not in (99, 100)
--and  p.PaymentVoid = 0
--and datediff(day, LastContactDate, recdate )  > 1
--group by LastContactDate, q.followupid
--
--select t1.LastContactDate, 
--	t1.[Total Quote],   
--	isnull(th.[Total Hire],0) as [Total Hire], 
--	isnull(t2.[Same Day],0) as [Same Day], 
--	isnull(tn.[Next Day],0) as [Next Day],
--	isnull(tr.[other],0) as [other],
--	isnull(qr.quoteresultdescription, 'Not Yet Called') as quoteresultdescription
--
--from #temp1 t1 
--		left outer join  #tempSameDay t2 on t1.LastContactDate = t2.LastContactDate and t1.followupid = t2.followupid
--		left outer join #TempTotalHire th on t1.LastContactDate = th.LastContactDate and t1.followupid = th.followupid
--		left outer join #tempNextDay tn on  t1.LastContactDate = tn.LastContactDate and t1.followupid = tn.followupid
--		left outer join #tempRemaining tr on t1.LastContactDate = tr.LastContactDate and t1.followupid = tr.followupid
--		left outer join tblQuoteResult qr on t1.followupid = qr.quoteresultid
--order by t1.LastContactDate, t1.followupid
--
--drop table #temp 
--drop table #temp1
--drop table #tempSameDay
--drop table #tempNextDay
--drop table #tempRemaining
--drop table #TempTotalHire

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


 /****** 
Create by:  Syed Muhammad Ozair

-- Yasir Kamal 4924 01/02/2009 List Date Column Added.

Business Logic : this procedure is used to get any alert for letters printed from automated mailer at individual list date when the currnet
				 printed letter are below 50% of letters printed last week or letter count printed today is 0 .

List of Columns:

courtcategoryname 	                 
listdate
lettername
todaycount
lastweekcount

	Alert : it will dislapy the alert in this format e.g.
			"Houston Municipal Courts Arraignment1 letters printed on 05/09/2008 are 46% of letters printed last week."

******/



ALTER procedure dbo.usp_Mailer_Get_LetterCountAlert

	@MailerDate DATETIME = null

as

-- Yasir Kamal 4924 01/02/2009 List Date Column Added.

IF @MailerDate IS NULL
	SET @mailerDate = CONVERT(VARCHAR(10), GETDATE(), 101)


DECLARE @listdates TABLE ( rowid INT IDENTITY, letterid INT, courtid INT, sortorder INT, startdate DATETIME, enddate DATETIME)

-- tahir 4924 2/16/2009 fixed the bug related to missing list dates for alert..
-- GET LETTER INFORMATION WITH DEFAULT START/END DATES IN TEMP TABLE FOR EACH LIST DATE...
INSERT INTO @listdates(letterid, courtid,sortorder,startdate,enddate)
select	distinct 
	l.letterid_pk, b.courtid, l.sortorder,
	dateadd(day, ( l.DefaultStartDate  - (CASE WHEN DATEPART(dw, @mailerDate) = 2 AND l.CanPrintForOffDays =1  AND  l.OffDayType = 1 THEN 2 ELSE 0 end) ) , @mailerDate) as StartDate,
	dateadd(day,  (l.DefaultEndDate + (CASE WHEN DATEPART(dw, @mailerDate) = 6 AND l.CanPrintForOffDays = 1 AND  l.OffDayType = 2 THEN 2 ELSE 0 end))  , @mailerDate) as EndDate
from 
	tblletter l, 
	tblbatchletter b, 
	tblcourtcategories c
where 
	l.letterid_pk = b.lettertype
	and b.courtid = c.courtcategorynum
	and l.isautomated = 1 
order by 
	b.courtid,
	l.sortorder

DECLARE @idx INT, @reccount INT, @tempidx INT,  @letterid INT, @courtid INT, @sortorder INT, @startdate DATETIME, @enddate DATETIME
SELECT @idx= 1, @tempidx = 0, @reccount = COUNT(rowid) FROM @listdates
WHILE @idx <= @reccount
BEGIN
	SELECT @letterid  = letterid, @courtid= courtid, @sortorder = sortorder, @startdate = startdate, @enddate = enddate
	FROM @listdates WHERE rowid = @idx
	
	while @startdate <= @enddate
		BEGIN
			INSERT INTO @listdates (letterid, courtid, sortorder, startdate, enddate)
			SELECT @letterid, @courtid, @sortorder,  convert(varchar(10), @startdate,101),  @startdate
			
			SET @startdate =  DATEADD (DAY, 1,@startdate)
		END


	SET @idx = @idx + 1
END


DELETE FROM @listdates WHERE DATEDIFF(DAY, startdate, enddate) <> 0



select	distinct 
	l.letterid_pk, 
	l.lettername, 
	a.startdate AS listdate,
	DATEADD(DAY, -7, a.startdate) AS lastweekdate,
	b.courtid, 
	c.courtcategoryname, 
	l.sortorder
into
	#temp
from 
	@listdates a
	LEFT OUTER JOIN tblletter l ON a.letterid = l.LetterID_PK 
	INNER JOIN tblbatchletter b  ON l.letterid_pk = b.lettertype 
	INNER JOIN tblcourtcategories c ON b.courtid = c.courtcategorynum
order by b.courtid, l.sortorder
	
alter table #temp
	add todaycount float,
		lastweekcount float,
		per float


-- GET TODAY'S COUNT FOR EACH LETTER AND EACH LIST DATE...
-- Yasir 5905 05/27/2009 fixed the today count bug...
SELECT a.letterid_pk, a.listdate, ISNULL(SUM(ISNULL(b.LCount,0)),0) AS lcount
INTO #todaycount
FROM #temp a LEFT OUTER JOIN tblbatchletter b ON a.letterid_pk = b.LetterType AND DATEDIFF(DAY, a.listdate, b.ListDate) = 0
GROUP BY a.letterid_pk, a.listdate


UPDATE a
SET a.todaycount = b.lcount 
FROM #temp a INNER JOIN #todaycount b ON a.letterid_pk = b.Letterid_pk AND DATEDIFF(DAY, a.listdate, b.ListDate) = 0


-- GET PAST WEEK COUNT FOR EACH LETTER AND EACH LIST DATE..
-- Yasir 5905 05/27/2009 fixed the today count bug...
SELECT a.letterid_pk, a.lastweekdate, ISNULL(SUM(ISNULL(b.LCount,0)),0) AS lcount
INTO #lastweek
FROM #temp a LEFT OUTER JOIN tblbatchletter b ON a.letterid_pk = b.LetterType AND DATEDIFF(DAY, a.listdate, b.ListDate) = -7
GROUP BY a.letterid_pk, a.lastweekdate


UPDATE a
SET a.lastweekcount = b.LCount 
FROM #temp a INNER JOIN #lastweek b ON a.letterid_pk = b.Letterid_pk AND DATEDIFF(DAY, a.lastweekdate, b.lastweekdate) = 0

-- UPDATE PERCENTAGE ......
update #temp
set per = Convert(numeric(10,2),(isnull(todaycount,0)/ case when isnull(lastweekcount,0) >0 then lastweekcount else 1 end )*100)

-- FINAL OUT PUT....
-- DISPLAY RECORDS HAVING TODAY COUNT ZERO OR LESS THAN 50% TO LAST WEEK COUNT...
SELECT  courtcategoryname, lettername, convert(varchar,listdate,101)as listdate, isnull(convert(int,todaycount),0) as todaycount, 
		CONVERT(VARCHAR(10), LASTweekdate, 101) AS lastweekdate, isnull(convert(int,lastweekcount),0) as lastweekcount,isnull(per,0) as per 
from #temp 
where (  per BETWEEN 0 AND 50  OR todaycount = 0 )

drop table #temp
DROP TABLE #todaycount
DROP TABLE #lastweek


﻿
/**************

Business Logic:  This stored procedure is used to update open service ticket information 
				 and also add note in case history when comments are updated.

Input Parameters:  
@TicketID
@Fid 
@EmpID
@per_completion 
@Category
@Priority
@ShowTrialDocket
@ShowServiceInstruction
@ServiceInstruction
@AssignedTo
@ShowGeneralComments
@ContinuanceOption
@EnteredComments
*********/
      
ALTER  procedure [dbo].[usp_hts_updategeneralcomments_FromOpenServieTicketReport]       --4764,4368,3991,'Test',0,4,0,1,0,'Test'                 
                        
	    @TicketID AS INT,
        @Fid INT,
        @EmpID AS INT,
        @per_completion AS INT,
        @Category INT,
        @Priority INT,
        @ShowTrialDocket INT,
        @ShowServiceInstruction INT,
        @ServiceInstruction VARCHAR(2000),
        @AssignedTo INT,
        @ShowGeneralComments BIT,
        @ContinuanceOption INT,
        @EnteredComments VARCHAR(2000), --Fahad 5815 04/28/2009 Add Parameter @EnteredComments
        @Followupdate DATETIME  --Sabir Khan 5738 05/27/2009 Add Parameter...
        AS            
                   
                      

DECLARE @empnm as varchar(12)                                                    
select @empnm = abbreviation from tblusers where employeeid = @EmpID  
DECLARE @OldComments VARCHAR(2000)
SELECT @OldComments =ISNULL (ServiceTicketInstruction,'') FROM tblticketsflag WHERE  ticketid_pk = @TicketID AND fid = @Fid	 	
update tblTicketsFlag         
  set           
   ContinuanceStatus=@per_completion,           
   Priority=@Priority,          
   serviceticketcategory=@category,           
   Lastupdatedate = getdate(),        
   ShowTrailDocket = @ShowTrialDocket  ,      
   ServiceTicketInstruction = CASE (LTRIM (RTRIM (@EnteredComments))) WHEN '' THEN @OldComments ELSE  @OldComments+' '+@EnteredComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' ' END,      
   AssignedTo = case when @AssignedTo=0 then AssignedTo else @AssignedTo end,       
   ShowGeneralComments = @ShowGeneralComments,      
   ContinuanceOption = @ContinuanceOption ,
   --Sabir Khan 5738 05/27/2009 service ticket follow update...
   ServiceTicketFollowupDate = CONVERT(DATETIME,dbo.fn_DateFormat(@Followupdate,27,'/',':',1)) 
   where           
   (ticketid_pk = @TicketID and           
   FlagID=23 and Fid = @Fid)  
   

IF LEN(@EnteredComments) > 0
BEGIN
	--Fahad 5815 04/28/2009 Insert notes in history just new Comments           
	Insert Into tblTicketsNotes(TicketID,Subject,EmployeeID) Values(@TicketID, 'Service Ticket Notes: ' + UPPER (@EnteredComments)+' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' ', Convert(Varchar(10),@EmpID)) 	
END
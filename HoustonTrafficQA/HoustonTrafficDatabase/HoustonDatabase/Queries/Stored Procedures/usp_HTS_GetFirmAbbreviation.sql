SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetFirmAbbreviation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetFirmAbbreviation]
GO



CREATE procedure [dbo].[usp_HTS_GetFirmAbbreviation] --56555
@FirmID int 
as

declare @FirmAbbreviation varchar(10)

Select @FirmAbbreviation = FirmAbbreviation  from tblfirm where firmid = @FirmID
Select isnull(@FirmAbbreviation,'SULL')




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Inactive_SchedulePayment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Inactive_SchedulePayment]
GO


CREATE procedure USP_HTS_Inactive_SchedulePayment  
  
@ScheduleID  int  
  
as   
  
Update tblschedulepayment  
set scheduleflag=0  
where scheduleid=@ScheduleID   

-------------if no payment is paid and schedule count is zero the set active flag=0
declare @chargeamount money,
	@ticketid int
select @ticketid=ticketid_pk from tblschedulepayment where scheduleid=@ScheduleID   
--Cheking if charge amount in null        
 select  @chargeamount = sum(chargeamount)        
 from tblticketspayment where  ticketid =@ticketid        
 and paymenttype not in (99,100)        
 and paymentvoid = 0  

--checking if schedule payment already exist    
select scheduleid from tblschedulepayment
where ticketid_pk= @ticketid and scheduleflag<>0
      
 if ( @chargeamount is null  and @@rowcount =0  )
 begin        
  update tbltickets set activeflag = 0 where ticketid_pk = @ticketid        
 end          
   
  
  
  
  
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


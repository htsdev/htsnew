﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/22/2009 3:33:46 PM
 ************************************************************/

/************************************************************  
 * Code formatted by SoftTree SQL Assistant © v4.0.34  
 * Time: 4/14/2009 3:35:27 PM  
 ************************************************************/  
  
/***  
* Created By : Waqas javed  5771 04/14/2009  
* Business Logic : This procedure is used to get records with following logics  
* Records will be displayed whose last name, date of birth and first name initial is same      
* Parameter :
* @LastName
* @FirstInitial
* @DOB
***/  
    
Create PROCEDURE dbo.USP_HTP_GET_ContactLookUp
	@LastName VARCHAR(20),
	@FirstInitial VARCHAR(1),
	@DOB DATETIME
AS
	SELECT c.ContactID,
	       ISNULL(c.FirstName, '') AS FirstName,
	       --ISNULL(c.LastName, '') AS LastName,    
	       '<a href="javascript:window.open(' +
	       '''/ClientInfo/AssociatedMatters.aspx?cid=' + CONVERT(VARCHAR(20), c.ContactID) 
	       + ''');void('''');">' + ISNULL(c.LastName, '') + '</a> ' AS LastName,
	       ISNULL(CONVERT(VARCHAR(12), c.DOB, 101), '') AS DOB,
	       ISNULL(c.DLNumber, '') AS DLNumber,
	       ISNULL(c.Address1, '') +
	       CASE 
	            WHEN ISNULL(c.Address2, '') = '' THEN ''
	            ELSE ' ' + c.Address2
	       END + ', ' + ISNULL(c.City, '') 
	       + ', ' +
	       ISNULL(
	           (
	               SELECT TOP 1 ts.[State]
	               FROM   tblState ts
	               WHERE  ts.StateID = c.StateID_FK
	           ),
	           ''
	       ) + ' ' + ISNULL(c.Zip, '') AS ADDRESS,
	       CASE 
	            WHEN LEN(
	                     ISNULL(c.Address1, '') + ' ' + ISNULL(c.Address2, '') +
	                     ', ' + ISNULL(c.City, '') + ', ' +
	                     ISNULL(
	                         (
	                             SELECT TOP 1 ts.[State]
	                             FROM   tblState ts
	                             WHERE  ts.StateID = c.StateID_FK
	                         ),
	                         ''
	                     ) + ' ' + ISNULL(c.Zip, '')
	                 ) > 15 THEN SUBSTRING(
	                     ISNULL(c.Address1, '') + ' ' + ISNULL(c.Address2, '') +
	                     ', ' + ISNULL(c.City, '') + ', ' +
	                     ISNULL(
	                         (
	                             SELECT TOP 1 ts.[State]
	                             FROM   tblState ts
	                             WHERE  ts.StateID = c.StateID_FK
	                         ),
	                         ''
	                     ) + ' ' + ISNULL(c.Zip, ''),
	                     1,
	                     15
	                 ) + '...'
	            WHEN LEN(
	                     LTRIM(
	                         RTRIM(
	                             ISNULL(c.Address1, '') + ISNULL(c.Address2, '') 
	                             +
	                             ISNULL(c.City, '') +
	                             ISNULL(
	                                 (
	                                     SELECT TOP 1 ts.[State]
	                                     FROM   tblState ts
	                                     WHERE  ts.StateID = c.StateID_FK
	                                 ),
	                                 ''
	                             ) + ISNULL(c.Zip, '')
	                         )
	                     )
	                 ) = 0 THEN ''
	            ELSE ISNULL(c.Address1, '') + ' ' + ISNULL(c.Address2, '') +
	                 ', ' + ISNULL(c.City, '') + ', ' +
	                 ISNULL(
	                     (
	                         SELECT TOP 1 ts.[State]
	                         FROM   tblState ts
	                         WHERE  ts.StateID = c.StateID_FK
	                     ),
	                     ''
	                 ) + ' ' + ISNULL(c.Zip, '')
	       END AS Address1,
	       (
	           SELECT MAX(tt.ticketid_pk)
	           FROM   tblTickets tt
	           WHERE  tt.ContactID_FK = c.ContactID
	       ) AS TicketID
	FROM   Contact c
	WHERE  ISNULL(c.LastName, '') LIKE ISNULL(@LastName, '')
	       AND ISNULL(c.FirstName, '') LIKE @FirstInitial + '%'
	       AND (CONVERT(DATETIME, c.DOB, 101)) = CONVERT(DATETIME, @DOB, 101)
GO

GRANT EXECUTE ON [dbo].[USP_HTP_GET_ContactLookUp] TO dbr_webuser

GO

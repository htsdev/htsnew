USE TrafficTickets
GO
/******  
* Created By :	Sameeullah Daris.
* Create Date :   2/3/2011 4:22:15 AM
* Task ID :				8882
* Business Logic :  This procedure is used to insert File Records for Attorney Cases .
* List of Parameter :
* @FilePath: Path of the file.
* @FileDisplayName: Name of file.
* @Description:	Discription from user.
* @InsertedBy:	ID of user whose inserting the file.
* @NewId:	Output Paramter.
*       
******/

ALTER PROCEDURE [dbo].[USP_HTP_Insert_AttorneyCasesFiles]
	@FilePath VARCHAR(300),
	@FileDisplayName VARCHAR(150),
	@Description VARCHAR(2000),
	@InsertedBy INT
	--@NewId INT OUTPUT
AS
declare @NewId int
	INSERT INTO OutsideAttorneyCasesFiles
	  (
	    FilePath,
		FileDisplayName,
		[Description],
	    Uploadedby,
	    UploadedDate,
	    UpdatedBy,
	    UpdatedDate
	  )
	VALUES
	  (
	   
	    @FilePath,
	    @FileDisplayName,
	    @Description,
	    @InsertedBy,
	    GETDATE(),
	    @InsertedBy,
	    GETDATE()
	  )
	SET @NewId = SCOPE_IDENTITY()
	SELECT @NewId as FileID

GO

GRANT EXECUTE ON [dbo].[USP_HTP_Insert_AttorneyCasesFiles] TO dbr_webuser
GO


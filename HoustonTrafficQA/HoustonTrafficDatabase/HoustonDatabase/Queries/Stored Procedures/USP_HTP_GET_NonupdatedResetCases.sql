set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/******
* Created By :		Babar Ahmad
* Create Date :		03/29/2011
* Task ID :			9055
* Business Logic :	Retrieve all cases other than HMC and HCJP whose Court Status is "Waiting" and document is uploaded. 
* List of Parameter:
* List of Columns:
*		TicketID
*		Cause Number
*		Ticket Number
*		Last Name
*		First Name
*		Court Status
*		Document Uploaded
******/
-- USP_HTP_GET_NonupdatedResetCases 1,0

ALTER PROCEDURE [dbo].[USP_HTP_GET_NonupdatedResetCases]
	@ShowAll BIT,
	@ValidationCategory INT = 0
AS
BEGIN
    SELECT DISTINCT t.TicketID_PK,
           tv.casenumassignedbycourt AS CauseNumber,
           tv.RefCaseNumber AS TicketNumber,
           t.Lastname,
           t.Firstname,
           csv.[Description],
           tv.TicketsViolationID,
           CASE 
                WHEN (
                         SELECT COUNT(*)
                         FROM   tblScanBook
                         WHERE  ISNULL(IsDeleted, 0) = 0
                                AND TicketID = t.ticketid_pk
                                AND Events NOT LIKE '%Print%'
                     ) > 0 THEN 'Yes'
                ELSE CASE 
                          WHEN (
                                   SELECT COUNT(*)
                                   FROM   tbl_htp_documenttracking_batch hdb
                                          INNER JOIN 
                                               tbl_htp_documenttracking_batchtickets 
                                               dbt
                                               ON  dbt.BatchID_FK = hdb.BatchID
                                   WHERE  dbt.VerifiedDate IS NOT NULL
                                          AND dbt.TicketID_FK = t.TicketID_PK
                               ) > 0 THEN 'Yes'
                          ELSE CASE 
                                    WHEN (
                                             SELECT COUNT(*)
                                             FROM   tblLORBatchHistory lbh
                                                    INNER JOIN 
                                                         tblHTSBatchPrintLetter 
                                                         hbp
                                                         ON  hbp.BatchID_PK = 
                                                             lbh.BatchID_FK
                                             WHERE  hbp.USPSResponse IS NOT NULL
                                                    AND hbp.EDeliveryFlag = 1
                                                    AND hbp.IsPrinted = 1
                                                    AND lbh.TicketID_PK = t.TicketID_PK
                                                    AND hbp.TicketID_FK = t.TicketID_PK
                                         ) > 0 THEN 'Yes'
                                    ELSE 'No'
                               END
                     END
           END AS DocumentUploaded
    FROM   tbltickets t
           INNER JOIN tblticketsviolations tv
                ON  tv.TicketID_PK = t.TicketID_PK
           INNER JOIN tblCourtViolationStatus csv
                ON  csv.CourtViolationStatusID = tv.CourtViolationStatusIDmain
           -- Babar Ahmad 9727 11/02/2011 Fixed issue of HCJP clients which were appearing in the report.    
           INNER JOIN tblcourts c
                ON  c.Courtid = tv.CourtID AND c.courtcategorynum NOT IN (1,2)
    WHERE  ISNULL(t.Activeflag, 0) = 1
           AND tv.CourtViolationStatusIDmain = 104
           AND (@ShowAll = 1 OR @ValidationCategory = 2)
           AND (
                   (
                       -- Babar Ahmad 9727 10/19/2011 Document upload date should be greater than the date on which status was changed to Waiting.
                       (
                           SELECT MAX(UPDATEDATETIME) -- Document Upload Date
                           FROM   tblScanBook
                           WHERE  ISNULL(IsDeleted, 0) = 0
                                  AND TicketID = t.ticketid_pk
                                  AND Events NOT LIKE '%Print%'
                       ) >(
                           SELECT MAX(tvl.recdate) -- Status change date
                           FROM   tblticketsviolationlog tvl
                           WHERE  tvl.TicketviolationID = tv.TicketsViolationID
                                  AND tvl.newverifiedstatus = 104
                       )
                       AND ISNULL(tv.IsNonUpdatedWaitingRemoved, 0) = 0
                   )
                   OR (
                          -- Babar Ahmad 9727 10/19/2011 Document upload date should be greater than the date on which status was changed to Waiting.
                          (
                              SELECT MAX(dbt.VerifiedDate) -- Document upload (LOR) date
                              FROM   tbl_htp_documenttracking_batch hdb
                                     INNER JOIN 
                                          tbl_htp_documenttracking_batchtickets 
                                          dbt
                                          ON  dbt.BatchID_FK = hdb.BatchID
                              WHERE  dbt.VerifiedDate IS NOT NULL
                                     AND dbt.TicketID_FK = t.TicketID_PK
                          ) >(
                              SELECT MAX(tvl.recdate) -- Status change date
                              FROM   tblticketsviolationlog tvl
                              WHERE  tvl.TicketviolationID = tv.TicketsViolationID
                                     AND tvl.newverifiedstatus = 104
                          )
                          AND ISNULL(tv.IsNonUpdatedWaitingRemoved, 0) = 0
                      )
                   OR (
                          -- Babar Ahmad 9727 10/19/2011 USPS Confirmation date should be greater than the date on which status was changed to Waiting.
                          (
                              SELECT MAX(hbp.USPSConfirmDate) -- USPS Confirmation date
                              FROM   tblLORBatchHistory lbh
                                     INNER JOIN tblHTSBatchPrintLetter hbp
                                          ON  hbp.BatchID_PK = lbh.BatchID_FK
                              WHERE  hbp.USPSResponse IS NOT NULL
                                     AND hbp.EDeliveryFlag = 1
                                     AND hbp.IsPrinted = 1
                                     AND lbh.TicketID_PK = t.TicketID_PK
                                     AND hbp.TicketID_FK = t.TicketID_PK
                          ) >(
                              SELECT MAX(tvl.recdate) -- Status change date
                              FROM   tblticketsviolationlog tvl
                              WHERE  tvl.TicketviolationID = tv.TicketsViolationID
                                     AND tvl.newverifiedstatus = 104
                          )
                          AND ISNULL(tv.IsNonUpdatedWaitingRemoved, 0) = 0
                      )
               )
           OR  (
                   (
                       -- Babar Ahmad 9727 10/19/2011 Document upload date should be greater than the date on which record was removed.
                       (
                           (
                               (
                                   SELECT MAX(UPDATEDATETIME) -- Document Upload Date
                                   FROM   tblScanBook
                                   WHERE  ISNULL(IsDeleted, 0) = 0
                                          AND TicketID = t.ticketid_pk
                                          AND Events NOT LIKE '%Print%'
                               )
                               >
                               
                               
                               ISNULL(tv.NonUpdatedWaitingRemovedDate, '1/1/1900')
                           )
                           AND (
                                   ISNULL(tv.IsNonUpdatedWaitingRemoved, 0) = 1
                                   AND tv.CourtViolationStatusIDmain = 104
                               )
                       )
                   )
                   OR (
                          -- Babar Ahmad 9727 10/19/2011 Document upload date should be greater than the date on which record was removed.
                          (
                              ISNULL(
                                  (
                                      SELECT MAX(dbt.VerifiedDate) -- Document upload (LOR) date
                                      FROM   tbl_htp_documenttracking_batch hdb
                                             INNER JOIN 
                                                  tbl_htp_documenttracking_batchtickets 
                                                  dbt
                                                  ON  dbt.BatchID_FK = hdb.BatchID
                                      WHERE  dbt.VerifiedDate IS NOT NULL
                                             AND dbt.TicketID_FK = t.TicketID_PK
                                  ),
                                  '1/1/1900'
                              ) >
                              -- Babar Ahmad 9727 10/27/2011 Default remove date should be 1/1/1900
                              ISNULL(tv.NonUpdatedWaitingRemovedDate, '1/1/1900')
                          )
                          AND (
                                  ISNULL(tv.IsNonUpdatedWaitingRemoved, 0) = 1
                                  AND tv.CourtViolationStatusIDmain = 104
                              )
                      )
                   OR (
                          (
                              -- Babar Ahmad 9727 10/19/2011 USPS confirmation date should be greater than the date on which record was removed.
                              
                              
                              
                              ISNULL(
                                  (
                                      SELECT MAX(hbp.USPSConfirmDate) -- USPS Confirmation date
                                      FROM   tblLORBatchHistory lbh
                                             INNER JOIN tblHTSBatchPrintLetter 
                                                  hbp
                                                  ON  hbp.BatchID_PK = lbh.BatchID_FK
                                      WHERE  hbp.USPSResponse IS NOT NULL
                                             AND hbp.EDeliveryFlag = 1
                                             AND hbp.IsPrinted = 1
                                             AND lbh.TicketID_PK = t.TicketID_PK
                                             AND hbp.TicketID_FK = t.TicketID_PK
                                  ),
                                  '1/1/1900'
                              ) >
                              
                              
                              ISNULL(tv.NonUpdatedWaitingRemovedDate, '1/1/1900')
                          )
                          -- Babar Ahmad 9727 10/27/2011 Remove date should be greater then USPS confirmation date when case is in waiting and removed from report.
                          AND (
                                  ISNULL(tv.IsNonUpdatedWaitingRemoved, 0) = 1
                                  AND tv.CourtViolationStatusIDmain = 104
                              )
                      )
               )
END











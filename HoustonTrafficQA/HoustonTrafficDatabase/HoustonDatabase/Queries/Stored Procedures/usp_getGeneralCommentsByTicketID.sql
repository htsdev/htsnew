/****** 
Create by:  UnKnown

Business Logic : The procedure is used to get general comments for a case.

List of Parameters:	
	@TicketId : Identification of case to fetch general comments

List of Columns: 	
	GeneralComments: 

******/
  
alter procedure usp_getGeneralCommentsByTicketID  
  
@ticketid as int   
as  
  
--Getting general comments   

-- tahir 4398 07/12/2008  
--select  generalcomments from tbltickets   
select replace(generalcomments, '"','') as  generalcomments from tbltickets
where ticketid_pk = @ticketid  
  
  
  
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_Hts_Check_CourtidNull]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_Hts_Check_CourtidNull]
GO


create procedure Usp_Hts_Check_CourtidNull 

@ticketid int

as


Select Count(courtid) from tblticketsviolations where ticketid_pk = @ticketid and 
courtid = 0 or
 courtid is  null
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿ /****** 
Created by:		Farrukh iftikhar
Task ID :		9332
Business Logic:	The Procedure is used to get the event dial	time
	
List of Parameter :
				@EventTypeID	: Event Type ID
List of Columns:
				EventTypeID,
				DialTime
			
*******/

CREATE Procedure dbo.usp_AD_Get_EventDialTimeByEventTypeID
@EventTypeID TINYINT
as

SELECT EventTypeID,DialTime
FROM   AutoDialerEventDialTime
WHERE EventTypeID = @EventTypeID


go
grant exec on dbo.usp_AD_Get_EventDialTimeByEventTypeID to dbr_webuser
go
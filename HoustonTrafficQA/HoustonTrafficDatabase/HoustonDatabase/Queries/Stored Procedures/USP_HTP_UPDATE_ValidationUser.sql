
/*  
* Created By		: SAEED AHMED
* Task ID			: 7791  
* Created Date		: 05/27/2010  
* Business logic	: This procedure update primary/secondary users who are associated with the reports. 
*					  For each report there are two users[Primary & Secondary] and each primary & secondary
*					  user is also associated with weekdays & saturdays. So for one user there may be 
*					  maximum 6 rows associated with days[Mon,Tue,Wed,Thu,Fri,Sat], therefore for both 
*					  users[Primary & Secondary] there can be maximum 12 entries for one reports.
*					  If primary/secondary user is already associated with the report, this procedure 
*					  updates the entry with new user otherwise it creates a new entry in 'ValidationReportSettings'
*					  table for that user. 
*					   
*					  					  
* Parameter List  
* @ReportID			: Represents reportID whose user need to be updated.
* @Pri_Mon			: Represents primary user of monday for that report.
* @Pri_Tue			: Represents primary user of tuesday for that report.
* @Pri_Wed			: Represents primary user of wednesday for that report.
* @Pri_Thu			: Represents primary user of thursday for that report.
* @Pri_Fri			: Represents primary user of friday for that report.
* @Pri_Sat			: Represents primary user of saturday for that report.
* @Sec_Mon			: Represents secondary user of monday for that report.
* @Sec_Tue			: Represents secondary user of tuesday for that report.
* @Sec_Wed			: Represents secondary user of wednesday for that report.
* @Sec_Thu			: Represents secondary user of thursday for that report.
* @Sec_Fri			: Represents secondary user of friday for that report.
* @Sec_Sat			: Represents secondary user of saturday for that report.
* 
*/  

CREATE PROC dbo.USP_HTP_UPDATE_ValidationUser    
@ReportID INT,    
@Pri_Mon INT,    
@Pri_Tue INT,    
@Pri_Wed INT,    
@Pri_Thu INT,    
@Pri_Fri INT,    
@Pri_Sat INT,    
@Sec_Mon INT,    
@Sec_Tue INT,    
@Sec_Wed INT,    
@Sec_Thu INT,    
@Sec_Fri INT,    
@Sec_Sat INT    
AS    

-- Each day is associated with following numbers.
-- Sun=1,Mon=2,Tue=3,Wed=4,Thu=5,Fri=6,Sat=7    
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=2)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Pri_Mon WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=2;        
 END    
ELSE  
 BEGIN  
 IF @Pri_Mon <> -1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Pri_Mon,2,0,GETDATE());   
 END  
 END   
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=3)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Pri_Tue WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=3;      
 END    
ELSE  
 BEGIN  
 IF @Pri_Tue<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Pri_Tue,3,0,GETDATE());  
 END  
 END    
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=4)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Pri_Wed WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=4;      
 END    
ELSE  
 BEGIN  
 IF @Pri_Wed<> -1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Pri_Wed,4,0,GETDATE());  
 END    
 END   
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=5)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Pri_Thu WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=5;      
 END    
ELSE  
 BEGIN  
  IF @Pri_Thu<>-1  
  BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Pri_Thu,5,0,GETDATE());   
  END  
 END    
  
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=6)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Pri_Fri WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=6;      
 END    
ELSE  
 BEGIN  
 IF @Pri_Fri<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Pri_Fri,6,0,GETDATE());  
 END  
 END   
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=7)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Pri_Sat WHERE ReportID=@ReportID AND UserType=0 AND DayNumber=7;      
 END    
ELSE  
 BEGIN  
 IF @Pri_Sat<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Pri_Sat,7,0,GETDATE());  
 END    
 END    
  
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=2)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Sec_Mon WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=2;      
 END    
ELSE  
 BEGIN  
 IF @Sec_Mon<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Sec_Mon,2,1,GETDATE());  
 END  
 END   
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=3)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Sec_Tue WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=3;      
 END    
ELSE  
 BEGIN  
 IF @Sec_Tue<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Sec_Tue,3,1,GETDATE());  
 END  
 END    
  
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=4)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Sec_Wed WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=4;      
 END    
ELSE  
 BEGIN  
 IF @Sec_Wed<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Sec_Wed,4,1,GETDATE());  
 END  
 END   
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=5)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Sec_Thu WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=5;      
 END    
ELSE  
 BEGIN  
 IF @Sec_Thu<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Sec_Thu,5,1,GETDATE());  
 END  
 END    
  
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=6)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Sec_Fri WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=6;      
 END    
ELSE  
 BEGIN  
 IF @Sec_Fri<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Sec_Fri,6,1,GETDATE());  
 END  
 END   
   
IF EXISTS(SELECT SettingID FROM  ValidationReportSettings WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=7)  
 BEGIN  
  UPDATE ValidationReportSettings SET UserID = @Sec_Sat WHERE ReportID=@ReportID AND UserType=1 AND DayNumber=7;    
 END    
ELSE  
 BEGIN  
 IF @Sec_Sat<>-1  
 BEGIN  
  INSERT INTO ValidationReportSettings(ReportID,UserID,DayNumber,UserType,InsertDate)   
  VALUES(@ReportID,@Sec_Sat,7,1,GETDATE());   
 END  
 END    

   
 
GO
GRANT EXECUTE ON dbo.USP_HTP_UPDATE_ValidationUser TO dbr_webuser
GO
   
 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_TicketsStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_TicketsStatus]
GO

CREATE procedure dbo.USP_HTS_Get_TicketsStatus

as

SELECT	distinct
		'<a href=../clientinfo/violationfeeold.aspx?search=0&casenumber=' + convert(varchar(15), t.ticketid_pk ) + '>'+ v.refcasenumber + '</a>' as [CASE NUMBER],
		t.lastname + ', ' + t.firstname as [CLIENT NAME],
		dbo.formatdateandtimeintoshortdateandtime(v.CourtDateMain) as [COURT DATE], 
		v.CourtNumbermain as [COURT NUMBER], 
		c.ShortName as COURT, 
		s.Description as [CASE STATUS], 
		d.Description AS [PRIMARY STATUS]
FROM    dbo.tblTickets t
INNER JOIN
		dbo.tblTicketsViolations v
ON		t.TicketID_PK = v.TicketID_PK 
INNER JOIN
		dbo.tblCourts c
ON		v.CourtID = c.Courtid 
INNER JOIN
		dbo.tblCourtViolationStatus s
ON		v.CourtViolationStatusIDmain = s.CourtViolationStatusID 
INNER JOIN
		dbo.tblDateType d
ON		s.CategoryID = d.TypeID
where	t.activeflag = 1
and		d.typeid = 51



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


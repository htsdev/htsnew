  
/******************************************  
Altered By:  Syed Muhammad Ozair  
Altered Date: 01/09/2009  
Task:   5384  
  
Business Logic:  
 This procedure is used to return payment type summary by date range and firm id.  
   
  
Input Parameters:  
 @RecDate:  
 @RecTo:  
 @firmId: 0 means all firms  
  
Output Columns:  
 OrderID:  
 PaymentType_PK:  
 description:  
 TotalCount:  
 Amount:  
  
********************************************/  
  
ALTER procedure [dbo].[usp_Get_All_PaymentSumByRecDateRange]             
  
@RecDate DateTime,            
@RecTo DateTime,    
@firmId int = null ,
@BranchID INT=1 -- Sabir Khan 10920 05/27/2013 Branch Id added.  
  
AS           
          
DECLARE @sql varchar(max)    
    
 SELECT PaymentType, COUNT(ticketid) AS TotalCount, SUM(ChargeAmount) AS Amount            
 Into #temp1            
 FROM         dbo.tblTicketsPayment            
 WHERE     (PaymentVoid <> 1)            
 --ozair 5384 01/09/2009 modify date check  
 AND datediff(day, @RecDate, recdate)>=0 and datediff(day, @RecTo,recdate)<=0  
 and paymenttype not in (99,100)    
    
    and ticketId in (select ticketId_pk from tblTicketsviolations where coveringFirmId = coalesce(@firmid,coveringFirmId)) 
     AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))  -- Sabir Khan 10920 05/27/2013 Branch Id check added.
 GROUP BY  PaymentType    
          
Select PaymentType_PK, Description            
Into #temp2             
From tblPaymenttype             
Where Left(Description,1) <>  '-'            
            
ALTER TABLE #temp2  ADD OrderID int NOT NULL DEFAULT 0            
            
Select * Into #temp5 From #temp2            
update #temp5 set orderid = -2 where paymenttype_pk = 1          
Update #temp5 Set OrderID= 1 Where PaymentType_PK = 5             
Update #temp5 Set OrderID= 3 Where PaymentType_PK > 5             
Update #temp5 Set OrderID= 5 Where Description = 'Refund'             
            
            
SELECT  CardType, Count(ticketid) As TotalCount, Sum(ChargeAmount)As Amount            
Into #temp3             
FROM  tblTicketsPayment            
Where              
PaymentVoid <> 1             
AND CardType <> 0            
--ozair 5384 01/09/2009 modify date check  
 AND datediff(day, @RecDate, recdate)>=0 and datediff(day, @RecTo,recdate)<=0  
--ozair 5384 01/09/2009 included covering firm check  
and ticketId in (select ticketId_pk from tblTicketsviolations where coveringFirmId = coalesce(@firmid,coveringFirmId)) 
AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))   -- Sabir Khan 10920 05/27/2013 Branch Id check added.   
Group by CardType            
          
           
Select CCTypeID, CCType            
Into #temp4             
From tblCCType             
            
ALTER TABLE #temp4 ADD OrderID int NOT NULL DEFAULT 2            
            
Select * Into #temp6 from #temp4            
            
-- ************************************************************            
Select OrderID,           
 PaymentType_PK,           
 case when a.paymenttype = 2 then 'Reg. Check' else Description end as description,           
 isnull(TotalCount,0) AS TotalCount,           
 isnull(Amount,0)AS Amount             
from #temp1 a right outer join #temp5 b        
on a.paymenttype = b.paymenttype_pk            
        
        
UNION            
          
          
-- FOR CREDIT CARD DETAIL SECTION...            
SELECT OrderID,           
 CCTypeID AS PaymentType_PK ,           
 CCType AS Description,          
 isnull(TotalCount,0) AS TotalCount,           
 isnull(Amount,0)AS Amount             
from #temp3 c right outer join         
 #temp6  d        
on c.cardtype = d.cctypeid        
          
union           
          
          
-- FOR SUM OF CHEQUES            
select -1, 200, 'Checks',   count (ticketid) , isnull(sum(isnull(chargeamount,0)) ,0)          
FROM         dbo.tblTicketsPayment            
WHERE     (PaymentVoid <> 1)            
--ozair 5384 01/09/2009 modify date check  
AND datediff(day, @RecDate, recdate)>=0 and datediff(day, @RecTo,recdate)<=0         
and paymenttype in (2,4,3)   --waqas 5755 04/07/2009 Add prepaid  legal payment processing type.   
  
 and ticketId in (select ticketId_pk from tblTicketsviolations where coveringFirmId = coalesce(@firmid,coveringFirmId))    
 AND Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))   -- Sabir Khan 10920 05/27/2013 Branch Id check added.     
drop table #temp1    
drop table #temp2    
drop table #temp3    drop table #temp4    
drop table #temp5    
drop table #temp6    
  
  
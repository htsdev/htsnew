SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_NoMailFlag_ChkByDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_NoMailFlag_ChkByDetails]
GO


CREATE PROCEDURE [dbo].[usp_hts_NoMailFlag_ChkByDetails]      
@FirstName varchar(20),        
@LastName varchar(20),        
@Address varchar(50),      
@Zip varchar(12),    
@City varchar(50),  
@stateid_fk int  
as      
      
select count (*) as RecCount from tblticketsarchive      
      
where      
      
tblticketsarchive.firstname = @FirstName         
     AND        
     tblticketsarchive.lastname = @LastName      
     AND        
     tblticketsarchive.address1 = @Address      
   AND      
  tblticketsarchive.ZipCode Like(@Zip+'%')      
  AND    
  tblticketsarchive.City = @City    
  AND  
 tblticketsarchive.StateID_FK = @stateid_fk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


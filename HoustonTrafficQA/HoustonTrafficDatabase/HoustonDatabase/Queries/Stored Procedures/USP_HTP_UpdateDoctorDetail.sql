﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to update doctor detial.
* List of Parameter : @id
*					: @LastName
*					: @LastName 
					: @FirstName
					: @Address 
					: @City 
					: @State
					: @Zip 

* Column Return :     
******/
-- USP_HTP_UpdateDoctorDetail 
CREATE PROC  dbo.USP_HTP_UpdateDoctorDetail 
@id INT,
@LastName VARCHAR(100),
@FirstName VARCHAR(100),
@Address VARCHAR(200),
@City VARCHAR(50),
@State INT,
@Zip VARCHAR(10)
as

UPDATE Doctor
SET
	LastName = @LastName,
	FirstName = @FirstName,
	[Address] = @Address,
	City = @City,
	[State] = @State,
	Zip = @Zip
	
WHERE Id=@id
GO

GRANT EXECUTE ON dbo.USP_HTP_UpdateDoctorDetail TO dbr_webuser
GO
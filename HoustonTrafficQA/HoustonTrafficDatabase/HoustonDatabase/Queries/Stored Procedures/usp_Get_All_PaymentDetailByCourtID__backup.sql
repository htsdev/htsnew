SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentDetailByCourtID__backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_PaymentDetailByCourtID__backup]
GO


CREATE PROCEDURE dbo.usp_Get_All_PaymentDetailByCourtID

@RecDate DateTime,
@CourtID varchar(10)

AS


IF (@CourtID='0' )
	BEGIN
	
	SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
	                      dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
             			         CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
		END AS CustomerName,
		dbo.tblCourts.ShortName AS Court,
		dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag, 
		dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
		FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
		                      dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
		                      dbo.tblCourts RIGHT OUTER JOIN
		                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
		                      dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
		                      dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 		
				( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDate+1)   And  PaymentVoid <> 1 				
				--AND  dbo.tblTicketsPayment.PaymentType IN (6)  
				--AND dbo.tblTicketsPayment.CardType = Right(@PaymentType,1)	

	END	
--*******
ELSE
	BEGIN
		SELECT     CONVERT(Varchar(20), dbo.tblTicketsPayment.ChargeAmount, 101) AS ChargeAmount, dbo.tblTicketsPayment.EmployeeID, 
		                      dbo.tblTicketsPayment.PaymentType, CONVERT(Varchar(12), dbo.tblTicketsPayment.RecDate, 101) AS RecDate, dbo.tblUsers.Lastname, 
             			         CASE 
		WHEN (LEN(dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname)>19) THEN  dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname + '...'
		ELSE dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname
	         	END AS CustomerName,
			dbo.tblCourts.ShortName AS Court,
	             		         dbo.tblPaymenttype.Description AS CCType,  dbo.tblTicketsPayment.TicketID, REPLACE(dbo.tblTickets.BondFlag, 1, 'B') AS BondFlag, 
				dbo.tblTicketsPayment.CardType, CONVERT(Varchar(12), MailDate, 101) AS ListDate,
		dbo.Fn_FormateTime(dbo.tblTicketsPayment.RecDate) As RecTime
			FROM         dbo.tblPaymenttype RIGHT OUTER JOIN
		 dbo.tblTicketsPayment ON dbo.tblPaymenttype.Paymenttype_PK = dbo.tblTicketsPayment.PaymentType LEFT OUTER JOIN
		                      dbo.tblCourts RIGHT OUTER JOIN
		                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc ON 
		                      dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK LEFT OUTER JOIN
		                      dbo.tblUsers ON dbo.tblTicketsPayment.EmployeeID = dbo.tblUsers.EmployeeID
		Where 		
				( tblTicketsPayment.RecDate  Between  @RecDate  AND  @RecDate+1)   And  PaymentVoid <> 1 
				AND dbo.tblCourts.Courtid = @CourtID						
				--AND  dbo.tblTicketsPayment.PaymentType IN (6)  
				--And  dbo.tblTicketsPayment.EmployeeID= @EmployeeID
				--AND dbo.tblTicketsPayment.CardType = Right(@PaymentType,1)			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


    
ALTER procedure [dbo].[USP_PS_Create_ReceiptFile_For_QuoteClient]    
(    
   
@CustomerID int,    
@TicketID int,  
@rowid int    
    
)    
as    
  

declare @empid int

-- SYSTEM EMPLOYEE ID...
set @empid = 3992
  
----- Set Payment Type To Internet ----------------  
  
Update tblticketspayment set PaymentType = 7   
where   
InvoiceNumber_Pk = ( Select invnumber from tblticketscybercash where rowid = @rowid)  
  
---------------------------------------------------  
  
  
  
DECLARE @BookID int, @picID int , @cmd varchar(255)  , @notes varchar(500)                    
          
 begin transaction          
          
   insert into tblScanBook (EmployeeID,TicketID,DocTypeID,updatedatetime,DocCount,ResetDesc)               
   values(@empid,@ticketid,11,getdate(),1,'INTERNET CLIENT SIGNUP RECEIPT') -- insert into book                        
                 
   select @BookID =  Scope_Identity() --Get Book Id of the latest inserted                        
                 
   insert into tblScanPIC (ScanBookID,updatedatetime,DocExtension)               
   values( @BookID,getdate(),'PDF') --insert new image into database                        
                 
   select @picID =  Scope_Identity() --Get Book Id of the latest inserted                        
   --Docpic = @image                  
 commit transaction                    
                 
   set @notes = '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+ convert(varchar(10), @BookID)+'&RecType=1&DocNum=0&DocExt=PDF'');void('''');"''>SCANNED DOCUMENT - OTHER [INTERNET CLIENT SIGNUP RECEIPT]</a>'                     
   insert into tblTicketsNotes (TicketID,Subject,EmployeeID)               
   values (@TicketID, @notes ,@empid)                   
                 
--   set @cmd = 'copy \\srv-dev\docstorage\publicsite\'+ Convert(varchar(10), @customerid) + '.PDF \\srv-dev\docstorage\scandoc\images\' +  convert(varchar(10), @bookid) +'-'+ convert(varchar(10), @picid) + '.PDF'                     
--   exec master.dbo.xp_cmdshell @cmd , no_output              

Select convert(varchar(10), @bookid) +'-'+ convert(varchar(10), @picid) + '.PDF' 







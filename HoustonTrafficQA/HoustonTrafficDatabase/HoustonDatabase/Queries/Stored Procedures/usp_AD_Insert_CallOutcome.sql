﻿ /****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The procedure is used to insert Call outcome

List of Parameters:				
	@EventTypeID_FK:
	@CallDateTime:
	@TicketID_FK:
	@IsBond:
	@ContactType_FK:
	@ContactNumber:
	@RecordingPath:
	@TicketsViolationIDs:
	@CallID:
	
*******/
ALTER Procedure dbo.usp_AD_Insert_CallOutcome

@EventTypeID_FK tinyint,
@CallDateTime datetime,
@TicketID_FK int,
@IsBond bit,
@ContactType_FK tinyint,
@ContactNumber varchar(20),
@RecordingPath varchar(200),
@TicketsViolationIDs varchar(max),
@CallID int output

as

set @CallID=0

INSERT INTO AutoDialerCallOutcome
           (EventTypeID_FK
           ,CallDateTime
           ,TicketID_FK
           ,IsBond
           ,ContactType_FK
           ,ContactNumber
           ,ResponseID_FK
           ,RecordingPath
           )
     VALUES
           (@EventTypeID_FK
           ,@CallDateTime
           ,@TicketID_FK
           ,@IsBond
           ,@ContactType_FK
           ,@ContactNumber
           ,6
           ,@RecordingPath
           )

select @CallID=scope_identity()

if @CallID>0
begin
	insert into autodialercalloutcomedetail(callid_fk,ticketsviolationid_fk,courtid,courtdatetime,courtno,courtviolationstatusid_fk)
	--Ozair 6570 09/16/2009 included isnull check for court number
	select @CallID,ticketsviolationid,courtid,courtdatemain,isnull(courtnumbermain,'0'),courtviolationstatusidmain
	from tblticketsviolations
	where ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs))
end

return
go
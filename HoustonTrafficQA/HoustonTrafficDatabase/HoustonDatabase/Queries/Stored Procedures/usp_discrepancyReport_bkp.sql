 ALTER  PROCEDURE [dbo].[usp_discrepancyReport_bkp]  
  
@startdate as datetime,  
@enddate as datetime,  
@showall as int=0  
  
AS  

set @enddate = @enddate + '23:59:59'

IF @showall=1  
 BEGIN  
	select  distinct
	 TT.TicketID_PK, refcasenumber as refcasenumberseq,  
	 convert(varchar(11),TTV.courtdate,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdate),13,18),'12:00AM','0:00AM')    AS courtdate,         --upload--auto  
 	 convert (varchar(10),TTV.courtnumber)  as courtnumber, 
	 convert(varchar(11),TTV.courtdatescan,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdatescan),13,18),'12:00AM','0:00AM')    AS courtdatescan, --scan   
 	 TTV.courtnumberscan,          
  	 replace(TC.shortname,'--Choose','N/A')as shortname  ,
                 ticketsviolationid,   
	replace(dt1.description,'---Choose----','N/A') as SDesc,	
	replace(dt2.description,'---Choose----','N/A') as ADesc,
	tt.activeflag 

FROM    

tbltickets tt, tblticketsviolations ttv, tblCourtViolationStatus,
 tblCourtViolationStatus tblCourtViolationStatus_1, 
tbldatetype dt1,tblcourts tc,tbldatetype dt2
where tt.ticketid_pk = ttv.ticketid_pk
and ttv.courtviolationstatusid = tblCourtViolationStatus_1.courtviolationstatusid
and tblCourtViolationStatus_1.categoryid = dt1.typeid
and ttv.courtviolationstatusidscan = tblCourtViolationStatus.courtviolationstatusid
and tblCourtViolationStatus.categoryid = dt2.typeid
and tt.currentcourtloc = tc.courtid


and  
	 dt2.typeid!=50      --dont need records with 'disposed'status for auto    
and 	( datediff(day,isnull(TTV.courtdatescan,'01/01/1900'),isnull(TTV.courtdate,'01/01/1900')) <> 0
	or  
	datediff(hour,isnull(TTV.courtdatescan,'01/01/1900'),isnull(TTV.courtdate,'01/01/1900')) <> 0
	or
	--dbo.FormatAlphaCourtNumbers(TTV.courtnumberscan) <> dbo.FormatAlphaCourtNumbers(TTV.courtnumber)
	ttv.courtnumberscan <> ttv.courtnumber
        or   
	dt1.typeid <> dt2.typeid
	) 
	and dt1.typeid in (2,3,4,5)    
	and activeflag = 1 
order by TT.ticketid_pk      

 END  
ELSE  

 BEGIN  
 


	select  distinct
	 TT.TicketID_PK,  
 	 convert(varchar(11),TTV.courtdate,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdate),13,18),'12:00AM','0:00AM')    AS courtdate,         --upload--auto  
 	 convert (varchar(10),TTV.courtnumber)  as courtnumber, 
	 convert(varchar(11),TTV.courtdatescan,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdatescan),13,18),'12:00AM','0:00AM')    AS courtdatescan, --scan   
 	 TTV.courtnumberscan,          
         TC.shortname ,
         ticketsviolationid,   
	replace(dt1.description,'---Choose----','N/A') as SDesc,	
	replace(dt2.description,'---Choose----','N/A') as ADesc,
	tt.activeflag

FROM         

tbltickets tt, tblticketsviolations ttv, tblCourtViolationStatus,
 tblCourtViolationStatus tblCourtViolationStatus_1, 
tbldatetype dt1,tblcourts tc,tbldatetype dt2
where tt.ticketid_pk = ttv.ticketid_pk
and ttv.courtviolationstatusid = tblCourtViolationStatus_1.courtviolationstatusid
and tblCourtViolationStatus_1.categoryid = dt1.typeid
and ttv.courtviolationstatusidscan = tblCourtViolationStatus.courtviolationstatusid
and tblCourtViolationStatus.categoryid = dt2.typeid
and tt.currentcourtloc = tc.courtid

and    
	dt2.typeid!=50      --dont need records with 'disposed'status for auto    
	and 	( datediff(day,isnull(TTV.courtdatescan,'01/01/1900'),isnull(TTV.courtdate,'01/01/1900')) <> 0
	or  

	datediff(hour,isnull(TTV.courtdatescan,'01/01/1900'),isnull(TTV.courtdate,'01/01/1900')) <> 0
	or
	dbo.FormatAlphaCourtNumbers(TTV.courtnumberscan) <> dbo.FormatAlphaCourtNumbers(TTV.courtnumber)
        or   
	dt1.typeid <> dt2.typeid
	) 
	and dt1.typeid in (2,3,4,5)    
	and activeflag = 1 	
-- and TTV.courtdate between @startdate   and  @enddate  
and datediff(day, ttv.courtdate, @startdate) >= 0 
and datediff(day, ttv.courtdate, @enddate) <= 0 

order by TT.ticketid_pk      
 END
go
--Noufil 4632 09/17/2008
/*****
	Business Logic : This Procedure return casetype ids for a given Service Ticket Category
	Parameter : @servicecategoryid (id of Service Ticket Category)
	Column Return : casetypeid (case type for that @servicecategoryid)

*****/
create procedure [dbo].[USP_HTP_Get_CaseType_by_sevicecategory] 
@servicecategoryid int
as

select distinct casetypeid from CaseTypeServiceCategory where servicecategoryid = @servicecategoryid

go
grant exec on [dbo].[USP_HTP_Get_CaseType_by_sevicecategory] to dbr_webuser
go


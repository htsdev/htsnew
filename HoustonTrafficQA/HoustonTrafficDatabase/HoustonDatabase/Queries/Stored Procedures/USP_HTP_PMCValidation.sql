﻿ USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_PMCValidation]    Script Date: 09/23/2010 08:10:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Muhammad Muneer 8276 09/22/2010 Added the new stored procedure for pmc validation         

Business Logic:

Cause Number length must be exactly 7 characters long and it should start with 1.

If Ticket number start with 6 or 7 then length must be exactly 6 characters long

If Ticket number start with E then length must be exactly 8 characters long

If Cause Number is valid, then Ticket Number should be valid or it will be blank

        
*/            

-- USP_HTP_PMCValidation 205985           
ALTER PROCEDURE [dbo].[USP_HTP_PMCValidation]
	@ticketid INT
AS
DECLARE @IsInvalidPMCTicket       INT
	DECLARE @IsInvalidPMCCausenumber  INT
	
	SELECT @IsInvalidPMCTicket = COUNT(TicketsViolationID)
	FROM   tblTicketsViolations
	WHERE  TicketID_PK = @ticketid
	       AND ISNULL(CourtID, 0) = 3004
	       AND (
	               (
	                   (LEN(ISNULL(refcasenumber, '')) = 0 AND (LEN(ISNULL(casenumassignedbycourt, 0)) <> 7) OR (SUBSTRING(UPPER(casenumassignedbycourt), 1, 1) <> '1'))
	                   OR (
	                          (
	                              (SUBSTRING(RefCaseNumber, 1, 1) = '6')
	                              OR (SUBSTRING(RefCaseNumber, 1, 1) = '7')
	                          )
	                          AND LEN(refcasenumber) <> 6
	                      )
	                   OR (
	                          (SUBSTRING(UPPER(RefCaseNumber), 1, 1) = 'E')
	                          AND LEN(refcasenumber) <> 8
	                      )
	                   OR (
	                          (SUBSTRING(RefCaseNumber, 1, 1) <> '6')
	                          AND (SUBSTRING(RefCaseNumber, 1, 1) <> '7')
	                          AND (SUBSTRING(UPPER(RefCaseNumber), 1, 1) <> 'E')
	                          AND  LEN(ISNULL(refcasenumber, '')) <> 0	                          
	                      )
	               )
	             
	           )
	       AND ISNULL(CourtViolationStatusIDmain, 0) NOT IN (80, 236)
	
	SELECT @IsInvalidPMCCausenumber = COUNT(TicketsViolationID)
	FROM   tblTicketsViolations
	WHERE  TicketID_PK = @ticketid
	       AND ISNULL(CourtID, 0) = 3004
	       AND ISNULL(CourtViolationStatusIDmain, 0) NOT IN (80, 236)
	       AND (
	               (	               	 
	               	 ((LEN(ISNULL(casenumassignedbycourt, 0)) <> 7) OR (SUBSTRING(casenumassignedbycourt, 1, 1) <> '1'))
	                   AND (
	                           LEN(ISNULL(refcasenumber, '')) = 0
	                           OR (
	                                  (
	                                      (SUBSTRING(RefCaseNumber, 1, 1) = '6')
	                                      OR (SUBSTRING(RefCaseNumber, 1, 1) = '7')
	                                  )
	                                  AND LEN(refcasenumber) <> 6
	                              )
	                           OR 
	                           (
	                           	(SUBSTRING(UPPER(RefCaseNumber), 1, 1) = 'E')
	                            AND (LEN(refcasenumber) <> 8)
	                           )
	                           OR (
	                                  (SUBSTRING(RefCaseNumber, 1, 1) <> '6')
	                                  AND (SUBSTRING(RefCaseNumber, 1, 1) <> '7')
	                                  AND (SUBSTRING(UPPER(RefCaseNumber), 1, 1) <> 'E')
	                              )
	                       )
	               )
	               OR (
	                      SUBSTRING(ISNULL(casenumassignedbycourt, ''), 1, 1) = '1'
	                      AND LEN(ISNULL(casenumassignedbycourt, '')) = 7
	                      AND (
	                              (
	                                  (
	                                      (SUBSTRING(ISNULL(RefCaseNumber, ''), 1, 1) = '6')
	                                      OR (SUBSTRING(ISNULL(RefCaseNumber, ''), 1, 1) = '7')
	                                  )
	                                  AND (LEN(ISNULL(RefCaseNumber, '')) <> 6)
	                              )
	                              OR (
	                                     (
	                                         (SUBSTRING(ISNULL(UPPER(RefCaseNumber), ''), 1, 1) = 'E')
	                                         AND (LEN(ISNULL(RefCaseNumber, '')) <> 8)
	                                     )
	                                 )
	                              OR (LEN(ISNULL(refcasenumber, '')) = 0)
	                              OR (
	                                     SUBSTRING(ISNULL(RefCaseNumber, ''), 1, 1) 
	                                     <> 'E'
	                                     AND SUBSTRING(ISNULL(RefCaseNumber, ''), 1, 1) 
	                                         <> '6'
	                                     AND SUBSTRING(ISNULL(RefCaseNumber, ''), 1, 1) 
	                                         <> '7'
	                                 )
	                          )
	                  )
	           )
	
	
	
	
	SELECT @IsInvalidPMCTicket AS IsInvalidPMCTicket,
	       @IsInvalidPMCCausenumber AS IsInvalidPMCCausenumber	

GO
GRANT EXECUTE ON [dbo].[USP_HTP_PMCValidation] TO dbr_webuser 
GO
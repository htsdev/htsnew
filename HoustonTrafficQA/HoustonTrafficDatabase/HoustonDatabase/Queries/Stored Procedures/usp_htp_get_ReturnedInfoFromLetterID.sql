/*
* Task : Waqas 5932 06/22/2009
* This procedure is used to get information of the letter that has been returned 
* Columns : 
* Letter ID, 
* Ticket number (Comma sperated) , 
* Record id, 
* First Name, 
* Last Name 
*/
ALTER PROCEDURE usp_htp_get_ReturnedInfoFromLetterID
@letterid int
AS
BEGIN
	
SET NOCOUNT ON;
	
SELECT DISTINCT 
ln.NoteId AS LetterID, ( SELECT dbo.fn_htp_get_TicketNumberCommasForRecID (tta.RecordID) ) AS TicketNumber, 
ln.RecordID AS RecordID, tta.FirstName, tta.LastName
  FROM tblletternotes ln WITH(NOLOCK) INNER JOIN tblTicketsArchive tta WITH(NOLOCK)
  ON ln.RecordID =tta.RecordID 
  INNER JOIN tblticketsviolationsarchive ttva WITH(NOLOCK)
  ON tta.RecordID=ttva.RecordID    
WHERE noteid = @letterid

end



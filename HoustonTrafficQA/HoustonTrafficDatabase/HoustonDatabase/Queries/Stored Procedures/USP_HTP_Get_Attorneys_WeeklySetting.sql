﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure is used to get attorney schedule by given given date.
			
List of Parameters:	
			@StartDate docket Start Date
			@EndDate	docket end Date
******/

--USP_HTP_Get_Attorneys_WeeklySetting '12-23-2009','12-28-2009'  
  
Create PROCEDURE dbo.USP_HTP_Get_Attorneys_WeeklySetting  
 @StartDate DATETIME='01-01-1900',  
 @EndDate DATETIME='01-01-1900'  
AS  
BEGIN  
    DECLARE @Date DATETIME  
      
    DECLARE @count INT  
    SET @count = DATEDIFF(DAY ,@StartDate ,@EndDate) + 1  
      
    CREATE TABLE #ShowSettingData  
    (  
     AttorneyScheduleID  INT  
       ,DocketDate          DATETIME  
       ,AttorneyID          INT  
       ,ContactNumber       VARCHAR(14)  
       ,EmpIDContact  VARCHAR(20)  
    )  
      
    WHILE @count>0  
    BEGIN  
        --SELECT @AttorneyID=  
        SET @Date = DATEADD(DAY ,@count-1 ,@StartDate)   
          
        INSERT INTO #ShowSettingData  
          (  
            DocketDate  
          )  
        VALUES  
          (  
            @Date  
          )  
          
        UPDATE #ShowSettingData  
        SET    AttorneyScheduleID = ISNULL(  
                   (  
                       SELECT TOP 1 AttorneyScheduleID  
                       FROM   AttorneysSchedule  
                       WHERE CONVERT(VARCHAR,DocketDate,101) = CONVERT(VARCHAR,@Date,101)  
                   )  
                  ,0  
               )  
              ,AttorneyID = ISNULL(  
                   (  
                       SELECT TOP 1 AttorneyID  
                       FROM   AttorneysSchedule  
                       WHERE CONVERT(VARCHAR,DocketDate,101) = CONVERT(VARCHAR,@Date,101)  
                   )  
                  ,'-1'  
               )  
              ,ContactNumber = ISNULL(  
                   (  
                       SELECT TOP 1 ContactNumber  
                       FROM   AttorneysSchedule  
                       WHERE CONVERT(VARCHAR,DocketDate,101) = CONVERT(VARCHAR,@Date,101)  
                   )  
                  ,''  
              )  
               ,EmpIDContact =   
                   (  
                       SELECT TOP 1 CONVERT (VARCHAR,AttorneyID) + '-' + ISNULL(tu.ContactNumber,'')
                       FROM   AttorneysSchedule  
                       INNER JOIN tblUsers tu ON AttorneysSchedule.AttorneyID=tu.EmployeeID   
                       WHERE CONVERT(VARCHAR,DocketDate,101) = CONVERT(VARCHAR,@Date,101)  
                   )  
                    
                 
        WHERE  DocketDate = @Date  
        
          
        SET @count = @count-1  
    END  
      
    SELECT AttorneyScheduleID   
       ,CONVERT(VARCHAR,DocketDate,101)   AS DocketDate          
       ,CONVERT(DATETIME, DocketDate)   AS DocketDateSort 
       ,AttorneyID           
       ,ContactNumber   
       ,ISNULL(EmpIDContact ,'-1') AS EmpIDContact    
    FROM   #ShowSettingData  
    ORDER BY DocketDateSort
      
    DROP TABLE #ShowSettingData  
END  
  
  GO 
   
   GRANT EXECUTE on dbo.USP_HTP_Get_Attorneys_WeeklySetting   TO dbr_webuser
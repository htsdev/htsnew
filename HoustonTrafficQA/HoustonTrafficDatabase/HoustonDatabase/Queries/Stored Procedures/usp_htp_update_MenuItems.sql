/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 12/15/2009 2:19:24 PM
 ************************************************************/

/*
* Created By  : Waqas Javed
* Task ID	  : 7077
* Created Date : 12/14/2009
* Business logic : This procedure is used to update menu items for menu configuration
* 
* Parameter List
* ID				It represents the Id of the menu item
* @LevelID			It represents the Level of the Menu
* @Title			It represents the title of menu item
* @URL				It represents the url of the menu item
* @IsActive			It represents that menu item is active or not.
* @IsSelected		It represents that menu item is selected or not.
* @IsAdminLog		It represents that admin log is allowed or not
* @OrderedIDs		It represents the comma seperated IDs to be ordered.
* @Category			It represents report category, report category can be an Alert=1 or Report=2.
*/

  
alter PROCEDURE dbo.usp_htp_update_MenuItems
	@ID
AS
	INT = NULL, 
	@LevelID AS INT = NULL, 
	@Title AS VARCHAR(200) = NULL, 
	@URL AS VARCHAR(2000) = NULL, 
	@IsActive AS BIT = NULL, 
	@IsSelected AS INT = NULL, 
	@IsAdminLog AS BIT = NULL, 
	@OrderedIDs AS VARCHAR(MAX) = NULL,
	@IsSubMenuHidden AS BIT = TRUE,
	@Category AS INT =NULL	-- SAEED 7791 05/25/2010, represents report category, two available category for reports are: Alerts=1, Reports=2.
	AS  
BEGIN
    DECLARE @MenuID INT,
            @Pos INT,
            @Count INT  
    
    IF @LevelID = 1
    BEGIN
        --Updating Menu Details.  
        UPDATE tbl_TAB_MENU
        SET    TITLE = @Title,
               [active] = @IsActive,
               IsSubMenuHidden = @IsSubMenuHidden
               
        WHERE  ID = @ID 
        
        --Updating New Ordering          
        
        SET @OrderedIDs = LTRIM(RTRIM(@OrderedIDs)) + ','  
        SET @Pos = CHARINDEX(',', @OrderedIDs, 1)  
        SET @Count = 0  
        IF REPLACE(@OrderedIDs, ',', '') <> ''
        BEGIN
            WHILE @Pos > 0
            BEGIN
                SET @MenuID = CONVERT(INT, ISNULL(LTRIM(RTRIM(LEFT(@OrderedIDs, @Pos - 1))), 0))  
                IF @MenuID <> 0
                BEGIN
                    SET @Count = @Count + 1  
                    UPDATE tbl_TAB_MENU
                    SET    ORDERING = @Count
                    WHERE  ID = @MenuID
                END  
                
                SET @OrderedIDs = RIGHT(@OrderedIDs, LEN(@OrderedIDs) - @Pos)  
                SET @Pos = CHARINDEX(',', @OrderedIDs, 1)
            END
        END
    END
    ELSE
    BEGIN
        IF @IsSelected = 1
        BEGIN
            UPDATE tbl_TAB_SUBMENU
            SET    SELECTED = 0
            WHERE  ID IN (SELECT ID
                          FROM   tbl_TAB_SUBMENU ttm
                          WHERE  ttm.MENUID = (
                                     SELECT TOP 1 MENUID
                                     FROM   tbl_TAB_SUBMENU
                                     WHERE  ID = @ID
                                 ))
            
            UPDATE tbl_TAB_SUBMENU
            SET    SELECTED = 1
            WHERE  ID = @ID
        END
        
        
        UPDATE tbl_TAB_SUBMENU
        SET    TITLE = @Title,
               SELECTED = @IsSelected,
               IsAdminLog = @IsAdminLog,
               [URL] = @URL,
               [active] = @IsActive,
               Category	=@Category   -- SAEED 7791 05/25/2010, updates report category, two available category for reports are: Alerts=1, Reports=2.	
        WHERE  ID = @ID 
        
        
        --Updating New Ordering   
        SET @OrderedIDs = LTRIM(RTRIM(@OrderedIDs)) + ','  
        SET @Pos = CHARINDEX(',', @OrderedIDs, 1)  
        SET @Count = 0  
        IF REPLACE(@OrderedIDs, ',', '') <> ''
        BEGIN
            WHILE @Pos > 0
            BEGIN
                SET @MenuID = CONVERT(INT, ISNULL(LTRIM(RTRIM(LEFT(@OrderedIDs, @Pos - 1))), 0))  
                IF @MenuID <> 0
                BEGIN
                    SET @Count = @Count + 1  
                    UPDATE tbl_TAB_SUBMENU
                    SET    ORDERING = @Count
                    WHERE  ID = @MenuID
                END  
                
                SET @OrderedIDs = RIGHT(@OrderedIDs, LEN(@OrderedIDs) - @Pos)  
                SET @Pos = CHARINDEX(',', @OrderedIDs, 1)
            END
        END  
        
        IF NOT EXISTS (
               SELECT *
               FROM   tbl_TAB_SUBMENU tts
               WHERE  MENUID = (
                          SELECT TOP 1 MENUID
                          FROM   tbl_TAB_SUBMENU
                          WHERE  ID = @ID
                      )
                      AND tts.[active] = 1
                      AND tts.SELECTED = 1
           )
        BEGIN
            UPDATE tbl_TAB_SUBMENU
            SET    SELECTED = 1
            WHERE  id = (
                       SELECT TOP 1 id
                       FROM   tbl_TAB_SUBMENU tts
                       WHERE  tts.MENUID = (
                                  SELECT TOP 1 MENUID
                                  FROM   tbl_TAB_SUBMENU
                                  WHERE  ID = @ID
                              )
                              AND tts.[active] = 1
                       ORDER BY
                              tts.ORDERING
                   )
        END
    END
END

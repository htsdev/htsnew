SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_DEFAULT_PLANID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_DEFAULT_PLANID]
GO


CREATE procedure USP_HTS_GET_DEFAULT_PLANID     
as    
select Top 1  isnull (  PlanID  , 90 ) from  tblpriceplans     
where courtid = 3001    
    
and isactiveplan = 1    
    
    
    
    
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



USE TrafficTickets

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Sabir Khan
-- Create date:		02/18/2011
-- Business Logic:	GETs PMC Pretrial Cases Having No Fax Confirmation
-- Sabir Khan 8931 02/18/2011 Pasadena Pretrial report 
-- =============================================
-- Babar Ahmad 07/14/2011 Added business logic

ALTER procedure [dbo].[USP_HTP_GETPMCPretrialCasesHavingNoFaxConfirm]
as  

BEGIN
SELECT DISTINCT t.ticketid_pk, t.FirstName,t.lastname,ISNULL(tv.RefCaseNumber,tv.casenumassignedbycourt) AS [CaseNumber], tvs.[Description]
,Case ISNULL(tv.UnderlyingBondFlag,0) when 0 then 'No' else 'Yes' end AS Bond, Convert(varchar,tv.courtdatemain,101) as [CourtDate]
into #temp
FROM tbltickets t INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
INNER JOIN tblCourtViolationStatus tvs ON tvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain
--Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
inner join faxLetter fl on fl.ticketid=t.ticketid_pk 
WHERE tv.CourtViolationStatusIDmain = 101 AND ISNULL(t.Activeflag,0) = 1 AND tv.CourtID = 3004
AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
and fl.status not in ('SUCCESS', 'Cancelled')

SELECT DISTINCT t.ticketid_pk, t.FirstName,t.lastname,ISNULL(tv.RefCaseNumber,tv.casenumassignedbycourt) AS [CaseNumber], tvs.[Description]
,Case ISNULL(tv.UnderlyingBondFlag,0) when 0 then 'No' else 'Yes' end AS Bond, Convert(varchar,tv.courtdatemain,101) as [CourtDate]
into #temp1
FROM tbltickets t INNER JOIN tblticketsviolations tv ON tv.TicketID_PK = t.TicketID_PK
INNER JOIN tblCourtViolationStatus tvs ON tvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain
WHERE tv.CourtViolationStatusIDmain = 101 AND ISNULL(t.Activeflag,0) = 1 AND tv.CourtID = 3004
AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'

delete from #temp1 where ticketid_pk in (select ticketid from faxLetter)

select FirstName AS [First Name],lastname AS [Last Name], CaseNumber AS [Case Number], [Description] AS [Case Status]
,Bond, CourtDate as [Court Date] from #temp
UNION ALL
select FirstName AS [First Name],lastname AS [Last Name], CaseNumber AS [Case Number], [Description] AS [Case Status]
,Bond, CourtDate as [Court Date] from #temp1

drop table #temp
drop table #temp1
END


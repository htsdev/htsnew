SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_QCB_Users]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_QCB_Users]
GO


create procedure dbo.USP_HTS_Get_QCB_Users

as


select	employeeid, 
	username 
from 	tblusers
where	employeeid in (
		select distinct employeeid from tbltickets
		where	datediff(day, recdate, getdate() ) between 0 and 90
		)
order by
	username




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


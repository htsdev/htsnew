SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_Get_BusinessLogicByMenuID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_Get_BusinessLogicByMenuID]
GO


Create procedure [dbo].[Usp_Get_BusinessLogicByMenuID]

@MenuID int

as

select businesslogic from tbl_report
where Menu_ID=@MenuID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


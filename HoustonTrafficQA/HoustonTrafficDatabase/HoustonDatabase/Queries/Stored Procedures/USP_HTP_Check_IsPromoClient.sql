﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Check_IsPromoClient]    Script Date: 11/27/2013 15:28:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
-- =============================================
-- Author:		Sabir Khan
-- Task ID:		11460
-- Create date: 09/24/2013
-- Description:	This procedure is used to check for promo price
-- =============================================
*******/
--[dbo].[USP_HTP_Check_IsPromoClient] 23903
ALTER PROCEDURE [dbo].[USP_HTP_Check_IsPromoClient]
@TicketID INT
AS
BEGIN	
SET NOCOUNT ON;
IF EXISTS(
		  SELECT tn.recordid 
		  FROM tbltickets t 
		  INNER JOIN tblticketsviolations tv 
		  ON tv.TicketID_PK = t.TicketID_PK 
		  INNER JOIN tblLetterNotes tn 
		  ON tn.RecordID = tv.RecordID 
		  AND ISNULL(tn.isPromo,0) = 1
		  WHERE t.TicketID_PK = @TicketID
		)
BEGIN
	--Sabir Khan 11560 11/26/2013 New Promo Logic has been implemented
	SELECT CONVERT(BIT,1) AS IsPromo, a.PromoMessageForHTP AS PromoMessageForHTP
		  FROM tbltickets t 
		  INNER JOIN tblticketsviolations tv 
		  ON tv.TicketID_PK = t.TicketID_PK 
		  INNER JOIN tblLetterNotes tn 
		  ON tn.RecordID = tv.RecordID 
		  INNER JOIN AttorneyPromoTemplates a
		  ON a.PromoID = tn.PromoID
	WHERE ISNULL(tn.isPromo,0) = 1
	AND t.TicketID_PK = @TicketID
		 
	
END
ELSE
BEGIN
	SELECT CONVERT(BIT,0) AS IsPromo, '' AS PromoMessageForHTP
END
END


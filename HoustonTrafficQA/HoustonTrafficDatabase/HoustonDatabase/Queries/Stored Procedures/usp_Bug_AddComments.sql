SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_AddComments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_AddComments]
GO





CREATE procedure [dbo].[usp_Bug_AddComments]  
  
@Description as varchar(1000),  
@CommentDate as datetime ,  
@EmployeeID as int,  
@BugID as int  
  
as  
  
update  tbl_bug_comments  set
 
 description=@Description,  
 --commentsdate=@CommentDate,  
 commentsdate = getdate(),
 EmployeeID=@EmployeeID  

where 
	Bug_id  =@BugID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/*  Business Logic:
	This procedure display the total count and total amount of courts...

List of Input Parameters:
	@sDate:	Start Date.
	@eDate: End Date.
	@FirmId: Firm ID
List of Output Columns:
Category Type
Total count
Total Amount
Display Col
*/  
--USP_HTS_Get_FinReport_BalancePaid_Ver2 '12/22/2008', '12/22/2008', null        
ALTER procedure [dbo].[USP_HTS_Get_FinReport_BalancePaid_Ver2]          
 (          
 @sDate datetime,          
 @eDate datetime,          
 @firmId int = NULL,
 @BranchId INT =1 -- Sabir Khan 10920 05/27/2013 Branch Id added.             
 )          
as           
          
select @eDate = @eDate + '23:59:59.998'              
            
 declare @temp1 table (ticketid int,bondflag  bit,chargeamount money,courtid  int,paymenttype int, recdate datetime)              
             
            
 if @firmId is not null             
  insert into @temp1             
  select distinct p.ticketid,              
   t.bondflag,              
   isnull(p.chargeamount,0),  
   --Sabir Khan 4942 01/05/2009 fixed duplication issue ...
   --v.courtid ,              
   Courtid = (select top 1 CourtID from tblTicketsViolations where ticketid_pk = v.ticketid_pk order by courtdate DESC ),  
   p.paymenttype,        
 P.RECDATE        
  from  tbltickets T , tblticketspayment P,   tblticketsviolations v              
  where T.ticketid_pk = P.ticketid               
  and t.ticketid_pk = v.ticketid_pk              
  --and P.recdate between @sDate and @eDate              
  AND DATEDIFF(DAY, p.recdate, @sdate)<=0  
  AND DATEDIFF(DAY, p.RecDate, @edate)>=0  
  and p.paymentvoid = 0              
  and p.paymenttype not in (99,100,9,11,8,103)              
--and v.coveringFirmId = @firmId        
  and v.ticketsviolationid in (              
    select  ticketsviolationid from tblticketsviolations              
    where ticketid_pk  = v.ticketid_pk and (courtid is not null and courtid <> 0)and (v.coveringFirmId = @firmId))            
 else if @firmId is null            
   insert into @temp1             
  select distinct p.ticketid,              
   t.bondflag,              
   isnull(p.chargeamount,0),              
   --v.courtid ,              
   Courtid = (select top 1 CourtID from tblTicketsViolations where ticketid_pk = v.ticketid_pk order by courtdate DESC ),       
   p.paymenttype  ,        
 P.RECDATE            
  from  tbltickets T , tblticketspayment P,   tblticketsviolations v              
  where T.ticketid_pk = P.ticketid               
  and t.ticketid_pk = v.ticketid_pk              
  --and P.recdate between @sDate and @eDate              
  AND DATEDIFF(DAY, p.recdate, @sdate)<=0  
  AND DATEDIFF(DAY, p.RecDate, @edate)>=0  
  
  and p.paymentvoid = 0              
  and p.paymenttype not in (99,100,9,11,8,103)              
  and v.ticketsviolationid in (              
    select top 1 ticketsviolationid from tblticketsviolations              
    where ticketid_pk  = v.ticketid_pk and (courtid is not null and courtid <> 0))            
    AND p.BranchID IN (SELECT branchid FROM fn_get_branchIDs(@BranchId))            
           
-- CREATING TEMPORARY TABLE......              
declare @temp3 table (              
 categorytype varchar(50),              
 totalcount int,              
 totalamount money,              
 DisplayCol int              
 )              
              
insert  into @temp3 select 'Tickets',0,0,0              
              
-- GETTING HOUSTON REGULAR TICKETS.......              
insert  into @temp3               
select 'Houston Municipal Courts' , count(ticketid), isnull(sum(chargeamount),0),1              
            
from @temp1 t right outer join tblcourts c on t.courtid = c.courtid              
where bondflag = 0              
and c.courtid in (3001,3002,3003)
             
              
-- GETTING OUTSIDE COURTS REGULAR TICKETS.......              
insert  into @temp3               
select 'Haris County JP Courts' , count(ticketid),  isnull(sum(chargeamount),0),1              
              
from @temp1 t right outer join tblcourts c              
on  t.courtid = c.courtid              
where bondflag = 0              
and  isnull(c.courtcategorynum,0) = 2 
             
--and c.inactiveflag = 0              
              
              
              
-- GETTING OUTSIDE COURTS REGULAR TICKETS.......              
insert  into @temp3               
select c.courtname , count(ticketid),  isnull(sum(chargeamount),0),1              
from @temp1 t right outer join tblcourts c              
on  t.courtid = c.courtid              
where bondflag = 0  and c.courtId not in (3071,3077)            
and  isnull(c.courtcategorynum,0) not in (1,2)              
and c.iscriminalcourt = 0              
--and c.inactiveflag = 0              
group by              
 c.courtname              
having count(ticketid) > 0              
              
              
              
insert into @temp3 select 'Bonds',0,0,0              
              
-- GETTING HOUSTON BOND TICKETS....              
insert  into @temp3               
select 'Houston Municipal Courts' , count(ticketid),  isnull(sum(chargeamount),0),1              

from @temp1 t right outer join tblcourts c on t.courtid = c.courtid               
where bondflag = 1              
and c.courtid in (3001,3002,3003) 
              
              
-- GETTING OUTSIDE COURTS BOND TICKETS.......              
insert  into @temp3               
select 'Haris County JP Courts' , count(ticketid),  isnull(sum(chargeamount),0),1           

from @temp1 t, tblcourts c              
--from @temp1 t right outer join tblcourts c on t.courtid = c.courtid              
where t.courtid = c.courtid              
and bondflag = 1              
and  isnull(c.courtcategorynum,0) = 2              
--and  c.inactiveflag = 0              
and c.iscriminalcourt = 0 
             
              
-- GETTING OUTSIDE COURTS BOND TICKETS.......              
insert  into @temp3               
select c.courtname , count(ticketid),  isnull(sum(chargeamount),0),1              
from @temp1 t right outer join tblcourts c              
on  t.courtid = c.courtid              
where bondflag = 1              
and  t.courtid not in (               
  select courtid from tblcourts               
  where ( isnull(courtcategorynum,0) in ( 1,2)  or iscriminalcourt =1 )              
  )              
--and c.inactiveflag = 0              
group by              
 c.courtname              
having count(ticketid) > 0              
              
              
insert into @temp3 select 'Criminal Division',0,0,0              
              
-- GETTING  Harris County Criminal TICKETS              
insert  into @temp3               
select c.courtname , count(ticketid),  isnull(sum(chargeamount),0),1              
from @temp1 t right outer join tblcourts c              
on  t.courtid = c.courtid              
where  c.iscriminalcourt = 1              
--and c.inactiveflag = 0              
group by               
 c.courtname              
having count(ticketid) > 0              
        
--Sabir Khan 4750 09/11/2008 For Title 4D category      
---------------------------------------------------------------------- 
----Sabir Khan 5418 01/15/2009 Title 4D should be Family....       
insert into @temp3 select 'Family',0,0,0                        
-- GETTING  Harris County Title 4D Ct TICKETS              
--insert  into @temp3               
--select 'Harris County Title 4D Ct' , count(ticketid),  isnull(sum(chargeamount),0),1              
--from @temp1 t right outer join tblcourts c              
--on  t.courtid = c.courtid              
--where  c.CourtID = 3077              
----and c.inactiveflag = 0              
--group by               
-- c.courtname              
--having count(ticketid) > 0        
        
--select * from @temp1        
insert  into @temp3 
--Sabir Khan 5515 02/11/2009 Court name has been get from database...              
select c.courtname , count(ticketid),  isnull(sum(chargeamount),0),1              
from @temp1 t right outer join tblcourts c on t.courtid = c.courtid               
--where c.courtid = 3077 
where c.CaseTypeId = 4
group by c.courtname       
----------------------------------------------------------------------        
        
        
--Sabir Khan 4750 09/11/2008 For Civil category        
----------------------------------------------------------------------        
insert into @temp3 select 'Civil',0,0,0             
-- GETTING  Harris County Civil Court TICKETS              
insert  into @temp3 
--Sabir Khan 5515 02/11/2009 Court name has been get from database...              
select  c.courtname  , count(ticketid),  isnull(sum(chargeamount),0),1              
from @temp1 t  right outer join tblcourts c on t.courtid = c.courtid           
--where  c.courtid = 3071
where c.CaseTypeId = 3
group by c.courtname              
        
----------------------------------------------------------------------        
-- RETURNING THE FINAL RESULT SET.......              
select * from @temp3 
USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_get_list_all_firms]    Script Date: 11/14/2008 05:55:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Sabir Khan 4640 08/20/2008
-- Business Logic : this procedure is used to display Out side clients 
-- Parameter:	@FirmFlag
						 --0.  will display all firms
						 --1.  will display all Outside firms where "Outside firm client" or "Firms covering cases for us" or both
						 --2.  will display only that don't have  "Outside firm client" or "Firms covering cases for us"
						
						
ALTER procedure [dbo].[usp_hts_get_list_all_firms]
@FirmFlag int      
as
BEGIN
IF @FirmFlag=1 --Active
BEGIN
select FirmName,FirmAbbreviation,FirmID,Address,Address2,      
City,State,Zip,Phone,Fax,AttorneyName,Firmsubtitle,basefee,daybeforefee,      
judgefee,
case when isnull(cancoverfrom,0)=1 then 'Yes' else 'No' end as coverfrom,
case when isnull(cancoverby,0)=1 then 'Yes' else 'No' end as coverby    
from tblfirm
Where
CanCoverFrom = 1 or CanCoverBy=1      
order by firmname 
END
ELSE
IF @FirmFlag=2 --Inactive
BEGIN
select FirmName,FirmAbbreviation,FirmID,Address,Address2,      
City,State,Zip,Phone,Fax,AttorneyName,Firmsubtitle,basefee,daybeforefee,      
judgefee,
case when isnull(cancoverfrom,0)=1 then 'Yes' else 'No' end as coverfrom,
case when isnull(cancoverby,0)=1 then 'Yes' else 'No' end as coverby    
from tblfirm 
Where
CanCoverFrom=0 and CanCoverBy=0     
order by firmname 
END
ELSE
IF @FirmFlag=0 --All
BEGIN      
select FirmName,FirmAbbreviation,FirmID,Address,Address2,      
City,State,Zip,Phone,Fax,AttorneyName,Firmsubtitle,basefee,daybeforefee,      
judgefee,
case when isnull(cancoverfrom,0)=1 then 'Yes' else 'No' end as coverfrom,
case when isnull(cancoverby,0)=1 then 'Yes' else 'No' end as coverby    
from tblfirm      
order by firmname 
END
END

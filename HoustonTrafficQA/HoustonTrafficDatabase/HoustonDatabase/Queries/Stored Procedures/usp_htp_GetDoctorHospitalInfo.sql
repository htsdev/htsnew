﻿/******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to Get get doctor/hospital information.
* List of Parameter : @PatientID 
* Column Return :     
******/
-- USP_HTP_GetDoctorHospitalInfo 29
CREATE PROC  dbo.USP_HTP_GetDoctorHospitalInfo 
@PatientID INT
as
SELECT Doctor.Id AS doctor1Id,
       Doctor.LastName AS Doctor1LastName,
       Doctor.FirstName AS Doctor1FirstName,
       Doctor.Address AS Doctor1Address,
       Doctor.City AS Doctor1City,
       Doctor.State AS Doctor1State,
       Doctor.Zip AS Doctor1Zip,
       Doctor_1.Id AS doctor2Id,
       Doctor_1.LastName AS doctor2Lastname,
       Doctor_1.FirstName AS doctor2FirstName,
       Doctor_1.Address AS doctor2Address,
       Doctor_1.City AS doctor2City,
       Doctor_1.State AS doctor2State,
       Doctor_1.Zip AS doctor2Zip,
       Hospital.Name AS Hosptial1Name,
       Hospital.City AS Hosptial1City,
       Hospital.State AS Hosptial1State,
       Hospital.ZipCode AS Hosptial1Zipcode,
       Hospital.ADDRESS AS Hosptial1Address,
       Hospital_1.Name AS Hosptial12Name,
       Hospital_1.City AS Hosptial12City,
       Hospital_1.State AS Hosptial12State,
       Hospital_1.ZipCode AS Hosptial12Zipcode,
       Hospital_1.ADDRESS AS Hosptial12Address,
       Patient.PatientStatusId,
       Patient.SourceId,
       Patient.OtherSource
FROM   Hospital AS Hospital_1
       RIGHT OUTER JOIN Patient
            ON  Hospital_1.Id = Patient.HospitalId2
       LEFT OUTER JOIN Hospital
            ON  Patient.HostpitalId1 = Hospital.Id
       LEFT OUTER JOIN Doctor
            ON  Patient.DoctorId = Doctor.Id
       LEFT OUTER JOIN Doctor AS Doctor_1
            ON  Patient.Doctorid2 = Doctor_1.Id
WHERE Patient.Id=@PatientID            
GO

GRANT EXECUTE ON dbo.USP_HTP_GetDoctorHospitalInfo TO dbr_webuser
GO
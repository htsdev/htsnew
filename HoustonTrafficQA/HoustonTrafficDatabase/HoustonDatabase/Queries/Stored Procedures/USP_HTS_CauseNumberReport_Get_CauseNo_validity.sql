SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_CauseNumberReport_Get_CauseNo_validity]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_CauseNumberReport_Get_CauseNo_validity]
GO



CREATE procedure USP_HTS_CauseNumberReport_Get_CauseNo_validity --ID103913,3001 
  
@courtid int,  
@causenumber varchar(20)  
  
as   
  
declare @tempcausenumber int  
  
  
 select  @tempcausenumber = count(tv.casenumassignedbycourt)  
  from  tblticketsviolations tv inner join           
    tbltickets t on           
     tv.ticketid_pk=t.ticketid_pk          
   where  tv.casenumassignedbycourt = @causenumber                 
     and  tv.courtid=@courtid     
  
select @tempcausenumber  
  
  
  




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_INSERT_PLADocument]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_INSERT_PLADocument]
GO



CREATE Procedure [dbo].[USP_HMC_INSERT_PLADocument]   
(  
  
@RecordIDs varchar(200),  
@FileName varchar(50),  
@PLADate DateTime   
)  
  
as  
  
  
declare @Records  table(ID int)          
insert into @Records select * from dbo.sap_string_param(@RecordIDs)     
  
  
Update tbl_HMC_Efile  
  
Set   
Flag = 1 ,  
PlADocument = @FileName,  
PlADate = @PLADate  
  
where RecordID in ( Select ID from @Records )  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


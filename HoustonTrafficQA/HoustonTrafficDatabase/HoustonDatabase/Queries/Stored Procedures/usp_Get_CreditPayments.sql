USE TrafficTickets
GO
/***
* Business Logic: This procedure is used to get credit card payment.
***/
--khalid 3309 3/11/08 to add firms compatablity
ALTER procedure [dbo].[usp_Get_CreditPayments]           
                  
@RecDate DateTime,              
@RecToDate datetime,  
@firmId int = NULL,
@BranchId INT =1  -- Sabir Khan 10920 05/27/2013 Branch id check added.               
              
                  
AS                  
                  
set @RecDate = @RecDate + '00:00:00'                      
set @RecToDate = @RecToDate + '23:59:58'     
            
            
SELECT distinct p.ChargeAmount AS ChargeAmount,                     
p.EmployeeID,                           
p.PaymentType,                     
CONVERT(Varchar(12), p.RecDate, 101) AS RecDate,                     
u.Lastname,                           
CASE  wHEN (LEN(t.Lastname + ', ' + t.Firstname)>19) THEN  t.Lastname + ', ' +t.Firstname + '...'                          
ELSE t.Lastname + ', ' + t.Firstname                          
END AS CustomerName,                          
(Select ShortName from TblCourts Where CourtID = (Select top 1 CourtID from tblTicketsViolations where ticketid_pk = v.ticketid_pk order by courtdate desc)) AS Court,                    
pt.Description AS CCType,                      
p.TicketID,                    
case t.bondflag when 1 then 'B' else '' end AS BondFlag,                           
-- Added By Zeeshan Ahmed On 6/20/2007------                  
CASE WHEN isnull(t.initialadjustment,0) <> 0 then 'YES'  ELSE '' END as Adj1,                    
CASE WHEN ISNULL(t.adjustment,0) <> 0 THEN 'YES' ELSE '' END as Adj2,                     
--------------------------------------------                  
p.CardType,                    
             
dbo.Fn_FormateTime(p.RecDate) As RecTime  ,                    
p.PaymentType ,    
p.recdate as sorttime           
, IPAddress -- Sabir Khan 10920 05/27/2013 getting IP Address in result set.                
FROM dbo.tblTickets t                     
INNER JOIN                    
dbo.tblCourts c                     
INNER JOIN                    
dbo.tblTicketsViolations v                     
ON  c.Courtid = v.CourtID                     
and v.ticketsviolationid in(
 select  ticketsviolationid from tblticketsviolations       
    where ticketid_pk  = v.ticketid_pk and (courtid is not null and courtid <> 0)AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))               
      )   
ON  t.TicketID_PK = v.TicketID_PK                     
RIGHT OUTER JOIN                    
dbo.tblPaymenttype pt                     
RIGHT OUTER JOIN                    
dbo.tblTicketsPayment p                     
ON  pt.Paymenttype_PK = p.PaymentType                     
ON  t.TicketID_PK = p.TicketID                     
LEFT OUTER JOIN                    
dbo.tblUsers u                     
ON  p.EmployeeID = u.EmployeeID             
Where        p.RecDate  Between  @RecDate  AND  @RecToDate                    
And   p.paymenttype in (101,102)        
AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId)) 
AND p.BranchID IN (SELECT branchid FROM fn_get_branchIDs(@BranchId))               
ORDER by              
p.RecDate desc

GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_GetNCOA18Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_GetNCOA18Report]
GO


--select datediff(day,'08/12/2007','08/10/2007')


--select * from tblletternotes

CREATE procedure [dbo].[USP_Mailer_GetNCOA18Report]
@StartDate datetime,
@EndDate datetime
as

select distinct noteid into #NoteID from tblticketsarchive T, tblletternotes N
where T.recordid = N.recordid
and ncoa18flag = 1 
and N.courtid in  (3001,3002,3003)
and currentdate between @StartDate and dateadd(day,1,@EndDate)
and datediff(day,currentdate,accuzipprocessdate) <=0
and lettertype = 9


--drop table #temp

create table #temp(TotalMailed int,ClientCount int, QuoteCount int, Revenue money, MailerExpense money, NetIncome money  )

insert into #temp 
values (0,0,0,0,0,0)

select Activeflag,count(distinct(ticketid_pk)) as clientcount,sum(isnull(totalfeecharged,0)) as totalrev
into #totalrev
from #noteid N, tbltickets T
where N.noteid = T.mailerid
group by activeflag 


update #temp set clientcount = isnull((select clientcount from #totalrev where activeflag = 1),0)

update #temp set quotecount = isnull((select clientcount from #totalrev where activeflag = 0),0)

update #temp set Revenue = isnull((select totalrev from #totalrev where activeflag = 1),0)

update #temp set totalmailed = isnull((select count(*) from #noteid),0)

update #temp set MailerExpense = isnull(totalmailed * 0.45,0)


update #temp set NetIncome = isnull(Revenue - MailerExpense ,0)


drop table #totalrev
drop table #noteid

select * from #temp

drop table #temp


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


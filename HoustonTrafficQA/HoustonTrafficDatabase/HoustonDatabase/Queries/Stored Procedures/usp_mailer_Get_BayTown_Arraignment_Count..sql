﻿/****** 
Created by:		Rab Nawaz Khan
Task	  :		10436
Date	  :		09/26/2012
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				COUNT for the Baytown Municipal Court Appearance letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

-- usp_mailer_Get_BayTown_Arraignment_Count 36, 135,36, '09/20/2012', '09/20/2012', 0
CREATE PROCEDURE [dbo].[usp_mailer_Get_BayTown_Arraignment_Count]
	(
	@catnum			INT,
	@LetterType 	INT,
	@crtid 			INT,                                                 
	@startListdate 	DATETIME,
	@endListdate 	DATETIME,                                                      
	@SearchType     INT
	)

AS                                                              
                          
-- DECLARING LOCAL VARIABLES FOR FILTERS.....                                  
DECLARE	@officernum VARCHAR(50), 
	@officeropr VARCHAR(50), 
	@tikcetnumberopr VARCHAR(50), 
	@ticket_no VARCHAR(50), 
	@zipcode VARCHAR(50), 
	@zipcodeopr VARCHAR(50), 
	@fineamount MONEY,
	@fineamountOpr VARCHAR(50) ,
	@fineamountRelop VARCHAR(50),
	@singleviolation MONEY, 
	@singlevoilationOpr VARCHAR(50) , 
	@singleviolationrelop VARCHAR(50), 
	@doubleviolation MONEY,
	@doublevoilationOpr VARCHAR(50) , 
	@doubleviolationrelop VARCHAR(50), 
	@count_Letters INT,
	@violadesc VARCHAR(500),
	@violaop VARCHAR(50)

-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL.
DECLARE @sqlquery NVARCHAR(max), @sqlquery2 NVARCHAR(max), @sqlParam NVARCHAR(1000)
SELECT 	@sqlquery ='',@sqlquery2 = ''

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
SELECT @sqlparam = '@catnum INT, @LetterType INT, @crtid INT, @startListdate DATETIME, 	
		    @endListdate DATETIME, @SearchType INT'
                                                      
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                                              
SELECT	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
FROM 	tblmailer_letters_to_sendfilters  
WHERE 	courtcategorynum=@catnum 
AND 	Lettertype=@LetterType 
                                                      
-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY 
-- INSERTING VALUES IN A TEMP TABLE....
SET 	@sqlquery=@sqlquery+'                                          
SELECT DISTINCT  
CONVERT(DATETIME , CONVERT(VARCHAR(10),ta.listdate,101)) AS mdate,   
flag1 = ISNULL(ta.Flag1,''N'') ,                                                      
ta.donotmailflag, ta.recordid,                                                    
COUNT(tva.ViolationNumber_PK) AS ViolCount,                                                      
SUM(tva.fineamount)AS Total_Fineamount
INTO #temp                                                            
FROM dbo.tblTicketsViolationsArchive TVA INNER JOIN dbo.tblTicketsArchive TA ON TVA.recordid = TA.recordid 
	INNER JOIN tblstate S ON ta.stateid_fk = S.stateID INNER JOIN dbo.tblCourtViolationStatus tcvs 
	ON 	tva.violationstatusid = tcvs.CourtViolationStatusID

-- First Name AND Last name should not be null. . . 
WHERE ISNULL(ta.FirstName, '''') <> ''''
AND ISNULL(ta.LastName, '''') <> ''''

-- ONLY NON-CLIENTS....
AND ta.clientflag=0 

-- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
AND ISNULL(ta.ncoa48flag,0) = 0

-- FOR THE SELECTED LIST DATE RANGE ONLY...
AND datediff(day,  ta.listdate , @startListdate)<=0
AND datediff(day,  ta.listdate , @endlistdate)>=0
 
--Only Appearance status
AND tcvs.CategoryID = 2 

'
 
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@crtid = @catnum ) 
	SET @sqlquery=@sqlquery+' 
	AND tva.CourtLocation In (SELECT courtid FROM tblcourts WHERE courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else 
	SET @sqlquery =@sqlquery +' 
	AND tva.CourtLocation = @crtid' 

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                           
if(@officernum<>'')                                                        
	SET @sqlquery =@sqlquery + '  
	AND ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...

if(@ticket_no<>'')                        
	SET @sqlquery=@sqlquery + ' 
	AND ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              

-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                    
if(@zipcode<>'')                                                                              
	SET @sqlquery=@sqlquery+ ' 
	AND ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION...
if(@violadesc<>'')            
	SET @sqlquery=@sqlquery+ ' 
	AND ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 AND @fineamountOpr<>'not'  )                                  
	SET @sqlquery =@sqlquery+ '
        AND  tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 AND @fineamountOpr = 'not'  )                                  
	SET @sqlquery =@sqlquery+ '
        AND not tva.fineamount'+ CONVERT (VARCHAR(10),@fineamountRelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@fineamount) +')'                                       
                                                      
-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
SET @sqlquery =@sqlquery+ ' 

GROUP BY CONVERT(DATETIME , CONVERT(VARCHAR(10),ta.listdate,101)), ta.Flag1, ta.donotmailflag ,ta.recordid    

DELETE FROM #temp
FROM #temp a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
WHERE 	lettertype =@LetterType 
or (
		l.courtcategory = 36
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
	)

-- GETTING THE RESULT SET IN TEMP TABLE.....
SELECT DISTINCT  ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag, recordid INTO #temp1  FROM #temp  WHERE 1=1                                                        
'                                  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 AND @doubleviolation=0)                                  
	SET @sqlquery =@sqlquery+ '
	AND '+ @singlevoilationOpr+ '  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND ViolCount=1) or violcount>3
    '                                  
                                  
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                       
if(@doubleviolation<>0 AND @singleviolation=0 )                                  
	SET @sqlquery =@sqlquery + '
	AND'+ @doublevoilationOpr+ '   (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND ViolCount=2)  or violcount>3 
	'

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 AND @singleviolation<>0)                                  
	SET @sqlquery =@sqlquery+ '
	AND'+ @singlevoilationOpr+ '  (Total_FineAmount '+ CONVERT (VARCHAR(10),@singleviolationrelop) +'CONVERT(MONEY,'+ CONVERT(VARCHAR(10),@singleviolation)+') AND ViolCount=1)
	or' + @doublevoilationOpr+'   (Total_FineAmount '+CONVERT (VARCHAR (10),@doubleviolationrelop) +'CONVERT (MONEY,'+ CONVERT(VARCHAR(10),@doubleviolation) + ')AND ViolCount=2) or violcount>3 
	'

-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
SET @sqlquery2=@sqlquery2 +'
SELECT COUNT(DISTINCT recordid) AS Total_Count, mdate 
INTO #temp2 
FROM #temp1 
GROUP BY mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
SELECT COUNT(DISTINCT recordid) AS Good_Count, mdate 
INTO #temp3
FROM #temp1 
WHERE Flag1 in (''Y'',''D'',''S'')   
AND donotmailflag = 0    
GROUP BY mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS 
SELECT COUNT(DISTINCT recordid) AS Bad_Count, mdate  
INTO #temp4  
FROM #temp1 
WHERE Flag1  not in (''Y'',''D'',''S'') 
AND donotmailflag = 0 
GROUP BY mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
SELECT COUNT(DISTINCT recordid) AS DonotMail_Count, mdate  
INTO #temp5   
FROM #temp1 
WHERE donotmailflag=1 
GROUP BY mdate
	

-- OUTPUTTING THE REQURIED INFORMATION........         
SELECT   #temp2.mdate AS listdate,
        Total_Count,
        ISNULL(Good_Count,0) Good_Count,  
        ISNULL(Bad_Count,0)Bad_Count,  
        ISNULL(DonotMail_Count,0)DonotMail_Count  
FROM #Temp2 left OUTER JOIN #temp3   
on #temp2.mdate=#temp3.mdate  
left OUTER JOIN #Temp4  
on #temp2.mdate=#Temp4.mdate    
left OUTER JOIN #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
WHERE Good_Count > 0 ORDER BY #temp2.mdate desc

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
DROP TABLE #temp DROP TABLE #temp1 DROP TABLE #temp2  DROP TABLE #temp3  DROP TABLE #temp4  DROP TABLE #temp5 
'                                                      
                                                      
-- print @sqlquery  + @sqlquery2                                       

-- CONCATENATING THE QUERY ....
SET @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL QUERY...
EXEC sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


ALTER procedure [dbo].[USP_HTS_Update_CaseDispositionDetail]   
           
 @Ticketsviolationid  int,          
 @MISCComments   varchar(200),           
 @ShowCauseDate   datetime,           
 @IsMISCSelected   int,           
 @IsRETSelected   int,           
 @IsDSCSelected   int,           
 @TimeToPayDate  datetime,           
 @TimeToPay    int,           
 @DSCProbationDate  datetime,           
 @DSCProbationTime  int,           
 @DSCProbationAmount  money,           
 @ViolationStatusID  int,          
 @EmployeeId   int,        
 @ShowCauseCourtNumber int,        
 @RetDate datetime,        
 @RetCourtNumber int,        
 @IsInsSelected int,        
 @CourseCompletionDays int,        
 @DrivingRecordsDays int,        
 @IsShowCauseSelected int  ,        
 @CoveringFirmID int--Added by M.Azwar Alam for taskno 1126--        
          
As          
          
declare @CurrStatus int,          
 @CaseNumber varchar(25),          
 @TicketID int,          
 @OutComeStatus varchar(25),          
 @PreviousFirmID int,      
 @PreviousFirmName varchar(25),      
 @CoveringFirmName varchar(25),      
 @command varchar(max),      
 @TicketNumber varchar(20)      
      
      
      
      
--select * from tblticketsviolations where ticketId_pk = 8445      
          
select @CurrStatus = violationstatusid ,          
 @CaseNumber = refcasenumber,          
@TicketNumber= refcasenumber,      
@Ticketid = ticketid_pk ,          
 @PreviousFirmID = CoveringFirmID      
from  tblticketsviolations           
where  ticketsviolationid = @ticketsviolationid          
          
select @outcomestatus = description from tblcasedispositionstatus where casestatusid_pk = @ViolationStatusID           
          
begin          
          
 update  tblTicketsviolations          
  set  MISCComments   =@MISCComments,           
   ShowCauseDate   =@ShowCauseDate,           
   IsMISCSelected   =@IsMISCSelected,           
   IsRETSelected   =@IsRETSelected,           
   IsDSCSelected   =@IsDSCSelected,           
   TimeToPayDate  =@TimeToPayDate,           
   TimeToPay   =@TimeToPay,           
   DSCProbationDate =@DSCProbationDate,           
   DSCProbationTime  =@DSCProbationTime,           
   DSCProbationAmount =@DSCProbationAmount,           
   ViolationStatusID =@ViolationStatusID,        
   ShowCauseCourtNumber=@ShowCauseCourtNumber ,        
   RetDate=@RetDate ,        
   RetCourtNumber=@RetCourtNumber ,        
   IsIns=@IsInsSelected ,        
   CourseCompletionDays=@CourseCompletionDays ,        
   DrivingRecordDays=@DrivingRecordsDays ,        
   IsShowCause=@IsShowCauseSelected ,        
   CoveringFirmID= @CoveringFirmID        
where ticketsviolationid = @ticketsviolationid          
      
      
if ( @PreviousFirmID <> @CoveringFirmID )      
      
Begin      
      
if(@PreviousFirmID is not null )      
      
begin      
   select @PreviousFirmName=FirmAbbreviation from tblfirm where firmid=@PreviousFirmID      
end      
      
else      
      
begin      
    set @PreviousFirmName='N/A'      
end      
      
select @CoveringFirmName=FirmAbbreviation from tblfirm where firmid=@CoveringFirmID      
      
set @command='Firm Changed - ' + @TicketNumber + ' - ' + isnull(@PreviousFirmName,'N/A') + ' to ' + @CoveringFirmName      
exec usp_HTS_Insert_CaseHistoryInfo  @TicketID,@command ,@EmployeeId,''      
      
      
End      
      
      
      
update t       
set t.CoveringFirmID = @CoveringFirmID,      
 t.employeeidupdate = @EmployeeId      
from tbltickets t inner join tblticketsviolations v on t.ticketid_pk = v.ticketid_pk       
where v.ticketsviolationid = @ticketsviolationid      
  
 if @CurrStatus <> @ViolationStatusID          
  begin          
     
 insert into tblticketsnotes (ticketid , subject , recdate, employeeid)          
   values  (@Ticketid , 'Viol. Outcome changed to ' + @outcomestatus + ' for Case # ' + @CaseNumber, getdate(), @employeeid  )          
  end         
end      
  
go 
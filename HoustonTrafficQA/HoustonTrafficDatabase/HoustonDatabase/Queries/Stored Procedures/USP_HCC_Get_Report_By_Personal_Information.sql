use JIMS

go





/****** 
Create by:  Sarim Ghani
Business Logic : This procedure is used to get the case information of criminal cases.

List of Parameters:     
      @ViolationDesc : Violation Description of case

List of Columns: 
      Connection : represents connection of the case
      
******/

--Kazim 3735 05/27/2008 Get Connection type of Attorneys 

 --USP_HCC_Get_Report_By_Personal_Information '','','','','','','','-1','','','','','','',''  
                 
 ALTER Procedure [dbo].[USP_HCC_Get_Report_By_Personal_Information] --@Spn='' ,@Lastname='', @Attorney='HINTON, M J'                 
(                    
                      
@CDI varchar(3) =''  ,                  
@CaseNumber varchar(30) ='',                    
@LastName varchar(50)='',                    
@FirstName varchar(50)='',                    
@Spn varchar(50)='',                    
@DOB varchar(12)='',    
@CourtRoom varchar(4)='',  
@Attorney varchar(80)='',  
@NextSettingDateFrom varchar(12)='',                    
@NextSettingDateTo varchar(12)='',                    
@DataLoadDateFrom varchar(12)='',                    
@DataLoadDateTo varchar(12)='',  
@Level varchar(20)='',                 
@Disposition varchar(20)='',
@ViolationDesc varchar(200)='',
@rowcount int = 1000    
)                    
                    
as                    
                    
declare @Query  varchar(8000)                    
                    
-- Agha Usman 4239 06/25/2008 - Page Size                    
Set @Query = '                    
    
set rowcount ' + Convert(varchar(20),@rowcount) + '    
    
select  distinct                               
isnull(left(C.FirstName,20), ltrim(substring(C.Name,(case when CHARINDEX('','', C.Name) > 0 then  CHARINDEX('','', C.Name) else  CHARINDEX('' '', C.Name) end)+1, len(C.Name)) )   ) as FirstName  ,                        
isnull(left(C.LastName,20), ltrim(SUBSTRING(C.Name,1,   (case when CHARINDEX('','', C.Name) > 0 then  CHARINDEX('','', C.Name) else  CHARINDEX('' '', C.Name) end)    -1))) as lastName ,                           
                    
 left(C.MiddleName,6) as MiddleName ,                         
  C.BookingNumber,                              
  L.Span,                              
  C.casenumber,                    
  DOB,                    
  L.ccr as CourtRoom,                              
  C.chargewording as ViolationDescription,                    
  Disposition,                    
  NextSettingDate,                    
  SettingReason ,                    
  HCC.Floor,                  
  --qc.Name,                  
  C.Chargelevel,          
  l.recdate,
  (Select count(*) from  tblQCOC qcoc where qcoc.connection = ''HIRED DEFENSE ATTORNEY'' and qcoc.casenumber= C.casenumber ) as connection                                 
  from criminalcases C left outer join  Criminal503 L on C.bookingnumber = L.bookingnumber                               
  left outer join tblHCCourts HCC                     
  ON L.ccr = Courtid                    
  left outer join tblQCOC qc
  on qc.casenumber= C.casenumber
  where  1 = 1 '                    
                    
  If @CaseNumber != ''                    
  Set @Query = @Query + ' and C.CaseNumber =''' + @CaseNumber + ''''                    
                    
  If @LastName !=''                    
  Set @Query = @Query + ' and left(C.LastName,20) like ''' + @LastName + '%'''                    
                    
  If @FirstName !=''                    
  Set @Query = @Query + ' and left(C.FirstName,20) like ''' + @FirstName + '%'''                    
                    
  If @Spn !=''                    
  Set @Query = @Query + ' and L.Span =''' + @Spn + ''''                    
                    
  If @DOB !=''                    
  Set @Query = @Query + ' and DOB =''' + @DOB + ''''                   
                    
  If @CDI !='' and  @CDI !='-1'                  
  Set @Query = @Query + ' and Left (L.Casenumber,3) =''' + @CDI + ''''                    
                  
  if @CaseNumber ='' and @LastName ='' and @FirstName ='' and @Spn ='' and @DOB = '' and @CDI = '' and @Attorney ='-1'                 
  Set @Query = @Query + ' and 1 = 1 '                   
                   
  if @NextSettingDateFrom !='' and @NextSettingDateTo !=''                    
	begin
  --Set @Query = @Query  + ' and L.NextSettingDate between ''' +    @NextSettingDateFrom +   ' 00:00:00'' and ''' +  + ' 23:59:59'''                    
	Set @Query = @Query  + ' and datediff(day, L.NextSettingDate ,  '''+@NextSettingDateFrom+''' ) <= 0 '                    
	Set @Query = @Query  + ' and datediff(day, L.NextSettingDate ,  '''+@NextSettingDateTo+''' ) >= 0'                    
	end
                    
  if @DataLoadDateFrom !='' and @DataLoadDateTo !=''                    
    begin
  --Set @Query = @Query  + ' and L.RecDate between ''' + @DataLoadDateFrom +   ' 00:00:00'' and ''' +  @DataLoadDateTo + ' 23:59:59'''                                   
    Set @Query = @Query  + ' and datediff(day, l.recdate, '''+@DataLoadDateFrom+''') <= 0  '
	Set @Query = @Query  + ' and datediff(day, l.recdate, '''+@DataLoadDateTo+''') >= 0  '
   end
                     
  if @CourtRoom != ''                    
  Set @Query = @Query + ' and L.CCR =''' + @CourtRoom + ''''  
--  --by khalid for attorney  
   if @Attorney !='-1'      
Set @Query = @Query +'and qc.name ='''+@Attorney+''' and  qc.connection = ''HIRED DEFENSE ATTORNEY'''  
--
if(@Level!= '' and  @Level != '0')
  Set @Query = @Query + ' and C.Chargelevel =''' + @Level + '''' 

if(@Disposition!='')

  Set  @Query = @Query + ' and Disposition =''' + @Disposition + '''' 

   
if(@ViolationDesc!='')

  Set  @Query = @Query + 'and  C.chargewording like ' + '''%' + @ViolationDesc + '%''' 
               
        
Set  @Query = @Query + 'Order by L.SPAN '          
print ( @Query)             
exec (@Query)                    
    
go


/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for DMC Appearance letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@BondFlag:		Flag that identifies if searching for warrant letters or for appearance.
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	CourtDate:		court date associated with the violation.
	Abb:			Short abbreviation of employee who is printing the letter
	ChkGroup:		Recordid plus language flag for grouping of records on the report file.
	
*******/


--  usp_Mailer_Send_Dallas_Letters_New 6, 0, 6, '6/5/2007,' , 3991, 0, 0, 0
ALTER procedure [dbo].[usp_Mailer_Send_Dallas_Letters_New]                           
	(
	@catnum		int=6,                                    
	@bondFlag 	int=0,                                    
	@courtid 	int=6,                                    
	@startListdate 	varchar (500) = '03/24/2006,' ,                                    
	@empid 		int=3991,                              
	@printtype 	int =0 ,
	@language	int = 1  ,  
	@isprinted bit                              
	)
as                                    
                                          
-- DECLARING LOCAL VARIABLES FOR FILTERS...
declare	@officernum varchar(50),                                    


	@officeropr varchar(50),                                    
	@tikcetnumberopr varchar(50),                                    
	@ticket_no varchar(50),                                                                    
	@zipcode varchar(50),                                             
	@zipcodeopr varchar(50),                                    
	@fineamount money,                                            
	@fineamountOpr varchar(50) ,                                            
	@fineamountRelop varchar(50),                   
	@singleviolation money,                                            
	@singlevoilationOpr varchar(50) ,                                            
	@singleviolationrelop varchar(50),                                    
	@doubleviolation money,                                            
	@doublevoilationOpr varchar(50) ,                                            
	@doubleviolationrelop varchar(50),                                  
	@count_Letters int,                  
	@violadesc varchar(500),                  
	@violaop varchar(50),
	@LetterType int,
	@lettername varchar(50)

-- SETTING LETTER TYPE AND LETTER NAME..
set @lettertype = 29
set @lettername = 'DMC Appearance'


-- 
-- tahir 4756 09/11/2008 need to send a letter to barry...
declare @tempDate datetime, @mailbarry bit
select @tempDate = getdate(), @mailbarry = 1
--Yasir Kamal 6329 08/10/2009 send letter to barry for two weeks.
--Yasir Kamal 6397 08/17/2009 stop sending letters to barry
if @tempdate >= '8/11/08' and @Tempdate <= '8/24/08'
	set @mailbarry = 0 
else if not exists (select batchid from tblbatchletter where lettertype= @lettertype and datediff(month,batchsentdate,getdate())=0 )
	set @mailbarry = 0
else set @mailbarry = 1


-----------------------
                                    
declare @sqlquery varchar(MAX)  ,
	@sqlquery2 varchar(MAX)  ,
	@sqlquery3 varchar(MAX)                                
                                        
                          
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....                  
Select	@officernum=officernum,                                    
	@officeropr=oficernumOpr,                                    
	@tikcetnumberopr=tikcetnumberopr,                                    
	@ticket_no=ticket_no,                                                                      
	@zipcode=zipcode,                                    
	@zipcodeopr=zipcodeLikeopr,                                    
	@singleviolation =singleviolation,                                    
	@singlevoilationOpr =singlevoilationOpr,                                    
	@singleviolationrelop =singleviolationrelop,                                    
	@doubleviolation = doubleviolation,                                    
	@doublevoilationOpr=doubleviolationOpr,                                            
	@doubleviolationrelop=doubleviolationrelop,                  
	@violadesc=violationdescription,                  
	@violaop=violationdescriptionOpr ,              
	@fineamount=fineamount ,                                            
	@fineamountOpr=fineamountOpr ,                                            
	@fineamountRelop=fineamountRelop              
from 	tblmailer_letters_to_sendfilters                                    
where 	courtcategorynum=@catnum 
and lettertype =   @lettertype                                 
                                 


-- INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
Select @sqlquery ='', @sqlquery2 =''  , @sqlquery3 = ''

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid


-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'  declare @recCount int                                  

Select	distinct 
	tva.recordid, 
	mdate = convert(datetime,convert(varchar(10),ta.recloaddate,101)),
	count(tva.ViolationNumber_PK) as ViolCount,
	sum(isnull(tva.fineamount,0))as Total_Fineamount 
into #temptable                                          
FROM   	dallastraffictickets.dbo.tblTicketsViolationsArchive tva 
inner join 
	dallastraffictickets.dbo.tblTicketsArchive ta 
on 	tva.recordid = ta.recordid
inner join  dallastraffictickets.dbo.tblcourts tc  on 	ta.courtid = tc.courtid

-- PERSON NAME SHOULD NOT BE EMPTY..
where	ta.lastname is not null  and ta.firstname is not null

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0 	

-- EXCLUDE BOND CASES...
and  tva.violationbonddate is null 

-- EXCLUDE V TICKETS....
and charindex(''v'',tva.ticketnumber_pk)<>1

-- ONLY VERIFIED AND VALID ADDRESSES
and 	Flag1 in (''Y'',''D'',''S'')  

-- NOT MARKED AS STOP MAILING...
and 	isnull(donotmailflag,0)  =0 
-- tahir 5038 10/23/2008 no need to send for JUD/JUR
--Sabir Khan 5051 11/12/2008 Send DMC Appearance and DMC Appearance Day Of letters  only to APPEARANCE cases...
-- and tva.violationstatusid not in (26,103)
and tva.violationstatusid = 116
--Sabir Khan 6682 10/02/2009 EXCLUDE CASE HAVING STATUS IN (Hold the warrant ,Motion ,Motion: see problem ,Set for Court ,Work Release)
-- tahir 6786 10/16/2009 do not exclude HOLD THE WARRANT website status...
--and isnull(tva.WebSiteCourtStatus,0) not in (3,4,5,6,7)
-- Muneer Sheikh 8302 09/06/2010 "MOTION", "MOTION: SEE PROBLEM", "WORK RELEASE", "SET FOR COURT", "HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional //EN", "PAID: OFF-SITE; AGENCY; OTHER" Excluded from mailing.
and isnull(tva.WebSiteCourtStatus,0) not in ((SELECT wscs.ID  FROM DallasTrafficTickets.dbo.WebSiteCourtStatus wscs WHERE (wscs.MailAllowed=0 AND wscs.WebSite=''DMC'')))
'

-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED CATEGORY.....
if(@courtid = @CATNUM)
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum  = '+CONVERT(vARCHAR, @CATNUM)+' )'          

-- PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
else
	set @sqlquery=@sqlquery+' and tva.courtlocation=' + convert(varchar,@courtid)
                                    
-- ONLY FOR THE SELECTED DATE RANGE.....
if(@startListdate<>'')                                    
	set @sqlquery=@sqlquery+ ' 
	and '+dbo.Get_String_Concat_With_DatePart_ver2(''+@startListdate +'', 'ta.recloaddate' ) 
                                    

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery +'  
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
                                    
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                                    
	set @sqlquery=@sqlquery+ '
	and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'              


-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                
set @sqlquery =@sqlquery+ '
      and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                     

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                
set @sqlquery =@sqlquery+ '
      and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                     

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR APP DAY OF AND WARRANT LETTER PRINTED IN PAST FIVE DAYS FOR THE SAME RECORD..
-- Babar Ahmad 8597	07/13/2011 Changed number of days from 14 days to 5 business days. 
set @sqlquery =@sqlquery + '                                              
	and ta.recordid not in (                                                                    
		select	recordid from tblletternotes 
		where 	lettertype =29
		-- Babar Ahmad 8597	07/13/2011 Excluded App Day Of and Warrant letters. 
		union  
 select recordid from dbo.tblletternotes where lettertype in (18,30)   
 and datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0  
	)
                                     
group by   tva.recordid,  convert(datetime,convert(varchar(10),ta.recloaddate,101))
having 1=1 
'            

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    


-- SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                  
  
-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
set @sqlquery =@sqlquery+'                             
Select	distinct 
	a.recordid, a.mdate , /*dateadd(day, 21, ta.violationdate)*/ /*Modified by Adil 09/03/08 4723*/tva.courtdate as courtdate,	ta.courtid,  flag1 = isnull(ta.Flag1,''N'') , ta.officerNumber_Fk, 
	tva.TicketNumber_PK, isnull(ta.donotmailflag,0) as donotmailflag,
	ta.clientflag, 	upper(firstname) as firstname, upper(lastname) as lastname, upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city, s.state,
	tc.CourtName, zipcode, 	midnumber,  dp2 as dptwo, dpc, 	violationdate, 	violationstatusid, 
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, case when isnull(tva.fineamount,0)=0 then 100 else convert(numeric(10,0),tva.fineamount) end as FineAmount, 
	a.ViolCount, a.Total_Fineamount 
into #temp                                          
FROM   	dallastraffictickets.dbo.tblTicketsViolationsArchive tva 
inner join 
	dallastraffictickets.dbo.tblTicketsArchive ta 
on 	tva.recordid = ta.recordid
inner join  dallastraffictickets.dbo.tblcourts tc  on 	ta.courtid = tc.courtid
INNER JOIN
tblstate S ON
ta.stateid_fk = S.stateID
inner join #temptable a on a.recordid = ta.recordid where 1=1 
-- tahir 5038 10/23/2008 no need to send for JUD/JUR
--Sabir Khan 5051 11/12/2008 Send DMC Appearance and DMC Appearance Day Of letters  only to APPEARANCE cases...
--and tva.violationstatusid not in (26,103)
and tva.violationstatusid = 116
'

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
begin
	-- NOT LIKE FILTER.......
	if(charindex('not',@violaop)<> 0 )              
		set @sqlquery=@sqlquery+' 
		and	 not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           

	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )              
		set @sqlquery=@sqlquery+' 
		and	 ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
end
                 
-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....                            
set @sqlquery=@sqlquery+                     
'
Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, mdate, FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, courtdate,
 -- Sabir Khan 6361 08/11/2009 get promotional price scheme...
 dbo.GetPromoPriceTemplate(zipcode,'+convert(Varchar,@lettertype)+') AS promotemplate  
 into #temp1  from #temp                                    
 '            

set @sqlquery=@sqlquery+                       
' -- Sabir Khan 7099 12/07/2009 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on isnull(a.recordid,0) = isnull(v.recordid,0)
) 
 ' 

-- 6 copies of the letter will be mailed to Greg on this date for testing purposes
-- Abbas Shahid Khwaja 8799 4/19/2011 Change the values of dpc and dptwo in the #temp1 for barcode values and change zip code. 
-- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry
if @mailbarry = 0
	set @sqlquery=@sqlquery+ 
	'
	insert into #temp1 
	select ''1'', 116, 2, 150, '''+ convert(Varchar(10), @tempdate, 101) +''', ''BARRY'', ''BOBBIT'', ''7 Butterfly Place'',
	''Austin'', ''TX'', 100, ''Printed on '+ convert(varchar, getdate()) +''', ''TESTING'', ''1'',''22'', ''Y'', 0, ''TESTING 1'', 3049, ''78738-1343'', 0, 0, ''78738-1343'', '''', ''2/20/2008'',''0,35.00''
	'             

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype<>0) 
	BEGIN        
		set @sqlquery2 =@sqlquery2 +                                    
		'
		Declare @ListdateVal DateTime, @totalrecs int,@count int, @p_EachLetter money,@recordid varchar(50),                              
			@zipcode   varchar(12),@maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)
		declare @tempBatchIDs table (batchid int)

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
		Select @count=Count(*) from #temp1                                

		set @p_EachLetter=convert(money,0.50)                                

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                  
		Select distinct mdate  from #temp1                                
		open ListdateCur                                   
		Fetch Next from ListdateCur into @ListdateVal                                                      
		while (@@Fetch_Status=0)                              
			begin                                

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE ......	                     
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
				('+ convert(varchar(10),@empid) +', '+ convert(varchar(10),@lettertype) +' , @ListdateVal, '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                   

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter 
					insert into @tempBatchIDs select @maxbatch                                                       

					-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
					Declare RecordidCur Cursor for                               
					Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
					open RecordidCur                                                         
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
					while(@@Fetch_Status=0)                              
					begin                              

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...					
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +',@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
					close RecordidCur 
					deallocate RecordidCur                                                               
				Fetch Next from ListdateCur into @ListdateVal                              
			end                                            

		close ListdateCur  
		deallocate ListdateCur 

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
		dptwo, TicketNumber_PK, t.zipcode  , courtdate, '''+@user+''' as Abb , promotemplate                                     
		into #temp2
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by T.zipcode 
		 
		select @recCount = count(distinct recordid) from #temp2 

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
		select @subject =  convert(varchar(20), @lettercount) + '' ''  +  '''+ @lettername +' Letters Printed''
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
		exec usp_mailer_send_Email @subject, @body

		update #temp2 set letterid = convert(Varchar,@lettercount) + '' Letters sent'' where recordid = 1

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs

		'
	END

else
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
		set @sqlquery2 = @sqlquery2 + '
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, LastName,address,city, state,FineAmount,
		violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode , courtdate, '''+@user+''' as Abb,  promotemplate                                         
		into #temp3 
		from #temp1 
		order by zipcode 
		select @recCount = count(distinct recordid) from #temp3
		'
	END

-- IF PRINTING SINGLE SIDED LETTERS....
if @language = 0
	begin
		-- IF ONLY PREVIEWING THE LETTER....
		-- OUT PUT THE DATA FOR REPORT FILE...
		if @printtype = 0
			select @sqlquery3 = @sqlquery3 + ' 
			select t1.*, 0 as language, convert(varchar(15), t1.recordid) + ''0'' as ChkGroup from #temp3 t1 order by t1.zipcode'

		-- IF SENDING LETTERS TO BATCH...	
		-- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..	
		if @printtype = 1
			select @sqlquery3 = @sqlquery3 + ' 
			select t2.*, 0 as language, convert(varchar(15), t2.recordid) + ''0'' as ChkGroup from #temp2 t2 
			order by [zipcode]
			
			select isnull(@batchIDs_filname,'''') as batchid
			'
	end

-- FOR DOUBLE SIDED PRINTING....
else
	begin

		-- IF ONLY PREVIEWING THE LETTER....
		if @printtype = 0
			BEGIN
				select @sqlquery3 = @sqlquery3 + ' 
				declare @table4 TABLE  (
					[letterId] [varchar] (13)  ,
					[recordid] [int] ,
					[FirstName] [varchar] (20)  ,
					[LastName] [varchar] (20)  ,
					[address] [varchar] (100) ,
					[city] [varchar] (50) ,
					[state] [varchar] (2)  ,
					[FineAmount] [money]  ,
					[violationdescription] [varchar] (200) ,
					[CourtName] [varchar] (50) ,
					[dpc] [varchar] (10)  ,
					[dptwo] [varchar] (10)  ,
					[TicketNumber_PK] [varchar] (20)  ,
					[zipcode] [varchar] (15) ,
					[language] [int],
					[courtdate] [datetime],
					[Abb] [varchar] (10),
					--Yasir Kamal 7226 01/18/2010 datatype changed from int to varchar
					[promotemplate] [varchar] (100)
				)

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

				select letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb, promotemplate from #temp3 	

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb, promotemplate)		

				select letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb, promotemplate from #temp3 ;	


				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 order by zipcode, language ;
				'
			END

		ELSE if @printtype = 1 
			begin	

				-- IF SENDING LETTERS TO BATCH...	
				-- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..	
				select @sqlquery3 = @sqlquery3 + ' 
				declare @table4 TABLE  (
					[letterId] [varchar] (13)  ,
					[recordid] [int] ,
					[FirstName] [varchar] (20)  ,
					[LastName] [varchar] (20)  ,
					[address] [varchar] (100) ,
					[city] [varchar] (50) ,
					[state] [varchar] (2)  ,
					[FineAmount] [money]  ,
					[violationdescription] [varchar] (200) ,
					[CourtName] [varchar] (50) ,
					[dpc] [varchar] (10)  ,
					[dptwo] [varchar] (10)  ,
					[TicketNumber_PK] [varchar] (20)  ,
					[zipcode] [varchar] (15) ,
					[language] [int], 
					[courtdate] [datetime],
					[Abb] [varchar] (10),
					--Yasir Kamal 7226 01/18/2010 datatype changed from int to varchar
					[promotemplate] [varchar] (100)
				)

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

				select letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb,promotemplate from #temp2 	

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)		

				select letterid, recordid, firstname, lastname, address, city, state, fineamount,
						violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb,promotemplate from #temp2 ;	

	                        
				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 
				order by [zipcode], [language]

				select isnull(@batchIDs_filname,'''') as batchid 
				'
		end
	end
                                    
-- print @sqlquery + @sqlquery2 + @sqlquery3

-- EXECUTING THE DYNAMIC SQL ....
exec  ( @sqlquery + @sqlquery2 + @sqlquery3 )


/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:27:51 PM
 ************************************************************/

USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_ReportSetting_Get_ReportAttribute]    Script Date: 09/27/2010 12:27:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to get the list of report attributes.
*					  
* Example			: USP_ReportSetting_Get_ReportAttribute 
*
*/

CREATE PROCEDURE [dbo].[USP_ReportSetting_Get_ReportAttribute]
AS
	SELECT AttributeID,
	       AttributeName,
	       ShortName,
	       DESCRIPTION,
	       AttributeType
	FROM   ReportAttribute
GO

GRANT EXECUTE ON [dbo].[USP_ReportSetting_Get_ReportAttribute] TO dbr_webuser
GO

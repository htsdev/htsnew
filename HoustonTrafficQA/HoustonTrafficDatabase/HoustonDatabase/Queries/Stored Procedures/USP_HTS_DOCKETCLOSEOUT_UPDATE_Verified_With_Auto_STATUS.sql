﻿/************************************************************
 * Alter By : Noufil Khan 
 * Alter Date: 9/18/2010 3:06:36 PM
 * Business Logic : This Procedure is used to set status of violations to AUTO STATUS
 * Parameters :
 *				@TicketsViolationID : Ticket Violation Id
				@empId : Employee id who is updating the record. 
 ************************************************************/
ALTER PROCEDURE [dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS]
	@TicketsViolationID INT,
	@empId INT = 3992 -- Noufil 8268 09/17/2010 Emp Parameter added
AS
BEGIN
    UPDATE tblTicketsViolations
    SET    courtDateMain = CourtDate,
           CourtNumberMain = CourtNumber,
           CourtViolationStatusIDMain = (
               CASE CourtViolationStatusID
                    WHEN 146 THEN 105
                    ELSE CourtViolationStatusID
               END
           ),
           -- Noufil 8268 09/17/2010 Update EmpId
           VEmployeeID = @empId
    WHERE  TicketsViolationID = @TicketsViolationID
END        
        

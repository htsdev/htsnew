/*
* Created By: Haris Ahmed
* Task : 10003
* Date: 01/20/2012
* 
* Business logic: This procedure is used to get Fid for No Bad Address flag against Ticket ID
* 
* Input Parameters:
*			@ticketid = Ticket ID of record
*/


-- [dbo].[usp_htp_Get_FidForNoBadAddressFlagAgainstTicketId] 8790 

CREATE PROCEDURE [dbo].[usp_htp_Get_FidForNoBadAddressFlagAgainstTicketId]
	@ticketid INT
AS
BEGIN

	SELECT Fid FROM tblticketsflag WHERE TicketID_PK = @ticketid AND FlagID = 45
     
END
GO

GRANT EXECUTE ON usp_htp_Get_FidForNoBadAddressFlagAgainstTicketId TO dbr_webuser
GO



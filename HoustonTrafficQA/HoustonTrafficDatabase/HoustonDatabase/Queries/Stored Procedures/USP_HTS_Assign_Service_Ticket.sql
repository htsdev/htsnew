SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Assign_Service_Ticket]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Assign_Service_Ticket]
GO

      
Create  Procedure [dbo].[USP_HTS_Assign_Service_Ticket]       
(      
@Ticketid int,      
@EmpId int ,  
@Fid int     
)      
as      
      
Update tblticketsflag set AssignedTo = @EmpId where Ticketid_pk = @Ticketid and Flagid = 23 and FID = @Fid  
--Recdate = ( Select Max(RecDate) from tblticketsflag where Ticketid_pk = @Ticketid and Flagid = 23 )      


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


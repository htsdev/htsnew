SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_GetAttachment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_GetAttachment]
GO



CREATE procedure [dbo].[usp_Bug_GetAttachment]   
@Bugid as int  
  
as  
  
select
	a.attachmentid, 
	a.attachfilename,  
    a.description,  
    convert(varchar(12),a.uploadeddate,101) as uploadeddate,  
    u.username   
from tbl_bug_attachment a inner join   
       
   tblusers u on   
   a.employeeid=u.employeeid  
where   
 a.bug_id=@Bugid  
    
   
  





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


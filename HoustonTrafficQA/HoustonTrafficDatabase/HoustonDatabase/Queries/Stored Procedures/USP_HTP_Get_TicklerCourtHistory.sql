
/*
* Business Logic:	This stored procedure is used to get tickler information for cases.
* List of parameter:
* @ticketid: Ticket ID of the case.
*/

ALTER PROCEDURE [dbo].[USP_HTP_Get_TicklerCourtHistory] --209808
	@tickeid INT
AS
	SET NOCOUNT ON;
	SELECT DISTINCT v.casenumassignedbycourt
	      ,te.ticklerevent
	      ,te.updatedate	      
	FROM   loaderfilesarchive.dbo.tblTicklerExtract AS te WITH(NOLOCK)
	       INNER JOIN dbo.tblTicketsViolations AS v WITH(NOLOCK)
	            ON  v.casenumassignedbycourt = te.CauseNumber2 --REPLACE(te.CauseNumber ,' ' ,'') //Sabir Khan 6091 06/30/2009 fixed performance issue...
	       --INNER JOIN tblticketsarchive ta  --Sabir Khan 9105 03/31/2011 No need to check non client data base for tickler history.
	       --     ON  ta.RecordID = v.recordid
	--Faique Ali 11175 06/13/2013 adding HMC-W for tickler History
	WHERE  v.courtid IN (3001 ,3002 ,3003,3075)
	       AND LEN(ISNULL(v.casenumassignedbycourt ,''))>0
	       --AND DATEDIFF(DAY ,ta.RecLoadDate ,te.UpdateDate)>= 0
	       AND v.TicketID_PK = @tickeid

	ORDER BY
		--Yasir Kamal 6086 07/29/2009 by default sort by event date.
	       te.UpdateDate DESC


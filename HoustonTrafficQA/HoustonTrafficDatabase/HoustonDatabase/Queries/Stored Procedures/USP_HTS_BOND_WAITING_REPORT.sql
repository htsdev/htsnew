﻿
/******   
Alter by:  Rab Nawaz Khan  
  
Business Logic : this procedure is used to Insert the information in document tracking batch tickets table and
				 also insert the entry in case history for the related cases. it also gets the required information 
				 that will be displayed under bond summary report.			  
  
List of Parameters:   
 @fromDate : Start Date
 @toDate : End Date
 @showAllFlag : 0 or 1
  
List of Columns:    
 ticketid :
 causeno :
 ticketno : 
 lastname : 
 firstname :
 dlnumber : 
 dob : 
 courtdate : 
 midnum: 
 Time : 
 status :  
 bonddate : 
 ticketsviolationid :
 docid : document id
 bondflag :
   
******/

ALTER PROCEDURE [dbo].[USP_HTS_BOND_WAITING_REPORT]        
              
@fromDate datetime,                      
@toDate datetime,                      
@showAllFlag bit                      

as                      
                      
begin                            
                        
	declare @sqlQuery varchar(8000)                        
                        
	set @fromDate = @fromDate+'12:00:00 AM'                      
	set @toDate = @toDate ++'11:59:59:998 PM'                      
                      
	set @sqlQuery = '                      
                      
	select  distinct                                        
                              
	t.ticketid_pk as ticketid,                                         
	tv.casenumassignedbycourt as causeno,                                          
	tV.REFCASENUMBER AS TICKETNO,                                   
	upper (t.lastname) as lastname,                                          
	upper(t.firstname) as firstname,                  
	Convert(varchar(12),tvl.recdate,101) as BondDate ,                          
    (case                      
	when t.bondflag=1 then ''B;''                      
	else ''''                      
	end)  +     
    isnull( ( select top 1 case when isnull(sb.scanbookid,0) =0  then ''S;'' else '''' end       
    from tblscanbook sb where sb.ticketid = t.ticketid_pk and sb.doctypeid = 8 and isnull(sb.isdeleted,0)=0    
    ) ,''S;'')    
	+ (CASE WHEN TF.FlagID IS NULL THEN '''' ELSE ''N;'' END )  as BondFlag  ,                                          
	t.dlnumber,                                          
    t.dob as dob,                                   
	t.midnum,                                     
    isnull(datediff(day,tvl.recdate,getdate()),0) as Time,                                      
    ( case                                       
	when   tcv.shortdescription=''BOND'' then ''BND''                                      
	else tcv.shortdescription                                      
    end )as status, tv.CourtDate,                                  
	(case when isnull((select top 1 BondWaitingdate from tblticketsviolations where ticketid_pk =tv.ticketid_pk and ViolationNumber_pk =tv.ViolationNumber_pk),0) <> 0   
	then (select top 1 BondWaitingdate from tblticketsviolations where ticketid_pk =tv.ticketid_pk and ViolationNumber_pk =tv.ViolationNumber_pk)  
	else tvl.recdate end) as WaitingDate,                                     
    tv.ticketsviolationid,            
	--ozair 4073 05/26/2008 incorporating new structure with old one
	(isnull((SELECT COUNT(ticketid)            
	FROM Tblwaitinglogs w inner join tblbatchsummary bs on          
    bs.batchid=w.batchid            
	where t.ticketid_pk = w.ticketid and bs.docpicflag=1           
	GROUP BY  ticketid),0)+isnull((select count(bt.ticketid_fk)
	from tbl_htp_documenttracking_batchtickets bt
	where t.ticketid_pk = bt.ticketid_fk
	and bt.VerifiedDate is not null
	group by bt.ticketid_fk),0))  as doccount ,
	(case when isnull((select top 1 isnull(b.docpicflag,0)    
	from tblwaitinglogs w          
	inner join tblbatchsummary b on           
	w.batchid=b.batchid      	
	where w.ticketid=tv.ticketid_pk ),0) + isnull((select count(bt.ticketid_fk)
	from tbl_htp_documenttracking_batchtickets bt
	where t.ticketid_pk = bt.ticketid_fk
	and bt.VerifiedDate is not null
	group by bt.ticketid_fk ),0) > 0 then 1 else 0 end) as showscan
	--end ozair 4073                                                            
	from tbltickets t inner join                                           
    tblticketsviolations tv on                                           
    t.ticketid_pk=tv.ticketid_pk inner join                                           
    tblcourtviolationstatus tcv on                                           
    tv.courtviolationstatusidmain=tcv.courtviolationstatusid LEFT OUTER JOIN            
    tblticketsviolationlog tvl on                             
    tv.ticketsviolationid=tvl.ticketviolationid                            
    and tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid                           
	and newverifiedstatus <> oldverifiedstatus     
	)       
	LEFT OUTER join tblTicketsFlag TF on    
	tv.TICKETID_PK = TF.TICKETID_PK     
	AND TF.FlagID = 15        
	'       
    
	set @sqlquery = @sqlquery +      
	' where                                          
    t.activeflag=1  and tv.courtid in (3001,3002,3003, 3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Bond Waiting Report. . .                                
	and tv.courtdatemain is not null AND tV.courtviolationstatusidmain = 202'                       
         
	if @showAllFlag = 0                      
	begin                      
		set @sqlquery = @sqlquery + '       
		AND tvl.recdate BETWEEN '''+ Convert(varchar,@fromDate,101)+ ' 00:00:00:000''' +' AND '''+ Convert(varchar,@ToDate,101)+ ' 23:59:59:998'''                         
	end     
    
	set @sqlQuery = @sqlQuery + ' ORDER BY [time] desc, [lastname]  ,[firstname], ticketid'                      
                      
	--print @sqlQuery                      
	exec (@sqlQuery)                      
end   
  
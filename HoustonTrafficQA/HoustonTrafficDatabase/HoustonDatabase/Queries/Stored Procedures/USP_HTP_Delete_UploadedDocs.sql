﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Created by  : Fahad Muhammad Qureshi.
Created Date: 13/01/2009
Task ID     : 5376

Business Logic : this procedure is delete the uploaded file or documents. 

List of Parameters:	
			@MenuID : Menu id 
			@DocID	:HelpDocumentId

******/   

 
CREATE PROCEDURE [dbo].[USP_HTP_Delete_UploadedDocs]
@MenuID INT,
@DocID  INT
as 
 
UPDATE tblHelpDocument
SET
	IsDeleted = 1
		
WHERE

	SubMenuID_FK=@MenuID
AND
	HelpDocID_PK=@DocID
	
	SELECT @@ROWCOUNT
	
	GO
	GRANT EXECUTE ON [dbo].[USP_HTP_Delete_UploadedDocs] TO dbr_webuser
	

 
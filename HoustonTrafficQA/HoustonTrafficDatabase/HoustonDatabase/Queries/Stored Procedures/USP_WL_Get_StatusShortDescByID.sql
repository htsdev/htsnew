
CREATE procedure dbo.USP_WL_Get_StatusShortDescByID

@StatusID int

as

select dbo.Fn_GetViolationStatusShortDescription(@statusid)

go

grant exec on dbo.USP_WL_Get_StatusShortDescByID to dbr_webuser
go
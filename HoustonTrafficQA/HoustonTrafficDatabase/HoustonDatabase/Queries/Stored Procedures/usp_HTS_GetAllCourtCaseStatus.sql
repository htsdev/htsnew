/***********

Alter By : Zeeshan Ahmed 
Business Login : This Procedure is used to get statuses by court id and case type.

List of  Parameters :


	@courtid : Court Location
	@IsDisposition : Used for Case Disposition Status   
	@CaseType : Case Type 1 : Traffic ,2: Criminal , 3: Civil

List Of Columns
	id			: Status ID 
	description : Description
	sortorder   : Sort Order
************/
Alter PROCEDURE  [dbo].[usp_HTS_GetAllCourtCaseStatus]  
	(
	@courtid int = 0,
	@IsDisposition bit = 0 ,          
	@CaseType int = 1
	)
    
AS    
    
    
    
declare @temp table (id int, description varchar(100), sortorder int)
declare @courtcategory int

select @courtcategory = courtcategorynum from tblcourts where courtid = @courtid

-- FOR HMC COURTS......
if @courtcategory = 1
	INSERT into @temp (id, description, sortorder)
	select courtviolationstatusid, description, sortorder 
	from tblcourtviolationstatus 
	-- tahir 4201 06/06/2008
	-- removed WAIT, PRE & OTHER case statuses for HMC courts...
	--where courtviolationstatusid in (3,135,101,103,26,196,104,161,27,105,123,147,80)
	where courtviolationstatusid in (3,135,103,26,196,161,27,105,123,80,236)
	-- 4201
	and statustype =1
	
-- FOR HCJP COURTS.......
else if @courtcategory = 2
	INSERT into @temp (id, description, sortorder)
	select courtviolationstatusid, description, sortorder 
	from tblcourtviolationstatus 
	where courtviolationstatusid in (3,135,101,103,26,196,104,161,27,105,123,147,80,32,139,236,256,257,258,259)
	and statustype =1	
	

--ozair 4442 07/22/2008
-- FOR ALR Courts (HOUSTON, FORTE BEND, CONROE)......
--Nasir 5310 12/23/2008 add IsALRProcess function to Check court is ALR process on
--else if (@courtid = 3047 or @courtid = 3048 or @courtid = 3070 OR @courtid= 3079)
else if dbo.FN_HTP_Check_IsALRProcess(@courtid) = 1
	INSERT into @temp (id, description, sortorder)
	select courtviolationstatusid, description, sortorder 
	from tblcourtviolationstatus 
	where courtviolationstatusid in (104,237)
	and statustype =1
	
--For Harris County Civil Court
else if @courtid = 3071 
Begin

INSERT into @temp (id, description, sortorder)
	select courtviolationstatusid, description, sortorder 
	from tblcourtviolationstatus 
	where courtviolationstatusid in (3,135,101,103,26,196,104,161,27,105,123,147,80)
	and statustype =1
	Or CategoryID = 60

End

-- Noufil 4948 10/13/2008 Show status for court id 3077
else if @courtid = 3077   
Begin
INSERT into @temp (id, description, sortorder)  
 select courtviolationstatusid, description, sortorder   
 from tblcourtviolationstatus   
 --Sabir Khan 5011 10/22/2008 include "Waiting" (104) as a status...  
 where courtviolationstatusid in (101,103,104,26,147)
 
  
End


-- For Criminal Cases
-- Zeeshan  4369 07/24/2008
else if ( Select isCriminalCourt from tblcourts where courtid = @courtid) = 1
BEGIN

INSERT into @temp (id, description, sortorder)
	select courtviolationstatusid, description, sortorder 
	from tblcourtviolationstatus 
	where courtviolationstatusid in (3,101,103,26,196,104,161,27,105,123,147,80,236) AND CategoryID <>12 -- Noufil 6489 10/30/2009 Dont show bond for criminal cases
	and statustype =1	
end



-- FOR ALL OTHER COURTS.....
else
	INSERT into @temp (id, description, sortorder)
	select courtviolationstatusid, description, sortorder 
	from tblcourtviolationstatus 
	where courtviolationstatusid in (3,135,101,103,26,196,104,161,27,105,123,147,80)
	and statustype =1	


-- FOR CASE DISPOSITION PAGE.....
if @IsDisposition = 1 and not exists (
		select id from @temp where id = 80
		)
		INSERT into @temp (id, description, sortorder)
		select courtviolationstatusid, description, sortorder 
		from tblcourtviolationstatus 
		where courtviolationstatusid in (80)
		and statustype =1		


--Afaq 8057 07/23/2010 Remove no hire status.
select * from @temp WHERE id != 236 order by sortorder
--ozair 5171 11/19/2008
select courtviolationstatusid as description,categoryid as id
from tblcourtviolationstatus
where courtviolationstatusid in (select id from @temp)
order by sortorder
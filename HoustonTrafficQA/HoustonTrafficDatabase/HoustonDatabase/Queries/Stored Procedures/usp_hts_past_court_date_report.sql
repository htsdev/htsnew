-- Noufil 4432 08/05/2008 html serial number added added for validation report

ALTER  Procedure [dbo].[usp_hts_past_court_date_report]  
as  
  
SELECT DISTINCT   
                      tv.TicketID_PK AS ticketid_pk, tv.RefCaseNumber AS ticketnumber, t.Firstname AS fname, t.Lastname AS lname, dbo.fn_DateFormat(tv.CourtDateMain,   
                      24, '/', ':', 1) AS courtdatemain, convert(int,tv.CourtNumbermain) AS courtnumber, tcvs.ShortDescription AS status, c.ShortName AS shortname,   
                      tv.CourtDateMain AS CourtDate
						
  
FROM         tblTickets AS t INNER JOIN  
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN  
                      tblCourtViolationStatus AS tcvs ON tv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID INNER JOIN  
                      tblCourts AS c ON tv.CourtID = c.Courtid  
WHERE     (t.Activeflag = 1) 
AND 

--Modified By Zeeshan Ahmed on 12/31/2007
--Don't Display Cases having bond status 
(tv.CourtViolationStatusIDmain Not IN(80,105,104,201,202,123,135)) 

AND (tv.CourtDateMain < GETDATE() - 3) AND (tv.CourtDateMain >= '01/01/2007') AND   
                      (DATEPART(dw, tv.CourtDateMain) NOT IN (7, 1))  
ORDER BY tv.ticketid_pk,tv.courtdatemain 






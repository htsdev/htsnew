/*
* Business logic: This stored procedure is used to get user Info from tblusers table.
* 
*/



alter procedure [dbo].[USP_Mailer_Get_UserInfo]-- fahim,iqbal     
 (    
 @LoginId varchar(25),    
 @Password varchar(25)    
 )    
    
    
as    
    
select employeeid as EmpId,    
ltrim(rtrim( UserName)) as UserName,    
 [Password],     
 LastName,    
 FirstName,    
 Abbreviation,    
 AccessType,
 --Yasir Kamal 7048 11/24/2009 get user location.
 ISNULL(UserLocation,1) AS UserLocation    
from  tblusers    
where username = @LoginId    
and [password] = @password  
and status = 1
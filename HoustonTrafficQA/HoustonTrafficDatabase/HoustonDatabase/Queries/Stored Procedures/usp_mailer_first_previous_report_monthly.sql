/****** 

Business Logic:	The procedure is used by LMS application to get the returns on the selected mailer type.

				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS Reports
	@sDate:			Starting date for search criteria.
	@eDate:			Ending date for search criteria.
	@SearchType:	Flag that identifies if searching by upload date or by mailer date. 0= upload date, 1 = mailer date
	@IsFirst :   

List of Columns:	
	mdate:			Maier Date or upload date as selected in the search criteria.
	lettercount:	Number of letters printed
	nonclient:		Number of people not hired/contacted us
	client:			Number of people hired us.
	revenue:		Revenue generated from the hired people.
	expense:		Expenses occured on mailing.
	profit:			Profit earned by the maiing.
	NPM:			The profit margin tells you how much profit a company makes for every $1 in generates in revenue.  In our case, the net profit margin tells us how much revenue is generated per dollar of postage spent. NPM = Profits / Revenue
	ROP:			Return on postage tells us how much profit is earned per $1 of postage spent.  ROP = Profit / Expense
	QPMP:			Quotes per Mail Piece tells us how many quotes are given per mail piece sent.  QPMP = Quote / Mailer Count
	HPMP:			Hires per Mail Piece tells us how many clients hire per mail piece sent.  HPMP = Hire / Mailer Count
	PPMP:			Profit per Mail Piece �PPMP� tells us how much money is earned per mail piece sent.  PPMP = Profits / Mailer Count
	
******/

-- usp_mailer_first_previous_report_monthly 6, 18, '6/1/2007', '7/7/2007', 1,1  
alter procedure [dbo].[usp_mailer_first_previous_report_monthly]  
 (  
--declare  
 @CatNum  int,                  
 @lettertype  int,                  
 @sDate  datetime,                  
 @eDate   datetime,  
 @SearchType tinyint,    
 @IsFirst tinyint     
 )  
  
as  

declare @CourtCatNum int  
--select @catnum = 1, @lettertype = 9, @sdate  = '6/5/07', @edate = '6/5/07', @searchtype = 1  
  
set nocount on   
--Yasir Kamal 7218 01/13/2010 lms structure changed.    
declare @DBid int  
-- 1 = Traffic Tickets  
-- 2 = Dallas Traffic Tickets  
  
select @dbid =	 CASE ISNULL(tl.courtcategory,0) WHEN 26 THEN 5 ELSE ISNULL(tcc.LocationId_FK ,0) END
from tblletter tl INNER JOIN tblCourtCategories tcc ON tl.courtcategory = tcc.CourtCategorynum where letterid_pk = @lettertype

SELECT @dbid= ISNULL(@dbid,0)
  
declare @tblcourts table (courtid int)  
  
----------------------------------------------------------------------------------------------------      
--  DALLAS DATABASE....      
----------------------------------------------------------------------------------------------------      
if @dbid = 2
	BEGIN
 	 if @catnum < 1000      
		begin  
			insert into @tblcourts       
			select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = @catnum      
			set @CourtCatNum=@catnum  
		end   
	 else      
		begin  
			insert into @tblcourts select @catnum      
			select @CourtCatNum=courtcategorynum from dallastraffictickets.dbo.tblcourts where courtid=@catnum  
		end  
	END      
----------------------------------------------------------------------------------------------------      
--  TRAFFIC TICKETS DATABASE....      
----------------------------------------------------------------------------------------------------      
ELSE
	BEGIN
	 if @catnum < 1000      
		begin  
			insert into @tblcourts       
			select courtid from dbo.tblcourts where courtcategorynum = @catnum      
			set @CourtCatNum=@catnum  
		end    
	else      
		begin  
			insert into @tblcourts select @catnum      
			select @CourtCatNum=courtcategorynum from dbo.tblcourts where courtid=@catnum  
		end      
	END

----------------------------------------------------------------------------------------------------
--7218 end.    
  
-- GETTING LETTER HISTORY INTO TEMP TABLE AS PER SELECTION CRITERIA.....  
declare @letters table (  
 noteid int,   
 mdate datetime  
 )  
  
--------------    
 select recordid,count(recordid) as count    
 into #tempcount    
 from tblletternotes l inner join tblbatchletter b     
  on l.batchid_fk=b.batchid     
 group by recordid,b.courtid    
 having b.courtid=@CourtCatNum     
--------------  
if @searchtype = 0       
 BEGIN     
  if @IsFirst=1    
 begin    
   insert into @letters      
   select noteid,  convert(varchar(10), listdate, 101)      
   from tblletternotes n, @tblcourts c      
   where n.courtid = c.courtid      
   and n.lettertype = @lettertype      
   and datediff(day, listdate, @sdate)<= 0      
   and datediff(day, listdate, @edate)>= 0     
   and recordid in (select recordid from #tempcount where count=1)       
 end    
  else    
 begin    
   insert into @letters      
   select noteid,  convert(varchar(10), listdate, 101)      
   from tblletternotes n, @tblcourts c      
   where n.courtid = c.courtid      
   and n.lettertype = @lettertype      
   and datediff(day, listdate, @sdate)<= 0      
   and datediff(day, listdate, @edate)>= 0     
   and recordid in (select recordid from #tempcount where count>1)     
 end    
 END      
else      
 BEGIN      
  if @IsFirst=1    
 begin    
   insert into @letters      
   select noteid,  convert(varchar(10), recordloaddate, 101)      
   from tblletternotes n, @tblcourts c      
   where n.courtid = c.courtid      
   and n.lettertype = @lettertype      
   and datediff(day, recordloaddate, @sdate)<= 0      
   and datediff(day, recordloaddate, @edate)>= 0      
   and recordid in (select recordid from #tempcount where count=1)       
 end    
  else    
 begin    
   insert into @letters      
   select noteid,  convert(varchar(10), recordloaddate, 101)      
   from tblletternotes n, @tblcourts c      
   where n.courtid = c.courtid      
   and n.lettertype = @lettertype      
   and datediff(day, recordloaddate, @sdate)<= 0      
   and datediff(day, recordloaddate, @edate)>= 0     
   and recordid in (select recordid from #tempcount where count>1)     
 end     
 END      
    
-------------    
drop table #tempcount    
------------- 
  
-- GETTING COST FOR EACH MAILER/LIST DATE....  
declare @cost table (  
 mdate datetime,  
 pcost money  
 )  
  
 insert into @cost  
 select l.mdate, sum(n.pcost) from tblletternotes n inner join  @letters l  
 on l.noteid = n.noteid   
 group by l.mdate  
  
  
  
-- GETTING INFORMATION FROM CLIENTS/QUOTES......  
declare @QuoteClient table (  
 ticketid int,   
 mailerid int,  
 activeflag int,  
 hiredate datetime,  
 quotedate datetime,  
 chargeamount money  
 )  
  
if @dbid = 2  
 insert into @QuoteClient  
 select t.ticketid_pk, t.mailerid, t.activeflag,   
   min(p.recdate), t.recdate, isnull(sum(isnull(p.chargeamount,0)),0)  
 from dallastraffictickets.dbo.tbltickets t   
 left outer join    
  dallastraffictickets.dbo.tblticketspayment p   
 on p.ticketid = t.ticketid_pk and p.paymentvoid = 0  
 inner join   
  @letters l  
 on l.noteid = t.mailerid  
 group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate  
else  
 insert into @QuoteClient  
 select t.ticketid_pk, t.mailerid, t.activeflag,   
   min(p.recdate), t.recdate, isnull(sum(isnull(p.chargeamount,0)),0)  
 from tbltickets t   
 left outer join    
  tblticketspayment p   
 on p.ticketid = t.ticketid_pk and p.paymentvoid = 0  
 inner join   
  @letters l  
 on l.noteid = t.mailerid  
 group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate  
  
-- SUMMARIZING THE INFORMATION....  
declare @temp2 table  
  (  
  mdate datetime,  
  letterCount int,  
  NonClient int,  
  Client  int,  
  Quote  int,  
  Revenue  money,  
  expense money,  
  weekno int  
  )  
  
INSERT into @temp2 (mdate, lettercount, client, Quote, revenue)  
select l.mdate,    
 count(distinct l.noteid),   
 sum(case q.activeflag when 1 then 1 end),   
 sum(case q.activeflag when 0 then 1 end),    
 sum(q.chargeamount)  
from @letters l  
left outer join  
 @QuoteClient q  
on  l.noteid = q.mailerid  
group  by l.mdate  
  
update t  
set t.expense = p.pcost,  
 nonclient = lettercount - (isnull(quote,0)+isnull(client,0))  
from @temp2 t, @cost p  
where datediff(day, t.mdate, p.mdate ) = 0  
  
select year(mdate) as ydate,   
 MONTH(mdate) as mdate,  
 isnull(sum(nonclient),0) as nonclient,   
 isnull(sum(client),0) as client,   
 isnull(sum(quote),0) as quote,  
 isnull(sum(revenue),0) as revenue,   
 isnull(sum(lettercount),0) as lettercount,   
 isnull(sum(expense),0) as expense,   
 isnull(sum(isnull(revenue,0) - expense),0) as profit  
into #temp  
from @temp2   
group by year(mdate), month(mdate)  
order by year(mdate) desc, month(mdate) desc  
  
select convert(datetime, convert(varchar,mdate)+'/1/'+convert(varchar,ydate),101 ) as mdate,   
 nonclient, client, quote, revenue, lettercount, expense, profit,  
  
 NPM = convert(numeric(10,3),case when isnull(revenue,0) = 0 then 0 else (isnull(revenue,0) - expense)/revenue end) ,  
 ROP = convert(numeric(10,3),case when isnull(expense,0) = 0 then 0 else (isnull(revenue,0) - expense)/expense end),  
 QPMP = convert(numeric(10,3),cast(isnull(quote,0)/cast(lettercount as float) as float)),  
 HPMP = convert(numeric(10,3),cast(isnull(client,0)/cast(lettercount as float) as float)),  
 PPMP = convert(numeric(10,3),(isnull(revenue,0) - expense)/cast(lettercount as float))  
into #temp2  
from #temp  
  
select left( datename( month, mdate ), 3 )+ '-' + right( datepart( year, mdate ), 2 )as mdate,  
 nonclient, client, quote, revenue, lettercount, expense, profit, npm, rop, qpmp, hpmp, ppmp  
from #temp2  
  
  
drop table #temp  
drop table #temp2  



/******************
Alter By: Zeeshan Ahmed


Business Logic: This procedure is used to get information about hmc cases
				Zeeshan 4420 08/08/2008 Add Columns for Email Count and Arraignment Setting Summary Document
				Zeeshan 4679 09/02/2008 Change Default Followup Date to 5 Business Days from the court date
List of Parameters:
-------------------
--Yasir 5423 02/11/2009 cases having follow up date in next 6 business days

(@showAll =1 & @validation=0) will display all cases in arraignment waiting status
(@showAll =0 & @validation=0) will cases having follow up date in today or in past or in next 6 business days.
(@showAll =1 & @validation=1) show those records whose followupdate is today or in the past
 @Ticketids:	Pass comma separated Ticket id's for seleced records

List Of Columns
----------------

DBID :		 Arraignment Setting Summary Document Id Information
SBID :		 Arraignment Setting Summary Document id Information
EmailCount : No of Times Email Sent for Setting Case Status

*****************/
--Sabir Khan 6074 06/23/2009 logic has been removed which was implemented in 6026...

-- usp_hts_HMC_AW_DLQ_Alert '',1,0
ALTER PROCEDURE [dbo].[usp_hts_HMC_AW_DLQ_Alert]
--Yasir 5423 02/26/2009 cases having follow up date in next 6 business days
--@show int=0,

--@pastfollowupdate int=0,
	@Ticketids VARCHAR(MAX) = '',
	@showAll INT = 0,
	@validation BIT = 0,
	@ValidationCategory INT = 0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	DECLARE @bd        INT,
	        @idx       INT,
	        @tempdate  DATETIME,
	        @bdorg     INT
	-- Sabir Khan 6074 06/23/2009 report id has been replaced from 137 to 92...
	SET @bdorg = ABS(
	        (
	            SELECT [dbo].[fn_GetReportValue] ('days', 92)
	        )
	    )
	
	SET @bd = @bdorg
	
	SET @idx = 0
	WHILE @idx <= @bd
	BEGIN
	    --Sabir Khan 5727 03/31/2009 business days setting for saturday and sunday...
	    IF DATEPART(dw, DATEADD(DAY, @idx, GETDATE())) IN (1, 7)
	        SET @bd = @bd + 1
	    
	    SET @idx = @idx + 1
	END
	
	SET @tempdate = DATEADD(DAY, @bd, GETDATE())
	
	
	
	
	
	--5423 end                        
	
	SELECT DISTINCT 
	       tv.casenumassignedbycourt AS CauseNo,
	       tv.TicketID_PK AS ticketid_pk,
	       tv.RefCaseNumber AS ticketnumber,
	       t.Firstname AS fname,
	       t.Lastname AS lname,
	       CONVERT(VARCHAR, t.Dob, 101) AS Dob,
	       dbo.fn_DateFormat(tv.CourtDateMain, 24, '/', ':', 1) AS courtdatemain,
	       CONVERT(
	           DATETIME,
	           (
	               dbo.fn_DateFormat(
	                   ISNULL(tx.ArrWaitingFollowUpDate, courtdatemain),
	                   27,
	                   '/',
	                   ':',
	                   1
	               )
	           )
	       ) AS FollowUpDate,
	       --Muhammad Muneer 8445 10/26/2010 changing int to varchar to resolve the issue
	       CONVERT(VARCHAR, tv.CourtNumbermain) AS courtnumber,
	       cvs.ShortDescription AS STATUS,
	       c.shortcourtname AS shortname,
	       (
	           CASE 
	                WHEN ISNULL(
	                         (
	                             SELECT TOP 1 ArrignmentWaitingdate
	                             FROM   tblticketsviolations
	                             WHERE  ticketid_pk = tv.ticketid_pk
	                                    AND ViolationNumber_pk = tv.ViolationNumber_pk
	                         ),
	                         0
	                     ) <> 0 THEN (
	                         SELECT TOP 1 dbo.fn_DateFormat(ArrignmentWaitingdate, 24, '/', ':', 1)
	                         FROM   tblticketsviolations
	                         WHERE  ticketid_pk = tv.ticketid_pk
	                                AND ViolationNumber_pk = tv.ViolationNumber_pk
	                     )
	                ELSE dbo.fn_DateFormat(tvl.recdate, 24, '/', ':', 1)
	           END
	       ) AS arrwaitdate,
	       ISNULL(
	           (
	               SELECT MAX(recordid) AS recordid
	               FROM   tblhtsnotes
	                      INNER JOIN tblusers
	                           ON  tblhtsnotes.empid = tblusers.employeeid
	               WHERE  ticketid_fk = tv.TicketID_PK
	                      AND letterid_fk = 9
	           ),
	           0
	       ) AS recordid,
	       tv.CourtDateMain AS CourtDateSort,
	       ISNULL(sbd.DocumentBatchID, 0) AS DBID,
	       ISNULL(sbd.ScanBatchID_FK, 0) AS SBID,
	       ISNULL(t.ArrSetEmailCount, 0) AS EmailCount,
	       --Zeeshan Ahmed 4679 09/02/2008 Change Default Business Days to 5 Days 
	       (
	           dbo.fn_DateFormat(
	               ISNULL(tx.ArrWaitingFollowUpDate, courtdatemain),
	               27,
	               '/',
	               ':',
	               1
	           )
	       ) AS FollowUp
	       --ozair 4879 09/29/2008 included document from old scannig structure
	       ,
	       ISNULL(N.recordid, 0) AS docid
	FROM   tblTickets AS t
	       LEFT OUTER JOIN tblticketsextensions tx
	            ON  t.ticketid_pk = tx.ticketid_pk
	       INNER JOIN tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN tblCourts AS c
	            ON  tv.CourtID = c.Courtid
	       INNER JOIN tblCourtViolationStatus AS cvs
	            ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID
	       LEFT OUTER JOIN tblticketsviolationlog tvl
	            ON  tv.ticketsviolationid = tvl.ticketviolationid
	            AND tvl.recdate = (
	                    SELECT MAX(recdate)
	                    FROM   tblticketsviolationlog
	                    WHERE  ticketviolationid = tvl.ticketviolationid
	                )
	       LEFT OUTER JOIN tbl_htp_documenttracking_batchtickets bt
	            ON  bt.ticketid_fk = tv.ticketid_pk
	            AND bt.verifiedDate = (
	                    SELECT MAX(verifiedDate)
	                    FROM   tbl_htp_documenttracking_batchtickets
	                    WHERE  ticketid_fk = tv.TicketID_PK
	                )
	       LEFT OUTER JOIN tbl_htp_documenttracking_scanbatch_detail sbd
	            ON  bt.batchid_fk = sbd.documentbatchid
	            AND sbd.ScanBatchID_FK = (
	                    SELECT MAX(ScanBatchID_FK)
	                    FROM   tbl_htp_documenttracking_scanbatch_detail
	                    WHERE  documentbatchid = bt.batchid_fk
	                )
	                --ozair 4879 09/29/2008 included document from old scannig structure
	                
	       LEFT OUTER JOIN tblHTSNotes n
	            ON  n.TicketID_FK = tv.ticketid_pk
	            AND N.LetterID_FK = 10
	            AND n.printdate = (
	                    SELECT MAX(printdate)
	                    FROM   tblhtsnotes
	                    WHERE  ticketid_fk = tv.TicketID_PK
	                )
	WHERE  --Ozair 7791 07/24/2010  where clause optimized
	       (
	           --Sabir Khan 5727 03/30/2009 court date should be past logic has been removed.
	           --and   datediff(dd, tv.courtdate, getdate()) >0
	           (
	               (tv.CourtViolationStatusID = 146)
	               OR (tv.CourtViolationStatusID = 186)
	               OR (
	                      tv.courtviolationstatusid IN (SELECT 
	                                                           courtviolationstatusid
	                                                    FROM   
	                                                           tblcourtviolationstatus
	                                                    WHERE  categoryid = 2)
	                  )
	               OR tv.courtviolationstatusid = 201
	           )
	           AND (tv.CourtViolationStatusIDmain = 201)
	           AND (c.Courtid IN (3001, 3002, 3003,3075))--Fahad 11369 08/29/2013 Added HMC-W for this report
	           AND (t.Activeflag = 1)
	       )
	       
	       --Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
	       --Sabir Khan 5727 03/31/2009 excluding today from follow up date...
	       --Yasir Kamal 5734 03/31/2009 default records logic changed.
	       AND (
	               (@showAll = 1 OR @ValidationCategory = 2) -- Saeed 7791 07/10/2010 display all records if showAll=1 or validation category is 'Report'
	               OR (
	                      (@showAll = 0 OR @ValidationCategory = 1) -- Saeed 7791 07/10/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	                      AND @validation = 0
	                      AND DATEDIFF(
	                              DAY,
	                              GETDATE(),
	                              ISNULL(tx.ArrWaitingFollowUpDate, courtdatemain)
	                          ) <= @bd
	                  )
	           )
	           --5734 end
	       AND (
	               (@validation = 0 OR @ValidationCategory = 2) -- Saeed 7791 07/10/2010 display all records if @validation=0 or validation category is 'Report'
	               OR (
	                      (@validation = 1 OR @ValidationCategory = 1) -- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	                      AND DATEDIFF(
	                              DAY,
	                              ISNULL(
	                                  tx.ArrWaitingFollowUpDate,
	                                  dbo.fn_getnextbusinessday(courtdatemain, @bdorg)
	                              ),
	                              GETDATE()
	                          ) >= 0
	                  )
	           )
	           --ozair 4929 10/07/2008 exclude case having Not on System Flag set
	       AND t.ticketid_pk NOT IN (SELECT tf.ticketid_pk
	                                 FROM   tblticketsflag tf
	                                 WHERE  tf.flagid = 15)
	           --end ozair 4929
	       AND (
	               @Ticketids = ''
	               OR (
	                      @Ticketids <> ''
	                      AND tv.TicketID_PK IN (SELECT maincat
	                                             FROM   dbo.Sap_String_Param_bigint(@Ticketids))
	                  )
	           )
	ORDER BY
	       FollowUpDate,
	       TicketID_PK

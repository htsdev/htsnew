SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_MissingCauseNumbers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_MissingCauseNumbers]
GO


CREATE procedure [dbo].[USP_HTS_Get_MissingCauseNumbers]

as


declare @temp table ( ticketid_pk int)
insert into @temp
select distinct t.ticketid_pk     
from tblticketsviolations v inner join 
	 tbltickets t on t.ticketid_pk = v.ticketid_pk    inner join 
	 tblcourtviolationstatus c on c.courtviolationstatusid = v.courtviolationstatusidmain
where len(isnull(v.casenumassignedbycourt,'')) = 0    
and v.courtid in (3001,3002,3003)    
and t.activeflag = 1    
and t.recdate  >= '1/1/2006'    
and isnull(v.courtdatemain, '1/1/1900') > getdate()   
and  c.categoryid <> 50

select isnull(v.casenumassignedbycourt,'') as [Cause Number],
		v.refcasenumber as [Ticket Number],
		d.description as [Violation Description],
		t.lastname + ', '+ firstname as [Name],
		convert(varchar(10), t.dob,101) as [Date Of Birth],
		isnull(t.midnum, 'N/A') as [MID Number],
		convert(varchar(10),isnull(v.courtdatemain,'1/1/1900'),101) as [Verified Court Date]
from	tbltickets t , tblticketsviolations v , tblviolations d, @temp a
where	t.ticketid_pk = v.ticketid_pk
and		v.violationnumber_pk = d.violationnumber_pk
and		t.ticketid_pk = a.ticketid_pk
order by t.ticketid_pk desc

		

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****** 
Create by:  Muhammad Kazim
Business Logic : this procedure is used to get the uploaded files information of particular file.
 
List of Parameters:     
      @id : represnts id of the file 
 
List of Columns: 
    
      Documentname:	this field will display the documentname of uploaded file.
      DocumentPath: this field will display the DocumentPath of uploaded file.
 
******/

Create procedure [dbo].[USP_HTP_Get_FilePathById]
@id int
as
select documentpath,documentname from Courtfiles where id=@id
go


grant execute on dbo.[USP_HTP_Get_FilePathById] to [dbr_webuser]
go

 
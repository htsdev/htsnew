﻿/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to get the Scan batch detail id by Scan Batch ID.

List of Parameters:	
	@ScanBatchID_FK : scan batch id for which ScanBatchDetailID will be fetched

List of Columns: 	
	ScanBatchDetailID : detail id

******/

Create Procedure dbo.usp_HTP_Get_DocumentTracking_ScanBatchDetailID

@ScanBatchID_FK int 

as

select ScanBatchDetailID 
from tbl_htp_documenttracking_scanbatch_detail
where scanbatchid_fk=@ScanBatchID_FK

go

grant exec on dbo.usp_HTP_Get_DocumentTracking_ScanBatchDetailID to dbr_webuser
go

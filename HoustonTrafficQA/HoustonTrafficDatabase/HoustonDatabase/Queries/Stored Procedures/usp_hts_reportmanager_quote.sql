ALTER procedure [dbo].[usp_hts_reportmanager_quote]    
    
    
@fromdate datetime,    
@todate datetime    
    
    
as    
    
SELECT     (Select top 1 refcasenumber from tblticketsviolations where ticketId_pk = t1.ticketId_pk) as 'Ticket Number', t1.Firstname as 'First Name', t1.Lastname as 'Last Name',  
  (Select top 1 courtdatemain from tblticketsviolations where ticketId_pk = t1.ticketId_pk) as 'Main Court Date', t2.Description  as 'Main Case Status'  
FROM         tblTickets t1 INNER JOIN    
				tblticketsviolations TV on t1.ticketId_Pk = TV.ticketId_pk INNER JOIN    
                      tblDateType t2 ON TV.CourtViolationStatusIDmain = t2.TypeID    
WHERE     (t1.Recdate BETWEEN @fromdate AND @todate)    
 and t1.activeflag=0    


go 
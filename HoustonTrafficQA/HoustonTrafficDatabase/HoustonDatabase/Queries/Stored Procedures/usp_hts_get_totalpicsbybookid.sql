SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_totalpicsbybookid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_totalpicsbybookid]
GO

create procedure [dbo].[usp_hts_get_totalpicsbybookid]

@sbookid int
as

select docid from tblscanpic where scanbookid=@sbookid


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


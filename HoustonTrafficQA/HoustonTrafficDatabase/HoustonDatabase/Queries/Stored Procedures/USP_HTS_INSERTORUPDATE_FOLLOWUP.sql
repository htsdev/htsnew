SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_INSERTORUPDATE_FOLLOWUP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_INSERTORUPDATE_FOLLOWUP]
GO


CREATE PROCEDURE [dbo].[USP_HTS_INSERTORUPDATE_FOLLOWUP]    
@TicketID int,    
@CallbackDate datetime ,
@followup tinyint = 1 
    
as    
    
declare @rec_count int    
    
set @rec_count = (select count(ticketid_fk) from tblviolationquote where ticketid_fk = @TicketID)    
    
if(@rec_count = 0)    
 begin    
  INSERT INTO tblviolationquote(TicketID_FK, FollowUPYN, CallbackDate, recdate)    
  values (@TicketID, @followup, @CallbackDate, @CallbackDate)    
 end    
    
else    
 begin    
 UPDATE tblviolationquote    
  set FollowUpYN = @followup, CallbackDate = @CallbackDate , recdate = @CallbackDate     
  where TicketID_FK = @TicketID    
 end    
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


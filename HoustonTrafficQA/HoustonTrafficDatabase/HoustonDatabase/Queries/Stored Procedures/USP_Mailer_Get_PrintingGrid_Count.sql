set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Created by:		Tahir Ahmed
Business Logic:	The stored procedure is used by LMS/Reports/Misc Reports/Printing Grid report.
				It is used to get the mailer count for each court category or individual court house
				for the selected mailer date and previous 11 business days. If no mailer is printed 
				in the 12 days, then will return the last printing date & count if printed in past 2 months...
				
List of Parameters:
	@SelectedDate:	Mailer printing date.
	@UserLocation:	To display only Houston (1), Dallas (2), or both (0) the locations.

List of Columns:	
	categoryid:		ID of the court category...
	courtid:		ID of the individual court house.
	lettertype:		ID of the mailer type..
	BatchID:		Batch ID associated with the mailer.
	LetterCount:	Count of mailer printed for a batch of the specified mailer type.
	Flag:			Flag to determine if letter is not printed in 12 business days.
	MailerDate:		Date when mailers get printed.
	FileName		PDF file name for the batch.

******/

-- USP_Mailer_Get_PrintingGrid_Count '1/14/10,1/13/10,1/12/10,1/11/10,1/9/10,1/8/10,1/7/10,1/6/10,1/5/10,1/4/10,1/2/10,1/1/10,',0
ALTER procedure [dbo].[USP_Mailer_Get_PrintingGrid_Count]
	(
	@SelectedDates varchar(200),
	@UserLocation int = 0
	)

as 

declare @minDate datetime 
declare @dates table (mailerdate datetime)
declare @letters TABLE (categoryid int, courtid int, lettertype int, batchid int, lettercount int, letterdate datetime, Filename varchar(200), flag int)

insert into @dates
select DISTINCT * from dbo.Sap_Dates(@SelectedDates) --sarim 7804 07/06/2010 need only distinct....

select @minDate = min(mailerdate) from @dates

-- SEARCHING FOR ALL or HOUSTON...
if @userlocation in (0,1)
	begin

		-- HTP COURTS....
		-- GETTING LETERS IN TEMP TABLE PRINTED ON THE PROVIDED DATE RANGES...
		insert into @letters (categoryid , courtid , lettertype , batchid , lettercount , letterdate, filename )
		select b.courtid, case when b.courtid = 2 then n.courtid else  b.courtid end, n.lettertype, b.batchid, count(n.noteid), convert(varchar(10),n.recordloaddate,101), b.filename
		from tblletternotes n inner join tblbatchletter b
		on n.batchid_fk  =  b.batchid inner join @dates d 
		on datediff(day, n.recordloaddate, d.mailerdate) = 0
		inner join tblcourts c on c.courtid = n.courtid
		-- tahir 4903 10/04/2008 fixed the DMC letter count duplication bug
		--Muhammad Ali 8334 09/29/2010 -- add PLano MC into Printing grid..31
		--Farrukh 10397 08/13/2012 Added category id of Tarrant County Criminal Court....--	
		where b.courtid not in (6,11,16,25,27,28,31,34) --Sabir Khan 6888 10/29/2009 Added category id of fortworth and arlington....
		-- end  4903
		group by 
				b.courtid, case when b.courtid = 2 then n.courtid else  b.courtid end, 
				n.lettertype, b.batchid, convert(varchar(10),n.recordloaddate,101), filename
	end

-- SEARCHING FOR ALL or DALLAS...
if @userlocation in (0,2)
	begin

		-- DTP COURTS....
		-- GETTING LETERS IN TEMP TABLE PRINTED ON THE PROVIDED DATE RANGES...
		insert into @letters (categoryid , courtid , lettertype , batchid , lettercount , letterdate, filename )
		select b.courtid,case when b.courtid = 11 then n.courtid else  b.courtid end, n.lettertype, b.batchid, count(n.noteid), convert(varchar(10),n.recordloaddate,101), b.filename
		from tblletternotes n inner join tblbatchletter b
		on n.batchid_fk  =  b.batchid inner join @dates d 
		on datediff(day, n.recordloaddate, d.mailerdate) = 0
		inner join dallastraffictickets.dbo.tblcourts c on c.courtid = n.courtid
		-- tahir 4674 08/26/2008 fixed the DCJP letter count issue...
		--where c.courtid = 3049 and b.courtid = 6
			--Muhammad Ali 8334 09/29/2010 -- add PLano MC into Printing grid..31
			--Farrukh 10397 08/13/2012 Added category id of Tarrant County Criminal Court....
		-- Rab Nawaz Khan 10246 08/29/2012 Dallas District Criminal Court category id added. . . 
		where b.courtid in (6,11,16,25,27,28,29,30,31,34) --Sabir Khan 6888 10/29/2009 Added category id of fortworth and arlington....
		-- end 4674
		group by 
				b.courtid, case when b.courtid = 11 then n.courtid else  b.courtid end, n.lettertype, 
				b.batchid, convert(varchar(10),n.recordloaddate,101), filename

	end

update @letters set flag = 0

-- GETTING LAST PRINTING DATE & COUNT OF LETTERS IF NOT PRINTED
-- DURING THE SELECTED DATE RANGE...

select b.courtid as categoryid, case when b.courtid in( 2,11)  then n.courtid else b.courtid end as courtid,
	n.lettertype, max(b.batchid) as batchid
into #tmp
from tblletternotes n inner join tblbatchletter b on b.batchid = n.batchid_fk
where datediff(day, b.batchsentdate, @mindate) > 0
group by b.courtid, case when b.courtid in( 2,11)  then n.courtid else b.courtid end ,
	n.lettertype

alter table #tmp add 
	lettercount int,
	letterdate datetime, 
	filename varchar(200)

select b.batchid, b.courtid , b.lettertype, b.courtid as categoryid, 
	  b.lcount, convert(varchar(10), b.batchsentdate,101) as letterdate,
	 b.filename
into #tmp2
from tblbatchletter b inner join #tmp t on t.batchid = b.batchid
where b.courtid not in (2,11)

insert into #tmp2
select b.batchid, n.courtid , b.lettertype, b.courtid as categoryid, 
	  count(n.noteid), convert(varchar(10), b.batchsentdate,101) as letterdate,
	 b.filename
from tblbatchletter b inner join #tmp t on t.batchid = b.batchid 
inner join tblletternotes n on n.batchid_fk = t.batchid
where b.courtid  in (2,11) and t.courtid = n.courtid
group by b.batchid, n.courtid, b.lettertype, b.courtid, convert(varchar(10), b.batchsentdate,101), b.filename 


update t 
set t.lettercount= b.lcount,
	t.letterdate = b.letterdate,
	t.filename = b.filename
from #tmp t inner join #tmp2 b on b.batchid = t.batchid
and b.courtid = t.courtid and b.lettertype = t.lettertype

insert into @letters (categoryid , courtid , lettertype , batchid , lettercount , letterdate, filename, flag )
select distinct categoryid, courtid, lettertype, batchid, lettercount, letterdate, filename, 1 from #tmp

SELECT categoryid, courtid, lettertype, batchid, lettercount, flag,  letterdate as mailerdate, isnull(filename,'') as filename
FROM @LETTERS 
order by categoryid, courtid, letterdate desc, lettertype


drop table #tmp
alter Procedure usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1      
AS      
SET NOCOUNT ON      
 create TABLE [#tblTickets] (      
 [TicketID_PK] [int] NOT NULL ,      
 [TicketNumber_PK] [varchar] (20) NOT NULL ,      
 [Lastname] [varchar] (20) NULL ,      
 [Firstname] [varchar] (20)  NULL ,      
 [Violations] [varchar] (50) NULL,      
 [DLNumber] [varchar] (30) NULL ,      
 [DOB] [datetime] NULL ,      
 [MID] [varchar] (20) NULL ,      
 [BondFlag] [int] NULL ,        
 [CurrentCourtNum] varchar(3) NULL,      
 [CurrentDateSet] [datetime] NULL,      
 [OfficerDay]  [varchar] (30) NULL,      
 [SEQ] int NULL)      
      
      
 INSERT INTO #tblTickets (TicketID_PK, refcasenumber, Lastname, Firstname, DLNumber, DOB, MID,  BondFlag, CurrentCourtNum, CurrentDateSet, OfficerDay)      
 SELECT DISTINCT TT.TicketID_PK, TV.Refcasenumber,  TT.Lastname,       
 TT.Firstname, TT.DLNumber, TT.DOB,TT.MidNum , TT.BondFlag,  TV.CourtNumbermain , TV.CourtDateMain, O.OfficerType      
 FROM         tblTickets TT INNER JOIN      
                      tblTicketsViolations TV ON TT.TicketID_PK = TV.TicketID_PK INNER JOIN      
                      tblViolations TRV ON TV.ViolationNumber_PK = TRV.ViolationNumber_PK INNER JOIN      
                      tblCourtViolationStatus CV ON TV.CourtViolationStatusIDmain = CV.CourtViolationStatusID LEFT OUTER JOIN      
                      tblOfficer O ON TV.ticketOfficerNumber = O.OfficerNumber_PK      
   WHERE     (CV.CourtViolationStatusID in (select courtviolationstatusid from tblcourtviolationstatus where categoryid = 5))       
  AND ((DATEDIFF([day], TV.courtdatemain, GETDATE()) <= 0) )      
 and TRV.ViolationType not IN (1)      
 AND (TT.Activeflag = 1) AND       
 CV.CategoryID <> 50 and       
 (TV.CourtID IN(3001, 3002, 3003))       
 ORDER BY TV.CourtDateMain ASC      
      
       
      
 DECLARE @TicketID int      
 DECLARE @TicketNumber varchar(12)      
 DECLARE @Violations varchar(50)      
 DECLARE @SEQ int      
 DECLARE @TicketID2 varchar(50)      
      
 SELECT @SEQ =1      
      
 DECLARE CurWeekDays CURSOR READ_ONLY FOR       
 Select TicketID_PK, refcasenumber From #tblTickets       
 ORDER BY      
 CurrentDateSet ASC  ,      
 TicketID_PK ASC ,      
 refcasenumber ASC      
      
 OPEN CurWeekDays      
 FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber       
      
 Select @TicketID2 = @TicketID      
      
 WHILE (@@fetch_status <> -1)      
 BEGIN      
  SELECT @Violations = coalesce(@Violations + ', ',' ') + Convert(varchar,TV.RefCaseNumber)       
  FROM dbo.tblTicketsViolations TV INNER JOIN dbo.tblViolations TRV ON      
  TV.ViolationNumber_PK = TRV.ViolationNumber_PK       
  WHERE TRV.ViolationType IN (0,2)      
  AND TV.ViolationStatusID = 1      
  AND TV.TicketID_PK =  @TicketID       
  AND TV.RefCaseNumber = @TicketNumber      
   UPDATE #tblTickets       
   Set #tblTickets.Violations= @Violations ,      
   SEQ = @SEQ      
   WHERE  #tblTickets.TicketID_PK = @TicketID AND      
   #tblTickets.refcasenumber = @TicketNumber      
         
   SELECT @TicketNumber = NULL      
   SELECT @Violations = NULL       
   SELECT @TicketID = NULL      
         
   FETCH NEXT FROM CurWeekDays INTO @TicketID , @TicketNumber       
      
  IF (@TicketID2 <> @TicketID)      
  BEGIN      
   SELECT @SEQ = @SEQ + 1      
  END      
 END      
 CLOSE CurWeekDays      
 DEALLOCATE CurWeekDays      
      
       
      
 SELECT TicketID_PK, refcasenumber , Lastname, Firstname , Violations =       
 CASE LEFT(UPPER(refcasenumber),1)      
 WHEN 'F' THEN NULL      
 ELSE Violations       
 END , DLNumber , DOB ,MID ,  BondFlag =       
 CASE BondFlag       
 WHEN 1 THEN 'b'      
 ELSE NULL      
 END ,  CurrentCourtNum, CurrentDateSet, OfficerDay, SEQ      
 FROM #tblTickets       
 ORDER BY        
 CurrentDateSet ASC  ,      
 TicketID_PK ASC ,      
 refcasenumber ASC      
go
/*      
* Created By  : SAEED AHMED    
* Task ID   : 7791      
* Created Date  : 05/29/2010      
* Business logic : This procedure is used to update the eamil timings against the input reportID. For a single report user can have    
*     : six timings[3 weekdays timings & 3 saturday timings]. If weekdays or saturday timings already exist for a particular    
*     : report then this sp only updates those entries, otherwise it creates new entris against that report.      
*     
* Parameter List      
* @ReportID   : Represents reportID against which the schedule need to update.    
* @WeekdayTiming1 : Represents Weekday Timing1 which need to be set agisnt input @ReportID.    
* @WeekdayTiming2 : Represents Weekday Timing2 which need to be set agisnt input @ReportID.    
* @WeekdayTiming3 : Represents Weekday Timing3 which need to be set agisnt input @ReportID.    
* @SaturdayTiming1 : Represents Saturday Timing1 which need to be set agisnt input @ReportID.    
* @SaturdayTiming2 : Represents Saturday Timing2 which need to be set agisnt input @ReportID.    
* @SaturdayTiming3 : Represents Saturday Timing3 which need to be set agisnt input @ReportID.    
* @IdWeekdayTiming1 : Represents Weekday Timing1 in ValidationEmailTimings table.    
* @IdWeekdayTiming2 : Represents Weekday Timing2 in ValidationEmailTimings table.     
* @IdWeekdayTiming3 : Represents Weekday Timing3 in ValidationEmailTimings table.     
* @IdSaturdayTiming1: Represents Saturday Timing1 in ValidationEmailTimings table.    
* @IdSaturdayTiming2: Represents Saturday Timing2 in ValidationEmailTimings table.    
* @IdSaturdayTiming3: Represents Saturday Timing3 in ValidationEmailTimings table.    
*    
*/    
CREATE PROC dbo.USP_HTP_Update_ValidationSchedule      
 @ReportID INT,      
 @WeekdayTiming1 INT,      
 @WeekdayTiming2 INT,      
 @WeekdayTiming3  INT,    
       
 @SaturdayTiming1  INT,      
 @SaturdayTiming2  INT,      
 @SaturdayTiming3  INT,    
     
 @IdWeekdayTiming1 INT,      
 @IdWeekdayTiming2 INT,      
 @IdWeekdayTiming3  INT,    
       
 @IdSaturdayTiming1  INT,      
 @IdSaturdayTiming2  INT,      
 @IdSaturdayTiming3  INT    
     
 AS      
    
-- insert new weekday schedule 1     
IF @IdWeekdayTiming1 = -1    
 BEGIN    
  INSERT INTO ValidationReportScheduling(ReportID,EmailTimingsID,IsActive, sequenceid, isweekday)    
  select @ReportID, @WeekdayTiming1,1, 1,1 where @WeekdayTiming1 <> -1    
 END    
-- update weekday schedule 1    
ELSE    
BEGIN  
 IF @WeekdayTiming1=-1    
 BEGIN    
  UPDATE ValidationReportScheduling SET IsActive = 0 WHERE SchedulingID=@IdWeekdayTiming1    
 END       
 else  
 begin  
 update ValidationReportScheduling set IsActive = 1,EmailTimingsID = @WeekdayTiming1 where SchedulingID =  @IdWeekdayTiming1    
 end  
    
END     
     
-- insert new weekday schedule 2     
IF @IdWeekdayTiming2 = -1    
 BEGIN     
  INSERT INTO ValidationReportScheduling(ReportID,EmailTimingsID,IsActive, sequenceid, isweekday)    
  select @ReportID, @WeekdayTiming2, 1,2,1 where @WeekdayTiming2 <> -1    
    
 END    
-- update weekday schedule 2     
ELSE    
 BEGIN     
  if @WeekdayTiming2=-1  
  begin    
  update ValidationReportScheduling set IsActive = 0 where SchedulingID =  @IdWeekdayTiming2      
  end  
  else  
  begin  
 update ValidationReportScheduling set IsActive = 1,EmailTimingsID = @WeekdayTiming2 where SchedulingID =  @IdWeekdayTiming2      
  end   
 END      
       
       
-- insert new weekday schedule 3    
IF @IdWeekdayTiming3 = -1    
 BEGIN     
  INSERT INTO ValidationReportScheduling(ReportID,EmailTimingsID,IsActive, sequenceid, isweekday)    
  select @ReportID, @WeekdayTiming3, 1,3,1 where @WeekdayTiming3 <> -1    
 END    
-- update weekday schedule 3    
ELSE    
 BEGIN     
    if @WeekdayTiming3=-1  
  begin    
  update ValidationReportScheduling set IsActive = 0 where SchedulingID =  @IdWeekdayTiming3      
  end  
  else  
  begin  
 update ValidationReportScheduling set IsActive = 1,EmailTimingsID = @WeekdayTiming3 where SchedulingID =  @IdWeekdayTiming3      
  end   
 END       
     
-- insert new off day schedule 1     
IF @IdSaturdayTiming1 = -1    
 BEGIN     
  INSERT INTO ValidationReportScheduling(ReportID,EmailTimingsID,IsActive, sequenceid, isweekday)    
  select @ReportID, @SaturdayTiming1,1,1,0 where @SaturdayTiming1 <> -1    
 END    
-- update off day schedule 1    
ELSE    
 BEGIN     
  if @SaturdayTiming1=-1  
 begin  
  update ValidationReportScheduling set IsActive = 0 where SchedulingID =  @IdSaturdayTiming1      
 end  
 else  
 begin  
  update ValidationReportScheduling set IsActive = 1,EmailTimingsID = @SaturdayTiming1 where SchedulingID =  @IdSaturdayTiming1      
 end  
 END      
     
-- insert new off day schedule 2     
IF @IdSaturdayTiming2 = -1    
 BEGIN     
  INSERT INTO ValidationReportScheduling(ReportID,EmailTimingsID,IsActive, sequenceid, isweekday)    
  select @ReportID, @SaturdayTiming2, 1,2,0 where @SaturdayTiming2 <> -1    
 END    
-- update off day schedule 2    
ELSE    
 BEGIN     
  if @SaturdayTiming2=-1  
 begin  
  update ValidationReportScheduling set IsActive = 0 where SchedulingID =  @IdSaturdayTiming2      
 end  
 else  
 begin  
  update ValidationReportScheduling set IsActive = 1,EmailTimingsID = @SaturdayTiming2 where SchedulingID =  @IdSaturdayTiming2      
 end  
 END      
     
-- insert new off day schedule 3    
IF @IdSaturdayTiming3 = -1    
 BEGIN     
  INSERT INTO ValidationReportScheduling(ReportID,EmailTimingsID,IsActive, sequenceid, isweekday)    
  select @ReportID, @SaturdayTiming3, 1,3,0 where @SaturdayTiming3 <> -1    
 END    
-- update off day schedule 3    
ELSE    
 BEGIN     
  if @SaturdayTiming3=-1  
 begin  
  update ValidationReportScheduling set IsActive = 0 where SchedulingID =  @IdSaturdayTiming3      
 end  
 else  
 begin  
  update ValidationReportScheduling set IsActive = 1,EmailTimingsID = @SaturdayTiming3 where SchedulingID =  @IdSaturdayTiming3      
 end  
 END         


GO
GRANT EXECUTE ON dbo.USP_HTP_Update_ValidationSchedule TO dbr_webuser
GO

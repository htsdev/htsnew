
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_getEdeliveryLetterForTracking]    Script Date: 04/30/2008 17:25:36 ******/
/******    
Create by:  Agha Usman    
Business Logic : It is used by Edelivery service to get those records which needs to be tracked by the help USPS Delivery Confirmation API.   

List of Parameters:      
 
      No Parameters  
 
List of Columns:  
  
  BatchId_PK : this field will display Batch Id  
  TicketId_Fk : this field will display Ticket Id  
  USPSTrackingNumber: this field will display Tracking Number   
  DeliveryStatus : this field will display Delivery Status   
******/  
  
  
CREATE PROCEDURE [dbo].[usp_hts_getEdeliveryLetterForTracking]   
AS  
BEGIN  
 select t.LastName + ', ' + t.FirstName  as [FullName],BatchId_PK, TicketId_Fk,USPSTrackingNumber,DeliveryStatus from tblHtsbatchprintletter thbp
inner join tbltickets t on    thbp.ticketId_Fk = t.ticketId_Pk
where EDeliveryFlag = 1 and   
isnull(uspsTrackingNumber,'0') != '0'  
and isnull(DeliveryStatus,0) != 1   
END  

go
GRANT EXEC ON [dbo].[usp_hts_getEdeliveryLetterForTracking]    to dbr_webuser
go
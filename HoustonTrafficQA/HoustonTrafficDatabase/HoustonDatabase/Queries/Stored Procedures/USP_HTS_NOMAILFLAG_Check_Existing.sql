/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 7/18/2009 4:30:11 AM
 ************************************************************/

-- =============================================
-- Business Logic:	This procedure is used to get total count of records which are already exists in No Mail Flag Table
--
-- List Of Parameters:
--	@FirstName:	First Name of Client.
--	@LastName:	Last Name of Client.
--	@Address:	Address of Client.
--	@Zip:		Zip code of client.

-- List of output columns:
--	RecCount:			Total number of records.
-- =============================================

-- USP_HTS_NOMAILFLAG_Check_Existing 'TODD','BALSLEY','555 BRADFORD AVE # D-23','77565-2314'
ALTER PROCEDURE USP_HTS_NOMAILFLAG_Check_Existing
	@FirstName VARCHAR(20),
	@LastName VARCHAR(20),
	@Address VARCHAR(50),
	@Zip VARCHAR(12)
AS
	SELECT COUNT(*) AS RecCount
	FROM   tbl_hts_DoNotMail_Records
	WHERE -- abbas Qamar 9726 12/23/2011 where clause has been changed.
	CASE WHEN LEN(LTRIM(RTRIM(@FirstName))) > 0 THEN LTRIM(RTRIM(FirstName)) ELSE '' END  =  LTRIM(RTRIM(@FirstName))							
    AND
    LTRIM(RTRIM(LastName)) = LTRIM(RTRIM(@LastName))                        
    AND    
	LTRIM(RTRIM(ADDRESS)) = LTRIM(RTRIM(@Address))
	AND 
	LEFT(LTRIM(RTRIM(Zip)),5) LIKE LEFT(LTRIM(RTRIM(@Zip)),5)  -- Sabir Khan 6167 07/17/2009 Fix the zip code comparison issue...
	
	








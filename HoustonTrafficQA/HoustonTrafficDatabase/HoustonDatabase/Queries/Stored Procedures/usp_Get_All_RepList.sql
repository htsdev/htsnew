USE [TrafficTickets]
GO
/****** Business Logic:
* This procedure is used to get all rep list.
* Altered by: Sabir Khan
* Task ID: 10920
* Date: 05/27/2013
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  usp_Get_All_RepList '06/15/05', '06/15/05'
ALTER  PROCEDURE [dbo].[usp_Get_All_RepList] 
	(
	@sDate		datetime,
	@eDate	DATETIME,
	@branchID INT =1 -- Sabir Khan 10920 05/27/2013 Branch ID added.
	)

AS

set @eDate = @eDate + '23:59:59.999'

SELECT distinct u.Lastname as RepName, u.EmployeeID as RepID 

FROM tblUsers u inner join tblticketspayment p 
on p.employeeid = u.employeeid
and p.recdate between @sDate and @eDate
AND p.BranchID IN (SELECT branchid FROM fn_get_branchIDs(@branchID))
--Where IsSalesRep=1

Order By u.lastname  --EmployeeID



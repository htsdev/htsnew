SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_All_NOMAILFLAG_BY_TicketNum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_All_NOMAILFLAG_BY_TicketNum]
GO


        
CREATE procedure [dbo].[USP_HTS_All_NOMAILFLAG_BY_TicketNum] --002352277        
        
@TicketNumber varchar(20)                
as        
         
        
        
        
declare @cnt int          
          
select @cnt=count(firstname) from  tblticketsarchive where ltrim(rtrim(TicketNumber)) = @TicketNumber        
               
---------------------------------------------For Houston -------------------------------------------------------        
if @cnt > 0        
begin        
 INSERT INTO tbl_hts_donotmail_records               
 (              
 FirstName, LastName, Address,Zip , City, StateID_FK)               
 select FirstName,LastName,Address1+' '+address2,case when len(ZipCode)>5 then substring(ZipCode,1,5) else ZipCode end, City,stateid_fk from tblticketsarchive where ltrim(rtrim(TicketNumber)) = @TicketNumber                
 Update tblticketsarchive                
 set DoNotMailFlag =2                
 WHERE  ltrim(rtrim(TicketNumber)) = @TicketNumber         
end        
        
        
-------------------------------------------for Dallas-----------------------------------------------------------------        
        
select @cnt=count(firstname) from dallastraffictickets.dbo.tblticketsarchive where ltrim(rtrim(dallastraffictickets.dbo.tblticketsarchive.TicketNumber)) = @TicketNumber        
        
if @cnt > 0        
begin        
 INSERT INTO tbl_hts_donotmail_records               
 (              
 FirstName, LastName, Address,Zip , City, StateID_FK)               
 select FirstName,LastName,Address1+' '+address2,case when len(ZipCode)>5 then substring(ZipCode,1,5) else ZipCode end, City,stateid_fk from dallastraffictickets.dbo.tblticketsarchive where ltrim(rtrim(dallastraffictickets.dbo.tblticketsarchive.TicketNumber))    
       
= @TicketNumber                
 Update dallastraffictickets.dbo.tblticketsarchive                
 set DoNotMailFlag =2                
 WHERE  ltrim(rtrim(dallastraffictickets.dbo.tblticketsarchive.TicketNumber)) = @TicketNumber         
end        
        
------------------------------------------for JIMS--------------------------------------------------------------        
        
        
        
select @cnt=count(firstname) FROM         JIMS.dbo.CriminalCases AS cc where  cc.BookingNumber=  @TicketNumber        
        
if @cnt > 0        
begin        
        
  INSERT INTO tbl_hts_donotmail_records  (FirstName, LastName, Address, Zip, City, StateID_FK)                         
  SELECT                   
  cc.FirstName,               
  cc.LastName,               
  cc.Address,               
  CASE WHEN len(cc.Zip) > 5 THEN substring(cc.Zip, 1, 5) ELSE cc.Zip END AS Zip,               
  cc.City,               
  st.StateID              
  FROM         JIMS.dbo.CriminalCases AS cc         
        
    INNER JOIN              
    tblState AS st ON cc.State = st.State         
    where  cc.BookingNumber= @TicketNumber            
           
                
                        
  update jims.dbo.CriminalCases              
  set DoNotMailFlag = 2              
                
  FROM         JIMS.dbo.CriminalCases AS jcc where        
    jcc.BookingNumber = @TicketNumber         
                
         
        
end           
           
        
select @cnt        



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


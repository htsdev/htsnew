/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 7/6/2009 7:39:28 PM
 ************************************************************/

/*********************
* Business Logic :  This stored procedure is used by HTP/Activites/Text Message/History Page.To get Email History.
* 
* Input Parameters:
* @startdate : from date in selection criteria.
* @enddate : to date in selection criteria.
* 
*********************/
-- [dbo].[USP_HTS_list_all_email_send] '08/25/2009','08/25/2009',1
ALTER PROCEDURE [dbo].[USP_HTS_list_all_email_send] --'05/05/2009','07/06/2009'
	--Yasir kamal 6064 07/01/2009 allow searching for past records.
	@startdate AS DATETIME,
	@enddate AS DATETIME,
	@messagecategory INT
	
	AS
	
	SET @startdate = CONVERT(VARCHAR, @startdate, 101) 
	SET @enddate = CONVERT(VARCHAR, @enddate, 101) 
	
	
	SELECT CASE @messagecategory
	            WHEN 0 THEN EmailTo
	            ELSE '[' + Client + ']' + '</br>' + PhoneNumber -- Noufil 6461 08/25/2009 Add space new line between name and phone number
	       END AS EmailTo,
	       CASE @messagecategory
	            WHEN 0 THEN Client
	            ELSE 
	            	-- Rab Nawaz Khan 10914 05/28/2013 Alert Type Text has been changed for Set call and Reminder Call. . . 
					CASE WHEN SmsCallType = 'Set Call' THEN 'Set Text'
					WHEN SmsCallType = 'Reminder Call' THEN 'Reminder Text'
					ELSE SmsCallType END
	       END AS Client,
	       sentinfo AS SUBJECT,
	       -- Noufil 5884 07/06/2009 Format Date time according to Design Standard
	       dbo.fn_DateFormat(recdate, 25, '/', ':', 1) AS Dates,
	       [id],
	       sentinfo AS caseno,
	       -- Noufil 5884 07/06/2009 case added
	       CASE @messagecategory
	            WHEN 0 THEN resendby
	            ELSE (
	                     SELECT tu.UserName
	                     FROM   tblUsers tu
	                     WHERE  tu.EmployeeID = tblemailhistory.resendby
	                 )
	       END AS resendby,
	       ISNULL(TicketID, 0) AS ticketid,
	       ISNULL(PhoneNumber, 0) AS PhoneNumber,
	       TicketNumber,
	       CASE WHEN SmsCallType = 'Invalid Response Text' THEN '01/01/1900' ELSE CourtDate END AS CourtDate,
	       CourtNumber,
	       Client AS Clientname,
	       (
	       		-- noufil 6177 08/24/2009 Use court full name
	           SELECT TOP 1 tc.CourtName
	           FROM   tblCourts tc
	                  INNER JOIN tblTicketsViolations ttv
	                       ON  ttv.CourtID = tc.Courtid
	           WHERE  ttv.TicketID_PK = tblemailhistory.TicketID
	                  AND DATEDIFF(DAY, ttv.CourtDateMain, tblemailhistory.CourtDate) = 
	                      0
	       ) AS shortname
	FROM   tblemailhistory
	WHERE  DATEDIFF(DAY, recdate, @startdate) <= 0
	       AND DATEDIFF(DAY, recdate, @enddate) >= 0
	       AND (
	               (@messagecategory = 0 AND ISNULL(Smstype, 0) = 0)
	               OR (@messagecategory = 1 AND Smstype = 1)
	           )
	ORDER BY
	       recdate DESC --Sabir Khan 6160 07/17/2009 Most recent should be displayed first...

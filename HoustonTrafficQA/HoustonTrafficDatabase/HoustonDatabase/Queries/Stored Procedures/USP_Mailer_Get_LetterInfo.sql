  
  
  
/********  
Created By:  
Business Logic:  
List of Parameters:   
 @LetterType: Unique letter id  
List of Columns:  
 lettertype:  Uniqe letter id  
 courtcategory:  Court Categories ID    
 lettername:   Name of letter to be print.  
 courtcategoryname: Court Category Name  
 reportfilename: File name of report  
 count_spname: Stored proceedure name for counting  
 data_spname: Stored Proceedure name for data  
 StartDate: Start date for search data  
    EndDate: End date for search data  
 SearchType: Type of search   
 CanPrintForOffDays: If true means off days will be counted and vice versa.  
  
********/  
-- USP_Mailer_Get_LetterInfo 19    
alter procedure [dbo].[USP_Mailer_Get_LetterInfo]    
 (    
  @LetterType int ,  
  @LetterDate datetime = null  
 )    
  
    
as    
if  (@LetterDate is null )    
 set @LetterDate = getdate()  
    
    
select distinct top 1     
 l.letterid_pk as lettertype ,     
 l.courtcategory as courtcategoryid,     
 l.lettername,     
 c.courtcategoryname,     
 l.reportfilename,     
 l.count_spname,     
 l.data_spname, 0 as querytype,   
-- Zahoor Start 4622  
dateadd(day, l.DefaultStartDate, @LetterDate) as StartDate,  
dateadd(day, l.DefaultEndDate, @LetterDate) as EndDate,  
l.SearchType,  
l.CanPrintForOffDays,  
l.OffDayType  
-- End 4622   
from dbo.tblletter l    
inner join     
 dbo.tblcourtcategories c    
on l.courtcategory = c.courtcategorynum     
where l.letterid_pk = @lettertype    
and l.isactive  = 1     
    
  
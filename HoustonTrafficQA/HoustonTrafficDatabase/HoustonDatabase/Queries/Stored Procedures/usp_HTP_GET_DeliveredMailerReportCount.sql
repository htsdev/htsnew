
 /********************************************************************      
Created By     : Abbas Qamar
Creation Date   : 16/02/2012
Task ID			: 10020
Business Logic : This procedure is used to get the report for mailer that are having delivered in specific date range and return the Delvery Date 
				 
  
Parameter:             
 @StartDate, 
 @EndDate  
 @LetterType,
 @ReportType: For Mailer Date type(Daily,Weekly,Monthly)
 
Columns:  
   
 [DeliveredCount], 
 [DeliveryDate] 
     
   
*********************************************************************/
--[usp_HTP_GET_DeliveredMailerReportCount] '2009-12-28', '2009-12-28', 9,'Daily'


Create PROCEDURE [dbo].[usp_HTP_GET_DeliveredMailerReportCount] 
( 
 @StartDate VARCHAR(30) , 
 @EndDate VARCHAR(30)  ,
 @LetterType INT,
 @ReportType VARCHAR(20)
)
AS
CREATE TABLE #Mailers(currentdate DATETIME,noteid INT)


BEGIN		
-- GET ALL MAILERS DATA PRINTED DURING THE SPECIFIED DATE RANGE....
INSERT INTO #Mailers(currentdate,noteid)
SELECT CONVERT(DATETIME, CONVERT(VARCHAR(10), Currentdate, 101)), noteid

FROM tblletternotes   
WHERE LetterType = @LetterType
AND DATEDIFF(DAY, Currentdate, CONVERT(DATETIME, @StartDate)) <= 0
AND DATEDIFF(DAY, Currentdate, CONVERT(DATETIME ,@EndDate)) >= 0  
END



CREATE TABLE #PackageData  
(

	LetterID VARCHAR(50),
	InsertDateTime DATETIME,

)



DELETE M
FROM #Mailers M INNER JOIN  MailerUSPSInfo.dbo.USPSPackageData p ON p.LetterID = M.noteid
WHERE p.OperationCode IN  ('091','099')


INSERT Into #PackageData  
SELECT  p.LetterID,MIN(p.InsertDateTime) AS InsertDateTime 
FROM MailerUSPSInfo.dbo.USPSPackageData p
INNER JOIN #Mailers a ON a.noteid = p.LetterID
WHERE p.OperationCode IN 
				(  '828', '878', '898', '908','912', '914','918', '146','266', '276','286', '336',
				'366', '376','396', '406','416', '426','446', '466','486', '496','506', '806', 
				'816', '826', '829', '836','846', '856','866', '876','879', '886','896', '899', 
				'905', '906','909', '911','913', '915','919', '966','976'
				)
GROUP BY p.LetterID



ALTER TABLE #PackageData  ADD OperationCode VARCHAR(200) 



-- CREATING INDEX ON TEMP TABLE FOR PERFORMANCE   
BEGIN TRY   

	CREATE NONCLUSTERED INDEX IX_Mailers_LetterId ON #Mailers
	(LetterID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX IX_PackageData_LetterId ON #PackageData
	(LetterID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
END TRY

BEGIN CATCH
	-- DO NOTHING
END CATCH

UPDATE p
SET p.operationCode = m.operationCode
FROM MailerUSPSInfo.dbo.USPSPackageData m
INNER JOIN #PackageData  p ON p.LetterID= m.LetterID AND p.InsertDateTime =m.InsertDateTime 
WHERE m.OperationCode IN 
				('828', '878', '898', '908','912', '914','918', '146','266', '276','286', '336',
				'366', '376','396', '406','416', '426','446', '466','486', '496','506', '806', 
				'816', '826', '829', '836','846', '856','866', '876','879', '886','896', '899', 
				'905', '906','909', '911','913', '915','919', '966','976'
				)


CREATE TABLE #mytemp(	LetterID VARCHAR(50),Deliverydate DateTime)

INSERT INTO #mytemp
SELECT p.LetterID,p.InsertDateTime AS InsertDateTime 
FROM #PackageData p
INNER JOIN #Mailers a ON a.noteid = p.LetterID
WHERE p.OperationCode IN 
				('828', '878', '898', '908','912', '914','918', '146','266', '276','286', '336',
				'366', '376','396', '406','416', '426','446', '466','486', '496','506', '806', 
				'816', '826', '829', '836','846', '856','866', '876','879', '886','896', '899', 
				'905', '906','909', '911','913', '915','919', '966','976'
				)
GROUP BY p.LetterID,p.InsertDateTime
ORDER BY p.InsertDateTime

----------------------------------------------------------------------





-----------------------------------------------------------------------
-------------- Final Result showing Total Deliverd --------------------
-----------------------------------------------------------------------



SELECT CONVERT(VARCHAR(10), t1.Deliverydate, 101) AS Deliverydate,COUNT(DISTINCT t1.letterid) AS TotalDeliverd
FROM   #mytemp t1
GROUP BY CONVERT(VARCHAR(10), t1.Deliverydate, 101) 





BEGIN TRY
	DROP TABLE #mytemp  
	DROP TABLE #Mailers
	DROP TABLE #PackageData
	
END TRY
BEGIN CATCH
	-- DO NOTHING

END CATCH



GO
GRANT EXECUTE ON [dbo].[usp_HTP_GET_DeliveredMailerReportCount] TO dbr_webuser
GO 



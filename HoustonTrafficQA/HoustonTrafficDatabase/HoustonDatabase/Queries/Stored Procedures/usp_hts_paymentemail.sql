  
  
--  usp_hts_paymentemail 202884        
ALTER procedure [dbo].[usp_hts_paymentemail]     
         
@ticketid_pk int        
        
AS                              
declare @RefCaseNumber  varchar(500)    
declare @Ticketid_pk1 int           
declare @Firstname varchar(500)    
declare @Lastname varchar(500)    
declare @PymtComments varchar(500)    
declare @TotalFeeCharged int    
declare @TotalDue int    
declare @TotalPayment int    
declare @PaymentHistory varchar(max)    
    
set @RefCaseNumber = ''    
set @PaymentHistory = ''    
select @PaymentHistory = @PaymentHistory + '<b>Payment:</b> $' + convert(Varchar, convert(int,p.chargeamount)) + ' ('+convert(varchar(10),p.recdate,101)+' - ' + u.abbreviation + ') <br>'    
from tblticketspayment p inner join tblusers u on p.employeeid = u.employeeid    
where p.ticketid = @ticketid_pk and paymentvoid = 0    
order by p.recdate desc    
    
set @PaymentHistory = isnull(@PaymentHistory,'')    
    
if len(@PaymentHistory) > 4    
SET @PaymentHistory = upper(left(@PaymentHistory,len(@PaymentHistory)-4))    
    
select @TotalPayment = isnull(sum(isnull(chargeamount,0)),0)    
from tblticketspayment where ticketid =  @ticketid_pk    
and paymentvoid = 0    
    
SELECT  @Ticketid_pk1 = ttv.TicketID_PK ,     
  @Firstname = t.Firstname,     
  @Lastname = t.Lastname,     
  @RefCaseNumber = @RefCaseNumber + ttv.RefCaseNumber + ', ' ,     
  @PymtComments = '',   -- Agha Usman 2664 06/04/2008  
  @TotalFeeCharged = t.TotalFeeCharged        
FROM    tblTickets AS t INNER JOIN        
                      tblTicketsViolations AS ttv ON t.TicketID_PK = ttv.TicketID_PK     
 where t.ticketid_pk = @Ticketid_pk    
      
select @totaldue = @TotalFeeCharged - @TotalPayment    
    
select @Ticketid_pk1 as Ticketid_pk, @Firstname as Firstname, @Lastname as Lastname,     
left(@RefCaseNumber,len(@RefCaseNumber)-2) as RefCaseNumber ,     
@PymtComments as PymtComments, @TotalFeeCharged as TotalFeeCharged , @PaymentHistory as PaymentHistory,    
@totaldue as totaldue    
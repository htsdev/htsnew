ALTER procedure [dbo].[usp_HTS_ArraignmentSnapShot_VER_2_Update]            
AS              
SET NOCOUNT ON            
Declare @intTotal int            
Declare @intTot int            
Select @intTotal = 0            
Select @intTot = 0            
            
            
-- Create Table for weekdays            
CREATE TABLE #MON(            
CustNumber INT IDENTITY (1, 1) NOT NULL,            
TicketID_PK int,            
LastName varchar(30) NULL,            
FirstName varchar(30) NULL,            
ViolationDate datetime,             
TrialDesireddateTime datetime NULL,            
TrialComments varchar(100) NULL,            
BondFlag int  NULL DEFAULT 0,            
TrialDateTime varchar(10) NULL,            
pretrialstatus varchar(1)            
 )            
CREATE TABLE #TUE(            
CustNumber INT IDENTITY (1, 1) NOT NULL,            
TicketID_PK int,            
LastName varchar(30) NULL,            
FirstName varchar(30) NULL,            
ViolationDate datetime,             
TrialDesireddateTime datetime NULL,            
TrialComments varchar(100) NULL,            
BondFlag int  NULL DEFAULT 0,            
TrialDateTime varchar(10) NULL,            
pretrialstatus varchar(1)             
 )            
CREATE TABLE #WED(            
CustNumber INT IDENTITY (1, 1) NOT NULL,            
TicketID_PK int,            
LastName varchar(30) NULL,            
FirstName varchar(30) NULL,            
ViolationDate datetime,             
TrialDesireddateTime datetime NULL,            
TrialComments varchar(100) NULL,            
BondFlag int  NULL DEFAULT 0,            
TrialDateTime varchar(10) NULL,            
pretrialstatus varchar(1)            
 )            
CREATE TABLE #THR(            
CustNumber INT IDENTITY (1, 1) NOT NULL,            
TicketID_PK int,            
LastName varchar(30) NULL,            
FirstName varchar(30) NULL,            
ViolationDate datetime,             
TrialDesireddateTime datetime NULL,            
TrialComments varchar(100) NULL,            
BondFlag int  NULL DEFAULT 0,            
TrialDateTime varchar(10) NULL,            
pretrialstatus varchar(1))            
            
            
CREATE TABLE #FRI(            
CustNumber INT IDENTITY (1, 1) NOT NULL,            
TicketID_PK int,            
LastName varchar(30) NULL,            
FirstName varchar(30) NULL,            
ViolationDate datetime,             
TrialDesireddateTime datetime NULL,            
TrialComments varchar(100) NULL,            
BondFlag int  NULL DEFAULT 0,            
TrialDateTime varchar(10) NULL,            
pretrialstatus varchar(1) )            
CREATE TABLE #SEQ(            
CustNumber INT IDENTITY (1, 1) NOT NULL ,            
Pola varchar(1))            
            
            
CREATE TABLE #temp(            
TicketID_PK int,            
LastName varchar(30) NULL,            
FirstName varchar(30) NULL,            
ViolationDate datetime,             
TrialDesireddateTime datetime NULL,            
TrialComments varchar(100) NULL,            
BondFlag int  NULL DEFAULT 0,            
TrialDateTime varchar(10) NULL,            
pretrialstatus varchar(1),            
ticketsviolationid int,            
officertype varchar(10),            
datepartvalue int            
 )            
            
--select * from tblofficer            
            
insert into #temp            
Select distinct T.TicketID_PK, T.Lastname,T.Firstname,             
 V.Courtdatemain ,             
null as setdate            
--dbo.fn_CourtDesiredSetDate_ver_3(V.ticketsviolationid,officertype,v.courtdatemain)            
, T.SettingComments,             
--T.BondFlag , O.TrialDateTime, pretrialstatus = case pretrialstatus when 2 then 'P' else 'N' end,            
T.BondFlag , O.TrialDateTime, pretrialstatus = '', -- Agha Usman 2664 06/30/2008
0,officertype,0             
            
FROM         tblTickets T INNER JOIN            
                      tblTicketsViolations V ON T.TicketID_PK = V.TicketID_PK LEFT OUTER JOIN            
                      tblOfficer O ON V.ticketofficernumber = O.OfficerNumber_PK            
WHERE     V.courtviolationstatusidmain --in (1,2,3,5,8)             
in (select courtviolationstatusid from tblcourtviolationstatus where CATEGORYID IN (2,12,51))            
--and (DATEPART(dw, dbo.fn_CourtDesiredSetDate_ver_3(V.ticketsviolationid,officertype,v.courtdatemain)) = 2)            
AND (T.Activeflag = 1) AND (V.courtid IN (3001, 3002, 3003))            
and v.courtdatemain is not null            
AND V.courtviolationstatusidmain <> 80        
AND V.courtviolationstatusidmain <> 162 --added by ozair for bug # 1432            
order by v.courtdatemain, T.lastname, T.firstname            
            
INSERT INTO #MON(TicketID_PK ,FirstName, LastName, ViolationDate , TrialDesireddateTime, TrialComments, BondFlag , TrialDateTime, pretrialstatus)            
            
select  TicketID_PK,Firstname,Lastname,            
violationdate,TrialDesireddateTime as setdate,TrialComments,BondFlag,            
TrialDateTime,pretrialstatus            
from #temp            
where officertype = 'MONDAY'            
ORDER BY datediff(day,violationdate,getdate()) desc,ltrim(lastname), firstname            
            
Select @intTotal = @intTotal + @@ROWCOUNT            
INSERT INTO #TUE(TicketID_PK,FirstName, LastName, ViolationDate , TrialDesireddateTime, TrialComments, BondFlag ,TrialDateTime, pretrialstatus)            
select TicketID_PK,Firstname,Lastname,            
violationdate,TrialDesireddateTime as setdate,TrialComments,BondFlag,            
TrialDateTime,pretrialstatus            
from #temp            
where officertype = 'TUESDAY'            
ORDER BY datediff(day,violationdate,getdate()) desc,ltrim(lastname), firstname            
            
Select @intTotal = @intTotal + @@ROWCOUNT            
INSERT INTO #WED(TicketID_PK, FirstName, LastName, ViolationDate , TrialDesireddateTime, TrialComments, BondFlag,TrialDateTime, pretrialstatus)            
select TicketID_PK,Firstname,Lastname,            
violationdate,TrialDesireddateTime as setdate,TrialComments,BondFlag,            
TrialDateTime,pretrialstatus            
from #temp            
where officertype = 'WEDNESDAY'            
ORDER BY datediff(day,violationdate,getdate()) desc,ltrim(lastname), firstname            
            
Select @intTotal = @intTotal + @@ROWCOUNT            
INSERT INTO #THR(TicketID_PK,FirstName, LastName, ViolationDate , TrialDesireddateTime, TrialComments, BondFlag,TrialDateTime, pretrialstatus)            
select TicketID_PK,Firstname,Lastname,            
violationdate,TrialDesireddateTime as setdate,TrialComments,BondFlag,            
TrialDateTime,pretrialstatus            
from #temp            
where officertype = 'THURSDAY'            
ORDER BY datediff(day,violationdate,getdate()) desc,ltrim(lastname), firstname            
            
Select @intTotal = @intTotal + @@ROWCOUNT            
INSERT INTO #FRI(TicketID_PK, FirstName, LastName, ViolationDate , TrialDesireddateTime, TrialComments, BondFlag,TrialDateTime, pretrialstatus)            
select TicketID_PK,Firstname,Lastname,            
violationdate,TrialDesireddateTime as setdate,TrialComments,BondFlag,            
TrialDateTime,pretrialstatus            
from #temp            
where officertype = 'FRIDAY'            
ORDER BY datediff(day,violationdate,getdate()) desc,ltrim(lastname), firstname            
            
Select @intTotal = @intTotal + @@ROWCOUNT            
While NOT (@intTot = @intTotal )            
BEGIN            
  Select  @intTot = @intTot + 1            
  INSERT INTO   #SEQ(Pola) Values('P')            
END               
-- now select the customers              
SELECT  #SEQ.CustNumber AS SEQ_CustNumber , #MON.TicketID_PK AS MON_TicketID_PK, Left(#MON.LastName,10)+','+Left(#MON.FirstName,1) AS MON_Name,            
  #MON.ViolationDate AS MON_ViolationDate, #MON.TrialDesireddateTime AS MON_TrialDesireddateTime,               
                   #MON.TrialComments AS MON_TrialComments, ISNULL(#MON.BondFlag,0) AS MON_BondFlag, #MON.TrialDateTime AS MON_TrialDateTime,               
                      #MON.pretrialstatus as MON_pretrialstatus            
FROM                #SEQ LEFT OUTER JOIN              
                    #MON ON #SEQ.CustNumber = #MON.CustNumber              
WHERE               
#MON.TicketID_PK IS NOT NULL               
ORDER BY #SEQ.CustNumber ASC              
              
            
SELECT     #SEQ.CustNumber AS SEQ_CustNumber , #TUE.TicketID_PK AS TUE_TicketID_PK, Left(#TUE.LastName,10)+','+Left(#TUE.FirstName,1) AS TUE_Name,               
                      #TUE.ViolationDate AS TUE_ViolationDate, #TUE.TrialDesireddateTime AS TUE_TrialDesireddateTime, #TUE.TrialComments AS TUE_TrialComments,               
                       ISNULL(#TUE.BondFlag,0) AS TUE_BondFlag, #TUE.TrialDateTime AS TUE_TrialDateTime, #TUE.pretrialstatus  as TUE_pretrialstatus            
FROM                #SEQ LEFT OUTER JOIN              
                      #TUE ON #SEQ.CustNumber = #TUE.CustNumber             
                 
WHERE               
#TUE.TicketID_PK IS NOT NULL               
            
ORDER BY #SEQ.CustNumber ASC              
            
            
SELECT     #SEQ.CustNumber AS SEQ_CustNumber , #WED.TicketID_PK AS WED_TicketID_PK,               
                      Left(#WED.LastName,10)+','+Left(#WED.FirstName,1) AS WED_Name, #WED.ViolationDate AS WED_ViolationDate,               
                      #WED.TrialDesireddateTime AS WED_TrialDesireddateTime, #WED.TrialComments AS WED_TrialComments,               
           ISNULL(#WED.BondFlag,0) AS WED_BondFlag, #WED.TrialDateTime AS WED_TrialDateTime, #WED.pretrialstatus  as WED_pretrialstatus            
FROM                #SEQ LEFT OUTER JOIN              
                      #WED ON #SEQ.CustNumber = #WED.CustNumber              
WHERE               
            
#WED.TicketID_PK IS NOT NULL               
            
ORDER BY #SEQ.CustNumber ASC              
            
            
SELECT     #SEQ.CustNumber AS SEQ_CustNumber , #THR.TicketID_PK AS THR_TicketID_PK,               
                      Left(#THR.LastName,10)+','+Left(#THR.FirstName,1) AS THR_Name, #THR.ViolationDate AS THR_ViolationDate,               
                      #THR.TrialDesireddateTime AS THR_TrialDesireddateTime, #THR.TrialComments AS THR_TrialComments,  ISNULL(#THR.BondFlag,0) AS THR_BondFlag,               
                      #THR.TrialDateTime AS THR_TrialDateTime,#THR.pretrialstatus  as  THR_pretrialstatus            
FROM                #SEQ LEFT OUTER JOIN              
                      #THR ON #SEQ.CustNumber = #THR.CustNumber              
WHERE               
            
#THR.TicketID_PK IS NOT NULL               
            
ORDER BY #SEQ.CustNumber ASC              
            
            
SELECT     #SEQ.CustNumber AS SEQ_CustNumber , #FRI.TicketID_PK AS FRI_TicketID_PK, Left(#FRI.LastName,10)+','+Left(#FRI.FirstName,1) AS FRI_Name,               
                      #FRI.ViolationDate AS FRI_ViolationDate, #FRI.TrialDesireddateTime AS FRI_TrialDesireddateTime, #FRI.TrialComments AS FRI_TrialComments,               
                       ISNULL(#FRI.BondFlag,0) AS FRI_BondFlag, #FRI.TrialDateTime AS FRI_TrialDateTime, #FRI.pretrialstatus as FRI_pretrialstatus              
FROM                #SEQ LEFT OUTER JOIN              
                      #FRI ON #SEQ.CustNumber = #FRI.CustNumber              
WHERE               
            
#FRI.TicketID_PK IS NOT NULL              
ORDER BY #SEQ.CustNumber ASC                
              
              
drop table #MON              
drop table #TUE              
drop table #WED              
drop table #THR              
drop table #FRI              
drop table #SEQ              
drop table #temp              
      
  
-------------------------------Proc End-----------------------------------------------------  
  
go
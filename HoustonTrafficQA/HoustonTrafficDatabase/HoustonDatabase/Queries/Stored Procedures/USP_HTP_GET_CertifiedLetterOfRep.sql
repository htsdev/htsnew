﻿/***********************************************************************************  
 * Created By  : Abid Ali  
 * Created Date  : 1/2/2009  
 * Task Id   : 5359  
 *   
 * Business Logic : To get certififed letter of rep from batch history  
 *   
 * Parameter List :  
 *      @CertifiedMailNumber: Certified Mail Number  
 *      @FromDate   : Printed date from record display  
 *      @ToDate    : Printed date to record display  
 *      @Status    : All, pending or confirmed  
 *      @ShowAll   : Show all or not  
 *      @FilterType   : Filter type which create  
 *      @SalesRep   : Sale rep  
 *      @CourtId   : Court  
 *        
 * Columns Return :  
 *      ID  
 *      TicketID_PK  
 *      FullName  
 *      DOB  
 *      vacdate  
 *      refcasenumber  
 *      casenumassignedbycourt  
 *      languagespeak  
 *      currentdateset  
 *      attn  
 *      judgename  
 *      phone  
 *      courtname  
 *      CourtShortName  
 *      address  
 *      cityzip  
 *      FAXNO  
 *      Attorneyname  
 *      firmsubtitle  
 *      RepDate  
 *      courtid  
 *      SettingRequest  
 *      causenumbers  
 *      BatchID_FK  
 *      dlnumber  
 *      viol_desc  
 *      IsBond  
 *      USPSTrackingNumber  
 *      DeliveryConfirmationLabel  
 *      DeliveryStatus  
 *      EDeliveryFlag   
 *      DocPath  
 *      BatchDate  
 *      PrintDate  
 *      PrintEmpID  
 *      PrintRep  
 *      EmpId  
 *      Rep  
 *      CourtViolationStatus
 ********************************************************************************/  
  USE TrafficTickets
  go
 -- exec [USP_HTP_GET_CertifiedLetterOfRep] 'bulk1111111111111111'
  
--CREATE PROCEDURE [dbo].[USP_HTP_GET_CertifiedLetterOfRep] 
Alter PROCEDURE [dbo].[USP_HTP_GET_CertifiedLetterOfRep] 
(
    @CertifiedMailNumber  VARCHAR(20) = NULL,
    @FromDate             DATETIME = '1/1/1900',
    @ToDate               DATETIME = '12/31/9999',
    @Status               INT = 0,
    @ShowAll              BIT = 0,
    @FilterType           INT = 1,
    @SalesRep             VARCHAR(10) = NULL,
    @CourtId              INT = 0
)
AS
	IF (@FilterType = 3)
	BEGIN
	    SET @FromDate = CONVERT(VARCHAR, @FromDate, 101) + ' 00:00:00'  
	    SET @ToDate = CONVERT(VARCHAR, @ToDate, 101) + ' 23:59:59'
	END
	ELSE   
	IF (@FilterType = 5)
	BEGIN
	    SET @ToDate = CONVERT(VARCHAR, @ToDate, 101) + ' 23:59:59'
	END
	ELSE   
	IF (@FilterType = 2)
	    SET @ShowAll = 1
	ELSE   
	IF (@FilterType = 4)
	    SET @ShowAll = 0  
	
	DECLARE @DeliveredStatus SMALLINT  
	
	IF (@ShowAll = 1)
	BEGIN
	    SET @FilterType = 2 
	        --Sarim 5665 03/19/2009
	        -- SET @Status = 2
	END  
	
	IF @Status = 0
	    SET @DeliveredStatus = 0
	ELSE
	    SET @DeliveredStatus = 1 
	--Yasir Kamal 5948 06/09/2009 RowNo generated. 
	SELECT dbo.tblLORBatchHistory.ID,
	       RANK() OVER(ORDER BY dbo.tblLORBatchHistory.ID) RowNo,
	       dbo.tblLORBatchHistory.TicketID_PK,
	       dbo.tblLORBatchHistory.FullName,
	       dbo.tblLORBatchHistory.DOB,
	       dbo.tblLORBatchHistory.vacdate,
	       CASE 
	            WHEN LEN(ISNULL(dbo.tblLORBatchHistory.refcasenumber, '')) > 0 THEN 
	                 ISNULL(dbo.tblLORBatchHistory.refcasenumber, 'N/A')
	            ELSE 'N/A'
	       END AS refcasenumber,
	       dbo.tblLORBatchHistory.casenumassignedbycourt,
	       dbo.tblLORBatchHistory.languagespeak,
	       dbo.tblLORBatchHistory.currentdateset,
	       dbo.tblLORBatchHistory.attn,
	       dbo.tblLORBatchHistory.judgename,
	       dbo.tblLORBatchHistory.phone,
	       dbo.tblLORBatchHistory.courtname,
	       dbo.tblCourts.ShortName AS CourtShortName,
	       dbo.tblLORBatchHistory.address,
	       dbo.tblLORBatchHistory.cityzip,
	       dbo.tblLORBatchHistory.FAXNO,
	       dbo.tblLORBatchHistory.Attorneyname,
	       dbo.tblLORBatchHistory.firmsubtitle,
	       dbo.tblLORBatchHistory.RepDate,
	       dbo.tblLORBatchHistory.courtid,
	       dbo.tblLORBatchHistory.SettingRequest,
	       dbo.tblLORBatchHistory.causenumbers,
	       dbo.tblLORBatchHistory.BatchID_FK,
	       dbo.tblLORBatchHistory.dlnumber,
	       dbo.Fn_htp_get_InitCap(dbo.tblLORBatchHistory.viol_desc) AS viol_desc,	-- abid 5544 02/17/2009 column name was not provided  
	       dbo.tblLORBatchHistory.IsBond,
	       dbo.tblHTSBatchPrintLetter.USPSTrackingNumber,
	       --dbo.tblHTSBatchPrintLetter.USPSResponse,  
	       dbo.tblHTSBatchPrintLetter.DeliveryConfirmationLabel,
	       --dbo.tblHTSBatchPrintLetter.USPSResponseDelivery,  
	       CASE 
	            WHEN ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) = 0 THEN 
	                 'Pending'
	            ELSE 'Confirmed'
	       END AS DeliveryStatus,
	       --Ozair 5359 02/13/2009 included isnull check  
	       ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) AS EDeliveryFlag,
	       dbo.tblHTSBatchPrintLetter.DocPath,
	       --ozair 5665 03/20/2009 resolved sent date(print date) and upload date(confirmed date)issue
	       dbo.tblHTSBatchPrintLetter.BatchDate AS BatchDate,
	       dbo.tblHTSBatchPrintLetter.PrintDate AS PrintDate,
	       dbo.tblHTSBatchPrintLetter.USPSConfirmDate AS ConfirmDate,
	       -- abid 5569 02/23/2009 default set to system         
	       ISNULL(dbo.tblHTSBatchPrintLetter.PrintEmpID, 3992) AS PrintEmpID,
	       printUser.Abbreviation AS PrintRep,
	       -- abid 5569 02/23/2009 default value        
	       ISNULL(dbo.tblHTSBatchPrintLetter.EmpID, 3992) AS EmpID,
	       sendUser.Abbreviation AS Rep,
	       --ozair 5665 03/27/2009
	       CASE 
	            WHEN (EDeliveryFlag = 1 AND USPSResponse IS NULL) THEN 
	                 'Manually Uploaded'
	            ELSE ''
	       END AS ManualUpdate,
	       --Sabir Khan 5763 04/17/2009 LOR Subpoena and LOR Motion for Discovery...
	       dbo.tblcourts.IsLORSubpoena,
	       dbo.tblcourts.IsLORMOD,
	       dbo.tblcourts.PrecinctID,
	       dbo.tblcourts.courtcategorynum,
	       mc.CountyName,
	       -- Noufil 6551 09/09/2009 Court Violation Status category added.
	       tcvs.categoryid,
	       [dbo].[FN_GET_ConcatedRefCaseNumbersFromLORBatchHistory](dbo.tblLORBatchHistory.BatchID_FK) AS 
	       causenumbers1,
	       [dbo].[fn_HTP_Get_ContactNumber](dbo.tblLORBatchHistory.TicketID_PK) AS 
	       ClientPhoneNumber,
	       tt.Address1 + ' ' + ISNULL(tt.Address2, '') AS ClientAddress,
	       --Zeeshan Haider 11023 07/01/2013 Interpreter req doc. for Missouri City
	       tt.gender,
	       IsInterpreter = '0',
	       --Zeeshan Haider 11127 07/01/2013 Add Motion for discovery to LOR for Missouri City
	       IsMotionFD = '0'
	       INTO #tempLOR
	FROM   dbo.tblLORBatchHistory
	       -- Babar Ahmad 9660  included tbltickets table for client information	       
	       INNER JOIN tbltickets tt
	            ON  tt.TicketID_PK = dbo.tblLORBatchHistory.TicketID_PK
	       LEFT OUTER JOIN dbo.tblHTSBatchPrintLetter
	            ON  dbo.tblLORBatchHistory.BatchID_FK = dbo.tblHTSBatchPrintLetter.BatchID_PK
	       LEFT OUTER JOIN tblCourts
	            ON  dbo.tblLORBatchHistory.courtid = dbo.tblCourts.Courtid
	       LEFT OUTER JOIN tblCourtCategories cc
	            ON  dbo.tblcourts.courtcategorynum = cc.CourtCategorynum
	       LEFT OUTER JOIN tbl_Mailer_County mc
	            ON  cc.CountyID_FK = mc.CountyID
	       LEFT OUTER JOIN tblUsers sendUser 
	            -- abid 5569 02/23/2009 default value
	            ON  ISNULL(dbo.tblHTSBatchPrintLetter.EmpID, 3992) = sendUser.EmployeeID
	       LEFT OUTER JOIN tblUsers printUser 
	            -- abid 5569 02/23/2009 default set to system
	            ON  ISNULL(dbo.tblHTSBatchPrintLetter.PrintEmpID, 3992) = 
	                printUser.EmployeeID
	                -- Noufil 6551 09/09/2009 Court Violation Status category added.
	                
	       LEFT OUTER JOIN tblcourtviolationstatus tcvs
	            ON  dbo.tblLORBatchHistory.CourtViolationStatus = tcvs.CourtViolationStatusid
	WHERE  -- abid 5563 02/21/2009 show only show printed  
	       dbo.tblHTSBatchPrintLetter.IsPrinted = 1
	       AND (
	               @ShowAll = 1
	               OR (
	                      (
	                          (
	                              @FilterType = 1
	                              AND dbo.tblHTSBatchPrintLetter.USPSTrackingNumber 
	                                  = @CertifiedMailNumber
	                          )
	                          OR (
	                                 @FilterType = 3
	                                 AND -- abid 5563 02/21/2009 USPSConfirmDate criteria for certified records  
	                                     (
	                                         (
	                                             ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) 
	                                             = 0
	                                             AND @Status IN (0, 2)
	                                             AND dbo.tblHTSBatchPrintLetter.PrintDate 
	                                                 BETWEEN @FromDate 
	                                                 AND @ToDate
	                                         )
	                                         OR (
	                                                ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) 
	                                                = 1
	                                                AND @Status IN (1, 2)
	                                                AND ISNULL(dbo.tblHTSBatchPrintLetter.USPSConfirmDate, '01/01/1900')
	                                                    BETWEEN @FromDate 
	                                                    AND @ToDate
	                                            )
	                                     )
	                             )
	                          OR (
	                                 @FilterType = 5
	                                 AND dbo.tblHTSBatchPrintLetter.PrintDate <=
	                                     @ToDate
	                             )
	                      )
	                      AND (
	                              @Status = 2
	                              OR ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) 
	                                 = @DeliveredStatus
	                          )
	                  )
	               OR (
	                      @FilterType = 4
	                      AND (
	                              @Status = 2
	                              OR ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) 
	                                 = @DeliveredStatus
	                          )
	                  )
	           )
	       AND ISNULL(dbo.tblHTSBatchPrintLetter.USPSTrackingNumber, '') <> ''
	           --ozair 5665 03/27/2009 removed temp fix
	       AND (
	               (ISNULL(@SalesRep, '') = '')
	               OR sendUser.Abbreviation = @SalesRep
	           )
	       AND (@CourtId = 0 OR dbo.tblLORBatchHistory.courtid = @CourtId)
	           --Sarim 5665 03/19/2009
	       AND (
	               @Status = 2
	               OR ISNULL(dbo.tblHTSBatchPrintLetter.EDeliveryFlag, 0) = @status
	           )
	ORDER BY
	       dbo.tblLORBatchHistory.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause 
	
	--Zeeshan Haider 11023 07/01/2013 Interpreter req doc.
	UPDATE #tempLOR
	SET    IsMotionFD = '1'
	WHERE  Rowno IN (SELECT MAX(Rowno)
	                 FROM #tempLOR
	                 WHERE #tempLOR.courtid = 3068
	                 GROUP BY
	                        TicketID_PK)
	                        
	UPDATE #tempLOR
	SET    IsInterpreter = '1'
	WHERE  Rowno IN (SELECT MAX(Rowno)
	                 FROM #tempLOR
	                 WHERE #tempLOR.courtid = 3068
	                 AND UPPER(#tempLOR.languagespeak) <> 'ENGLISH'
	                 AND UPPER(#tempLOR.languagespeak) NOT LIKE '%CHOOSE%'
	                 GROUP BY
	                        TicketID_PK)
	
	SELECT *
	FROM   #tempLOR
	--Faique Ali 11023 07/11/2013 to set rpt data ordering 
	ORDER BY #tempLOR.ROwNo
	
	DROP TABLE #tempLOR
	
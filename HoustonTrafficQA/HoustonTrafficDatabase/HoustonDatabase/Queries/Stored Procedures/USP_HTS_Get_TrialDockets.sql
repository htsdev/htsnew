SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_TrialDockets]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_TrialDockets]
GO











--USP_HTS_Get_TrialDockets '3001', '3/2/2005',0,0,0

CREATE  procedure dbo.USP_HTS_Get_TrialDockets

	@courtloc varchar(10)  = 'None',
	@courtdate varchar(12)  = '01/01/1900',
	@page varchar(12) = 'TRIAL' ,
	@courtnumber varchar(6) = '0',
	@datetype varchar(20) = '0' 
as

set nocount on

declare  @temp table
(
	[TicketID_PK] [int]   ,
	[Firstname] [varchar] (20)  ,
	[MiddleName] [varchar] (6)  ,
	[Lastname] [varchar] (20)  ,
	[Ofirstname] [varchar] (50)   ,
	[Olastname] [varchar] (50)   ,
	[ShortDesc] [varchar] (20)  ,
	[CourtDate] [datetime]  ,
	[currentdateset] [datetime]  ,
	[currentcourtloc] [int]  ,
	[currentcourtnum] [int]  ,
	[description] [varchar] (4)  ,
	[activeflag] [tinyint]  ,
	[courtname] [varchar] (50)  ,
	[Address] [varchar] (50)  ,
	[trialcomments] [varchar] (500)  ,
	[bondflag] [tinyint]  ,
	[firmabbreviation] [varchar] (6)  ,
	[courttype] [tinyint]  ,
	[accflag] [int]   ,
	[speeding] [varchar] (10)  ,
	[courtarrdate] [varchar] (10)  ,
	[courttime] [int]  ,
	[ViolationStatusID] [varchar] (50)  ,
	[violationnumber] [smallint]   ,
	[address1] [varchar] (50)  ,
	[midnum] [varchar] (20)  ,
	[violationtype] [tinyint]  ,
	[courtaddress] [varchar] (50)  ,
	[courtviolationstatusid] [int]
) 
/*
declare  @temp1 TABLE 
(
	[ticketid] [int]   ,
	[activitytypeid] [tinyint]   
) 
*/

Declare @temp2 table ([ticketid] [int], [dueamount] [money])

declare @temp3 table
(
	[TicketID_PK] [int]   ,
	[Firstname] [varchar] (20)  ,
	[MiddleName] [varchar] (6)  ,
	[Lastname] [varchar] (20)  ,
	[Ofirstname] [varchar] (50)   ,
	[Olastname] [varchar] (50)   ,
	[ShortDesc] [varchar] (20)  ,
	[CourtDate] [datetime]  ,
	[currentdateset] [datetime]  ,
	[currentcourtloc] [int]  ,
	[currentcourtnum] [int]  ,
	[description] [varchar] (4)  ,
	[activeflag] [tinyint]  ,
	[courtname] [varchar] (50)  ,
	[Address] [varchar] (50)  ,
	[trialcomments] [varchar] (500)  ,
	[bondflag] [tinyint]  ,
	[firmabbreviation] [varchar] (6)  ,
	[courttype] [tinyint]  ,
	[accflag] [int]   ,
	[speeding] [varchar] (10)  ,
	[courtarrdate] [varchar] (10)  ,
	[courttime] [int]  ,
	[ViolationStatusID] [varchar] (50)  ,
	[violationnumber] [smallint]   ,
	[address1] [varchar] (50)  ,
	[midnum] [varchar] (20)  ,
	[violationtype] [tinyint]  ,
	[courtaddress] [varchar] (50)  ,
	[courtviolationstatusid] [int]  ,
--	[continuance] [tinyint]   ,
	[dueamount] [money]  
) 

declare @temp4 table 
(
	[TicketID_PK] [int],
	[Firstname] [varchar] (20),
	[MiddleName] [varchar] (6),
	[Lastname] [varchar] (20),
	[Ofirstname] [varchar] (50),
	[Olastname] [varchar] (50),
	[ShortDesc] [varchar] (20),
	[CourtDate] [datetime],
	[currentdateset] [datetime],
	[currentcourtloc] [int],
	[currentcourtnum] [int],
	[description] [varchar] (4),
	[activeflag] [tinyint],
	[courtname] [varchar] (50),
	[Address] [varchar] (50),
	[trialcomments] [varchar] (500),
	[bondflag] [tinyint]  ,
	[firmabbreviation] [varchar] (6)  ,
	[courttype] [tinyint]  ,
	[accflag] [int]  ,
	[speeding] [varchar] (10)  ,
	[courtarrdate] [varchar] (10)  ,
	[courttime] [int]  ,
	[ViolationStatusID] [varchar] (50)  ,
	[violationnumber] [smallint]  ,
	[address1] [varchar] (50)  ,
	[midnum] [varchar] (20)  ,
	[violationtype] [tinyint]  ,
	[courtaddress] [varchar] (50)  ,
	[courtviolationstatusid] [int]  ,
	--[continuance] [tinyint]  ,
	[dueamount] [money]  ,
	[clientcount] [int]  
)

insert into @temp
SELECT  distinct  
	t.TicketID_PK, 
	t.Firstname, 
	t.MiddleName, 
	t.Lastname, 
	ISNULL(o.FirstName, 'N/A') AS Ofirstname, 
	ISNULL(o.LastName, 'N/A') AS Olastname, 
	v.ShortDesc, 
	t.CourtDate, 
	tv.CourtDateMain AS currentdateset, 
	tv.CourtID AS currentcourtloc, 
	CONVERT(int, tv.CourtNumbermain) AS currentcourtnum, 
	cvs.ShortDescription AS description, 
	t.Activeflag, 
	c.CourtName, 
	c.Address, 
	trialcomments=  
	case left(cvs.Description,3)  
	when 'PRE' then t.pretrialcomments  
	else t.trialcomments end, 
	t.BondFlag, 
	f.FirmAbbreviation, 
	c.CourtType, 
	case tv.violationnumber_pk  
	when 11 then 1  
		 else 0  
	end as accflag, 
	dbo.GetViolationSpeed(tv.SequenceNumber, tv.RefCaseNumber, tv.FineAmount) AS speeding, 
	CONVERT(varchar(2), MONTH(tv.CourtDateMain)) + '/' + CONVERT(varchar(2), DAY(tv.CourtDateMain)) + '/' + CONVERT(varchar(4), YEAR(tv.CourtDateMain)) AS courtarrdate, 
	DATEPART([hour], tv.CourtDateMain) AS courttime, 
	cd.Description AS ViolationStatusID, 
	tv.ViolationNumber_PK AS violationnumber, 
	t.Address1, 
	t.Midnum, 
	v.Violationtype, 
	c.Address AS courtaddress, 
	cvs.CategoryID AS courtviolationstatusid
FROM         dbo.tblTickets t INNER JOIN
                      dbo.tblTicketsViolations tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN
                      dbo.tblCaseDispositionstatus cd ON tv.ViolationStatusID = cd.CaseStatusID_PK INNER JOIN
                      dbo.tblViolations v ON tv.ViolationNumber_PK = v.ViolationNumber_PK INNER JOIN
                      dbo.tblCourtViolationStatus cvs ON tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID INNER JOIN
                      dbo.tblCourts c ON tv.CourtID = c.Courtid LEFT OUTER JOIN
                      dbo.tblFirm f ON t.FirmID = f.FirmID LEFT OUTER JOIN
                      dbo.tblOfficer o ON t.OfficerNumber = o.OfficerNumber_PK
WHERE     (v.Violationtype NOT IN (1)or v.violationnumber_pk in (11,12))
		 AND (cvs.CategoryID IN
                          (SELECT DISTINCT categoryid
                            FROM          tblcourtviolationstatus
                            WHERE      violationcategory IN (0, 2) AND categoryid NOT IN (50))) AND (t.Activeflag = 1) AND (t.CurrentCourtloc <> 0)

--select * from @temp

if @@rowcount > 0   
begin  

insert into @temp2
select  ticketid, totalfeecharged-sum(ChargeAmount) as dueamount  
	from tblticketspayment A,tbltickets T 
where   T.ticketid_pk = A.ticketid  and activeflag = 1  and paymenttype not in (99,100)  
	group by ticketid, totalfeecharged  


declare @sqlquery varchar(2000)  
set @sqlquery = ' '  


insert into @temp3
select 	T.*, dueamount 
FROM @temp T left outer join @temp2 T2 on T.ticketid_pk = T2.ticketid   



insert into @temp4
SELECT  T3.*,
	count(*) as clientcount 
FROM         @temp3 T3
	RIGHT OUTER JOIN  
		tblTickets T ON T3.Firstname = T.Firstname 
		AND T3.Lastname = T.Lastname 
		AND (T3.Address1 = T.Address1 or T3.midnum = T.midnum)  
	GROUP BY   
		T3.TicketID_PK,
		T3.Firstname,
		T3.MiddleName,
		T3.Lastname,
		T3.Ofirstname,
		T3.Olastname,
		T3.ShortDesc,
		T3.CourtDate,
		T3.currentdateset,
		T3.currentcourtloc,
		T3.currentcourtnum,
		T3.Description,
		T3.activeflag,
		T3.courtname,
		T3.Address,
		T3.trialcomments,
		T3.bondflag,
		T3.firmabbreviation,
		T3.courttype,
		T3.accflag,
		T3.speeding,
	--	T3.continuance,
		T3.dueamount,
		T3.courtarrdate,
		T3.courttime,
		T3.ViolationStatusID,
		T3.violationnumber,
		T3.address1,
		T3.midnum,
		T3.violationtype,
		T3.courtaddress,
		T3.courtviolationstatusid  

set @sqlquery = 
'select 
	P.TicketID_PK,
	P.Firstname,
	P.MiddleName,
	P.Lastname,
	P.Ofirstname,
	P.Olastname,
	replace(P.ShortDesc,''None'',''Other'') as shortdesc,
	P.CourtDate,
	P.currentdateset,
	P.currentcourtloc,
	P.currentcourtnum,
	P.Description,
	P.activeflag,
	P.courtname,
	P.Address,
	P.trialcomments,
	P.bondflag,
	P.firmabbreviation,
	P.courttype,
	P.accflag,
	P.speeding,
	--P.continuance,
	P.dueamount,
	P.courtarrdate,
	P.courttime,
	P.ViolationStatusID,
	P.violationnumber,
	P.clientcount,
	T.coveringfirmid,
	F.FirmAbbreviation as coveringfirm,
	P.violationtype,
	courtaddress,
	cdlflag,
	courtviolationstatusid,
	convert(varchar(20),P.TicketID_PK) + isnull(convert(varchar(4),courtviolationstatusid),0) as ticketstatusid,   
	pretrialstatus =  case  
			when pretrialstatus = 1 and T.currentcourtloc between 3001 and 3022 and left(Description,3) <> ''JUR'' then ''RFT''  
			when pretrialstatus =2 and  left(Description,3) <> ''JUR'' then ''WBT''  
			when pretrialstatus =4 and  left(Description,3) <> ''JUR'' then ''DA''  
			when pretrialstatus =5 and  left(Description,3) <> ''JUR'' then ''DSC''  
			else '' '' end  
  
into 
	#temp5 
from 
	@temp4 P,
	dbo.tbltickets T,
	dbo.tblfirm F  
	where 
		P.ticketid_pk = T.ticketid_pk 
		and T.coveringfirmid = F.firmid 
		and P.ticketid_pk is not null '  

if @courtloc is not null and @courtloc <> 'None'    
begin  
	if @courtloc = 'JP' or @courtloc = 'OU'  
		if @courtloc = 'JP'  
			begin  
				set @sqlquery = @sqlquery + ' and P.currentcourtloc not in (3001,3002,3003)'  
			end  
		else  
			begin  
				set @sqlquery = @sqlquery + ' and P.currentcourtloc in (3001,3002,3003)'  
			end  
	ELSE  
		begin  
			set @sqlquery = @sqlquery + ' and P.currentcourtloc = ' + @courtloc  
		end  
	end  
	if @courtdate is not null and @courtdate <> '01/01/1900'  
		begin  
			 set @sqlquery = @sqlquery +  ' and datediff(day,P.currentdateset,'''  + @courtdate + ''') = 0'  
		end  
	if @courtnumber <> '0'  
		begin  
			 set @sqlquery = @sqlquery +  ' and P.currentcourtnum = ' + @courtnumber  
		end  
	if @datetype <> '0'  
		begin  
			set @sqlquery = @sqlquery +  ' and courtviolationstatusid in (' + @datetype + ')'  
		end  	
	set @sqlquery = @sqlquery +  ' and P.ShortDesc not in (''CDL'',''ACC'',''PROB'') order by P.courtarrdate,P.currentcourtloc,P.Address,convert(int,P.currentcourtnum) asc,P.courttime,P.lastname,P.firstname,P.middlename '  
  	set @sqlquery = @sqlquery +  'select * into #violations from tblviolations where description like ''%speeding%'' and violationtype <> 0 '  
  
	set @sqlquery = @sqlquery +  'select T.*, isnull(V.violationnumber_pk,0) as speedingvnum from #temp5 T left outer join #violations V on T.violationnumber = V.violationnumber_pk order by case T.courtviolationstatusid  
	when 9 then 9  
	when 0 then 9  
	else 1 end  
	T.courtdate,T.courtarrdate,T.currentcourtloc,T.Address,convert(int,T.currentcourtnum) asc,T.courttime,T.lastname,T.firstname,T.middlename,T.courtviolationstatusid drop table #temp5'  
print @sqlquery  
exec(@sqlquery)  
end  











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/*
* Created by : Noufil Khan against 6052
* Business Logic : This method insert document information in tblhtsnotes and in history of client
* 
*/

CREATE PROCEDURE [dbo].[USP_HTP_INSERT_TBLHTSNOTES]
	@BatchID INT,
	@ticketid INT,
	@PrintDate DATETIME,
	@LetterID_FK INT,
	@EmpID INT,
	@LetterName VARCHAR(200),
	@DocPath VARCHAR(120),
	@SUBJECT VARCHAR(200)
AS
	DECLARE @RecordID VARCHAR(12)
	
	INSERT INTO [tblHTSNotes]
	  (
	    [BatchID_PK],
	    [TicketID_FK],
	    [PrintDate],
	    [LetterID_FK],
	    [EmpID],
	    [Note],
	    [DocPath]
	  )
	VALUES
	  (
	    @BatchID,
	    @ticketid,
	    @PrintDate,
	    @LetterID_FK,
	    @EmpID,
	    @LetterName,
	    @DocPath
	  )
	
	
	SET @RecordID = CONVERT(VARCHAR(12), SCOPE_IDENTITY())
	
	DECLARE @lnk VARCHAR(200)
	SET @lnk = '<a href="javascript:window.open(''frmPreview.aspx?RecordID=' + @RecordID 
	    + ''');void('''');">' + @SUBJECT + '</a>'
	
	INSERT INTO tblTicketsNotes
	  (
	    TicketID,
	    SUBJECT,
	    EmployeeID
	  )
	VALUES
	  (
	    @ticketid,
	    @lnk,
	    @EmpID
	  )
GO


GRANT EXECUTE ON [dbo].[USP_HTP_INSERT_TBLHTSNOTES] TO dbr_webuser
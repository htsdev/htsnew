SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetPricingTemplates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetPricingTemplates]
GO










--Getting Pricing Templates by Template ID
CREATE PROCEDURE usp_HTS_GetPricingTemplates
@TemplateID as int = 0

AS

if ( @TemplateID>0 ) --if Template exsists
	begin
		SELECT     TemplateID, TemplateName, TemplateDescription, InsertDate, LastUpdatedDate, EmployeeId
		FROM         dbo.tblPricingTemplate
		WHERE     (TemplateID = @TemplateID)
	end
else --if Template ID doesn't exsists, Display all
	begin
		SELECT     TemplateID, TemplateName, TemplateDescription, InsertDate, LastUpdatedDate, EmployeeId
		FROM         dbo.tblPricingTemplate
	end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


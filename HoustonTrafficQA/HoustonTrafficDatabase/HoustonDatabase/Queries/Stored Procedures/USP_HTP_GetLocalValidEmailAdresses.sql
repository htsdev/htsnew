/****** Object:  StoredProcedure [dbo].[USP_HTP_GetLocalValidEmailAdresses]    Script Date: 07/21/2008 13:02:57 ******/

/**
Business Logic: This Procedure take Email Addresses and return only those email address that exist in Database
Parameter: Email Addresses 
Column Return : Valid Email (Return Only THose Email That Exist in Database and remove remaining
**/

-- Noufil 4407 07/21/2008 Return Valid Email Address

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[USP_HTP_GetLocalValidEmailAdresses]

@EmailAddresses varchar(1000)
as

if  Right(@EmailAddresses,1) != ','
	begin
	set	@EmailAddresses=@EmailAddresses+','		
	end


declare @result as varchar(1000)


select   @result = isnull(@result,'') + maincat + ','  from dbo.Sap_String_Param_forstring (@EmailAddresses)
where Rtrim(Ltrim(maincat)) in (select emailaddress from localemailaddress)

select substring(isnull(@result,''),0,len(@result))as ValidEmail


go

grant execute on  [dbo].[USP_HTP_GetLocalValidEmailAdresses] to dbr_webuser


go
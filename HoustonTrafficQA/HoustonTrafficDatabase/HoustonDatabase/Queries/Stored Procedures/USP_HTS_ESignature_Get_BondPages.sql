set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

 
/*
Created by:  Muhammad Adil Aleem

Business Logic : this procedure is used to get all the bond document pages with there names according to No. of violations.
				 It helps application to locate signatures correctly at each page of bond document.

List of Parameters:	
	@NoOfViolations : No. of violations of any particular case.

List of Columns: 	
	Page Number : Sr. No. of a page.
	Page Description : Title of the page.
	Violation Number : Sr. No. of violations at violation page.
*/
ALTER procedure [dbo].[USP_HTS_ESignature_Get_BondPages] 
@NoOfViolations as int
As

Begin
	declare @LoopCounter as int
	declare @PageNumber as int

	Set @PageNumber = 0
	Set @LoopCounter = 1

	create table #Tbl_BondPageDesc_Temp (PageNumber int, PageDescription varchar(30), ViolationNumber int)
	while @LoopCounter <= @NoOfViolations
	Begin
		Set @PageNumber = @PageNumber + 1
		insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'ViolationPage', @LoopCounter)
		Set @LoopCounter = @LoopCounter + 1
	End
--Fahad 5806 04/27/2009 Remove pages fron Bond Reports
--	Set @PageNumber = @PageNumber + 1
--	insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'ClientInitialsPage', 0)
	-- 08/20/2008 Adil 4633
	--Set @PageNumber = @PageNumber + 1
	--insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'ContractPage', 0)

--	Set @PageNumber = @PageNumber + 1
--	insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'PowerOfAttorneyPage', 0)

--	Set @LoopCounter = 1
--	while @LoopCounter <= @NoOfViolations
--	Begin
--		Set @PageNumber = @PageNumber + 1
--		insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'ViolationPage', @LoopCounter)
--		Set @LoopCounter = @LoopCounter + 1
--	End

	Set @PageNumber = @PageNumber + 1
	insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'ViolationPage', 0)
	Set @PageNumber = @PageNumber + 1
	insert into #Tbl_BondPageDesc_Temp values(@PageNumber, 'PleaOfNotGuilty', 0)

	Select * from #Tbl_BondPageDesc_Temp order by PageNumber
	drop table #Tbl_BondPageDesc_Temp
End











SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Bug_GetBugs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Bug_GetBugs]
GO



--usp_Bug_GetBugs 3991      
CREATE procedure [dbo].[usp_Bug_GetBugs] --4001      
@EmployeeID as int      
      
as      
      
select b.bug_id,      
    b.shortdescription,      
    b.statusid,      
       b.priorityid,      
    convert(varchar(12),b.posteddate,101) as posteddate,      
    p.priority_name,      
    s.status_name,  
 b.ticketid as TicketNumber       
from tbl_bug b inner join       
  tbl_bug_priority p on       
  b.priorityid=p.priority_id inner join       
  tbl_bug_status s on       
  b.statusid=s.status_id        
where       
b.employeeid=@EmployeeID      
    
order by b.bug_id desc    
      
      
   





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


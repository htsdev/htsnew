USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GetTextMessageNumber]    Script Date: 11/12/2008 01:16:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Sabir Khan  5084   
-- Create date: 11/10/2008    
-- Description:  Get the Text Message Number and NoticePeriodDays    
--    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_HTP_GetTextMessageNumber]    
 @TicketID int    
AS    
BEGIN  
select distinct top 1 
	isnull(m.SendTextMessage,0) as sendtextmessage,  
	isnull(m.textmessagenumber,'')  as textmessagenumber,
	--Sabir Khan 5298 12/03/2008 senderemail column has been added...
    isnull(m.SenderEmail,'') as SenderEmail,      
	isnull(m.NoticePeriodDays,0) as noticeperioddays  ,
	v.courtdatemain as 'CourtDateMain',
	v.courtnumbermain as 'CourtNumberMain'
	
from tblticketsviolations v   
left outer join tblfirm m on m.firmid = v.coveringfirmid  
where v.ticketid_pk = @ticketid  
and datediff(day, v.courtdatemain, getdate()) < 0
order by v.courtdatemain 
  
END    
 
GO

Grant Execute on [dbo].[USP_HTP_GetTextMessageNumber] to dbr_webuser

GO 
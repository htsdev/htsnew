/**    
Business logic : This report take store procedures execute it and return its XML which can be shown in HTML.  
Parameter :     
   @EmailAddress : Email adress to which this report will send to    
--Yasir Kamal 5460 02/17/2009 Email Alert/Report Summary   
   @DisplayReport : if 1 then it should only display Report.  
     if 0 then it should Email Report.  
   @Detail : if 0 then it should email or display only the first 2 tables that are Alert Summary and Report Summary.  
             if 1 then it should display or email whole Report.  
   @ThisResultSet : This is a output parameter which returns the XML   
Column Return:    
   @ThisResultSet : contain XML  
**/    
-- [USP_HTS_ValidationEmailReportHouston] 1,'',1,1,'15','',1      

ALTER PROCEDURE [dbo].[USP_HTS_ValidationEmailReportHouston] 
(
    --Yasir Kamal 5460 02/17/2009 Email Alert/Report Summary   
    @Detail         INT = 1,
    --5460  end        
    @EmailAddress   VARCHAR(500) = '',
    @DisplayReport  BIT = 0,
    @isRunSchedule  BIT = 0,
    @TimeMargin     VARCHAR(10) = 15,
    @ThisResultSet  VARCHAR(MAX) = '' OUTPUT,
    @DebugMode      BIT = 0
)
AS
	-------------------------------------------------------------------------------------------------------------------------------------------    
	DECLARE @currentTime VARCHAR(20)  
	SET @currentTime = (CONVERT(VARCHAR, RTRIM(LTRIM(RIGHT(GETDATE(), 7)))))  
	SET @currentTime = REPLACE(@currentTime, 'PM', ' PM')
	SET @currentTime = REPLACE(@currentTime, 'AM', ' AM')
	
	DECLARE @dumpid INT
	DECLARE @tempvar VARCHAR(MAX)
	DECLARE @tempvar1 VARCHAR(MAX)
	SET @dumpid = 0
	SET @tempvar = 'outside'
	SET @tempvar1 = ''
	
	DECLARE @SerialHyperlink VARCHAR(1000)  
	SET @SerialHyperlink = 'ln.legalhouston.com'  
	
	IF (@EmailAddress = '')
	BEGIN
	    DECLARE @ServerIP VARCHAR(100)    
	    CREATE TABLE #tempIP
	    (
	    	Results VARCHAR(1000) NULL
	    ) 
	    BULK 
	    INSERT #tempIP 
	           FROM 'C:\Program Files\Microsoft SQL Server\SQLServerIP.txt'
	    SELECT @ServerIP = results
	    FROM   #tempIP 
	    
	    DROP TABLE #tempIP    
	    
	    
	    IF @ServerIP = '66.195.101.51' --Live Settings
	    BEGIN
	        SELECT @EmailAddress = 'hvalidation@sullolaw.com'  
	        SET @SerialHyperlink = 'ln.legalhouston.com'
	    END
	    ELSE 
	    IF @ServerIP = '192.168.168.220' --Developer Settings
	    BEGIN
	        SELECT @EmailAddress = 'dev@lntechnologies.com'  
	        SET @SerialHyperlink = 'newpakhouston.legalhouston.com'
	    END
	    ELSE
	        -- QA Settings
	    BEGIN
	        SELECT @EmailAddress = 'QA@lntechnologies.com'  
	        SET @SerialHyperlink = 'qa.houston.com'
	    END
	END 
	--------------------------------------------------------------------------------------------------------------------------------------------------    
	DECLARE @SessionID VARCHAR(500)
	SELECT @SessionID = NEWID() 
	--SET @SessionID= @SessionID +'_' + CONVERT(VARCHAR,GETDATE()) 
	DECLARE @ThisResultalertsummary VARCHAR(MAX)
	DECLARE @ThisResultpendingsummary VARCHAR(MAX) 
	--TRUNCATE TABLE summaryreport    
	DECLARE @ThisResultSetalert VARCHAR(MAX)    
	DECLARE @ThisPendingCount VARCHAR(5)    
	DECLARE @ThisAlertCount VARCHAR(5)    
	DECLARE @ThisTotalCount VARCHAR(5)   
	DECLARE @errormessage VARCHAR(1000)   
	DECLARE @tempReportHtml TABLE (ReportID INT, ReportHtml VARCHAR(MAX))  
	SET @ThisTotalCount = 0 
	--DECLARE @isRunSchedule BIT
	DECLARE @tempEmailSchedule TABLE(
	            id INT IDENTITY(1, 1),
	            Userid INT,
	            ReportId INT,
	            DayId INT,
	            Email VARCHAR(50),
	            Timings VARCHAR(20),
	            Title VARCHAR(500),
	            Username VARCHAR(20),
	            UserType INT,
	            category INT,
	            STATUS BIT
	        )
	
	DECLARE @tempUser TABLE (
	            ID INT IDENTITY(1, 1),
	            UserID INT,
	            EmailAddress VARCHAR(200),
	            usertype INT
	        )
	
	DECLARE @tempScheduleReport TABLE (
	            ReportID INT,
	            title VARCHAR(100),
	            category INT,
	            DayID INT,
	            Timings VARCHAR(20),
	            UserType INT,
	            primaryUser VARCHAR(100),
	            secondaryUser VARCHAR(100)
	        )
	
	DECLARE @reportEmailTime VARCHAR(20)
	DECLARE @reportEmailDay INT
	DECLARE @errorDesc VARCHAR(500)    
	DECLARE @Counter INT     
	DECLARE @CurrentDay INT
	SET @CurrentDay = DATEPART(dw, GETDATE())
	
	--SET @isRunSchedule='1'
	SET @ThisResultalertsummary = ''
	SET @ThisResultpendingsummary = ''
	
	
	-- SAEED 7791 06/26/2010 Code for email scheduling.
	SET @reportEmailTime = (CONVERT(VARCHAR, RTRIM(LTRIM(RIGHT(GETDATE(), 7)))))  
	SET @reportEmailTime = REPLACE(@reportEmailTime, 'PM', ' PM')
	SET @reportEmailTime = REPLACE(@reportEmailTime, 'AM', ' AM')
	SET @reportEmailDay = DATEPART(dw, GETDATE()) 
	
	--Checking weather current day is weekday or saturday
	DECLARE @isweekday BIT
	DECLARE @Daynumber INT
	SELECT @Daynumber = DATEPART(dw, GETDATE()) 
	IF @Daynumber = 7
	BEGIN
	    SET @isweekday = 0
	END
	ELSE
	BEGIN
	    SET @isweekday = 1
	END 
	
	/*
	* Insert list of all active reports in @tempScheduleReport table with primary/secondory user, if any report is associated with the user.
	* */
	INSERT INTO @tempScheduleReport
	SELECT DISTINCT sub.ID AS ReportID,
	       CASE sub.Category
	            WHEN 1 THEN sub.TITLE + ' (Alert)'
	            ELSE sub.TITLE + ' (Report)'
	       END title,
	       ISNULL(sub.Category, 2) AS category,
	       vr.DayNumber AS DayID,
	       et.TimingValue AS Timings,
	       vr.UserType,
	       CASE DayNumber
	            WHEN DATEPART(dw, GETDATE()) THEN CASE ISNULL(vr.UserType, -1)
	                                                   WHEN 0 THEN 
	                                                        'Primary User: ' + CASE 
	                                                                                ISNULL(vr.UserID, - 1)
	                                                                                WHEN 
	                                                                                     -
	                                                                                     1 THEN 
	                                                                                     'N/A'
	                                                                                ELSE 
	                                                                                     UPPER(ISNULL(username, 'N/A'))
	                                                                           END
	                                              END
	            ELSE 'Primary User: N/A'
	       END AS PrimaryUser,
	       CASE DayNumber
	            WHEN DATEPART(dw, GETDATE()) THEN CASE ISNULL(vr.UserType, -1)
	                                                   WHEN 1 THEN 
	                                                        ' , Secondary User: ' 
	                                                        + CASE ISNULL(vr.UserID, -1)
	                                                               WHEN -1 THEN 
	                                                                    'N/A'
	                                                               ELSE UPPER(ISNULL(username, 'N/A'))
	                                                          END
	                                              END
	            ELSE ' , Secondary User: N/A'
	       END AS SecondaryUser
	FROM   dbo.tblUsers
	       INNER JOIN dbo.ValidationReportSettings AS vr
	            ON  dbo.tblUsers.EmployeeID = vr.UserID
	       RIGHT OUTER JOIN dbo.tbl_TAB_SUBMENU AS sub
	       LEFT OUTER JOIN dbo.ValidationReportScheduling AS vrs
	            ON  sub.ID = vrs.ReportID
	       LEFT OUTER JOIN dbo.ValidationEmailTimings AS et
	            ON  vrs.EmailTimingsID = et.TimingID
	            ON  vr.ReportID = sub.ID
	WHERE  sub.active = 1 
	--AND tblusers.[Status]=1
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	DECLARE @ReportTitle VARCHAR(1000)
	DECLARE @ReportCategory INT
	DECLARE @SpParam VARCHAR(100)
	DECLARE @ReportUserName VARCHAR(200)
	DECLARE @OldReportTitle VARCHAR(200)
	DECLARE @UserNameFormat VARCHAR(200)
	SET @ReportTitle = ''
	SET @SpParam = ''
	SET @ReportUserName = ''
	SET @OldReportTitle = ''
	SET @UserNameFormat = 'Primary User: N/A , Secondary User: N/A'
	/*---------------------------------------------------------------------------------------------------------------------------------------------------  
												Potential Bond Forfeiture Start
	--------------------------------------------------------------------------------------------------------------------------------------------------- */
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 297
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Afaq 8521 12/13/2010 Add new report of Potential Bond Forfeiture
	        DECLARE @ThisPotentialBondForfeiture VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''  
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 297
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 297
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 297
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	         EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_htp_get_potentialBond', 
	        '1', 
	        @ReportTitle, 
	        'TicketID_PK,RefCaseNumber as [Ticket Number],casenumassignedbycourt [Cause Number], Lastname [Last Name], Firstname [First Name],ShortName As Court,CourtDateMain  As [Crt Date & Time],ViolationDescription As [Violations],(contact1+''<br/>''+ contact2+''<br/>'' + contact3) as [Contact Info]' , 
	        1,
	        -- Kashif Jawed 8771 03/07/2011 Pass '[Crt Date & Time]' for sorting
	        '[Crt Date & Time]',
	        @SerialHyperlink,@ResultSet = @ThisPotentialBondForfeiture  
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT,@ShowHyperLink=1,@Removecolumn='Follow Up Date'     
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                297,
	                @ThisPotentialBondForfeiture
	              )
	        END 
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             297,
	             @SessionID
	        
	        PRINT('Potential Bond Forfeiture')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisPotentialBondForfeiture = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_htp_get_potentialBond '
	    END CATCH
	END
	/*---------------------------------------------------------------------------------------------------------------------------------------------------  
												Potential Bond Forfeiture End
	--------------------------------------------------------------------------------------------------------------------------------------------------- */
	-- SAEED 7791 06/26/2010 please view this case[having report id=154] for the changes done for the task 7791, the same changes also apply to all other cases.  
	
	-- SAEED 7791 06/26/2010 if exist check apply to all cases. If @isRunSchedule=0 the report will run for validation summary as it was running earlier.
	-- If @isRunSchedule=1 then report will run from sql job, and only those reports will be process which are scheduled for particular day & time.
	
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 154
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- SAEED 7791 07/12/2010, append primary/secondary user name with the report if report is associated with any particular user, other wise diaplay 'N/A' 
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 154
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 154
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 154
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_ALR_Request_Alert', 
	        @SpParam, --SAEED 7791 07/12/2010 the new parameter[@ValidationCategory] is added in procedures of all reports which has followup date column,passing the report category[1=Alert,2=Report] paramter from here.
	        @ReportTitle, --SAEED 7791 07/12/2010 primary/secondary user name is appended with report title,if report is associated with any user for particular day, otherwise append 'N/A' for primary & secondary user.
	        'ticketid_pk,ticketnumber as [Ticket Number],lname as [Last Name],fname as [First Name],courtdatemain as [Crt.Date/Time], courtnumber as [Crt.No],status as [Status],shortname as [Crt.Name]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetalert OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1 --SAEED 7791 07/12/2010 if report is scheduled to send email then adding that report html in temporary table for later use.
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                154,
	                @ThisResultSetalert
	              )
	        END 
	        -- For Summary    
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             154,
	             @SessionID --SAEED 7791 07/12/2010 passing actual report name[without appending primary/secondary user] which will display in 'Alert' & 'Report' summary. Also passing the report id in last parameter which is use for scheduling.
	        
	        PRINT('HCCC ALR Request')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetalert = ''   
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_ALR_Request_Alert ' --SAEED 7791 07/12/2010 passing updated report title having primary/secondary user name appended with report title.
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 137
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetHCJP VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''  
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 137
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 137
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 137
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = '1'	--Saeed 8101 09/28/2010 display only those recoeds whose follow up date is null,past or today.
	        
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_hts_outsidecases', 
	        @SpParam, 
	        @ReportTitle, 
	        'ticketid_pk,bondflag as [Bond], refcasenumber as [Cause Number],lastname as [Last Name],firstname as [First Name],shortname as [Crt],autostatus as [A],autocourtdate as [A date],verifiedcourtstatus as [V],verifiedcourtdate as [Vdate],waittime as [Days]  ,LORPrintDate as [LOR],FollowUpDate as [Follow Up Date]' ,  --Saeed 8101 09/28/2010 follow up date column added.
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetHCJP OUTPUT, @PendingCount -- Saeed 8101 09/28/2010 sort records by follow up date
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT 
	        -- For Summary     
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                137,
	                @ThisResultSetHCJP
	              )
	        END
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             137,
	             @SessionID
	        
	        PRINT('HCJP Auto Update ')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetHCJP = ''  
	        SET @ThisResultSetalert = ''   
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_hts_outsidecases '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	--Fahad 06/13/2013 10675 moved the discrepency report after HCJP Auto update alert
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 158
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Yasir Kamal 5330 12/17/2008  move report at the bottom of the alert Summary report
	        --Sabir Khan 5977 07/06/2009 Follow Up Date has been added to HMC Setting Discrepancy   
	        DECLARE @ThisResulthmcsetting VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 158
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 158
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 158
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_hts_hmcsettingdiscrepancy', 
	        '1', 
	        @ReportTitle, 
	        'ticketid_pk,causenumber as [Cause No.], lastname as [Last Name],firstname as [First Name],Vdesc as [V Status],courtdatemain as [V Date],courttimemain as [V Time],courtnumbermain as [V Room],Adesc as [A Status],courtdate as [A Date],courttime as [A Time],courtnumber as [A Room], followupdate as [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResulthmcsetting 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                158,
	                @ThisResulthmcsetting
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             158,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResulthmcsetting = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_hts_hmcsettingdiscrepancy '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 151
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetHMC_DLQ VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 151
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 151
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 151
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ''
	        
	        
	        --Zeeshan 4679 09/04/2008 Add DOB Column In The Report    
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        --Yasir 5423 02/25/2009 cases having follow up date in past Including Today   
	        'traffictickets.dbo.usp_hts_HMC_AW_DLQ_Alert', 
	        ''''''''',1,0 ', 
	        @ReportTitle, 
	        'ticketid_pk,causeno as [Cause No],ticketnumber as [Ticket No],lname as [Last Name], fname as [First Name], Dob as [Dob] ,courtdatemain as [Crt Date & Time],arrwaitdate as [ArrWaitDate & Time],FollowUp as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetHMC_DLQ 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT 
	        -- For Summary     
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                151,
	                @ThisResultSetHMC_DLQ
	              )
	        END
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             151,
	             @SessionID
	        
	        PRINT('HMC A/W DLQ')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetHMC_DLQ = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_hts_HMC_AW_DLQ_Alert '
	    END CATCH
	END
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 152
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4980 11/04/2008 Follow update column added
	        -- Noufil 5131 11/13/2008 Show past followUpdate  
	        DECLARE @ThisResultSetbond VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 152
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 152
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 152
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ''
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        --Yasir 5423 02/25/2009 cases having follow up date in past Including Today
	        --Sabir Khan 03/26/2010 show all parameter has been sent unchecked...  
	        'traffictickets.dbo.USP_HTS_Get_BondWaitingViolations', 
	        '0,0', 
	        @ReportTitle, 
	        'ticketid_pk,bondflag as [Bond], refcasenumber as [Cause Number],lastname as [Last Name],firstname as [First Name],shortname as [Crt],courtnumber as [Crt.No],verifiedcourtstatus as [Status],daysover as [Days],bonddate as [Bond Waiting Date],bondwaitingfollow as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetbond 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                152,
	                @ThisResultSetbond
	              )
	        END 
	        
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             152,
	             @SessionID
	        
	        PRINT('HMC Bond Waiting')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetbond = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_Get_BondWaitingViolations '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Waqas 6509 09/18/2009 Added Removing columns parameter  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 142
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetreminder VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 142
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 142
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 142
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTS_reminderCalls_For_ValidationReport', 
	        @SpParam , 
	        @ReportTitle, 
	        'ticketid as Ticketid_pk,fullname as [Name], languagespeak as [Language],bondflag as [Bond],comments as [Comments],shortname as [Crt Loc],insurance as [Insurance],trialdesc as [Trail Desc], CourtDateMain as [Court]' , 
	        1,'[Court]',@SerialHyperlink,@ResultSet = @ThisResultSetreminder 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT , @Removecolumn = 
	        '[Court]'   
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                142,
	                @ThisResultSetreminder
	              )
	        END        
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             142,
	             @SessionID
	        
	        PRINT('Reminder Call Alert')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetreminder = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_reminderCalls_For_ValidationReport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Ozair 7281 01/21/2010 Past Due Calls Report removed as it is already being displayed case type wise
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------      
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 208
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 5618 04/02/2009 Past Due calls Report added  
	        DECLARE @ThisResultPaymentDueCallsReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = '' 
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 208
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 208
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 208
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ''
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_Payment_Due_Calls', 
	        '', 
	        @ReportTitle, --Nasir 6049 07/15/2009 add crt type change and change column name  
	        'ticketid_pk, ClientName as Client,crt as Crt,CrtType as [Crt Type],CourtDate as [Crt Date], ''$''+convert(varchar(10),(convert(int,TotalFeeCharged))) as [Total Fee], ''$''+convert(varchar(10),(convert(int,Owes))) as [Amt Due], PaymentDate as [Payment Due],PaymentFollowUpdate as [Follow Up Date],Remainingdays as [Remaining Days]', 	        
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultPaymentDueCallsReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                208,
	                @ThisResultPaymentDueCallsReport
	              )
	        END
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             208,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultPaymentDueCallsReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_Payment_Due_Calls '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------      
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 150
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- -- Noufil 5691 04/06/2009 Past Court Date (HMC) Report added  
	        DECLARE @ThisResultSetpast VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''  
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 150
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 150
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 150
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_PastCourtDateForHMC', 
	        '1', 
	        @ReportTitle, 
	        'ticketid_pk,ticketnumber as [Ticket No],fname as [First Name], lname as [Last Name], courtdatemain as [Crt DateTime],courtnumber as [Crt No],shortname as [Crt. Loc],status as [Trial Category],convert(varchar(20),PastCourtDateFollowUpDate,101) as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetpast 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                150,
	                @ThisResultSetpast
	              )
	        END 
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             150,
	             @SessionID
	        
	        PRINT('Past Court Date')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetpast = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_PastCourtDateForHMC '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 209
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- -- Noufil 5691 04/06/2009 Past Court Date (Other) Report added  
	        DECLARE @ThisResultSetpastother VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = '' 
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 209
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 209
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 209
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_PastCourtDateForNonHMC', 
	        '1', 
	        @ReportTitle, 
	        'ticketid_pk,ticketnumber as [Ticket No],fname as [First Name],lname as [Last Name],courtdatemain as [Crt DateTime],courtnumber as [Crt No],shortname as [Crt. Loc],status as [Trial Category],convert(varchar(20),PastCourtDateFollowUpDate,101) as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetpastother 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                209,
	                @ThisResultSetpastother
	              )
	        END 
	        
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             209,
	             @SessionID
	        
	        PRINT('Past Court Date')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetpastother = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_PastCourtDateForNonHMC '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Fahad 6638 11/10/2009 Below three reports added
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 271
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Fahad 6638 11/10/2009 Past Due Calls(Traffic)
	        -- Sabir Khan 7497 06/24/2010 added validation parameter... 
	        DECLARE @ThisResultSetPastDueCallsTraffic VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = '' 
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 271
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 271
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 271
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_Past_Due_Calls', 
	        '1,1', 
	        @ReportTitle, 
	        --Ozair 7281 01/21/2010 date format issue resolved   
	        'TicketID_PK,ClientName as [Client],Crt as [Crt],CrtType as [Crt Type],convert(varchar(10),CourtDate,101) as [Crt Date],TotalFeeCharged as [Total Fee],Owes as [Amt Due],convert(varchar(10),PaymentDate,101) as [Payment Due],convert(varchar(10),PaymentFollowUpdate,101) as [Follow Up],Pastdays as [Past Days]' , 
	        1,'[Follow Up]',@SerialHyperlink,@ResultSet = @ThisResultSetPastDueCallsTraffic 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                271,
	                @ThisResultSetPastDueCallsTraffic
	              )
	        END 
	        
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             271,
	             @SessionID
	        
	        PRINT('Past Due Calls Traffic')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetPastDueCallsTraffic = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_Past_Due_Calls'
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
---------------------------------------------------------------------------------------------------------------------------------------------------
--														Current Client Court Date Checker
--														Babar Ahmad 9223 08/26/2011 Report added.
---------------------------------------------------------------------------------------------------------------------------------------------------

	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 364
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY

	        DECLARE @ThisResultCurrentClientCourtDateChecker VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = '' 
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 364
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 364
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 364
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	   	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_CurrentClientCourtDateChecker', 
			' ',
	        @ReportTitle, 
	        'TicketID_PK,ticketnumber as [Ticket No],CauseNumber as [Cause No],Firstname as [First Name],Lastname as [Last Name],Verified_Court_Info as [Verified Court Info], Verified_status as [Verified Court Status],Auto_Court_Info as [Auto Court Info], Auto_status as [Auto Court Status]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultCurrentClientCourtDateChecker
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                364,
	                @ThisResultCurrentClientCourtDateChecker
	              )
	        END 
	        
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             364,
	             @SessionID
	        
	        PRINT('Current Client Court Date Checker')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultCurrentClientCourtDateChecker = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_CurrentClientCourtDateChecker'
	    END CATCH
	END




	
------------------------------------------------------------- Babar Ahmad 9223 08/26/2011 Current client court date checker Alert ----------------------------------------------------------

	
	--Fahad 6638 11/10/2009 Below three reports added
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 273
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Fahad 6638 11/10/2009 Past Due Calls(Traffic)  
	        DECLARE @ThisResultSetPastDueCallsFamily VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 273
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 273
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 273
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_Past_Due_Calls', 
	        '4,1',
	        @ReportTitle, 
	        --Ozair 7281 01/21/2010 date format issue resolved    
	        'TicketID_PK,ClientName as [Client],Crt as [Crt],CrtType as [Crt Type],convert(varchar(10),CourtDate,101) as [Crt Date],TotalFeeCharged as [Total Fee],Owes as [Amt Due],convert(varchar(10),PaymentDate,101) as [Payment Due],convert(varchar(10),PaymentFollowUpdate,101) as [Follow Up],Pastdays as [Past Days]' , 
	        1,'[Follow Up]',@SerialHyperlink,@ResultSet = @ThisResultSetPastDueCallsFamily 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                273,
	                @ThisResultSetPastDueCallsFamily
	              )
	        END 
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             273,
	             @SessionID
	        
	        PRINT('Past Due Calls Family')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetPastDueCallsFamily = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_Past_Due_Calls'
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	--Fahad 6638 11/10/2009 Below three reports added
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 272
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Fahad 6638 11/10/2009 Past Due Calls(Traffic)  
	        DECLARE @ThisResultSetPastDueCallsCriminal VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 272
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 272
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 272
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_Past_Due_Calls', 
	        '2,1', 
	        @ReportTitle, 
	        --Ozair 7281 01/21/2010 date format issue resolved   
	        'TicketID_PK,ClientName as [Client],Crt as [Crt],CrtType as [Crt Type],convert(varchar(10),CourtDate,101) as [Crt Date],TotalFeeCharged as [Total Fee],Owes as [Amt Due],convert(varchar(10),PaymentDate,101) as [Payment Due],convert(varchar(10),PaymentFollowUpdate,101) as [Follow Up],Pastdays as [Past Days]' , 
	        1,'[Follow Up]',@SerialHyperlink,@ResultSet = @ThisResultSetPastDueCallsCriminal 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                272,
	                @ThisResultSetPastDueCallsCriminal
	              )
	        END 
	        -- For Summary    
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             272,
	             @SessionID
	        
	        PRINT('Past Due Calls Criminal')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetPastDueCallsCriminal = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_Past_Due_Calls'
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 190
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        --Zahoor 4845 09/29/08
	        --Fahad 5722 04/03/2009 Remove parameter Search date and add follow-Up date Column  
	        DECLARE @ThisResultSetArrApp VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        DECLARE @Param0 VARCHAR(200)    
	        SET @Param0 = '1'    
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 190
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 190
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 190
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.[dbo].[USP_HTP_Get_CourtArrignmantAppearance_Report]', 
	        @Param0, 
	        @ReportTitle, 
	        'TICKETID_PK, FIRSTNAME as [First Name], LASTNAME as [Last Name], CAUSENUMBER as [Cause No],TICKETNUMBER as [Ticket No],CRT_LOCATION as [Court Loc],COURTSTATUS as [Status], convert(varchar(20),COURTDATE, 101)as [Court Date],NonHMCFollowUpDate as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetArrApp 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                190,
	                @ThisResultSetArrApp
	              )
	        END
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             190,
	             @SessionID
	        
	        PRINT('Outside Court ')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetArrApp = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.[dbo].[USP_HTP_Get_CourtArrignmantAppearance_Report] '
	    END CATCH
	END 
	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	--Nasir 7234 01/08/2010 add HMCSameDayArr report  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 230
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetHMCSameDayArr VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        DECLARE @ParamHMCSameDayArr VARCHAR(200)    
	        SET @ParamHMCSameDayArr = '1'    
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 230
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 230
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 230
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.[dbo].[USP_HTP_Get_HMCSameDayArr]', 
	        @ParamHMCSameDayArr, 
	        @ReportTitle, 
	        'TICKETID_PK, FIRSTNAME as [First Name], LASTNAME as [Last Name], CAUSENUMBER as [Cause No],TICKETNUMBER as [Ticket No],CourtLoc as [Court Loc],Status, convert(varchar(20),COURTDATE, 101)as [Court Date],Followupdate as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetHMCSameDayArr 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                230,
	                @ThisResultSetHMCSameDayArr
	              )
	        END
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             230,
	             @SessionID
	        
	        PRINT('HMC Same Day Arraignment')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetHMCSameDayArr = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.[dbo].[USP_HTP_Get_HMCSameDayArr]'
	    END CATCH
	END
	
	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------------------  
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 116
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSettrail VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 116
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 116
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 116
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.Usp_Hts_Get_NoTrialLetter', 
	        ' ', 
	        @ReportTitle, 
	        'ticketid_pk,ticketnumber as [Ticket Number],clientname as [Name],ncourtdate as [Crt Date & Time],crtlocation as [Crt Loc],Status as [status],rep as [Rep]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSettrail OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                116,
	                @ThisResultSettrail
	              )
	        END 
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             116,
	             @SessionID
	        
	        PRINT('No Trial Letter')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSettrail = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.Usp_Hts_Get_NoTrialLetter '
	    END CATCH
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 60
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        --Sabir Khan 5332 12/31/2008 Cause number and X-ref number has been added to split report in validation email...
	        --Fahad 5753 04/10/2004 Add parameter and add new Field Follow Up date   
	        DECLARE @ThisResultSetsplit VARCHAR(MAX)    
	        SET @ThisTotalCount = 0   
	        DECLARE @Param123 VARCHAR(200)    
	        SET @Param123 = '0'      
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 60
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 60
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 60
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_splitreport_new ', 
	        '1', 
	        @ReportTitle, 
	        'ticketid_pk,refcasenumberseq as [Ticket No],casenumassignedbycourt as [Cause Number],Midnum as [X-ref Number],VerifiedCourtInfo as [Verified Court Info],ShortName as [Court Location],FollowUpDate as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetsplit 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                60,
	                @ThisResultSetsplit
	              )
	        END 
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             60,
	             @SessionID
	        
	        PRINT('Split Report')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetsplit = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_splitreport_new '
	    END CATCH
	END 
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 159
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetNOS VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 159
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 159
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 159
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_hts_NOS_Report ', 
	        '0', 
	        @ReportTitle, 
	        'ticketid_pk,lastname as [LastName],firstname as [FirstName],causenumber as [Cause Number],refcasenumber as [Ticket Number],shortname as [Court.Loc],hiredate as [HireDate], followupdate as [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetNOS 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                159,
	                @ThisResultSetNOS
	              )
	        END 
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             159,
	             @SessionID
	        
	        PRINT('Not On System')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetNOS = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_hts_NOS_Report '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 160
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetdis VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 160
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 160
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 160
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_hts_bondflagdiscrepancy', 
	        '', 
	        @ReportTitle, 
	        'TicketID_PK,causenumber as [Cause Number],lastname as [LastName],firstname as [FirstName],status as [Status],courtinfo as [Court Info],bondflag as [Bond Flag],bonddocuments as [Bond Documents]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetdis OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                160,
	                @ThisResultSetdis
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             160,
	             @SessionID
	        
	        PRINT('Bond Flag Discrepancy')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetdis = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_hts_bondflagdiscrepancy '
	    END CATCH
	END 
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 164
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultsetcall VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 164
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 164
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 164
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_setCalls_For_ValidationReport ', 
	        '', 
	        @ReportTitle, 
	        'ticketid as ticketid_pk,name as [Name], Language as [Language],bond as [Bond],Comments as [Comments],CRT as [CRT],trialdesc as [Setting Information],contact as [Contact Info]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultsetcall OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                164,
	                @ThisResultsetcall
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             164,
	             @SessionID
	        
	        PRINT('Set Call Report')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultsetcall = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_setCalls_For_ValidationReport '
	    END CATCH
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Nasir 6098 08/13/2009 Add Complaints Reports
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 223
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        --Yasir Kamal 7315 01/26/2010 court setting info added.  
	        DECLARE @ThisResultComplaintReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 223
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 223
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 223
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GET_Complaint_Report ', 
	        ''''''''','''''''',-1,-1,1', 
	        @ReportTitle, 
	        'TicketID_PK as ticketid_pk,FullName  as [Client name],CourtLocation as [Crt Loc],Status as [Status],Courtdate as [Crt Date & Time],AttorneyName as [Attorney], CDate as [C-date]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultComplaintReport OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                223,
	                @ThisResultComplaintReport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             223,
	             @SessionID
	        
	        PRINT('Complaint Comment Report')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultComplaintReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GET_Complaint_Report '
	    END CATCH
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 202
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Sarim, Adil 5359 02/11/2009 - LOR Batch Print  
	        DECLARE @ThisResultUSPS VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 202
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 202
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 202
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GET_CertifiedLetterOfRep ', 
	        --   '-1,''''2/1/1900'''',''''1/1/1900'''',1',     
	        ''''''''',''''1900-01-01'''',''''9999-12-31'''',0,0,3,'''''''',0', 
	        @ReportTitle, -- Noufil 6012 06/12/2009 Title updated.
	                      --Ozair 5665 02/03/2009 Batch date changed toprint date for sent date  
	        'TicketID_PK,fullname as [Client Name], casenumassignedbycourt as [Cause Number], deliverystatus as [Delivery Status], PrintDate as [Send Date & Time], Rep as [Rep],USPSTrackingNumber', 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultUSPS OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                202,
	                @ThisResultUSPS
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             202,
	             @SessionID
	        
	        PRINT('No Certified Mail Confirmation')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultUSPS = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GET_CertifiedLetterOfRep '
	    END CATCH
	END
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 181
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultfax VARCHAR(MAX)    
	        SET @ThisTotalCount = 0 
	        -- Agha Usman 08/13/2008 4491  - Use Faxmaker for this report
	        -- Ozair 6013 07/13/2009 use faxmaker for this report instead of traffic tickets    
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 181
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 181
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 181
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
	        'traffictickets.dbo.Usp_Htp_Faxreport_faxmaker', --Fahad 9917 12/19/2011 Faxmaker sp issues fixed.
	        '1,-1,'''''''','''''''',1,1', 
	        @ReportTitle, 
	        'ticketid as [ticketid_pk],causenumber as [Cause #],faxid as [Fax ID], faxdate as [Fax Date], ResentCount as [Resent], LastResentFaxdate as [Resent Date],faxstatus as [Fax Status],lname as [Last Name],fname as [First Name],courtinfo as [Crt Info], status as [Status],crt as [Loc]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultfax OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                181,
	                @ThisResultfax
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             181,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultfax = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.Usp_Htp_Faxreport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 182
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 5113 11/13/2008 Title renamed and move report to alert section
	        -- Ozair 6013 07/13/2009 Title bond and verified status columns removed and added fax sent column  
	        DECLARE @ThisResultJpfax VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 182
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 182
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 182
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_JpAppFaxreport', 
	        '', 
	        @ReportTitle, 
	        'Ticketid as ticketid_pk,causenumber as [Cause Number], lastname as [Last Name],firstname as [First Name],court as [CRT], verifieddate as [Verified Date], FaxDate as [Fax Sent]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultJpfax OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                182,
	                @ThisResultJpfax
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             182,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultJpfax = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_JpAppFaxreport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Muhammad Ali 7747 07/02/2010 --> Add Missing Cause Follow Update in Email Validation report.
	
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 155
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultmiscause VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 155
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 155
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 155
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTS_GET_MissingCauseNumberReport', 
	        '', 
	        @ReportTitle, 
	        'ticketid_pk,casenumassignedbycourt as [Cause Number],refcasenumber as [Ticket No], lastname as [Last],firstname as [First],dob as [DOB],dlnumber as [DL],courtdatemain as [Court Date],shortdescription as [Status],shortname as [Loc],bondflag as [Flags], FollowUpdateForValidation as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultmiscause 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                155,
	                @ThisResultmiscause
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             155,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultJpfax = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_GET_MissingCauseNumberReport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 28
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultidticket VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 28
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 28
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 28
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.Sp_get_Idtickets ', 
	        ''''''''','''''''','''''''',''''''''', 
	        @ReportTitle, 
	        'TicketID_PK,refcasenumber as [Ticket No],casenumassignedbycourt as [Cause No],fullname as [Name],DOB as [D.O.B.],midnum as [MID],description as [Description],oldno as [Underlying],courtloc as [Court] ', 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultidticket OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                28,
	                @ThisResultidticket
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             28,
	             @SessionID
	        
	        PRINT ('Idtickets')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultidticket = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.Sp_get_Idtickets '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 125
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultidpayment VARCHAR(MAX)    
	        DECLARE @Param VARCHAR(200)    
	        SET @Param = '''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', 0'
	        
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 125
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 125
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 125
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_hts_PaymentDiscrepancyReport ', 
	        @Param, 
	        @ReportTitle, 
	        'ticketid_pk,ticketno as [Ticket Number],firstname as [First Name],lastname as [Last Name],(contact1 + contact2+contact3) as [Phone Number],transactiondate as [Transaction Date] ', 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultidpayment OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                125,
	                @ThisResultidpayment
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             125,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultidpayment = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_hts_PaymentDiscrepancyReport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 268
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultidServicetraff VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        DECLARE @Param2 VARCHAR(200) 
	        -- Noufil 5069 11/05/2008 New parameter value of date range added
	        --Sabir Khan 5738 05/25/2009  Add followupdate Column to Service ticket report...\
	        --Sabir Khan 6245 08/12/2009 Fixed Due and pending issue by adding fid column...  
	        SET @Param2 = '''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', 3992,1,0,1,1'    
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 268
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 268
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 268
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords ', 
	        @Param2, 
	        @ReportTitle, 
	        ' fid, TicketID_PK,casetypename as [Division],category as [Category],ClientName as [Name],courtdate as [Crt Date],courttime as [Crt Time],shortdescription as [Status],settinginformation as [Loc],recsortdate as [Open D],assignedto as [Open By],rec as [Update D],priority as [Priority],completion as [Comp.%],followupdate as [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultidServicetraff 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                268,
	                @ThisResultidServicetraff
	              )
	        END 
	        --select (@ThisResultidServicetraff)    
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             268,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultidServicetraff = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 269
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultidServicecrim VARCHAR(MAX)    
	        DECLARE @Paramdt VARCHAR(200) 
	        --Sabir Khan 5738 05/25/2009  Add followupdate Column to Service ticket report...
	        --Sabir Khan 6245 08/12/2009 Fixed Due and pending issue by adding fid column...  
	        SET @Paramdt = '''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', 3992,1,0,2,1'
	        
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 269
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 269
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 269
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords ', 
	        @Paramdt, 
	        @ReportTitle, 
	        'fid,TicketID_PK,casetypename as [Division],category as [Category],ClientName as [Name],courtdate as [Crt Date],courttime as [Crt Time],shortdescription as [Status],settinginformation as [Loc],recsortdate as [Open D],assignedto as [Open By],rec as [Update D],priority as [Priority],completion as [Comp.%],followupdate as [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultidServicecrim 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT 
	        --select (@ThisResultidServicecrim)    
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                269,
	                @ThisResultidServicecrim
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             269,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultidServicecrim = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 270
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultidServicecivil VARCHAR(MAX)    
	        DECLARE @Param1 VARCHAR(200) 
	        --Sabir Khan 5738 05/25/2009   Add followupdate Column to Service ticket report...
	        --Sabir Khan 6245 08/12/2009 Fixed Due and pending issue by adding fid column...  
	        SET @Param1 = '''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', 3992,1,0,4,1'
	        
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 270
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 270
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 270
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords ', 
	        @Param1, 
	        @ReportTitle, 
	        'fid,TicketID_PK,casetypename as [Division],category as [Category],ClientName as [Name],courtdate as [Crt Date],courttime as [Crt Time],shortdescription as [Status],settinginformation as [Loc],recsortdate as [Open D],assignedto as [Open By],rec as [Update D],priority as [Priority],completion as [Comp.%],followupdate as [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultidServicecivil 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                270,
	                @ThisResultidServicecivil
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             270,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultidServicecivil = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportCategory,
	             'Validation Email Issue in executing traffictickets.dbo.Usp_Hts_Get_OpenServiceTicketsRecords '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 183
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- zahoor 4481 08/06/2008
	        -- added bond report.....
	        -- Noufil 4735 09/04/2008 Move bond reprot from report section to Alert section in the summary    
	        DECLARE @ThisResultBondReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 183
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 183
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 183
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_HTP_Bond_Report', 
	        '0', 
	        @ReportTitle, --Nasir 5564 03/16/2009 Change first to first name and last to last name  
	        'ticketid_pk,causenumber as [Cause No],ticketnumber as [Ticket No],lastname as [Last Name],firstname as [First Name],CRT_Location as [Crt],status as [Status],FollowUpdate as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultBondReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                183,
	                @ThisResultBondReport
	              )
	        END
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             183,
	             @SessionID
	             -- end 4481
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultBondReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_HTP_Bond_Report '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 187
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Adil 4640 08/23/2008
	        -- HMC Warrant Alret added    
	        DECLARE @ThisResultHMCWarrAlert VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 187
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 187
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 187
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTS_Get_HMCWarrantAlert ', 
	        ' ', 
	        @ReportTitle, 
	        'TicketID_PK, BondFlag as [Bond], RefCaseNumber as [Cause No], LastName [Last Name], FirstName [First Name], ShortName as [Crt], Courtnumber [Crt.No.], AutoStatus [A], CourtDate as [A.Date], VerifiedCourtStatus [V], VerifiedCourtDate [V.Date], DaysOver [Days], Followupdate [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultHMCWarrAlert 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                187,
	                @ThisResultHMCWarrAlert
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             187,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultHMCWarrAlert = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_Get_HMCWarrantAlert '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 191
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4461 09/02/2008
	        -- Noufil 4947 11/11/2008 show all pending records
	        --Sabir Khan 5214 11/25/2008 Visitor Report has been changed into Online Follow up...
	        -- Online Follow up Report Added   
	        DECLARE @ThisResultvisitorreport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        DECLARE @Paramonline VARCHAR(200)    
	        SET @Paramonline = '1,1, ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) 
	            + ''''',0'    
	        
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 191
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 191
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 191
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'Sullolaw.[dbo].[USP_PS_Get_OnlineTracking] ', 
	        @Paramonline, 
	        -- Noufil 5524 03/16/2009 Report name Renamed.  
	        @ReportTitle, 
	        'row_number() over (order by serial ) as S#,(lastname + '' , ''+ firstname) as [Name], trialdesc as [Setting Information],comments as Comments,(contact1+''</br>'' + contact2 +''</br>''+ contact3) as [Contact],recdate as [Email Recieved Date] ', 
	        0,'',@SerialHyperlink,@ResultSet = @ThisResultvisitorreport OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                191,
	                @ThisResultvisitorreport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             191,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultvisitorreport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing Sullolaw.[dbo].[USP_PS_Get_OnlineTracking] '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 192
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4461 09/02/2008
	        -- Noufil 4919 /02/2008 merge legal consultation and Contact us report.
	        -- Noufil 4947 11/11/2008 show all pending records
	        --Sabir Khan 5214 11/25/2008 Legal Consultation has been changed into 'Online inquiries...
	        -- Online inquiries Report Added     
	        DECLARE @ThisResultconsultation VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        DECLARE @Paramcons VARCHAR(200) 
	        --Set @Paramcons = '1,0, ''''' + convert(varchar(20),getdate(), 101) + ''''',0,'+ convert(varchar(20),getdate(), 101)
	        --Yasir Kamal 7372 02/12/2010 follow-up date added.
	        --Yasir Kamal 7507 03/03/2010 online inquires issue fixed.  
	        SET @Paramcons = '1,1, ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) 
	            + ''''' ,1,''''' + CONVERT(VARCHAR(20), GETDATE(), 101) 
	            + ''''' '  
	        
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 192
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 192
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 192
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        --Afaq 07/30/2011 9111 Remove question colum which is already removed from the main sp
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'Sullolaw.[dbo].[USP_PS_Get_LegalConsultation] ', 
	        @Paramcons, 
	        @ReportTitle, 
	        'Serial as S#,Name,Phone,Comments,recdate as [Email Recieved Date],followUpDate as [follow-up Date]', 
	        0,'',@SerialHyperlink,@ResultSet = @ThisResultconsultation OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                192,
	                @ThisResultconsultation
	              )
	        END 
	        
	        --Sabir Khan 6409 08/18/2009 Online inquires has been moved from Report summary into Alert Summary...   
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             192,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultconsultation = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing Sullolaw.[dbo].[USP_PS_Get_LegalConsultation] '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 185
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4748 09/04/2008 HCJP Auto Update Report added     
	        DECLARE @ThisResultSetHCJPReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 185
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 185
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 185
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_HcjpAutoReport', 
	        ' ', 
	        @ReportTitle, 
	        'ticketid_pk,bondflag as [Bond], refcasenumber as [Cause Number],lastname as [Last Name],firstname as [First Name],shortname as [Crt],autostatus as [A],autocourtdate as [A date],verifiedcourtstatus as [V],verifiedcourtdate as [Vdate],waittime as [Days],LORPrintDate as [LOR]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetHCJPReport OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                185,
	                @ThisResultSetHCJPReport
	              )
	        END 
	        
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             185,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetHCJPReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_HcjpAutoReport '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 126
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        --Sabir Khan 4749 09/15/2008  For Internet Signup report    
	        DECLARE @ThisResultInernetsignUp VARCHAR(MAX)    
	        DECLARE @ParamSignup VARCHAR(200)    
	        SET @ParamSignup = '''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + 
	            ''''', ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', 1,2'    
	        
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 126
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 126
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 126
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTS_Internet_Signup_Report', 
	        @ParamSignup, 
	        -- Noufil 5524 03/16/2009 Report name change to "Online Internet Signup" from "Internet Signup".   
	        @ReportTitle, 
	        'ticketid_pk,lastname as [Last Name],FirstName as [First Name], SignupDate, SignupTime' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultInernetsignUp OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                126,
	                @ThisResultInernetsignUp
	              )
	        END 
	        
	        --For Summary    
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             126,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultInernetsignUp = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_Internet_Signup_Report '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 189
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4747 New Report Added for AG validation  
	        DECLARE @ThisResultSetAG VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0  
	        SET @ThisPendingCount = 0  
	        SET @ThisAlertCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 189
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 189
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 189
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GetAGCasesForValidation', 
	        ' ', 
	        @ReportTitle, 
	        'ticketid_pk,refcasenumber as [Cause Number],lastname as [Last Name],firstname as [First Name],shortname as [Crt],autostatus as [A],autocourtdate as [A date],verifiedcourtstatus as [V],verifiedcourtdate as [Vdate]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetAG OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                189,
	                @ThisResultSetAG
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             189,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetAG = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetAGCasesForValidation '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 193
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4840 New Report Added for No LOR Confirmation  
	        DECLARE @ThisResultSetLOR VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 193
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 193
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 193
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_NOLOR', 
	        ' ', 
	        @ReportTitle, 
	        -- Muhammad Nadir Siddiqui 9134 4/29/2011 added follow up date for No LOR confirmation.
	        'ticketid_pk,CauseNumber as [Cause Number],ticketnumber as [Tcket Number],LastName as [Last Name],FirstName as [First Name],Status as Status,CourtDate as [Court Date],CourtTime as [Court Time],CourtRoom as [Court Room], convert(varchar,NoLORConfirmFollowUpDate,101) as [Follow Up Date]',
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetLOR OUTPUT, @PendingCount 
	        = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount OUTPUT ,@TotalCount
	        = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                193,
	                @ThisResultSetLOR
	              )
	        END       
	        
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             193,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetLOR = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_NOLOR '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Waqas 5864 06/30/2009 ALR Case Summary  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 219
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetALREmailCaseSummary VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 219
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 219
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 219
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_CaseEmailSummary', 
	        ' ', 
	        @ReportTitle, 
	        'ticketid_pk , CauseNumber as [Cause Number], LastName as [Last Name], FirstName as [First Name], CourtDate as [Court Date] , CourtTime as [Court Time] , CourtRoom as [RM #]', 
	        1,'[Last Name]',@SerialHyperlink,@ResultSet = @ThisResultSetALREmailCaseSummary 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                219,
	                @ThisResultSetALREmailCaseSummary
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             219,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetALREmailCaseSummary = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_CaseEmailSummary '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 113
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Noufil 4895 New Report Added for No LOR
	        --Waqas 5653 03/21/2009 LOR FollowUp Date
	        -- Noufil 6011 06/12/2009
	        -- Note : - Dont remove column CourtID as i have use this column in SP USP_HTP_Generics_Get_XMLSet_For_Validation. If some one change title then change this tile on SP USP_HTP_Generics_Get_XMLSet_For_Validation too.  
	        DECLARE @ThisResultSetNOLOR VARCHAR(MAX)  
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 113
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 113
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 113
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTS_HMC_NO_LOR', 
	        '1 ', 
	        @ReportTitle,	-- Noufil 6011 06/12/2009 Title Is also use in SP USP_HTP_Generics_Get_XMLSet_For_Validation . If some one changes then change there too.  
	        'ticketid_pk,CauseNumber as [Cause Number],TicketNumber as [Ticket Number],lastname as [Last Name],firstname as [First Name],Status as Status,convert(varchar,CourtDate,101) as [Court Date],CourtTime as [Court Time]  ,CourtRoom as [Court Room], convert(varchar,LORFollowUpDate,101) as [Follow Up Date],CourtID', -- DOnt remove CourtID column from here. I will be remove under generic SP.  
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetNOLOR 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                113,
	                @ThisResultSetNOLOR
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             113,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetNOLOR = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_HMC_NO_LOR '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 264
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Abid 4912 11/14/2008 New Report Added for Traffic Waiting Follow Up report
	        --Sabir Khan 5183 11/20/2008 to exclude HMC and HCJP...
	        --Sabir Khan 5188 11/21/2008 to exclude HMC and HCJP...
	        --Sabir Khan 6399 08/18/2009 rename report from Traffic Waiting Follow Up(Non HMC and HCJP) to Waiting Follow Up (Traffic  - Non HMC and HCJP)   
	        DECLARE @ThisResultSetTrafficWaitingFollowUp VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 264
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 264
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 264
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_HTP_Get_TrafficWaitingFollowUpReport', 
	        '1', 
	        @ReportTitle, 
	        'ticketid_pk,lastname as [Last Name],firstname as [First Name],refcasenumber as [Ticket Number],CauseNumber as [Cause Number],VerifiedCourtStatus as Status,convert(varchar,TrafficWaitingFollowUp,101) as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetTrafficWaitingFollowUp 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                264,
	                @ThisResultSetTrafficWaitingFollowUp
	              )
	        END 
	        
	        --Yasir Kamal 7066 11/30/2009 move to alert section.  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             264,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetTrafficWaitingFollowUp = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_HTP_Get_TrafficWaitingFollowUpReport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Sabir Khan 6399 08/18/2009 report has been rename from 'Criminal Follow Up Report' into Waiting Follow Up (Criminal)  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 266
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultcriminal VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 266
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 266
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 266
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GetCriminalFollow ', 
	        '-1,''''01/01/1900'''',''''01/01/9999'''',1', --Nasir 6013 07/17/2009 is valid   
	        @ReportTitle, 
	        'TicketID_PK,causenumber as [Cause No], ticketnumber as [Ticket No], lastname as [Last], firstname as [First], dob as [DOB], dl as [DL], stat as [Stat], loc as [Loc], (courtdate + '' '' +courtno)as [Setting Info],  cov as [Cov], bflag as [Flag], followupdate as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultcriminal 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                266,
	                @ThisResultcriminal
	              )
	        END 
	        --Yasir Kamal 7066 11/30/2009 move to alert section.  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             266,
	             @SessionID
	        
	        PRINT('Criminal Follow Up')
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultcriminal = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetCriminalFollow '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 265
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Abid 5018 12/26/2008 New Report Added for Family Waiting Follow Up report
	        --sabir Khan 6399 08/18/2009 report has been renamed from Family Waiting Follow Up into Waiting Follow Up (Family)  
	        DECLARE @ThisResultSetFamilyWaitingFollowUp VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 265
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 265
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 265
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_HTP_Get_FamilyWaitingFollowUpReport', 
	        '1', 
	        @ReportTitle, 
	        'ticketid_pk,lastname as [Last Name],firstname as [First Name],refcasenumber as [Ticket Number],CauseNumber as [Cause Number],VerifiedCourtStatus as Status,convert(varchar,TrafficWaitingFollowUp,101) as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetFamilyWaitingFollowUp 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                265,
	                @ThisResultSetFamilyWaitingFollowUp
	              )
	        END 
	        
	        --Yasir Kamal 7066 11/30/2009 move to alert section.  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             265,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetFamilyWaitingFollowUp = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_HTP_Get_FamilyWaitingFollowUpReport '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 211
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        --Waqas 5770 04/14/2009 Address Validation Report  
	        DECLARE @ThisResultSetAddressValidationReport VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 211
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 211
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 211
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_htp_get_AddressCIDValidation', 
	        '1', --SAEED 7844 06/02/2010, remove validation parameter which was not using in the sp. ShowAll new parameter added in the sp to display all records, therefore it need to pass from here.
	        @ReportTitle, 
	        --Ozair 5929 05/21/2009 SNo generated on ContactID and not linked and Last name is HyperLinked to Matters page  
	        'ContactID as TicketID_PK,ContactID as [CID], ''<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=''+convert(varchar, TicketID_PK)+''" > '' + convert(varchar,Lastname) + '' </a>'' as [Last Name],DOB as [DOB], FirstNameInitial as [FI],Address as [Matter Address], ContactAddress as [Contact Address], FollowUpDate as [Follow Up Date]' , -- SAEED 7844 06/02/2010,[Follow Up Date] added in the, so that internal sp can use this column to get pending counts based on this field
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetAddressValidationReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT, @ShowHyperLink = 0  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                211,
	                @ThisResultSetAddressValidationReport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             211,
	             @SessionID --SAEED 7844 06/02/2010, category has changed from report to alert, 1=Alert,2=Report
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetAddressValidationReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_htp_get_AddressCIDValidation ' --SAEED 7844 06/02/2010, category has changed from report to alert
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 212
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        --Waqas 5770 04/14/2009 First Name Validation Report  
	        DECLARE @ThisResultSetFirstNameValidationReport VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 212
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 212
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 212
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_htp_get_FirstNameCIDValidation', 
	        '1 ', 
	        @ReportTitle, 
	        --Ozair 5929 05/21/2009 SNo generated on ContactID and not linked and Last name is HyperLinked to Matters page  
	        'ContactID as TicketID_PK,ContactID as [CID], ''<a href="http://ln.legalhouston.com/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=''+convert(varchar, TicketID_PK)+''" > '' + convert(varchar,Lastname) + '' </a>'' as [Last Name],DOB as [DOB], FirstNameInitial as [First Name Initial], FirstName as [Matter First Name], ContactFirstName as [Contact First Name]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetFirstNameValidationReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT, @ShowHyperLink = 0  
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                212,
	                @ThisResultSetFirstNameValidationReport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             212,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetFirstNameValidationReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_htp_get_FirstNameCIDValidation '
	    END CATCH
	END
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 206
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Waqas 5697 03/27/2009 New Report Added for HMC FTA Follow Up report
	        -- Waqas 5756 04/08/2009 Court time column added    
	        DECLARE @ThisResultSetHMCFTAFollowUp VARCHAR(MAX)    
	        
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 206
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 206
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 206
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_HMC_FTA_FOLLOWUP', 
	        '0', 
	        @ReportTitle, 
	        'ticketid_pk, FTACauseNumber as [FTA Cause Number], TicketNumber as [Ticket Number], FirstName [First Name], LastName [Last Name], DOB as [DOB], XRef [X-Ref], STATUS as [STATUS], CourtDate as [Court Date],CourtTime as [Court Time], FTAIssuedDate as [Insert Date], HMCFTAFollowUpDate as [Follow Up Date]', 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetHMCFTAFollowUp 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                206,
	                @ThisResultSetHMCFTAFollowUp
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             206,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetHMCFTAFollowUp = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_HMC_FTA_FOLLOWUP '
	    END CATCH
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 197
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Fahad 5032 12/05/2008 Missed Court Date Calls--  
	        DECLARE @ThisResultSetMissedCourtDateCalls VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 197
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 197
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 197
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_MissedCourtDateCalls', 
	        ' ', 
	        @ReportTitle, 
	        'TicketID_PK,causenumber as [Cause No],ticketno as [Ticket No],lastname as [Last Name],firstname as [First Name],courtstatus as [Crt. Status],courtdate as [Crt. Date],courttime as [Crt. Time],courtroom as [Crt. No],courtloc as [Crt. Loc],contactstatus as [Contact Status]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetMissedCourtDateCalls 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                197,
	                @ThisResultSetMissedCourtDateCalls
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             197,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetMissedCourtDateCalls = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_MissedCourtDateCalls '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Fahad 6429 09/17/2009 Bad Email Adress Report Added   
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 226
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetBadEmailAddress VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 226
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 226
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 226
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = '1' + ',' + CONVERT(VARCHAR, @ReportCategory)
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_BadEmailAdressReport', 
	        @SpParam, --SAEED 7844 06/02/2010, showAll new parameter added in the sp to display all records, therefore it need to pass from here.
	        @ReportTitle, -- Afaq 8162 08/17/2010 Remove column email sent date and report type.
	        --'TicketID_PK,FirstName as [First Name],LastName as [Last Name],Email as [Existing Email],CourtDate as [Court Date],HireDate as [Hire Date],EmailSentDate as [Email Sent Date],ReportType as [Report Type], FollowupDate as [Follow Up Date]' , -- SAEED 7844 06/02/2010,[Follow Up Date] added in the, so that internal sp can use this column to get pending counts based on this field
	        --Sabir Khan 8247 12/23/2010 Rep Name has been added...
	        'TicketID_PK,FirstName as [First Name],LastName as [Last Name],Email as [Existing Email],RepLastName as [Rep Name],CourtDate as [Court Date],HireDate as [Hire Date], FollowupDate as [Follow Up Date]' ,  -- SAEED 7844 06/02/2010,[Follow Up Date] added in the, so that internal sp can use this column to get pending counts based on this field 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetBadEmailAddress 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                226,
	                @ThisResultSetBadEmailAddress
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             226,
	             @SessionID --SAEED 7844 06/02/2010, category has changed from report to alert
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetBadEmailAddress = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_BadEmailAdressReport '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Fahad 6934 11/26/2009 Today's Qoute Call Back Report Added   
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 228
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetTodayQouteCallDate VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 228
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 228
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 228
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_All_TodayQouteCallDate', 
	        '''''01/01/1900'''',''''01/01/1900'''',1', 
	        @ReportTitle, 
	        'TicketID_PK,Customer as [Client Name],Convert(Varchar,LastContactDate) + ''(''+ Convert(Varchar,Days) + '')'' as [Contact],CourtDateMain as [Court Date],shortcourtname as [Court Location],calculatedtotalfee as [Qoute],QuoteResultDescription as [Follow Up Status],CallBackDate as [Call Back Date],STATUS as [Status],CaseType as [Case Type]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetTodayQouteCallDate 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                228,
	                @ThisResultSetTodayQouteCallDate
	              )
	        END 
	        --Ozair 7496 03/03/2010 Report moved to Alert Section  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             228,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetTodayQouteCallDate = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_All_TodayQouteCallDate '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Fahad 6934 11/26/2009 Before Qoute Call Back Report Added   
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 227
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetBeforeQouteCallDate VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 227
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 227
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 227
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_BeforeQouteCallDate', '', 
	        @ReportTitle, 
	        'TicketID_PK,Customer as [Client Name],LastContactDate + ''(''+convert(varchar,Days)+ '')'' as [Contact],CourtDateMain as [Court Date],shortcourtname as [Court Location],calculatedtotalfee as [Qoute],QuoteResultDescription as [Follow Up Status],CallBackDate as [Call Back Date],STATUS as [Status],CaseType as [Case Type]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetBeforeQouteCallDate 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                227,
	                @ThisResultSetBeforeQouteCallDate
	              )
	        END 
	        
	        --Ozair 7496 03/03/2010 Report moved to Alert Section  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             227,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetBeforeQouteCallDate = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_BeforeQouteCallDate'
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 204
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Nasir 5690 03/25/2009 HMC A/W DLQ Calls--    
	        DECLARE @ThisResultHMCAWDLQCalls VARCHAR(MAX)    
	        
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 204
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 204
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 204
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_HMCAWDLQCallsValidation', 
	        ' ', 
	        @ReportTitle, 
	        'TicketID_PK,causenumber as [Cause No],ticketno as [Ticket No],lastname as [Last Name],firstname as [First Name],courtstatus as [Crt. Status],courtdate as [Crt. Date],courttime as [Crt. Time],courtroom as [Crt. No],courtloc as [Crt. Loc],contactstatus  as [Contact Status]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultHMCAWDLQCalls OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                204,
	                @ThisResultHMCAWDLQCalls
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             204,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultHMCAWDLQCalls = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             'HMC A/W DLQ Calls Report',
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_HMCAWDLQCallsValidation '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------        
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 201
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        ---- Fahad 5098 12/05/2009 No Signed Contract Report--
	        ---Fahad 5477 02/02/2009  Change Columns Order  
	        DECLARE @ThisResultSetNoSignedContractReport VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 201
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 201
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 201
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GetContractFollowUp', 
	        ' ', 
	        @ReportTitle, 
	        'TicketID_PK,LASTNAME as [Last Name],FIRSTNAME as [First Name],TICKETNUMBER as [Ticket No],CAUSENUMBER as [Cause No],LOC as [Loc],COV as [Cov],STAT as [Status],CaseType as [Case Type],COURTDATE as [Court Date],FollowUpDate as [Follow Up Date]' , 
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetNoSignedContractReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                201,
	                @ThisResultSetNoSignedContractReport
	              )
	        END 
	        
	        --Yasir Kamal 7066 11/30/2009 move to report section.  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             201,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetNoSignedContractReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetContractFollowUp '
	    END CATCH
	END
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------   
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 198
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Nasir 5256 12/12/2008 Legal Consultation Report--  
	        DECLARE @ThisResultSetLegalConsultationReport VARCHAR(MAX)  
	        DECLARE @ParamLC VARCHAR(200)    
	        SET @ParamLC = '''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', ''''' + CONVERT(VARCHAR(20), GETDATE(), 101) + ''''', 0,true'
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 198
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 198
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 198
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_Legal_Consultation_Request', 
	        @ParamLC, 
	        -- Yasir 5357 12/25/2008  RecieveDate Column added
	        -- Noufil 5524 03/16/2009 Report name change to "Online Legal Consultation" from "Legal Consultation report".  
	        @ReportTitle, 
	        'TicketID_PK,ClientName as [Name],comments as [Comments],Description AS [Contact Status],RecieveDate AS [Record Date]' , --Fahad 5533 02/26/2009 Columns added and order changed  
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultSetLegalConsultationReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                198,
	                @ThisResultSetLegalConsultationReport
	              )
	        END       
	        
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             198,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetLegalConsultationReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_Legal_Consultation_Request '
	    END CATCH
	END
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 200
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        -- Yasir Kamal 5363 12/30/2008  HMC Missing X-Ref Report
	        -- Sabir Khan 5809 04/22/2009 Add Court Date Column...  
	        DECLARE @ThisResultHMCMissingXRefReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 200
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 200
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 200
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_MissingMidNumbers', 
	        '', 
	        @ReportTitle, 
	        'ticketid_pk, [Client Name] ,[Date of birth],[Hire Date],Courtdate as [Court Date], TicketNumber as [Ticket Number] ', 
	        
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultHMCMissingXRefReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                200,
	                @ThisResultHMCMissingXRefReport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             200,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultHMCMissingXRefReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_MissingMidNumbers '
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Yasir Kamal 6030 06/30/2009 Null Status Report  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 215
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultNullStatusReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 215
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 215
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 215
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_NullStatusReport', 
	        '', 
	        @ReportTitle, 
	        'ticketid_pk, LastName as [Last Name] ,FirstName as [First Name],[DOB],TicketNumber as [Ticket Number],CauseNumber as [Cause Number] ', 
	        
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultNullStatusReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                215,
	                @ThisResultNullStatusReport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             215,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultNullStatusReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_NullStatusReport'
	    END CATCH
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Noufil Khan 6126 07/24/2009 Traffic New Hire report added  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 220
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultNewhireReport VARCHAR(MAX)    
	        SET @ThisTotalCount = 0     
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 220
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 220
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 220
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GET_NEWHIRE_REPORT', 
	        '', 
	        @ReportTitle, 
	        'TicketID_PK , Lastname as [Last Name] , Firstname as [First Name],dbo.fn_DateFormat(paymentdate,27,''/'','':'',1) as [Sign up Date],dbo.fn_DateFormat(paymentdate,26,''/'','':'',1) as [Sign up Time], Source as Source ', 
	        
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultNewhireReport OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                220,
	                @ThisResultNewhireReport
	              )
	        END 
	        
	        -- tahir 6561 09/10/2009 moved to alert summary section...  
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             220,
	             @SessionID
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultNullStatusReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GET_NEWHIRE_REPORT'
	    END CATCH
	END 
	
	---------------------------------------------------------------------------------------------------------------------------------------------------      
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Afaq 7752 05/04/2010 Add disposed alert report.  
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 262
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultDisposedAlert VARCHAR(MAX)  
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 262
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 262
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 262
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GET_disposedAlert', 
	        '1',
	        @ReportTitle, --SAEED 7752, rename title
	        'TicketID_PK,CauseNumber as [Cause Number],FirstName as [First Name],LastName as [Last Name],VerifiedStatus as [Verified Status],VerifiedCourtDate as [Verified Court Date],AutoStatus as [Auto Status],AutoCourtDate as [Auto Court Date],FollowUpDate as [Follow Up Date]',
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultDisposedAlert 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                262,
	                @ThisResultDisposedAlert
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             262,
	             @SessionID --SAEED 7752, rename title
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultDisposedAlert = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTS_GET_disposedAlert' --SAEED 7752, rename title
	    END CATCH
	END
	
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 368
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	)
	
	-- Afaq 8213 11/02/2010 Add new report 'Other Payments'
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultPaymentReport VARCHAR(MAX)  
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 368
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 368
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 368
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.usp_HTP_Get_OtherPayments', 
	        '0,'''''''','''''''',1',
	        @ReportTitle, --SAEED 7752, rename title
	        'TicketID_PK,Firstname as [First Name],Lastname as [Last Name],ChargeAmount as [Last Payment],generalComments as [Last 5 days of General Comments]',
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultPaymentReport 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT    
	        
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                283,
	                @ThisResultPaymentReport
	              )
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             262,
	             @SessionID --SAEED 7752, rename title
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultPaymentReport = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.usp_HTP_Get_OtherPayments' --SAEED 7752, rename title
	    END CATCH
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
--														Bad Addresses Report section
--														Sabir Khan 8488 12/24/2010 Added Bad Addresses Report.
---------------------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 360 --Babar Ahmad 8488 07/20/2011 Report ID has been changed to the corrected one.
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultSetBadAddress VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0   
	        
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 360 --Babar Ahmad 8488 07/20/2011 Report ID has been changed to the corrected one.
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 360 --Babar Ahmad 8488 07/20/2011 Report ID has been changed to the corrected one.
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 360 --Babar Ahmad 8488 07/20/2011 Report ID has been changed to the corrected one.
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        --Fahad 8488 08/03/2011 Future Rocords added for Due Column Count
	        SET @SpParam = '1' 
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_Get_BadAddressClients', 
	        @SpParam, 
	        @ReportTitle, 	        
	        'TicketID_PK,lastname as [Last Name],ADDRESS as [Existing Address],CourtDateMain as [Court Date],Hiredate as [Hire Date], FollowUpDate as [Follow Up Date]' ,  
	        1,'[Follow Up Date]',@SerialHyperlink,@ResultSet = @ThisResultSetBadAddress 
	        OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT  
	        
	        IF @isRunSchedule = 1
	        BEGIN
	        	
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                360, --Babar Ahmad 8488 07/20/2011 Report ID has been changed to the corrected one.,
	                @ThisResultSetBadAddress
	              )
	             
	              
	        END       
	        
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             360, --Babar Ahmad 8488 07/20/2011 Report ID has been changed to the corrected one.
	             @SessionID 
	             
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultSetBadAddress = ''  
	        SELECT @errormessage = ERROR_MESSAGE()  
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_Get_BadAddressClients '
	             PRINT ERROR_MESSAGE()  
	    END CATCH
	END
---------------------------------------------------------------------------------------------------------------------------------------------------
--														End Bad Addresses Report section
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
--														Non-Updated Waiting Cases Report
--														Babar Ahmad 9055 04/06/2011 Report added.
---------------------------------------------------------------------------------------------------------------------------------------------------

	--BABAR
	IF EXISTS (
	       SELECT ReportID
	       FROM   @tempScheduleReport
	       WHERE  ReportID = 332
	              AND (
	                      @isRunSchedule = 0
	                      OR (
	                             @isRunSchedule = 1
	                             AND DAYID = @reportEmailDay
	                             AND DATEDIFF(
	                                     minute,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + @reportEmailTime,
	                                     CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) 
	                                     + ISNULL(Timings, '00 AM')
	                                 ) BETWEEN 0 AND @TimeMargin
	                         )
	                  )
	   )
	BEGIN
	    BEGIN TRY
	        DECLARE @ThisResultNonupdatedWaiting VARCHAR(MAX)  
	        
	        SET @ThisTotalCount = 0     
	        SET @ReportUserName = ''
	        SELECT TOP 1 @ReportTitle = title,
	               @OldReportTitle = title,
	               @ReportCategory = category
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 332
	        
	        SELECT TOP 1 @ReportUserName = primaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 332
	               AND UserType = 0
	               AND DayID = @CurrentDay
	        
	        SELECT TOP 1 @ReportUserName = @ReportUserName + secondaryuser
	        FROM   @tempScheduleReport
	        WHERE  ReportID = 332
	               AND UserType = 1
	               AND DayID = @CurrentDay
	        
	        IF @ReportUserName = ''
	        BEGIN
	            SET @ReportUserName = @UserNameFormat
	        END 
	        
	        SET @ReportTitle = @ReportTitle + '<br><span style=font-size:12px;>' 
	            + @ReportUserName + '</span>'
	        
	        
	        SET @SpParam = ' '
	        
	        
	        EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	        'traffictickets.dbo.USP_HTP_GET_NonupdatedResetCases ', 
	        '1,0', 
	        @ReportTitle, 
	        'TicketID_PK as ticketid_pk,CauseNumber as [Cause No], TicketNumber as [Ticket No],Lastname as [Last Name],Firstname as [First Name],Description as [Crt. Status], DocumentUploaded as [Document Uploaded]' , 
	        1,'',@SerialHyperlink,@ResultSet = @ThisResultNonupdatedWaiting OUTPUT, 
	        @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	        OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT
	       
	        IF @isRunSchedule = 1
	        BEGIN
	            INSERT INTO @tempReportHtml
	            VALUES
	              (
	                332,
	                @ThisResultNonupdatedWaiting
	              )
	        END 
	        
	        SELECT * FROM @tempReportHtml
	        -- For Summary     
	        EXEC USP_HTP_InsertIntoSummaryReport @OldReportTitle,
	             @ThisTotalCount,
	             @ThisPendingCount,
	             @ThisAlertCount,
	             @ReportCategory,
	             332,
	             @SessionID
	        
	        PRINT('Non-Updated Waiting Cases')
	        
	    END TRY  
	    BEGIN CATCH
	        SET @ThisResultNonupdatedWaiting = ''  
	        SELECT @errormessage = ERROR_MESSAGE() 	       
	        EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	             'Validation Email',
	             @ReportTitle,
	             'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GET_NonupdatedResetCases '
	    END CATCH
	    
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	---BABAR
	
	--Saeed 7791 07/12/2010 the block of code is written to send emails to users who are associated with the report & also scheduled for particular day & time. If day & time maches with current date those users will
	-- receive validation report as an email.
	----------------------CODE TO SEND EMAIL TO SHCEDULED USERS----------------------------------------------------
	-- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	IF @DebugMode = 1
	BEGIN
	    SELECT TOP 1 @tempvar1 = ReportID
	    FROM   @tempReportHtml
	    
	    INSERT INTO ValidationEmailDumpMessage
	      (
	        SUBJECT,
	        MESSAGE,
	        sessionID
	      )
	    VALUES
	      (
	        '''' + @tempvar + '''',
	        '''' + @tempvar1 + '''',
	        '''' + @SessionID + ''''
	      )
	    SET @dumpid = SCOPE_IDENTITY()
	END
	
	
	
	
	IF @isRunSchedule = 1
	   AND EXISTS(
	           SELECT ReportID
	           FROM   @tempReportHtml
	       )
	BEGIN
	    TRY
	    BEGIN
	        -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	        IF @DebugMode = 1
	        BEGIN
	            SET @tempvar = 'inside schedule routine'
	            SET @tempvar1 = ''
	            SELECT TOP 1 @tempvar1 = ReportID
	            FROM   @tempReportHtml
	            
	            INSERT INTO ValidationEmailDumpMessage
	              (
	                SUBJECT,
	                MESSAGE,
	                sessionID
	              )
	            VALUES
	              (
	                '''' + @tempvar + '''',
	                '''' + @tempvar1 + '''',
	                '''' + @SessionID + ''''
	              )
	            SET @dumpid = SCOPE_IDENTITY()
	        END
	        
	        DECLARE @Emailsubject VARCHAR(500)  
	        SET @Emailsubject = ''
	        /*
	        * Table @tempEmailSchedule holds all distinct reports which have been scheduled for current day & time for all active users.
	        * */
	        INSERT INTO @tempEmailSchedule
	        SELECT DISTINCT tu.EmployeeID,
	               vrs.ReportID,
	               vr.DayNumber AS DayID,
	               tu.Email,
	               ValidationEmailTimings.Timingvalue AS Timings,
	               tts.TITLE,
	               tu.UserName,
	               vr.UserType,
	               tts.Category,
	               tu.[Status]
	        FROM   ValidationReportScheduling vrs
	               INNER JOIN tbl_TAB_SUBMENU tts
	                    ON  vrs.ReportID = tts.ID
	               INNER JOIN ValidationReportSettings vr
	               INNER JOIN tblUsers tu
	                    ON  vr.UserID = tu.EmployeeID
	                    ON  tts.ID = vr.ReportID
	               INNER JOIN ValidationEmailTimings
	                    ON  vrs.EmailTimingsID = dbo.ValidationEmailTimings.TimingID
	        WHERE  daynumber = DATEPART(dw, GETDATE())
	               AND vr.UserID <> -1
	               AND tu.[Status] = 1
	               AND vrs.IsWeekDay = @isweekday
	               AND vrs.IsActive=1
	               AND --dbo.ValidationEmailTimings.TimingValue='08:00 PM'
	                   DATEDIFF(
	                       minute,
	                       CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) + 
	                       @currentTime,
	                       CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) + 
	                       ISNULL(TimingValue, '00 AM')
	                   ) BETWEEN 0 AND @TimeMargin
	        ORDER BY
	               tu.EmployeeID  
	        
	        DECLARE @tempsql VARCHAR(MAX)
	        SET @tempsql = 
	            'SELECT  DISTINCT   tu.EmployeeID,vrs.ReportID,vr.DayNumber AS DayID,tu.Email,ValidationEmailTimings.Timingvalue AS Timings,tts.TITLE,tu.UserName,vr.UserType,tts.Category,tu.[Status],@SessionID'
	        
	        SET @tempsql = @tempsql + 
	            ' FROM ValidationReportScheduling vrs   INNER JOIN tbl_TAB_SUBMENU tts   ON vrs.ReportID = tts.ID    INNER JOIN ValidationReportSettings vr    '
	        
	        SET @tempsql = @tempsql + 
	            ' INNER JOIN tblUsers tu   ON vr.UserID = tu.EmployeeID ON tts.ID = vr.ReportID   INNER JOIN ValidationEmailTimings   ON vrs.EmailTimingsID = dbo.ValidationEmailTimings.TimingID '
	        
	        SET @tempsql = @tempsql + 
	            ' WHERE daynumber=DATEPART(dw,GETDATE()) AND vr.UserID<>-1 AND tu.[Status]=1     AND vrs.IsWeekDay=@isweekday  AND '
	        
	        SET @tempsql = @tempsql + 
	            ' DATEDIFF(minute, CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) ' 
	            + @currentTime + 
	            ', CONVERT(DATETIME, CONVERT(VARCHAR(12), GETDATE())) ISNULL(TimingValue, ''00 AM'')) BETWEEN 0 AND ' + @TimeMargin
	        
	        SET @tempsql = @tempsql + ' ORDER BY tu.EmployeeID  '
	        
	        
	        -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	        IF @DebugMode = 1
	        BEGIN
	            SET @tempvar = 'query ::'  
	            INSERT INTO ValidationEmailDumpMessage
	              (
	                SUBJECT,
	                MESSAGE,
	                sessionID
	              )
	            VALUES
	              (
	                '''' + @tempvar + '''',
	                '''' + @tempsql + '''',
	                '''' + @SessionID + ''''
	              )
	            SET @dumpid = SCOPE_IDENTITY()
	        END 
	        
	        DECLARE @FinalSet VARCHAR(MAX)    
	        DECLARE @UserEmail VARCHAR(200)  
	        DECLARE @body VARCHAR(MAX)
	        DECLARE @UserID INT
	        DECLARE @ReportIDs VARCHAR(MAX)
	        DECLARE @ReportParam VARCHAR(MAX)
	        DECLARE @CurReportId INT
	        --DECLARE @PriSecUser VARCHAR(400)
	        DECLARE @ReplaceTitle VARCHAR(300)
	        DECLARE @CurrentUserType INT
	        DECLARE @CurrentCategory INT
	        DECLARE @TempTitle VARCHAR(300) 
	        SET @ReportIDs = ''
	        SET @ReportParam = ''
	        SET @body = ''
	        SET @UserEmail = '' 
	        --SET @PriSecUser=''
	        SET @ReplaceTitle = ''
	        SET @TempTitle = ''
	        
	        -----------------------CODE TO SEND EMAIL TO USERS -----------------------------------------------  
	        DELETE 
	        FROM   @tempUser
	        
	        DECLARE @summaryCounter INT
	        
	        -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	        IF @DebugMode = 1
	        BEGIN
	            SELECT @tempvar = '@emailCount' + CONVERT(VARCHAR, COUNT(*))
	            FROM   @tempEmailSchedule
	            WHERE  [STATUS] = 1
	            
	            INSERT INTO ValidationEmailDumpMessage
	              (
	                SUBJECT,
	                MESSAGE,
	                sessionID
	              )
	            VALUES
	              (
	                '''' + @tempvar + '''',
	                '''' + @tempvar1 + '''',
	                '''' + @SessionID + ''''
	              )
	            SET @dumpid = SCOPE_IDENTITY()
	        END 
	        
	        --Table @tempUser holds all primary & secondary users list who have been scheduled to receive email for current day & time.
	        INSERT INTO @tempUser
	        SELECT DISTINCT USERID,
	               Email,
	               UserType
	        FROM   @tempEmailSchedule
	        WHERE  [STATUS] = 1 
	        
	        -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	        IF @DebugMode = 1
	        BEGIN
	            SELECT @tempvar = '@tempusercount' + CONVERT(VARCHAR, COUNT(*))
	            FROM   @tempUser
	            
	            INSERT INTO ValidationEmailDumpMessage
	              (
	                SUBJECT,
	                MESSAGE,
	                sessionID
	              )
	            VALUES
	              (
	                '''' + @tempvar + '''',
	                '''' + @tempvar1 + '''',
	                '''' + @SessionID + ''''
	              )
	            SET @dumpid = SCOPE_IDENTITY()
	        END 
	        
	        SET @Counter = 1      
	        SET @summaryCounter = 1
	        
	        
	        --WHILE @Counter<=(SELECT COUNT(DISTINCT USERID ) FROM @tempEmailSchedule )  
	        /*
	        * This loop iterate for each distinct primary & secondary user who have been scheduled to receive email for current day & time.
	        * E.g: If one user is associted with one report as a primary user & with another report as a secondary user then the loop will itreate
	        * two times.
	        */
	        WHILE @Counter <= (
	                  SELECT COUNT(*)
	                  FROM   @tempUser
	              )
	        BEGIN
	            TRY     
	            SET @FinalSet = ''  
	            SET @ReportIDs = ''
	            SET @CurReportId = -1
	            
	            SELECT @UserID = UserID,
	                   @CurrentUserType = usertype
	            FROM   @tempUser
	            WHERE  ID = @Counter
	            
	            SELECT @CurReportId = t2.ReportId
	            FROM   @tempEmailSchedule t2
	            WHERE  t2.Userid = @UserID
	                   AND t2.UserType = @CurrentUserType 
	            /*
	            * Summaryreport table holds all reports summary which are scheduled for current day & time. Initially setting isScheduled=0. 
	            * And setting isScheduled=1 for those reports which is scheduled for particular user.   
	            */
	            UPDATE Summaryreport
	            SET    isScheduled = 0
	            WHERE  sessionid = @SessionID
	            
	            /*
	            * Holds the report ids which is scheduled to send email for primary/secondary user.
	            * */
	            CREATE TABLE #tempSummary
	            (
	            	ID        INT IDENTITY(1, 1),
	            	ReportID  INT
	            )
	            INSERT INTO #tempSummary
	              (
	                ReportID
	              )
	            SELECT DISTINCT t2.ReportId
	            FROM   @tempEmailSchedule t2
	            WHERE  t2.Userid = @UserID
	                   AND t2.UserType = @CurrentUserType 
	            
	            
	            /*
	            * Set isScheduled=1 in Summaryreport table.  The 'Alert Summary' & 'Report Summary' procedures only displays summary of those records 
	            * whose isScheduled is set to 1.
	            * */
	            WHILE @summaryCounter <= (
	                      SELECT COUNT(*)
	                      FROM   #tempSummary
	                  )
	            BEGIN
	                --SELECT @summaryCounter
	                SET @CurReportId = -1
	                SELECT @CurReportId = ReportId
	                FROM   #tempSummary
	                WHERE  id = @summaryCounter
	                
	                UPDATE Summaryreport
	                SET    isScheduled = 1,
	                       reportid = @CurReportId
	                WHERE  ReportID = @CurReportId
	                       AND SessionID = @SessionID
	                
	                SET @summaryCounter = @summaryCounter + 1
	            END
	            SET @summaryCounter = 1
	            DROP TABLE #tempSummary
	            
	            -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	            IF @DebugMode = 1
	            BEGIN
	                SET @tempvar = 'drop temp summary'
	                INSERT INTO ValidationEmailDumpMessage
	                  (
	                    SUBJECT,
	                    MESSAGE,
	                    sessionID
	                  )
	                VALUES
	                  (
	                    '''' + @tempvar + '''',
	                    '''' + @tempvar1 + '''',
	                    '''' + @SessionID + ''''
	                  )
	                SET @dumpid = SCOPE_IDENTITY()
	            END 
	            
	            --Concatinating final html. 
	            SELECT @FinalSet = ISNULL(@FinalSet, '') + ISNULL(t1.ReportHtml, ''),
	                   --@CurrentUserType=t2.UserType,
	                   @CurrentCategory = t2.category,
	                   @ReportIDs = ISNULL(@ReportIDs, '') + CONVERT(VARCHAR, t2.ReportId) 
	                   + CONVERT(VARCHAR, ',')
	            FROM   @tempReportHtml t1
	                   INNER JOIN @tempEmailSchedule t2
	                        ON  t1.ReportID = t2.ReportId
	                   INNER JOIN tbl_TAB_SUBMENU tts
	                        ON  tts.id = t2.ReportId
	                        AND t2.Userid = (
	                                SELECT UserID
	                                FROM   @tempUser
	                                WHERE  ID = @Counter
	                                       AND UserType = @CurrentUserType
	                            )
	                        AND t2.UserType = @CurrentUserType
	            ORDER BY
	                   tts.Category
	            
	            -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	            IF @DebugMode = 1
	            BEGIN
	                SET @tempvar = 'Temp Report Html'
	                INSERT INTO ValidationEmailDumpMessage
	                  (
	                    SUBJECT,
	                    MESSAGE,
	                    sessionID
	                  )
	                VALUES
	                  (
	                    '''' + @tempvar + '''',
	                    '''' + @tempvar1 + '''',
	                    '''' + @SessionID + ''''
	                  )
	                SET @dumpid = SCOPE_IDENTITY()
	            END 
	            
	            
	            --Appending Primary/Back up in the subject line based on user type.
	            DECLARE @SubjectTag VARCHAR(20)
	            SET @SubjectTag = ''
	            IF @CurrentUserType = 0
	                SET @SubjectTag = '[Primary]'
	            ELSE 
	            IF @CurrentUserType = '1'
	                SET @SubjectTag = '[Back up]'
	            
	            SELECT @UserEmail = EmailAddress
	            FROM   @tempUser
	            WHERE  ID = @Counter 
	            
	            SET @Emailsubject = 'Validation Report (Traffic) ' + CONVERT(VARCHAR(20), GETDATE(), 101) 
	                + ' ' + @SubjectTag
	            
	            SET @Counter = @Counter + 1 
	            
	            
	            --SET @ReportIDs =SUBSTRING(@ReportIDs,1, LEN(@ReportIDs)-1)   
	            SET @ReportIDs = SUBSTRING(
	                    @ReportIDs,
	                    1,
	                    CASE 
	                         WHEN LEN(@ReportIDs) = 0 THEN LEN(@ReportIDs)
	                         ELSE LEN(@ReportIDs) - 1
	                    END
	                )
	            
	            SET @ReportIDs = '''' + @ReportIDs + ''''   
	            
	            SET @ReportParam = '1,''' + @ReportIDs + ''',''''' + @SessionID 
	                + '''''' 
	            --Get alert summary
	            BEGIN TRY
	                -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	                IF @DebugMode = 1
	                BEGIN
	                    SET @tempvar = 'before summry'
	                    INSERT INTO ValidationEmailDumpMessage
	                      (
	                        SUBJECT,
	                        MESSAGE,
	                        sessionID
	                      )
	                    VALUES
	                      (
	                        '''' + @tempvar + '''',
	                        '''' + @tempvar1 + '''',
	                        '''' + @SessionID + ''''
	                      )
	                    SET @dumpid = SCOPE_IDENTITY()
	                END 
	                
	                EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	                'traffictickets.dbo.USP_HTP_GetScheduleSummaryReport', 
	                @ReportParam,
	                'Alert Summary', 
	                '[Report Name],[Alert] as Due,[Pending]' ,
	                0,'','newpakhouston.legalhouston.com',@ResultSet = @ThisResultalertsummary 
	                OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount 
	                = @ThisAlertCount OUTPUT ,@TotalCount = @ThisTotalCount 
	                OUTPUT
	            END TRY  
	            BEGIN CATCH
	                SET @ThisResultalertsummary = ''  
	                SELECT @errormessage = ERROR_MESSAGE()  
	                EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	                     'Validation Email',
	                     'Alert Summary',
	                     'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetScheduleSummaryReport'
	            END CATCH 
	            
	            -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	            IF @DebugMode = 1
	            BEGIN
	                SET @tempvar = 'aftter summary'
	                INSERT INTO ValidationEmailDumpMessage
	                  (
	                    SUBJECT,
	                    MESSAGE,
	                    sessionID
	                  )
	                VALUES
	                  (
	                    '''' + @tempvar + '''',
	                    '''' + @tempvar1 + '''',
	                    '''' + @SessionID + ''''
	                  )
	                SET @dumpid = SCOPE_IDENTITY()
	            END 
	            
	            SET @ReportParam = '2,''' + @ReportIDs + ''',''''' + @SessionID 
	                + '''''' 
	            --Get report summary
	            BEGIN TRY
	                EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	                'traffictickets.dbo.USP_HTP_GetScheduleSummaryReport', 
	                @ReportParam, 
	                'Report Summary', 
	                '[Report Name],[Alert] as Due,[Pending]' ,	-- Noufil 5900 06/09/2009 Remove * and use column name to show only that column at report  
	                0,'','newpakhouston.legalhouston.com',@ResultSet = @ThisResultpendingsummary 
	                OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount 
	                = @ThisAlertCount OUTPUT ,@TotalCount = @ThisTotalCount 
	                OUTPUT
	            END TRY  
	            BEGIN CATCH
	                SET @ThisResultpendingsummary = ''  
	                SELECT @errormessage = ERROR_MESSAGE()  
	                EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	                     'Validation Email',
	                     'Alert Summary',
	                     'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetScheduleSummaryReport'
	            END CATCH     
	            
	            
	            
	            
	            SET @ThisResultalertsummary = REPLACE(@ThisResultalertsummary, '(Alert)', '')
	            SET @ThisResultalertsummary = REPLACE(@ThisResultalertsummary, '(Report)', '')
	            SET @ThisResultpendingsummary = REPLACE(@ThisResultpendingsummary, '(Alert)', '')
	            SET @ThisResultpendingsummary = REPLACE(@ThisResultpendingsummary, '(Report)', '')
	            
	            SET @body = @ThisResultalertsummary + @ThisResultpendingsummary 
	                + ISNULL(@FinalSet, '')
	            
	            SET @body = REPLACE(@body, '&lt;', '<')  
	            SET @body = REPLACE(@body, '&gt;', '>') 
	            
	            -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	            IF @DebugMode = 1
	            BEGIN
	                --inserting data in ValidationEmailDumpMessage table for email logging.
	                INSERT INTO ValidationEmailDumpMessage
	                  (
	                    useremail,
	                    SUBJECT,
	                    MESSAGE,
	                    sessionID
	                  )
	                VALUES
	                  (
	                    '''' + @UserEmail + '''',
	                    '''' + @Emailsubject + '''',
	                    '''' + @body + '''',
	                    '''' + @SessionID + ''''
	                  )	
	                SELECT @dumpid = SCOPE_IDENTITY()
	            END
	            
	            EXEC msdb.dbo.sp_send_dbmail 
	                 @profile_name = 'Traffic System',
	                 @recipients = @UserEmail,
	                 @subject = @Emailsubject,
	                 @body = @body,
	                 @body_format = 'HTML'
	            
	            ; 
	            
	            -- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	            IF @DebugMode = 1
	            BEGIN
	                UPDATE ValidationEmailDumpMessage
	                SET    emailSent = 1
	                WHERE  id = @dumpid
	            END
	        END TRY    
	        
	        BEGIN CATCH
	            SET @FinalSet = ''  
	            SELECT @errormessage = ERROR_MESSAGE() 
	            --DECLARE @errorDesc VARCHAR(500)    
	            SET @errorDesc = 'Validation Email Issue in executing '     
	            EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	                 'Validation Email',
	                 'Validation Scheduling',
	                 @errorDesc
	        END CATCH
	    END
	END TRY
	
	BEGIN CATCH
	    SELECT @errormessage = ERROR_MESSAGE() 
	    --DECLARE @errorDesc VARCHAR(500)    
	    SET @errorDesc = 'Validation Email Issue in executing '     
	    EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	         'Validation Email',
	         'Validation Scheduling',
	         @errorDesc
	END CATCH
	
	
	
	-- Saeed 7791 07/12/2010 END CODE TO SEND EMAIL TO SHCEDULED USERS---------------------------------------------
	
	IF @isRunSchedule = 0
	BEGIN
	    ---------------------------------------------------------------------------------------------------------------------------------------------------
	    --IF EXISTS (SELECT ID FROM tbl_TAB_SUBMENU tts WHERE tts.ID = 220 AND tts.[active] = 1)
	    BEGIN
	        BEGIN TRY
	            --declare @ThisResultalertsummary varchar(max)    
	            
	            SET @SpParam = ' 1,'''''''',''''' + @SessionID + '''''' 
	            EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	            'traffictickets.dbo.USP_HTP_GetSummaryReport', 
	            @SpParam, 
	            'Alert Summary', 
	            '[Report Name],[Alert] as Due,[Pending]' ,	-- Noufil 5900 06/09/2009 Remove * and use column name to show only that column at report  
	            0,'',@SerialHyperlink,@ResultSet = @ThisResultalertsummary 
	            OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	            OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT
	        END TRY  
	        BEGIN CATCH
	            SET @ThisResultalertsummary = ''  
	            SELECT @errormessage = ERROR_MESSAGE()  
	            EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	                 'Validation Email',
	                 'Alert Summary',
	                 'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetSummaryReport'
	        END CATCH
	    END 
	    
	    ---------------------------------------------------------------------------------------------------------------------------------------------------
	    ---------------------------------------------------------------------------------------------------------------------------------------------------
	    --IF EXISTS (SELECT ID FROM tbl_TAB_SUBMENU tts WHERE tts.ID = 220 AND tts.[active] = 1)
	    BEGIN
	        BEGIN TRY
	            --declare @ThisResultpendingsummary varchar(max)    
	            SET @SpParam = ' 2,'''''''',''''' + @SessionID + '''''' 
	            EXECUTE USP_HTP_Generics_Get_XMLSet_For_Validation 
	            'traffictickets.dbo.USP_HTP_GetSummaryReport', 
	            @SpParam, 
	            'Report Summary', 
	            '[Report Name],[Alert] as Due,[Pending]' ,	-- Noufil 5900 06/09/2009 Remove * and use column name to show only that column at report  
	            0,'',@SerialHyperlink,@ResultSet = @ThisResultpendingsummary 
	            OUTPUT, @PendingCount = @ThisPendingCount OUTPUT,@AlertCount = @ThisAlertCount 
	            OUTPUT ,@TotalCount = @ThisTotalCount OUTPUT
	        END TRY  
	        BEGIN CATCH
	            SET @ThisResultpendingsummary = ''  
	            SELECT @errormessage = ERROR_MESSAGE()  
	            EXEC [dbo].[usp_hts_ErrorLog] @errormessage,
	                 'Validation Email',
	                 'Report Summary',
	                 'Validation Email Issue in executing traffictickets.dbo.USP_HTP_GetSummaryReport'
	        END CATCH
	    END
	    ---------------------------------------------------------------------------------------------------------------------------------------------------
	END 
	---------------------------------------------------------------------------------------------------------------------------------------------------    
	DELETE 
	FROM   Summaryreport
	WHERE  SessionID = @SessionID
	-- For debugging purpose only, if DebugMode=1 then log details in ValidationEmailDumpMessage table.
	IF @DebugMode = 1
	BEGIN
	    UPDATE ValidationEmailDumpMessage
	    SET    isDeleted = 1
	    WHERE  id = @dumpid
	END
	
	IF @isRunSchedule = 0
	BEGIN
	    --Yasir Kamal 5460 02/17/2009 Email Alert/Report Summary   
	    IF @Detail = 1
	    BEGIN
	        -- Saeed 7791 07/12/2010 remove '(Alert)' & '(Report)' from report title in report & alert summary 
	        SET @ThisResultalertsummary = REPLACE(@ThisResultalertsummary, '(Alert)', '')
	        SET @ThisResultalertsummary = REPLACE(@ThisResultalertsummary, '(Report)', '')
	        SET @ThisResultpendingsummary = REPLACE(@ThisResultpendingsummary, '(Alert)', '')
	        SET @ThisResultpendingsummary = REPLACE(@ThisResultpendingsummary, '(Report)', '')
	        
	        SET @ThisResultSet = @ThisResultalertsummary + @ThisResultpendingsummary
				+ISNULL(@ThisPotentialBondForfeiture,'') --8521 Afaq 12/13/2010 Add new report of Potential Bond Forfeitures 
	            + ISNULL(@ThisResultSetalert, '') 
	            + ISNULL(@ThisResultSetHCJP, '') 
	            --faique 10675 01/17/2013 'HMC Setting Discrepancy Report' placed here
	            + ISNULL(@ThisResulthmcsetting, '') 
	            + ISNULL(@ThisResultSetHMC_DLQ, '') 
	            + ISNULL(@ThisResultSetbond, '') 
	            --Ozair 7281 01/21/2010 Past Due Calls Report removed as it is already being displayed case type wise  
	            + ISNULL(@ThisResultPaymentDueCallsReport, '') 
	            + ISNULL(@ThisResultSetreminder, '') 
	            + ISNULL(@ThisResultSetpast, '') 
	            + ISNULL(@ThisResultSetpastother, '') 
	            + ISNULL(@ThisResultBondReport, '') 
	            + ISNULL(@ThisResultHMCWarrAlert, '') 
	            + ISNULL(@ThisResultSetAG, '') 
	            + ISNULL(@ThisResultSetLOR, '') 
	            + ISNULL(@ThisResultSetNOLOR, '') 
	            + ISNULL(@ThisResultSettrail, '') 
	            + ISNULL(@ThisResultSetsplit, '') 
	            + ISNULL(@ThisResultSetNOS, '') 
	            + ISNULL(@ThisResultSetdis, '') 
	            + ISNULL(@ThisResultsetcall, '') 
	            + ISNULL(@ThisResultComplaintReport, '') -- Nasir 6098 08/13/2009 add Complaint report  
	            + ISNULL(@ThisResultSetArrApp, '') 
	            + ISNULL(@ThisResultSetHMCSameDayArr, '') --Nasir 7234 01/08/2010 add HMCSameDayArr report        
	            + ISNULL(@ThisResultUSPS, '') -- Sarim, Adil 5359 02/11/2009 - LOR Batch Print  
	            + ISNULL(@ThisResultfax, '') 
	            + ISNULL(@ThisResultJpfax, '') 
	            + ISNULL(@ThisResultmiscause, '') 
	            --faique 10675 01/17/2013 'HMC Setting Discrepancy Report moved from here place it right below the HCJP Auto Update Alert Report
	            --+ ISNULL(@ThisResulthmcsetting, '') 
	            --Yasir Kamal 6030 06/30/2009 Null Status Report  
	            +ISNULL(@ThisResultNullStatusReport, '') 
	            + ISNULL(@ThisResultidticket, '') 
	            + ISNULL(@ThisResultidpayment, '') 
	            + ISNULL(@ThisResultidServicetraff, '') 
	            + ISNULL(@ThisResultidServicecrim, '') 
	            + ISNULL(@ThisResultidServicecivil, '') 
	            + ISNULL(@ThisResultvisitorreport, '') 
	            + ISNULL(@ThisResultconsultation, '') 
	            + ISNULL(@ThisResultSetHCJPReport, '') 
	            + ISNULL(@ThisResultInernetsignUp, '') 
	            -- Abid Ali 4912 11/14/2008... Traffic Wating Follow Up report   
	            + ISNULL(@ThisResultSetTrafficWaitingFollowUp, '') 
	            --Sabir Khan 6399 09/07/2009 Waiting follow up (criminal) has been placed to its proper location...  
	            + ISNULL(@ThisResultcriminal, '') 
	            -- Abid Ali 5018 12/26/2008... Family Wating Follow Up report   
	            + ISNULL(@ThisResultSetFamilyWaitingFollowUp, '') 
	            -- Fahad 5302 12/11/2008.. missed court date call report  
	            +ISNULL(@ThisResultSetMissedCourtDateCalls, '') 
	            -- Nasir 5690 03/25/2009.. HMC A/W DLQ Calls  
	            +ISNULL(@ThisResultHMCAWDLQCalls, '') 
	            -- Waqas 5697 03/30/2009.. HMC FTA Follow up Report  
	            +ISNULL(@ThisResultSetHMCFTAFollowUp, '') 
	            --Nasir 5256 12/12/2008..... Legal Consultation Report  
	            +ISNULL(@ThisResultSetLegalConsultationReport, '') 
	            -- Yasir Kamal 5363 12/30/2008  HMC Missing X-Ref Report  
	            +ISNULL(@ThisResultHMCMissingXRefReport, '') 
	            -- Noufil 6126 07/24/2009 New hire report variable added.  
	            + ISNULL(@ThisResultNewhireReport, '') 
	            -- Fahad 5098 01/05/2009.. missed court date call report  
	            +ISNULL(@ThisResultSetNoSignedContractReport, '') 
	            --Waqas 5770 04/14/2009 Address Validation Report  
	            +ISNULL(@ThisResultSetAddressValidationReport, '') 
	            --SAEED 7844 06/02/2010, this variable was not added here, because of this detail was not appearing in validation reprot.
	            +ISNULL(@ThisResultSetBadEmailAddress, '') 
	            --Waqas 5770 04/14/2009 First Name Validation Report  
	            +ISNULL(@ThisResultSetFirstNameValidationReport, '') 
	            --Waqas 5864 06/30/2009 ALR Case Email Summary  
	            +ISNULL(@ThisResultSetALREmailCaseSummary, '') 
	            --Fahad 6638 11/10/2009 PastDueCalls Traffic  
	            +ISNULL(@ThisResultSetPastDueCallsTraffic, '') 
	            -- Babar Ahmad 9223 10/03/2011 Current Client Court Date Checker
	            +ISNULL(@ThisResultCurrentClientCourtDateChecker, '')
	            --Fahad 6638 11/10/2009 PastDueCalls Traffic  
	            +ISNULL(@ThisResultSetPastDueCallsFamily, '') 
	            --Fahad 6638 11/10/2009 PastDueCalls Traffic  
	            +ISNULL(@ThisResultSetPastDueCallsCriminal, '') 
	            --Fahad 6934 12/01/2009   
	            +ISNULL(@ThisResultSetBeforeQouteCallDate, '') 
	            + ISNULL(@ThisResultSetTodayQouteCallDate, '') 
	            + ISNULL(@ThisResultDisposedAlert, '') -- SAEED-7752-05/06/2010, Disposed alert check
	            +ISNULL(@ThisResultPaymentReport,'') -- Afaq 8213 11/02/2010 Add report 'Other Payments' in dataset.
	            +ISNULL(@ThisResultSetBadAddress,'')--Asad Ali 8488 12/28/2010 add bad address Report in data set
	            +ISNULL(@ThisResultNonupdatedWaiting,'')
	            
	    END
	    ELSE 
	    IF @Detail = 0
	    BEGIN
	        SET @ThisResultSet = @ThisResultalertsummary + @ThisResultpendingsummary
	    END 
	    
	    --5460 end    
	    
	    SET @ThisResultSet = REPLACE(@ThisResultSet, '&lt;', '<')  
	    SET @ThisResultSet = REPLACE(@ThisResultSet, '&gt;', '>') 
	    
	    --Yasir Kamal 5460 03/03/2009 Display or Email Report  
	    
	    IF @DisplayReport = 1
	    BEGIN
	        SELECT (@ThisResultSet)
	    END
	    ELSE 
	    IF @displayreport = 0
	    BEGIN
	        IF ISNULL(@ThisResultSet, '') <> ''
	           AND @EmailAddress <> ''
	        BEGIN
	            DECLARE @subject VARCHAR(MAX) 
	            --ozair 4891 10/03/2008 subject modified    
	            SET @subject = 'Validation Report (Traffic) ' + CONVERT(VARCHAR(20), GETDATE(), 101)    
	            
	            EXEC msdb.dbo.sp_send_dbmail 
	                 @profile_name = 'Traffic System',
	                 @recipients = @EmailAddress,
	                 @subject = @subject,
	                 @body = @ThisResultSet,
	                 @body_format = 'HTML'
	            
	            ;
	        END
	    END
	END--SAEED 7791 END
	   

 
        
ALTER     procedure USP_HTS_GetRetentionReport_VER1        
@startdate datetime,        
@enddate datetime,        
@employeeid int = 0,        
@reporttype int = 0        
as        
        
create table #temp(employeeid int,ticketid_pk int,sumtotalfee money,calcfee money,paymentdate  int,activeflag int,inactiveflag int,clicks money, clickscount int)        
        
--this query retrieves all customers that called us and became client on the specified date        
set @enddate = dateadd(day,1,@enddate)        
if @reporttype = 0        
begin        
insert into #temp        
select TV.Vemployeeid as employeeid,T.ticketid_pk,sum(chargeamount) as sumtotalfee,        
null as calcfee,        
0 as paymentdate,1 as activeflag,0 as inactiveflag,null,0         
        
 from tbltickets T , tblticketsviolations TV ,tblticketspayment P        
 where T.ticketid_pk = P.ticketid        
and T.ticketid_Pk = TV.ticketId_pk    
and paymentvoid = 0        
and activeflag = 1        
and paymenttype not in (0,7,8,9,10,98,99,100)        
and firmid = 3000        
group by TV.Vemployeeid,T.ticketid_pk        
having min(P.recdate) between @startdate and @enddate        
end          
else        
begin        
insert into #temp        
select TV.Vemployeeid as employeeid,T.ticketid_pk,sum(chargeamount) as sumtotalfee,        
null as calcfee,        
0 as paymentdate,1 as activeflag,0 as inactiveflag,null,0         
 from tbltickets T ,  tblticketsviolations TV,tblticketspayment P        
 where T.ticketid_pk = P.ticketid        
and T.ticketid_pk  = TV.ticketid_Pk    
and paymentvoid = 0        
and activeflag = 1        
and paymenttype not in (0,7,8,9,10,98,99,100)        
and firmid = 3000        
group by TV.Vemployeeid,T.ticketid_pk        
having min(T.recdate) between @startdate and @enddate         
end        
        
        
        
insert into #temp        
select TV.Vemployeeid,T.ticketid_pk,null,sum(calculatedtotalfee) as sumtotalfee         
,0 as paymentdate,0 as activeflag,1,null,0        
from tbltickets T        
inner join tblticketsviolations tv on  T.ticketId_pk = TV.ticketId_pk    
where ( recdate between @startdate and @enddate)        
and firmid = 3000        
group by TV.Vemployeeid,T.ticketid_pk        
        
        
insert into #temp        
select T.EmployeeID ,T.ticketid_pk,null,null         
,0 as paymentdate,0 as activeflag,0,sum(calculatedtotalfee),1        
from tbltickets T        
where ( recdate between @startdate and @enddate)        
and firmid = 3000        
group by T.EmployeeID,T.ticketid_pk        
        
--select * from #temp        
        
if @employeeid = 0        
 select dateadd(day,-paymentdate,getdate()),U.employeeid,U.abbreviation,sum(activeflag) as totalclient,        
 sum(inactiveflag) as totalquote,sum(isnull(sumtotalfee,0)) as Totalfee,sum(isnull(calcfee,0)) as potrev,sum(isnull(clicks,0)) as clickrev,isnull(sum(clickscount),0) as clickscount        
         
  from #temp T,tblusers U        
 where  T.employeeid = U.employeeid        
 group by paymentdate,U.employeeid,U.abbreviation        
 order by paymentdate,U.employeeid,U.abbreviation        
else        
 select dateadd(day,-paymentdate,getdate()),U.employeeid,U.abbreviation,sum(activeflag) as totalclient,        
 sum(inactiveflag) as totalquote,sum(isnull(sumtotalfee,0)) as Totalfee,sum(isnull(calcfee,0)) as potrev,sum(isnull(clicks,0)) as clickrev,isnull(sum(clickscount),0) as clickscount        
         
 from #temp T,tblusers U        
 where  T.employeeid = U.employeeid        
 and T.employeeid = @employeeid        
 group by paymentdate,U.employeeid,U.abbreviation        
order by paymentdate,U.employeeid,U.abbreviation        
        
--select * from #temp where employeeid = 4014 and activeflag = 1        
        
        
drop table #temp 
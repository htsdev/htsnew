SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_InsertManualCC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_InsertManualCC]
GO




CREATE PROCEDURE [dbo].[USP_HTS_InsertManualCC] 
@fname			varchar(50),
@lname			varchar(50),
@nameoncard		varchar(50),
@ccno			varchar(100),
@expdate		varchar(20),
@cin			varchar(4),
@amount			money,
@clienttype		varchar(20),
@description	varchar(500),
@employeeid		int,
@rowid int = 0 output	
AS
BEGIN
	declare @abbr varchar(10)

	select @abbr  = abbreviation from tblusers where employeeid = @employeeid

	set @description = 'Payment received by '+ @abbr+ ' on '+ dbo.formatdateandtimeintoshortdateandtime(getdate())+ '(' +  @description + ')'

	insert into tblmanualpayment
	(recdate,fname,lname,nameoncard,ccno,expdate,cin,amount,clienttype,description,
	employeeid)

	values(getdate(),@fname,@lname,@nameoncard,@ccno,@expdate,@cin,@amount,@clienttype,@description,
	@employeeid)

select @rowid = scope_identity()


END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/****** Object:  StoredProcedure [dbo].[USP_HTP_CHECK_SERVICETICKET_CATEGORY_BYNAME]  ******/       
/*          
Created By     : Mohammad Nasir
Created Date   : 09/10/2012    
TasK     : 10233
Business Logic : Returns service ticket category number of rows which macthes the name
               
Parameter:         
 @Description : Service Ticket Category.  
*/  
CREATE PROCEDURE [dbo].[USP_HTP_CheckServiceTicketCategoryByName]  
 @Description VARCHAR(50)
 
 
 As
 DECLARE @CATEGORY_COUNT INT
 
 SELECT @CATEGORY_COUNT = COUNT(ID) FROM tblServiceTicketCategories tstc
 WHERE LTRIM(RTRIM([DESCRIPTION])) = LTRIM(RTRIM (@Description))
 
 IF @CATEGORY_COUNT > 0
	SELECT 1
 ELSE 
 	SELECT 0
 	
GO
GRANT EXECUTE ON [dbo].[USP_HTP_CheckServiceTicketCategoryByName] TO dbr_webuser 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_MissedCourtInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_MissedCourtInfo]
GO


CREATE PROCEDURE [dbo].[USP_HTS_Get_MissedCourtInfo]
@StartDate_Input as datetime,
@EndDate_Input as datetime,
@ClientStatus as int
AS
Declare @StartDate as datetime
Declare @EndDate as datetime
Set @StartDate = Convert(datetime, Convert(Varchar(12), @StartDate_Input, 101)) + '00:00:00:000'
Set @EndDate = Convert(datetime, Convert(Varchar(12), @EndDate_Input, 101)) + '23:59:59:998'

SELECT     Distinct dbo.tblTickets.Lastname + ', ' + dbo.tblTickets.Firstname AS Name,  CONVERT(varchar(5), 
                      isnull(dbo.tblTicketsViolations.CourtID,'')) + ' ' + isnull(dbo.tblCourts.CourtName,'') + ', ' + isnull(dbo.tblState.State,'') + ' ' + isnull(dbo.tblTickets.Zip,'') AS Location, 
                      dbo.tblTickets.LanguageSpeak, dbo.tblTickets.BondFlag, 
                      dbo.tblTickets.Activeflag, dbo.tblTickets.TicketID_PK, dbo.fn_FormatContactNumber(dbo.tbltickets.Contact1, dbo.tbltickets.ContactType1) as contact1,
		      dbo.fn_FormatContactNumber(dbo.tbltickets.Contact2, dbo.tbltickets.ContactType2) as contact2, dbo.fn_FormatContactNumber(dbo.tbltickets.Contact3, dbo.tbltickets.ContactType3) as contact3, dbo.tblTicketsViolations.CourtDate
Into #TempCaseStatus
FROM         dbo.tblCourts RIGHT OUTER JOIN
                      dbo.tblCourtViolationStatus RIGHT OUTER JOIN
                      dbo.tblTicketsViolations ON dbo.tblCourtViolationStatus.CourtViolationStatusID = dbo.tblTicketsViolations.CourtViolationStatusID ON 
                      dbo.tblCourts.Courtid = dbo.tblTicketsViolations.CourtID LEFT OUTER JOIN
                      dbo.tblState RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblState.StateID = dbo.tblTickets.Stateid_FK ON dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK
WHERE     (dbo.tblCourtViolationStatus.CourtViolationStatusID = 146) And (dbo.tblTicketsViolations.CourtDate Between @StartDate And @EndDate)
Order By dbo.tblTicketsViolations.CourtDate
If @ClientStatus = 0
	Begin
		Select #TempCaseStatus.*, dbo.fn_HTS_Get_MissedCourtCaseStatus(TicketID_PK, CourtDate) As CaseStatus from #TempCaseStatus order by #TempCaseStatus.CourtDate
	End
Else If @ClientStatus = 1
	Begin
		Select #TempCaseStatus.*, dbo.fn_HTS_Get_MissedCourtCaseStatus(TicketID_PK, CourtDate) As CaseStatus from #TempCaseStatus Where #TempCaseStatus.ActiveFlag = 0 order by #TempCaseStatus.CourtDate
	End
Else If @ClientStatus = 2
	Begin
		Select #TempCaseStatus.*, dbo.fn_HTS_Get_MissedCourtCaseStatus(TicketID_PK, CourtDate) As CaseStatus from #TempCaseStatus Where #TempCaseStatus.ActiveFlag = 1 order by #TempCaseStatus.CourtDate
	End

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


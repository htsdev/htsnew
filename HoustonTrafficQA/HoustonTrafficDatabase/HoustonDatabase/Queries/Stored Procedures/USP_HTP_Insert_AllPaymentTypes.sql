
/*  
Created By : Abbas Qamar
Task Id    : 9957 
Creation Date : 04-01-2011
Business Logic : This procedure insert values into payment type table
Parameter:
			@Description   : Description of payment
			@ShortDescription : Short Description of payment
			@IsActive : Active Flag

*/



Create PROCEDURE [dbo].[USP_HTP_Insert_AllPaymentTypes]  

@Description       varchar(200),
@ShortDescription	varchar(200),
@IsActive			bit

AS		

DECLARE @PaymentId int
SET @PaymentId = (SELECT TOP 1 Paymenttype_PK FROM tblPaymenttype ORDER BY Paymenttype_PK desc)

SET @PaymentId =@PaymentId+1

INSERT INTO tblPaymenttype
(
	Paymenttype_PK,
	[Description],
	ShortDescription,
	IsActive
)
VALUES
(
	@PaymentId,
	@Description,
	@ShortDescription,
	@IsActive
	
)



GO
GRANT EXECUTE ON [dbo].[USP_HTP_Insert_AllPaymentTypes] TO dbr_webuser
GO 
	

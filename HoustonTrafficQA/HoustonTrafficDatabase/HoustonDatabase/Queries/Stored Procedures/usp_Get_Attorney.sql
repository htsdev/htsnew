﻿ /****** 
Altered by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure is used to get attorneys Name.
			
List of Parameters:	
			@IsAttorney  is attorney record		
List of Coloumns
			Name :	 Attorney name
			Employee:Employee identification
			IDAndContact: attorney identification with contact number	
******/
  
Alter PROCEDURE [dbo].[usp_Get_Attorney] 
(@IsAttorney BIT=NULL)
AS
BEGIN
    SELECT lastname+' '+firstname AS NAME
          ,EmployeeID
          --Nasir 6968 12/14/2009 add EmployeeID + IDAndContact
          ,CONVERT(VARCHAR,EmployeeID) + '-' + ISNULL (ContactNumber,'') AS IDAndContact
    FROM   tblusers
    WHERE  isAttorney = COALESCE(@IsAttorney ,isAttorney)
           AND STATUS = 1
           ORDER  BY NAME ASC
END  
  
  
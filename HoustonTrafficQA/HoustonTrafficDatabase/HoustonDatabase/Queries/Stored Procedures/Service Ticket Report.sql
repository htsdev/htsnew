update tbl_report set businesslogic='       <strong class="label">This Report displays the cases of the Clients and Quote Clients whose service ticket flag has either added          or closed.</strong><br />              <br />         <br />          <table style="width: 100%">              
                          <tr>
                              <td class="label" colspan="2" rowspan="1" style="height: 51px" valign="top"><b>
                                  User can search the cases based on following criteria:</b></td>
                          </tr>
                          <tr >                  <td valign="top" class="label" colspan="" rowspan="" style="height: 35px">                      1. Open Ticket:                  </td>                  <td class="label" colspan="" rowspan="" valign="top" style="height: 35px">                      This option displays the records of the clients whose service ticket flag has been                      added.</td>              </tr>              <tr>                  <td style="width: 30%; height: 52px;" valign="top" class="label">                      2. Closed Ticket:                  </td>                  <td class="label" valign="top" style="height: 52px">                      If this option is selected then only those clients records will be displayed whose service                      ticket flag has been closed.</td>              </tr>              
             <tr>
                 <td class="label" style="width: 30%; height: 34px;" valign="top">
                     3. Date Range</td>
                 <td class="label" valign="top" style="height: 34px">
                     User can search the records within specific date range by selecting Start Date and
                     End Date on the report.&nbsp;</td>
             </tr>
       <tr>
           <td class="label" valign="top" style="height: 117px">
               4. Show all user Ticket<br />
               &nbsp; &nbsp; and Show all my &nbsp;<br />
               &nbsp; &nbsp; Ticket: &nbsp;&nbsp;
           </td>
           <td class="label" valign="top" style="height: 117px">
               If both of these checkboxes are unchecked then only those records will be displayed
               whose Open Date is within the given date range and assigned to currently login user.
               If Show all My Ticket is checked then records will be displayed of currently login
               user without restriction of given dates and if Show All User Ticket is checked then
               records will be displayed from all users without restriction of given dates.
           </td>
       </tr>
                       <tr>
                           <td class="label" style="width: 30%;" valign="top">
                               Close Service Ticket
                               Email&nbsp;Logic: &nbsp;&nbsp;
                           </td>
                           <td class="label" valign="top">
                               It is necessary to complete the ticket to 100% before closing it. When we closed service
                               ticket an email will be send to the address defined in "CloseServiceUser" key in
                               web.config. If service ticket is closed and the category is Angry Client then email
                               will be send to the address defined in "CloseServiceUser" and "AngryClientClosedEmail"
                               keys in web.config.</td>
                       </tr>
       </table>
    
   
   
 
       
 '
where menu_id=140 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_emailtrialnotification]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_emailtrialnotification]
GO


--- This procedue update tblemailtrialnotification and insert Confirmation date when client click on Confirmation link 

create procedure [dbo].[USP_HTS_Update_emailtrialnotification]

@EmailID varchar(250)

AS

Update tblemailtrialnotification set ConfirmationDate = getdate()
where EmailID = @EmailID


------------------------------------------------------------------------------------------------------------------------------------



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


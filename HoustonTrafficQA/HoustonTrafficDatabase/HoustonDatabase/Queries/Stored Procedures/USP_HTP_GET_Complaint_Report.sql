﻿/****** 
Created by:		Muhammad Nasir
Task ID :		6098
Business Logic:	The procedure is used by houston to get the complaint by given search criteria its should be complaint and not reviewed.
				
List of Parameters:	
	@startdate:		Starting date of complaint flag add
	@endListDAte:	Ending date of complaint flag add
	@CaseType		Civil, Criminal, Traffic, Family
	@Attorney		Attorney who was covering the case
	@ShowAll		By default all non reviewed cases will be displayed

List of Columns:	
	TicketID_PK:	Case number.
	FullName:		FullName of client
	Area			 case type Civil, Criminal, Traffic, Family
	AttorneyName:	Attorney who was covering the case
	CDate:			complaint date and time
	Complaint:		system shows the full complaint comment

******/

--USP_HTP_GET_Complaint_Report '1/27/2010','1/27/2010',-1,-1,1 

ALTER PROCEDURE dbo.USP_HTP_GET_Complaint_Report
	@StartDate DATETIME,
	@EndDate DATETIME,
	@CaseType INT,
	@Attorney INT,
	@ShowAll BIT
AS

	SET NOCOUNT ON;
    SELECT DISTINCT tt.TicketID_PK,
           ttn.NotesID_PK,
           tt.Firstname + ' ' + tt.Lastname AS FullName,
           --Yasir Kamal 7315 01/26/2010 court settings columns added.
           ISNULL(c.ShortName, 'N/A') AS CourtLocation,
           ISNULL(dbo.fn_DateFormat(ttv.CourtDateMain, 24, '/', ':', 1), '') AS 
           Courtdate,
           ISNULL(ttv.CourtDateMain, '') AS CourtDateMain,
           ISNULL(dt.Description, 'N/A') AS STATUS,
           --           ct.CaseTypeName AS Area,
           tf.FirmName AS AttorneyName,
           -- Noufil 6098 09/25/2009 code commented
           dbo.fn_DateFormat(ttn.RecDate, 27, '/', ':', 1) + ' @ ' + dbo.fn_DateFormat(ttn.RecDate, 26, '/', ':', 1) AS 
           CDate,
           ttn.[Subject] AS Complaint,
           CASE ttn.Fk_CommentID
                WHEN 1 THEN tt.GeneralComments
                WHEN 3 THEN ttv.ReminderComments
           END AS Comments,
           -- Noufil 6098 09/25/2009 use recdate of tblTicketsNotes
           ttn.RecDate
    FROM   dbo.tblCourts AS c WITH(NOLOCK) 
           RIGHT OUTER JOIN dbo.tblTickets AS tt WITH(NOLOCK) 
           INNER JOIN dbo.tblTicketsNotes AS ttn WITH(NOLOCK) 
                ON  tt.TicketID_PK = ttn.TicketID
           INNER JOIN dbo.tblTicketsFlag AS ttf WITH(NOLOCK) 
                ON  tt.TicketID_PK = ttf.TicketID_PK
           INNER JOIN dbo.tblTicketsViolations AS ttv WITH(NOLOCK) 
                ON  tt.TicketID_PK = ttv.TicketID_PK
           INNER JOIN dbo.tblFirm AS tf WITH(NOLOCK) 
                ON  ttv.CoveringFirmID = tf.FirmID
                ON  c.Courtid = ttv.CourtID
           LEFT OUTER JOIN dbo.tblDateType AS dt WITH(NOLOCK) 
           RIGHT OUTER JOIN dbo.tblCourtViolationStatus AS cvs WITH(NOLOCK)
                ON  dt.TypeID = cvs.CategoryID
                ON  ttv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID
    WHERE  --Ozair 7791 07/24/2010  where clause optimized  
           ttv.TicketsViolationID IN (SELECT TOP 1 ticketsviolationid
                                      FROM   tblticketsviolations WITH(NOLOCK) 
                                      WHERE  TicketID_PK = tt.TicketID_PK)
           AND (
                   @ShowAll = 1
                   OR (
                          -- Noufil 6098 09/25/2009 use recdate of tblTicketsNotes
                          DATEDIFF(DAY, ttn.RecDate, @StartDate) <= 0
                          AND DATEDIFF(DAY, ttn.RecDate, @EndDate) >= 0
                      )
               )
           AND ttn.IsReviewed = 0
           AND ttn.IsComplaint = 1
           AND (@Attorney = -1 OR ttv.CoveringFirmID = @Attorney)
           AND (@CaseType = -1 OR tt.CaseTypeId = @CaseType)
    ORDER BY
           FullName,
           tt.TicketID_PK


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_TrialLetterClients_ForBatch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_TrialLetterClients_ForBatch]
GO

  
  
  
CREATE procedure [dbo].[USP_HTS_Get_TrialLetterClients_ForBatch]  
  
@TrialLetterDays int = 7  
  
as  
  
SELECT   
 'TrialNotificationBatch' as Description,  
 t.TicketID_PK As TicketID,  
 isnull(t.LastName,'') + ', ' + isnull(t.FirstName,'') As ClientName,  
 t.EmployeeID,  
 t.Email AS EMailAddress,  
 dbo.fn_DateFormat(getdate(), 23, '','',1) + '-' + (convert(varchar,t.TicketID_PK) + '- Trial Letter Notification.PDF') as PDFFileName,  
 'Not Processed' as IsProcessed  
into #Temp  
FROM    tblTickets t     
INNER JOIN    
 tblTicketsViolations tv     
ON  t.TicketID_PK = tv.TicketID_PK     
LEFT OUTER JOIN    
 tblCourtViolationStatus cvs     
ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID  
where (datediff (day,getdate(),isnull(tv.CourtDateMain,'01/01/1900')) < @TrialLetterDays  and isnull(tv.CourtDateMain,'01/01/1900')> getdate())  
and cvs.CategoryID in (2,3,4,5) and tv.CourtID not in (3001,3002,3003)   
and t.Ticketid_pk not in (Select distinct ticketid_pk from tblEmailTrialNotification)  
and t.ticketid_pk not in(select TicketID_FK from tblHTSBatchPrintLetter where Ticketid_FK =t.ticketid_pk and IsPrinted =0)  
and (isnull(t.jurystatus,0) =0 or isnull(t.jurystatus,0) =4)  
and t.ActiveFlag =1
and tv.courtviolationstatusidmain not in(50,80)  
  
Insert into #Temp  
SELECT  
 'TrialNotificationBatch' as Description,  
 t.TicketID_PK As TicketID,   
 isnull(t.LastName,'') + ', ' + isnull(t.FirstName,'') As ClientName,  
 t.EmployeeID,  
 t.Email AS EMailAddress,  
 dbo.fn_DateFormat(getdate(), 23, '','',1) + '-' + (convert(varchar,t.TicketID_PK) + '- Trial Letter Notification.PDF') as PDFFileName,  
 'Not Processed' as IsProcessed  
FROM    tblTickets t     
INNER JOIN    
 tblTicketsViolations tv     
ON  t.TicketID_PK = tv.TicketID_PK     
LEFT OUTER JOIN    
 tblCourtViolationStatus cvs     
ON  tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID  
where (datediff (day,getdate(),isnull(tv.CourtDateMain,'01/01/1900')) < @TrialLetterDays  and isnull(tv.CourtDateMain,'01/01/1900')> getdate())  
and cvs.CategoryID = 4 and tv.CourtID in (3001,3002,3003)  
and t.Ticketid_pk not in (Select distinct ticketid_pk from tblEmailTrialNotification)  
and t.ticketid_pk not in(select TicketID_FK from tblHTSBatchPrintLetter where Ticketid_FK =t.ticketid_pk and IsPrinted =0)  
and (isnull(t.jurystatus,0) =0 or isnull(t.jurystatus,0) =4)  
and t.ActiveFlag =1
and tv.courtviolationstatusidmain not in(50,80)
  
Select distinct Description, TicketID, ClientName, EmployeeID, EMailAddress, PDFFileName, IsProcessed into #temp2 from #Temp   
Select * from #temp2 order by TicketID  
  
drop table #Temp
drop table #temp2
  
  
  
  













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


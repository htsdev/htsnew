set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Create by:  Muhammad Kazim
Business Logic : this procedure is used to delete all the files of particular court.
 
List of Parameters:     
      @Courtid : Courtid that has to be deleted
 
******/
 


Create procedure [dbo].[USP_HTP_Delete_AllCourtFiles]
@courtid int
as
delete from courtfiles where courtid=@courtid
select @@rowcount


go





grant execute on dbo.[USP_HTP_Delete_AllCourtFiles] to [dbr_webuser]
go









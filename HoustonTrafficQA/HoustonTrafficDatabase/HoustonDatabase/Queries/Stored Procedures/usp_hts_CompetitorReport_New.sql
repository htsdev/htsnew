
-- usp_hts_CompetitorReport_New '6/23/2008','6/23/2008',1           
ALTER procedure [dbo].[usp_hts_CompetitorReport_New]            
            
@startdate datetime,            
@enddate datetime,            
@Type int -- 0->Att, 1-> Bond            
            
as            

-- BOND TRACKER REPORT...
if @Type=1            
begin            
	select t.midnumber,             
	BondCompany=isnull(tv.bondingcompanyname,tv.bondingnumber), 
	isnull(tv.fineamount,0)  as BondAmount,
	c.categoryid            
	into #temp            
	from tblticketsviolationsarchive tv inner join tblcourtviolationstatus c            
	on c.courtviolationstatusid=tv.violationstatusid inner join tblticketsarchive t  on t.recordid=tv.recordid           
	where   ( 
		len(isnull(tv.bondingcompanyname,''))>0
		or 
		len(isnull(tv.bondingnumber,''))>0	
		 )        
	and c.categoryid in (2,4,5)       
	and datediff(day,  tv.initialeventdate_bondingcompany , @startdate) <=0
	and datediff(day,  tv.initialeventdate_bondingcompany , @enddate) >=0       

            
	declare @totalcases float            
	select @totalcases=count(*) from #temp  
	declare @totalclients float 
	declare @JuryCount int,@NonJuryCount int ,@ArrCount int

	select count(distinct midnumber) as sumBond_clients
	into #SumBond1
	from #temp            
	group by BondCompany     
	Set @totalclients = (select sum(sumBond_clients) from #SumBond1)
	drop table #SumBond1

            
	select BondCompany as name,
	count(BondCompany) as noofcases,
	count(distinct midnumber) as sumBond_clients,
	sum(BondAmount) as AmountSum, 
	round(((count(BondCompany)/@totalcases)*100),2) as percentage,
	(round((count(distinct midnumber) *100 / @totalclients),2)) as perc_clients,
	count(case when categoryid=2 then categoryid end)as ArrCount,
	count(case when categoryid=4 then categoryid end)as JuryCount,
	count(case when categoryid=5 then categoryid end)as NonJuryCount
	from #temp       
	group by BondCompany  
	order by noofcases desc            

            
 drop table #temp            
 
end     

-- ATTORNEY TRACKER REPORT...       
else            
	begin            
		select t.midnumber,            
		Attorney=p.name,
		c.categoryid     
		into #tempp            
		from tblticketsviolationsarchive tv inner join tblcourtviolationstatus c            
		on c.courtviolationstatusid=tv.violationstatusid inner join tblticketsarchive t            
		on t.recordid=tv.recordid inner join tblCompetitors p    
		on (isnull(tv.attorneyname,'')=p.name or isnull(tv.barcardnumber,'')=p.name )   
		where            
		(            
		isnull(tv.barcardnumber,'')<>''            
		or isnull(tv.attorneyname,'')<>''            
		)            
		and             
		(            
		isnull(tv.bondingnumber,'')=''            
		and isnull(tv.bondingcompanyname,'')=''            
		)            
		and c.categoryid=4  
		-- tahir 5654 03/13/2009 fixed the time part issue...
		--and tv.initialeventdate_attorney between @startdate and @enddate           
		and datediff(day,  tv.initialeventdate_attorney , @startdate) <=0
		and datediff(day,  tv.initialeventdate_attorney , @enddate) >=0 

		--and p.isattorney=1   

		declare @totalcasess float            
		select @totalcasess=count(*) from #tempp            

		/*Total Clients*/
		select count(distinct midnumber) as sumBond_clients
		into #SumBond
		from #tempp            
		group by Attorney     
		Set @totalclients = (select sum(sumBond_clients) from #SumBond)
		drop table #SumBond
		/*Total Clients*/
		/*kamran 3624 04/11/08 persentage of no of cases */
		select Attorney as name,count(Attorney) as noofcases,count(distinct midnumber) as sumBond_clients,
		0 as AmountSum,round(((count(Attorney)/@totalcasess)*100),2) as percentage,
		(round((count(distinct midnumber) *100 / @totalclients),2)) as perc_clients,
		0 as ArrCount,count(case when categoryid=4 then categoryid end)as JuryCount,0 as NonJuryCount        
		--into #tempp1            
		from #tempp            
		group by Attorney     
		order by noofcases desc           

		drop table #tempp            

	end







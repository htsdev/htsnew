﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/20/2009 11:25:47 AM
 ************************************************************/

/***  
* Waqas Javed 5771 04/16/2009  
* Business Logic :  This procedure is used to update ContactIDConfirmed Flag of the client.  
* If Flag is 1 then Flag will be updated and history note will be added also.  
* If Flag is 0 then Flag will be updated and disassociate the contact id of the case.
* 
* Parameters:
*   @TicketID
*   @IsContactIDConfirmed
*   @EMPID   
***/  
  
CREATE PROCEDURE dbo.USP_Htp_Update_ContactIDConfirmedFlag
	@TicketID INT,
	@IsContactIDConfirmed INT,
	@EMPID INT
AS
	UPDATE tblTickets
	SET    IsContactIDConfirmed = @IsContactIDConfirmed
	WHERE  TicketID_PK = @TicketID    
	
	IF (@IsContactIDConfirmed = 1)
	BEGIN
	    DECLARE @Name AS VARCHAR(50),
	            @CID AS INT    
	    
	    SELECT @Name = (
	               SELECT ISNULL(tu.Firstname, '') + ' ' + ISNULL(tu.Lastname, '')
	               FROM   tblUsers tu
	               WHERE  tu.EmployeeID = @EMPID
	           )    
	    
	    SELECT @CID = (
	               SELECT contactid_fk
	               FROM   tblTickets tt
	               WHERE  tt.TicketID_PK = @TicketID
	           )    
	    
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    VALUES
	      (
	        @TicketID,
	        'CID <a href="javascript:window.open(' +
	        '''/ClientInfo/AssociatedMatters.aspx?cid=' + CONVERT(VARCHAR, @TicketID) 
	        + ''');void('''');">' + CONVERT(VARCHAR, @CID) +
	        '</a> Confirmed - ' + @Name + ' ',
	        @EMPID
	      )
	END
	ELSE
	BEGIN
	    EXEC USP_HTP_Disassociate_ContactID @TicketID,
	         @EMPID
	END
GO

GRANT EXECUTE ON [dbo].[USP_Htp_Update_ContactIDConfirmedFlag] TO dbr_webuser

GO

   
   
   
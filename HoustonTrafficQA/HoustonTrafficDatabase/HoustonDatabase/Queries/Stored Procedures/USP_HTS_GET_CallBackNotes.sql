SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CallBackNotes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CallBackNotes]
GO










CREATE procedure USP_HTS_GET_CallBackNotes   
  
@ticketid int      --Providing Ticketid to get callnotes info  
    
as  
  
select top 1    
  t.firstname+' '+t.lastname as ClientName,  
  convert(varchar(12),dbo.formatphonenumbers(isnull(T.Contact1,'0000000000'))) as contactnumber,--telephone    
  convert(varchar(10),convert(datetime,dbo.formatdate(TTV.CourtDateMain)),101) as appeardate,   --Apperance   
  convert(varchar(12),T.Callbackdate,101),  
  generalcomments,  
  isnull(callstatusforquote,0) as callstatus   --status id  
  
  
from tbltickets t,tblticketsviolations ttv  
where t.ticketid_pk=ttv.ticketid_pk and  
 t.ticketid_pk=@ticketid  
    






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


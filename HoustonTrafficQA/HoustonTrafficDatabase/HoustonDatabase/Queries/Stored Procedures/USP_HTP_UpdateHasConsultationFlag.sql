﻿/************************************************************
 * Created By : Noufil Khan
 * Create Date : 2/10/2010 5:38:53 PM
 * Business Logic : This procedure update Hasconsultationflag for Client
 * Parameter :
 *				@TicketId : TicketID of Client.
				@HasConsultationFlag : Client has/ hasnot Consulation Flag.
 * Column Return : Number of rows effected
 ************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_UpdateHasConsultationFlag]
	@TicketId INT,
	@HasConsultationFlag BIT
AS
	UPDATE tblTickets
	SET    hasConsultationComments = @HasConsultationFlag
	WHERE  TicketID_PK = @TicketId
	SELECT @@ROWCOUNT
	
GO

GRANT EXECUTE ON [dbo].[USP_HTP_UpdateHasConsultationFlag] TO dbr_webuser
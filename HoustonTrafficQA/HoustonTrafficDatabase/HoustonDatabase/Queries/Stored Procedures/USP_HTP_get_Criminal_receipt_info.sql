
/*        
TasK		   : 9949        
Business Logic : This procedure retrived all the information about the Reciept Report's Documnets for the Criminal Cases.  
           
Parameter:       
	@ticketID   : identifiable key  TicketId      
	@employeeid		: Employee which have Right to access the system
*/ 
     
--  [dbo].[USP_HTP_get_Criminal_receipt_info] 161322,3991      

CREATE   procedure [dbo].[USP_HTP_get_Criminal_receipt_info]
@ticketID int ,                                          
@employeeid int = 3992      
as                                          
      
declare @paymentamount money                                          
declare @casenumbers varchar(1000),                                          
@paymentinfo varchar(4000),                                          
@contactinfo varchar(100),                                  
@batchid     int      
                                
                              
set @batchid=0                              
                                          
select @paymentamount = sum(chargeamount)                                           
from tblticketspayment where ticketid = @ticketID                                          
and paymenttype not in (99,100)                                          
and paymentvoid not in (1)                                          
                                          
                                          
set @casenumbers = ' '                                          
select @casenumbers = @casenumbers + (case when casenumassignedbycourt is null or casenumassignedbycourt ='' or left(casenumassignedbycourt, 2) = 'ID' then isnull(refcasenumber,' ') else casenumassignedbycourt end) + ','                                   
  
    
        
                                          
from tblticketsviolations                                          
where ticketid_pk = @ticketid                                          
set @casenumbers = ltrim(left(@casenumbers,len(@casenumbers)-1))                                          
        
declare @Address1 as varchar(50)        
declare @Address2 as varchar(50)  
      
set @Contactinfo = 
	(
	
	SELECT ISNULL(dbo.formatphonenumbers(t.contact1), '') + ' ' + CASE tc1.ContactType_PK WHEN 0 THEN ' 'ELSE ISNULL(tc1.[Description], '') END +
CASE ISNULL(dbo.formatphonenumbers(t.contact2), '') WHEN '' THEN '' ELSE CASE ISNULL(dbo.formatphonenumbers(t.contact1), '') WHEN '' THEN '' ELSE ' / ' END END + ISNULL(dbo.formatphonenumbers(t.contact2), '') + ' ' + CASE tc2.ContactType_PK WHEN 0 THEN ' ' ELSE ISNULL(tc2.[Description], '') END +
CASE ISNULL(dbo.formatphonenumbers(t.contact3), '') 
WHEN '' 
	THEN '' 
	ELSE 
	CASE ISNULL(dbo.formatphonenumbers(t.contact2), '') WHEN '' THEN CASE ISNULL(dbo.formatphonenumbers(t.contact1), '') WHEN '' THEN '' ELSE ' / ' END ELSE ' / ' END
	
  END
 + ISNULL(dbo.formatphonenumbers(t.contact3), '') + ' ' + CASE tc3.ContactType_PK WHEN  0 THEN ' ' ELSE ISNULL(tc3.[Description], '') END
from tbltickets t 
LEFT OUTER JOIN tblContactstype tc1 ON tc1.ContactType_PK = t.ContactType1
LEFT OUTER JOIN tblContactstype tc2 ON tc2.ContactType_PK = t.ContactType2
LEFT OUTER JOIN tblContactstype tc3 ON tc3.ContactType_PK = t.ContactType3
	where ticketid_pk = @ticketid
	)

create table #ResultSet       
(ticketid_pk int,      
firstname varchar(50),      
MiddleName varchar(50),      
lastname varchar(50),      
Address1 varchar(100),      
Address2 varchar(100),      
ticketnumber_pk varchar(8000),      
languagespeak varchar(50),      
batchid int,      
paymentamount money,      
city varchar(50),      
state varchar(10),      
zip varchar(20),      
Address varchar(100),      
currentdateset datetime,      
totalfeecharged money,      
dueamount int,      
casenumbers varchar(8000),      
settintype varchar(50),      
paymentinfo varchar(8000),      
contactinfo varchar(200),      
FirmName varchar(50),      
ctno varchar(50),      
empName varchar(50),      
CaseNumber varchar(100),      
Status varchar(50),      
CourtDateTime datetime,      
CourtNo varchar(50),      
Location varchar(50),      
RowNo int NOT NULL IDENTITY (1, 1),      
ViolDesc varchar(100),
hasBond INT,     
Email VARCHAR(100),
DLNumber VARCHAR(30),
DOB DATETIME,
ArrestDate DATETIME,
CountyName VARCHAR(100),
ArrestingAgencyName VARCHAR(50),
ALROfficerName VARCHAR(50),
IsALRHearingRequired BIT,
ALRIntoxilyzerResult VARCHAR(10)
)                                     
declare @empName varchar(50)            
declare @numPayment int            
declare @numPlan int

set @empName=(select upper(firstname)+' '+upper(lastname) from tblusers where employeeid=@employeeid)                    
Set @numPlan = (Select Count(TicketID_PK) from tblSchedulePayment where ticketid_pk = @ticketid and ScheduleFlag <> 0)
                             
set @paymentinfo = ''                                          
set @numPayment=0                    
select @paymentinfo =          
        
(        
case when p.paymenttype=5 then @paymentinfo +   
 '$' + convert(varchar(20),convert(int,chargeamount)) +   
 (case when len(chargeamount) = 4 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 5 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 6 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 7 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 8 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) > 8 then ' ' + char(9) + char(9) + char(9) + char(9)  
 else ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 end)  
 +   
 isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'') +   
 (case when len(isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'')) >= 4 and len(isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'')) < 11 then  
 ' ' + char(9) + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'')) >= 11 and len(isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'')) < 16 then  
 ' ' + char(9) + char(9) + char(9) + char(9)  
 when len(isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'')) > 16 then   
 ' ' + char(9) + char(9) + char(9)  
 else ' ' + char(9) + char(9) + char(9)  
 end)  
+  
 dbo.formatdateandtimeintoshortdateandtime(p.recdate) +   
 (case when len(dbo.formatdateandtimeintoshortdateandtime(p.recdate)) <= 17 then   
' ' + char(9) + char(9) + char(9) + char(9)  
else  
' ' + char(9) + char(9) + char(9)  
end)  
+  
 u.firstname + char(13)        
  
else  
  
@paymentinfo +   
 '$' + convert(varchar(20),convert(int,chargeamount))+         
(case when len(chargeamount) = 4 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 5 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 6 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 7 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) = 8 then ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(chargeamount) > 8 then ' ' + char(9) + char(9) + char(9) + char(9)  
 else ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 end)  
+  
 t.description +   
  (case when len(isnull(t.description,'')) >= 4 and len(isnull(t.description,'')) < 6 then  
 ' ' + char(9) + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(isnull(t.description,'')) >= 6 and len(isnull(t.description,'')) < 13 then  
 ' ' + char(9) + char(9) + char(9) + char(9) + char(9)  
 when len(isnull(t.description,'')) >= 6 and len(isnull(t.description,'')) < 16 then  
 ' ' + char(9) + char(9) + char(9) + char(9)  
 when len(isnull(t.description,'')) > 16 then   
 ' ' + char(9) + char(9) + char(9)  
 else ' ' + char(9) + char(9) + char(9)  
 end)  
+  
 dbo.formatdateandtimeintoshortdateandtime(p.recdate) +   
(case when len(dbo.formatdateandtimeintoshortdateandtime(p.recdate)) <= 17 then   
' ' + char(9) + char(9) + char(9) + char(9)  
else  
' ' + char(9) + char(9) + char(9)  
end)  
 +  
 u.firstname + char(13)        
end  
),        
 @numpayment=@numpayment+1                      
from tblticketspayment p inner join tblpaymenttype t                    
 on p.paymenttype=t.paymenttype_pk left outer join tblcctype c      
 on p.cardtype=c.cctypeid left outer join tblusers u      
 on p.employeeid = u.employeeid      
      
where ticketid = @ticketID                    
and paymenttype not in (99,100)                                          
and paymentvoid not in (1)                    
                                       
insert into #ResultSet (ticketid_pk, firstname, MiddleName, lastname, Address1, Address2, ticketnumber_pk, languagespeak, batchid, paymentamount, city, state, zip, Address, currentdateset, totalfeecharged, dueamount, casenumbers, settintype, paymentinfo, 
  
    
contactinfo, FirmName, ctno, empName, CaseNumber, Status, CourtDateTime, CourtNo, Location, ViolDesc, hasBond , Email,DLNumber,DOB,ArrestDate,CountyName,ArrestingAgencyName,ALROfficerName,IsALRHearingRequired,ALRIntoxilyzerResult)
select T.ticketid_pk, firstname, MiddleName, lastname,        
         
(case when T.FirmID =3000 then T.Address1 else (Select Address from tblfirm where Firmid = T.FirmId)  end) as Address1,        
(case when T.FirmID =3000 then T.Address2 else (Select Address2 from tblfirm where Firmid = T.FirmId) end ) as Address2,        
        
 @casenumbers as ticketnumber_pk,                            
 t.languagespeak, @batchid as batchid, @paymentamount as paymentamount,         
        
 (case when T.FirmID = 3000 then T.city else (select city from tblfirm where FirmID =T.FirmID) end) as city,        
 (case when T.FirmID =3000 then S.state else (select S.state where S.StateID =(select state from tblfirm where FirmID =T.FirmID))end ) as state,        
 (case when T.FirmID =3000 then T.zip else (select Zip from tblfirm where FirmID =T.FirmID) end )as zip,        
         
 C.Address + ' ' + C.Address2 + ', ' + C.city + ', ' + 'TX ' + C.zip as Address,                                          
 v.courtdatemain as currentdateset, totalfeecharged, Convert(int,totalfeecharged-@paymentamount) as dueamount,                                          
 @casenumbers as casenumbers, o.description as settintype, @paymentinfo as paymentinfo,         
        
(case when T.FirmID =3000 then @contactinfo else (select dbo.formatphonenumbers(Phone) from tblfirm where FirmID =T.FirmID) end) as contactinfo,                       
(case when T.FirmID = 3000 then (isnull(Firstname,'') +' ' + isnull(MiddleName,'') + ' ' + isnull(Lastname,'')) else (select FirmName from tblfirm where tblfirm.FirmID =T.FirmID) end) as FirmName,
                     
 v.courtnumbermain as ctno ,                    
 @empName as empName ,                  
(case when V.casenumassignedbycourt is null or V.casenumassignedbycourt ='' or left(V.casenumassignedbycourt, 2) = 'ID' then isnull(V.refcasenumber,' ') else V.casenumassignedbycourt end) as CaseNumber,               
  o.shortdescription as Status,                  
  v.courtdatemain as CourtDateTime,                  
  v.courtnumbermain as CourtNo,                  
  c.shortname as Location,      
  UPPER(case when len(isnull(vd.description,''))>25 then left(vd.description,25)+'....' else isnull(vd.description,'') END) as ViolDesc, 
  t.BondFlag,    
  t.Email,		 
  t.DLNumber,t.DOB,v.ArrestDate,tmc.CountyName,v.ArrestingAgencyName,t.ALROfficerName,t.IsALRHearingRequired,t.ALRIntoxilyzerResult
from tbltickets T,  tblcourts C, tbldatetype D ,tblstate S , tblticketsviolations V , tblcourtviolationstatus O, tblviolations vd ,tbl_Mailer_County tmc
      
where      
T.stateid_fk=S.stateid                                    
and T.ticketid_pk = V.ticketid_pk                              
and V.courtviolationstatusidmain = O.courtviolationstatusid                              
and O.categoryid = D.typeid                                      
and T.ticketid_pk in (@ticketID)                                          
and activeflag = 1                                     
and c.courtid = v.courtid      
and v.ViolationNumber_PK=vd.ViolationNumber_PK
AND tmc.CountyID=c.CountyId
      
select *,  @numPlan as NoOfPaymentPlans  from #ResultSet      
      
drop table #ResultSet      


GO
GRANT EXEC ON [dbo].[USP_HTP_get_Criminal_receipt_info] TO dbr_webuser
GO

  



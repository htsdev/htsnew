set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go






-- =============================================  
-- Created By:  Zeeshan Zahoor  8/05/208
-- Business Logic: This is proceedure is used to display the bond status of active cliens in the HTP System in validation reports.  
-- List of Parameters: 
--		@TicketID_PK:		unique identifier
--		@BondFollowUpDate:	Bond Follow Update to be update
-- List of Columns: 
--		TicketID_PK:			This field is the unique identifier from tblTickets table.
--		BondFollowUpDate:		Bond Follow Update to be insert/update.   



ALTER PROCEDURE [dbo].[USP_HTP_Update_BondFollowUpDate]

@TicketID_PK int,
@EmployeeID  INT,
@BondFollowUpDate datetime=NULL
AS

BEGIN

declare @L_Exist int 
set @L_Exist =0

select  @L_Exist = Count(TicketID_PK) from tblTicketsExtensions
where TicketID_PK = @TicketID_PK
--Muhammad Muneer 8285 9/17/2010 added the functionality to log in tblticketNotes
DECLARE @OldFollowUpDate DATETIME	        

if(@L_Exist=0)
	begin
	insert into tblTicketsExtensions(TicketID_PK ,BondFollowUpDate)
	values(@TicketID_PK ,@BondFollowUpDate)


	end
else
	begin	
--Muhammad Muneer 8285 9/17/2010 added the functionality to log in tblticketNotes

		set @OldFollowUpDate = (SELECT BondFollowUpDate FROM   tblTicketsExtensions WHERE  TicketID_PK = @TicketID_PK)
		
			update tblTicketsExtensions
			set BondFollowUpDate = @BondFollowUpDate			
			where TicketID_PK = @TicketID_PK
	
	end

--Muhammad Muneer 8285 9/17/2010 added the functionality to log in tblticketNotes

DECLARE @Message AS VARCHAR(4000)
 


IF @OldFollowUpDate IS NULL
BEGIN
SET @Message = 'Bond Follow Up Date has changed from N/A to ' + CONVERT(VARCHAR, @BondFollowUpDate, 101)
END
ELSE
	BEGIN
SET @Message = 'Bond Follow Up Date has changed from '+CONVERT(VARCHAR, @OldFollowUpDate, 101)+' to ' + CONVERT(VARCHAR, @BondFollowUpDate, 101)
	END
	            
       EXECUTE [dbo].[usp_HTS_Insert_CaseHistoryInfo] @TicketID_PK, @Message ,  @EmployeeID,''
END




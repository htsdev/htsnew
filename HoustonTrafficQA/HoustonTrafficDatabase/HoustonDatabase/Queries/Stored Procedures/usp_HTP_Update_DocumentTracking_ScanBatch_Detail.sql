﻿/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to update the information retrived after performing OCR.

List of Parameters:	
	@ScanBatchDetailID : id against which records are updated
	@DocumentBatchID : docuemnt id 
	@DocumentBatchPageCount : total page count
	@DocumentBatchPageNo : page no
	@OCRData : Complete data retrived from OCR process

******/


Create Procedure dbo.usp_HTP_Update_DocumentTracking_ScanBatch_Detail

@ScanBatchDetailID int,
@DocumentBatchID int,
@DocumentBatchPageCount int,
@DocumentBatchPageNo int,
@OCRData varchar(max)

as

update tbl_HTP_DocumentTracking_ScanBatch_Detail
set
	DocumentBatchID = @DocumentBatchID,
	DocumentBatchPageCount = @DocumentBatchPageCount,
	DocumentBatchPageNo = @DocumentBatchPageNo,
	OCRData=@OCRData

where ScanBatchDetailID = @ScanBatchDetailID

go

grant exec on dbo.usp_HTP_Update_DocumentTracking_ScanBatch_Detail to dbr_webuser
go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_scan_insert_tblscandata]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_scan_insert_tblscandata]
GO

CREATE PROCEDURE [dbo].[usp_scan_insert_tblscandata]  
 (     
@doc_id int,    
@docnum_2  int,        
  @data_3  text -- due to some unusual problem. used text instead of varchar
)        
        
AS INSERT INTO tblscandata  
  ( doc_id,    
docnum,        
  data)         
         
VALUES         
 ( @doc_id,    
@docnum_2,        
  Convert(varchar(8000),@data_3))      
      
    
  
  














      
        
    
       
    
    
    
  
  
  
  














        
        
      
      
      
      
      
      
      
      
    




       
            
            
            
















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WEBSCAN_Recovery_12MAR07]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WEBSCAN_Recovery_12MAR07]
GO

CREATE Procedure usp_WEBSCAN_Recovery_12MAR07    
    
as    
  
select o.CauseNo, p.BatchID,  o.picid 
from tbl_webscan_ocr o inner join 
	tbl_webscan_pic p on p.id=o.picid
where o.checkstatus=4 and p.batchid=0 --331
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


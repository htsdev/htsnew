SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Get_SignatureLocation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Get_SignatureLocation]
GO








--[USP_HTS_ESignature_Get_SignatureLocation]     'ImageStream_Bond_ClientInitial3', 4, 'Bond', 124546, 'Client', 'ClientInitialsPage'
CREATE PROCEDURE [dbo].[USP_HTS_ESignature_Get_SignatureLocation]     
@ImageTrace as varchar(50),    
@SignLocation as int, -- 0 - FirstSignLocation, 1 - NextSignLocation,  2 - PreviousSignLocation,  3 - LastSignLocation,  4 - LastSignLocation
@KeyWord as varchar(50),    
@TicketID as int,  
@Entity as varchar(20),  
@PageDescription as varchar(30)
AS    
    
BEGIN    
Declare @TopIncreament as int    
Set @TopIncreament = 0    
Set @PageDescription = upper(@PageDescription)
if @KeyWord = 'LetterContinuance'    
 Begin    
  Set @TopIncreament = (Select Count(TicketID_PK) from TblTicketsViolations Where TicketID_PK = @TicketID)    
  if @TopIncreament > 0    
   Begin    
    Set @TopIncreament = (@TopIncreament - 1) * 12    
   End    
 End    
    
if @SignLocation = 0    
 Begin    
  Select Top 1 Pnl_Left, Pnl_Top + @TopIncreament As Pnl_Top, ImageTrace, SelectionOrder from Tbl_HTS_ESignature_Location where Entity = @Entity  And KeyWord = @KeyWord And PageDescription = @PageDescription order by SelectionOrder
 End    
if @SignLocation = 1    
 Begin    
  Select Top 1 Pnl_Left, Pnl_Top + @TopIncreament As Pnl_Top, ImageTrace, SelectionOrder from Tbl_HTS_ESignature_Location where SelectionOrder > (Select Top 1 SelectionOrder from Tbl_HTS_ESignature_Location where imagetrace=@ImageTrace) and KeyWord = @KeyWord And Entity 
= @Entity And PageDescription = @PageDescription order by SelectionOrder  
 End    
if @SignLocation = 2    
 Begin    
  Select Top 1 Pnl_Left, Pnl_Top + @TopIncreament As Pnl_Top, ImageTrace, SelectionOrder from Tbl_HTS_ESignature_Location where SelectionOrder < (Select Top 1 SelectionOrder from Tbl_HTS_ESignature_Location where imagetrace=@ImageTrace) and KeyWord = @KeyWord And Entity 
= @Entity And PageDescription = @PageDescription order by SelectionOrder Desc  
 End    
if @SignLocation = 3
 Begin    
	Select Top 1 Pnl_Left, Pnl_Top + @TopIncreament As Pnl_Top, ImageTrace, SelectionOrder from Tbl_HTS_ESignature_Location where Entity = @Entity  And KeyWord = @KeyWord And PageDescription = @PageDescription order by SelectionOrder Desc
 End    
if @SignLocation = 4
 Begin    
  Select Top 1 Pnl_Left, Pnl_Top + @TopIncreament As Pnl_Top, ImageTrace, SelectionOrder from Tbl_HTS_ESignature_Location where ImageTrace=@ImageTrace And KeyWord = @KeyWord 
 End    
END    
    
    
    
  
  








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_InsertInBatch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_InsertInBatch]
GO


CREATE procedure [dbo].[usp_WebScan_InsertInBatch]    
    
    
@ScanType as varchar(50),    
@ScanDate as datetime,  
@EmpID as int,
@BatchID int=0 output 
    
as

insert into tbl_WebScan_Batch    
(     
     
 ScanType,    
 ScanDate ,
 EmpID	   
)    
values    
(    
     
 @ScanType,    
 @ScanDate    ,
 @EmpID
) 

set @BatchID=Scope_identity()





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CASESTATUS_REPORT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CASESTATUS_REPORT]
GO

CREATE PROCEDURE [dbo].[USP_HTS_GET_CASESTATUS_REPORT] 
@fromDate datetime,  
@toDate Datetime  
as  
  
set @toDate = @toDate + '23:59:59'  
  
SELECT      l.TicketID,  
   MAX(l.LogDate) AS LogDate,  
   l.LastName,  
   MAX((CASE WHEN DateDiff(day, CONVERT(datetime, '1/1/1900'), l.DOB) = 0 THEN '' ELSE CONVERT(varchar, l.DOB, 101) END)) AS DOB,  
   dbo.fn_FormatContactNumber(MAX(l.PhoneNumber),isnull(t.ContactType1,1)) AS PhoneNumber,  
   l.Email,  
   l.DLNumber  
FROM        dbo.tbl_CheckCaseStatus_Log l
			inner join tbltickets t on l.TicketID =t.TicketID_pk
GROUP BY TicketID,  
   l.LastName,  
   l.Email,  
   l.DLNumber,
   t.ContactType1  
  
HAVING      (MAX(LogDate) between @fromDate and @toDate)  
  
ORDER BY MAX(LogDate) DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


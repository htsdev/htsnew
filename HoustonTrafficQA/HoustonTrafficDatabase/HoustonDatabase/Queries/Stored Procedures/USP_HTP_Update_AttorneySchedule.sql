﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure update attorney schedule by given parameter.
			
List of Parameters:	
			@AttorneyScheduleID attorney schedule identification
			@DocketDate			trial date
			@AttorneyID:		attorney identification
			@ContactNumber		attorney contact number
			@EmployeeID			Employee identification
					
******/

Create PROCEDURE dbo.USP_HTP_Update_AttorneySchedule
	@AttorneyScheduleID INT,
	@DocketDate VARCHAR(10),
	@AttorneyID INT,
	@ContactNumber VARCHAR(14),
	@EmployeeID INT
AS
BEGIN
    UPDATE AttorneysSchedule
    SET    ContactNumber = @ContactNumber,
           AttorneyID = @AttorneyID,
           UpdatedByEmployeeID = @EmployeeID,
           LastUpdatedDate = GETDATE()
    WHERE  AttorneyScheduleID = @AttorneyScheduleID
END 

GO 
   
   GRANT EXECUTE ON dbo.USP_HTP_Update_AttorneySchedule TO dbr_webuser
     
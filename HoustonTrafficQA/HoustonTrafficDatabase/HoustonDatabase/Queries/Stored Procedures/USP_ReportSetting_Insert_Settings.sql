/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:33:52 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used insert a new entry in report setting table.
*					: 
* Parameter List	
* @CourtID			: Court Id
* @AttributeId		: Attribute Id
  @AttributeValue	: No of days which can be either business days, followup date days or default followup date days ect, based on attribute id of the record.
  @InsertBy			: User id who is performing insertion of the record.
  @LastUpdateBy		: User id who is performing modification of the record.  
* 
* USP_ReportSetting_Insert_Settings 
*/
CREATE PROC [dbo].[USP_ReportSetting_Insert_Settings]

@ReportID INT,
	@CourtID INT,
	@AttributeId INT,
	@AttributeValue VARCHAR(500),	
	@InsertBy INT,	
	@LastUpdateBy INT
AS



INSERT INTO ReportSetting
  (
    ReportID,
    CourtID,
    AttributeId,
    AttributeValue,
    InsertDate,
    InsertBy,
    LastUpdateDate,
    LastUpdateBy,
    IsActive
  )
VALUES
  (
    @ReportID,
    @CourtID,
    @AttributeId,
    @AttributeValue,
    GETDATE(),
    @InsertBy,
    GETDATE(),
    @LastUpdateBy,
    1
  )


GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_Insert_Settings] TO dbr_webuser
GO
/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This method returns all attorney paid letters against the selected prindate and attorney
* Parameter : 
*			@startdate
			@enddate
			@attorney
			
  Column return : 
				sno
				empname
				FirmName,
				PrintDatetime,
				Filepath
*/
CREATE PROCEDURE [dbo].[USP_HTP_GetAttorneyPaidHistory]
	@startdate DATETIME,
	@enddate DATETIME,
	@attorney INT
AS
	SELECT ROW_NUMBER() OVER(ORDER BY aph.ID) AS sno,
	       tu.Firstname + ' ' + tu.Lastname AS empname,
	       tf.FirmName,
	       aph.PrintDatetime,
	       aph.Filepath
	FROM   AttorneyPayoutHistory aph
	       INNER JOIN tblUsers tu
	            ON  tu.EmployeeID = aph.EmpID
	       INNER JOIN tblFirm tf
	            ON  tf.FirmID = aph.firmid
	WHERE  DATEDIFF(DAY, @startdate, aph.PrintDatetime) >= 0
	       AND DATEDIFF(DAY, aph.PrintDatetime, @enddate) >= 0
	       AND (
	               @attorney = -1
	               OR (@attorney <> -1 AND @attorney = aph.FirmID)
	           )
GO

	       GRANT EXECUTE ON [dbo].[USP_HTP_GetAttorneyPaidHistory] TO 
dbr_webuser
	       
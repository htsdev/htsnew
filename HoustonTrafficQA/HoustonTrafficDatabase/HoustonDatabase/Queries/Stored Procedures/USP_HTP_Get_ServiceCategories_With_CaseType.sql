﻿-- Noufil 4632 09/17/2008
/*****
	Business Logic :  this procedure returns the servie ticket categories with the care type associated with it in comma seperated value	
	Column Return:
				SNo : Serial number
				ID : ID of Service Category
				Description : Name of Service Category
				casetypename : Comma seperated case type name
				assignto: Name of employee haing this service Category Ticket.
*****/

ALTER Procedure [dbo].[USP_HTP_Get_ServiceCategories_With_CaseType]
as

declare @tbl table         
(
ID int,        
Description varchar(50),        
casetypename varchar(200),
assignto INT --Fahad 6054 07/22/2009 New Parameter assignto Added
)
declare @tblout table         
(
	SNo int identity,	
	ID int,        
	Description varchar(50),
	casetypename varchar(200),
	EmpName VARCHAR(50),
	AssignTo INT
	 --Fahad 6054 07/22/2009 New Parameter assignto Added       
)           
        
insert into @tbl        
	Select Distinct stc.Id,stc.Description,ct.casetypename,stc.AssignedTo--Fahad 6054 07/22/2009 New Parameter assignto Added
	  from CaseTypeServiceCategory ctsc 
		inner join tblServiceTicketCategories stc on stc.ID = ctsc.ServiceCategoryId    
		inner join CaseType ct on ctsc.CaseTypeId = ct.CaseTypeId

order by stc.Description

declare @maxid int
declare @i int
declare @category varchar(max)
set @category=''
declare @desc varchar(max)
DECLARE @Assignto INT--Fahad 6054 07/22/2009 New Parameter assignto Added


set @i=0

set @maxid= (select max(id) from @tbl)
print @maxid

while @i<=@maxid
begin
	set @category =''
	SELECT @Assignto=assignto, @category= case @category when '' then '' else  @category +','end +isnull(casetypename,'')
	  from @tbl where id=@i

	--Sabir Khan 6670 10/02/2009 User Name has been changed into First Name and Last Name...
	insert into @tblout select top 1 id, DESCRIPTION,@category,(SELECT tu.Firstname + ' ' + tu.Lastname FROM tblUsers tu WHERE tu.EmployeeID=assignto),assignto--Fahad 6054 07/22/2009 New Parameter assignto Added
	                      from @tbl where id=@i	
	set @i = @i+1
end

select * from @tblout order by sno


﻿/***********

--Zeeshan 4656 08/25/2008 
Created By : Zeeshan Ahmed

Business Logic : This process lock or release the userid & password to access the JIMS website


List Of Parameters 

@Empid : Employee Id
@lockFlag : Lock Flag


***********/


Create proc dbo.USP_HTP_Set_User_LockFlag 
(

	@Empid as int,
	@lockFlag as bit

)
as

update tblusers set islock = @lockFlag where employeeid = @Empid



Select @@rowcount

Go

grant execute on dbo.USP_HTP_Set_User_LockFlag  to dbr_webuser


Go
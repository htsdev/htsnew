SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_NOMAILFLAG_ChkByLetterID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_NOMAILFLAG_ChkByLetterID]
GO


CREATE PROCEDURE [dbo].[USP_HTS_NOMAILFLAG_ChkByLetterID]  
@LetterID int      
as      
select  ta.FirstName, ta.LastName, ta.Address1, ta.City, st.State, case when len(ta.ZipCode)>5 then substring(ta.ZipCode,1,5) else ta.ZipCode end as ZipCode from tblticketsarchive ta
join
tblstate st 
on ta.StateID_FK = st.StateID
where recordid = @LetterID  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_UpdateMain]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_UpdateMain]
GO


--usp_webscan_UpdateMain  171,'2006TR0287649','JURY TRIAL','LUBBOCK',6,'5/5/2006','5/5/2006 11:00'
CREATE procedure [dbo].[usp_webscan_UpdateMain]  

@PicID int,  
@CauseNo varchar(50),      
@Status varchar(50),      
@Location varchar(50) ,    
@CourtNo varchar(3),  
@ScanUpdateDate datetime,  
@CourtDateMain datetime  
   
  
  
as  
  
  
declare @TempCID as varchar(3)  
declare @CourtNumScan as varchar(50)  
  
set @TempCID=@CourtNo  
  
if @TempCID='13' or @TempCID='14'  
 begin  
  set @CourtNumScan='3002'  
 end  
else if @TempCID='18'  
 begin  
  set @CourtNumScan='3003'  
 end  
 -- Abbas Qamar 10088 05-12-2012 Adding Court location for HMC-W
 else if @TempCID='20'  
 begin  
  set @CourtNumScan='3075'  
 end  

else  
 begin  
  set @CourtNumScan='3001'  
 end  
  
declare @tempCVID int  
Select @tempCVID=courtviolationstatusid from tblcourtviolationstatus where description=@Status  
  
  
update tblticketsviolations  
  
set  
  
CourtDateMain=@CourtDateMain,  
CourtNumberMain=@CourtNo,  
CourtViolationStatusIDmain=@tempCVID,  
VerifiedStatusUpdateDate=@ScanUpdateDate,  
CourtID=@CourtNumScan,  
vemployeeid=3991  
   
  
where   
 casenumassignedbycourt=@CauseNo   
 and  CourtID in (3001,3002,3003,3075)  

update tbl_webscan_ocr
	set 
	adminverified=1
	where
	picid=@PicID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


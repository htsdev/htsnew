﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/15/2010 6:13:14 PM
* Task ID :				8422
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return :     
******/
CREATE PROCEDURE [dbo].[USP_HTP_GetPatientById]
	@PatientId INT
AS
	SELECT 
	p.Id,
	p.TicketId,
	p.FirstName,
	p.LastName,
	p.[Address],
	p.Contact1,
	p.Contact2,
	p.SurgeryDate,
	p.DoctorContacted,
	p.ContactComment,
	p.ScheduleDate,
	p.ProblemComments,
	p.PainSeverity,
	p.GeneralComments,
	p.PatientRelation,
	p.DoctorId,
	p.ManufacturerId
	FROM Patient p
	WHERE  
	p.id=@PatientId
GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetPatientById]  TO dbr_webuser
GO 	 
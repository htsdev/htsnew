﻿/****** 
Created by:  Muhammad Nasir
Task Id:		6483
Created Date:		10/06/2009
Business Logic : this procedure return payment type and if exist secondary user and bond client last payment passess 3 week 
        if not internet client then two month passes
List of Parameters:	
	@Ticketid : Ticket Id of the case

List of Columns: 	
	paymenttype : Client payment type
	Exist:		if client exist of above logic

******/


Create PROCEDURE dbo.USP_HTP_Check_BondReportPrint
	@TicketID INT
AS
BEGIN
    DECLARE @paymenttype AS INT
    DECLARE @Exist AS INT
    
    SET @paymenttype = (
            SELECT TOP 1 ttp.PaymentType
            FROM   tblTicketsPayment ttp
                   INNER JOIN tblTickets tt
                        ON  ttp.TicketID = tt.TicketID_PK
            WHERE  ttp.TicketID = @TicketID
                   AND tt.BondFlag = 1
            ORDER BY
                   ttp.RecDate DESC
        )
    
  --  SELECT @paymenttype
    
    IF (@paymenttype IS NOT NULL)
    BEGIN
        IF (@paymenttype = 7)
        BEGIN
            SELECT @Exist = CASE 
                                 WHEN DATEDIFF(
                                          DAY,
                                          (
                                              SELECT TOP 1 MAX(ttp.RecDate)
                                              FROM   tblTicketsPayment ttp
                                              WHERE  ttp.TicketID = @TicketID
                                          ),
                                          GETDATE()
                                      ) <= 60 THEN 0
                                 ELSE 1
                            END
        END
        ELSE
        BEGIN
            SELECT @Exist = CASE 
                                 WHEN DATEDIFF(
                                          DAY,
                                          (
                                              SELECT TOP 1 MAX(ttp.RecDate)
                                              FROM   tblTicketsPayment ttp
                                              WHERE  ttp.TicketID = @TicketID
                                          ),
                                          GETDATE()
                                      ) <= 21 THEN 0
                                 ELSE 1
                            END
        END
        
        SELECT @paymenttype AS paymenttype,
               @Exist AS exist
    END
    ELSE
        SELECT 0 paymenttype,
               0 exist
END


GO
GRANT EXECUTE ON dbo.USP_HTP_Check_BondReportPrint TO dbr_webuser
GO
﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/05/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to update doctor/hospital information.
* List of Parameter : @PatientID 
* Column Return :     
******/
-- USP_HTP_GetDoctorHospitalInfo 29
alter PROC  dbo.USP_HTP_UpdateDoctorHospitalInfo 
@PatientID INT,
@Doctor1LastName VARCHAR(100),
@Doctor1FirstName VARCHAR(100),
@Doctor1Address VARCHAR(200),
@Doctor1City VARCHAR(50),
@Doctor1State INT,
@Doctor1Zip VARCHAR(10),
@doctor2Lastname VARCHAR(100),
@doctor2FirstName VARCHAR(100),
@doctor2Address VARCHAR(200),
@doctor2City VARCHAR(50),
@doctor2State INT,
@doctor2Zip VARCHAR(10),
@Hosptial1Name VARCHAR(100),
@Hosptial1City VARCHAR(50),
@Hosptial1State INT,
@Hosptial1Zipcode VARCHAR(10),
@Hosptial1Address VARCHAR(200),
@Hosptial12Name VARCHAR(100),
@Hosptial12City VARCHAR(50),
@Hosptial12State INT,
@Hosptial12Zipcode VARCHAR(10),
@Hosptial12Address VARCHAR(200)


as

DECLARE @doctor1Id INT,@doctor2id INT,@hospital1Id INT,@hospital2Id INT
SET @doctor1Id=0
SELECT @doctor1Id=ISNULL(p.DoctorId,0),@doctor2id=ISNULL(p.Doctorid2,0),@hospital1Id=ISNULL (p.HostpitalId1,0), @hospital2Id=ISNULL (p.HospitalId2,0) FROM Patient p WHERE p.Id=@PatientID

IF @doctor1Id<>0 
	BEGIN
		-- update doctors details
		EXEC USP_HTP_UpdateDoctorDetail @doctor1Id,@Doctor1LastName,@Doctor1FirstName,@Doctor1Address,@Doctor1City,@Doctor1State,@Doctor1Zip		
	END
ELSE 
	BEGIN	
		-- add doctor details
		EXEC USP_HTP_AddDoctorDetail @Doctor1LastName,@Doctor1FirstName,@Doctor1Address,@Doctor1City,@Doctor1State,@Doctor1Zip,@doctor1Id OUTPUT		
		UPDATE patient SET DoctorId =  @doctor1Id WHERE Id=@PatientID
	END

IF @doctor2Id<>0 
	BEGIN
		-- update doctors details
		EXEC USP_HTP_UpdateDoctorDetail @doctor2Id,@doctor2Lastname,@doctor2FirstName,@doctor2Address,@doctor2City ,@doctor2State ,@doctor2Zip 		
	END
ELSE	
	BEGIN
		-- add doctor details
		EXEC USP_HTP_AddDoctorDetail @doctor2Lastname,@doctor2FirstName,@doctor2Address,@doctor2City ,@doctor2State ,@doctor2Zip,@doctor2Id OUTPUT
		UPDATE patient SET Doctorid2 = @doctor2Id WHERE Id=@PatientID
	END
	
IF @hospital1Id<>0 
	BEGIN
		EXEC USP_HTP_UpdateHospitalDetail @hospital1Id,@Hosptial1Name,@Hosptial1City,@Hosptial1State,@Hosptial1Zipcode,@Hosptial1Address			
		
	END
ELSE 
	BEGIN
		EXEC USP_HTP_AddHospitalDetail @Hosptial1Name,@Hosptial1City,@Hosptial1State,@Hosptial1Zipcode,@Hosptial1Address,@hospital1Id OUTPUT
		UPDATE patient SET HostpitalId1 = @hospital1Id WHERE Id=@PatientID
		
	END

IF @hospital2Id<>0 
	BEGIN
		EXEC USP_HTP_UpdateHospitalDetail @hospital2Id,@Hosptial12Name,@Hosptial12City,@Hosptial12State,@Hosptial12Zipcode,@Hosptial12Address 			
	END
ELSE 
	BEGIN
		EXEC USP_HTP_AddHospitalDetail @Hosptial12Name,@Hosptial12City,@Hosptial12State,@Hosptial12Zipcode,@Hosptial12Address, @hospital2Id OUTPUT
		UPDATE patient SET HospitalId2 = @hospital2Id WHERE Id=@PatientID
		
	END

GO
GRANT EXECUTE ON dbo.USP_HTP_UpdateDoctorHospitalInfo  TO dbr_webuser
GO



GO
/****** Object:  StoredProcedure [dbo].[Usp_Hts_getinfobyticketid]    Script Date: 04/30/2008 14:06:57 ******/

/*  
Created By : Noufil Khan  
Business Logic : This procedure use to get information about perticular client by using Ticketid parameter
 parameter= @ticketid 
*/

CREATE procedure [dbo].[Usp_Hts_getinfobyticketid] 
@ticketid int
as
select lastname+' '+firstname as [name],Address1,Address2,city,tblstate.state,substring (zip,0,6) as zip5,substring (zip,7,13) as zip5 from tbltickets
inner join tblstate
	on tblstate.stateId=tbltickets.stateid_fk
where TicketId_pk=@ticketid

go

GRANT EXEC ON [dbo].[Usp_Hts_getinfobyticketid]  to dbr_webuser
go 

/*    
Created By: Haris Ahmed    
Date:   08/30/2012    
    
Business Logic:    
 The stored procedure is used to get Immigration Comment Status    
*/

CREATE PROCEDURE [dbo].[USP_HTS_Get_ImmigrationCommentStatus]
AS
	SELECT * FROM tbl_HTS_ImmigrationCommentsStatus thics	
GO
GRANT EXECUTE ON USP_HTS_Get_ImmigrationCommentStatus TO dbr_webuser;
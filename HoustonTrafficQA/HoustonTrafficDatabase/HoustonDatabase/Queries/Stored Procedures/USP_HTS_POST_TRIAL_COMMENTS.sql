SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_POST_TRIAL_COMMENTS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_POST_TRIAL_COMMENTS]
GO


create PROCEDURE USP_HTS_POST_TRIAL_COMMENTS            
@TicketID int,            
@Date datetime,          
@EmpID int,      
@Comments varchar(8000)            
as            
      
declare @empname varchar(5)      
set @empname = (select Upper(Abbreviation) from tblusers where employeeID = @EmpID)      
      
SET @Comments = @Comments + ' ( '+@empname+ ' - ' +  dbo.formatdateandtimeintoshortdateandtime(getdate()) +')'        
UPDATE tbl_hts_docketcloseout_snapshot            
set            
 Comments = substring(@Comments, 1 ,8000)    
where            
 TicketID_PK = @TicketID and          
 DateDiff(day,CourtDate,@Date)=0    
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


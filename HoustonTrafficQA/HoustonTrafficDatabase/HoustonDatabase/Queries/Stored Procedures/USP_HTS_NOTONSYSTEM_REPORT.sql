USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_NOTONSYSTEM_REPORT]    Script Date: 12/15/2008 23:29:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
ALTER PROCEDURE [dbo].[USP_HTS_NOTONSYSTEM_REPORT]    
@FlagID int    
as    
SELECT distinct    
 t.TicketID_PK as ticketid,    
 t.Firstname,     
 t.Lastname,     
 tv.casenumassignedbycourt AS CauseNo,     
 tv.RefCaseNumber AS TicketNo,     
 t.DLNumber,     
 t.DOB,     
 t.Midnum,     
    tv.CourtDateMain AS CourtDate,    
 cvs.ShortDescription AS Status,    
 isnull(datediff(day,tf.recdate,getdate()),0) as [Time]    
FROM         tblTickets AS t INNER JOIN    
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN    
                      tblTicketsFlag AS tf ON t.TicketID_PK = tf.TicketID_PK INNER JOIN    
                      tblCourtViolationStatus AS cvs ON tv.CourtViolationStatusIDmain = cvs.CourtViolationStatusID    
WHERE    
 tf.FlagID = @FlagID    
 and t.ActiveFlag = 1
 --Sabir Khan 5309 12/15/2008  allow Problem client(no hire)  flag on Flag Report... 
 and (@FlagID = 13 or tv.courtviolationstatusidmain <> 80)
 

 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_ViolationByCategory]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_ViolationByCategory]
GO



CREATE PROCEDURE [dbo].[USP_HTS_Get_ViolationByCategory]      
@CategoryID int      
      
AS      
      
SELECT     ViolationNumber_PK, Description      
FROM         dbo.tblViolations      
WHERE     CategoryID =@CategoryID      
--AND  violationtype in(0)  -- 0 means violations to be assigned      
AND       ViolationNumber_PK !=0 -- not select 1 record (--Choose--)    
order by   description


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


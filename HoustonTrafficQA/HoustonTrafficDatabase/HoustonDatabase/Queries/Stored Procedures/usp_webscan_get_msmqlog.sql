SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_get_msmqlog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_get_msmqlog]
GO


CREATE procedure usp_webscan_get_msmqlog   
  
@Batchid int,
@sentdate  datetime
  
as  
  
select top 5 message,
msgdate,
isnull(reset,'') as reset,
isnull(causeno,'') as causeno,
isnull(scanstatus,'')+'/'+isnull(trialstatus,'') as status,
(select count(id)
from tbl_webscan_msmqlog
where reset is not null
and batchid=@Batchid
and isnull(scanstatus,'')<>'Sending'
and isnull(trialstatus,'')<>'Sending'
and msgdate>@sentdate)as processed  
from tbl_webscan_msmqlog  
where batchid=@Batchid  
and msgdate>@sentdate
order by id desc


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


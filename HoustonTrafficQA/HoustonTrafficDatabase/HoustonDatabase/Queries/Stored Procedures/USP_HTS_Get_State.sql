
-- Faique Ali 10235 12/08/2012 avoid select All by specifying selected column..
ALTER PROCEDURE [dbo].[USP_HTS_Get_State]
AS
	SET NOCOUNT ON;
	SELECT stateID,
	       STATE
	FROM   tblstate WITH(NOLOCK)
	WHERE  stateid NOT IN (54, 55)

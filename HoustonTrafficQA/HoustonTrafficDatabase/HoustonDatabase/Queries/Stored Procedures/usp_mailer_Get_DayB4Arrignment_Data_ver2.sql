USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Get_DayB4Arrignment_Data_ver2]    Script Date: 09/05/2011 22:45:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the HMC Day Before Arraignment letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--  usp_mailer_Get_DayB4Arrignment_Data_ver2 1, 12, 1, '1/1/10', '10/27/10', 1
ALTER  Procedure [dbo].[usp_mailer_Get_DayB4Arrignment_Data_ver2]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int                                             
	)

as                                                              
                                                            
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(max)  ,
	@sqlquery2 nvarchar(max)  ,
	@sqlParam nvarchar(1000)

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL....
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

select 	@sqlquery ='', @sqlquery2 = ''

-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                                                    
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                                                      

-- MAIN QUERY....
-- LIST/COURT DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY ARRAIGNMENT (PRIMARY STATUS) CASES
-- INSERTING VALUES IN A TEMP TABLE....
set 	@sqlquery=@sqlquery+'                                          
 Select distinct '
				
if @searchtype = 0 
	set 	@sqlquery=@sqlquery+'      
	convert(datetime , convert(varchar(10),ta.listdate,101)) as mdate,   '
else
	set 	@sqlquery=@sqlquery+'                                                         
	convert(datetime , convert(varchar(10),tva.courtdate,101)) as mdate, '

set 	@sqlquery=@sqlquery+'      
 flag1 = isnull(ta.Flag1,''N'') ,                                                      
 ta.donotmailflag, ta.recordid,                                                     
 count(tva.ViolationNumber_PK) as ViolCount,                                                      
 isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount
into #temp                                                            
FROM dbo.tblCourtViolationStatus tcvs,  dbo.tblTicketsViolationsArchive TVA  ,dbo.tblTicketsArchive TA                     
where tcvs.CourtViolationStatusID = TVA.violationstatusid 
and  TVA.recordid = TA.recordid
and tcvs.CategoryID=2
--Muhammad Muneer 8465 10/27/2010 added the new functionality for the first name and the last name
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0
and isnull(ta.ncoa48flag,0) = 0

--Rab Nawaz Khan Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0
'               

if @SearchType = 0 
		set @sqlquery =@sqlquery+ ' 
		and datediff(day,  ta.listdate , @startListdate)<=0
		and datediff(day, ta.listdate, @endlistdate)>= 0 '
else if @searchtype = 1 
		set @sqlquery =@sqlquery+ ' 
		and datediff(day,  tva.courtdate , @startListdate )<=0 
		and datediff(day, tva.courtdate, @endlistdate )>=0 '

-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if(@crtid = @catnum )                                    
	set @sqlquery=@sqlquery+' and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else 
	set @sqlquery =@sqlquery +' and tva.courtlocation = @crtid' 

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------

-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                                    
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...                                                       
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
                                                      
-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and not  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
-----------------------------------------------------------------------------------------------------------------------------------------------------




                                                      
if @searchtype = 0 
	set @sqlquery =@sqlquery+ ' group by                                                       
 convert(datetime , convert(varchar(10),ta.listdate,101)) ,
 ta.Flag1,      
 ta.donotmailflag    , ta.recordid   
having 1 =1                                                
'                                  
else
	set @sqlquery =@sqlquery+ ' group by                                                       
  convert(datetime , convert(varchar(10),tva.courtdate,101)),
  ta.Flag1,      
  ta.donotmailflag     , ta.recordid  
having 1 = 1                                                
'                                  


-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNTif(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    
                                
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...      
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))                                
    and ('+ @doublevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                  

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
-- OR ARRAIGNMENT 1 LETTER PRINTED IN PAST 5 DAYS FOR THE SAME RECORD
-- tahir 5611 03/13/2009 fine tunning...
set @sqlquery =@sqlquery + '                                                  

delete from #temp 
from #temp a inner join tblletternotes V 
on a.recordid = V.recordid  
where (v.lettertype = @lettertype) 
or (v.lettertype = 9 and datediff(day, recordloaddate, getdate()) <= 5)

'   


-- GETTING THE RESULT SET IN TEMP TABLE.....
set @sqlquery=@sqlquery + ' 
Select distinct  a.ViolCount, a.Total_Fineamount, a.mdate , a.Flag1, a.donotmailflag, a.recordid 
into #temp1  from #temp a, tblticketsviolationsarchive tva where a.recordid = tva.recordid 
and tva.violationstatusid <> 80 -- Abbas 01/16/2012 Excluding disposed violation
--Rab Nawaz Khan Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0

 '                                  

-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
BEGIN
	-- NOT LIKE FILTER.......
	if(charindex('not',@violaop)<> 0 )              
		BEGIN
			set @sqlquery=@sqlquery+' 
			-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
			and	 not (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           		
		END
		-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
		-- LIKE FILTER.......
		--if(charindex('not',@violaop) =  0 )      
	ELSE
		BEGIN        
			set @sqlquery=@sqlquery+' 
			-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
			and	 (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           			
		END
END
                                  
-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery2=@sqlquery2 +'

-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624

Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 

-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...  
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS                                                      
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate

-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
-- SELECTED DATE RANGE.......
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end
'
	

-- OUTPUTTING THE REQURIED INFORMATION........         
set @sqlquery2 = @sqlquery2 +'                                                        
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3 
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
order by l.mdate

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5  drop table #letterdates
'                                                      
                                                      
--print @sqlquery  + @sqlquery2                                       

-- CONCATENATING THE QUERY ....
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType












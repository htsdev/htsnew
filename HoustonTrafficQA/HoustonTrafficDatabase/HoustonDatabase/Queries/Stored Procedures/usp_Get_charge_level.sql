SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_charge_level]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_charge_level]
GO



      
    
    
    
CREATE PROCEDURE   [dbo].[usp_Get_charge_level]     
(    
@Courtid int     
)    
as      
BEGIN      
     
-- If Harris County Criminal Court    
--if @Courtid = 3037    
--select ID,LevelCode  from tblChargelevel  where LevelCode like 'M%' or LevelCode like 'F%'    
--    
--else    
SELECT  ID,LevelCode  from tblChargelevel     order by levelcode  
    
    
END      
     


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


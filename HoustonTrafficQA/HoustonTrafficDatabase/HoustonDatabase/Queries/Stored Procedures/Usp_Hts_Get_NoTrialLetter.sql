/****** 
Altered by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get no trial letter report for report under validation section and report.
				Display All Clients. Having Future Court dates.
				Only for Traffic case type i.e. Civil, Criminal and Family Law cases are not included.
				For inside Courts display only cases that are in Jury Status.
				For outside courts Display only Cases that are in Jury Trial or in Pre-Trial Status.
				For HCJP 4-2 court displays Judge Trial & Jury Trial cases.
				If case setting information changed due to any reason that client will appear in this report. It includes:
					Court Date 
					Court Number 
					Court Location 
					Case Status 
				Trial Letter not printed after last setting information change. 
				Exclude all cases with ‘No Letter’ Flag.
				
List of Columns:	
	ticketid_pk:
	ClientName:
	ticketnumber:
	CourtDate:
	Rep:
	crtlocation:
	status:
	ncourtdate:
	
*******/   
ALTER procedure [dbo].[Usp_Hts_Get_NoTrialLetter]  
                
as                
                
 select   
   tt.ticketid_pk,                
   upper(tt.lastname + ', '+tt.firstname) as ClientName,                 
   min(ttv.refcasenumber) as ticketnumber,                
   min(ttv.courtdatemain) as CourtDate,                
   upper(u.username) as Rep ,              
   upper(tc.shortname) as crtlocation,              
   upper(cv.shortdescription) as status,   
dbo.fn_DateFormat(isnull(min(ttv.courtdatemain),Convert(datetime,'1/1/1900')),30,'/',':',1) as ncourtdate 

from tbltickets tt  
  
 inner join tblticketsviolations ttv on tt.ticketid_pk=ttv.ticketid_pk  
 inner join tblcourts tc on tc.Courtid = ttv.courtid 
 inner join tblcourtviolationstatus cv on ttv.courtviolationstatusidmain=cv.courtviolationstatusid     
 inner join tblusers u on ttv.vemployeeid = u.employeeid  
  
where 
tt.casetypeid=1 --ozair 4967 10/15/2008 only for traffic cases 
and
ttv.refcasenumber in 
				(
					select 
						  min(tv.refcasenumber)
					from tbltickets t              
					 inner join tblticketsviolations tv on t.ticketid_pk=tv.ticketid_pk              
					 inner join tblcourtviolationstatus s on  s.courtviolationstatusid = tv.courtviolationstatusidmain              
					 where  t.activeflag=1 and t.jurystatus=0 and  tv.courtdatemain > getdate()
						and
							((s.categoryid = 4 and tv.courtid in (3001,3002,3003))
							or 
							(s.categoryid  IN (3, 4) and  tv.courtid not in (3001,3002,3003,3014))
							or 
							(s.categoryid in (4,5) and  tv.courtid = 3014))
							and
							tv.ticketid_pk not in (select distinct ticketid_PK from tblticketsFLAG where FLAGid = 7)

					group by tv.ticketid_pk 
				)
and tt.ticketid_pk in 
				(
					select 
						  tv.ticketid_pk
					from tbltickets t              
					 inner join tblticketsviolations tv on t.ticketid_pk=tv.ticketid_pk              
					 inner join tblcourtviolationstatus s on  s.courtviolationstatusid = tv.courtviolationstatusidmain              
					 where  t.activeflag=1 and t.jurystatus=0 and  tv.courtdatemain > getdate() 
						and
							((s.categoryid = 4 and tv.courtid in (3001,3002,3003))
							or 
							(s.categoryid  IN (3, 4) and  tv.courtid not in (3001,3002,3003,3014))
							or 
							(s.categoryid in (4,5) and  tv.courtid = 3014))
							and
							tv.ticketid_pk not in (select distinct ticketid_PK from tblticketsFLAG where FLAGid = 7)

					 group by tv.ticketid_pk     
				)
				
group by tt.ticketid_pk,tt.lastname + ', '+tt.firstname,u.username,tc.shortname,cv.shortdescription,ttv.courtdatemain
order by ttv.courtdatemain desc, [ClientName]

                
  
  
  
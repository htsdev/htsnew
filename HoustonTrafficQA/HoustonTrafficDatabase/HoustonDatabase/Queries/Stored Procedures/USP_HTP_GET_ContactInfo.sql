﻿/************************************************************    
 * Code formatted by SoftTree SQL Assistant © v4.0.34    
 * Time: 4/14/2009 5:25:19 PM    
 ************************************************************/    
    
/*    
* Created By Waqas 5771 04/13/2009    
* Business Logic :  This procedure returns the contact information    
* Parameter : ContactID - It is used to pass contact id of the client.      
*/    
     
    
Create PROCEDURE dbo.USP_HTP_GET_ContactInfo     
 @ContactID INT          
AS          
 SELECT ISNULL(c.FirstName, '') AS FirstName,          
        ISNULL(c.MiddleName, '') AS MiddleName,          
        ISNULL(c.LastName, '') AS LastName,          
        ISNULL(CONVERT(VARCHAR(12), DOB, 101), '') AS DOB,          
        (          
            CASE           
                 WHEN c.NoDL = 0 THEN ISNULL(c.DLNumber, '')          
                 ELSE 'N/A'          
            END          
        ) AS DLNumber,          
        (          
            CASE           
                 WHEN c.NoDL = 0 THEN ISNULL(          
                          (          
                              SELECT TOP 1 ts.[State]          
                              FROM   tblState ts          
                              WHERE  ts.StateID = c.DLState          
                          ),          
                          ''          
                      )--ISNULL(ts2.[State], '')          
                 ELSE 'N/A'          
            END          
        ) AS DLState,          
        ISNULL(c.Address1, '')  AS Address1,    
        ISNULL(c.Address2, '') AS Address2,          
        ISNULL(c.City, '') AS City,          
        ISNULL(          
            (          
                SELECT TOP 1 ts.[State]          
                FROM   tblState ts          
                WHERE  ts.StateID = c.StateID_FK          
            ),          
            ''          
        ) AS [State],     
        ISNULL(c.Zip, '') AS Zip,          
        CASE           
             WHEN CDLFlag = 1 THEN 'YES'          
             WHEN CDLFlag = 0 THEN 'NO'          
             ELSE 'N/A'          
        END AS CDLFlag,          
        (          
            CASE           
                 WHEN c.EmailNotAvailable = 0 THEN ISNULL(c.Email, '')          
                 ELSE 'N/A'          
            END          
        ) AS Email,          
        ISNULL(LanguageSpeak, '') AS [LANGUAGE],          
        ISNULL(Race, '') AS Race,          
        ISNULL(Gender, '') AS gender,          
        ISNULL(Height, '') AS Height,          
        ISNULL(WEIGHT, '') AS WEIGHT,          
        ISNULL(HairColor, '') AS HairColor,          
        ISNULL(Eyes, '') AS Eyes,          
        ISNULL(Phone1, '') AS Phone1,          
        CASE           
             WHEN ISNULL(PhoneType1, 0) = 0 THEN 'N/A'          
             ELSE (          
                      SELECT tc.[Description]          
                      FROM   tblContactstype tc          
                      WHERE  tc.ContactType_PK = PhoneType1          
                  )          
        END AS PhoneType1,          
        ISNULL(Phone2, '') AS Phone2,          
        CASE           
             WHEN ISNULL(PhoneType2, 0) = 0 THEN 'N/A'          
             ELSE (          
                      SELECT tc.[Description]          
                      FROM   tblContactstype tc          
                      WHERE  tc.ContactType_PK = PhoneType2          
                  )          
        END AS PhoneType2,          
        ISNULL(Phone3, '') AS Phone3,          
        CASE           
             WHEN ISNULL(PhoneType3, 0) = 0 THEN 'N/A'          
             ELSE (          
                      SELECT tc.[Description]          
                      FROM   tblContactstype tc          
                      WHERE  tc.ContactType_PK = PhoneType3          
                  )          
        END AS PhoneType3,          
        ISNULL(MidNumber, '') AS MidNumber          
 FROM   Contact c          
                  
 WHERE  c.ContactID = @ContactID 
 
 
 go

grant execute on [dbo].[USP_HTP_GET_ContactInfo] to dbr_webuser

go

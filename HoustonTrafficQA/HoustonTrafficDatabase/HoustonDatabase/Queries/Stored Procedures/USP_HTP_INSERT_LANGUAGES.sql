/*
* Business Logic : This procedure insert language that not exist in our system.
* Parameter : 
*				@languagename : Name of language that will be inserted.
*/

CREATE PROCEDURE [dbo].[USP_HTP_INSERT_LANGUAGES] 
	@languagename VARCHAR(200)
AS

IF NOT EXISTS (SELECT * FROM [Language] l WHERE  RTRIM(LTRIM(l.LanguageName)) = RTRIM(LTRIM(@languagename)))
BEGIN
	
	INSERT INTO [Language]
	(		
		LanguageName
	)
	VALUES
	(		
		RTRIM(LTRIM(@languagename))
	)	
	
END	
	
	SELECT @@ROWCOUNT
GO

GRANT EXECUTE ON [dbo].[USP_HTP_INSERT_LANGUAGES] TO dbr_webuser
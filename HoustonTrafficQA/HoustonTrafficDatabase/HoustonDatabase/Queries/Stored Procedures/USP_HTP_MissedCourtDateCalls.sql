﻿/****** 
Create by		: Fahad Muhammad Qureshi
Created Date	: 12/05/2008
TasK			: 5302

Business Logic : 
			This procedure simply return the records having contact status "Pending" since Dec' 01 2008.
			
List of Parameters:	
			None
			
Column Return : 
			TicketID
			Firstname
			Lastname			
			TicketNumber
			CourtDate
			CourtTime
			CourtRoom
			CourtLocation
			ContactStatus						
******/
Alter Procedure [dbo].[USP_HTP_MissedCourtDateCalls]

as

SELECT DISTINCT   
	t.TicketID_PK,    
	isnull(tv.CaseNumAssignedByCourt,'') as CauseNumber,
	isnull(tv.refCaseNumber,'') as TicketNo,
	t.Lastname,   
	t.Firstname,   
	cvs.shortdescription as CourtStatus, 
	isnull(tv.FTACallStatus,0) as status,  
	--Sabir Khan Court date has been changed from auto court date to verified court date... 
	dbo.fn_DateFormat(tv.CourtDateMain,1,'/',':',1) as courtdate,
	dbo.fn_DateFormat(tv.CourtDateMain,26,'/',':',1)as courttime,   	
	isnull(tv.CourtNumbermain,'0') AS CourtRoom,
	c.shortname as courtloc,
	rs.Description as contactstatus      

FROM  tblTicketsViolations TV 
INNER JOIN dbo.tblTickets t 
	ON  TV.TicketID_PK = t.TicketID_PK                          
inner join dbo.tblCourtViolationStatus cvs 
	ON	TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
INNER JOIN dbo.tblCourts c 
	ON  TV.CourtID = c.Courtid             
INNER JOIN dbo.tblticketspayment TP 
	ON TV.TicketID_PK =TP.TicketID
left OUTER JOIN  dbo.tblTicketsFlag TF
	ON T.TicketID_pk = TF.TicketID_PK and TF.FlagID not in (5)     
left outer join tblReminderStatus rs 
	on rs.Reminderid_pk = isnull(tv.FTACallStatus,0)
WHERE isnull(t.firmid,3000) = 3000  
and t.CaseTypeId=1 
AND DATEDIFF([day], TV.CourtDatemain, '11/30/2008')<0  
--ozair 5302 12/13/2008 duplication of records removed
--Sabir Khan 5333 12/19/2008 verified status should be only DLQ, FTA or MISSED COURT ...
AND (cvs.courtviolationstatusid in (146,186,105)) 
and isnull(t.BondFlag,0) <> 1
and isnull(tv.FTACallStatus,0) = 0
order by t.lastname, t.firstname 
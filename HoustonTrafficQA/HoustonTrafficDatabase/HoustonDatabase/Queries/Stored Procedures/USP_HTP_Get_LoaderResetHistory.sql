
-- USP_HTP_Get_LoaderResetHistory '4/13/09', '4/13/09', 1
alter procedure [dbo].[USP_HTP_Get_LoaderResetHistory]
	@sdate datetime = null,
	@edate datetime = null,
	@LoaderId int = 6

as

--declare @sdate datetime, @edate datetime
--select @sdate = '4/3/09', set @edate = '4/3/09'

declare @isDefault bit

if @sdate  is null  or @edate is null
	set @isDefault = 1
else 
	set @isDefault = 0

select  t.midnum as [X-Ref], 
		'<a href="../ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&casenumber=' + convert(Varchar,t.ticketid_pk) + '" target="_blank">'+ v.refcasenumber + '</a>' as [Ticket Number],
		v.casenumassignedbycourt [Cause Number],
		c1.shortname as [Prev Court],
		vstatus.shortdescription as [Prev Status],
		a.courtdatemain as [Prev Crt Date],
		a.courtnumbermain as [Prev Room],
		c2.shortname as [New Court],
		--CASE @LoaderId when 8 then 'DISP' Else astatus.shortdescription end as [New Status],
		astatus.shortdescription as [New Status],
		a.courtdate as [New Crt Date],
		a.courtnumber as [New Room]
FROM         
		dbo.tblTickets AS t INNER JOIN
		dbo.tblTicketsViolations AS v ON v.TicketID_PK = t.TicketID_PK INNER JOIN
		dbo.LoaderResetHistory AS a ON a.TicketsViolationID = v.TicketsViolationID INNER JOIN
		dbo.tblCourts AS c1 ON 
		c1.Courtid = case when a.courtid in (3001,3002,3003) then 
							CASE a.courtnumbermain WHEN '13' THEN 3002 WHEN '14' THEN 3002 WHEN '18' THEN 3003 ELSE 3001 END 
						  else a.courtid end INNER JOIN
		dbo.tblCourts AS c2 ON 
		c2.Courtid = case when a.courtid in (3001,3002,3003) then 
							CASE a.courtnumber WHEN '13' THEN 3002 WHEN '14' THEN 3002 WHEN '18' THEN 3003 ELSE 3001 END 
						  else a.courtid end INNER JOIN
		dbo.tblCourtViolationStatus AS astatus ON a.CourtViolationStatusID = astatus.CourtViolationStatusID INNER JOIN
		dbo.tblCourtViolationStatus AS vstatus ON a.CourtViolationStatusIDmain = vstatus.CourtViolationStatusID

where	(
			(@isDefault = 1)
		OR
			(
			datediff(day, a.loaderupdatedate, @sdate) <=0 and
			datediff(day, a.loaderupdatedate, @edate) >=0
			)
		)
and a.loaderid = @loaderid
order by [New Crt Date], [X-Ref], [Ticket Number]


go

grant execute on [dbo].[USP_HTP_Get_LoaderResetHistory] to dbr_Webuser
go
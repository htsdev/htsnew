/****** 
Created by:		Noufil Khan
Business Logic:	The procedure is used by LMS application to get data for ForthWorth Warrant letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@BondFlag:		Flag that identifies if searching for warrant letters or for appearance.
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	Abb:			Short abbreviation of employee who is printing the letter
	Language:		Flag if need to print single sided (English) or double sided (both English & Spanish) letters.
	ChkGroup:		Recordid plus language flag for grouping of records on the report file.

*******/

--  usp_Mailer_Send_ForthWorth_DLQ 27, 7, 27, '8/22/2010,' , 3991, 0, 0, 0
Alter PROCEDURE [dbo].[usp_Mailer_Send_ForthWorth_DLQ] 
(
    @catnum         INT = 27,
    @bondFlag       INT = 0,
    @courtid        INT = 27,
    @startListdate  VARCHAR(500) = '03/24/2006,',
    @empid          INT = 3991,
    @printtype      INT = 0,
    @language       INT = 1,	-- by default print both English & Spanish letters   ,      
    @isprinted      BIT
)
AS
	-- DECLARING LOCAL VARIABLES FOR FILTERS...                                              
	DECLARE @officernum VARCHAR(50),
	        @officeropr VARCHAR(50),
	        @tikcetnumberopr VARCHAR(50),
	        @ticket_no VARCHAR(50),
	        @zipcode VARCHAR(50),
	        @zipcodeopr VARCHAR(50),
	        @fineamount MONEY,
	        @fineamountOpr VARCHAR(50),
	        @fineamountRelop VARCHAR(50),
	        @singleviolation MONEY,
	        @singlevoilationOpr VARCHAR(50),
	        @singleviolationrelop VARCHAR(50),
	        @doubleviolation MONEY,
	        @doublevoilationOpr VARCHAR(50),
	        @doubleviolationrelop VARCHAR(50),
	        @count_Letters INT,
	        @violadesc VARCHAR(500),
	        @violaop VARCHAR(50),
	        @LetterType INT,
	        @lettername VARCHAR(50) 
	
	-- SETTING LETTER TYPE AND LETTER NAME..        
	SET @lettertype = 86
	SET @lettername = 'Fort Worth DLQ' 
	
	
	--SETTING FLAG IF NEED TO SEND THE LETTER TO BARRY...
	-- SEND COPY OF LETTER TO BARRY ONCE IN A MONTH...
	DECLARE @mailbarry BIT  
	SET @mailbarry = 1
	
	-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
	DECLARE @sqlquery VARCHAR(MAX)
	SELECT @sqlquery = '' 
	
	-- ASSIGNING VALUES TO THE VARIABLES
	-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
	-- FOR THIS LETTER TYPE....                                                  
	SELECT @officernum = officernum,
	       @officeropr = oficernumOpr,
	       @tikcetnumberopr = tikcetnumberopr,
	       @ticket_no = ticket_no,
	       @zipcode = zipcode,
	       @zipcodeopr = zipcodeLikeopr,
	       @singleviolation = singleviolation,
	       @singlevoilationOpr = singlevoilationOpr,
	       @singleviolationrelop = singleviolationrelop,
	       @doubleviolation = doubleviolation,
	       @doublevoilationOpr = doubleviolationOpr,
	       @doubleviolationrelop = doubleviolationrelop,
	       @violadesc = violationdescription,
	       @violaop = violationdescriptionOpr,
	       @fineamount = fineamount,
	       @fineamountOpr = fineamountOpr,
	       @fineamountRelop = fineamountRelop
	FROM   tblmailer_letters_to_sendfilters
	WHERE  courtcategorynum = @catnum
	       AND lettertype = @lettertype 
	
	
	-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN
	-- EMPLOYEE IN THE LOCAL VARIABLE....      
	DECLARE @user VARCHAR(10)      
	SELECT @user = UPPER(abbreviation)
	FROM   tblusers
	WHERE  employeeid = @empid 
	
	-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
	SET @sqlquery = @sqlquery + 
	    '  
			declare @recCount int                                        
			Select distinct tva.recordid,
							mdate =convert(datetime,convert(varchar(10),tva.DLQUpdateDate,101)), 
							count(tva.ViolationNumber_PK) as ViolCount,      
							isnull(sum(isnull(tva.bondamount,0)),0) as Total_Fineamount       
			into #temptable                                               
			FROM dallastraffictickets.dbo.tblTicketsViolationsArchive tva       
			inner join dallastraffictickets.dbo.tblTicketsArchive ta on  tva.recordid = ta.recordid       
			inner join dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid     
			INNER JOIN tblstate S ON ta.stateid_fk = S.stateID     

			-- PERSON NAME SHOULD NOT BE EMPTY..
			where ta.lastname is not null  and ta.firstname is not null

			-- EXCLUDE V TICKETS....
			and charindex(''v'',tva.ticketnumber_pk)<>1

			-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
			and isnull(ta.ncoa48flag,0) = 0

			-- ONLY VERIFIED AND VALID ADDRESSES
			and Flag1 in (''Y'',''D'',''S'')  

			-- NOT MARKED AS STOP MAILING...
			and isnull(donotmailflag,0)  =0
			
			-- Get letters with Court violation status APPEARANCE
			and tva.violationstatusid = 116
			
			-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
			AND (SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''O'' AND SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''M'')
			' 
	
	-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED CATEGORY.....
	IF (@courtid = @CATNUM)
	    SET @sqlquery = @sqlquery + 
	        ' and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum  = '+ CONVERT(VARCHAR, @CATNUM) + ' )' 
	        
	        -- PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
	ELSE
	    SET @sqlquery = @sqlquery + ' and tva.courtlocation=' + CONVERT(VARCHAR, @courtid) 
	
	
	-- ONLY FOR THE SELECTED DATE RANGE.....
	IF (@startListdate <> '')
	    SET @sqlquery = @sqlquery + '
	    and ' + dbo.Get_String_Concat_With_DatePart_ver2('' + @startListdate + '', 'tva.DLQUpdateDate') 
	
	
	-- LMS FILTERS SECTION:
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
	IF (@officernum <> '')
	    SET @sqlquery = @sqlquery + '
	    and  ta.officerNumber_Fk ' + @officeropr + '(' + @officernum + ')' 	
	
	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
	IF (@ticket_no <> '')
	    SET @sqlquery = @sqlquery + '
	    and (' + dbo.Get_String_Concat_With_op('' + @ticket_no + '','tva.TicketNumber_PK',+ '' + @tikcetnumberopr) + ')' 
	    
	-- ZIP CODE FILTER......
	-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
	IF (@zipcode <> '')
	    SET @sqlquery = @sqlquery + '
	    and (' + dbo.Get_String_Concat_With_op('' + @zipcode + '',
	            'left(ta.zipcode,2)',+ '' + @zipcodeopr) + ')' 
	
	
	-- FINE AMOUNT FILTER.....
	-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
	IF (@fineamount <> 0 AND @fineamountOpr <> 'not')
	    SET @sqlquery = @sqlquery + '
	    and  tva.bondamount' + CONVERT(VARCHAR(10), @fineamountRelop) + 'Convert(Money,' + CONVERT(VARCHAR(10), @fineamount) + ')' 	
	
	-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT..... 
	IF (@fineamount <> 0 AND @fineamountOpr = 'not')
	    SET @sqlquery = @sqlquery + '
	    and  not tva.bondamount' + CONVERT(VARCHAR(10), @fineamountRelop) + 'Convert(Money,' + CONVERT(VARCHAR(10), @fineamount) + ')' 
	
	-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
	-- OR APP OR APP DAY OF LETTER PRINTED IN PAST 3 DAYS FOR THE SAME RECORD.. 
	SET @sqlquery = @sqlquery + '
	group by  tva.recordid,  convert(datetime,convert(varchar(10),tva.DLQUpdateDate,101)) having  1=1' 
	
	-- SINGLE VIOLATION FINE AMOUNT FILTER....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
	IF (@singleviolation <> 0 AND @doubleviolation = 0)
	    SET @sqlquery = @sqlquery + '
			and (' + @singlevoilationOpr + ' (isnull(sum(isnull(tva.bondamount,0)),0) ' + CONVERT(VARCHAR(10), @singleviolationrelop)
	        + 'Convert(Money,' + CONVERT(VARCHAR(10), @singleviolation) + ') and count(tva.ViolationNumber_PK)=1))'
	
	-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF (@doubleviolation <> 0 AND @singleviolation = 0)
	    SET @sqlquery = @sqlquery + '
			and (' + @doublevoilationOpr +'(isnull(sum(isnull(tva.bondamount,0)),0) ' + CONVERT(VARCHAR(10), @doubleviolationrelop) 
	        + 'convert (Money,' + CONVERT(VARCHAR(10), @doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )' 	
	
	-- SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF (@doubleviolation <> 0 AND @singleviolation <> 0)
	    SET @sqlquery = @sqlquery + '
	    and (' + @singlevoilationOpr + '  (isnull(sum(isnull(tva.bondamount,0)),0) '+ CONVERT(VARCHAR(10), @singleviolationrelop) + 
	    'Convert(Money,' + CONVERT(VARCHAR(10), @singleviolation) + ') and count(tva.ViolationNumber_PK)=1))
		and (' + @doublevoilationOpr + '(isnull(sum(isnull(tva.bondamount,0)),0) ' + CONVERT(VARCHAR(10), @doubleviolationrelop) 
	        + 'convert (Money,' + CONVERT(VARCHAR(10), @doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))' 
	        
	-- EXCLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
	-- OR APP PRINTED IN PAST 5 DAYS FOR THE SAME RECORD.. 		
	-- Babar Ahmad 8597 07/15/2011 Included App Day Of and Warrant Letters
	SET @sqlquery  = @sqlquery + '	delete FROM #temptable FROM #temptable a
	inner join tblletternotes n on n.recordid = a.recordid where n.lettertype = 86
					  or (n.lettertype in (63,64,76,86) and datediff(day, n.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5))<=0)'
					  
	-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION
	-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
	SET @sqlquery = @sqlquery + 
	    '
			Select distinct a.recordid,
				    a.mdate , 
					tva.courtdate, 
					ta.courtid,  
					flag1 = isnull(ta.Flag1,''N'') , ta.officerNumber_Fk,       
					tva.TicketNumber_PK, 
					isnull(ta.donotmailflag,0) as donotmailflag,      
					ta.clientflag, 
					upper( firstname) as firstname, 
					upper(lastname) as lastname, 
					upper(address1) + '''' + isnull(ta.address2,'''') as address,                            
					upper(ta.city) as city, 
					s.state,
					tc.CourtName, 
					zipcode,  
					midnumber,  
					dp2 as dptwo, 
					dpc,  
					violationdate,  
					violationstatusid,       
					left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid, 
					ViolationDescription, 
					case when isnull(tva.bondamount,0)=0 then 100 else convert(numeric(10,0),tva.bondamount) end as FineAmount, 
					a.ViolCount,      
					a.Total_Fineamount       
			into #temp                                                
			FROM dallastraffictickets.dbo.tblTicketsViolationsArchive tva       
			inner join dallastraffictickets.dbo.tblTicketsArchive ta on  tva.recordid = ta.recordid      
			inner join dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid
			INNER JOIN tblstate S ON ta.stateid_fk = S.stateID  
			inner join #temptable a on a.recordid  = ta.recordid
			--Afaq 8199 08/31/2010 Remove records containing null dlqupdatedate
			and datediff(day,  a.mdate,  isnull(tva.DLQUpdateDate,''1/1/1900'') ) = 0 
			-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
			AND (SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''O'' AND SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''M'')
			' 
	
	
	-- VIOLATION DESCRIPTION FILTER.....
	-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....if(@violadesc <> '')
	IF (@violadesc <> '')
	BEGIN
	    -- NOT LIKE FILTER.......
	    IF (CHARINDEX('not', @violaop) <> 0)
	        SET @sqlquery = @sqlquery + 'and not ( a.violcount =1  and (' + 
							dbo.Get_String_Concat_With_op('' + @violadesc + '','  tva.violationdescription  ',+ '' + 'like') + '))' 
	    
	    -- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	    -- LIKE FILTER.......
	    IF (CHARINDEX('not', @violaop) = 0)
	        SET @sqlquery = @sqlquery + 'and ( a.violcount =1  and (' + 
							dbo.Get_String_Concat_With_op('' + @violadesc + '','  tva.violationdescription  ',+ '' + 'like') + '))'
	END
	
	-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....                 
	SET @sqlquery = @sqlquery + 
	    '
	    
	    -- Rab Nawaz Khan 8754 03/02/2011 Excluding Dispose violations
		Delete from #temp
		where violationstatusid = 80
		-- End 8754
	    
			Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, mdate, FirstName,LastName,address,                                    
					city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                           
					courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber,dbo.GetPromoPriceTemplate(zipcode,'
					+ CONVERT(VARCHAR, @lettertype) + ') AS promotemplate into #temp1  from #temp' 
	-- Sabir Khan 8559 11/25/2010 exclude client cases
	set @sqlquery=@sqlquery+                       
	' delete from #temp1 where recordid in (
	select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
	on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
	inner join #temp1 a on a.recordid = v.recordid
	)' 
	
	-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
	-- CREATE LETTER HISTORY FOR EACH PERSON....  
	IF (@printtype = 1)
	BEGIN
	    SET @sqlquery = @sqlquery +
	        '      
		Declare @ListdateVal DateTime, @totalrecs int,@count int, @p_EachLetter money,@recordid varchar(50),                                    
		@zipcode   varchar(12),@maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)      
		declare @tempBatchIDs table (batchid int) 
		     

		-- GETTING TOTAL LETTERS AND COSTING ......		
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                         
		Select @count=Count(*) from #temp1                                      
		set @p_EachLetter=convert(money,0.50)                                      


		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                        
		Select distinct mdate  from #temp1                                      
		open ListdateCur                                         
		Fetch Next from ListdateCur into @ListdateVal                                                            
		while (@@Fetch_Status=0)                                    
			begin                                      

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE ......	                     
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                       

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                        
				(' + CONVERT(VARCHAR(10), @empid) + ', ' + CONVERT(VARCHAR(10), @lettertype) 
	        + ' , @ListdateVal, ' + CONVERT(VARCHAR(10), @catnum) + 
	        ', 0, @totalrecs, @p_EachLetter)                                         


				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter       
				insert into @tempBatchIDs 
				select @maxbatch                                                             

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                                     
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                                     
				open RecordidCur                                                               
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                                
				while(@@Fetch_Status=0)                                    
					begin                                    

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...					
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                                    
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'
	        + CONVERT(VARCHAR(10), @lettertype) + 
	        ',@lCourtId, @dptwo, @dpc)                                    
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                
					end                                    
				close RecordidCur       
				deallocate RecordidCur                               
				Fetch Next from ListdateCur into @ListdateVal                                    
			end                                                  

		close ListdateCur        
		deallocate ListdateCur       


		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                                    
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                                    
		dptwo, TicketNumber_PK, t.zipcode, ''' + @user + ''' as Abb,promotemplate                                            
		into #temp2      
		from #temp1 t , tblletternotes n, @tempBatchIDs tb      
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '
	        + CONVERT(VARCHAR(10), @lettertype) + 
	        '       
		order by T.zipcode       
		select @recCount = count(distinct recordid) from #temp2       

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)      
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid      
		select @subject = convert(varchar(20), @lettercount) + '' '' + ''' + @lettername 
	        + ' Letters Printed''      
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( ''' + @user + ''')      
		exec usp_mailer_send_Email @subject, @body,1      

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		'
	END
	ELSE   
	IF (@printtype = 0)
	   -- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	   -- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE
	   -- TO DISPLAY THE LETTER...
	BEGIN
	    SET @sqlquery = @sqlquery + '      
		 
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, LastName,address,city, state,FineAmount,
		violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode, ''' + @user + ''' as Abb,promotemplate
		into #temp3 from #temp1       
		order by zipcode       
		select @recCount = count(distinct recordid) from #temp3'
	END 
	
	-- IF PRINTING SINGLE SIDED LETTERS....      
	IF @language = 0
	BEGIN
	    -- IF ONLY PREVIEWING THE LETTER....
	    -- OUT PUT THE DATA FOR REPORT FILE...
	    IF @printtype = 0
	    BEGIN
	        SELECT @sqlquery = @sqlquery + '
	        select t1.*, 0 as language, convert(varchar(15), t1.recordid) + ''0'' as ChkGroup from #temp3 t1 order by t1.zipcode'
	    END
	    ELSE 
	    IF @printtype = 1 
	       -- IF SENDING LETTERS TO BATCH...
	       -- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..
	    BEGIN
	        SELECT @sqlquery = @sqlquery + '
	        select t2.*, 0 as language, convert(varchar(15), t2.recordid) + ''0'' as ChkGroup from #temp2 t2' 
-- Abbas Shahid Khwaja 8799 4/19/2011 Change the values of dpc and dptwo for barcode values and change zip code.	        
-- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry 
	        -- IF NEED TO SEND A LETTER TO BARRY....
	        IF (@mailbarry = 0)
	        BEGIN
	            SELECT @sqlquery = @sqlquery + '
	            union  
				select convert(varchar(20),@recCount) + '' letters sent''  , ''0'' , ''BARRY'', ''BOBBIT'', 
				''7 Butterfly Place'', ''Austin'', ''TX'', 0, ''N/A'', ''DALLAS MUNICIPAL COURT'', 1, 22 , 
               ''N/A'', ''78738-1343'',''' + @user + ''',''0,35.00'', 0, 0 '
	        END
	        
	        SELECT @sqlquery = @sqlquery + '
	        order by [zipcode] 
	        select isnull(@batchIDs_filname,'''') as batchid'
	    END
	END-- FOR DOUBLE SIDED PRINTING....
	ELSE 
	IF @language = 1
	BEGIN
	    -- IF ONLY PREVIEWING THE LETTER....
	    IF @printtype = 0
	    BEGIN
	        SELECT @sqlquery = @sqlquery + 
	               '       
				declare @table4 TABLE  (      
				[letterId] [varchar] (13)  ,      
				[recordid] [int] ,      
				[FirstName] [varchar] (20)  ,      
				[LastName] [varchar] (20)  ,      
				[address] [varchar] (100) ,      
				[city] [varchar] (50) ,      
				[state] [varchar] (2)  ,      
				[FineAmount] [money]  ,      
				[violationdescription] [varchar] (200) ,      
				[CourtName] [varchar] (50) ,      
				[dpc] [varchar] (10)  ,      
				[dptwo] [varchar] (10)  ,      
				[TicketNumber_PK] [varchar] (20)  ,      
				[zipcode] [varchar] (15) ,      
				[Abb] [varchar] (10) ,
				[promotemplate] [varchar] (100),                
				[language] [int]      
				)      
				  
				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, language)        
				  
				select letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, 0 from #temp3        
				  
				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, language)        
				  
				select letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, 1 from #temp3 ;       
				  
				  
				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 order by zipcode, language ;      
				'
	    END-- IF SENDING LETTERS TO BATCH...
	       -- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..
	    ELSE 
	    IF @printtype = 1
	    BEGIN
	        SELECT @sqlquery = @sqlquery + 
	               '       
				declare @table4 TABLE  (      
				[letterId] [varchar] (13)  ,      
				[recordid] [int] ,      
				[FirstName] [varchar] (20)  ,      
				[LastName] [varchar] (20)  ,      
				[address] [varchar] (100) ,      
				[city] [varchar] (50) ,      
				[state] [varchar] (2)  ,      
				[FineAmount] [money]  ,      
				[violationdescription] [varchar] (200) ,      
				[CourtName] [varchar] (50) ,      
				[dpc] [varchar] (10)  ,      
				[dptwo] [varchar] (10)  ,      
				[TicketNumber_PK] [varchar] (20)  ,      
				[zipcode] [varchar] (15) ,      
				[Abb] [varchar] (10) ,
				[promotemplate] [varchar] (100),          
				[language] [int]      
				)      
				'
	        
	        SELECT @sqlquery = @sqlquery + 
	               ' 
				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, language)        

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,      
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, 0 from #temp2        

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, language)        

				select letterid, recordid, firstname, lastname, address, city, state, fineamount,      
				violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, Abb,promotemplate, 1 from #temp2 ; '
	        
	        SELECT @sqlquery = @sqlquery + 
	               ' 
				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 '  
-- Abbas Shahid Khwaja 8799 4/19/2011 Change the values of dpc and dptwo for barcode values and change zip code.	        
-- Rab Nawaz Khan 10187 04/12/2012 Address, City and Zip Code Changed for the Testing Advertisment mailer which send to Barry 
	        IF (@mailbarry = 0)
	        BEGIN
	            SELECT @sqlquery = @sqlquery + 
	                   'union  
						select convert(varchar(20),@recCount) + '' letters sent''  , ''0'' , ''BARRY'', ''BOBBIT'', 
						''7 Butterfly Place'', ''Austin'', ''TX'', 0, ''N/A'', ''DALLAS MUNICIPAL COURT'', 1, 22 , 
	                    ''N/A'', ''78738-1343'',''' + @user + ''',''0,35.00'', 0, 0 '
	        END
	        
	        SELECT @sqlquery = @sqlquery + 
	               'order by [zipcode], [language] 
					select isnull(@batchIDs_filname,'''') as batchid'
	    END
	END 
	
	print @sqlquery
	
	-- EXECUTING THE DYNAMIC SQL ....
	EXEC (@sqlquery)  
	

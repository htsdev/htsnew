SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_OUTSIDECOURT_RECORDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_OUTSIDECOURT_RECORDS]
GO


CREATE PROCEDURE [dbo].[USP_HTS_GET_OUTSIDECOURT_RECORDS]    
AS        
select distinct tv.ticketid_pk, tv.casenumassignedbycourt as TicketNumber, tv.courtdate as Courtdate        
from tblticketsviolations tv         
inner join tblticketsnotes tn on tv.ticketid_pk = tn.ticketid       
inner join tbltickets t on   t.TicketID_PK = tv.TicketID_PK      
where tv.courtviolationstatusid IN (104,135)         
and courtid between 3007 and 3022        
--commented by ozair for task 2613 at 01/15/2008
--and tn.subject like ('%letter of Rep printed%')        
--and DateDiff(day,(Select max(recdate) from tblticketsnotes where notesid_pk = tn.notesid_pk),getdate())>=30 
--end task 2613
and t.ActiveFlag = 1 and tv.courtviolationstatusidmain <> 80      
and len(tv.casenumassignedbycourt)=12    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


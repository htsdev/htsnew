/****** 
Altered by:  Muhammad Nasir

Business Logic : this procedure is used to Insert the information in tbl_htp_documenttracking_scanbatch_detail table and 
				 returns the inserted id back to the calling method

List of Parameters:	
	@startdate : start date record added
	@enddate : end date record added 
	@firmid : firm id	

List of Columns: 	
	TicketID_PK : Case ID. 
	clientname	: Client Name
	firmname	: Firm Name
	coveringfirm: Convering firm
	TotalFeeCharged: total fees
	Addition: addition
	
******/

--Nasir 7007 11/17/2009 modify whole sp remove dynamic query and replace 'CurrentCourtloc' to court id

--USP_HTS_GET_ExchangeDocket_Report '09/24/2009', '11/17/2009', 3000
ALTER PROCEDURE USP_HTS_GET_ExchangeDocket_Report-- '','',0
	@startdate VARCHAR(20),
	@enddate VARCHAR(20),
	@firmid VARCHAR(10)=0 
	 
	 /*    
	 set @startdate='06/01/2002'    
	 set @enddate='10/25/2002'    
	 set @firmid='0'    
	 */
AS

	
	SET @enddate = @enddate+' 23:58:00'
	
	
	
	SELECT DISTINCT T.TicketID_PK
	      ,T.Lastname+', '+T.Firstname AS clientname
	      ,F.FirmAbbreviation AS firmname
	      ,F1.FirmAbbreviation AS coveringfirm
	      ,C.shortcourtname
	      ,T.TotalFeeCharged
	      ,T.Addition INTO #temp
	FROM   dbo.tblTickets T
	       INNER JOIN tblTicketsViolations ttv
	            ON  t.TicketID_PK = ttv.TicketID_PK
	       INNER JOIN dbo.tblFirm F
	            ON  T.FirmID = F.FirmID
	       INNER JOIN dbo.tblFirm F1
	            ON  T.CoveringFirmID = F1.FirmID
	       INNER JOIN dbo.tblCourts C
	            ON  ttv.CourtID = C.Courtid
	WHERE  (T.Activeflag=1)
	       AND (T.FirmID<>3000 OR T.coveringfirmid<>3000)
	       AND (@firmid=0 OR (@firmid<>0 AND t.FirmID=@firmid))
	
	
	SELECT #temp.*
	      ,dbo.formatdate(MIN(recdate)) AS clientdate
	FROM   #temp
	      ,tblticketspayment P
	WHERE  #temp.ticketid_pk = P.ticketid
	GROUP BY
	       TicketID_PK
	      ,clientname
	      ,firmname
	      ,coveringfirm
	      ,shortcourtname
	      ,TotalFeeCharged
	      ,Addition
	HAVING (
	           MIN(recdate) BETWEEN @startdate AND dbo.formatdate(DATEADD(DAY ,1 ,@enddate))
	       )
	ORDER BY
	       CONVERT(DATETIME ,dbo.formatdate(MIN(recdate))) 
	       DESC
	
  
  
  
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/******************

CREATED BY : SARIM GHANI

BUSINESS LOGIC : This procedure is used to get cases which have discrepancies.

LIST OF INPUT PARAMETERS :

	@startdate = Start Date             
	@enddate = End Date      
	@showall = Show All Records               
	@showdisp = Show Disposed Cases
	@NotShowAWorBW = Show Arraignment or Bond Waiting Case

LIST OF COLUMNS :
	
	CoveringFirmID : Covering Firm Id Which Cover The Case	
	Arrest Date : Arrest Date Associated With The Violation
	CaseTypeId  : Case Type Of Case
	MissedCourtType : Violation Missed Court Type
*******************/  
      
      
--Zeeshan Ahmed 3757 04/17/2007
--Add HasNOS Column in the report for Update Violation Control      
-- usp_validationReport_2 '4/3/2009','4/3/2009' ,1 ,0 , 0                   
ALTER PROCEDURE [dbo].[usp_validationReport_2]
	@startdate
AS
	DATETIME, 
	@enddate AS DATETIME, 
	@showall AS INT = 0, 
	@showdisp AS INT = 0 , 
	@NotShowAWorBW AS BIT = 0 
	
	AS                              
	
	DECLARE @Vld INT                    
	SET @Vld = 1 
	IF @showall = 1
	    SELECT @startdate = '01/01/1900',
	           @enddate = CONVERT(VARCHAR(12), GETDATE()) + ' 23:59:58'
	
	CREATE TABLE #temp
	(
		-- Adil 5737 04/06/2009 change table variable into temp table
		rowid                 INT IDENTITY(1, 1),
		TicketID_PK           INT,
		refcasenumberseq      VARCHAR(30),
		courtdate             VARCHAR(50),
		courtnumber           VARCHAR(20),
		courtdatemain         VARCHAR(50),
		courtnumbermain       VARCHAR(20),
		shortname             VARCHAR(20),
		ticketsviolationid    INT,
		VDesc                 VARCHAR(20),
		ADesc                 VARCHAR(20),
		activeflag            INT,
		Vtypeid               INT,
		Atypeid               INT,
		Vld                   INT,
		VrS                   INT,
		AtS                   INT,
		-- Added By Zeeshan Ahmed For Update Panel ---            
		BondAmount            MONEY,
		FineAmount            MONEY,
		CauseNumber           VARCHAR(20),
		PlanId                INT,
		BondFlag              TINYINT,
		ViolationNumber_pk    INT,
		OscarCourtDetail      VARCHAR(50),
		CourtDateMain_1       DATETIME,
		ViolationDescription  VARCHAR(1000),
		courtid               INT,
		chargelevel           INT,
		iscriminalcourt       INT,
		CDI                   VARCHAR(3),
		hasFTAViolations      INT,
		isFTAViolation        INT,
		hasNOS                INT,
		CoveringFirmId        INT,
		ArrestDate            DATETIME,
		CaseTypeId            INT,
		MissedCourtType       INT 
		-----------------------------------------------
	)                      
	
	INSERT INTO #temp
	SELECT DISTINCT 
	       TT.TicketID_PK,
	       refcasenumber AS refcasenumberseq,
	       dbo.fn_DateFormat(ISNULL(TTV.courtdate, '1/1/1900'), 25, '/', ':', 1) AS 
	       courtdate,	--Fahad 5557 03/09/2009 Add dateformat function and comment below Line
	                 	--convert(varchar(11),TTV.courtdate,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdate),13,18),'12:00AM','0:00AM') AS courtdate,         --upload--auto                              
	       dbo.Formatcourtnumbers(CONVERT(INT, ISNULL(TTV.courtnumber, 0))) AS 
	       courtnumber,	--Fahad 5557 03/09/2009 Add Function to format Court No                           
	       dbo.fn_DateFormat(ISNULL(TTV.courtdatemain, '1/1/1900'), 25, '/', ':', 1) AS 
	       courtdatemain,	--Fahad 5557 03/09/2009 Add dateformat function and comment below Line
	                     	--convert(varchar(11),TTV.courtdatemain,101)+'@'+replace(substring(convert(varchar(20),TTV.courtdatemain),13,18),'12:00AM','0:00AM') AS courtdatemain, --verified                               
	       dbo.Formatcourtnumbers(
	           CONVERT(
	               INT,
	               ISNULL(dbo.Formatcourtnumbers(TTV.courtnumbermain), 0)
	           )
	       ) AS courtnumbermain,	--Fahad 5557 03/09/2009 Add Function to format Court No                                       
	       TC.shortname,
	       ticketsviolationid,
	       ISNULL(c1.shortdescription, 'N/A') AS VDesc,
	       ISNULL(c2.shortdescription, 'N/A') AS ADesc,
	       tt.activeflag,
	       c1.categoryid AS Vtypeid,
	       c2.categoryid AS Atypeid,
	       dbo.Fn_CheckDiscrepency(ttv.Ticketsviolationid) AS Vld,
	       ttv.courtviolationstatusidmain,
	       ttv.courtviolationstatusid 
	       
	       -- Added By Zeeshan Ahmed For Update Panel            
	       ,
	       ttv.bondamount,
	       ttv.fineamount,
	       ttv.casenumassignedbycourt AS causenumber,
	       ttv.planid,
	       ttv.underlyingbondflag AS bondflag,
	       ttv.violationnumber_pk,
	       ttv.oscarcourtdetail,
	       ttv.Courtdatemain,
	       dbo.tblViolations.Description + (
	           CASE 
	                WHEN ttv.violationnumber_pk = 0 THEN ''
	                ELSE ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + ')'
	           END
	       ) AS violationDescription,
	       ttv.courtid,
	       ------------------------------            
	       ISNULL(ttv.chargelevel, 0),
	       tc.iscriminalcourt,
	       ISNULL(cdi, '0'),
	       dbo.fn_CheckFTAViolationInCase(TT.TicketID_PK),
	       dbo.fn_CheckFTAViolation(ttv.ticketsviolationid),
	       CASE 
	            WHEN (
	                     SELECT COUNT(*)
	                     FROM   tblticketsflag
	                     WHERE  flagid = 15
	                            AND ticketid_pk = tt.ticketid_pk
	                 ) = 0 THEN 0
	            ELSE 1
	       END,
	       ttv.CoveringFirmId,
	       ttv.ArrestDate,
	       ISNULL(tt.casetypeid, 0) AS casetypeid,
	       ISNULL(TTV.MissedCourtType, -1)
	FROM   tbltickets tt,
	       tblticketsviolations ttv,
	       tblCourtViolationStatus c1,
	       tblCourtViolationStatus c2,
	       tblcourts tc,
	       tblViolations,
	       tblViolationCategory vc 
	       
	       -- Added By Zeeshan Ahmed ------------------------------------------------------
	       --------------------------------------------------------------------------------
	WHERE  1 = 1 
	       -- JOIN CLAUSES.........
	       AND tt.ticketid_pk = ttv.ticketid_pk
	       AND ttv.courtviolationstatusid = c2.courtviolationstatusid
	       AND ttv.courtviolationstatusidmain = c1.courtviolationstatusid
	       AND ttv.courtid = tc.courtid
	       AND ttv.ViolationNumber_PK = tblViolations.ViolationNumber_PK
	       AND tblViolations.CategoryID = vc.CategoryId
	           ---------Added By Zeeshan Ahmed            
	           
	           -------------------------------------------------------------------------------------
	           -- FILTERS FOR DISCREPANCY LOGIC.....
	       AND (
	               (
	                   ttv.courtnumber <> ttv.courtnumbermain
	                   OR ttv.courtdate <> ttv.courtdatemain
	                   OR c1.categoryid <> c2.categoryid
	               )
	           ) 
	           
-- ELIMINATE VERIFIED DISPOSED CASES IF COURT DATES ARE SAME OR AUTO COURT DATE IS PASSED.....                      
AND not (                      
   c1.categoryid = 50                      
   and (c1.courtviolationstatusid = c2.courtviolationstatusid or datediff(day,ttv.courtdate, getdate())   > 0 )                      
  )                      
                      
                    
                     
-- ELIMINATE MISSED & NON ISSUE CASES IF AUTO STATUS IS DISPOSED.........                      
and not (                      
  c1.categoryid in (7,52)                      
 and c2.categoryid = 50                      
 )                      
                      
-- ELIMINATE IF  VERIFIED STATUS = DISP & AUTO STATUS = DEF                      
AND NOT ( C1.categoryid = 50  and c2.categoryid = 52 )                      
                      
-- ELIMINATE IF VERIFIED STATUS = MIS & AUTO STATUS = FTA....                      
AND NOT ( c1.categoryid = 7 and c2.categoryid = 52) 

--Fahad 5557 03/04/2009
--ELIMINATE IF VERIFIED STATUS = MISSED COURT,DISPOSED & AUTO STATUS = JURY,JUDGE,PRETRIAL,ARRAIGNMENT
AND NOT (c1.CategoryID=7)
AND NOT ((c1.categoryid =50) AND c2.categoryid IN (4, 5, 3, 2))
AND DATEDIFF(DAY, ttv.CourtDate, GETDATE()) >= 0
	           
	           -- FILTER CLAUSES......
	       AND activeflag = 1
	       AND TTV.courtdate BETWEEN @startdate AND @enddate
	       AND ttv.courtid IN (3001, 3002, 3003)
	ORDER BY
	       TT.ticketid_pk 
	--------------- Greg's Modification 06 MAR 07 ----------------
	-- ELIMINATE AUTO DISPOSED CASES; and VERIFIED COURT DATE IS PASSED.                    
	
	IF @showdisp = 1
	BEGIN
	    DELETE 
	    FROM   #temp
	    WHERE  Atypeid = 50
	           AND DATEDIFF(
	                   DAY,
	                   CONVERT(DATETIME, SUBSTRING(courtdatemain, 0, 9)),
	                   GETDATE()
	               ) > 0
	END 
	
	--------------- Greg's Modification 06 MAR 07 ----------------                    
	
	CREATE TABLE #temp2
	(
		sortorder             INT IDENTITY(1, 1),
		rowid                 INT,
		TicketID_PK           INT,
		refcasenumberseq      VARCHAR(30),
		courtdate             VARCHAR(50),
		courtnumber           VARCHAR(20),
		courtdatemain         VARCHAR(50),
		courtnumbermain       VARCHAR(20),
		shortname             VARCHAR(20),
		ticketsviolationid    INT,
		VDesc                 VARCHAR(20),
		ADesc                 VARCHAR(20),
		activeflag            INT,
		Vtypeid               INT,
		Atypeid               INT,
		Vld                   INT,
		VrS                   INT,
		AtS                   INT,
		-- Added By Zeeshan Ahmed For Update Panel ---            
		BondAmount            MONEY,
		FineAmount            MONEY,
		CauseNumber           VARCHAR(20),
		PlanId                INT,
		BondFlag              TINYINT,
		ViolationNumber_pk    INT,
		OscarCourtDetail      VARCHAR(50),
		CourtDateMain_1       DATETIME,
		ViolationDescription  VARCHAR(1000),
		courtid               INT,
		chargelevel           INT,
		iscriminalcourt       INT,
		cdi                   VARCHAR(3),
		hasFTAViolations      INT,
		isFTAViolation        INT,
		hasNOS                INT,
		CoveringFirmId        INT,
		ArrestDate            DATETIME,
		CaseTypeId            INT,
		MissedCourtType       INT
		-----------------------------------------------
	) 
	
	-- FIRST DISPLAY VERIFIED = JUR OR ARR AND AUTO = JUR OR ARR                      
	INSERT INTO #temp2
	  (
	    rowid,
	    ticketid_pk,
	    refcasenumberseq,
	    courtdate,
	    courtnumber,
	    courtdatemain,
	    courtnumbermain,
	    shortname,
	    ticketsviolationid,
	    vdesc,
	    adesc,
	    activeflag,
	    vtypeid,
	    atypeid,
	    Vld,
	    VrS,
	    AtS 
	    -- Added By Zeeshan Ahmed            
	    ,
	    BondAmount,
	    FineAmount,
	    CauseNumber,
	    PlanId,
	    BondFlag,
	    ViolationNumber_pk,
	    OscarCourtDetail,
	    CourtDateMain_1,
	    ViolationDescription,
	    courtid,
	    chargelevel,
	    iscriminalcourt,
	    cdi,
	    hasFTAViolations,
	    isFTAViolation,
	    hasNOS,
	    CoveringFirmId,
	    ArrestDate,
	    CaseTypeId,
	    MissedCourtType
	    ----------------------------
	  )
	SELECT rowid,
	       ticketid_pk,
	       refcasenumberseq,
	       courtdate,
	       courtnumber,
	       courtdatemain,
	       courtnumbermain,
	       shortname,
	       ticketsviolationid,
	       vdesc,
	       adesc,
	       activeflag,
	       vtypeid,
	       atypeid,
	       Vld,
	       VrS,
	       AtS 
	       --- Added By Zeeshan Ahmed---            
	       ,
	       BondAmount,
	       FineAmount,
	       CauseNumber,
	       PlanId,
	       BondFlag,
	       ViolationNumber_pk,
	       OscarCourtDetail,
	       CourtDateMain_1,
	       ViolationDescription,
	       courtid,
	       chargelevel,
	       iscriminalcourt,
	       cdi,
	       hasFTAViolations,
	       isFTAViolation,
	       hasNOS,
	       CoveringFirmId,
	       ArrestDate,
	       CaseTypeId,
	       MissedCourtType 
	       
	       ------------------------------
	FROM   #temp
	WHERE  (
	           courtnumber <> courtnumbermain
	           OR courtdate <> courtdatemain
	           OR vdesc <> adesc
	       )
	       AND vtypeid IN (2, 4)
	       AND atypeid IN (2, 4)
	ORDER BY
	       vtypeid,
	       atypeid 
	
	-- THEN DISPLAY VERFIED = JUR AND AUTO <> JUR & ARR                      
	INSERT INTO #temp2
	  (
	    rowid,
	    ticketid_pk,
	    refcasenumberseq,
	    courtdate,
	    courtnumber,
	    courtdatemain,
	    courtnumbermain,
	    shortname,
	    ticketsviolationid,
	    vdesc,
	    adesc,
	    activeflag,
	    vtypeid,
	    atypeid,
	    Vld,
	    VrS,
	    AtS 
	    -- Added By Zeeshan Ahmed            
	    ,
	    BondAmount,
	    FineAmount,
	    CauseNumber,
	    PlanId,
	    BondFlag,
	    ViolationNumber_pk,
	    OscarCourtDetail,
	    CourtDateMain_1,
	    ViolationDescription,
	    courtid,
	    chargelevel,
	    iscriminalcourt,
	    cdi,
	    hasFTAViolations,
	    isFTAViolation,
	    hasNOS,
	    CoveringFirmId,
	    ArrestDate,
	    CaseTypeId,
	    MissedCourtType
	    ----------------------------
	  )
	SELECT rowid,
	       ticketid_pk,
	       refcasenumberseq,
	       courtdate,
	       courtnumber,
	       courtdatemain,
	       courtnumbermain,
	       shortname,
	       ticketsviolationid,
	       vdesc,
	       adesc,
	       activeflag,
	       vtypeid,
	       atypeid,
	       Vld,
	       VrS,
	       AtS 
	       --- Added By Zeeshan Ahmed---            
	       ,
	       BondAmount,
	       FineAmount,
	       CauseNumber,
	       PlanId,
	       BondFlag,
	       ViolationNumber_pk,
	       OscarCourtDetail,
	       CourtDateMain_1,
	       ViolationDescription,
	       courtid,
	       chargelevel,
	       iscriminalcourt,
	       cdi,
	       hasFTAViolations,
	       isFTAViolation,
	       hasNOS,
	       CoveringFirmId,
	       ArrestDate,
	       CaseTypeId,
	       MissedCourtType 
	       ------------------------------
	FROM   #temp
	WHERE  (
	           courtnumber <> courtnumbermain
	           OR courtdate <> courtdatemain
	           OR vdesc <> adesc
	       )
	       AND vtypeid IN (4)
	       AND atypeid NOT IN (2, 4)
	       AND rowid NOT IN (SELECT rowid
	                         FROM   #temp2)
	ORDER BY
	       vtypeid,
	       atypeid 
	
	-- THEN DISPLAY REMAINING CASES.......                      
	INSERT INTO #temp2
	  (
	    rowid,
	    ticketid_pk,
	    refcasenumberseq,
	    courtdate,
	    courtnumber,
	    courtdatemain,
	    courtnumbermain,
	    shortname,
	    ticketsviolationid,
	    vdesc,
	    adesc,
	    activeflag,
	    vtypeid,
	    atypeid,
	    Vld,
	    VrS,
	    AtS 
	    --- Added By Zeeshan Ahmed---            
	    ,
	    BondAmount,
	    FineAmount,
	    CauseNumber,
	    PlanId,
	    BondFlag,
	    ViolationNumber_pk,
	    OscarCourtDetail,
	    CourtDateMain_1,
	    ViolationDescription,
	    courtid,
	    chargelevel,
	    iscriminalcourt,
	    cdi,
	    hasFTAViolations,
	    isFTAViolation,
	    hasNOS,
	    CoveringFirmId,
	    ArrestDate,
	    CaseTypeId,
	    MissedCourtType 
	    ------------------------------
	  )
	SELECT rowid,
	       ticketid_pk,
	       refcasenumberseq,
	       courtdate,
	       courtnumber,
	       courtdatemain,
	       courtnumbermain,
	       shortname,
	       ticketsviolationid,
	       vdesc,
	       adesc,
	       activeflag,
	       vtypeid,
	       atypeid,
	       Vld,
	       VrS,
	       AtS 
	       --- Added By Zeeshan Ahmed---            
	       ,
	       BondAmount,
	       FineAmount,
	       CauseNumber,
	       PlanId,
	       BondFlag,
	       ViolationNumber_pk,
	       OscarCourtDetail,
	       CourtDateMain_1,
	       ViolationDescription,
	       courtid,
	       chargelevel,
	       iscriminalcourt,
	       cdi,
	       hasFTAViolations,
	       isFTAViolation,
	       hasNOS,
	       CoveringFirmId,
	       ArrestDate,
	       CaseTypeId,
	       MissedCourtType 
	       ------------------------------
	FROM   #temp
	WHERE  (
	           courtnumber <> courtnumbermain
	           OR courtdate <> courtdatemain
	           OR vdesc <> adesc
	       )
	       AND rowid NOT IN (SELECT rowid
	                         FROM   #temp2)
	ORDER BY
	       vtypeid,
	       atypeid 
	
	-- Added by Zeeshan
	-- Dont Show A/W and B/W in discrepency if the @NotShowAWorBW =1              
	
	IF @NotShowAWorBW = 0
	BEGIN
	    SELECT DISTINCT *
	    FROM   #temp2
	    WHERE  vld = 1
	    ORDER BY
	           sortorder
	END
	ELSE
	BEGIN
	    SELECT DISTINCT *
	    FROM   #temp2
	    WHERE  vld = 1
	           AND Vrs NOT IN (201, 202)
	    ORDER BY
	           sortorder
	END 
	
	
	DROP TABLE #temp
	DROP TABLE #temp2

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_generalcomments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_generalcomments]
GO


Create Procedure [dbo].[usp_hts_get_generalcomments] --124773        
        
@TicketID int,
@fid int        
        
as        
        
        
select generalcomments from tbltickets where ticketid_pk=@TicketID       
--added khalid 14-9-07      
 select top 1  isnull(ContinuanceStatus,0) as ContinuanceStatus  from tblTicketsFlag  where (ticketid_pk=@TicketID and FlagID=23 AND Fid = @fid) order by fid desc    


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


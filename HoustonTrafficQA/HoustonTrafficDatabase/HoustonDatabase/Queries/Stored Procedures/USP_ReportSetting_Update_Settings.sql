/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:37:52 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 /*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used update report setting based on setting id.
*					: 
* Parameter List	
  @AttributeValue	: No of days which can be either business days, followup date days or default followup date days ect, based on attribute id of the record.
  @LastUpdateBy		: User id who is performing modification of the record.
  @ReportSettingId	: Primary key of the table aginst which modification will perform.	
* 
* USP_DTP_GET_ReportSetting 52
*/
CREATE PROC [dbo].[USP_ReportSetting_Update_Settings]
@AttributeValue VARCHAR(500),
@LastUpdateBy INT,
@ReportSettingId INT
AS
UPDATE ReportSetting
SET    AttributeValue = @AttributeValue,
       LastUpdateDate = GETDATE(),
       LastUpdateBy = @LastUpdateBy
WHERE  ReportSettingId = @ReportSettingId	

GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_Update_Settings] TO dbr_webuser
GO

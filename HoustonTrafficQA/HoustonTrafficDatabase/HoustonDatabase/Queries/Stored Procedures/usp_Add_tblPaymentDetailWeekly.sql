SET QUOTED_IDENTIFIER ON 
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Business Logic:
* This procedure is used to insert weekly payment detail. ******/

ALTER PROCEDURE [dbo].[usp_Add_tblPaymentDetailWeekly]
	@EmployeeID int,
	@TransDate datetime,
	@ActualCash money,
	@ActualCheck money,
	@Notes varchar(1200),
	@branchID INT =0 -- Sabir Khan 10920 05/27/2013 Branch id added.
AS
SET NOCOUNT ON
INSERT [dbo].[tblPaymentDetailWeekly]
(
	[EmployeeID],
	[TransDate],
	[ActualCash],
	[ActualCheck],
	[Notes],
	[BranchID]
)
VALUES
(
	@EmployeeID,
	@TransDate,
	@ActualCash,
	@ActualCheck,
	@Notes,
	@branchID
)

	





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


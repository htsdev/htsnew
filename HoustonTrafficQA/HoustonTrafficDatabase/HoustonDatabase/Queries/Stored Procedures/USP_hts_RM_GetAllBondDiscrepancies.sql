SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_hts_RM_GetAllBondDiscrepancies]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_hts_RM_GetAllBondDiscrepancies]
GO

CREATE procedure USP_hts_RM_GetAllBondDiscrepancies  
--  
as  
SELECT --updateddate,       
  t.ticketid_Pk, tv.RefCaseNumber AS [Ticket Number],   
  tv.casenumassignedbycourt AS [Cause Number],   
  tv.CourtDate AS [Auto Court Date],   
  tv.CourtDateMain AS [Verified Court Date],   
	case when t.BondFlag = 1 then 'YES' else 'NO' end AS [Bond Flag Main],   
  case when ISNULL(tv.UnderlyingBondFlag, 0) = 1 then 'YES' else 'NO' end AS [Underlying Bond Flag],
 courtid  
FROM    dbo.tblTickets AS t INNER JOIN  
           dbo.tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK  
WHERE  (t.Activeflag = 1)   
 and (t.BondFlag = 1)   
 and Courtviolationstatusidmain <> 80  
 and t.ticketid_pk not in (select ticketid_pk from tblticketsviolations where (ISNULL(UnderlyingBondFlag, 0) = 1) )  
  --order by updateddate desc
  
  
   
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


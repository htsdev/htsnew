SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_Manualcc_Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_Manualcc_Report]
GO



-- =============================================
-- Author:		<Asghar>
-- Create date: <Feb/16/2007>
-- Description:	<This procedure is just get data from tblmanualpayment to bind with manualcc report>
-- =============================================
--select top 5 * from tblmanualpayment 
--	USP_HTS_GET_Manualcc_Report '9/1/2005 12:00:00 AM','7/24/2007 12:00:00 AM',0
CREATE PROCEDURE [dbo].[USP_HTS_GET_Manualcc_Report]

	@startdate	datetime,
	@enddate	datetime,
	@clienttype	int	
AS

select @enddate =@enddate +'23:59:59.000'

if(@clienttype =0)
 BEGIN
		select 
		m.PaymentID,
		m.fname +' '+lname as cardholder,
		dbo.formatdateandtimeintoshortdateandtime(m.recdate) as recdate,
		m.ccno,
		m.expdate,
		m.cin,
		m.transNum,
		m.amount,
		m.description,	
		m.employeeid,
		m.voidpayment,
		m.refundamount,
			--m.response_reason_text,	
			case m.approveflag when 1 then 'Approved' else 'Declined'  end as response_reason_text,
		u.abbreviation,
		case m.clienttype 
			when 1 then 'Traffic'
			when 2 then 'Oscar Client'
			when 3 then 'Lisa Client'
			when 4 then 'Other'
		end as clienttype
	from tblmanualpayment m inner join tblusers u
		 on m.employeeid =u.employeeid
		 and m.recdate between @startdate and @enddate
		-- and m.approveflag =1	
END 

 ELSE
	BEGIN
      		select 
			m.PaymentID,
			m.fname +' '+lname as cardholder,
			dbo.formatdateandtimeintoshortdateandtime(m.recdate) as recdate,
			m.ccno,
			m.expdate,
			m.cin,
			m.transNum,
			m.amount,
			m.description,	
			m.employeeid,
			m.voidpayment,
			m.refundamount,	
			--m.response_reason_text,	
			case m.approveflag when 1 then 'Approved' else 'Declined'  end as response_reason_text,
			u.abbreviation,
		case m.clienttype 
			when 1 then 'Traffic'
			when 2 then 'Oscar Client'
			when 3 then 'Lisa Client'
			when 4 then 'Other'
		end as clienttype
		from tblmanualpayment m inner join tblusers u
			 on m.employeeid =u.employeeid 
			 and m.recdate between @startdate and @enddate
		AND clienttype =@clienttype   
		--and m.approveflag =1
	END






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


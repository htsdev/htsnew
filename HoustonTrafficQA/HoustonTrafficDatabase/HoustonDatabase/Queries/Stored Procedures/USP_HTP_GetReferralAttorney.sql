﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   12/03/2010 
* Task ID :		  8585
* Business Logic :  This procedure is used to get get all referral attroneys in the system.
* List of Parameter :
* Column Return :     
******/
CREATE PROCEDURE [dbo].[USP_HTP_GetReferralAttorney]
AS
	SELECT Id,[Name] FROM ReferralAttorney  WHERE id <>2
GO
GRANT EXECUTE ON [dbo].[USP_HTP_GetReferralAttorney] TO dbr_webuser
GO 	    
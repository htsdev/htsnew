﻿    
/************    
    
Alter By : Zeeshan Ahmed    
    
Business Logic : This procedure is used to get Arrignment Setting Summary Documents of the cases    
				 Zeeshan 4420 08/08/2008 Modify Procedure to display distinct documents          
				 Noufil 4707 09/12/2008 Select Batch Date In The Procedure
List Of Parameters :    
    
@Ticketids : Case Numbers    
    
List Of Columns :    
    
SBID    
SBDID    
DocumentBatchPageNo    
DBID    
BatchDate 
    
************/    
  --[usp_htp_get_HMCArrWaitingDocuments] '5334,'  
Alter procedure [dbo].[usp_htp_get_HMCArrWaitingDocuments]    
@Ticketids varchar(max)    
as    
    
declare @Tickets as table(tickets int)    
    
insert into @Tickets select maincat from dbo.Sap_String_Param_bigint(@ticketids)    
    
 select   
  sbd.DocumentBatchID,  
  sbd.ScanBatchID_FK ,   
  B.BatchDate   
  into #tempScanBatchIds    
 from tbl_htp_documenttracking_batchtickets bt    
  
 inner join tbl_htp_documenttracking_scanbatch_detail sbd    
   on bt.batchid_fk=sbd.documentbatchid     
 inner join @Tickets T   
   on T.tickets=bt.ticketid_fk    
 inner join tbl_htp_documenttracking_batch B   
   on b.batchid = bt.batchid_fk    
  
 group by sbd.ScanBatchID_FK,sbd.DocumentBatchID  ,B.BatchDate   
    
    
 select Detail.ScanBatchID_FK as SBID,  
   max(Detail.scanbatchdetailid) as SBDID,      
   Detail.DocumentBatchID as DBID,   
   max(Detail.documentbatchpageno) as documentbatchpageno    
   ,convert(varchar,Ids.BatchDate,106) as BatchDate  
 from tbl_htp_documenttracking_scanbatch_detail as Detail, #tempScanBatchIds as Ids      
  where Detail.ScanBatchID_FK=Ids.ScanBatchID_FK      
   and Detail.DocumentBatchID=Ids.DocumentBatchID      
 group by Detail.ScanBatchID_FK , Detail.DocumentBatchID ,Ids.BatchDate     
    
--ozair 4879 09/29/2008 included document from old scannig structure
select     
  n.recordid as docid,    
  max(n.printdate) as printdate  
from tblhtsnotes n      
 inner join @Tickets T     
   on T.tickets=n.ticketid_fk 
where n.letterid_fk=10      
group by n.recordid, n.printdate
    
drop table #tempScanBatchIds    


    
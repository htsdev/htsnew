USE [TrafficTickets]
GO
/*  
Created By : Noufil Khan  
Business Logic : This procedure Update USPSTrackingNumber and USPSResponse and active EFLAG and is pirnted field
Parameter : @PrintEmpId,@USPSTrackingNumber,@USPSResponse,@BatchId_pk,@eflag,@DeliveryConfirmationLabel
*/
-- Abid Ali 5359 1/10/2009 formatted sp

ALTER PROCEDURE [dbo].[Usp_Hts_Edelivery_Update]
	@PrintEmpId INT,
	@USPSTrackingNumber VARCHAR(50),
	@USPSResponse XML,
	@BatchId_pk INT,
	@eflag BIT,
	@DeliveryConfirmationLabel IMAGE = NULL
AS

	-- abid 5563 02/21/2009 USPSConfirmDate date
	DECLARE @LetterType INT
	SELECT @LetterType = LetterID_FK
	FROM tblHTSBatchPrintLetter
	WHERE BatchId_pk = @BatchId_pk
	      AND deleteflag <> 1

	IF @eflag = 1 AND @LetterType = 6
	BEGIN
		UPDATE tblhtsbatchprintletter
		SET    ISPrinted = 1,
			   USPSConfirmDate = GETDATE(),
			   PrintEmpID = 3992,
			   USPSTrackingNumber = @USPSTrackingNumber,
			   USPSResponse = @USPSResponse,
			   EDeliveryFlag = @eflag,
			   DeliveryConfirmationLabel = @DeliveryConfirmationLabel
		WHERE  BatchId_pk = @BatchId_pk
			   AND deleteflag <> 1	
	END
	ELSE
	BEGIN
		UPDATE tblhtsbatchprintletter
		SET    ISPrinted = 1,
			   PrintDate = GETDATE(),
			   PrintEmpID = @PrintEmpId,
			   USPSTrackingNumber = @USPSTrackingNumber,
			   USPSResponse = @USPSResponse,
			   EDeliveryFlag = @eflag,
			   DeliveryConfirmationLabel = @DeliveryConfirmationLabel
		WHERE  BatchId_pk = @BatchId_pk
			   AND deleteflag <> 1
	END
	
	-- Abid Ali 5359 1/10/2009 add notes into history
	IF (
	       EXISTS(
	           SELECT *
	           FROM   tblHTSBatchPrintLetter
	           WHERE  BatchId_pk = @BatchId_pk
	                  AND deleteflag <> 1
	                  AND LetterID_FK = 6
	       )
	   )
	BEGIN
	    DECLARE @TicketId INT
	    
	    SELECT @TicketId = TicketID_FK
	    FROM   tblHTSBatchPrintLetter t
	    WHERE  BatchId_pk = @BatchId_pk
	           AND deleteflag <> 1
	    
	    DECLARE @URL VARCHAR(500)
	    IF @eflag = 0
	    BEGIN
	    	-- Abid Ali 5544 02/17/2009 comments has been modified
	        SET @URL = '<a href="../docstorage/Letter Of Rep/' + @USPSTrackingNumber
	            + '_Printed.pdf" target="_blank">Letter of Rep printed through Batch print � CM � ' + @USPSTrackingNumber + '</a>'
	    END
	    
	    BEGIN
	    	-- Abid Ali 5544 02/17/2009 comments has been modified
	        SET @URL = '<a href="../docstorage/Letter Of Rep/' + @USPSTrackingNumber
	            + '_Confirmed.pdf" target="_blank">Letter of Rep Certified Mail Confirmation - CM � ' + @USPSTrackingNumber + '</a>'
	    END
	    
	    INSERT INTO tblTicketsNotes
	      (
	        TicketID,
	        [Subject],
	        Notes,
	        EmployeeID
	      )
	    VALUES
	      (
	        @TicketId,
	        @URL,
	        '',
	        @PrintEmpId
	      )
	      
	      
	      -- Rab Nawaz 8899 03/17/2011 Need to change status and court date for PMC cases on LOR confirmation.
	      IF EXISTS (SELECT * FROM tblticketsviolations WHERE TicketID_PK = @TicketId AND CourtID = 3004 AND @eflag <> 0)
	      BEGIN
	      	DECLARE @repDate DATETIME
	      	SELECT @repDate = tlh.repdate FROM tblhtsbatchprintletter thl INNER JOIN tblLORBatchHistory tlh ON tlh.BatchID_FK = thl.BatchID_PK
	      	WHERE tlh.TicketID_PK = @TicketId
	      	
	      	
	      	UPDATE tblticketsviolations SET CourtDate = @repDate, CourtDateMain = @repDate, CourtDateScan = @repDate, VEmployeeID = 3992
	      	WHERE TicketID_PK = @TicketId
	      	
	      	
	      	IF EXISTS(SELECT * FROM tblticketsviolations WHERE TicketID_PK = @TicketId  AND CourtID = 3004 AND ISNULL(UnderlyingBondFlag,0) =0 AND CourtViolationStatusIDmain = 104)
	      	BEGIN
	      		
	      		UPDATE tblticketsviolations SET CourtViolationStatusID = 101, CourtViolationStatusIDmain = 101, CourtViolationStatusIDScan = 101, VEmployeeID = 3992 WHERE TicketID_PK = @TicketId
	      		
	      		--Send Trail Notification Letter into Batch Print...
				 DECLARE @currDate DATETIME
				 SET @currDate = GETDATE() 
				 EXEC USP_HTS_Insert_BatchPrintLetter @ticketId,@currDate,'1/1/1900',0,2,3992,''	      		
	      	
	      	END    	
	      END 
	      -- End 8899  
	END
GO

GRANT EXEC ON [Usp_Hts_Edelivery_Update] TO dbr_webuser
GO 

  
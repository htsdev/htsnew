SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UPDATE_ID_CAUSENUMBER]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UPDATE_ID_CAUSENUMBER]
GO







CREATE  PROCEDURE USP_HTS_UPDATE_ID_CAUSENUMBER
@ViolationticketId	int,
@CauseNumber	varchar(20),
@empId	int,
@ticketId int

AS

UPDATE    tblTicketsViolations
SET              casenumassignedbycourt = @CauseNumber
WHERE    (TicketsViolationID = @ViolationticketId)


exec sp_Add_Notes @ticketId , 'Cause Number Updated', 'Cause Number Updated' , @empid




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


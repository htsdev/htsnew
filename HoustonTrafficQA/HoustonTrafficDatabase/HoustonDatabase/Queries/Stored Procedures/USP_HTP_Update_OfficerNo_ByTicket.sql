﻿
  
/******          
Created by:  Muhammad Nasir 6393 09/15/2009  
Business Logic : this procedure is used to update the officer information if given officer information is not availble then insert it.
       
       
List of Parameter: @ticketid_pk  
		@TicketID: ticket id of client
       @Last Name : last name of client
       @First Name : first name of client
         
     
******/  
Alter PROCEDURE dbo.USP_HTP_Update_OfficerNo_ByTicket --240085,'DOUGLAS','PHIPPS'
	@TicketID INT,
	@FirstName VARCHAR(100),
	@LastName VARCHAR(100)
AS
	DECLARE @OfficerNo VARCHAR(100)    
	
	IF NOT EXISTS(
	       SELECT to1.OfficerNumber_PK
	       FROM   tblOfficer to1
	       WHERE  to1.FirstName = @FirstName
	              AND to1.LastName = @LastName
	   )
	BEGIN
	    SELECT @OfficerNo = MAX(to1.OfficerNumber_PK)+1
	    FROM   tblOfficer to1    
	    
	    INSERT INTO tblOfficer
	      (
	        OfficerNumber_PK
	       ,OfficerType
	       ,FirstName
	       ,LastName
	       ,Comments
	      )
	    VALUES
	      (
	        @OfficerNo
	       ,'N/A'/* OfficerType */
	       ,@FirstName/* FirstName */
	       ,@LastName/* LastName */
	       ,'Inserted at'+CONVERT(VARCHAR ,GETDATE() ,101) /* Comments */
	      )
	END 
	
	SELECT TOP 1 @OfficerNo = to1.OfficerNumber_PK
	FROM   tblOfficer to1
	WHERE  to1.FirstName = @FirstName
	       AND to1.LastName = @LastName
	
	
	UPDATE tblTicketsViolations
	SET    TicketOfficerNumber = @OfficerNo
	WHERE  TicketID_PK = @TicketID
     	
    --Nasir 7009 11/17/2009 insert history notes
    insert INTO tblTicketsNotes
    (   TicketID,
    	[Subject],
    	Notes,
    	Recdate,
    	EmployeeID
    )
    VALUES
    (	@TicketID
    	,'Officer Name updated to ' + @FirstName + ' ' + @LastName + ' (HMC Officer Downloader)'
    	,'Officer Name updated to ' + @FirstName + ' ' + @LastName + ' (HMC Officer Downloader)'
    	,GETDATE()
    	,3992
    )
    
    
    
    
  GO
  
  GRANT EXECUTE ON dbo.USP_HTP_Update_OfficerNo_ByTicket TO dbr_webuser
  
  GO
  
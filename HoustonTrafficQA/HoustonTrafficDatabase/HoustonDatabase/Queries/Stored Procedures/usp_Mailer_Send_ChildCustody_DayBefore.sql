/****** 
Created by:		Tahir Ahmed
Create Date:	07/29/2008
Business Logic:	The procedure is used by LMS application to get data for Harris Civil Child Custody 
				day before letter and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	ConnectionName:	Name of the connection to the case.
	CaseNumber:		case number of the child custody case.
	SettingTypeName:Type of the setting i.e. defendant or obligor etc.
	SettingDate:	settting date of the case
	Name:			Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	AddressDate:	date on which the address is associated with the case.
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	ZipCode:		Person's Home Zip code.
	DPTwo:			Address status that is used in barcode font on the letter
	DPC:			Address status that is used in barcode font on the letter
	Flag1:			Address status flag.
	Abb:			Short abbreviation of employee who is printing the letter


******/

-- usp_Mailer_Send_Harris_Civil 26, 54, 26, '5/6/2008,' , 3992, 1 ,0,0
      
alter proc [dbo].[usp_Mailer_Send_ChildCustody_DayBefore]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int            ,
@isprinted bit                     
                                    
as                                    
                                          
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
declare @sqlquery varchar(max),
	@sqlquery2 varchar(max) 
                                            
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

Select @sqlquery =''  , @sqlquery2 = ''


declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid

                                    
set @sqlquery=@sqlquery+'                                    
Select	distinct 
		ccp.ChildCustodyPartyId as recordid, 
		convert(varchar(10),c.nextsettingdate,101) as mdate,
		ccc.ConnectionName, 
		c.CaseNumber, 
		cst.SettingTypeName, 
		c.SettingDate, 
		isnull(ccp.firstname,'''')+ '' '' + isnull(ccp.middlename,'''') + '' '' + ccp.lastname as name,
		upper(ccp.Address) as address, 
        ccp.AddressDate, 
		upper(ccp.City) as city, 
		upper(tblState.State) as state, 
		ccp.ZipCode, 
		ccp.DP2 as dptwo, 
		ccp.DPC, 
        3071 as courtid,
		isnull(ccp.flag1,''N'') as flag1
		
into #temp1                                    
FROM	civil.dbo.ChildCustody AS c 
INNER JOIN
		civil.dbo.ChildCustodySettingType AS cst 
ON		c.ChildCustodySettingTypeId = cst.ChildCustodySettingTypeId 
INNER JOIN
		civil.dbo.ChildCustodyParties AS ccp 
ON		c.ChildCustodyId = ccp.ChildCustodyId 
INNER JOIN
		civil.dbo.ChildCustodyConnection ccc
ON		ccp.ChildCustodyConnectionID = ccc.ChildCustodyConnectionID 
INNER JOIN
        tblState ON ccp.StateId = tblState.StateID
WHERE   ccc.CanBeMailed = 1

-- tahir 4450 07/22/2008 stop sending letters to attorneys
and		charindex(''attorney'', ccp.name) = 0  

and		ccp.flag1 in (''y'',''d'',''s'')
and		datediff(day, c.settingdate, getdate()) < 0
and		isnull(ccp.donotmailflag,0) = 0
'                                      

                                                            
set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'c.nextsettingdate' ) 

-- removing the duplicate addressses
set @sqlquery =@sqlquery+ ' 
select distinct childcustodypartyid as recordid, isnull(p.address,'''')+ isnull(p.city,'''')+ convert(varchar,isnull(p.stateid,45)) + isnull(p.zipcode,'''') as address
into #addresses
from civil.dbo.childcustodyparties p inner join #temp1 a on a.recordid = p.childcustodypartyid

select address, min(recordid) recordid into #exists from #addresses group by address

delete from #temp1 where recordid not in (select recordid from #exists)
'

set @sqlquery =@sqlquery+ ' 
select distinct	n.recordid into #printed from tblletternotes n inner join civil.dbo.ChildCustodyParties ccp on ccp.childcustodypartyid = n.recordid
where 	lettertype ='+convert(varchar(10),@lettertype)+'  

delete from #temp1 where recordid in (select recordid from #printed)

-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624
'

      
if( @printtype<>0)         
set @sqlquery2 =@sqlquery2 +                                    
'
	Declare @ListdateVal DateTime,                                  
		@totalrecs int,                                
		@count int,                                
		@p_EachLetter money,                              
		@recordid varchar(50),                              
		@zipcode   varchar(12),                              
		@maxbatch int  ,
		@lCourtId int  ,
		@dptwo varchar(10),
		@dpc varchar(10)
	declare @tempBatchIDs table (batchid int)
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(*) from #temp1                                

	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                
                              
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			( '+convert(varchar(10),@empid)+' , '+convert(varchar(10),@lettertype)+' , @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                   

			Select @maxbatch=Max(BatchId) from tblBatchLetter   
				insert into @tempBatchIDs select @maxbatch                                                    
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            

	close ListdateCur  
	deallocate ListdateCur 

	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid, t.ConnectionName, 
		t.CaseNumber, t.settingTypeName, t.settingDate, t.Name, t.Address, t.AddressDate, t.City, t.State, t.ZipCode, 
		t.dptwo, t.dPC, t.flag1   , '''+@user+''' as Abb
		--Yasir Kamal 6648 10/07/2009 display IMB Barcode
		--Sabir Khan 6912 11/05/2009 IMBImage column has been removed as using new solution for IMB...
		--master.dbo.fn_getIMBImage(''00040001120''+ RIGHT(REPLICATE(''0'',9) + CAST(n.noteid AS VARCHAR(9)),9)  ,replace(n.zipcode, ''-'','''') + n.dp2) as                                
	from #temp1 t , tblletternotes n, @tempBatchIDs tb
	where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
	 order by [recordid]

declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500), @str varchar(2000)
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
select @subject = convert(varchar(20), @lettercount) + '' LMS Harris Civil Child Custody Day Before Letters Printed''
select @body = ''TOTAL ''+ convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'

else
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid, ConnectionName, 
		CaseNumber, 
		SettingTypeName, 
		settingDate, 
		Name, 
		Address, 
        AddressDate, 
		City, 
		State, 
		ZipCode, 
		dptwo, 
		dPC, 
		flag1  , '''+@user+''' as Abb
		--Yasir Kamal 6648 10/07/2009 display IMB Barcode
		--Sabir Khan 6912 11/05/2009 IMBImage column has been removed as using new solution for IMB...
		--master.dbo.fn_getIMBImage(''00040001120000000000'' ,replace(zipcode, ''-'','''')+isnull(dptwo,'''')) as IMBImage                                 
	 from #temp1 
	 order by recordid 
 '

set @sqlquery2 = @sqlquery2 + '
drop table #temp1'
                                    
--print @sqlquery + @sqlquery2
set @sqlquery = @sqlquery + @sqlquery2
--exec sp_executesql @sqlquery, @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @empid, @printtype, @searchtype
exec (@sqlquery)


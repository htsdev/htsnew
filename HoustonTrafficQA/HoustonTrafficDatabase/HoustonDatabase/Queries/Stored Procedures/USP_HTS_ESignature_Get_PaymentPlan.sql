set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*
Created By     : Adil Aleem.
Created Date   : N/A  
Business Logic : This procedure retrives all the payment schedules of client by Ticket ID.  
           
Parameter:       
	@TicketID   : identifiable key  TicketId      
*/
-- USP_HTS_Get_New_SchedulePaymentDetail 4344

ALTER procedure [dbo].[USP_HTS_ESignature_Get_PaymentPlan] 
@TicketID as int 
as 
-- Getting payment plan detail against given ticket ID.
SELECT     ts.ScheduleID, ts.PaymentDate, CONVERT(varchar(20), ts.Amount) AS Amount, DATEDIFF(day, GETDATE(), ts.PaymentDate) AS daysleft,
tt.LanguageSpeak -- Adil 5987 06/11/2009 include client language for English/Spanish translation display
from tblschedulepayment ts
INNER JOIN tblTickets tt
ON ts.TicketID_PK = tt.TicketID_PK
WHERE     (ts.TicketID_PK = @TicketID) AND (ts.ScheduleFlag <> 0)
ORDER BY ts.PaymentDate



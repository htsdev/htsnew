    
      
      
ALTER procedure [dbo].[sp_upload_outside_courts_new_3]      
      
as      
      
      
--select * from tblOutsideCourtstemp      
      
delete from tblOutsideCourtstemp where isnumeric(substring(caseno,3,2)) = 0      
      
declare @state varchar(2)      
select distinct VIOLATION as violations into #temp       
from tblOutsideCourtstemp      
      
      
insert into dbo.tblviolations(description,sequenceorder,IndexKey,IndexKeys,ChargeAmount,BondAmount,      
Violationtype,ChargeonYesflag,ShortDesc,ViolationCode,AdditionalPrice)      
select violations,null,2,100,100,140,3,1,'None','None',0 from #temp       
where violations not in (      
select description from dbo.tblviolations where violationtype = 3) and courtloc = 3013       
      
      
insert into tblcourtviolationstatus(description,violationcategory,sortorder)      
select distinct status,1,0  from tblOutsidecourtstemp      
where status not in (select description from tblcourtviolationstatus)      
and len(isnull(status,'')) > 0      
      
--------------------------------------------------------------------------      
-- ADDED BY TAHIR AHMED DT:7/9/07      
-- UPDATE CASE STATUS IN NON-CLIENTS FOR EXISTING TICKETS      
-- NEED TO ONLY UPDATE CASES THAT HAVE CHANGED CASES STATUSES.....      
--------------------------------------------------------------------------      
update  v      
set  v.violationstatusid  = s.courtviolationstatusid,      
  v.loaderupdatedate = getdate(),      
  v.updationloaderid = 1      
from tblticketsviolationsarchive v, tblticketsarchive t,      
  tblcourtviolationstatus s, tbloutsidecourtstemp o,      
  tblcourts c      
where v.recordid = t.recordid      
and  t.courtid = c.courtid      
and  c.courtcategorynum = 2      
and  s.description = o.status      
and  v.ticketnumber_pk = o.caseno      
and  substring(o.caseno,3,2) = c.precinctid      
and  len(isnull(o.status,'')) > 0       
and  s.violationcategory = 1      
and  v.violationstatusid <> s.courtviolationstatusid      
and  (v.loaderupdatedate is null or datediff(day, v.loaderupdatedate, getdate()) > 0   )      
      
--------------------------------------------------------------------------      
      
      
--      
--select * from #temp2      
--select * from tblcourts      
--drop table #temp2      
      
select firstname,lastname,ADD1,ADD2,T.CITY,isnull(T.STATE,'TX') as state,T.ZIP,T.PHONE,CASENO,Status,ENTRYDATE,VIOLATION,outsidemisc1,outsidemisc2,DP2,DPC,ERRSTAT,DPV,listdate,Officernumber,MidNumber,courtid      
 into #temp2       
from tblOutsideCourtstemp T, tblcourts C      
where substring(T.caseno,3,2) = precinctid      
and CASENO not in (      
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive)      
      
--select * from dallasdata      
--select * from dbo.tblTicketsArchive 
--drop table  #duplicatecases    
select distinct CASENO into #duplicatecases      
from #temp2,dbo.tblstate S      
where #temp2.state = S.state      
and caseno not in (      
select distinct TicketNumber from dbo.tblTicketsArchive )      
group by caseno      
having count( CASENO) > 1      
      
INSERT INTO dbo.tblTicketsArchive(TicketNumber,  FirstName, LastName,       
Initial, Address1, Address2,City, StateID_FK,  ZipCode,PhoneNumber,violationdate, listdate      
,DP2 ,DPC,flag1,courtid,officerNumber_Fk,officername,midnumber,outsidemisc1,outsidemisc2)      
      
select distinct CASENO as ticketnumber,firstname,lastname,'' as initial,      
ADD1 as address1 , ADD2 as address2,      
 CITY as city,      
stateid,ZIP as zip,phone ,      
dbo.convertstringtodate(entrydate),listdate,dp2,dpc,DPV,#temp2.courtid as courtid,      
962528,'N/A',midnumber,outsidemisc1,outsidemisc2      
from #temp2,dbo.tblstate S      
where #temp2.state = S.state      
and caseno not in (      
select distinct TicketNumber from dbo.tblTicketsArchive )      
and caseno not in (      
select distinct caseno from #duplicatecases)      
--select * from #temp2      
      
      
DECLARE @temp_ticketnumber varchar(20),      
@temp_violationnmber int,      
@temp_fineamount money,      
@temp_violation varchar(100),      
@tenp_violationcode varchar(10),      
@temp_status int,      
@temp_courtdate datetime,      
@temp_courtnum varchar(10),      
@temp_courtid int,      
@temp_bondamount money,      
@temp_violationdate datetime,      
@temp_officernum varchar(20)      
      
--select * from #temp2      
--select * from #tempticketsviolationsarchive       
  

--drop table #CountCourtWise 
declare @Count  int  
set @Count=0  
---------------------------------For Court Wise Count------------------------------
select distinct CASENO as ticketnumber,violationnumber_pk,chargeamount as fineamount,      
VIOLATION  as description,violationnumber_pk as violationcode,      
isnull(CourtViolationStatusID,32) as Courtviolationstatusid,'01/01/1900' as courtdate,O.courtid as  courtid 
into #CountCourtWise     
from #temp2 O inner join dbo.tblviolations V on ltrim(O.violation) = V.description and v.violationtype = 3      
left outer join  tblcourtviolationstatus C  on O.status = C.description    
where   caseno not in (      
 select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive     
 union    
 select distinct caseno from #duplicatecases    
 )  


declare @Count_new table(
id int identity(1,1),
courtid	int,
courtname	varchar(50),
reccount int
)

insert into @Count_new(courtid,courtname,reccount)
select v.courtid,c.shortname,count(v.courtid) as recordcount 
from #CountCourtWise v inner join tblcourts c
on c.courtid=v.courtid
group by v.courtid,c.shortname 

declare @rec int
declare @idx int
set @idx=1
select @rec=max(id) from @Count_new

declare @tab varchar(max)
set @tab='<table border="1" cellpadding="0" cellspacing="0" bordercolor="black" style="border-collapse: collapse"><tr><td>Court</td><td>Record Uploaded</td></tr>'
while @idx<=@rec
begin
	select @tab=@tab+'<tr><td>'+convert(varchar,courtname)+'</td><td>'+convert(varchar,reccount)+'</td></tr>' from @Count_new where id=@idx
	set @idx = @idx + 1
end

set @tab=@tab+'</table>'
-----------------------------------------------------------------------------------      
DECLARE mycursor CURSOR FOR       
select distinct CASENO as ticketnumber,violationnumber_pk,chargeamount as fineamount,      
VIOLATION  as description,violationnumber_pk as violationcode,      
isnull(CourtViolationStatusID,32),'01/01/1900',O.courtid as  courtid      
from #temp2 O inner join dbo.tblviolations V on ltrim(O.violation) = V.description and v.violationtype = 3      
left outer join  tblcourtviolationstatus C  on O.status = C.description    
where   caseno not in (      
 select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive     
 union    
 select distinct caseno from #duplicatecases    
 )      
      


      
OPEN mycursor       
      
FETCH NEXT FROM mycursor       
INTO       
@temp_ticketnumber ,      
@temp_violationnmber ,      
@temp_fineamount ,      
@temp_violation ,      
@tenp_violationcode ,      
@temp_status ,      
@temp_courtdate,      
@temp_courtid       
      
WHILE @@FETCH_STATUS = 0       
BEGIN       
      
      
INSERT INTO dbo.tblticketsViolationsArchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,ViolationDescription,ViolationCode,      
 violationstatusid,courtdate,Recordid,courtlocation, causenumber)      
select @temp_ticketnumber ,      
@temp_violationnmber ,      
@temp_fineamount ,      
@temp_violation ,      
@tenp_violationcode ,      
@temp_status ,      
@temp_courtdate ,0 ,@temp_courtid, @temp_ticketnumber      
      
set @Count=@Count+1  
      
FETCH NEXT FROM mycursor       
INTO       
@temp_ticketnumber ,      
@temp_violationnmber ,      
@temp_fineamount ,      
@temp_violation ,      
@tenp_violationcode ,      
@temp_status ,      
@temp_courtdate,@temp_courtid      
      
END       
      
CLOSE mycursor       
DEALLOCATE mycursor       
      
      
      
      
/*      
INSERT INTO dbo.tblticketsViolationsArchive(TicketNumber_PK, ViolationNumber_PK, FineAmount,ViolationDescription,ViolationCode,violationstatusid,courtdate)      
select distinct CASENO as ticketnumber,violationnumber_pk,chargeamount as fineamount,      
VIOLATION  as description,violationnumber_pk as violationcode,      
CourtViolationStatusID,'01/01/1900'       
from #temp2 O, dbo.tblviolations V, tblcourtviolationstatus C      
where ltrim(O.violation) = V.description      
and O.status = C.description      
and caseno not in (      
select distinct TicketNumber_pk from dbo.tblTicketsviolationsArchive )      
and violationtype = 3      
and caseno not in (      
select distinct caseno from #duplicatecases)      
*/      
      
drop table #temp      
drop table #temp2      
drop table #duplicatecases      
      
--drop table #temp4      
--select * from tblticketsviolationsarchive where ticketnumber_pk in       
--(select left(ltrim(ticketnumber),12) from dbo.tblOutsideCourtstemp where ticketnumber = 'BC21C0023953' )      
      
--update tblOutsideCourtstemp set correctaddrflag = 'Y'      
--select * from tblOutsideCourtstemp      
      
--delete from dbo.tblticketsarchive where ticketnumber in (select caseno from dbo.tblOutsideCourtstemp)      
--delete from dbo.tblticketsviolationsarchive where ticketnumber_pk in (select caseno from dbo.tblOutsideCourtstemp)      
      
      
/*      
select violationnumber_pk,* from tbloutsidecourtstemp T,tblviolations V      
where T.col009 = description      
and violationtype = 3      
*/      
      
    
-----------------------------------------------------------------------------------------------------------------------      
-- UPDATE DO NOT MAIL FLAG FOR NEWLY INSERTED RECORDS..      
-- IF PERSON'S NAME & ADDRESS MARKED AS DO NOT MAIL...      
-----------------------------------------------------------------------------------------------------------------------      
declare @Courtids varchar(8000)    
set @Courtids=''    
select @Courtids=@Courtids+convert(varchar,courtid)+',' from tblcourts    
where courtcategorynum=2    
    
declare @CurrDate datetime    
set @CurrDate=getdate()    
EXEC USP_HTS_UPDATE_NOMAILFLAG @CurrDate, @Courtids   
  
-- SENDING UPLOADED NO OF RECORDS THROUGH EMAIL   
  
EXEC usp_sendemailafterloaderexecutes  @Count,1,'harris county jp',@tab

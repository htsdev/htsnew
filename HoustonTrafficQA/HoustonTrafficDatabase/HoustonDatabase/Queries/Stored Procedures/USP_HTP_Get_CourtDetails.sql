set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


--USP_HTP_Get_CourtDetails 3016
create PROCEDURE [dbo].[USP_HTP_Get_CourtDetails]    
@CourtID int    
as    
SELECT     tblCourts.CourtName, tblCourts.Address+ ', ' + tblCourts.City + ', ' + tblState.State + ', ' +  tblCourts.Zip as Address, tblCourts.Phone   
FROM         tblCourts INNER JOIN    
                      tblState ON tblCourts.State = tblState.StateID    
WHERE tblCourts.CourtID = @CourtID



go

GRANT EXECUTE ON [dbo].[USP_HTP_Get_CourtDetails] TO [dbr_webuser]


go



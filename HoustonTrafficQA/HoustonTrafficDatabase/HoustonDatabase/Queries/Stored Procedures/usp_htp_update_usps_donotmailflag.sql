﻿

 /***
* Waqas 5932 06/08/2009
* Business Logic :  This procedure is used to update donotmail flag for the case in non client database.
* 
* Parameter : LetterID
* 
***/

CREATE PROCEDURE dbo.[usp_htp_update_usps_donotmailflag]
	@LetterID INT
AS
BEGIN
    DECLARE @DBiD INT -- 1-TrafficTickets, 2-DallasTrafficTickets, 3-JIMs  
    DECLARE @Recordid INT   
    
    DECLARE @DataSet TABLE(
                Sno INT,
                dbid INT,
                recordid INT,
                casetype VARCHAR(50),
                ticketnumber VARCHAR(20),
                FirstName VARCHAR(20),
                lastname VARCHAR(20),
                Address1 VARCHAR(50),
                city VARCHAR(20),
                STATE VARCHAR(15),
                ZipCode VARCHAR(12),
                nomailflag BIT
            )
    
    INSERT INTO @DataSet
    EXEC usp_hts_CheckAll_NOMAILFLAG_ChkByLetterID @LetterID
    
    
    SELECT @Recordid = recordid,
           @dbid = dbid
    FROM   @DataSet
    
    EXEC USP_HTP_Update_SetNoMailFlag @DBId,
         @RecordId
END
GO

GRANT EXECUTE ON dbo.[usp_htp_update_usps_donotmailflag] TO dbr_webuser
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

ALTER procedure [dbo].[sp_resend_text_message]
	(
	@MsgId	int
	)

as

declare	@adminemail	varchar(100),
	@email		varchar(100),
	@emailalias	varchar(100),
	@mailserver	varchar(100),
	@serverport	int,
	@rc		int,
	@body		varchar(100),
	@subject	varchar(100),
	@client		varchar(100)

select	@email = s.email,
	@emailalias = s.name,
	@client  = e.client,
	@subject = e.subject,
	@body = e.sentinfo,
	@mailserver ='mail.legalhouston.com',
	@serverport=25,
	@adminemail = 'gsullo@legalhouston.com'
from	tblemailhistory e,
	emailsettings s
where 	e.emailto = s.name
and	e.id= @msgid

insert	into tblemailhistory (emailto, client, subject, sentinfo)  
select	@emailalias, @client, @subject, @body  

--	Commented by Adil 18-Jun-08 Task 4263: because xp_smtp_sendmail sp is not suitable for SQL 2005
--exec @rc = master.dbo.xp_smtp_sendmail  
--	@FROM       = @adminemail,  
--	@FROM_NAME  = @emailalias,  
--	@TO         = @email,  
--	@server     = @mailserver,  
--	@subject    = @subject,  
--	@message    = @body,  
--	@port       = @serverport  

exec @rc = msdb.dbo.sp_send_dbmail
     @profile_name = 'admin',
     @recipients =  @email,
     @subject    =  @subject,
	 @body    =		@body,
	 @body_format = 'HTML';  

   select RC = @rc   
   if @rc <> 0  
      begin  
         update tblemailhistory set sentflag = @rc where id = @msgid  
      end  


 
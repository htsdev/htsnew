/****** 
Created by:		Tahir Ahmed
Business Logic:	-The procedure is used by LMS application to get data for Dallas County JP Appearance Day Of letter
				and to create LMS history for this letter for the selected date range.
				
				-Do not send this mailer if already sent, or DCJP Appearance letter sent in past 1 week. --Yasir Kamal 5593 03/04/2009 
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@BondFlag:		Flag if need to print bond cases as well.
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information for the logged in user.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@Language:		Flag if need to print single side (English) or double sided (English & Spanish) letters.
	@isPrinted:		Flag to include already printed letters in the batch.
		
List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	Court Name:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	CourtDate:		Court date associated with the case.
	Abb:			Short abbreviation of employee who is printing the letter

******/  

 --  usp_Mailer_Send_DallasJP_App_DayOf 11, 0, 11, '3/26/2009,' , 3991, 0, 0, 0    
ALTER procedure [dbo].[usp_Mailer_Send_DallasJP_App_DayOf]                               
 (    
 @catnum  int=6,                                        
 @bondFlag  int=0,                                        
 @courtid  int=6,                                        
 @startListdate  varchar (500) = '03/24/2006,' ,                                        
 @empid   int=3991,                                  
 @printtype  int =0 ,    
 @language int = 0  ,  -- by default print  English letters   ,    
 @isprinted bit
 )    
as          
                              
-- DECLARING LOCAL VARIABLES FOR FILTERS...                                              
declare @officernum varchar(50),                                        
 @officeropr varchar(50),                                        
 @tikcetnumberopr varchar(50),                                        
 @ticket_no varchar(50),                                                                        
 @zipcode varchar(50),                                                 
 @zipcodeopr varchar(50),                                        
 @fineamount money,                                                
 @fineamountOpr varchar(50) ,                                                
 @fineamountRelop varchar(50),                       
 @singleviolation money,                                                
 @singlevoilationOpr varchar(50) ,                                                
 @singleviolationrelop varchar(50),                                        
 @doubleviolation money,                                                
 @doublevoilationOpr varchar(50) ,                                                
 @doubleviolationrelop varchar(50),                                      
 @count_Letters int,                      
 @violadesc varchar(500),                      
 @violaop varchar(50),    
 @LetterType int,    
 @lettername varchar(50)    
    
-- SETTING LETTER TYPE AND LETTER NAME..      
 set @lettertype = 38    
 set @lettername = 'DALLAS JP Appearance Day Of'    
    
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...                                        
declare @sqlquery varchar(max), @sqlquery2 varchar(max), @sqlquery3 varchar(max)                                    
Select @sqlquery ='', @sqlquery2 =''  , @sqlquery3 = ''                                                

-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....                                                
Select @officernum=officernum,                                        
 @officeropr=oficernumOpr,                                        
 @tikcetnumberopr=tikcetnumberopr,                                        
 @ticket_no=ticket_no,                                                                          
 @zipcode=zipcode,                                        
 @zipcodeopr=zipcodeLikeopr,                                        
 @singleviolation =singleviolation,                                        
 @singlevoilationOpr =singlevoilationOpr,                                        
 @singleviolationrelop =singleviolationrelop,                                        
 @doubleviolation = doubleviolation,                                        
 @doublevoilationOpr=doubleviolationOpr,                                                
 @doubleviolationrelop=doubleviolationrelop,                      
 @violadesc=violationdescription,                      
 @violaop=violationdescriptionOpr ,                  
 @fineamount=fineamount ,                                                
 @fineamountOpr=fineamountOpr ,                                                
 @fineamountRelop=fineamountRelop                  
from  tblmailer_letters_to_sendfilters                                        
where  courtcategorynum=@catnum      
and lettertype = @lettertype                                
                                     
-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)    
select @user = upper(abbreviation) from tblusers where employeeid = @empid    
                     
-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'  
declare @recCount int                                          

Select distinct tva.recordid,         
--mdate = convert(datetime,convert(varchar(10),ta.recloaddate,101)),  
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date. 
recloaddate = case when datediff(day, tva.courtdate, getdate()) in (0,1,2) then convert(datetime, convert(varchar(10),tva.courtdate,101)) else convert(datetime, convert(varchar(10),ta.recloaddate,101)) end,
mdate = convert(datetime,convert(varchar(10),tva.TicketViolationDate,101)),  
count(tva.ViolationNumber_PK) as ViolCount,        
isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount
into #temprary                                                  
FROM    dallastraffictickets.dbo.tblTicketsViolationsArchive tva         
inner join         
dallastraffictickets.dbo.tblTicketsArchive ta         
on  tva.recordid = ta.recordid        
inner join  dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid      
INNER JOIN      
tblstate S ON      
ta.stateid_fk = S.stateID        

-- ONLY VERIFIED AND VALID ADDRESSES
where Flag1 in (''Y'',''D'',''S'')  

-- NOT MARKED AS STOP MAILING...
and  isnull(donotmailflag,0)  =0         

--TAHIR 4374 07/09/2008 EXLCUDE DIFFICULT JP COURT CASES..
--Yasir Kamal 6520 09/03/2009 Include difficult JP court cases.
--and tva.courtlocation not in (3085,3086,3088,3090)


-- tahir 3796 4/18/08
-- ONLY CONST & SHERIFF COMPLAINANT AGENCIES....
and
-- Abbas Shahid Khwaja 9296 05/18/2011 Screen out all cases with Offense Date more than 1 year ago and also having DISD in complainant agency
--(charindex(''CONST'', isnull(tva.complainant_agency,'''')) > 0   or charindex(''SHERIFF'', isnull(tva.complainant_agency,'''')) > 0  )
charindex(''DISD'', isnull(tva.complainant_agency,'''')) = 0 
and [dbo].[fn_CalculateOffenseDate](tva.ticketviolationdate,getdate()) <= 0  

-- Haris Ahmed 10412 09/05/2012 Screen out all cases having DCHD in complainant agency
and charindex(''DCHD'', isnull(tva.complainant_agency,'''')) = 0  

-- ozair 3742 on 04/16/2008 
-- EXCLUDE FAILURE TO PAY TOLL VIOLATIONS.....
and charindex(''Failure to Pay toll'', isnull(tva.violationdescription,'''')) = 0
'
--Ozair 6543 09/29/2009 Check Implemented for Dallas DCJP Scarpping Service no need to exclude future date.
if (@startListdate<>'-1')
	set @sqlquery=@sqlquery+ '
	-- ONLY TODAY OR PAST COURT DATES....
	and datediff(day,tva.courtdate,getdate())>= 0
	-- end ozair 3742
	'

--Ozair 6543 09/29/2009
set @sqlquery=@sqlquery+ '
-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0     
'   

-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED CATEGORY.....
if(@courtid=@catnum)    
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation In (select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = '+ convert(varchar, @catnum) +')'               

-- PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
else    
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation=' + convert(varchar,@courtid)    
                                       

--Ozair 6543 09/29/2009 Check Implemented for Dallas DCJP Scarpping Service for Given Court Date
if (@startListdate='-1')
begin
	--Waqas 6731 10/13/2009 check for today also only those who has not found status if exists.
	set @sqlquery=@sqlquery+ '     
	and ( 
		  datediff(Day, getdate(), tva.courtdate) = 1		  
		  OR 
		  (	datediff(Day, getdate(), tva.courtdate) = 0 
			and 
			(isnull(tva.WebSiteCourtStatus, 12) = 12)				
		  )
		)
	'
end
-- ONLY FOR SELECTED DATE RANGE...                    
-- TAHIR 5593 03/24/2009  also print for today's court date.
-- Ozair 6543 09/29/2009 IsScrappingService check implemented
else if(@startListdate<>'')                                        
	set @sqlquery=@sqlquery+ '     
	and ( 
		(datediff(Day, tva.courtdate, getdate()) in (0,1,2) ) 
		-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date. 
		or  '+dbo.Get_String_Concat_With_DatePart_ver2(''+@startListdate +'', 'ta.recloaddate' )  + '
		)
	'

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....  
if(@officernum<>'')                                        
	set @sqlquery =@sqlquery +'      
	and  ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                         
                                        
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                                        
	set @sqlquery=@sqlquery+ '    
	and   ('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                              
                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                              
	set @sqlquery=@sqlquery+ '     
	and ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,2)' ,+ ''+ @zipcodeopr) +')'                 
    
-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                    
	set @sqlquery =@sqlquery+ '    
    and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
    
-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                    
	set @sqlquery =@sqlquery+ '    
    and  not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                         
                            
                                                            
set @sqlquery =@sqlquery+'  
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date.                                            
group by  case when datediff(day, tva.courtdate, getdate()) in (0,1,2) then convert(datetime, convert(varchar(10),tva.courtdate,101)) else convert(datetime, convert(varchar(10),ta.recloaddate,101)) end, tva.recordid, convert(datetime, convert(varchar(10),tva.TicketViolationDate,101))
having 1=1     
'          

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT        
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '  
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
      '                                  

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + '  
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )  
      '                                                      
         
-- SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                         
if(@doubleviolation<>0 and @singleviolation<>0)                                  
	set @sqlquery =@sqlquery+ '  
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))  
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))  
        '   

                                 

-- EXCLUDE LETTERS THAT HAVE BEEN PRINTED ALREADY...         
-- APPEARANCE OR DAY OF LETTER
set @sqlquery =@sqlquery + '                                                  
-- TAHIR 4374 07/09/2008     
--select recordid into #tempvar from tblletternotes where  lettertype IN (38,39)
-- Yasir Kamal 5593 03/04/2009 DCJP Appearance letter sent in past 1 week
-- Babar Ahmad 8597	07/15/2011 Changed number of days from 7 to 5. Added business days function for 5 business days.
create table #tempvar (recordid int)  
insert into #tempvar select recordid  from tblletternotes where lettertype = 38  
insert into #tempvar select recordid  from tblletternotes where lettertype = 39 and datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0 
--5593 end     

select T.recordid into #tempvarexist from #temprary T , #tempvar V  
where T.recordid = V.recordid  

delete from #temprary where recordid in (select recordid from #tempvarexist)  
   
'    
  
-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...  
set @sqlquery=@sqlquery+'  

-- Rab Nawaz Khan 9102 4/14/2011 added due to changes in DTP mailers, Now DTP mailer displays ViolationDate insetd of CourtDate on App Day Of Mailers
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date.  
SELECT recordid, MAX(mdate) AS mdate, recloaddate, SUM(violCount) AS violcount, SUM(total_fineamount) AS total_fineamount
INTO #temptable
FROM #temprary 
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date. 
GROUP BY recordid,recloaddate
-- End 9102
                                   
Select distinct a.recordid,     
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date. 
 a.mdate ,a.recloaddate, a.mdate as courtdate, ta.courtid,  flag1 = isnull(ta.Flag1,''N'') , ta.officerNumber_Fk,     
 isnull(tva.causenumber, tva.ticketnumber_pk) as TicketNumber_PK, isnull(ta.donotmailflag,0) as donotmailflag,    
 ta.clientflag,  upper(firstname) as firstname, upper(lastname) as lastname, upper(address1) + '''' + isnull(ta.address2,'''') as address,                          
 upper(ta.city) as city, s.state,    
 tc.CourtName, zipcode,  midnumber,  dp2 as dptwo, dpc, violationstatusid,     
 left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,    
 ViolationDescription, case when isnull(tva.fineamount,0)=0 then 100 else convert(numeric(10,0),tva.fineamount) end as FineAmount,   
 a.ViolCount,    
 a.Total_Fineamount
into #temp                                              
FROM    dallastraffictickets.dbo.tblTicketsViolationsArchive tva     
inner join     
 dallastraffictickets.dbo.tblTicketsArchive ta     
on  tva.recordid = ta.recordid    
inner join  dallastraffictickets.dbo.tblcourts tc  on  ta.courtid = tc.courtid    
INNER JOIN    
tblstate S ON    
ta.stateid_fk = S.stateID    
inner join #temptable a on a.recordid  = ta.recordid where 1=1

-- Abbas Shahid Khwaja 9296 05/18/2011 Screen out all cases with Offense Date more than 1 year ago and also having DISD in complainant agency
--(charindex(''CONST'', isnull(tva.complainant_agency,'''')) > 0   or charindex(''SHERIFF'', isnull(tva.complainant_agency,'''')) > 0  )
and charindex(''DISD'', isnull(tva.complainant_agency,'''')) = 0 
and [dbo].[fn_CalculateOffenseDate](tva.ticketviolationdate,getdate()) <= 0 
-- Haris Ahmed 10412 09/05/2012 Screen out all cases having DCHD in complainant agency
and charindex(''DCHD'', isnull(tva.complainant_agency,'''')) = 0  
-- Rab Nawaz Khan 9315 06/30/ 2011 Added for separately generating mailers of court date and record load date. 
and '+dbo.Get_String_Concat_With_DatePart_ver2(''+@startListdate +'', 'a.recloaddate' )  + '
'  
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')  
begin  
	-- NOT LIKE FILTER.......  
	if(charindex('not',@violaop)<> 0 )                
		set @sqlquery=@sqlquery+'   
		and  not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
	  
	-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	-- LIKE FILTER.......
	if(charindex('not',@violaop) =  0 )                
		set @sqlquery=@sqlquery+'   
		and  ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'             
end  
                         
-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
set @sqlquery=@sqlquery+                         
'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount, mdate, FirstName,LastName,address,                                  
city,state,FineAmount + 50 as fineamount, violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK, 
--Yasir Kamal 7226 01/18/2010 get promotional price                                        
courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, courtdate,dbo.GetPromoPriceTemplate(zipcode,'+convert(Varchar,@lettertype)+') AS promotemplate  into #temp1  from #temp                                        
where len(isnull(firstname,'''')+ isnull(lastname,'''') ) > 0 
and charindex(''law office'', isnull(lastname,'''')+isnull(firstname,'''') ) = 0  
and charindex(''law firm'', isnull(lastname,'''')+isnull(firstname,'''') ) = 0  
and charindex(''attorney'', isnull(lastname,'''')+isnull(firstname,'''') ) = 0   

 '                  
 
--Sabir Khan 6784 10/16/2009  
--EXCULDE CASES HAVING ANY OF THE FOLLOWING STATUS....
set @sqlquery=@sqlquery+                                         
'delete from #temp1 
from #temp1 a inner join dallastraffictickets.dbo.tblticketsviolationsarchive tva on tva.recordid = a.recordid
inner join dallastraffictickets.dbo.WebSiteCourtStatus w on w.id  = tva.WebSiteCourtStatus
-- Muneer Sheikh 8302 09/06/2010 "DISPOSED CASE", "FULL OR PARTIAL PREPAYMENT", "UNDER 17 YEARS OF AGE", "ZERO FINE AMOUNT" Excluded from mailing.
 where w.MailAllowed = 1 AND w.WebSite=''DCJP''
--Sabir Khan 6947 11/06/2009 Removed warrant check....
--or charindex(''Warrant'', w.CourtStatus) > 0
' 

--Sabir Khan 6898 10/30/2009 EXCLUDE CLIENT CASES...
set @sqlquery=@sqlquery +'  
delete from #temp1 where recordid in (
select v.recordid from dallastraffictickets.dbo.tblticketsviolations v inner join dallastraffictickets.dbo.tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)

-- Rab Nawaz Khan 8754 01/12/2011 Exclude Disposed Violations
	Delete from #temp1
	where #temp1.violationstatusid = 80
-- END 8754
  

'   
-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype = 1 )   
	begin          
		set @sqlquery2 =@sqlquery2 +                                        
		'    
		Declare @ListdateVal DateTime, @totalrecs int,@count int, @p_EachLetter money,@recordid varchar(50),                                  
		@zipcode   varchar(12),@maxbatch int  ,@lCourtId int  ,@dptwo varchar(10),@dpc varchar(10)    
		declare @tempBatchIDs table (batchid int)    

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                     
		Select @count=Count(*) from #temp1                                    

		set @p_EachLetter=convert(money,0.50)                                    

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                      
		Select distinct mdate  from #temp1                      
		open ListdateCur                                       
		Fetch Next from ListdateCur into @ListdateVal                                                          
		while (@@Fetch_Status=0)                                  
			begin                                    

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE ......
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                     

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                      
				('+ convert(varchar(10),@empid) +', '+ convert(varchar(10),@lettertype) +' , @ListdateVal, '+ convert(varchar(10),@catnum) +', 0, @totalrecs, @p_EachLetter)                                       

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter     
				insert into @tempBatchIDs select @maxbatch                                                           

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                                   
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                                   
				open RecordidCur                                                             
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                              
				while(@@Fetch_Status=0)                                  
					begin                                  

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...					
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                                  
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+ convert(varchar(10),@lettertype) +',@lCourtId, @dptwo, @dpc)                                  
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                              
					end                                  
				close RecordidCur     
				deallocate RecordidCur                                                                   
				Fetch Next from ListdateCur into @ListdateVal                                  
			end                                                

		close ListdateCur      
		deallocate ListdateCur     

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                                  
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                                  
		dptwo, TicketNumber_PK, t.zipcode  , courtdate , '''+@user+''' as Abb,promotemplate
		--Yasir Kamal 6838 10/23/2009 IMB added.
		--Sabir Khan 6912 11/11/2009 remove IMBImage field...
		--master.dbo.fn_getIMBImage(''00040001120''+ RIGHT(REPLICATE(''0'',9) + CAST(n.noteid AS VARCHAR(9)),9)  ,replace(n.zipcode, ''-'','''') + n.dp2) as IMBImage                                      
		into #temp2    
		from #temp1 t , tblletternotes n, @tempBatchIDs tb    
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+'     
		order by T.zipcode     
		select @recCount = count(distinct recordid) from #temp2     

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @user varchar(10), @sql varchar(500)    
		select @user = abbreviation from tblusers where employeeid = '+ convert(varchar(10), @empid) +'    
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid    
		select @subject = convert(varchar(20), @lettercount) + '' ''  + '''+ @lettername +' Letters Printed''    
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')    
		exec usp_mailer_send_Email @subject, @body    

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)  
		set @batchIDs_filname = ''''  
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs  

		'    
	end    
else  

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	begin  
		set @sqlquery2 = @sqlquery2 + '    
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName, LastName,address,city, state,FineAmount,
		violationdescription,CourtName, dpc,dptwo, TicketNumber_PK, zipcode , courtdate, '''+@user+''' as Abb,promotemplate
		--Yasir Kamal 6838 10/23/2009 IMB added.
		--Sabir Khan 6912 11/11/2009 remove IMBImage field...
		--master.dbo.fn_getIMBImage(''00040001120000000000'' ,replace(zipcode, ''-'','''')+isnull(dptwo,'''')) as IMBImage                                        
		into #temp3  from #temp1 order by zipcode     
		select @recCount = count(distinct recordid) from #temp3    
		 '    
	end

-- IF PRINTING SINGLE SIDED LETTERS....    
if @language = 0    
	begin    

		-- IF ONLY PREVIEWING THE LETTER....
		-- OUT PUT THE DATA FOR REPORT FILE...
		if @printtype = 0   
			begin 
				select @sqlquery3 = @sqlquery3 + '     
				select t1.*, 0 as language, convert(varchar(15), t1.recordid) + ''0'' as ChkGroup from #temp3 t1 order by t1.zipcode'    
			end

		-- IF SENDING LETTERS TO BATCH...	
		-- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..			      
		else if @printtype = 1    
			begin
				select @sqlquery3 = @sqlquery3 + '     
				select t2.*, 0 as language, convert(varchar(15), t2.recordid) + ''0'' as ChkGroup from #temp2 t2     
				order by [zipcode]   
				select isnull(@batchIDs_filname,'''') as batchid   
				'    
			end
	end    

-- FOR DOUBLE SIDED PRINTING....
else  if @language = 1  
	begin    

		-- IF ONLY PREVIEWING THE LETTER....
		if @printtype = 0    
			begin
			   select @sqlquery3 = @sqlquery3 + '     
			   declare @table4 TABLE  (    
				[letterId] [varchar] (13)  ,    
				[recordid] [int] ,    
				[FirstName] [varchar] (20)  ,    
				[LastName] [varchar] (20)  ,    
				[address] [varchar] (100) ,    
				[city] [varchar] (50) ,    
				[state] [varchar] (2)  ,    
				[FineAmount] [money]  ,    
				[violationdescription] [varchar] (200) ,    
				[CourtName] [varchar] (50) ,    
				[dpc] [varchar] (10)  ,    
				[dptwo] [varchar] (10)  ,    
				[TicketNumber_PK] [varchar] (20)  ,    
				[zipcode] [varchar] (15) ,    
				[language] [int],    
				[courtdate] [datetime],    
				[Abb] [varchar] (10),
				--Yasir Kamal 7226 01/18/2010 get promotional price
				[promotemplate] [varchar] (100)
				--Yasir Kamal 6838 10/23/2009 IMB added.
				--Sabir Khan 6912 11/11/2009 remove IMBImage field...
				--[IMBImage]  [Image]  
			   )    
			    
			   insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)      
			    
			   select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0, courtdate, Abb,promotemplate from #temp3      
			    
			   insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)      
			    
			   select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1, courtdate, Abb,promotemplate from #temp3 ;     
			    
			    
			   select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4 order by zipcode, language ;    
			'    
			end

		-- IF SENDING LETTERS TO BATCH...	
		-- OUT PUT THE DATA TO REPORT FILE WITH BATCH FILE NAME..	    
		else if @printtype = 1  
			begin    
				select @sqlquery3 = @sqlquery3 + '     
				declare @table4 TABLE  (    
				[letterId] [varchar] (13)  ,    
				[recordid] [int] ,    
				[FirstName] [varchar] (20)  ,    
				[LastName] [varchar] (20)  ,    
				[address] [varchar] (100) ,    
				[city] [varchar] (50) ,    
				[state] [varchar] (2)  ,    
				[FineAmount] [money]  ,    
				[violationdescription] [varchar] (200) ,    
				[CourtName] [varchar] (50) ,    
				[dpc] [varchar] (10)  ,    
				[dptwo] [varchar] (10)  ,    
				[TicketNumber_PK] [varchar] (20)  ,    
				[zipcode] [varchar] (15) ,    
				[language] [int],     
				[courtdate] [datetime],    
				[Abb] [varchar] (10),
				--Yasir Kamal 7226 01/18/2010 get promotional price
				[promotemplate] [varchar] (100)
				--Yasir Kamal 6838 10/23/2009 IMB added.
				--Sabir Khan 6912 11/11/2009 remove IMBImage field...
				--[IMBImage] [Image]    
				)    

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)      

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 0 , courtdate, Abb,promotemplate from #temp2      

				insert into @table4 (letterid, recordid, firstname, lastname, address, city, state, fineamount,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, language, courtdate, Abb,promotemplate)      

				select letterid, recordid, firstname, lastname, address, city, state, fineamount+60,    
				 violationdescription, courtname, dpc, dptwo, ticketnumber_pk, zipcode, 1 , courtdate, Abb,promotemplate from #temp2 ;     

				select *, convert(varchar(15), recordid) +  convert(varchar(5),language) as ChkGroup from @table4     
				order by [zipcode], [language]   
				 
				select isnull(@batchIDs_filname,'''') as batchid    
				'    
			end
	end    
                                        
--print @sqlquery + @sqlquery2 + @sqlquery3    

-- EXECUTING THE DYNAMIC SQL ....
exec  ( @sqlquery + @sqlquery2 + @sqlquery3 )    
  


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Insert_RestrictedPhoneNumbers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Insert_RestrictedPhoneNumbers]
GO


-- PROCEDURE THAT ADDS A PHONE NUMBER IN A TABLE HAVING BLOCK PHONE NUMBERS...
-- THESE NUMBERS WILL NOT BE USED TO CONTACT CLIENTS......
CREATE   procedure [dbo].[usp_MID_Insert_RestrictedPhoneNumbers]
	(
	@PhoneNo	varchar(25)
	)
As

-- REMOVING THE ACTUAL FORMATTING FROM THE CONTACT NUMBER.... 
select @phoneno = left(@phoneno,3)+substring(@phoneno,5,3)+substring(@phoneno,9,len(@phoneno))

-- CHECK IF IT ALREADY EXISTS IN THE BLOCK LIST.....
select * from tblrestrictedphonenumbers where phonenumber = @phoneno

-- IF NOT ALREADY EXISTS.. THEN INSERT......
if @@rowcount <= 0
	begin
		insert into tblRestrictedPhoneNumbers ( phonenumber) values (@phoneno)
	end








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


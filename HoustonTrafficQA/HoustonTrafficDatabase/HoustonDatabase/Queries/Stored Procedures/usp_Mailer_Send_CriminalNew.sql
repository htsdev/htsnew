/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for Harris County Criminal letter
				and to create LMS history for this letter for the selected date range.
				Filters structure is not implemented for this letters.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@empid:			Employee information of the logged in employee.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	ClientId:		Identity value to look up the case in non-clients. used to group the records in report file.
	Name:			Person's first and last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	Zip:			Zip code associated with person home address
	RecordId:		Booking Number of the case
	Span:			SPN number associated with the person
	ArrestDate:		Date when person arrested
	ChargeWording:	Description of charge on the person
	ChargeLevel:	Charge level associated with the charge wording
	Disposition:	Disposition status of the case
	BookingDate:	Booking date of the case
	CaseNumber:		Case number charged on the person
	ClientFlag:		Flag to identify if it is hired/contacted
	ListDate:		Upload date of the case
	DPTwo:			Address status that is used in barcode font on the letter
	DPC:			Address status that is used in barcode font on the letter
	ZipSpan:		First five characters of zip code and SPN number
	Abb:			Short abbreviation of employee who is printing the letter

******/

-- usp_Mailer_Send_CriminalNew 8, 26, 8, '5/19/2006,' , 3992, 0,0 ,0
      
ALTER proc [dbo].[usp_Mailer_Send_CriminalNew]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int    ,
@isprinted  bit                             
                                    
                                    
as                                    
                          
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...
declare @sqlquery varchar(max), @sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''
              
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                    
-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+'                                    
Select	distinct c.bookingnumber as recordid, 
	c.clientid, 
	isnull(l.span, c.bookingnumber) as span, 
	c.arrestdate, 
	chargewording, 
	chargelevel, 
	c.disposition, 	
	c.bookingdate, 
	c.casenumber , 
	c.clientflag,
	left(C.zip,5) + rtrim(ltrim(isnull(L.span,c.bookingnumber))) as zipspan,
	convert(varchar(10),c.recdate,101) as listdate, 
	flag1 = isnull(c.Flag1,''N'') ,
	c.donotmailflag,
	upper(c.firstname + '' ''+ c.lastname) as name,
	upper(c.address) as address,
	upper(c.city) as city,
	c.state,
	c.zip,
	dp2 as dptwo,
	dpc
into #temp                                          
FROM    jims.dbo.criminalcases c left outer join  jims.dbo.criminal503 l on l.bookingnumber = c.bookingnumber

-- EXCLUDE CERTAIN DISPOSITION STATUSES....
where  	disposition NOT in (''CABS'',''CEXP'',''COMM'',''COTH'',''CREJ'',''CREQ'',''DADJ'',''DERR'',''DISM'',''DISP'',''GILT'',''NOB'',''NOTG'',''NPCF'',''PART'',''PDFC'',''PROB'',''USTP'') 

-- ONLY VERIFIED AND VALID ADDRESSES
and 	Flag1 in (''Y'',''D'',''S'') 

-- NOT MARKED AS STOP MAILING...
and		isnull(c.donotmailflag,0) = 0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and		isnull(c.ncoa48flag,0) = 0'

-- ONLY FOR SELECTED DATE RANGE...
set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'c.recdate' ) 


-- EXCLUDE LETTERS THAT HAVE ALREADY PRINTED....
set @sqlquery =@sqlquery+ ' 
and c.bookingnumber not in (                                                                    
	select	recordid from tblletternotes 
	where 	lettertype ='+convert(varchar(10),@lettertype)+'   
	)'
                                    

-- GETTING REOCORDS IN TEMP TABLE...
set @sqlquery=@sqlquery+'

Select distinct recordid, clientid, span, arrestdate, chargewording, chargelevel, disposition, bookingdate, casenumber, clientflag,
	zipspan, listdate as mdate,Name,address,                              
 city,state,dpc,dptwo,Flag1,donotmailflag, zip as zipcode into #temp1  from #temp                                    
 '              
         

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype = 1 )         
	BEGIN
		set @sqlquery2 =@sqlquery2 +                                    
		'
			Declare @ListdateVal DateTime,                                  
				@totalrecs int,                                
				@count int,                                
				@p_EachLetter money,                              
				@recordid varchar(50),                              
				@zipcode   varchar(12),                              
				@maxbatch int  ,
				@lCourtId int  ,
				@dptwo varchar(10),
				@dpc varchar(10)
			declare @tempBatchIDs table (batchid int)

			-- GETTING TOTAL LETTERS AND COSTING ......
			Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
			Select @count=Count(*) from #temp1                                

			if @count>=500                                
				set @p_EachLetter=convert(money,0.3)                                
		                                
			if @count<500                                
				set @p_EachLetter=convert(money,0.39)                                
		                              
			-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
			-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
			Declare ListdateCur Cursor for                                  
			Select distinct mdate  from #temp1                                
			open ListdateCur                                   
			Fetch Next from ListdateCur into @ListdateVal                                                      
			while (@@Fetch_Status=0)                              
				begin                                

					-- GET TOTAL LETTERS FOR THE LIST DATE
					Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

					-- INSERTING RECORD IN BATCH TABLE......
					insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
					( '+convert(varchar(10), @empid)+'  , '+convert(varchar(10),@lettertype)+' , @ListdateVal, '+convert(varchar(10),@crtid)+' , 0, @totalrecs, @p_EachLetter)                                   


					-- GETTING BATCH ID OF THE INSERTED RECORD.....
					Select @maxbatch=Max(BatchId) from tblBatchLetter  
					insert into @tempBatchIDs select @maxbatch                                                     

					-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
					Declare RecordidCur Cursor for                               
					Select distinct recordid,zipcode, 3037, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
					open RecordidCur                                                         
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
					while(@@Fetch_Status=0)                              
					begin                              

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
					close RecordidCur 
					deallocate RecordidCur                                                               
					Fetch Next from ListdateCur into @ListdateVal                              
				end                                            

			close ListdateCur  
			deallocate ListdateCur 

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, clientid, Name, address, city, state, t.zipcode as zip , t.recordid, span, arrestdate,
		chargewording, chargelevel, disposition, bookingdate, casenumber, clientflag, t.zipspan, mdate as listdate,
		dptwo, t.dpc,  clientflag, '''+@user+''' as Abb          
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and  n.courtid = 3037 and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by [ZIPSPAN]

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
		select @subject = convert(varchar(20), @lettercount) + '' LMS Harris County Criminal Letters Printed''
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
		exec usp_mailer_send_Email @subject, @body

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'

	END
else

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
		set @sqlquery2 = @sqlquery2 + '
		Select distinct ''NOT PRINTABLE'' as letterId, clientid, Name, address, city, state, zipcode as zip , recordid, span, arrestdate,
		chargewording, chargelevel, disposition, bookingdate, casenumber, clientflag,  mdate as listdate,
		dptwo, dpc, zipspan, clientflag, '''+@user+''' as Abb
		from #temp1 
		order by [zipspan]
		'
	END

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'
                                    
--print @sqlquery + @sqlquery2

-- CONCATENATING THE DYNAMIC SQL
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)

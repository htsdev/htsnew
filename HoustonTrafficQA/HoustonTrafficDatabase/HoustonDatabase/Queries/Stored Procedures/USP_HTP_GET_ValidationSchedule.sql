/*      
* Created By  : SAEED AHMED    
* Task ID   : 7791      
* Created Date  : 05/27/2010      
* Business logic : This procedure is used to get the schedule of validation report, based on provided reportid and active flag.     
*       The schedule represent weekday & saturday timings.These are the timings when user associated with the report will receive emails.       
* Parameter List      
* @ReportID   : Represents reportID whose schedule user wants to view.     
* @isActive   : Represents report status i.e. report is currently active or not.     
*     
*/      
    
-- USP_HTP_GET_ValidationSchedule 214  
    
CREATE PROC dbo.USP_HTP_GET_ValidationSchedule     
@ReportID INT,            
@isActive BIT=1    
AS    

DECLARE @Scheduleing AS TABLE    
(    
rowid INT IDENTITY(1,1) ,    
reportid INT,    
title VARCHAR(200),    
scheduleid INT,    
timingid INT,    
isWeekday BIT,    
sequenceid INT,  
isActive bit    
)    
    
DECLARE @temp AS TABLE     
(    
ReportID INT,    
Title VARCHAR(200),    
scheduleIdWeekday1 INT default(-1),    
timingIdWeekday1 INT default(-1),    
    
scheduleIdWeekday2 INT default(-1),    
timingIdWeekday2 INT default(-1),    
    
scheduleIdWeekday3 INT default(-1),    
timingIdWeekday3 INT default(-1),    
    
scheduleIdSaturday1 INT default(-1),    
timingIdSaturday1 INT default(-1),    
    
scheduleIdSaturday2 INT default(-1),    
timingIdSaturday2 INT default(-1),    
    
scheduleIdSaturday3 INT default(-1),    
timingIdSaturday3 INT default(-1)    
)    
    
    
    
INSERT INTO @Scheduleing(reportid ,title ,scheduleid ,timingid ,isWeekday ,sequenceid,isActive )    
SELECT     tts.ID AS reportID,tts.TITLE, ISNULL(vrs.SchedulingID,-1), ISNULL(et.TimingID,-1), vrs.isWeekDay, vrs.SequenceId,vrs.isactive     
FROM         dbo.ValidationReportScheduling AS vrs RIGHT OUTER JOIN    
                      dbo.ValidationEmailTimings AS et ON et.TimingID = vrs.EmailTimingsID RIGHT OUTER JOIN    
                      dbo.tbl_TAB_SUBMENU AS tts ON tts.ID = vrs.ReportID    
WHERE                           
tts.ID=@ReportID AND tts.[active]=1    
    
    
INSERT INTO @temp(ReportID,Title)    
SELECT DISTINCT reportid,title  FROM @Scheduleing    
    
--SELECT * FROM @Scheduleing    
    
    
DECLARE @idx    INT,    
        @count  INT,    
        @scheduleid INT,    
        @timingid INT,    
        @isWeekday BIT,    
        @sequenceid INT,    
  @activeSchedule bit  
    
SELECT @idx = 1,    
       @count = COUNT(rowid)    
FROM   @Scheduleing    
    
    
WHILE @idx <= @count    
BEGIN    
 SELECT      
 @scheduleid= scheduleid ,    
    @timingid=timingid ,    
    @isWeekday=isWeekday ,    
    @sequenceid=sequenceid,  
 @activeSchedule=isActive    
 FROM @Scheduleing  WHERE rowid=@idx    
     
 UPDATE @temp SET scheduleIdWeekday1 = @scheduleid , timingIdWeekday1 = @timingid WHERE  @isWeekday=1  AND @sequenceid=1 and @activeSchedule=1  
 UPDATE @temp SET scheduleIdWeekday2 = @scheduleid , timingIdWeekday2 = @timingid WHERE   @isWeekday=1  AND @sequenceid=2 and @activeSchedule=1    
 UPDATE @temp SET scheduleIdWeekday3 = @scheduleid , timingIdWeekday3 = @timingid WHERE   @isWeekday=1  AND @sequenceid=3   and @activeSchedule=1  
     
 UPDATE @temp SET scheduleIdSaturday1 = @scheduleid, timingIdSaturday1 = @timingid WHERE   @isWeekday=0  AND @sequenceid=1   and @activeSchedule=1  
 UPDATE @temp SET scheduleIdSaturday2 = @scheduleid, timingIdSaturday2 = @timingid WHERE   @isWeekday=0  AND @sequenceid=2   and @activeSchedule=1  
 UPDATE @temp SET scheduleIdSaturday3 = @scheduleid, timingIdSaturday3 = @timingid WHERE   @isWeekday=0  AND @sequenceid=3   and @activeSchedule=1  
     
 SET @scheduleid= -1    
 SET @timingid=-1     
 set @activeSchedule=0  
        
 SET @idx = @idx + 1    
END    
    
SELECT * FROM @temp 

GO
GRANT EXECUTE ON dbo.USP_HTP_GET_ValidationSchedule TO dbr_webuser
GO

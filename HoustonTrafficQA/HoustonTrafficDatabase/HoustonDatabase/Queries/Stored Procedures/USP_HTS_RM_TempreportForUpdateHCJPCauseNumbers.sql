SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_RM_TempreportForUpdateHCJPCauseNumbers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_RM_TempreportForUpdateHCJPCauseNumbers]
GO

CREATE procedure USP_HTS_RM_TempreportForUpdateHCJPCauseNumbers  
  
as  
  
  
select   
 t.ticketid_pk,  
 tv.casenumassignedbycourt,   
 refcasenumber,   
 lastname,   
 firstName,   
 dbo.fn_DateFormat(dob, 1, '/', ':', 0) as dob,   
 dlnumber,     
dbo.fn_DateFormat(tv.courtdatemain, 1, '/', ':', 1)   as courtdatemain,  
 cvs.shortdescription,   
 c.shortname,   
 case when t.bondflag = 0 then 'B'   
 else ''  
 end  as bondflag  
from tbltickets t  
  inner join tblticketsviolations tv on t.ticketid_pk = tv.ticketid_pk   
  inner join tblcourtviolationstatus cvs on tv.courtviolationstatusidmain = cvs.courtviolationstatusid   
  inner join tblcourts c on c.courtid = tv.courtid   
  inner join tbldatetype d on d.typeid = cvs.categoryid  
where   
 tv.courtid between 3007 and 3022 and   
 (  
  (  
    tv.refcasenumber like 'CR%' or   
    tv.refcasenumber like 'TR%'  
  ) and len(tv.refcasenumber) = 12  
 )  
and tv.refcasenumber <> isnull(tv.casenumassignedbycourt,'')  
and courtviolationstatusidmain not in (80)  
and activeflag = 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


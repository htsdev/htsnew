
  
  
-- =============================================  
-- Created By:  Zeeshan Zahoor  8/05/208
-- Business Logic: This is proceedure is used to display the bond status of active cliens in the HTP System in validation reports.  
-- List of Parameters: 
--		@validation: Validation status of client.
-- List of Columns: 
--		TicketID_PK:	This field is the unique identifier from tblTickets table.
--		Lastname:		This field will display the Client's Last Name.   
--		Firstname:		This field will display the client's First Name.  
--		TicketNumber:   This will display the unique reference number of case for client
--		CauseNumber:	This will display the unique id issue by court to the case of client   
--		CRT_Location:	This will display the Short name of court location/address.    
--		Status:			This will display the bond status of case of client.   
--		followupdate:	This will display the follow up date if issued before.  
--		generalcomments:This will display the general comment on followup date assignment.  
--		courtid:		This will display the unique court id of the courts.     


-- 'N/A' will be display if court location is not available.  
-- '1/1/1900'will be display if bondfollowupdate is not avaialble  
-- =============================================  

-- [dbo].[usp_HTP_Bond_Report] 1
ALTER PROCEDURE [dbo].[usp_HTP_Bond_Report]   
@validation as INT,
@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS  
BEGIN 
--Sabir Khan 5521 02/20/2009 remove distinct... 
 SELECT   
 t.TicketID_PK,  
 t.Lastname,   
 t.Firstname,   
 v.RefCaseNumber TicketNumber,   
 v.casenumassignedbycourt CauseNumber,   
 isnull(c.ShortName,'N/A')  CRT_Location,    
 cvs.Description Status,   
 -- tahir 4710 08/28/2008 fixed the date format issue for validation email.......
 -- Noufil 5073 11/01/2008 SHow null follow update too in Validation report
 case when isnull(te.BondFollowUpdate,'01/01/1900')='01/01/1900' then ' ' else  convert(varchar(10), te.BondFollowUpdate, 101) end as followupdate,
 --Sabir Khan 5297 12/03/2008 sort by follow up date...
 te.bondfollowupdate,
 t.GeneralComments AS generalcomments,  
 v.courtid     
FROM         dbo.tblTickets AS t INNER JOIN  
                      dbo.tblTicketsViolations AS v ON t.TicketID_PK = v.TicketID_PK LEFT OUTER JOIN  
                      dbo.tblticketsextensions AS te ON t.TicketID_PK = te.Ticketid_pk LEFT OUTER JOIN  
    dbo.tblCourtViolationStatus AS cvs ON v.CourtViolationStatusIDmain = cvs.CourtViolationStatusID LEFT OUTER JOIN  
                      dbo.tblCourts AS c ON v.CourtID = c.Courtid  
WHERE     (cvs.CategoryID = 12)  
and t.activeflag = 1   
and (  
  ( (@validation = 1 OR @ValidationCategory=1) and (datediff (day,isnull(te.BondFollowUpdate,getdate()),getdate() ) >= 0))   -- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
  or (@validation = 0 OR @ValidationCategory=2)  -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
 )
 --Sabir Khan 5297 12/03/2008   sort by follow up date...
 order by te.BondFollowUpdate 
END  
  

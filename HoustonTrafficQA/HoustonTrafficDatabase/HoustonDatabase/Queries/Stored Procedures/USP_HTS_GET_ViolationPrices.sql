SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/*        
Alter By: Rab nawaz Khan
Date:   07/17/2012
Task ID : 10330
        
Business Logic:  The stored procedure is used to get Violations Prices when use click on view Calculation link. . .
 
 */
 
ALTER procedure [dbo].[USP_HTS_GET_ViolationPrices] --4431 
 (    
 @TicketId  int    
 )    
as    
     
SELECT tv.TicketsViolationID,     
 p.planid,    CourtViolationStatusIDmain,
 isnull(tv.FineAmount,0) as FineAmount,     
 isnull(tv.BondAmount,0) as BondAmount,     
 isnull(pd.BasePrice,0) as BasePrice,     
 isnull(pd.SecondaryPrice,0) as SecondaryPrice,     
 isnull(pd.BasePercentage,0) as BasePercentage,     
 isnull(pd.SecondaryPercentage,0)as SecondaryPercentage,     
 isnull(pd.BondBase,0) as BondBase,     
 isnull(pd.BondSecondary,0) as BondSecondary ,     
 isnull(pd.BondBasePercentage,0) as BondBasePercentage,     
 isnull(pd.BondSecondaryPercentage,0) as BondSecondaryPercentage,     
 isnull(pd.BondAssumption,0) as BondAssumption,     
 isnull(pd.FineAssumption, 0) as FineAssumption,   
 isnull(tv.underlyingbondflag,0) as underlyingbondflag,
 ISNULL (tv.ViolationNumber_PK, 0) AS ViolationNumber_PK,
 v.[Description] AS Description
INTO #tempTable    
FROM 	tblTicketsViolations tv 
INNER JOIN
	dbo.tblViolations v 
ON 	tv.ViolationNumber_PK = v.ViolationNumber_PK 
INNER JOIN
	dbo.tblPricePlans p 
ON 	 tv.PlanID = p.PlanId -- AND tv.CourtID = p.CourtId 
LEFT OUTER JOIN
	dbo.tblPricePlanDetail pd 
ON 	p.PlanId = pd.PlanId 
AND 	isnull(v.CategoryID,24) = pd.CategoryId
    
where 	tv.ticketid_pk = @TicketId    
and tv.CourtViolationStatusIDmain <> 239
	
IF((SELECT TOP 1 casetypeid FROM tblTickets tk WHERE tk.TicketID_PK = @TicketId) = 1)
	BEGIN
		SELECT TicketsViolationID
		INTO #tmpSelectedViolations
		FROM #tempTable
		WHERE (Description LIKE '%Registration%' OR Description LIKE '%Inspection%' OR Description LIKE '%seat belt%'
		OR Description LIKE '%seatbelt%')
			

		UPDATE #tempTable
		SET BasePrice = 45, SecondaryPrice = 45, BondBase = 45, BondSecondary = 45
		WHERE TicketsViolationID IN (SELECT vv.TicketsViolationID FROM #tmpSelectedViolations vv)	
	END


SELECT TicketsViolationID, planid, CourtViolationStatusIDmain,FineAmount,BondAmount,BasePrice,SecondaryPrice,BasePercentage,
SecondaryPercentage,BondBase,BondSecondary, BondBasePercentage,BondSecondaryPercentage, BondAssumption,FineAssumption,   
underlyingbondflag    
FROM #tempTable

DROP TABLE #tempTable
-- END 10330

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


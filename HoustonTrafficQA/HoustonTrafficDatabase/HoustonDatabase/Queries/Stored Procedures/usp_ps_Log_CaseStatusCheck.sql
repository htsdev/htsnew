SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_ps_Log_CaseStatusCheck]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_ps_Log_CaseStatusCheck]
GO

CREATE PROCEDURE usp_ps_Log_CaseStatusCheck  
@LastName varchar(30),  
@DOB datetime,  
@PhoneNumber varchar(25),  
@Email varchar(30),  
@TicketID int,  
@DLNumber varchar(20)  
as  
  
INSERT INTO tbl_CheckCaseStatus_Log(  
LastName,DOB,PhoneNumber,Email,TicketID,DLNumber)  
  
VALUES(  
@LastName,@DOB,@PhoneNumber,@Email,@TicketID,@DLNumber)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


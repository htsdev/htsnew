  GO
/****** Object:  StoredProcedure [dbo].[Usp_Trailletter_New]    Script Date: 06/18/2008 21:27:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Alter Procedure [dbo].[Usp_Trailletter_New]         
 (        
 @TicketIDList  int,        
 @empid int        
 )        
as        
SELECT dbo.tblTickets.TicketID_PK,         
 dbo.tblTickets.MiddleName,         
 dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK,         
 dbo.tblTickets.Firstname,         
 dbo.tblTickets.Lastname,         
 dbo.tblTickets.Address1,         
 dbo.tblTickets.Address2,         
 dbo.tblTickets.City AS Ccity,         
 dbo.tblState.State AS cState,         
 dbo.tblTickets.Zip AS Czip,         
 dbo.tblTicketsViolations.CourtDate,         
 dbo.tblTicketsViolations.CourtNumber,         
 dbo.tblCourts.Address,         
 dbo.tblCourts.City,         
 dbo.tblCourts.Zip,         
 tblState_1.State,         
--Adil 2664 06/18/2008 dbo.tblTickets.Datetypeflag,  
dbo.tblDateType.TypeID as Datetypeflag,
 dbo.tblTickets.Activeflag,         
 dbo.tblTickets.LanguageSpeak,         
--Adil 2664 06/18/2008 dbo.tblTicketsViolations.SequenceNumber,         
 dbo.tblCourtViolationStatus.CategoryID,         
 dbo.tblTicketsViolations.TicketViolationDate,         
 dbo.tblViolations.Description,         
 dbo.tblDateType.Description AS CaseStatus        
FROM    dbo.tblTickets         
INNER JOIN        
 dbo.tblTicketsViolations         
ON  dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK         
INNER JOIN        
 dbo.tblCourts         
--ON  dbo.tblTickets.CurrentCourtloc = dbo.tblCourts.Courtid         
ON  dbo.tblTicketsviolations.courtid = dbo.tblCourts.Courtid         
INNER JOIN        
 dbo.tblState         
ON  dbo.tblTickets.Stateid_FK = dbo.tblState.StateID         
INNER JOIN        
 dbo.tblState tblState_1         
ON  dbo.tblCourts.State = tblState_1.StateID         
INNER JOIN        
 dbo.tblCourtViolationStatus         
ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID         
INNER JOIN        
 dbo.tblViolations         
ON  dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK         
INNER JOIN        
 dbo.tblDateType         
ON  dbo.tblCourtViolationStatus.CategoryID = dbo.tblDateType.TypeID        
WHERE   (dbo.tblTickets.Activeflag = 1)         
AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)         
AND     (dbo.tblCourtViolationStatus.CategoryID IN (2, 3, 4, 5))         
--AND  (NOT (dbo.tblTicketsViolations.RefCaseNumber LIKE 'ID%'))        
AND     (dbo.tblTickets.TicketID_PK = @ticketidlist)        
        
        
if @@rowcount >=1        
 exec sp_Add_Notes @TicketIDList,'Trial Letter Printed',null,@empid        
        




GO
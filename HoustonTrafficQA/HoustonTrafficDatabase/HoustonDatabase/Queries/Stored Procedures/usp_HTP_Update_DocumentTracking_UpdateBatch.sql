/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to update the information entered from Update Batch page (No Letter ID)
				 this information is entered by User. 

List of Parameters:	
	@ScanBatchDetailID : id against which records are updated
	@DocumentBatchID : docuemnt id 
	@DocumentBatchPageCount : total page count
	@DocumentBatchPageNo : page no
	@EmployeeID : employee id of current logged in user

******/

Create Procedure dbo.usp_HTP_Update_DocumentTracking_UpdateBatch

@ScanBatchDetailID	int,
@DocumentBatchID	int,
@DocumentBatchPageCount	int,
@DocumentBatchPageNo	int,
@EmployeeID	int

as

update tbl_htp_documenttracking_scanbatch_detail
set	DocumentBatchID=@DocumentBatchID,
	DocumentBatchPageCount=@DocumentBatchPageCount,
	DocumentBatchPageNo=@DocumentBatchPageNo,
	EmployeeID=@EmployeeID
where ScanBatchDetailID=@ScanBatchDetailID


go

grant exec on dbo.usp_HTP_Update_DocumentTracking_UpdateBatch to dbr_webuser
go
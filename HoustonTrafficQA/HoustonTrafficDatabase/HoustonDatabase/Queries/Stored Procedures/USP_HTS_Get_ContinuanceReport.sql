 /******
 Business Logic : This report is used to get Continuance flag clients.
    
******/    
-- USP_HTS_Get_ContinuanceReport '1/1/1900' ,'1/1/1900',0,1,''              
ALTER procedure [dbo].[USP_HTS_Get_ContinuanceReport] -- '5/9/2007' ,'5/9/2007','1'          
(          
@StartDate datetime,          
@EndDate datetime,          
@CourtID int,      
@ViewAll bit,      
@Status varchar(200)          
)          
          
as          
          
declare  @Query varchar(2000)           
--declare @Records  table(status int)                    
--insert into @Records select * from dbo.sap_string_param(@Status)               
 --Sabir Khan 7979 07/14/2010 check for sugarland court for couurt room number  
        
Set @Query = 'SELECT   distinct  T.TicketID_PK, T.Firstname, T.Lastname, TCS.Description AS ContinuanceStatus,  
( case when convert(varchar,isnull(T.ContinuanceDate,''01/01/1900''),101)=''01/01/1900'' then '''' else convert(varchar(10),T.ContinuanceDate,101) end) as ContinuanceDate,  
t.continuancedate,  
TV.courtdatemain,TU.Abbreviation AS Rep,tc.shortname,Convert(varchar ,tv.courtnumber) as courtnumber,        
                          (SELECT     TOP (1) CONVERT(Varchar, tsc.ScanBookID) + ''-'' + CONVERT(varchar, tsp.DOCID) + ''.'' + tsp.DocExtension AS Doc        
                            FROM          dbo.tblScanBook AS tsc INNER JOIN        
                                                   dbo.tblTickets AS T ON T.TicketID_PK = tsc.TicketID RIGHT OUTER JOIN        
                                                   dbo.tblScanPIC AS tsp ON tsc.ScanBookID = tsp.ScanBookID        
                            WHERE      (T.ContinuanceStatus > 0) AND (tsc.DocTypeID = 2) AND (tsc.TicketID = TV.TicketID_PK)) AS Doc        
FROM         dbo.tblTickets AS T inner join tblticketsviolations tv on t.ticketid_pk=tv.ticketid_pk LEFT OUTER JOIN        
                      dbo.tblContinousStatus AS TCS ON T.ContinuanceStatus = TCS.StatusID LEFT OUTER JOIN        
                      dbo.tblUsers AS TU ON T.EmployeeID = TU.EmployeeID inner join tblcourts tc on tv.courtid =  tc.courtid          
WHERE T.ContinuanceStatus > 0  and isnull(T.ContinuanceFlag,0)<>0 '          
        
/*Set @Query = 'Select T.TicketID_pk , T.FirstName , T.LastName ,TCS.Description as ContinuanceStatus , T.ContinuanceDate , T.CourtDate , TU.Abbreviation as Rep          
from tbltickets T Join tblcontinousstatus TCS on T.continuancestatus = TCS.StatusID Join tblUsers TU on T.EmployeeID = TU.EmployeeID Where '  */        
       
if(@ViewAll =0)      
 begin      
         
if (  convert(datetime, convert(varchar(12), @StartDate, 101)) != '1/1/1900' )          
 Begin          
  set @StartDate = convert(datetime, convert(varchar(12), @StartDate, 101)) + '00:00:00:000'          
  set @EndDate = convert(datetime,  convert(varchar(12), @EndDate, 101)) + '23:59:59.998'          
  Set @Query = @Query + 'and tv.courtdatemain between ''' +  Convert(varchar,@StartDate)           
    + ''' and ''' +  Convert(varchar,@EndDate)  + ''''          
  end          
 Else          
  Begin          
  Set @Query = @Query + ' and tv.courtdatemain > getdate() '          
  end          
           
 if(@CourtID = 2)      
  Begin      
  Set @Query = @Query + ' AND Tv.CourtID in(3001,3002,3003)'      
  End      
 else if(@CourtID = 3)      
  Begin      
  Set @Query = @Query + ' AND Tv.CourtID not in(3001,3002,3003)'      
  End      
 else if(@CourtID <> 0 and @CourtID <> 1)      
  Begin      
  Set @Query = @Query + ' AND Tv.CourtID='+convert(varchar,@CourtID)      
  End      
      
 if ( len(@Status) > 0 )          
 Begin          
  Set @Query = @Query + ' AND T.continuancestatus In ('+ @Status + ')'          
 end          
          
 end   
Else          
  Begin          
  Set @Query = @Query + ' and datediff(day,tv.courtdatemain,getdate())<=0 '          
  end        
  
Set @Query = @Query + ' order by t.ContinuanceDate desc '         
      
      
--print( @Query)          
exec( @Query )          
          
        
        
------------------------------------------------------------------------------------------ 
﻿/****** 
Created by:		Rab Nawaz Khan
Task ID   :     10391
Date      :     09/12/2012
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Immigration Letters for the selected date range for the Non-Clients.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

-- [dbo].[USP_MAILER_Send_Immigration_Data_For_Clients]  131
ALTER PROCEDURE [dbo].[USP_MAILER_Send_Immigration_Data_For_Clients] 
(  
	@LetterType  INT = 131,
	@empid int=3992
)  
  
AS

SELECT DISTINCT ta.TicketID_PK, DATEDIFF(DAY, ISNULL(ta.DOB, '01/01/1900'), GETDATE()) / 365.25 AS ageLimit,
		DATEDIFF(DAY, ISNULL(ta.DOB, '01/01/1900'), GETDATE()) / 365 AS AgeYears  
INTO #MainQuery 
FROM dbo.tblTicketsViolations tva INNER JOIN dbo.tblTickets ta ON  ta.TicketID_PK = tva.TicketID_PK
	INNER JOIN tblViolations tv ON tva.ViolationNumber_PK = tv.ViolationNumber_PK
	INNER JOIN tblTicketsPayment ttp ON ta.TicketID_PK = ttp.TicketID

-- First Name last should not be null or empty. . . 
WHERE ISNULL(ta.FirstName, '') <> ''  
and ISNULL(ta.LastName, '') <> '' 

-- Only Clients 
and ta.Activeflag = 1

-- Only Spanish Speakers
AND ta.LanguageSpeak = 'SPANISH'


-- Only violations which contains "license", OR any thing related to "Driver's license" in the violation Description. . . 
AND tv.[Description] LIKE '%license%' 

-- Only Valid payment Types. . . 
AND ttp.PaymentType NOT IN (0, 10, 98, 99, 100)

-- payment must not be void. . . 
AND ttp.PaymentVoid = 0

-- EMPLOYEE IN THE LOCAL VARIABLE....  
declare @user varchar(10)  
select @user = upper(abbreviation) from tblusers where employeeid = @empid  

SELECT DISTINCT UPPER(t.Firstname) AS firstname, UPPER (t.Lastname) AS lastname, CONVERT(VARCHAR(10),t.DOB,101) AS DOB, UPPER(address1) + '' + ISNULL(t.address2,'') as address,
	v.CourtDate, t.City, t.Stateid_FK, t.Zip AS zipcode, @user AS Abb, 39 AS dptwo, v.CourtID, AgeYears, m.TicketID_PK, ageLimit
INTO #FinalQuery 
FROM tbltickets t INNER JOIN tblTicketsViolations v ON t.TicketID_PK = v.TicketID_PK
	INNER JOIN #MainQuery m ON m.TicketID_PK = t.TicketID_PK


-- Deleting the Records which are greater on less then age limit 16 to 30. . . 
DELETE FROM #FinalQuery
WHERE ageLimit < 16
OR ageLimit > 30 


ALTER TABLE #FinalQuery
ADD STATE VARCHAR(100)

UPDATE f
SET f.STATE = ts.[State] 
FROM tblState ts INNER JOIN #FinalQuery f ON ts.StateID = f.Stateid_FK


--SELECT DISTINCT firstname,lastname, DOB, address, CourtDate, City, Stateid_FK, zipcode, Abb, dptwo, CourtID, AgeYears, TicketID_PK
SELECT DISTINCT firstname,lastname, DOB, address, City, STATE, zipcode, Abb, 39 AS dptwo, AgeYears, 
(SELECT TOP 1 TicketID_PK FROM #FinalQuery q
 WHERE f.firstname = q.firstname AND f.lastname = q.lastname
 AND f.DOB = q.DOB AND f.address = q.address AND f.city = q.city
 AND f.STATE = q.STATE AND f.zipcode = q.zipcode
)  AS RecordID
INTO #Display
FROM #FinalQuery f
ORDER BY f.firstname, f.lastname

-- Disabeling the clients Immigration letter just after 1st time execution . . . 
UPDATE tblletter SET isactive = 0 WHERE courtcategory = 35 AND LetterID_PK = 131

SELECT DISTINCT firstname,lastname, DOB, address, City, STATE, zipcode, Abb, 39 AS dptwo, AgeYears, 
	CONVERT(VARCHAR(100), ROW_NUMBER() OVER (ORDER BY RecordID)+ 100000) AS letterId, RecordID, 131 AS LetterType
FROM #Display
WHERE firstname  <> 'Test'
AND lastname <> 'Test'


-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
DROP TABLE #MainQuery
DROP TABLE #FinalQuery
DROP TABLE #Display

GO 
GRANT EXECUTE ON [USP_MAILER_Send_Immigration_Data_For_Clients] TO webuser;
Go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_WebScan_DeleteBatch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_WebScan_DeleteBatch]
GO



CREATE procedure Usp_WebScan_DeleteBatch  
  
@BatchID as int   
  
  
as  
  
  
Begin  
   
 DELETE FROM TBL_WEBSCAN_OCR WHERE PICID in (SELECT P.ID FROM TBL_WEBSCAN_PIC P INNER JOIN TBL_WEBSCAN_OCR O ON P.ID=O.PICID WHERE P.BATCHID= @BatchID)  
  
 DELETE FROM TBL_WEBSCAN_DATA WHERE PICID in (SELECT P.ID FROM TBL_WEBSCAN_PIC P INNER JOIN TBL_WEBSCAN_DATA D ON P.ID=D.PICID WHERE P.BATCHID= @BatchID)  
  
   
 DELETE FROM TBL_WEBSCAN_PIC WHERE BATCHID=@BatchID  
  
 DELETE FROM TBL_WEBSCAN_BATCH WHERE BATCHID=@BatchID  
  
 -- Delete batch for BSDA windows service
declare @RecCount int

set @RecCount = (select count(*) from tbl_hts_WebScan_WinService where BatchID = @BatchID)

if(@RecCount <> 0)
begin
	update tbl_hts_WebScan_WinService
	set BatchID = -1
	Where BatchID = @BatchID
end

  
End  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


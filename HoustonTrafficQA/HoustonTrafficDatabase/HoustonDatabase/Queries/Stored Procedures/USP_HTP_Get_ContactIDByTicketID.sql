﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/20/2009 11:18:16 AM
 ************************************************************/

/****    
* Created BY : Nasir  
* Business Logic: select top 1 contactID of given ticket ID    
* Parameter:  @TicketID  
* Selection: ContactID   
* Task ID: Nasir 5771 04/17/2009
****/    
--USP_HTP_Get_ContactIDByTicketID 80175    
    
    
    
CREATE PROCEDURE dbo.USP_HTP_Get_ContactIDByTicketID
	@TicketID INT
AS
	DECLARE @ContactID INT    
	
	SET @ContactID = (
	        SELECT TOP 1 ContactID_FK
	        FROM   tbltickets
	        WHERE  TicketID_PK = @TicketID
	    )    
	
	IF @ContactID IS NULL
	    SELECT '0'
	ELSE
	    SELECT @ContactID
GO

GRANT EXECUTE ON dbo.USP_HTP_Get_ContactIDByTicketID TO dbr_webuser

GO
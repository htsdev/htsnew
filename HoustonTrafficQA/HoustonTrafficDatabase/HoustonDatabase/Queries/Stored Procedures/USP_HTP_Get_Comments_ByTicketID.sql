﻿

/******   
Created by:  Muhammad Nasir  
Task ID :  6098  
Business Logic: The procedure is used by houston to get the Comments by given search criteria its comment type should be General,reminder call,trial,continuance,set call or FTA call.  
      
List of Parameters:   
 @startdate:  Starting date of complaint flag add  
 @endListDAte: Ending date of complaint flag add  
 @CaseType  Civil, Criminal, Traffic, Family  
 @Attorney  Attorney who was covering the case  
 @ShowAll  By default all non reviewed cases will be displayed  
  
List of Columns:   
 CommDate: Comments Date.  
 CommDay:  Comm day  
 CommType:  Comments type
 CommentID: Comments type ID
 Rep:   Reps  
 sortCommDate: Comments Date for date  
  
******/  
  


Create PROCEDURE USP_HTP_Get_Comments_ByTicketID
	@TicketID INT
AS
BEGIN
    SELECT dbo.fn_DateFormat(ttn.Recdate, 24, '/', ':', 1) AS CommDate,
           CASE (DATEPART(dw, ttn.Recdate) + @@DATEFIRST) % 7
                WHEN 1 THEN 'Sun'
                WHEN 2 THEN 'Mon'
                WHEN 3 THEN 'Tue'
                WHEN 4 THEN 'Wed'
                WHEN 5 THEN 'Thur'
                WHEN 6 THEN 'Fri'
                WHEN 0 THEN 'Sat'
           END  AS CommDay,
           c.CommentType CommType,
           ttn.Fk_CommentID AS CommentID,
           ttn.[Subject] Comm,
           tu.Lastname Rep,
           ttn.Recdate AS sortCommDate
    FROM   tblTicketsNotes ttn
           LEFT JOIN Comments c
                ON  ttn.Fk_CommentID = c.CommentID
           LEFT JOIN tblUsers tu
                ON  ttn.EmployeeID = tu.EmployeeID
    WHERE  ttn.TicketID = @TicketID
    AND c.CommentID IN (1,3,4,6,9,10)
   
END



    
SELECT       
 ISNULL(T.Firstname, '') AS Firstname,                             
 ISNULL(T.Lastname, '') AS Lastname,                                 
 ISNULL(T.Midnum, '') AS Midnum,                             
 dbo.fn_HTS_Get_CustomerInfoByMidnumber(T.TicketID_PK) AS CaseCount                             
                            
FROM     tblTickets T                     
where                      
(T.TicketID_PK = @TicketID)  



GO
GRANT EXECUTE ON dbo.USP_HTP_Get_Comments_ByTicketID TO dbr_webuser
GO
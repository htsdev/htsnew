﻿/****** Object:  StoredProcedure [dbo].[USP_HTP_UpdateBondFlagByTicketId]  ******/     
/*        
 Created By     : Fahad Muhammad Qureshi.
 Created Date   : 05/18/2009  
 TasK		    : 5807        
 Business Logic : This procedure Set the Bond Flag 
				
Parameter:       
  @ticketid_pk	: Ticket id      
  
*/ 

CREATE PROCEDURE [dbo].[USP_HTP_UpdateBondFlagByTicketId]
@TicketID INT
AS
UPDATE tblTickets
SET
	BondsRequiredflag=1,
	BondFlag = 1

WHERE   
     TicketID_PK=@TicketID  
     
GO
GRANT EXEC ON [dbo].[USP_HTP_UpdateBondFlagByTicketId] TO dbr_webuser
GO   
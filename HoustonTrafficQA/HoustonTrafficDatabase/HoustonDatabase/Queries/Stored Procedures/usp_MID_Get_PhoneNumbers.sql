SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_PhoneNumbers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_PhoneNumbers]
GO



--  usp_MID_Get_PhoneNumbers_Ver_1 '96999,' , '189729,'
CREATE procedure [dbo].[usp_MID_Get_PhoneNumbers]

	(
	@TicketIDs	varchar(400),  -- INPUT PARAMETER TO GET THE CONTACTS FROM PREVIOUS CASES...
	@RecordIDs	varchar(400)   -- INPUT PARAMETER TO GET THE CONTACTS FROM NEW CASES......
	)

as

declare @strSQL varchar(8000)

-- REMOVING TRAILING COMMA FROM THE CASE IDs
if right(@ticketids,1) = ',' 
	begin 
		set @ticketids = left(@ticketids,len(@ticketids)-1)
	end

-- REMOVING TRAILING COMMA FROM THE CASE IDs
if right(@RecordIDs,1) = ',' 
	begin 
		set @RecordIDs = left(@RecordIDs,len(@RecordIDs)-1)
	end


select @strSQL='
-- CREATING TEMPORARY TABLE.......
declare @phoneno varchar(500)
declare	@tmpphone	table   
	(   
	PhNumber varchar(50)   
	)   

-- INSERTING CONTACT NUMBERS IN TEMPORARY TABLE 
insert into @tmpphone

	-- PHONE NUMBERS RELATED TO THE PREVIOUS CASES.....
	select	distinct ltrim(rtrim(phonenumber)) as PhNo
	from	tblticketsarchive 
	where	recordid in (' + @recordids + ')
	and 	phonenumber not like ''%0000000%''  
	and 	phonenumber not like ''00000%''  
	and 	phonenumber not like ''01000%''       
	and 	phonenumber is not null  
	and 	phonenumber not in (
			select distinct phonenumber from tblrestrictedphonenumbers
			)
	union 

	-- PHONE NUMBERS RELATED TO THE NEW CASES.....
	select distinct ltrim(rtrim(contact)) as PhNo
	from	tblticketscontact
	where	ticketid_pk in (' + @ticketids + ')
	and 	contact not like ''00000%''
	and 	contact not like ''%0000000%''
	and 	contact not like ''01000%''

	-- EXCLUDING BLOCKED PHONE NUMBERS........
	and 	contact not in (
			select distinct phonenumber from tblrestrictedphonenumbers
			)

-- NOW ADDING CONTACT TYPE LIKE HOME, WORK, MOBILE ... TO THE CONTACT NUMBER.....
select	left(m.phnumber,3)+''-''+substring(m.phnumber,4,3)+''-''+substring(m.phnumber,7,len(m.phnumber))    +''(''+isnull(
				(
			  	select top 1 c.description from tblcontactstype c 
			  	inner join tblticketscontact t 
				on c.ContactType_pk = t.ContactType_pk
				where t.contact = m.phnumber
				order by t.recdate desc
				)
		 	 ,'''')+'')''
from	@tmpphone m' 	


exec(@strSQL)




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


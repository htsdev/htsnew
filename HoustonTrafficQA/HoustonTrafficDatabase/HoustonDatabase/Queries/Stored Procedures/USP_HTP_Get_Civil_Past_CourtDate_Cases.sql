
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Get_Civil_Past_CourtDate_Cases]    Script Date: 09/15/2008 10:18:14 ******/
-- Noufil 4747
/******   
Create by:  Noufil Khan  
Business Logic : This Procedure returns information about clients in Civil with court id = 3077 and main category id in Pre-trialand Waiting
					and thier court date is of past.

List of Columns:    
		ticketsviolationid
		refcasenumber
		courtdatemain
		ticketid_pk
		Description
  
******/  


alter Procedure [dbo].[USP_HTP_Get_Civil_Past_CourtDate_Cases]
as  

select 
		tv.ticketsviolationid , 
		tv.refcasenumber  , 
		tv.courtdatemain, 
		tv.ticketid_pk , 
		T.Description as Status  

from tblticketsviolations Tv 
	inner join tblcourtviolationstatus S on Tv.courtviolationstatusidmain = S.courtviolationstatusid
	inner join tbldatetype T on S.CategoryId = T.Typeid

where tv.courtid = 3077   
and T.Typeid in ( 1,3)
and datediff(day,tv.courtdatemain,getdate())>0 

go

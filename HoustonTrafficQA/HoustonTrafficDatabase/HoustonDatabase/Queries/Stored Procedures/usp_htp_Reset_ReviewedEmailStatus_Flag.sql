/*
* Created By: Abbas Qamar
* Task : 9676
* Date: 10/12/2011
* 
* Business logic: This procedure is used to set the bad email flag for the against Ticket ID
* 
* Input Parameters:
*			@ticketid = Ticket ID of record
*/


-- [dbo].[usp_htp_Reset_ReviewedEmailStatus_Flag] 8790 

CREATE PROCEDURE [dbo].[usp_htp_Reset_ReviewedEmailStatus_Flag]
	@ticketid INT
AS
BEGIN

UPDATE tbltickets                                            
SET ReviewedEmailStatus = 0
WHERE TicketID_PK=@ticketid

     
END


GO
GRANT EXECUTE ON [dbo].[usp_htp_Reset_ReviewedEmailStatus_Flag] TO dbr_webuser
GO 
	


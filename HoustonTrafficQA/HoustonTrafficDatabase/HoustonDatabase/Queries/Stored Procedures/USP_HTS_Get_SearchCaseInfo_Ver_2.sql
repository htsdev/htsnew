



 /******************  
  
Alter BY : Zeeshan Ahmed  
  
BUSINESS LOGIC : This procedure is used to get cases which have discrepancies.  
     Modify Civil Search Query To Display Party First Name , Last Name with the case.  
     Modify Civil Search Query To Search On Cause Number  
LIST OF INPUT PARAMETERS :  
   
  @FilterType     : 0= Client, 1=Quote, 2=NonClient, 3=Archive, 4=JIMS , 5=CIVIL, -1=Client / Quote (4053 05/23/2008 Agha Usman)  
  @TicketNumber  : Ticket Number                  
  @SearchKeyWord1 : Search Keyword               
  @SearchKeyType1 : 0= ticketno, 1= lastname, 2=firstname, 3=midnum, 4=dob, 5=phone no, 6=court dt, 7=drv. license , 9=RecordId, 10=Cause No,11=Hire Date, 12=Payment Date 13=Last Contact Date 14=Email (Added by Ozair for Task Id 8982 Outlook Addin) 
  @SearchKeyWord2 : Search Keyword                      
  @SearchKeyType2 : 0= ticketno, 1= lastname, 2=firstname, 3=midnum, 4=dob, 5=phone no, 6=court dt, 7=drv. license , 9=RecordId, 10=Cause No,11=Hire Date, 12=Payment Date 13=Last Contact Date 14=Email (Added by Ozair for Task Id 8982 Outlook Addin) . 
  @LIDad    : Letter Id  
  @Causenumberad  : Cause Number  
  @courtid   : Court ID   
  @SearchKeyWord3 : Search Keyword     
  @SearchKeyType3 : 0= ticketno, 1= lastname, 2=firstname, 3=midnum, 4=dob, 5=phone no, 6=court dt, 7=drv. license, 8=contact dt. , 9=RecordId, 10=Cause No,11=Hire Date, 12=Payment Date 13=Last Contact Date 14=Email (Added by Ozair for Task Id 8982 Outlook Addin) .  
  
LIST OF COLUMNS :  
  
  LastName  :            
  FirstName :                 
  Casenumassignedbycourt : Cause Number  
  RefCaseNumber :  Ticket Number   
  SequenceNumber : Sequence No  
  Address1 : Address  1  
  Zipcode : Zip  
  Address   
  MIDNo              
  DOB                 
  CourtDate                 
  CaseStatus              
  TicketId  
  CourtId        
  dbid        
  Matter : Violation Description  
  Court  : Court Name   
*******************/  
   
   
 --Zeeshan Ahmed 3013 02/12/08
 --The Advance Search Procedure has been modified
 --An other search keyword and search key type have been added to the procedure  
  
-- Noufil 4369 07/17/2008 Merging Clients and Quote in a single filtertype.  
   
  --  USP_HTS_Get_SearchCaseInfo_Ver_2 '' ,-1,'07/23/2008',8,'','','',-1  
ALTER PROCEDURE [dbo].[USP_HTS_Get_SearchCaseInfo_Ver_2]
(
    @FilterType      INT = 0,
    @TicketNumber    VARCHAR(25) = '', 
    @SearchKeyWord1  VARCHAR(50) = '',
    @SearchKeyType1  INT = NULL,
    @SearchKeyWord2  VARCHAR(50) = '',
    @SearchKeyType2  INT = NULL,
    -- Updated on 31/08/2007 for advance search      
    @LIDad           VARCHAR(50) = '',
    @Causenumberad   VARCHAR(50) = '',
    @courtid         INT = 0,
    --Zeeshan Ahmed 3013 02/12/2008  
    @SearchKeyWord3  VARCHAR(50) = '',
    @SearchKeyType3  INT = NULL 
)
AS
	SET NOCOUNT ON -- Do not show number of rows returned by the query.                
	SET ROWCOUNT 250 -- Limiting the output of the query to 250 records.                
	
	DECLARE @strSQL NVARCHAR(MAX),
	        @strSQL2 NVARCHAR(MAX),
	        @strWhere NVARCHAR(MAX),
	        @strSQLTemp NVARCHAR(MAX),
	        @strParamDef NVARCHAR(1000),
	        @isDefault BIT,
	        @tempDate DATETIME,
	        @IsLIDSearch INT 
	
	-- PROCEDURE CALLED WITHOUT ANY PARAMETER THAN SETTING THE DEFAULT FLAG....            
	IF (
	       @filtertype = 0
	       AND @ticketnumber = ''
	       AND @SearchKeyWord1 = ''
	       AND @SearchKeyWord2 = ''
	       AND @SearchKeyWord3 = ''
	       AND @LIDad = ''
	       AND @Causenumberad = ''
	   )
	    SET @isDefault = 1 
	
	-- tahir 4167 06/03/2008
	-- SETTING FLAG IF SEARCHING BY LID......  
	IF (
	       (LEN(ISNULL(@LIDad, '')) > 0)
	       OR (
	              LEN(ISNULL(@SearchKeyWord1, '')) > 0
	              AND @SearchKeyType1 = 9
	          )
	       OR (
	              LEN(ISNULL(@SearchKeyWord2, '')) > 0
	              AND @SearchKeyType2 = 9
	          )
	       OR (
	              LEN(ISNULL(@SearchKeyWord3, '')) > 0
	              AND @SearchKeyType3 = 9
	          )
	   )
	    SET @IsLIDSearch = 1
	ELSE
	    SET @IsLIDSearch = 0 
	
	-- FORMATTING TICKET NUMBER......            
	IF @TicketNumber <> ''
	    SET @TicketNumber = LEFT(@TicketNumber, LEN(@TicketNumber) - 1) + '%' 
	
	-- FORMATTING CAUSE NUMBER......  
	IF @CauseNumberAd <> ''
	BEGIN
	    SET @Causenumberad = REPLACE(@Causenumberad, ' ', '')      
	    SET @CauseNumberAd = LEFT(@CauseNumberAd, LEN(@CauseNumberAd) -1) + '%'
	END  
	
	IF (
	       (@SearchKeyType1 = 0 OR @SearchKeyType1 = 10)
	       AND @SearchKeyWord1 <> ''
	   )
	    SET @SearchKeyWord1 = LEFT(@SearchKeyWord1, LEN(@SearchKeyWord1) - 1) + 
	        '%'             
	
	IF (
	       (@SearchKeyType2 = 0 OR @SearchKeyType2 = 10)
	       AND @SearchKeyWord2 <> ''
	   )
	    SET @SearchKeyWord2 = LEFT(@SearchKeyWord2, LEN(@SearchKeyWord2) - 1) + 
	        '%'             
	
	IF (
	       (@SearchKeyType3 = 0 OR @SearchKeyType3 = 10)
	       AND @SearchKeyWord3 <> ''
	   )
	    SET @SearchKeyWord3 = LEFT(@SearchKeyWord3, LEN(@SearchKeyWord3) - 1) + 
	        '%'         
	
	IF (@LIDad <> '')
	BEGIN
	    SET @LIDad = REPLACE(@LIDad, ' ', '')
	END 
	
	
	-- FORMATTING MIDNUMBER IF SEARCHING BY MID NUMBER.......            
	IF (@SearchKeyType1 = 3 AND @SearchKeyWord1 <> '')
	    SET @SearchKeyWord1 = '%' + @SearchKeyWord1             
	
	IF (@SearchKeyType2 = 3 AND @SearchKeyWord2 <> '')
	    SET @SearchKeyWord2 = '%' + @SearchKeyWord2             
	
	IF (@SearchKeyType3 = 3 AND @SearchKeyWord3 <> '')
	    SET @SearchKeyWord3 = '%' + @SearchKeyWord3 
	
	-- FORMATTING CAUSE NUMBER INPUT IF SEARCHING BY CAUSE NUMBER OF SearchKeyWord1.............................................................            
	IF (@SearchKeyType1 = 10 AND @SearchKeyWord1 <> '')
	BEGIN
	    -- REMOVING SPACES FROM THE SEARCH KEY WORD....          
	    SET @SearchKeyWord1 = REPLACE(@SearchKeyWord1, ' ', '')
	END 
	-- FORMATTING CAUSE NUMBER INPUT IF SEARCHING BY CAUSE NUMBER  OF SearchKeyWord2.............................................................                
	IF (@SearchKeyType2 = 10 AND @SearchKeyWord2 <> '')
	BEGIN
	    -- REMOVING SPACES FROM THE SEARCH KEY WORD....          
	    SET @SearchKeyWord2 = REPLACE(@SearchKeyWord2, ' ', '')
	END 
	
	-- FORMATTING CAUSE NUMBER INPUT IF SEARCHING BY CAUSE NUMBER  OF SearchKeyWord3.............................................................                
	IF (@SearchKeyType3 = 10 AND @SearchKeyWord3 <> '')
	BEGIN
	    -- REMOVING SPACES FROM THE SEARCH KEY WORD....          
	    SET @SearchKeyWord3 = REPLACE(@SearchKeyWord3, ' ', '')
	END 
	
	--Nasir 5863	04/05/2009 add hire date payment date contact date
	-- FORMATTING INPUT DATA........................            
	IF NOT (
	       @SearchKeyType1 = 4
	       OR @SearchKeyType1 = 6	       
	       OR @SearchKeyType1 = 9
	       OR @SearchKeyType1 = 11
	       OR @SearchKeyType1 = 12
	       OR @SearchKeyType1 = 13
	   )
	    IF @SearchKeyWord1 <> ''
	        SELECT @SearchKeyWord1 = @SearchKeyWord1 + '%'            
	--Nasir 5863	04/05/2009 add hire date payment date contact date
	IF NOT (
	       @SearchKeyType2 = 4
	       OR @SearchKeyType2 = 6	       
	       OR @SearchKeyType2 = 9
	       OR @SearchKeyType2 = 11
	       OR @SearchKeyType2 = 12
	       OR @SearchKeyType2 = 13	       
	   )
	    IF @SearchKeyWord2 <> ''
	        SELECT @SearchKeyWord2 = @SearchKeyWord2 + '%'            
	--Nasir 5863	04/05/2009 add hire date payment date contact date
	IF NOT (
	       @SearchKeyType3 = 4
	       OR @SearchKeyType3 = 6	      
	       OR @SearchKeyType3 = 9
	       OR @SearchKeyType3 = 11
	       OR @SearchKeyType3 = 12
	       OR @SearchKeyType3 = 13	       
	   )
	    IF @SearchKeyWord3 <> ''
	        SELECT @SearchKeyWord3 = @SearchKeyWord3 + '%' 
	--Nasir 5863	04/05/2009 add hire date payment date contact date
	-- FORMATTING INPUT DATA IF SEARCHING BY A DATE FIELD....            
	IF (
	       @SearchKeyType1 = 6
	       OR @SearchKeyType1 = 4
	       
	   )
	BEGIN
	    SET @tempDate = @SearchKeyWord1            
	    IF ISDATE(@tempDate) = 1
	        SELECT @SearchKeyWord1 = CONVERT(VARCHAR(10), @tempDate, 101)
	END             
	--Nasir 5863	04/05/2009 add hire date payment date contact date
	IF (
	       @SearchKeyType2 = 6
	       OR @SearchKeyType2 = 4
	    
	   )
	BEGIN
	    SET @tempDate = @SearchKeyWord2            
	    IF ISDATE(@tempDate) = 1
	        SELECT @SearchKeyWord2 = CONVERT(VARCHAR(10), @tempDate, 101)
	END            
	--Nasir 5863	04/05/2009 add hire date payment date contact date
	IF (
	       @SearchKeyType3 = 6
	       OR @SearchKeyType3 = 4
	     
	   )
	BEGIN
	    SET @tempDate = @SearchKeyWord3            
	    IF ISDATE(@tempDate) = 1
	        SELECT @SearchKeyWord3 = CONVERT(VARCHAR(10), @tempDate, 101)
	END 
	
	-- IF SEARCHING BY RECORD ID
	-- GET ALL MAILER IDs IN TEMP. TABLE        
	CREATE TABLE #MailerIDs
	(
		noteid    INT,
		recordid  INT
	) 
	
	-- GET MAILER IDS FROM TRAFFIC TICKETS IF SEARCHING BY CLIENT/QUOTE/NON-CLIENT/ARCHIVE/JIMS...
	-- tahir 4394 7/10/2008 -- added civil cases for LID search...  
	IF @FilterType IN (-1, 0, 1, 2, 4, 5)
	BEGIN
	    IF (@LIDad <> '')
	    BEGIN
	        INSERT INTO #MailerIDs
	          (
	            noteid,
	            recordid
	          )
	        SELECT n.noteid,
	               n.recordid
	               --Yasir Kamal 7218 01/13/2010 mailer structure changes 
	        FROM   tblletternotes n WITH(NOLOCK) INNER JOIN tblbatchletter b WITH(NOLOCK) ON n.batchid_fk = b.batchid
									INNER JOIN tblCourtCategories tcc WITH(NOLOCK) ON tcc.CourtCategorynum = b.CourtID
	        WHERE  
					tcc.LocationId_FK <> 2
	                --b.courtid NOT IN (6, 11, 16, 25)
	               AND n.noteid = @LIDad
	    END      
	    
	    IF @SearchKeyType1 = 9
	       AND @SearchKeyWord1 <> ''
	    BEGIN
	        INSERT INTO #MailerIDs
	          (
	            noteid,
	            recordid
	          )
	        SELECT n.noteid,
	               n.recordid
	        FROM   tblletternotes n WITH(NOLOCK) INNER JOIN tblbatchletter b WITH(NOLOCK) ON n.batchid_fk = b.batchid
									INNER JOIN tblCourtCategories tcc WITH(NOLOCK) ON tcc.CourtCategorynum = b.CourtID
	        WHERE  tcc.LocationId_FK <> 2
	               -- b.courtid NOT IN (6, 11, 16, 25)
	               AND noteid = @SearchKeyWord1
	    END
	    
	    IF @SearchKeyType2 = 9
	       AND @SearchKeyWord2 <> ''
	    BEGIN
	        INSERT INTO #MailerIDs
	          (
	            noteid,
	            recordid
	          )
	        SELECT n.noteid,
	               n.recordid
	        FROM   tblletternotes n WITH(NOLOCK) INNER JOIN tblbatchletter b WITH(NOLOCK) ON n.batchid_fk = b.batchid
									INNER JOIN tblCourtCategories tcc WITH(NOLOCK) ON tcc.CourtCategorynum = b.CourtID
	        WHERE	tcc.LocationId_FK <> 2				
	               -- b.courtid NOT IN (6, 11, 16, 25)
	               AND noteid = @SearchKeyWord2
	    END      
	    
	    IF @SearchKeyType3 = 9
	       AND @SearchKeyWord3 <> ''
	    BEGIN
	        INSERT INTO #MailerIDs
	          (
	            noteid,
	            recordid
	          )
	        SELECT n.noteid,
	               n.recordid
	        FROM   tblletternotes n WITH(NOLOCK) INNER JOIN tblbatchletter b WITH(NOLOCK) ON n.batchid_fk = b.batchid
									INNER JOIN tblCourtCategories tcc WITH(NOLOCK) ON tcc.CourtCategorynum = b.CourtID
	        WHERE  tcc.LocationId_FK <> 2
	               -- b.courtid NOT IN (6, 11, 16, 25)
	               AND noteid = @SearchKeyWord3
	    END
	END        
	--Yasir Kamal 7218 end
	SET @strWhere = ''    
	SET @strSQLTemp = ''              
	SET @strParamDef = 
	    '@FilterType int, @TicketNumber varchar(25), @SearchKeyWord1 varchar(50),                 
 @SearchKeyType1 int, @SearchKeyWord2 varchar(50), @SearchKeyType2 int, @LIDad varchar(50),   
 @Causenumberad varchar(50), @courtid int,@SearchKeyWord3 varchar(50), @SearchKeyType3 int,  
 @IsLIDSearch int' 
	
	-- CREATING TEMPORARY TABLE FOR STORING THE SEARCH RESULTS AND WILL BE USED TO GROUP RECORDS ON TICKETNUMBER.....            
	SET @strSQL = 
	    '  
declare @tempResult table (lastname varchar(50), firstname varchar(50), causenumber varchar(30), ticketnumber varchar(25), /* sequenceno varchar(5),*/ -- Sarim 2664 06/04/2008             
address1 varchar(200), zipcode varchar(20), address  varchar(200), midno varchar(20), dob datetime, courtdate datetime, casestatus varchar(50),            
ticketid varchar(20), courtid int, MailerIDFlag int, dbid int , Matter varchar(200) , Court varchar(100),ActiveFlag int,CaseType Varchar (100))    
          
  
' 
	
	--------------------------------------------------------------------------------------------------
	--    CLIENTS / QUOTE
	--------------------------------------------------------------------------------------------------
	--if (@FilterType = 0 OR @FilterType =1 or @FilterType =-1)  
	IF (@FilterType = 0 OR @FilterType = -1) -- filtertype 0 for client nad quote.
	BEGIN
	    SET @strSQL = @strSQL + 
	        '  
  insert into @tempResult  
              
  SELECT t.Lastname,                 
   t.Firstname,                 
   v.casenumassignedbycourt,        
   v.RefCaseNumber ,            
   --convert(varchar(10),isnull(v.SequenceNumber,0)),   -- Sarim 2664 06/04/2008              
   t.address1 as address1,            
   t.zip as zipcode,            
   t.Address1+'' ''+isnull(t.Address2,'''') as address,                 
   t.midnum as MIDNo,                
   convert(varchar(10),t.DOB,101) as DOB,                 
   convert(varchar(10), isnull(v.CourtDatemain,''01/01/1900'') ,101) as CourtDate,                 
   isnull(c.shortDescription,''N/A'') as CaseStatus,                
   t.ticketid_pk as TicketId,                
   v.courtid as CourtId,  -- Sarim 2664 06/04/2008       
   @IsLIDSearch as MailerIDFlag   ,0 as dbid ,  
   vio.Description as Matter,  
   CRT.ShortName as Court,  
   t.activeflag as ActiveFlag,  
   CT.CaseTypeName  
   FROM    dbo.tblDateType d WITH(NOLOCK)                
   INNER JOIN                
   dbo.tblCourtViolationStatus c WITH(NOLOCK)                
  ON  d.TypeID = c.CategoryID                 
  RIGHT OUTER JOIN                
   dbo.tblTickets t WITH(NOLOCK)
   --Nasir 5863 05/05/2009 for payment date
   LEFT OUTER JOIN tblTicketsPayment ttp WITH(NOLOCK) on ttp.ticketid=t.TicketID_PK
  INNER JOIN                
   dbo.tblTicketsViolations v WITH(NOLOCK)                 
  ON  t.TicketID_PK = v.TicketID_PK                 
  ON  c.CourtViolationStatusID = v.CourtViolationStatusIDmain      
  --Added By Zeeshan To Show Court Detail  
  Inner Join tblcourts CRT WITH(NOLOCK) 
  On CRT.CourtID = V.CourtID  
  Inner Join CaseType CT WITH(NOLOCK) 
  On CT.CaseTypeId = isnull(T.CaseTypeID,1)  
    
    --Added By Ozair, not show cases of violation type 1      
  LEFT OUTER JOIN      
 dbo.tblViolations vio WITH(NOLOCK)    
  ON  v.violationnumber_pk=vio.violationnumber_pk                  
  --      
  where vio.violationtype<>1 --and (t.activeflag=1 or t.activeflag=0) and casetypeid=1              
  '                
	    
	    IF @isDefault = 1
	        SET @strSQL = @strSQL + ' and t.calculatedtotalfee > 0  ' 
	    
	    
	    --if @FilterType = 0
	    -- set @strSQL=@strSQL+ ' and t.activeflag=1  '
	    --else if @FilterType = -1
	    -- set @strSQL=@strSQL+ ' and (t.activeflag=1 or t.activeflag=0)'
	    --else
	    -- set @strSQL=@strSQL+ ' and t.activeflag=0'                
	    
	    
	    -- SEARCHING WITH TICKETNUMBER.........            
	    IF (@TicketNumber <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.refcasenumber like @TicketNumber' 
	    
	    -- tahir 4167 06/03/2008
	    -- SEARCHING WITH LID AND CAUSE NUMBER OF ADVANCE SECTION      
	    IF (@LIDad <> '')
	        SET @strWhere = @strWhere + ' and t.mailerId =  @LIDad'      
	    
	    IF (@Causenumberad <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CaseNumAssignedByCourt like  @Causenumberad' 
	    
	    -- SEARCHING WITH ONE KEY WORD......            
	    IF (@SearchKeyType1 = 0 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.refcasenumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 1 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 2 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord1'                 
	    
	    IF (@SearchKeyType1 = 3 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.midnum like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 4 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  = @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 5 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and ( t.contact1 like @SearchKeyWord1 or  t.contact2 like @SearchKeyWord1 or t.contact3 like @SearchKeyWord1  )'                
	    
	    IF (@SearchKeyType1 = 6 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdatemain,101) = @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 7 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord1' 	    
	    
	    -- tahir 4167 06/03/2008          
	    IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.mailerid = convert(int, @SearchKeyWord1)'                
	    
	    IF (@SearchKeyType1 = 10 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CaseNumAssignedByCourt like   @SearchKeyWord1 '          
	    	  	   
	   
	     --Nasir 5863 05/05/2009 for hire date
	   IF (@SearchKeyType1 = 11 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            '  AND ttp.RecDate IN (SELECT MIN(ttp2.RecDate) FROM tblTicketsPayment ttp2 WHERE ttp2.PaymentVoid=0  GROUP BY ttp2.TicketID HAVING DATEDIFF(DAY ,MIN(ttp2.recdate) ,@SearchKeyWord1)=0) and t.ActiveFlag=1 '  
	        
	        --Nasir 5863 05/05/2009 for payment date
	  IF (@SearchKeyType1 = 12 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            '  and DATEDIFF(DAY,ttp.recdate,@SearchKeyWord1)=0 and t.ActiveFlag=1 '  	            	    
	            
	            --Nasir 5863 05/05/2009 for contact date
	  IF (@SearchKeyType1 = 13 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            '  and DATEDIFF(DAY,t.LastContactDate,@SearchKeyWord1)=0 '  	            	 
	  
	  --Ozair 8982 03/18/2011 added type for Email (Outlook Addin)  
	  IF (@SearchKeyType1=14 and @SearchKeyWord1 <>'')             
		SET @strWhere = @strWhere + ' and t.Email like   @SearchKeyWord1 '     
	    
	    -- SEARCHING WITH 2ND KEY WORD.........            
	    IF (@SearchKeyType2 = 0 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.refcasenumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 1 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname LIKE @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 2 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 3 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.midnum like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 4 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 5 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and ( t.contact1 like @SearchKeyWord2 or  t.contact2 like @SearchKeyWord2 or t.contact3 like @SearchKeyWord2  )'                
	    
	    IF (@SearchKeyType2 = 6 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdatemain,101) like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 7 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord2' 
	    	    
	    
	    -- tahir 4167 06/03/2008  
	    IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.mailerid = convert(int, @SearchKeyWord2)'                
	    
	    IF (@SearchKeyType2 = 10 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CaseNumAssignedByCourt like  @SearchKeyWord2 '                
	    	        
	          --Nasir 5863 05/05/2009 for Hire date
	     IF (@SearchKeyType2 = 11 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            '  AND ttp.RecDate IN (SELECT MIN(ttp2.RecDate) FROM tblTicketsPayment ttp2 WITH(NOLOCK) WHERE ttp2.PaymentVoid=0 GROUP BY ttp2.TicketID HAVING DATEDIFF(DAY ,MIN(ttp2.recdate) ,@SearchKeyWord2)=0) and t.ActiveFlag=1 '  
	        
	        --Nasir 5863 05/05/2009 for payment date
	    IF (@SearchKeyType2 = 12 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            '  and DATEDIFF(DAY,ttp.recdate,@SearchKeyWord2)=0 and t.ActiveFlag=1 '
	            
	            --Nasir 5863 05/05/2009 for Contact date
	    IF (@SearchKeyType2 = 13 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            '  and DATEDIFF(DAY,t.LastContactDate,@SearchKeyWord2)=0 '
	    
	    --Ozair 8982 03/18/2011 added type for Email (Outlook Addin)
		IF (@SearchKeyType2=14 and @SearchKeyWord2 <>'')            
			SET @strWhere = @strWhere + ' and t.Email like  @SearchKeyWord2 '                

	    
	    -- SEARCHING WITH 3RD KEY WORD.........            
	    IF (@SearchKeyType3 = 0 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.refcasenumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 1 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname LIKE @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 2 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 3 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.midnum like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 4 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 5 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and ( t.contact1 like @SearchKeyWord3 or  t.contact2 like @SearchKeyWord3 or t.contact3 like @SearchKeyWord3  )'                
	    
	    IF (@SearchKeyType3 = 6 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdatemain,101) like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 7 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord3' 	    
	    
	    -- tahir 4167 06/03/2008  
	    IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.mailerid = convert(int, @SearchKeyWord3)'                
	    
	    IF (@SearchKeyType3 = 10 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CaseNumAssignedByCourt like  @SearchKeyWord3 '                
	    	    
	      
	      --Nasir 5863 05/05/2009 for Hire date
	     IF (@SearchKeyType3 = 11 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            '  AND ttp.RecDate IN (SELECT MIN(ttp2.RecDate) FROM tblTicketsPayment ttp2 WITH(NOLOCK) WHERE ttp2.PaymentVoid=0 GROUP BY ttp2.TicketID HAVING DATEDIFF(DAY ,MIN(ttp2.recdate) ,@SearchKeyWord3)=0) and t.ActiveFlag=1 '  
	        
	        --Nasir 5863 05/05/2009 for payment date
	    IF (@SearchKeyType3 = 12 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and DATEDIFF(DAY,ttp.recdate,@SearchKeyWord3)=0 and t.ActiveFlag=1 '
	            
	            --Nasir 5863 05/05/2009 for Contact date
	    IF (@SearchKeyType3 = 13 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and DATEDIFF(DAY,t.LastContactDate,@SearchKeyWord3)=0 '
	    
	    --Ozair 8982 03/18/2011 added type for Email (Outlook Addin)
		IF (@SearchKeyType3=14 and @SearchKeyWord3 <>'') 
			SET @strWhere = @strWhere + ' and t.Email like  @SearchKeyWord3 '       
	    
	    IF @courtid <> 0
	    BEGIN
	        IF @courtid = -1
	            SET @strWhere = @strWhere + 
	                '  and v.courtid in (3001,3002,3003) '
	        ELSE 
	        IF @courtid = -2
	            SET @strWhere = @strWhere + 
	                '  and v.courtid in (select courtid from tblcourts WITH(NOLOCK) where courtcategorynum = 2) '
	        ELSE
	            SET @strWhere = @strWhere + ' and v.courtid = @courtid '
	    END
	END 
	
	--------------------------------------------------------------------------------------------------
	--    NON CLIENT
	--------------------------------------------------------------------------------------------------                
	IF @FilterType = 2
	BEGIN
	    SET @strSQL = @strSQL + 
	        '     
  insert into @tempResult  
           
  SELECT   t.Lastname,                 
		   t.Firstname,              
		   v.causenumber,           
		   v.ticketnumber_pk ,            
			-- convert(varchar(10),v.violationnumber_pk) ,    -- Sarim 2664 06/04/2008                
		   t.address1 as address1,            
		   t.zipcode,            
		   t.Address1+'' ''+isnull(t.Address2,'''') as address,                 
		   t.midnumber as MIDNo,                
		   convert(varchar(10),t.DOB,101) as DOB,                 
		   convert(varchar(10), isnull(v.CourtDate,''01/01/1900'')  ,101) as CourtDate,                 
		   isnull(c.shortDescription,''N/A'') as CaseStatus,                
		   t.ticketnumber as TicketId,                
		   V.CourtLocation as CourtId, -- Sarim 2664 06/04/2008         
		   @IsLIDSearch as MailerIDFlag  , 2 as dbid,  
		   v.ViolationDescription as Matter,  
		   CRT.ShortName as Court,  
		   2,   
		   ''Traffic'' as CaseType

 FROM  dbo.tblDateType d WITH(NOLOCK)
	  INNER JOIN dbo.tblCourtViolationStatus c WITH(NOLOCK) ON d.TypeID = c.CategoryID                 
	  RIGHT OUTER JOIN dbo.tblTicketsarchive t WITH(NOLOCK) INNER JOIN dbo.tblTicketsViolationsarchive v WITH(NOLOCK)                
			ON  t.recordid = v.recordid and t.IsIncorrectMidNum = 0 ON  c.CourtViolationStatusID = v.ViolationStatusID      
	  --Added By Zeeshan To Show Court Detail  
	  Inner Join tblcourts CRT WITH(NOLOCK) On CRT.CourtID = V.CourtLocation  
	  -- tahir 4167 06/03/2008  
	  LEFT OUTER JOIN #MailerIDs AS m ON v.RecordID = m.recordid
	  --Added By Ozair, not get cases of violation type 1      
	  LEFT OUTER JOIN dbo.tblViolations vio WITH(NOLOCK) ON v.violationnumber_pk=vio.violationnumber_pk
	-- -- Noufil 4905 10/13/2008 Casetype added for criminal courts
 where t.clientflag=0 and vio.violationtype<>1 and crt.casetypeid=1
  ' 
	    
	    -- SEARCHING BY TICKET NUMBER........            
	    IF (@TicketNumber <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @TicketNumber' 
	    
	    -- tahir 4167 06/03/2008
	    -- SEARCHING WITH LID AND CAUSE NUMBER OF ADVANCE SECTION
	    
	    IF (@LIDad <> '')
	        SET @strWhere = @strWhere + ' and m.noteid = @LIDad'      
	    
	    IF (@Causenumberad <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.causenumber like  @Causenumberad' 
	    
	    -- SEARCHING BY 1ST KEY WORD...............            
	    IF (@SearchKeyType1 = 0 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 1 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 2 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 3 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 4 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 5 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.phonenumber like @SearchKeyWord1 '                
	    
	    IF (@SearchKeyType1 = 6 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 7 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord1' 
	    
	    -- tahir 4167 06/03/2008  
	    IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and m.noteid = convert(bigint, @SearchKeyWord1)'                
	    
	    IF (@SearchKeyType1 = 10 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.causenumber like  @SearchKeyWord1 ' 
	    
	    
	    
	    -- SEARCHING BY 2ND KEY WORD..........            
	    IF (@SearchKeyType2 = 0 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 1 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 2 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 3 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 4 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 5 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.phonenumber like @SearchKeyWord2 '                
	    
	    IF (@SearchKeyType2 = 6 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 7 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord2' 
	    
	    -- tahir 4167 06/03/2008  
	    IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and m.noteid = convert(bigint,@SearchKeyWord2)'                
	    
	    IF (@SearchKeyType2 = 10 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CauseNumber like   @SearchKeyWord2 ' 
	    
	    -- SEARCHING BY 3RD KEY WORD..........            
	    IF (@SearchKeyType3 = 0 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 1 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 2 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 3 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 4 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 5 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.phonenumber like @SearchKeyWord3 '                
	    
	    IF (@SearchKeyType3 = 6 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 7 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord3' 
	    
	    -- tahir 4167 06/03/2008  
	    IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and m.noteid = convert(bigint,@SearchKeyWord3)'                
	    
	    IF (@SearchKeyType3 = 10 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CauseNumber like @SearchKeyWord3 '   
	    
	    IF @courtid <> 0
	    BEGIN
	        IF @courtid = -1
	            SET @strWhere = @strWhere + 
	                '  and v.courtlocation in (3001,3002,3003) '
	        ELSE 
	        IF @courtid = -2
	            SET @strWhere = @strWhere + 
	                '  and v.courtlocation in (select courtid from tblcourts WITH(NOLOCK) where courtcategorynum = 2) '
	        ELSE
	            SET @strWhere = @strWhere + ' and v.courtlocation = @courtid '
	    END 
	    
	    
	    -- tahir 4167 06/03/2008
	    -- ALSO GET RECORDS FROM JIMS DATABASE IF SEARCHING BY LID.....  
	    IF @IsLIDSearch = 1
	    BEGIN
	        SET @strSQLTemp = @strSQLTemp + 
	            '      
  UNION  
        
  select distinct                 
  c.lastname as lastname,            
  c.firstname as firstname,            
  c.casenumber,        
  C.casenumber as CaseNumber,             
  --0 ,-- Sarim 2664 06/04/2008               
  c.address as address1,            
  c.zip as zipcode,            
  C.Address as Address,                
  isnull(L.span,c.bookingnumber) as MIDNo,                
  convert(varchar(10),DOB,101) as DOB,                
  convert(varchar(10), isnull(l.bookingdate,''01/01/1900'') ,101) as CourtDate,                
  ''Non Client'' as CaseStatus,                
  c.casenumber as TicketId,                
  3037 as CourtId,        
  @IsLIDSearch as MailerIDFlag  ,4 as dbid ,  
  C.ChargeWording as matter,  
  ''HCCC'' as court,  
  2,  
       (select top 1 casetypename from tbltickets t WITH(NOLOCK) inner join casetype ct WITH(NOLOCK) on t.casetypeid =ct.casetypeid where t.activeflag=0) as CaseType  
             
  
  FROM   
		JIMS.dbo.CriminalCases AS C WITH(NOLOCK)
	LEFT OUTER JOIN #MailerIDs m ON C.BookingNumber = m.RecordID   
	LEFT OUTER JOIN JIMS.dbo.Criminal503 AS L ON C.BookingNumber = L.BookingNumber
	where  c.clientflag=0 and disposition NOT in (''COMM'',''DADJ'',''DISP'',''DERR'',''PROB'') '  
	        
	        IF @LIDad <> ''
	            SET @strSQLTemp = @strSQLTemp + ' and m.noteid = @LIDad'                
	        
	        
	        IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	            SET @strSQLTemp = @strSQLTemp + 
	                ' and m.noteid = @SearchKeyWord1'                
	        
	        IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	            SET @strSQLTemp = @strSQLTemp + 
	                ' and m.noteid = @SearchKeyWord2'                
	        
	        IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	            SET @strSQLTemp = @strSQLTemp + 
	                ' and m.noteid = @SearchKeyWord3'
	    END
	END 
	
	--------------------------------------------------------------------------------------------------
	--    JIMS
	--------------------------------------------------------------------------------------------------                
	IF @FilterType = 4
	BEGIN
	    SET @strSQL = @strSQL + 
	        '    
  insert into @tempResult  
          
  select distinct                 
			c.lastname as lastname,            
			c.firstname as firstname,            
			c.casenumber,        
			C.casenumber as CaseNumber,             
			--0 , -- Sarim 2664 06/04/2008               
			c.address as address1,            
			c.zip as zipcode,            
			C.Address as Address,                
			isnull(L.span,c.bookingnumber) as MIDNo,                
			convert(varchar(10),DOB,101) as DOB,                
			convert(varchar(10), isnull(l.bookingdate,''01/01/1900'') ,101) as CourtDate,                
			''Non Client'' as CaseStatus,                
			c.casenumber as TicketId,                
			3037 as CourtId,        
			@IsLIDSearch as MailerIDFlag  ,4 as dbid ,  
			C.ChargeWording as matter,  
			''HCCC'' as court,  
			2,  
			''Criminal''  
             
 FROM JIMS.dbo.CriminalCases AS C WITH(NOLOCK)  
	LEFT OUTER JOIN #MailerIDs m ON C.BookingNumber = m.RecordID 
	LEFT OUTER JOIN JIMS.dbo.Criminal503 AS L ON C.BookingNumber = L.BookingNumber              
		where  c.clientflag=0 and disposition NOT in (''COMM'',''DADJ'',''DISP'',''DERR'',''PROB'') ' 
	    
	    -- SEARCHING BY TICKET NUMBER.......       
	    IF (@TicketNumber <> '')
	        SET @strWhere = @strWhere + ' and C.casenumber like @TicketNumber' 
	    
	    -- Search by Cause Number.  Adil 07/04/2008 2447  
	    IF (@Causenumberad <> '')
	        SET @strWhere = @strWhere + ' and C.casenumber like  @Causenumberad'      
	    
	    IF @LIDad <> ''
	        SET @strWhere = @strWhere + ' and m.noteid = @LIDad' 
	    
	    -- SEARCHING BY 1ST KEY WORD...................            
	    IF (@SearchKeyType1 = 0 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and C.casenumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 1 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and c.lastname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 2 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and c.firstname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 3 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and l.span like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 4 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),c.dob,101)  like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 6 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),l.bookingdate,101) like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 7 AND @SearchKeyWord1 <> '')
	        SET @strWhere = ' and c.bookingnumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and m.noteid = @SearchKeyWord1' 
	    
	    
	    
	    -- SEARCHING BY 2ND KEY WORD..................            
	    IF (@SearchKeyType2 = 0 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and C.casenumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 1 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and c.lastname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 2 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and c.firstname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 3 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and l.span like @SearchKeyWord2'        
	    
	    IF (@SearchKeyType2 = 4 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),c.dob,101)  like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 6 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),l.bookingdate,101) like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 7 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and c.bookingnumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and m.noteid = @SearchKeyWord2' 
	    
	    -- SEARCHING BY 3RD KEY WORD..................            
	    IF (@SearchKeyType3 = 0 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and C.casenumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 1 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and c.lastname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 2 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and c.firstname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 3 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and l.span like @SearchKeyWord3'        
	    
	    IF (@SearchKeyType3 = 4 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),c.dob,101)  like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 6 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),l.bookingdate,101) like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 7 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and c.bookingnumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and m.noteid = @SearchKeyWord3' 
	    -- 25/06/08 4227 Added By Adil
	    -- START OF TASK 4227 CHANGES --------------------------  
	    SET @strSQL = @strSQL + @strWhere 
	    -- ALSO SEARCH IN TRAFFIC DATABASE  
	    SET @strSQL = @strSQL + 
	        '     
  insert into @tempResult  
           
SELECT t.Lastname,                 
   t.Firstname,              
   v.causenumber,           
   v.ticketnumber_pk ,            
   -- convert(varchar(10),v.violationnumber_pk) ,    -- Sarim 2664 06/04/2008                
   t.address1 as address1,            
   t.zipcode,            
   t.Address1+'' ''+isnull(t.Address2,'''') as address,                 
   t.midnumber as MIDNo,                
   convert(varchar(10),t.DOB,101) as DOB,                 
   convert(varchar(10), isnull(v.CourtDate,''01/01/1900'')  ,101) as CourtDate,                 
   isnull(c.shortDescription,''N/A'') as CaseStatus,                
   t.ticketnumber as TicketId,                
   V.CourtLocation as CourtId, -- Sarim 2664 06/04/2008         
   @IsLIDSearch as MailerIDFlag  , 2 as dbid,  
   v.ViolationDescription as Matter,  
   CRT.ShortName as Court,  
   2,  
  ''Criminal''  
            
FROM    dbo.tblDateType d  WITH(NOLOCK)               
  INNER JOIN dbo.tblCourtViolationStatus c WITH(NOLOCK) ON  d.TypeID = c.CategoryID                 
  RIGHT OUTER JOIN dbo.tblTicketsarchive t WITH(NOLOCK)               
	INNER JOIN dbo.tblTicketsViolationsarchive v WITH(NOLOCK) ON  t.recordid = v.recordid ON  c.CourtViolationStatusID = v.ViolationStatusID      
   --Added By Zeeshan To Show Court Detail  
  Inner Join tblcourts CRT WITH(NOLOCK) On CRT.CourtID = V.CourtLocation  
-- tahir 4167 06/03/2008  
  LEFT OUTER JOIN #MailerIDs AS m ON v.RecordID = m.recordid
  --Added By Ozair, not get cases of violation type 1      
  LEFT OUTER JOIN dbo.tblViolations vio ON  v.violationnumber_pk=vio.violationnumber_pk
-- Noufil 4905 10/13/2008 Casetype added for criminal courts
-- Waqas 6791 11/17/2009 Not checking harcoded HCCC court now
where t.clientflag=0 and vio.violationtype<>1 -- and V.CourtLocation = 3037 
and casetypeid=2
  '
	    
	    SET @strWhere = '' 
	    -- SEARCHING BY TICKET NUMBER........            
	    IF (@TicketNumber <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @TicketNumber' 
	    
	    -- SEARCHING WITH LID AND CAUSE NUMBER OF ADVANCE SECTION       
	    IF (@LIDad <> '')
	        SET @strWhere = @strWhere + ' and m.noteid = @LIDad'      
	    
	    IF (@Causenumberad <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.causenumber like  @Causenumberad' 
	    
	    -- SEARCHING BY 1ST KEY WORD...............            
	    IF (@SearchKeyType1 = 0 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 1 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 2 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 3 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 4 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 5 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.phonenumber like @SearchKeyWord1 '                
	    
	    IF (@SearchKeyType1 = 6 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 7 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and m.noteid = convert(bigint, @SearchKeyWord1)'                
	    
	    IF (@SearchKeyType1 = 10 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.causenumber like  @SearchKeyWord1 ' 
	    
	    -- SEARCHING BY 2ND KEY WORD..........            
	    IF (@SearchKeyType2 = 0 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 1 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 2 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 3 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 4 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 5 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.phonenumber like @SearchKeyWord2 '                
	    
	    IF (@SearchKeyType2 = 6 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 7 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and m.noteid = convert(bigint,@SearchKeyWord2)'                
	    
	    IF (@SearchKeyType2 = 10 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CauseNumber like   @SearchKeyWord2 ' 
	    
	    -- SEARCHING BY 3RD KEY WORD..........            
	    IF (@SearchKeyType3 = 0 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.ticketnumber_pk like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 1 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 2 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 3 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 4 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 5 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and t.phonenumber like @SearchKeyWord3 '                
	    
	    IF (@SearchKeyType3 = 6 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 7 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and m.noteid = convert(bigint,@SearchKeyWord3)'                
	    
	    IF (@SearchKeyType3 = 10 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and v.CauseNumber like @SearchKeyWord3 '   
	    
	    IF @courtid <> 0
	    BEGIN
	        IF @courtid = -1
	            SET @strWhere = @strWhere + 
	                '  and v.courtlocation in (3001,3002,3003) '
	        ELSE 
	        IF @courtid = -2
	            SET @strWhere = @strWhere + 
	                '  and v.courtlocation in (select courtid from tblcourts WITH(NOLOCK) where courtcategorynum = 2) '
	        ELSE
	            SET @strWhere = @strWhere + ' and v.courtlocation = @courtid '
	    END 
	    
	    -- END OF TASK 4227 CHANGES-----------------  
	    
	    -- tahir 4167 06/03/2008
	    -- ALSO GET RECORDS FROM JIMS DATABASE IF SEARCHING BY LID.....  
	    IF @IsLIDSearch = 1
	    BEGIN
	        SET @strSQLTemp = @strSQLTemp + 
	            '      
  UNION  
        
  select distinct                 
  c.lastname as lastname,            
  c.firstname as firstname,            
  c.casenumber,        
  C.casenumber as CaseNumber,             
  --0 ,-- Sarim 2664 06/04/2008               
  c.address as address1,            
  c.zip as zipcode,            
  C.Address as Address,                
  isnull(L.span,c.bookingnumber) as MIDNo,                
  convert(varchar(10),DOB,101) as DOB,                
  convert(varchar(10), isnull(l.bookingdate,''01/01/1900'') ,101) as CourtDate,                
  ''Non Client'' as CaseStatus,                
  c.casenumber as TicketId,                
  3037 as CourtId,        
  @IsLIDSearch as MailerIDFlag  ,4 as dbid ,  
  C.ChargeWording as matter,  
  ''HCCC'' as court,  
  2,  
  ''Criminal'' as CaseType  
            
  FROM   
   JIMS.dbo.CriminalCases AS C WITH(NOLOCK)  
  LEFT OUTER JOIN  
   #MailerIDs m   
  ON C.BookingNumber = m.RecordID   
  LEFT OUTER JOIN  
   JIMS.dbo.Criminal503 AS L   
  ON C.BookingNumber = L.BookingNumber              
  
  where  c.clientflag=0   
  and disposition NOT in (''COMM'',''DADJ'',''DISP'',''DERR'',''PROB'') '  
	        
	        IF @LIDad <> ''
	            SET @strSQLTemp = @strSQLTemp + ' and m.noteid = @LIDad'                
	        
	        
	        IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	            SET @strSQLTemp = @strSQLTemp + 
	                ' and m.noteid = @SearchKeyWord1'                
	        
	        IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	            SET @strSQLTemp = @strSQLTemp + 
	                ' and m.noteid = @SearchKeyWord2'                
	        
	        IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	            SET @strSQLTemp = @strSQLTemp + 
	                ' and m.noteid = @SearchKeyWord3'
	    END
	END 
	
	--------------------------------------------------------------------------------------------------
	--   CIVIL CASE
	--------------------------------------------------------------------------------------------------                
	IF @FilterType = 5
	BEGIN
	    SET @strSQL = @strSQL + 
	        '        
  set rowcount 0  
  declare @table as table   
 (   
  id int,  
  firstname varchar(100) ,   
  Lastname varchar(100) ,   
  casenumber varchar(100),  
  CourtDate datetime,  
  CourtId int,  
  Matter varchar(500),  
  AddressDate Datetime  
  )   
  
Insert into @table ( id,firstname ,Lastname,casenumber,CourtDate,Courtid,Matter,AddressDate)  
 select    
  distinct      
   CC.childcustodyid ,  
   p.FirstName,  
   P.LastName,CC.CaseNumber,CC.NextSettingDate,           
   ChildCustodyCourtId,SettingTypeName,max(P.AddressDate)  
     
   from civil.dbo.ChildCustody CC WITH(NOLOCK)  
   join civil.dbo.ChildCustodySettingType CCST WITH(NOLOCK)
   on CC.ChildCustodySettingTypeId = CCST.ChildCustodySettingTypeId  
   Left Outer join civil.dbo.ChildCustodyParties P WITH(NOLOCK)
   on CC.ChildCustodyId = P.ChildCustodyID  
   inner join civil.dbo.ChildCustodyConnection C WITH(NOLOCK)
   on C.ChildCustodyConnectionid = P.ChildCustodyConnectionid  
  
   where CC.ClientFlag=0  
   and C.Canbemailed = 1   
   and isnull(P.IsCompany,0) = 0    
   
   group by   
  
   FirstName ,   
   LastName,  
   CC.casenumber,  
   CC.NextSettingDate,  
   ChildCustodyCourtId,  
   SettingTypeName,  
   CC.childcustodyid  
    
    
  insert into @tempResult  
    
 Select Distinct    
   
  T.LastName,  
  T.FirstName,  
  T.CaseNumber,  
  T.CaseNumber,  
  max(P.Address) as address1,            
  max(P.zipcode) as zipcode,            
  max(P.Address) as Address,                
  isnull(max(p.childcustodypartyid),-1) as MIDNo,        
  '''' as DOB,                
  convert(varchar(10), isnull(T.CourtDate,''01/01/1900'') ,101) as CourtDate,                
  ''Non Client'' as CaseStatus,                
  T.casenumber as TicketId,                
  CourtId, @IsLIDSearch as MailerIDFlag  ,5 as dbid ,  
  Matter,  
  -- tahir 4763 09/08/2008 default court ...  
  -- ''HC Cvl'' as court,  
   ''HC FAM AG'' as Court,  
  -- end 4763  
  2,    
  ''Family Law'' -- Noufil 4782 09/23/2008 Case type family Law added  
  
  from @table T   
  join civil.dbo.ChildCustodyParties P WITH(NOLOCK) 
  on   
  T.FirstName = P.FirstName   
  and T.LastName = P.LastName  
  and T.AddressDate = P.addressdate  
  inner join civil.dbo.ChildCustodyConnection C WITH(NOLOCK)
  on C.ChildCustodyConnectionid = P.ChildCustodyConnectionid  
  left outer join #mailerids m on m.recordid = p.childcustodypartyid  
  where  C.Canbemailed = 1    
      
' 
	    
	    -- SEARCHING BY TICKET NUMBER.......       
	    IF (@TicketNumber <> '')
	        SET @strWhere = @strWhere + ' and casenumber like @TicketNumber'                
	    
	    IF (@Causenumberad <> '')
	        SET @strWhere = @strWhere + ' and casenumber like @Causenumberad'                   
	    
	    IF (@LIDad <> '')
	        SET @strWhere = @strWhere + ' and m.noteid = @LIDad' 
	    
	    
	    -- SEARCHING BY 1ST KEY WORD...................            
	    IF (@SearchKeyType1 = 1 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and T.lastname like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 2 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and T.firstname like @SearchKeyWord1'    
	    
	    IF (@SearchKeyType1 = 0 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and casenumber like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 6 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),CourtDate,101) like @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 9 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and m.noteid =  @SearchKeyWord1'                
	    
	    IF (@SearchKeyType1 = 10 AND @SearchKeyWord1 <> '')
	        SET @strWhere = @strWhere + ' and CaseNumber like @SearchKeyWord1 ' 
	    
	    
	    -- SEARCHING BY 2ND KEY WORD..................            
	    
	    IF (@SearchKeyType2 = 1 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and T.lastname like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 2 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and T.firstname like @SearchKeyWord2'     
	    
	    IF (@SearchKeyType2 = 0 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and casenumber like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 6 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),CourtDate,101) like @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 9 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and m.noteid =  @SearchKeyWord2'                
	    
	    IF (@SearchKeyType2 = 10 AND @SearchKeyWord2 <> '')
	        SET @strWhere = @strWhere + ' and CaseNumber like @SearchKeyWord2 ' 
	    
	    -- SEARCHING BY 3RD KEY WORD..................            
	    IF (@SearchKeyType3 = 1 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and T.lastname like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 2 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and T.firstname like @SearchKeyWord3'   
	    
	    IF (@SearchKeyType3 = 0 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and casenumber like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 6 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + 
	            ' and convert(varchar(10),CourtDate,101) like @SearchKeyWord3'                
	    
	    IF (@SearchKeyType3 = 9 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and m.noteid =  @SearchKeyWord3'              
	    
	    IF (@SearchKeyType3 = 10 AND @SearchKeyWord3 <> '')
	        SET @strWhere = @strWhere + ' and CaseNumber like @SearchKeyWord3 '   
	    
	    
	    
	    
	    SET @strWhere = @strWhere + 
	        '        
   
 group by   
  T.LastName,   
  T.FirstName,  
  T.CourtDate,  
  T.Casenumber,  
  T.CourtID,  
  T.Matter  
set rowcount 250  
 '
	END 
	
	
	-------------------------------------------------------              
	
	-- Agha Usman 4480 - Rename ActiveFlag to isActive since it is in used by outlook addin  
	
	IF @FilterType = -1
	BEGIN
	    SET @strsql2 = 
	        '         
  
  select distinct  a.ticketnumber as casenumber,             
  a.lastname as lastname, a.firstname as firstname, causenumber as causenumber,  address1 as address1, zipcode as zipcode, address as address, ltrim(midno) as midno, dob as dob,             
  a.courtdate as courtdate, casestatus as casestatus, ticketid as ticketid, courtid as courtid, MailerIDFlag , dbid   , Matter , Court, ActiveFlag as isActive  ,CaseType   
  from  @tempResult a         
  order by [lastname], [firstname], [midno], [casenumber]            
  '
	END
	ELSE
	BEGIN
	    SET @strsql2 = 
	        '         
  
  select distinct  a.ticketnumber as casenumber,             
  a.lastname as lastname, a.firstname as firstname, causenumber as causenumber,  address1 as address1, zipcode as zipcode, address as address, ltrim(midno) as midno, dob as dob,             
  a.courtdate as courtdate, casestatus as casestatus, ticketid as ticketid, courtid as courtid,  MailerIDFlag , dbid   , Matter , Court, ActiveFlag as isActive ,CaseType  
  from  @tempResult a         
  order by [lastname], [firstname], [midno], [casenumber]            
  '
	END   
	
	SET @strSQL = @strSQL + @strWhere + @strSQLTemp + @strsql2 
	
	PRINT @strSQL 
	EXECUTE sp_executesql @strSQL, @strParamDef, @FilterType, @TicketNumber, @SearchKeyWord1, 
	@SearchKeyType1, @SearchKeyWord2, @SearchKeyType2, @LIDad, @Causenumberad , 
	@courtid ,@SearchKeyWord3, @SearchKeyType3 ,@IsLIDSearch 
	
	
	DROP TABLE #MailerIDs  
  





/******************  
    
BUSINESS LOGIC : This procedure is used to get cases info.  
  
LIST OF INPUT PARAMETERS :  
   
 @TicketID_PK : Ticket ID   
 
****************************/

--sp_helptext usp_hts_Get_casedispositioninfo  124577
ALTER procedure [dbo].[usp_HTS_Get_CaseDispositionInfo]      

 @TicketID_PK  int      
 
 AS      

begin        
SET NOCOUNT ON ;
SELECT  
		t.Lastname, 
		t.Firstname, 
		ISNULL(dt.Description, 'N/A') AS description, 
		dbo.fn_HTS_Get_CustomerInfoByMidnumber(t.TicketID_PK) AS CaseCount, 
		t.Midnum, 
		dt.TypeID, 
		ISNULL(tv.courtdatemain, '01/01/1900') AS currentdateset, 
		tv.courtnumbermain as CurrentCourtNum, -- Sarim 2664 06/04/2008   
		tv.CoveringFirmID AS FirmID,
		--Yasir Kamal 6073 04/24/2009 Null Courtid Bug fixed. 
		ISNULL(tv.CourtID,0) as CurrentCourtloc, -- Sarim 2664 06/04/2008   
		t.GeneralComments, 
		t.TrialComments
FROM      tblDateType AS dt WITH(NOLOCK) INNER JOIN
          tblCourtViolationStatus AS cvs WITH(NOLOCK) ON dt.TypeID = cvs.CategoryID RIGHT OUTER JOIN
          tblTickets AS t WITH(NOLOCK) INNER JOIN
          tblTicketsViolations AS tv WITH(NOLOCK) ON t.TicketID_PK = tv.TicketID_PK 
				ON cvs.CourtViolationStatusID = tv.CourtViolationStatusIDmain
WHERE     (t.TicketID_PK = @TicketID_PK )
      
 end  

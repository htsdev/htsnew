﻿/******       
Created by: Waqas      
Business Logic : Get all the CaseType and Covering firm against a ticket id provided       
     violation belongs to a criminical court      
     violation status is not "NO hire" and "Dispose"      
       
List of Parameters:      
 @TicketId      
 @TicketsViolationID        
        

List of Columns:      
 CaseTypeID - CaseType
 CoveringfirmID - Covering Attorney
 courtdatemain - Verified Court Date.
 CategoryID - Primary status.
 
******/      
 --  grant execute on [dbo].[USP_HTP_Get_CaseTypeCoveringFirmForSharepoint] to dbr_webuser      
 
ALTER PROCEDURE [dbo].[USP_HTP_Get_CaseTypeCoveringFirmForSharepoint]      
(       
 @TicketId int = NULL,      
 @TicketsViolationID int = NULL      
)      
AS       
	SELECT tv.TicketsViolationID
	      ,ISNULL(t.CaseTypeId,0) AS CaseTypeId
	      ,ISNULL(tv.CoveringFirmID ,3000) AS CoveringFirmID
	      ,Convert(varchar,ISNULL(tv.CourtDateMain,'1/1/1900'),101) AS courtdatemain -- Sabir Khan 6248 07/30/2009 columns added for checking future court date and status...
	      ,ISNULL(tcv.CategoryID,0) AS Category
	      ,PaidAmount = ISNULL( (SELECT Convert(int,ISNULL(SUM(ISNULL(chargeamount,0)),0)) FROM tblticketspayment WHERE ticketid = tv.TicketID_PK AND paymentvoid = 0),0)
	FROM   tbltickets t
	       INNER JOIN tblTicketsViolations tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       LEFT OUTER JOIN tblCourtViolationStatus tcv		--Sabir khan 6248 07/30/2009 Join has been added for court violation status...
	            ON  tv.CourtViolationStatusIDmain = tcv.CourtViolationStatusID	       
	       INNER JOIN tblCourts c
	            ON  c.CourtId = tv.CourtID
	       LEFT OUTER JOIN tblFirm f
	            ON  f.FirmID = tv.CoveringFirmId
	WHERE  
	        (@TicketId=-1 OR t.TicketId_pk=@TicketId) 
	       AND (
	               @TicketsViolationID=-1
	               OR tv.TicketsViolationID=@TicketsViolationID
	           ) 
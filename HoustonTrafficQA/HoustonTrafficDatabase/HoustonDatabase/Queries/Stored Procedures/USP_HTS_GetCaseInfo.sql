﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 4/20/2009 10:35:09 AM
 ************************************************************/

        
/*        
Alter By: Tahir Ahmed        
Date:   09/11/2008        
        
Business Logic:        
 The stored procedure is used to get client's personal and contact information         
 displayed Matter page.        
        
List of Input Parameters:        
 @TicketId: Case identification for the client.        
        
List of Output Columns:        
 TicketID_PK,                                                                    
 Firstname,                                                                   
 MiddleName,                                                                   
 Lastname,                                                                       
 Midnum,                                                                   
 Contact1,                                                                   
 ContactType1,                                                                                      
 DOB,                                                                 
 CurrentCourtNum,        
 CurrentCourtloc,        
 Datetypeflag,        
 LanguageSpeak,                                                                       
 GeneralComments,                                                                   
 BondFlag,                                                                   
 BondsRequiredflag,                                                      
 AccidentFlag,                                                                   
 CDLFlag,                                                                   
 BaseFeeCharge,                                                                   
 FastTrackFee,                                                                   
 ContinuanceAmount,                                                                   
 RerapAmount,                                                                   
 AS Adjustment,                                                                   
 Addition,                                                                   
 Extraadditonalcharge,                                                                   
 TotalFeeCharged,                                                                   
 Activeflag,          
 calculatedtotalfee,                                                                   
 FirmID,                                                                       
 CaseCount,                                                                   
 LockFlag,        
 email,                                 
 Paid,        
 SpeedingID,            
 InitialAdjustment,                                   
 InitialAdjustmentInitial  ,                                                            
 Addressconfirmflag         ,                                              
 YDS ,                                    
 dlstate,                                  
 email,                          
 Recordid   ,                       
 LateFlag,                      
 ContinuanceFlag  ,                    
 ContinuanceAmount,                
 paymentstatus ,              
 Followupflag,              
 Callbackdate,          
 Zip,          
 CaseTypeID,        
 IsTrafficAlert,        
 caseTypeName ,        
 LastGerenalcommentsUpdatedate,
 ContactID,
 IsContactIDConfirmed,
 isALR, Waqas 5864 07/08/2009
 HaveALRPriorAttorney,
 ALRPriorAttorneyType,
 ALRPriorAttorneyName, 
 CaseSummaryComments, 
 HaveALRPriorAttorney,
 CountyOfArrest, 
 ALROfficerName,
 ALRPriorAttorneyType,
ALRPriorAttorneyName,
ALROfficerName,
ALROfficerbadge,
ALRPrecinct,
ALROfficerAddress,
ALROfficerCity,
ALROfficerStateID,
ALROfficerZip,
ALROfficerContactNumber,
ALRIsIntoxilyzerTakenFlag,
ALRIntoxilyzerResult,
ALRBTOFirstName,
ALRBTOLastName,,
ALRBTOBTSSameFlag,
ALRBTSFirstName,,
ALRBTSLastName,
ALRArrestingAgency,
ALRMileage,
IsALRArrestingObservingSame,
ALRObservingOfficerName,
ALRObservingOfficerBadgeNo,
ALRObservingOfficerPrecinct,
ALRObservingOfficerAddress,
ALRObservingOfficerCity,
ALRObservingOfficerState,
ALRObservingOfficerZip,
ALRObservingOfficerContact,
ALRObservingOfficerArrestingAgency,
ALRObservingOfficerMileage,
IsALRHearingRequired,
ALRHearingRequiredAnswer
Occupation
Employer
IsUnemployed
@VehicleType
         
        
*/          
              
ALTER PROCEDURE --        USP_HTS_GetCaseInfo 109000                    

[dbo].[USP_HTS_GetCaseInfo] --212932
	@ticketid INT
AS
	--Nasir 5381 15/08/2008 update LastGerenalcommentsUpdatedate to getdate if null        
	DECLARE @LastGerenalcommentsUpdatedate1 DATETIME        
	SELECT @LastGerenalcommentsUpdatedate1 = LastGerenalcommentsUpdatedate
	FROM   tblTickets
	WHERE  TicketID_PK = @ticketid
	
	IF @LastGerenalcommentsUpdatedate1 IS NULL
	BEGIN
	    UPDATE tblTickets
	    SET    LastGerenalcommentsUpdatedate = GETDATE()
	    WHERE  TicketID_PK = @ticketid
	END        
	
	
	
	SELECT T.TicketID_PK,
	       ISNULL(T.Firstname, '') AS Firstname,
	       ISNULL(LEFT(T.MiddleName, 1), '') AS MiddleName,
	       ISNULL(T.Lastname, '') AS Lastname,
	       ISNULL(T.Midnum, '') AS Midnum,
	       ISNULL(T.Contact1, '') AS Contact1,
	       ISNULL(T.ContactType1, 0) AS ContactType1,
	       ISNULL(CONVERT(VARCHAR(12), DOB, 101), '') AS DOB,
	       0 AS CurrentCourtNum,	-- Sarim 2664 06/04/2008                                                               
	       0 AS CurrentCourtloc,	-- Sarim 2664 06/04/2008                                                                
	       0 AS Datetypeflag,	-- Sarim 2664 06/04/2008                                                              
	       ISNULL(T.LanguageSpeak, -1) as LanguageSpeak, -- Noufil 6138 08/17/2009 Set -- Choose -- by defalut if language is null
	       T.GeneralComments,
	       ISNULL(T.BondFlag, 2) AS BondFlag,
	       t.BondsRequiredflag,
	       ISNULL(T.AccidentFlag, 2) AS AccidentFlag,
	       ISNULL(T.CDLFlag, 2) AS CDLFlag,
	       ISNULL(T.BaseFeeCharge, 0) AS BaseFeeCharge,
	       ISNULL(T.FastTrackFee, 0) AS FastTrackFee,
	       ISNULL(T.ContinuanceAmount, 0) AS ContinuanceAmount,
	       ISNULL(T.RerapAmount, 0) AS RerapAmount,
	       ISNULL(CONVERT(VARCHAR, T.Adjustment), 0) AS Adjustment,
	       ISNULL(T.Addition, 0) AS Addition,
	       ISNULL(T.Extraadditonalcharge, 0) AS Extraadditonalcharge,
	       ISNULL(T.TotalFeeCharged, 0) AS TotalFeeCharged,
	       ISNULL(T.Activeflag, 0) AS Activeflag,
	       ISNULL(T.calculatedtotalfee, 0) AS calculatedtotalfee,
	       T.FirmID,
	       dbo.fn_HTS_Get_CustomerInfoByMidnumber(T.TicketID_PK) AS CaseCount,
	       ISNULL(T.LockFlag, 0) AS LockFlag,
	       ISNULL(T.Email, '') AS email,
	       Paid = (
	           SELECT ISNULL(SUM(ISNULL(p.ChargeAmount, 0)), 0)
	           FROM   tblticketspayment p
	           WHERE  p.ticketid = t.ticketid_pk
	                  AND p.paymentvoid = 0
	       ),
	        -- Abbas Qamar 10114 04-03-2012 Checking Fortbend,Montgoemry , HCCC criminal Courts
	       (
	           SELECT COUNT(*)
	           FROM   tblticketsviolations v
	                  JOIN tblcourts c
	                       ON  v.courtid = c.courtid
	           WHERE  c.iscriminalcourt = 1 AND c.Courtid IN (3037,3043,3036) AND c.InActiveFlag=0
	                  AND v.ticketid_pk = T.ticketid_pk
	       ) AS IsCriminalCourt,
	       ISNULL(t.DLNumber,'') AS DLNumber,
	       ISNULL(t.SSNumber,'') AS SSNumber,
	       ISNULL (t.SSNTypeId,'') AS SSNTypeId,
	       ISNULL (t.SSNOtherQuestion,'') AS SSNOtherQuestion,
	       -- End 10114 
	       ISNULL(T.SpeedingID, 0) AS SpeedingID,
	       ISNULL(T.InitialAdjustment, 0) AS InitialAdjustment,
	       ISNULL(('-' + T.InitialAdjustmentInitial), '') AS 
	       InitialAdjustmentInitial,
	       Addressconfirmflag,
	       ISNULL(t.YDS, 'N') AS YDS,
	       ISNULL(t.dlstate, 0) AS dlstate,
	       ISNULL(t.email, '') AS email,
	       --,
	       -- tahir 4788 09/11/2008 fixed the recordid bug...
	       --isnull(t.recordid,0) as Recordid   ,                       
	       Recordid = (
	           SELECT TOP 1 ISNULL(recordid, 0)
	           FROM   tblticketsviolations
	           WHERE  ticketid_pk = t.ticketid_pk
	       ),
	       ISNULL(t.lateflag, 0) AS LateFlag,
	       ISNULL(t.ContinuanceFlag, 0) AS ContinuanceFlag,
	       ISNULL(t.continuanceamount, 0) AS ContinuanceAmount,
	       -- Noufil 5618 04/03/2009 payment status column added
	       ISNULL(t.Paymentstatus,1) AS paymentstatus,
	       ISNULL(q.followupyn, 2) AS Followupflag,
	       ISNULL(q.callbackdate, '1/1/1900') AS Callbackdate,
	       --ozair 3717 on 04/12/2008          
	       ISNULL(zip, '') AS Zip,
	       --end ozair 3717                      
	       ISNULL(T.CaseTypeID, 1) AS CaseTypeID,	-- Sarim Ghani change isnull value from 0 to 1 july 03,2008        
	       ISNULL(istrafficalertsignup, 0) AS IsTrafficAlert,
	       ct.CaseTypeName AS caseTypeName,	-- Agha usman 4271 06/26/2008                                                
	       ISNULL(t.haspaymentplan, 0) AS HasPaymentPlan,	-- tahir 4786 10/08/2008        
	       ISNULL(t.LastGerenalcommentsUpdatedate, GETDATE()) AS 
	       LastGerenalcommentsUpdatedate,	--Nasir 5381 15/08/2008 for getting last update date         
	       ISNULL(T.ContactID_FK, '') AS ContactID,	--Nasir 5771 04/13/2009        
	       ISNULL(t.IsContactIDConfirmed, 0) AS IsContactIDConfirmed, -- Waqas 5771 04/15/2009
	       (SELECT CASE COUNT(tbv.TicketID_PK) WHEN 0 THEN 0 ELSE 1 end  
            FROM   tblticketsviolations tbv INNER JOIN tblViolations tv  
            ON tbv.ViolationNumber_PK = tv.ViolationNumber_PK  
            WHERE  ticketid_pk = t.ticketid_pk  
			AND tv.CategoryID = 28) AS isALR , -- Waqas 5864 06/26/2009 ALR Criminal changes
	       --Waqas 6342 08/24/2009 For criminal and family cases now
	       (SELECT CASE COUNT(tbv.TicketID_PK) WHEN 
	       (SELECT COUNT(tbv.TicketID_PK) 
            FROM   tblticketsviolations tbv INNER JOIN tblViolations tv  
            ON tbv.ViolationNumber_PK = tv.ViolationNumber_PK  
            WHERE  ticketid_pk = t.ticketid_pk ) THEN 1 ELSE 0 end  
            FROM   tblticketsviolations tbv INNER JOIN tblViolations tv  
            ON tbv.ViolationNumber_PK = tv.ViolationNumber_PK  
            WHERE  ticketid_pk = t.ticketid_pk  
			AND tv.CategoryID = 28) AS isALLALR, --If all violations are of ALR it will return true
	       HavePriorAttorney as HavePriorAttorney, 
	       ISNULL(PriorAttorneyType, '') AS PriorAttorneyType,
	       ISNULL(PriorAttorneyName, '') AS PriorAttorneyName,
	       ISNULL(CaseSummaryComments, '') AS CaseSummaryComments,
	       ISNULL(HavePriorAttorney, '') AS HavePriorAttorney,
	       --Yasir Kamal 7150 01/01/2010 get county of arrest and ALRcourt
	       ISNULL((	SELECT     TOP (1) isnull(tmc.CountyName,'')
					FROM         dbo.tbl_Mailer_County AS tmc 
					RIGHT OUTER JOIN   dbo.tblCourts AS tc ON tmc.CountyID = tc.CountyId 
					RIGHT OUTER JOIN   dbo.tblTicketsViolations AS ttv 
					LEFT OUTER JOIN   dbo.tblViolations AS tv ON ttv.ViolationNumber_PK = tv.ViolationNumber_PK AND tv.CategoryID = 28 ON tc.Courtid = ttv.CourtID
					where ttv.ticketid_pk = t.TicketID_PK AND tv.CategoryID = 28),'') AS CountyOfArrest
			,ISNULL((SELECT TOP(1) ttv.CourtID FROM tblTicketsViolations ttv INNER JOIN tblviolations tv ON ttv.ViolationNumber_PK = tv.ViolationNumber_PK where tv.CategoryID = 28 AND ttv.TicketID_PK = t.TicketID_PK),'') AS ALRCourt		
			,ISNULL(ALROfficerName, '') AS ALROfficerName
			,ISNULL(ALROfficerbadge, '') AS ALROfficerbadge
			,ISNULL(ALRPrecinct, '') AS ALRPrecinct
			,ISNULL(ALROfficerAddress, '') AS ALROfficerAddress
			,ISNULL(ALROfficerCity, '') AS ALROfficerCity
			,ISNULL(ALROfficerStateID, '0') AS ALROfficerStateID
			,ISNULL(ALROfficerZip, '') AS ALROfficerZip
			,ISNULL(ALROfficerContactNumber, '') AS ALROfficerContactNumber
			,ISNULL(ALRIsIntoxilyzerTakenFlag, '') AS ALRIsIntoxilyzerTakenFlag
			,ISNULL(ALRIntoxilyzerResult, '') AS ALRIntoxilyzerResult
			,ISNULL(ALRBTOFirstName, '') AS ALRBTOFirstName
			,ISNULL(ALRBTOLastName, '') AS ALRBTOLastName
			,ISNULL(ALRBTOBTSSameFlag, '') AS ALRBTOBTSSameFlag
			,ISNULL(ALRBTSFirstName, '') AS ALRBTSFirstName
			,ISNULL(ALRBTSLastName, '') AS ALRBTSLastName
			,ISNULL((Select TOP 1 tbtv.ArrestingAgencyName from tblticketsviolations tbtv where tbtv.ticketid_pk = @ticketid), '') AS ALRArrestingAgency
			,ISNULL(ALRMileage, '') AS ALRMileage
	       --Waqas 6342 8/13/2009 alr for observing officer	       
	        ,IsALRArrestingObservingSame AS IsALRArrestingObservingSame
	        ,ISNULL(ALRObservingOfficerName, '') AS ALRObservingOfficerName
			,ISNULL(ALRObservingOfficerBadgeNo, '') AS ALRObservingOfficerBadgeNo
			,ISNULL(ALRObservingOfficerPrecinct, '') AS ALRObservingOfficerPrecinct
			,ISNULL(ALRObservingOfficerAddress, '') AS ALRObservingOfficerAddress
			,ISNULL(ALRObservingOfficerCity, '') AS ALRObservingOfficerCity
			,ISNULL(ALRObservingOfficerState, '0') AS ALRObservingOfficerState
			,ISNULL(ALRObservingOfficerZip, '') AS ALRObservingOfficerZip
			,ISNULL(ALRObservingOfficerContact, '') AS ALRObservingOfficerContact
			,ISNULL(ALRObservingOfficerArrestingAgency, '') AS ALRObservingOfficerArrestingAgency
			,ISNULL(ALRObservingOfficerMileage, '') AS ALRObservingOfficerMileage
			--Asad Ali 8153 09/20/2010 remove isnull check b/c by it is setting by  defualt to no the ALR hearing Question that is not a correct behavior
			,IsALRHearingRequired
			,ISNULL(ALRHearingRequiredAnswer, '') AS ALRHearingRequiredAnswer
			--Waqas 6599 09/19/2009 occupation , employer, isunemployed, VehicleType
			--Waqas 6894 10/30/2009 calling occupation and employer from seperate tables
			,ISNULL((Select TOP 1 o.Occupation from Occupation o where o.OccupationID = T.OccupationID), '') AS Occupation
			,ISNULL((Select TOP 1 e.Employer from Employer e where e.EmployerID = T.EmployerID), '') AS Employer
			,ISNULL(IsUnemployed, '') AS IsUnemployed
			,ISNULL(VehicleType, '0') as VehicleType
			--Sabir 7110 12/09/2009 Get coverubg firm for Oscar client....
			,IsOscarClient = isnull((SELECT COUNT(TicketsViolationID) FROM tblticketsviolations 
									WHERE ticketid_pk = t.ticketid_pk
									AND ISNULL(coveringfirmid,0) != 3043
									AND CourtViolationStatusIDmain NOT IN (80,236)
						),0)
					
	       --Asad Ali 8153 09/20/2010 add column NODL 
	       ,NoDL
	       , ISNULL(T.ArraignmentStatusFlag, 0) AS ArraignmentStatusFlag, -- Rab Nawaz Khan 10330 07/17/2012 ArraignmentStatusFlag added in the select list. . . 
	       ci.CallerId, CASE WHEN LEN(ISNULL(ci.CallerId, '')) > 0 THEN CONVERT(BIT,0) WHEN ISNULL(ci.isUnknown, 0) = 0 THEN CONVERT(BIT, 1) ELSE ci.isUnknown END AS isUnknown -- Rab Nawaz Khan 11473 10/23/2013 Added the CallerId and Unknown columns. . . 
	       
	FROM   dbo.tblTickets T -- LEFT OUTER JOIN -- Sarim 2664 06/04/2008
	                        --         dbo.tblOfficer O ON T.OfficerNumber = O.OfficerNumber_PK   -- Sarim 2664 06/04/2008                                                                      
	       
	       LEFT OUTER JOIN tblviolationquote q
	            ON  t.ticketid_pk = q.ticketid_fk 
	                
	                -- TAHIR 4344 07/02/2008
	                -- BY DEFAULT IF CASE TYPE IS NOT AVAILABLE THEN, DISPLAY THE CASE AS TRAFFIC CLIENT....
	                --inner join CaseType ct on isnull(T.CaseTypeId,0) = ct.CaseTypeId-- Adil 06/30/2008 isnull function implemented for T.CaseTypeID        
	                
	       INNER JOIN CaseType ct
	            ON  ISNULL(T.CaseTypeId, 1) = ct.CaseTypeId-- Adil 06/30/2008 isnull function implemented for T.CaseTypeID
	            -- Rab Nawaz Khan 11473 10/12/2013 Join the tblCallerIdsFromClients Table. . . 
	       LEFT OUTER JOIN tblCallerIdsFromClients ci ON t.TicketID_PK = ci.ticketId
	WHERE  (T.TicketID_PK = @ticketid) 
	       
	       --order by t.quoteid desc    -- Sarim 2664 06/04/2008 
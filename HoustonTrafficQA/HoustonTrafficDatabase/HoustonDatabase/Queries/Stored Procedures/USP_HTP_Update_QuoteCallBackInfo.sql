﻿/******  
Created by:		    Fahad Muhammad Qureshi
Business Logic :    This procedure is used update the Quote call back Information.
TaskId:				6934     
List of Parameters: @QuoteID : QuoteID.
					 @TicketID_FK : TicketId associated with case.
					 @FollowUPID : FollowUp Id.
					 @FollowUPDate : FollowUp date.
					 @FollowUpYN:
					 @CallBackDate : call back date.
					 @CallBackTime : Call back time.
					 @EmpID : EmployeeId associated with the Employee. 
					 
******/
CREATE PROCEDURE [dbo].[USP_HTP_Update_QuoteCallBackInfo]
	@TicketID_FK VARCHAR(12),
	@FollowUPID INT,
	@FollowUpYN INT,
	@CallBackDate VARCHAR(12),	
	@EmpID INT
AS
	DECLARE @QuoteID NUMERIC(18, 0)
	SET @QuoteID=(SELECT tvq.QuoteID FROM tblViolationQuote tvq WHERE tvq.TicketID_FK=@TicketID_FK)
	
	UPDATE [dbo].[tblViolationQuote]
	SET    [FollowUPID] = @FollowUPID,
	       [FollowUpYN] = @FollowUpYN,
	       [CallBackDate] = @CallBackDate,
	       [LastContactDate] = GETDATE()
	WHERE  [QuoteID] = @QuoteID
	AND    TicketID_FK=@TicketID_FK

GO

GRANT EXECUTE ON [dbo].[USP_HTP_Update_QuoteCallBackInfo] TO dbr_webuser
GO
  
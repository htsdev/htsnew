SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_TicketNo_from_LetterID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_TicketNo_from_LetterID]
GO

  
      
      
CREATE procedure [dbo].[usp_hts_get_TicketNo_from_LetterID]       
(          
          
@LetterID int          
          
)          
          
as          
          
declare @temp table (recordid int, clientflag int, MailerIDFlag int)      
      
--insert into @temp       
-- SEARCH BY LETTER ID          
--Select recordid , clientflag ,0 as MailerIDFlag from tblticketsArchive where RecordID = @LetterID          
      
      
-- SEARCH BY MAILER ID      
insert into @temp       
SELECT n.recordid, t.clientflag, 1 as MailerIDFlag from tblticketsarchive t      
inner join tblletternotes n      
on n.recordid = t.recordid      
and n.noteid = @letterid      
--union      
--SELECT n.recordid, t.clientflag, 1 as MailerIDFlag  from tblticketsarchive t      
--inner join tblletternotes n      
--on n.recordid = t.recordid      
--inner join tblletternotes_detail d      
--on d.noteid = n.noteid      
--where n.noteid = @letterid      
      
      
select distinct top 1 recordid, clientflag, MailerIDFlag from @temp      
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


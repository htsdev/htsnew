USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Mailer_Send_Harris_Arraignment]    Script Date: 09/05/2011 23:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get data for HMC Judge letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee information who is printing the letter.
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	MidNumber:		MidNumber of the person associated with the case.
	CourtId:		Court identification associated with the violation.
	Abb:			Short abbreviation of employee who is printing the letter

*******/

-- usp_Mailer_Send_Harris_Arraignment 2, 20, 2, '4/17/2008,' , 3992, 1 ,0,0
      
ALTER proc [dbo].[usp_Mailer_Send_Harris_Arraignment]
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int ,
@isprinted bit                     
                                    
as                                    

-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                                                                                    
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
-- DECLARING & INITIALIZING THE VARIABLE FOR DYNAMIC SQL...                                    
declare @sqlquery varchar(max), @sqlquery2 varchar(max) , @lettername varchar(50) -- Rab Nawaz Khan 9682 10/04/2011 Added to make the single template file for Appearance mailers 
SET @lettername = 'APPEARANCE' 
Select @sqlquery =''  , @sqlquery2 = ''
                                            
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...                                                                    
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid, 
	count(tva.ViolationNumber_PK) as ViolCount,
	isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount  
into #temp2                                       
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 
INNER JOIN
	dbo.tblCourts 
ON 	ta.CourtID = dbo.tblCourts.Courtid 

-- ONLY VERIFIED AND VALID ADDRESSES
where  	Flag1 in (''Y'',''D'',''S'') 

-- EXCLUDE BOND CASES.....
and		datediff(day, isnull(tva.bonddate,''1/1/1900'') , ''1/1/1900'')=0

-- NOT MARKED AS STOP MAILING...
and 	donotmailflag = 0 

-- SHOULD BE NON-CLIENT
and 	ta.clientflag=0

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0  

-- ONLY FUTURE COURT APPEARANCE OR COURT DATE IS NOT AVAILABLE.....
and ( datediff(day, tva.courtdate,''1/1/1900'') = 0 or datediff(day, tva.courtdate, getdate()) < 0 )

-- ONLY ARRAIGNMENT, APP, PIW & ESCALATION CASES....
and tva.violationstatusid  in (3,32,88,127)

-- EXCLUDE JUVENILE CASES.......
and isnull(tva.juvenileflag,0) = 0
'                                      

-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED CATEGORY.....
if( @crtid=@catnum )-- Adil 8928 02/17/2011 HCJP 7-1(3019) Excluded from Printing.
	set @sqlquery=@sqlquery+' 
	and 	ta.courtid In (Select courtid from tblcourts where courtcategorynum = '+convert(varchar,@catnum)+' and inactiveflag = 0  and courtid <> 3019)'

-- PRINTING FOR AN INDIVIDUAL COURT OF THE SELECTED CATEGORY...
else 
	set @sqlquery =@sqlquery +'                                  
	and 	ta.courtid =  '  + convert(varchar(10),@crtid) + ' and ta.courtid <> 3019 '
                                  
-- ONLY FOR THE SELECTED DATE RANGE.....
if(@startListdate<>'')                                    
	set @sqlquery=@sqlquery+'
	and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery +'  
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
              
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                    
if(@ticket_no<>'')                                    
	set @sqlquery=@sqlquery+ '
	and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
	set @sqlquery =@sqlquery+ '
	and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr  = 'not'  )              
	set @sqlquery =@sqlquery+ '
	and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   

-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                                           
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                  

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (                                                                    
	select	recordid from tblletternotes 
	where 	lettertype ='+convert(varchar(10),@lettertype)+'   
	)

group by ta.recordid having  1=1'     

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)              
	set @sqlquery =@sqlquery+ '
	and  ('+ @singlevoilationOpr+ '  (isnull(sum(isnull(tva.fineamount,0)),0)  '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))              
    '              

-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )              
	set @sqlquery =@sqlquery + '
	and (   '+ @doublevoilationOpr+ '   (isnull(sum(isnull(tva.fineamount,0)),0)  '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)<=2) )                                   
    '                                  
              
-- GETTING THE CLIENT'S PERSONAL & VIOLATION INFORMATION 
-- IN TEMP TABLE FOR RECORDS FILTERED IN THE ABOVE QUERY...
set @sqlquery =@sqlquery+ '
Select	distinct ta.recordid, 
	convert(datetime , convert(varchar(10),ta.listdate,101)) as listdate, 
 	ta.courtid, 
 	ta.officerNumber_Fk,                                          
 	case when len(isnull(tva.causenumber,''''))>0 then tva.causenumber else tva.TicketNumber_PK end as TicketNumber_PK,
  	upper(firstname) as firstname,
  	upper(lastname) as lastname,
  	upper(ta.address1) + '' '' + isnull(ta.address2,'''') as address,                      
  	upper(ta.city) as city,
  	s.state,
  	dbo.tblCourts.CourtName,
  	zipcode,
 	midnumber,
 	dp2 as dptwo,
 	dpc,                
 	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription,
	-- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . . 
 	-- case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,                                   
 	isnull(tva.fineamount,0) as FineAmount, 
	a.ViolCount,
	a.Total_Fineamount 
into #temp
FROM    dbo.tblTicketsViolationsArchive tva , dbo.tblTicketsArchive ta, dbo.tblCourts , #temp2 a, tblstate s
where  	tva.RecordID = ta.RecordID and ta.stateid_fk = s.stateid
and	ta.CourtID = dbo.tblCourts.Courtid 
and 	a.recordid = ta.recordid
and tva.violationstatusid  in (3,32,88,127)
and ( datediff(day, tva.courtdate,''1/1/1900'') = 0 or datediff(day, tva.courtdate, getdate()) < 0 )
and isnull(tva.juvenileflag,0) = 0
'
                                  
-- VIOLATION DESCRIPTION FILTER.....
-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
if(@violadesc <> '')
	BEGIN
		-- NOT LIKE FILTER.......
		if(charindex('not',@violaop)<> 0 )              
			BEGIN
				set @sqlquery=@sqlquery+'
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
				and	 not (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           		
			END
			-- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
			-- LIKE FILTER.......
			--if(charindex('not',@violaop) =  0 )              		
		ELSE
			BEGIN
				set @sqlquery=@sqlquery+'
				-- Rab Nawaz Khan 9659 09/06/2011 Filter Setting has been changed 
				and	 (  ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           			
			END
	END


-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....                        
set @sqlquery=@sqlquery+'

Select distinct recordid, ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid, zipcode, midnumber 
 into #temp1  from #temp                                    
 
 -- Rab Nawaz Khan 9704 09/28/2011 added to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 

 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1 WHERE ISNULL(FineAmount,0) = 0   
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid   
 
 '              
              

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....
if( @printtype<>0)    
	begin     
		set @sqlquery2 =@sqlquery2 +                                    
		'
			Declare @ListdateVal DateTime,                                  
				@totalrecs int,                                
				@count int,                                
				@p_EachLetter money,                              
				@recordid varchar(50),                              
				@zipcode   varchar(12),                              
				@maxbatch int  ,
				@lCourtId int  ,
				@dptwo varchar(10),
				@dpc varchar(10)
			declare @tempBatchIDs table (batchid int)

			-- GETTING TOTAL LETTERS AND COSTING ......
			Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
			Select @count=Count(*) from #temp1                                

			if @count>=500                                
				set @p_EachLetter=convert(money,0.3)                                
		                                
			if @count<500                                
				set @p_EachLetter=convert(money,0.39)                                

			-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
			-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
			Declare ListdateCur Cursor for                                  
			Select distinct mdate  from #temp1                                
			open ListdateCur                                   
			Fetch Next from ListdateCur into @ListdateVal                                                      
			while (@@Fetch_Status=0)                              
				begin           
					-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE ......	                     
					Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

					-- INSERTING RECORD IN BATCH TABLE......                               
					insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
					( '+convert(varchar(10),@empid)+' , '+convert(varchar(10),@lettertype)+' , @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                   

					-- GETTING BATCH ID OF THE INSERTED RECORD.....
					Select @maxbatch=Max(BatchId) from tblBatchLetter   
					insert into @tempBatchIDs select @maxbatch                                                    

					-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....                                                 
					Declare RecordidCur Cursor for                               
					Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
					open RecordidCur                                                         
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
	
					-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...					
					while(@@Fetch_Status=0)                              
					begin                              
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
					close RecordidCur 
					deallocate RecordidCur                                                               
					Fetch Next from ListdateCur into @ListdateVal                              
				end                                            

			close ListdateCur  
			deallocate ListdateCur 

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc, 
		-- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
		dptwo, TicketNumber_PK, t.zipcode, t.midnumber , t.courtid     , '''+@user+''' as Abb, isFineAmount,
		 '''+@lettername +''' as lettername, 0 as promotemplate, t.midnumber as midnum, t.zipmid, t.mdate as courtdate  
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by [recordid]

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500), @str varchar(2000)
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
		set @str = char(13)+char(13)
		select  @str = @str +  c.shortcourtname + '' - '' + convert(varchar(20),  count(distinct n.noteid)) + '' Letters''+char(13)
		from tblletternotes n, @tempBatchIDs b , tblcourts c
		where n.batchid_fk = b.batchid and c.courtid = n.courtid group by c.shortcourtname
		select @subject = convert(varchar(20), @lettercount) + '' LMS Harris County Appearance Letters Printed''
		select @body = ''TOTAL ''+ convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')+ @STR
		exec usp_mailer_send_Email @subject, @body

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'
	end
else
	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	begin
		set @sqlquery2 = @sqlquery2 + '
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,  LastName,address,city, state,FineAmount, 
		-- Rab Nawaz Khan 9682 10/14/2011 Columns added for the global templates for HMC mailers  
		violationdescription,CourtName, dpc, dptwo, TicketNumber_PK, zipcode , midnumber , courtid   , '''+@user+''' as Abb, isFineAmount,
		'''+@lettername +''' as lettername, 0 as promotemplate, midnumber as midnum, zipmid, mdate as courtdate  
		from #temp1 
		order by recordid 
		'
	end

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '

drop table #temp; drop table #temp1'
                                    
--print @sqlquery 
--print @sqlquery2

-- EXECUTING THE DYNAMIC SQL ....
set @sqlquery = @sqlquery + @sqlquery2
exec (@sqlquery)



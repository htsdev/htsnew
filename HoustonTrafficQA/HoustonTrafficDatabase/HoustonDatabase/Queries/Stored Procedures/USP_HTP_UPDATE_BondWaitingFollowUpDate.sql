/**
-- Noufil 4980 10/31/2008 
Business Logic : This function update the Bond Waiting Follow Up Date
Parameter : @ticketid_pk : TicketID of Client
			@date : Bond Waiting Follow up Date which will be updated
			@empid: ID of Employee
**/



Create procedure [dbo].[USP_HTP_UPDATE_BondWaitingFollowUpDate]

@ticketid_pk int,
@date datetime,
@empid int

as
update tbltickets 
	set bondwaitingFollowUpdate=@date
	where ticketid_pk=@ticketid_pk

go
grant exec on [dbo].[USP_HTP_UPDATE_BondWaitingFollowUpDate] to dbr_webuser
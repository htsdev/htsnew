SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_HTS_GET_ContinuanceAmount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_HTS_GET_ContinuanceAmount]
GO

Create procedure [dbo].[Usp_HTS_GET_ContinuanceAmount]

as

declare @amount int 

Select @amount = Amount from tblspeeding where description = 'Continuance'


select isnull(@amount,50) as amount



-----------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


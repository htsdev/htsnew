SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_ServiceTicketAlerts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_ServiceTicketAlerts]
GO


    
CREATE procedure [dbo].[usp_hts_get_ServiceTicketAlerts]          
          
@EmpID as int          
          
as          
                  
              
  select count(ticketid_pk) as servicecount , (select isnull(serviceticketalertdate,'01/01/1900') from tblusers where employeeid = @EmpID ) as serviceticketalertdate  from tblticketsflag f             
  where            
  (flagid=23) and (isnull(continuancestatus,0) <> 100) and (f.assignedto=@EmpID)         
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


--usp_htp_get_AllMailerLetter 134161  
  
--Kazim 3/12/2008 This Procedure is used to get all the letters from a particular client  
--  
--  
alter procedure [dbo].[usp_htp_get_AllMailerLetter]  
@Ticketid int   
as  
  
select distinct l.lettername [MailerType] , dbo.fn_DateFormat(recordloaddate, 30, '/', ':', 1) [MailDate] 
from tblticketsviolations as tv    
inner join tblletternotes as ln on tv.recordid = ln.recordid inner join  
tblLetter AS l ON ln.LetterType = l.LetterID_PK  
where tv.ticketid_pk = @Ticketid  
  

  
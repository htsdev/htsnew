/*    
* Created By		: SAEED AHMED  
* Task ID			: 7844   
* Created Date		: 06/01/2010    
* Business logic	: This procedure is used update followup date for address validation report against ContactId which is pass as an input parameter. 
* Parameter List    
* @FollowUpDate		: Represents followup date for bad email address which need to be set.  
* @TicketID			: Represents TicketID against the updation will perform.  
* @empid			: Represents EmployeeID which need to be set.  
* @ContactID		: Represents ContactID against which the updation is performed
*  
*/           
CREATE PROCEDURE [dbo].[USP_HTP_Update_AddressValidationFollowupDate]  
 @FollowUpDate DATETIME,  
 @TicketID INT,  
 @empid INT  ,
 @ContactID INT 
AS  

UPDATE tblTickets  SET 
AddressValidationFollowupDate = @FollowUpDate,  
EmployeeID = @empid  
WHERE  ContactID_FK=@ContactID 
GO
GRANT EXECUTE ON dbo.USP_HTP_Update_AddressValidationFollowupDate TO dbr_webuser
GO


 
   
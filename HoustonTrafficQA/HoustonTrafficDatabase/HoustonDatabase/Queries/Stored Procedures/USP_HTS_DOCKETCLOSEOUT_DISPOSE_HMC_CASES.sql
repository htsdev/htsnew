SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DOCKETCLOSEOUT_DISPOSE_HMC_CASES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DOCKETCLOSEOUT_DISPOSE_HMC_CASES]
GO


CREATE PROCEDURE USP_HTS_DOCKETCLOSEOUT_DISPOSE_HMC_CASES        
@CourtDate DATETIME,        
@EmpID int            
            
as             
        
declare @StatusID int        
        
set @StatusID = (SELECT top 1 CourtViolationStatusID from tblcourtviolationstatus where statustype=1 and ShortDescription LIKE('%DISP%'))        
        
UPDATE tblTicketsViolations            
 set            
  CourtViolationStatusID = @StatusID,            
  CourtViolationStatusIDMain = @StatusID,            
  CourtViolationStatusIDScan = @StatusID,            
  VEmployeeID = @EmpID          
WHERE TicketsViolationID IN (SELECT TicketsViolationID from tbl_hts_docketcloseout_snapshot where datediff(day,CourtDate,@CourtDate)=0 and CourtID IN (3001,3002,3003) and ShortDescription NOT LIKE('%DISP%') and (LastUpdate Is NULL or lastupdate ='' or lastupdate =' '))       
   
        
UPDATE tbl_hts_docketcloseout_snapshot        
 set LastUpdate = 'DISPOSED'        
 WHERE TicketsViolationID IN (SELECT TicketsViolationID from tbl_hts_docketcloseout_snapshot where datediff(day,CourtDate,@CourtDate)=0 and CourtID IN (3001,3002,3003) and ShortDescription NOT LIKE('%DISP%') and (LastUpdate Is NULL or lastupdate ='' or lastupdate =' '))    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


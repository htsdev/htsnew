﻿ 
/******   
Created by:  Noufil Khan  
Business Logic : this procedure gets the fid of case flag by case number and flag type
  
List of Parameters:   
	@ticketid : Case Number
	@flagid : 
List of Columns:    
	fid : Fid of Case Flag
  
******/      
Create procedure [dbo].[USP_HTP_Getflagid_byticketnumber] 
@ticketid int,  
@flagid int  

as  

declare @fid as  int

select  top 1 @fid = isnull(fid,-1)  from tblticketsflag where ticketid_pk=@ticketid and flagid=@flagid  
 
Select isnull( @fid ,-1) as fid
  
 Go
 
 grant execute on [dbo].[USP_HTP_Getflagid_byticketnumber] to dbr_webuser
 
 go
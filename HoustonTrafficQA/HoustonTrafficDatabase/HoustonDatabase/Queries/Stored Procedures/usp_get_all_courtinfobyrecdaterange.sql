

/****** 
Created by:		N/A
Business Logic:	The procedure is used by HTP/Backroom/Financial Report and HTP/Activities/Close out 
				report to get court wise number of Transactions and revenue generated. Courts are 
				grouped in 4 main Categogies
					1. Houston Municipal Court
					2. Harris County JP Courts
					3. Harris County Criminal Court
					4. Other Courts
				
List of Parameters:
	@RecDate:	Starting payment date selected on the financial/close out report.
	@RecTo:		Ending payment date selected on the financial/close out report.
	@FirmId:	Selected Firm's ID on the financial report.
	@branchId:	Branch id of the houston office.
List of Columns:	
	currentcourtloc:    Court ID of the court.
	CourtName:			Court Name that will be displayed on the report.
	Trans:				Total number of transactions for the court or category.
	Amount:				Total revenue generated for the court or category.
	IsCourtCategory		Flag to determine if it is a court category
	CategoryNumber:		Category id for each court house.
	SortOrder:			Sort order for each court house.

******/


ALTER procedure [dbo].[usp_Get_All_CourtInfoByRecDateRange]         
       
@RecDate DateTime,        
@RecTo DateTime,  
@firmId int = NULL,  
@BranchID int =1  -- Sabir Khan 10920 05/27/2013 Branch Id added.      
AS        
     
declare @temp table     
  (      
   id int identity,    
   courtid int,    
   courtcategory int,
   courtname varchar(100),    
   ticketid int,          
   chargeamunt money    
  )    
declare @temp2 table     
  (        
   currentcourtloc int,    
   CourtName varchar(100),    
   Trans varchar(1000),    
   Amount money ,
   IsCourtCategory tinyint ,
   CategoryNumber int,
   SortOrder int  
  )    
    
    
insert into @temp    
SELECT v.courtid as currentcourtloc,  
 c.courtcategorynum,     
 case c.courtcategorynum when 2 then c.shortname else c.CourtName end,       
 COUNT( p.ticketid) AS Trans,      
 SUM(p.ChargeAmount)  AS Amount        
FROM  dbo.tblCourts   c    
RIGHT OUTER JOIN        
 tblTicketsviolations v    
ON   c.Courtid = v.courtid    
RIGHT OUTER JOIN        
 tblTicketsPayment p    
ON   v.TicketID_PK = p.TicketID        
WHERE   (p.PaymentVoid <> 1)        
and (v.coveringFirmId = coalesce(@firmId,coveringFirmId))  
and  p.paymenttype not in (99,100)    
and datediff(day, p.recdate, @RecDate)<=0
and datediff(day, p.recdate, @RecTo)>=0
and  v.ticketsviolationid = (    
  select  min(ticketsviolationid)     
  from  tblticketsviolations     
  where ticketid_pk = v.ticketid_pk    
  and  courtid is not null 
and coveringFirmId = coalesce(@firmId,coveringFirmId)    
  )  
and p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@BranchID)) -- Sabir Khan 10920 05/27/2013 Branch Id check added.    
GROUP BY       
 v.courtid, c.courtcategorynum,      
 case c.courtcategorynum when 2 then c.shortname else c.CourtName end       
Order By        
 v.courtid    
    
-- GROUPING HMC COURTS....    
insert into @temp2 
select courtcategory,  'Houston Municipal Court',  isnull(sum(ticketid),0), isnull(sum(chargeamunt),0), 0,0, 0 
from @temp where courtcategory = 1 
group by courtcategory


-- GROUPING HCJP COURTS...
insert into @temp2 
select courtcategory,  'Harris County JP',  isnull(sum(ticketid),0), isnull(sum(chargeamunt),0), 1, 2, 1 
from @temp where courtcategory = 2
group by courtcategory

-- INSERTING INDIVIDUAL HCJP COURTS...
insert into @temp2 select courtid,courtname,ticketid,chargeamunt, 0, 2, 2 from @temp where courtcategory = 2   

-- INSERTING HARRIS COUNTY CRIMINAL COURT...
insert into @temp2 select courtid,courtname,ticketid,chargeamunt, 0,8, 3 from @temp where courtcategory = 8   

--ozair 4111 05/23/2008 null is not comapred in not in clause.
-- GROUPING ALL OTHER COURTS...
if exists (select courtid from @temp where isnull(courtcategory,0) not in (1,2,8))
begin
insert into @temp2 
select 100,  'Other',  isnull(sum(ticketid),0), isnull(sum(chargeamunt),0), 1, 100, 4 
from @temp where isnull(courtcategory,0) not in (1,2,8)

-- INSERTIG ALL OTHER COURTS...
insert into @temp2 
select courtid,courtname,ticketid,chargeamunt, 0,100, 5 from @temp where isnull(courtcategory,0) not in (1,2,8)
end
--end ozair 4111

select * from @temp2 
order by sortorder, courtname


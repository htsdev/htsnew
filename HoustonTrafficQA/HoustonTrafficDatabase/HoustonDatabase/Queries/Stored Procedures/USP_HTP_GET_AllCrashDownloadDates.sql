USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_GET_AllCrashDownloadDates]    Script Date: 08/27/2013 00:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created by		:		Sabir Khan
Task ID			:		11309
Business Logic	:		The Procedure is used to get all download dates of Texas Department of Transportation Information file. 
						
*******/
ALTER PROCEDURE [dbo].[USP_HTP_GET_AllCrashDownloadDates]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT  CONVERT(VARCHAR, tp.InsertDate, 101) AS DownloadDate
	FROM   Crash.dbo.tbl_Person tp
	       INNER JOIN Crash.dbo.tblTexasTransportationPersonOperators tpo
            ON  tpo.CrashID = tp.Crash_ID
	       INNER JOIN Crash.dbo.tbl_Crash TC
	            ON  tc.Crash_ID = tp.Crash_ID
	            AND TC.Case_ID = TPO.IncidentNumber
	            AND DATEDIFF(DAY, TC.CIDProcessLastDate, GETDATE()) = 0
	ORDER BY CONVERT(VARCHAR, tp.InsertDate, 101) DESC
	
	
END




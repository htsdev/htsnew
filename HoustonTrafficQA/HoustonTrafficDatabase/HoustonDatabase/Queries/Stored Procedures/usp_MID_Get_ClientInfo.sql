SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_MID_Get_ClientInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_MID_Get_ClientInfo]
GO


CREATE       procedure [dbo].[usp_MID_Get_ClientInfo] 
	(
	@RecordId	int,
	@CourtType	int
	)

as

if @CourtType = 1
	begin
		declare @MidNum	varchar(25)


		select 	@MidNum = midnumber from tblticketsarchive 
		where	recordid = @RecordId


		select	distinct 
			upper(isnull(t.lastname,'')+' '+t.firstname),
--			isnull(t.languagespeak,'-NA-'),
			upper(isnull(( select top 1  isnull(languagespeak,'-NA-') from tbltickets a
			  where a.midnum = t.midnum
			  and ( a.languagespeak is not null and len(a.languagespeak) > 0 )
			),'-NA-')),
			EstFee= convert(varchar(10),(
				select 50 * Count(*) from tblticketsviolationsarchive
				where	ticketnumber_pk in (
					select ticketnumber from tblticketsarchive a
					where a.midnumber=t.midnum
					)
				))
--			ContactNos = upper(isnull(replace(dbo.GetPhoneNo(t.midnum,@recordid),'<br>',','),'-NA-')),
--			CDLFlag = dbo.fn_MID_GetCDLFlag ('','','','',t.midnum,1)
		from 	tbltickets t
		where	t.midnum = @MidNum
	
	end

else if @CourtType=2
	begin
		select	firstname,
			lastname,
			address1,
			address2
		into 	#tmpclient
		from 	tblticketsarchive
		where 	recordid=@RecordId

		select	distinct
			upper(isnull(t.lastname,'')+' '+t.firstname),
--			isnull(t.languagespeak,'-NA-'),
			upper(isnull(( select top 1  isnull(languagespeak,'-NA-') from tbltickets a
			  where a.firstname =  t.firstname
			  and 	a.lastname =   t.lastname
			  and 	a.address1 = t.address1
			  and 	isnull(a.address2,'') = isnull(t.address2,'')
			  and ( a.languagespeak is not null and len(a.languagespeak) > 0 )
			),'-NA-')),
			EstFee= convert(varchar(10),(
				select 50 * Count(*) from tblticketsviolationsarchive
				where	ticketnumber_pk in (
					select ticketnumber from tblticketsarchive a
					where a.midnumber=t.midnum
					)
				))
--			ContactNos = upper(isnull(replace(dbo.GetPhoneNo(t.midnum,@recordid),'<br>',','),'-NA-')),
--			CDLFlag = dbo.fn_MID_GetCDLFlag (c.firstname, c.lastname, c.address1, c.address2, '',2)		
		from 	#tmpClient c
		inner join
			tbltickets t
		on	c.firstname=t.firstname
		and	c.lastname=t.lastname
		and	c.address1=t.address1
		and	isnull(c.address2,'') = isnull(t.address2,'')


		drop table #tmpClient

	end



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


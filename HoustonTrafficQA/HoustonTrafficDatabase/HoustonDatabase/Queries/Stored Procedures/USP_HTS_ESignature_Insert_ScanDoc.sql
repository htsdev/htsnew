SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Insert_ScanDoc]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Insert_ScanDoc]
GO








CREATE  PROCEDURE [dbo].[USP_HTS_ESignature_Insert_ScanDoc] --'124546', '3991', 9
@TicketID int,
@EmployeeID int,
@DocID nvarchar(50),
@DocMessage varchar(200),
@PageCount int 
AS
declare @BookID nvarchar(50)
declare @picID nvarchar(10)
Set @BookID = 0
insert into tblScanBook (EmployeeID,TicketID,DocTypeID,updatedatetime,DocCount,ResetDesc, Events)
values(@EmployeeID,@TicketID,@docID,GetDate(), @PageCount, @DocMessage, 'Sign') -- insert into book
      
select @BookID =  Scope_Identity() --Get Book Id of the latest inserted

insert into tblScanPIC (ScanBookID,updatedatetime,DocExtension) values( @BookID,getdate(),'PDF') --insert new image into database
select @picID =  Scope_Identity() --Get Book Id of the latest inserted
select @BookID + '-' + @picID









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


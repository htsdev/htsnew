SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_BondAmountConfirmFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_BondAmountConfirmFlag]
GO


--usp_helptext USP_HTS_ESignature_BondAmountConfirmFlag
--go

create procedure [dbo].[USP_HTS_ESignature_BondAmountConfirmFlag]
@TicketID int,
@Mode int -- 1 = Check, 2 = Insert, 3 = Delete the flag
as
If @Mode = 1
	Begin
		Select Count(TicketID_PK) as RecordCount from tblTicketsFlag Where TicketID_PK = @TicketID And FlagID = 30
	End
Else If @Mode = 2
	Begin
		if(Select Count(TicketID_PK) as RecordCount from tblTicketsFlag Where TicketID_PK = @TicketID And FlagID = 30)=0
			Begin
				Insert Into tblTicketsFlag (TicketID_PK, FlagID) Values(@TicketID, 30)
			End
	End
Else If @Mode = 3
	Begin
		Delete From tblTicketsFlag Where TicketID_PK = @TicketID And FlagID = 30
	End



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_BatchLetter_for_LetterTypes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_BatchLetter_for_LetterTypes]
GO






CREATE  Procedure [dbo].[USP_HTS_BatchLetter_for_LetterTypes]  --3992,1,2,0          
 (            
 @empid int,            
-- @ChkVal  int,            
 @letterType int ,            
-- @printed int,          
-- @filedate datetime ='01/01/1900',            
 @ticketidlist varchar(8000)  = '0'          
 )            
as         


    
-- CREATING TEMP TABLE TO STORE THE TICKET IDs                  
declare @tickets table (                  
 ticketid int                     
 )                
    
--Displaying records sorted when printed    
declare @ticketsort table    
(    
 ticketsort int,    
        batchid int     
)      
  
begin                  
 insert into @tickets                  
 select * from dbo.Sap_String_Param(@ticketidlist)     
 insert into @ticketsort     
   select  t.ticketid,hts.batchid_pk from @tickets t inner join tblhtsbatchprintletter hts    
  on t.ticketid=hts.ticketid_fk and hts.batchid_pk=(  select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                       
     where  tbv.ticketid_fk=hts.ticketid_fk and tbv.LetterID_Fk = @letterType and deleteflag<>1   )    
  --  where hts.LetterID_Fk = @letterType and deleteflag<>1     
end      
  
SELECT dbo.tblTickets.TicketID_PK,             
 dbo.tblTickets.MiddleName,             
 dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK,             
 dbo.tblTickets.Firstname,             
 dbo.tblTickets.Lastname,             
 dbo.tblTickets.Address1,             
 dbo.tblTickets.Address2,             
 dbo.tblTickets.City AS Ccity,             
 dbo.tblState.State AS cState,             
 dbo.tblTickets.Zip AS Czip,             
 dbo.tblTicketsViolations.CourtDate,             
 dbo.tblTicketsViolations.CourtNumber,             
 dbo.tblCourts.Address,             
 dbo.tblCourts.City,             
 dbo.tblCourts.Zip,             
 tblState_1.State,             
 dbo.tblTickets.Datetypeflag,             
 dbo.tblTickets.Activeflag,             
 isnull(dbo.tblTickets.LanguageSpeak,'ENGLISH') AS languagespeak,             
 dbo.tblTicketsViolations.SequenceNumber,             
 dbo.tblCourtViolationStatus.CategoryID,             
 dbo.tblTicketsViolations.TicketViolationDate,             
 dbo.tblViolations.Description,             
 dbo.tblDateType.Description AS CaseStatus,    
 ts.batchid            
FROM @ticketsort ts inner join    
    dbo.tblTickets             
on ts.ticketsort=dbo.tblTickets.TicketID_PK    
INNER JOIN            
 dbo.tblTicketsViolations             
ON  dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK             
INNER JOIN            
 dbo.tblCourts             
--ON  dbo.tblTickets.CurrentCourtloc = dbo.tblCourts.Courtid             
ON  dbo.tblTicketsviolations.courtid = dbo.tblCourts.Courtid             
INNER JOIN            
 dbo.tblState             
ON  dbo.tblTickets.Stateid_FK = dbo.tblState.StateID             
INNER JOIN            
 dbo.tblState tblState_1             
ON  dbo.tblCourts.State = tblState_1.StateID             
INNER JOIN            
 dbo.tblCourtViolationStatus             
ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID             
INNER JOIN            
 dbo.tblViolations             
ON  dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK             
INNER JOIN            
 dbo.tblDateType             
ON  dbo.tblCourtViolationStatus.CategoryID = dbo.tblDateType.TypeID            
WHERE   (dbo.tblTickets.Activeflag = 1)             
AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)             
AND     (dbo.tblCourtViolationStatus.CategoryID IN (2, 3, 4, 5))      
--and ts.ticketsort=tbltickets.ticketid_pk           
order by ts.batchid    








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


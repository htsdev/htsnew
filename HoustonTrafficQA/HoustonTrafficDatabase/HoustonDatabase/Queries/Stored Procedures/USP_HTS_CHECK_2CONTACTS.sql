﻿USE TrafficTickets
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/************************************************** 
Created by:		Farrukh Iftikhar
Created Date:	11/30/2011
Task ID:		9925     
Business Logic:	The procedure is used check at least 2 Contact Numbers of a particular ticket
				
List of Parameters:
	@TicketID:		Ticket ID for which to check contacts				
	
**************************************************/


CREATE PROCEDURE USP_HTS_CHECK_2CONTACTS
	@TicketID AS INT
AS
BEGIN
    SELECT contact1,
           contact2,
           contact3
    FROM   tbltickets
    WHERE  ticketid_pk = @TicketID
           AND (
                   (
                       contact1 IS NOT NULL AND contact1 NOT LIKE '' AND contact2 IS NOT NULL AND contact2 NOT LIKE ''
                   )
                   OR (
                          contact3 IS NOT NULL AND contact3 NOT LIKE '' AND contact1 IS NOT NULL AND contact1 NOT LIKE ''
                      )
                   OR (
                          contact3 IS NOT NULL AND contact3 NOT LIKE '' AND contact2 IS NOT NULL AND contact2 NOT LIKE ''
                      )
               )
END
GO




GRANT EXECUTE ON USP_HTS_CHECK_2CONTACTS TO dbr_webuser
GO
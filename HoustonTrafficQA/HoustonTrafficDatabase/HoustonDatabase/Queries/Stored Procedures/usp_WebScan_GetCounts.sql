SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_GetCounts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_GetCounts]
GO


-- usp_WebScan_GetCounts  '12/01/2006','12/28/2006'     
CREATE procedure [dbo].[usp_WebScan_GetCounts]     
@StarDate as datetime,    
@EndDate as datetime    
        
as   
       
        
/*declare @main table( batchid int , TotalCount int )        
declare @subtable table(batchid int,badcount int)        
declare @GoodCount table(batchid int,Goodcount int)        
        
Insert into @main Select batchid , count(id) as TotalCount from tbl_WebScan_Pic group by batchid        
insert into @subtable select p.batchid,count(l.picid) from tbl_WebScan_Pic p         
left outer join tbl_WebScan_Log l on p.id=l.picid group by p.batchid        
        
insert into @GoodCount select p.batchid,count(o.picid) from tbl_WebScan_Pic p         
inner join tbl_WebScan_OCR o on p.id=o.picid        
where o.picid not in (Select picid from tbl_WebScan_Log)        
group by p.batchid        
        
        
Select distinct b.batchid,c.TotalCount,b.scantype,convert(varchar(12),b.scandate,101) as scandate,isnull(s.badcount,0) as badcount,isnull(gc.goodcount,0) as goodcount      
from tbl_WebScan_Batch b inner join         
@main c on         
        
b.batchid=c.batchid inner join @subtable s        
on b.batchid=s.batchid left outer join @GoodCount gc        
on b.batchid=gc.batchid        
where     
 b.scandate between @StarDate and @EndDate    */


Select	
		distinct b.batchid,
		b.ScanType,
		convert(varchar(12),b.scandate,101)as scandate,
		TotalCount=(select count(p.id) from tbl_webscan_pic p where p.batchid=b.batchid )
from	tbl_webscan_batch b 
	where
		 b.scandate between @StarDate and @EndDate

	
    
        
        
        
        


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



/***********************
* 
Altered By: Noufil Khan
Dated:		04/27/2009

Business Logic:
	The procedure is used by Houston Docket Emails ervice Program to generate the Trial Docket.

Input Parameters:
	@courtloc:		Court id for the docket
	@courtdate:		docket date	
	@URL:			URL of the traffic program


**********************/

ALTER PROCEDURE [dbo].[USP_HTS_GET_ST_TrialDocket]                 
                 
@URL as varchar(100)='newpakhouston.legalhouston.com',  
@Courtloc varchar(10)='None',
@courtdate DATETIME ='01/01/1900'             
AS                
                
BEGIN   
SET NOCOUNT ON;
-- Noufil 5772 04/12/2009 New two sub-report added.
EXEC [dbo].[USP_HTP_GET_ArraingmentReport] @courtdate,@URL,@Courtloc  
EXEC [dbo].[USP_HTP_GET_JUDGEREPORT_FOR_TRIALDOCKET] @courtdate,@URL,@Courtloc
--Nasir 5817 04/28/2009 Add officer summary report
Exec [dbo].[USP_HTP_Get_Officer_Summary_Report_CR] @Courtloc,@courtdate,@URL           
                
select    
 distinct                  
 t.ticketid_pk,                 
 t.lastname,                
 t.firstname,                
 t.midnum,            
 dbo.fn_DateFormat(isnull(tv.courtdatemain,Convert(datetime,'1/1/1900')),27,'/','/',0) as courtDate ,                   
 dbo.fn_DateFormat(isnull(tv.courtdatemain,convert(datetime,'1/1/1900')),26,':',':',1) as courtTime,                               
  tv.refcasenumber,t.Casetypeid as CaseType, -- Sabir Khan 4527 08/25/2008          
 --Sabir Khan 7979 07/12/2010 fix trial docket issue...          
 case when isnull(tv.courtnumbermain,'0')!= '0' then ('#'+isnull(tv.courtnumbermain,'')) else '' end as CourtNumber,               
 isnull(tc.ShortDescription,'') as Status,            
 case when len(o.FirstName + o.LastName ) > 0 then o.FirstName + ' ' + o.LastName else 'N/A  N/A' end as Officer,            
          
            
-- Abid Ali 4946 undo changes
 case when len(t.trialcomments) > 0 then '<b>TRI:</b>' +(isnull(t.trialcomments,' ')) else ''end  as trialcomments,              
            
 (                
  select count(ticketsviolationid) from tblticketsviolations tv2 WITH(NOLOCK)                 
  where tv2.ticketid_pk = t.ticketid_pk                
 ) as violationcount,          
          
    
case            
 when             
 (            
 select count(*) from tblticketsflag WITH(NOLOCK) where flagid = 23 and ticketid_pk = t.ticketid_pk             
 ) > 0 then 'ST' else '' end             
          
               
 as flags , @URL as URL          
       
--Added By Zeeshan For Service Ticket Modification  
--,Case when  isnull( tf.showgeneralcomments,0) = 1 then CONVERT(VARCHAR(8000),t.generalcomments) else '' end as GeneralComments    
,Case when  isnull( tf.showgeneralcomments,0) = 1 then t.generalcomments else '' end as GeneralComments    
,Case when  isnull( tf.showserviceinstruction,0) = 1 then tf.serviceticketinstruction else '' end as ServiceTicketInstruction                  
,tv.casenumassignedbycourt as causenumber  
,tcrt.shortname as loc  
,  isnull(tc.ShortDescription,'')  + ' ' +  dbo.fn_DateFormat(isnull(tv.courtdatemain,Convert(datetime,'1/1/1900')),27,'/','/',0) + ' ' + dbo.fn_DateFormat(isnull(tv.courtdatemain,convert(datetime,'1/1/1900')),26,':',':',1)  + ' ' + Convert ( varchar , isnull(tv.courtnumbermain,'0')) as  Setting  , isnull(tu.lastname,'') + ' ' + isnull(tu.firstname,'') as attorney  
-- Added By Ozair For Task 2612 on 01/17/2008  
,tcrt.courtid  
into #temp  
--End Ozair  
from tbltickets t WITH(NOLOCK)                
 inner join tblticketsflag tf WITH(NOLOCK) on t.ticketid_pk = tf.ticketid_pk                
 inner join tblticketsviolations tv WITH(NOLOCK) on t.ticketid_pk =tv.ticketid_pk                
 inner join tblcourtviolationstatus tc WITH(NOLOCK) on tv.CourtViolationStatusID =tc.CourtViolationStatusID               
 inner join tblViolations tbs WITH(NOLOCK) on tv.ViolationNumber_PK = tbs.ViolationNumber_PK              
 inner join tblcourts tcrt WITH(NOLOCK) on tv.courtid = tcrt.courtid   
 left join tblofficer o WITH(NOLOCK) on tv.ticketofficerNumber = o.officernumber_pk            
 left outer join tblusers tu WITH(NOLOCK) on tu.employeeid = tf.assignedto    
where                            
  (tf.flagid =23 and isnull(tf.ShowTrailDocket,0) = 1 and isnull(tf.DeleteFlag,0) = 0)           
  and t.ActiveFlag = 1    
  and tv.ticketsviolationid = (select top 1 ticketsviolationid from tblticketsviolations WITH(NOLOCK) where ticketid_pk = t.ticketid_pk)              
  --added by ozair for court loc selection./ HCCC trial docket email report Task 2612     
order by t.ticketid_pk ,courtnumber         
--  and (datediff(DAY,tv.courtdatemain,@courtdate) = 0)               
--Sabir Khan 4527 08/25/2008 check rule for case type.
------------------------------------------------------               
if @Courtloc ='Traffic' or @courtloc = 'Criminal' or @courtloc = 'Civil'
Begin
if @courtloc = 'Traffic'
begin
Select * from #temp where CaseType =1
end
if @courtloc = 'Criminal'
begin
Select * from #temp where CaseType =2
end
if @courtloc = 'Civil'
begin
Select * from #temp where CaseType =3
end
END
------------------------------------------------------
ELSE  
BEGIN
-- Added By Ozair For Task 2612 on 01/17/2008  
 if @Courtloc is not null and @Courtloc <> 'None'     
 begin     
 if @Courtloc = 'JP' or @Courtloc = 'OU'     
  if @Courtloc = 'JP'     
  BEGIN
  	--Fahad 10378 01/09/2013 HMC-W alos excluded along other HMC Courts     
   Select * from #temp where courtid not in (3001,3002,3003,3075)     
  end     
  else     
  BEGIN
  	--Fahad 10378 01/09/2013 HMC-W alos Included along other HMC Courts     
   Select * from #temp where courtid in (3001,3002,3003,3075)     
  end     
 else  
 begin     
  Select * from #temp where courtid = @Courtloc     
 end     
 end  
 else  
 begin   
 Select * from #temp  
 end  
--End Ozair                
END
END     


/****** Object:  StoredProcedure [dbo].[USP_DTP_Update_NonHMCFollowUpdate]  ******/     
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/27/2009  
TasK		   : 6054        
Business Logic : This procedure is used to Add the Service Ticket Category of the client.
				         
Parameter:       
	@Description	: Wording Description of Service Category.
	@casetypeid		: Id of Category on basis of we are fetching information from the database.
	@AssignTo	    : This Field is showing that to which User the field is assign .
	
*/
Alter PROCEDURE [dbo].[USP_HTS_ADD_SERVICETICKET_CATEGORY]
	@Description VARCHAR(50),
	@casetypeid INT,
	@AssignTo INT
AS
	SET @Description = RTRIM(LTRIM(@Description))
	
	
	IF NOT EXISTS (
	       SELECT *
	       FROM   tblServiceTicketCategories
	       WHERE  DESCRIPTION = @Description
	   )
	BEGIN
	    INSERT INTO tblServiceTicketCategories
	      (
	        [Description],
	        AssignedTo	--Fahad 6054 07/22/2009 Add new Parameter "AssignedTo"
	      )
	    VALUES
	      (
	        @Description,
	        @AssignTo	--Fahad 6054 07/22/2009 Add new Parameter "@AssignTo"
	      )
	END
	
	DECLARE @servicecategoryid INT
	SET @servicecategoryid = (
	        SELECT MAX(id)
	        FROM   tblServiceTicketCategories
	        WHERE  DESCRIPTION = @Description
	    )
	
	IF NOT EXISTS (
	       SELECT *
	       FROM   CaseTypeServiceCategory
	       WHERE  casetypeid = @casetypeid
	              AND servicecategoryid = @servicecategoryid
	   )
	BEGIN
	    INSERT INTO CaseTypeServiceCategory
	      (
	        casetypeid,
	        servicecategoryid
	      )
	    VALUES
	      (
	        @casetypeid,
	        @servicecategoryid
	      )
	END
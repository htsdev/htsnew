SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_HarisCountyViolations]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_HarisCountyViolations]
GO

Create Procedure [dbo].[USP_HTS_Get_HarisCountyViolations]

as


SELECT     ViolationNumber_PK as ID, Description as Description        
FROM         dbo.tblViolations        
WHERE     ViolationNumber_PK in (0,15931,15932,15933,15934,15935,15936,15937,15938,15939)   
order by   ID 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


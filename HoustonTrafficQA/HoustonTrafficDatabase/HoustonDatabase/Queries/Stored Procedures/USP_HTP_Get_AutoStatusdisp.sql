

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[USP_HTP_Get_AutoStatusdisp] 

@TicketId_PK int              
                 
AS             

select 
tv.TicketId_Pk, tv.RefCaseNumber, tv.TicketsViolationID, 
tvLog.RecDate, cvs.Description, tvLog.NewAutoDate,tvLog.NewAutoRoom

from tblTicketsViolationLog tvLog
inner join tblTicketsViolations tv on tvLog.TicketViolationID = tv.TicketsViolationID
inner join tblCourtViolationStatus cvs on tvLog.NewAutoStatus = cvs.CourtViolationStatusId
Where ( (tvLog.NewAutoStatus != tvLog.OldAutoStatus)
	OR (tvLog.NewAutoDate != tvLog.OldAutoDate)
	OR (tvLog.NewAutoRoom != tvLog.OldAutoRoom)
	)
AND (tv.TicketId_Pk = @TicketId_PK and tvLog.RecDate = (Select max(recdate) from tblTicketsViolationLog where TicketViolationID = tv.TicketsViolationID))
order by RecDate desc

Go

Grant Execute ON dbo.USP_HTP_Get_AutoStatusdisp to dbr_webuser

Go
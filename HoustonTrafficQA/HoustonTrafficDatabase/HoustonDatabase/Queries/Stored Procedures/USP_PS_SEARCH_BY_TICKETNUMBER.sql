GO
/****** Object:  StoredProcedure [dbo].[USP_PS_SEARCH_BY_TICKETNUMBER]    Script Date: 04/30/2008 18:47:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
/***************  

CREATED BY		   : ZEESHAN AHMED
BUSINESS LOGIC	   : This Procedure Search the Ticket in Client and Non Client Cases   
LIST OF PARAMETERS :
LIST OF COLUMNS	   :			

*****************/          
Alter PROCEDURE [dbo].[USP_PS_SEARCH_BY_TICKETNUMBER] --'I107210923'        
(          
@TicketNumber varchar(20)          
)          
AS          
BEGIN          
          
           
if Len(@TicketNumber) > 1                        
 select @Ticketnumber = left(@Ticketnumber, len (@TicketNumber)-1 ) + '%'                     
          
-- Declare Temp Table                   
declare @temp table  (           
      RecordID int ,          
      TicketID int,          
      TicketNumber_Pk varchar(20),          
      Name varchar(50),          
      Address varchar(100),          
      CourtDate varchar(12)    
         
      )         




if ( Len(@TicketNumber) > 2)
Begin
	
		declare @Prefix as varchar(2)
		Set @Prefix = Left(@TicketNumber,2)
		
		
		if ( @Prefix = 'TR' Or @Prefix = 'BC' or @Prefix = 'CR')
		Begin
				
				Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate )           
				select   distinct top 100 isnull(v.recordid,0),0,v.causenumber, t.lastname + ',' + t.firstname,isnull(t.address1,'') + isnull(t.address2,''),                        
				isnull ( case when convert( varchar(12) , v.courtdate,101)='01/01/1900' then 'N/A' else convert( varchar(12) , v.courtdate,101) end   , 'N/A' ) as CourtDate       
				from  tblticketsviolationsarchive v                         
				inner join tblticketsarchive t on t.recordid = v.recordid                        
				and  v.causenumber like @ticketnumber                         
				and t.ClientFlag = 0
				and  v.violationstatusid != 80  
					
		End

End



      
      
      
                       
                  
-- Houston Search            
-- Search Ticket Number In NON Client Cases                  
Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate )           
select   distinct top 100 isnull(v.recordid,0),0,v.ticketnumber_pk, t.lastname + ',' + t.firstname,isnull(t.address1,'') + isnull(t.address2,''),                        
isnull ( case when convert( varchar(12) , v.courtdate,101)='01/01/1900' then 'N/A' else convert( varchar(12) , v.courtdate,101) end   , 'N/A' ) as CourtDate       
from  tblticketsviolationsarchive v                         
inner join tblticketsarchive t on t.recordid = v.recordid                        
and  v.ticketnumber_pk like @ticketnumber                         
and t.ClientFlag = 0
and  v.violationstatusid != 80                        
                          
        
-- Search Ticket In Quote Client                   
Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate  )                       
                    
Select  Distinct top 100  isnull(t.RecordID,0) , t.TicketID_Pk ,tv.refcaseNumber , t.lastname + ',' + t.firstname , isnull(t.address1,'') + isnull(t.address2,'')                  
, isnull ( case when convert( varchar(12) , tv.courtdate,101)='01/01/1900' then 'N/A' else convert( varchar(12) , tv.courtdate,101) end   , 'N/A' ) as CourtDate                
from tbltickets t                           
join tblticketsviolations  tv                          
on                           
t.ticketid_pk = tv.Ticketid_pk                          
where tv.refcaseNumber like  @TicketNumber + '%'                    
and t.activeFlag = 0 and tv.courtviolationstatusidmain != 80                        
                      
          
      
Select top 100 * from @temp               
          
END   


Go

Grant Execute on dbo.USP_PS_SEARCH_BY_TICKETNUMBER to dbr_webuser

Go
  
  

 
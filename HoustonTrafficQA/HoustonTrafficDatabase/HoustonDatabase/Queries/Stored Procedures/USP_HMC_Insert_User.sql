SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_Insert_User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_Insert_User]
GO




CREATE Procedure [dbo].[USP_HMC_Insert_User]      
 @Uid varchar(20),      
 @Pwd varchar(20),    
 @AccountType int,    
 @Status int,    
 @FName varchar(25),    
 @LName varchar(25),    
 @Email varchar(50),    
 @Description varchar(200),
 @Firm varchar(50),    
 @Val bit output     
as    
    
if not exists (select uid from tbl_hmc_users where uid = @uid)    
begin        
 Insert into tbl_HMC_Users ( Uid, Pwd, AccountType, Status, Fname, Lname, Email, Description, Firm )    
 Values(@Uid, @Pwd, @AccountType, @Status, @FName, @LName, @Email, @Description, @Firm )    
 set @Val = 1;    
end    
    
else    
begin    
 set @Val = 0    
end


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


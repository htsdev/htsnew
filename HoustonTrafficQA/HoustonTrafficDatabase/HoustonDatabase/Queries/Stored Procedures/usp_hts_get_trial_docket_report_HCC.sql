  /******       
Business Logic : This procedure is used for Attorneydockets.com, to list all the cases related to the login Attorney or User.  --Yasir Kamal 5559 02/23/2009 add option to select all cases.
      
Parameters :  
------------       
courtloc : This is Court Location   
Fromcourtdate : This is Start date of selection criteria  
Tocourtdate : This is end date of selection criteria  
page : not used yet  
courtnumber : This is Court Number of Court  
datetype :  not used yet  
singles : not used yet  
firmid  : This is Firm ID 
showAll : To show all cases regardless of date 
******/  
  
-- usp_hts_get_trial_docket_report_HCC '3008,3007,3011,3013,3014,3015,3016,3019,3020,3003,3001,3002,3004,', '03/15/2004', '03/15/2009','','','',0,3000,1
    
alter   procedure [dbo].[usp_hts_get_trial_docket_report_HCC]                                  
-- Abid Ali 5137 11/19/2008  
@courtloc varchar(max) = '3037,',                                    
@Fromcourtdate datetime = '01/01/1900',        
@Tocourtdate datetime = '01/01/1900',                                  
@page varchar(12) = 'TRIAL',                                    
@courtnumber varchar(6) = '-1',                                    
@datetype varchar(20) = '0',    
@singles int = 0,  
@firmid  int = 3000,
--Yasir Kamal 5559 02/23/2009 add option to select all cases.
@showAll int =0  
-- 5559 end
                              
as    
    
set @Tocourtdate = @Tocourtdate + '23:59:58'    
    
if @courtnumber = ''    
 begin    
  set @courtnumber ='-1'    
 end    
    
set nocount on                                    
    
SELECT      
 dbo.tblTickets.TicketID_PK ,   
 dbo.tblTickets.Firstname,   
 dbo.tblTickets.Lastname,  
 case   
 when  ltrim(casenumassignedbycourt) = ''  then    refcasenumber      
 else isnull(casenumassignedbycourt,refcasenumber) end as causenumber,  
 ' ' Bond,   
 dbo.tblTicketsViolations.courtdatemain,  
 dbo.tblcourts.shortname,  
 --dbo.tblTicketsViolations.courtnumbermain,  
 case when isnumeric(isnull(dbo.tblTicketsViolations.courtnumbermain,'0')) = 0 then 0   else convert(int, dbo.tblTicketsViolations.courtnumbermain) end as courtnumbermain,
 dbo.tblViolations.ShortDesc,        
 isnull(dbo.tblViolationCategory.ShortDescription,'N/A') as ViolationCategory,              
 dbo.tblcourtviolationstatus.ShortDescription as description,  
 dbo.tblCourts.Courtid   
into #temp   
FROM		dbo.tblTickets INNER JOIN  
			dbo.tblTicketsViolations ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK INNER JOIN  
			dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK INNER JOIN  
			dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID INNER JOIN  
			dbo.tblCourts ON dbo.tblTicketsViolations.CourtID = dbo.tblCourts.Courtid left outer JOIN  
			dbo.tblViolationCategory ON dbo.tblViolations.CategoryID = dbo.tblViolationCategory.CategoryId  
 WHERE     (dbo.tblViolations.Violationtype not IN (1)   
    or dbo.tblViolations.violationnumber_pk in (11,12) )  
    AND (dbo.tblcourtviolationstatus.categoryid   
      IN (select distinct categoryid from tblcourtviolationstatus where violationcategory in (0,2)  
    and categoryid not in (7,50)  
    and courtviolationstatusid not in (104) ))  
    AND (dbo.tblTickets.Activeflag = 1)  
    and dbo.tblTicketsViolations.courtid <> 0    
    -- ozair 5305 12/05/2008 included covering firm check
    -- Waqas 5461 02/06/2009 all cases for firmid 3000
    and (@firmid = 3000 or  isnull(dbo.tblTicketsViolations.coveringfirmid,3000)=@firmid )
    -- Abid Ali 5137 11/19/2008
    and dbo.tblTicketsViolations.courtid in (SELECT * FROM [dbo].[Sap_String_Param](@courtloc))                                
    --and datediff(DAY,dbo.tblTicketsViolations.courtdatemain,@courtdate) = 0  
	-- tahir 5007 10/21/2008 fixed the date comparision bug...
    --and dbo.tblTicketsViolations.courtdatemain between @Fromcourtdate and @Tocourtdate  
--Yasir Kamal 5559 02/23/2009 add option to select all cases.
and
(@showAll=1   or (datediff(Day, dbo.tblTicketsViolations.courtdatemain, @fromcourtdate) <= 0   and datediff(day, dbo.tblTicketsViolations.courtdatemain, @tocourtdate ) >= 0))
-- 5559 end    
--Waqas 5319 01/16/2009       
--if @@rowcount > 0    
--begin     
    
select DISTINCT ticketid, totalfeecharged-sum(ChargeAmount) as dueamount     
 into  #temp2    
from tblticketspayment A,tbltickets T where     
 T.ticketid_pk = A.ticketid     
 and activeflag = 1     
 and paymentvoid=0    
 and paymenttype not in (99,100)
 --Waqas 5319 01/16/2009   
 and T.ticketid_pk in (Select TicketID_PK from #temp)    
group by ticketid,totalfeecharged                                        
      
declare @sqlquery varchar(8000)                
    
                                  
set @sqlquery = ' '                                    
                
   
if @courtnumber = '-1'    
begin                  
  select T.*, case when dueamount > 0 then '$$' else '' end as fee    
   FROM #temp T                                   
    left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid    
  
   
 order by datediff(day,t.courtdatemain,getdate()) desc, t.courtnumbermain, t.Lastname , t.firstname                                  
end     
    
else     
begin     
 select T.*, case when dueamount > 0 then '$$' else '' end as fee    
   FROM #temp T                                   
    left outer join #temp2 T2 on T.ticketid_pk = T2.ticketid     
 where courtnumbermain = @courtnumber    
  
   
 order by datediff(day,t.courtdatemain,getdate()) desc, t.courtnumbermain, t.Lastname , t.firstname     
end     
 --Waqas 5319 01/16/2009      
--end    
    
  
  
  
  
  
﻿/****** 
Object:	StoredProcedure [dbo].[USP_HTS_Add_users]

Altered by:Fahad Muhammad Qureshi

Business Logic : 
			This report display all records of user in which user name,access ,status etc containing all related firms.
List of Parameters:	
			 @firstname
			 @lastname                
			 @Abbreviation                    
			 @username                    
			 @password                   
			 @accesstype                     
			 @CanUpdateCloseOutLog                  
			 @employeeid                   
			 @email             
			 @FlagCanSPNSearch             
			 @SPNUserName             
			 @SPNPassword           
			 @Status         
			 @NTUserID      
			 @IsAttorney     
			 @IsSTAdmin   
			 @CloseOutAccessDays  
			 @FirmID int 
			 @ContactNumber
	
******/   
ALTER PROCEDURE [dbo].[USP_HTS_Add_users]
	@firstname VARCHAR(20),
	@lastname VARCHAR(20),
	@Abbreviation VARCHAR(3),
	@username VARCHAR(20),
	@password VARCHAR(20),
	@accesstype TINYINT,
	@CanUpdateCloseOutLog INT,
	@employeeid INT,
	@email VARCHAR(100),
	@FlagCanSPNSearch BIT,
	@SPNUserName VARCHAR(20),
	@SPNPassword VARCHAR(20),
	@Status INT,
	@NTUserID VARCHAR(20),
	@IsAttorney BIT,
	@IsSTAdmin BIT,
	@CloseOutAccessDays INT, -- Noufil 4232 07/02/2008 Closeoutdayaccess parameter added
	@FirmID INT, --Fahad 5196 11/27/2008--FirmID added
	@ContactNumber VARCHAR(10), --Nasir 6968 12/04/2009 added contact number
	--Ozair 7379 03/12/2010 added parameter
	@LoginEmployeeID INT =3992
AS
	--keep only one service ticket admin    
BEGIN
     --Asad Ali 7781 5/5/2010 Need to allow multiple admin's
   -- IF @IsSTAdmin = 1
    --    UPDATE TrafficTickets.dbo.tblUsers
   --     SET    isServiceTicketAdmin = 0    
    
    IF @employeeid = 0
    BEGIN
    	--Ozair 7379 03/12/2010 added parameter for holding values
        DECLARE @userid      INT
        DECLARE @insertedBy  INT
        
        INSERT INTO TrafficTickets.dbo.tblUsers
          (
            firstname,
            lastname,
            abbreviation,
            username,
            [password],
            accesstype,
            CanUpdateCloseOutLog,
            Email,
            FlagCanSPNSearch,
            SPNUserName,
            SPNPassword,
            STATUS,
            NTUserID,
            IsAttorney,
            isServiceTicketAdmin,
            CloseOutAccessDays,
            FirmID,	--Fahad 5196 11/27/2008--FirmID added        
            ContactNumber --Nasir 6968 12/04/2009 added contact number
          )
        VALUES
          (
            @firstname,
            @lastname,
            @abbreviation,
            @username,
            @password,
            @accesstype,
            @CanUpdateCloseOutLog,
            @email,
            @FlagCanSPNSearch,
            @SPNUserName,
            @SPNPassword,
            @Status,
            @NTUserID,
            @IsAttorney,
            @IsSTAdmin,
            @CloseOutAccessDays,
            @FirmID,
            @ContactNumber --Nasir 6968 12/04/2009 added contact number
          )
        --Ozair 7379 03/12/2010 getting and setting newly inserted employeeid
        SELECT @userid = SCOPE_IDENTITY()
        
        --Ozair 7379 03/12/2010 getting users database user table userid
        SELECT TOP 1 @insertedBy = u.UserId
        FROM   users.dbo.[User] u
        WHERE  u.TPUserID = @LoginEmployeeID
        
        --Ozair 7379 03/12/2010 inserting records in users database user table
        INSERT INTO users.dbo.[User]
          (
            UserName,
            [Password],
            FirstName,
            LastName,
            InsertedBy,
            LastUpdatedBy,
            Abbreviation,
            Email,
            IsActive,
            NTUserID,
            TPUserID
          )
        SELECT tu.UserName,
               tu.[Password],
               tu.Firstname,
               tu.Lastname,
               @insertedBy,
               @insertedBy,
               tu.Abbreviation,
               tu.Email,
               tu.[Status],
               tu.NTUserID,
               tu.EmployeeID
        FROM   tblUsers tu
        WHERE  tu.EmployeeID = @userid
    END
    ELSE
    BEGIN
        UPDATE tblUsers
        SET    firstname = @firstname,
               lastname = @lastname,
               abbreviation = @abbreviation,
               username = @username,
               [password] = @password,
               accesstype = @accesstype,
               CanUpdateCloseOutLog = @CanUpdateCloseOutLog,
               Email = @email,
               FlagCanSPNSearch = @FlagCanSPNSearch,
               SPNUserName = @SPNUserName,
               SPNPassword = @SPNPassword,
               STATUS = @Status,
               NTUserID = @NTUserID,
               IsAttorney = @IsAttorney,
               isServiceTicketAdmin = @IsSTAdmin,
               CloseOutAccessDays = @CloseOutAccessDays,
               FirmID = @FirmID,	--Fahad 5196 11/27/2008--FirmID added      
               ContactNumber = @ContactNumber --Nasir 6968 12/04/2009 added contact number
        WHERE  employeeid = @employeeid
        
        --Ozair 7379 03/12/2010 updat records in users database user table
        UPDATE Users.dbo.[User]
        SET    firstname = @firstname,
               lastname = @lastname,
               abbreviation = @abbreviation,
               username = @username,
               [password] = @password,
               Email = @email,
               IsActive = @Status,
               NTUserID = @NTUserID               
        WHERE  TPUserID = @employeeid
    END
END  
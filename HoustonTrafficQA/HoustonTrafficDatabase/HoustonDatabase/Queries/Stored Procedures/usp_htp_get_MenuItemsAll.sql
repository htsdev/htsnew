/*  
* Created By  : Waqas Javed  
* Task ID   : 7077  
* Created Date : 12/14/2009  
* Business logic : This procedure is used to get menu items all for menu configuration  
*   
* Parameter List :   
* LevelID - It represents the LevelID (1 for First Level (Main Menu), 2 for Second Level (Sub Menu)  
* MenuID - It represents the Menu Id of menu item  
*/  
  
ALTER PROCEDURE dbo.usp_htp_get_MenuItemsAll    
 @LevelID INT = null,    
 @MENUID INT = null   
AS    
BEGIN    
    IF @LevelID = 1    
    BEGIN    
        SELECT ID,(CASE LTRIM(RTRIM(ISNULL(TITLE,''))) WHEN '' THEN 'Not Available' ELSE TITLE END )AS TITLE,ORDERING,ACTIVE,IsSubMenuHidden  
        FROM   tbl_TAB_MENU    
        ORDER BY    
               ORDERING    
    END    
    ELSE    
    BEGIN    
        SELECT ID,(CASE LTRIM(RTRIM(ISNULL(TITLE,''))) WHEN '' THEN 'Not Available' ELSE TITLE END ) AS TITLE,URL,ORDERING,SELECTED,MENUID,ACTIVE,IsAdminLog,ISNULL(Category,-1) AS Category    --SAEED 7791 05/25/2010,Category column added in the select statment, this column is used to display report category.
        FROM   tbl_TAB_SUBMENU ttm    
        WHERE  ttm.MENUID = (    
                   SELECT TOP 1 MENUID    
                   FROM   tbl_TAB_SUBMENU     
                   WHERE  ID = @MENUID    
               )    
        ORDER BY    
               ttm.ORDERING    
    END    
END  
  


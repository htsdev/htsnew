USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Insert_ResendSMSInfo]    Script Date: 06/04/2013 12:02:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
* Created By :- Rab Nawaz 10914 05/24/2013
* Business Logic : This procedure is used to insert the records which are resended to the clients due to wrong SMS Reply. . . 
* Parameters : 
*				@TicketID
				@phoneNumber
				@ticketNumber
				@sentText
* 
*/


CREATE PROCEDURE [dbo].[USP_HTP_Insert_ResendSMSInfo]

	@TicketID INT,
	@phoneNumber VARCHAR(100),
	@ticketNumber VARCHAR (100),
	@sentText VARCHAR(500)
	
AS
BEGIN
	
	DECLARE @clientName VARCHAR(150)
	SELECT @clientName = FirstName + ' ' + LastName FROM TrafficTickets.dbo.tblTickets tt WHERE tt.TicketID_PK = @TicketID
 
	 INSERT INTO tblEmailHistory
	 ( 	Client, 	SentInfo,	 	recdate, TicketID, 	Smstype, 	PhoneNumber, 	SmsCallType, 	TicketNumber )
	 VALUES (	@clientName,	@sentText, GETDATE(),	@TicketID,	1,	@PhoneNumber,	'Invalid Response Text',	@ticketNumber )
	 	 
	 -- Updating sending Wrong SMS to client info. . . 
	 UPDATE [ArticleManagementSystem].[dbo].[SMSDetails]
	 SET IsWrongReplySmsSend = 1
	 WHERE FromNumber = REPLACE(@phoneNumber, '-', '')
	 AND TicketID = @TicketID
	 AND LTRIM(RTRIM(TicketNumber)) = LTRIM(RTRIM(@ticketNumber))
	 
	 DECLARE @generalComments VARCHAR (1000)
	 SET @generalComments = ' Invalid Response Text sent to client via SMS on ' + @PhoneNumber
	 EXECUTE [TrafficTickets].[dbo].[USP_HTS_UpdateGeneralComments] @TicketID, 3992, 0, @generalComments

END 
GO
GRANT EXECUTE ON [dbo].[USP_HTP_Insert_ResendSMSInfo] TO dbr_webuser
GO
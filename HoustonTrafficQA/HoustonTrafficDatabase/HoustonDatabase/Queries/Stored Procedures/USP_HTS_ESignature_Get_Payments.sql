set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*
Created By     : Adil Aleem.
Created Date   : N/A  
Business Logic : This procedure retrives all the payment info of client by Ticket ID.  
           
Parameter:       
	@TicketID   : identifiable key  TicketId      
*/
--USP_HTS_ESignature_Get_Payments 6062, 3991
ALTER procedure [dbo].[USP_HTS_ESignature_Get_Payments]
@ticketID int,
@employeeid int
As
declare @paymentinfo varchar(4000)
declare @empName varchar(50)

--Set @ticketID = 124773
--Set @employeeid = 3991

set @empName=(select upper(firstname)+' '+upper(lastname) from tblusers where employeeid = @employeeid)
set @paymentinfo = ''
Select @ticketID as TicketID, @employeeid as EmployeeID, newid() as ID, 0 as Page, '$' + convert(varchar(20),convert(int,chargeamount)) as PaymentAmount,
(case when p.paymenttype=5 then isnull(c.cctype,'') + isnull(right(p.cardnumber,7),'') else t.description end) as Method,
p.recdate as PaymentDateTime, u.firstname as Representative,
tt.LanguageSpeak -- Adil 5987 06/11/2009 include client language for English/Spanish translation display
into #TempPayments
from tbltickets tt INNER JOIN tblticketspayment p ON tt.TicketID_PK = p.TicketID
inner join tblpaymenttype t 
on p.paymenttype=t.paymenttype_pk left outer join tblcctype c      
on p.cardtype=c.cctypeid left outer join tblusers u      
on p.employeeid = u.employeeid      
where ticketid = @ticketID                    
and paymenttype not in (99,100)                                          
and paymentvoid not in (1) 
order by p.recdate

Update #TempPayments Set Page = 1 where ID in (Select top 3 ID from #TempPayments order by PaymentDateTime desc)
Update #TempPayments Set Page = 2 where Page <> 1

Select TicketID, EmployeeID, Page, PaymentAmount, Method, PaymentDateTime, Representative, languagespeak from #TempPayments order by PaymentDateTime desc

drop table #TempPayments





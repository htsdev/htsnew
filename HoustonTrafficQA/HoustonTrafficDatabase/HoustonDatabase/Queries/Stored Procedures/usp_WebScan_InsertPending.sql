SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_WebScan_InsertPending]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_WebScan_InsertPending]
GO


CREATE procedure [dbo].[usp_WebScan_InsertPending]  
                  
@PicID as int,                  
@CheckStatus as int   ,  
@Location as varchar(50),
@Status as varchar(50)            
as                  
                  
Insert into tbl_WebScan_OCR                  
(                   
 PicID,                  
 CheckStatus ,
 Location,
 Status		   
            
)                  
values                  
(                   
	@PicID ,                  
	@CheckStatus                ,
	@Location,
	@Status  
) 




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


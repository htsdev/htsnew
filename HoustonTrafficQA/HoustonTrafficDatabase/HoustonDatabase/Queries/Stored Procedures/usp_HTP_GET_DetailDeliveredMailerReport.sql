USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTP_GET_DetailDeliveredMailerReport]    Script Date: 12/16/2011 16:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 /********************************************************************      
Created By     : Muhammad Ali 
Modified Date   : 24/09/2010
Business Logic : This procedure is used to get the Detail report for mailer that are having delivered in specific date range.
  
Parameter:             
 @StartDate, 
 @EndDate  
 @LetterType
 
Columns:  
   First Name:First Name of Client.
   last NAme: last name of Client
 Address1: Address of Client.
 City: City name of Client
 state: State of Client.
 BarCode: Barcode 
 Delivery date: delivery date to Mailer.
     
   
*********************************************************************/
-- [usp_HTP_GET_DetailDeliveredMailerReport]  '09/10/2010', '09/10/2010', 25, 'monthly'

ALTER PROCEDURE [dbo].[usp_HTP_GET_DetailDeliveredMailerReport] 
( 
 @StartDate DATETIME  ,
 @EndDate DATETIME  ,
 @LetterType INT,
  -- Abbas Shahid Khwaja 4900 07/07/2011 Include for report date type
 @ReportType VARCHAR(20)
 
)
AS
-- GET ALL MAILERS DATA PRINTED DURING THE SPECIFIED DATE RANGE....
SELECT CONVERT(DATETIME, CONVERT(VARCHAR(10), RecordLoadDate, 101)) AS currentdate, noteid
INTO #Mailers
FROM tblletternotes   
WHERE LetterType = @lettertype
AND DATEDIFF(DAY, RecordLoadDate, @startdate ) <= 0
AND DATEDIFF(DAY, RecordLoadDate, @enddate) >= 0   


SELECT p.* INTO #PackageData 
FROM MailerUSPSInfo.dbo.USPSPackageData p
INNER JOIN #Mailers a ON a.noteid = p.LetterID

-- CREATING INDEX ON TEMP TABLE FOR PERFORMANCE   
BEGIN TRY   

	CREATE NONCLUSTERED INDEX IX_Mailers_LetterId ON #Mailers
	(LetterID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX IX_PackageData_LetterId ON #PackageData
	(LetterID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
END TRY

BEGIN CATCH
	-- DO NOTHING
END CATCH



CREATE TABLE #temp9
(
	Deliverydate VARCHAR(15),
	mailerids VARCHAR(50)
)

--Sabir Khan 9921 12/16/2011 Fixed deliverd mailer issue.
INSERT INTO #temp9

SELECT distinct CONVERT(VARCHAR(10), a.currentdate, 101),t1.letterid
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  EXISTS 
	(
		SELECT m.id FROM #PackageData m
		WHERE m.LetterID = t1.LetterID
		AND m.OperationCode IN ('828', '878', '898', '908', '912', '914', '918')
		AND NOT EXISTS 
				(
				SELECT o.id FROM #PackageData o
				WHERE o.LetterID = t1.LetterID
				AND o.OperationCode IN (
						'146','266','276','286','336','366','376','396', 
						'406','416','426','446','466','486','496','506','806','816','826','829','836','846','856', 
						'866', '876','879','886','896','899','905','906','909','911','913','915','919','966','976'
						)
				)
		AND NOT EXISTS 
				(
				SELECT p.id FROM #PackageData p
				WHERE p.LetterID = t1.LetterID
				AND p.OperationCode IN ('091','099')
				)				
	)

-- GETTING COUNT FOR 2ND PASS DELIVERED
INSERT INTO #temp9(Deliverydate,mailerids)

SELECT DISTINCT CONVERT(VARCHAR(10), a.currentdate, 101) , t1.letterid
FROM   #PackageData t1
INNER JOIN #Mailers a ON a.noteid = t1.LetterID
AND  EXISTS 
	(
		SELECT m.id FROM #PackageData m 
		WHERE m.LetterID = t1.LetterID
		AND m.OperationCode IN 
				(
				'146', '266','276', '286','336', '366','376', '396','406', '416','426', '446', 
				'466', '486','496', '506','806', '816','826', '829','836', '846','856', '866', 
				'876', '879','886', '896','899', '905','906', '909','911', '913','915', '919', 
				'966', '976'
				)
		AND NOT EXISTS 
				(
				SELECT p.id FROM #PackageData p
				WHERE p.LetterID = t1.LetterID
				AND p.OperationCode IN ('091','099')
				)				
	)

if @LetterType = 47
BEGIN

SELECT distinct case when len(isnull(B.BusinessName,''))>0 then B.BusinessName else BO.ownername end AS FirstName,
'' as LastName,
case when len(isnull(B.[Address],''))>0 then B.[Address] else BO.ownerADdress end AS Address1,
case when len(isnull( B.City,''))>0 then  B.City else BO.ownerCity end AS City,
ts.[State],'00040001120'+CONVERT(VARCHAR,tln.NoteId)+CONVERT (VARCHAR ,tln.ZipCode)+CONVERT (VARCHAR,tln.dp2) AS barcode,
CONVERT (VARCHAR,t.Deliverydate,101) AS deliveryDate
FROM AssumedNames.dbo.Business B inner join AssumedNames.dbo.BusinessOwner BO on BO.BusinessID = B.BusinessID
INNER JOIN tblletternotes tln ON B.BusinessID=tln.RecordID
INNER JOIN tblState ts ON ts.State=BO.OwnerState
INNER JOIN #temp9 t ON tln.NoteId= t.mailerids
--ORDER BY 
--case when len(isnull(B.BusinessName,''))>0 then B.BusinessName else BO.ownername end,
--case when len(isnull(B.[Address],''))>0 then B.[Address] else BO.ownerADdress end,
--case when len(isnull( B.City,''))>0 then  B.City else BO.ownerCity end,
--ts.[State],barcode,t.Deliverydate
END
ELSE
BEGIN
SELECT tva.FirstName,tva.LastName,tva.Address1,tva.City,ts.[State],
'00040001120'+CONVERT(VARCHAR,tln.NoteId)+CONVERT (VARCHAR ,tln.ZipCode)+CONVERT (VARCHAR,tln.dp2) AS barcode,
CONVERT (VARCHAR,t.Deliverydate,101) AS deliveryDate
  FROM tblTicketsArchive tva INNER JOIN tblletternotes tln ON tva.RecordID=tln.RecordID
  INNER JOIN tblState ts ON ts.StateID=tva.StateID_FK
  INNER JOIN #temp9 t ON tln.NoteId= t.mailerids
ORDER BY tva.FirstName,tva.LastName,tva.Address1,tva.City,ts.[State],barcode,t.Deliverydate
END


BEGIN TRY
	DROP TABLE #temp 
	DROP TABLE #temp9
	DROP TABLE #Mailers
	DROP TABLE #PackageData
	DROP TABLE #First_Pass_DeliveredCount
	DROP TABLE #Second_Pass_DeliveredCount
END TRY
BEGIN CATCH
	-- DO NOTHING
END CATCH



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_InsertOrUpdate_Summary_Support]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_InsertOrUpdate_Summary_Support]
GO



CREATE PROCEDURE USP_HTS_LA_InsertOrUpdate_Summary_Support
@GroupID as int,
@StartsFrom as varchar(5),
@NoOfRecords as int
AS
Declare @Record_ID as int

Set @Record_ID = (Select GroupID From Tbl_HTS_LA_Summary_Support Where GroupID = @GroupID And StartsFrom = @StartsFrom)

If @Record_ID is null
	Begin
		Insert Into Tbl_HTS_LA_Summary_Support (GroupID, StartsFrom, NoOfRecords) Values (@GroupID, @StartsFrom, @NoOfRecords)
	End
Else
	Begin
		Update Tbl_HTS_LA_Summary_Support Set NoOfRecords = @NoOfRecords Where GroupID = @GroupID And StartsFrom = @StartsFrom
	End


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


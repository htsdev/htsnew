SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetUnassignedViolation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetUnassignedViolation]
GO










CREATE PROCEDURE  usp_HTS_GetUnassignedViolation

 AS
Select ViolationNumber_PK,Description from tblviolations
where CategoryId=0





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


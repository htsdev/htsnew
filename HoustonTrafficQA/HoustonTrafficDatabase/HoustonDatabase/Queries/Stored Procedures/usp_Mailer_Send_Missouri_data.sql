
/****** 
Created by:		Rab Nawaz Khan
Task ID   :     10401
Date      :     08/15/2012 

Business Logic:	The procedure is used by LMS application to get data for Missouri City Arraignmnet 
				and Warrant letters and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee id for the currently logged in user
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber:	Case number for the violation
	ZipCode:		Person's Home Zip code.
	MidNumber:		MID Number associated with the person
	ViolationDate:	Date of the violation
	ZipMid:			First five characters of the zipcode plus mid number
	Abb:			Short abbreviation of employee who is printing the letter

******/

-- usp_Mailer_Send_Missouri_data 10, 28, 10, '08/13/2012,' , 3992, 0 , 0, 0  
ALTER proc [dbo].[usp_Mailer_Send_Missouri_data]  
@catnum int=1,                                      
@LetterType int=10,                                      
@crtid int=1,                                      
@startListdate varchar (500) ='01/04/2006',                                      
@empid int=3992,                                
@printtype int =0 ,  
@searchtype int ,  
@isprinted bit                                  
                                      
as                                      
  
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                                            
declare @officernum varchar(50),                                      
@officeropr varchar(50),                                      
@tikcetnumberopr varchar(50),                                      
@ticket_no varchar(50),                                                                      
@zipcode varchar(50),                                               
@zipcodeopr varchar(50),                                                                          
@fineamount money,                                              
@fineamountOpr varchar(50) ,                                              
@fineamountRelop varchar(50),                     
@singleviolation money,                                              
@singlevoilationOpr varchar(50) ,                                              
@singleviolationrelop varchar(50),                                      
@doubleviolation money,                                              
@doublevoilationOpr varchar(50) ,                                              
@doubleviolationrelop varchar(50),                                    
@count_Letters int,                    
@violadesc varchar(500),                    
@violaop varchar(50)                            

           
-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL..                                      
declare @sqlquery varchar(max), @sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''  
          
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                      
@officeropr=oficernumOpr,                                      
@tikcetnumberopr=tikcetnumberopr,                                      
@ticket_no=ticket_no,                                                                        
@zipcode=zipcode,                                      
@zipcodeopr=zipcodeLikeopr,                                      
@singleviolation =singleviolation,                                      
@singlevoilationOpr =singlevoilationOpr,                                      
@singleviolationrelop =singleviolationrelop,                                      
@doubleviolation = doubleviolation,                                      
@doublevoilationOpr=doubleviolationOpr,                                              
@doubleviolationrelop=doubleviolationrelop,                    
@violadesc=violationdescription,                    
@violaop=violationdescriptionOpr ,                
@fineamount=fineamount ,                                              
@fineamountOpr=fineamountOpr ,                                              
@fineamountRelop=fineamountRelop                
                                   
from tblmailer_letters_to_sendfilters                                      
where courtcategorynum=@catnum                                      
and Lettertype=@LetterType                                    
  

-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....  
declare @user varchar(10)  
select @user = upper(abbreviation) from tblusers where employeeid = @empid  
                                      

-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
set @sqlquery=@sqlquery+' 
Select distinct ta.recordid,
convert(varchar(10),ta.listdate,101) as listdate, 
count(tva.ViolationNumber_PK) as ViolCount,  
sum(isnull(tva.fineamount,0))as Total_Fineamount   
into #temp2  
FROM    dbo.tblTicketsViolationsArchive tva WITH(NOLOCK) INNER JOIN  dbo.tblTicketsArchive ta WITH(NOLOCK) 
ON  tva.RecordID = ta.RecordID INNER JOIN  dbo.tblCourtViolationStatus tcvs WITH(NOLOCK)   
ON  tva.violationstatusid = tcvs.CourtViolationStatusID   

-- ONLY VERIFIED AND VALID ADDRESSES
where   Flag1 in (''Y'',''D'',''S'')  

-- NOT MARKED AS STOP MAILING...
and donotmailflag = 0   

-- SHOULD BE NON-CLIENT
and  ta.clientflag=0  

-- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
and isnull(ta.ncoa48flag,0) = 0

-- First Name last should not be null or empty. . . 
and TA.FirstName IS NOT NULL 
and TA.LastName IS NOT NULL 

-- Only Apprearance Status. . .  
AND TVA.violationstatusid = 116 

-- only future court date
AND datediff(day, tva.courtdate, getdate()) < 0 

'                 

-- IF PRINTING WARRANT LETTER THEN STATUS SHOULD BE WARRANT...
if @LetterType=33
	set @sqlquery =@sqlquery+ ' 
	and TVA.violationstatusid= 209 '
            

-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY....
if(@crtid = @catnum )                
	set @sqlquery=@sqlquery+'   
	and  ta.courtid In (Select courtid from tblcourts where courtcategorynum = ' + convert(varchar(10),@catnum)+ ')'                    
  
-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....  
else
	set @sqlquery =@sqlquery +'                                    
	and  ta.courtid =  ' + convert(varchar(10),@crtid)                                   
                                    
-- FOR THE SELECTED DATE RANGE ONLY.....  
if(@startListdate<>'')                                      
	set @sqlquery=@sqlquery+'  
	and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' )   
  
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                      
if(@officernum<>'')                                      
	set @sqlquery =@sqlquery +'    
	and  ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                       
                                      
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                                      
	set @sqlquery=@sqlquery+ '  
	and   ('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                            


-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                
	set @sqlquery =@sqlquery+ '
    and  tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                     

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                
	set @sqlquery =@sqlquery+ '
    and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                     
                            
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                            
	set @sqlquery=@sqlquery+ '   
	and ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'               

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION
if(@violadesc<>'')                
	set @sqlquery=@sqlquery+'   
	and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'            

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ '        
group by    
 ta.recordid, 
 convert(varchar(10),ta.listdate,101)  


-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
Select distinct ta.recordid,   
 a.listdate,  
 ta.courtdate,  
 ta.courtid,   
 flag1 = isnull(ta.Flag1,''N'') ,  
 ta.officerNumber_Fk,                                            
 tva.TicketNumber_PK,  
 ta.donotmailflag,  
 ta.clientflag,  
 upper(firstname) as firstname,  
 upper(lastname) as lastname,  
 upper(address1) + '''' + isnull(ta.address2,'''') as address,                        
 upper(ta.city) as city,  
 s.state,  
 c.CourtName,  
 zipcode,  
 midnumber,  
 dp2 as dptwo,  
 dpc,                  
 tva.ticketviolationdate as violationdate,  
 violationstatusid,  
 left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,  
 ViolationDescription,   
 tva.fineamount, 
 a.ViolCount,  
 a.Total_Fineamount   
into #temp                                         
from tblticketsarchive ta WITH(NOLOCK), tblticketsviolationsarchive tva WITH(NOLOCK) , #temp2 a, tblcourts c , tblstate s
where ta.recordid = tva.recordid  and ta.stateid_fk = s.stateid
and ta.recordid = a.recordid  
and ta.courtid = c.courtid 




-- Excluding already printed letters. . . 
Delete from #temp
FROM #temp a INNER JOIN tblletternotes n WITH(NOLOCK) ON n.RecordID = a.recordid 
INNER JOIN tblletter l WITH(NOLOCK) ON l.LetterID_PK = n.LetterType 
WHERE 	lettertype = '+convert(varchar(10),@lettertype)+'  
or (
		l.courtcategory = 10
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
	)



 ALTER TABLE #temp 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 SELECT recordid into #fine from #temp   WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #temp t inner join #fine f on t.recordid = f.recordid 


-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....
Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                                
city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                       
courtid,zipmid,donotmailflag,clientflag, zipcode, midnumber, violationdate, isfineamount, courtdate into #temp1  from #temp where 1=1                                     
'                

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                
	set @sqlquery =@sqlquery+ '
	and'+ @singlevoilationOpr+'  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1) or violcount>3
    '                
                
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                     
if(@doubleviolation<>0 and @singleviolation=0 )                
	set @sqlquery =@sqlquery + '
	and'+ @doublevoilationOpr+'   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2) or violcount>3  
	'
                
-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...              
if(@doubleviolation<>0 and @singleviolation<>0)                
	set @sqlquery =@sqlquery+ '
	and'+ @singlevoilationOpr+'  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)
	or' + @doublevoilationOpr+'  (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2) or violcount>3  
	'

-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....                   
if( @printtype = 1 )
	begin
		set @sqlquery2 =@sqlquery2 +                                      
		'  
		Declare @ListdateVal DateTime,                                    
		@totalrecs int,                                  
		@count int,                                  
		@p_EachLetter money,                                
		@recordid varchar(50),                                
		@zipcode   varchar(12),                                
		@maxbatch int  ,  
		@lCourtId int  ,  
		@dptwo varchar(10),  
		@dpc varchar(10)  

		declare @tempBatchIDs table (batchid int)  

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal 
		set @p_EachLetter=convert(money,0.45)  

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                    
		Select distinct mdate  from #temp1                                  
		open ListdateCur                                     
		Fetch Next from ListdateCur into @ListdateVal                                                        
		while (@@Fetch_Status=0)                                
			begin                                  

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE...
				Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                   

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                    
				('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                     

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter   
				insert into @tempBatchIDs select @maxbatch                                                       

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                                 
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                                 
				open RecordidCur                                                           
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                            
				while(@@Fetch_Status=0)                                
					begin                                

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                                
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                                
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                            
					end                                
				close RecordidCur   
				deallocate RecordidCur                                                                 
				Fetch Next from ListdateCur into @ListdateVal                                
			end                                              

		close ListdateCur    
		deallocate ListdateCur   

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....		 
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid, t.courtdate, t.FirstName, 
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription, t.dpc, 
		dptwo, TicketNumber_PK, t.zipcode, zipmid, '''+@user+''' as Abb, isfineamount, ''Missouri City'' as CourtName, 
		''ARRAIGNMENT'' as lettername, 0 as promotemplate, t.midnumber, t.midnumber as midnum, t.courtid,
		CAST(0 AS BIT) as isSpanish
		from #temp1 t , tblletternotes n, @tempBatchIDs tb  
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+'   
		order by [zipmid]  

		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........		
		declare @lettercount int, @subject varchar(200), @body varchar(400) ,  @sql varchar(500)  
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid  
		select @subject = convert(varchar(20), @lettercount) +  '' Missouri City Municipal Court Arraignment Letters printed''  
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')  
		exec usp_mailer_send_Email @subject, @body  

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'  
	end
  
else  if( @printtype = 0 )

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	begin
		set @sqlquery2 = @sqlquery2 + '  		
		
		Select distinct ''NOT PRINTABLE'' as letterId, t.recordid, t.courtdate, t.FirstName, 
		t.LastName,t.address,t.city, t.state,FineAmount,violationdescription, t.dpc, 
		dptwo, TicketNumber_PK, t.zipcode, zipmid, '''+@user+''' as Abb, isfineamount, ''Missouri City'' as CourtName, 
		''ARRAIGNMENT'' as lettername, 0 as promotemplate, t.midnumber, t.midnumber as midnum, t.courtid,
		CAST(0 AS BIT) as isSpanish
		from #temp1 t
		order by [zipmid] '  
	end


-- DROPPING TEMPORARY TABLES ....  
set @sqlquery2 = @sqlquery2 + '  
  
drop table #temp   
drop table #temp1'  
                                      
--print @sqlquery + @sqlquery2  

-- CONCATENATING THE DYNAMIC SQL..
set @sqlquery = @sqlquery + @sqlquery2  

-- EXECUTING THE DYNAMIC SQL ....
exec (@sqlquery)  
  
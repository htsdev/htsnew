/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the letter information to display on document
				history page.
				
List of Parameters:
	@location:		location of the currently logged in employee. For dallas it will be 2 else 1.

List of Columns:	
	LetterName		full letter name (court category name plus letter name)
	CourtCategornum	court category id of the mailer
	lettertype		mailer type id

******/

alter procedure dbo.USP_Mailer_Get_LetterName-- 2 
	@Location int = 0 
 
as

select distinct c.courtcategoryname + ' - ' + lettername as lettername, c.courtcategorynum, l.letterid_pk
from tblletter l, tblcourtcategories c
where c.courtcategorynum = l.courtcategory 
and l.isactive  =1
-- tahir 5764 04/08/2009 allowed dallas users to access...
--Yasir Kamal 7218 01/13/2010 lms structure changed. 
and (@location != 2 or ISNULL(c.LocationId_FK,0) = 2 ) -- tahir 7196 12/29/09 added arlington mailers
order by [lettername]



﻿ USE TrafficTickets
GO

/****** 
Create by		: Farrukh Iftikhar
Created Date	: 10/08/2012
TasK			: 10437

Business Logic : 
			This procedure is used to get all MDL Cases
								
******/

CREATE PROCEDURE [dbo].[USP_HTP_Delete_MDLCase]
@CaseId INT
AS
BEGIN
    DELETE FROM tblMDLCases WHERE Id = @CaseId
END



GO
GRANT EXECUTE ON [dbo].[USP_HTP_Delete_MDLCase] TO dbr_webuser



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_CONSULTATION_REPORT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_CONSULTATION_REPORT]
GO


CREATE PROCEDURE [dbo].[USP_HTS_GET_CONSULTATION_REPORT]      
@startDate datetime,      
@endDate datetime      
      
as
declare @temp table      
(      
SNo int identity ,       
TicketID int,       
FirstName varchar(20),      
LastName varchar(20), 
     
Comments varchar(4999),    
username varchar(20),    
commentsdate varchar(12),  
sortcommentsdate datetime
       
)      
set @endDate = @endDate + '23:59:59'       
      
INSERT INTO @temp      
SELECT     t.TicketID_PK, t.Firstname, t.Lastname, c.Comments,u.username  , convert(varchar(12),c.commentsdate,101) as commentsdate,c.commentsdate as sortcommentsdate   
FROM         dbo.tblTickets AS t INNER JOIN      
                      dbo.tbl_hts_ConsultationComments AS c ON t.TicketID_PK = c.TicketID_FK  inner join tblusers u on     
   t.employeeid=u.employeeid    
WHERE     (c.CommentsDate BETWEEN @startDate AND @endDate)      
      
select * from @temp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


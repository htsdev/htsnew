﻿/****** 
Created by:		Fahad Muhammad Qureshi.
TaskID:			6934
Created Date:	01/14/2010
Business Logic:	The procedure is used by Houston Traffic Program in backroom in order to trace out the Rep's activities related to the report Quote Call Back in Activities Section.  
				
List of Parameters:
	@TicketID :Id by which we identified our client.
	@EventDescription : Description of that event which occured in our report.
	@EmployeeID : For which we identified the employee.

******/
CREATE PROCEDURE dbo.USP_HTP_SetQuoteCallTrackingReport
	@TicketID INT,
	@EventDescription VARCHAR(300),
	@EmployeeID INT
AS
	DECLARE @QuoteId DECIMAL(18, 0)
	SET @QuoteId = (
	        SELECT DISTINCT tvq.QuoteID
	        FROM   tblViolationQuote tvq
	        WHERE  tvq.TicketID_FK = @TicketID
	    )
	
	DECLARE @QuoteCallbackTrackingID INT
	SET @QuoteCallbackTrackingID = (
	        SELECT DISTINCT qct.QuoteCallbackTrackingID
	        FROM   QuoteCallbackTracking qct
	        WHERE  qct.QuoteID = @QuoteId
	    )
	
	INSERT INTO QuoteCallbackTrackingDetail
	  (
	    QuoteCallbackTrackingID,
	    EventDescription,
	    EmployeeID
	  )
	VALUES
	  (
	    @QuoteCallbackTrackingID,
	    @EventDescription,
	    @EmployeeID
	  )
GO

GRANT EXECUTE ON dbo.USP_HTP_SetQuoteCallTrackingReport TO dbr_webuser
GO  
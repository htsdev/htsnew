


/****** Object:  StoredProcedure [dbo].[USP_HTP_UPDATECASETYPE]    Script Date: 07/10/2008 17:21:51 ******/
/******  Created By : Noufil Khan
	Business Logic : This Procedure Updates the case type 
	Parameter Used : 
					@casetypeid   Id of case 
					@ServiceTicketEmpId  Id of employee
					@CaseTypeName  name of Case
					@ServiceTicketEmailAlert  Close Email
					@ServiceTicketEmailClose  Open Email
					@ServiceTicketAngryEmailOpen Angry open email
					@ServiceTicketAngryEmailClose  Angry close email
					@ServiceTicketUpdateEmail	Udpate Email
	List Of Column :
					@@rowcount  ( return rowcount if row update or not)

 Noufil 4338 07/10/2008 This procedure Update the case type.
 ******/

ALTER procedure [dbo].[USP_HTP_UPDATECASETYPE] 
@casetypeid int,
@ServiceTicketEmpId int,
@CaseTypeName varchar(200),
@ServiceTicketEmailAlert varchar(200),
@ServiceTicketEmailClose varchar(200),
@ServiceTicketAngryEmailOpen varchar(200),
@ServiceTicketAngryEmailClose varchar(200),
-- Zahoor 4757 09/09/2008 Update Email
@ServiceTicketUpdateEmail	varchar(200)

as
Update casetype 
set
	CaseTypeName=@CaseTypeName,  
    ServiceTicketEmpId=@ServiceTicketEmpId,  
	ServiceTicketEmailAlert=@ServiceTicketEmailAlert,
	ServiceTicketEmailClose=@ServiceTicketEmailClose,
	ServiceTicketAngryEmailOpen=@ServiceTicketAngryEmailOpen,
	ServiceTicketAngryEmailClose=@ServiceTicketAngryEmailClose,
	-- Zahoor 4757 09/09/2008 Update Email
	ServiceTicketUpdateEmail = @ServiceTicketUpdateEmail
where casetypeid=@casetypeid

select (@@rowcount)
GO



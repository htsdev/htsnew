﻿/******  
* Created By :	Fahad Muhammad Qureshi.
* Create Date :   10/19/2010 12:44:00 PM
* Task ID :				6766
* Business Logic :  This procedure is used to Get .
* List of Parameter :
* Column Return : 
      
******/
 
CREATE PROCEDURE dbo.USP_HTP_GetDoctorFirstNameByCriteria 
@Criteria VARCHAR(MAX) = ''
AS      
      
BEGIN
    SELECT DISTINCT TOP 15 d.FirstName AS Doctor
    FROM   Doctor d
    WHERE  d.FirstName LIKE @criteria + '%'
           AND d.FirstName IS NOT NULL
           AND LEN(d.FirstName) > 0
    ORDER BY
           d.FirstName
END 
GO
GRANT EXECUTE ON dbo.USP_HTP_GetDoctorFirstNameByCriteria TO dbr_webuser
GO 	  
ALTER Procedure [dbo].[USP_PS_Update_Flags_Of_QuoteClient] 
(              
             
@ticketid_pk int,              
@bondflag int,              
@accidentflag int,              
@cdlflag int  ,            
@EmailAddress varchar(100),      
@FirstName varchar(50),          
@LastName varchar(50),          
@Contact1 varchar(20),          
@Contact2 varchar(20),          
@Contact3 varchar(20),          
@height varchar(20),          
@weight varchar(20),          
@haircolor varchar(20),          
@eyecolor varchar(20),          
@homeaddress varchar(100),          
@city  varchar(20),          
@state varchar(20),          
@zip varchar(12),          
@dob varchar(12),          
@dl varchar(20),          
@dlstate int,          
@gender char(6),          
@race varchar(20)     
)    
as    
         

declare @empid int 

-- SYSTEM EMPLOYEE ID.....
set @empid = 3992
          
          
Update tbltickets           
         
Set           
          
  firstname = @FirstName ,          
  lastname =@LastName ,          
  Address1 = @homeaddress,          
  City = @city,          
  Stateid_FK = @state,          
  Zip = @zip,          
  Contact1 = @Contact1,          
  contactType1 =case when len(@Contact1)> 0 then 100 else 0 end,                                  
  contact2 = @Contact2,           
  Contacttype2 =case when len(@Contact2)> 0 then 100 else 0 end,                       
  contact3 = @Contact3,          
  contacttype3 =case when len(@Contact1)> 0 then 100 else 0 end,                      
  height = @height,          
  weight = @weight,          
  haircolor = @haircolor,          
  eyes = @eyecolor,          
  DOB = dob,          
  DLNumber = @dl,          
  dlstate = @dlstate,            
  employeeidupdate = @empid,          
  Race = @race,          
  Gender = @gender,          
  email = @EmailAddress,          
  bondflag = @bondflag ,              
  bondsrequiredflag = @bondflag,              
  accidentflag = case when @accidentflag  = 3 then 1 else @accidentflag end ,              
  cdlflag =case when @cdlflag  = 3 then 1 else @cdlflag end ,              
  lockflag = 1,              
  TotalFeeCharged = CalculatedTotalFee ,  
 EmailNotAvailable = 0  ,         
 NoDl = case when len(@dl)> 0 then 0 else 1 end,  
 WalkInClientFlag=0                  
where ticketid_pk = @ticketid_pk              
          
update traffictickets.dbo.tblticketsviolations               
set underlyingbondflag = @bondflag , vemployeeid = @empid             
where ticketid_pk = @ticketid_pk 



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_GET_PercentageOfClientsToViolCount_test]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_GET_PercentageOfClientsToViolCount_test]
GO

-- USP_RPT_GET_PercentageOfClientsToViolCount_test '01/01/2006', '12/31/2006'
--go
-- USP_RPT_GET_PercentageOfClientsToViolCount '01/01/2006', '12/31/2006'

CREATE procedure [dbo].[USP_RPT_GET_PercentageOfClientsToViolCount_test]
	(
	@sDate datetime,
	@eDate datetime
	)
as 



select @edate = @edate + '12/31/06 23:59:59.999'

select @edate

declare @totClients int
declare @temp table (ticketid int)
declare @tmp table (ticketid_pk int , violcount int)
declare @tmp2 table (violcount int, casecount int)
declare @tmp3 table (violcount varchar(10), casecount int)


insert into @temp
select distinct ticketid from tblticketspayment
where paymentvoid = 0
and recdate between @sdate and @edate
group by ticketid
having sum(chargeamount) > 0

select @totClients = count(ticketid) from @temp

insert into @tmp 
select t.ticketid_pk, count(v.ticketsviolationid) as ViolCount
from tbltickets t, tblticketsviolations v, @temp a
where t.ticketid_pk = v.ticketid_pk
and t.ticketid_pk = a.ticketid
and t.activeflag = 1
group by t.ticketid_pk
order by count(v.ticketsviolationid) desc


insert into @tmp2 
select violcount, count(ticketid_pk)
from @tmp
group by violcount

insert into @tmp3
select violcount, casecount
from @tmp2
where violcount < 9

insert into @tmp3
select '9+'  , sum(casecount)
from @tmp2
where violcount > 8


if @totClients > 0 
	select	violcount as [Violation Count], 
			casecount as [Client Count],
			convert( numeric(15,2) ,round (convert(numeric(15,2), casecount) * 100 / (convert(numeric(15,2), @totClients)),2)) as [% of Total Clients]
	from	@tmp3
else
	select 'No Record Found.'
	



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


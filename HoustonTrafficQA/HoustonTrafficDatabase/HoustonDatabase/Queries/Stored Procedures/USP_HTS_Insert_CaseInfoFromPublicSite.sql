/*********

Alter By Zeeshan Ahmed

Business Logic : This procedure is used to migrate case from Non Client to Client Cases
				 Set Case Type Id '1' For Internet Sign Up Clients
List of Parameters

@CustomerId : Customer id 
@sOutPut : Receipt Image Name

***********/


ALTER procedure [dbo].[USP_HTS_Insert_CaseInfoFromPublicSite]                      
  (                        
  @CustomerId int,                      
  @sOutPut varchar(20) = '' output                      
  )                        
as                        
                      
set nocount on                        
declare @Officernumber  int,                        
 @courtid  int,                        
 @courtdate datetime,                      
 @violationdate  datetime,                        
 @PlanId int,                      
 @RecCount int,                      
 @listdate datetime,                      
 @TicketId int ,                      
 @iFlag int  ,                      
 @RecordId int,                      
 @TicketNumber varchar(20),                      
 @empid int,                      
 @PaidAmount float,                      
 @strinitial varchar(20),                      
 @courtno int,                      
 @empdesc varchar(10),                       
 @bondFlag varchar(10),                      
 @cdlflag varchar(10),                      
 @accidentflag varchar(10),                      
              
-- Added By Zeeshan To Add Outside Court Date --              
 @outside_courtdate datetime,              
 @outside_courtno int          
-- Added By Zeeshan To Add Related FTA Tickets In The Case    
,@FTALinkID int    
------------------------------------------------                    
              
select @empid = 3992, @empdesc = abbreviation from tblusers where employeeid = 3992                      
declare @tempRecId table (RecordId int)                      
                      
insert into @tempRecId                       
select distinct recordid from legalhouston.dbo.transactionviolations                      
where id_customer = @customerid                      
               
              
select @paidamount = amount, @courtno = courtno from legalhouston.dbo.tblcustmer                      
where id_customer = @customerid                      
                      
select @accidentflag =  accident  ,                           
 @cdlflag =   CDL ,                           
 @bondFlag =  IsEmailBond,              
@outside_courtdate = courtdate ,               
@outside_courtno = courtnumber                       
from  legalhouston.dbo.violationmaster where id_customer = @customerid                      
                     
                          
if @accidentflag = 'y'                       
 set @accidentflag = 1                       
else                       
 set @accidentflag = 0                      
                      
if @cdlflag = 'y'                       
 set @cdlflag = 1                       
else                       
 set @cdlflag = 0                      
                      
if @bondFlag = 'y'                       
 set @bondFlag = 1                       
else                        
 set @bondFlag = 0                      
                      
-- GETTING INITIALS FOR SYSTEM USER.........                      
select @strInitial = abbreviation from tblusers where employeeid = @empid                      
                      
    
                      
select @RecordId = ( select top 1 recordid from @tempRecId )                      
    
    
    
-- Added By Zeeshan Ahmed ---              
              
Update tblticketsviolationsarchive               
Set               
              
CourtDate = @outside_courtdate,              
CourtNumber = @outside_courtno              
where recordid = @RecordId and courtdate = '1/1/1900'              
              
------------------------------              
              
                      
select @iFlag = 2     

-- TAHIR AHMED 4000 05/09/2008

-- CREATING TEMP TABLES.................................. 
declare @tblTicketsArchive table (
 [TicketNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
 [MidNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [FirstName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [LastName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [Initial] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [Address1] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [Address2] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [StateID_FK] [int] NULL , 
 [ZipCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [PhoneNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
[WorkPhoneNumber] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [DOB] [datetime] NULL, 
 [DLNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [ViolationDate] [datetime] NULL, 
 [Race] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [Gender] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [Height] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [Weight] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 [ListDate] [datetime] NOT NULL, 
 [LetterCode] [smallint] NULL, 
 [MailDateREGULAR] [datetime] NULL, 
 [MailDateFTA] [datetime] NULL, 
 [Bondflag] [tinyint] NULL , 
 [officerNumber_Fk] [int] NULL, 
 [RecordID] [int], 
 [DP2] [varchar](10), 
 [DPC] [varchar](10), 
 [Flag1] [varchar](1) ,
 [CourtDate] [datetime],
 [courtid] [int]
) 
 
declare @tblTicketsViolationsArchive table ( [TicketNumber_PK] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
 [ViolationNumber_PK] [int] NULL , --Haris 9709 09/29/2011 datatype changed to int from tinyint
 [FineAmount] [money] NULL, 
 [ViolationDescription] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [ViolationCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [violationstatusid] [int] NULL, 
 [CourtDate] [datetime] NULL, 
 [Courtnumber] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [BondAmount] [money] NULL , 
 [TicketOfficerNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
 [RecordID] [int] NULL , 
 [CauseNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, [CourtLocation] [int] NULL, 
 [OffenseLocation] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, [OffenseTime] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 [ArrestingAgencyName] [varchar] (50) NULL 
 ) 
 

INSERT into @tblTicketsArchive( 
 TicketNumber, MidNumber, FirstName, LastName, Initial, Address1, Address2, City, StateID_FK, ZipCode, PhoneNumber,WorkPhoneNumber, DOB, DLNumber, ViolationDate, Race, Gender, Height,weight, ListDate, MailDateREGULAR, MailDateFTA, Bondflag, officerNumber_Fk, RecordID , 
 DPC , DP2, FLAG1, courtdate, courtid ) 

select t.TicketNumber, t.MidNumber, t.FirstName, t.LastName, t.Initial, t.Address1, t.Address2, t.City, t.StateID_FK, t.ZipCode,t.PhoneNumber,t.WorkPhoneNumber, t.DOB, t.DLNumber, t.ViolationDate, t.Race, t.Gender, t.Height,t.Weight, t.ListDate, t.MailDateREGULAR, t.MailDateFTA, t.Bondflag, t.officerNumber_Fk, t.RecordID ,t.DPC , t.DP2, t.FLAG1 , t.courtdate, t.courtid
 from tblticketsarchive t, tblticketsviolationsarchive v 
 where t.recordid = v.recordid and t.recordid in (select * from @temprecid)
                 
                 
insert into @tblTicketsViolationsArchive ( 
 TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode, violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber, RecordID, CauseNumber, courtlocation, OffenseLocation, OffenseTime,ArrestingAgencyName) 

select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode, v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber, v.RecordID, v.CauseNumber, v.courtlocation, v.OffenseLocation, v.OffenseTime,v.ArrestingAgencyName 
from tblticketsviolationsarchive v , @tblticketsarchive t 
where t.recordid = v.recordid and v.recordid in (select * from @temprecid)
                 
                      
-- end changes task 4000
------------------------------------------------------------------------------------------------    
-- Added By Zeeshan     
-- Getting FTA Link ID To Add Other FTA Related Tickets In The Case     
Set @FTALinkID = 0    
Select Top 1 @FTALinkID = isnull(FTALinkID,0) from tblticketsviolationsarchive where recordid = @RecordId              
    
if @FTALinkID <> 0     
Begin    
 insert into @tblTicketsViolationsArchive ( 
 TicketNumber_PK, ViolationNumber_PK, FineAmount, ViolationDescription, ViolationCode, violationstatusid, CourtDate, Courtnumber, BondAmount, TicketOfficerNumber, RecordID, CauseNumber, courtlocation, OffenseLocation, OffenseTime,ArrestingAgencyName) 
 
 select v.TicketNumber_PK, v.ViolationNumber_PK, v.FineAmount, v.ViolationDescription, v.ViolationCode, v.violationstatusid, v.CourtDate, v.Courtnumber, v.BondAmount, v.TicketOfficerNumber, v.RecordID, v.CauseNumber, v.courtlocation, v.OffenseLocation, v.OffenseTime,v.ArrestingAgencyName 
from tblticketsviolationsarchive v 
where v.recordid <>  @RecordId  and v.FTALinkID = @FTALinkID    
End           
                      
select  @officernumber = isnull(officernumber_fk,962528),                      
 @violationdate = isnull(violationdate,'01/01/1900') ,                      
 @courtdate = t.courtdate,                      
 @courtid = t.courtid,                      
 @listdate = t.listdate                      
from @tblTicketsArchive t inner join @tblTicketsViolationsArchive v                      
on t.recordid = v.recordid                      
                        
declare @Temp table                        
  (                        
  TicketNumber varchar(20),                        
  CourtDate datetime,                        
  ViolationDate datetime,                      
  recordid int                        
  )                        
                       
 insert into @Temp                          
 select  distinct                        
  TicketNumber,                        
  courtdate,                        
  isnull(violationdate,'01/01/1900') ,                      
  recordid                       
 from   @tblTicketsArchive                         
 where recordid = @recordid                       
                       
-- Get Case Type

Select top 1 @courtid = courtlocation from tblticketsviolationsarchive where recordid = @RecordId
declare @casetypeid as int
Select @casetypeid = isnull(CaseTypeId,1)  from tblcourts where courtid = @courtid
                     
                      
 ------------------------------------------------------------------------------------------------------------------------                        
 --    INSERTING MAIN INFORMATION INTO TBLTICKETS FROM TBLTICKETSARCHIVE                        
 ------------------------------------------------------------------------------------------------------------------------                        
begin tran                     
insert into tbltickets                        
  (                        
  midnum,  firstname,  lastname,  Address1,  Address2,  City,  Stateid_FK,  Zip,  Contact1, contactType1,                      
  contact2, Contacttype2, contact3, contacttype3, height, weight, haircolor, eyes, DOB,  DLNumber,                       
  dlstate, employeeid,  Race,   Gender,                        
   employeeidupdate, RecordId, email, generalcomments,                      
  accidentflag, cdlflag, bondflag,  BondsRequiredflag  , languagespeak, MailerID,WalkInClientFlag ,casetypeid                      
  )                       
                         
 select distinct                         
  MidNumber,                       
  t.firstname,                      
  t.lastname,                       
  t.address1,                      
  t.address2,                      
  t.city,                      
  isnull(t.stateid_fk,45),                      
  t.zipcode,                      
  c.telephone,                      
  case when len(c.telephone)> 0 then 100 else 0 end,                      
  c.alttelephone,                      
  case when len(c.alttelephone)> 0 then 100 else 0 end,                      
  c.alttelephone2,                      
  case when len(c.alttelephone2)> 0 then 100 else 0 end,                      
  c.height,                      
  c.weight,       
  c.haircolor,                      
  c.eyecolor,                      
  c.dtbirth,                      
  c.driverlicense,                      
  c.dlstate,                      
  @EmpId,                        
  c.Race,                      
  c.Gender,                        
  @EmpId,                      
  RecordId,                      
  c.email,                      
  case when len(isnull(convert(varchar(1900),c.comments),'')) > 0                       
  then convert(varchar(1900),c.comments) + ' ('+dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empdesc + ')' + ' '                       
       else '' end ,                      
 @accidentflag, @cdlflag, @bondflag, @bondflag, 'ENGLISH', c.MailerID,        
 0 ,@casetypeid                    
 from   @tblTicketsArchive t inner join legalhouston.dbo.tblcustmer c                      
 on t.recordid = @recordid                      
 and  c.id_customer = @customerid                      
                         
 if @@error = 0                       
  --ozair Task 4775 09/16/2008 replace @@Identity with Scope_Identity 
  --as @@Identity returns the last inserted identity value instead of the insert statement executed in current scope
  --select @TicketID=@@identity 
  select @TicketID=SCOPE_IDENTITY() 
  
                      
                        
 ------------------------------------------------------------------------------------------------------------------------                        
 --  INSERTING DETAILED INFORMATION INTO TBLTICKETSVIOLATIONS FROM TBLTICKETSVIOLATIONSARCHIVE                         
 ------------------------------------------------------------------------------------------------------------------------                        
 IF @TicketId <> 0                       
  begin                      
   declare @temp1 table                         
    (                        
    violationnumber  int,                        
    bondamount  money,                        
    FineAmount   money,                        
    TicketNumber   varchar(20), 
    --ozair 4775 09/16/2008 datatype changed to int from tinyint as the data/field inserted in this is of type smallint
    sequenceNumber   int,                        
    Violationcode   varchar(10),                        
    ViolationStatusID  int,                        
    CourtNumber   varchar(10),                        
    CourtDate   datetime,                        
    ViolationDescription  varchar(200),                        
    courtid   int,                        
    ticketviolationdate  datetime,                        
    ticketofficernum  varchar(20)  ,                      
    CauseNumber varchar(25),                      
    RecordId  int                      
    )                      
                           
   declare @temp2 table                        
    (                        
    violationnumber  int,                        
    fineamount  money,                        
    ticketnumber  varchar(25),                        
    sequencenummber  int,                        
    violationcode  varchar(10),                        
    bondamount  money,                        
    violationstatusid int,                        
    courtnumber  int,                        
    courtdate  datetime,                        
    courtid   int  ,                      
    CauseNumber varchar(25),                       
    recordid int                      
    )                      
                      
   if @courtid in (3001,3002,3003)                      
    begin                      
     insert into @temp1                        
     SELECT  distinct                         
      isnull(L.ViolationNumber_PK,0) as ViolationNumber,                        
      isnull(V.bondamount,0) as BondAmount,                         
      V.FineAmount,                        
      v.ticketnumber_pk,                      
      V.ViolationNumber_PK as sequenceNumber,                        
      V.violationcode as Violationcode,                         
      case v.ViolationStatusID when 186 then 135 when 146 then 135 else v.ViolationStatusID end,                        
      v.CourtNumber,                        
      V.CourtDate,                        
      isnull(v.ViolationDescription,'N/A') as violationdescription,                         
	  courtlocation,                    
      t.violationdate,                        
      isnull(v.TicketOfficerNumber,962528)  ,                      
      v.CauseNumber,                      
      v.recordid                      
     from  @tblticketsarchive t                        
     INNER JOIN                         
      @tblticketsviolationsarchive V                         
     on ( t.recordid = v.recordid )                      
     LEFT OUTER JOIN                         
      tblViolations  L                         
     ON   V.violationcode = L.violationcode                         
     -- ADDED BY TAHIR AHMED DT:4/16/08 BUG # 3741
     AND v.violationdescription = l.description
    end                      
   else                      
    begin                      
     insert into @temp1                        
     SELECT  distinct                         
      isnull(L.ViolationNumber_PK,0) as ViolationNumber,                        
      isnull(V.bondamount,0) as BondAmount,                         
      V.FineAmount,                        
      v.ticketnumber_pk,                      
      '0' as sequenceNumber,                        
      V.violationcode as Violationcode,                         
      v.ViolationStatusID,                        
      v.CourtNumber,                        
      V.CourtDate,                        
      isnull(v.ViolationDescription,'N/A') as violationdescription,                         
	  courtlocation,                    
	  t.violationdate,                        
      isnull(v.TicketOfficerNumber,962528)  ,                      
      v.CauseNumber,                      
      v.recordid                      
     from  @tblticketsarchive t                        
     INNER JOIN                         
      @tblticketsviolationsarchive V                         
     on ( t.recordid = v.recordid )                      
     LEFT OUTER JOIN                         
      tblViolations  L          
     ON   V.violationdescription  = L.description                      
     and  l.violationtype = (                      
       select violationtype from tblcourtviolationtype                      
       where courtid = @courtid                      
       )                      
    end                      
                               
                      
   insert into @temp2                        
   select V.ViolationNumber_PK ,                        
    t1.fineamount,                        
    t1.TicketNumber,                        
    t1.sequencenumber,                        
    t1.violationcode,                        
    t1.bondamount,                        
    ViolationStatusID,                        
    CourtNumber,                        
    CourtDate,                        
    courtid  ,                       
    t1.CauseNumber,                      
    t1.recordid                      
   from @temp1 t1                        
   inner join                         
    tblViolations V                         
   on  t1.sequencenumber = V.sequenceorder                        
   where  t1.violationnumber = 0                           
                         
   update  t1                        
   set t1.violationnumber = t2.violationnumber                        
   from   @temp1 t1,                         
    @temp2 t2                        
   where   t1.violationcode = t2.violationcode                        
             
   select  @PlanId =                       
    (                      
    select  top 1 planid                       
    from  tblpriceplans                       
    where  courtid =@Courtid                      
    and  effectivedate <= @listdate                      
    and enddate > = @listdate                      
    and isActivePlan = 1                      
    order by planid                      
    )                      
                          
                       
   insert into tblticketsviolations                        
    (                        
    TicketID_PK,                        
    ViolationNumber_PK,                        
    FineAmount,                        
    refcasenumber,                        
    bondamount,                        
    CourtViolationStatusID,                        
    CourtNumber,                        
    CourtDate,                        
    violationdescription,      
    courtid,                        
    ticketviolationdate,                        
    TicketOfficerNumber,                        
    vemployeeid,                        
    CourtViolationStatusIDmain,                        
    CourtNumbermain,                        
    CourtDatemain,                        
    CourtViolationStatusIDscan,                        
    CourtNumberscan,                        
    CourtDatescan ,                      
    casenumassignedbycourt,                      
    underlyingbondflag,                      
    PlanId,          
    RecordID                      
    )                        
                           
   SELECT  distinct                          
    @TicketID,                        
    violationNumber,                        
    FineAmount,                        
    TicketNumber,                        
    bondamount,                        
    ViolationStatusID,                        
    CourtNumber,                        
    CourtDate,                        
    violationdescription,                        
    courtid,                    
    ticketviolationdate,                        
    ticketofficernum,                        
    @empid,                        
    ViolationStatusID,                        
    CourtNumber,                        
    CourtDate,                        
    ViolationStatusID,              CourtNumber,                        
    CourtDate,                      
    CauseNumber,                      
    case when isnull(courtdate, '1/1/1900') < getdate() then 1 else 0 end, 
    --Yasir Kamal 6403  07/12/2009 set default price plan to 45.                    
    ISNULL(@PlanId,45) ,          
    -- Added By Zeeshan Ahmed ---           
 @RecordId                      
                from  @temp1                        
                      
   --------------------------------------------------------------------------------------------------------------------                        
   --    INSERTING HISTORY NOTES                        
   --------------------------------------------------------------------------------------------------------------------                        
   declare @strNote varchar(100)                      
   select  @strNote = 'INITIAL INQUIRY '+ upper(isnull(tc.shortname,'N/A')) + ':' + upper(isnull(c.ShortDescription,'N/A')) + ':' + CONVERT(varchar(10), isnull(v.CourtDateMain,'1/1/1900'), 101) + ' #' +  isnull(v.courtnumbermain ,'0')+ ' @ ' +           
  
    
      
        
         
 upper(dbo.formattime(isnull(v.CourtDateMain,'1/1/1900')))                      
   FROM tblTicketsViolations v                       
   left outer join                      
                        tblCourtViolationStatus c                       
   ON  v.CourtViolationStatusIDmain = c.CourtViolationStatusID                      
   left outer  join                      
    tblcourts tc                      
   on  v.courtid=tc.courtid                      
   where  v.ticketid_pk = @TicketID                      
                      
                      
   if @strNote is not null                      
      insert into tblticketsnotes(ticketid, subject, employeeid) values (@TicketID, @strNote, @empid )     
                      
   --------------------------------------------------------------------------------------------------------------------                        
   --    UPDATING CASE STATUS & COURT INFO IN TBLTICKETS....                      
   --------------------------------------------------------------------------------------------------------------------                        
   -- Agha 2664 06/10/2008
   /*update t                      
   set t.datetypeflag = c.CategoryID                       
   FROM tblTickets t                       
   INNER JOIN                      
                        tblTicketsViolations v                      
   ON  t.TicketID_PK = v.TicketID_PK                       
   INNER JOIN                      
                        tblCourtViolationStatus c                       
   ON  v.CourtViolationStatusIDmain = c.CourtViolationStatusID                      
   where v.courtdatemain = (                      
     select min(courtdatemain) from tblticketsviolations v2                      
     where v2.ticketid_pk = t.ticketid_pk                       
     )                      
   and t.ticketid_pk = @TicketID                      
   */                   
                      
   -------------------------------------------------------------------------------------------------------                      
   -- NOW INSERTING PAYMENT INFORMATION..................                      
   -------------------------------------------------------------------------------------------------------                
                         
   -- FIRST INSERTING NOTE IN CASE HISTORY......                      
   insert into tblticketsnotes (ticketid, subject, employeeid)                      
   select @ticketid , 'Internet Client Signup', 3992                      
                      
   -- NOW INSERTING FEE INFORMATION.............                      
   update tbltickets                       
   set basefeecharge = @PaidAmount,                      
    calculatedtotalfee = @PaidAmount,                      
    totalfeecharged = @PaidAmount,                      
    feeinitial = @strInitial,                      
    initialadjustmentinitial = @strInitial,                      
    lockflag = 1,                      
    initialadjustment = 0,                      
    adjustment = 0,                      
    ContinuanceAmount = 0,                      
    InitialTotalFeeCharged = @PaidAmount,                      
    employeeidupdate = 3992                      
   where ticketid_pk = @ticketid                      
                      
                      
   -- NOW INSERTING PAYEMNT INFORMATION.................                      
   declare @PaymentId int                      
                         
   insert into tblticketspayment ( ticketid, paymenttype, chargeamount, employeeid)                      
   select  @ticketid, 7, @PaidAmount, 3992                      
                        
   select @paymentid = scope_identity()                      
        
   --Added By Zeeshan Ahmed For TrackPayment      
 update traffictickets.dbo.tblticketscybercashlog set invnumber = Convert(varchar,@paymentid) , reftransnum =''       
 where ticketid = @customerid and transtype = 0       
                       
       
   insert into tblticketscybercash (                     
    ticketid, invnumber, amount, ccnum, expdate, name, recdate, status,                       
    transnum, retcode, errmsg, employeeid, approvedflag, clearflag, cin, cardnumber, response_subcode,                      
    response_reason_code, response_reason_text , auth_code, avs_code, cardtype                      
    )                      
   select @ticketid, @paymentid, @paidamount, ccnum, expdate, name, getdate(), status,                       
    transnum, retcode, errmsg, 3992, approvedflag, clearflag, cin, ccnum, response_subcode,                      
    response_reason_code, response_reason_text, auth_code, avs_code, cardtype                      
   from legalhouston.dbo.tblticketscybercash                      
   where id_customer = @customerid                      
                      
                      
   --------------------------------------------------------------------------------------------------------------------                            
   --    INSERTING INTERNET CLIENT RECEIPT IMAGE TO SCANNED DOCS                      
   --------------------------------------------------------------------------------------------------------------------                            
                  
                  
                   
   DECLARE @BookID int, @picID int , @cmd varchar(255)  , @notes varchar(500)                            
                  
 begin transaction                  
                  
   insert into tblScanBook (EmployeeID,TicketID,DocTypeID,updatedatetime,DocCount,ResetDesc)                       
   values(@empid,@ticketid,11,getdate(),1,'INTERNET CLIENT SIGNUP RECEIPT') -- insert into book                                
                         
   select @BookID =  Scope_Identity() --Get Book Id of the latest inserted                                
                         
   insert into tblScanPIC (ScanBookID,updatedatetime,DocExtension)                       
   values( @BookID,getdate(),'PDF') --insert new image into database                                
                         
   select @picID =  Scope_Identity() --Get Book Id of the latest inserted                                

 commit transaction                            
                         
   set @notes = '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+ convert(varchar(10), @BookID)+'&RecType=1&DocNum=0&DocExt=PDF'');void('''');"''>SCANNED DOCUMENT - OTHER [INTERNET CLIENT SIGNUP RECEIPT]</a>'                      
  
     
      
   insert into tblTicketsNotes (TicketID,Subject,EmployeeID)                       
   values (@TicketID, @notes ,@EMPID)                           
                         
   update legalhouston.dbo.tblcustmer set imagefilename = convert(varchar(10), @bookid) +'-'+ convert(varchar(10), @picid) + '.PDF'  ,                    
  ticketid = @ticketid                
   where id_customer = @customerid    
                     
   --------------------------------------------------------------------------------------------------------------------                        
   --    UPDATING CLIENT FLAG IN TBLTICKETSARHCIVE                      
   --------------------------------------------------------------------------------------------------------------------                        
   update  tblticketsarchive                      
   set clientflag = 1                       
   where recordid  in (                      
     select * from @tempRecId                       
     )                      
                      
   --------------------------------------------------------------------------------------------------------------------                        
   --    INSERTING GENERAL & TRIAL NOTES IF CLIENT NAME DIFFERS                      
   --------------------------------------------------------------------------------------------------------------------                        
   exec USP_HTS_Update_GeneralTrialComments @TicketID, @customerid  
   
                       
   --------------------------------------------------------------------------------------------------------------------                        
   --     Added By Zeeshan For Mailer ID for 2nd June 2007 Mailer ID Modifications          
   --------------------------------------------------------------------------------------------------------------------                        
   exec USP_HTS_Update_Mailer_id @TicketID, @empid, 1                   
  end                        
                      
if isnull(@Ticketid ,0) = 0                       
 select @Ticketid = @RecCount                      
select @sOutPut = convert(varchar(100),@ticketid) + ','+  convert(varchar(3),@iFlag)                      
commit tran   

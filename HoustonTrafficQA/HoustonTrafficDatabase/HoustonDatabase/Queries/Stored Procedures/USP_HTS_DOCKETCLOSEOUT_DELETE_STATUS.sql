SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DOCKETCLOSEOUT_DELETE_STATUS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DOCKETCLOSEOUT_DELETE_STATUS]
GO

CREATE PROCEDURE USP_HTS_DOCKETCLOSEOUT_DELETE_STATUS  --'1/24/2007',784142, 3991  
@UploadDate datetime,      
@TicketsViolationID int,      
@EmpID int      
as      
      
declare @Status nvarchar(5)      
declare @CourtLoc int      
declare @CourtDate datetime      
      
select @Status=ShortDescription, @CourtLoc=CourtNumber, @CourtDate=CourtDate from tbl_hts_docketcloseout_snapshot where TicketsViolationID = @TicketsViolationID --and DateDiff(day,@UploadDate,UploadDate)=-1  and DateDiff(day,@UploadDate,CourtDate)=0    
declare @StatusID int      
set @StatusID = (SELECT CourtViolationStatusID from tblcourtviolationstatus where ShortDescription =@Status and statustype=1 )      
begin transaction      
UPDATE tblTicketsViolations            
  set            
   --Update Court Date            
   CourtDate  = @CourtDate,            
   CourtDateMain = @CourtDate,            
   CourtDateScan = @CourtDate,            
            
   --Update Court Location            
   CourtNumber = @CourtLoc,            
   CourtNumbermain = @CourtLoc,            
   CourtNumberscan = @CourtLoc,            
            
   --Update Court Status            
   CourtViolationStatusID = @StatusID,            
   CourtViolationStatusIDMain = @StatusID,            
   CourtViolationStatusIDScan = @StatusID,            
        
 --EmployeeID        
 VEmployeeID = @EmpID            
        
  WHERE            
               
   TicketsViolationID = @TicketsViolationID             
     
    
UPDATE tbl_hts_docketcloseout_snapshot      
 set LastUpdate = ' ' where TicketsViolationID = @TicketsViolationID /*and DateDiff(day,@UploadDate,UploadDate)=-1*/  and DateDiff(day,@UploadDate,CourtDate)=0    
      
    
commit transaction 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


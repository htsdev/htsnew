/****** 
Create by		: Tahir Ahmed
Created Date	: 5/12/2009
TasK ID			: 5859

Business Logic:
	The stored procedure is by SQL Server Job "Update Jury Outcome Report".
	It is responsible to update case outome information in docket close out 
	snapshot table by getting information from HMC Data files.
	It gets update from 7 days dockets and searches information in data files
	untill next 7 days.

******/

-- USP_HTP_Update_JuryOutCome
alter procedure dbo.USP_HTP_Update_JuryOutCome

as

declare @NumberOfDockets int -- Number of dockets (startting from previous day) to update .....
declare @NumberOfDataFiles int -- number of data files to check for each docket after docket date....

-- Number of dockets (startting from previous day) to update .....
set  @NumberOfDockets = 6


-- number of data files to check for each docket after docket date....
set @NumberOfDataFiles = 7


declare @DocketDate datetime, @tempdate datetime

select @tempdate = dateadd(Day, -1, convert(varchar(10), getdate(), 101) )
select @docketdate = dateadd(day, -1 * @NumberOfDockets, @tempdate)

-- outer loop for last 7 dockets....
While datediff(day, @docketdate, @tempdate) >= 0 
begin

		declare @idx int,  @CourtDate datetime, @now datetime
		select @idx = 0,  @courtdate = @docketdate, @now = getdate()

		-- inner loop for checking data files upto next [n] days after docket date.....
		while @idx < @NumberOfDataFiles
		begin

			if datediff(day, dateadd(day, @idx, @courtdate) , getdate()) < 0
				break;

			-- gettting update from dlq file....
			update s
			set s.outcometype =3, 
				s.statusid = 146,
				s.outcomecomments = 'DLQ',
				S.outcomeupdatedate = @now
			from tbl_HTS_DocketCloseOut_Snapshot s 
			inner join tblticketsviolations v on v.ticketsviolationid  = s.ticketsviolationid
			inner join loaderfilesarchive.dbo.tbldlq a on v.casenumassignedbycourt = replace(a.causenumber,' ','')
			where datediff(Day, s.courtdate, @courtdate)=0
			and shortdescription = 'jur'
			and isnull(s.outcometype,0) =0 
			and datediff(Day, s.courtdate, a.associatedcasenumber) = @idx

			-- gettting update from event file....
			update s
			set s.outcometype =4, 
				s.outcomecomments = a.status,
				S.outcomeupdatedate = @now
			from tbl_HTS_DocketCloseOut_Snapshot s 
			inner join tblticketsviolations v on v.ticketsviolationid  = s.ticketsviolationid
			inner join loaderfilesarchive.dbo.tbleventextracttemp a on v.casenumassignedbycourt = replace(a.causenumber,' ','')
			where datediff(Day, s.courtdate, @courtdate)=0
			and shortdescription = 'jur'
			and isnull(s.outcometype,0) =0 
			and datediff(Day, s.courtdate, a.updatedate) = @idx

			-- gettting update from disposed file....
			update s
			set s.outcometype = 99, 
				s.outcomecomments = a.closecomments,
				S.outcomeupdatedate = @now
			from tbl_HTS_DocketCloseOut_Snapshot s 
			inner join tblticketsviolations v on v.ticketsviolationid  = s.ticketsviolationid
			inner join loaderfilesarchive.dbo.tblclosedcases a on v.casenumassignedbycourt = replace(a.causenumber,' ','')
			where datediff(Day, s.courtdate, @courtdate)=0
			and shortdescription = 'jur'
			and isnull(s.outcometype,0) =0 
			and datediff(Day, s.courtdate, a.closedate) = @idx

			-- tahir 6002 06/12/2009 
			-- gettting update from tickler file....
			update s
			set s.outcometype = 99, 
				s.outcomecomments = a.ticklerevent,
				S.outcomeupdatedate = @now
			from tbl_HTS_DocketCloseOut_Snapshot s 
			inner join tblticketsviolations v on v.ticketsviolationid  = s.ticketsviolationid
			inner join loaderfilesarchive.dbo.tblticklerextract a on v.casenumassignedbycourt = replace(a.causenumber,' ','')
			where datediff(Day, s.courtdate, @courtdate)=0
			and shortdescription = 'jur'
			and isnull(s.outcometype,0) =0 
			and datediff(Day, s.courtdate, a.updatedate) = @idx
			and isnull(charindex('CAPIAS PRO FINE IF CITIZEN DOES NOT PAY TODAY', a.ticklerevent),0) + isnull(charindex('DSC SHOW CAUSE HEARING', a.ticklerevent),0) > 0

			set @idx = @idx + 1
		end
		-- end of inner loop for 7 days data files....

		-- updating status id in snap shot table...
		update s
		set s.statusid = c.courtviolationstatusid
		from tbl_HTS_DocketCloseOut_Snapshot s
		inner join tblcourtviolationstatus c on c.description = s.outcomecomments
		where c.violationcategory = 0  
		and s.outcomeupdatedate = @now

		-- mover plead...
		update s
		set s.outcometype = c.dispositiontype
		from tbl_HTS_DocketCloseOut_Snapshot s
		inner join tblticketsviolations v on v.ticketsviolationid = s.ticketsviolationid
		inner join tblviolations m on m.violationnumber_pk = v.violationnumber_pk
		inner join tblcourtviolationstatus c on c.courtviolationstatusid = s.statusid
		where s.outcomeupdatedate = @now
		and s.outcometype  in (99,4)
		and c.dispositiontype in ( 2 )
		and m.categoryid = 1

		-- dsc/dadj
		update s
		set s.outcometype = c.dispositiontype
		from tbl_HTS_DocketCloseOut_Snapshot s
		inner join tblcourtviolationstatus c on c.courtviolationstatusid = s.statusid
		where s.outcomeupdatedate = @now
		and s.outcometype  in (99,4)
		and c.dispositiontype in (1 )

		-- dsc/dadj
		update tbl_HTS_DocketCloseOut_Snapshot
		set outcometype = 1
		where outcomecomments = 'DISMISSED AFTER PROOF OF FINANCIAL RESPONSIBILITY'
		and outcometype =99 
		and outcomeupdatedate = @now

		-- plea
		update s 
		set s.outcometype = 2
		from tbl_HTS_DocketCloseOut_Snapshot s
		inner join tblticketsviolations v on v.ticketsviolationid = s.ticketsviolationid
		inner join tblviolations m on m.violationnumber_pk = v.violationnumber_pk
		where s.outcomecomments = 'GUILTY TRIAL BY JUDGE'
		and s.outcometype =99 
		and s.outcomeupdatedate = @now
		and m.categoryid = 1

		-- dismissed/disposed...
		update tbl_HTS_DocketCloseOut_Snapshot
		set outcometype = 5
		where outcomecomments = 'DISMISSED AT TRIAL'
		and outcometype =99 
		and outcomeupdatedate = @now

		-- tahir 6002 06/12/2009 update from tickler data file...
		-- mover plead from tickler file...
		update s
		set s.outcometype = 2
		from tbl_HTS_DocketCloseOut_Snapshot s
		inner join tblticketsviolations v on v.ticketsviolationid = s.ticketsviolationid
		inner join tblviolations m on m.violationnumber_pk = v.violationnumber_pk
		where s.outcomeupdatedate = @now
		and s.outcometype  in (99)
		and m.categoryid = 1
		and s.outcomecomments  = 'CAPIAS PRO FINE IF CITIZEN DOES NOT PAY TODAY'

		-- DSC/DADJ from tickler file...
		update s
		set s.outcometype = 1
		from tbl_HTS_DocketCloseOut_Snapshot s
		where s.outcomeupdatedate = @now
		and s.outcometype  in (99)
		and s.outcomecomments = 'DSC SHOW CAUSE HEARING'

	set @docketdate = dateadd(day, 1, @docketdate)	
end
-- end outer loop for last 7 days dockets.....


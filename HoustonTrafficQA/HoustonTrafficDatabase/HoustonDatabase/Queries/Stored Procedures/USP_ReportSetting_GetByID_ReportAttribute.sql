/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:31:40 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  
* Created By		: Saeed Ahmed
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to return attributename, shortname
*                   : description 
* Parameter List  
* @AttributeID   	: this parameter contains the attribute id
*				
*/

CREATE PROCEDURE [dbo].[USP_ReportSetting_GetByID_ReportAttribute]
(@attributeid INT)
AS
	SELECT AttributeName,
	       ShortName,
	       DESCRIPTION,
	       AttributeType
	FROM   ReportAttribute
	WHERE  AttributeID = @attributeid
GO


GRANT EXECUTE ON [dbo].[USP_ReportSetting_GetByID_ReportAttribute] TO 
dbr_webuser
GO



﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_HTS_Insert_CourtInfo]    Script Date: 10/27/2008 10:56:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
    
    
/******     
Create by:  Sarim Ghnai    
Business Logic : This procedure is used to insert the court information in the database.    
     
List of Parameters:         
      @CaseIdentification: represnts Case Identification of court    
      @Accept Appeals:     represents Accept Appeals of court    
      @Appeallocation:     represents Appeal Location of court    
   @InitialSetting:  represents InitialSetting of court    
      @LOR:     represents LO Method of court    
      @AppearanceHire:  represents AppearanceHire of court    
      @JudgeTrialHire:  represents Judge Trial Hire of court    
   @JuryTrialHire:  represents Jury Trial Hire of court     
   @Bond Type:   represents Bond Type of court    
   @HandWritingAllowed: specify that handwriting is allowed or not     
   @AdditionalFTAIssues: specify additional FTA Issues    
      @GracePeriod:   represents Grace Period of the court    
   @Continuance   represents continuance of the court    
   @SupportingDocuments: specify that whether supporing documents are allowed or not.    
      @Comments:   represents comments about the court.     
    
******/    
    
--updated by khalid 2520, 2-1-08     
-- Noufil 4237 06/24/2008 Casetype field added to add the nature of court i-e civil,criminal or traffic  
-- Noufil 4945 10/27/2008 New column added isletterofRep
-- Nasir 5310 12/24/2008 New Column added ALR Process
ALTER procedure [dbo].[usp_HTS_Insert_CourtInfo]              
@courtname varchar(50) = null,              
@address varchar(50) = null,              
@address2 varchar(20) = null,              
@shortname varchar(9) = null,              
@city varchar(20) = null,              
@state int = 0,              
@visitcharges money = 0,              
@courttype tinyint = 0,              
@settingrequesttype tinyint = 0,              
@judgename varchar(50) = null,              
@zip varchar(10) = null,              
@courtcontact varchar(50) = null,              
@phone varchar(20) = null,              
@fax varchar(20) = null,              
@shortcourtname varchar(50)= null,              
@bondtype int = 0,              
@activeflag int = 0,    
@IsCriminalCourt int=0,    
@RepDate datetime,    
@CausenoDuplicate bit,      
@CourtId int,    
@CaseIdentifiaction int,    
@AcceptAppeals int,    
@AppealLocation varchar(200),    
@InitialSetting int,    
@LOR int,    
@AppearnceHire varchar(200),    
@JudgeTrialHire varchar(200),    
@JuryTrialHire varchar(200),    
@HandwritingAllowed int,    
@AdditionalFTAIssued int,    
@GracePeriod int,
--Nasir 5310 12/29/2008 take parameter @ALRProcess
@ALRProcess INT,    
@Continuance varchar(200),    
@SupportingDocument int,    
@Comments varchar(1000),  
@CaseTypeId int,
@isletterofrep INT,
--Sabir Khan 5763 04/16/2009 
@islorsubpoena INT,
@islormod INT,
--Sabir Khan 5941 07/24/2009 CountyId parameter has been added for association of county with court location...
@CountyId INT,
--Nasir 7369 02/19/2010 added AllowOnlineSignUp     
--@AllowOnlineSignUp BIT,
-- Asad Ali 8153 09/20/2010 add associated court ID 
@AssociatedALRCourtID INT,
@AllowCourtNumbers BIT -- Rab Nawaz Khan 8997 10/17/2011 AllowCourtNumbers column added for insertion... 

      
as

declare @tempcourtname varchar(50)              
select @tempcourtname = courtname from tblcourts where courtname = @courtname

if @tempcourtname is null         
	begin          --Nasir 5310 12/29/2008 insert parameter @ALRProcess
		--Sabir Khan 5941 07/24/2009 CountyId column has been added for association of county wwith court location...
		--Nasir 7369 02/19/2010 added AllowOnlineSignUp
		insert into tblcourts(courtname,address,address2,city,state,visitcharges,courttype,settingrequesttype,judgename,zip,courtcontact,phone,fax,shortcourtname,bondtype,inactiveflag,shortname,IsCriminalCourt,RepDate,CausenoDuplicate,CaseIdentifiaction,AcceptAppeals,AppealLocation,InitialSetting,LOR,AppearnceHire,JudgeTrialHire,JuryTrialHire,HandwritingAllowed,AdditionalFTAIssued,GracePeriod,ALRProcess,Continuance,SupportingDocument,Comments,CaseTypeId,IsletterofRep,IsLORSubpoena,IsLORMOD,CountyId,AssociatedALRCourtID, AllowCourtNumbers)
		values (@courtname,@address,@address2,@city,@state,@visitcharges,@courttype,@settingrequesttype,@judgename,@zip,@courtcontact,@phone,@fax,@shortcourtname,@bondtype,@activeflag,@shortname,@IsCriminalCourt,@RepDate,@CausenoDuplicate,@CaseIdentifiaction,@AcceptAppeals,@AppealLocation,@InitialSetting,@LOR,@AppearnceHire,@JudgeTrialHire,@JuryTrialHire,@HandwritingAllowed,@AdditionalFTAIssued,@GracePeriod,@ALRProcess,@Continuance,@SupportingDocument,@Comments,@CaseTypeId,@isletterofrep,@islorsubpoena,@islormod,@CountyId,@AssociatedALRCourtID, @AllowCourtNumbers)
		set @CourtId = SCOPE_IDENTITY()        
		select @CourtId      
	end          
else       
	begin         
		set @CourtId = 0       
		select @CourtId    
	end    
    
    
    
     
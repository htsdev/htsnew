SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_GET_CasesInQueue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_GET_CasesInQueue]
GO



CREATE Procedure [dbo].[USP_HMC_GET_CasesInQueue] --3992 
(  
 @empid int  
)  
  
as  
  
SELECT DISTINCT   
   
   
   tva.TicketNumber_PK,
   dbo.tbl_HMC_EFile.Flag,
   ta.MidNumber,   
   tva.ViolationDescription,   
   tva.FineAmount,   
   ta.FirstName,   
   ta.LastName,   
   ta.Initial,   
   tva.CauseNumber,   
   ta.DOB,   
   tva.TicketViolationDate,   
   tva.Courtnumber,   
   tva.CourtDate,   
   tva.RecordID,   
dbo.tbl_HMC_EFile.id,
   ISNULL(tcv.ShortDescription, N'N/A') AS ShortDescription  
 FROM         dbo.tblTicketsArchive AS ta INNER JOIN  
      dbo.tblCourtViolationStatus AS tcv INNER JOIN  
      dbo.tblTicketsViolationsArchive AS tva ON tcv.CourtViolationStatusID = tva.violationstatusid ON ta.RecordID = tva.RecordID INNER JOIN  
      dbo.tbl_HMC_EFile ON ta.RecordID = dbo.tbl_HMC_EFile.RecordId  
 where  empid = @empid  
  
  
  
 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


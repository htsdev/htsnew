USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Get_Businesses_Count]    Script Date: 12/28/2011 17:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Altered by:		Sabir Khan
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Business Startup letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting Registration date for Business letter.
	@endListDAte:	Ending Registration date for Business letter.	

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

-- usp_mailer_Get_Businesses_Count 24, 47 , 24, '11/18/2011','11/18/2011', 0
ALTER Procedure [dbo].[usp_mailer_Get_Businesses_Count] 
 ( 
 @catnum int, 
 @LetterType int, 
 @crtid int, 
 @startListdate Datetime, 
 @endListdate DateTime, 
 @SearchType int 
 ) 
 
as 
 
declare @officernum varchar(50), 
 @officeropr varchar(50), 
 @tikcetnumberopr varchar(50), 
 @ticket_no varchar(50), 
 @zipcode varchar(50), 
 @zipcodeopr varchar(50), 
 @fineamount money, 
 @fineamountOpr varchar(50) , 
 @fineamountRelop varchar(50), 
 @singleviolation money, 
 @singlevoilationOpr varchar(50) , 
 @singleviolationrelop varchar(50), 
 @doubleviolation money, 
 @doublevoilationOpr varchar(50) , 
 @doubleviolationrelop varchar(50), 
 @count_Letters int, 
 @violadesc varchar(500), 
 @violaop varchar(50), 
 @sqlquery nvarchar(4000) , 
 @sqlquery2 nvarchar(4000) , 
 @sqlParam nvarchar(1000) 
 
 
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 
 @endListdate DateTime, @SearchType int' 
 
 
 
select @endlistdate = @endlistdate+'23:59:59.000', 
 @sqlquery ='', 
 @sqlquery2 = '' 
 
Select @officernum=officernum, 
 @officeropr=oficernumOpr, 
 @tikcetnumberopr=tikcetnumberopr, 
 @ticket_no=ticket_no, 
 @zipcode=zipcode, 
 @zipcodeopr=zipcodeLikeopr, 
 @singleviolation =singleviolation, 
 @singlevoilationOpr =singlevoilationOpr, 
 @singleviolationrelop =singleviolationrelop, 
 @doubleviolation = doubleviolation, 
 @doublevoilationOpr=doubleviolationOpr, 
 @doubleviolationrelop=doubleviolationrelop, 
 @violadesc=violationdescription, 
 @violaop=violationdescriptionOpr , 
 @fineamount=fineamount , 
 @fineamountOpr=fineamountOpr , 
 @fineamountRelop=fineamountRelop 
from tblmailer_letters_to_sendfilters 
where courtcategorynum=@catnum 
and Lettertype=@LetterType 
 
set @sqlquery=@sqlquery+' 
 Select distinct ' 
 
if @searchtype = 0 
 set @sqlquery=@sqlquery+' 
 convert(datetime , convert(varchar(10),ta.registrationdate,101)) as mdate, ' 
else 
 set @sqlquery=@sqlquery+' 
 convert(datetime , convert(varchar(10),tva.courtdate,101)) as mdate, ' 
 
 
set @sqlquery=@sqlquery+' 
 flag1 = isnull(ta.Flags,''N'') , 
 flag2 = isnull(tva.Flags,''N''),
 case when isnull(ta.nomailflag, 0) = 1 or isnull(tva.nomailflag, 0) = 1 then 1 else 0 end as donotmailflag, ta.businessid as recordid, 
 count(tva.ownernumber) as ViolCount, 
 0 as Total_Fineamount
 ,TA.[Address], TVA.OwnerAddress , TVA.OwnerName , TA.BusinessName
into #temp 
FROM assumednames.dbo.businessowner TVA ,assumednames.dbo.business TA 
where TVA.businessid = TA.businessid 
and datediff(Day, ta.registrationdate , @startListdate)<=0
and datediff(Day, ta.registrationdate , @endlistdate )>=0' 

set @sqlquery =@sqlquery+ ' 
and not exists (
	select recordid from tblletternotes 
	where recordid = ta.businessid
	and lettertype = @lettertype
	) 

 group by 
 convert(datetime , convert(varchar(10),ta.registrationdate,101)) , 
 ta.Flags,tva.Flags, 
 ta.nomailflag, tva.nomailflag , ta.businessid
, TA.[Address], TVA.OwnerAddress , TVA.OwnerName , TA.BusinessName 
' 

set @sqlquery=@sqlquery + ' 
Select distinct IDENTITY(INT,1,1) AS RowID, 
ViolCount, Total_Fineamount, mdate ,Flag1,Flag2, donotmailflag, recordid 
,[Address], OwnerAddress , OwnerName , BusinessName 
into #temp1 from #temp 
 ' 
 
set @sqlquery2=@sqlquery2 +' 

Select 
--Sabir Khan 9975 01/11/2012 Need to count record id instead of row id
--count(distinct RowID) as Total_Count, 
count(distinct recordid) as Total_Count,
mdate 
into #temp2 
from #temp1 
group by mdate 
 
Select 
--Sabir Khan 9975 01/11/2012 Need to count record id instead of row id
--count(distinct RowID) as Good_Count, 
count(distinct recordid) as Good_Count, 
mdate 
into #temp3 
from #temp1 
where Flag1 in (''Y'',''D'',''S'') 
and Flag2 in (''Y'',''D'',''S'') -- Sabir Khan 9948 12/28/2011 Fixed barcode issue.
and donotmailflag = 0 
group by mdate 
 
Select 
--Sabir Khan 9975 01/11/2012 Need to count record id instead of row id
--count(distinct RowID) as Bad_Count, 
count(distinct recordid) as Bad_Count, 
mdate 
into #temp4 
from #temp1 
where Flag1 not in (''Y'',''D'',''S'') 
and Flag2 not in (''Y'',''D'',''S'') -- Sabir Khan 9948 12/28/2011 Fixed barcode issue.
and donotmailflag = 0 
group by mdate 
 
Select 
--count(distinct RowID) as DonotMail_Count, 
count(distinct recordid) as DonotMail_Count, 
mdate 
into #temp5 
from #temp1 
where donotmailflag=1 
group by mdate' 
 
 
set @sqlquery2 = @sqlquery2 +' 
Select #temp2.mdate as listdate, 
 Total_Count, 
 isnull(Good_Count,0) Good_Count, 
 isnull(Bad_Count,0)Bad_Count, 
 isnull(DonotMail_Count,0)DonotMail_Count 
from #Temp2 left outer join #temp3 
on #temp2.mdate=#temp3.mdate 
left outer join #Temp4 
on #temp2.mdate=#Temp4.mdate 
left outer join #Temp5 
on #temp2.mdate=#Temp5.mdate 
where Good_Count > 0 order by #temp2.mdate desc 
drop table #temp drop table #temp1 drop table #temp2 drop table #temp3 drop table #temp4 drop table #temp5 
' 
 
print @sqlquery + @sqlquery2 
--exec( @sqlquery) 
set @sqlquery = @sqlquery + @sqlquery2 
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


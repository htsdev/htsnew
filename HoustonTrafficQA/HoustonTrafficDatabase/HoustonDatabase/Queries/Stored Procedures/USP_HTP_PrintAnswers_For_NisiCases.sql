USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_MAILER_Send_TrackBatches]    Script Date: 04/11/2013 18:36:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Created By :	Rab Nawaz
Task ID	:		10729
Date :			04/11/2013
Business Logic:	The procedure is used by HTP to get data for selected clients to print the NISI Answer letter .
				
List of Parameters:
	@TicketIds :	Ticket IDs of Clients 
	@EmployeeId		Employee Id who Print the letter
	
*******/
--[dbo].[USP_HTP_PrintAnswers_For_NisiCases] '1,2,3,', 3992, 1, 'Testing'
CREATE PROCEDURE [dbo].[USP_HTP_PrintAnswers_For_NisiCases]
 (                    
	@RowId varchar(1000),
	@EmployeeId INT = 3992,
	@btn_printAnswerClicked BIT, 
	@filePath VARCHAR(1000) = NULL
 )            
AS

BEGIN
	IF @btn_printAnswerClicked = 1
	BEGIN
		
		-- Checking for the already printed answers. if already printed then do not print the answer again.
		SELECT RowID INTO #EmptyPrintAnswers 
		FROM TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader
		WHERE RowID IN (SELECT * FROM dbo.sap_string_param(@RowId))
		AND (ISNULL(PrintAnswerDate, '01/01/1900') = '01/01/1900' OR  (PrintAnswerDate IS NULL))
		
		-- Only Updating the Records which answer has not printed. . . 
		UPDATE TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader
		SET PrintAnswerDate = GETDATE(), PrintedDocs = @filePath, PrintUserId = @EmployeeId
		WHERE RowID IN (SELECT * FROM #EmptyPrintAnswers)
		
		-- Selecting all the records which selected in the grid to print the letters again.
		SELECT nisi.RowID, ISNULL(nisi.PrintAnswerDate, '01/01/1900') AS PrintAnswerDate, nisi.NisiCauseNumber, nisi.[First Name], nisi.[Last Name]
		FROM TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader nisi
		WHERE nisi.RowID IN (SELECT * FROM dbo.sap_string_param(@RowId))
		
	END
	ELSE
	BEGIN
		-- If clicked From Grid then only show the Answered file. do not update the Print Answer
		SELECT nisi.RowID, ISNULL(nisi.PrintAnswerDate, '01/01/1900') AS PrintAnswerDate, nisi.NisiCauseNumber, nisi.[First Name], nisi.[Last Name]
		FROM TrafficTickets.dbo.tblNISI_ClientsUploadedByLoader nisi
		WHERE nisi.RowID IN (SELECT * FROM dbo.sap_string_param(@RowId))
	END	
END

GO
	GRANT EXECUTE ON [dbo].[USP_HTP_PrintAnswers_For_NisiCases] TO dbr_webuser
GO
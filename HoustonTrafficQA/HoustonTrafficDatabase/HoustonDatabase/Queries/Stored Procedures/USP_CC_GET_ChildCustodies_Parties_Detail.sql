set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/*********

Created By		: Zeeshan Ahmed

Business Logic	: This Procedure is used to get list of child custody parties details.

List of Input Parameters

		@ChildCustodyID Child Custody Case Id

List Of Columns :

		PartyStatus
		AssociatedAttorney
		Address
		PhoneNumber
		AddressDate
		ConnectionName
		ChildCustodyId
		City
		state
		ZipCode
		DP2
		DPC
		Flag1
		FirstName
		LastName
		MiddleName
		IsCompany
*********/

Create  PROCEDURE [dbo].[USP_CC_GET_ChildCustodies_Parties_Detail]
(
	@ChildCustodyID int


)AS


SELECT 


Name,
P.PartyStatus,
P.AssociatedAttorney,
P.Address,
P.PhoneNumber,
case when datediff( day , '01/01/1900',isnull(P.AddressDate,'01/01/1900'))=0 then 'N/A' else convert(varchar , P.AddressDate,101) end as AddressDate , 


C.ConnectionName,
P.ChildCustodyId,
P.City,
s.state,
P.ZipCode,
P.DP2,
P.DPC,
P.Flag1,
P.FirstName,
P.LastName,
P.MiddleName,
P.IsCompany
from 
ChildCustodyParties P
JOin 
childcustodyconnection C
on p.ChildCustodyConnectionID = C.ChildCustodyConnectionID
join tblstate  S
on s.stateid = P.StateId

WHERE 
ChildCustodyId = @ChildCustodyId

go

grant execute on [USP_CC_GET_ChildCustodies_Parties_Detail] to dbr_webuser

go
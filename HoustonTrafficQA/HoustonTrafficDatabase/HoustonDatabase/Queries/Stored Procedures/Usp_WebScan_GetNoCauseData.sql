SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_WebScan_GetNoCauseData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_WebScan_GetNoCauseData]
GO


CREATE procedure [dbo].[Usp_WebScan_GetNoCauseData]    
              
@BatchID as int                         
as                        
                        
              
select o.picid,              
  o.CauseNo,              
  o.Status,              
  convert(varchar(12),o.NewCourtDate,101) as   CourtDate,            
  o.CourtNo as CourtRoomNo,              
  o.Location as CourtLocation,              
  o.Time as CourtTime,  
  isnull(TypeScan,'') as TypeScan,
  isnull(ProblemMsg,'') as ProblemMsg               
from tbl_webscan_ocr o              
  inner join tbl_webscan_pic p        
 on o.picid=p.id              
  where              
  p.batchid=@BatchID and o.checkstatus=  5           
  order by o.picid                
              

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿
    
/******       
      
Create by:  Nasir 5771 04/16/2009    
      
Business Logic : This procedure is used get data of disposed case and active non disposed  case and quote client and non client against Contact     
      
List of Parameters:           
      
     Ticket ID : Ticket ID of case      
           
List of Columns:      
    
• Cause Number     
• Ticket Number    
• Matter Description    
• Crt (Court Number)    
• Crt Loc (Court Location which is a short name of a court)    
• Status (Verified Status)    
• Crt Date (Court Date)    
• Crt Time (Court Time)    
• Insert Date	
• CaseTypeId  
  
******/      
    
    
    
--USP_HTP_Get_AssociatedMattersOfContact 59791     
             
ALTER PROCEDURE dbo.USP_HTP_Get_AssociatedMattersOfContact
	@TicketID INT
AS 
	SET NOCOUNT ON ; 
	DECLARE @ContactID INT        
	SELECT @ContactID = tt.ContactID_FK
	FROM   tblTickets tt WITH(NOLOCK)
	WHERE  tt.TicketID_PK = @TicketID 
	
	
	--Select the records of active client whose case status is not disposed    
	SELECT TV.TicketID_PK,
	       ISNULL(casenumassignedbycourt, 'N/A') AS CauseNumber,
	       TV.RefCaseNumber AS TicketNumber,
	       UPPER(
	           dbo.tblViolations.Description + (
	               CASE 
	                    WHEN tv.violationnumber_pk = 0 THEN ''
	                    ELSE ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + 
	                         ')'
	               END
	           )
	       ) AS MatterDescription,
	       dbo.tblCourts.ShortName AS CourtLocation,
	       ISNULL(TV.CourtDateMain, '') AS CourtDate,
	       ISNULL(tblCourtViolationStatus_2.ShortDescription, 'N/A') AS 
	       CourtStatus,
	       CASE   --Sabir Khan 7979 07/07/2010 type changed   
	            WHEN (LEN(TV.CourtNumbermain) > 0) THEN 
	                 CONVERT(VARCHAR(3), TV.CourtNumbermain)	            
	            	ELSE
	            		'0'
	       END AS RoomNumber,
	       ISNULL(T.Recdate, '') AS InsertDate 
	       --,ISNULL(CL.LevelCode ,'') AS LevelCode
	       --,ISNULL(TV.ArrestDate ,'1/1/1900') AS ArrestDate  
	       ,
	       t.CaseTypeId
	FROM   dbo.tblTicketsViolations AS TV WITH(NOLOCK)
	       INNER JOIN dbo.tblTickets AS t WITH(NOLOCK)
	            ON  TV.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblViolations WITH(NOLOCK)
	            ON  TV.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK
	       LEFT OUTER JOIN dbo.tblViolationCategory AS vc WITH(NOLOCK)
	            ON  dbo.tblViolations.CategoryID = vc.CategoryId
	       LEFT OUTER JOIN dbo.tblCourts WITH(NOLOCK)
	            ON  TV.CourtID = dbo.tblCourts.Courtid
	       LEFT OUTER JOIN dbo.tblCourtViolationStatus AS 
	            tblCourtViolationStatus_2 WITH(NOLOCK)
	            ON  TV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID
	       LEFT OUTER JOIN tblDateType AS tdt WITH(NOLOCK)
	            ON  tdt.TypeID = tblCourtViolationStatus_2.CategoryID
	WHERE  t.ContactID_FK = @ContactID
	       AND tdt.TypeID <> 50
	       AND t.Activeflag = 1 
	
	
	
	
	
	
	
	
	
	--Select the records of quote client    
	
	SELECT TV.TicketID_PK,
	       ISNULL(casenumassignedbycourt, 'N/A') AS CauseNumber,
	       TV.RefCaseNumber AS TicketNumber,
	       UPPER(
	           dbo.tblViolations.Description + (
	               CASE 
	                    WHEN tv.violationnumber_pk = 0 THEN ''
	                    ELSE ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + 
	                         ')'
	               END
	           )
	       ) AS MatterDescription,
	       dbo.tblCourts.ShortName AS CourtLocation,
	       ISNULL(TV.CourtDateMain, '') AS CourtDate,
	       ISNULL(tblCourtViolationStatus_2.ShortDescription, 'N/A') AS 
	       CourtStatus,
	       CASE  --Sabir Khan 7979 07/07/2010 type changed   
	            WHEN (LEN(TV.CourtNumbermain) > 0) AND (ISNUMERIC(TV.CourtNumbermain) = 1) THEN 
	                 CONVERT(VARCHAR(3), TV.CourtNumbermain)	            
	            ELSE
	            		'0'
	       END AS RoomNumber,
	       ISNULL(T.Recdate, '') AS InsertDate 
	       --,ISNULL(CL.LevelCode ,'') AS LevelCode
	       --,ISNULL(TV.ArrestDate ,'1/1/1900') AS ArrestDate  
	       ,
	       t.CaseTypeId
	FROM   dbo.tblTicketsViolations AS TV
	       INNER JOIN dbo.tblTickets AS t
	            ON  TV.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblViolations
	            ON  TV.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK
	       LEFT OUTER JOIN dbo.tblViolationCategory AS vc
	            ON  dbo.tblViolations.CategoryID = vc.CategoryId
	       LEFT OUTER JOIN dbo.tblCourts
	            ON  TV.CourtID = dbo.tblCourts.Courtid
	       LEFT OUTER JOIN dbo.tblCourtViolationStatus AS 
	            tblCourtViolationStatus_2
	            ON  TV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID
	       LEFT OUTER JOIN tblDateType AS tdt
	            ON  tdt.TypeID = tblCourtViolationStatus_2.CategoryID
	WHERE  t.ContactID_FK = @ContactID
	       AND t.Activeflag = 0 
	
	
	
	--Select the records of non client    
	SELECT tta.RecordID AS TicketID_PK,  
        ISNULL(ttva.CauseNumber, '') AS CauseNumber,  
        ttva.TicketNumber_PK AS TicketNumber          
        ,ISNULL(ttva.ViolationDescription, '') AS MatterDescription,  
        dbo.tblCourts.ShortName AS CourtLocation,  
         ISNULL(ttva.CourtDate, '') AS CourtDate,  
        ISNULL(tcvs.ShortDescription, 'N/A') AS CourtStatus,  
        CASE   --Sabir Khan 7979 07/07/2010 type changed   
             WHEN (LEN(ttva.Courtnumber) > 0) AND (ISNUMERIC(ttva.Courtnumber) = 1) THEN   
                  CONVERT(VARCHAR(3), ttva.Courtnumber)
	         ELSE
	            		'0'
        END AS RoomNumber,  
        ISNULL(tta.RecLoadDate, '') AS InsertDate,  
        dbo.tblCourts.CaseTypeID,  
        tta.Address1,  
        tta.ZipCode , 
        tta.MidNumber  
 FROM   dbo.tblTicketsArchive tta  
        INNER JOIN dbo.tblTicketsViolationsArchive ttva  
             ON  tta.RecordID = ttva.RecordID  
        LEFT JOIN dbo.tblViolations  
             ON  ttva.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK  
        LEFT OUTER JOIN dbo.tblViolationCategory AS vc  
             ON  dbo.tblViolations.CategoryID = vc.CategoryId  
        LEFT OUTER JOIN tblCourtViolationStatus tcvs  
             ON  ttva.violationstatusid = tcvs.CourtViolationStatusID  
        LEFT OUTER JOIN dbo.tblCourts  
             ON  ttva.CourtLocation = dbo.tblCourts.Courtid  
 WHERE  tta.Clientflag = 0  
        AND tta.PotentialContactID_FK = @ContactID   
        
     union
          
SELECT CONVERT( VARCHAR ,MAX(p.ChildCustodyPartyId)) AS TicketID_PK 
      ,ISNULL(cc.CaseNumber,'') AS CaseNumber  
      ,ISNULL(cc.CaseNumber,'') AS TicketNumber  
      ,ISNULL(SettingTypeName,'') AS MatterDescription  
      ,'HC Cvl' AS CourtLocation  
      ,ISNULL(CC.NextSettingDate,'') AS CourtDate  
      ,'Non Client' AS CourtStatus  
      ,'' AS RoomNumber  
      ,ISNULL(cc.RecLoadDate,'') AS InsertDate  
      ,'4' AS CaseTypeID  
      ,MAX(p.[Address]) AS [Address]  
      ,MAX(p.ZipCode) AS ZipCode  
     ,CONVERT( VARCHAR ,MAX(p.ChildCustodyPartyId)) AS MidNumber  
FROM   civil.dbo.ChildCustody CC  
       JOIN civil.dbo.ChildCustodySettingType CCST  
            ON  CC.ChildCustodySettingTypeId = CCST.ChildCustodySettingTypeId  
       LEFT OUTER JOIN civil.dbo.ChildCustodyParties P  
            ON  CC.ChildCustodyId = P.ChildCustodyID  
       INNER JOIN civil.dbo.ChildCustodyConnection C  
            ON  C.ChildCustodyConnectionid = P.ChildCustodyConnectionid  
WHERE  CC.ClientFlag = 0  
       AND C.Canbemailed = 1  
       AND ISNULL(P.IsCompany ,0) = 0  
       AND P.PotentialContactID_FK = @ContactID
GROUP BY  
       cc.CaseNumber  
      ,CC.childcustodyid  
      ,SettingTypeName  
      ,CC.NextSettingDate  
      ,cc.RecLoadDate  
      ,p.ChildCustodyPartyId  
	
	
	--Select the records of active client whose case status is disposed    
	SELECT TV.TicketID_PK,
	       ISNULL(casenumassignedbycourt, 'N/A') AS CauseNumber,
	       TV.RefCaseNumber AS TicketNumber,
	       UPPER(
	           dbo.tblViolations.Description + (
	               CASE 
	                    WHEN tv.violationnumber_pk = 0 THEN ''
	                    ELSE ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A') + 
	                         ')'
	               END
	           )
	       ) AS MatterDescription,
	       dbo.tblCourts.ShortName AS CourtLocation,
	       ISNULL(TV.CourtDateMain, '') AS CourtDate,
	       ISNULL(tblCourtViolationStatus_2.ShortDescription, 'N/A') AS 
	       CourtStatus,
	       CASE  --Sabir Khan 7979 07/07/2010 type changed  
	            WHEN (LEN(TV.CourtNumbermain) > 0) AND (ISNUMERIC(TV.CourtNumbermain) = 1) THEN 
	                 CONVERT(VARCHAR(3), TV.CourtNumbermain)	            
	            ELSE
	            		'0'
	       END AS RoomNumber,
	       ISNULL(T.Recdate, '') AS InsertDate 
	       --,ISNULL(CL.LevelCode ,'') AS LevelCode
	       --,ISNULL(TV.ArrestDate ,'1/1/1900') AS ArrestDate  
	       ,
	       t.CaseTypeId
	FROM   dbo.tblTicketsViolations AS TV
	       INNER JOIN dbo.tblTickets AS t
	            ON  TV.TicketID_PK = t.TicketID_PK
	       INNER JOIN dbo.tblViolations
	            ON  TV.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK
	       LEFT OUTER JOIN dbo.tblViolationCategory AS vc
	            ON  dbo.tblViolations.CategoryID = vc.CategoryId
	       LEFT OUTER JOIN dbo.tblCourts
	            ON  TV.CourtID = dbo.tblCourts.Courtid
	       LEFT OUTER JOIN dbo.tblCourtViolationStatus AS 
	            tblCourtViolationStatus_2
	            ON  TV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID
	       LEFT OUTER JOIN tblDateType AS tdt
	            ON  tdt.TypeID = tblCourtViolationStatus_2.CategoryID
	WHERE  t.ContactID_FK = @ContactID
	       AND tdt.TypeID = 50
	       AND t.Activeflag = 1   
	
	SELECT @ContactID AS ContactID     
            
            
            
            

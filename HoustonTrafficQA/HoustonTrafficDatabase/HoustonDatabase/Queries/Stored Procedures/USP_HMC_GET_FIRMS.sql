SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_GET_FIRMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_GET_FIRMS]
GO



CREATE procedure [dbo].[USP_HMC_GET_FIRMS]
as
select * from tblfirm


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


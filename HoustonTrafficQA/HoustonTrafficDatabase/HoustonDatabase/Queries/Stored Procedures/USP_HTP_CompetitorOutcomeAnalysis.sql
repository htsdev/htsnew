/**************************  
Created By:  Waqas Javed  
Create Date: 16/12/2008  
  
Business Logic:  
 The stored procedure is used by HTP /Backroom /Competitor Outcome Analysis Report.   
 The purpose of stored procedure is determine if hiring an attorney is more beneficial 
 to the client than the client handling his traffic matter without an attorney.

 The second purpose is to determine if hiring one law firm is more 
 beneficial to the client than hiring  another law firm.

  
Input Parameters:  
 @startdate:  Start date to search attorneys/bonding companies hired on   
 @enddate:  End date to search attorneys/bonding companies hired on   
 @Type:   Type of the report 'By Client' or 'By Cases' 
 @NoOfRecords: Number of attornyes to be displayed  
  
attorney : Name of the attorney or bonding company 
clients : Total number of clients
cases : Total number of cases
c_clients : Number of client Disposed
c_cases : Number of cases Disposed
dsc_clients : Number od client with DSC/DADJ
dsc_cases : Number of Cases with DSC/ DADJ
plea_clients : Number of Clients with movers Plead
plea_cases : Number of cases with movers Plead
perc_f_clients : Clients Disposed in Percentage
perc_f_cases : Cases Disposed in Percentage
perc_g_clients : Clients DSC/DADJ/Movers Plead in Percentage
perc_g_cases : Cases DSC/DADJ/Movers Plead in Percentage 
  
******************************/  
  
    
-- USP_HTP_CompetitorOutcomeAnalysis '4/9/09', '4/9/09', 1, 10
alter procedure [dbo].[USP_HTP_CompetitorOutcomeAnalysis]                
                
@sdate datetime,                
@edate datetime,                
@Type int,     
@NoOfRecords int   

as

--Declare @sdate datetime, @edate datetime
--select @sdate = '06/01/2008', @edate = '11/30/2008'

-- tahir 5462 04/09/2009 fine tuned the stored procedure for performance...

select distinct replace(causenumber, ' ','') as causenumber, max(rowid) maxrowid
into #cases
from loaderfilesarchive.dbo.tblEventExtractTemp
where datediff(day, courtdate, @sdate) <= 0
and   datediff(day, courtdate, @edate) >= 0
group by replace(causenumber, ' ','')

-- select * from #cases

-- first get all the uploads.....
select distinct v.causenumber, t.midnumber, case when isnull(m.categoryid,0) = 1 then 1 else 0 end as Moving,
	c.maxrowid
into #uploads
from tblticketsarchive t inner join tblticketsviolationsarchive v on v.recordid = t.recordid
inner join tblviolations m on m.description = v.violationdescription
inner join #cases c on c.causenumber = v.causenumber
where v.courtlocation in (3001,3002,3003)

-- select * from #uploads

-- getting attorney info for all cases...
select distinct b.*, a.attorneyname
into #compt2
from loaderfilesarchive.dbo.tblEventExtractTemp_competitor a
inner join #uploads b on b.causenumber = replace(a.causenumber, ' ','')
and len(isnull(a.bondingcompanyname,'') + isnull(a.bondingnumber,'')) = 0

-- select * from #compt2

select distinct 
	v.midnumber, 
	v.causenumber, 
	c.status,
	convert(datetime,convert(varchar,datepart(month, c.courtdate)) + '/01/'+ convert(varchar,datepart(year, c.courtdate)))  as eventdate,
	isnull(v.attorneyname,'') as attorney,
	s. dispositiontype, 
	v.moving
into #temp
from loaderfilesarchive.dbo.tblEventExtractTemp c
inner join #compt2 v on v.causenumber = replace(c.causenumber, ' ','') and c.rowid= v.maxrowid
inner join tblcourtviolationstatus s on s.description = c.status

-- select * from #temp


update #temp set attorney = 'NO ATTORNEY' where len(attorney) = 0

select distinct c.causenumber , min(c.rowid) as rowid
into #disp
from loaderfilesarchive.dbo.tblclosedcases c inner join #temp a on a.causenumber = replace(c.causenumber,' ','')
group by c.causenumber

select distinct replace(a.causenumber, ' ' ,'') as causenumber, 
	   closecomments,
		convert(smalldatetime, closedate) as eventdate
into #disp2
from loaderfilesarchive.dbo.tblclosedcases a , #disp b
where a. rowid = b.rowid

update a
set a.status = 'DISPOSED/DISMISSED', A.dispositiontype = 99
from #temp a  inner join #disp2 d 
on d.causenumber = a.causenumber
and a.dispositiontype not in (1,2)

delete from #temp where dispositiontype not in (1,2,99)

-- total clients...
select distinct attorney, eventdate, count(distinct midnumber) as clients, count(distinct causenumber) as cases
into #attorney 
from #temp group by attorney, eventdate


-- dsc/dadj cases...
select attorney, eventdate, count(distinct midnumber) as clients, count(distinct causenumber) as cases
into #temp2
from #temp
where  dispositiontype = 1 
group by attorney,eventdate

-- plead guilty cases...
select attorney, eventdate, count(distinct midnumber) as clients, count(distinct causenumber) as cases
into #temp3
from #temp
where  dispositiontype = 2 and moving = 1 and charindex('fta',causenumber) = 0 
group by attorney,eventdate

select a.attorney, a.eventdate, 
	   isnull(sum(isnull(a.clients,0)),0) as clients, 
	   isnull(sum(isnull(a.cases,0)),0)	as cases, 
	   isnull(sum(isnull(c.clients,0)),0) as dsc_clients,
	   isnull(sum(isnull(c.cases,0)),0) as dsc_cases,
	   isnull(sum(isnull(d.clients,0)),0) as plea_clients,
	   isnull(sum(isnull(d.cases,0)),0) as plea_cases
into #result
from #attorney a
left outer join #temp2 c on c.attorney = a.attorney and c.eventdate = a.eventdate
left outer join #temp3 d on d.attorney = a.attorney and d.eventdate = a.eventdate
group by a.attorney , a.eventdate 

select attorney, eventdate, clients, cases,
		(clients - ( dsc_clients + plea_clients)) as c_clients,
		(cases - ( dsc_cases + plea_cases)) as c_cases,
		dsc_clients, dsc_cases, plea_clients, plea_cases

into #final
from #result

alter table #final alter column clients numeric(10,2)
alter table #final alter column cases numeric(10,2)
alter table #final alter column c_clients numeric(10,2)
alter table #final alter column c_cases numeric(10,2)
alter table #final alter column dsc_clients numeric(10,2)
alter table #final alter column dsc_cases numeric(10,2)
alter table #final alter column plea_clients numeric(10,2)
alter table #final alter column plea_cases numeric(10,2)

if @Type=1
Begin
select top (@NoOfRecords) *, (c_clients / clients)  as perc_f_clients, 
	  (c_cases / cases) as perc_f_cases,
	  (dsc_clients + plea_clients) / (clients )  as perc_g_clients,
	  (dsc_cases + plea_cases) / (cases)  as perc_g_cases

from #final 
order by clients desc
end
else
begin
select top (@NoOfRecords) *, (c_clients / clients)  as perc_f_clients, 
	  (c_cases / cases) as perc_f_cases,
	  (dsc_clients + plea_clients) / (clients )  as perc_g_clients,
	  (dsc_cases + plea_cases) / (cases)  as perc_g_cases

from #final 
order by cases desc
end


drop table #cases
drop table #uploads
drop table #compt2
drop table #disp
drop table #disp2
drop table #temp
drop table #attorney
drop table #temp2
drop table #temp3
drop table #result
drop table #final

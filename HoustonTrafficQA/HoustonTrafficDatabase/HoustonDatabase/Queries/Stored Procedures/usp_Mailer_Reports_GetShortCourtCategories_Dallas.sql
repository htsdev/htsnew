ALTER   PROCEDURE  [dbo].[usp_Mailer_Reports_GetShortCourtCategories_Dallas]        
        
AS        
        
Select  url,  
 courtcategorynum,    
 CourtCategoryName   
From  tblmailerreports        
where  isActive = 1  
and courtcategorynum in (6,11,21,25)
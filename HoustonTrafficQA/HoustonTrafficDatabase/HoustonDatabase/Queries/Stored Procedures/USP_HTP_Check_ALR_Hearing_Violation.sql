﻿/******   
Create by:  Syed Muhammad Ozair  
Business Logic : this procedure is used to check for ALR Hearing Violation for any ALR court in the given case excluding disposed and no hire violations.   
  
List of Parameters:   
 @Ticketid : Ticket Id of the case  
  
List of Columns:    
 isfound : if found then 1 else 0  
  
******/  

-- dbo.USP_HTP_Check_ALR_Hearing_Violation 198884,1
ALTER Procedure dbo.USP_HTP_Check_ALR_Hearing_Violation
@Ticketid int,
@Includedispose int = 0
as
SET NOCOUNT ON;
declare @count int
  --Nasir 5310 12/23/2008 Adding courts that are ALR Process flag on
select @count=count(tv.ticketid_pk) from tblticketsviolations tv WITH(NOLOCK)
where tv.violationnumber_pk=16159 -- ALR Hearing Violation
--and tv.courtid in (3047,3048,3070,3079) -- ALR courts ( Houston, fort bend, conroe)
and tv.courtid in (SELECT Courtid FROM tblCourts WITH(NOLOCK) WHERE ALRProcess = 1 ) -- ALR courts ( Houston, fort bend, conroe)
-- Noufil 5819 05/14/2009 clause updated with new parameter
and ((@Includedispose=0 and tv.courtviolationstatusidmain not in (80,236)) or (@Includedispose=1 )) -- excluding disposed and no hire cases
and tv.ticketid_pk=@Ticketid  
  
if @count>0  
 select 1 as isfound  
else  
 select 0 as isfound  
  



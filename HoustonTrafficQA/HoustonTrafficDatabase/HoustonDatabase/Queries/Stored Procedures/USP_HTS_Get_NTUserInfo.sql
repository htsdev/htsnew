SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_NTUserInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_NTUserInfo]
GO



CREATE procedure [dbo].[USP_HTS_Get_NTUserInfo]               
 (              
 @NTUserID varchar(20)      
 )              
              
              
as              
              
select employeeid as EmpId,              
ltrim(rtrim( UserName)) as UserName,              
 [Password],               
 LastName,              
 FirstName,              
 Abbreviation,              
 AccessType  ,            
 isnull(canupdatecloseoutlog,0) as canupdatecloseoutlog,          
 isnull(flagCanSPNSearch,0) as flagCanSPNSearch,          
 isnull(SPNUserName,'') as SPNUserName,          
 isnull(SPNPassword,'') as SPNPassword,    
 isnull(NTUserID,'') as  NTUserID     
from  tblusers              
where NTUserID = @NTUserID        
and status = 1        



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


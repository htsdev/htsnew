USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_UpdateFaxLetter]    Script Date: 11/15/2011 19:14:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************
 * Modified By		: Muhammad Nasir	
 * Created Date		: 07/10/2009
 * Task Id			: 6013
 * 
 * Business Logic	: update status in faxletter return from fax maker
 *						and update case status to waiting count if court HCJP 4-1 5-2 with status pretrial and 
 *						pasadena court with status judge and  Court date is in future with more than two business days 
 * 
 * Parameter List	:
 *						@FaxID			: FaxID 
 *						@FaxStatus		: FaxStatus
 ********************************************************************************/


CREATE PROCEDURE [dbo].[USP_HTS_UpdateFaxLetter]
	@FaxId INT,
	@FaxStatus VARCHAR(20)
AS
BEGIN
    -- Declare local use varaible  
    DECLARE @dbname VARCHAR(20)  
    DECLARE @StatusId INT  
    DECLARE @Query VARCHAR(MAX)  
    DECLARE @ticketId INT  
    DECLARE @Count INT 
    
    -- Getting Local values   
    SELECT @dbname = DbName,
           @StatusId = PostFaxStatusId
    FROM   SYSTEM
    WHERE  systemId = (
               SELECT SystemId
               FROM   faxletter
               WHERE  FaxLetterId = @FaxId
           )
    
    SELECT @ticketId = ticketid
    FROM   faxletter
    WHERE  FaxLetterId = @FaxId 
    
    
    -- Update Fax Status  
    UPDATE faxletter
    SET    STATUS = @FaxStatus
    WHERE  FaxLetterid = @FaxId 
    
    
    --Update ticket Status  
    IF @dbname = 'TrafficTickets'
    BEGIN
        IF @FaxStatus = 'SUCCESS'
        BEGIN
            --Nasir 6013 07/10/2009 Get violation count if court HCJP 4-1 5-2 with status pretrial and pasadena court with status judge and  Court date is in future with more than two business days 
            SELECT @Count = COUNT(*)
            --Ozair 6285 08/05/2009 db name issue resolved
            FROM   TrafficTickets.dbo.tblTicketsViolations ttv
                   INNER JOIN TrafficTickets.dbo.tblCourtViolationStatus tcvs
                        ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
            WHERE  ttv.TicketID_PK = @TicketID
                   AND DATEDIFF(
                           DAY,
                           TrafficTickets.dbo.fn_getnextbusinessday(GETDATE(), 2),
                           ttv.CourtDateMain
                       ) > 0
                   AND (
                           (ttv.CourtID IN (3013, 3016) AND tcvs.CategoryID = 3)
                           OR (ttv.CourtID = 3004 AND tcvs.CategoryID = 5)
                       )
            
            IF @Count <> 0
            BEGIN
                EXEC TrafficTickets.dbo.usp_hts_update_violationstatus @ticketid,
                     3992
            END
            ELSE
            BEGIN
                --Nasir 6013 07/08/2009 set status waiting if status is appearence	
                IF EXISTS (
                       --Nasir 6013 07/08/2009 set status waiting if status is appearence	
                       SELECT *
                       FROM   TrafficTickets.dbo.tblTicketsViolations ttv
                              INNER JOIN TrafficTickets.dbo.tblCourtViolationStatus 
                                   tcvs
                                   ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
                       WHERE  tcvs.CategoryID = 2
                              AND ttv.CourtViolationStatusIDmain NOT IN (80, 236)
                              AND ttv.TicketID_PK = @ticketid
                   )
                BEGIN
                    EXEC TrafficTickets.dbo.usp_hts_update_violationstatus @ticketid,
                         3992
                END
            END
            -- Muneer 8227 09/30/2010 added the new functionality for the LOR Dates
            IF EXISTS (SELECT COUNT(*) FROM TrafficTickets.dbo.tblTicketsViolations WHERE TicketID_PK = @ticketId AND CourtViolationStatusIDmain= 104 AND isnull(UnderlyingBondFlag,0) = 0 AND CourtID = 3004)
            BEGIN
            	
            	
            	EXEC TrafficTickets.dbo.USP_HTP_Update_PMCMaxOut @ticketId,@FaxId
            END
            
        END
    END
    ELSE
    BEGIN
        SET @Query = 'update ' + @dbname +
            '.dbo.tblticketsviolations set courtviolationstatusidmain =' +
            CONVERT(VARCHAR(20), @StatusId) + ' where ticketId_pk = ' + CONVERT(VARCHAR(20), @ticketId)
        
        EXECUTE (@Query)
    END
END
GO
GRANT EXECUTE ON [dbo].[USP_HTS_UpdateFaxLetter]  TO dbr_webuser
Go
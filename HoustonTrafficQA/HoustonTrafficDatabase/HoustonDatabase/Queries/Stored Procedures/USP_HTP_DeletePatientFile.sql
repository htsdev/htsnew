﻿ /************************************************************
 * Created By : Noufil Khan 
 * Created Date: 10/30/2010 5:14:20 PM
 * Business Logic : This procedure deletes file against the appropiate patient
 * Parameters :
 *				@Id : Primary Id of table 
 ************************************************************/

alter PROCEDURE [dbo].[USP_HTP_DeletePatientFile]
	@Id INT
AS
	DELETE 
	FROM   PatientsUploadedFile
	WHERE  Id = @id
GO

GRANT EXECUTE ON [dbo].[USP_HTP_DeletePatientFile] TO dbr_webuser
USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTS_Get_Payment_Info_By_MinDate]    Script Date: 07/17/2012 17:03:48 
* Created By : Rab Nawaz Khan
* Task ID : 10330 
* ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/**
* Business Logic : This method returns true if the payment date is greater then 07/17/2012 based on ticket id
**/

--	[dbo].[USP_HTS_Get_Payment_Info_By_MinDate]  184030
CREATE PROCEDURE [dbo].[USP_HTS_Get_Payment_Info_By_MinDate] 
	@TicketID INT
AS	

	SELECT ISNULL (MIN(p.RecDate), '01/01/1900') AS RecDate	
	INTO #temp
	FROM tblticketspayment p
	WHERE ISNULL (p.PaymentVoid, 0) <> 1	AND p.TicketID = @TicketID
	
	IF EXISTS(SELECT RecDate FROM #temp WHERE DATEDIFF(DAY, recdate, '01/01/1900') = 0) 
		BEGIN
			SELECT CAST(0 AS BIT) AS isPaymentFound
		END	
	
	ELSE
		BEGIN
			SELECT CAST(1 AS BIT) AS isPaymentFound
		END
DROP TABLE #temp
GO

GRANT EXEC ON dbo.USP_HTS_Get_Payment_Info_By_MinDate TO dbr_webuser
GO
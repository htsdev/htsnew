SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_remainderCalls]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_remainderCalls]
GO




CREATE procedure USP_HTS_Get_remainderCalls --'04/28/2006',2        
 (                
 @reminderdate  datetime,                              
-- @employeeid  int ,                               
 @status int=0                                 
 )                              
as                               
                           
                                 
SELECT DISTINCT                                   
                               
 t.TicketID_PK as ticketid,                               
 TV.TicketsViolationID as ticketViolationId,                                
 t.Firstname,                                
 t.Lastname,                                
 t.LanguageSpeak,                                
 t.BondFlag,                                
 tv.ReminderComments as comments,                                  
 tv.ReminderCallStatus as status,                                 
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''))+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' as contact1,                              
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''))+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' as contact2,                              
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''))+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' as contact3,                               
 T.TotalFeeCharged,                                
 f.FirmAbbreviation,                                   
 f.Phone,                                   
 f.Fax,                              
 c.Address + ' ' + c.City + ', TX  ' + c.Zip AS courtaddress                                  
into #temp4                                  
FROM  tblTicketsViolations TV                 
INNER JOIN        --added in order to get single ticketid                            
 dbo.tblTickets t                 
ON  TV.TicketID_PK = t.TicketID_PK                            
and  tv.TicketsViolationID=(                
  select top 1 tbv.TicketsViolationID from tblticketsviolations tbv                             
  where  tbv.ticketid_pk=t.ticketid_pk                            
  AND  (DATEDIFF([day], tbv.CourtDateMain, @reminderdate) = 0)                               
  )                              
inner join                            
 dbo.tblCourtViolationStatus cvs                 
ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID                 
INNER JOIN                                  
--   dbo.tblDateType dt ON cvs.CategoryID = dt.TypeID INNER JOIN                                  
 dbo.tblCourts c                 
ON  TV.CourtID = c.Courtid                 
LEFT OUTER JOIN                                  
 dbo.tblFirm f                 
ON t.FirmID = f.FirmID                 
LEFT OUTER JOIN                                  
 dbo.tblContactstype ct2                 
ON  t.ContactType2 = ct2.ContactType_PK                 
LEFT OUTER JOIN                                  
 dbo.tblContactstype ct1                 
ON  t.ContactType1 = ct1.ContactType_PK                 
LEFT OUTER JOIN                                  
 dbo.tblContactstype ct3                 
ON  t.ContactType3 = ct3.ContactType_PK                                  
WHERE (          
  TV.CourtID IN (3001, 3002, 3003)          
  AND  cvs.categoryid IN (4, 5, 9)                         
  AND  t.Activeflag = 1                               
  AND  DATEDIFF([day], TV.CourtDateMain, @reminderdate) = 0          
 )                               
 OR           
 (                                  
 TV.CourtID NOT IN (3001, 3002, 3003)                               
 AND cvs.categoryid IN (3, 4, 5, 9)                               
 AND  t.Activeflag = 1                               
 AND  DATEDIFF([day], TV.CourtDateMain, @reminderdate) = 0           
 )                                  
          
--AND  t.Activeflag = 1                               
--AND  DATEDIFF([day], TV.CourtDateMain, @reminderdate) = 0          
                       
  
                            
select distinct                                    
 ticketid,                 
 ticketViolationId,                                  
 firstname,                                  
 lastname,                                  
 languagespeak,                                  
 bondflag,                                
 comments,                                  
 status,                               
 Contact1,                       
 Contact2,                                 
 Contact3,                    
--T.CourtDateMain,                         
 totalfeecharged,                                     
-- courtnumber,                                  
 FirmAbbreviation,                                  
 phone,                                  
 fax,                              
-- trialdesc,                                  
 courtaddress,                                        
 case dbo.tblTicketsFlag.flagid                                    
  when 5 then 1  else 2  end as flagid                               
  into #temp1                                   
 from #temp4 T                                   
  left OUTER JOIN                                    
  dbo.tblTicketsFlag ON T.TicketID = dbo.tblTicketsFlag.TicketID_PK                                  
  and dbo.tblTicketsFlag.FlagID in (5)                                    
  --  order by lastname, firstname                          
                          
                         
                          
select                                   
 T.*,  owedamount = t.totalfeecharged -      
  (                          
  select isnull(sum(isnull(chargeamount,0)),0) from tblticketspayment p                          
  where p.ticketid = t.ticketid                          
  and   p.paymenttype not in (99,100)      
  and   p.paymentvoid<>1                                    
  )                         
                     
  into #temp2                                    
  from #temp1 T      
order by T.lastname, T.firstname                     
                  
--newly added to call function in order to get distinct violations                  
                  
select                   
T.*,dbo.fn_hts_get_TicketsViolations (ticketid,@reminderdate) as trialdesc                  
      
into #temp5                  
from #temp2 T                      
                  
                  
                                 
                                 
if @status  = 5                                   
begin                                    
 select * from #temp5                                           
end                                    
else                                    
begin                                    
 select * from #temp5 where status = @status                                    
end                                    
                                  
drop table #temp1                                    
drop table #temp4                                 
drop table #temp2                               
drop table #temp5                              
                
                
              
            
          
          
        
      
    
  






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


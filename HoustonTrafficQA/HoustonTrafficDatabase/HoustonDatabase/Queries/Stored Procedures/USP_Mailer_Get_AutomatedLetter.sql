SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_AutomatedLetter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_AutomatedLetter]
GO






CREATE PROCEDURE [dbo].[USP_Mailer_Get_AutomatedLetter] 
AS
BEGIN
	
	Select LetterID_PK,LetterName,CourtCategoryName,printerName,courtCategory from [tblLetter] inner join [tblCourtCategories] on  [tblLetter].[courtcategory] = [tblCourtCategories].[CourtCategorynum] where isAutomated = 1 and [tblLetter].[isactive]  =1 ORDER BY [courtcategory] , [sortorder] 
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


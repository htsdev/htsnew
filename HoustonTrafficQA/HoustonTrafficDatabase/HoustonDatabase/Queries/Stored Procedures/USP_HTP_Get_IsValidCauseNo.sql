
--USP_HTP_Get_IsValidCauseNo 8445

create PROCEDURE [dbo].[USP_HTP_Get_IsValidCauseNo]
@TicketId int	
AS
declare @count int

    
select @count=count(*) from tblticketsviolations where ticketid_pk=@TicketId     
and     
(courtid between 3007 and 3022 )    
and    
(     lower(substring(casenumassignedbycourt,1,2))<>'tr'    
 and lower(substring(casenumassignedbycourt,1,2))<>'bc'     
 and lower(substring(casenumassignedbycourt,1,2))<>'cr'     
 or   isnull(casenumassignedbycourt,'')  = ''
)  
   




if(@count>0)
	select 0 
else
	select 1 



go

GRANT EXECUTE ON [dbo].[USP_HTP_Get_IsValidCauseNo] TO [dbr_webuser]


go

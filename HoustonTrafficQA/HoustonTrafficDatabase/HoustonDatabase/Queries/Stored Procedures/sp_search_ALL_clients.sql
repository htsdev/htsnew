﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[sp_search_ALL_clients]    Script Date: 11/23/2011 18:13:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[sp_search_ALL_clients] as
SET NOCOUNT ON
DECLARE @ticketnumber varchar(12),
@violationNumber int, --Haris 9709 09/29/2011 datatype changed to int from tinyint
@tempticketnumber varchar(12),
@tempviolationNumber varchar(50),
@firstname varchar(20),
@lastname varchar(20),
@midnum varchar(20),
@fineamount money,
@dateset datetime,
@courtnum smallint,
@category varchar(10),
@sumfineamount money
create TABLE #temp (
ticketnumber varchar(12),
violationNumber varchar(50), 
firstname varchar(20),
lastname varchar(20),
midnum varchar(20),
fineamount money,
dateset datetime,
courtnum smallint,
category varchar(10)
)
DECLARE tickets_cursor CURSOR FOR 
--select ticketnumber_pk,violationNumber_pk from ticketsviolations
select distinct firstname,lastname,midnum,t.ticketnumber_pk,v.violationNumber_pk,fineamount,dateset_pk,courtnum,
category = 
case datetype 
when 1 then 'ARRAIGN'
when 2 then 'TRIAL'
else 'UNKNOWN'
end
from tickets t,ticketsviolations v,ticketstrialarr A
where t.ticketnumber_pk = v.ticketnumber_pk
and t.ticketnumber_pk = A.ticketnumber_pk
order by t.ticketnumber_pk
--and t.ticketnumber_pk = '0'
OPEN tickets_cursor
  FETCH NEXT FROM tickets_cursor INTO 
	@firstname,@lastname,@midnum,@ticketnumber,@violationNumber,@fineamount,@dateset,@courtnum,@category
IF @@FETCH_STATUS = 0 
     -- PRINT 'No Records Found'     
begin
set @tempticketnumber = @ticketnumber
set @sumfineamount = 0
WHILE @@FETCH_STATUS = 0
BEGIN
	if (@tempticketnumber) = @ticketnumber
		begin	
			set @tempviolationNumber = isnull(@tempviolationNumber,' ') + ' ' + convert(varchar(5),@violationNumber)
			set @sumfineamount = @sumfineamount + @fineamount 
		end
	else
		begin
			print rtrim(@tempticketnumber) + ' ' + @tempviolationNumber
			insert into #temp(ticketnumber,violationNumber , firstname ,lastname ,midnum ,
				fineamount ,dateset ,courtnum ,category)
				 values(rtrim(@tempticketnumber),@tempviolationNumber,@firstname,@lastname,@midnum,
				@sumfineamount,@dateset,@courtnum,@category)
			set @tempviolationNumber = ' '
			set @sumfineamount = 0
			set @tempviolationNumber = isnull(@tempviolationNumber,' ') + ' ' + convert(varchar(5),@violationNumber)
			set @sumfineamount = @sumfineamount + @fineamount 
		end
	set @tempticketnumber = @ticketnumber
	FETCH NEXT FROM tickets_cursor INTO 
	@firstname,@lastname,@midnum,@ticketnumber,@violationNumber,@fineamount,@dateset,@courtnum,@category
END 
insert into #temp(ticketnumber,violationNumber , firstname ,lastname ,midnum ,
				fineamount ,dateset ,courtnum ,category)
				 values(rtrim(@tempticketnumber),@tempviolationNumber,@firstname,@lastname,@midnum,
				@sumfineamount,@dateset,@courtnum,@category)
end 
CLOSE tickets_cursor
DEALLOCATE tickets_cursor
select * from #temp
drop table #temp 
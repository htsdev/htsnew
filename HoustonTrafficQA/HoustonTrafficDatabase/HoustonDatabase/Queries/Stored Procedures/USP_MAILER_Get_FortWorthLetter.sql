/****** 
Created by:		Noufil Khan 5591 02/27/2009
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Fort Worth Appearance  letter for the selected date range.
				
List of Parameters:
	@startdate:		Starting list date for Arraignment letter.
	@endDAte:		Ending list date for Arraignment letter.
	@CourtId:		Court id of the selected court category if printing for individual court hosue.
	@BondFlag:		Flag that identifies if searching for warrant letters or for appearance.
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category


List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--  USP_MAILER_Get_FortWorthLetter '2/20/09', '2/28/09', 3059, 0, 5

ALTER PROCEDURE [dbo].[USP_MAILER_Get_FortWorthLetter]
(
    @startdate  DATETIME,
    @enddate    DATETIME,
    @courtid    INT = 3059,
    @bondflag   TINYINT = 0,
    @catnum     INT = 5
)
AS
	-- DECLARING LOCAL VARIABLES FOR FILTERS...
	DECLARE @officernum VARCHAR(50),
	        @officeropr VARCHAR(50),
	        @tikcetnumberopr VARCHAR(50),
	        @ticket_no VARCHAR(50),
	        @zipcode VARCHAR(50),
	        @zipcodeopr VARCHAR(50),
	        @fineamount MONEY,
	        @fineamountOpr VARCHAR(50),
	        @fineamountRelop VARCHAR(50),
	        @singleviolation MONEY,
	        @singlevoilationOpr VARCHAR(50),
	        @singleviolationrelop VARCHAR(50),
	        @doubleviolation MONEY,
	        @doublevoilationOpr VARCHAR(50),
	        @doubleviolationrelop VARCHAR(50),
	        @count_Letters INT,
	        @violadesc VARCHAR(500),
	        @violaop VARCHAR(50),
	        @sqlquery VARCHAR(MAX),
	        @lettertype INT                                                
	
	
	SET @sqlquery = ''      
	SET @lettertype = 63 
	
	-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
	-- IF ASSOCIACTED WITH THE LETTER.......
	SELECT @officernum = officernum,
	       @officeropr = oficernumOpr,
	       @tikcetnumberopr = tikcetnumberopr,
	       @ticket_no = ticket_no,
	       @zipcode = zipcode,
	       @zipcodeopr = zipcodeLikeopr,
	       @singleviolation = singleviolation,
	       @singlevoilationOpr = singlevoilationOpr,
	       @singleviolationrelop = singleviolationrelop,
	       @doubleviolation = doubleviolation,
	       @doublevoilationOpr = doubleviolationOpr,
	       @doubleviolationrelop = doubleviolationrelop,
	       @violadesc = violationdescription,
	       @violaop = violationdescriptionOpr,
	       @fineamount = fineamount,
	       @fineamountOpr = fineamountOpr,
	       @fineamountRelop = fineamountRelop
	FROM   tblmailer_letters_to_sendfilters
	WHERE  courtcategorynum = @catnum
	       AND lettertype = @lettertype 
	
	
	-- MAIN QUERY....
	-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
	-- FOR THE SELECTED DATE RANGE ONLY ARRAIGNMENT (PRIMARY STATUS) CASES
	-- INSERTING VALUES IN A TEMP TABLE....
	SET @sqlquery = @sqlquery + 
	    '                                        
        Select distinct tva.recordid, 
				convert(datetime, convert(varchar,ta.recloaddate,101))as mdate,                                      
				flag1 = isnull(ta.Flag1,''N'') ,                                                    
				isnull(ta.donotmailflag,0) as donotmailflag,                                                    	
				count(tva.ViolationNumber_PK) as ViolCount,                                                    
				isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount
		into #temp
		FROM dallastraffictickets.dbo.tblTicketsViolationsArchive TVA                     
		INNER JOIN dallastraffictickets.dbo.tblTicketsArchive TA ON TVA.recordid= TA.recordid

		-- PERSON NOT MOVED IN PAST 48 MONTHS (ACCUZIP)
		where isnull(ta.ncoa48flag,0) = 0

		-- PERSON NAME SHOULD NOT BE EMPTY....
		and ta.lastname is not null  and ta.firstname is not null

		-- Adil 7966 06/28/2010 EXCLUDE BOND CASES.
		-- EXCLUDE BOND CASES...
		and tva.violationbonddate is null 

		-- EXCLUDE V TICKETS....
		--and charindex(''v'',tva.ticketnumber_pk)<>1

		-- FOR THE SELECTED DATE RANGE ONLY.....
		and datediff(day, ta.recloaddate, ''' + CONVERT(VARCHAR, @startDate, 101) + ''')<=0
		and datediff(day, ta.recloaddate, ''' + CONVERT(VARCHAR, @endDate, 101) + ''')>=0
		and tva.violationstatusid = 116
		
		-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
		AND (SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''O'' AND SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''M'')
		'
	
	-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED COURT CATEGORY...    
	IF (@courtid = @CATNUM)
	    SET @sqlquery = @sqlquery + 
	        ' and tva.courtlocation In (Select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum ='+ CONVERT(VARCHAR(10), @catnum) + ')' 
	        
	-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
	ELSE
	    SET @sqlquery = @sqlquery + 'and tva.courtlocation=' + CONVERT(VARCHAR, @courtid)
	
	-- LMS FILTERS SECTION:
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                                     
	IF (@officernum <> '')
	    SET @sqlquery = @sqlquery + 'and  ta.officerNumber_Fk ' + @officeropr + '(' + @officernum + ')' 
	
	-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...                                                     
	IF (@ticket_no <> '')
	    SET @sqlquery = @sqlquery + 
	    ' and (' + dbo.Get_String_Concat_With_op('' + @ticket_no + '','tva.TicketNumber_PK',+ '' + @tikcetnumberopr) + ')' 
	
	-- ZIP CODE FILTER......
	-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                  
	IF (@zipcode <> '')
	    SET @sqlquery = @sqlquery + ' 
	and  (' + dbo.Get_String_Concat_With_op(
	            '' + @zipcode + '',
	            'left(ta.zipcode,2)',
	            + '' + @zipcodeopr
	        ) + ')'
	
	-- FINE AMOUNT FILTER.....
	-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
	IF (@fineamount <> 0 AND @fineamountOpr <> 'not')
	    SET @sqlquery = @sqlquery + '
		and tva.fineamount' + CONVERT(VARCHAR(10), @fineamountRelop) + 'Convert(Money,' + CONVERT(VARCHAR(10), @fineamount) + ')' 
	
	-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
	IF (@fineamount <> 0 AND @fineamountOpr = 'not')
	    SET @sqlquery = @sqlquery + '
		and not tva.fineamount' + CONVERT(VARCHAR(10), @fineamountRelop) + 'Convert(Money,' + CONVERT(VARCHAR(10), @fineamount) + ')' 
	
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	
	-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
	SET @sqlquery = @sqlquery + 
	    'group by  convert(datetime, convert(varchar, ta.recloaddate, 101)),
		 ta.flag1, ta.donotmailflag,tva.recordid
		 having 1=1' 
	
	-- SINGLE VIOLATION FINE AMOUNT FILTER....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
	IF (@singleviolation <> 0 AND @doubleviolation = 0)
	    SET @sqlquery = @sqlquery + ' and (  ' + @singlevoilationOpr + 
	        '  (isnull(sum(isnull(tva.fineamount,0)),0) ' + CONVERT(VARCHAR(10), @singleviolationrelop) 
	        + 'Convert(Money,' + CONVERT(VARCHAR(10), @singleviolation) + ') and count(tva.ViolationNumber_PK)=1))' 
	
	-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF (@doubleviolation <> 0 AND @singleviolation = 0)
	    SET @sqlquery = @sqlquery + '
			and (' + @doublevoilationOpr + 
	        '   (isnull(sum(isnull(tva.fineamount,0)),0) ' + CONVERT(VARCHAR(10), @doubleviolationrelop) 
	        + 'convert (Money,' + CONVERT(VARCHAR(10), @doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )' 
	
	-- SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
	-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS
	-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
	IF (@doubleviolation <> 0 AND @singleviolation <> 0)
	    SET @sqlquery = @sqlquery + '
	and (' + @singlevoilationOpr + '  (isnull(sum(isnull(tva.fineamount,0)),0) '
	        + CONVERT(VARCHAR(10), @singleviolationrelop) + 'Convert(Money,' + CONVERT(VARCHAR(10), @singleviolation) + ') and count(tva.ViolationNumber_PK)=1))                                
    and (' + @doublevoilationOpr + '  (isnull(sum(isnull(tva.fineamount,0)),0) '
	        + CONVERT(VARCHAR(10), @doubleviolationrelop) + 'convert (Money,' + CONVERT(VARCHAR(10), @doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))' 
	
	-- GETTING THE RESULT SET IN TEMP TABLE.....                                                    
	SET @sqlquery = @sqlquery +'
    -- Noufil Khan 8199 08/23/2010
	-- EXCLUDE APPEARANCE LETTERS THAT HAVE BEEN ALREADY PRINTED......
	-- OR ANY OTHER FORTHWORT LETTER EXCLUDING APPEARANCE LETTER PRINTED IN PAST 5 DAYS FOR THE SAME RECORD.. 
	delete from #temp 
	from #temp a inner join tblletternotes n on n.recordid = a.recordid
	where (	
			-- day of letter already printed...
			(n.lettertype = 63) OR
			-- ANY OTHER FORTHWORT LETTER EXCLUDING APPEARANCE LETTER printed in past 5 business days. 
			(n.Lettertype in (SELECT tl.LetterID_PK  FROM tblLetter tl WHERE tl.courtcategory = 27 AND tl.LetterID_PK <> 63) 
			and datediff(day, n.recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0))

	Select distinct a.recordid, a.ViolCount, a.Total_Fineamount,  a.mDate , a.Flag1, a.donotmailflag 
	into #temp1  from #temp a, dallastraffictickets.dbo.tblTicketsViolationsArchive tva 
	where a.recordid=  tva.recordid and tva.violationbonddate is null -- Adil 7966 06/28/2010 EXCLUDE BOND CASES. 
	-- Rab Nawaz Khan 10516 10/25/2012 Exclude the ordinance cases and juvenile cases . . . 
	AND (SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''O'' AND SUBSTRING(ISNULL(tva.CauseNumber, ''TaskO_10516''), 5, 1) <> ''M'') ' 


 set @sqlquery=@sqlquery+                       
' 
	' 
 		
	-- VIOLATION DESCRIPTION FILTER.....
	-- EXCLUDE THE LETTER IF SPECIFIED VIOLATION IS THE ONLY VIOLATION IN THE LETTER....
	IF (@violadesc <> '')
	BEGIN
	    -- NOT LIKE FILTER.......
	    IF (CHARINDEX('not', @violaop) <> 0)
	        SET @sqlquery = @sqlquery + 
	        'and	 not ( a.violcount =1  and (' + dbo.Get_String_Concat_With_op('' + @violadesc + '',
	                '  tva.violationdescription  ',+ '' + 'like') + '))' 
	    
	    -- INCLUDE ONLY THOSE LETTER THAT SINGLE VIOLATION AND MATCHES THE SPECIFIED VIOLATION DESCRIPTION...
	    -- LIKE FILTER.......
	    IF (CHARINDEX('not', @violaop) = 0)
	        SET @sqlquery = @sqlquery + ' 
		and	 ( a.violcount =1  and (' + dbo.Get_String_Concat_With_op(
	                '' + @violadesc + '',
	                '  tva.violationdescription  ',
	                + '' + 'like'
	            ) + '))'
	END
	
	
	-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
	-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
	SET @sqlquery = @sqlquery + 
	    ' 

-- Sabir Khan 7099 12/07/2009 exclude client cases
	delete from #temp1 where recordid in (
	select v.recordid from dallastraffictickets.dbo.tblticketsviolations v 
	inner join dallastraffictickets.dbo.tbltickets t on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
	inner join #temp1 a on a.recordid = v.recordid)
						
Select count(distinct recordid) as Total_Count, mDate                                                  
into #temp2                                                
from #temp1                                                
group by mDate                                                     

-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mDate                                                                                              
into #temp3                                                
from #temp1                                                    
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag =  0                                                 
group by mDate 
                                                    
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mDate                                                  
into #temp4                                                    
from #temp1                                                
where Flag1  not in (''Y'',''D'',''S'')                                                    
and donotmailflag = 0                                                 
group by mDate 
                 
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mDate                                                 
 into #temp5                                                    
from #temp1        
where donotmailflag = 1  
--and flag1  in  (''Y'',''D'',''S'')                                                    
group by mDate 
       
-- OUTPUTTING THE REQURIED INFORMATION........                                                    
Select   listdate = #temp2.mDate ,
        Total_Count,
        isnull(Good_Count,0) Good_Count,
        isnull(Bad_Count,0)Bad_Count,
        isnull(DonotMail_Count,0)DonotMail_Count
from #Temp2 left outer join #temp3 
on #temp2.mDate = #temp3.mDate
                                                   
left outer join #Temp4
on #temp2.mDate = #Temp4.mDate
  
left outer join #Temp5
on #temp2.mDate = #Temp5.mDate                                                 
  
where Good_Count > 0              
order by #temp2.mDate desc                                                     
          
-- DROPPING THE TEMPORARY TABLES USED ABOVE.....                                              
drop table #temp                                                
drop table #temp1                                                
drop table #temp2                                                    
drop table #temp3                                                
drop table #temp4
drop table #temp5
'
	
	--print @sqlquery
	
	-- EXECUTING THE DYNAMIC SQL QUERY...
	EXEC (@sqlquery)



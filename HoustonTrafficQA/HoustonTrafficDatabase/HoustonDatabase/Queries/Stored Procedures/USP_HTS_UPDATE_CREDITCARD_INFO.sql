
/*******

Alter By: Noufil Khan 3961 07/16/2008 

Business Logic: This procedure is used to update credit card transacition detail
 
Parameter : 
			@responsereasoncode : Save Response Reason Code
 
*******/

ALTER procedure [dbo].[USP_HTS_UPDATE_CREDITCARD_INFO] --111965,72322,null,'','254789','1','2','declined','P','m',0,0                               
                       
--declare                         
@ticketid int,                                  
@invnumber varchar(10),                                  
@status varchar(2),                                  
@errmsg varchar(255),                                  
@transnum varchar(50),                                  
@response_subcode  varchar(20),                                      
@response_reason_code   varchar(20),                                   
@response_reason_text varchar(100),                                   
@auth_code   varchar(6),                                             
@avs_code varchar(1),                                
@scheduleid int,  --if schedule payment is being paid by card                                
@processflag int,  --In order to track either payments are schedule or not                              
@PaymentStatusID int,
@responsecode int=0,   -- Noufil 3961 New parameter Response_Code added                       
@paymentIdentifier INT = 0 -- Afaq 8213 11/02/2010 New parameter added.
                          
as                              
SET NOCOUNT ON;
/*                          
select @ticketid=124165,                        
@invnumber=N'6673',                        
@status=N'0',                        
@errmsg=N'',                        
@transnum=N'0',                        
@response_subcode=N'1',                        
@response_reason_code=N'1',                        
@response_reason_text=N'(TESTMODE) This transaction has been approved.',                        
@auth_code=N'000000',                        
@avs_code=N'P',                        
@scheduleid=0,                        
@processflag=1,                        
@PaymentStatusID=1                        
*/                        
                          
declare @approvedflag int                                  
declare @amount money                                  
declare @ccnum varchar(20)                                  
declare @expdate varchar(10)                                  
declare @name varchar(40)                                  
declare @employeeid int                                  
declare @cardtype int                                 
declare @paymentmethod as varchar(50)  ,                          
 @PAYMENTID INT, @ChargeAmount float                          
declare @owes as money                            
                            
                          
set @approvedflag = 0                                          
if @status = '0'                          
 begin                          
  set @approvedflag = 1                                  
 end                          
else                          
 begin                          
  set @status=null                           
 end                          
                          
--in all scenerios either successful or failed transaction                          
update tblticketscybercash set                                  
 status = @status,                                  
 transnum = @transnum,                                  
 errmsg = @errmsg,                                  
 approvedflag = @approvedflag,                                  
 response_subcode  = @response_subcode   ,                                   
 response_reason_code  = @response_reason_code ,                                     
 response_reason_text  = @response_reason_text  ,                                  
 auth_code       = @auth_code ,                                   
 avs_code = @avs_code,
 Response_Code=  @responsecode                         
 where ROWID = @invnumber                                             
                            
   --successfull transaction                                  
--inserting record in tblticketspayment                                  
if @approvedflag = 1                                   
  begin                                  
  --getting payments info by invoicenumber                                
   select @amount=amount,@ccnum=ccnum,@expdate=expdate,@name=name,@employeeid=employeeid,@cardtype=cardtype from tblticketscybercash WITH(NOLOCK)                                  
--   where invnumber = @invnumber                                  
   where ROWID = @invnumber                              
                               
  --Insert successful payment 
  --Afaq 8213 11/02/2010 New parameter @paymentIdentifier added in the insert statement.                                 
   insert into tblticketspayment             
   (PaymentType,Ticketid,RefNumber,CardNumber,CardExpdate,                                  
   Name,ChargeAmount,EmployeeID,duedate,cardtype,paymentIdentifier)                                  
  values(5,@ticketid,@transnum,@ccnum,@expdate,@Name,@amount,                                  
   @EmployeeID,getdate(),@cardtype,@paymentIdentifier)                                  
                          
  set @paymentid = scope_identity()                          
                           
 update tblticketscybercash set invnumber = @paymentid                          
 where rowid = @invnumber                           
       
 ----- Added By Zeeshan Ahmed For Payment Tracking ------      
 update tblticketscybercashlog set invnumber = @paymentid , reftransnum = ''                         
 where reftransnum = @invnumber                           
 --------------------------------------------------------                          
      
update  tbltickets set activeflag =1 where ticketid_pk = @ticketid                                      
--                                   
 declare @amt money --cursor variable                                        
 declare @diff as money                                         
 declare @id as int                                        
                                    
 --In order to set schedule flag=0 of schedule amount with min payment date fall under Charge Amount                                         
                        
--print 'cursur will start now '                        
                        
set @ChargeAmount = @amount                        
                        
 DECLARE abc CURSOR FOR                                            
 SELECT amount,scheduleid FROM tblschedulepayment WITH(NOLOCK)                                        
  where ticketid_pk=@Ticketid and scheduleflag=1                                        
   order by paymentdate                                        
                        
 OPEN abc                                        
                        
 FETCH NEXT FROM abc                                        
 into @amt,@id                                    
                                     
--select ' yah while k bahir hai == @ChargeAmount  ' + isnull(converT(varchar(10), @ChargeAmount ),0) + ' @amt ' + isnull(convert(varchar(10),@amt ),0)                          
                        
 WHILE (@@FETCH_STATUS = 0)                                        
 begin                                           
  --print ' main while k andar aa gaya hu @ChargeAmount  ' +converT(varchar(10), @ChargeAmount ) + ' @amt ' + convert(varchar(10),@amt )                          
  if (@ChargeAmount>=@amt) --Till ChargeAmount is not < 0                                        
   begin                          
    update tblschedulepayment                                        
     set scheduleflag=0                                        
     where scheduleid=@id                                                   
                            
    set @ChargeAmount=@ChargeAmount-@amt    --setting new charge amt                                        
   end                             
                          
                          
                        
  else if(@amt>=@ChargeAmount and  @ChargeAmount>0)                          
   begin                               
  -- print ' else if main aa gaya hu ' +converT(varchar(10), @ChargeAmount ) + ' @amt ' + convert(varchar(10),@amt )                             
    update tblschedulepayment                                       
     set     amount   =   abs(@ChargeAmount-@amt)                
    where scheduleid=@id                          
                                 
  set @ChargeAmount=0                          
  end                                        
                          
  FETCH NEXT FROM abc                                        
   into @amt,@id                                         
 end                                         
 close abc                                        
 DEALLOCATE abc                          
                          
                          
 declare @ScheduleDate as datetime                          
 set @ScheduleDate=getdate()+15                          
                           
 SELECT  @owes= ISNULL(SUM(DISTINCT t.TotalFeeCharged) - ISNULL(SUM(tp.ChargeAmount), 0), 0) - ISNULL                          
  ((SELECT     SUM(Amount) FROM         dbo.tblSchedulePayment AS sp WITH(NOLOCK)                          
       WHERE     (TicketID_PK = t.TicketID_PK) AND (ScheduleFlag = 1)), 0)                           
 FROM         dbo.tblTickets AS t WITH(NOLOCK) LEFT OUTER JOIN                          
   dbo.tblTicketsPayment AS tp WITH(NOLOCK) ON t.TicketID_PK = tp.TicketID AND tp.PaymentVoid <> 1 AND tp.PaymentType <> 99                          
 GROUP BY t.TicketID_PK                          
 HAVING      (t.TicketID_PK = @Ticketid)                          
                           
                           
                          
 if  @owes > 0                          
  begin                               
   --print @owes                          
   exec USP_HTS_Insert_Edit_SchedulePayment @Ticketid,@owes,@ScheduleDate,@EmployeeID,0                                
  end                          
                          
 -- update tbltickets set paymentstatus=@PaymentStatusID where ticketid_pk = @Ticketid  Agha Usman Task Id 2664 06/18/2008                             
                          
end   
  
  


USE [TrafficTickets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
* Created By: Sabir Khan
* Task Id: 10920
* Date: 05/27/2013
* This stored procedure is used to get all company name
*/

CREATE Procedure [dbo].[USP_HTP_Get_BranchesName]
as
select * from Branch order BY branchid

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_BranchesName] TO dbr_webuser

GO



-- usp_Mailer_Send_HMC_DayOFArraignment 1, 48, 1, '3/8/2008,' , 3992, 0, 1, 0, 1
      
ALTER proc [dbo].[usp_Mailer_Send_HMC_DayOFArraignment] 
@catnum int=1,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0 ,
@searchtype int ,
@isprinted bit, 
@language int = 0                                
                                    
as                                    
                                          
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50)                                     
                                    
declare @sqlquery varchar(8000),
	@sqlquery2 varchar(8000) 
        
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  

Select @sqlquery =''  , @sqlquery2 = ''


declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                    
set @sqlquery=@sqlquery+'                                    
Select	distinct ta.recordid,' 
if @searchtype = 0
	set @sqlquery = @sqlquery + '
	convert(varchar(10),ta.listdate,101) as listdate, '
else
	set @sqlquery = @sqlquery + '
	convert(varchar(10),tva.courtdate,101) as listdate, '

set @sqlquery = @sqlquery + '
count(tva.ViolationNumber_PK) as ViolCount,
isnull(sum(isnull(tva.fineamount,0)),0) as Total_Fineamount 
into #temp2                                          
FROM    dbo.tblTicketsViolationsArchive tva 
INNER JOIN
	dbo.tblTicketsArchive ta 
ON 	tva.RecordID = ta.RecordID 

INNER JOIN
	dbo.tblCourtViolationStatus tcvs 
ON 	tva.violationstatusid = tcvs.CourtViolationStatusID 
where  	tcvs.CategoryID=2
and 	Flag1 in (''Y'',''D'',''S'')
and 	donotmailflag = 0
and 	ta.clientflag=0
and isnull(ta.ncoa48flag,0) = 0
'               
          
if(@crtid=1 )              
	set @sqlquery=@sqlquery+' 
and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = 1 )'                  


if @crtid <> 1              
	set @sqlquery =@sqlquery +'                                  
and 	taa.courtlocation =  ' + convert(varchar(10),@crtid)                                 
                                  
if @searchtype = 0
begin
	if(@startListdate<>'')                                    
		set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'ta.listdate' ) 
end
else
begin
	if(@startListdate<>'')                                    
		set @sqlquery=@sqlquery+'
and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.courtdate' ) 
end

if(@officernum<>'')                                    
begin                                    
set @sqlquery =@sqlquery +                                     
            '  
and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
end                                    
                                    
if(@ticket_no<>'')                                    
set @sqlquery=@sqlquery+ '
and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                                    
if(@zipcode<>'')                                                          
set @sqlquery=@sqlquery+ ' 
and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                  if(@fineamount<>0 and @fineamountOpr<>'not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end    

if(@fineamount<>0 and @fineamountOpr='not'  )              
begin              
set @sqlquery =@sqlquery+ '     '+                                    
      '  and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   
end    
set @sqlquery =@sqlquery+ ' 
group by  
	ta.recordid, '

if @searchtype = 0
	set @sqlquery = @sqlquery + '
	convert(varchar(10),ta.listdate,101) having 1=1  '
else
	set @sqlquery = @sqlquery + '
	convert(varchar(10),tva.courtdate,101) having 1=1   '

                            
if(@singleviolation<>0 and @doubleviolation=0)                                
	set @sqlquery =@sqlquery+ '
	and (  '+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
      '                                
if(@doubleviolation<>0 and @singleviolation=0 )                                
	set @sqlquery =@sqlquery + '
	and ('+ @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and count(tva.ViolationNumber_PK)=2) )
      '                                                    
                                
if(@doubleviolation<>0 and @singleviolation<>0)                                
	set @sqlquery =@sqlquery+ '
	and ('+ @singlevoilationOpr+  '  (isnull(sum(isnull(tva.fineamount,0)),0) '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and count(tva.ViolationNumber_PK)=1))
    and ('+  @doublevoilationOpr+  '   (isnull(sum(isnull(tva.fineamount,0)),0) '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ') and count(tva.ViolationNumber_PK)=2))
        '                                                                   

set @sqlquery=@sqlquery+'                                    
delete from #temp2 where recordid  in (                                                                    
	select	n.recordid from tblletternotes n inner join tblticketsarchive t on t.recordid  = n.recordid
	where 	lettertype ='+convert(varchar(10),@lettertype)+'  
	) '    


set @sqlquery=@sqlquery+'                                    

Select	distinct ta.recordid, 
	a.listdate,
	ta.courtdate,
	ta.courtid, 
	flag1 = isnull(ta.Flag1,''N'') ,
	ta.officerNumber_Fk,                                          
	tva.TicketNumber_PK,
	ta.donotmailflag,
	ta.clientflag,
	upper(firstname) as firstname,
	upper(lastname) as lastname,
	upper(address1) + '''' + isnull(ta.address2,'''') as address,                      
	upper(ta.city) as city,
	S.state as state,
	c.CourtName,
	zipcode,
	midnumber,
	dp2 as dptwo,
	dpc,                
	violationdate,
	violationstatusid,
	left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,
	ViolationDescription, 
	case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,                                
	a.ViolCount,
	a.Total_Fineamount 
into #temp                                          
from tblticketsarchive ta, tblticketsviolationsarchive tva, #temp2 a, tblcourts c, tblstate s,	tblCourtViolationStatus tcvs
where ta.recordid = tva.recordid
and ta.recordid = a.recordid
and ta.courtid = c.courtid
and ta.stateid_fk = S.stateID

- Abbas Qamar 9991 10-01-2012 added category Id
and tva.violationstatusid = tcvs.CourtViolationStatusID 
where  	tcvs.CategoryID=2



'

-- VIOLATION DESCRIPTION FILTER.....
if(@violadesc <> '')
begin
-- NOT LIKE FILTER.......
if(charindex('not',@violaop)<> 0 )              
	set @sqlquery=@sqlquery+' 
	and	 not ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           

-- LIKE FILTER.......
if(charindex('not',@violaop) =  0 )              
	set @sqlquery=@sqlquery+' 
	and	 ( a.violcount =1  and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ 'like')+'))'           
end

                                         
set @sqlquery=@sqlquery+'

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                              
 city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
 courtid,zipmid,donotmailflag,clientflag, zipcode into #temp1  from #temp                                    
 '              
              
         
if( @printtype<>0) 
begin        
set @sqlquery2 =@sqlquery2 +                                    
'
	Declare @ListdateVal DateTime,                                  
		@totalrecs int,                                
		@count int,                                
		@p_EachLetter money,                              
		@recordid varchar(50),                              
		@zipcode   varchar(12),                              
		@maxbatch int  ,
		@lCourtId int  ,
		@dptwo varchar(10),
		@dpc varchar(10)

	declare @tempBatchIDs table (batchid int)
	Select @totalrecs =count(*) from #temp1 where mdate = @ListdateVal                                 
	Select @count=Count(*) from #temp1                                

	if @count>=500                                
		set @p_EachLetter=convert(money,0.3)                                
                                
	if @count<500                                
		set @p_EachLetter=convert(money,0.39)                                
                              
	Declare ListdateCur Cursor for                                  
	Select distinct mdate  from #temp1                                
	open ListdateCur                                   
	Fetch Next from ListdateCur into @ListdateVal                                                      
	while (@@Fetch_Status=0)                              
		begin                                
			Select @totalrecs =count(distinct recordid) from #temp1 where mdate = @ListdateVal                                 

			insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                  
			('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+', 0, @totalrecs, @p_EachLetter)                                   

			Select @maxbatch=Max(BatchId) from tblBatchLetter 
			insert into @tempBatchIDs select @maxbatch                                                     
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where mdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
				begin                              
					insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
					values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
					Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
				end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
			Fetch Next from ListdateCur into @ListdateVal                              
		end                                            

	close ListdateCur  
	deallocate ListdateCur 

	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb  , 0 as language, ''0'' as chkgroup , mdate as courtdate                                   
	 from #temp1 t , tblletternotes n, @tempBatchIDs tb
	 where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)

	if @language = 1 
	set @sqlquery2 = @sqlquery2 + '	
	 union
	 Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
	 t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc,                              
	 dptwo, TicketNumber_PK, t.zipcode, '''+@user+''' as Abb  , 1 as language, ''1'' as chkgroup , mdate as courtdate                                   
	 from #temp1 t , tblletternotes n, @tempBatchIDs tb
	 where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)
	

	set @sqlquery2 = @sqlquery2 + '	
	order by recordid

	declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
	select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
	select @subject = convert(varchar(20), @lettercount) +  '' LMS HMC Day Of ARR Letters Printed''
	select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
	exec usp_mailer_send_Email @subject, @body

	declare @batchIDs_filname varchar(500)
	set @batchIDs_filname = ''''
	select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
	select isnull(@batchIDs_filname,'''') as batchid

	'
end
else
begin
	set @sqlquery2 = @sqlquery2 + '
	 Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
	 LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              	 dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb , 0 as language, ''0'' as chkgroup , mdate as courtdate                                   
	 from #temp1 '

	if @language = 1 
		set @sqlquery2 = @sqlquery2 + '
		union
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
		LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc,                              		dptwo, TicketNumber_PK, zipcode, '''+@user+''' as Abb , 1 as language, ''1'' as chkgroup , mdate as courtdate                                   
		from #temp1 '

	set @sqlquery2 = @sqlquery2 + '
	 order by recordid 
 '

end
set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'
                                    
--print @sqlquery + @sqlquery2
set @sqlquery = @sqlquery + @sqlquery2
--exec sp_executesql @sqlquery, @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @empid, @printtype, @searchtype
exec (@sqlquery)








/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to get the event config settings by event id. 
				
List of Parameters:	
	@EventTypeID		
	
List of Columns:
	EventTypeID,
	EventTypeName,
	EventTypeDays,
	CourtID,
	SettingDateRange,
	EventState,
	DialTime
			
*******/

Create Procedure dbo.usp_AD_Get_EventConfigSettingsByID

@EventTypeID tinyint

as

select 
	EventTypeID,
	EventTypeName,
	EventTypeDays,
	CourtID,
	SettingDateRange,
	EventState,
	DialTime
from AutoDialerEventConfigSettings
where EventTypeID=@EventTypeID
go

grant exec on dbo.usp_AD_Get_EventConfigSettingsByID to dbr_webuser
go
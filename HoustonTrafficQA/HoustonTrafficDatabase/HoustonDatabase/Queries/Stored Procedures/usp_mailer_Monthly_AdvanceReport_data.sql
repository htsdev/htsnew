﻿/****** 
Created by:		Muhammad Ali	
Create Date:	09/23/2010

Business Logic:	The procedure is used by LMS application to get the returns on the selected mailer type.

				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS Reports
	@sDate:			Starting date for search criteria.
	@eDate:			Ending date for search criteria.
	@SearchType:	Flag that identifies if searching by upload date or by mailer date. 0= upload date, 1 = mailer date

List of Columns:	
	mdate:			Maier Date or upload date as selected in the search criteria.
	lettercount:	Number of letters printed
	nonclient:		Number of people not hired/contacted us
	client:			Number of people hired us.
	quote :			Number of people just contact us for information..
	

******/

-- usp_mailer_Monthly_AdvanceReport_data 1, 9, '7/1/2007', '8/15/2007', 1
CREATE procedure [dbo].[usp_mailer_Monthly_AdvanceReport_data]
(	
	@CatNum		int,                
	@lettertype 	int,                
	@sDate		datetime,                
	@eDate 		datetime,
	@SearchType	tinyint
)	

as

--DECLARE	@CatNum		int                
--DECLARE	@lettertype 	int              
--DECLARE	@sDate		datetime                
--DECLARE	@eDate 		datetime
--DECLARE	@SearchType	tinyint
--SET @CatNum=1
--SET @lettertype=9
--SET @sDate='7/1/2007'
--SET @eDate ='8/15/2007'
--SET @SearchType=1


set nocount on 

--Sets First day of the current month of start date @sDate
Select @sDate = dateadd(mm,datediff(mm,0,@sDate),0)
--Sets last day of the current month of end date @eDate
Select @eDate = dateadd(ms,- 3,dateadd(mm,0,dateadd(mm,datediff(mm,0,@eDate)+1,0))) 
	
declare @DBid int
-- 1 = Traffic Tickets
-- 2 = Dallas Traffic Tickets

select @dbid =	 CASE ISNULL(tl.courtcategory,0) WHEN 26 THEN 5 ELSE ISNULL(tcc.LocationId_FK ,0) END
from tblletter tl INNER JOIN tblCourtCategories tcc ON tl.courtcategory = tcc.CourtCategorynum where letterid_pk = @lettertype

SELECT @dbid= ISNULL(@dbid,0)

declare @tblcourts table (courtid int)

----------------------------------------------------------------------------------------------------
--		DALLAS DATABASE....
----------------------------------------------------------------------------------------------------
if @dbid = 2
	BEGIN
		if @catnum < 1000
			insert into @tblcourts 
			select courtid from dallastraffictickets.dbo.tblcourts where courtcategorynum = @catnum
		else
			insert into @tblcourts select @catnum
	END
----------------------------------------------------------------------------------------------------
--		TRAFFIC TICKETS DATABASE....
----------------------------------------------------------------------------------------------------
ELSE
    BEGIN
		if @catnum < 1000
			insert into @tblcourts 
			select courtid from dbo.tblcourts where courtcategorynum = @catnum
		else
			insert into @tblcourts select @catnum	
    END
	
----------------------------------------------------------------------------------------------------		


-- GETTING LETTER HISTORY INTO TEMP TABLE AS PER SELECTION CRITERIA.....
declare @letters table (
	noteid int, 
	mdate datetime
	)

if @searchtype = 0 
	BEGIN
		insert into @letters
		select noteid,  convert(varchar(10), listdate, 101)
		from tblletternotes n, @tblcourts c
		where n.courtid = c.courtid
		and n.lettertype = @lettertype
		and datediff(day, listdate, @sdate)<= 0
		and datediff(day, listdate, @edate)>= 0
	END
else
	BEGIN
		insert into @letters
		select noteid,  convert(varchar(10), recordloaddate, 101)
		from tblletternotes n, @tblcourts c
		where n.courtid = c.courtid
		and n.lettertype = @lettertype
		and datediff(day, recordloaddate, @sdate)<= 0
		and datediff(day, recordloaddate, @edate)>= 0
	END

-- GETTING INFORMATION FROM CLIENTS/QUOTES......
declare @QuoteClient table (
	ticketid int, 
	mailerid int,
	activeflag int,
	hiredate datetime,
	quotedate datetime
	)

if @dbid = 2
	insert into @QuoteClient
	select t.ticketid_pk, t.mailerid, t.activeflag, 
			min(p.recdate), t.recdate
	from dallastraffictickets.dbo.tbltickets t 
	left outer join  
		dallastraffictickets.dbo.tblticketspayment p 
	on	p.ticketid = t.ticketid_pk and p.paymentvoid = 0
	inner join 
		@letters l
	on	l.noteid = t.mailerid
	group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate
else
	insert into @QuoteClient
	select t.ticketid_pk, t.mailerid, t.activeflag, 
			min(p.recdate), t.recdate
	from tbltickets t 
	left outer join  
		tblticketspayment p 
	on	p.ticketid = t.ticketid_pk and p.paymentvoid = 0
	inner join 
		@letters l
	on	l.noteid = t.mailerid
	group by t.ticketid_pk, t.mailerid, t.activeflag, t.recdate

-- SUMMARIZING THE INFORMATION....
declare @temp2 table
		(
		mdate	datetime,
		letterCount	int,
		NonClient	int,
		Client		int,
		Quote		int,
		weekno INT
		)

INSERT into @temp2 (mdate, lettercount, client, Quote)
select l.mdate,  
	count(distinct l.noteid), 
	sum(case q.activeflag when 1 then 1 end), 
	sum(case q.activeflag when 0 then 1 end) 
from @letters l
left outer join
	@QuoteClient q
on  l.noteid = q.mailerid
group  by l.mdate

update t
set nonclient = lettercount - (isnull(quote,0)+isnull(client,0))
from @temp2 t

--Ids concatenates..
CREATE TABLE #temp3
(
	dates VARCHAR(20),
	ClientsIds VARCHAR(MAX)
)
---Client ids...
INSERT INTO #temp3(dates,ClientsIds) 
  SELECT convert(varchar,p1.mdate,101),
       ( SELECT CONVERT(VARCHAR, noteid)+ ','
           FROM  @letters p2
left outer join
	@QuoteClient q
on  p2.noteid = q.mailerid
          WHERE p2.mdate = p1.mdate AND q.activeflag=1
          ORDER BY mdate
            FOR XML PATH('') ) AS ids
      from @letters p1
left outer join
	@QuoteClient q
on  p1.noteid = q.mailerid
  WHERE q.activeflag=1
group  by p1.mdate


------- Non Client ids...

CREATE TABLE #temp4
(
	dates VARCHAR(20),
	nonClientsIds VARCHAR(MAX)
)

INSERT INTO #temp4(dates,nonClientsIds) 
  SELECT convert(varchar,p1.mdate,101),
       ( SELECT CONVERT(VARCHAR, noteid)+ ','
           FROM  @letters p2
left outer join
	@QuoteClient q
on  p2.noteid = q.mailerid
          WHERE p2.mdate = p1.mdate AND q.activeflag=0
          ORDER BY mdate
            FOR XML PATH('') ) AS ids
      from @letters p1
left outer join
	@QuoteClient q
on  p1.noteid = q.mailerid
  WHERE q.activeflag=0
group  by p1.mdate

CREATE TABLE #temp5
(
	date VARCHAR(20),
	clientsids VARCHAR(MAX),
	nonclients VARCHAR(MAX)
)
--INSERT INTO #temp5 (date,clientsids,nonclients)
--SELECT t3.dates,t3.ClientsIds,t4.nonClientsIds
--  FROM #temp3 t3 INNER JOIN #temp4 t4 ON t3.dates=t4.dates
INSERT INTO #temp5 (date,clientsids,nonclients)
SELECT t.mdate, t3.ClientsIds,t4.nonClientsIds
FROM @temp2 t LEFT OUTER JOIN #temp3 t3 ON t3.dates = t.mdate
LEFT OUTER JOIN #temp4 t4 ON t4.dates = t.mdate
  

-- group by year...
CREATE TABLE #temp6
(
	ydate VARCHAR(5),
	mdate VARCHAR(5),
	clientids VARCHAR(MAX),
	nonclientids VARCHAR(MAX)
)

-- group by year...
INSERT INTO #temp6 (ydate,mdate,clientids,nonclientids)
SELECT year(date), 
	MONTH(date) ,
	clientsids, nonclients
	FROM #temp5 
	group by year(date), month(date),clientsids,nonclients
order by year(date) desc, month(date) DESC 

CREATE TABLE #temp7
(
	ydate VARCHAR(5),
	mdate VARCHAR(5),
	clientids VARCHAR(MAX),
	nonclientids VARCHAR(MAX)
)
INSERT INTO #temp7 (mdate,ydate,clientids,nonclientids)
 SELECT p1.mdate,p1.ydate,
          ( SELECT clientids +'' 
              FROM #temp6 p2
             WHERE p2.mdate = p1.mdate AND p2.ydate=p1.ydate
            -- ORDER BY clientids
               FOR XML PATH('') ),
               ( SELECT nonclientids + '' 
              FROM #temp6 p2
             WHERE p2.mdate = p1.mdate AND p2.ydate=p1.ydate
            -- ORDER BY nonclientids
               FOR XML PATH('') ) 
      FROM #temp6 p1
     GROUP BY  p1.ydate,p1.mdate

select year(mdate) as ydate, 
	MONTH(mdate) as mdate,
	isnull(sum(nonclient),0) as nonclient, 
	isnull(sum(client),0) as client, 
	isnull(sum(quote),0) as quote,
	isnull(sum(lettercount),0) as lettercount
	into #temp
from @temp2 
group by year(mdate), month(mdate)
order by year(mdate) desc, month(mdate) desc

select convert(datetime, convert(varchar,mdate)+'/1/'+convert(varchar,ydate),101 ) as mdate,	
	nonclient, client, quote, lettercount,CONVERT(VARCHAR, DATEADD(mm, DATEDIFF(mm, 0, @sDate), 0),101)+' - '+ CONVERT(VARCHAR, DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, @eDate) + 1, 0)),101) AS dateRange
into #temp2
from #temp



select left( datename( month, t2.mdate ), 3 )+ '-' + right( datepart( year, t2.mdate ), 2 )as mdate,
	t2.nonclient,t2.client, t2.quote, t2.lettercount, t2.dateRange,t7.clientids AS clientsids,t7.nonclientids AS nonclients,
	CASE WHEN @DBid =1 THEN 1 ELSE 2 END AS ProjectType
	 from #temp2 t2 INNER JOIN #temp7 t7 ON ( YEAR (t2.mdate)=t7.ydate AND MONTH(t2.mdate)=t7.mdate )
ORDER BY t2.mdate DESC
drop table #temp
drop table #temp2

GO

GRANT EXECUTE ON [dbo].[usp_mailer_Monthly_AdvanceReport_data] TO dbr_webuser

GO




if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_PaymentSumByRecDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.usp_Get_All_PaymentSumByRecDate
go
 
create procedure dbo.usp_Get_All_PaymentSumByRecDate         
        
@RecDate DateTime,  
@firmId int = null        
        
AS        
      
        
SELECT PaymentType, COUNT(PaymentType) AS TotalCount, SUM(ChargeAmount) AS Amount        
Into #temp1        
FROM         dbo.tblTicketsPayment        
WHERE     (PaymentVoid <> 1)        
AND  datediff (day, recdate, @recdate ) = 0       
AND ticketId in (select ticketId_pk from tblticketsviolations where coveringFirmId = coalesce(@firmId,coveringFirmId))  
 --( RecDate  Between  @RecDate AND @RecDate+1)        
GROUP BY  PaymentType        
--HAVING      (CONVERT(varchar(12), RecDate, 101) = '03/01/2005')        
--AND ( RecDate  Between  @RecDate AND @RecTo+1)        
        
        
Select PaymentType_PK, Description        
Into #temp2         
From tblPaymenttype         
Where Left(Description,1) <>  '-'        
        
ALTER TABLE #temp2  ADD OrderID int NOT NULL DEFAULT 0        
        
Select * Into #temp5 From #temp2        
Update #temp5 Set OrderID= 1 Where PaymentType_PK = 5         
Update #temp5 Set OrderID= 3 Where PaymentType_PK > 5         
Update #temp5 Set OrderID= 5 Where Description = 'Refund'         
        
SELECT  CardType, Count(PaymentType) As TotalCount, Sum(ChargeAmount)As Amount        
Into #temp3         
FROM  tblTicketsPayment        
Where          
PaymentVoid <> 1         
AND CardType <> 0        
AND datediff (day, recdate, @recdate ) = 0       
AND ticketId in (select ticketId_pk from tblticketsviolations where coveringFirmId = coalesce(@firmId,coveringFirmId))  
Group by CardType        
        
        
        
Select CCTypeID, CCType        
Into #temp4         
From tblCCType         
        
ALTER TABLE #temp4 ADD OrderID int NOT NULL DEFAULT 2        
        
Select * Into #temp6 from #temp4        
        
-- ************************************************************        
Select  OrderID, PaymentType_PK, Description, isnull(TotalCount,0) AS TotalCount, isnull(Amount,0)AS Amount         
FROM #temp1 a right outer join #temp5 b    
on a.paymenttype = b.paymenttype_pk    
        
UNION        
        
SELECT OrderID, CCTypeID AS PaymentType_PK , CCType AS Description,isnull(TotalCount,0) AS TotalCount, isnull(Amount,0)AS Amount         
from #temp3 c right outer join #temp6 d    
on c.cardtype = d.cctypeid 


go

GRANT EXEC ON dbo.usp_Get_All_PaymentSumByRecDate   TO dbr_webuser
go   
 
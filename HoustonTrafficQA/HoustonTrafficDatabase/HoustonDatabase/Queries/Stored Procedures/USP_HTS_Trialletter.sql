         /****** 
Altered by:		Sabir Khan
Business Logic:	This procedure is used to pullup records on the trial letter report. 
				
				
List of Parameters:
	@TicketIDList
	@empid
	
*******/ 
--EXEC [dbo].[USP_HTS_Trialletter] 307874,3992
ALTER Procedure [dbo].[USP_HTS_Trialletter] --5819,3991         
 (                   
 @TicketIDList  int,                  
 @empid int                  
 )                  
as                  
SELECT dbo.tblTickets.TicketID_PK,               
 upper(dbo.tblTickets.MiddleName) as middlename,               
 dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK,               
          
          
 upper(dbo.tblTickets.Firstname) as firstname,               
 upper(dbo.tblTickets.Lastname) as lastname,            
             
-- added by asghar          
upper((case when dbo.tblTickets.FirmID = 3000 then (dbo.tblTickets.Firstname +' '+  dbo.tblTickets.MiddleName + ' '+dbo.tblTickets.Lastname) else (select FirmName from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end)) as FirmName,          
upper((case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.Address1 else (select Address from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID)  end)) as Address1,          
upper((case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.Address2 else (select Address2 from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end)) as Address2,          
upper((case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.City else (select City from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end)) as Ccity,          
upper((case when dbo.tblTickets.FirmID = 3000 then dbo.tblState.State else (select dbo.tblState.State where dbo.tblState.StateID =(select state from dbo.tblfirm where dbo.tblfirm.FirmID =dbo.tblTickets.FirmID)) end)) as cState,          
upper((case when dbo.tblTickets.FirmID = 3000 then dbo.tblTickets.Zip else (select Zip from tblfirm where tblfirm.FirmID =dbo.tblTickets.FirmID) end)) as Czip,          
          
          
 --dbo.tblTickets.Address1,               
 --dbo.tblTickets.Address2,          
 --dbo.tblTickets.City AS Ccity,               
 --dbo.tblState.State AS cState,               
 --dbo.tblTickets.Zip AS Czip,  
--Sabir Khan 5349 12/20/2008 auto court date has been changed into verified court date...               
 --dbo.tblTicketsViolations.CourtDate,                
dbo.tblTicketsViolations.CourtDatemain as CourtDate,                
 --dbo.tblTicketsViolations.CourtNumber,  
--Sabir Khan 5349 12/20/2008 auto court number has been changed into verified court number...               
dbo.tblTicketsViolations.CourtNumbermain as CourtNumber,               
 dbo.tblCourts.Address,               
 dbo.tblCourts.City,               
 dbo.tblCourts.Zip,               
 tblState_1.State,               
 '0' as Datetypeflag,               
 dbo.tblTickets.Activeflag,               
 isnull( dbo.tblTickets.LanguageSpeak, 'ENGLISH') AS Languagespeak,               
 '0' as SequenceNumber,               
 dbo.tblCourtViolationStatus.CategoryID,               
 --Farrukh 10272 05/10/2012 Replaced violationDate from 1/1/9100 to empty string
 (CASE WHEN DATEDIFF(DAY,ISNULL(dbo.tblTicketsViolations.TicketViolationDate,'1/1/1900'),'1/1/1900') <> 0 THEN CONVERT(VARCHAR(10),dbo.tblTicketsViolations.TicketViolationDate,101) ELSE '' END) AS TicketViolationDate,
 --Farrukh 10148 04/18/2012 Added Spanish Violation Description for Spanish Clients
 (CASE WHEN isnull( dbo.tblTickets.LanguageSpeak, 'ENGLISH') <> 'ENGLISH' AND ISNULL(dbo.tblViolations.SpanishDescription,'') <> '' THEN dbo.tblViolations.SpanishDescription ELSE dbo.tblViolations.[Description] END) AS [Description],
 --dbo.tblViolations.Description, --Sabir Khan 8862 02/23/2011 get violation description in case of HCJP court and Juvnile Status...                 
 CASE WHEN dbo.tblCourts.courtcategorynum = 2 AND dbo.tblTicketsViolations.CourtViolationStatusIDmain IN (139,256,257,258,259) THEN dbo.tblCourtViolationStatus.[Description] ELSE dbo.tblDateType.Description END AS CaseStatus ,         
 dbo.tblTicketsViolations.courtid as courtid 
 --Yasir Kamal 7590 04/01/2010 check Discrepancy in court date.          
 INTO #temp           
FROM    dbo.tblTickets               
INNER JOIN              
 dbo.tblTicketsViolations               
ON  dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK               
INNER JOIN              
 dbo.tblCourts               
--ON  dbo.tblTickets.CurrentCourtloc = dbo.tblCourts.Courtid               
ON  dbo.tblTicketsviolations.courtid = dbo.tblCourts.Courtid               
INNER JOIN              
 dbo.tblState               
ON  dbo.tblTickets.Stateid_FK = dbo.tblState.StateID               
INNER JOIN              
 dbo.tblState tblState_1               
ON  dbo.tblCourts.State = tblState_1.StateID               
INNER JOIN              
 dbo.tblCourtViolationStatus    
--Sabir Khan 5349 12/20/2008 auto court violation status has been changed into verified court  violation status...             
--ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID               
ON  dbo.tblTicketsViolations.CourtViolationStatusIDmain = dbo.tblCourtViolationStatus.CourtViolationStatusID               
INNER JOIN              
 dbo.tblViolations               
ON  dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK               
INNER JOIN              
 dbo.tblDateType               
ON  dbo.tblCourtViolationStatus.CategoryID = dbo.tblDateType.TypeID              
WHERE   (dbo.tblTickets.Activeflag = 1)   
--Sabir Khan 5349 12/20/2008 auto court date has been changed into verified court date...              
--AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)    
AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDatemain, GETDATE()) <= 0)
--Sabir Khan 7074 12/04/2009 No need to print arrignment cases in trial letter...             
--Sabir Khan 8862 02/23/2011 Include Juvnile statuses for HCJP court only.            
AND  ((dbo.tblCourtViolationStatus.CategoryID IN (3, 4, 5)) OR (dbo.tblCourts.courtcategorynum = 2 AND dbo.tblTicketsViolations.CourtViolationStatusIDmain IN (139,256,257,258,259) ))          
--AND  (NOT (dbo.tblTicketsViolations.RefCaseNumber LIKE 'ID%'))              
AND  (dbo.tblTickets.TicketID_PK = @ticketidlist)              
              
--Yasir Kamal 7590 04/01/2010 check Discrepancy in court date.              
--if @@rowcount >=1              
-- exec sp_Add_Notes @TicketIDList,'Trial Letter Printed',null,@empid 

ALTER TABLE #temp 
	ADD isDiscrip INT 
	
UPDATE #temp SET 
	isDiscrip = 0
	
SELECT distinct a.TicketID_PK INTO #temp2 
from #temp a inner join #temp b on a.TicketID_PK = b.TicketID_PK
where a.courtdate <> b.courtdate

UPDATE a SET  a.isDiscrip = 1
FROM  #temp a INNER JOIN #temp2 v ON a.TicketID_PK = v.TicketID_PK

SELECT * FROM #temp         

           

DROP TABLE #temp           
DROP TABLE #temp2 
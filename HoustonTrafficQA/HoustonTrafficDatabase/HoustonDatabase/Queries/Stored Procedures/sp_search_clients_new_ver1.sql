﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[sp_search_clients_new_ver1]    Script Date: 11/23/2011 18:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO










--select * from tblcourtviolationstatus

--exec sp_search_clients_new_ver1 'TR41C3106607','','','','','','','','',''


CREATE                                procedure [dbo].[sp_search_clients_new_ver1]
@Inputticketnumber varchar(20) = null,
@InputDLNumber varchar(30) =null  ,
@InputLastName varchar(20)= null ,
@InputFirstname varchar(20)= null ,
@InputMidnum varchar(20) =null  ,
@InputDOB datetime = null ,
@InputPhoneNumber varchar(20) = null,
@InputTrialDate datetime = null ,
@InputArraignDate datetime  = null,
@Inputcontactdate datetime = null,
@quoteflag int = 1
as
set nocount on
Set rowcount 250
--declare @Inputcontactdate datetime
DECLARE @SQLString NVARCHAR(1500),
@ParmDefinition NVARCHAR(500),
 @ticketnumber varchar(20),
@violationNumber int, --Haris 9709 09/29/2011 datatype changed to int from tinyint
@tempticketnumber varchar(20),
@tempviolationNumber varchar(50),
@tempticketid int,
@firstname varchar(20),
@lastname varchar(20),
@midnum varchar(20),
@fineamount money,
@dateset datetime,
@courtnum varchar(3),
@category varchar(50),
@sumfineamount money,
@TotalFeeCharged money,
@violationdate datetime,
@dob datetime,
@contactdate datetime,
@tempcontactdate datetime,
@tempfirstname varchar(20),
@templastname varchar(20),
@tempmidnum varchar(20),
@tempdateset datetime,
@tempcourtnum varchar(3),
@tempcategory varchar(50),
@tempaddress1 varchar(50),
@activeflag int,
@tempactiveflag int,
@tempactualviolationnumber int, --Haris 9709 09/29/2011 datatype changed to int from tinyint
@actualviolationnumber int,
@Address1 varchar(50),
@inttempviolationnumber  int,
@tempTotalFeeCharged money,
@tempviolationdate datetime,
@tempDOB datetime,
@ticketid int,
@orderbyfirstnameclause tinyint,
@orderbylastnameclause tinyint
set @inttempviolationnumber = 0
set @tempactualviolationnumber = 0
create TABLE #temp (
ticketnumber varchar(20),
violationNumber varchar(50), 
firstname varchar(20),
lastname varchar(20),
midnum varchar(20),
fineamount money,
dateset datetime,
courtnum varchar(3),
category varchar(50),
TotalFeeCharged money,
violationdate datetime,
DOB datetime,
ticketid int,
address1 varchar(50),
activeflag int,
actualviolationnumber int ,
contactdate datetime
)
SET @SQLString =
     N' DECLARE tickets_cursor CURSOR FOR  SELECT DISTINCT 
                      t.Firstname, t.Lastname, t.Midnum, LTRIM(v.RefCaseNumber), v.SequenceNumber, v.FineAmount, v.courtdatemain, v.courtnumbermain, 
                      isnull(S.Description,''Not Set'') as Description, t.TotalFeeCharged, t.ViolationDate, t.DOB, t.TicketID_PK, t.Address1, t.Activeflag, v.ViolationNumber_PK, t.LastContactDate
FROM         dbo.tblTickets t INNER JOIN
                         dbo.tblTicketsViolations v ON t.TicketID_PK = v.TicketID_PK INNER JOIN
                      dbo.tblViolations lv ON v.ViolationNumber_PK = lv.ViolationNumber_PK LEFT OUTER JOIN
		      dbo.tblcourtviolationstatus S On V.courtviolationstatusidmain = S.courtviolationstatusid LEFT OUTER JOIN
		      dbo.tblDateType Y ON S.categoryid = Y.TypeID LEFT OUTER JOIN
                      dbo.tblTicketsContact C ON t.TicketID_PK = C.TicketID_PK 
WHERE     lv.Violationtype not IN (1) '

set @sqlstring = @sqlstring + N' and activeflag = ' + convert(varchar(2),@quoteflag)

if @Inputticketnumber <> ''
	begin
	set @sqlstring = @sqlstring + N' and v.RefcaseNumber like @Inputticketnumber + ''%'''  
	--set @sqlstring = @sqlstring + N' and v.RefcaseNumber like left(@Inputticketnumber,9) + ''%'''  
	end
if @InputDLNumber <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.DLNumber = @InputDLNumber'
	end
if @InputLastName <> ''
	begin
	set @orderbylastnameclause = 1
	set @sqlstring = @sqlstring + N' and Rtrim(t.lastname) like  @InputLastName + ''%'''  
	end
if @InputFirstname <> ''
	begin
	set @orderbyfirstnameclause = 1
	set @sqlstring = @sqlstring + N' and rtrim(t.firstname) like  @InputFirstname + ''%'''    
	end
if @InputMidnum <> ''
	begin
	
	set @sqlstring = @sqlstring + N' and t.midnum = @InputMidnum' 
	end
if @InputDOB <> ''
	begin
	set @sqlstring = @sqlstring + N' and t.DOB = @InputDOB' 
	end
if @InputPhoneNumber <> ''
	begin
	set @sqlstring = @sqlstring + N' and C.contact = @InputPhoneNumber' 
	end
if @InputTrialDate <> ''
	begin
	set @sqlstring = @sqlstring + N' and datediff(day,t.currentdateset,@InputTrialDate) = 0' 
	end
if @InputArraignDate <> ''
	begin
	set @sqlstring =@sqlstring + N' and t.currentdateset = @InputArraignDate and datetypeflag=1' 
	end
if @InputcontactDate <> ''
	begin
	set @orderbylastnameclause = 1
	set @sqlstring =@sqlstring + N' and datediff(day,t.lastcontactdate,@Inputcontactdate) = 0' 
	end
if @orderbyfirstnameclause = 1
begin
	set @sqlstring =@sqlstring + N' order by t.firstname,t.lastname,t.midnum'
end
else
	if @orderbylastnameclause = 1
	begin
		set @sqlstring =@sqlstring + N' order by t.lastname,t.firstname,t.midnum,V.courtdatemain'
	end
if @orderbyfirstnameclause is null and @orderbylastnameclause is null
begin
	set @sqlstring =@sqlstring + N' order by t.ticketid_pk,ltrim(refcasenumber),v.violationnumber_pk' 
end

--print @sqlstring
set @ParmDefinition = N'@Inputticketnumber varchar(20),@InputDLNumber varchar(30),@InputLastName varchar(20),
			@InputFirstname varchar(20),@InputMidnum varchar(20) ,@InputDOB datetime ,
			@InputPhoneNumber varchar(20),@InputTrialDate datetime,@InputArraignDate datetime,@InputcontactDate datetime'
EXECUTE sp_executesql @SQLString,@ParmDefinition ,@Inputticketnumber,@InputDLNumber,@InputLastName,@InputFirstname,
		@InputMidnum,@InputDOB,@InputPhoneNumber,@InputTrialDate,@InputArraignDate,@InputContactDate
 
OPEN tickets_cursor
  FETCH NEXT FROM tickets_cursor INTO 
	@firstname,@lastname,@midnum,@ticketnumber,@violationNumber,@fineamount,@dateset,@courtnum,@category,@TotalFeeCharged,@violationdate,@DOB,@ticketid,@address1,@activeflag,@actualviolationnumber,@contactdate
	IF @@FETCH_STATUS = 0     
		begin
			set @tempticketnumber = @ticketnumber
			set @tempcategory = @category
			set @sumfineamount = 0
			WHILE @@FETCH_STATUS = 0
				BEGIN
					if (@tempticketnumber) = @ticketnumber and @tempcategory = @category
					
						begin	
							
 								if  @tempactualviolationnumber  <> @actualviolationnumber
								begin
									set @tempviolationNumber = ltrim(isnull(@tempviolationNumber,' ') + ' ' + convert(varchar(5),@violationNumber))
									set @sumfineamount = @sumfineamount + @fineamount 
								end
						end
					else
						begin
							--print rtrim(@tempticketnumber) + ' ' + @tempviolationNumber
							insert into #temp(ticketnumber,violationNumber , firstname ,lastname ,midnum ,
							fineamount ,dateset ,courtnum ,category,TotalFeeCharged,violationdate,dob,ticketid,address1,activeflag,actualviolationnumber)
							 values(rtrim(@tempticketnumber),@tempviolationNumber,@tempfirstname,@templastname,@tempmidnum,
							@sumfineamount,@tempdateset,@tempcourtnum,@tempcategory,@tempTotalFeeCharged,@tempviolationdate,@tempdob,@tempticketid,@tempaddress1,@tempactiveflag,@tempactualviolationnumber)
						
							set @tempviolationNumber = ' '
							set @tempactualviolationnumber = ' '
							set @sumfineamount = 0
							set @tempviolationNumber = ltrim(isnull(@tempviolationNumber,' ') + ' ' + convert(varchar(5),@violationNumber))
							
							set @sumfineamount = @sumfineamount + @fineamount 
						end
					set @tempticketnumber = @ticketnumber
					set @tempfirstname = @firstname
					set @templastname = @lastname
					set @tempmidnum = @midnum
					set @tempdateset = @dateset
					set @tempcourtnum = @courtnum
					set @tempcategory = @category
					set @tempTotalFeeCharged = @TotalFeeCharged
					set @tempviolationdate = @violationdate
					set @inttempviolationnumber = @violationNumber
					set @tempDOB = @DOB
					set @tempticketid = @ticketid
					set @tempaddress1 = @address1
					set @tempactiveflag =@activeflag
					set @tempactualviolationnumber =@actualviolationnumber
					set @tempcontactdate = @contactdate
					FETCH NEXT FROM tickets_cursor INTO 
					@firstname,@lastname,@midnum,@ticketnumber,@violationNumber,@fineamount,@dateset,@courtnum,@category,@TotalFeeCharged,@violationdate,@DOB,@ticketid,@address1,@activeflag,@actualviolationnumber,@contactdate
				END 
			insert into #temp(ticketnumber,violationNumber , firstname ,lastname ,midnum ,
			fineamount ,dateset ,courtnum ,category,TotalFeeCharged,violationdate,DOB,ticketid,address1,activeflag,actualviolationnumber,contactdate)
			values(rtrim(@tempticketnumber),@tempviolationNumber,@firstname,@lastname,@midnum,
			@sumfineamount,@dateset,@courtnum,@category,@tempTotalFeeCharged,@violationdate,@DOB,@ticketid,@address1,@activeflag,@actualviolationnumber,@contactdate) 
		
		end 
CLOSE tickets_cursor
DEALLOCATE tickets_cursor
select * from #temp
drop table #temp 
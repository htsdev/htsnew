SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_JIMS_GetBadAddressfromDataFilebyDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_JIMS_GetBadAddressfromDataFilebyDate]
GO



CREATE procedure usp_JIMS_GetBadAddressfromDataFilebyDate
@ReportDate datetime 
as



select T.ClientID, T.bookingnumber,V.span,T.firstname,T.lastname,middlename,T.address,T.city,
T.state,T.zip,convert(varchar(12),dob) as dob,convert(varchar(12),arrestdate) as arrestdate,flag1 
from jims.dbo.criminalcases T, jims.dbo.criminal503 V
where T.Bookingnumber = V.bookingnumber
and datediff(day,T.recdate,@ReportDate) = 0
and flag1 not in ('D','S', 'Y')
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


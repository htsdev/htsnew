IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HTS_GetActiveFirms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HTS_GetActiveFirms]
go  
create PROCEDURE  [dbo].[usp_HTS_GetActiveFirms]      
@FirmType int  
   
AS      

if @FirmType = 0   
Begin  
  
Select  FirmID, FirmAbbreviation  From tblFirm      
where  cancoverFrom =1
order by FirmAbbreviation           
  
end  
  
if @FirmType = 1   
Begin  
Select  FirmID, FirmAbbreviation  From tblFirm      
where cancoverby =1 
order by FirmAbbreviation      
end
go
go
GRANT EXEC ON [dbo].[usp_HTS_GetActiveFirms] TO [dbr_webuser]
go
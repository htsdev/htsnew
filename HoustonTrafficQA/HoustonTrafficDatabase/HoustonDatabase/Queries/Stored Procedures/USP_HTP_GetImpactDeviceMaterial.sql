﻿/************************************************************
 * Created By : Noufil Khan 
 * Created Date: 10/30/2010 12:35:27 PM
 * Business Logic : This procedure returns all Impact device material 
 * Column Return :
 *					Id, Description
 ************************************************************/

alter PROCEDURE [dbo].[USP_HTP_GetImpactDeviceMaterial]
AS
	SELECT idm.Id,
	       idm.Description
	FROM   ImplantDeviceMaterial idm
	ORDER BY
	       DESCRIPTION
GO

GRANT EXECUTE ON [dbo].[USP_HTP_GetImpactDeviceMaterial] TO dbr_webuser

go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Get_UnassignedCourt]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Get_UnassignedCourt]
GO


CREATE procedure [dbo].[USP_Get_UnassignedCourt]

as

select CourtViolationStatusID,description from  tblcourtviolationstatus where isnull(categoryid,0)=0
order by description

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


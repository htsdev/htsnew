﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Created by:   Fahad Muhammad Qureshi.
Created Date: 10/01/2009
Task ID   : 5376

Business Logic : this procedure retrieve the User Type.

List of Parameters:	
			@EmpID : Employee id 
			
List of Columns:
			Accesstype
	

******/

Create PROCEDURE [dbo].[USP_HTP_Get_UserAccessType]
@EmpID int 
as  
SELECT tu.Accesstype FROM tblUsers tu
	
WHERE 
	 tu.EmployeeID=@EmpID
	 


GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_UserAccessType] TO dbr_webuser
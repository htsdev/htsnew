﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure insert new attorney schedule by given parameter and return inserted Atttorney Schedule ID.
			
List of Parameters:				
			@DocketDate			trial date
			@AttorneyID:		attorney identification
			@ContactNumber		attorney contact number
			@EmployeeID			Employee identification
			@AtttorneyScheduleID   return inserted Atttorney Schedule ID
					
******/  
  
Create PROCEDURE dbo.USP_HTP_Insert_AttorneySchedule   
 @DocketDate VARCHAR(10),  
 @AttorneyID INT,  
 @ContactNumber VARCHAR(14),  
 @EmployeeID INT,
 @AtttorneyScheduleID INT=0 output
AS  
     
    BEGIN  
        INSERT INTO AttorneysSchedule  
          (             
           DocketDate,  
           RecDate,  
           ContactNumber,  
           AttorneyID,
           UpdatedByEmployeeID             
          )  
        VALUES  
          (  
             
           @DocketDate,/* DocketDate */  
           GETDATE(),/* RecDate */  
           @ContactNumber,/* ContactNumber */  
           @AttorneyID,/* AttorneyID */  
           @EmployeeID/* EmployeeID */  
          )  
          
          SET @AtttorneyScheduleID= @@IDENTITY
    END         
    
    GO 
   
   GRANT EXECUTE on dbo.USP_HTP_Insert_AttorneySchedule TO dbr_webuser
     
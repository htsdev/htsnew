USE [TrafficTickets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 /******  
* Created By :	  Sabir Khan.
* Create Date :   09/09/2013 
* Task ID :		  11277
* Business Logic :  This procedure is used to get the loader id of HCCC Scrapper Loader.
  
******/
CREATE PROCEDURE [dbo].[USP_HTP_GetHCCCScraperLoaderID]
AS
SELECT Loader_ID  
FROM LA_Houston.dbo.tbl_HTS_LA_Loaders 
WHERE Loader_Name LIKE 'HCCC Criminal Filings Scraper Loader'

GO

GRANT EXECUTE ON [dbo].[USP_HTP_GetHCCCScraperLoaderID] TO dbr_webuser

GO


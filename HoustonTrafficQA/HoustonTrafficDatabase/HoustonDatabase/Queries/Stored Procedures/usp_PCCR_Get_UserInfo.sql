SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_PCCR_Get_UserInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_PCCR_Get_UserInfo]
GO


-- PROCEDURE USED TO FILL USERS IN DROP DOWN AS SELECTION CRITERIA......
CREATE  procedure usp_PCCR_Get_UserInfo

as

select 	convert(varchar(5),employeeid),
	ltrim(rtrim(upper(username)))
from 	tblusers
where 	len(username) > 0
and 	username is not null



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


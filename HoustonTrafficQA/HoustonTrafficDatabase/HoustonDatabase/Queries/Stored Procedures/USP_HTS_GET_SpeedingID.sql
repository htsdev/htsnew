﻿--   USP_HTS_GET_SpeedingID 8879
ALTER procedure [dbo].[USP_HTS_GET_SpeedingID]
	(
	@TicketID	int,
	@Speed		int = 0  output
	)
as
SET NOCOUNT ON;
declare @FineAmount	money,
	@SchoolZone	bit

set	@FineAmount = 0

declare @temp table
	(
	ticketsviolationid	int,
	fineamount		money,
	schoolzone		bit
	)	

insert into @temp
select	tv.ticketsviolationid,
	tv.fineamount,
	(case when v.description like '%schl%' then 1 else 0 end ) as SchoolZone
from 	tblticketsviolations tv WITH(NOLOCK)
inner join
	tblviolations v WITH(NOLOCK)
on 	v.violationnumber_pk = tv.violationnumber_pk
where	1=1 
--and	tv.courtid in (3001,3002,3003)
and 	ticketid_pk = @TicketID
and	v.description like '%speeding%'

--select * from @temp

select	@FineAmount = isnull( ( select max(fineAmount) from @temp) , 0)
select	@SchoolZone = schoolzone from @temp where fineamount = @fineAmount


IF @FineAmount <> 0 
	IF(@FineAmount <= 160)
		Set @Speed = 10
	ELSE
		if @SchoolZone=0
			Set @Speed = (@fineamount - 160)/5  
		else
			Set @Speed = (@FineAmount - 160)/5 +10
ELSE
	Set @Speed = -1
--select @speed
-- Agha Usman - 4274 - 07/22/2008 - Update speeding information 
select 	@Speed  = speedingid_pk 
from 	tblspeeding WITH(NOLOCK) 
where @Speed between  speedfrom and speedto
and 	typeid = 1


select isnull(@speed,0) 







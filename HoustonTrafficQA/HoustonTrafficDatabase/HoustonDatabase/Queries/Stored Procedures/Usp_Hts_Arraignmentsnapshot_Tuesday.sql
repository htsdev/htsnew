ALTER procedure [dbo].[Usp_Hts_Arraignmentsnapshot_Tuesday]          
          
          
as          
          
          
Declare @intTotal int                      
Declare @intTot int                      
Select @intTotal = 0                      
Select @intTot = 0         
declare @strComments varchar(5000)            
          
          
declare  @Tuesday table          
(                      
          
CustNumber INT IDENTITY (1, 1) NOT NULL,                      
TicketID_PK int,                      
LastName varchar(30) NULL,                      
FirstName varchar(30) NULL,                      
ViolationDate datetime,                       
TrialDesireddateTime datetime NULL,                      
TrialComments varchar(100) NULL,                      
BondFlag int  NULL DEFAULT 0,                      
TrialDateTime varchar(10) NULL,                      
pretrialstatus varchar(1)                       
          
 )                
          
declare  @SEQ table          
(                      
CustNumber INT IDENTITY (1, 1) NOT NULL ,                      
Pola varchar(1)          
)             
                      
                      
declare  @temp table          
(          
                      
TicketID_PK int,                      
LastName varchar(30) NULL,                      
FirstName varchar(30) NULL,                      
ViolationDate datetime,                       
TrialDesireddateTime datetime NULL,                      
TrialComments varchar(100) NULL,                      
BondFlag int  NULL DEFAULT 0,                      
TrialDateTime varchar(10) NULL,                      
pretrialstatus varchar(1),                      
ticketsviolationid int,                      
officertype varchar(10),                      
datepartvalue int                      
          
 )               
          
insert into @temp                      
Select distinct T.TicketID_PK, T.Lastname,T.Firstname,                       
 V.Courtdatemain ,                       
null as setdate                      
--dbo.fn_CourtDesiredSetDate_ver_3(V.ticketsviolationid,officertype,v.courtdatemain)                      
, T.SettingComments,                       
T.BondFlag , O.TrialDateTime, pretrialstatus = '',                       
0,officertype,0                       
                      
FROM         tblTickets T INNER JOIN                      
                      tblTicketsViolations V ON T.TicketID_PK = V.TicketID_PK LEFT OUTER JOIN                      
                      tblOfficer O ON V.ticketofficernumber = O.OfficerNumber_PK                      
WHERE     V.courtviolationstatusidmain --in (1,2,3,5,8)                       
in (select courtviolationstatusid from tblcourtviolationstatus where CATEGORYID IN (2,12,51))                      
--and (DATEPART(dw, dbo.fn_CourtDesiredSetDate_ver_3(V.ticketsviolationid,officertype,v.courtdatemain)) = 2)                      
AND (T.Activeflag = 1) AND (V.courtid IN (3001, 3002, 3003))                      
and v.courtdatemain is not null                      
AND V.courtviolationstatusidmain <> 80                  
AND V.courtviolationstatusidmain <> 162 --added by ozair for bug # 1432                      
order by v.courtdatemain, T.lastname, T.firstname                 
          
          
          
Select @intTotal = @intTotal + @@ROWCOUNT                      
INSERT INTO @Tuesday(TicketID_PK,FirstName, LastName, ViolationDate , TrialDesireddateTime, TrialComments, BondFlag ,TrialDateTime, pretrialstatus)                      
select TicketID_PK,Firstname,Lastname,                      
violationdate,TrialDesireddateTime as setdate,TrialComments,BondFlag,                      
TrialDateTime,pretrialstatus                      
from @temp                      
where officertype = 'TUESDAY'                      
ORDER BY datediff(day,violationdate,getdate()) desc,ltrim(lastname), firstname             
          
Select @intTotal = @intTotal + @@ROWCOUNT                      
While NOT (@intTot = @intTotal )                      
BEGIN                      
  Select  @intTot = @intTot + 1                      
  INSERT INTO   @SEQ(Pola) Values('P')                      
END             
          
        
---This query is for merging comments all cases to show on crystal report----------------            
          
 set @strComments=''          
            
       
   select       
      @strComments= @strComments +' '+ case when isnull(t.trialcomments,'') != '' or t.trialcomments != ' ' then  convert(varchar,s.CustNumber) +' '+ isnull(t.trialcomments,'')  +char(13) else ''  end                         
           from    
   @SEQ s LEFT OUTER JOIN                            
   @Tuesday t ON s.CustNumber = t.CustNumber             
                 
WHERE                             
t.TicketID_PK IS NOT NULL                             
ORDER BY s.CustNumber ASC             
          
          
---------------------------------------------------------------------------------------            
        
        
SELECT     s.CustNumber AS SEQ_CustNumber , t.TicketID_PK AS TUE_TicketID_PK, Left(t.LastName,10)+','+Left(t.FirstName,1) AS TUE_Name,                         
                      t.ViolationDate AS TUE_ViolationDate, t.TrialDesireddateTime AS TUE_TrialDesireddateTime,                          
                                           
        
 (case when  t.TrialComments is not null and t.trialcomments <> '' then '*'            
     else ''            
            
     end) AS TUE_TrialComments,             
            
     (case             
      when t.BondFlag=1 then 'b'            
            
                   
      else ''            
               
     end)AS TUE_BondFlag,             
            
     (case  when t.TrialDateTime='10:30AM' then 't'            
     else ''            
     end)            
            
      AS TUE_TrialDateTime,                             
            
      (case when  t.pretrialstatus != 'P' then ''                      
     else  t.pretrialstatus end)as TUE_pretrialstatus ,            
               
     (case when datediff(day,getdate(),t.ViolationDate) >=0 and datediff(day,getdate(),t.ViolationDate)<=2 then            
      '+'            
      else ''            
      end) as PlusSymbol,            
                 
     @strComments as tcomments,          
  t.trialcomments          
        
        
        
FROM                @SEQ s LEFT OUTER JOIN                        
                    @Tuesday t ON s.CustNumber = t.CustNumber                       
                           
WHERE                         
t.TicketID_PK IS NOT NULL                         
                      
ORDER BY s.CustNumber ASC   
  
go
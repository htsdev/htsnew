﻿/*
* Task : 5966   
* Created By : Waqas Javed 
* 
* Business Logic:
* This procedure is used to get distinct occupation of the clients
* 
* Columns:
* Occupation
*/
   
--Waqas 6666 10/22/2009 with search criteria     
alter  procedure dbo.usp_htp_get_clientOccupation    
@criteria varchar(max) = ''
as    
    
begin 
--Waqas 6894 10/30/2009 calling from table occupation
 select distinct TOP 15 occupation as occupation from occupation    
 where occupation like @criteria + '%'
 AND occupation is not null and len(occupation)>0    
 order by occupation    
end    
  
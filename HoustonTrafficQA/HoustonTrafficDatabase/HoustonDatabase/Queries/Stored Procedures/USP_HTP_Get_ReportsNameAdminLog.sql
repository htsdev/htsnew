﻿/****** 
Create by		: Muhammad Nasir Mamda
Created Date	: 12/14/2009
TasK			: 6968

Business Logic : 
			This procedure simply return the Reports Name having given IsAdminLog.
			
List of Parameters:	
			@IsAdminLog: allow admin log
			
Column Return : 
			ID:	menu id
			Title: report title
******/

Create PROCEDURE dbo.USP_HTP_Get_ReportsNameAdminLog
@IsAdminLog BIT
AS
SELECT tts.ID,tts.TITLE FROM tbl_TAB_SUBMENU tts 
WHERE tts.IsAdminLog=@IsAdminLog

GO

GRANT EXECUTE ON dbo.USP_HTP_Get_ReportsNameAdminLog TO dbr_webuser
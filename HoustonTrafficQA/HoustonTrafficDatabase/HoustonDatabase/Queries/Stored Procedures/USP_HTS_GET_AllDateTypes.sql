SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_AllDateTypes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_AllDateTypes]
GO



CREATE  procedure [dbo].[USP_HTS_GET_AllDateTypes] as

select	TypeID,
	[Description] from tbldatetype
--where (typeid<> 0 and description not like '%------%')
where isactive = 1
order by orderfield



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


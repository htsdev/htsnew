USE TrafficTickets
GO
/****** 
Created by:		Sabir Khan
Task ID:		9017
Business Logic:	This procedure is used to get the sprecher cases that have one day court date in past and up disposed them.
******/
Alter PROCEDURE dbo.USP_HTP_SprecherDisposed
AS
BEGIN
    SELECT DISTINCT t.TicketID_PK,
           IDENTITY(INT, 1, 1) AS RowID INTO #temp
    FROM   tbltickets t
           INNER JOIN tblticketsviolations tv
                ON  tv.TicketID_PK = t.TicketID_PK
           INNER JOIN tblTicketsPayment tp
                ON  tp.TicketID = t.TicketID_PK
           INNER JOIN tblTicketsFlag tf
                ON  tf.TicketID_PK = t.TicketID_PK
           INNER JOIN tblFirm tfm
                ON  tfm.FirmID = t.FirmID
    WHERE  tfm.FirmID = 3090
           AND tv.CoveringFirmID = 3090
           AND tp.PaymentReference = 3090
           AND tp.PaymentType = 104
           AND t.LanguageSpeak = 'SPANISH'
           AND tv.CourtID = 3001
           AND tf.FlagID = 8
           AND tf.assignedto = 3090
           AND t.Activeflag = 1
           AND tv.CourtViolationStatusIDmain <> 80
           AND DATEDIFF(DAY, tv.CourtDateMain, dbo.fPreviousWorkingDay(GETDATE())) >= 0   
    DECLARE @tecketID_pk INT
    DECLARE @RowID INT
    SET @RowID = 1
    WHILE (
              (
                  SELECT COUNT(*)
                  FROM   #temp
              ) > 0
          )
    BEGIN
        UPDATE tblticketsviolations
        SET    CourtViolationStatusID = 80,
               CourtViolationStatusIDmain = 80,
               CourtViolationStatusIDScan = 80,
               VEmployeeID = 3992
        WHERE  TicketID_PK IN (SELECT TicketID_PK
                               FROM   #temp
                               WHERE  RowID = @RowID)	
        
        DELETE 
        FROM   #temp
        WHERE  RowID = @RowID
        
        SET @RowID = @RowID + 1
    END
    
    DROP TABLE #temp
END	

GO

GRANT EXECUTE ON dbo.USP_HTP_SprecherDisposed TO dbr_webuser

GO





USE [TroubleTicket]
GO
/****** Object:  StoredProcedure [dbo].[usp_tt_get_caseinfoforbug]    Script Date: 02/08/2008 18:02:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tt_get_caseinfoforbug]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tt_get_caseinfoforbug]

go

create procedure [dbo].[usp_tt_get_caseinfoforbug]  --3790 ,1    
@ticketID int  
as          
  
Select  top 1          
  t.ticketid_pk,          
  --t.firstname,          
  --t.middlename,          
  --t.lastname,          
  tv.casenumassignedbycourt,          
  --c.shortname,          
  --tcv.shortdescription,          
  --v.description,        
  tv.refcasenumber           
          
from TrafficTickets.dbo.tbltickets t inner join           
 TrafficTickets.dbo.tblticketsviolations tv on           
 t.ticketid_pk=tv.ticketid_pk       
where           
 t.ticketid_pk=@ticketID   

go


go
GRANT EXECUTE ON [dbo].[usp_tt_get_caseinfoforbug] TO [dbr_webuser]
go


/***************  
  
Alter By : Zeeshan Ahmed   
  
Business Logic : This Procedure is used to view or print the letters in the batch  
     Display Missed Court Status violation for Missed Court & Pled Out letter and LOR .  
  
ist of Parameters:           
      
 @LType     : Letter Type                                                      
 @emp_id    : Employee Name               
 @courtid   : Court ID                          
 @isprinted : Used to display Printed records                  
 @fromdate  : Start date   
 @todate    : End Date    
 @bondflag  : Bond Flag   
      
List of Columns:       
       
 [name]                    
 Tooltip                
 batchdate                   
 b_emp                    
 courtdate                   
 courtnumber                   
 printdate                   
 BatchID               
 p_emp                   
 isprinted                   
 countp                  
 trialdate                 
 trilroom                   
 batchdate1                   
 batchemp                   
 printdate1  
 printemp                
 isSplit                
 courtid                
 status                
 courtname  
 email  
 bond  
 trackingnumber  
 eflag  
 Response  
  
*****************/  
  
-- Fahad 2769 (2-1-08)       
-- Procedure Modified By Fahad Add new field (email) in output parameters     
-- Modified by Noufil trial Notification include new field USPSTrackingNumber, EDeliveryFlag,uspsresponse     
--[dbo].[USP_HTS_GET_BatchTicketID_New_v1] 2,'FA',0,1,'11/17/2008','11/17/2008',1   
ALTER PROCEDURE [dbo].[USP_HTS_GET_BatchTicketID_New_v1] -- 13,'FA',0,0,'04/14/2008','04/14/2008'
	@LType INT ,
	@emp_id VARCHAR(10),
	@courtid INT,
	@isprinted INT,
	@fromdate DATETIME,
	@todate DATETIME,
	@bondflag INT = 0
AS
	-- Aziz 3220 02/22/08 Used initial time for FromDate and end time for Todate. There was problem in searching      
	SET @fromdate = @fromdate + '00:00:00';      
	SET @todate = @todate + '23:59:58';      
	
	DECLARE @tmp TABLE (batchid_pk INT, ticketid INT)            
	
	IF @isprinted = 0
	BEGIN
	    INSERT INTO @tmp
	      (
	        ticketid,
	        batchid_pk
	      )
	    SELECT ticketid_fk,
	           MAX(batchid_pk)
	    FROM   tblhtsbatchprintletter
	    WHERE  deleteflag <> 1
	           AND letterid_fk = @ltype
	           AND isprinted = 0
	    GROUP BY
	           ticketid_fk
	END
	ELSE
	BEGIN
	    INSERT INTO @tmp
	      (
	        ticketid,
	        batchid_pk
	      )
	    SELECT ticketid_fk,
	           MAX(batchid_pk)
	    FROM   tblhtsbatchprintletter
	    WHERE  deleteflag <> 1
	           AND letterid_fk = @ltype
	           AND isprinted = 1
	           AND printdate BETWEEN @fromdate AND @todate
	    GROUP BY
	           ticketid_fk
	END        
	
	DECLARE @tbl1 TABLE 
	        (
	            RowNo INT IDENTITY,
	            ticketid_pk INT,
	            [Name] VARCHAR(100),
	            Tooltip VARCHAR(100),
	            batchdate DATETIME,
	            BatchID INT,
	            b_Emp VARCHAR(10),
	            printdate DATETIME,
	            p_Emp VARCHAR(10),
	            isprinted INT,
	            countp INT,
	            Courtdate DATETIME,
	            courtnumber VARCHAR(3), --Sabir Khan 7979 07/07/2010 type changed...
	            IsSplit INT,
	            letterid_fk INT,
	            courtid INT,
	            courtname VARCHAR(50),
	            STATUS VARCHAR(15),
	            Email VARCHAR(100),
	            bond VARCHAR(3),
	            trackingnumber VARCHAR(50),
	            eflag VARCHAR(1),
	            Response XML,
	            --Fahad 5071 11/17/2008 (IsemailFlag flag added)
	            IsemailFlag INT
	        )                        
	
	IF (@LType = 2) --Trial Letter
	BEGIN
	    --Fahad 5071 11/17/2008 (IsemailFlag flag added)
	    INSERT INTO @tbl1
	      (
	        ticketid_pk,
	        [name],
	        tooltip,
	        batchdate,
	        b_emp,
	        printdate,
	        BatchID,
	        p_emp,
	        isprinted,
	        IsSplit,
	        letterid_fk,
	        courtid,
	        courtname,
	        STATUS,
	        CourtDate,
	        courtnumber,
	        Email,
	        bond,
	        trackingnumber,
	        eflag,
	        Response,
	        IsemailFlag
	      )
	    SELECT TOP(100) PERCENT 
	           dbo.tbl_hts_batch_trialletter.ticketid_pk,
	           [name] = CASE 
	                         WHEN LEN(
	                                  dbo.tbl_hts_batch_trialletter.LastName +
	                                  ', ' + dbo.tbl_hts_batch_trialletter.FirstName
	                              ) > 15 THEN SUBSTRING(
	                                  dbo.tbl_hts_batch_trialletter.LastName +
	                                  ', ' + dbo.tbl_hts_batch_trialletter.FirstName,
	                                  0,
	                                  15
	                              ) + '...'
	                         ELSE dbo.tbl_hts_batch_trialletter.LastName + ', ' 
	                              +
	                              dbo.tbl_hts_batch_trialletter.FirstName
	                    END,
	           dbo.tbl_hts_batch_trialletter.LastName + ', ' + dbo.tbl_hts_batch_trialletter.FirstName,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           tblUsers_1.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tbl_hts_batch_trialletter.BatchID,
	           dbo.tblUsers.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           0,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           MIN(dbo.tbl_hts_batch_trialletter.courtid),
	           c.ShortName,
	           --dbo.tbl_hts_batch_trialletter.CaseStatus,            
	           ISNULL(cs.ShortDescription, 'N/A') AS ShortDescription,
	           MIN(dbo.tbl_hts_batch_trialletter.CourtDate),
	           dbo.tbl_hts_batch_trialletter.CourtNumber,
	           email = CASE 
	                        WHEN LEN(tbltickets.email) > 17 THEN SUBSTRING(tbltickets.email, 0, 18)
	                             + '...'
	                        ELSE LOWER(ISNULL(tbltickets.email, 'N/A'))
	                   END,	-- Noufil 4044 05/15/2008 if email lenght > 17 then ... will show  
	           CASE 
	                WHEN tbltickets.bondflag = 1 THEN 'Y'
	                ELSE 'N'
	           END AS bond,
	           CASE ISNULL(tblHTSBatchPrintLetter.USPSTrackingNumber, '0')
	                WHEN '0' THEN CASE 
	                                   WHEN (
	                                            SELECT TOP 1 CONVERT(
	                                                       VARCHAR(MAX),
	                                                       uspsresponse.query(
	                                                           'for $s in DeliveryConfirmationV3.0Response return $s/DeliveryConfirmationNumber'
	                                                       )
	                                                   ) + CONVERT(
	                                                       VARCHAR(MAX),
	                                                       uspsresponse.query('for $s in Error return $s/Description')
	                                                   )
	                                            FROM   tblHTSBatchPrintLetter 
	                                                   tbp
	                                            WHERE  BatchID_PK = dbo.tbl_hts_batch_trialletter.BatchID
	                                        ) IS NULL THEN 'N/A'
	                                   ELSE 'Bad Response'
	                              END
	                ELSE tblHTSBatchPrintLetter.USPSTrackingNumber
	           END AS TrackingNumber,
	           ISNULL(tblHTSBatchPrintLetter.EDeliveryFlag, '0') AS eflag,
	           (
	               SELECT TOP 1 CONVERT(
	                          VARCHAR(MAX),
	                          uspsresponse.query(
	                              'for $s in DeliveryConfirmationV3.0Response return $s/DeliveryConfirmationNumber'
	                          )
	                      ) + CONVERT(
	                          VARCHAR(MAX),
	                          uspsresponse.query('for $s in Error return $s/Description')
	                      )
	               FROM   tblHTSBatchPrintLetter tbp
	               WHERE  BatchID_PK = dbo.tbl_hts_batch_trialletter.BatchID
	           ) AS Response,
	           --fahad 5071 11/24/2008 email flag edit--
	           ISNULL(tblHTSBatchPrintLetter.IsEmail, 0) AS IsemailFlag
	    FROM   dbo.tblHTSBatchPrintLetter
	           LEFT OUTER JOIN dbo.tblUsers
	                ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = dbo.tblUsers.EmployeeID
	           LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1
	                ON  dbo.tblHTSBatchPrintLetter.EmpID = tblUsers_1.EmployeeID
	           RIGHT OUTER JOIN dbo.tbl_hts_batch_trialletter
	                ON  dbo.tblHTSBatchPrintLetter.BatchID_PK = dbo.tbl_hts_batch_trialletter.BatchID
	                AND dbo.tblHTSBatchPrintLetter.TicketID_FK = dbo.tbl_hts_batch_trialletter.ticketid_pk
	           INNER JOIN dbo.tbltickets AS tbltickets
	                ON  dbo.tbl_hts_batch_trialletter.ticketid_pk = tbltickets.ticketid_pk
	           INNER JOIN tblcourts c
	                ON  dbo.tbl_hts_batch_trialletter.CourtID = c.CourtID
	           LEFT OUTER JOIN tblcourtviolationstatus cs
	                ON  cs.CourtViolationStatusID = tbl_hts_batch_trialletter.courtviolationstatusid
	                --AND cs.categoryid = 4 -- tahir 6502 09/01/2009 fixed the status bug.

			   -- tahir 6945 11/06/2009 performance tuning...
	           inner join @tmp a on a.batchid_pk = tbl_hts_batch_trialletter.BatchID

	    WHERE  ISNULL(dbo.tblHTSBatchPrintLetter.deleteflag, 0) = 0
	           AND ISNULL(dbo.tblHTSBatchPrintLetter.isprinted, 0) = @isprinted
	           AND tbl_hts_batch_trialletter.ticketid_pk NOT IN (SELECT 
	                                                                    ticketid_pk
	                                                             FROM   
	                                                                    tblticketsflag 
	                                                                    f
	                                                             WHERE  flagid = 
	                                                                    7
	                                                                    AND 
	                                                                        tbl_hts_batch_trialletter.ticketid_pk = 
	                                                                        f.ticketid_pk)
	    GROUP BY
	           dbo.tbl_hts_batch_trialletter.ticketid_pk,
	           dbo.tbl_hts_batch_trialletter.BatchID,
	           dbo.tbl_hts_batch_trialletter.FirstName,
	           dbo.tbl_hts_batch_trialletter.MiddleName,
	           dbo.tbl_hts_batch_trialletter.LastName,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           dbo.tblHTSBatchPrintLetter.DeleteFlag,
	           tblUsers_1.Abbreviation,
	           c.ShortName,
	           dbo.tbl_hts_batch_trialletter.CaseStatus,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           dbo.tblUsers.Abbreviation,
	           dbo.tbl_hts_batch_trialletter.CourtNumber,
	           cs.ShortDescription,
	           tbltickets.email,
	           tbltickets.bondflag,
	           tblHTSBatchPrintLetter.USPSTrackingNumber,
	           tblHTSBatchPrintLetter.EDeliveryFlag,
	           tblHTSBatchPrintLetter.IsEmail
	    ORDER BY
	           dbo.tbl_hts_batch_trialletter.BatchID
	END            
	
	IF (@LType = 3) -- Receipt
	BEGIN
	    INSERT INTO @tbl1
	      (
	        ticketid_pk,
	        [name],
	        tooltip,
	        batchdate,
	        b_emp,
	        printdate,
	        BatchID,
	        p_emp,
	        isprinted,
	        IsSplit,
	        letterid_fk,
	        courtid,
	        courtname,
	        STATUS,
	        CourtDate,
	        courtnumber,
	        email
	      )
	    SELECT TOP(100) PERCENT 
	           dbo.tbl_HTS_Batch_Receipt.ticketid_pk,
	           [name] = CASE 
	                         WHEN LEN(
	                                  dbo.tbl_HTS_Batch_Receipt.LastName + ', ' 
	                                  +
	                                  dbo.tbl_HTS_Batch_Receipt.FirstName
	                              ) > 15 THEN SUBSTRING(
	                                  dbo.tbl_HTS_Batch_Receipt.LastName + ', ' 
	                                  +
	                                  dbo.tbl_HTS_Batch_Receipt.FirstName,
	                                  0,
	                                  15
	                              ) + '...'
	                         ELSE dbo.tbl_HTS_Batch_Receipt.LastName + ', ' +
	                              dbo.tbl_HTS_Batch_Receipt.FirstName
	                    END,
	           dbo.tbl_HTS_Batch_Receipt.LastName + ', ' + dbo.tbl_HTS_Batch_Receipt.FirstName,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           tblUsers_1.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tbl_HTS_Batch_Receipt.BatchID,
	           dbo.tblUsers.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           0,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           MIN(dbo.tbl_HTS_Batch_Receipt.courtid),
	           dbo.tbl_HTS_Batch_Receipt.Location,
	           dbo.tbl_HTS_Batch_Receipt.Status,
	           MIN(dbo.tbl_HTS_Batch_Receipt.currentdateset),
	           dbo.tbl_HTS_Batch_Receipt.ctno,
	           LOWER(ISNULL(tbltickets.email, 'N/A')) AS email
	    FROM   dbo.tblHTSBatchPrintLetter
	           LEFT OUTER JOIN dbo.tblUsers
	                ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = dbo.tblUsers.EmployeeID
	           LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1
	                ON  dbo.tblHTSBatchPrintLetter.EmpID = tblUsers_1.EmployeeID
	           RIGHT OUTER JOIN dbo.tbl_HTS_Batch_Receipt
	                ON  dbo.tblHTSBatchPrintLetter.BatchID_PK = dbo.tbl_HTS_Batch_Receipt.BatchID
	                AND dbo.tblHTSBatchPrintLetter.TicketID_FK = dbo.tbl_HTS_Batch_Receipt.ticketid_pk
	           INNER JOIN dbo.tbltickets AS tbltickets
	                ON  tblHTSBatchPrintLetter.TicketID_FK = tbltickets.ticketid_pk
			   -- tahir 6945 11/06/2009 performance tuning...
	           inner join @tmp a on a.batchid_pk = tbl_hts_batch_receipt.BatchID

	    WHERE  ISNULL(dbo.tblHTSBatchPrintLetter.deleteflag, 0) = 0

	    GROUP BY
	           dbo.tbl_HTS_Batch_Receipt.ticketid_pk,
	           dbo.tbl_HTS_Batch_Receipt.BatchID,
	           dbo.tbl_HTS_Batch_Receipt.FirstName,
	           dbo.tbl_HTS_Batch_Receipt.MiddleName,
	           dbo.tbl_HTS_Batch_Receipt.LastName,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           dbo.tblHTSBatchPrintLetter.DeleteFlag,
	           tblUsers_1.Abbreviation,
	           dbo.tbl_HTS_Batch_Receipt.Location,
	           dbo.tbl_HTS_Batch_Receipt.Status,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           dbo.tblUsers.Abbreviation,
	           dbo.tbl_HTS_Batch_Receipt.ctno,
	           tbltickets.email
	    ORDER BY
	           dbo.tbl_HTS_Batch_Receipt.BatchID
	END 
	
	
	
	--IF LETTER TYPE IS MISSED COURT OR PLED OUT LETTER  OR SET CALL
	-- Noufil 4215 06/27/2008 SOL letter type added
	-- Abid Ali 5359 12/30/2008 LOR letter type added
	-- abid 5563 02/21/2009 for letter of rep removed
	IF (@LType IN (11, 12, 13, 15))
	BEGIN
	    INSERT INTO @tbl1
	      (
	        ticketid_pk,
	        [name],
	        tooltip,
	        batchdate,
	        b_emp,
	        printdate,
	        BatchID,
	        p_emp,
	        isprinted,
	        IsSplit,
	        letterid_fk,
	        courtid,
	        courtname,
	        STATUS,
	        CourtDate,
	        courtnumber,
	        email
	      )
	    SELECT TOP(100) PERCENT 
	           
	           dbo.tbltickets.ticketid_pk,
	           [name] = CASE 
	                         WHEN LEN(dbo.tbltickets.LastName + ', ' + dbo.tbltickets.FirstName)
	                              > 15 THEN SUBSTRING(
	                                  dbo.tbltickets.LastName + ', ' + dbo.tbltickets.FirstName,
	                                  0,
	                                  15
	                              ) + '...'
	                         ELSE dbo.tbltickets.LastName + ', ' + dbo.tbltickets.FirstName
	                    END,
	           dbo.tbltickets.LastName + ', ' + dbo.tbltickets.FirstName,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           tblUsers_1.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tblHTSBatchPrintLetter.BatchID_PK,
	           dbo.tblUsers.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           0,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           MIN(dbo.tblticketsviolations.courtid),
	           dbo.tblcourts.shortname,
	           dbo.tblCourtViolationStatus.ShortDescription,
	           MIN(dbo.tblticketsviolations.courtdatemain),
	           dbo.tblticketsviolations.courtnumbermain,
	           LOWER(ISNULL(tbltickets.email, 'N/A')) AS email
	    FROM   dbo.tblHTSBatchPrintLetter
	           LEFT OUTER JOIN dbo.tblUsers
	                ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = dbo.tblUsers.EmployeeID
	           LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1
	                ON  dbo.tblHTSBatchPrintLetter.EmpID = tblUsers_1.EmployeeID
	           INNER JOIN dbo.tblticketsviolations
	                ON  tblHTSBatchPrintLetter.TicketID_FK = dbo.tblticketsviolations .ticketid_pk
	           INNER JOIN tbltickets
	                ON  dbo.tbltickets.ticketid_pk = dbo.tblticketsviolations.ticketid_pk
	           INNER JOIN tblCourtViolationStatus
	                ON  tblCourtViolationStatus.courtviolationstatusid = dbo.tblticketsviolations .courtviolationstatusidmain
	           INNER JOIN tblcourts
	                ON  tblcourts.courtid = dbo.tblticketsviolations.courtid
			   -- tahir 6945 11/06/2009 performance tuning...
	           inner join @tmp a on a.batchid_pk = dbo.tblHTSBatchPrintLetter.BatchID_PK
	    WHERE  ISNULL(dbo.tblHTSBatchPrintLetter.deleteflag, 0) = 0
	           AND (
	                   (
	                       (@LType IN (11, 12))
	                       AND dbo.tblticketsviolations .courtviolationstatusidmain
	                           = 105
	                   )
	                   OR (@LType IN (13, 15, 6))
	               ) -- Abid Ali 5359 1/15/2009 optimizing query
	    GROUP BY
	           dbo.tbltickets.ticketid_pk,
	           dbo.tblHTSBatchPrintLetter.BatchID_pk,
	           dbo.tbltickets.FirstName,
	           dbo.tbltickets.MiddleName,
	           dbo.tbltickets.LastName,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           dbo.tblHTSBatchPrintLetter.DeleteFlag,
	           tblUsers_1.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           dbo.tblUsers.Abbreviation,
	           dbo.tblticketsviolations.courtid,
	           dbo.tbltickets.ticketid_pk,
	           dbo.tblticketsviolations.courtnumbermain,
	           dbo.tblCourtViolationStatus.ShortDescription,
	           dbo.tblcourts.shortname,
	           tbltickets.email
	    ORDER BY
	           dbo.tblHTSBatchPrintLetter.BatchID_PK
	END 
	
	-- abid 5563 02/21/2009 for letter of rep
	IF (@LType = 6)
	BEGIN
	    INSERT INTO @tbl1
	      (
	        ticketid_pk,
	        [name],
	        tooltip,
	        batchdate,
	        b_emp,
	        printdate,
	        BatchID,
	        p_emp,
	        isprinted,
	        IsSplit,
	        letterid_fk,
	        courtid,
	        courtname,
	        STATUS,
	        --Nasir 5661 04/01/2009 bond column added	        
	        Bond,
	        CourtDate,
	        courtnumber,
	        email
	      )
	    SELECT TOP(100) PERCENT 
	           
	           tlh.TicketID_PK,
	           [name] = CASE 
	                         WHEN LEN(tlh.FullName)
	                              > 15 THEN SUBSTRING(tlh.FullName, 0, 15) +
	                              '...'
	                         ELSE tlh.FullName
	                    END,
	           tlh.FullName AS FullName2,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           tblUsers_1.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tblHTSBatchPrintLetter.BatchID_PK,
	           dbo.tblUsers.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           0,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           MIN(tlh.courtid),
	           dbo.tblcourts.shortname,
	           dbo.tblCourtViolationStatus.ShortDescription,	 
	           --Nasir 5661 04/01/2009 bond column added          
	           CASE 
	                WHEN tlh.IsBond = 1 THEN 'B'
	                ELSE ''
	           END AS bond,
	           MIN(tlh.CourtDate),
	           tlh.courtnumber AS courtnumbermain,
	           LOWER(ISNULL(tlh.email, 'N/A')) AS email
	    FROM   dbo.tblHTSBatchPrintLetter
	           LEFT OUTER JOIN dbo.tblUsers
	                ON  dbo.tblHTSBatchPrintLetter.PrintEmpID = dbo.tblUsers.EmployeeID
	           LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1
	                ON  dbo.tblHTSBatchPrintLetter.EmpID = tblUsers_1.EmployeeID
	           INNER JOIN tblLORBatchHistory tlh
	                ON  dbo.tblHTSBatchPrintLetter.BatchID_PK = tlh.BatchID_FK
	           INNER JOIN tblCourtViolationStatus
	                ON  tblCourtViolationStatus.courtviolationstatusid = tlh.CourtViolationStatus
	           INNER JOIN tblcourts
	                ON  tblcourts.courtid = tlh.courtid	                    
			   -- tahir 6945 11/06/2009 performance tuning...
	           inner join @tmp a on a.batchid_pk = dbo.tblHTSBatchPrintLetter.BatchID_PK
	    WHERE  ISNULL(dbo.tblHTSBatchPrintLetter.deleteflag, 0) = 0

	    GROUP BY
	           tlh.ticketid_pk,
	           tlh.FullName,
	           dbo.tblHTSBatchPrintLetter.BatchID_pk,
	           dbo.tblHTSBatchPrintLetter.IsPrinted,
	           dbo.tblHTSBatchPrintLetter.PrintDate,
	           dbo.tblHTSBatchPrintLetter.BatchDate,
	           dbo.tblHTSBatchPrintLetter.DeleteFlag,
	           tblUsers_1.Abbreviation,
	           dbo.tblHTSBatchPrintLetter.LetterID_FK,
	           dbo.tblUsers.Abbreviation,
	           tlh.courtid,
	           tlh.courtnumber,
	           dbo.tblCourtViolationStatus.ShortDescription,
	           --Nasir 5661 04/01/2009 bond column added	           
	           tlh.IsBond,
	           dbo.tblcourts.shortname,
	           tlh.email
	    ORDER BY
	           dbo.tblHTSBatchPrintLetter.BatchID_PK
	END 
	
	
	SELECT ticketid_pk,
	       batchid,
	       MIN(rowno) AS rowno INTO #tbl2
	FROM   @tbl1
	GROUP BY
	       ticketid_pk,
	       batchid               
	
	DELETE 
	FROM   @tbl1
	WHERE  rowno NOT IN (SELECT rowno
	                     FROM   #tbl2) 
	
	DROP TABLE #tbl2             
	
	
	DECLARE @countp INT            
	SELECT @countp = COUNT(DISTINCT ticketid_pk)
	FROM   @tbl1
	WHERE  isprinted = 1
	
	PRINT @countp 
	PRINT @isprinted            
	
	IF @isprinted = 1
	    IF @courtid <> 0
	        SELECT @countp = COUNT(DISTINCT ticketid_pk)
	        FROM   @tbl1
	        WHERE  isprinted = 1
	               AND printdate BETWEEN @fromdate AND @todate
	               AND b_emp LIKE @emp_id
	               AND courtid = @courtid
	    ELSE
	    BEGIN
	        SELECT @countp = COUNT(DISTINCT ticketid_pk)
	        FROM   @tbl1
	        WHERE  isprinted = 1
	               AND printdate BETWEEN @fromdate AND @todate
	               AND b_emp LIKE @emp_id
	        
	        PRINT @countp
	    END
	ELSE                
	IF @courtid <> 0
	    SELECT @countp = COUNT(DISTINCT ticketid_pk)
	    FROM   @tbl1
	    WHERE  isprinted = 0
	           AND b_emp LIKE @emp_id
	           AND courtid = @courtid
	ELSE
	    SELECT @countp = COUNT(DISTINCT ticketid_pk)
	    FROM   @tbl1
	    WHERE  isprinted = 0
	           AND b_emp LIKE @emp_id            
	
	UPDATE @tbl1
	SET    countp = @countp                 
	
	IF @courtid <> 0
	    DELETE 
	    FROM   @tbl1
	    WHERE  courtid <> @courtid                
	
	IF (@isprinted = 1)
	BEGIN
	    SELECT ticketid_pk,
	           [name],
	           Tooltip,
	           dbo.fn_dateformat(batchdate, 29, '/', ':', 1) AS batchdate,
	           b_emp,
	           CASE 
	                WHEN courtdate IS NULL THEN ''
	                ELSE dbo.fn_dateformat(courtdate, 29, '/', ':', 1)
	           END AS courtdate,
	           courtnumber,
	           CASE 
	                WHEN printdate IS NULL THEN ''
	                ELSE dbo.fn_dateformat(printdate, 29, '/', ':', 1)
	           END AS printdate,
	           BatchID,
	           p_emp,
	           isprinted,
	           countp,
	           courtdate AS trialdate,
	           courtnumber AS trilroom,
	           batchdate AS batchdate1,
	           b_emp AS batchemp,
	           printdate AS printdate1,
	           p_emp AS printemp,
	           isSplit,
	           courtid,
	           STATUS,
	           courtname,
	           email,
	           bond,
	           trackingnumber,
	           eflag,
	           CONVERT(VARCHAR(MAX), Response) AS Response,
	           --Fahad 5071 11/17/2008 (IsemailFlag flag added)
	           IsemailFlag 
	           
	           -- (select top 1 uspsresponse.query('for $s in DeliveryConfirmationV3.0Response return $s/DeliveryConfirmationLabel') from tblHTSBatchPrintLetter tbp where BatchID_PK = t.BatchID) as Response
	           --Response
	           --case bond when 1 then 'Y' else 'N' end
	    FROM   @tbl1 t
	    WHERE  p_emp LIKE @emp_id
	           AND isprinted = 1
	           AND printdate BETWEEN @fromdate AND @todate
-- tahir 6502 09/01/09 order by court date	           
--	    ORDER BY  
--	           [trialdate],
--	           courtid,
--	           STATUS,
--	           [trilroom]
	    ORDER BY CASE @LType WHEN 2 THEN courtdate ELSE courtid END , courtid, STATUS, trialdate, trilroom
	END
	ELSE
	BEGIN
	    SELECT ticketid_pk,
	           [name],
	           Tooltip,
	           dbo.fn_dateformat(batchdate, 29, '/', ':', 1) AS batchdate,
	           b_emp,
	           CASE 
	                WHEN courtdate IS NULL THEN ''
	                ELSE dbo.fn_dateformat(courtdate, 29, '/', ':', 1)
	           END AS courtdate,
	           courtnumber,
	           CASE 
	                WHEN printdate IS NULL THEN ''
	                ELSE dbo.fn_dateformat(printdate, 29, '/', ':', 1)
	           END AS printdate,
	           BatchID,
	           p_emp,
	           isprinted,
	           countp,
	           courtdate AS trialdate,
	           courtnumber AS trilroom,
	           batchdate AS batchdate1,
	           b_emp AS batchemp,
	           printdate AS printdate1,
	           p_emp AS printemp,
	           isSplit,
	           courtid,
	           STATUS,
	           courtname,
	           --BatchID,      
	           email,
	           bond,
	           trackingnumber,
	           eflag,
	           CONVERT(VARCHAR(MAX), Response) AS Response,
	           --Fahad 5071 11/17/2008 (IsemailFlag flag added)
	           IsemailFlag 
	           
	           --case bond when 1 then 'Y' else 'N' end
	    FROM   @tbl1 t
	    WHERE  b_emp LIKE @emp_id
	           AND isprinted = 0
-- tahir 6502 09/01/09 order by court date	           
--	    ORDER BY 
--			   [trialdate],
--	           courtid,
--	           STATUS,
--	           [trilroom]
	    ORDER BY CASE @LType WHEN 2 THEN courtdate ELSE courtid END , courtid, STATUS, trialdate, trilroom
	END
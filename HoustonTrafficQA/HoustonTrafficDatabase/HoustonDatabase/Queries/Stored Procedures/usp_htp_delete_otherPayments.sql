--****************************************************************************
--Created By   : Afaq Ahmed
--Creation Data: 11/02/2010
--Task ID :8213
--Parameter List : 
--@Id: InvoiceNumber
--Return Column: N/A
--Business Logic : Delete the record from Other payments report.
--****************************************************************************

Create PROCEDURE usp_htp_delete_otherPayments
@Id INT 
 AS 
 
 UPDATE tblticketspayment SET isRemoved = 1 WHERE InvoiceNumber_PK = @Id
 
 GO
 
 GRANT EXECUTE ON usp_htp_delete_otherPayments TO webuser_hts
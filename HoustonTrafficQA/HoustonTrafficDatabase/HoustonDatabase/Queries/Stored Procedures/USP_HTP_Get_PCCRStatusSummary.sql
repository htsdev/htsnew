﻿/**********************************************************************

Created By: Muhammad Nasir

Business Login: this procedure is used in HTP /Activities /Traffic Alert application to get 
				the count of Alert status 

List of Parameters:
	@sdate:				INPUT PARAMETER TO FILTER RECORDS ON A DATE RANGE....                  
	@edate:				INPUT PARAMETER TO FILTER RECORDS ON A DATE RANGE....                  
	
List of Columns:
	statusdescription:	Status description
	Counts:				Counts of each status
	


**********************************************************************/

create procedure dbo.USP_HTP_Get_PCCRStatusSummary
(
@sdate as datetime,            
@edate as datetime  
)
as
/*
Declare @sdate as datetime
set @sdate='7-1-2008'          
Declare  @edate as datetime
  set @edate='7-31-2008'
*/

select ps.statusdescription, isnull(count(tp.pccrStatusID),0) as Counts from tblpccrstatus PS
left outer join tblTicketsArchivePCCR TP on  PS.statusID=tp.pccrstatusid and TP.InsertDate between  @sdate and @edate
--where 
group by pccrStatusID,ps.statusdescription

union 

select 'Hired',isnull(count(*),0) as Counts from tblTicketsArchivePCCR
where issignup=1 and InsertDate between  @sdate and @edate

go

grant exec on dbo.USP_HTP_Get_PCCRStatusSummary to dbr_webuser
go
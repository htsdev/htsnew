﻿ /******   
Created BY Asad Ali 
Business Logic : This procedure is used to get the court ALR Court information.  
TFS ID: 8153
 
******/

 Create PROCEDURE USP_HTP_GET_AssociatedALRCourts
 As
 
 SELECT tc.Courtid, tc.CourtName  FROM tblCourts tc 
WHERE tc.ALRProcess=1 AND tc.InActiveFlag =0


go

grant exec on dbo.USP_HTP_GET_AssociatedALRCourts to dbr_webuser 
go 

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Delete_ScanDoc]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Delete_ScanDoc]
GO

CREATE PROCEDURE [dbo].[usp_HTS_Delete_ScanDoc]  
  
@docid  int,  
@RecType int,  
@DocNum  int,  
@DocExt  varchar(10)  
          
AS             
          
declare @typeofdoc varchar(50)      
declare @tid int      
declare @empid int      
declare @doctypeid int
declare @subdoctypeid int      
declare @Description varchar(50)      
    
      
select @tid=TicketID, @empid=EmployeeID, @doctypeid=doctypeid,@subdoctypeid=isnull(subdoctypeid,0),@Description=ResetDesc from tblscanbook where scanbookid=@docid      
select @typeofdoc=doctype from tbldoctype where doctypeid=@doctypeid      
if @subdoctypeid<>0  
 set @typeofdoc=@typeofdoc+' - ' + (select subdoctype from tblsubdoctype where subdoctypeid=@subdoctypeid)              
      
--Delete from tblscandata where doc_id=@docid     
--Delete from tblscanpic where ScanBookID=@docid     
--Delete from tblscanbook where ScanBookID=@docid            
update tblscanbook set IsDeleted=1 where ScanBookID=@docid   
      
insert into tblTicketsNotes (TicketID,Subject,EmployeeID) values (@tid,  
'<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+Convert(varchar,@docid)+'&RecType='+Convert(varchar,@RecType)+'&DocNum='+Convert(varchar,@DocNum)+'&DocExt='+@DocExt+''');void('''');"''>Scanned document deleted - '+@typeofdoc+' - '
+@Description + '</a>',  
@empid)      
  
  
        
          
      
         
      
      
      
    
    
    
    
  
  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


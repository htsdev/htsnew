﻿ /******  
* Created By :	  Saeed Ahmed.
* Create Date :   11/10/2010 
* Task ID :		  8501
* Business Logic :  This procedure is used to update patient status information.
* List of Parameter :
* 					@PatientId ,
					@PatientStatusId ,
					@SourceID ,
					@OtherSource 
* Column Return :     
******/
ALTER PROCEDURE dbo.USP_HTP_UpdatePatientInfoStatus
	@PatientId INT,
	@PatientStatusId INT,
	@SourceID INT,
	@OtherSource VARCHAR(200),	
	@referralAttorneyId INT=0, --Saeed 8585 12/03/2010, add parameter for referral attroneys.
	@referralAttorneyName VARCHAR(200)='' --Saeed 8585 12/03/2010, add parameter for referral attroneys.
AS
IF @SourceID=4
BEGIN
	UPDATE patient
	SET    PatientStatusId = @PatientStatusId,
	       SourceID = @SourceID,
	       OtherSource = @OtherSource
	WHERE  Id = @PatientId
	-- if patient status is closed then move it to inactive report.
	IF @PatientStatusId=3
	BEGIN
		UPDATE patient SET IsActive =0 WHERE Id = @PatientId
	END
END
ELSE
BEGIN
	UPDATE patient
	SET    PatientStatusId = @PatientStatusId,
	       SourceID = @SourceID,
	       OtherSource = NULL
	WHERE  Id = @PatientId
	-- if patient status is closed then move it to inactive report.
	IF @PatientStatusId=3
	BEGIN
		UPDATE patient SET IsActive =0 WHERE Id = @PatientId
	END
END

--Saeed 8585 12/03/2010 code to insert new attorneys if they don't exist in the system.
IF ISNULL(@referralAttorneyName,'')	<>''
	BEGIN
		IF NOT EXISTS(SELECT [name] FROM ReferralAttorney WHERE [name]=@referralAttorneyName)
		BEGIN
			INSERT INTO ReferralAttorney([name]) VALUES(@referralAttorneyName)
			DECLARE @Newid INT
			SET @Newid=SCOPE_IDENTITY()
			UPDATE patient SET ReferralAttorneyId=@Newid WHERE id=@PatientId
			
		END
	END
--Saeed 8585 12/03/2010 update referral attorneys in patient table.
ELSE IF ISNULL(@referralAttorneyId,0)<>0
	BEGIN
		UPDATE patient SET ReferralAttorneyId=@referralAttorneyId WHERE id=@PatientId
	END	

GO
GRANT EXECUTE ON dbo.USP_HTP_UpdatePatientInfoStatus TO dbr_webuser
GO



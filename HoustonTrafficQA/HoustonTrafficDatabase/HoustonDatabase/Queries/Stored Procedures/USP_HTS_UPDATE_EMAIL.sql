SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UPDATE_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UPDATE_EMAIL]
GO

CREATE PROCEDURE USP_HTS_UPDATE_EMAIL  
@LastName varchar(50),    
@DLNumber varchar(20),  
@DOB varchar(15),  
@PhoneNumber varchar(25),  
@EmailAddress varchar(50)  
as    
  
declare @sql varchar(7999)    
    
set @sql = ' UPDATE tbltickets  
set email = ''' +@EmailAddress + ''' WHERE 1=1 '  
  
if @DLNumber <> ''  
 set @sql = @sql + ' and DLNumber = ''' + @DLNumber + ''''    
  
if @DOB <> ''  
 set @sql = @sql + ' and DateDiff(day,DOB,convert(datetime,'''+@DOB+'''))=0 '  
  
if @PhoneNumber <> ''  
 set @sql = @sql + ' and (Contact1 = '''+@PhoneNumber+''' OR Contact2 = '''+@PhoneNumber+''' OR Contact3 = '''+@PhoneNumber+''') '  
  
  
  
--print (@sql)  
exec(@sql)  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


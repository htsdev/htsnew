﻿ /*
* Created By : Waqas 5864 06/23/2009 
* Business Logic : This procedure Checks if client has arrest date is more than 15 days old.
*	 
*	Parameter: @ticketid
*	Output	:	Count
*/

CREATE PROCEDURE [dbo].[usp_htp_CheckALRArrestDateinDaysRange]
	@ticketid INT
AS
	SELECT COUNT(ttv.casenumassignedbycourt)
	FROM   tblTicketsViolations ttv
	INNER JOIN tblTickets tt ON tt.TicketID_PK = ttv.TicketID_PK
	WHERE  ttv.TicketID_PK = @ticketid
	       AND ttv.ViolationNumber_PK = 16159
	       AND DATEDIFF(DAY, ttv.ArrestDate, GETDATE() - 15) > 0
	       AND tt.Activeflag=0
	       
GO
GRANT EXECUTE ON [dbo].[usp_htp_CheckALRArrestDateinDaysRange] TO dbr_webuser

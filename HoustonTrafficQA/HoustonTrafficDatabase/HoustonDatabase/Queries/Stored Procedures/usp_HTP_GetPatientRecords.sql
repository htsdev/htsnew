﻿ /******    
* Created By :   Ozair.  
* Create Date :   11/05/2010   
* Task ID :    8501  
* Business Logic :  This procedure is used to get patient reords
* List of Parameter : @Name,  
     : @City,  
     : @State,  
     : @ZipCode  
* Column Return :    Auto generated id of the table.   
******/  
ALTER PROCEDURE dbo.usp_HTP_GetPatientRecords
	@PatientId INT
AS
	SELECT ISNULL(p.SurgeonRequestedDate,'01/01/1900') AS SurgeonRequestedDate,
       ISNULL(p.SurgeonReceivedDate,'01/01/1900') AS SurgeonReceivedDate,
       ISNULL(p.SurgeonSentToClientDate,'01/01/1900') AS SurgeonSentToClientDate,
       ISNULL(p.HospitalRequestedDate,'01/01/1900') AS HospitalRequestedDate,
       ISNULL(p.HospitalReceivedDate,'01/01/1900') AS HospitalReceivedDate,
       ISNULL(p.HospitalSentToClientDate,'01/01/1900') AS HospitalSentToClientDate,
       ISNULL(p.GPRequestedDate,'01/01/1900') AS GPRequestedDate,
       ISNULL(p.GPReceivedDate,'01/01/1900') AS GPReceivedDate,
       ISNULL(p.GPSentToClientDate,'01/01/1900') AS GPSentToClientDate,
       ISNULL(p.MiscRequestedDate,'01/01/1900') AS MiscRequestedDate,
       ISNULL(p.MiscReceivedDate,'01/01/1900') AS MiscReceivedDate,
       ISNULL(p.MiscSentToClientDate,'01/01/1900') AS MiscSentToClientDate,
       p.MiscRecords
FROM   Patient p
	WHERE  p.Id = @PatientId
GO

GRANT EXECUTE ON dbo.usp_HTP_GetPatientRecords TO dbr_webuser
GO
﻿ /*
* Created By: Babar Ahmad
* Task:		  9727
* Date:		  10/14/2011
* 
* Business Logic: This SP deletes record from Non-updated Reset Cases and marks the date.
* 
* Input Parameters: 
*				@TicketID
*				@TicketViolationID
* 
*/


CREATE PROCEDURE [dbo].[USP_HTP_Delete_NonUpdatedWaitingCases]
@TicketID INT,
@TicketViolationID INT

AS
BEGIN
	UPDATE tblTicketsViolations
	SET
		IsNonUpdatedWaitingRemoved = 1,
		NonUpdatedWaitingRemovedDate = GETDATE()
	WHERE
		TicketID_PK = @TicketID AND TicketsViolationID = @TicketViolationID
	
END
-- Babar Ahmad 9727 11/02/2011 Grant command added.
GO 
GRANT EXECUTE ON [dbo].[USP_HTP_Delete_NonUpdatedWaitingCases] TO dbr_webuser
GO


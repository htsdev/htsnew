﻿ /*
* Waqas 5864 06/24/2009
* Business Logic : This procedure is used to get the attorney names associated with covering firm with all violations of the case.
* Parameter : @TicketID
* Output : AttorneyName, Email
*/
Alter PROCEDURE dbo.usp_htp_get_AttorneysByTicketID
@TicketID INT 
AS
BEGIN
	
-- Afaq 8521 01/04/2011 Remove cell number.
SELECT DISTINCT  
ISNULL(tf.AttorenyFirstName + ' ' + tf.AttorneyLastName, tf.FirmName) AS AttorneyName,
ISNULL(TF.AttorneyEmailAddress, '') AS Email
  FROM tblticketsviolations t
  INNER JOIN tblFirm tf ON t.CoveringFirmID = tf.FirmID
  LEFT OUTER JOIN tblUsers tu ON tf.FirmID = tu.FirmID
WHERE t.TicketID_PK = @TicketID
--Nasir 6619 11/25/2009 not select whose case description is ALR hearing
and t.ViolationNumber_PK != '16159'

END




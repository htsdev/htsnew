﻿/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:30:02 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to get attribute type based on supplied attribute id.
*					: 
* Parameter List	
  @AttributeId		: Attribute Id.
* 
* USP_ReportSetting_IsReportSettingExist 52,3051,1
*/ 
CREATE PROC [dbo].[USP_ReportSetting_GetAttributeType]
@AttributeId INT
AS
SELECT ra.AttributeType
FROM   ReportAttribute ra
WHERE  ra.AttributeID = @AttributeId
GO

GRANT EXECUTE ON [dbo].[USP_ReportSetting_GetAttributeType] TO dbr_webuser
GO

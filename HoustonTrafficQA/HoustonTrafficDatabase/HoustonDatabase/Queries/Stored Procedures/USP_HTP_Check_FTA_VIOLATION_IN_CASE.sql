

  
CREATE procedure USP_HTP_Check_FTA_VIOLATION_IN_CASE
@ticketid int  
as  
Select dbo.fn_CheckFTAViolationInCase(@ticketid)  



GO


GRANT EXEC ON USP_HTP_Check_FTA_VIOLATION_IN_CASE  TO dbr_webuser

GO


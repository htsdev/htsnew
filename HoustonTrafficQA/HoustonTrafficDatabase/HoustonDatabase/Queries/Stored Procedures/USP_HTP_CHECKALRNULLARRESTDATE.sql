/*
* Created By : Noufil 05/15/2009 
* Business Logic : This procedure Checks if client has null arrest date with ALR violation
*/

CREATE PROCEDURE [dbo].[USP_HTP_CHECKALRNULLARRESTDATE]
	@ticketid INT
AS
	SELECT COUNT(ttv.casenumassignedbycourt)
	FROM   tblTicketsViolations ttv
	INNER JOIN tblTickets tt ON tt.TicketID_PK = ttv.TicketID_PK
	WHERE  ttv.TicketID_PK = @ticketid
	       AND ttv.ViolationNumber_PK = 16159
	       AND ttv.ArrestDate IS NULL AND tt.Activeflag=0
	       
GO
GRANT EXECUTE ON [dbo].[USP_HTP_CHECKALRNULLARRESTDATE] TO dbr_webuser

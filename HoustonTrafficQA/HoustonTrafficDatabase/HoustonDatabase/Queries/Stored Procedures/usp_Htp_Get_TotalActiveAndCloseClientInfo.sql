USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Htp_Get_TotalActiveAndCloseClientInfo]    Script Date: 02/10/2012 15:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Abbas Qamar
-- Work Item ID: 10031 
-- Create date: 02/07/2012
-- Business Logic: This stored procedure is used to display Total number of Closes and active cases and list of active/closed clients for both dallas and houston
-- Input Parameters: 
-- @DBID: 1 - for Houston database, 2 - for Dallas database.
-- =============================================
-- [dbo].[usp_Htp_Get_TotalActiveAndCloseClientInfo] 2

ALTER PROCEDURE [dbo].[usp_Htp_Get_TotalActiveAndCloseClientInfo] 
	@DBID INT
AS
	
	DECLARE @totalDisposeCases AS INT
	DECLARE @totalActiveCases AS INT
	
	DECLARE @totalDisposeClientsUnique AS INT
	DECLARE @totalDisposeClientsIndividual AS INT
	
	DECLARE @totalActiveClientsUnique AS INT
	DECLARE @totalActiveClientsIndividual AS INT
	
	
	
	IF @DBID = 2
	BEGIN
		
		
		-----------------------------------------------------
		--------Total Number of disposed cases
		-----------------------------------------------------
	    SELECT @totalDisposeCases = COUNT(DISTINCT tblTicketsViolations.TicketsViolationID)
	    FROM   dallastraffictickets.dbo.tblCourtViolationStatus
	           INNER JOIN dallastraffictickets.dbo.tblTicketsViolations
	                ON  tblCourtViolationStatus.CourtViolationStatusID = 
	                    tblTicketsViolations.CourtViolationStatusIDmain
	           INNER JOIN dallastraffictickets.dbo.tbltickets t
	                ON  t.TicketID_PK = tblTicketsViolations.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblTicketsPayment tp
	                ON  tp.TicketID = t.TicketID_PK
	    WHERE  (tblCourtViolationStatus.CategoryID = 50)
	           AND ISNULL(t.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
			   AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			   AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
			   and t.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
	    
	    
	    -----------------------------------------------------
	    -------- Total number of active cases.
	    -----------------------------------------------------
	    
	    SELECT @totalActiveCases = COUNT(DISTINCT tblTicketsViolations.TicketsViolationID)
	    FROM   dallastraffictickets.dbo.tblCourtViolationStatus
	           INNER JOIN dallastraffictickets.dbo.tblTicketsViolations
	                ON  tblCourtViolationStatus.CourtViolationStatusID = 
	                    tblTicketsViolations.CourtViolationStatusIDmain
	           INNER JOIN dallastraffictickets.dbo.tbltickets t
	                ON  t.TicketID_PK = tblTicketsViolations.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblTicketsPayment tp
	                ON  tp.TicketID = t.TicketID_PK
	    WHERE  (tblCourtViolationStatus.CategoryID <> 50)
	           AND ISNULL(t.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			   AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
			   and t.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
	    
	    
	    -----------------------------------------------------
	    ------- Total number of disposed clients. Unique
	    -----------------------------------------------------
	    
	    
	    
	    SELECT DISTINCT tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #temp2
	    FROM   dallastraffictickets.dbo.tblTickets AS tt
	           INNER JOIN dallastraffictickets.dbo.tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID = 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
	    -------------------
	    
	    SELECT @totalDisposeClientsUnique = COUNT(*)
	    FROM   #temp2
	    
	    DROP TABLE #temp2
	    
	    
	    -----------------------------------------------------
	    ------- Total number of disposed clients. Individual
	    -----------------------------------------------------
	    
	    
	    SELECT DISTINCT tt.TicketID_PK,
	           tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #tempDisposedClientsIndividual
	    FROM   dallastraffictickets.dbo.tblTickets AS tt
	           INNER JOIN dallastraffictickets.dbo.tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID = 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
	    
	    SELECT @totalDisposeClientsIndividual = COUNT(*)
	    FROM   #tempDisposedClientsIndividual
	    
	    DROP TABLE #tempDisposedClientsIndividual
	    
	    
	    
	    ---------------------------------------------
	    -- total number of active clients. Unique
	    ---------------------------------------------
	    ------ Final ----
	    SELECT DISTINCT tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #temp1
	    FROM   dallastraffictickets.dbo.tblTickets AS tt
	           INNER JOIN dallastraffictickets.dbo.tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID <> 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
	    
	    SELECT @totalActiveClientsUnique = COUNT(*)
	    FROM   #temp1
	    
	    DROP TABLE #temp1 
	          
	           
	            
	    ---------------------------------------------
	    -- total number of active clients. Individual
	    ---------------------------------------------
	    ------ Final ----
	    SELECT DISTINCT tt.TicketID_PK, tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #tempActiveIndividual
	    FROM   dallastraffictickets.dbo.tblTickets AS tt
	           INNER JOIN dallastraffictickets.dbo.tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN dallastraffictickets.dbo.tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID <> 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (78608,158339,158342,178799,179085,179086,166209)
	    SELECT @totalActiveClientsIndividual = COUNT(*)
	    FROM   #tempActiveIndividual
	    
	    DROP TABLE #tempActiveIndividual
	    -------------------
	    
	    CREATE TABLE #result	
	    (
			Fields VARCHAR(200) NOT NULL,
			Total_Count int NOT NULL
	    
	    )
	    
	    INSERT INTO #result ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Close Cases', 	@totalDisposeCases  )
		INSERT INTO #result ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Active Cases', 	@totalActiveCases )
 
		INSERT INTO #result ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Dispose Clients Unique', 	@totalDisposeClientsUnique )
		INSERT INTO #result ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Dispose Clients Individual', 	@totalDisposeClientsIndividual )
		
		INSERT INTO #result ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Active Clients Unique', 	@totalActiveClientsUnique )
		INSERT INTO #result ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Active Clients Individual', 	@totalActiveClientsIndividual )
          
	           SELECT * FROM #result
	           DROP TABLE #result
	END
	
	ELSE
		
	----------------------------------------------------------------------------------------	
	-----------------------------Houston Traffic System -------------------------------------
	----------------------------------------------------------------------------------------	
	BEGIN
		
		-----------------------------------------------------
		--------Total Number of disposed cases
		-----------------------------------------------------
	    
	    SELECT @totalDisposeCases = COUNT(DISTINCT tblTicketsViolations.TicketsViolationID)
	    FROM   tblCourtViolationStatus
	           INNER JOIN tblTicketsViolations
	                ON  tblCourtViolationStatus.CourtViolationStatusID = 
	                    tblTicketsViolations.CourtViolationStatusIDmain
	           INNER JOIN tbltickets t
	                ON  t.TicketID_PK = tblTicketsViolations.TicketID_PK
	           INNER JOIN tblTicketsPayment tp
	                ON  tp.TicketID = t.TicketID_PK
	    WHERE  (tblCourtViolationStatus.CategoryID = 50)
	           AND ISNULL(t.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			   AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
	           and t.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
				   192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
				   340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)
	    
	    -----------------------------------------------------
	    -------- Total number of active cases.
	    -----------------------------------------------------
	    
	    SELECT @totalActiveCases = COUNT(DISTINCT tblTicketsViolations.TicketsViolationID)
	    FROM   tblCourtViolationStatus
	           INNER JOIN tblTicketsViolations
	                ON  tblCourtViolationStatus.CourtViolationStatusID = 
	                    tblTicketsViolations.CourtViolationStatusIDmain
	           INNER JOIN tbltickets t
	                ON  t.TicketID_PK = tblTicketsViolations.TicketID_PK
	           INNER JOIN tblTicketsPayment tp
	                ON  tp.TicketID = t.TicketID_PK
	    WHERE  (tblCourtViolationStatus.CategoryID <> 50)
	           AND ISNULL(t.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(t.Firstname,'') + ISNULL(t.Lastname,'')) > 0
			   AND t.Firstname NOT LIKE 'Test' AND t.Lastname NOT LIKE 'Test'
	           and t.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
				   192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
				   340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)
	    
	    -----------------------------------------------------
	    ------- Total number of disposed clients Unique
	    -----------------------------------------------------
	    
	    SELECT DISTINCT tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #temp3
	    FROM   tblTickets AS tt
	           INNER JOIN tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID = 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
				   192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
				   340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)
	    -------------------
	    SELECT @totalDisposeClientsUnique = COUNT(*)
	    FROM   #temp3
	    
	    DROP TABLE #temp3
	    
	    
	    
	    -----------------------------------------------------
	    ------- Total number of disposed clients Individual
	    -----------------------------------------------------
	    
	    SELECT DISTINCT tt.TicketID_PK, tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #tempDallasDisposedIndividual
	    FROM   tblTickets AS tt
	           INNER JOIN tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID = 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
				   192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
				   340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)
	    -------------------
	    SELECT @totalDisposeClientsIndividual = COUNT(*)
	    FROM   #tempDallasDisposedIndividual
	    
	    DROP TABLE #tempDallasDisposedIndividual
	    
	    	   
	   
		---------------------------------------------
	    -- total number of active clients unique
	    ---------------------------------------------
	    ------- Final ----
	    SELECT DISTINCT tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #temp4
	    FROM   tblTickets AS tt
	           INNER JOIN tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID <> 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
			   and tt.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
				   192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
				   340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)
	    SELECT @totalActiveClientsUnique = COUNT(*)
	    FROM   #temp4
	    
	    DROP TABLE #temp4
	    
	    ---------------------------------------------
	    -- total number of active clients Individual
	    ---------------------------------------------
	    
	    SELECT DISTINCT tt.TicketID_PK, tt.Firstname,
	           tt.Lastname,
	           tt.DOB,
	           tt.Zip INTO #tempDallasActiveIndividual
	    FROM   tblTickets AS tt
	           INNER JOIN tblTicketsViolations AS ttv
	                ON  tt.TicketID_PK = ttv.TicketID_PK
	           INNER JOIN tblTicketsPayment tp
	                ON  tp.TicketID = tt.TicketID_PK
	           INNER JOIN tblCourtViolationStatus tcvs
	                ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	    WHERE  tcvs.CategoryID <> 50
	           AND ISNULL(tt.Activeflag, 0) = 1
	           AND ISNULL(tp.PaymentVoid, 0) = 0
	           AND LEN(ISNULL(tt.Firstname,'') + ISNULL(tt.Lastname,'')) > 0
			   AND tt.Firstname NOT LIKE 'Test' AND tt.Lastname NOT LIKE 'Test'
	           and tt.ticketid_pk not in (4485,6362,6774,8271,8810,9090,9212,13446,107104,141748,180922,184109,190178,190179,191713,
				   192536,204470,209388,216998,218215,218216,236436,241094,242807,256869,75545,294170,299221,301694,322718,332203,
				   340280,394850,395013,395237,398204,275545,194549,242817,380322,380323)
	    SELECT @totalActiveClientsIndividual = COUNT(*)
	    FROM   #tempDallasActiveIndividual
	    
	    DROP TABLE #tempDallasActiveIndividual
	    
	    
	    -------------------------------------------------------
	    ------------------- Results ----------------------------
	    -------------------------------------------------------

		CREATE TABLE #result1	
	    (
			Fields VARCHAR(200) NOT NULL,
			Total_Count int NOT NULL
	    
	    )
	    
	    INSERT INTO #result1 ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Close Cases', 	@totalDisposeCases  )
		INSERT INTO #result1 ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Active Cases', 	@totalActiveCases )
 
		INSERT INTO #result1 ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Dispose Clients Unique', 	@totalDisposeClientsUnique )
		
		INSERT INTO #result1 ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Dispose Clients Individual', 	@totalDisposeClientsIndividual )
		
		INSERT INTO #result1 ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Active Clients Unique', 	@totalActiveClientsUnique )
		INSERT INTO #result1 ( 	Fields, 	Total_Count )
		VALUES ( 	'Total Number of Active Clients Individual', 	@totalActiveClientsIndividual) 
	    


	           SELECT * FROM #result1
	           DROP TABLE #result1
	END


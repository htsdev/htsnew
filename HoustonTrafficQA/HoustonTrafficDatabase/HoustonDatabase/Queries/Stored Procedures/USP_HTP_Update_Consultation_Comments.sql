﻿

/******           
Created by:  Muhammad Nasir        
Business Logic : This Procedure Update consultation Comments and Consultation Status on ticket number basis.
          
List of Parameters:           
 @TicketID_PK   : Client Ticket Number 
 @empid : Employee Id by which employee login
 @ConsultaionComments : Comment given by client when ask fro consultation
 @ConsultationStatusID : Consultation Status on when state the request

         
******/   

--Nasir 5256 12/12/2008  

Create PROCEDURE  [dbo].[USP_HTP_Update_Consultation_Comments]      
      
@TicketID_PK  int,      
@empid INT,      
@ConsultaionComments varchar(MAX),   -- Adil - For 2585    
@ConsultationStatusID INT    
      
as      
      
set @ConsultaionComments = ltrim(rtrim(@ConsultaionComments))    
  
print @ConsultaionComments  
      
if ((right(@ConsultaionComments,1) <> ')')  )   --if comments are updated  
 begin                     
 declare @empnm as varchar(12)                    
 select @empnm = abbreviation from tblusers where employeeid = @empid    
--print @empnm                  
if @empnm is null    
 begin    
  Set @empnm = 'N/A'    
 end    
 if len(@ConsultaionComments) > 0    
--print len(@GeneralComments)  
  set @ConsultaionComments = @ConsultaionComments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  upper(@empnm) + ')' + ' '       
    --print @GeneralComments  
 UPDATE tbl_hts_ConsultationComments  
 SET    
  CommentsDate = GETDATE(),  
  Comments =  @ConsultaionComments,  
  CallBackStatus = @ConsultationStatusID  
      
 WHERE TicketID_FK = @TicketID_PK   
  
end      
   
        
  
Go
grant exec on dbo.[USP_HTP_Update_Consultation_Comments] to dbr_webuser
GO
      
  
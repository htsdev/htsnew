ALTER PROCEDURE [dbo].[usp_dts_get_tickets_rpt_batch]  --'3087,3170,3219,'  
 @TicketID varchar(max)    ,
 @CertifiedMailNumber varchar(50)= null
AS    
BEGIN    
 SET NOCOUNT ON;    
    
  declare @tbltickets  table(id int IDENTITY PRIMARY KEY,ticketId int)          
  insert into @tbltickets select * from dbo.sap_dates(@TicketID)     
    Select t.ticketId_Pk,t.bondflag,t.LastName + ' ' + t.FirstName as [ClientName],(Select top 1 courtid from tblticketsviolations where ticketId_pk = t.ticketId_Pk) as courtId, (Select top 1 courtviolationstatusidmain 
	from tblticketsviolations where ticketId_pk = t.ticketId_Pk) as courtviolationstatus, (select dbo.fn_getcausenumbyticketid(t.ticketid_pk)) as causenumber, @CertifiedMailNumber as certifiedMailNumber   
  from  
  tbltickets as t inner join     
  @tbltickets as tt on t.ticketId_Pk = tt.ticketId   
	
  --WHERE  
END  
go
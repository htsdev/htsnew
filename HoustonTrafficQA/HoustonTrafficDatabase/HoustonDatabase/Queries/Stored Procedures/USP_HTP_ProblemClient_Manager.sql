﻿/*

Created By : Agha Usman 
List Of Parameters: 
TicketID
@FirstName
@LastName
@MiddleName
@DOB
@isManual
--- Old Values from here (Optional)
@dFirstName 
@dLastName
@dMiddleName
@dDOB datetime

See the case condition and decide accordingly about the process
*/
ALTER PROCEDURE [dbo].[USP_HTP_ProblemClient_Manager] 
	-- Add the parameters for the stored procedure here
	@TicketID int,
	@FirstName varchar(20), 
	@LastName varchar(20),
	@MiddleName varchar(6),
	@DOB datetime,
	@isManual bit = null,
	@dFirstName varchar(20) = null, 
	@dLastName varchar(20)= null,
	@dMiddleName varchar(6)= null,
	@dDOB datetime= null
	


AS
SET NOCOUNT ON;
BEGIN

	-- Updating Problem Client Lookup  (Manual Process)



--insert into tst 
--select convert(varchar(20),@TicketID) + convert(varchar(20),@isManual) + isnull(@dFirstName,'no val')

declare @desc varchar(100)                                                   
select @desc=description from tbleventflags WITH(NOLOCK) where flagid_pk=13  -- Haris Ahmed 10290 07/04/2012 Fix bug for Problem Client (No Hire) Flag                                                   


if @isManual = 1
begin 
	-- Agha Usman 4497  07/31/2008 - Remove Middle Name Check
	declare @problemclientId int
	select @problemclientId = id from problemclientlookup WITH(NOLOCK)	
	where FirstName = isnull(@dFirstName,'') and LastName = isnull(@dLastName,'') and datediff(day,DOB,isnull(@dDOB ,'1-1-1900')) = 0
	if (@problemclientId is not null) 
	begin 
		update problemclientlookup 	
		set FirstName = @Firstname,
		Lastname = @Lastname,
		DOB = @DOB
		where id = @problemclientId
	end
end 

-- Updating Problem Client Lookup  (Automatic Process)

if @isManual = 0
begin
	-- Agha Usman 4497  07/31/2008 - Remove Middle Name Check 
	declare @problemclientIda int
	select @problemclientIda = id from problemclientlookup WITH(NOLOCK) where FirstName = isnull(@Firstname,'') and LastName = isnull(@Lastname,'') and  datediff(day,DOB,isnull(@DOB ,'1-1-1900')) = 0
	if (@problemclientIda is null) 
	begin 
			
     	delete from tblTicketsFlag where ticketId_Pk = @TicketID and FlagId = 13 -- Haris Ahmed 10290 07/04/2012 Fix bug for Problem Client (No Hire) Flag

		update tbltickets set isProblemClientManual= null where ticketId_pk = @TicketID

		insert into tblticketsnotes(ticketid,subject,employeeid,Notes) 
		select @TicketID,'Flag - ' + @desc + '-Removed'  ,3992,null
	end
end


-- Updating Problem Client Lookup  (Simple Name change)

if @isManual is null
begin 
	-- Agha Usman 4497  07/31/2008 - Remove Middle Name Check
	declare @problemclientIdb int
	select @problemclientIdb = id from problemclientlookup WITH(NOLOCK) where FirstName = isnull(@Firstname,'') and LastName = isnull(@Lastname,'')  and datediff(day,DOB,isnull(@DOB ,'1-1-1900')) = 0
	
	if (@problemclientIdb is not null) 
	begin 
		insert into tblticketsflag(ticketid_pk,flagid,empid)
		select @TicketID,13,3992 -- Haris Ahmed 10290 07/04/2012 Fix bug for Problem Client (No Hire) Flag
		
		update tbltickets set isProblemClientManual= 0 where ticketId_pk = @TicketID

		insert into tblticketsnotes(ticketid,subject,employeeid,Notes) 
		select @TicketID,'Flag - ' + @desc ,3992,null
	end
end
	
END





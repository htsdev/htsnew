﻿USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_JpAppFaxreport]    Script Date: 11/13/2008 12:47:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Noufil 4432 08/05/2008 html serial number added added for validation report

/****** Object:  StoredProcedure [dbo].[USP_HTP_JpAppFaxreport]    Script Date: 05/26/2008 08:10:27 ******/
/*  
Created By : Noufil Khan  
Business Logic : This Procedure return All the clients that are in appearance status with no letter of report confirmation fax under documents page
Column :
		Ticketid
		causenumber
		firstname
		Lastname
		bond
		verified Status
		verified date
		court
*/

-- Noufil 3999 06/19/2008 This Procedure return All the clients that are in appearance status with no letter of report confirmation fax under documents page

ALTER procedure [dbo].[USP_HTP_JpAppFaxreport]
as  
  
Select distinct   
t.ticketid_pk as Ticketid,  
tv.casenumassignedbycourt as causenumber,  
--Nasir 6013 06/26/2009 remove bond and verified status
t.firstname as FirstName,  
t.Lastname as LastName,  
dbo.fn_DateFormat(isnull(tv.Courtdate,Convert(datetime,'1/1/1900')),30,'/',':',1) as verifieddate,    
c.shortname as court,
dbo.fn_DateFormat(isnull(fl.FaxDate,Convert(datetime,'1/1/1900')),30,'/',':',1) as FaxDate--Nasir 6013 06/26/2009 add faxdate

from tbltickets t  
	inner join tblticketsviolations tv on t.ticketid_pk = tv.TicketID_PK  
	inner join tblcourts c on tv.CourtID=c.Courtid  
	inner join dbo.tblCourtViolationStatus cvs ON  tv.CourtViolationStatusIDMain = cvs.CourtViolationStatusID
	--Sabir Khan 9917 11/30/2011 Faxmakerarchive DB has been removed.
	inner join faxLetter fl on fl.ticketid=t.ticketid_pk
  
where 
	tv.CourtViolationStatusIDMain not in (80)AND 
	-- tahir 6318 08/06/2009 exclude cancelled faxes as well...
	-- Noufil 5113 11/13/2008 fax letter status must not be success
	--fl.status <> 'SUCCESS'
	fl.status not in ('SUCCESS', 'Cancelled')
	and t.activeflag=1
	--Sabir Khan 5439 01/21/2009 Do'nt show records having No Lor Confirmation document type or Fax letter type...
	--Sarim Ghani 5448 01/22/2009 Fix the changes implement by Sabir.
	AND t.TicketID_PK Not IN (SELECT ticketid FROM tblScanBook WHERE DocTypeID IN (17,18))
	--Nasir 6013 06/26/2009 order by TicketID_PK
	ORDER BY t.TicketID_PK


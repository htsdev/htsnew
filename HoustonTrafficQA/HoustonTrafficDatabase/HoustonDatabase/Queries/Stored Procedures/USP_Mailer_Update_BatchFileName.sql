SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Update_BatchFileName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Update_BatchFileName]
GO

CREATE procedure dbo.USP_Mailer_Update_BatchFileName
	(
	@BatchIDs varchar(500),
	@FileName varchar(200)
	)


as 

update tblbatchletter
set filename = @filename
where batchid in (select * from dbo.Sap_String_Param(@batchids))



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




GO
/****** Object:  StoredProcedure [dbo].[USP_PS_SEARCH_CASE_BY_Recordid]    Script Date: 04/30/2008 20:44:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- ================================================        
-- Copy Rights 2007 LN Technologies        
-- ================================================        
        
-- =============================================        
-- Author:   Zeeshan Ahmed        
-- Create date:  13th August 2007         
-- Description:  This Procedure Search the Ticket in Client and Non Client Cases By LetterID        
-- =============================================        
      
    
      
Alter PROCEDURE [dbo].[USP_PS_SEARCH_CASE_BY_Recordid]  --1      
(        
@RecordID int        
)        
AS        
BEGIN        
        
-- Declare Temp Table                 
declare @temp table  (         
  RecordID int ,        
  TicketID int,        
  TicketNumber_Pk varchar(20),        
  [Name] varchar(50),        
  Address varchar(100),        
  CourtDate datetime,        
  SiteId int,      
  CaseType int           
      )                        
              
      
--------------------------------------------------      
-- Search Letter ID In Houston Traffic DataBase --      
--------------------------------------------------      
 -- Search Letter ID In Quote Clients               
 Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate ,SiteID , CaseType )         
 select   distinct top 1 v.recordid,0, case when v.courtlocation between 3007 and 3022 then isnull(v.causenumber,v.ticketnumber_pk) else v.ticketnumber_pk end, t.lastname + ',' + t.firstname,isnull(t.address1,'') + isnull(t.address2,''),                      
 v.courtdate,1,2                
 from  tblticketsviolationsarchive v                       
 inner join tblticketsarchive t     
 on t.recordid = v.recordid                      
 where t.ClientFlag = 0  and        
  v.recordid =@RecordID
  and v.violationstatusid != 80             
      
 -- Search Letter ID In Non Clients       
 Insert Into @temp ( RecordID , TicketID,TicketNumber_pk ,Name,Address , CourtDate , SiteID , CaseType)         
 Select  Distinct top 100 t.RecordID , t.TicketID_Pk ,case when tv.courtid between 3007 and 3022 then  tv.casenumassignedbycourt  else tv.refcaseNumber  end, t.lastname + ',' + t.firstname , isnull(t.address1,'') + isnull(t.address2,'')                
 ,tv.courtdate,1,1                
 from tbltickets t                         
 join tblticketsviolations  tv                        
 on                         
 t.ticketid_pk = tv.Ticketid_pk                        
 where         
 t.activeFlag = 0 and tv.courtviolationstatusidmain !=80  and t.recordid = @RecordID    
      
    
      
      
Select distinct  * from @temp        
        
END        
     
 
    
Go

grant execute on USP_PS_SEARCH_CASE_BY_Recordid to dbr_webuser 

Go  
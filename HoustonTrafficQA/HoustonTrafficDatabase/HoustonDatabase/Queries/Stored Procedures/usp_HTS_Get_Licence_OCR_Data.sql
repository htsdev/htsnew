SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_Licence_OCR_Data]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_Licence_OCR_Data]
GO



CREATE PROCEDURE [dbo].[usp_HTS_Get_Licence_OCR_Data]  
@TicketID as numeric  
as  
  
declare @ScanBookID as numeric  
declare @ResetDesc as varchar(100)  
  
if (SELECT Count(*) FROM tblscanbook where TicketID = @TicketID AND DocTypeID = 4) > 0  
begin  
   
 set @ScanBookID = (Select top 1 scanbookid from tblscanbook where ticketid = @TicketID AND DocTypeID = 4)  
 set @ResetDesc = (Select top 1 ResetDesc from tblscanbook where ticketid = @TicketID AND DocTypeID = 4 )  
  if(SELECT count(*) FROM tblscandata where doc_id = @ScanBookID) > 0   
  begin  
   select 1 as ReturnCode,@ResetDesc as ResetDesc, sd.doc_id, sd.docnum, sd.data,sp.DocExtension from tblscandata sd inner join tblscanpic sp on sd.doc_id=sp.scanbookid where sd.doc_id = @ScanBookID  
  end  
  else  
  begin  
   select 2 as ReturnCode, docid, ScanBookID, DocExtension from tblscanpic where ScanBookID = @ScanBookID  
  end  
end  
  
else  
begin  
Select 0 as ReturnCode  
end


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


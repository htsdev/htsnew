﻿ USE TrafficTickets
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Task ID:		11473
-- Create date: 10/30/2013
-- Description:	This Procedure is used to get the history of submited Leads by clients
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_HTP_GetLeadsHistory 
	@ticketId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements. 
	SET NOCOUNT ON;

    SELECT CONVERT(VARCHAR(50), ISNULL(t.RecDate, '01/01/1900'), 101) AS LeadDate, ISNULL(t.CallerId, 'N/A') AS CallerId, t.PersonName, 
    ISNULL(t.[Language], 'English') AS LanguageSpk, t.EmailAddress, t.Phone, t.PracticeAreaID_FK, 
    ISNULL(t.RepId, 3992) AS EmpId, t.TicketID, ISNULL(tu.Lastname, 'N/A') AS EmpLastName, ISNULL(tu.Firstname, 'N/A') AS EmpFirtsName, tu.UserName,
    pa.[Description] AS PracticeArea
    INTO #temp
    FROM SulloLaw.dbo.tblcontactus t INNER JOIN tblTickets tt ON t.TicketID = tt.TicketID_PK
    LEFT OUTER JOIN TrafficTickets.dbo.tblUsers tu ON t.SaleRepId = tu.EmployeeID
    LEFT OUTER JOIN SulloLaw.dbo.PracticeArea pa ON pa.ID = t.PracticeAreaID_FK
    WHERE t.SiteId = 1 -- Only HTP Leads. . . 
    AND ISNULL(t.TicketID, 0) = @ticketId
    
    SELECT ROW_NUMBER () OVER (ORDER BY LeadDate DESC) AS RowNum, *
    FROM #temp
    
    DROP TABLE #temp
END
GO
	GRANT EXECUTE ON USP_HTP_GetLeadsHistory TO dbr_webuser
GO

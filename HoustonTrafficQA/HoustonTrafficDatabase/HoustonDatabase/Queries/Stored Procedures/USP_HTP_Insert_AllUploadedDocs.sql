﻿/******
Created by:   Fahad Muhammad Qureshi.
Created Date: 10/01/2009
Task ID:	  5376

Business Logic : This procedure is used to insert the uploaded files information of the courts.

List of Parameters:     
   @Courtid:		represnts id of a court
   @DocumentName:	represents name of uploaded document
   @UploadedDate:	represents the date on which the file is uploaded on server
   @Rep:			represents the employee id of the rep which has uploaded the file
   @DocumentPath:	represents the path of uploaded document 
 
******/

Create PROCEDURE [dbo].[USP_HTP_Insert_AllUploadedDocs]
	@SubMenuID_FK INT,
	@DocName VARCHAR(500),
	@EmpID INT,
	@SavedDocName VARCHAR(200)
AS
	INSERT INTO tblHelpDocument
	  (
	    DocName,
	    SubMenuID_FK,
	    EmpID,
	    UploadedDate,
	    SavedDocName
	  )
	VALUES
	  (
	    @DocName,
	    @SubMenuID_FK,
	    @EmpID,
	    (dbo.fn_DateFormat(GETDATE(), 28, '-', ':', 1)),
	    @SavedDocName
	  )
	
	SELECT @@ROWCOUNT
	
	GO
	GRANT EXECUTE ON [dbo].[USP_HTP_Insert_AllUploadedDocs] TO [dbr_webuser] 




































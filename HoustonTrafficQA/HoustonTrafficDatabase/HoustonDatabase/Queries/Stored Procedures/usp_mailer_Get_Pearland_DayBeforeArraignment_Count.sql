﻿USE [TrafficTickets] 
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_Get_Pearland_DayBeforeArraignment_Count]    Script Date: 08/05/2013 12:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
* Created By : Rab Nawaz Khan
* Task ID	: 11108
* Date		: 08/16/2013
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Pearland Municipal Court Appearance day of letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/
-- usp_mailer_Get_Pearland_DayBeforeArraignment_Count 9,75,9,'3/1/10','8/20/10',0

ALTER Procedure [dbo].[usp_mailer_Get_Pearland_DayBeforeArraignment_Count]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int                                             
	)

as                                                              
          
-- DECLARING LOCAL VARIABLES FOR FILTERS.....                                                  
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(MAX)  ,
	@sqlquery2 nvarchar(MAX)  ,
	@sqlParam nvarchar(1000)

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL.....
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

select 	@sqlquery ='', @sqlquery2 = ''
                                                              
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 


-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY ARRAIGNMENT (PRIMARY STATUS) CASES
-- INSERTING VALUES IN A TEMP TABLE....  
set 	@sqlquery=@sqlquery+'                                          
SELECT DISTINCT 
       CONVERT(DATETIME, CONVERT(VARCHAR(10), tva.courtdate, 101)) AS mdate,
       flag1 = ISNULL(ta.Flag1, ''N''),
       ta.donotmailflag,
       ta.recordid,
       COUNT(tva.ViolationNumber_PK) AS ViolCount,
       SUM(tva.fineamount) AS Total_Fineamount 
       INTO #temp
FROM   dbo.tblTicketsViolationsArchive TVA 
       INNER JOIN dbo.tblTicketsArchive TA ON  TVA.recordid = TA.recordid 
       -- Noufil 8168 08/17/2010 Add join with tblCourtViolationStatus 
       INNER JOIN tblCourtViolationStatus tcvs ON tcvs.CourtViolationStatusID = tva.violationstatusid 
       
	   -- NOT MOVED IN PAST 48 MONTHS (ACCUZIP PROCESSED)...
WHERE   ISNULL(ta.ncoa48flag, 0) = 0
       
       -- show court violation category of type 2 eg: ARRAIGNMENT ,APPEARANCE etc
       AND tcvs.CategoryID = 2
        
       -- FOR THE SELECTED DATE RANGE ONLY...
       AND DATEDIFF(DAY, tva.CourtDate, @startListdate) <= 0
       AND DATEDIFF(DAY, tva.CourtDate, @endlistdate) >= 0 
       
       -- First Name and Last Name should not be empty . . 
		AND LEN(ISNULL(ta.LastName, '''')) > 0 AND LEN(ISNULL(ta.FirstName, '''')) > 0
	 
		-- Attorney should not be assigned. . . 
		AND LEN(ISNULL(tva.AttorneyName, '''')) = 0
       
       '

-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY...  
if( @crtid = @CATNUM )                                    
	set @sqlquery=@sqlquery+' 
	and tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = @crtid )'                                        

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE 
	set @sqlquery =@sqlquery +' 
	and tva.courtlocation = @crtid' 
                                                        
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                                      
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  
	and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         
                                                       
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' 
	and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
            
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                        
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' 
	and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION...
if(@violadesc<>'')            
	set @sqlquery=@sqlquery+ ' 
	and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
    and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr  = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
    and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       
                                                      
-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
--Sabir Khan 7601 03/22/2010 Exclude clients whose DLQ letter is sent in past 5 business days.
set @sqlquery =@sqlquery+ ' 

group by                                                       
convert(datetime , convert(varchar(10),tva.courtdate,101)) , ta.Flag1,tva.courtdate, ta.donotmailflag ,ta.recordid
-- no letter of other mailer type will be send in the next 5 working days from th previous sent date. 
Delete from #temp
FROM #temp a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
where 	lettertype =@LetterType 
or (
		l.courtcategory = 38
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
	)                                                    
'                                  


-- GETTING THE RESULT SET IN TEMP TABLE.....                                                      
set @sqlquery=@sqlquery + ' 
Select distinct  ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag, recordid into #temp1  from #temp WHERE 1=1                                                         
 '                                  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ '
	AND  '+ @singlevoilationOpr+  '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1) or violcount>3
    '                                  
                                  
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                       
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + '
	AND '+ @doublevoilationOpr+ '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2) or violcount>3 
	'
                                  

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation<>0)                                  
begin                                  
	set @sqlquery =@sqlquery+ '
	AND '+  @singlevoilationOpr+ '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)  
	OR ' +  @doublevoilationOpr+ '  (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2) or violcount>3 
	'
end 


set @sqlquery2=@sqlquery2 + 'delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)                
'
-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery2=@sqlquery2 +'
Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS 
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate'
	
         
-- OUTPUTTING THE REQURIED INFORMATION........
set @sqlquery2 = @sqlquery2 +'                                                        
Select   #temp2.mdate as listdate,
        Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #Temp2 left outer join #temp3   
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
where Good_Count > 0 order by #temp2.mdate desc

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5 
'                                                      
                                                      
print @sqlquery  + @sqlquery2                                       

-- CONCATENATING THE QUERY ....
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType

GO
GRANT EXECUTE ON [dbo].[usp_mailer_Get_Pearland_DayBeforeArraignment_Count] TO dbr_webuser
GO 




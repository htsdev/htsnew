ALTER procedure USP_HTS_GET_PartialPays   
@partialpay  int , --All, Active, Inactive ,                      
@Uid int =0  ,       
@selectreport int=0 ,  --Which report to display either partailpay or partialpayreport    
@sduedate varchar(12),    
@eduedate varchar (12),    
@shownonschedule  int                        
as                        
                      
                        
                       
   begin                        
                        
 declare @SqlString varchar(6000)                         
      --All active records from tblschedule                      
 set @SqlString='declare @tempschedule  table                        
   (                        
   ticketid_pk int,                          
   scheduleamount money                          
   )                        
                      
  insert into @tempschedule                          
  select T.ticketid_pk,sum(amount) as scheduleamount                      
  from tbltickets T,tblschedulepayment S                          
  where T.ticketid_pk = S.ticketid_pk                        
  and scheduleflag=1    group by T.ticketid_pk'                        
                        
 set @SqlString=@SqlString + '                      
  declare @temp  table                        
   (                        
   ticketid_pk int,                          
   owedamount money                          
   )                        
                        
  insert into @temp                          
  select T.ticketid_pk,max(totalfeecharged)-sum(chargeamount) as owedamount                         
  from tbltickets T,tblticketspayment P                          
  where P.ticketid = T.ticketid_pk                           
  and activeflag = 1                          
  and paymentvoid <>1     
  and paymenttype not in (99,100)   
  and ticketid in (  
  select distinct ticketid_pk from tblticketsviolations v  
  where v.ticketid_pk = t.ticketid_pk  '        
      
if (@selectreport=1)      
 set @SqlString=@SqlString + '  
  and v.courtid in (select courtid from tblcourts where IsCriminalCourt = 1)'      
      
  
 if @partialpay=1   --Partial Pays Active                        
 set @SqlString=@SqlString +'                      
   and datediff(day,v.courtdatemain,getdate()) <= 0 '  
  
 if @partialpay=2   --Partial Pays InActive                        
 set @SqlString=@SqlString +'                      
   and datediff(day,v.courtdatemain,getdate()) > 0  '                        
  
 set @SqlString=@SqlString +'   
  )                     
     group by T.ticketid_pk                        
     having max(totalfeecharged)-sum(chargeamount) > 0   
  
 SELECT                    
--  T.currentcourtloc,              
  T.TicketID_PK,                        
  T.Lastname+'', ''+ T.Firstname  as ClientName,                        
  convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''''))+''(''+ISNULL(LEFT(C1.Description, 1),'''')+'')'' as contact1,                        
  convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''''))+''(''+ISNULL(LEFT(C2.Description, 1),'''')+'')'' as contact2,                        
  convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''''))+''(''+ISNULL(LEFT(C3.Description, 1),'''')+'')'' as contact3,                         
--  T.TicketNumber_PK,                         
  isnull(U.Firstname + '' '' + U.Lastname,''N/A'') AS EmpName,                         
  S.ScheduleID as RecID,                                  
  S.PaymentDate as DueDate,                        
--  T.Currentdateset as Currentdateset ,     
  CurrentDateSet = ( select top 1 courtdatemain from tblticketsviolations where ticketid_pk = t.ticketid_pk  
   and courtdatemain is not null order by courtdatemain  
   ) ,                    
  S.amount as Amount                      
                        
 FROM          dbo.tblTickets T INNER JOIN                        
        @temp TMP ON T.TicketID_PK = TMP.ticketid_pk INNER JOIN                         
                      dbo.tblSchedulePayment S ON T.TicketID_PK = S.TicketID_PK left outer JOIN                        
                      dbo.tblUsers U ON T.salesEmployeeID = U.EmployeeID LEFT OUTER JOIN                        
                      dbo.tblContactstype C1 ON T.ContactType1 = C1.ContactType_PK LEFT OUTER JOIN                        
                      dbo.tblContactstype C2 ON T.ContactType2 = C2.ContactType_PK LEFT OUTER JOIN                        
                      dbo.tblContactstype C3 ON T.ContactType3 = C3.ContactType_PK                        
 WHERE     (S.ScheduleFlag = 1)                      
'                        
set @SqlString =@SqlString + 'and S.PaymentDate between '''+@sduedate+''' and '''+@eduedate+''' '      
                         
 if @Uid <> 0                           
  begin                   
   if @uid = -1         
     set @SqlString = @SqlString + ' and (T.salesEmployeeID is null )'        
   else        
     set @SqlString = @SqlString + ' and (T.salesEmployeeID = ' + convert(varchar(20),@Uid) + ')'                         
  end                          
                         
-- set @SqlString = @SqlString + ' ORDER BY T.CurrentDateset  '                        
if @shownonschedule=1  --schedule owes    
 begin    
                       
 set @SqlString = @SqlString +'union                       
'                      
                       
 set @SqlString = @SqlString +'SELECT                            
       
--    T.currentcourtloc,         
 T.TicketID_PK,                             
  T.Lastname+'' ''+ T.Firstname  as ClientName,                        
    convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''''))+''(''+ISNULL(LEFT(C1.Description, 1),'''')+'')'' as contact1,                        
         convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''''))+''(''+ISNULL(LEFT(C2.Description, 1),'''')+'')'' as contact2,                        
         convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''''))+''(''+ISNULL(LEFT(C3.Description, 1),'''')+'')'' as contact3,                         
--  T.TicketNumber_PK,                         
  isnull(U.Firstname + '' '' + U.Lastname,''N/A'') AS EmpName,                      
  0 as RecID,                                  
  ''01/01/1900''  as DueDate,                        
--  T.Currentdateset as Currentdateset ,     
  CurrentDateSet = ( select top 1 courtdatemain from tblticketsviolations where ticketid_pk = t.ticketid_pk  
   and courtdatemain is not null order by courtdatemain  
   )   ,                  
  TMP.owedamount  -  isnull (TS.scheduleamount,0)  as Amount                          
                        
 FROM          dbo.tblTickets T INNER JOIN                        
        @temp TMP ON T.TicketID_PK = TMP.ticketid_pk left outer  JOIN                         
                      @tempschedule TS ON TMP.TicketID_PK = TS.TicketID_PK left outer join                       
                      dbo.tblUsers U ON T.salesEmployeeID = U.EmployeeID LEFT OUTER JOIN                        
                      dbo.tblContactstype C1 ON T.ContactType1 = C1.ContactType_PK LEFT OUTER JOIN                        
                      dbo.tblContactstype C2 ON T.ContactType2 = C2.ContactType_PK LEFT OUTER JOIN                        
                      dbo.tblContactstype C3 ON T.ContactType3 = C3.ContactType_PK                        
                      
 WHERE     (TMP.owedamount-isnull(TS.scheduleamount,0))>0'                      
                    
if @Uid <> 0                           
  begin        
  if @uid = -1         
       set @SqlString = @SqlString + ' and (T.salesEmployeeID is null) '        
    else                          
    set @SqlString = @SqlString + ' and (T.salesEmployeeID = ' + convert(varchar(20),@Uid) + ')'                         
  end                     
end --schedule owes    
 set @SqlString = @SqlString + ' order by ClientName'          
                        
--print (@sqlstring)                        
                        
 exec(@SqlString)                        
                         
   end  
go
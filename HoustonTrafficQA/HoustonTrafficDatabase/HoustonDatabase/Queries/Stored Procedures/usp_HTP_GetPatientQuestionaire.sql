﻿ /******    
* Created By :   Ozair
* Create Date :   11/05/2010   
* Task ID :    8501  
* Business Logic :  This procedure is used to get patient questionaire.  
*/

ALTER PROCEDURE dbo.usp_HTP_GetPatientQuestionaire
	@PatientId INT
AS
	SELECT p.DoctorContacted,
	       CONVERT(VARCHAR,p.SurgeryDate,101) AS SurgeryDate,
	       isnull(p.SurgeryMonth,0) AS SurgeryMonth,
	       isnull(p.SurgeryDay,0) AS SurgeryDay,
	       isnull(p.SurgeryYear,0) AS SurgeryYear,
	       p.ContactComment,
	       p.HadBloodTest,
	       p.BloodTestDetail,
	       p.IsProblem,
	       p.ProblemComments,
	       p.PainSeverity,
	       p.isManufactrurer,
	       p.ManufacturerComments,
	       p.WereScrewImplanted,
	       p.ImplantMaterialId,
	       p.isDeviceDefected,
	       p.DeviceDefectedDetail,
	       p.HasImplantRemoved,
	       p.IsSchedule,
	       ISNULL(p.ScheduleDate,'01/01/1900') AS ScheduleDate,
	       p.ScheduleComments,
	       p.ObserverComments,
	       p.ManufactuerId,	--Saeed 8585 12/03/2010 manufacturer id added in select statment.
	       p.ImplantedHip,
	       p.ImplantedHipComments,
	       OtherDeviceImplanted,
	       OtherDeviceImplantedId,
	       OtherDeviceImplantedComments,
	       ProductId,
	       WhyHispImplanted,
	       p.SpanishSpeaker
	FROM   Patient p
	WHERE  p.Id = @PatientId
GO

GRANT EXECUTE ON dbo.usp_HTP_GetPatientQuestionaire TO dbr_webuser
GO

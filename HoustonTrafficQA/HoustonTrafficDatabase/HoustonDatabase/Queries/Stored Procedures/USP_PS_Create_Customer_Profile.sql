/****************
Created By : Zeeshan Ahmed

Business Logic : This Procedure is used to migrate case information from Sullolaw site to Traffic System

List Of Input Parameters :

-- Customer Information

 @customerid         
 @firstname     
 @lastname 
 @telephone 
 @alttelephone 
 @alttelephone2           
 @height  
 @weight 
 @HairColor   
 @EyeColor 
 @email 
 @contact  
 @homeaddress    
 @city          
 @zip           
 @dob        
 @driverlicense     
 @comments       
 @crname            
 @crnumber   
 @trafficticket    
 @csc     
 @crmonth      
 @crtype        
 @cryear 
 @amount 
 @SessionID 
 @Recdate 
 @ApprovedFlag 
 @IsProcessCompleted               
 @StateID_Fk          
 @ClientType
 @dlstate
 @Gender
 @Race 
 @TelType 
 @MailerID
 @TicketNo
 @ReceiptFileName
 @LastPageIndex  
 @siteid 
 @recordid                 

-- Case Setting Information        

@accident    
@school
@bondflag
@cdl
@CourtSettingID
@CourtNumber

-- Case Violation Information        

@fineamount
@bondamount
@courtid
@courtdate
@violationids

-- Payment Information        
@invnumber 
@status 
@errmsg 
@transnum 
@response_subcode  
@response_reason_code 
@response_reason_text 
@auth_code
@avs_code     


List Of Columns :

ReceiptFileName  : Receipt File Name
ApprovedFlag  : Approved Flag
TicketID  : Ticket Id
InvoiceNumber : Invoice Number

***************/


alter PROCEDURE [dbo].[USP_PS_Create_Customer_Profile] 
(
    --Customer Information Parameter         
    @customerid            INT,
    @firstname             [varchar] (100),
    @lastname              [varchar] (100),
    @telephone             [varchar] (20),
    @alttelephone          [varchar] (20),
    @alttelephone2         [varchar] (20),
    @height                [varchar] (20),
    @weight                [varchar] (20),
    @HairColor             [varchar] (20),
    @EyeColor              [varchar] (20),
    @email                 [varchar] (50),
    @contact               [varchar] (50),
    @homeaddress           [varchar] (100),
    @city                  [varchar] (50),
    @zip                   [varchar] (12),
    @dob                   DATETIME,
    @driverlicense         [varchar] (20),
    @comments              [text],
    @crname                [varchar] (50),
    @crnumber              [varchar] (20),
    @trafficticket         [varchar] (20),
    @csc                   [varchar] (5),
    @crmonth               [tinyint],
    @crtype                [varchar] (20),
    @cryear                [varchar] (4),
    @amount                [numeric](18, 3),
    @SessionID             [varchar](100),
    @Recdate               [varchar] (30),
    @ApprovedFlag          [tinyint] = 0,
    @IsProcessCompleted    BIT,
    @StateID_Fk            [int] = 0,
    @ClientType            [tinyint],
    @dlstate               INT,
    @Gender                CHAR(6),
    @Race                  VARCHAR(20),
    @TelType               INT,
    @MailerID              INT = 0,
    @TicketNo              INT,
    @ReceiptFileName       VARCHAR(200),
    @LastPageIndex         INT,
    @siteid                INT,
    @recordid              INT,
    -- Case Setting Information        
    @accident              BIT,
    @school                BIT,
    @bondflag              BIT,
    @cdl                   BIT,
    @CourtSettingID        INT,
    @CourtNumber           VARCHAR(3),
    -- Case Violation Information        
    @fineamount            MONEY,
    @bondamount            MONEY,
    @courtid               INT,
    @courtdate             DATETIME,
    @violationids          VARCHAR(2000),
    -- Payment Information        
    @invnumber             VARCHAR(10),
    @status                VARCHAR(2),
    @errmsg                VARCHAR(255),
    @transnum              VARCHAR(50),
    @response_subcode      VARCHAR(20),
    @response_reason_code  VARCHAR(20),
    @response_reason_text  VARCHAR(100),
    @auth_code             VARCHAR(6),
    @avs_code              VARCHAR(1)
)
AS
	-- VARIABLE DECLARATIONS.......                    
	DECLARE --Used For Court Search Case        
	        @RecCount INT,
	        @TicketId INT,
	        @InvoiceNumber INT,
	        @empid INT,
	        @empdesc VARCHAR(10),
	        --Used For Non Client Case s           
	        @FTALinkID INT,
	        @Officernumber INT,
	        @violationdate DATETIME,
	        @listdate DATETIME 
	
	
	
	--Start Transaction               
	BEGIN TRANSACTION 
	
	--Get Case Type
	DECLARE @casetypeid AS INT
	SELECT @casetypeid = ISNULL(CaseTypeId, 1)
	FROM   tblcourts
	WHERE  courtid = @courtid
	
	
	--Get Employee Abbreviation        
	SELECT @empid = 3992,
	       @empdesc = abbreviation
	FROM   dbo.tblusers
	WHERE  employeeid = 3992 
	
	--If Case Is Non Client         
	IF @TicketNo = 0
	   AND @Recordid <> 0
	BEGIN
	    --Update Court Date In Archive Table If Not Available         
	    UPDATE tblticketsviolationsarchive
	    SET    CourtDate = @CourtDate,
	           CourtNumber = @CourtNumber
	    WHERE  recordid = @RecordId
	           AND courtdate = '1/1/1900' 
	    
	    
	    -- CREATING TEMP TABLES.................................. 
	    DECLARE @tblTicketsArchive TABLE (
	                [TicketNumber] [varchar](20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NOT NULL,
	                [MidNumber] [varchar](20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [FirstName] [varchar](20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [LastName] [varchar](20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [Initial] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [Address1] [varchar](50) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [Address2] [varchar](50) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [StateID_FK] [int] NULL,
	                [ZipCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [PhoneNumber] [varchar](15) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [WorkPhoneNumber] [varchar](12) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [DOB] [datetime] NULL,
	                [DLNumber] [varchar](30) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [ViolationDate] [datetime] NULL,
	                [Race] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [Gender] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [Height] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [Weight] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS 
	                NULL,
	                [ListDate] [datetime] NOT NULL,
	                [LetterCode] [smallint] NULL,
	                [MailDateREGULAR] [datetime] NULL,
	                [MailDateFTA] [datetime] NULL,
	                [Bondflag] [tinyint] NULL,
	                [officerNumber_Fk] [int] NULL,
	                [RecordID] [int],
	                [DP2] [varchar](10),
	                [DPC] [varchar](10),
	                [Flag1] [varchar](1),
	                [courtid] [int],
	                [Courtdate] [datetime] NULL
	            ) 
	    
	    DECLARE @tblTicketsViolationsArchive TABLE (
	                [TicketNumber_PK] [varchar](20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NOT NULL,
	                [ViolationNumber_PK] [smallint] NULL,
	                [FineAmount] [money] NULL,
	                [ViolationDescription] [varchar](200) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [ViolationCode] [varchar](10) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [violationstatusid] [int] NULL,
	                [CourtDate] [datetime] NULL,
	                [Courtnumber] [varchar](3) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [BondAmount] [money] NULL,
	                [TicketOfficerNumber] [varchar](20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [RecordID] [int] NULL,
	                [CauseNumber] [varchar](30) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [CourtLocation] [int] NULL,
	                [OffenseLocation] [varchar](200) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [OffenseTime] [varchar] (20) COLLATE 
	                SQL_Latin1_General_CP1_CI_AS NULL,
	                [ArrestingAgencyName] [varchar] (50) NULL,
	                [Statutenumber] VARCHAR(50) NULL --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	            ) 
	    
	    
	    INSERT INTO @tblTicketsArchive
	      (
	        TicketNumber,
	        MidNumber,
	        FirstName,
	        LastName,
	        Initial,
	        Address1,
	        Address2,
	        City,
	        StateID_FK,
	        ZipCode,
	        PhoneNumber,
	        WorkPhoneNumber,
	        DOB,
	        DLNumber,
	        ViolationDate,
	        Race,
	        Gender,
	        Height,
	        WEIGHT,
	        ListDate,
	        MailDateREGULAR,
	        MailDateFTA,
	        Bondflag,
	        officerNumber_Fk,
	        RecordID,
	        DPC,
	        DP2,
	        FLAG1,
	        courtid,
	        courtdate
	      )
	    SELECT t.TicketNumber,
	           t.MidNumber,
	           t.FirstName,
	           t.LastName,
	           t.Initial,
	           t.Address1,
	           t.Address2,
	           t.City,
	           t.StateID_FK,
	           t.ZipCode,
	           t.PhoneNumber,
	           t.WorkPhoneNumber,
	           t.DOB,
	           t.DLNumber,
	           t.ViolationDate,
	           t.Race,
	           t.Gender,
	           t.Height,
	           t.Weight,
	           t.ListDate,
	           t.MailDateREGULAR,
	           t.MailDateFTA,
	           t.Bondflag,
	           t.officerNumber_Fk,
	           t.RecordID,
	           t.DPC,
	           t.DP2,
	           t.FLAG1,
	           t.courtid,
	           t.courtdate	           
	    FROM   tblticketsarchive t
	           INNER JOIN tblticketsviolationsarchive v
	                ON  t.recordid = v.recordid
	    WHERE  t.recordid = @RecordId       
	    
	    
	    INSERT INTO @tblTicketsViolationsArchive
	      (
	        TicketNumber_PK,
	        ViolationNumber_PK,
	        FineAmount,
	        ViolationDescription,
	        ViolationCode,
	        violationstatusid,
	        CourtDate,
	        Courtnumber,
	        BondAmount,
	        TicketOfficerNumber,
	        RecordID,
	        CauseNumber,
	        courtlocation,
	        OffenseLocation,
	        OffenseTime,
	        ArrestingAgencyName,
	        Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	      )
	    SELECT v.TicketNumber_PK,
	           v.ViolationNumber_PK,
	           v.FineAmount,
	           v.ViolationDescription,
	           v.ViolationCode,
	           v.violationstatusid,
	           v.CourtDate,
	           v.Courtnumber,
	           v.BondAmount,
	           v.TicketOfficerNumber,
	           v.RecordID,
	           v.CauseNumber,
	           v.courtlocation,
	           v.OffenseLocation,
	           v.OffenseTime,
	           v.ArrestingAgencyName,
	           v.Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	    FROM   tblticketsviolationsarchive v
	           INNER JOIN @tblticketsarchive t
	                ON  t.recordid = v.recordid
	    WHERE  v.recordid = @RecordId 
	    
	    
	    
	    
	    --Retrive Case Information From Archive Tables
	    --select * into #tblTicketsArchive
	    --from tblticketsarchive
	    --where recordid =@RecordId                          
	    
	    --Retrive Violation Infromation From Archive Table
	    --select * into #tblTicketsViolationsArchive
	    --from tblticketsviolationsarchive
	    --where recordid = @RecordId        
	    
	    -- Getting FTA Link ID To Add Other FTA Related Tickets In The Case             
	    SET @FTALinkID = 0            
	    SELECT TOP 1 @FTALinkID = ISNULL(FTALinkID, 0)
	    FROM   tblticketsviolationsarchive
	    WHERE  recordid = @RecordId
	           AND FTALinkID IS NOT NULL 
	    
	    -- Getting Related FTA Tickets        
	    IF @FTALinkID <> 0
	    BEGIN
	        --Insert into #tblTicketsViolationsArchive select * from tblticketsviolationsarchive
	        --where recordid <>  @RecordId  and FTALinkID = @FTALinkID            
	        INSERT INTO @tblTicketsViolationsArchive
	          (
	            TicketNumber_PK,
	            ViolationNumber_PK,
	            FineAmount,
	            ViolationDescription,
	            ViolationCode,
	            violationstatusid,
	            CourtDate,
	            Courtnumber,
	            BondAmount,
	            TicketOfficerNumber,
	            RecordID,
	            CauseNumber,
	            courtlocation,
	            OffenseLocation,
	            OffenseTime,
	            ArrestingAgencyName,
	            Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	          )
	        SELECT v.TicketNumber_PK,
	               v.ViolationNumber_PK,
	               v.FineAmount,
	               v.ViolationDescription,
	               v.ViolationCode,
	               v.violationstatusid,
	               v.CourtDate,
	               v.Courtnumber,
	               v.BondAmount,
	               v.TicketOfficerNumber,
	               v.RecordID,
	               v.CauseNumber,
	               v.courtlocation,
	               v.OffenseLocation,
	               v.OffenseTime,
	               v.ArrestingAgencyName,
	               v.Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	        FROM   tblticketsviolationsarchive v
	        WHERE  v.recordid <> @RecordId
	               AND v.FTALinkID = @FTALinkID      
	        
	        INSERT INTO @tblTicketsArchive
	          (
	            TicketNumber,
	            MidNumber,
	            FirstName,
	            LastName,
	            Initial,
	            Address1,
	            Address2,
	            City,
	            StateID_FK,
	            ZipCode,
	            PhoneNumber,
	            WorkPhoneNumber,
	            DOB,
	            DLNumber,
	            ViolationDate,
	            Race,
	            Gender,
	            Height,
	            WEIGHT,
	            ListDate,
	            MailDateREGULAR,
	            MailDateFTA,
	            Bondflag,
	            officerNumber_Fk,
	            RecordID,
	            DPC,
	            DP2,
	            FLAG1,
	            courtid,
	            courtdate
	          )
	        SELECT t.TicketNumber,
	               t.MidNumber,
	               t.FirstName,
	               t.LastName,
	               t.Initial,
	               t.Address1,
	               t.Address2,
	               t.City,
	               t.StateID_FK,
	               t.ZipCode,
	               t.PhoneNumber,
	               t.WorkPhoneNumber,
	               t.DOB,
	               t.DLNumber,
	               t.ViolationDate,
	               t.Race,
	               t.Gender,
	               t.Height,
	               t.Weight,
	               t.ListDate,
	               t.MailDateREGULAR,
	               t.MailDateFTA,
	               t.Bondflag,
	               t.officerNumber_Fk,
	               t.RecordID,
	               t.DPC,
	               t.DP2,
	               t.FLAG1,
	               t.courtid,
	               t.courtdate
	        FROM   tblticketsarchive t,
	               tblticketsviolationsarchive v
	        WHERE  t.recordid = v.recordid
	               AND t.recordid != @RecordId
	               AND v.FTALinkid = @FTALinkID
	    END 
	    
	    -- Set Information In Variables                      
	    SELECT @officernumber = ISNULL(officernumber_fk, 962528),
	           @violationdate = ISNULL(violationdate, '01/01/1900'),
	           @courtdate = t.courtdate,
	           --@courtid = t.courtid,          
	           @listdate = t.listdate
	    FROM   @tblTicketsArchive t
	           INNER JOIN @tblTicketsViolationsArchive v
	                ON  t.recordid = v.recordid
	                AND t.recordid = @RecordId 
	    
	    --Declare Temp Table        
	    DECLARE @Temp TABLE (
	                TicketNumber VARCHAR(20),
	                CourtDate DATETIME,
	                ViolationDate DATETIME,
	                recordid INT
	            )            
	    
	    INSERT INTO @Temp
	    SELECT DISTINCT TicketNumber,
	           CourtDate,
	           ISNULL(violationdate, '01/01/1900'),
	           recordid
	    FROM   @tblTicketsArchive
	    WHERE  recordid = @recordid 
	    
	    -- Retrive Mail Date                            
	    
	    
	    ------------------------------------------------------------------------------------------------------------------------
	    -- INSERTING MAIN INFORMATION INTO TBLTICKETS FROM TBLTICKETSARCHIVE
	    ------------------------------------------------------------------------------------------------------------------------            
	    
	    INSERT INTO tbltickets
	      (
	        midnum,
	        firstname,
	        lastname,
	        Address1,
	        Address2,
	        City,
	        Stateid_FK,
	        Zip,
	        Contact1,
	        contactType1,
	        contact2,
	        Contacttype2,
	        contact3,
	        contacttype3,
	        height,
	        WEIGHT,
	        haircolor,
	        eyes,
	        DOB,
	        DLNumber,
	        dlstate,
	        ViolationDate,
	        OfficerNumber,
	        employeeid,
	        Race,
	        Gender,
	        employeeidupdate,
	        RecordId,
	        email,
	        generalcomments,
	        accidentflag,
	        cdlflag,
	        bondflag,
	        BondsRequiredflag,
	        languagespeak,
	        MailerID,
	        WalkInClientFlag,
	        hasConsultationComments,
	        AddressConfirmFlag,
	        isPR,
	        NODL,
	        CaseTypeId
	      )
	    SELECT DISTINCT MidNumber,
	           t.firstname,
	           t.lastname,
	           t.address1,
	           t.address2,
	           t.city,
	           ISNULL(t.stateid_fk, 45),
	           t.zipcode,
	           @telephone,
	           CASE 
	                WHEN LEN(@telephone) > 0 THEN 1
	                ELSE 0
	           END,
	           @alttelephone,
	           CASE 
	                WHEN LEN(@alttelephone) > 0 THEN 4
	                ELSE 0
	           END,
	           @alttelephone2,
	           CASE 
	                WHEN LEN(@alttelephone2) > 0 THEN ISNULL(@TelType, 0)
	                ELSE 0
	           END,
	           @height,
	           @weight,
	           @haircolor,
	           @eyecolor,
	           @dob,
	           @driverlicense,
	           @dlstate,
	           ViolationDate,
	           OfficerNumber_FK,
	           @EmpId,
	           @Race,
	           @Gender,
	           @EmpId,
	           RecordId,
	           @email,
	           CASE 
	                WHEN LEN(ISNULL(CONVERT(VARCHAR(1900), @comments), '')) > 0 THEN 
	                     CONVERT(VARCHAR(1900), @comments) + ' (' + dbo.formatdateandtimeintoshortdateandtime(GETDATE()) 
	                     + ' - ' + @empdesc + ')' + ' '
	                ELSE ''
	           END,
	           @accident,
	           @cdl,
	           @bondflag,
	           @bondflag,
	           'ENGLISH',
	           @MailerID,
	           0,
	           0,
	           1,
	           3,
	           CASE 
	                WHEN @driverlicense = '' THEN 1
	                ELSE 0
	           END,
	           @casetypeid
	    FROM   @tblTicketsArchive t
	    WHERE  t.recordid = @recordid          
	    
	    IF @@error = 0
	    -- Afaq 8115 08/07/2010 replace @@identity with scope_identity()
	        --SELECT @TicketID = @@identity 
	        SELECT @TicketID = SCOPE_IDENTITY()
	    
	    ------------------------------------------------------------------------------------------------------------------------
	    --  INSERTING DETAILED INFORMATION INTO TBLTICKETSVIOLATIONS FROM TBLTICKETSVIOLATIONSARCHIVE
	    ------------------------------------------------------------------------------------------------------------------------            
	    IF @TicketId <> 0
	    BEGIN
	        -- Declare Temporary Tables        
	        DECLARE @temp1 TABLE 
	                (
	                    violationnumber INT,
	                    bondamount MONEY,
	                    FineAmount MONEY,
	                    TicketNumber VARCHAR(20),
	                    sequenceNumber TINYINT,
	                    Violationcode VARCHAR(10),
	                    ViolationStatusID INT,
	                    CourtNumber VARCHAR(10),
	                    CourtDate DATETIME,
	                    ViolationDescription VARCHAR(200),
	                    courtid INT,
	                    ticketviolationdate DATETIME,
	                    ticketofficernum VARCHAR(20),
	                    CauseNumber VARCHAR(25),
	                    RecordId INT,
	                    Statutenumber VARCHAR(50) --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	                )          
	        
	        DECLARE @temp2 TABLE 
	                (
	                    violationnumber INT,
	                    fineamount MONEY,
	                    ticketnumber VARCHAR(25),
	                    sequencenummber INT,
	                    violationcode VARCHAR(10),
	                    bondamount MONEY,
	                    violationstatusid INT,
	                    courtnumber INT,
	                    courtdate DATETIME,
	                    courtid INT,
	                    CauseNumber VARCHAR(25),
	                    recordid INT,
	                    Statutenumber VARCHAR(50) --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number 
	                ) 
	        
	        --If Inside Courts         
	        IF @courtid IN (3001, 3002, 3003)
	        BEGIN
	            INSERT INTO @temp1
	            SELECT DISTINCT 
	                   ISNULL(L.ViolationNumber_PK, 0) AS ViolationNumber,
	                   ISNULL(V.bondamount, 0) AS BondAmount,
	                   V.FineAmount,
	                   v.ticketnumber_pk,
	                   V.ViolationNumber_PK AS sequenceNumber,
	                   V.violationcode AS Violationcode,
	                   CASE v.ViolationStatusID
	                        WHEN 186 THEN 135
	                        WHEN 146 THEN 135
	                        ELSE v.ViolationStatusID
	                   END,
	                   v.CourtNumber,
	                   V.CourtDate,
	                   ISNULL(v.ViolationDescription, 'N/A') AS 
	                   violationdescription,
	                   --@CourtId,             
	                   courtlocation,
	                   t.violationdate,
	                   ISNULL(v.TicketOfficerNumber, 962528),
	                   v.CauseNumber,
	                   v.recordid,
	                   v.Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	            FROM   @tblTicketsArchive t
	                   INNER JOIN @tblticketsviolationsarchive V
	                        ON  (t.recordid = v.recordid)
	                   LEFT OUTER JOIN tblViolations L
	                        ON  V.violationcode = L.violationcode
	                        AND v.violationdescription = l.description -- tahir 5571 02/23/2009 fixed the duplication issue.
	        END-- If Out Side Courts
	        ELSE
	        BEGIN
	            INSERT INTO @temp1
	            SELECT DISTINCT 
	                   ISNULL(ISNULL(L.ViolationNumber_PK, V.violationNumber_PK), 0) AS 
	                   ViolationNumber,
	                   ISNULL(V.bondamount, 0) AS BondAmount,
	                   V.FineAmount,
	                   v.ticketnumber_pk,
	                   '0' AS sequenceNumber,
	                   V.violationcode AS Violationcode,
	                   v.ViolationStatusID,
	                   v.CourtNumber,
	                   V.CourtDate,
	                   ISNULL(v.ViolationDescription, 'N/A') AS 
	                   violationdescription,
	                   --@CourtId,             
	                   courtlocation,
	                   t.violationdate,
	                   ISNULL(v.TicketOfficerNumber, 962528),
	                   v.CauseNumber,
	                   v.recordid,
	                   v.Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	            FROM   @tblticketsarchive t
	                   INNER JOIN @tblticketsviolationsarchive V
	                        ON  (t.recordid = v.recordid)
	                   LEFT OUTER JOIN tblViolations L
	                        ON  V.violationdescription = L.description
	                        AND v.violationcode = l.violationcode
	                        AND l.violationtype = (
	                                SELECT violationtype
	                                FROM   tblcourtviolationtype
	                                WHERE  courtid = @courtid
	                            )
	        END 
	        
	        -- Save Violation Information                    
	        INSERT INTO @temp2
	        SELECT V.ViolationNumber_PK,
	               t1.fineamount,
	               t1.TicketNumber,
	               t1.sequencenumber,
	               t1.violationcode,
	               t1.bondamount,
	               ViolationStatusID,
	               CourtNumber,
	               CourtDate,
	               courtid,
	               t1.CauseNumber,
	               t1.recordid,
	               t1.Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	        FROM   @temp1 t1
	               INNER JOIN tblViolations V
	                    ON  t1.sequencenumber = V.sequenceorder
	        WHERE  t1.violationnumber = 0               
	        
	        UPDATE t1
	        SET    t1.violationnumber = t2.violationnumber
	        FROM   @temp1 t1,
	               @temp2 t2
	        WHERE  t1.violationcode = t2.violationcode            
	        
	        INSERT INTO tblticketsviolations
	          (
	            TicketID_PK,
	            ViolationNumber_PK,
	            FineAmount,
	            refcasenumber,
	            bondamount,
	            CourtViolationStatusID,
	            CourtNumber,
	            CourtDate,
	            violationdescription,
	            courtid,
	            ticketviolationdate,
	            TicketOfficerNumber,
	            vemployeeid,
	            CourtViolationStatusIDmain,
	            CourtNumbermain,
	            CourtDatemain,
	            CourtViolationStatusIDscan,
	            CourtNumberscan,
	            CourtDatescan,
	            casenumassignedbycourt,
	            underlyingbondflag,
	            PlanId,
	            RecordID,
	            Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	          )
	        SELECT DISTINCT 
	               @TicketID,
	               violationNumber,
	               FineAmount,
	               TicketNumber,
	               bondamount,
	               ViolationStatusID,
	               CourtNumber,
	               CourtDate,
	               violationdescription,
	               courtid,
	               ticketviolationdate,
	               ticketofficernum,
	               @empid,
	               ViolationStatusID,
	               CourtNumber,
	               CourtDate,
	               ViolationStatusID,
	               CourtNumber,
	               CourtDate,
	               CauseNumber,
	               CASE 
	                    WHEN ISNULL(courtdate, '1/1/1900') < GETDATE() THEN 1
	                    ELSE 0
	               END,
	               (
	                   SELECT TOP 1 planid
	                   FROM   tblpriceplans
	                   WHERE  courtid = @Courtid
	                          AND effectivedate <= @listdate
	                          AND enddate >  = @listdate
	                          AND isActivePlan = 1
	                   ORDER BY
	                          planid
	               ),
	               @RecordId,
	               Statutenumber --Muhammad Muneer 8240 09/09/2010 add the extra column of statute number
	        FROM   @temp1
	    END 
	    
	    --Update Case Information
	    EXEC USP_PS_Update_CaseInformation @Ticketid,
	         @firstname,
	         @lastname,
	         @homeaddress,
	         @stateid_fk,
	         @city,
	         @zip
	END 
	
	-- If Client Is A Court Search Client         
	IF @ClientType = 0
	   AND @recordid = 0
	   AND @ticketno = 0
	BEGIN
	    ------------------------------------------------
	    -- FIRST INSERTING RECORDS IN TBLTICKETS
	    ------------------------------------------------                     
	    INSERT INTO dbo.tbltickets
	      (
	        firstname,
	        lastname,
	        contact1,
	        contacttype1,
	        contact2,
	        contacttype2,
	        contact3,
	        contacttype3,
	        height,
	        WEIGHT,
	        haircolor,
	        eyes,
	        email,
	        address1,
	        city,
	        stateid_fk,
	        zip,
	        dob,
	        dlnumber,
	        dlstate,
	        calculatedtotalfee,
	        totalfeecharged,
	        recdate,
	        accidentflag,
	        cdlflag,
	        bondsrequiredflag,
	        bondflag,
	        employeeid,
	        employeeidupdate,
	        activeflag,
	        lockflag,
	        firmid,
	        basefeecharge,
	        languagespeak,
	        generalcomments,
	        gender,
	        race,
	        WalkInClientFlag,
	        hasConsultationComments,
	        AddressConfirmFlag,
	        isPR,
	        nodl,
	        casetypeid
	      )
	    VALUES
	      (
	        LEFT(LTRIM(@firstname), 20),
	        LEFT(LTRIM(@lastname), 20),
	        LEFT(LTRIM(@telephone), 15),
	        CASE 
	             WHEN LEN(@telephone) > 0 THEN 1
	             ELSE 0
	        END,
	        LEFT(LTRIM(@alttelephone), 15),
	        CASE 
	             WHEN LEN(@alttelephone) > 0 THEN 4
	             ELSE 0
	        END,
	        LEFT(LTRIM(@alttelephone2), 15),
	        CASE 
	             WHEN LEN(@alttelephone2) > 0 THEN ISNULL(@TelType, 0)
	             ELSE 0
	        END,
	        CASE 
	             WHEN @height = '-NA-' THEN NULL
	             ELSE LEFT(LTRIM(@height), 10)
	        END,
	        CASE 
	             WHEN @weight = '-NA-' THEN NULL
	             ELSE LEFT(LTRIM(@weight), 10)
	        END,
	        CASE 
	             WHEN @HairColor = '-NA-' THEN NULL
	             ELSE LEFT(LTRIM(@HairColor), 10)
	        END,
	        CASE 
	             WHEN @EyeColor = '-NA-' THEN NULL
	             ELSE LEFT(LTRIM(@EyeColor), 10)
	        END,
	        @email,
	        LEFT(LTRIM(@homeaddress), 50),
	        @city,
	        @stateid_fk,
	        @zip,
	        CONVERT(DATETIME, @dob, 101),
	        @driverlicense,
	        @dlstate,
	        @amount,
	        @amount,
	        @Recdate,
	        @accident,
	        @cdl,
	        @bondFlag,
	        @bondFlag,
	        @empid,
	        @empid,
	        1,
	        1,
	        3000,
	        @amount,
	        'ENGLISH',
	        CASE 
	             WHEN LEN(ISNULL(CONVERT(VARCHAR(1900), @comments), '')) > 0 THEN 
	                  CONVERT(VARCHAR(1900), @comments) + ' (' + dbo.formatdateandtimeintoshortdateandtime(GETDATE()) 
	                  + ' - ' + @empdesc + ')' + ' '
	             ELSE ''
	        END,
	        @gender,
	        @race,
	        0,
	        0,
	        1,
	        3,
	        CASE 
	             WHEN @driverlicense = '' THEN 1
	             ELSE 0
	        END,
	        @casetypeid
	      )           
	     -- Afaq 8115 08/07/2010 replace @@identity with scope_identity()
	    SET @TicketId = SCOPE_IDENTITY()
	    
	    IF @TicketId IS NOT NULL
	    BEGIN
	        -------------------------------------------------
	        -- NOW INSERTING RECORDS IN TBLTICKETSVIOLATIONS
	        -------------------------------------------------        
	        
	        --Declare Temp Table For Court Search Violations         
	        DECLARE @violations TABLE (violationnumber_pk INT) 
	        
	        --Get ViolationIds from CSV format        
	        INSERT INTO @violations
	        SELECT *
	        FROM   dbo.Sap_String_Param(@violationids) 
	        
	        -- FIRST GETTING RECORDS IN A TEMP TABLE.....                    
	        DECLARE @tempCourt TABLE 
	                (
	                    RowId INT IDENTITY(1, 1),
	                    violationnumber_pk INT,
	                    refcasenumber VARCHAR(30),
	                    courtdate DATETIME,
	                    courtid INT,
	                    courtviolationstatusidmain INT,
	                    courtnumber VARCHAR(3)
	            )
	        INSERT INTO @tempCourt
	          (
	            violationnumber_pk,
	            refcasenumber,
	            courtdate,
	            courtid,
	            courtviolationstatusidmain,
	            courtnumber
	          )
	        SELECT V.ViolationNumber_PK,
	               'ID' + CONVERT(VARCHAR(15), @TicketId),
	               @CourtDate,
	               @CourtID,
	               CASE @courtsettingid
	                    WHEN 5 THEN 103
	                    WHEN 4 THEN 26
	                    ELSE 3
	               END,
	               @courtnumber
	        FROM   @violations V
	               INNER JOIN dbo.tblViolations v2
	                    ON  v.ViolationNumber_PK = v2.ViolationNumber_PK 
	        --AND  v2.Violationtype = 10                    
	        
	        -- UPDATING SEQUENCE NUMBER IN TEMP TABLE......                    
	        SELECT @reccount = COUNT(*)
	        FROM   @tempCourt 
	        
	        -- NOW INSERTING RECORDS FROM TEMP TABLE TO TBLTICKETSVIOLATIONS....                    
	        INSERT INTO tblticketsviolations
	          (
	            ticketid_pk,
	            violationnumber_pk,
	            refcasenumber,
	            courtdate,
	            courtnumber,
	            courtviolationstatusid,
	            courtdatemain,
	            courtnumbermain,
	            courtviolationstatusidmain,
	            courtdatescan,
	            courtnumberscan,
	            courtviolationstatusidscan,
	            courtid,
	            planid,
	            underlyingbondflag
	          )
	        SELECT @ticketid,
	               t.violationnumber_pk,
	               t.refcasenumber,
	               t.courtdate,
	               t.courtnumber,
	               t.courtviolationstatusidmain,
	               t.courtdate,
	               t.courtnumber,
	               t.courtviolationstatusidmain,
	               t.courtdate,
	               t.courtnumber,
	               t.courtviolationstatusidmain,
	               t.courtid,
	               planid = (
	                   SELECT TOP 1 planid
	                   FROM   tblpriceplans
	                   WHERE  courtid = t.Courtid
	                          AND effectivedate <= GETDATE()
	                          AND enddate >  = GETDATE()
	                          AND isActivePlan = 1
	               ),
	               @bondflag
	        FROM   @tempCourt t
	    END        
	    
	    IF @trafficticket <> ''
	    BEGIN
	        DECLARE @TicketNote AS VARCHAR(MAX)
	        DECLARE @EmpAbb VARCHAR(10)
	        
	        SELECT @EmpAbb = abbreviation
	        FROM   tblusers
	        WHERE  employeeid = @empid  
	        
	        SET @TicketNote = 'Online Signup Ticket Number : ' + @trafficticket 
	            + ' (' + dbo.formatdateandtimeintoshortdateandtime(GETDATE()) +
	            '-' + @empabb + ')' 
	        
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	        VALUES
	          (
	            @TicketID,
	            @TicketNote,
	            @empid
	          )    
	        
	        UPDATE t
	        SET    t.generalcomments = ISNULL(t.generalcomments, '') + @TicketNote
	        FROM   tbltickets t
	        WHERE  t.ticketid_pk = @TicketID
	    END
	END 
	
	
	--If Quote Client         
	IF @TicketNo <> 0
	BEGIN
	    UPDATE tbltickets
	    SET    firstname = @FirstName,
	           lastname = @LastName,
	           Address1 = @homeaddress,
	           City = @city,
	           Stateid_FK = @StateID_Fk,
	           Zip = @zip,
	           Contact1 = @telephone,
	           contactType1 = CASE 
	                               WHEN LEN(@telephone) > 0 THEN 1
	                               ELSE 0
	                          END,
	           contact2 = @alttelephone,
	           Contacttype2 = CASE 
	                               WHEN LEN(@alttelephone) > 0 THEN 4
	                               ELSE 0
	                          END,
	           contact3 = @alttelephone2,
	           contacttype3 = CASE 
	                               WHEN LEN(@alttelephone2) > 0 THEN ISNULL(@TelType, 0)
	                               ELSE 0
	                          END,
	           height = @height,
	           WEIGHT = @weight,
	           haircolor = @haircolor,
	           eyes = @eyecolor,
	           DOB = @dob,
	           DLNumber = @driverlicense,
	           dlstate = @dlstate,
	           employeeid = 3992,
	           Race = @race,
	           Gender = @gender,
	           email = @Email,
	           bondflag = @bondflag,
	           bondsrequiredflag = @bondflag,
	           accidentflag = @accident,
	           cdlflag = @cdl,
	           lockflag = 1,
	           TotalFeeCharged = CalculatedTotalFee,
	           EmailNotAvailable = 0,
	           NoDl = CASE 
	                       WHEN LEN(@driverlicense) > 0 THEN 0
	                       ELSE 1
	                  END,
	           WalkInClientFlag = 0,
	           ActiveFlag = 1,
	           hasConsultationComments = 0,
	           AddressConfirmFlag = 1,
	           isPR = 3
	    WHERE  ticketid_pk = @TicketNo                        
	    
	    UPDATE dbo.tblticketsviolations
	    SET    underlyingbondflag = @bondflag
	    WHERE  ticketid_pk = @TicketNo           
	    
	    
	    SET @TicketId = @TicketNo
	END 
	
	
	
	-----------------------------------------------
	--Common Quries For Non / Quote / Court Clients
	-----------------------------------------------        
	
	
	
	
	
	
	-------------------------------------------------------------------------------------------------------
	-- NOW INSERTING PAYMENT INFORMATION..................
	-------------------------------------------------------------------------------------------------------
	-- INSERTING A NOTE IN CASE HISTORY.....                        
	
	INSERT INTO tblticketsnotes
	  (
	    ticketid,
	    SUBJECT,
	    employeeid
	  )
	VALUES
	  (
	    @TicketId,
	    'Internet Client Signup',
	    @empid
	  ) 
	
	-- NOW INSERTING FEE INFORMATION.............          
	UPDATE tbltickets
	SET    basefeecharge = @amount,
	       calculatedtotalfee = @amount,
	       totalfeecharged = @amount,
	       feeinitial = @empdesc,
	       initialadjustmentinitial = @empdesc,
	       lockflag = 1,
	       initialadjustment = 0,
	       adjustment = 0,
	       ContinuanceAmount = 0,
	       InitialTotalFeeCharged = @amount,
	       employeeidupdate = 3992
	WHERE  ticketid_pk = @ticketid 
	
	--------------------------------------------------------------------------------------------------------------------
	--    UPDATING CASE STATUS & COURT INFO IN TBLTICKETS....
	--------------------------------------------------------------------------------------------------------------------
	-- Agha Usman 2664 06/10/2008
	
	--update t
	--   set t.datetypeflag = c.CategoryID,
	--    t.CurrentDateset = v.courtdatemain,
	--    t.CurrentCourtNum = v.courtnumbermain,
	--    t.CurrentCourtloc = v.courtid
	--   FROM    tblTickets t
	--   INNER JOIN
	--                tblTicketsViolations v
	--   ON  t.TicketID_PK = v.TicketID_PK
	--   INNER JOIN
	--                tblCourtViolationStatus c
	--   ON  v.CourtViolationStatusIDmain = C.CourtViolationStatusID
	--   where v.courtdatemain = (
	--     select min(courtdatemain) from tblticketsviolations v2
	--     where v2.ticketid_pk = t.ticketid_pk
	--     )
	--   and t.ticketid_pk = @TicketID                    
	
	--------------------------------------------------------------------------------------------------------------------
	--    INSERTING PAYMENT RECORDS.........
	--------------------------------------------------------------------------------------------------------------------                      
	
	INSERT INTO tblticketspayment
	  (
	    paymenttype,
	    ticketid,
	    chargeamount,
	    paymentvoid,
	    recdate,
	    employeeid,
	    BranchID,
	    IPAddress
	  )
	VALUES
	  (
	    7,
	    @TicketId,
	    @amount,
	    0,
	    GETDATE(),
	    @empid,
	    2, -- Sabir Khan 10920 05/27/2013 default branch is Houston 2020
	    ''
	  )                    
	
	SELECT @InvoiceNumber = SCOPE_IDENTITY() 
	
	--Added By Zeeshan Ahmed For TrackPayment            
	UPDATE tblticketscybercashlog
	SET    invnumber = CONVERT(VARCHAR, @InvoiceNumber),
	       reftransnum = ''
	WHERE  ticketid = @customerid
	       AND transtype = 0 
	
	-- INSERTING CREDIT CARD INFORMATION....                    
	INSERT INTO dbo.tblticketscybercash
	  (
	    ticketid,
	    invnumber,
	    amount,
	    ccnum,
	    expdate,
	    NAME,
	    recdate,
	    STATUS,
	    transnum,
	    retcode,
	    errmsg,
	    employeeid,
	    approvedflag,
	    clearflag,
	    cin,
	    cardnumber,
	    response_subcode,
	    response_reason_code,
	    response_reason_text,
	    auth_code,
	    avs_code,
	    cardtype
	  )
	VALUES
	  (
	    @ticketid,
	    @InvoiceNumber,
	    @amount,
	    @crnumber,
	    CONVERT(VARCHAR, @crmonth) + '/' + CONVERT(VARCHAR, @cryear),
	    @crname,
	    GETDATE(),
	    @status,
	    @transnum,
	    '',	--@retcode,--NA Comment By zeeshan        
	    
	    @errmsg,
	    @empid,
	    @approvedflag,
	    0,	--@clearflag, --NA        
	    
	    @csc,
	    @crnumber,
	    @response_subcode,
	    @response_reason_code,
	    @response_reason_text,
	    @auth_code,
	    @avs_code,
	    @crtype
	  ) 
	
	
	--------------------------------------------------------------------------------------------------------------------
	--    INSERTING INTERNET CLIENT RECEIPT IMAGE TO SCANNED DOCS
	--------------------------------------------------------------------------------------------------------------------                      
	
	DECLARE @BookID INT,
	        @picID INT,
	        @cmd VARCHAR(255),
	        @notes VARCHAR(500)
	
	INSERT INTO tblScanBook
	  (
	    EmployeeID,
	    TicketID,
	    DocTypeID,
	    updatedatetime,
	    DocCount,
	    ResetDesc
	  )
	VALUES
	  (
	    @empid,
	    @ticketid,
	    11,
	    GETDATE(),
	    1,
	    'INTERNET CLIENT SIGNUP RECEIPT'
	  ) -- insert into book                          
	
	SELECT @BookID = SCOPE_IDENTITY() --Get Book Id of the latest inserted                          
	
	INSERT INTO tblScanPIC
	  (
	    ScanBookID,
	    updatedatetime,
	    DocExtension
	  )
	VALUES
	  (
	    @BookID,
	    GETDATE(),
	    'PDF'
	  ) --insert new image into database                          
	SELECT @picID = SCOPE_IDENTITY() --Get Book Id of the latest inserted
	                                 --Docpic = @image          
	
	SET @notes = '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID=' + CONVERT(VARCHAR(10), @BookID) +
	    '&RecType=1&DocNum=0&DocExt=PDF'');void('''');"''>SCANNED DOCUMENT - OTHER [INTERNET CLIENT SIGNUP RECEIPT]</a>'
	
	INSERT INTO tblTicketsNotes
	  (
	    TicketID,
	    SUBJECT,
	    EmployeeID
	  )
	VALUES
	  (
	    @TicketID,
	    @notes,
	    @EMPID
	  )                     
	
	SET @ReceiptFileName = CONVERT(VARCHAR(10), @bookid) + '-' + CONVERT(VARCHAR(10), @picid) 
	    + '.PDF' 
	
	--------------------------------------------------------------------------------------------------------------------
	--    INSERTING HISTORY NOTES
	--------------------------------------------------------------------------------------------------------------------                      
	
	DECLARE @strNote VARCHAR(100)                
	SELECT @strNote = 'INITIAL INQUIRY ' + UPPER(ISNULL(tc.shortname, 'N/A')) +
	       ':' + UPPER(ISNULL(c.ShortDescription, 'N/A')) + ':' + CONVERT(VARCHAR(10), ISNULL(v.CourtDateMain, '1/1/1900'), 101) 
	       + ' #' + ISNULL(v.courtnumbermain, '0') + ' @ ' 
	       + UPPER(dbo.formattime(ISNULL(v.CourtDateMain, '1/1/1900')))
	FROM   tblTicketsViolations v
	       LEFT OUTER JOIN tblCourtViolationStatus c
	            ON  v.CourtViolationStatusIDmain = c.CourtViolationStatusID
	       LEFT OUTER  JOIN tblcourts tc
	            ON  v.courtid = tc.courtid
	WHERE  v.ticketid_pk = @TicketID                
	
	IF @strNote IS NOT NULL
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        SUBJECT,
	        employeeid
	      )
	    VALUES
	      (
	        @TicketID,
	        @strNote,
	        @empid
	      ) 
	
	
	--------------------------------------------------------------------------------------------------------------------
	--    UPDATING CLIENT FLAG IN TBLTICKETSARHCIVE
	--------------------------------------------------------------------------------------------------------------------            
	IF @recordid <> 0
	BEGIN
	    UPDATE tblticketsarchive
	    SET    clientflag = 1
	    WHERE  recordid = @Recordid
	END 
	
	--------------------------------------------------------------------------------------------------------------------
	--     Added By Zeeshan For Mailer ID for 2nd June 2007 Mailer ID Modifications
	--------------------------------------------------------------------------------------------------------------------            
	IF @MailerID <> 0
	BEGIN
	    EXEC USP_HTS_Update_Mailer_id @TicketID,
	         @empid,
	         1
	END 
	
	
	
	---Update Transaction Log Information  
	UPDATE tblTicketsCybercashLog
	SET    TicketID = @ticketid,
	       invnumber = @InvoiceNumber
	WHERE  invnumber = @customerid
	       AND transnum = @transnum 
	
	-- Noufil 6126 07/24/2009 Update new hire status of client
	EXEC [dbo].[USP_HTP_UPDATE_NEWHIRESTATUS] 1,
	     @TicketID,
	     3992,
	     1 
	
	--Commit Transaction        
	COMMIT TRANSACTION         
	
	
	SELECT @ReceiptFileName AS ReceiptFileName,
	       1 AS ApprovedFlag,
	       @ticketid AS TicketID,
	       @InvoiceNumber AS InvoiceNumber  



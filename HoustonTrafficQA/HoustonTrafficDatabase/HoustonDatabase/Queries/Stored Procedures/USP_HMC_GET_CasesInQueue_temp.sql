SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_GET_CasesInQueue_temp]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_GET_CasesInQueue_temp]
GO







CREATE Procedure [dbo].[USP_HMC_GET_CasesInQueue_temp] --3992         
(          
 @empid int          
)          
          
as          
          
 SELECT         
         
   thef.id,      
   thef.TicketNumber_fK,     
   thef.ViolationNumber_fk,     
   thef.Flag,      
   ta.MidNumber,         
 (case   
 when len(tva.ViolationDescription) > 20 then left(tva.ViolationDescription,20) + '...'  
 else tva.ViolationDescription end )  
 as ViolationDescription  
   ,         
   tva.FineAmount,         
   ta.LastName +' '+ ta.FirstName as FullName,   
   ta.FirstName,         
   ta.LastName,         
   ta.Initial,         
   tva.CauseNumber,         
   ta.DOB,         
   tva.TicketViolationDate,         
   tva.Courtnumber,         
   tva.CourtDate,         
   tva.RecordID,         
   ISNULL(tcv.ShortDescription, N'N/A') AS ShortDescription ,    
   Case when Flag > 0 then 'PLEA OF NOT GUILTY' end as Documents       
         
  FROM            
    
tblticketsviolationsarchive tva    
join tblticketsarchive ta    
on tva.recordid = ta.recordid    
Join tbl_hmc_efile thef    
on tva.ticketnumber_pk = thef.ticketnumber_fk    
and tva.violationnumber_pk = thef.violationnumber_fk    
join dbo.tblCourtViolationStatus AS tcv    
on    
tcv.CourtViolationStatusID = tva.violationstatusid    
where thef.empid  = @empid      
and thef.flag > -1     
order by tva.recordid         
          


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


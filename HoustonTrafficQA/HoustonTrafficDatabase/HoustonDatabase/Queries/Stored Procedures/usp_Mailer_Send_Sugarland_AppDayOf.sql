
/****** 
* Created By: Sabir Khan
Business Logic:	The procedure is used by LMS application to get data for Sugarland Appearance Day of letter
				and to create LMS history for this letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Comma separated dates selected to print letter.
	@EmpId:			Employee id for the currently logged in user
	@PrintType:		Flag to just preview the letter or to mark letters as sent. If 1 then LMS letter history will be created.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date
	@isPrinted:		Flag to include already printed letters in the batch.

List of Columns:	
	LetterId:		Letter ID generated for each letter. It will be "NOT PRINTABLE"	if @PrintType = 0
	RecordId:		Identity value to look up the case in non-clients. used to group the records in report file.
	FirstName:		Person's first name that will appear on the letter
	LastName:		Person's last name that will appear on the letter
	Address:		Person's Home address that will appear on the letter
	City:			Person's Home City that will appear on the letter
	State:			Person's Home State that will appear on the letter
	FineAmount:		Fine amount for each violation that will appear on the letter
	ViolationDescritpion: violation description that will appear on the letter
	CourtName:		Name of court with which the violation is associated. Used in report file
	DPC:			Address status that is used in barcode font on the letter
	DPTwo:			Address status that is used in barcode font on the letter
	TicketNumber_pk:Case number for the violation
	ZipCode:		Person's Home Zip code.
	CourtDate:		Court date associated with the violation
	Abb:			Short abbreviation of employee who is printing the letter


******/

-- usp_Mailer_Send_Sugarland_AppDayOf 9,75,9,'4/27/2010,06/17/2010',3991,0,0,0
Alter Procedure [dbo].[usp_Mailer_Send_Sugarland_AppDayOf]
@catnum int=3,                                    
@LetterType int=9,                                    
@crtid int=1,                                    
@startListdate varchar (500) ='01/04/2006',                                    
@empid int=2006,                              
@printtype int =0,
@searchtype int,
@isprinted bit                           
                                    
                                    
as                                    
                          
-- DECLARING LOCAL VARIABLES FOR THE FILTERS....                
declare @officernum varchar(50),                                    
@officeropr varchar(50),                                    
@tikcetnumberopr varchar(50),                                    
@ticket_no varchar(50),                                                                    
@zipcode varchar(50),                                             
@zipcodeopr varchar(50),                                    
                                    
@fineamount money,                                            
@fineamountOpr varchar(50) ,                                            
@fineamountRelop varchar(50),                   
              
@singleviolation money,                                            
@singlevoilationOpr varchar(50) ,                                            
@singleviolationrelop varchar(50),                                    
                                    
@doubleviolation money,                                            
@doublevoilationOpr varchar(50) ,                                            
@doubleviolationrelop varchar(50),                                  
@count_Letters int,                  
@violadesc varchar(500),                  
@violaop varchar(50),
@endlistdate VARCHAR (500)                                     

-- DECLARING & INITIALIZING LOCAL VARIABLES FOR DYNAMIC SQL...                                    
declare @sqlquery varchar(max),@sqlquery2 varchar(max)
Select @sqlquery =''  , @sqlquery2 = ''
                                            
-- ASSIGNING VALUES TO THE VARIABLES 
-- BY GETTTING INFORMATION FROM LMS FILTER STRUCTURE...
-- FOR THIS LETTER TYPE....
Select @officernum=officernum,                                    
@officeropr=oficernumOpr,                                    
@tikcetnumberopr=tikcetnumberopr,                                    
@ticket_no=ticket_no,                                                                      
@zipcode=zipcode,                                    
@zipcodeopr=zipcodeLikeopr,                                    
@singleviolation =singleviolation,                                    
@singlevoilationOpr =singlevoilationOpr,                                    
@singleviolationrelop =singleviolationrelop,                                    
@doubleviolation = doubleviolation,                                    
@doublevoilationOpr=doubleviolationOpr,                                            
@doubleviolationrelop=doubleviolationrelop,                  
@violadesc=violationdescription,                  
@violaop=violationdescriptionOpr ,              
@fineamount=fineamount ,                                            
@fineamountOpr=fineamountOpr ,                                            
@fineamountRelop=fineamountRelop              
                                 
from tblmailer_letters_to_sendfilters                                    
where courtcategorynum=@catnum                                    
and Lettertype=@LetterType                                  


-- GETTING THE SHORT ABBREVIATION OF THE CURRENTLY LOGGED IN 
-- EMPLOYEE IN THE LOCAL VARIABLE....
declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid
                                    
-- CREATING DYNAMIC SQL FOR THE MAIN QUERY...
-- Afaq 8168 08/19/2010 replace tva.DLQUpdateDate with tva.courtdate
set @sqlquery=@sqlquery+'
SELECT DISTINCT ta.recordid,
       CONVERT(VARCHAR(10), tva.courtdate, 101) AS listdate,
       CONVERT(VARCHAR(10), tva.TicketViolationDate, 101) AS OffenceDate,
       tva.courtdate,
       tva.courtlocation courtid,
       flag1 = ISNULL(ta.Flag1, ''N''),
       ta.officerNumber_Fk,
       --Sabir Khan 9683 09/15/2011 Need to remove last two character from ticket number and cancatinate -0x                                          
	   SUBSTRING(tva.TicketNumber_PK,0,LEN(tva.ticketnumber_pk)-1) + ''-0x'' as ticketnumber_pk,
       ta.donotmailflag,
       ta.clientflag,
       UPPER(firstname) AS firstname,
       UPPER(lastname) AS lastname,
       UPPER(address1) + '''' + ISNULL(ta.address2, '''') AS ADDRESS,
       UPPER(ta.city) AS city,
       s.state,
       dbo.tblCourts.CourtName,
       zipcode,
       midnumber,
       dp2 AS dptwo,
       dpc,
       violationdate,
       violationstatusid,
       LEFT(zipcode, 5) + RTRIM(LTRIM(midnumber)) AS zipmid,
       ViolationDescription,
       -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . .  
       -- CASE  WHEN ISNULL(tva.fineamount, 0) = 0 THEN 100 ELSE tva.fineamount END AS FineAmount,
       ISNULL(tva.fineamount, 0) AS FineAmount, 
       COUNT(tva.ViolationNumber_PK) AS ViolCount,
       SUM(ISNULL(tva.fineamount, 0)) AS Total_Fineamount
       INTO #temp
FROM   dbo.tblTicketsViolationsArchive tva
       INNER JOIN dbo.tblTicketsArchive ta ON  tva.RecordID = ta.RecordID
       INNER JOIN dbo.tblCourts ON  tva.courtlocation = dbo.tblCourts.Courtid
       INNER JOIN tblstate S ON  ta.stateid_fk = S.stateID
       -- Afaq 8168 08/17/2010 Add join with tblCourtViolationStatus
       INNER JOIN tblCourtViolationStatus tcvs ON tcvs.CourtViolationStatusID = tva.violationstatusid
                
       -- ONLY VERIFIED AND VALID ADDRESSES
WHERE  Flag1 IN (''Y'', ''D'', ''S'') 
       
       --ONLY SUGARLAND CASES...
       AND (tva.insertionloaderid = 30 OR tva.UpdationLoaderId = 30)
           
           -- NOT MARKED AS STOP MAILING...
       AND donotmailflag = 0 
           
           -- PERSONS ADDRESS IS NOT CHANGED IN PAST 48 MONTHS (ACCUZIP)...
       AND ISNULL(ta.ncoa48flag, 0) = 0
       
       -- Afaq 8168 08/17/2010  
       -- show court violation category of type 2 eg: ARRAIGNMENT ,APPEARANCE etc
       AND tcvs.CategoryID = 2
       -- Show only past court date records
       AND DATEDIFF(DAY, tva.CourtDate,GETDATE()) > 0'                                    
          
-- IF PRINTING FOR ALL COURTS OF THE SELECTED COURT CATEGORY....
if(@crtid = @catnum)              
	set @sqlquery=@sqlquery+' 
	and 	tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = '+ convert(varchar(10),@crtid) + ' )'                  

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
else 
	set @sqlquery =@sqlquery +' 
	and 	tva.courtlocation = '+ convert(varchar(10),@crtid)
                                  
-- FOR THE SELECTED DATE RANGE ONLY.....
-- Afaq 8168 08/19/2010 replace tva.DLQUpdateDate with tva.courtdate 
                                 
if(@startListdate<>'')                                    
	set @sqlquery=@sqlquery+'
	and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.CourtDate' ) 

                                   
-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....
if(@officernum<>'')                                    
	set @sqlquery =@sqlquery + ' 
	and 	ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                     
              
              
-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE TICKET NUMBER...                                    
if(@ticket_no<>'')                                    
	set @sqlquery=@sqlquery+ ' 
	and  	('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                          
                         
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....
if(@zipcode<>'')                                                          
	set @sqlquery=@sqlquery+ ' 
	and	('+  dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'               

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTION
if(@violadesc<>'')              
	set @sqlquery=@sqlquery+' 
	and	('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'           

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )              
	set @sqlquery =@sqlquery+ '  
	and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
--Sabir Khan 7601 03/22/2010 Exclude clients whose DLQ letter is sent in past 5 business days.
if(@fineamount<>0 and @fineamountOpr = 'not'  )              
	set @sqlquery =@sqlquery+ '  
	and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                   

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ ' 
group by ta.recordid, convert(varchar(10),tva.courtdate,101), tva.courtdate, tva.courtlocation, ta.Flag1, ta.officerNumber_Fk,
tva.TicketNumber_PK, ta.donotmailflag, ta.clientflag, violationstatusid, upper(firstname) , upper(lastname) , upper(address1) + '''' + isnull(ta.address2,'''') ,
upper(address1), isnull(ta.address2,''''), upper(ta.city),s.state,  tblCourts.CourtName, zipcode, midnumber, dp2,
dpc, violationstatusid,	violationdate, left(zipcode,5) + rtrim(ltrim(midnumber)), ViolationDescription, FineAmount,tva.DLQUpdateDate,tva.TicketViolationDate  
'              
 
-- GETTING THE FINAL RESULSTS IN ANOTHER TEMP TABLE.....	                           
set @sqlquery=@sqlquery+' 

Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate,OffenceDate, FirstName,LastName,address,                              
city,state,FineAmount,violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                     
courtid,zipmid,donotmailflag,clientflag, zipcode, courtdate into #temp1  from #temp where 1=1                                   
'              

set @sqlquery=@sqlquery+' 
-- Afaq 8135 08/07/2010 no letter of other mailer type will be send in the next 5 working days from th previous sent date. 
Delete from #temp1
FROM #temp1 a INNER JOIN tblletternotes n ON n.RecordID = a.recordid 
INNER JOIN tblletter l ON l.LetterID_PK = n.LetterType 
where 	lettertype ='+convert(varchar(10),@lettertype)+' 
or (
		l.courtcategory = 9
		AND datediff(day, recordloaddate, dbo.fn_getprevbusinessday(GETDATE(),5)) <=0
	)
		
		
   
   -- EXCLUDE CLIENT CASES
	delete from #temp1 where recordid in (
	select v.recordid from tblticketsviolations v inner join tbltickets t 
	on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
	inner join #temp1 a on a.recordid = v.recordid
	)

'

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)              
	set @sqlquery =@sqlquery+ '
	AND  '+ @singlevoilationOpr+ '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1) or violcount>3                                            
    '              
              
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...
if(@doubleviolation<>0 and @singleviolation=0 )              
	set @sqlquery =@sqlquery + '
	AND'+ @doublevoilationOpr+ '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)  or violcount>3  
	'

-- BOTH SINGLE & DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE AND SINGLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...              
if(@doubleviolation<>0 and @singleviolation<>0)              
	set @sqlquery =@sqlquery+ '
	AND'+ @singlevoilationOpr+ '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)
    OR '+ @doublevoilationOpr+ '  (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2) or violcount>3  '
    
    
 -- Rab Nawaz Khan 9704 09/28/2011 added to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 
 set @sqlquery2 =@sqlquery2 +  '
 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1   WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid   
    
	'
  
-- IF "MARK LETTER AS SENT" OPTION IS SELECTED....
-- CREATE LETTER HISTORY FOR EACH PERSON....         
if( @printtype  = 1)         
	BEGIN
		set @sqlquery2 =@sqlquery2 +                                    
		'
		Declare @ListdateVal DateTime, @totalrecs int, @count int,                                
		@p_EachLetter money, @recordid varchar(50),
		@zipcode   varchar(12),	@maxbatch int  ,@lCourtId int  ,
		@dptwo varchar(10),@dpc varchar(10)
		declare @tempBatchIDs table (batchid int)

		-- GETTING TOTAL LETTERS AND COSTING ......
		Select @totalrecs =count(*) from #temp1 where listdate = @ListdateVal                                 
		Select @count=Count(*) from #temp1                                
		if @count>=500                                
			set @p_EachLetter=convert(money,0.3)                                
		if @count<500                                
			set @p_EachLetter=convert(money,0.39)                                

		-- DECLARING CURSOR TO INSERT THE RECORDS IN LETTER HISTORY ONE BY ONE.....
		-- THERE WILL A SEPARATE BATCH FOR EACH LIST/COURT DATE VALUE ......
		Declare ListdateCur Cursor for                                  
		Select distinct listdate  from #temp1                                
		open ListdateCur                                   
		Fetch Next from ListdateCur into @ListdateVal                                                      
		while (@@Fetch_Status=0)                              
			begin                                

				-- GETTING TOTAL LETTERS FOR THE LIST/COURT DATE...
				Select @totalrecs =count(distinct recordid) from #temp1 where listdate = @ListdateVal                                 

				-- INSERTING RECORD IN BATCH TABLE......                               
				insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) 
				values ('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@catnum)+'  , 0, @totalrecs, @p_EachLetter)                                   

				-- GETTING BATCH ID OF THE INSERTED RECORD.....
				Select @maxbatch=Max(BatchId) from tblBatchLetter  
				insert into @tempBatchIDs select @maxbatch                                                    

				-- CREATING CURSOR TO INSERT RECORDS FOR EACH LETTER FOR THE NEWLY INSERTED BATCH....
				Declare RecordidCur Cursor for                               
				Select distinct recordid,zipcode, courtid, dptwo, dpc from #temp1 where listdate = @ListdateVal                               
				open RecordidCur                                                         
				Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                                          
				while(@@Fetch_Status=0)                              
					begin                              

						-- INSERTING LETTERS IN LETTER HISTORY FOR THE SPECIFIED BATCH...
						insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc)                              
						values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc)                              
						Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc                          
					end                              
				close RecordidCur 
				deallocate RecordidCur                                                               
				Fetch Next from ListdateCur into @ListdateVal                              
			end                                            

		close ListdateCur  
		deallocate ListdateCur

		-- OUTPUTTING THE DATA TO THE APPLICATION FOR REPROT FILE.....		 
		Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName,                              
		t.LastName,t.address,t.city, t.state,FineAmount,OffenceDate, violationdescription,CourtName, t.dpc,                              
		dptwo, TicketNumber_PK, t.zipcode, courtdate , '''+@user+''' as Abb, isfineamount                                     
		from #temp1 t , tblletternotes n, @tempBatchIDs tb
		where t.recordid = n.recordid and t.courtid = n.courtid and n.batchid_fk = tb.batchid and n.lettertype = '+convert(varchar(10),@lettertype)+' 
		order by T.recordid 
		-- Afaq 8168 08/20/2010 Update email subject.
		-- GENERATING AND SENDING EMAIL FOR LETTER PRINTING NOTIFICATION........		
		declare @lettercount int, @subject varchar(200), @body varchar(400) , @sql varchar(500)
		select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
		select @subject = convert(varchar(20), @lettercount) +  '' LMS Sugarland Appearance Day Of Letters Printed''
		select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
		exec usp_mailer_send_Email @subject, @body

		-- OUTPUTING THE PDF FILE NAME TO THE APPLICATION...
		declare @batchIDs_filname varchar(500)
		set @batchIDs_filname = ''''
		select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
		select isnull(@batchIDs_filname,'''') as batchid

		'
	END
else if( @printtype  = 0)         

	-- IF "MARK LETTER AS SENT" OPTION IS NOT SELECTED....
	-- THEN DO NOT CREATE LETTER HISTORY. JUST OUTPUT THE DATA FOR REPORT FILE 
	-- TO DISPLAY THE LETTER...
	BEGIN
		set @sqlquery2 = @sqlquery2 + '
		Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,                              
		LastName,address,city, state,FineAmount,OffenceDate, violationdescription,CourtName, dpc,                              
		dptwo, TicketNumber_PK, zipcode , courtdate , '''+@user+''' as Abb, isfineamount                                   
		from #temp1 
		order by recordid 
		'
	END

-- DROPPING TEMPORARY TABLES ....
set @sqlquery2 = @sqlquery2 + '

drop table #temp 
drop table #temp1'


-- EXECUTING THE DYNAMIC SQL ....
exec(@sqlquery + @sqlquery2)


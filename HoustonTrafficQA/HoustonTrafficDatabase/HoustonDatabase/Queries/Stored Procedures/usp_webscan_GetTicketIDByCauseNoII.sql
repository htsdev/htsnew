﻿ALTER procedure [dbo].[usp_webscan_GetTicketIDByCauseNoII]   

--ozair 4330 07/02/2008 causeno length increased to 200 from 50
@CauseNo varchar(200)            
            
as            
            
select distinct tv.ticketid_pk as TicketID,tv.courtdatemain,tv.courtid,cvs.categoryid             
from tblticketsviolations  tv inner join       
tbltickets t on       
t.ticketid_pk=tv.ticketid_pk inner join          
tblcourtviolationstatus cvs on           
cvs.courtviolationstatusid=tv.courtviolationstatusidmain          
where t.activeflag=1      
and casenumassignedbycourt in (select sValue from dbo.splitstring(@CauseNo,','))
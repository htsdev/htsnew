  
/******       
      
Create by:  Sarim Ghani      
      
Business Logic : This procedure is used to get violation details of the case.      
      
List of Parameters:           
      
     TicketID_PK : Ticket Id of the case. (i.e. primary key of the case).       
      
List of Columns:       
      
  TicketID_PK :       
  ViolationNumber_PK :      
     CategoryID :      
     FineAmount :      
     BondInvoice :      
     RefCaseNumber :      
     TicketsViolationID :      
     CourtDate :      
     CourtNumber :      
     CourtViolationStatusID :      
     CourtNumbermain :      
     CourtViolationStatusIDmain :      
     CourtDateMain :      
     CourtDateScan :      
     CourtViolationStatusIDScan :      
     CourtNumberscan  :      
     PlanID :      
     BondFlag :      
     violationDescription :      
     ShortName :      
     AutoCourtDesc :      
     VerCourtDesc :      
     ScanCourtDesc :      
     AutoDate :      
     VerifiedDate :      
     ScanDate :      
     ulstatus :      
     PlanDescription :      
     CauseNumber :      
     OfficerName :      
     OfficerDay :      
     AutoStatus :      
     VerifiedStatus  :      
     ScanStatus  :      
     Discrepency :      
     CourtId :      
     OscarCourtDetail :      
     ChargeLevel :      
     IsCriminalCourt :      
     CDI :      
     LevelCode :      
     CourtAddress :      
     ClientAddress :      
     hasFTAViolations :      
     isFTAViolation :       
     ArrestingAgencyName : Arresting Agency Name associated with the violation      
     CoveringFirmId: Covering Firm Id Which Cover The Case      
     ArrestDate : Arrest Date on which client arrest.      
     CaseTypeID : Case Type Of Case    
     MissedCourtType : Get Missed Court Type     
      
******/      
      
--USP_HTS_GetCaseInfoDetail 166314 
      
ALTER Procedure [dbo].[USP_HTS_GetCaseInfoDetail]              
@ticketid int                                                                                
                                                                                
as                                                                                   
               
--Zeeshan Ahmed 3535 04/08/2008      
--FTA Modifications      
declare @HasFTAViolations int       
select @HasFTAViolations = dbo.fn_CheckFTAViolationInCase(@ticketid)      
      
                                                                       
SELECT                                                           
TV.TicketID_PK,                                                       
TV.ViolationNumber_PK,                                                       
tblCourtViolationStatus_1.CategoryID,                                                       
TV.FineAmount,        
--TV.BondInvoice, -- Sarim 2664 06/04/2008     
TV.BondAmount,
CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(TV.RefCaseNumber, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(TV.RefCaseNumber, 'N/A'))))
ELSE 'N/A' END AS RefCaseNumber,--Fahad 5368 12/30/2008 -- Sarim 2664 06/04/2008         
TV.TicketsViolationID,                                                       
TV.CourtDate,                                                       
TV.CourtNumber,                                                                       
TV.CourtViolationStatusID,
--Waqas Javed 4841 12/29/08 Sorting Feature enabled 
--Sabir Khan 7979 07/02/2010 No need to convert into int.                                                         
case when  (len(TV.CourtNumbermain) >0) and (isnumeric(TV.CourtNumbermain) = 1) Then TV.CourtNumbermain 
WHEN (len(TV.CourtNumbermain) >0) and (tv.CourtID = 3060) THEN TV.CourtNumbermain else '0' end as CourtNumbermain,                                                       
isnull(TV.CourtViolationStatusIDmain,0)as CourtViolationStatusIDmain,                                                       
isnull(TV.CourtDateMain,'')as CourtDateMain,                                                       
isnull(TV.CourtDateScan,'')as CourtDateScan,                                                       
isnull(TV.CourtViolationStatusIDScan,0)as CourtViolationStatusIDScan,                                                                       
isnull(TV.CourtNumberscan,'')as CourtNumberscan,                                                       
isnull(TV.PlanID,45) as PlanID,                                                                       
(case                  
 when TV.UnderlyingBondFlag  = 1 then 'YES'                                                                              
 when isnull(TV.UnderlyingBondFlag,0)  = 0 then 'NO'                                                                
 end ) as BondFlag,                                 
upper(m.Description +                                     
(case                                    
when tv.violationnumber_pk = 0 then ''                                    
else  ' ' + '(' + ISNULL(vc.ShortDescription, 'N/A')+ ')'                                     
end )) as violationDescription,                                      
C.ShortName,
tblCourtViolationStatus_1.ShortDescription AS AutoCourtDesc,                                                       
isnull(tblCourtViolationStatus_2.ShortDescription,'') AS VerCourtDesc,                                                                        
isnull(tblCourtViolationStatus_3.ShortDescription,'') AS ScanCourtDesc,                                                       
                                                  
'A: ' + tblCourtViolationStatus_1.ShortDescription + ' ' + CONVERT(varchar, TV.CourtDate,101) + ' ' + SUBSTRING(CONVERT(varchar, TV.CourtDate), 13, 7) + ' #' + case                                                 
when  isnumeric(isnull(TV.CourtNumber,'')) = 1 then convert(varchar(4),TV.CourtNumber) ---- Sabir Khan 7979 07/02/2010 No need to convert into int.              
when  isnumeric(isnull(TV.CourtNumber,'')) = 0 then convert(varchar(4),isnull(TV.CourtNumber,''))                                                
end  AS AutoDate,                                                      
'V: ' + tblCourtViolationStatus_2.ShortDescription + ' ' + CONVERT(varchar, TV.CourtDateMain, 101) + ' ' + SUBSTRING(CONVERT(varchar,TV.CourtDateMain), 13, 7) + ' #' + 
-- Sabir Khan 7979 07/02/2010 No need to convert into int.                                           
convert(varchar(4),isnull(TV.CourtNumbermain,''))  as VerifiedDate,                                   
'S: ' + tblCourtViolationStatus_3.ShortDescription + ' ' + CONVERT(varchar, TV.CourtDateScan, 101) + ' ' + SUBSTRING(CONVERT(varchar, TV.CourtDateScan), 13, 7) + ' #' + case                               
when  isnumeric(isnull(TV.CourtNumberscan,'')) = 1 then convert(varchar(4),TV.CourtNumberscan)  -- Sabir Khan 7979 07/02/2010 No need to convert into int.                                                 
when  isnumeric(isnull(TV.CourtNumberscan,'')) = 0 then convert(varchar(4),isnull(TV.CourtNumberscan,''))                                                
end  AS ScanDate,                                       
tblCourtViolationStatus_2.categoryid as ulstatus,                                                                       
isnull( pp.PlanShortName,'') as PlanDescription,
CASE WHEN (LEN(RTRIM(LTRIM(ISNULL(casenumassignedbycourt, 'N/A'))))) > 0 THEN (RTRIM(LTRIM(ISNULL(casenumassignedbycourt, 'N/A'))))
ELSE 'N/A' END AS CauseNumber,--Fahad 5368 12/30/2008                                 
ltrim(isnull(o.LastName,'')+' '+isnull(o.FirstName,'')) as OfficerName,                                       
(case isnull(o.OfficerType,'N/A')                                   
 when 'MONDAY' then 'M'                                  
 when 'TUESDAY' then 'TU'                                  
 when 'WEDNESDAY' then 'W'                                  
 when 'THURSDAY' then 'TH'                                  
 when 'FRIDAY' then 'F'                                  
 when 'SATURDAY' then 'SA'                                  
 when 'SUNDAY' then 'SU'                                  
 when 'N/A' then 'N/A'                                  
end)                                  
as OfficerDay,                        
tblCourtViolationStatus_1.courtviolationstatusid as AutoStatus,                        
tblCourtViolationStatus_2.courtviolationstatusid as VerifiedStatus,                        
tblCourtViolationStatus_3.courtviolationstatusid as ScanStatus,                      
Convert(int,dbo.Fn_CheckDiscrepency(TV.TicketsViolationID)) as Discrepency  ,    
--Yasir Kamal 6073 06/24/2009 Null Courtid Bug fixed.                
ISNULL(TV.CourtID,0) AS CourtID,                
isnull(TV.OscarCourtDetail,'') as OscarCourtDetail,            
isnull(TV.ChargeLevel,0) as ChargeLevel,            
Convert ( int ,c.IsCriminalCourt) as IsCriminalCourt,          
isnull ( TV.CDI  ,'0' ) as CDI ,      
isnull(CL.LevelCode ,'') as LevelCode,      
c.Address +',' +c.City +','+st.State+',' + c.Zip as CourtAddress,            
t.Address1+',' +t.City +','+st2.State+','+t.Zip as ClientAddress,      
@HasFTAViolations as hasFTAViolations,      
dbo.fn_CheckFTAViolation(TV.TicketsViolationID) as isFTAViolation,      
isnull(TV.ArrestingAgencyName,'N/A') as  ArrestingAgencyName ,       
TV.CoveringFirmId ,      
isnull(TV.ArrestDate ,'1/1/1900') as ArrestDate  ,     
isnull(TV.MissedCourtType,-1) as    MissedCourtType,    
isnull(T.CaseTypeID ,1) as  CaseTypeID ,
ISNULL(c.courtcategorynum,0) AS crtCategory, --Sabir Khan 8862 02/22/2011 Added for HCJP Category.   
CASE c.LOR WHEN 0 THEN 'Fax' WHEN 1 THEN 'Certified Mail' WHEN 2 THEN 'Hand Delivery' WHEN 3 THEN 'Mail' ELSE 'Other' END LOR,  -- Sabir Khan 9031 03/15/2011 Fixed LOR issue for PMC and all other courts having Fax as LOR Method on Court interface.
--Abbas Shahid 9215 04/29/2011 Added contact information field for criminal case summary email.
ISNULL(t.Contact1,'') AS Contact1, ISNULL(t.Contact2,'') AS Contact2, ISNULL(t.Contact3,'') AS Contact3,
-- Rab Nawaz Khan 10349 08/30/2013 Criminal cases info has been added
CONVERT(VARCHAR(100), '') AS JailTimePeriod, CONVERT(VARCHAR(100), '') AS TypeOfJail, CONVERT(MONEY, 0) AS MaxFineAmount, CONVERT(BIT, 0) AS isNewClient, t.Recdate
INTO #temp
FROM  dbo.tblTicketsViolations AS TV LEFT OUTER JOIN       -- TAHIR 6051  06/17/2009 FIXED THE JOIN BUG
 --Yasir Kamal 6073 06/24/2009 Null Courtid Bug fixed.
 dbo.tblCourts AS c ON TV.CourtID = c.Courtid LEFT OUTER JOIN      
 dbo.tblTickets AS t ON TV.TicketID_PK = t.TicketID_PK LEFT OUTER JOIN      
 dbo.tblViolations m ON TV.ViolationNumber_PK = m.ViolationNumber_PK and m.violationtype <> 1  LEFT OUTER JOIN      
 dbo.tblState AS st ON c.State = st.StateID LEFT OUTER JOIN      
 dbo.tblState AS st2 ON t.Stateid_FK = st2.StateID LEFT OUTER JOIN      
 dbo.tblOfficer AS o ON t.OfficerNumber = o.OfficerNumber_PK LEFT OUTER JOIN      
 dbo.tblViolationCategory AS vc ON m.CategoryID = vc.CategoryId LEFT OUTER JOIN      
 dbo.tblCourtViolationStatus AS tblCourtViolationStatus_2 ON TV.CourtViolationStatusIDmain = tblCourtViolationStatus_2.CourtViolationStatusID LEFT OUTER JOIN      
 dbo.tblCourtViolationStatus AS tblCourtViolationStatus_1 ON TV.CourtViolationStatusID = tblCourtViolationStatus_1.CourtViolationStatusID LEFT OUTER JOIN      
 dbo.tblCourtViolationStatus AS tblCourtViolationStatus_3 ON TV.CourtViolationStatusIDScan = tblCourtViolationStatus_3.CourtViolationStatusID LEFT OUTER JOIN      
 dbo.tblPricePlans AS pp ON TV.planid = pp.PlanId LEFT OUTER JOIN      
 dbo.tblChargelevel AS CL ON CL.ID = TV.ChargeLevel                                                      
WHERE     TV.TicketID_PK =  @ticketid

-- Rab Nawaz Khan 10349 08/30/2013 if criminal court then lookup the charge level matches for HCCC. if found then pick the Max fine Amount, Jail type and Jail Time. . . 
DECLARE @DatetimeforNewclienst DATETIME
SET @DatetimeforNewclienst = (SELECT MIN(InsertDate) FROM TrafficTickets.dbo.tblHCCCViolationsInfo thi)

-- Update isNewClient flag if recdate in tbltickets is greater then Mininum date for tblHCCCviolationInfo. . .
UPDATE #temp SET isNewClient = 1
FROM #temp WHERE DATEDIFF(DAY, Recdate, @DatetimeforNewclienst) <= 0

IF ((SELECT COUNT(*) FROM #temp WHERE IsCriminalCourt = 1 AND courtid = 3037) > 0)
BEGIN
	
	UPDATE #temp
	SET #temp.JailTimePeriod = dbo.fn_HTP_GetJailTimePeriod(tv.[Description], tmp.ChargeLevel), #temp.TypeOfJail = thi.TypeOfJail, #temp.MaxFineAmount = thi.Maxfine 
	FROM TrafficTickets.dbo.tblViolations tv INNER JOIN TrafficTickets.dbo.tblHCCCViolationsInfo thi
	ON REPLACE(tv.[Description], ' ', '') = REPLACE(thi.ViolationDescription, ' ', '')
	INNER JOIN #temp tmp ON tmp.ChargeLevel = thi.ChargeLevel
	AND tv.ViolationNumber_PK = tmp.ViolationNumber_PK
	WHERE tmp.CourtID = 3037
END

SELECT * FROM #temp
DROP TABLE #temp
-- END 10349


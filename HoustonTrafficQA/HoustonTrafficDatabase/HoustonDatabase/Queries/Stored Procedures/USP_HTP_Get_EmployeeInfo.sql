USE TrafficTickets
GO
/******  
* Created By :	Sameeullah Daris.
* Create Date :   2/3/2011 4:22:15 AM
* Task ID :				8882
* Business Logic :  To get Employee ID information.
* List of Parameter :
* @EmployeeID: ID of employee.
*       
******/

CREATE PROCEDURE USP_HTP_Get_EmployeeInfo(
@EmployeeID INT)
As
BEGIN
	
	
	SELECT tu.Firstname,tu.Lastname,tu.Email,tu.IsAttorney,tu.IsSalesRep
	,tu.ContactNumber,tf.FirmName,tf.FirmAbbreviation,tf.[Address],tf.Address2,tf.City,tf.[State],tf.Zip,tf.Phone,tf.Fax
	  FROM tblUsers tu INNER JOIN  tblFirm tf ON tu.FirmID=tf.FirmID
	
	 WHERE tu.EmployeeID=@EmployeeID
	

END

GO
GRANT EXECUTE ON USP_HTP_Get_EmployeeInfo TO dbr_webuser
GO
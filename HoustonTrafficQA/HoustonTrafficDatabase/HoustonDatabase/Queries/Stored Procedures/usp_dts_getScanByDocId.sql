if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_dts_getScanByDocId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_dts_getScanByDocId]
GO

CREATE PROCEDURE [dbo].[usp_dts_getScanByDocId] 
	@DocId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * from tblScanPIC where ScanBookId = @DocId
END
go
grant execute on [dbo].[usp_dts_getScanByDocId] to dbr_webuser
GO

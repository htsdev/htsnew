USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_Mailer_Send_NewWarrants]    Script Date: 09/05/2011 23:18:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 
Altered by:		Sabir Khan
Business Logic:	The procedure is used by LMS application to get data for selected date range for displaying in Warrant letters.
				
List of Parameters:
	@catnum:		Category number.
	@lettertype:	Type of letter.     
	@startListdate	Start list date            
	@crtid :		Court ID 
	@empid:			Employee ID
	@printtype:		Print Type
	@searchtype:	Search Type
	@isprinted:		Boolean value printed	
	
*******/

/* 
exec  usp_mailer_Get_Houston_NewWarrants  1, 25, 1, '01/01/2010', '10/28/2010', 0
exec  usp_Mailer_Send_NewWarrants 1, 41, 1, '07/18/2012,' , 3991,0,0,0
*/
        
ALTER procedure [dbo].[usp_Mailer_Send_NewWarrants]
@catnum int=1,                                      
@LetterType int=25,                                      
@crtid int=1,                                      
@startListdate varchar (4000) ='12/1/2006',                                      
@empid int=3991,                                
@printtype int = 0 ,
@searchtype int = 0,
@isprinted bit =0
                                      
as
set nocount on                               
                                            
declare @officernum varchar(50),                                      
@officeropr varchar(50),                                      
@tikcetnumberopr varchar(50),                                      
@ticket_no varchar(50),                                                                      
@zipcode varchar(50),                                               
@zipcodeopr varchar(50),                                      
                                      
@fineamount money,                                              
@fineamountOpr varchar(50) ,                                              
@fineamountRelop varchar(50),                     
                
@singleviolation money,                                              
@singlevoilationOpr varchar(50) ,                                              
@singleviolationrelop varchar(50),                                      
                                      
@doubleviolation money,                                              
@doublevoilationOpr varchar(50) ,                                              
@doubleviolationrelop varchar(50),                                    
@count_Letters int,                    
@violadesc varchar(500),                    
@violaop varchar(50)                                       
                                      
declare @sqlquery varchar(8000),  
@sqlquery2 varchar(8000), @lettername varchar(50)
                                              
Select @officernum=officernum,                                      
@officeropr=oficernumOpr,                                      
@tikcetnumberopr=tikcetnumberopr,                                      
@ticket_no=ticket_no,                                                                        
@zipcode=zipcode,                                      
@zipcodeopr=zipcodeLikeopr,                                      
@singleviolation =singleviolation,                                      
@singlevoilationOpr =singlevoilationOpr,                                      
@singleviolationrelop =singleviolationrelop,                                      
@doubleviolation = doubleviolation,                                      
@doublevoilationOpr=doubleviolationOpr,                                              
@doubleviolationrelop=doubleviolationrelop,                    
@violadesc=violationdescription,                    
@violaop=violationdescriptionOpr ,                
@fineamount=fineamount ,                                              
@fineamountOpr=fineamountOpr ,                                              
@fineamountRelop=fineamountRelop                
                                   
from tblmailer_letters_to_sendfilters                                      
where courtcategorynum=@catnum                                      
and Lettertype=@LetterType                                    

--Sabir Khan 6410 08/19/2009 three new warrant letter added.
--Sabir Khan 6503 09/01/2009 The warrant letter (180 - 270) has been changed into Warrant (180 - 220)
Select	@sqlquery =''  , @sqlquery2 = ''  , 
		@lettername  =  case @lettertype 
							when 41 then ' HMC Warrant (30-45)'
							when 42 then ' HMC Warrant (60-75)'
							when 43 then ' HMC Warrant (90-105)'
							WHEN 65 THEN ' HMC Warrant (180-220)'
							WHEN 66 THEN ' HMC Warrant (360-390)'
							WHEN 67 THEN ' HMC Warrant (720-750)'
						end


declare @user varchar(10)
select @user = upper(abbreviation) from tblusers where employeeid = @empid

                                      
set @sqlquery=@sqlquery+'                                      
Select distinct ta.recordid, 
 convert(varchar(10),getdate(),101) as listdate,
 left(zipcode,5) + rtrim(ltrim(midnumber)) as zipmid,  
 tva.ftalinkid,
 count(tva.ViolationNumber_PK) as ViolCount,  
 isnull(sum(isnull(tva.fineamount,0)) ,0) as Total_Fineamount   
into #temptable
FROM tblTicketsViolationsArchive tva, tblTicketsArchive ta
where  tva.RecordID = ta.RecordID   
and  donotmailflag = 0
and  Flag1 in (''Y'',''D'',''S'')  
--Haris Ahmed 10365 07/09/2012 Change filter
--and charindex(''FTA'', tva.causenumber) > 0
and tva.causenumber LIKE ''%FTA%''
--Muhammad Muneer 8465 11/11/2010 added the new functionality that is the null checks for the first name and the last name 
and LEN(ISNULL(RTRIM(LTRIM(ta.lastname)),''''))<>0
and LEN(ISNULL(RTRIM(LTRIM(ta.firstname)),''''))<>0  
and isnull(ta.ncoa48flag,0) = 0
and FTAlinkid is not null and (violationstatusid = 186)  and ta.address1 <> ''11002 HAMMERLY BLVD APT 76''

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0
 '
--Sabir Khan 6410 08/19/2009 Three new warrant letter added.
---------------------------------------------------------------------
--Sabir Khan 6503 09/01/2009 The warrant letter (180 - 270) has been changed into Warrant (180 - 220)
if @lettertype = 41
	set @sqlquery=@sqlquery+' 
	and datediff(Day, tva.ftaissuedate, getdate()) between 30 and 45 '
else if @lettertype = 42
	set @sqlquery=@sqlquery+' 
	and datediff(Day, tva.ftaissuedate, getdate()) between 60 and 75 '
else if @lettertype = 43
	set @sqlquery=@sqlquery+' 
	and datediff(Day, tva.ftaissuedate, getdate()) between 90 and 105 '
ELSE IF @LetterType = 65
	SET @sqlquery = @sqlquery+'
	and datediff(Day, tva.ftaissuedate, getdate()) between 180 and 220 '
ELSE IF @LetterType = 66
	SET @sqlquery = @sqlquery+'
	and datediff(Day, tva.ftaissuedate, getdate()) between 360 and 390 '
ELSE IF @LetterType = 67
	SET @sqlquery = @sqlquery+'
	and datediff(Day, tva.ftaissuedate, getdate()) between 720 and 750 '
-----------------------------------------------------------------------
if(@crtid=1 )                
 set @sqlquery=@sqlquery+'   
and  tva.courtlocation In (Select courtid from tblcourts where courtcategorynum = 1 )'                    
  
if @crtid <> 1 
 set @sqlquery =@sqlquery +'                                    
and  tva.courtlocation =  ' +convert(varchar(10),@crtid)                                   
                                    
--set @sqlquery=@sqlquery+'  
--and   ' + dbo.Get_String_Concat_With_DatePart_ver2 ( ''+  @startlistdate + '', 'tva.ftaissuedate' )   
                                      
if(@officernum<>'')                                      
begin                                      
set @sqlquery =@sqlquery +                                       
            '    
and  ta.officerNumber_Fk '+@officeropr +'('+ @officernum+')'                                       
end                                      
                                      
if(@ticket_no<>'')                                      
set @sqlquery=@sqlquery+ '  
and   ('+ dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr)+')'                                            
                                      
if(@zipcode<>'')                                                            
--Haris Ahmed 10365 07/09/2012 Change filter
--set @sqlquery=@sqlquery+ '   
-- and ('+  dbo.Get_String_Concat_With_op(''+@zipcode+'%' , 'ta.zipcode' ,+ ''+ 'LIKE') +')'

-- Rab Nawaz Khan 10365 07/18/2012 After providing the filters (No Letter found for the list date) issue has been resolved. . . 
set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'
if(@violadesc<>'')                
set @sqlquery=@sqlquery+'   
and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop)+')'             

if(@fineamount<>0 and @fineamountOpr<>'not'  )                
begin                
set @sqlquery =@sqlquery+ '     '+                                      
      '  and tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                     
end                                               

if(@fineamount<>0 and @fineamountOpr = 'not'  )                
begin                
set @sqlquery =@sqlquery+ '     '+                                      
      '  and not tva.fineamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                     
end                                               


set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (                                                                    
	select	d.recordid from tblletternotes n, tblletternotes_detail d
	where 	lettertype ='+convert(varchar(10),@lettertype)+'   and n.noteid = d.noteid
	union
	-- exclude DLQ letters printed today or yesterday...
	select d.recordid from tblletternotes n inner join tblletternotes_detail d
	on d.noteid = n.noteid  where lettertype = 19 
	and datediff(day, recordloaddate, getdate()) in (0,1)
	) '


                                      
set @sqlquery =@sqlquery+'        
group by    
 ta.recordid, left(zipcode,5) + rtrim(ltrim(midnumber)), tva.ftalinkid '                
                              
set @sqlquery=@sqlquery+'   

Select distinct a.recordid,  a.listdate, ta.violationdate as courtdate,   tva.courtlocation as courtid,    flag1 = isnull(ta.Flag1,''N'') ,   ta.officerNumber_Fk,                                            
 tva.TicketNumber_PK,   ta.donotmailflag,   ta.clientflag,   upper(firstname) as firstname,   upper(lastname) as lastname,  upper(address1) + '''' + isnull(ta.address2,'''') as address,                        
 upper(ta.city) as city,   s.state,   dbo.tblCourts.CourtName,   zipcode,   midnumber, rtrim(ltrim(midnumber)) as midnum,  
 dp2 as dptwo,   dpc, violationdate,   violationstatusid,   a.zipmid, tva.ViolationNumber_PK,  ViolationDescription,  
 -- Rab Nawaz Khan 9704 09/26/2011 Default fine amount 100 logic is commented. . . 
 -- case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,  
 isnull(tva.fineamount,0) as FineAmount,  a.ViolCount,  
 a.Total_Fineamount, a.ftalinkid, case when charindex(''FTA'',tva.causenumber) <> 0 then 1 else 0 end as IsFTA
into #temp                                            
FROM tblTicketsViolationsArchive tva, tblTicketsArchive ta, tblCourts , #temptable a, tblstate s
where  tva.RecordID = ta.RecordID   and ta.recordid = a.recordid and tblcourts.courtid = tva.courtlocation and ta.stateid_Fk = s.stateid
and tva.violationstatusid <> 80 -- Abbas Qamar 9991 01/17/2012 Excluded Disposed cases.
--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0

insert into #temp
Select distinct ta.recordid,  a.listdate, ta.violationdate as courtdate,   tva.courtlocation as courtid, b.flag1 ,  
 b.officerNumber_Fk, tva.TicketNumber_PK,  b.donotmailflag,  b.clientflag,  b.firstname,  b.lastname,  b.address, b.city, b.state,  
 dbo.tblCourts.CourtName,  b.zipcode,  b.midnumber, b.midnum,  b.dptwo,  
 b.dpc, ta.violationdate,  tva.violationstatusid,   b.zipmid, tva.ViolationNumber_PK, 
 tva.ViolationDescription,    case when isnull(tva.fineamount,0)=0 then 100 else tva.fineamount end as FineAmount,0,0, a.ftalinkid,
  0 as IsFTA
from tblticketsarchive ta, tblticketsviolationsarchive tva, #temptable a, tblcourts , #temp b
where tva.recordid = ta.recordid and tblcourts.courtid = tva.courtlocation  
and a.ftalinkid = tva.ftalinkid and b.ftalinkid = a.ftalinkid
and charindex(''FTA'',tva.causenumber) = 0
and (tva.violationstatusid <> 80) and ta.recordid not in(select distinct recordid from #temp)

--Rab Nawaz Khan 9119 04/04/2011 added attorney association logic.
and len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) = 0 
and CHARINDEX(''KUBOSH'',isnull(tva.BondingCompanyName,''''))=0
-- Sabir Khan 10245 05/03/2012
and isnull(ta.IsIncorrectMidNum,0) = 0

select distinct ftalinkid into #tmp2 from #temp group by ftalinkid having count(recordid) > 1


insert into #temp 
select distinct a.recordid, a.listdate, a.courtdate, a.courtid,  a.flag1, a.officernumber_fk, 
f.causenumber, a.donotmailflag, a.clientflag, a.firstname, a.lastname, a.address, a.city, a.state, a.courtname, a.zipcode, a.midnumber,
a.midnum, a.dptwo, a.dpc, a.violationdate, a.violationstatusid, a.zipmid, a.ViolationNumber_PK, ''MISC. VIOLATIONS'', a.fineamount, a.violcount, a.total_fineamount, a.ftalinkid,
0 as IsFTA
from #temp a, tbllubbockfta f where f.linkid = a.ftalinkid and a.ftalinkid not in (select distinct ftalinkid from #tmp2)
and charindex(''FTA'',f.causenumber) = 0
and f.causenumber not in ( select distinct ticketnumber_pk from #temp)
  
Select distinct recordid, violationstatusid,ViolCount,Total_Fineamount,  listdate as mdate,FirstName,LastName,address,                                
 city,state,FineAmount,ViolationNumber_PK, violationdescription,CourtName,dpc,dptwo,Flag1,officerNumber_Fk,TicketNumber_PK,                                       
 courtid,courtdate,zipmid,donotmailflag,clientflag, zipcode,midnumber,midnum, ftalinkid, IsFTA into #temp1  from #temp                                      
 '                
if(@singleviolation<>0 and @doubleviolation=0)                
begin                
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                      
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                
         or violcount>3                                              
      '                
end                
                
if(@doubleviolation<>0 and @singleviolation=0 )                
begin                
set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+                
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                      
      or violcount>3  
'                                    
end 

--Yasir Kamal 7119 12/10/2009 send warrant letters to existing clients.
--Sabir Khan 7099 12/08/2009 Exclude client cases....
--set @sqlquery =@sqlquery+'  
--delete from #temp1 where recordid in (
--select v.recordid from tblticketsviolations v inner join tbltickets t 
--on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
--inner join #temp1 a on a.recordid = v.recordid
--)'               
                
set @sqlquery =@sqlquery+' 

--Rab Nawaz Khan 9685 09/14/20211 Remove cases having attorney association...
select tva.recordid,tva.violationnumber_pk,tva.violationdescription into #tempAttorney 
from #temp1 t inner join tblticketsviolationsarchive tva on tva.ftalinkid = t.ftalinkid 
where len(isnull(tva.attorneyname, '''')+isnull(tva.barcardnumber,'''')) <> 0

delete  f 
from #temp1 f
inner join #tempAttorney a on a.recordid = f.recordid and a.violationnumber_pk = f.violationnumber_pk
and a.violationdescription = f.violationdescription

-- Rab Nawaz Khan 9704 09/28/2011 added to hide the entire columm in mailer if fine amount is zero agains any violation of a recored . . . 

 alter table #temp1 
 ADD isfineamount BIT NOT NULL DEFAULT 1  
 
 select recordid into #fine from #temp1  WHERE ISNULL(FineAmount,0) = 0  
 
 update t   
 set t.isfineAmount = 0  
 from #temp1 t inner join #fine f on t.recordid = f.recordid   

'
           
if( @printtype<>0)           
set @sqlquery2 =@sqlquery2 +                                      
'  
 Declare @ListdateVal DateTime,                                    
  @totalrecs int,  @count int, @p_EachLetter money, @recordid varchar(50),  @zipcode   varchar(12),  @maxbatch int  ,  
  @lCourtId int  , @dptwo varchar(10),  @dpc varchar(10),  @tempmidnum varchar(50), @midnum  varchar(20),  @tempidentity  varchar(50) 	 
  declare @tempBatchIDs table (batchid int)
 Select @totalrecs =count(zipmid) from #temp1 where mdate = @ListdateVal                                   
 Select @count=Count(zipmid) from #temp1                             
  if @count>=500                                  
  set @p_EachLetter=convert(money,0.3)                                  
                                  
 if @count<500                                  
  set @p_EachLetter=convert(money,0.39)                                  
                                
 Declare ListdateCur Cursor for                                    
 Select distinct mdate  from #temp1                                  
 open ListdateCur                                     
 Fetch Next from ListdateCur into @ListdateVal                                                        
 while (@@Fetch_Status=0)                                
  begin                                  
   Select @totalrecs =count(distinct zipmid) from #temp1 where mdate = @ListdateVal                                   
   insert into tblBatchLetter(Empid, Lettertype,Listdate,CourtId,ParentID,LCount,PExpense) values                                    
   ('+convert(varchar(10),@empid)+' ,'+convert(varchar(10),@lettertype)+', @ListdateVal, '+convert(varchar(10),@crtid)+', 0, @totalrecs, @p_EachLetter)                                     
   Select @maxbatch=Max(BatchId) from tblBatchLetter  
    insert into @tempBatchIDs select @maxbatch 
    Declare RecordidCur Cursor for                                 
    Select  recordid,zipcode, courtid, dptwo, dpc,midnum from #temp1 where mdate = @ListdateVal order by zipmid, IsFTA desc                              
    open RecordidCur                                                           
    Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc,@midnum                                              
    while(@@Fetch_Status=0)                                
    begin  
     if(@midnum = @tempmidnum)	
	begin		
	    insert into tblletternotes_detail (noteid, recordid) select @tempidentity, @recordid
	end
     else
    	begin                         
	     insert into tblLetterNotes(BatchId_Fk,listdate,ZipCode,RecordID,PCost,LetterType, courtid, dp2, dpc,midnumber)                                
	     values (@maxbatch,@ListdateVal,@zipcode,@recordid,@p_EachLetter,'+convert(varchar(10),@lettertype)+' ,@lCourtId, @dptwo, @dpc, @midnum)  
	     set @tempidentity =@@identity
	     set @tempmidnum = @midnum 
	    insert into tblletternotes_detail (noteid, recordid) select @tempidentity, @recordid
        end	
	Fetch Next from  RecordidCur into @recordid,@zipcode, @lCourtId , @dptwo, @dpc,@midnum  
    end                                
    close RecordidCur   
    deallocate RecordidCur                                                                 
   Fetch Next from ListdateCur into @ListdateVal                               
  end                                              
 close ListdateCur    
 deallocate ListdateCur   
Select distinct convert(varchar(20),n.noteid) as letterid, t.recordid,  t.FirstName, isFTA,                                
t.LastName,t.address,t.city, t.state,FineAmount,violationdescription,CourtName, t.dpc, ftalinkid ,                              
dptwo, TicketNumber_PK, n.zipcode,  t.zipmid,t.recordid,t.courtdate,t.courtid,n.midnumber, 
n.midnumber as midnum, '''+@user+''' as Abb , '''+@lettername +'''as lettername, isfineamount                                   
from #temp1 t , tblletternotes n  , tblletternotes_detail d, 	@tempBatchIDs tb 
where 	d.RECORDID = T.RECORDID and 	d.noteid = n.noteid AND N.COURTID = T.COURTID
and 	TB.BATCHID = N.BATCHID_FK and  n.lettertype = '+convert(varchar(10),@lettertype)+' order by zipmid, IsFTA desc

declare @lettercount int, @subject varchar(200), @body varchar(400) ,  @sql varchar(500)
select @lettercount = count(distinct n.noteid) from tblletternotes n, @tempBatchIDs b where n.batchid_fk = b.batchid
select @subject =  convert(varchar(20), @lettercount) +''' + @lettername +  ' Letters Printed''
select @body = convert(varchar(20), @lettercount) + '' LETTERS ('' + CONVERt(VARCHAR(12), GETDATE()) + '') - ''+ upper( '''+@user+''')
exec usp_mailer_send_Email @subject, @body

declare @batchIDs_filname varchar(500)
set @batchIDs_filname = ''''
select @batchIDs_filname = @batchIDs_filname + convert(varchar,batchid) + '',''  from @tempBatchIDs
select isnull(@batchIDs_filname,'''') as batchid

'  
  
else  
 set @sqlquery2 = @sqlquery2 + '  
  Select distinct ''NOT PRINTABLE'' as letterId,  recordid,  FirstName,  isFTA,                               
  LastName,address,city, state,FineAmount,violationdescription,CourtName, dpc, ftalinkid, dptwo, TicketNumber_PK, zipcode,zipmid,
  recordid,courtdate,courtid,midnumber,midnum, '''+@user+''' as Abb, '''+@lettername +'''as lettername,  isfineamount                            
  from #temp1 order by zipmid, isFTA desc
 '  
  
set @sqlquery2 = @sqlquery2 + ' drop table #temp drop table #temp1'  
                                      
--print @sqlquery 
--print @sqlquery2  
exec (@sqlquery + @sqlquery2)



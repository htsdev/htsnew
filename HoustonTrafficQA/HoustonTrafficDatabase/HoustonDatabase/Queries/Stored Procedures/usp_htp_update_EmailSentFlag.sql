﻿   /*    
* Waqas 5864 06/26/2009  
Created By     : Waqas Javed.    
Created Date   : 06/26/2009      
Business Logic : This procedure update email case summary flag of client by Ticket ID.      
               
Parameter:           
 @TicketID   : TicketId          
*/    
  
--Waqas 6342 08/25/2009 Changes for criminal and family 
ALTER PROCEDURE dbo.usp_htp_update_EmailSentFlag
@TicketID INT
AS
BEGIN
	
	UPDATE tbltickets SET EmailSentFlag = 1
	WHERE TicketID_PK = @TicketID
END

GO 

GRANT EXECUTE ON dbo.usp_htp_update_EmailSentFlag TO dbr_webuser
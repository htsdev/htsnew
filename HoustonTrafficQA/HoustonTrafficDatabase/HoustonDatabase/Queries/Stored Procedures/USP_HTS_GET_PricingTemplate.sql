SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_PricingTemplate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_PricingTemplate]
GO










--Gets all pricing templates
CREATE PROCEDURE [dbo].[USP_HTS_GET_PricingTemplate] AS


SELECT	TemplateID, 
		TemplateName, 
		TemplateDescription
FROM         	dbo.tblPricingTemplate





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


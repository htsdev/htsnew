SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_hts_RM_GetAllBondDiscrepanciesWithContract]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_hts_RM_GetAllBondDiscrepanciesWithContract]
GO


CREATE procedure USP_hts_RM_GetAllBondDiscrepanciesWithContract

as

SELECT     
		t.ticketid_Pk, tv.RefCaseNumber AS [Ticket Number], 
		tv.casenumassignedbycourt AS [Cause Number], 
		tv.CourtDate AS [Auto Court Date], 
		tv.CourtDateMain AS [Verified Court Date], 
		t.BondFlag AS [Bond Flag Main], 
		ISNULL(tv.UnderlyingBondFlag, 0) AS [Underlying Bond Flag], courtid
FROM    dbo.tblTickets AS t INNER JOIN
           dbo.tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK
			inner join tblticketsnotes n on t.ticketid_pk = n.ticketid
WHERE  (t.Activeflag = 1) 
	and (t.BondFlag = 1) 
	and Courtviolationstatusidmain <> 80
	and t.ticketid_pk not in (select ticketid_pk from tblticketsviolations where (ISNULL(UnderlyingBondFlag, 0) = 1) )
	and n.subject like '%Signed Bonds%'


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_MakeHistory_ServerSide]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_MakeHistory_ServerSide]
GO









CREATE Procedure [dbo].[USP_HTS_ESignature_MakeHistory_ServerSide]
@TicketID as int,
@LetterID as int,
@EmployeeID as int,
@Notes as varchar(200),
@DocPath as varchar(120),
@BookID int
As
Begin

INSERT INTO tblTicketsNotes(TicketID, Subject, EmployeeID)
VALUES	(@TicketID, '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+convert(varchar,@BookID)+'&RecType=1&DocNum=0&DocExt=PDF'');void('''');"''>ESignature - '+ @Notes +'</a>', @EmployeeID)

End












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to fetch the scan batch details ids against a scan batch to delete pictures

List of Parameters:	
	@ScanBatchID : scan bact id for which records will be fetched.


List of Columns: 	
	scanbatchdetailid : scan batch details ids 

******/
  
Create procedure dbo.usp_HTP_Get_DocumentTracking_DeletePics
      
@ScanBatchID as int                 

as                
      
select	sbd.scanbatchdetailid         
from	tbl_HTP_DocumentTracking_ScanBatch_Detail sbd inner join tbl_HTP_DocumentTracking_ScanBatch sb 
on	sbd.scanbatchid_fk=sb.scanbatchid    
where	sbd.isverified=0
and sbd.scanbatchid_fk=@ScanBatchID
order by sbd.scanbatchdetailid       
      
go

grant exec on dbo.usp_HTP_Get_DocumentTracking_DeletePics to dbr_webuser
go
                  
  
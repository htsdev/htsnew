SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_splitreport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_splitreport]
GO


--[usp_splitreport] '2007-01-01','2007-12-31',0
--sp_helptext usp_splitreport  
create procedure [dbo].[usp_splitreport]          
          
@startdate as datetime,          
@enddate as datetime ,           
@showall as int=0                  
as           
          
--In order to search all records below criteria    
IF @showall=1        
 begin          
          
SELECT TT.TicketID_PK, TTV.RefCaseNumber   AS refcasenumberseq,     
 TTV.RefCaseNumber, --+ '-' + CONVERT(varchar, ISNULL(TTV.SequenceNumber, ' ')) AS refcasenumberseq,           
  convert(varchar(11),TTV.CourtDateMain,101)+'@'+replace(substring(convert(varchar(20),TTV.CourtDateMain),13,18),'12:00AM','0:00AM')    AS courtdatemain,             
 ('# '+TTV.CourtNumbermain) as CourtNumbermain,           
 dt.Description as VerifiedStatus,          
 ttv.courtviolationstatusidmain,          
 TC.shortname,          
 TT.generalcomments         , tt.activeflag  
 into #tmp1        
 FROM    dbo.tblDateType dt           
  INNER JOIN          
  dbo.tblCourtViolationStatus cvs           
  ON  dt.TypeID = cvs.CategoryID           
  INNER JOIN          
  dbo.tblTickets TT           
  INNER JOIN          
  dbo.tblTicketsViolations TTV           
  ON  TT.TicketID_PK = TTV.TicketID_PK           
  ON  cvs.CourtViolationStatusID = TTV.CourtViolationStatusIDmain          
  INNER JOIN dbo.tblcourts TC          
  ON  TTV.courtid=TC.courtid          
          
 WHERE   

  dt.typeid in (1, 2,3,4,5,12)     --status of waiting, arrignment,jury,pretrial,judge,bond  
 and  TTV.RefCaseNumber <>'0'

  AND  TT.Activeflag<>0          --only clients   
  AND TTV.TicketID_PK IN (                --in order to look for tickets with same casenumber but having different Verified Statuses   
   SELECT  v.ticketid_pk          
   FROM dbo.tblTicketsViolations v           
   INNER JOIN       
                       dbo.tblCourtViolationStatus cvs2           
   ON  v.CourtViolationStatusIDmain = cvs2.CourtViolationStatusID           
   INNER JOIN          
                       dbo.tblDateType dt2           
   ON  cvs2.CategoryID = dt2.TypeID        
   WHERE   v.ticketid_pk = ttv.ticketid_pk           
   AND    V.RefCaseNumber <>'0'          
   and (datediff(day,getdate(),V.courtdatemain) > 0 and  dt2.typeid in (3,4,5))
   
   and             
  (           
  (datediff(day,TTV.courtdatemain,V.courtdatemain) <> 0)   --Compares one ticket Verified Court Date with the another ticket             
   or            
  (datediff(hour,TTV.courtdatemain,V.courtdatemain) <> 0)   -- Compares one ticket Verified Court time with the another ticket             
  or          
  (datediff(minute,TTV.courtdatemain,V.courtdatemain) <> 0)             
  or          
  (convert(int,TTV.courtnumbermain) <> convert(int,V.courtnumbermain)) -- Compares one ticket Verified Court num with the another ticket                    
  or             
  (dt.typeid <> dt2.typeid)                             --Compare Statuses  
  )            
 )      
  order by tt.ticketid_pk        
  
 delete from #tmp1 where ticketid_pk in ( select ticketid_pk from tblticketsflag where flagid =12)  
 select * from #tmp1  
 drop table #tmp1  
  
  
end          
else          
 begin      --When date criteria has been specified by the user  
          
 SELECT TT.TicketID_PK,           
 TTV.RefCaseNumber, --+ '-' + CONVERT(varchar, ISNULL(TTV.SequenceNumber, ' ')) AS refcasenumberseq,           
 CONVERT(varchar(11), TTV.CourtDateMain, 101) + ' @ ' + SUBSTRING(CONVERT(varchar(20), TTV.CourtDateMain), 14, 18) AS courtdatemain,           
 ('# '+TTV.CourtNumbermain) as CourtNumbermain,           
 dt.Description as VerifiedStatus,          
 ttv.courtviolationstatusidmain,          
 TC.shortname,          
 TT.generalcomments         , tt.activeflag  
into #tmp2  
 FROM    dbo.tblDateType dt           
 INNER JOIN          
 dbo.tblCourtViolationStatus cvs           
 ON  dt.TypeID = cvs.CategoryID           
 INNER JOIN          
 dbo.tblTickets TT           
 INNER JOIN          
 dbo.tblTicketsViolations TTV           
 ON  TT.TicketID_PK = TTV.TicketID_PK           
 ON  cvs.CourtViolationStatusID = TTV.CourtViolationStatusIDmain          
 INNER JOIN dbo.tblcourts TC          
 ON       TTV.courtid=TC.courtid          
	
 WHERE   dt.typeid in (1,2,3,4,5,12) and datediff(day,getdate(),TTV.courtdatemain) > 0 --(1,2,3,4,5,12)          --status of waiting, arrignment,jury,pretrial,judge,bond  
         AND    TTV.RefCaseNumber <>'0'          
	and Tc.isCriminalCourt =0
  AND TT.Activeflag<>0           --only clients   
  --and TTV.courtdatemain between @startdate and @enddate --date criteria    
  AND TTV.TicketID_PK IN (           --in order to look for tickets with same casenumber but having different Verified Statuses   
   SELECT  v.ticketid_pk          
   FROM dbo.tblTicketsViolations v           
   INNER JOIN          
                       dbo.tblCourtViolationStatus cvs2           
   ON  v.CourtViolationStatusIDmain = cvs2.CourtViolationStatusID           
   INNER JOIN          
                       dbo.tblDateType dt2           
   ON  cvs2.CategoryID = dt2.TypeID      
          WHERE   v.ticketid_pk = ttv.ticketid_pk           
    AND    V.RefCaseNumber <>'0'          
    and (datediff(day,getdate(),V.courtdatemain) > 0 and  dt2.typeid in (3,4,5))          --and V.courtdatemain between @startdate and @enddate        --date criteria       
   and        
 (           
  (datediff(day,TTV.courtdatemain,V.courtdatemain) <> 0)           --Compares one ticket Verified Court Date with the another ticket               
  or            
  (datediff(hour,TTV.courtdatemain,V.courtdatemain) <> 0)          -- Compares one ticket Verified Court time with the another ticket          
  or          
  (datediff(minute,TTV.courtdatemain,V.courtdatemain) <> 0)         -- Compares one ticket Verified Court num with the another ticket     
  or      
  (convert(int,TTV.courtnumbermain) <> convert(int,V.courtnumbermain))          
  or             
  (dt.typeid <> dt2.typeid)          
 )       
      ) --subquery    
 order by tt.ticketid_pk        
  
 delete from #tmp2 where ticketid_pk in ( select ticketid_pk from tblticketsflag where flagid =12)  
 select * from #tmp2  
 drop table #tmp2  
  
  
 end  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


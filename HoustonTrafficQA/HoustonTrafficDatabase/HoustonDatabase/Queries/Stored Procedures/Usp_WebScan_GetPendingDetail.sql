SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Usp_WebScan_GetPendingDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Usp_WebScan_GetPendingDetail]
GO

  
CREATE procedure [dbo].[Usp_WebScan_GetPendingDetail]       
@BatchID as int           
as          
          
select           
       
 p.id as picID,  
 o.status,  
 o.Location,  
 o.CourtNo,  -- Haris Ahmed 10361 07/05/2012 Add Court No Column
 isnull(TypeScan,'') as TypeScan     
 from tbl_webscan_pic p inner join       
   tbl_webscan_ocr o on       
   p.id=o.picid      
      
where       
 p.batchid=@BatchID and o.checkstatus=1      
order by picID    
  
        
            

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


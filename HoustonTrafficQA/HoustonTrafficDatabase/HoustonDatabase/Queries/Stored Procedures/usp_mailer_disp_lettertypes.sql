/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used to display letter types in drop down list 
				on different pages in LMS.
				
List of Parameters:
	@CatNum:		Court Category ID- to display letter types of only selected category.
	@ShowInActive:	Flag to display disabled letters. Default value is false.

List of Columns:	
	LetterId_pk:	Letter ID for the mailer type.
	LetterName:		Name of the mailer type.
	CatNum:			Category Id for the mailer type.

******/


alter Proc [dbo].[usp_mailer_disp_lettertypes]   
 (      
 @CatNum int ,
 @ShowInActive bit = 0
 )      
as    
              
 -- tahir 4357 07/04/2008
 -- if calling from LMS Reports....
 -- display
 if @showinactive = 1
	begin

		Select LetterId_pk,LetterName,url, courtcategory as catnum from tblletter                
		where courtcategory = @CatNum      
		order by sortorder
	end
else 
	begin
		Select LetterId_pk,LetterName,url, courtcategory as catnum from tblletter                
		where courtcategory = @CatNum 
		and isActive = 1      
		order by sortorder	
	end
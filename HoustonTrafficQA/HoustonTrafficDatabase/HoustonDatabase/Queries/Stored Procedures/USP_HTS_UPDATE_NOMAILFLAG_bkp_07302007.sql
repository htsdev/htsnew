SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_UPDATE_NOMAILFLAG_bkp_07302007]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_UPDATE_NOMAILFLAG_bkp_07302007]
GO

  
CREATE PROCEDURE [dbo].[USP_HTS_UPDATE_NOMAILFLAG_bkp_07302007]  
@LoadDate datetime,  
@CourtIDs varchar(8000)  
AS   
  
declare @tblcourtIDs table (                                    
 courtID int  
 )    
  
insert into @tblcourtIDs  
 select * from dbo.Sap_String_Param(@CourtIDs)  
  
  
  
update tblticketsarchive         
 set DoNotMailFlag =1        
WHERE         
 datediff(day,RecLoadDate,@LoadDate) =0         
 and        
   exists         
    (select distinct firstname,        
     lastname,        
     address,    
  zip         
             
    from tbl_hts_donotmail_records         
    WHERE         
             
     tblticketsarchive.firstname = tbl_hts_donotmail_records.FirstName         
     AND        
     tblticketsarchive.lastname = tbl_hts_donotmail_records.LastName         
     AND        
     tblticketsarchive.address1 = tbl_hts_donotmail_records.Address      
  AND      
  tblticketsarchive.ZipCode LIKE(tbl_hts_donotmail_records.Zip+'%')  
 AND  
 tblticketsarchive.CourtID IN (select courtID from @tblcourtIDs)  
 AND   
 tblticketsarchive.DoNotMailFlag = 0  
 )    
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


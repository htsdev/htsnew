﻿ /****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The procedure is used to insert note in log table

List of Parameters:				
	@EventType :
	@Note :

	
*******/

CREATE PROCEDURE dbo.usp_AD_Insert_Note

@EventType VARCHAR(100),
@Note VARCHAR(MAX)

AS


INSERT INTO AutoDialerLog
(	
	EventType,
	Note
)
VALUES
(
	@EventType,
	@Note
)

GO

GRANT EXECUTE ON dbo.usp_AD_Insert_Note TO dbr_webuser
GO
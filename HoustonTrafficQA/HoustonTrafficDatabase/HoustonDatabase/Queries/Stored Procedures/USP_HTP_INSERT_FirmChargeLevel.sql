/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This procedure Insert/Update firm charge level charge amount.
* Column returns : 
*				@firmid
				@chargelevelid
				@amount
*/

CREATE PROCEDURE [dbo].[USP_HTP_INSERT_FirmChargeLevel]
	@firmid INT,
	@chargelevelid INT,
	@amount MONEY
AS
	IF EXISTS(
	       SELECT *
	       FROM   FirmChargeLevel fcl
	       WHERE  fcl.FirmID = @firmid
	              AND fcl.ChargeLevelID = @chargelevelid
	   )
	BEGIN
	    UPDATE FirmChargeLevel
	    SET    ChargeAmount = @amount
	    WHERE  FirmID = @firmid
	           AND ChargeLevelID = @chargelevelid
	END
	ELSE
	BEGIN
	    INSERT INTO FirmChargeLevel
	      (
	        FirmID,
	        ChargeLevelID,
	        ChargeAmount
	      )
	    VALUES
	      (
	        @firmid,
	        @chargelevelid,
	        @amount
	      )
	END


GO
GRANT EXECUTE ON [dbo].[USP_HTP_INSERT_FirmChargeLevel] TO dbr_webuser
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Insert_PricePlanDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Insert_PricePlanDetail]
GO










--Insert values in  tblpriceplandetail
CREATE procedure USP_HTS_Insert_PricePlanDetail
	(
	@PlanId				int,
	@CategoryId			int,
	@BasePrice			money,
	@SecondaryPrice			money,
	@BasePercentage			numeric(18,4),
	@SecondaryPercentage		numeric(18,4),
	@BondBase			money,
	@BondSecondary			money,
	@BondBasePercentage		numeric(18,4),
	@BondSecondaryPercentage	numeric(18,4),
	@BondAssumption			money,
	@FineAssumption			money	
	)
as

insert into tblpriceplandetail
	(
	PlanId,
	CategoryId,
	BasePrice,
	SecondaryPrice,
	BasePercentage,
	SecondaryPercentage,
	BondBase,
	BondSecondary,
	BondBasePercentage,
	BondSecondaryPercentage,
	BondAssumption,
	FineAssumption	
	)
values
	(
	@PlanId,
	@CategoryId,
	@BasePrice,
	@SecondaryPrice,
	@BasePercentage,
	@SecondaryPercentage,
	@BondBase,
	@BondSecondary,
	@BondBasePercentage,
	@BondSecondaryPercentage,
	@BondAssumption,
	@FineAssumption	
	)





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Get_ImageByte]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Get_ImageByte]
GO



-- =============================================
-- Author:		Muhammad Adil Aleem
-- Create date: 14-Nov-06
-- Description:	To Get Images from table
-- =============================================
CREATE PROCEDURE [dbo].[USP_HTS_ESignature_Get_ImageByte]
	@TicketID varchar(25),
	@ImageTrace varchar(100)
AS
BEGIN
SELECT Top 1 ImageStream from TBL_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace =	@ImageTrace
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


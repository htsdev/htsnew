SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Resets_GetRecords]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Resets_GetRecords]
GO


CREATE procedure [dbo].[usp_Resets_GetRecords]    
as    
    
SELECT distinct             
 st.scanticketid as ticketid,    
 st.ScanTicketViolationID AS TicketViolationsid,    
 st.doc_id,            
 st.docnum as doc_num,       
   'Resets' AS DOCREF,                 
   isnull(tv.UpdatedDate,'01/01/1900') AS UPDATEDATETIME,                
 LTRIM(tv.RefCaseNumber) + ' - ' +             
 LTRIM(STR(tv.SequenceNumber)) + '  - ' +             
 isnull(cvs.ShortDescription,'') + '  - ' +             
 CONVERT(varchar(20),st.courtdate, 101) + ' ' +              
 SUBSTRING(SUBSTRING(CONVERT(char(26), st.courtdate, 109), 13, 14), 1, 5)+ ' ' +             
 SUBSTRING(SUBSTRING(CONVERT(char(26), st.courtdate, 109), 13, 14), 13, 2)+ '  - #' +             
 isnull(st.courtno, 0) + '  - ' +             
 ISNULL(v.Description, '') AS ResetDesc,    
 '0' AS RecordType,             
 1 AS ImageCount,            
 0 as did,            
 0 as ocrid,            
 '000' as DocExtension,          
 u.lastname as Abbreviation -- ref bug id 1428             
                
 FROM  dbo.tblTicketsViolations tv              
 inner join            
 dbo.tblScannedDocketCourtStatus st            
  on st.Scanticketviolationid = tv.ticketsviolationid            
 LEFT OUTER JOIN                
        dbo.tblViolations v             
 ON  tv.ViolationNumber_PK = v.ViolationNumber_PK             
  LEFT OUTER JOIN                
        dbo.tblCourtViolationStatus cvs             
  ON  st.ViolationStatus = cvs.CourtViolationStatusID           
 left outer join tblusers u on st.employeeid = u.employeeid              
WHERE   TicketID_PK in (select ticketid_pk from tbltickets)                  
 and st.scanticketid is not null    
Order By             
 ticketid,doc_num     
    
  
  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


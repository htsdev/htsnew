﻿   
  
  
  
-- [usp_hts_CompetitorReport_Archive] '05/01/2008','06/01/2008', 0 , 10     
  
-- grant execute on dbo.[usp_hts_CompetitorReport_Archive] to dbr_webuser   
create procedure [dbo].[usp_hts_CompetitorReport_Archive]              
              
@startdate datetime,              
@enddate datetime,              
@Type int,   
@NoOfRecords int          
              
as      
declare @totalclients float   
declare @totalcasess float     
  
  
  
select t.midnumber,Attorney=p.name,c.categoryid  
into #tempp                    
FROM	tblTicketsViolationsArchive AS tv 
INNER JOIN  
	tblTicketsArchive AS t 
ON	t.RecordID = tv.RecordID 
INNER JOIN  
-- TAHIR 4138 08/29/2008 CHANGED THE DATA SOURCE LOCATION FOR COMPETITOR DATABASE....
	LoaderFilesArchive.dbo.tblEventExtractTemp_competitor AS eet 
	--LoaderFilesArchive.dbo.tblEventExtractTemp AS eet 
-- END 4138
ON	REPLACE(eet.causenumber, ' ', '') = ISNULL(tv.CauseNumber, '') 
INNER JOIN  
	tblCompetitors AS p 
ON	ISNULL(eet.attorneyname, '') = p.Name 
INNER JOIN  
	tblCourtViolationStatus c ON eet.status = c.Description 
AND tv.violationstatusid = c.CourtViolationStatusID  
WHERE	(ISNULL(eet.barnumber, '') <> '' OR ISNULL(eet.attorneyname, '') <> '') 
AND		(ISNULL(eet.bondingnumber, '') = '') 
AND		(ISNULL(eet.bondingcompanyname, '') = '') 
AND		(tv.CauseNumber IS NOT NULL) 
AND		(eet.updatedate BETWEEN @startdate AND @enddate) 
AND		(tv.CourtLocation IN (3001, 3002, 3003)) 
AND		(c.CategoryID = 4)  
  
           
select @totalcasess=count(*) from #tempp     
select count(distinct midnumber) as sumBond_clients  
into #SumBond from #tempp              
group by Attorney       
  
--select * from #SumBond  
  
Set @totalclients = (select sum(sumBond_clients) from #SumBond)  
drop table #SumBond  
  
select top (@NoOfRecords) Attorney as name,count(Attorney) as noofcases,count(distinct midnumber) as sumBond_clients,  
0 as AmountSum,round(((count(Attorney)/@totalcasess)*100),2) as percentage,  
(round((count(distinct midnumber) *100 / @totalclients),2)) as perc_clients,  
0 as ArrCount,count(case when categoryid=4 then categoryid end)as JuryCount,0 as NonJuryCount          
--into #tempp1              
from #tempp              
group by Attorney       
order by noofcases desc             
  
drop table #tempp         
  
  
  go
  
  grant execute on [dbo].[usp_hts_CompetitorReport_Archive] to dbr_webuser
  go
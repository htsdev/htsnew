SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Update_ScannedReset]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Update_ScannedReset]
GO


CREATE procedure USP_HTS_Update_ScannedReset  
 (      
 @refcasenumber   varchar(20),      
 @sequencenumber  varchar(3),       
 @causenumber  varchar(30),  
 @courtnumber   varchar(3),      
 @courtdate   datetime,      
 @courtviolationstatusid int,      
 @docid    int,      
 @docnum   int,      
 @employeeid   int  
 )  
as      
      
declare @TicketViolId int,  
 @TicketId int,  
 @CourtDateScan datetime,  
 @IsUpdated bit,  
 @TicketNumber varchar(30)  
  
set  @TicketNumber = @refcasenumber + @sequencenumber  
  
-- MATCHING RECORDS BY CAUSE NUMBER................  
if len(@CauseNumber) > 0  
SELECT  @TicketViolId = v.ticketsviolationid  ,  
 @TicketId = v.ticketid_pk,  
 @CourtDateScan = v.courtdatescan  
FROM dbo.tblTicketsViolations v   
where v.casenumassignedbycourt = @causenumber  
  
-- IF NOT FOUND THEN ......  
-- MATCHING RECORDS BY CASE NUMBER (ADDING SEQ. NO)  
if @TicketViolId is null   
begin  
 SELECT  @TicketViolId = v.ticketsviolationid  ,  
  @TicketId = v.ticketid_pk,  
  @CourtDateScan = v.courtdatescan  
 FROM    dbo.tblTicketsViolations v   
 where  v.RefCaseNumber + convert(varchar(10),v.SequenceNumber) = @TicketNumber  
end  
  
  
-- IF NOT FOUND THEN......  
-- MATCHING RECORDS BY CASE NUMBER (ELEMINATION LAST CHARACTER) .............  
if @TicketViolId is null  
begin   
 SELECT  @TicketViolId = v.ticketsviolationid  ,  
  @TicketId = v.ticketid_pk,  
  @CourtDateScan = v.courtdatescan  
 FROM    dbo.tblTicketsViolations v   
 where LEFT(v.RefCaseNumber, LEN(v.RefCaseNumber) - 1) = @TicketNumber  
end  
  
-- IF NOT FOUND THEN......     
-- MATCHING RECORDS BY CASE NUMBER (COMPLETE)  
if @TicketViolId is null  
begin  
 SELECT  @TicketViolId = v.ticketsviolationid  ,  
  @TicketId = v.ticketid_pk,  
  @CourtDateScan = v.courtdatescan  
 FROM    dbo.tblTicketsViolations v   
 where   v.RefCaseNumber = @TicketNumber  
end  
  
  
-- NOW UPDATING THE INFORMATION IN TBLTICKETSVIOLATIONS......  
if ( @TicketViolId is not null and datediff(day, isnull(@CourtDateScan, '01/01/1900'), @courtdate) >= 0 )  
begin  
 update  tblticketsviolations      
 set courtdatescan= @courtdate,      
  courtnumberscan = @courtnumber,      
  courtviolationstatusidscan = @courtviolationstatusid,  
  vemployeeid = @employeeid    ,  
  updateddate = getdate()    
 where ticketsviolationid  = @TicketViolId  
  
 
  
 if left(@refcasenumber,1) = 'F'      
   set @sequencenumber = '0'      
  
 update  tblScannedDocketCourtStatus   
 set  Updateflag = 1,      
  ticketnumber = @refcasenumber + @sequencenumber,      
  causenumber = @causenumber,  
  courtdate = @courtdate,      
  courtno = @courtnumber,  
  violationstatus=@courtviolationstatusid,  
  manualupdatedate=getdate() ,  
  scanticketid = @TicketId,
  ScanTicketViolationid=@TicketViolId,
  Employeeid=@employeeid 
 where  doc_id = @docid   
 and  docnum = @docnum   
 and  Updateflag = 0      
  
 set  @IsUpdated = 1  
  
 insert into tblticketsnotes (ticketid, subject, employeeid)  
 select @ticketid, '<a href="javascript:window.open(''../paperless/PreviewMain.aspx?DocID='+convert(varchar(10),@docid)+'&RecType=0&DocNum='+convert(varchar(10),@docnum)+'&DocExt=000'');void('''');"''>Scanned Resets - Manually updated</a>',@employeeid  
  
end  
  

select isnull(@IsUpdated,0)  
  








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


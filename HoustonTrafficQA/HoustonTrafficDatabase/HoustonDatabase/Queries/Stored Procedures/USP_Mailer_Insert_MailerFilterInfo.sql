SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Insert_MailerFilterInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Insert_MailerFilterInfo]
GO









CREATE  Procedure [dbo].[USP_Mailer_Insert_MailerFilterInfo]        
--Save Mailer Filter Information         
  @courtcategorynum int,            
  @officernum varchar (500),       
  @oficernumOpr varchar (50),       
  @ticket_no varchar (500),        
  @tikcetnumberopr varchar(50),       
  @fineamount money ,       
  @fineamountOpr varchar (50),       
  @fineamountRelop varchar (50),       
  @violationdescription varchar (2000),       
  @violationdescriptionOpr varchar (50),       
  @zipcode varchar (20),       
  @zipcodeLikeopr varchar(50),       
  @Lettertype int,       
  @Singleviolation money,       
  @singlevoilationOpr varchar (50),       
  @singleviolationrelop varchar (50),       
  @doubleviolation money,       
  @doubleviolationOpr varchar (50),       
  @doubleviolationrelop varchar (50)            
        
As        
      
begin        
      
Insert into  tblmailer_letters_to_sendfilters       
(        
  courtcategorynum,       
 officernum,       
 oficernumOpr,       
 ticket_no,        
 tikcetnumberopr ,             
 fineamount ,       
 fineamountOpr,       
 fineamountRelop ,       
 violationdescription ,       
 violationdescriptionOpr,       
 zipcode,       
  zipcodeLikeopr,        
  Lettertype ,       
  Singleviolation ,       
  singlevoilationOpr,       
  singleviolationrelop,       
  doubleviolation ,       
  doubleviolationOpr,       
  doubleviolationrelop       
 )        
        
Values (         
  @courtcategorynum,            
  @officernum,       
  @oficernumOpr,       
  @ticket_no,        
  @tikcetnumberopr,       
  @fineamount,       
  @fineamountOpr,       
  @fineamountRelop ,       
  @violationdescription ,       
  @violationdescriptionOpr,       
  @zipcode,       
  @zipcodeLikeopr,       
  @Lettertype ,       
  @Singleviolation ,       
  @singlevoilationOpr,       
  @singleviolationrelop ,       
  @doubleviolation ,       
  @doubleviolationOpr ,       
  @doubleviolationrelop          
 )        
end        
      
        
        
        
        
        
        
        
        
        
        
      
    
  
          
          
          
          
          
          
          
          
          
          











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


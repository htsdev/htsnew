SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Update_MailFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Update_MailFlag]
GO



CREATE PROCEDURE USP_HTS_LA_Update_MailFlag

@ID as int,
@ID_Mode as bit,
@Flag_Updated int Output

AS  
Declare @RecordID as int
Set @RecordID = 0

-- @ID_Mode = 0 Means @ID has RecordID
If @ID_Mode = 0
	Begin
		Update tblTicketsArchive Set DoNotMailFlag = 1 Where (RecordID = @ID)
		Set @Flag_Updated = 1
	End
-- @ID_Mode = 1 Means @ID has NoteID
Else
	Begin
		Set @RecordID = (Select RecordID From TblLetterNotes Where NoteID = @ID)
		Update tblTicketsArchive Set DoNotMailFlag = 1 Where (RecordID = @RecordID)
		Set @Flag_Updated = 1
	End
Return @Flag_Updated


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿/**  
* Nasir 5864 06/25/2009  
* Business Logic: This procedure is use to get data for Motion To Request Letter  
* 
* List of parameter:
* First Name, 
* Last name,
* Case number,
* AttorenyFirstName,
* AttorneyLastName,
* AttorneyBarCardNumber,
* ALRBTOFirstName,
* ALRBTOLastName,
* ALRBTSFirstName,
* ALRBTSLastName

**/    
  
  
alter PROCEDURE dbo.USP_HTP_Get_ALR_Motion_To_Request_Letter
 @TicketID INT  
AS  
 SELECT distinct tt.Firstname  
       ,tt.Lastname  
       ,CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end AS CaseNumber
       ,tf.AttorenyFirstName as AttorenyFirstName         
       ,tf.AttorneyLastName as AttorneyLastName  
       ,tf.AttorneyBarCardNumber AS AttorneyBarCardNumber  
       ,tt.ALRBTOFirstName AS ALRBTOFirstName  
       ,tt.ALRBTOLastName AS ALRBTOLastName  
       ,CASE WHEN tt.ALRBTOBTSSameFlag =1 THEN tt.ALRBTOFirstName
          ELSE tt.ALRBTSFirstName
          END  AS ALRBTSFirstName  
       ,CASE WHEN tt.ALRBTOBTSSameFlag =1 THEN tt.ALRBTOLastName
          ELSE tt.ALRBTSLastName
          END  AS ALRBTSLastName
         
 FROM   tblTickets tt  
        INNER JOIN tblTicketsViolations ttv  
             ON  ttv.TicketID_PK = tt.TicketID_PK  
        INNER JOIN tblFirm tf  
             ON  tt.CoveringFirmID = tf.FirmID  
 WHERE tt.TicketID_PK=@TicketID
 AND ttv.ViolationNumber_PK=16159  
 
 
 GO
 GRANT EXECUTE ON dbo.USP_HTP_Get_ALR_Motion_To_Request_Letter TO dbr_webuser
 GO 
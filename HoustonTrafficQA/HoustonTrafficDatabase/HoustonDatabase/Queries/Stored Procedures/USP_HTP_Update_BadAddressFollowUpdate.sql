/*        
Created By		: Sabir Khan.
Created Date	: 12/24/2010  
TasK			: 8488        
Business Logic  : This procedure Updates Bad Address Follow Up Date.      
           
Parameter:       
   @TicketID     : Updating criteria with respect to TicketId      
   @FollowUpDate : Date of follow Up which has to be set     
     
      
*/      


CREATE PROCEDURE dbo.USP_HTP_Update_BadAddressFollowUpdate
	@TicketID INT,
	@FollowUpDate DATETIME
AS
BEGIN
    IF EXISTS(
           SELECT *
           FROM   tbltickets
           WHERE  Ticketid_pk = @TicketID
       )
    BEGIN
        UPDATE tbltickets
        SET BadAddressFollowUpDate = @FollowUpDate
        WHERE  TicketID_PK = @TicketID
    END
    ELSE
    BEGIN
        INSERT INTO tbltickets
          (
            Ticketid_pk,
            BadAddressFollowUpDate
          )
        VALUES
          (
            @TicketID,
            @FollowUpDate
          )
    END
END
GO
GRANT EXECUTE ON dbo.USP_HTP_Update_BadAddressFollowUpdate TO dbr_webuser
GO
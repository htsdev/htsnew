SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ADD_TelNo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ADD_TelNo]
GO



Create Procedure [dbo].[USP_HTS_ADD_TelNo]
(

@TicketID_Pk int,
@Contact1 varchar(14),
@Contact2 varchar(14),
@Contact3 varchar(14),
@ContactType1 int,
@ContactType2 int,
@ContactType3 int

)
as


Update tbltickets 

Set 

Contact1 = @Contact1,
Contact2 = @Contact2,
Contact3 = @Contact3,
ContactType1 = @ContactType1,
ContactType2 = @ContactType2,
ContactType3 = @ContactType3

where ticketid_pk = @TicketID_Pk

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_update_trialcomments_for_Ticket_ID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_update_trialcomments_for_Ticket_ID]
GO


CREATE   PROCEDURE [dbo].[usp_hts_update_trialcomments_for_Ticket_ID]     
  
@TicketNumber varchar(50) ,  
@trialcomments Varchar(50)  ,  
@EMPID INT   
  
  
as    
--   if (right(@trialcomments,1) <> ')' and left(@trialcomments,1) <> '')       --if trialcomments are updated    
 if (right(@trialcomments,1) <> ')' )       --if trialcomments are updated    
   begin  
  declare @empnm as varchar(12)              
  select @empnm = abbreviation from tblusers where employeeid = @empid        
     
  if len(@trialcomments) > 0
  	set @trialcomments = @trialcomments +' ' + '(' +dbo.formatdateandtimeintoshortdateandtime(getdate()) + ' - ' +  @empnm + ')' + ' '        
        
update 	t
set 	t.trialcomments = @trialcomments, 
	t.employeeidupdate = @empid  
from 	tbltickets t 
inner join
	tblticketsviolations v
on 	v.ticketid_pk = t.ticketid_pk
where v.refcasenumber = @TicketNumber
             
end    
    
   

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


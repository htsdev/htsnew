SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_LA_Update_Tickets_Disposed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_LA_Update_Tickets_Disposed]
GO


CREATE  PROCEDURE USP_HTS_LA_Update_Tickets_Disposed

@Record_Date datetime,  
@List_Date datetime,  
@Cause_Number as Varchar(20),
@Ticket_Number as Varchar(15),
@Updated_Date Smalldatetime,
@GroupID as Int,
@Flag_Updated int Output

AS  
Declare @IDFound as int

If @Updated_Date = '1/1/1900'
Begin
	Set @Updated_Date = Null
End
Set @Cause_Number = Left(RTrim(LTrim(@Cause_Number)), 30)
Set @Ticket_Number = Left(RTrim(LTrim(@Ticket_Number)), 20)
Set @IDFound = 0
Set @Cause_Number = Replace(@Cause_Number, ' ', '')
Set @Flag_Updated = 0

If Len(@Ticket_Number) < 1
	Begin
		Set @Ticket_Number = @Cause_Number
	End

Set @IDFound = (Select top 1 RecordID From tblTicketsViolationsArchive Where (CauseNumber = @Cause_Number Or (TicketNumber_LA = @Ticket_Number And TicketNumber_LA <> '' And TicketNumber_LA Is Not Null)) And (UpdatedDate < @Updated_Date Or UpdatedDate Is Null))

If @IDFound Is Not Null
	Begin
		Update tblTicketsArchive Set GroupID = @GroupID Where (RecordID = @IDFound)
		Update tblTicketsViolationsArchive Set ViolationStatusID = 80, UpdatedDate = @Updated_Date, LoaderUpdateDate = GetDate() Where (CauseNumber = @Cause_Number Or (TicketNumber_LA = @Ticket_Number And TicketNumber_LA <> '' And TicketNumber_LA Is Not Null)) And (UpdatedDate < @Updated_Date Or UpdatedDate Is Null)
		Update tblTicketsViolations Set UpdatedDate = @Updated_Date, CourtViolationStatusID = 80 Where (CaseNumAssignedByCourt = @Cause_Number And (UpdatedDate < @Updated_Date Or UpdatedDate Is Null))
		Set @Flag_Updated = 1
	End

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


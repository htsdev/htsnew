SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_OFFICERS_INFO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_OFFICERS_INFO]
GO









CREATE Procedure USP_HTS_GET_OFFICERS_INFO   
  
@OfficerNumber int =0,  
@FirstName varchar(20)  ='%' ,  
@LastName  varchar(20)   ='%'    
  
  
as  
  
if @OfficerNumber = 0  
Begin  
  
SELECT OfficerNumber_PK, FirstName, LastName, OfficerType, TrialDateTime , Comments FROM tblOfficer  
where FirstName like   @FirstName+'%'  
  
AND LastName like @LastName +'%'  
  
end  
  
else  
  
Begin  
SELECT OfficerNumber_PK, FirstName, LastName, OfficerType, TrialDateTime , Comments FROM tblOfficer  
where OfficerNumber_PK = @OfficerNumber  
end  
  
  
  






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


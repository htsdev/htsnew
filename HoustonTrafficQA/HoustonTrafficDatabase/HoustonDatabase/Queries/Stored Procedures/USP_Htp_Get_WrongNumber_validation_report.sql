/* 
Created By : Noufil Khan
Business Logic : All those records will display whos wrong number flag is active and court date should be entered by user and sort by recent court date
*/
 
CREATE procedure [dbo].[USP_Htp_Get_WrongNumber_validation_report]  
as  
select distinct  
  t.TicketID_PK as ticketid,   
  --TV.TicketsViolationID as ticketViolationId,   
  (t.Lastname + ',' + ' '+ t.Firstname) as [Name],   
  t.LanguageSpeak as [Language],   
 case when isnull(T.Contact1,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''))+' '+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' else '' end as contact1,  
 case when isnull(T.Contact2,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''))+' '+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' else '' end as contact2,   
 case when isnull(T.Contact3,'') <> ''then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''))+' '+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' else '' end as contact3,    
case t.BondFlag when 1 then 'Y' else 'N' end as Bond, 
c.ShortName as CRT,   
TrailDesc=dbo.fn_DateFormat(isnull(tv.CourtDateMain,Convert(datetime,'1/1/1900')),30,'/',':',1) + ' ' + isnull(cvs.shortdescription,'')  
  --dbo.fn_hts_get_TicketsViolations (t.TicketID_PK,TV.CourtDateMain) as trialdesc,    
   
  from tbltickets t  
  inner join tblticketsviolations TV  
   on t.TicketID_PK = TV.TicketID_PK   
    
  inner join                                            
 dbo.tblCourtViolationStatus cvs                                 
ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID     
  Left Outer join tblTicketsFlag  
    On tblTicketsFlag.TicketID_PK= t.TicketID_PK   
  
  Left Outer join  
    tblcourts c  
    on TV.CourtID=c.Courtid  
  
  LEFT OUTER JOIN   
   dbo.tblContactstype ct2   
    ON t.ContactType2 = ct2.ContactType_PK  
  LEFT OUTER JOIN   
   dbo.tblContactstype ct1   
    ON t.ContactType1 = ct1.ContactType_PK   
  LEFT OUTER JOIN   
   dbo.tblContactstype ct3   
    ON t.ContactType3 = ct3.ContactType_PK   
  
  where  tblTicketsFlag.FlagID=33 --and tv.ticketid_pk = 165749  
and TV.CourtDateMain >= getdate() --and TV.CourtDateMain = '2009-04-01 08:00:00.000'  
and TV.CourtDateMain = (Select max(CourtDateMain) from tblTicketsViolations where ticketid_pk = tv.ticketid_pk)  
  
  
union  
  
  
  
select distinct  
  t.TicketID_PK as ticketid,   
  --TV.TicketsViolationID as ticketViolationId,   
   (t.Lastname + ',' + ' '+ t.Firstname) as [Name],   
  t.LanguageSpeak as [Language],     
  case when isnull(T.Contact1,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''))+' '+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' else '' end as contact1,  
 case when isnull(T.Contact2,'') <> '' then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''))+' '+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' else '' end as contact2,   
 case when isnull(T.Contact3,'') <> ''then convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''))+' '+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' else '' end as contact3,    
  case t.BondFlag when 1 then 'Y' else 'N' end as Bond,  
  c.ShortName as CRT,   
 TrailDesc=dbo.fn_DateFormat(isnull(tv.CourtDateMain,Convert(datetime,'1/1/1900')),30,'/',':',1) + ' ' + isnull(cvs.shortdescription,'')  
  --dbo.fn_hts_get_TicketsViolations (t.TicketID_PK,TV.CourtDateMain) as trialdesc,    
  --case t.BondFlag when 1 then 'Y' else 'N' end as BondFlag   
  from tbltickets t  
  inner join tblticketsviolations TV  
   on t.TicketID_PK = TV.TicketID_PK   
  
  inner join                                            
 dbo.tblCourtViolationStatus cvs                                 
ON  TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID     
  Left Outer join tblTicketsFlag  
    On tblTicketsFlag.TicketID_PK= t.TicketID_PK   
  
  Left Outer join  
    tblcourts c  
    on TV.CourtID=c.Courtid  
  
  LEFT OUTER JOIN   
   dbo.tblContactstype ct2   
    ON t.ContactType2 = ct2.ContactType_PK  
  LEFT OUTER JOIN   
   dbo.tblContactstype ct1   
    ON t.ContactType1 = ct1.ContactType_PK   
  LEFT OUTER JOIN   
   dbo.tblContactstype ct3   
    ON t.ContactType3 = ct3.ContactType_PK   
  
  where  tblTicketsFlag.FlagID=33   
and TV.CourtDateMain < getdate()   
and TV.CourtDateMain = (Select max(CourtDateMain) from tblTicketsViolations where ticketid_pk = tv.ticketid_pk)  

go  
grant execute on [dbo].[USP_Htp_Get_WrongNumber_validation_report] to dbr_webuser  
go
  
  
      
  
  
  
  
  
  
  
  



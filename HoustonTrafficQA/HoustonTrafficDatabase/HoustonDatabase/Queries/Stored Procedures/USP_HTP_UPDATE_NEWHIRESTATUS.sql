/*
* Created By :- Noufil 6126 07/24/2009
* Business Logic : This procedure updated client's new hire status 
*				   if @updatewithbusinessdays = 0 then update new hire status with inputed @newhirestatus and add note to history if @newhirestatus=2 (contacted)
*				   else Update new hire status to 1 with @updatewithbusinessdays = 1 (true). Check for diffenrece between courtdate and hire date. IF lies between
*				   specified difference with status in Jury,Judge or pre-trial status 
* Parameter List:
*     	@newhirestatus
*	@ticketid
*	@empid
*	@updatewithbusinessdays
*				   						
*/
CREATE PROCEDURE [dbo].[USP_HTP_UPDATE_NEWHIRESTATUS]
	@newhirestatus INT,
	@ticketid INT,
	@empid INT,
	@updatewithbusinessdays BIT
AS

IF @updatewithbusinessdays = 0
BEGIN

	UPDATE tblTickets
	SET    NewHireCourtCoverageStatus = @newhirestatus
	WHERE  TicketID_PK = @ticketid
	
	IF (@newhirestatus = 2)
	BEGIN
	    INSERT INTO tblticketsnotes
	      (
	        ticketid,
	        [subject],
	        recdate,
	        EmployeeID
	      )
	    VALUES
	      (
	        @ticketid,
	        'Client contacted from traffic new hire report',
	        GETDATE(),
	        @empid
	      )
	END
END
ELSE
	BEGIN
		DECLARE @businessdays INT
	SET @businessdays = (
	        SELECT tbld.Attribute_Value
	        FROM   Tbl_BusinessLogicDay tbld
	        WHERE  tbld.Report_Id = (
									   SELECT tr.Report_ID
									   FROM   tbl_Report tr
									   WHERE  tr.Report_Url = '/Reports/NewHireReport.aspx'
									)
	)	
	IF EXISTS(SELECT ttv.TicketID_PK       
				FROM   tblTicketsViolations ttv
					INNER JOIN tblTicketsPayment ttp
						ON  ttv.TicketID_PK = ttp.TicketID
					INNER JOIN tblCourtViolationStatus tcvs
						ON  tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
				WHERE  ttp.PaymentVoid = 0
						AND tcvs.CategoryID IN (3, 4, 5)
				       AND ttv.TicketID_PK=@ticketid
				GROUP BY
					   ttp.RecDate,
					   ttv.CourtDateMain,
					   ttv.TicketID_PK
				HAVING DATEDIFF(DAY, MIN(ttp.RecDate), ttv.CourtDateMain) >= 0 AND DATEDIFF(DAY, [dbo].[fn_getnextbusinessday](MIN(ttp.RecDate),@businessdays), ttv.CourtDateMain) 
					   <= 0)
	   BEGIN
	   	UPDATE tbltickets SET NewHireCourtCoverageStatus = 1 WHERE TicketID_PK=@Ticketid
	   END
	END
GO

GRANT EXECUTE ON [dbo].[USP_HTP_UPDATE_NEWHIRESTATUS] TO dbr_webuser
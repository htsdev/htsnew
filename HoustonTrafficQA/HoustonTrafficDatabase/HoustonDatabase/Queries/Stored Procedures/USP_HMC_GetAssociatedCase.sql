SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HMC_GetAssociatedCase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HMC_GetAssociatedCase]
GO






CREATE procedure [dbo].[USP_HMC_GetAssociatedCase] --2206907           
                     
@RecordId int        
        
as                     
                
Select     case       
 when T.ViolationNumber_PK = 0 then T.TicketNumber_pk      
 else T.TicketNumber_pk +'-'+convert(varchar(4), T.ViolationNumber_PK)      
 end as TicketNumber,       
      
    TV.midnumber,                         
 T.ViolationDescription,                         
    T.FineAmount,                         
    TV.FirstName,                         
    TV.LastName,                         
 TV.initial ,      
    T.CauseNumber,                 
    TV.DOB,                         
    TV.ViolationDate,                          
  T.CourtNumber  ,                         
 T.CourtDate,  
             
 TV.RecordID ,                   
    isnull(T.BondAmount,0) as BondAmount ,       
   tcv.ShortDescription,
 case  when len(T.violationdescription) > 15 then left(t.violationdescription,10) + '...'                
  else T.violationdescription                
  end as VSdescription  
          
 into #temp                  
    from tblTicketsViolationsArchive T inner join tblTicketsArchive TV                         
     on tv.recordid = t.recordid                
 join tblcourtviolationstatus tcv  
 on T.violationstatusid = tcv.courtviolationstatusid  
 where t.recordid = @recordid               
                 
                
select * from #temp                  
drop table #temp     


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


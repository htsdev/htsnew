/*
* Created by : Noufil Khan agaisnt 6052
* Business Logic : This procedure returns all criminal clients cases whose case is dispose and attorney fees is not yet paid.
* Parameter : 
*			@startdate
			@enddate
			@Attorney
			@ReportURL
			
  Column return : 
			   TicketID_PK,
			   TicketsViolationID,
			   Firstname,
			   Lastname,
			   ticketnumber,
			   ViolationNumber_PK,
			   ChargeLevel,
			   violation Description,
			   calculatedtotalfee
			   chargeamount
			   dispositiondate,
			   FirmName
			   firmaddress
			   adressinfo
			   URL
			   AttorneyfeeR
			   lockprice
*/

-- [dbo].[USP_HTP_GetAttorneyPayout_Report] '03/22/2011','03/22/2011', 3041,''
-- Noufil 8090 07/28/2010 Restruct whole store procedure to reduce excution time.
ALTER PROCEDURE [dbo].[USP_HTP_GetAttorneyPayout_Report]
	@startdate DATETIME,
	@enddate DATETIME,
	@Attorney INT,
	@ReportURL VARCHAR(200)
AS
	SELECT tt.TicketID_PK,
	       ttv.TicketsViolationID,
	       tt.Firstname,
	       tt.Lastname,
	       CASE WHEN  LEN( ISNULL(ttv.casenumassignedbycourt, '')) =0 THEN ttv.refcasenumber ELSE ttv.casenumassignedbycourt
	       END AS ticketnumber,
	       ttv.ViolationNumber_PK,
	       tc.LevelCode AS ChargeLevel,
	       tv.[Description],
	       tt.calculatedtotalfee,
	       (
	           SELECT SUM(ISNULL(ttp2.ChargeAmount, 0))
	           FROM   tblTicketsPayment ttp2
	           WHERE  ttp2.TicketID = tt.TicketID_PK
	       ) AS chargeamount,
	       tf.FirmName,
	       CASE WHEN LEN(ISNULL(tf.[Address],'')) = 0 THEN tf.Address2 ELSE tf.[Address] END AS firmaddress,
	       tf.City + ', ' + ts.[State] + ' ' + tf.Zip AS adressinfo,
	       @ReportURL AS URL,
	       ISNULL(fcl.ChargeAmount, 0.0) AS Attorneyfee,
	       ISNULL(tt.totalfeecharged, 0) AS lockprice,
	       ttv.CourtViolationStatusIDmain AS CourtViolationStatusIDmain
	       INTO #temp1
	FROM   tblTickets tt
	       INNER JOIN tblTicketsViolations ttv
	            ON  ttv.TicketID_PK = tt.TicketID_PK
	       INNER JOIN tblViolations tv
	            ON  tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	       INNER JOIN tblTicketsPayment ttp
	            ON  ttp.TicketID = tt.TicketID_PK
	       INNER JOIN tblChargelevel tc
	            ON  tc.ID = ttv.ChargeLevel
	       LEFT OUTER JOIN tblFirm tf
	            ON  tf.FirmID = ttv.CoveringFirmID
	       LEFT OUTER JOIN tblState ts
	            ON  tf.[State] = ts.StateID
	       LEFT OUTER JOIN FirmChargeLevel fcl
	            ON  fcl.FirmID = ttv.CoveringFirmID
	            AND fcl.ChargeLevelID = ttv.ChargeLevel
	WHERE  tt.CaseTypeId = 2
	       AND tt.Activeflag = 1
	       AND (ttp.PaymentVoid <> 1)
	       AND (ttv.CoveringFirmID = @Attorney)
	       AND ISNULL(ttv.IsAttorneyPayoutPrinted, 0) = 0
	ORDER BY
	       tt.Lastname,
	       tt.TicketID_PK
	
	
	SELECT DISTINCT ttvl.TicketviolationID, -- Sabir Khan 8805 04/11/2011 need to display blank incase of 1/1/1900 as disposition date.
	       Case Convert(varchar,MAX(ttvl.newverifiedDate),101) when '01/01/1900' then '' else dbo.fn_DateFormat(MAX(ttvl.newverifiedDate),24,'/',':',1) end  AS dispositiondate
	       INTO #temp2
	FROM   tblTicketsViolationLog ttvl 
	INNER JOIN #temp1 b ON b.TicketsViolationID = ttvl.TicketviolationID
	WHERE  ttvl.oldverifiedstatus <> ttvl.newverifiedstatus
	       -- AND ttvl.newverifiedstatus = 80   //Sabir Khan 8805 03/14/2011 Should not check case status for disposed when pre trial diversion flag is active.
	GROUP BY
	       ttvl.TicketviolationID
	
	SELECT DISTINCT tt1.*,Case Convert(varchar,tt2.dispositiondate,101) when '01/01/1900' then '' else tt2.dispositiondate end as dispositiondate
	FROM   #temp2 tt2
	       INNER JOIN #temp1 tt1
	            ON  tt2.TicketviolationID = tt1.TicketsViolationID
	WHERE  (
	           (
	               tt1.CourtViolationStatusIDmain = 80
	               AND (
	                       DATEDIFF(DAY, @startdate, Convert(datetime,left(tt2.dispositiondate,10))) > = 0
	                       AND DATEDIFF(DAY, Convert(datetime,left(tt2.dispositiondate,10)), @enddate) > =
	                           0
	                   )
	           )
	           OR (
	                  tt1.TicketID_PK IN (SELECT ttf.TicketID_PK
	                                      FROM   tblTicketsFlag ttf
	                                      WHERE  ttf.FlagID = 41)
	              )
	       )
	
	DROP TABLE #temp1
	DROP TABLE #temp2



﻿/***************************************************************************************
* Created By		: Anonymous
* Created Date		: N/A
* 
* Buisness Logic	: Get the number record to in the batch fro ticket and letter type
* 
* Parameter List	:
*						@TicketID		: Ticket ID of record
*						@LetterType		: Letter Type of a case
 ***************************************************************************************/

ALTER PROCEDURE [dbo].[USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT]
(
    @TicketID    AS INT,
    @LetterType  AS INT = 2 -- Abid Ali 5359 01/01/2009 By defualt letter type should be trial letter
)
AS
	SELECT COUNT(*) AS RecordCount
	FROM   tblHTSBatchPrintLetter
	WHERE  TicketID_FK = @TicketID
	       AND IsPrinted = 0
	       AND DeleteFlag <> 1
	       AND LetterID_FK = @LetterType -- Abid Ali 5359 01/01/2009 filter by letter type



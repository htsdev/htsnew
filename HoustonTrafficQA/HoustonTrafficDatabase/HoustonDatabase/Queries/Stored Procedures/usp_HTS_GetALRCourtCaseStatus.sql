SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetALRCourtCaseStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetALRCourtCaseStatus]
GO



    
CREATE PROCEDURE  [dbo].[usp_HTS_GetALRCourtCaseStatus]          
@IsDisposition bit = 0           
AS            
  
if @IsDisposition = 1   
 Begin  
          
  Select  CourtViolationStatusID as ID,Description  From tblCourtViolationStatus          
  where courtviolationstatusid in(104,237,80)          
  and statustype =1    ORDER BY SORTORDER      
 End  
Else  
 Begin  
  Select  CourtViolationStatusID as ID,Description  From tblCourtViolationStatus          
  where courtviolationstatusid in(104,237)          
  and statustype =1   ORDER BY SORTORDER      
 End  
  
  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


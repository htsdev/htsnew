SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_PCCR_Get_Info]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_PCCR_Get_Info]
GO


-- -- procedure altered by Shekhani [06-26-2007]

CREATE	procedure [dbo].[usp_PCCR_Get_Info] 
	(
	@FromDate		datetime ,  --{	INPUT DATE RANGE PARAMETER }
	@ToDate			datetime ,  --{	INPUT DATE RANGE PARAMETER }
	@IsShowHiredHistory 	bit = 0,    --{ IF ONLY HIRING HISTORY REQUIRED... }
	@User			int = 0,    --{ IF RECORDS OF A PARTICULAR USER REQUIRED......}
	@PCCR			int = 0     --{ IF RECORDS OF SPECIFIC QUOTE STATUS REQUIRED.....}
	)

as

-- VARIABLES DECLARATION.........
declare	@strSQL		nvarchar(4000),
	@strParam	nvarchar(400)

-- DEFINING PARAMETER LIST FOR DYNAMIC SQL..........
select	@strParam = '@FromDate datetime, @ToDate datetime, @IsShowHiredHistory bit, @User int, @PCCR int'

-- ADDING TIME PART TO TODATE.........
select	@Todate = @Todate + '23:59:59'

declare @SqlScript  as nvarchar(3000)   
declare @param   as nvarchar(200)                      

Set @SqlScript ='                   
         
declare 
@casedescid as int

select @casedescid =  0
 '
                    
                   
declare @pendingID int    
set @pendingID = (select StatusID from tblpccrstatus where StatusDescription = 'PENDING')    

-- DECLARING TEMPORARY TABLE........                      
Set @SqlScript = @SqlScript + '                   
declare @tblClientLookup TABLE ( 
 [LastName] [varchar] (50), 
 [FirstName] [varchar] (50), 
 [ListDate] [datetime] NULL ,
 [MidNumber] [varchar] (20)  ,
 [CourtDate] [datetime] NULL,
 [TicketNumber] [varchar] (20),
[recordid] [int]    
) 

'                  
                  
-- INSERTING RECORDS IN TEMPORARY TABLE ON THE SPECIFIED CRITERIA.........                  
Set @SqlScript = @SqlScript +'                     
INSERT INTO @tblClientLookup 
SELECT DISTINCT 
 ta.LastName, 
 ta.FirstName, 
 ta.ListDate, 
 ta.MidNumber, 
 min(tva.CourtDate),
 ta.TicketNumber,
ta.recordid	 
FROM    dbo.tblDateType dt 
INNER JOIN 
 dbo.tblCourtViolationStatus cvs 
ON  dt.TypeID = cvs.CategoryID 
RIGHT OUTER JOIN 
 dbo.tblTicketsArchivePCCR tp 
RIGHT OUTER JOIN 
 dbo.tblTicketsViolationsArchive tva 
INNER JOIN 
 dbo.tblTicketsArchive ta 
--ON  tva.TicketNumber_PK = ta.TicketNumber 
on tva.recordid = ta.recordid 
ON  tp.RecordID_FK = ta.recordID 
ON  cvs.CourtViolationStatusID = tva.violationstatusid 

WHERE   ta.Clientflag = 0 
and ta.listdate between @FromDate and @ToDate 
'                  
    
  Set @SqlScript = @SqlScript + 'AND ((dt.typeid in (2,5)) or (dt.typeid  is null)) 
'                      
                  
   Set @SqlScript = @SqlScript +'and tp.PCCRStatusID is null 
'                      
              
                  
-- GROUPING THE RECORDS TO GET THE DISTINCT RECORDS....                  
Set @SqlScript = @SqlScript + 'group by ta.lastname, ta.firstname, ta.listdate, ta.midnumber, tva.CourtDate, ta.TicketNumber, ta.recordid	 
'                  
                      
-- NOW GETTING THE FINAL OUTPUT BY FETCHING RECORDS FROM THE TEMPORARY TABLE.                  
-- RECORDS HAVING AN ENTRY IN TBLRESTRICTED PHONE NUMBERS WILL NOT BE RETRIEVED...                  
Set @SqlScript = @SqlScript + '    
declare @tblClientLookupDist TABLE ( 
 [ClientName] [varchar] (200),
 [ListDate] [datetime] NULL ,  
 [CourtDate] [datetime] NULL,                    
 [MidNumber] [varchar] (20)  , 
 [Language]  [varchar] (20),                 
 [TicketNumber] [varchar] (20),
 [recordid] [int]                  
) 

insert into @tblClientLookupDist
SELECT   distinct                     
 b.[LastName]+ '' '' + b.[FirstName] as clientname,
 b.ListDate  , 
 b.CourtDate , 
 b.MidNumber,  
 isnull(max(t.LanguageSpeak),''N/A'')AS Language,
 b.TicketNumber,
 b.recordid 
FROM  dbo.tblTickets  t 
INNER JOIN 
 @tblClientLookup b 
ON  t.Midnum = b.MidNumber'
          
if(@PCCR = -2)    
	begin    
		set @SqlScript = @SqlScript + ' WHERE 1=1 '    
	end    
else    
	begin    
		SET @SqlScript = @SqlScript + '                 
			 where contact1 not in (select distinct phonenumber from tblrestrictedphonenumbers) 
			 and  contact2 not in (select distinct phonenumber from tblrestrictedphonenumbers)  
			 and contact3 not in (select distinct phonenumber from tblrestrictedphonenumbers)  
			'                  
		end          
          
Set @SqlScript = @SqlScript + 'AND (t.Activeflag = 1) 
'    
                  
set @SqlScript = @Sqlscript + ' and t.FirmID = 3000 '                      
Set @SqlScript = @SqlScript + 'group by b.[LastName]+ '' '' + b.[FirstName], b.listdate, b.courtdate, b.midnumber, b.courtdate, b.TicketNumber, b.recordid 
order by b.CourtDate desc,[ClientName], b.recordid	 

'
-- Clear the content of above query, 
	-- IF ONLY HIRED HISTORY IS REQURIED.......
	-- if @PCCR status is not equal to pending ......

if (@IsShowHiredHistory = 1 or ( @PCCR <> 10 and @PCCR <> 0))
	begin 
		set @SqlScript = '';
	end 


select	@strSQL = @SqlScript + '
declare @tblpccr TABLE (  
 [casenumber] [varchar] (20),
 [Client] [varchar] (150), 
 [username] [varchar] (50), 
 [insertdate] [datetime] NULL, 
 [pccrstatus] [varchar] (100) , 
 [Comments] [varchar] (2000)  , 
 [recordid] [int] 
) 
insert into @tblpccr
	SELECT distinct	upper(t.ticketnumber) as casenumber,
		upper(t.lastname+'' ''+t.firstname) as Client,
		upper(u.username) as username, 
		PCCR.lastupdatedate as insertdate, 
		upper(s.StatusDescription) as pccrstatus, 
		isnull(upper(PCCR.Comments),''N/A'') as comments, 
		t.recordid
	FROM    dbo.tblTicketsArchivePCCR pccr 
	INNER JOIN	
		dbo.tblticketsarchive t
	ON	t.recordid = pccr.recordid_fk
	INNER JOIN
		dbo.tblUsers u 
	ON 	PCCR.EmployeeID = u.EmployeeID 
	INNER JOIN
		dbo.tblPCCRStatus s 
	ON 	PCCR.PCCRStatusID = s.StatusID
'
	-- FILTERING RECORDS AS PER SELECTION CRITERIA....				
	select	@strSQL = @strSQL + ' where	pccr.lastupdatedate between @fromdate and @todate
	'

-- IF A PARTICULAR USER IS SELECTED IN SELECTION CRITERIA
if @user <> 0 
	begin
		select	@strSQL = @strSQL + '
		and 	pccr.employeeid = @user
		'
	end

-- IF A PARTICULAR PCCR STATUS IS SELECTED IN SELECTION CRITERIA....
-- and hired history flag is 0 then  -- Added by shekhani
if @pccr <> 0 and @IsShowHiredHistory <> 1
	begin
		select	@strSQL = @strSQL + '
		and 	s.statusid = @pccr
		'
	end

-- IF ONLY HIRED HISTORY IS REQURIED.......
if @IsShowHiredHistory = 1
	begin
		select	@strSQL = @strSQL + '
		and 	pccr.IsSignUp = 1
		'
	end

-- ORDER BY CLUASE................
	select @strSQL = @strSQL + '
	order by
		pccr.lastupdatedate desc

select * from @tblpccr '

if (@IsShowHiredHistory = 0 and (@PCCR = 10 or @PCCR = 0))
	begin 
		select @strSQL = @strSQL + '
			union all
			select [TicketNumber],[ClientName] as Client, '' '' as UserName, null as [CourtDate], '' PENDING '' as PCCRStatus, 
			 '' '' as Comments, [recordid]   from @tblClientLookupDist 
	order by recordid

	' 
	end 
else 
	begin
		select @strSQL = @strSQL + '	
		order by recordid
	'
	end 

print @strSQL

-- EXECUTING THE QUERY STRING............
execute sp_executesql @strSQL, @strParam, @FromDate, @ToDate, @IsShowHiredHistory, @User, @PCCR










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


﻿ USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Insert_MDLCase]    Script Date: 10/10/2012 21:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** 
Create by		: Zeeshan Haider
Created Date	: 10/02/2012
TasK			: 10437

Business Logic : 
			This procedure insert MDL Cases
			
List of Parameters:	
			@CaseNumber	MDL Case Number
			@Date		Date of insertion/modification
			@EmployeeID	Employee identification
			@isActive	active/inactive flag					
******/

CREATE PROCEDURE [dbo].[USP_HTP_Insert_MDLCase]
	@CaseNumber VARCHAR(20),
	@Date DATETIME = '01/01/1900',
	@EmployeeID VARCHAR(50),
	@isActive BIT
AS
BEGIN
    INSERT INTO [TrafficTickets].[dbo].[tblMDLCases]
      (
        [CaseNumber],
        [InsertDate],
        [InsertedBy],
        [isActive]
      )
    VALUES
      (
        @CaseNumber,
        @Date,
        @EmployeeID,
        @isActive
      )
END



GO
GRANT EXECUTE ON [dbo].[USP_HTP_Insert_MDLCase] TO dbr_webuser
GO
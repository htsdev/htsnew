SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_CourtInfoByRecDate__backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_CourtInfoByRecDate__backup]
GO


CREATE PROCEDURE [dbo].[usp_Get_All_CourtInfoByRecDate] 

@RecDate DateTime

AS

SELECT     dbo.tblTickets.CurrentCourtloc, dbo.tblCourts.CourtName, COUNT(dbo.tblTickets.CurrentCourtloc) AS Trans, 
		SUM(dbo.tblTicketsPayment.ChargeAmount)  AS Amount
FROM         dbo.tblCourts RIGHT OUTER JOIN
                      dbo.tblTickets ON dbo.tblCourts.Courtid = dbo.tblTickets.CurrentCourtloc RIGHT OUTER JOIN
                      dbo.tblTicketsPayment ON dbo.tblTickets.TicketID_PK = dbo.tblTicketsPayment.TicketID
WHERE     (dbo.tblTicketsPayment.PaymentVoid <> 1)  AND dbo.tblTicketsPayment.PaymentType IN (1,2,5,6,7,9)				
  	      And ( dbo.tblTicketsPayment.RecDate  Between  @RecDate AND @RecDate+1)
GROUP BY dbo.tblTickets.CurrentCourtloc, dbo.tblCourts.CourtName
Order By dbo.tblTickets.CurrentCourtloc

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/******  
Created by:  Yasir kamal
Taskid:5623
Business Logic : This procedure is used By Missed court date calls report to get  client(s) and quote(s) which have Court status in warrant, missed court, FTA and DLQ.  --Yasir Kamal 5623 03/06/2009 sort by courtdate
         
List of Parameters:   
    @reminderdate : date specify by user  
    @status       : call status  
       0 Pending  
       1 Contacted  
       2 Left Message  
       3 No Answer  
       4 Wrong Telephone Number  
       5 All  
    @showall    : show all record or not .  
    @showreport   : if ShowReport=2 then Missed Court date Calls report.  
  
    @mode  : not in use.
 
******/     
    
-- [USP_HTP_Get_MissedCourtDateCalls] '03/15/2009','5','1','2','0','1' 
Create procedure [dbo].[USP_HTP_Get_MissedCourtDateCalls] 
                                                   
 @reminderdate  datetime,                                              
 @status int=0 ,         
 @showall int=0,    
 @showreport int = 2,  
 @language int,  
 @mode int                                     
                                              
as   
 
DECLARE @lng VARCHAR(20)
    
set @reminderdate = @reminderdate + '23:59:58' 
    
IF @language = 0 
	SET @lng = 'ENGLISH'
ELSE IF @language  = 1 
	SET @lng = 'SPANISH'
	
   
SELECT DISTINCT     
 t.TicketID_PK as ticketid,     
 t.Firstname,     
 t.Lastname,     
 isnull(t.LanguageSpeak,'N/A') as LanguageSpeak,    
 t.BondFlag,    
 t.GeneralComments,      
 tv.FTACallComments as comments,     
 isnull(tv.FTACallStatus,0) as status,     
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''))+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' as contact1,                                              
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''))+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' as contact2,                                              
 convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''))+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' as contact3,                                               
 T.TotalFeeCharged,    
 f.FirmAbbreviation,    
 f.Phone,    
 f.Fax,    
  
 Insurance=(select top 1 vv.shortdesc+'('+isnull(convert(varchar,tvv.ticketviolationdate,101),'')+')'    
   from tblviolations vv inner join tblticketsviolations tvv    
    on vv.violationnumber_pk=tvv.violationnumber_pk     
   where tvv.ticketid_pk=t.ticketid_pk            
   and vv.shortdesc='ins'),    
 Child=(select top 1 vv.shortdesc            
   from tblviolations vv inner join tblticketsviolations tvv     
    on vv.violationnumber_pk=tvv.violationnumber_pk    
   where tvv.ticketid_pk=t.ticketid_pk            
   and vv.shortdesc='chld blt'),    

Convert(datetime,convert(varchar,CourtDateMain,101)) as CourtDateMain      
into #temp4    
FROM  tblTicketsViolations TV       
 INNER JOIN dbo.tblTickets t ON  TV.TicketID_PK = t.TicketID_PK                            
 inner join dbo.tblCourtViolationStatus cvs ON TV.CourtViolationStatusIDMain = cvs.CourtViolationStatusID      
 INNER JOIN dbo.tblCourts c ON  TV.CourtID = c.Courtid               
 LEFT OUTER JOIN dbo.tblFirm f ON t.FirmID = f.FirmID and  isnull(t.firmid,3000) = 3000                 
 LEFT OUTER JOIN dbo.tblContactstype ct2 ON  t.ContactType2 = ct2.ContactType_PK                                 
 LEFT OUTER JOIN dbo.tblContactstype ct1 ON  t.ContactType1 = ct1.ContactType_PK                                 
 LEFT OUTER JOIN dbo.tblContactstype ct3 ON  t.ContactType3 = ct3.ContactType_PK               
 INNER JOIN tblviolations v on v.violationnumber_pk = isnull(tv.violationnumber_pk,0)                                                

 INNER JOIN dbo.tblticketspayment TP ON TV.TicketID_PK =TP.TicketID  
 WHERE isnull(t.firmid,3000) = 3000    
   
and t.CaseTypeId=1   
AND (@language = -1 OR t.LanguageSpeak = @lng )
and ((cvs.courtviolationstatusid in (146,186,105)) and isnull(t.BondFlag,0) <> 1  )
and (@showall = 1 or DATEDIFF([day], TV.CourtDatemain, @reminderdate) = 0 )

IF @showall = 1 AND @status = 0 
BEGIN 
	DELETE FROM #temp4 WHERE datediff(day,CourtDatemain,'12/01/2008') > 0
END

IF @showall = 1 AND @status = 5
BEGIN
	DELETE FROM #temp4 
	WHERE	(
				isnull(Status,0)=0 
			and datediff(day,CourtDatemain,'12/01/2008') > 0
			)  
	or 
			(
				isnull(Status,0)> 0 
			and datediff(day,CourtDatemain,getdate()) < 0 
			)
END

IF @showall = 1 AND @status NOT IN (0,5)
BEGIN
	DELETE FROM #temp4 WHERE datediff(day,CourtDatemain,getdate()) < 0
END

select distinct       
 ticketid,     
 firstname,    
 lastname,     
 languagespeak,    
 bondflag,    
 comments,    
 GeneralComments,  
 ISNULL(status,0) as status,     
                  
 Contact1,     
 Contact2,     
 Contact3,     
T.CourtDateMain,     
 totalfeecharged,    
 FirmAbbreviation,     
 phone,     
 fax,                  
 Insurance,              
 Child,         
 case dbo.tblTicketsFlag.flagid     
 when 5 then 1  else 2  end as flagid,        
  rs.Description as callback        
  into #temp1     
 from #temp4 T     
  left OUTER JOIN    
  dbo.tblTicketsFlag ON T.TicketID = dbo.tblTicketsFlag.TicketID_PK     
  and dbo.tblTicketsFlag.FlagID not in (5)       
 left outer join tblReminderStatus rs on rs.Reminderid_pk = T.status                                                 
                                   
                                                                         
select                                                   
 T.*,  owedamount = t.totalfeecharged -     
  (     
  select isnull(sum(isnull(chargeamount,0)),0) from tblticketspayment p     
  where p.ticketid = t.ticketid            
  and   p.paymenttype not in (99,100)     
  and   p.paymentvoid<>1     
  )     
    
  into #temp2     
  from #temp1 T     
                           
                                  
select     
T.*,dbo.fn_htp_get_TicketsViolationsfta (ticketid,CourtDateMain) as trialdesc ,    
WrongNumberFlag = (select Count(*) from tblticketsflag where ticketid_pk = T.TicketId and FlagID=33)     
,dbo.fn_htp_get_TicketsViolationIdsfta (ticketid,CourtDateMain) as TicketViolationIds   
into #temp5     
from #temp2 T     


if (@status = 5)
	select * from #temp5 order by CourtDateMain,lastname,firstname
ELSE 
	select * from #temp5 where status =  convert(varchar, @status)  order by CourtDateMain,lastname,firstname 
   
                            
drop table #temp1                                                    
drop table #temp4                                                 
drop table #temp2                                                
drop table #temp5       
      


GO
grant execute on [dbo].[USP_HTP_Get_MissedCourtDateCalls] to dbr_webuser
GO  
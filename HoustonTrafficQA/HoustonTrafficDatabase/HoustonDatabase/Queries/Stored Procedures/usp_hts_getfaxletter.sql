USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_hts_getfaxletter]    Script Date: 11/15/2011 19:14:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************
 * Code formatted by SoftTree SQL Assistant � v4.0.34
 * Time: 1/22/2009 12:41:17 PM
 
 Adil 5419 1/26/2009 Just adding business logic, parameters and result set info.
 
 Business Logic : This procedure is used to get email message info inserted by Auto Fax Control.

 List of Parameters:     
 FaxLetterId:		Fax Letter ID

 List of Columns:
 FaxLetterid:				Reprensts Fax Letter ID. (Auto ID).
 Client Name:				Defendant's Name.
 Court Name:				Court Short Name.
 Case Number:				Cause Number/ Ticket Number.
 Ticket ID:					Ticket ID.
 Employee ID:				Sales Rep's ID.
 Employee Name:				Sales Rep's Name.
 Employee Email Address:	Sales Rep's Email Address.
 Fax Number:				Message Fax Number.
 Subject:					Message Subject.
 Body:						Message Body.
 Status:					Message Status Success/Failure.
 Fax Date:					Fax Date.
 Confirm Date:				Fax Confirmation Date.
 Decline Date:				Fax Decline Date.
 System ID:					Houston/Dallas.


 ************************************************************/


CREATE PROCEDURE [dbo].[usp_hts_getfaxletter]
	@FaxLetterId INT
AS
	SELECT DISTINCT
		   fl. FaxLetterid,
		   tt.Lastname+' '+tt.Firstname AS ClientName,
		   tc.CourtName AS CourtName,
			-- Fahad Q. 5419 1/26/2009 Getting cause number/ticket number for Auto Fax.
		   case when ltrim(rtrim(ISNULL(casenumassignedbycourt, ''))) != '' then ltrim(rtrim(ISNULL(casenumassignedbycourt,''))) else ltrim(rtrim(ISNULL(RefCaseNumber,''))) end AS CaseNumber,
	       fl.Ticketid,
	       fl.Employeeid,
		   -- Adil 5419 1/29/2009 Getting valid Sales Rep's name.
		   tu.lastname + ', ' + tu.firstname as EmployeeName,
		   --fl.EmployeeName,
		   -- Adil 5419 1/29/2009 Getting valid Sales Rep's email address.
		   (case when ltrim(rtrim(ISNULL(tu.EmAil, ''))) = '' then 'fahim@lntechnologies.com' else tu.EmAil end) as EmployeeEmail,
	       --fl.EmployeeEmail,
	       fl.FaxNumber,
	       fl.SUBJECT,
	       fl.Body,
	       fl.STATUS,
	       fl.FaxDate,
	       fl.ConfirmNote,
	       fl.DeclineNote,
	       fl.SystemId
	FROM   faxLetter fl
	INNER JOIN 
			TrafficTickets.dbo.tblTickets tt ON tt.TicketID_PK=fl.Ticketid
	INNER JOIN
			TrafficTickets.dbo.tblTicketsViolations ttv ON tt.TicketID_PK=ttv.TicketID_PK
	INNER JOIN
			TrafficTickets.dbo.tblCourts tc ON ttv.courtid=tc.Courtid
	INNER JOIN
			TrafficTickets.dbo.tblUsers tu ON fl.Employeeid = tu.Employeeid
	WHERE  
	(FaxLetterid = @FaxLetterId)
GO
GRANT EXECUTE ON [dbo].[usp_hts_getfaxletter]  TO dbr_webuser
Go
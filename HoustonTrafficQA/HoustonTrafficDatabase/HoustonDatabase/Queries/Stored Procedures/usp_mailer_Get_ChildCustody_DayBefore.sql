/****** 
Created by:		Tahir Ahmed
Create Date:	07/29/2008
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the Harris Civil Child Custody Day Before letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/

--  usp_mailer_Get_Harris_Civil 26, 54, 26, '4/1/08', '5/6/08', 0
alter Procedure [dbo].[usp_mailer_Get_ChildCustody_DayBefore]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int                                             
	)

as                                                              
                                                            
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(max)  ,
	@sqlquery2 nvarchar(max)  ,
	@sqlParam nvarchar(1000)


select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

                                                             
                                                      
select 	@sqlquery ='', @sqlquery2 = ''
                                                              
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                                                      
set 	@sqlquery=@sqlquery+' 
SELECT  distinct 
		convert(datetime , convert(varchar(10),c.nextsettingdate,101)) as mdate,  
		flag1 = isnull(ccp.Flag1,''N'') ,                                                      
		donotmailflag = isnull(ccp.donotmailflag,0) , 
		ccp.childcustodypartyid as recordid 
into #temp       
FROM	civil.dbo.ChildCustody AS c 
INNER JOIN
		civil.dbo.ChildCustodySettingType AS cst 
ON		c.ChildCustodySettingTypeId = cst.ChildCustodySettingTypeId 
INNER JOIN
		civil.dbo.ChildCustodyParties AS ccp 
ON		c.ChildCustodyId = ccp.ChildCustodyId 
INNER JOIN
		civil.dbo.ChildCustodyConnection ccc
ON		ccp.ChildCustodyConnectionID = ccc.ChildCustodyConnectionID 
INNER JOIN
		civil.dbo.tblState 
ON		ccp.StateId = tblState.StateID
WHERE   DATEDIFF( day, c.nextsettingdate, @startlistdate  ) <= 0
and     DATEDIFF( day, c.nextsettingdate, @endlistdate  ) >= 0
AND		ccc.CanBeMailed = 1

and		charindex(''attorney'', ccp.name) = 0  

and		datediff(day, c.settingdate, getdate()) < 0
'

 
-- removing the duplicate addressses
set @sqlquery =@sqlquery+ ' 
select distinct childcustodypartyid as recordid, isnull(address,'''')+ isnull(city,'''')+ convert(varchar,isnull(stateid,45)) + isnull(zipcode,'''') as address
into #addresses
from civil.dbo.childcustodyparties p inner join #temp a on a.recordid = p.childcustodypartyid

select address, min(recordid) recordid into #exists from #addresses group by address

delete from #temp where recordid not in (select recordid from #exists)
'

set @sqlquery =@sqlquery+ ' 
select distinct	n.recordid into #printed from tblletternotes n inner join civil.dbo.ChildCustodyParties ccp on ccp.childcustodypartyid = n.recordid
where 	lettertype =@LetterType   

delete from #temp where recordid in (select recordid from #printed)
'


                                                     
set @sqlquery=@sqlquery + ' 
Select distinct   a.mdate ,a.Flag1, a.donotmailflag, a.recordid 
into #temp1  from #temp a

-- tahir 4624 09/01/2008 exclude client cases
delete from #temp1 where recordid in (
select v.recordid from tblticketsviolations v inner join tbltickets t 
on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
inner join #temp1 a on a.recordid = v.recordid
)
-- end 4624
 '                                  

set @sqlquery2=@sqlquery2 +'
Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate'
	
         
set @sqlquery2 = @sqlquery2 +'                                                        
Select   #temp2.mdate as listdate,
        Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #Temp2 left outer join #temp3   
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
--where Good_Count > 0 
order by #temp2.mdate desc
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5 
'                                                      
                                                      
print @sqlquery  + @sqlquery2                                       
--exec( @sqlquery)
set @sqlquery = @sqlquery + @sqlquery2
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType


go

grant execute on [dbo].[usp_mailer_Get_ChildCustody_DayBefore] to dbr_webuser
go
/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to Insert the information in tbl_htp_documenttracking_batch table and 
				 gets the inserted id and then inserts the (cases) tickets id for that report against that batch.
				 it also returns the inserted.

List of Parameters:	
	@BatchDate : Batch date for this document/report
	@DocumentID_FK : docuemnt/Report id from tbl_HTP_DocumentTracking_Documents
	@TicketIDs : leading comma separated ticket id list
	@EmployeeID : employee id of current logged in user

List of Columns: 	
	DocumentBatchID : currently inserted ID, this field will be returned to the calling method. 
	
******/

Create Procedure dbo.usp_HTP_Insert_DocumentTracking_BacthWithDetail

@BatchDate	datetime,
@DocumentID_FK int,
@TicketIDs	varchar(max),
@EmployeeID	int

as

Declare @BID int

set @BID = 0

Insert into tbl_htp_documenttracking_batch (BatchDate, DocumentID_FK, EmployeeID)
Values (@BatchDate, @DocumentID_FK, @EmployeeID)

Select @BID = Scope_Identity()

Insert into tbl_htp_documenttracking_batchTickets (BatchID_FK, TicketID_FK)
select distinct @BID, MainCat as TicketID from dbo.sap_string_param(@TicketIDs)

select @BID as DocumentBatchID

Go

grant exec on dbo.usp_HTP_Insert_DocumentTracking_BacthWithDetail to dbr_webuser
go
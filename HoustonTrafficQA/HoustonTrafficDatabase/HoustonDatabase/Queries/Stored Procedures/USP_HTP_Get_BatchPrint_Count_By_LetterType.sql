﻿   
/****
Created By : Muhammad Nasir     
    
Business Logic : This Procedure is used to get count of  given letter in the batch.            

ist of Parameters:             
 @LetterTypeID     : Letter Type                                                                
 @emp_id    : Employee Name                 
 @courtid   : Court ID                            
 @isprinted : Used to display Printed records                    
 @fromdate  : Start date     
 @todate    : End Date       
 *****/
--Task 5681 05/20/2009

  
ALTER PROCEDURE dbo.USP_HTP_Get_BatchPrint_Count_By_LetterType  
 @LetterType VARCHAR(100)='Missed Court',  
 @LetterTypeID INT=11,  
 @IsPrinted BIT=0,  
 @StartDate DATETIME,  
 @EndDate DATETIME,  
 @court INT=0,  
 @EMPID VARCHAR(50)='fa'  
AS  
  
  
  
 IF (@IsPrinted=0)  
 BEGIN  
     SELECT @LetterType AS LetterType  
           ,COUNT(DISTINCT thpl.TicketID_FK) AS Counts  
     FROM   tblHTSBatchPrintLetter thpl  
            INNER JOIN tblticketsviolations ttv  
                 ON  thpl.TicketID_FK = ttv.ticketid_pk  
            LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1  
                 ON  thpl.EmpID = tblUsers_1.EmployeeID  
     WHERE  thpl.LetterID_FK = @LetterTypeID   
            AND thpl.IsPrinted = 0  
            AND ISNULL(thpl.deleteflag ,0) = 0  
            AND (  
                    (  
                        (@LetterTypeID IN (11 ,12))  
                        AND ttv.courtviolationstatusidmain   
                           =105  
                    )  
                    OR (@LetterTypeID IN (13 ,15 ,6))  
                )  
            AND ((@court<>0 AND ttv.CourtID=@court) OR (@court=0))  
            AND (tblUsers_1.Abbreviation like @EMPID)  
 END  
 ELSE  
 BEGIN  
     SELECT @LetterType AS LetterType  
           ,COUNT(DISTINCT thpl.TicketID_FK) AS Counts  
     FROM   tblHTSBatchPrintLetter thpl  
            INNER JOIN tblticketsviolations ttv  
                 ON  thpl.TicketID_FK = ttv.ticketid_pk  
            LEFT OUTER JOIN dbo.tblUsers AS tblUsers_1  
                 ON  thpl.EmpID = tblUsers_1.EmployeeID  
     WHERE  thpl.LetterID_FK = @LetterTypeID   
            AND thpl.IsPrinted = 1  
            AND ISNULL(thpl.deleteflag ,0) = 0  
            AND (  
                    (  
                        (@LetterTypeID IN (11 ,12))  
                        AND ttv.courtviolationstatusidmain   
                           =105  
                    )  
                    OR (@LetterTypeID IN (13 ,15 ,6))  
                )  
            --AND thpl.PrintDate BETWEEN @StartDate AND @EndDate  
            AND (DATEDIFF (DAY,thpl.PrintDate, @StartDate)<=0 AND DATEDIFF (DAY,thpl.PrintDate, @EndDate)>=0)
            AND ((@court<>0 AND ttv.CourtID=@court) OR (@court=0))  
            AND (tblUsers_1.Abbreviation like @EMPID)  
 END  
           
           
 GO
   
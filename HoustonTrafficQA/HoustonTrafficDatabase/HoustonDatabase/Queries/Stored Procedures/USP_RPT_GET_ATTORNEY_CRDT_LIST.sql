﻿ SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE TrafficTickets
GO
/**
* CREATED BY: Zeeshan Haider
* CREATED DATE: 07/1/2013
* TASK ID: 11267
* BUSINESS LOGIC: Get all client list which has payment type Attorney Docket in payment table.
* */
-- USP_RPT_GET_ATTORNEY_CRDT_LIST '6/27/2013', '6/27/2013'
ALTER PROCEDURE dbo.USP_RPT_GET_ATTORNEY_CRDT_LIST
(
    @fromDate  AS DATETIME = '1/1/2013',
    @toDate    AS DATETIME = '7/17/2013'
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;    
    SELECT MIN(p.RecDate) AS [SignUpDate],
           t.TicketID_PK,
           t.Firstname,
           t.Lastname,
           ISNULL(p.PaymentReference, 'N/A') AS [BarCard],
           CONVERT(VARCHAR, ISNULL(t.DOB, '1/1/1900'), 101) AS [DOB],
           CASE 
                WHEN ISNULL(t.Address2, '') = '' THEN ISNULL(t.Address1, 'N/A')
                ELSE t.Address1 + ', ' + t.Address2
           END AS [Address],
           ISNULL(t.Contact1, ISNULL(t.Contact2, ISNULL(t.Contact3, 'N/A'))) AS 
           [ContactNumber],
           ISNULL(NULLIF(LTRIM(RTRIM(t.Email)), ''), 'N/A') AS [Email],
           '<a href="../ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&casenumber=' 
           + CONVERT(VARCHAR, t.ticketid_pk) + '" target="_blank">' + ISNULL(NULLIF(LTRIM(RTRIM(v.casenumassignedbycourt)), ''), 'N/A') 
           + '</a>' AS [CaseNumber],
           CONVERT(VARCHAR, v.CourtDateMain, 101) AS [CourtDate],
           REPLACE(
               REPLACE(
                   RIGHT(
                       '0' + LTRIM(RIGHT(CONVERT(VARCHAR, v.CourtDateMain, 100), 7)),
                       7
                   ),
                   'AM',
                   ' AM'
               ),
               'PM',
               ' PM'
           ) AS [CourtTime],
           vs.[Description] AS [CourtStatus],
           c.ShortName AS [CourtLocation]
           INTO #TempAdoc
    FROM   tbltickets t WITH(NOLOCK)
           INNER JOIN tblTicketsPayment p WITH(NOLOCK)
                ON  p.TicketID = t.TicketID_PK
           INNER JOIN tblTicketsViolations v WITH(NOLOCK)
                ON  v.TicketID_PK = t.TicketID_PK
           INNER JOIN tblCourts c WITH(NOLOCK)
                ON  c.Courtid = v.CourtID
           INNER JOIN tblCourtViolationStatus vs
                ON  vs.CourtViolationStatusID = v.CourtViolationStatusIDmain
    WHERE  p.PaymentType = 9
           AND ISNULL(p.PaymentVoid, 0) = 0
           AND ISNULL(t.Activeflag, 0) = 1
    GROUP BY
           t.TicketID_PK,
           t.Firstname,
           t.Lastname,
           p.PaymentReference,
           t.DOB,
           t.Address1,
           t.Address2,
           t.Contact1,
           t.Contact2,
           t.Contact3,
           t.Email,
           v.casenumassignedbycourt,
           v.CourtDateMain,
           c.ShortName,
           vs.[Description]
    
    SELECT CONVERT(VARCHAR, a.signupdate, 101) AS [SignUpDate],
           a.FirstName,
           a.LastName,
           a.BarCard,
           a.DOB,
           a.Address,
           a.ContactNumber,
           a.Email,
           a.CaseNumber,
           a.CourtDate,
           a.CourtTime,
           a.CourtStatus,
           a.CourtLocation
    FROM   #TempAdoc a
    WHERE  a.signupdate BETWEEN @fromDate AND @toDate
           OR  DATEDIFF(DAY, a.signupdate, @fromDate) = 0
           OR  DATEDIFF(DAY, a.signupdate, @toDate) = 0
    ORDER BY
           a.signupdate DESC
    
    DROP TABLE #TempAdoc
END

GO

GRANT EXECUTE ON DBO.[USP_RPT_GET_ATTORNEY_CRDT_LIST] TO dbr_webuser

GO

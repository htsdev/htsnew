
/****** 
Create by:  Yasir Kamal
date : 07/17/2009
Task id : 6109
Business Logic : this procedure is used to Check the violation(s) of the case to which no court is assigned.

List of Parameters:	
	@ticketid : ticketid associcated with the case.

List of Columns: 	
	 returns true, if case have violation(s) to which no court is assigned.
	 returns false, if case does not have violation(s) to which no court is assigned.
	
******/




ALTER Procedure [dbo].[USP_HTP_Check_Violation_NoCourtAssigned] --4497

	@ticketid int 
as

SET NOCOUNT ON;

IF ( SELECT COUNT(*) FROM tblTicketsViolations ttv WITH(NOLOCK) 
     WHERE  ttv.TicketID_PK = @ticketid  and (ISNULL(ttv.courtid,0) = 0 OR ttv.courtid NOT IN ( SELECT courtid FROM tblcourts WITH(NOLOCK))
     )) = 0
select 0
else
select 1



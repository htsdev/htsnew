SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_GET_SHORTCOURTNAMES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_GET_SHORTCOURTNAMES]
GO


CREATE PROCEDURE USP_HTS_GET_SHORTCOURTNAMES  
AS  
SELECT courtid,shortname from tblcourts where courtid <> 0

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


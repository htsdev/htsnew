﻿/****** 
Alter by		: Syed Muhammad Ozair
Created Date	: 01/21/2009
Task ID			: 5434
Business Logic : This procedure is used to resend email from history page of text essages

Parameters : 
				@MsgId
				@empid

******/ 
    
ALTER PROCEDURE [dbo].[USP_HTS_resend_text_message]
	@MsgId INT,
	@empid INT
AS
	--ozair 5434 01/21/2009 remove extra unused fields
	DECLARE @TicketID INT,
	        @Resendby VARCHAR(10)   
	
	SELECT @Resendby = ISNULL(abbreviation, '')
	FROM   tblusers
	WHERE  employeeid = @empid   
	
	SELECT @TicketID = ticketid
	FROM   tblemailhistory
	WHERE  id = @msgid
	
	IF @ticketid IS NOT NULL
	    EXEC (
	             'sp_SendClientSignUpNotficationEmails ' + @TicketID + ',' + @Resendby
	         )    
  
    
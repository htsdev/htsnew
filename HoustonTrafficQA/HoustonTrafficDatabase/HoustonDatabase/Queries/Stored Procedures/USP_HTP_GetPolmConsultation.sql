﻿/************************************************************
 * Created By : Noufil Khan
 * Create Date : 2/20/2010 6:24:21 PM
 * Business Logic : This procedure is used to get POLM Condultation History of a case by Ticket Id
 * Parameter :
 *		@TicketId
 * Column Return :
 *		sno
 *		ID
 *		TicketID
 *		QuickLegalMatter
 *		LegalMatterDescription
 *		Damage
 *		AttorneyID
 *		fullname
 *		POLMCaseID
 ************************************************************/

ALTER PROCEDURE [dbo].[USP_HTP_GetPolmConsultation]
	@TicketId INT
AS
	SELECT ROW_NUMBER() OVER(ORDER BY [ID]) AS sno,
	       h.ID,
	       h.TicketID,
	       h.QuickLegalMatter,
	       h.LegalMatterDescription,
	       h.Damage,
	       h.BodilyDamage,
	       h.AttorneyID,
	       isnull(u.Firstname + ' ' + u.Lastname,'N/A') AS fullname,
	       h.POLMCaseID
	FROM   TrafficTickets.dbo.POLMConsultationHistory h
	       LEFT OUTER JOIN users.dbo.[User] u
	            ON  u.userid = h.AttorneyID
	WHERE  TicketID = @TicketId
GO
 
 GRANT EXECUTE ON [dbo].[USP_HTP_GetPolmConsultation] TO dbr_webuser
 GO
﻿
/*        
Created By     : Fahad Muhammad Qureshi.
Created Date   : 07/24/2009  
TasK		   : 6054        
Business Logic : This procedure is used to fetching out Data if Client Profile having payment type FreiendCredit Attorney Credit Or PartnerFirmCredit.
			 
*/

CREATE PROCEDURE [dbo].[USP_HTP_GetPaymentMethod]
	@TicketID
AS
	INT
	AS    
	SELECT *
	FROM   tblTicketsPayment ttp
	WHERE  ttp.PaymentType IN (11, 104, 9)--Fahad 6054 07/25/2009 (11-FriendCredit,104-PartnerFirmCredit,9-AttorneyCredit)
	       AND ttp.TicketID = @TicketID


GO
GRANT EXEC ON [dbo].[USP_HTP_GetPaymentMethod] TO dbr_webuser
GO   
/******

Business Logic : This procedure is used to Update Print Information About The Batch.    --Yasir Kamal 5489 17/02/2009 Business Logic Updated

List of Parameters: 

@LetterID   = Type of  Letter  i.e Trial Notification Letter,Letter of Rep etc
@ticketidlist 
@TicketIDBatch
@PrintEmpId      


******/


--USP_HTS_UPDATE_LetterBatch 2,'4550,','4550104120,',3991,'3262010 55634 AM','Trial Notification Letter.pdf'
       
ALTER procedure [dbo].[USP_HTS_UPDATE_LetterBatch] --2,'117885,'                                               
                                              
@LetterID int,                                              
@ticketidlist varchar(8000) = '0,',          
@TicketIDBatch varchar(8000),        
@PrintEmpId INT,
--Yasir Kamal 7590 03/29/2010 trial letter modifications
@Datetime VARCHAR(120) = '1/1/1900',
@ReportName VARCHAR(50) = '',
@Docpath AS varchar(120)='' output            
                                           
                                              
as                                            
                              
declare @print as varchar(100),@BatchID INT                                  
SET @ticketidlist = (SELECT dbo.Get_DistinctList(@ticketidlist, ',') DistinctList) --Farrukh 9885 12/09/2011 added Distinct keyword for getting unique records 
              
if (@LetterID=2)
BEGIN
set @print='Trial Letter Printed' 
SELECT @BatchID = MAX(batchid_pk) FROM tblHTSNotes th
SET @BatchID = @BatchID +1  
SELECT @Docpath = @Datetime + '-' + CONVERT(VARCHAR,@BatchID) + '-' + @ReportName                             
END                              
              
if (@LetterID=6)                              
 set @print='Letter of rep Printed'                              
              
if (@LetterID=5)                              
 set @print='Continuance Letter Printed'                              
              
if (@LetterID=3)                              
 set @print='Receipt Printed'        
 
-- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
if (@LetterID=34)
BEGIN
	SET @print='Immigration Letter Printed'
	SELECT @Docpath = hb.DocPath                      
	FROM    dbo.tblHTSBatchPrintLetter hb               
	INNER JOIN                                            
	dbo.tblHTSLetters hl               
	ON  hb.LetterID_FK = hl.LetterID_pk                      
	and  hb.batchid_pk=(                
	select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                                         
	where  tbv.ticketid_fk=hb.ticketid_fk and tbv.LetterID_Fk = @LetterID                
	and deleteflag<>1  
	AND ((@LetterID=2) OR (@LetterID<>2 AND tbv.isprinted <>1))                  
	and TicketID_FK  IN (              
	select * from dbo.Sap_String_Param(@ticketidlist)             
	)                    
	order by batchid_pk desc
	)	
END                             
                                 
              
                              
declare @temp table (              
 rowid int identity(1,1),               
 batchid_pk int,              
 ticketid_fk int,              
 recdate datetime,              
 letterid_fk int,              
 empid int,              
 notes varchar(500),              
 docpath varchar(1000),              
 lettername varchar(100)              
 )              
              
              
declare @reccount int, @idx int, @recordid int, @lnk varchar(500)              
select @reccount = 0, @idx = 1              


    
Begin                         
              
              
              
                                   
 insert into @temp               
 SELECT CASE WHEN @LetterID= 2 THEN @BatchID ELSE hb.BatchID_PK END,               
  hb.TicketID_FK,               
  GETDATE() AS RecDate,               
  hb.LetterID_FK,               
  hb.EmpID,              
  CASE WHEN @LetterID=34 THEN 'Immigration Letter Printed' ELSE 'Batch Print' END AS Notes,                                             
  CASE WHEN @LetterID= 2 THEN @Docpath ELSE hb.DocPath END, 
--7590 end              
  hl.lettername                                           
 FROM    dbo.tblHTSBatchPrintLetter hb               
 INNER JOIN                                            
  dbo.tblHTSLetters hl               
 ON  hb.LetterID_FK = hl.LetterID_pk                      
 and  hb.batchid_pk=(                
   select top 1 tbv.batchID_pk from tblhtsbatchprintletter tbv                                                         
   where  tbv.ticketid_fk=hb.ticketid_fk and tbv.LetterID_Fk = @LetterID                
   and deleteflag<>1  
  --Muhammad Ali 7571 07/28/2010 --> add Conditions for Electrnoic Mail and regular print.....
   --and tbv.isprinted <> 1
   AND ((@LetterID=2) OR (@LetterID<>2 AND tbv.isprinted <>1))                  
   and TicketID_FK  IN (              
    select * from dbo.Sap_String_Param(@ticketidlist)             
    )                    
	order by batchid_pk desc
   )                   
              
 select @recCount = count(rowid) from @temp              
              
 while @idx <= @reccount              
 begin               
  
  -- Haris Ahmed 9885 Add check to view if entry exists in same day for same letter
  DECLARE @RowCount AS INT
                
  -- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
  IF (@LetterID = 34)
  BEGIN
  	  SELECT @RowCount = COUNT(*) FROM @temp t where  t.rowid = @idx 
	  PRINT @RowCount
  		
	  insert into tblhtsnotes(BatchID_PK,TicketID_FK,PrintDate,LetterID_FK,EmpID,Note,DocPath)              
	  select batchid_pk, ticketid_fk, recdate, letterid_fk, @PrintEmpId, notes, docpath              
	  from  @temp t              
	  where  t.rowid = @idx
	  
	  set @RecordID = SCOPE_IDENTITY()--@@identity   
  END
  ELSE
  BEGIN
  	  SELECT @RowCount = COUNT(*) FROM @temp t where  t.rowid = @idx AND NOT EXISTS (SELECT * FROM tblhtsnotes WHERE tblhtsnotes.TicketID_FK = t.ticketid_fk AND DATEDIFF(DAY, tblhtsnotes.PrintDate, t.recdate) = 0 AND tblhtsnotes.LetterID_FK = t.letterid_fk)
      PRINT @RowCount
  	
	  insert into tblhtsnotes(BatchID_PK,TicketID_FK,PrintDate,LetterID_FK,EmpID,Note,DocPath)              
	  select batchid_pk, ticketid_fk, recdate, letterid_fk, @PrintEmpId, notes, docpath              
	  from  @temp t              
	  where  t.rowid = @idx 
	  -- Haris Ahmed 9885 Add check to view if entry exists in same day for same letter
	  AND NOT EXISTS (SELECT * FROM tblhtsnotes WHERE tblhtsnotes.TicketID_FK = t.ticketid_fk AND DATEDIFF(DAY, tblhtsnotes.PrintDate, t.recdate) = 0 AND tblhtsnotes.LetterID_FK = t.letterid_fk)           
	  
	  set @RecordID = SCOPE_IDENTITY()--@@identity  
  END

  IF (@RowCount > 0)           
  BEGIN
  	  -- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
  	  IF (@LetterID = 34)
  	  BEGIN
  	  	  insert into tblticketsnotes(ticketid,subject,employeeid)                              
		  select TicketID_fk , '<a href="javascript:window.open(''frmPreview.aspx?RecordID='+ convert(varchar(20),@RecordID)  +''');void('''');">' + @print + '</a>'   , @PrintEmpId                               
		  FROM  @temp               
		  where  rowid = @idx              
  	  END	
  	  ELSE
  	  BEGIN
		  insert into tblticketsnotes(ticketid,subject,employeeid)                              
		  select TicketID_fk , '<a href="javascript:window.open(''frmPreview.aspx?RecordID='+ convert(varchar(20),@RecordID)  +''');void('''');">' + LetterName + ' Printed from Batch Print</a>'   , @PrintEmpId                               
		  FROM  @temp               
		  where  rowid = @idx              	  		
	  END
	  
	  
	  INSERT INTO tblticketsnotes(ticketid,subject,employeeid)  
	  SELECT tm.TicketID_fk,
	  '<a href="javascript:window.open(''frmPreview.aspx?RecordID='+ convert(varchar(20),@RecordID)  +''');void('''');">'+'Trial Letter' + '</a>'
	 + ' sent via email to '+(SELECT Email  FROM tblTickets WHERE TicketID_PK=tm.TicketID_fk)
	   -- '<a href="javascript:window.open(''frmPreview.aspx?RecordID='+ convert(varchar(20),@RecordID)  +''');void('''');">' + tm.LetterName + ' Sent via email</a>'
	  ,3992
	  FROM @temp tm INNER JOIN tbltickets t ON t.TicketID_PK = tm.ticketid_fk
	  WHERE tm.rowid = @idx
	  and isnull(t.IsEmailedTrialLetter,0) =1
	  AND datediff(day,t.TrialLetterEmailedDate,getdate()) =0
  END
              
  set @idx = @idx + 1              
 end              
       
declare @tickets table (                                      
 ticketid bigint                                        
 )            
        
insert into @tickets                                   
 select * from dbo.Sap_String_Param_bigint(@TicketIDBatch)   
              
-- Haris Ahmed 10381 09/03/2012 Add Immigration Letter
IF (@LetterID = 34)
BEGIN
 update  tblhtsbatchprintletter                          
 set ISPrinted = 1,                                              
  PrintDate =  getdate(),          
 PrintEmpID=@PrintEmpId         
 where   LetterID_Fk = @LetterID                                               
 and  deleteflag<>1 AND ISPrinted = 0                       
 --and  TicketID_FK  IN (select * from dbo.Sap_String_Param( @ticketidlist ))                                       
and cast(ticketid_fk as bigint) IN (select ticketid from @tickets)
END 
ELSE
BEGIN
 update  tblhtsbatchprintletter                          
 set ISPrinted = 1,                                              
  PrintDate =  getdate(),          
 PrintEmpID=@PrintEmpId         
 where   LetterID_Fk = @LetterID                                               
 and  deleteflag<>1                        
 --and  TicketID_FK  IN (select * from dbo.Sap_String_Param( @ticketidlist ))                                       
and cast(convert(varchar(12), ticketid_fk) + convert(varchar(12), BatchID_PK) as bigint) IN (select ticketid from @tickets)
END 	

--Yasir Kamal 5442 02/19/2009 set Isprintflag=1 of a case having no letter flag.     
    
--and tblhtsbatchprintletter.ticketid_fk not in (select distinct ticketid_pk from tblticketsflag where flagid = 7)  --Added by Azwar    

--5442 end
    
 
-- tahir 5489 02/03/2009 set jury status 1 for trial letters printed from batch....
IF @LetterID = 2
BEGIN
	UPDATE t SET t.jurystatus = 1
	FROM tblhtsbatchprintletter bp INNER JOIN tbltickets t ON t.ticketid_pk = bp.ticketid_fk
	WHERE cast(convert(varchar(12), bp.ticketid_fk) + convert(varchar(12), bp.BatchID_PK) as bigint) IN (select ticketid from @tickets)  
END
     
    
 End       
      



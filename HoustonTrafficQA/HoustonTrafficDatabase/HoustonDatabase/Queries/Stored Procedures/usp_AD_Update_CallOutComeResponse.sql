﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to update call outcome response both in auto dialer response. 
				it'll insert a note to client case history if response is confirmed/call transferred
				the note is in link form which will be used to hear recorded conversation.
				
List of Parameters:	
	@CallID		
	@ResponseID 
	@EventTypeID_FK 
	@TicketsViolationIDs
	
*******/

ALTER procedure [dbo].[usp_AD_Update_CallOutComeResponse]

@CallID int,
@ResponseID tinyint,
@EventTypeID_FK tinyint,
@TicketsViolationIDs varchar(max),
@IsPickupNotConfirmed TINYINT --Farrukh 9708 10/13/2011 New Paramet added Pickup-Not Confirmed 

as

update AutoDialerCallOutcome 
set ResponseID_FK=@ResponseID,
	IsPickupNotConfirmed = @IsPickupNotConfirmed --Farrukh 9708 10/13/2011 New Paramet added Pickup-Not Confirmed 

where CallID=@CallID



if @EventTypeID_FK=1
begin
	update tblticketsviolations
	set AutoDialerSetCallStatus=@ResponseID
	where ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs))
	
	--Farrukh 9332 08/26/2011 Set SetCall Status to Contacted
	--Farrukh 10367 09/04/2012 Set SetCall Status to Autodialer-Confirmed
	IF @ResponseID=1
	BEGIN 
		UPDATE tblticketsviolations
		SET setcallstatus = 7
		WHERE ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs))		
	END
end
else if @EventTypeID_FK=2
begin
	update tblticketsviolations
	set AutoDialerReminderCallStatus=@ResponseID
	where ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs))
	
	--Farrukh 9332 08/26/2011 Set ReminderCall Status to Contacted
	--Farrukh 10367 09/04/2012 Set ReminderCall Status to Autodialer-Confirmed
	IF @ResponseID=1
	BEGIN 
		UPDATE tblticketsviolations
		SET ReminderCallStatus = 7
		WHERE ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs))		
	END
end

declare @TicketID int
declare @EventTypeName varchar(50)

select top 1 @TicketID=ticketid_pk from tblticketsviolations where ticketsviolationid in (select * from dbo.sap_string_param(@TicketsViolationIDs))

select @EventTypeName=EventTypeName from autodialereventconfigsettings where eventtypeid=@EventTypeID_FK

declare @RecPathLink varchar(500)
--Farrukh 9332 07/29/2011 Code Redundancy Removed
DECLARE @Subject VARCHAR(MAX)
DECLARE @ContactComments VARCHAR(MAX)
if @ResponseID=1 --Confirmed
begin
	
	select   @RecPathLink='../'+replace(substring(recordingpath,charindex('docstorage',recordingpath),len(recordingpath)- charindex('docstorage',recordingpath)+1),'\','/') from autodialercalloutcome where CallID=@CallID
	
	SET @Subject = '<a href="javascript:window.open('''+@RecPathLink+''');void('''');"''>'+@EventTypeName+' Auto Dialer Court Date Confirmed</a>'
	SET @ContactComments = @EventTypeName+' Auto Dialer Court Date Confirmed'
	
	IF @EventTypeID_FK=1
	BEGIN 
		EXEC [USP_HTS_Insert_Comments] @TicketID,3992,9,@ContactComments --Farrukh 9332 08/29/2011 Insert SetCall Comments
	END
	ELSE IF @EventTypeID_FK=2
	BEGIN 
		EXEC [USP_HTS_Insert_Comments] @TicketID,3992,3,@ContactComments --Farrukh 9332 08/29/2011 Insert ReminderCall Comments
	END
	
	
	--Farrukh 10367 09/04/2012 Inserting history note for AutoDialer-Confirmed Status
	DECLARE @Subject1 VARCHAR(MAX) 
	SET @Subject1 = @EventTypeName + ' Status: Changed from Pending To Autodialer-Confirmed'
	INSERT INTO tblTicketsNotes (TicketID, [Subject], EmployeeID) VALUES (@TicketID, @Subject1, 3992)
	
END
ELSE if @ResponseID=4 --Consultation Request
BEGIN	
	select   @RecPathLink='../'+replace(substring(recordingpath,charindex('docstorage',recordingpath),len(recordingpath)- charindex('docstorage',recordingpath)+1),'\','/') from autodialercalloutcome where CallID=@CallID
	
	SET @Subject = '<a href="javascript:window.open('''+@RecPathLink+''');void('''');"''>'+@EventTypeName+' - Consultation Request</a>'
END
ELSE if @ResponseID=3 --No Pickup/Answer
BEGIN	
	select   @RecPathLink='../'+replace(substring(recordingpath,charindex('docstorage',recordingpath),len(recordingpath)- charindex('docstorage',recordingpath)+1),'\','/') from autodialercalloutcome where CallID=@CallID
	
	SET @Subject = @EventTypeName + ' Auto Dialer - No Answer'
END
--Farrukh 9332 07/29/2011 Notes added for auto dialer responses other than confirmed
ELSE  --Farrukh 9708 10/13/2011 Notes added for auto dialer response Pickup-Not Confirmed
IF @ResponseID = 2 AND @IsPickupNotConfirmed = 1 --Pickup/No Response
BEGIN
    SET @Subject = @EventTypeName + ' Auto Dialer - Pick up - Court Date not confirmed'
END
ELSE 
IF @ResponseID = 2 --Pickup/No Response
BEGIN
    SET @Subject = @EventTypeName + ' Auto Dialer - Pickup/No Response'
END
ELSE 
IF @ResponseID = 5 --Number Busy
BEGIN
    SET @Subject = @EventTypeName + ' Auto Dialer - Number Busy'
END
ELSE -- Farrukh 9538 03/22/2012 Including Voicemail message left Notes
IF @ResponseID = 7 --Message Left
BEGIN
   SELECT  @RecPathLink='../'+replace(substring(recordingpath,charindex('docstorage',recordingpath),len(recordingpath)- charindex('docstorage',recordingpath)+1),'\','/') from autodialercalloutcome where CallID=@CallID
   SET @Subject = '<a href="javascript:window.open('''+@RecPathLink+''');void('''');"''>' + @EventTypeName + ' Auto Dialer - Voicemail Message Left</a>'
END

-- Farrukh 9708 10/19/2011 Excluding No Channel/Line Notes
	IF @ResponseID <> 6 --No Channel/Line
	BEGIN
		INSERT INTO tblTicketsNotes(TicketID,SUBJECT,EmployeeID) VALUES(@TicketID,@Subject,3992)
	END
go

grant exec on dbo.usp_AD_Update_CallOutComeResponse to dbr_webuser
go
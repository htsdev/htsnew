﻿ /*    
* Waqas 5864 06/26/2009  
Created By     : Waqas Javed.    
Created Date   : 06/26/2009      
Business Logic : This procedure retrives Max payment scheduled date of client by Ticket ID.      
               
Parameter:           
 @TicketID   : identifiable key  TicketId 

Output Parameter:
 PaymentDate        
*/    
  
Create procedure [dbo].[USP_HTP_Get_Max_SchedulePaymentDate]    
@TicketID as int                
as                
select ISNULL(MAX(ts.PaymentDate), '') AS PaymentDate  
from tblschedulepayment ts    
INNER JOIN tblTickets tt    
ON ts.TicketID_PK = tt.TicketID_PK    
where ts.ticketid_pk=@TicketID and ts.scheduleflag <>0     


GO

GRANT EXECUTE  ON dbo.USP_HTP_Get_Max_SchedulePaymentDate TO dbr_webuser
  
/*
* Business Logic :  This procedure is used to get sub menu items 
*/

--Waqas 7077 12/14/2009 reuse this procedure
ALTER PROCEDURE [dbo].[USP_TAB_GETSUBMENU_by_MENUID]
AS
	SELECT *
	FROM   tbl_tab_submenu
	WHERE  ACTIVE = 1
	       AND TITLE NOT IN ('iClient (H)', 'iClient (D)', 'iClient (H) 2', 
	                        'iClient (D) 2')
	ORDER BY
	       ORDERING



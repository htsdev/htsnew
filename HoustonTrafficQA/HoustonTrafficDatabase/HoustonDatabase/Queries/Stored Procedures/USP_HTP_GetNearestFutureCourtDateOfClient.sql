/*
* Created By :- Noufil 5884 07/14/2009
* Business Logic : This procedure returns the nearest future court date of client.
* columns returns : 
*					Court date
*					Court Number
*					Court Short Name
*					Ticket Number/Cause Number
*/
--[USP_HTP_GetNearestFutureCourtDateOfClient] 4708
ALTER PROCEDURE [dbo].[USP_HTP_GetNearestFutureCourtDateOfClient]
	@ticketid INT
AS

	SELECT TOP 1 dbo.fn_DateFormat(MIN(ttv.CourtDateMain),24,'/',':',1) AS CourtDateMain ,
	       ttv.CourtNumbermain,
	       tc.ShortName, 
	       CASE WHEN ISNULL(RTRIM(LTRIM(ttv.casenumassignedbycourt)),'')='' THEN ttv.refcasenumber ELSE ttv.casenumassignedbycourt END AS ticketnumber,
	       -- Noufil 6177 08/17/2009 Court short name added with Adress and Zip
	        --Muhammad Ali 8370 10/08/2010 add Court Address with court name via '-' seperator, for HTP Matter Page.
	       CONVERT(VARCHAR,tc.CourtName)+'-'+CONVERT(VARCHAR,tc.Address) AS CourtInfo,
	       --Abbas Shahid Khwaja 9231 05/17/2011 include zip, address2, city,state
	       tc.Zip,
	       tc.Address2 AS CourtAddress2,
	       tc.City,
	       ts.[State]
	       
	FROM   tblTicketsViolations ttv
	       INNER JOIN tblcourts tc
	            ON  ttv.CourtID = tc.Courtid
	       --Abbas Shahid 9231 07/13/2011 added state
	       LEFT OUTER JOIN tblState ts ON ts.StateID = tc.[State]
	WHERE  DATEDIFF(DAY, GETDATE(), ttv.CourtDateMain) > 0
	       AND ttv.TicketID_PK = @ticketid
	       AND ttv.CourtViolationStatusIDmain IN (103, 101, 237, 26)
	GROUP BY
	       ttv.CourtNumbermain,
	       tc.ShortName,ttv.refcasenumber,ttv.casenumassignedbycourt,tc.CourtName,tc.[Address],tc.Zip,ts.[State],tc.City,tc.Address2
	

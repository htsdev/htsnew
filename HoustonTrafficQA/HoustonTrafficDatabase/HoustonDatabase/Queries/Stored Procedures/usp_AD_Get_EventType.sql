﻿/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The procedure is used to get all the event types i.e. call types
				

List of Columns:	
	eventtypeid: 
	eventtypename:
	
*******/

Create procedure dbo.usp_AD_Get_AllEventTypes

as

select eventtypeid, eventtypename 
from autodialereventconfigsettings

go

grant exec on dbo.usp_AD_Get_AllEventTypes to dbr_webuser
go 
 /** noufil 4614 08/15/2008
Business Logic: This report display all the clients whose status are in jury judge or pretrail and their court date day must be saturday.
column return:
				Cause No.
				Ticket No.
				Fname,
				lname,
				Court Date
				Court Time
				Court Location
				Court Status

**/
Create Procedure [dbo].[USP_HTP_Trial_on_Saturday]

as
select distinct	
	isnull(tv.casenumassignedbycourt,'') as [Cause No.],
	tv.refcasenumber as [Ticket No.],	
	t.Firstname as [First Name],
	t.lastname as [Last Name],
	dbo.fn_DateFormat(courtdatemain,27,'/',':',1) as [Court Date],
	dbo.formattime(tv.courtdatemain) as [Court Time],
	c.shortname as [Court Location],	
	vs.shortdescription as [Court Status]
 

from tblticketsviolations tv 
		inner join tbltickets t on tv.ticketid_pk=t.ticketid_pk
		inner join tblcourts c on c.courtid = tv.courtid
		inner join tblCourtViolationStatus vs on vs.courtviolationstatusid=tv.courtviolationstatusidmain

where datename("dw",courtdatemain) ='Saturday' and tv.courtviolationstatusidmain in ( 101,26,103,256,257) and t.activeflag=1


go
grant exec on [dbo].[USP_HTP_Trial_on_Saturday] to dbr_webuser



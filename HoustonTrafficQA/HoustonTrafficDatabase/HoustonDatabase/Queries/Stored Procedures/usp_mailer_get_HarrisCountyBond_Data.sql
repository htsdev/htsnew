USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[usp_mailer_get_HarrisCountyBond_Data]    Script Date: 09/05/2011 22:50:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** 
Created by:		Tahir Ahmed
Business Logic:	The procedure is used by LMS application to get the number of printable letter
				count for the HCJP Warrant letter for the selected date range.
				
List of Parameters:
	@catnum:		Category number of the selected letter type, if printing for 
					all courts of the selected court category
	@LetterType:	Selected letter type in LMS
	@CrtId:			Court id of the selected court category if printing for individual court hosue.
	@startListdate:	Starting list date for Arraignment letter.
	@endListDAte:	Ending list date for Arraignment letter.
	@SearchType:	Flag that identifies if searching by court date or by list date. 0= list date, 1 = court date

List of Columns:	
	Total_Count:	Total number of letters exists for the selected date range.
	Good_Count:		Total number of letters that have valid addresses
	Bad_Count:		Total Number of letter that have undeliverable addresses
	DonotMail_Count:Total number of letter for addresses that had been marked to stop sending letters.
	

******/



--  usp_mailer_get_HarrisCountyBond_Data 2, 21,2, '12/1/06', '12/31/06', 0
ALTER Procedure [dbo].[usp_mailer_get_HarrisCountyBond_Data]
	(
	@catnum		int,
	@LetterType 	int,
	@crtid 		int,                                                 
	@startListdate 	Datetime,
	@endListdate 	DateTime,                                                      
	@SearchType     int                                             
	)

as                                                              
             
-- DECLARING LOCAL VARIABLES FOR DIFFERENT FILTERS...                                               
declare	@officernum varchar(50), 
	@officeropr varchar(50), 
	@tikcetnumberopr varchar(50), 
	@ticket_no varchar(50), 
	@zipcode varchar(50), 
	@zipcodeopr varchar(50), 
	@fineamount money,
	@fineamountOpr varchar(50) ,
	@fineamountRelop varchar(50),
	@singleviolation money, 
	@singlevoilationOpr varchar(50) , 
	@singleviolationrelop varchar(50), 
	@doubleviolation money,
	@doublevoilationOpr varchar(50) , 
	@doubleviolationrelop varchar(50), 
	@count_Letters int,
	@violadesc varchar(500),
	@violaop varchar(50),
	@sqlquery nvarchar(max)  ,
	@sqlquery2 nvarchar(max)  ,
	@sqlParam nvarchar(1000)

-- SETTING PARAMETER LIST AND ORDER FOR DYNAMIC SQL....
select @sqlparam = '@catnum int, @LetterType int, @crtid int, @startListdate Datetime, 	
		    @endListdate DateTime, @SearchType int'

select 	@sqlquery ='', @sqlquery2 = ''
                                                              
-- GETTING DIFFERENT FILTER TYPES AND THIER VALUES IN THE LOCAL VARIABLES
-- IF ASSOCIACTED WITH THE LETTER.......                                                                                                                                  
Select	@officernum=officernum,
	@officeropr=oficernumOpr,
	@tikcetnumberopr=tikcetnumberopr,
	@ticket_no=ticket_no,
	@zipcode=zipcode,
	@zipcodeopr=zipcodeLikeopr,
	@singleviolation =singleviolation,
	@singlevoilationOpr =singlevoilationOpr, 
	@singleviolationrelop =singleviolationrelop, 
	@doubleviolation = doubleviolation,
	@doublevoilationOpr=doubleviolationOpr,
	@doubleviolationrelop=doubleviolationrelop,
	@violadesc=violationdescription,
	@violaop=violationdescriptionOpr , 
	@fineamount=fineamount ,
	@fineamountOpr=fineamountOpr ,
	@fineamountRelop=fineamountRelop  
from 	tblmailer_letters_to_sendfilters  
where 	courtcategorynum=@catnum 
and 	Lettertype=@LetterType 
                                                      
-- MAIN QUERY....
-- LIST DATE WISE NUMBER OF DISTINCT NON-CLIENTS
-- FOR THE SELECTED DATE RANGE ONLY ARRAIGNMENT (PRIMARY STATUS) CASES
-- INSERTING VALUES IN A TEMP TABLE....
set 	@sqlquery=@sqlquery+'                                          
 Select distinct 
	convert(datetime , convert(varchar(10),tva.bonddate,101)) as mdate,       
 flag1 = isnull(ta.Flag1,''N'') ,                                                      
 ta.donotmailflag,  tva.recordid,                                                    
 count(tva.ViolationNumber_PK) as ViolCount,                                                      
 --sum(tva.fineamount)as Total_Fineamount
 isnull(sum(isnull(tva.bondamount,0)),0) as Total_Fineamount
into #temp                                                            
FROM dbo.tblTicketsViolationsArchive TVA                       
INNER JOIN                       
dbo.tblTicketsArchive TA ON TVA.recordid = TA.recordid

-- FOR THE SELECTED DATE RANGE......
WHERE datediff(day, tva.bonddate, @startlistdate)<=0
and datediff(day, tva.bonddate, @endlistdate)>=0

-- ONLY BOND & FTA CASES....
and tva.violationstatusid in (135,186) 

-- EXCLUDE JUVENILE CASES...
and isnull(tva.juvenileflag,0)=0

-- PERSON NOT MOVED IN PAST 48 MONTHS (ACCUZIP)
and isnull(ta.ncoa48flag,0) = 0 

  '

  
-- IF PRINTING FOR ALL ACTIVE COURTS OF THE SELECTED COURT CATEGORY...    
if( @crtid = @CATNUM ) -- Adil 8928 02/17/2011 HCJP 7-1(3019) Excluded from Printing.
	set @sqlquery=@sqlquery+' and ta.courtid In (Select courtid from tblcourts where courtcategorynum = @crtid and inactiveflag =0   and courtid <> 3019)'

-- ELSE PRINTING FOR THE INDIVIDUAL SELECTED COURT OF THE CATEGORY....
ELSE 
	set @sqlquery =@sqlquery +' and ta.courtid = @crtid  and ta.courtid <> 3019 '
                                                        

-- LMS FILTERS SECTION:
-----------------------------------------------------------------------------------------------------------------------------------------------------
-- IF THERE IS ANY FILTER SPECIFIED TO INCLUDE/EXCLUDE ONLY PARTICULAR OFFICER NUMBERS....                                                     
if(@officernum<>'')                                                        
	set @sqlquery =@sqlquery + '  and ta.officerNumber_Fk '  + @officeropr  +'('+ @officernum+')'                                                         

-- IF THERE IS ANY FILTER DEFINED FOR INCLUDE/EXCLUDE VIOLATION DESCRIPTIONS...                                                                                                              
if(@ticket_no<>'')                        
	set @sqlquery=@sqlquery + ' and ('+  dbo.Get_String_Concat_With_op(''+@ticket_no+'' , 'tva.TicketNumber_PK' ,+ ''+ @tikcetnumberopr) +')'                                                              
                                    
-- ZIP CODE FILTER......
-- INCLUDE/EXCLUDE THE SPECIFIED ZIP CODES.....                                    
if(@zipcode<>'')                                                                              
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@zipcode+'' , 'left(ta.zipcode,3)' ,+ ''+ @zipcodeopr) +')'                                   

-- EXCLUDE THE SPECIFIED VIOLATIONS....
if(@violadesc<>'')            
	set @sqlquery=@sqlquery+ ' and ('+ dbo.Get_String_Concat_With_op(''+@violadesc+'' , '  tva.violationdescription  ' ,+ ''+ @violaop) +')'

-- FINE AMOUNT FILTER.....
-- INCLUDE ONLY THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT....
if(@fineamount<>0 and @fineamountOpr<>'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and  tva.bondamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-- EXCLUDE THE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT.....
if(@fineamount<>0 and @fineamountOpr = 'not'  )                                  
	set @sqlquery =@sqlquery+ '
        and not  tva.bondamount'+ convert (varchar(10),@fineamountRelop) +'Convert(Money,'+ convert(varchar(10),@fineamount) +')'                                       

-----------------------------------------------------------------------------------------------------------------------------------------------------

-- ECLUDE THE LETTERS THAT HAVE BEEN ALREADY PRINTED......
set @sqlquery =@sqlquery+ ' 
and ta.recordid not in (                                                                    
	select	n.recordid from tblletternotes n inner join tblticketsarchive t on t.recordid  = n.recordid
	where 	n.lettertype =@LetterType   
	)
               
 group by                                                       
 convert(datetime , convert(varchar(10),tva.bonddate,101)) ,
 ta.Flag1,      
 ta.donotmailflag  ,tva.recordid                                                    
' 

-- GETTING THE RESULT SET IN TEMP TABLE.....                                                                                                            
set @sqlquery=@sqlquery + ' 
Select distinct  ViolCount, Total_Fineamount, mdate ,Flag1, donotmailflag, recordid into #temp1  from #temp                                                          
 '                                  

-- SINGLE VIOLATION FINE AMOUNT FILTER....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE SINGLE VIOLATIONS WITH THE SPECIFIED FINE AMOUNT
if(@singleviolation<>0 and @doubleviolation=0)                                  
	set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                    
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                  
         or violcount>3                                                               
      '                                  
                                  
-- DOUBLE VIOLATION FINE AMOUNT FILTER.....
-- INCLUDE/EXCLUDE THE LETTERS THAT HAVE DOUBLE VIOLATIONS 
-- HAVING TOTAL FINES AS SPECIFIED IN THE FILTER...                                       
if(@doubleviolation<>0 and @singleviolation=0 )                                  
	set @sqlquery =@sqlquery + 'where   '+ @doublevoilationOpr+                                                         
    '(Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                        
    or violcount>3 '                                                      
                                  
if(@doubleviolation<>0 and @singleviolation<>0)                                  
begin                                  
set @sqlquery =@sqlquery+ 'Where  '+ @singlevoilationOpr+                                                        
      '  (Total_FineAmount '+ convert (varchar(10),@singleviolationrelop) +'Convert(Money,'+ convert(varchar(10),@singleviolation)+') and ViolCount=1)                                  
         or   '                                  
set @sqlquery =@sqlquery +  @doublevoilationOpr+                                                         
       '   (Total_FineAmount '+convert (varchar (10),@doubleviolationrelop) +'convert (Money,'+ convert(varchar(10),@doubleviolation) + ')and ViolCount=2)                                                        
      or violcount>3 '                                    
                                  
end                       



-- SUMMARIZING THE TOTAL NUMBER OF LETTERS......
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS....
set @sqlquery2=@sqlquery2 +'

--Yasir Kamal 7119 12/10/2009 send warrant letters to existing clients.
-- tahir 4624 09/01/2008 exclude client cases
--delete from #temp1 where recordid in (
--select v.recordid from tblticketsviolations v inner join tbltickets t 
--on t.ticketid_pk = v.ticketid_pk and t.activeflag = 1 
--inner join #temp1 a on a.recordid = v.recordid
--)
-- end 4624

Select count(distinct recordid) as Total_Count, mdate 
into #temp2 
from #temp1 
group by mdate 
  
-- GETTTING LIST DATE WISE TOTAL NUMBER OF PRINTABLE LETTERS...  
Select count(distinct recordid) as Good_Count, mdate 
into #temp3
from #temp1 
where Flag1 in (''Y'',''D'',''S'')   
and donotmailflag = 0    
group by mdate   
                                                      
-- GETTING LIST DATE WISE TOTAL NUMBER OF NON-PRINTABLE LETTERS
Select count(distinct recordid) as Bad_Count, mdate  
into #temp4  
from #temp1 
where Flag1  not in (''Y'',''D'',''S'') 
and donotmailflag = 0 
group by mdate 
                   
-- GETTING LIST DATE WISE TOTAL NUMBER OF LETTERS THAT MARKED 
-- AS BLOCKED ADDRESSES.....                   
Select count(distinct recordid) as DonotMail_Count, mdate  
into #temp5   
from #temp1 
where donotmailflag=1 
group by mdate

-- tahir ahmed 4022 05/12/2008
-- GETTING ALL THE DATES IN TEMP TABLE THAT LIES BETWEEN THE 
-- SELECTED DATE RANGE.......
create table #letterdates (mdate datetime)
while datediff(day, @startListdate, @endlistdate)>=0
begin
	insert into #letterdates (mdate) select convert(varchar(10),@startlistdate,101)
	set @startlistdate = dateadd(day, 1, @startlistdate)
end

-- OUTPUTTING THE REQURIED INFORMATION........
Select 	l.mdate as listdate,
        isnull(Total_Count,0) as Total_Count,
        isnull(Good_Count,0) Good_Count,  
        isnull(Bad_Count,0)Bad_Count,  
        isnull(DonotMail_Count,0)DonotMail_Count  
from #letterdates l left outer join #Temp2 on #temp2.mdate = l.mdate left outer join #temp3   
on #temp2.mdate=#temp3.mdate  
left outer join #Temp4  
on #temp2.mdate=#Temp4.mdate    
left outer join #Temp5  
on #temp2.mdate=#Temp5.mdate                                                       
order by l.mdate

-- DROPPING THE TEMPORARY TABLES USED ABOVE.....
drop table #temp drop table #temp1 drop table #temp2  drop table #temp3  drop table #temp4  drop table #temp5 
'                                                      
                                                      
--print @sqlquery  + @sqlquery2                                       

-- CONCATENATING THE QUERY ....
set @sqlquery = @sqlquery + @sqlquery2

-- EXECUTING THE DYNAMIC SQL QUERY...
exec sp_executesql @sqlquery , @sqlparam, @catnum, @LetterType, @crtid, @startListdate, @endListdate, @SearchType

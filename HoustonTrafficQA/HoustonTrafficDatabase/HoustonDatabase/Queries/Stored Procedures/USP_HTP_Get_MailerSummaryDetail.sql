﻿
  
   
/******   
 
Business Logic : this procedure is used to get the Mailer summary within the specific date of range.  
   
  
List of Parameters :   
  
 @RecDate     : From Search Date                           
 @RecDateTo   : To Search Date                            
 @EmployeeID  : Employee ID                             
 @PaymentType : Payment Type                            
 @CourtID  : COurt Location      
 @firmId  : Firm ID  
  
List of Columns:   
     
      TotalRev    : Display the total revenue generated with respect to letter type.  
      TotalClients  : Display the total clients hire with respect to letter type.  
      CourtCategoryName  : Display Court Name  
      MailType   : Display Mailer Type  
*****/ 
--Sabir Khan 6047 06/19/2009 saparate this sp from transaction detail sp... 
--USP_HTP_Get_MailerSummaryDetail '07/30/2009','07/30/2009',3991,-1,null
alter procedure [dbo].[USP_HTP_Get_MailerSummaryDetail]                          
                            
@RecDate DateTime,                            
@RecDateTo DateTime,                            
@EmployeeID int,                            
@PaymentType Int,                            
@CourtID int = 0,      
@firmId int = NULL,
@BranchID INT=1 -- Sabir Khan 10920 05/27/2013 Branch Id added.                            
                            
AS      
                      
--ozair 4132 05/27/2008 set the lenght to max from 4000 as dynamic sql is trimming/cutting.
declare @SQL  nvarchar(max),                  
--end ozair 4132
 @Parm nvarchar(200)                  
select @SQL = '', @parm = '@RecDate DateTime, @RecDateTo DateTime, @EmployeeID int, @PaymentType Int, @CourtID int,@firmId int, @BranchID int'                  
  
                  
select @SQL = @SQL + ' SELECT DISTINCT p.ChargeAmount AS ChargeAmount,
      p.TicketID,      
       p.RecDate AS sorttime,
       ''N/A'' AS courtname,
	   t.mailerid , 	   
	   	ISNULL(v.RecordID,0) AS RecordID	   	
into #tempTransDetail               
 FROM dbo.tblCourts c      
 INNER JOIN   dbo.tblTicketsViolations v  ON  c.Courtid = v.CourtID                                 
 INNER JOIN    dbo.tblTickets t  ON  v.TicketID_PK = t.TicketID_PK                   
 and v.ticketsviolationid in(  
 select  Top 1 ticketsviolationid from tblticketsviolations         
    where ticketid_pk  = v.ticketid_pk and (courtid is not null and courtid <> 0)AND (v.coveringFirmId = coalesce(@firmId,v.coveringFirmId))                 
      )                   
 RIGHT OUTER JOIN                  
  dbo.tblPaymenttype pt                   
 RIGHT OUTER JOIN                  
  dbo.tblTicketsPayment p                   
 ON  pt.Paymenttype_PK = p.PaymentType                  
 ON  t.TicketID_PK = p.TicketID                   
 LEFT OUTER JOIN                  
  dbo.tblUsers u                   
 ON  p.EmployeeID = u.EmployeeID left outer join              
 tblletternotes ln on t.MailerID = ln.NoteID left outer join              
 tblletter tl on tl.LetterID_PK = ln.LetterType   left outer join          
 tblticketsarchive ta on ln.RecordID = ta.RecordID                
 where datediff(day, p.recdate, @recdate) <= 0 and datediff(day, p.recdate, @recdateto) >= 0
 and  p.paymentvoid = 0 AND (v.coveringFirmId = coalesce(@firmId  ,v.coveringFirmId))       
'                  
                  
If @EmployeeID <>  0                   
select @SQL = @SQL + '                         
 and p.employeeid = @employeeid                  
 '                  


-- tahir 4225 7/16/2008 
-- fixing bug related to selected court.
If @CourtID = 1                   
select @SQL = @SQL + '                         
 and c.courtcategorynum  =1                
'                 
                  
If @CourtID = 2                   
select @SQL = @SQL + '                         
 and c.courtcategorynum  = 2                
'                
              
If @CourtID = 100                   
select @SQL = @SQL + '                        
 and isnull(c.courtcategorynum,0) not in (1,2,8)
 '                  

If @CourtID > 3000                   
select @SQL = @SQL + '                        
 and c.courtid = @CourtId                  
 '                  

-- end 4225
                
-- IF 'Show All (Inc. Atty/Freind Credit)' IS SELECTED AS PAYMENT TYPE......                          
IF (@PaymentType= -1)                            
select @SQL = @SQL + '                         
    AND p.PaymentType IN (1,2,3,4,5,6,8,7,9,11,104)                            
'      -- Fahad 6054 08/19/2009 Partner Firm Credit Type (104) added            
                            
--else IF (@PaymentType=-200)                            
--select @SQL = @SQL + '                         
--   AND  p.PaymentType IN (1,2,4,5,6,8,7,11)                             
--'                  
                             
-- IF   'Show All (Exc. Atty/Freind Credit)' IS SELECTED AS PAYMENT TYPE....                          
else IF (@PaymentType= 200)                            
select @SQL = @SQL + '                         
   AND  p.PaymentType IN (1,2,3,4,5,6,8,7)                             
'                  
                         
else IF (@PaymentType=300)                            
select @SQL = @SQL + '                         
   And  p.PaymentType IN (5,6)                            
'                  
                             
else IF (@PaymentType=301)                            
select @SQL = @SQL + '                         
   And  p.PaymentType IN (5)                            
'                  
                           
ELSE                            
select @SQL = @SQL + ' And  p.PaymentType = @PaymentType                            
'                  
          
--ozair 4111 05/23/2008 removed date from selection and group by clause          
select @SQL = @SQL + ' AND p.Branchid IN (SELECT branchid FROM fn_get_branchIDs(@branchID))   order by p.recdate 

ALTER TABLE #tempTransDetail ADD MailDate varchar(10) NOT NULL DEFAULT ''1/1/1900'' ,mailtype VARCHAR(500) NOT NULL DEFAULT ''N/A'',
listdate varchar(10),CourtCategoryNum int  


-- update mailer id...
UPDATE a
SET    mailtype = CASE 
                       WHEN ISNULL(t.IsTrafficAlertSignup, 0) = 1 THEN 
                            ''Traffic Alert''
                       ELSE ISNULL(TL.SHORTNAME, ''N/A'') + (
                                CASE ISNULL(ta.NCOA18Flag, 0)
                                     WHEN 1 THEN ''-(18)''
                                     ELSE ''''
                                END
                            )
                  END
FROM   #tempTransDetail a LEFT OUTER JOIN tblletternotes ln ON a.mailerid = ln.NoteId
INNER JOIN tblletter tl ON tl.LetterID_PK  = ln.LetterType
INNER JOIN tbltickets t ON t.TicketID_PK = a.ticketid
LEFT OUTER JOIN tblticketsarchive ta ON ta.RecordID = a.recordid

-- update court category by letter batch...

UPDATE a 
SET a.courtcategorynum = ISNULL(b.courtid,0)
FROM #tempTransDetail a INNER JOIN tbltickets t ON t.TicketID_PK = a.ticketid
LEFT OUTER JOIN tblletternotes n ON n.noteid = t.MailerID
INNER JOIN tblbatchletter b ON b.BatchID = n.BatchId_Fk
WHERE a.courtcategorynum IS NULL

UPDATE a 
SET a.courtcategorynum = c.courtcategorynum
FROM #tempTransDetail a INNER JOIN tblticketsviolations v ON v.TicketID_PK = a.ticketid
LEFT OUTER JOIN tblcourts c ON c.courtid = v.courtid 
WHERE a.courtcategorynum IS null

						
'
     
SELECT @SQL  = @SQL + '  

select	SUM(ChargeAmount) as TotalRev, 
		Count(*) as TotalClients,  

-- tahir 4135 05/29/2008
-- if court category is not available then
-- display court name instead...
		--isnull(tblCourtCategories.CourtCategoryName,''N/A'') as CourtCategoryName, 
		CourtCategoryName = case when isnull(#tempTransDetail.CourtCategoryNum,0) =0  
									then #tempTransDetail.Courtname
								else tblCourtCategories.CourtCategoryName
							end,
-- end 4135....

--ozair 4131 05/27/2008 removed -(18) from letter type only from mailer summary section
--to combine iit with its letter type
  replace(mailtype, ''-(18)'','''') as mailtype,
 isnull(#tempTransDetail.CourtCategoryNum,0) as courtcategorynum
 INTO #TempSummary
FROM #tempTransDetail
left outer join  tblCourtCategories on #tempTransDetail.CourtCategoryNum = tblCourtCategories.CourtCategoryNum  
group by 
	case when isnull(#tempTransDetail.CourtCategoryNum,0) =0  
									then #tempTransDetail.Courtname
								else tblCourtCategories.CourtCategoryName
	end, 
	replace(mailtype, ''-(18)'','''') ,
	isnull(#tempTransDetail.CourtCategoryNum,0)

order by [CourtCategoryName], [MailType]  

alter table #TempSummary add IsGroup bit
update #TempSummary set IsGroup = 0

insert into #TempSummary (Courtcategoryname, courtcategorynum, Totalrev, totalclients, isgroup)
select distinct courtcategoryname, isnull(courtcategorynum,0), sum(totalrev), sum(totalclients), 1
from #TempSummary 
group by courtcategoryname, isnull(courtcategorynum,0)

select distinct courtcategorynum, courtcategoryname, mailtype, totalrev, totalclients, isgroup
from #TempSummary order by courtcategoryname, isgroup  desc

'                  
--end ozair 4111        

--print @SQL                  
exec sp_executesql @SQL, @parm, @RecDate , @RecDateTo , @EmployeeID , @PaymentType , @CourtID,@firmId , @BranchID     
  


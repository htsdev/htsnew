USE TrafficTickets
GO

/*
-- =============================================
Business Logic: This stored procedure is used to get total number of cases set for the selected date and having status in pre trail

Perameter: @crtDate: Selected Court Date

Return Columns: totalcases
-- =============================================
*/
-- USP_HTP_GET_PMCPretrialforSelectedDate @CourtID = 3004,@IsFromAutoFax  =1,@faxID = 9169  
ALTER PROCEDURE [dbo].[USP_HTP_GET_PMCPretrialforSelectedDate]
 @crtDate DATETIME = NULL,
 @CourtID INT,
 @IsFromAutoFax INT = 0 ,
 @faxID int = 0,
 -- Babar Ahmad 8899 09/30/2011 Added parameter for E- delivery service.
 @IsFromEDelivery INT = 0,
 @BatchId_pk INT = 0
AS

-- Babar Ahmad 8899 09/30/2011 Added condition for E-delivery service.
IF @IsFromAutoFax = 0 AND @IsFromEDelivery = 0
BEGIN
	SELECT  COUNT(distinct t.TicketID_PK) AS totalcases 
	FROM tblTicketsViolations tv INNER JOIN tbltickets t ON t.TicketID_PK = tv.TicketID_PK
	WHERE 
		CourtViolationStatusIDmain = 101 
		AND DATEDIFF(DAY,CourtDateMain,@crtDate) = 0
		AND t.Activeflag = 1
		AND tv.CourtID=@CourtID
END
-- Babar Ahmad 8899 09/30/2011 Added condition for E-delivery service.
	ELSE if @IsFromAutoFax  =1 AND @IsFromEDelivery = 0
	BEGIN
		 DECLARE @Maxout INT  
		 DECLARE @LORRepDateCheck DATETIME
		 DECLARE @totalcases INT
		-- Getting Maxout value...
		SET @Maxout = (SELECT v.[Value]  FROM   Configuration.dbo.[Value] v WHERE  v.DivisionID = 1 AND v.ModuleID = 4
	               AND v.SubModuleID = 14 AND v.KeyID = 26)
	               --Sabir Khan 9917 11/18/2011 Faxmaker DB has been removed.
		 SELECT	 distinct @LORRepDateCheck = (CONVERT(DATETIME,(tv.courtdatemain),101)) FROM tblticketsviolations tv inner join faxletter fl on fl.ticketid = tv.ticketid_pk WHERE  tv.Courtid = 3004 and tv.courtviolationstatusIDmain = 101 and fl.faxletterid = @faxID
		 
		 SELECT @totalcases = COUNT(DISTINCT t.TicketID_PK)
			FROM   tblTicketsViolations tv INNER JOIN tbltickets t  ON  t.TicketID_PK = tv.TicketID_PK
			WHERE  CourtViolationStatusIDmain = 101 AND DATEDIFF(DAY, CourtDateMain, @LORRepDateCheck) =0
			AND t.Activeflag = 1 AND tv.CourtID = 3004 
			
		SET @totalcases = @totalcases - @Maxout + 1
		SELECT @totalcases AS totalcases,@LORRepDateCheck AS Courtdate,@CourtID as Courtid,@Maxout as MaxOut	
	END
	-- Babar Ahmad 8899 09/30/2011 Getting total cases count for Edelivery for max out email PMC.
	ELSE IF @IsFromEDelivery = 1
	BEGIN
		
		DECLARE @TicketId INT
	    
	    SELECT @TicketId = TicketID_FK
	    FROM   tblHTSBatchPrintLetter t
	    WHERE  BatchId_pk = @BatchId_pk
	           AND deleteflag <> 1
	           
	    DECLARE @repDate DATETIME
	    SELECT @repDate = tlh.repdate FROM tblhtsbatchprintletter thl INNER JOIN tblLORBatchHistory tlh ON tlh.BatchID_FK = thl.BatchID_PK
	    WHERE tlh.TicketID_PK = @TicketId
		
		DECLARE @MaxoutForEDelivery INT  
		
		-- Babar Ahmad 8997 10/07/2011 Get MaxOut value from configuration to get PMC Pretrial cases.
		SET @MaxoutForEDelivery = (SELECT v.[Value]  FROM   Configuration.dbo.[Value] v WHERE  v.DivisionID = 1 AND v.ModuleID = 4
	               AND v.SubModuleID = 14 AND v.KeyID = 26)
		
		SELECT  COUNT(distinct t.TicketID_PK) AS totalcases,  @repDate AS Courtdate, @CourtID as Courtid,@MaxoutForEDelivery as MaxOut
		FROM tblTicketsViolations tv INNER JOIN tbltickets t ON t.TicketID_PK = tv.TicketID_PK
		-- Babar Ahmad 8997 10/07/2011 Check pretrial case status and client flag. 
		WHERE 
			CourtViolationStatusIDmain = 101 
			AND DATEDIFF(DAY,CourtDateMain,@repDate) = 0
			AND t.Activeflag = 1
			AND tv.CourtID= @CourtID
	END

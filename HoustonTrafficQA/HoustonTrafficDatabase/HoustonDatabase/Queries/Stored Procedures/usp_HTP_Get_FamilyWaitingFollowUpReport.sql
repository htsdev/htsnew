﻿/****** 
Create by		: Abid Ali
Created Date	: 12/23/2008

Business Logic : This procedure simply return the family waiting follow up records

Parameters List:
		@ShowAllRecords		: To show all records
		@ShowAllPastRecords : Show all past or today's records ( default set )
		@FromFollowUpDate	: Show according to From follow up date
		@ToFollowUpDate		: Show according to To follow up date

Column Return : 
				TicketID_PK
				Firstname
				Lastname
				TrafficWaitingFollowUpDate
				TrafficFollowUpDate
				TicketsViolationID
				CourtViolationStatusIDmain
				RefCaseNumber
				CauseNumber
				Courtnumber
				ShortName
				VerifiedCourtStatus
				CourtDate
				VerifiedCourtDate
				Criminalfollowupdate
				FamilyWaitingFollowUp	
******/

ALTER Procedure [dbo].[usp_HTP_Get_FamilyWaitingFollowUpReport] 
(
	@ShowAllRecords BIT = 0,
	@ShowAllPastRecords BIT = 1,
	@FromFollowUpDate DATETIME = '01/01/1900',
	@ToFollowUpDate DATETIME = '01/01/1900',
	@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
)
AS

	SET @FromFollowUpDate = convert(varchar,@FromFollowUpDate,101) + ' 00:00:00'
	SET @ToFollowUpDate = convert(varchar,@ToFollowUpDate,101) + ' 23:59:58'
	
	IF (@ShowAllRecords = 1 OR @ValidationCategory=2) -- Saeed 7791 07/10/2010 display all records if @ShowAllRecords =1 or validation category is 'Report'
	BEGIN

		SELECT	
				tblTickets.TicketID_PK,
				tblTickets.Firstname,
				tblTickets.Lastname,
				tblTickets.TrafficWaitingFollowUpDate,
				dbo.fn_DateFormat(trafficWaitingFollowUpDate, 25, '/', ':', 1) as TrafficFollowUpDate,
				tblTicketsViolations.TicketsViolationID,
				tblTicketsViolations.CourtViolationStatusIDmain,
				tblticketsViolations.RefCaseNumber,
				tblticketsViolations.casenumassignedbycourt as CauseNumber,
				tblticketsViolations.Courtnumber, 
				tblCourts.ShortName,
				tblTickets.DLNumber AS DL,
			-- Abid Ali 12/26/2008 5018 for Cov column
				tblFirm.FirmAbbreviation  AS COV,
				tblCourtViolationStatus.ShortDescription as VerifiedCourtStatus,
				dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) as CourtDate,
				dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) as  VerifiedCourtDate,
				tblTickets.Criminalfollowupdate AS followupdate,
				case	convert(varchar,isnull(tblTickets.trafficWaitingFollowUpDate,' '),101) 
						when '01/01/1900' then ' ' 
						else convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) 
						end as TrafficWaitingFollowUp,
						--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tblticketsViolations.CourtDateMain,101)	AS CrtDate,
			tblticketsViolations.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) AS sortedPastDue
			FROM	
					tblTickets
					INNER JOIN	tblTicketsViolations 
							ON	tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK
					INNER JOIN	tblCourts 
							ON	tblTicketsViolations.CourtID = tblCourts.CourtID
					INNER JOIN	tblCourtViolationStatus 
							ON	tblTicketsViolations.CourtViolationStatusID = tblCourtViolationStatus.CourtViolationStatusID
								AND 
								tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
					-- Abid Ali 5018 for Cov column					
					LEFT OUTER JOIN tblFirm
							ON	Isnull(tblTicketsViolations.CoveringFirmID,3000) = tblFirm.FirmID
					
			WHERE	
					tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
					AND
					tblCourts.CaseTypeId = 4 -- Family Law
					AND
					tblTickets.ActiveFlag = 1 -- Active client
					--sort by follow up date...
					order by  tblTickets.trafficWaitingFollowUpDate, tblTickets.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
					
	END -- end of begin ==> show all
	ELSE IF (@ShowAllPastRecords = 1 OR @ValidationCategory=1) -- Saeed 7791 07/10/2010 display  if @ShowAllPastRecords=1 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	BEGIN

		SELECT	
				tblTickets.TicketID_PK,
				tblTickets.Firstname,
				tblTickets.Lastname,
				tblTickets.TrafficWaitingFollowUpDate,
				dbo.fn_DateFormat(trafficWaitingFollowUpDate, 25, '/', ':', 1) as TrafficFollowUpDate,
				tblTicketsViolations.TicketsViolationID,
				tblTicketsViolations.CourtViolationStatusIDmain,
				tblticketsViolations.RefCaseNumber,
				tblticketsViolations.casenumassignedbycourt as CauseNumber,
				tblticketsViolations.Courtnumber, 
				tblCourts.ShortName,
				tblTickets.DLNumber AS DL,
			-- Abid Ali 12/26/2008 5018 for Cov column
				tblFirm.FirmAbbreviation  AS COV,
				tblCourtViolationStatus.ShortDescription as VerifiedCourtStatus,
				dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) as CourtDate,
				dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) as  VerifiedCourtDate,
				tblTickets.Criminalfollowupdate AS followupdate,
				case	convert(varchar,isnull(tblTickets.trafficWaitingFollowUpDate,' '),101) 
						when '01/01/1900' then ' ' 
						else convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) 
						end as TrafficWaitingFollowUp,
						--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tblticketsViolations.CourtDateMain,101)	AS CrtDate,
			tblticketsViolations.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) AS sortedPastDue
			FROM	
					tblTickets
					INNER JOIN	tblTicketsViolations 
							ON	tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK
					INNER JOIN	tblCourts 
							ON	tblTicketsViolations.CourtID = tblCourts.CourtID
					INNER JOIN	tblCourtViolationStatus 
							ON	tblTicketsViolations.CourtViolationStatusID = tblCourtViolationStatus.CourtViolationStatusID
								AND 
								tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
					-- Abid Ali 5018 for Cov column
					LEFT OUTER JOIN tblFirm
							ON	Isnull(tblTicketsViolations.CoveringFirmID,3000) = tblFirm.FirmID
					
			WHERE	
					tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
					AND
					tblCourts.CaseTypeId = 4 -- Family Law
					AND
					tblTickets.ActiveFlag = 1 -- Active client
					AND 
					DATEDIFF(DAY,ISNULL(tblTickets.TrafficWaitingFollowUpDate, GETDATE()) , GETDATE()) >= 0  -- today or in past
					--sort by follow up date...
					order by  tblTickets.trafficWaitingFollowUpDate, tblTickets.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
	END -- end of begin ==> show past record
	ELSE -- Filter by date
	BEGIN
		SELECT	
				tblTickets.TicketID_PK,
				tblTickets.Firstname,
				tblTickets.Lastname,
				tblTickets.TrafficWaitingFollowUpDate,
				dbo.fn_DateFormat(trafficWaitingFollowUpDate, 25, '/', ':', 1) as TrafficFollowUpDate,
				tblTicketsViolations.TicketsViolationID,
				tblTicketsViolations.CourtViolationStatusIDmain,
				tblticketsViolations.RefCaseNumber,
				tblticketsViolations.casenumassignedbycourt as CauseNumber,
				tblticketsViolations.Courtnumber, 
				tblCourts.ShortName,
				tblTickets.DLNumber AS DL,
			-- Abid Ali 12/26/2008 5018 for Cov column
				tblFirm.FirmAbbreviation  AS COV,
				tblCourtViolationStatus.ShortDescription as VerifiedCourtStatus,
				dbo.fn_DateFormat(tblticketsViolations.Courtdate, 25, '/', ':', 1) as CourtDate,
				dbo.fn_DateFormat(tblticketsViolations.CourtDateMain, 25, '/', ':', 1) as  VerifiedCourtDate,
				tblTickets.Criminalfollowupdate AS followupdate,
				case	convert(varchar,isnull(tblTickets.trafficWaitingFollowUpDate,' '),101) 
						when '01/01/1900' then ' ' 
						else convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) 
						end as TrafficWaitingFollowUp,
						--Muhammad Ali 8031 07/16/2010 --> Add four Columns.. two for sorting and 2 for info
			CONVERT(VARCHAR,tblticketsViolations.CourtDateMain,101)	AS CrtDate,
			tblticketsViolations.CourtDateMain AS sortcourtdate,
			 CASE
			 WHEN CONVERT(INT,DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()))< 0 THEN '  ' 
			 ELSE CONVERT(VARCHAR, DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) )
			END AS 	pastdue	,
			DATEDIFF(DAY,tblticketsViolations.CourtDateMain,GETDATE()) AS sortedPastDue
			FROM	
					tblTickets
					INNER JOIN	tblTicketsViolations 
							ON	tblTickets.TicketID_PK = tblTicketsViolations.TicketID_PK
					INNER JOIN	tblCourts 
							ON	tblTicketsViolations.CourtID = tblCourts.CourtID
					INNER JOIN	tblCourtViolationStatus 
							ON	tblTicketsViolations.CourtViolationStatusID = tblCourtViolationStatus.CourtViolationStatusID
								AND 
								tblTicketsViolations.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID
					-- Abid Ali 5018 for Cov column					
					LEFT OUTER JOIN tblFirm
							ON	Isnull(tblTicketsViolations.CoveringFirmID,3000) = tblFirm.FirmID
					
			WHERE	
					tblTicketsViolations.courtviolationstatusidmain = 104 -- waiting status
					AND
					tblCourts.CaseTypeId = 4 -- Family Law
					AND
					tblTickets.ActiveFlag = 1 -- Active client
					AND 
					( -- filter by date
						convert(varchar,tblTickets.trafficWaitingFollowUpDate,101) BETWEEN @FromFollowUpDate AND @ToFollowUpDate
					) 
					--sort by follow up date...
					order by  tblTickets.trafficWaitingFollowUpDate, tblTickets.TicketID_PK -- Adil Aleem 5857 05/15/2009 Include Ticket ID in order by clause
					
	END -- end of begin ==> filter by date


/****** Object:  StoredProcedure [dbo].[USP_HTP_faxcount]    Script Date: 05/23/2008 16:40:43 ******/

/*  
Created By : Noufil Khan  
Business Logic : This Procedure return the fax letter id for the ticket number
parameter  :
			@ticketid : ticket id from which faxletterid will be selected

*/
-- Noufil 3999 06/19/2008 this Sp returns max fax letter id
--[USP_HTP_faxcount] 8723
CREATE procedure [dbo].[USP_HTP_faxcount]
@ticketid int
as
select faxletterid from faxletter where ticketid=@ticketid

go
GRANT EXEC ON [dbo].[USP_HTP_faxcount] to dbr_webuser
go


 
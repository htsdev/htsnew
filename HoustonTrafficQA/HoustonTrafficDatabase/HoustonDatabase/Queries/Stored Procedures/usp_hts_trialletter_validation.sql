SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_trialletter_validation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_trialletter_validation]
GO


CREATE procedure usp_hts_trialletter_validation  
@TicketID int  
--set  @TicketID = 124421  
as  
begin  
  
declare @Status int  
declare @CourtDate datetime  
declare @CourtID int  
declare @CourtNum int  
declare @ResultFlag int  
    
set @ResultFlag = 0  
    
SELECT   
@ResultFlag = count(*)  
--CourtViolationStatusIDmain, CourtDatemain, courtnumbermain, courtid  
 FROM  dbo.tblTicketsViolations    
 WHERE TicketID_PK= @TicketID   
  and (  
    courtid in (3001,3002,3003) and  
    (       
     (  
      CourtViolationStatusIDmain = 26  
      and (CONVERT(VARCHAR,CourtDatemain,108) NOT IN ('08:00:00','09:00:00','10:30:00') or courtnumbermain NOT IN (1,2,3,6,8,11,12))  
     )  
     or CourtViolationStatusIDmain = 101  
    )  
   )  
  
SELECT @ResultFlag as ShowTrialLetter    
  
end    

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_webscan_getcaseinfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_webscan_getcaseinfo]
GO



CREATE procedure [dbo].[usp_webscan_getcaseinfo]    -- 124744
@ticketID int    
as    
    
Select  top 1    
  t.ticketid_pk,    
  --t.firstname,    
  --t.middlename,    
  --t.lastname,    
  tv.casenumassignedbycourt,    
  --c.shortname,    
  --tcv.shortdescription,    
  --v.description,  
  tv.refcasenumber     
    
from tbltickets t inner join     
 tblticketsviolations tv on     
 t.ticketid_pk=tv.ticketid_pk 
where     
 t.ticketid_pk=@ticketID  
  

		



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


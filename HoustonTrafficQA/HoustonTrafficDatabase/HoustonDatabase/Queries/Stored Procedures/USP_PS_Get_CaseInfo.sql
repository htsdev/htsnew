


-- USP_HTS_GET_TrialNotification 13446
ALTER procedure [dbo].[USP_PS_Get_CaseInfo]

@TicketID	int

as

declare @lastemaildate datetime 
select @lastemaildate=max(emaildate) from tblEmailTrialNotification where ticketid_pk=@TicketID

SELECT 	
	t.TicketID_PK As TicketID,
	t.Lastname,t.Firstname,
	isnull(t.LastName,'') + ', ' + isnull(t.FirstName,'') As ClientName,
	t.EmployeeID,
	t.Email AS EMailAddress, 
	t.DLNumber, --Farrukh 10148 04/24/2012 Added New fields for DLNumber,DOB,Zip.
	t.DOB,  
	t.Zip	
  
FROM    tblTickets t   

INNER JOIN  
 tblTicketsViolations tv   
ON  t.TicketID_PK = tv.TicketID_PK 

and t.ticketid_pk =@ticketid

and datediff (day,getdate(),isnull(tv.CourtDateMain,'01/01/1900')) > 0 

and (

    select count(*) 
	from tblTicketsViolationLog tvLog
	inner join tblTicketsViolations tv on tvLog.TicketViolationID = tv.TicketsViolationID
	inner join        tblEmailTrialNotification ON tv.TicketID_PK = tblEmailTrialNotification.TicketID_PK
	
	Where ( (NewVerifiedStatus != OldVerifiedStatus)
		OR (NewVerifiedDate != OldVerifiedDate)
		OR (NewVerifiedRoom != OldVerifiedRoom)
		)
	AND tv.TicketId_Pk = @TicketID
	AND @lastemaildate  < tvLog.RecDate
	) = 0

order by TicketID




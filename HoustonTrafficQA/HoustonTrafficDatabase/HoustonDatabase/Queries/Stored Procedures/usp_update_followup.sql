    
            
alter PROCEDURE [dbo].[usp_update_followup]     
---- This Proceudure is used to update folloeupdate from Report & also update general comments     
---- First it will check weather Followup date exist or not    
---- If followup date isnt exist insert new followup date ( Formula = todays date + 7 )    
---- If Followup date exist update followupdate     
---- Then check weather general comments exist or not if exist call usp_hts_insert_comments proceudure for insertion    
      
 @TicketID_PK int,        
 @Empid int,        
 @comments as varchar(max),        -- tahir 5008 10/21/2008 increased the length of comments...
 @Followup datetime=NULL             
        
AS            
            
BEGIN            
            
declare @L_Exist int             
set @L_Exist =0            
            
select  @L_Exist = Count(TicketID_PK) from tblTicketsExtensions            
where TicketID_PK = @TicketID_PK            
            
if(@L_Exist=0)            
 begin            
 insert into tblTicketsExtensions(TicketID_PK ,NOSFollowUpDate)            
 values(@TicketID_PK ,getdate() + 7)            
            
print  'new Row'            
 end            
else            
 begin            
  update tblTicketsExtensions            
  set NOSFollowUpDate =  @Followup          
  where TicketID_PK = @TicketID_PK            
            
print  'old Row'            
 end            
            
END         
        
select NOSFollowUpDate from tblTicketsExtensions  where TicketID_PK = @TicketID_PK      
      
      
if (len(@comments) > 0)      
begin      
exec USP_HTS_Insert_Comments  @TicketID_PK,@Empid ,1,@comments       
end      
      
    
    
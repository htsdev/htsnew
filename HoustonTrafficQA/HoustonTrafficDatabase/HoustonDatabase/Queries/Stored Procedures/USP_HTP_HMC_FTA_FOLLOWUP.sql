﻿        
/**********      
      
Created By : Waqas Javed 5697 03/26/2009 Add Comments In Procedure      
      
Business Logic : This Report display client cases with following logics    
*Verified status should be A/W or Arraignment [and] associated FTA in the non client database is not Closed or disposed

      
List Of Parameters :       
      @Validation : if 1 then follow up date will be checked for validation email
List Of Columns :    
      
FTACauseNumber
LastName
FirstName
StatusID
CourtDate
CourtRoom
TicketNumber
DOB
XRef
Status
TicketID_PK
CourtID
FTAIssuedDate
CourtTime
HMCFTAFollowUpDate
     
**********/      
      
ALTER PROCEDURE [dbo].[USP_HTP_HMC_FTA_FOLLOWUP]      
  
@Validation int =0,
@ValidationCategory int=0 -- Saeed 7791 07/10/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report     
          
AS 
SET NOCOUNT ON;               
--Ozair 5756 04/07/2009 Procedure Optimized
-- tahir 5898 05/11/2009 changed the business logic and optmized the code...
SELECT *
FROM   (
           SELECT DISTINCT
                  tt.TicketID_PK,
                  ttva2.CauseNumber AS FTACauseNumber,
                  ttv.RefCaseNumber AS TicketNumber,
                  tt.Firstname,
                  tt.Lastname,
                  CONVERT(VARCHAR, tt.DOB, 101) AS DOB,
                  tcvs.ShortDescription AS STATUS,
                  CONVERT(VARCHAR, ttv.CourtDateMain, 101) AS CourtDate,
                  dbo.Fn_FormateTime(ttv.CourtDateMain) AS CourtTime,
                  ttv.CourtNumbermain AS CourtRoom,
                  tt.Midnum AS XRef,
                  CONVERT(VARCHAR, ttva2.FTAIssueDate, 101) AS FTAIssuedDate,
                  ttv.CourtID AS CourtID,
                  CASE CONVERT(VARCHAR, ISNULL(tt.HMCFTAFollowUpDate, ' '), 101)
                       WHEN '01/01/1900' THEN ' '
                       ELSE CONVERT(VARCHAR, tt.HMCFTAFollowUpDate, 101)
                  END AS HMCFTAFollowUpDate,
                  --Waqas 5756 04/07/2009 For Sorting
                  tt.HMCFTAFollowUpDate as HMCFTAFollowUpDateSort,
                  ttv.CourtDateMain AS CourtDateSort,
                  ttva2.FTAIssueDate AS FTAIssuedDateSort,
                  tt.DOB AS DOBSort
           FROM   tblTickets tt WITH(NOLOCK)
                  INNER JOIN tblTicketsViolations ttv WITH(NOLOCK)
                       ON  tt.TicketID_PK = ttv.TicketID_PK
                  INNER JOIN tblTicketsViolationsArchive ttva WITH(NOLOCK)
                       ON  ttv.RecordID = ttva.RecordID
                  INNER JOIN tblTicketsViolationsArchive ttva2 WITH(NOLOCK)
                       ON  ttva.FTALinkId = ttva2.FTALinkId
                  INNER JOIN tblTicketsArchive tta WITH(NOLOCK)
                       ON  tta.RecordID = ttva.RecordID
                  INNER JOIN tblTicketsArchive tta2 WITH(NOLOCK)
                       ON  tta2.RecordID = ttva2.RecordID
                  INNER JOIN tblCourtViolationStatus tcvs WITH(NOLOCK)
                       ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
                  INNER JOIN tblCourtViolationStatus tcvs2 WITH(NOLOCK)
                       ON  ttva2.violationstatusid = tcvs2.CourtViolationStatusID
				  inner join tblticketsviolationlog l WITH(NOLOCK)
					   on l.ticketviolationid = ttv.ticketsviolationid
				  inner join tblcourts c WITH(NOLOCK)
					   on c.courtid = ttv.courtid

           WHERE  
				  tcvs2.CategoryID <> 50 -- fta violation is not disposed in non-clients...
                  AND (
                  	(@Validation=0 OR @ValidationCategory=2)   -- Saeed 7791 07/09/2010 display all records if @validation=0 or validation category is 'Report'
                  	OR  
                  	((@Validation=1 OR @ValidationCategory=1) and datediff(day,getdate(), isnull(tt.HMCFTAFollowUpDate,'')) <= 0)	-- Saeed 7791 07/10/2010 display  if call from validation sp or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
					)  
                  AND ISNULL(tt.IsHMCFTARemoved, 0) = 0
                  
            	  AND l.newverifiedstatus = 201 -- case was ever set for A/W
          		  AND l.newverifiedstatus != oldverifiedstatus
				  and datediff(day, l.recdate, ttva2.ftaissuedate ) >= 0 -- FTA issued after A/W

                  AND ttva.RecordID <> ttva2.RecordID -- join clause..
                  AND CHARINDEX('FTA', ttva2.CauseNumber) > 0 -- join clause...
                  AND ISNULL(ttv.RecordID, 0) > 0 -- join clause..
                  and tcvs.categoryid in (2,3,4,5) 
				  AND tt.Activeflag = 1 --for active cases
				  and c.courtcategorynum =1 -- for HMC courts

			union

           SELECT DISTINCT
                  tt.TicketID_PK,
                  ttva2.CauseNumber AS FTACauseNumber,
                  ttv.RefCaseNumber AS TicketNumber,
                  tt.Firstname,
                  tt.Lastname,
                  CONVERT(VARCHAR, tt.DOB, 101) AS DOB,
                  tcvs.ShortDescription AS STATUS,
                  CONVERT(VARCHAR, ttv.CourtDateMain, 101) AS CourtDate,
                  dbo.Fn_FormateTime(ttv.CourtDateMain) AS CourtTime,
                  ttv.CourtNumbermain AS CourtRoom,
                  tt.Midnum AS XRef,
                  CONVERT(VARCHAR, ttva2.FTAIssueDate, 101) AS FTAIssuedDate,
                  ttv.CourtID AS CourtID,
                  CASE CONVERT(VARCHAR, ISNULL(tt.HMCFTAFollowUpDate, ' '), 101)
                       WHEN '01/01/1900' THEN ' '
                       ELSE CONVERT(VARCHAR, tt.HMCFTAFollowUpDate, 101)
                  END AS HMCFTAFollowUpDate,
                  --Waqas 5756 04/07/2009 For Sorting
                  tt.HMCFTAFollowUpDate as HMCFTAFollowUpDateSort,
                  ttv.CourtDateMain AS CourtDateSort,
                  ttva2.FTAIssueDate AS FTAIssuedDateSort,
                  tt.DOB AS DOBSort
           FROM   tblTickets tt WITH(NOLOCK)
                  INNER JOIN tblTicketsViolations ttv WITH(NOLOCK)
                       ON  tt.TicketID_PK = ttv.TicketID_PK
                  INNER JOIN tblTicketsViolationsArchive ttva WITH(NOLOCK)
                       ON  ttv.RecordID = ttva.RecordID
                  INNER JOIN tblTicketsViolationsArchive ttva2 WITH(NOLOCK)
                       ON  ttva.FTALinkId = ttva2.FTALinkId
                  INNER JOIN tblTicketsArchive tta WITH(NOLOCK)
                       ON  tta.RecordID = ttva.RecordID
                  INNER JOIN tblTicketsArchive tta2 WITH(NOLOCK)
                       ON  tta2.RecordID = ttva2.RecordID
                  INNER JOIN tblCourtViolationStatus tcvs WITH(NOLOCK)
                       ON  ttv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
                  INNER JOIN tblCourtViolationStatus tcvs2 WITH(NOLOCK)
                       ON  ttva2.violationstatusid = tcvs2.CourtViolationStatusID
				  inner join tblticketsviolationlog l WITH(NOLOCK)
					   on l.ticketviolationid = ttv.ticketsviolationid
				  inner join tblcourts c WITH(NOLOCK)
					   on c.courtid = ttv.courtid
           WHERE  
				  tcvs2.CategoryID <> 50 -- fta violation is not disposed in non-clients...
                  AND ((@Validation=0) or (@Validation=1 and datediff(day,getdate(), isnull(tt.HMCFTAFollowUpDate,'')) <= 0))  
                  AND ISNULL(tt.IsHMCFTARemoved, 0) = 0
                  
            	  AND l.newverifiedstatus = 201 -- case was ever set for A/W
          		  AND l.newverifiedstatus != oldverifiedstatus
				  and datediff(day, l.recdate, ttva2.ftaissuedate ) >= 0 -- FTA issued after A/W

                  AND ttva.RecordID <> ttva2.RecordID -- join clause..
                  AND CHARINDEX('FTA', ttva2.CauseNumber) > 0 -- join clause...
                  AND ISNULL(ttv.RecordID, 0) > 0 -- join clause..
                  and ttv.courtviolationstatusidmain = 201  -- only open cases and A/W cases
				  AND tt.Activeflag = 1 --for active cases
				  and c.courtcategorynum =1 -- for HMC courts

       ) AS t
ORDER BY
--Waqas 5756 04/07/2009 For Sorting
       HMCFTAFollowUpDateSort    



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_ESignature_Get_TrialLetter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_ESignature_Get_TrialLetter]
GO



--sp_helptext 'USP_HTS_Trialletter'
          
CREATE procedure [dbo].[USP_HTS_ESignature_Get_TrialLetter] --[USP_HTS_ESignature_Get_TrialLetter] 124546, 3991, '995185845767931'
 (          
 @TicketIDList  int,          
 @empid int,
 @SessionID varchar(200)
 )          
as          
SELECT dbo.tblTickets.TicketID_PK,       
 dbo.tblTickets.MiddleName,       
 dbo.tblTicketsViolations.RefCaseNumber AS TicketNumber_PK,       
 dbo.tblTickets.Firstname,       
 dbo.tblTickets.Lastname,       
 dbo.tblTickets.Address1,       
 dbo.tblTickets.Address2,       
 dbo.tblTickets.City AS Ccity,       
 dbo.tblState.State AS cState,       
 dbo.tblTickets.Zip AS Czip,       
 dbo.tblTicketsViolations.CourtDate,       
 dbo.tblTicketsViolations.CourtNumber,       
 dbo.tblCourts.Address,       
 dbo.tblCourts.City,       
 dbo.tblCourts.Zip,       
 tblState_1.State,       
 dbo.tblTickets.Datetypeflag,       
 dbo.tblTickets.Activeflag,       
 isnull( dbo.tblTickets.LanguageSpeak, 'ENGLISH') AS Languagespeak,       
 dbo.tblTicketsViolations.SequenceNumber,       
 dbo.tblCourtViolationStatus.CategoryID,       
 dbo.tblTicketsViolations.TicketViolationDate,       
 dbo.tblViolations.Description,       
 dbo.tblDateType.Description AS CaseStatus ,  
 dbo.tblTicketsViolations.courtid as courtid,
(Select top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where ImageTrace = 'ImageStream_LetterTrial_Attorney1' and TicketID = @TicketIDList and SessionID = @SessionID) as AttorneySign1
FROM    dbo.tblTickets       
INNER JOIN      
 dbo.tblTicketsViolations       
ON  dbo.tblTickets.TicketID_PK = dbo.tblTicketsViolations.TicketID_PK       
INNER JOIN      
 dbo.tblCourts       
--ON  dbo.tblTickets.CurrentCourtloc = dbo.tblCourts.Courtid       
ON  dbo.tblTicketsviolations.courtid = dbo.tblCourts.Courtid       
INNER JOIN      
 dbo.tblState       
ON  dbo.tblTickets.Stateid_FK = dbo.tblState.StateID       
INNER JOIN      
 dbo.tblState tblState_1       
ON  dbo.tblCourts.State = tblState_1.StateID       
INNER JOIN      
 dbo.tblCourtViolationStatus       
ON  dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID       
INNER JOIN      
 dbo.tblViolations       
ON  dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK       
INNER JOIN      
 dbo.tblDateType       
ON  dbo.tblCourtViolationStatus.CategoryID = dbo.tblDateType.TypeID      
WHERE   (dbo.tblTickets.Activeflag = 1)       
AND  (DATEDIFF(day, dbo.tblTicketsViolations.CourtDate, GETDATE()) <= 0)       
AND     (dbo.tblCourtViolationStatus.CategoryID IN (2, 3, 4, 5))       
--AND  (NOT (dbo.tblTicketsViolations.RefCaseNumber LIKE 'ID%'))      
AND     (dbo.tblTickets.TicketID_PK = @ticketidlist)      



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


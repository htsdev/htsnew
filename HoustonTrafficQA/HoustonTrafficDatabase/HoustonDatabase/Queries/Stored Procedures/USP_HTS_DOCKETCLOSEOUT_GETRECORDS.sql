SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DOCKETCLOSEOUT_GETRECORDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DOCKETCLOSEOUT_GETRECORDS]
GO


CREATE PROCEDURE USP_HTS_DOCKETCLOSEOUT_GETRECORDS  --'1/15/2007'                
@CourtDate datetime                  
AS                  
         
SELECT                 
 s.TicketID_PK,                   
   --Convert(nvarchar,s.ClientCount) +':'+ s.LastName as Lastname,                   
convert(nvarchar,dbo.fn_HTS_Get_Client_Count(s.FirstName,s.LastName,s.Address1,s.MidNum)) +':'+ s.LastName as Lastname,                   
   s.FirstName +',' + s.MiddleName as FirstName,                   
   s.BondFlag,                   
   s.CourtDate as CourtDateMain,                                           
   Convert(int,s.CourtNumber) as CourtNumber,                   
   s.ShortDescription,                   
   s.CourtName,                  
   s.Address1 as Address,                   
   s.TrialComments,                   
   s.Address1,                                           
   s.Midnum,                   
   s.CourtName,                  
   s.TicketsViolationID,                  
   --CourtName + '-' + isnull(Address1,' ') +'-' +CONVERT(varchar, CourtDate, 101) as CourtNameComplete,--convert(nvarchar,tblTicketsViolations.CourtDateMain)                  
   s.CourtName + '-'+isnull((SELECT t.Address from tblcourts t where t.CourtID = s.CourtID),' ') +'-' +CONVERT(varchar, CourtDate, 101) as CourtNameComplete,            
   isnull(s.LastUpdate,' ') as LastUpdate,              
   s.RecordID,          
   s.CourtID,          
   (SELECT t.Address from tblcourts t where t.CourtID = s.CourtID) as CourtAddress,          
   TicketID_PK          
 from tbl_HTS_DocketCloseOut_Snapshot  s           
                
where DateDiff(day,CourtDate,@CourtDate)=0 and ShortDescription  in ('ARR','PRE','JUR','JUD')                 
order by CourtID, CourtNameComplete, CourtNumber, CourtDateMain, Lastname        
        
SELECT                 
 s.TicketID_PK,                   
   --Convert(nvarchar,s.ClientCount) +':'+ s.LastName as Lastname,                   
convert(nvarchar,dbo.fn_HTS_Get_Client_Count(s.FirstName,s.LastName,s.Address1,s.MidNum)) +':'+ s.LastName as Lastname,                   
   s.FirstName +',' + s.MiddleName as FirstName,                   
   s.BondFlag,                   
   s.CourtDate as CourtDateMain,                                           
   Convert(int,s.CourtNumber) as CourtNumber,                   
   s.ShortDescription,                   
   s.CourtName,                  
   s.Address1 as Address,                   
   s.TrialComments,                   
   s.Address1,                                           
   s.Midnum,                   
   s.CourtName,                  
   s.TicketsViolationID,                  
   --CourtName + '-' + isnull(Address1,' ') +'-' +CONVERT(varchar, CourtDate, 101) as CourtNameComplete,--convert(nvarchar,tblTicketsViolations.CourtDateMain)                  
   s.CourtName + '-'+isnull((SELECT t.Address from tblcourts t where t.CourtID = s.CourtID),' ') +'-' +CONVERT(varchar, CourtDate, 101) as CourtNameComplete,            
   isnull(s.LastUpdate,' ') as LastUpdate,              
   s.RecordID,          
   s.CourtID,          
   (SELECT t.Address from tblcourts t where t.CourtID = s.CourtID) as CourtAddress,          
   TicketID_PK          
 from tbl_HTS_DocketCloseOut_Snapshot  s           
                
where DateDiff(day,CourtDate,@CourtDate)=0 and ShortDescription  not in ('ARR','PRE','JUR','JUD')                 
order by CourtID, CourtNameComplete, CourtNumber, CourtDateMain, Lastname

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


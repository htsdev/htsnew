
 /******************    
Alter BY : Hafiz Khalid    
      
BUSINESS LOGIC : This procedure is used to save update violation info of the case      
     If Violaton Have Not Missed Court Status Than Delete Missed Court Letter From Batch If Exist    
     If Criminal Violation status changed to waiting than set Criminal Followup date.    
LIST OF INPUT PARAMETERS :    
    
  @ArrestDate : Arrest Date Associated With The Violation.     
  @MissedCourtType : Set Missed Court Type     
*******************/      
      
--Zeeshan Ahmed 3825 04/23/2008    
-- Add Covering Firm Update Logic in the procedure      
        
ALTER PROCEDURE [dbo].[USP_HTS_Update_ViolationUpdateInfo]
	@TicketsViolationID INT, 
	 --Fahad 5299 02/10/2009 Comment @TicketsViolationIDs
	 --@TicketsViolationIDs varchar(200),
	@RefCaseNumber VARCHAR(20),
	@CauseNumber VARCHAR(20),
	@ViolationId INT,--Fahad 10030 02/07/2012 Field Type changed from SmallInt to Int
	@FineAmount MONEY,
	@BondAmount MONEY,
	@BondFlag TINYINT,
	@PlanId INT,
	@VerifiedStatusId INT,
	@VerifiedCourtDate DATETIME,
	@VerifiedCourtNumber VARCHAR(3),
	@CourtId INT,
	@UpdateMain BIT,
	@EmployeeId INT, -- added by ozair
	@OscarCourtDetail VARCHAR(500), -- added by asghar for oscar court
	@ArrignmentWaitingdate BIT,
	@BondWaitingdate BIT,
	@ChargeLevel INT, -- Added By Zeeshan For Criminal Courts  on 26 Sep 2007
	@CDI VARCHAR(3), -- Added By Zeeshan For Harris County Criminal Courts  on 9 Oct 2007
	@ArrestDate DATETIME,
	@MissedCourtType INT = -1 ,
	@UpdateAll BIT --Fahad 5299 02/10/2009
AS
SET NOCOUNT ON;
	DECLARE @TicketId INT,
	        @Error INT,
	        @CaseType INT                                                  
	
	SELECT @error = 0                                   
	IF @planid = 0
	    SET @planid = 45 
	
	
	SELECT @Ticketid = t.ticketid_pk,
	       @CaseType = t.CaseTypeId
	FROM   tblticketsviolations v WITH(NOLOCK)
	       INNER JOIN tbltickets t WITH(NOLOCK)
	            ON  t.ticketid_pk = v.ticketid_pk
	            AND v.ticketsviolationid = @TicketsViolationID 
	
	-- tahir 8311 09/21/2010 get all ticket violation Ids in temp table
	DECLARE @TicketViolations TABLE (TicketsViolationId INT, TicketId_pk int) 
	
	INSERT INTO @TicketViolations (TicketsViolationId, TicketId_pk)
	SELECT DISTINCT  v.TicketsViolationID, t.TicketID_PK
	FROM tblticketsviolations v WITH(NOLOCK) INNER JOIN tbltickets t WITH(NOLOCK) ON t.TicketID_PK = v.TicketID_PK
	WHERE t.TicketID_PK = @TicketId
	

	
	
	-- IF VERIFIED STATUS  = 'BOND' THEN SET BOND FLAG TO TRUE...                                
	IF @VerifiedStatusId = 135
	    SET @BondFlag = 1 
	
		-- if Bond = yes then update "0" in bondflag field , update "0" all underlinebondflags in tblticketsviolation table                         
    UPDATE tbltickets 
    SET    bondflag = @BondFlag,
           employeeidupdate = @EmployeeId
    WHERE  ticketid_pk = @Ticketid
    AND bondflag <> @BondFlag                        
--   
	-- tahir 8311 09/21/10 fixed dead lock issues. update by matching the primary of table instead of ticketid_pk
	UPDATE v  
	SET v.UnderlyingBondFlag = @BondFlag, 
	v.VEmployeeID = @EmployeeId 
	FROM tblticketsviolations v 
	INNER JOIN @TicketViolations a ON a.TicketsViolationId = v.ticketsviolationid
	WHERE v.UnderlyingBondFlag <> @BondFlag
	
	
	-- Noufil 5722 04/10/2009 If violation category is 2 and old status is not equal to new then set NonHMCFollowUpDate = null    
	DECLARE @newviolcationcategory INT
	SET @newviolcationcategory = (SELECT tcvs.CategoryID FROM tblCourtViolationStatus tcvs WITH(NOLOCK) WHERE tcvs.CourtViolationStatusID=@VerifiedStatusId)
	
	DECLARE @oldstatusidmain INT
	SET @oldstatusidmain = (SELECT ttv.CourtViolationStatusIDmain
	                          FROM tblTicketsViolations ttv WITH(NOLOCK) WHERE ttv.TicketsViolationID=@TicketsViolationID)
	
	IF EXISTS (SELECT tcvs.CategoryID FROM tblCourtViolationStatus tcvs WITH(NOLOCK)
				INNER JOIN tblTicketsViolations ttv WITH(NOLOCK) ON ttv.CourtViolationStatusIDmain =tcvs.CourtViolationStatusID
	           WHERE tcvs.CategoryID=2 AND ttv.TicketsViolationID=@TicketsViolationID)
	BEGIN
		IF (@newviolcationcategory=2 AND @CourtId NOT IN (3001,3002,3003) AND @oldstatusidmain <> @VerifiedStatusId)
		BEGIN
			-- tahir 8311 09/21/10 fixed the deadlock issues.
			UPDATE t 
			SET t.NonHMCFollowUpDate = NULL ,t.employeeidupdate = @EmployeeId
			FROM tbltickets t INNER JOIN tblticketsviolations v ON v.TicketID_PK = t.ticketid_pk
			WHERE v.TicketsViolationID = @TicketsViolationID
		END
	END
	
	
	--if  @VerifiedCourtNumber is null
	--set @VerifiedCourtNumber = '0'
	-----------------------------------------------------------------
	-- IF USER ENTERS 05tr053164 THEN CONVERT IT TO 2005tr053164
	-----------------------------------------------------------------                                
	DECLARE @cnt INT,
	        @rec INT,
	        @pos INT
	
	SELECT @cnt = 0,
	       @rec = LEN(@causenumber),
	       @pos = 1
	
	WHILE @cnt < @rec
	BEGIN
	    IF ISNUMERIC(SUBSTRING(@Causenumber, @pos, 1)) = 1
	        SET @pos = @pos + 1
	    ELSE
	        BREAK
	    
	    SET @cnt = @cnt + 1
	END                                
	
	IF @Pos = 3
	    SET @causenumber = '20' + @causenumber 
	-----------------------------------------------------------------                                
	
	
	
	
	
	--Waqas 5697 03/27/2009 Check for only one time update.
	DECLARE @IsHmcFtaRemoveFlag BIT 
	SET @IsHmcFtaRemoveFlag = 0
	
	
	
	BEGIN TRAN 
	
	/* ==============================================================================================      
	ADDED BY TAHIR AHMED DT:2/20/2008 TASK 3169....      
	DO NOT UPDATE THE JURY STATUS IF SETTING INFO IS NOT CHANGED......      
	===============================================================================================*/                     
	DECLARE @IsSettingInfoChanged BIT      
	DECLARE @nCategoryid INT      
	
	SELECT @nCategoryid = categoryid
	FROM   tblcourtviolationstatus WITH(NOLOCK)
	WHERE  courtviolationstatusid = @verifiedstatusid
	
	SELECT @nCategoryid = ISNULL(@nCategoryid, 0) 
	
	-- FOR JUDGE TRIALS RELATED TO HCJP 4-2 COURTS ONLY.......      
	IF @nCategoryid = 5
	BEGIN
	    IF EXISTS 
	       (
	           SELECT ticketsviolationid
	           FROM   tblticketsviolations v WITH(NOLOCK)
	                  INNER JOIN tblcourtviolationstatus c WITH(NOLOCK)
	                       ON  c.courtviolationstatusid = v.courtviolationstatusidmain
	           WHERE  @nCategoryid = 5
	                  AND @CourtId = 3014 
	                      --and  ticketsviolationid in (select * from dbo.Sap_String_Param(@TicketsViolationIDs)) Fahad 5299 02/10/2009 Comment
	                  AND (
	                          (@UpdateAll = 1 AND ticketid_pk = @Ticketid)
	                          OR (
	                                 @UpdateAll = 0
	                                 AND ticketsviolationid = @TicketsViolationID
	                                 AND ticketid_pk = @Ticketid
	                             )
	                      )
	                      --    and  ticketsviolationid=@TicketsViolationID
	                      --    AND  ticketid_pk = @Ticketid
	                  AND (
	                          c.categoryid <> @nCategoryid
	                          OR v.courtdatemain <> @verifiedcourtdate
	                          OR v.courtnumbermain <> @verifiedcourtnumber
	                          OR v.courtid <> @courtid
	                      )
	       )
	        SELECT @IsSettingInfoChanged = 1
	    ELSE
	        SELECT @IsSettingInfoChanged = 0
	END
	ELSE
	    -- FOR JURY TRIALS & PRE TRIALS FOR ALL OTHER COURTS......
	BEGIN
	    IF EXISTS 
	       (
	           SELECT ticketsviolationid
	           FROM   tblticketsviolations v
	                  INNER JOIN tblcourtviolationstatus c WITH(NOLOCK)
	                       ON  c.courtviolationstatusid = v.courtviolationstatusidmain
	           WHERE  @nCategoryid IN (3, 4) 
	                  -- and  ticketsviolationid in (select * from dbo.Sap_String_Param(@TicketsViolationIDs)) Fahad 5299 02/10/2009 Comment
	                  AND (
	                          (@UpdateAll = 1 AND ticketid_pk = @Ticketid)
	                          OR (
	                                 @UpdateAll = 0
	                                 AND ticketsviolationid = @TicketsViolationID
	                                 AND ticketid_pk = @Ticketid
	                             )
	                      )
	                      --and  ticketsviolationid=@TicketsViolationID
	                      --AND  ticketid_pk = @Ticketid
	                  AND (
	                          c.categoryid <> @nCategoryid
	                          OR v.courtdatemain <> @verifiedcourtdate
	                          OR v.courtnumbermain <> @verifiedcourtnumber
	                          OR v.courtid <> @courtid
	                      )
	       )
	        SELECT @IsSettingInfoChanged = 1
	    ELSE
	        SELECT @IsSettingInfoChanged = 0
	END 
	
	--Waqas 5697 03/27/2009 
	
	DECLARE @OldCategoryId INT,
	        @NewCategoryId INT
	
	SELECT @OldCategoryId = CategoryID
	FROM   tblticketsviolations
	       INNER JOIN tblCourtViolationStatus tcvs
	            ON  tcvs.CourtViolationStatusID = CourtViolationStatusIDmain
	WHERE  ticketsviolationid = @ViolationId 
	
	SELECT @NewCategoryId = CategoryID
	FROM   tblticketsviolations WITH(NOLOCK)
	       INNER JOIN tblCourtViolationStatus WITH(NOLOCK)
	            ON  tblCourtViolationStatus.CourtViolationStatusID = 
	                CourtViolationStatusIDmain
	WHERE  CourtViolationStatusIDmain = @VerifiedStatusId 
	
	IF (
	       @OldCategoryId <> @NewCategoryId
	       AND @NewCategoryId IN (3, 4, 5)
	       AND @CourtId IN (3001,3002,3003)  --Sabir Khan 5940 05/28/2009 only for HMC Cases...
	   )
	BEGIN
	    IF (@IsHmcFtaRemoveFlag <> 1)
	    BEGIN
	        SET @IsHmcFtaRemoveFlag = 1
	        UPDATE tbltickets 
	        SET		IsHMCFTARemoved = 0
					-- tahir 8311 09/21/10 passed employeeid
					,EmployeeIDUpdate = @EmployeeId 
	        WHERE  TicketID_PK = @Ticketid
	        
	        DECLARE @note1 AS VARCHAR(100) 
	        SET @note1 = 'Flag for HMC FTA Follow Up report has been reset' 	                
	        EXEC usp_HTS_Insert_CaseHistoryInfo @Ticketid,
	             @note1,
	             @EmployeeId,
	             ''
	    END
	END
	
	
	
	
	/*==============================================================================================*/ 
	
	-------------------------------------------- ----------------------------
	--    Added by Asghar update IsEmailedTrialLetter in tbltickets for sending trial letter mail again                        
	
	DECLARE @CourtDate VARCHAR(50),
	        @Status INT,
	        @CID INT,
	        @CoveringFirmId INT                           
	--Ozair 8211 09/04/2010 implemnted null value check
	SELECT @CourtDate = isnull(courtdatemain,'01/01/1900'),
	       @Status = isnull(courtviolationstatusidmain,0),
	       @CID = courtid,
	       @CoveringFirmId = CoveringFirmId
	FROM   tblticketsviolations WITH(NOLOCK)
	WHERE  ticketsviolationid = @TicketsViolationID                 
	
	IF (
	       (@CourtDate != @VerifiedCourtDate)
	       OR (@Status != @VerifiedStatusId)
	       OR (@CID != @CourtId)
	   )
	BEGIN
	    UPDATE tbltickets 
	    SET    IsEmailedTrialLetter = 0
				-- tahir 8311 09/21/10 passed employeeid
				,EmployeeIDUpdate = @EmployeeId 
	    WHERE  ticketid_pk = @Ticketid
	END 
	
	
	
	--If Criminal Court Violation And Status Is Waiting      
	IF (
	       SELECT IsCriminalCourt
	       FROM   tblcourts WITH(NOLOCK)
	       WHERE  courtid = @CourtId
	   ) = 1
	   AND @VerifiedStatusId = 104
	BEGIN
	    DECLARE @Followupdate AS DATETIME    
	    
	    --Waqas 6342 08/20/2009 Follow set to coming friday.
	        SET @Followupdate = dbo.fn_get_next_coming_day(getdate(),6)	       
	    
	    IF (
	           (@VerifiedStatusId != @Status)
	           OR (
	                  (
	                      SELECT COUNT(*)
	                      FROM   tblticketsviolations WITH(NOLOCK)
	                      WHERE  ticketid_pk = @Ticketid
	                             AND courtviolationstatusidmain <> @VerifiedStatusId
	                  ) > 0
	                  AND @UpdateMain = 1
	              )
	       )
	    BEGIN
	        UPDATE tbltickets 
	        SET    criminalfollowupdate = @Followupdate
					-- tahir 8311 09/21/10 passed employeeid
					,EmployeeIDUpdate = @EmployeeId 
	        WHERE  ticketid_pk = @Ticketid    
	        
	        DECLARE @note AS VARCHAR(100)    
	        SET @note = 
	            'Criminal Follow Up Date : Follow Up Date has been changed to ' 
	            + CONVERT(VARCHAR, @Followupdate, 101)    
	        
	        EXEC usp_HTS_Insert_CaseHistoryInfo @Ticketid,
	             @note,
	             @EmployeeId,
	             ''
	    END
	END 
	
	-----------------------------------------------------------------
	-- Update at 30/August 2007 By Asghar
	-- if Bond = yes then update "1" in bondflag field , update "1" all underlinebondflags in tblticketsviolation table
	-----------------------------------------------------------------                                
	
	


	----------------------------------------                     
	
	UPDATE tblticketsviolations 
	SET    refcasenumber = @RefCaseNumber,
	       casenumassignedbycourt = @CauseNumber,
	       Violationnumber_pk = @ViolationId,
	       fineamount = @FineAmount,
	       bondamount = @BondAmount,
	       underlyingbondflag = @BondFlag,
	       VEmployeeID = @EmployeeId,	-- added by ozair                                  
	       OscarCourtDetail = @OscarCourtDetail,	-- added by asghar for oscar court                                      
	       CDI = @CDI,
	       chargelevel = @ChargeLevel,
	       ArrestDate = @ArrestDate,
	       MissedCourtType = CASE 
	                              WHEN @VerifiedStatusId = 105 THEN @MissedCourtType
	                              ELSE -1
	                         END
			-- tahir 8311 09/21/10 code refactored.....
			,ArrignmentWaitingdate=  CASE @ArrignmentWaitingdate WHEN 1 THEN CONVERT(VARCHAR, GETDATE(), 101) ELSE ArrignmentWaitingdate END                          
			,BondWaitingdate = CASE @BondWaitingdate WHEN 1 THEN CONVERT(VARCHAR, GETDATE(), 101) ELSE BondWaitingdate END
	WHERE  ticketsviolationid = @TicketsViolationID                                                    
	
	
	IF @@error <> 0
	BEGIN
	    SELECT @Error = @@error
	END 
	

	------------------------------------------------------------------------------------------------------
	-- CHANGED BY TAHIR AHMED TO ADD MULTIPLE NOTES IN CASE HISTORY IF MULTIPLE VIOLATIONS UPDATED...
	------------------------------------------------------------------------------------------------------                      
	DECLARE @Idx INT,
	        @RecCount INT,
	        @ViolId INT                             
	
	DECLARE @temp TABLE (RowId INT IDENTITY(1, 1), ViolId INT)                                
	
	INSERT INTO @temp
	  (
	    violid
	  )
	SELECT ticketsviolationid
	FROM   tblticketsviolations WITH(NOLOCK)
	WHERE  (
	           (@UpdateAll = 1 AND ticketid_pk = @Ticketid)
	           OR (
	                  @UpdateAll = 0
	                  AND ticketsviolationid = @TicketsViolationID
	                  AND ticketid_pk = @Ticketid
	              )
	       )
	
	SELECT @idx = 1,
	       @RecCount = COUNT(violid)
	FROM   @temp                                
	
	
	
	WHILE @idx <= @RecCount
	BEGIN
	    SELECT @violid = violid
	    FROM   @temp
	    WHERE  rowid = @idx 
	    
	    
	    IF (@IsHmcFtaRemoveFlag <> 1)
	    BEGIN
	        SELECT @OldCategoryId = CategoryID
	        FROM   tblticketsviolations WITH(NOLOCK)
	               INNER JOIN tblCourtViolationStatus tcvs WITH(NOLOCK)
	                    ON  tcvs.CourtViolationStatusID = 
	                        CourtViolationStatusIDmain
	        WHERE  ticketsviolationid = @violid 
	        
	        SELECT @NewCategoryId = CategoryID
	        FROM   tblticketsviolations WITH(NOLOCK)
	               INNER JOIN tblCourtViolationStatus WITH(NOLOCK)
	                    ON  tblCourtViolationStatus.CourtViolationStatusID = 
	                        CourtViolationStatusIDmain
	        WHERE  CourtViolationStatusIDmain = @VerifiedStatusId 
	        
	        IF (
	               @OldCategoryId <> @NewCategoryId
	               AND @NewCategoryId IN (3, 4, 5)
	               AND @CourtId IN (3001,3002,3003)  --Sabir Khan 5940 05/28/2009 only for HMC Cases...
	           )
	        BEGIN
	            SET @IsHmcFtaRemoveFlag = 1
	            UPDATE tbltickets 
	            SET    IsHMCFTARemoved = 0  
					-- tahir 8311 09/21/10 passed employeeid
					,EmployeeIDUpdate = @EmployeeId 
	            WHERE  TicketID_PK = @Ticketid
	            
	            SET @note = 'Flag for HMC FTA Follow Up report has been reset' 	                
	            
	            EXEC usp_HTS_Insert_CaseHistoryInfo @Ticketid,
	                 @note,
	                 @EmployeeId,
	                 ''
	        END
	    END 
	    ---------------------------------------------
	    --Waqas Javed 5173 01/02/2009 Updating Covering Firm ID
	    ---------------------------------------------  
	    DECLARE @OldCourtID INT,
	            @oldCoveringfirmid INT,
	            @tempRefCaseNum AS VARCHAR(20)    
	    
	    SELECT @OldCourtID = courtid,
	           @oldCoveringfirmid = coveringfirmid,
	           @tempRefCaseNum = Refcasenumber
	    FROM   tblticketsviolations WITH(NOLOCK)
	    WHERE  ticketsviolationid = @violid  
	    
	    IF (@CourtId <> @OldCourtID AND @CourtId = 3061)
	    BEGIN
	        UPDATE tblticketsviolations 
	        SET    coveringfirmid = 3043,
	               vemployeeid = @EmployeeId
	        WHERE  ticketsviolationid = @violid    
	        
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	        SELECT @Ticketid,
	               UPPER(ISNULL(@tempRefCaseNum, 'N/A')) +
	               ': Covering Firm changed from [' + UPPER(
	                   ISNULL(
	                       (
	                           SELECT TOP 1 FirmName
	                           FROM   tblfirm
	                           WHERE  firmid = @oldCoveringfirmid
	                       ),
	                       'N/A'
	                   )
	               ) + '] to [' + UPPER(
	                   ISNULL(
	                       (
	                           SELECT TOP 1 FirmName
	                           FROM   tblfirm
	                           WHERE  firmid = 3043
	                       ),
	                       'N/A'
	                   )
	               ) + ']',
	               @EmployeeId
	    END
	    
	    IF (@CourtId <> @OldCourtID AND @OldCourtID = 3061)
	    BEGIN
	        UPDATE tblticketsviolations 
	        SET    coveringfirmid = 3000,
	               vemployeeid = @EmployeeId
	        WHERE  ticketsviolationid = @violid    
	        
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	        SELECT @Ticketid,
	               UPPER(ISNULL(@tempRefCaseNum, 'N/A')) +
	               ': Covering Firm changed from [' + UPPER(
	                   ISNULL(
	                       (
	                           SELECT TOP 1 FirmName
	                           FROM   tblfirm WITH(NOLOCK)
	                           WHERE  firmid = @oldCoveringfirmid
	                       ),
	                       'N/A'
	                   )
	               ) + '] to [' + UPPER(
	                   ISNULL(
	                       (
	                           SELECT TOP 1 FirmName
	                           FROM   tblfirm WITH(NOLOCK)
	                           WHERE  firmid = 3000
	                       ),
	                       'N/A'
	                   )
	               ) + ']',
	               @EmployeeId
	    END 
	    --------------------------------------------                               
	    
	    
	    ------------------------------------------------
	    --Update Covering Firm ID of Multiple Violation
	    ------------------------------------------------    
	    DECLARE @tempRefCaseNumber AS VARCHAR(20)     
	    SELECT @CourtDate = courtdatemain,
	           @tempRefCaseNumber = Refcasenumber,
	           @CID = courtid,
	           @CoveringFirmId = CoveringFirmId
	    FROM   tblticketsviolations WITH(NOLOCK)
	    WHERE  ticketsviolationid = @violid    
	    
	    IF (
	           ISNULL(@VerifiedCourtDate, '1/1/1900') <> ISNULL(@CourtDate, '1/1/1900')
	           AND ISNULL(@VerifiedCourtDate, '1/1/1900') > GETDATE()
	           AND @coveringfirmid <> 3000 
	           AND @casetype = 1 
			   and @CourtId <> 3061
	       )
	    BEGIN
	        UPDATE tblticketsviolations 
	        SET    coveringfirmid = 3000,
	               vemployeeid = @EmployeeId
	        WHERE  ticketsviolationid = @violid    
	        
	        INSERT INTO tblticketsnotes
	          (
	            ticketid,
	            SUBJECT,
	            employeeid
	          )
	        SELECT @Ticketid,
	               UPPER(ISNULL(@tempRefCaseNumber, 'N/A')) +
	               ': Covering Firm changed from [' + UPPER(
	                   ISNULL(
	                       (
	                           SELECT TOP 1 FirmName
	                           FROM   tblfirm WITH(NOLOCK)
	                           WHERE  firmid = @coveringfirmid
	                       ),
	                       'N/A'
	                   )
	               ) + '] to [' + UPPER(
	                   ISNULL(
	                       (
	                           SELECT TOP 1 FirmName
	                           FROM   tblfirm WITH(NOLOCK)
	                           WHERE  firmid = 3000
	                       ),
	                       'N/A'
	                   )
	               ) + ']',
	               @EmployeeId
	    END 
	    ------------------------------------------------------
	    --End COVERING FIRM MODIFICATIONS
	    ------------------------------------------------------    
	    --Hafiz 10241 08/01/2012 Update the JuryCounter
	    DECLARE @PrevCourtDate DATETIME,
	            @CurrentStatus INT,
	            @JuryCounter INT
	    
	    SELECT @PrevCourtDate = ISNULL(courtdatemain, '01/01/1900'),
	           @CurrentStatus = ISNULL(courtviolationstatusidmain, 0),
	           @JuryCounter = ISNULL(JuryTrialCount, 0)
	    FROM   tblticketsviolations WITH(NOLOCK)
	    WHERE  ticketsviolationid = @violid
	   -- AND courtviolationstatusidmain=26
	   
	   --PRINT @ViolId 	   
	   -- WHERE  ticketsviolationid = @TicketsViolationID
	    
	    IF (
	           @PrevCourtDate != @VerifiedCourtDate
	           AND (@VerifiedStatusId = 26)
	       )
	    BEGIN
	        UPDATE tblTicketsViolations
	        SET    JuryTrialCount = @JuryCounter + 1
	        WHERE  ticketid_pk = @Ticketid
	               AND ticketsviolationid = @violid
	               
	              -- PRINT 'first'
	    END
	    ELSE 
	    IF (
	           @PrevCourtDate = @VerifiedCourtDate
	           AND (@VerifiedStatusId = 26)
	       )
	    BEGIN
	        IF (@JuryCounter = 0)
	        BEGIN
	            UPDATE tblTicketsViolations
	            SET    JuryTrialCount = 1
	            WHERE  ticketid_pk = @Ticketid
	                   AND ticketsviolationid = @violid
	                   
	                  --  PRINT '2nd'
	        END
	        ELSE
	        BEGIN
	            UPDATE tblTicketsViolations
	            SET    JuryTrialCount = @JuryCounter
	            WHERE  ticketid_pk = @Ticketid
	                   AND ticketsviolationid = @violid
	                   
	                   -- PRINT '3rd'
	        END
	    END
	    ELSE 
	    IF (@VerifiedStatusId != 26)
	    BEGIN
	        UPDATE tblTicketsViolations
	        SET    JuryTrialCount = 0
	        WHERE  ticketid_pk = @Ticketid
	               AND ticketsviolationid = @violid
	    END
   
	    --End 10241
	    
	    
	    
	    UPDATE tblticketsviolations 
	    SET    courtid = @CourtId,
	           courtdatemain = @VerifiedCourtDate,
	           CourtNumberMain = @VerifiedCourtNumber,
	           courtviolationstatusidmain = @VerifiedStatusId,
	           courtdate = @VerifiedCourtDate,
	           courtnumber = @VerifiedCourtNumber,
	           courtviolationstatusid = @VerifiedStatusId,
	           courtdatescan = @VerifiedCourtDate,
	           courtnumberscan = @VerifiedCourtNumber,
	           courtviolationstatusidscan = @VerifiedStatusId,
	           planid = @PlanId,	-- added by ozair                                                
	           VEmployeeID = @EmployeeId,	-- added by ozair                           
	           OscarCourtDetail = @OscarCourtDetail,	-- added by asghar for oscar court                                      
	           ChargeLevel = CASE 
	                              WHEN (
	                                       SELECT IsCriminalCourt
	                                       FROM   tblcourts WITH(NOLOCK)
	                                       WHERE  courtid = @CourtId
	                                   ) = 0 THEN 0
	                              ELSE ChargeLevel
	                         END,
	           MissedCourtType = CASE 
	                                  WHEN @VerifiedStatusId = 105 THEN @MissedCourtType
	                                  ELSE -1
	                             END
				-- tahir 8311 09/21/10 code refactored...	                             
				, ArrignmentWaitingdate = CASE @ArrignmentWaitingdate WHEN 1 THEN CONVERT(VARCHAR, GETDATE(), 101) ELSE  ArrignmentWaitingdate END                             
				, BondWaitingdate = CASE @BondWaitingdate WHEN 1 THEN CONVERT(VARCHAR, GETDATE(), 101) ELSE  BondWaitingdate END			   
			  --Muhammad Nadir Siddiqui 9236 05/04/2011 Added Arrest date for update-all option.
				, ArrestDate= @ArrestDate	
WHERE  ticketsviolationid = @violid                               
	    
	    SET @idx = @idx + 1
	END                                
	
	
	IF @@error <> 0
	BEGIN
	    SELECT @error = @@error
	END                                                 
	
	
	IF @UpdateMain = 1
	BEGIN
	    UPDATE tbltickets 
	    SET    EmployeeIDUpdate = @EmployeeId --added by ozair
	    WHERE  ticketid_pk = @Ticketid
	END                                                    
	
	IF @@error <> 0
	    SELECT @error = @@error                                                  
	
	DECLARE @speed INT                                 
	EXEC USP_HTS_GET_SpeedingID @Ticketid,
	     @speed OUTPUT
	
	UPDATE tbltickets 
	SET    speedingid = @speed
			-- tahir 8311 09/21/10 passed employeeid
			,EmployeeIDUpdate = @EmployeeId
	WHERE  ticketid_pk = @ticketid                                
	
	IF @@error <> 0
	BEGIN
	    SELECT @error = @@error
	END 
	
	-- ADDED BY TAHIR AHMED DT: 12/28/2006
	-- IF CAUSE NUMBER EXISTS IN NON-CLIENT THEN UPDATE CLIENTFLAG = 1
	-- FOR THIS CAUSE NUMBER.........
	-----------------------------------------------                              
	-- tahir 8311 09/23/10 commented after discussion with CTO; we are already doing this in data cleaning job;
--	IF EXISTS (
--	       SELECT DISTINCT v.recordid
--	       FROM   tblticketsviolationsarchive v,
--	              tblticketsarchive t
--	       WHERE  t.recordid = v.recordid
--	              AND v.causenumber = @CauseNumber
--	              AND t.clientflag = 0
--	   )
--	BEGIN
--		
--		
--		UPDATE	t
--		SET		t.clientflag = 1
--		FROM	tblticketsarchive t  inner join  @NonClients v
--		on		t.recordid = v.recordid
--		AND		t.clientflag = 0
--	END                                
	
	

	IF (
	       SELECT COUNT(*)
	       FROM   tblticketsviolations WITH(NOLOCK)
	       WHERE  ticketid_pk = @Ticketid
	              AND courtviolationstatusid = 105
	   ) = 0
	BEGIN
	    IF (
	           SELECT COUNT(*)
	           FROM   tblhtsbatchprintletter WITH(NOLOCK)
	           WHERE  letterid_fk IN (11, 12)
	                  AND ticketid_fk = @Ticketid
	                  AND ISNULL(deleteflag, 0) = 0
	                  AND isprinted = 0
	       ) > 0
	    BEGIN
	        EXEC USP_HTP_Delete_Letter_From_Batch 11,
	             @Ticketid,
	             @EmployeeId
	    END
	END 
	-- Fahad 10286 12/21/2012 Removed default insertion of traffic waiting follow up date now it will set via Show Setting Control 
	-- Abid Ali 4912 11/13/2008 -- traffic waiting follow up date    
	--IF @VerifiedStatusId = 104
	--BEGIN
	   -- EXECUTE [dbo].[usp_HTP_Update_TrafficWaitingFollowUpDate] @Ticketid, @EmployeeId
	--END 
	
	-- tahir 4710 08/28/2008
	-- set a default bond follow update
	-- if new verified status is BOND or JUV-WAR    
	IF (
	       @Status <> @VerifiedStatusId
	       AND @VerifiedStatusId IN (135, 258)
	   )
	BEGIN
	 
	
		
	    -- if bond follow up date exists .. then update the date 
	    DECLARE @Message AS VARCHAR(4000)   
	    IF EXISTS (
	           SELECT ticketid_pk
	           FROM   tblticketsextensions WITH(NOLOCK)
	           WHERE  ticketid_pk = @Ticketid
	       )
	    BEGIN
	        
	       --Muhammad Muneer 8285 09/28/2010 add the code to support new functionality
	        DECLARE @preBondFollowUpdate VARCHAR(20)
	        SELECT @preBondFollowUpdate = isnull(CONVERT(VARCHAR,bondfollowupdate,101),'N/A') FROM   tblticketsextensions WITH(NOLOCK)  WHERE  ticketid_pk = @Ticketid
	        SET @Message =  'Bond Follow Up Date has been set from [' + @preBondFollowUpdate + '] to ' + ' [' + CONVERT(VARCHAR, GETDATE(), 101) + ']'
	        UPDATE tblticketsextensions 
	        SET    bondfollowupdate =  GETDATE() --Fahad 8211 08/31/2010 Removed Addition of 7 days and assigned Today's Date
	        WHERE  ticketid_pk = @Ticketid 
	        EXECUTE [dbo].[usp_HTS_Insert_CaseHistoryInfo] @TicketId, @Message ,  @EmployeeID,''
	    END-- else insert new bond follow up date...
	    ELSE
	    BEGIN
	        INSERT INTO tblticketsextensions
	          (
	            ticketid_pk,
	            bondfollowupdate
	          )
	        SELECT @Ticketid,
	               GETDATE()--Fahad 8211 08/31/2010 Removed Addition of 7 days and assigned Today's Date
	        --Muhammad Muneer 8285 09/28/2010 add the code to support new functionality        
	       SET @Message =  'Bond Follow Up Date has been set from [N/A] to ' + ' [' + CONVERT(VARCHAR, GETDATE(), 101) + ']' 
	       EXECUTE [dbo].[usp_HTS_Insert_CaseHistoryInfo] @TicketId, @Message ,  @EmployeeID,''
	    END
	    
     
	    
	    
	    
	END 
	-- end 4710 
	-- Sabir Khan 8623 12/10/2010 Remove Pre Trial Diversion flag... 
    --@UpdateAll 
	if @VerifiedStatusId <> 101 AND @CourtId <> 3001 AND  @CourtId <> 3002 AND @CourtId <> 3003
	BEGIN
		IF NOT EXISTS(SELECT * FROM tblticketsviolations WITH(NOLOCK) WHERE TicketID_PK = @Ticketid AND CourtViolationStatusIDmain = 101 AND @CourtId NOT IN (3001,3002,3003))
		BEGIN
			IF EXISTS(SELECT * FROM tblTicketsFlag WITH(NOLOCK) WHERE TicketID_PK = @Ticketid AND FlagID in (42,43))
			BEGIN
				DECLARE @fid INT	
				SELECT @fid = min(ttf.Fid) FROM tblTicketsFlag ttf WITH(NOLOCK) WHERE ttf.TicketID_PK=@Ticketid AND ttf.FlagID=42			
				IF (ISNULL(@fid,0) > 0)
				BEGIN
					EXEC USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID @Ticketid,42,3992,@fid
				END
				SELECT @fid = min(ttf.Fid) FROM tblTicketsFlag ttf WITH(NOLOCK) WHERE ttf.TicketID_PK=@Ticketid AND ttf.FlagID=43	
				IF (ISNULL(@fid,0) > 0)
				BEGIN
					EXEC USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID @Ticketid,43,3992,@fid
				END		
			END
		END		
	END
	IF EXISTS(SELECT * FROM tblticketsviolations tv WITH(NOLOCK) INNER JOIN tblTicketsFlag tf ON tf.TicketID_PK = tv.TicketID_PK WHERE tv.TicketID_PK  = @TicketId AND tv.CourtID IN (3001,3002,3003) AND tv.TicketsViolationID = @TicketsViolationID AND tf.FlagID IN (42,43))
	BEGIN
		IF NOT EXISTS(SELECT * FROM tblticketsviolations tv WITH(NOLOCK) WHERE courtid NOT IN (3001,3002,3003) AND tv.CourtViolationStatusIDmain = 101 AND tv.TicketID_PK = @TicketId)
		BEGIN
			DECLARE @fid1 INT	
			SELECT @fid1 = min(ttf.Fid) FROM tblTicketsFlag ttf WITH(NOLOCK) WHERE ttf.TicketID_PK=@Ticketid AND ttf.FlagID=42			
			IF (ISNULL(@fid1,0) > 0)
			BEGIN
				EXEC USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID @Ticketid,42,3992,@fid1
			END
			SELECT @fid1 = min(ttf.Fid) FROM tblTicketsFlag ttf WITH(NOLOCK) WHERE ttf.TicketID_PK=@Ticketid AND ttf.FlagID=43	
			IF (ISNULL(@fid1,0) > 0)
			BEGIN
				EXEC USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID @Ticketid,43,3992,@fid1
			END	
		END
	END
	
	-- Sabir Khan 8862 02/15/2011 Remove JUVNILE flag... 
	IF  NOT EXISTS (SELECT * FROM tblticketsviolations WITH(NOLOCK) WHERE TicketID_PK = @Ticketid AND CourtViolationStatusIDmain  IN (139,256,257,258,259) AND CourtID IN (SELECT Courtid FROM tblcourts WHERE courtcategorynum  =2 AND InActiveFlag = 0))
    --if @VerifiedStatusId NOT IN (139,256,257,258,259) AND @CourtId IN (SELECT Courtid FROM tblcourts WHERE courtcategorynum  =2 AND InActiveFlag = 0) 
	BEGIN
		IF NOT EXISTS(SELECT * FROM tblticketsviolations WITH(NOLOCK) WHERE TicketID_PK = @Ticketid AND CourtViolationStatusIDmain IN (139,256,257,258,259) AND @CourtId NOT IN (SELECT Courtid FROM tblcourts WHERE courtcategorynum  =2 AND InActiveFlag = 0))
		BEGIN
			IF EXISTS(SELECT * FROM tblTicketsFlag WITH(NOLOCK) WHERE TicketID_PK = @Ticketid AND FlagID = 44)
			BEGIN
				DECLARE @JUVfid INT	
				SELECT @JUVfid = min(ttf.Fid) FROM tblTicketsFlag ttf WITH(NOLOCK) WHERE ttf.TicketID_PK=@Ticketid AND ttf.FlagID=44			
				IF (ISNULL(@JUVfid,0) > 0)
				BEGIN
					EXEC USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID @Ticketid,44,3992,@JUVfid
				END					
			END
		END		
	END
	
	
	
	IF @@error <> 0
	BEGIN
	    SELECT @error = @@error
	END                                 
	
	
	IF @error = 0
	    COMMIT TRAN
	ELSE
	    ROLLBACK TRAN 

GO

grant execute on dbo.[USP_HTS_Update_ViolationUpdateInfo] to dbr_webuser
go  
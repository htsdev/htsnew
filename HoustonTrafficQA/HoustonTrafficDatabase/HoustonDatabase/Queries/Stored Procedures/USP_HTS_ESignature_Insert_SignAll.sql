﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go







-- grant execute on [dbo].[USP_HTS_ESignature_Insert_SignAll] to dbr_webuser
--USP_HTS_ESignature_Insert_SignAll '124546', 'Client', 'Receipt', 0, '', 0, 1, 'pppppp', 4
ALTER PROCEDURE [dbo].[USP_HTS_ESignature_Insert_SignAll] --'124546', 'ClientThumbprint', 'Bond', 1, '', 0, 
@TicketID Varchar(25),
@Entity Varchar(50),
@KeyWord Varchar(50),
@CurrentDoc int,
@ClientLanguage as varchar(25),
@ForDefaultSigns as int,
@SignID as varchar(20),
@SessionID as varchar(50),
@ReceiptPgCount as int

AS
Declare @LoopCounter int
Declare @TotalViolations int
Declare @IsCurrentSignAvailable int
Declare @CurrentDateTime datetime

Set @CurrentDateTime = GetDate()

BEGIN

If @ForDefaultSigns = 1
Begin
	Delete From Tbl_HTS_ESignature_Images Where ImageTrace = 'Current Sign' And SessionID = @SessionID
End

Set @IsCurrentSignAvailable = (Select Count(TicketID) From Tbl_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace = 'Current Sign' And SessionID = @SessionID)
If @IsCurrentSignAvailable = 0
Begin
	Insert Into	Tbl_HTS_ESignature_Images Select @TicketID as TicketID, ImageStream, 'Current Sign' as ImageTrace, 0 as PageNumber, @SessionID As SessionID, @CurrentDateTime  From Tbl_HTS_ESignature_DefaultSignatures Where Entity = @Entity And SignID = @SignID
End
exec USP_HTS_ESignature_Get_BondDoc_NoOfViolations @TicketID, @NoOfViolations = @TotalViolations output

	if @CurrentDoc = 1	--Apply to the current document of the specified TicketID
	Begin
		Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%' + @Entity + '%' And ImageTrace Like '%' + @KeyWord + '%' And TicketID = @TicketID And SessionID = @SessionID
		Insert Into Tbl_HTS_ESignature_Images Select @TicketID As TicketID, Null as ImageStream, ImageTrace, 999 as PageNumber, @SessionID As SessionID, @CurrentDateTime From Tbl_HTS_ESignature_Location Where ImageTrace Like '%' + @Entity + '%' And ImageTrace Like '%' + @KeyWord + '%' And KeyWord <> 'Inside' And PageDescription <> 'ContractPageSpanish' And PageDescription <> 'PowerOfAttorneyPageSpanish'
		Update Tbl_HTS_ESignature_Images Set ImageStream = (Select Top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace = 'Current Sign' And SessionID = @SessionID) Where ImageTrace Like '%' + @Entity + '%' And ImageTrace Like '%' + @KeyWord + '%' And TicketID = @TicketID And SessionID = @SessionID
	End

	if @CurrentDoc = 0	--Apply to the all documents of the specified TicketID
	Begin        
		Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%' + @Entity + '%' And TicketID = @TicketID And SessionID = @SessionID
		Insert Into Tbl_HTS_ESignature_Images Select @TicketID As TicketID, Null as ImageStream, ImageTrace, 999 as PageNumber, @SessionID As SessionID, @CurrentDateTime From Tbl_HTS_ESignature_Location Where ImageTrace Like '%' + @Entity + '%' And KeyWord <> 'Inside' And PageDescription <> 'ContractPageSpanish' And PageDescription <> 'PowerOfAttorneyPageSpanish'
		Update Tbl_HTS_ESignature_Images Set ImageStream = (Select Top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace = 'Current Sign' And SessionID = @SessionID) Where ImageTrace Like '%' + @Entity + '%' And TicketID = @TicketID And SessionID = @SessionID
	End
	
	if (@KeyWord = 'Bond' or @CurrentDoc = 0) 
	Begin
		Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%Bond%' And ImageTrace Like '%' + @Entity + '%' And TicketID = @TicketID And PageNumber <> 999 And SessionID = @SessionID
		Set @LoopCounter = 1
		while @LoopCounter <= @TotalViolations --For Original Bond
			Begin
				if @Entity = 'Attorney'
				Begin
					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null , 'ImageStream_Bond_Attorney1', @LoopCounter, @SessionID, @CurrentDateTime)
					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_Attorney2', @LoopCounter, @SessionID, @CurrentDateTime)
					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_Attorney3', @LoopCounter, @SessionID, @CurrentDateTime)
				End
				if @Entity = 'ClientSign'
				Begin
					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_ClientSign1', @LoopCounter, @SessionID, @CurrentDateTime)
				End
				if @Entity = 'ClientThumbprint'
				Begin
					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_ClientThumbprint1', @LoopCounter, @SessionID, @CurrentDateTime)
				End
				Set @LoopCounter = @LoopCounter + 1
			End
		--For ClientInitialsPage
		--Update Tbl_HTS_ESignature_Images Set PageNumber = @LoopCounter Where TicketID = @TicketID And (ImageTrace = 'ImageStream_Bond_ClientSign2' Or ImageTrace Like 'ImageStream_Bond_ClientInitial%') And SessionID = @SessionID
--		--For Contract Page
--		Adil 08/28/2008 4633
--		Update Tbl_HTS_ESignature_Images Set PageNumber = @LoopCounter + 1 Where TicketID = @TicketID And ImageTrace = 'ImageStream_Bond_ClientSign3' And SessionID = @SessionID

		--For PowerOfAttorney Page
		--Update Tbl_HTS_ESignature_Images Set PageNumber = @LoopCounter + 1 Where TicketID = @TicketID And ImageTrace = 'ImageStream_Bond_ClientSign3' And SessionID = @SessionID

--		Set @LoopCounter = 1
--		while @LoopCounter <= @TotalViolations --For Duplicate Bond [Copies]
--			Begin
--				if @Entity = 'Attorney'
--				Begin																													-- Here 2 = Deference of no of pages between last original violation page and the first duplicate violation page
--					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_Attorney1', @LoopCounter + @TotalViolations + 2, @SessionID, @CurrentDateTime)
--					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_Attorney2', @LoopCounter + @TotalViolations + 2, @SessionID, @CurrentDateTime)
--					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_Attorney3', @LoopCounter + @TotalViolations + 2, @SessionID, @CurrentDateTime)
--				End
--				if @Entity = 'ClientSign'
--				Begin																														-- Here 2 = Deference of no of pages between last original violation page and the first duplicate violation page
--					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_ClientSign1', @LoopCounter + @TotalViolations + 2, @SessionID, @CurrentDateTime)
--				End
--				if @Entity = 'ClientThumbprint'
--				Begin																															-- Here 2 = Deference of no of pages between last original violation page and the first duplicate violation page
--					Insert Into Tbl_HTS_ESignature_Images values(@TicketID, Null, 'ImageStream_Bond_ClientThumbprint1', @LoopCounter + @TotalViolations + 2, @SessionID, @CurrentDateTime)
--				End
--				Set @LoopCounter = @LoopCounter + 1
--			End
--		--For Blank Page															-- Here 2 = Deference of no of pages between last original violation page
		Update Tbl_HTS_ESignature_Images Set PageNumber = @TotalViolations + 1 Where TicketID = @TicketID And (ImageTrace = 'ImageStream_Bond_ClientSign1' OR ImageTrace = 'ImageStream_Bond_ClientThumbprint1') And SessionID = @SessionID And PageNumber = 999

		--For Plea Of Not Guilty Page												-- Here 2 = Deference of no of pages between last original violation page and the first duplicate violation page
		Update Tbl_HTS_ESignature_Images Set PageNumber = @LoopCounter + @TotalViolations + 2 Where TicketID = @TicketID And (ImageTrace = 'ImageStream_Bond_Attorney4') And SessionID = @SessionID And PageNumber = 999

		Update Tbl_HTS_ESignature_Images Set ImageStream = (Select Top 1 convert(varbinary(8000),ImageStream) From Tbl_HTS_ESignature_Images Where TicketID = @TicketID And ImageTrace = 'Current Sign' And SessionID = @SessionID) Where (ImageTrace Like '%Bond%' or ImageTrace Like '%Receipt%') And ImageTrace Like '%' + @Entity + '%' And TicketID = @TicketID And SessionID = @SessionID
	End
	--Update Page Numbers 999 to 1 (Except bond related records)
	Update Tbl_HTS_ESignature_Images Set PageNumber = 1 Where TicketID = @TicketID And ImageTrace Not Like '%Bond%' And SessionID = @SessionID And PageNumber = 999

	--For Trial Letter
	if (@KeyWord = 'LetterTrial') 
	Begin
		Insert Into Tbl_HTS_ESignature_Images Select TicketID, ImageStream, ImageTrace, 2 as PageNumber, SessionID, TimeStamp From Tbl_HTS_ESignature_Images
		Where ImageTrace = 'ImageStream_LetterTrial_Attorney1' And SessionID = @SessionID And TicketID = @TicketID
	End

	--For Receipt -- Adil 5806 05/13/2009 receipt signatures for English and Spanish
--	if @ClientLanguage = 'ENGLISH'-- Adil 5987 06/11/2009 comment code for English. now both versions will have sign on second page.
--		begin
			Update Tbl_HTS_ESignature_Images Set PageNumber = 2 Where TicketID = @TicketID And ImageTrace Like '%Receipt%' And SessionID = @SessionID
--		end
--	else
--		begin
--			Update Tbl_HTS_ESignature_Images Set PageNumber = 1 Where TicketID = @TicketID And ImageTrace Like '%Receipt%' And SessionID = @SessionID
--		end

	
--	if (@KeyWord = 'Receipt')  
--	BEGIN
--		Update Tbl_HTS_ESignature_Images Set PageNumber = 2 Where TicketID = @TicketID And ImageTrace Like '%Receipt%' And SessionID = @SessionID
----		Set @LoopCounter = 2
----		while @LoopCounter <= @ReceiptPgCount
----			Begin
----				Insert Into Tbl_HTS_ESignature_Images Select Top 1 TicketID, ImageStream, ImageTrace, @LoopCounter as PageNumber, SessionID, TimeStamp 
----				From Tbl_HTS_ESignature_Images
----				Where ImageTrace = 'ImageStream_Receipt_ClientSign1' And SessionID = @SessionID And TicketID = @TicketID
--				--Set @LoopCounter = @LoopCounter + 1
----			End
--	End

	--Deleting unused signatures
	DELETE FROM Tbl_HTS_ESignature_Images WHERE PageNumber = 999
	
	--Deleting temporary & one day old ImageStreams
	Delete From Tbl_HTS_ESignature_Images Where (TicketID = @TicketID And SessionID = @SessionID And ImageTrace = 'Current Sign') or datediff(d, timestamp, getdate())>2
END

/****** 
Create by:  Syed Muhammad Ozair

Business Logic : this procedure is used to Insert the information in tbl_htp_documenttracking_scanbatch table and 
				 returns the inserted id back to the calling method

List of Parameters:	
	@ScanBatchDate : Batch scan date
	@EmployeeID : employee id of current logged in user

List of Columns: 	
	ScanBatchID : currently inserted ID, this field will be returned to the calling method. 
	
******/

Create Procedure dbo.usp_HTP_Insert_DocumentTracking_ScanBatch

@ScanBatchDate datetime,
@EmployeeID	int

as

Declare @SBID int

set @SBID = 0

insert into tbl_htp_documenttracking_scanbatch(ScanBatchDate, EmployeeID)
Values(@ScanBatchDate, @EmployeeID)

select @SBID = Scope_Identity()

select @SBID as ScanBatchID

go

grant exec on dbo.usp_HTP_Insert_DocumentTracking_ScanBatch to dbr_webuser
go


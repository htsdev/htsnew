SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Get_NameDiscrepancies]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Get_NameDiscrepancies]
GO

CREATE procedure [dbo].[USP_Get_NameDiscrepancies]

as


select distinct t.lastname, t.firstname, t.ticketid_pk, v.refcasenumber,   casenumassignedbycourt 
into #temp2
from tblticketsviolations v, tbltickets t
where t.ticketid_pk = v.ticketid_pk
and t.activeflag = 1
and v.courtviolationstatusid in(
	select courtviolationstatusid from tblcourtviolationstatus where categoryid in (2,3,4,5,7) 
	union 
	select 201
	union 
	select 202
	)


select  t.lastname, v.causenumber, t.firstname, v.ticketnumber_pk
into #temp3 
from tblticketsviolationsarchive v, tblticketsarchive t
where t.recordid = v.recordid
and causenumber in (select casenumassignedbycourt from #temp2)

select distinct 
	'<a href="javascript:window.open(''../clientinfo/violationfeeold.aspx?search=0&casenumber='+convert(varchar(10),a.ticketid_pk)+''');void('''');"''>' + a.refcasenumber+'</a>' as [Ticket Number],
	a.firstname as [First Name - Clients], 
	a.lastname as [Last Name -Clients], 
	a.casenumassignedbycourt as [Cause Number] ,
	b.ticketnumber_pk [Case # - NonClients], 
	b.firstname as [First Name - NonClients], 
	b.lastname as [Last Name - NonClients] 
from #temp2 a, #temp3 b
where a.casenumassignedbycourt = b.causenumber
and a.lastname <> b.lastname
order by [cause number]

drop table #temp2
drop table #temp3


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


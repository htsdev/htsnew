/*****
	Business Logic :  this procedure returns the email address of the user assigned to the selected category
	Column Return:
				Email Address:  Email Address of the users associated with the selected category...
*****/

CREATE Procedure [dbo].[USP_HTP_Get_AssociatedEmailAddress]

@assCategory VARCHAR(MAX)

as


Select ISNULL(tu.Email,'') AS emailto
	  from CaseTypeServiceCategory ctsc 
		inner join tblServiceTicketCategories stc on stc.ID = ctsc.ServiceCategoryId    
		inner join CaseType ct on ctsc.CaseTypeId = ct.CaseTypeId
		left outer join tblusers tu on stc.AssignedTo = tu.EmployeeID
	where stc.Description = LTRIM(RTRIM(@assCategory))
	
	
	
	




GO

GRANT EXECUTE ON 	[dbo].[USP_HTP_Get_AssociatedEmailAddress] TO webuser_hts

GO

	
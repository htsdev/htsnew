USE [TrafficTickets]
GO
/****** Object:  StoredProcedure [dbo].[USP_HTP_Get_Confirmation_Status]    Script Date: 04/09/2013 16:13:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*    
* Created By: Rab Nawaz   
* Task: 10729  
* Date: 09/04/2013    
*     
* Business Logic: Used to Get Nisi Contact Type from DB.     
*     
*/    
CREATE PROCEDURE [dbo].[USP_HTP_Get_NisiContactTypes]    
AS
BEGIN
	SELECT ContactTypeid, [Description] FROM [TrafficTickets].[dbo].[tblNisiContactTypes]	
END    

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_NisiContactTypes] TO dbr_webuser
GO



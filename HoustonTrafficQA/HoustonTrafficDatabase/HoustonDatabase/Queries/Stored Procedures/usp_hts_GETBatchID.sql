﻿Alter procedure [dbo].[usp_hts_GETBatchID]          
          
@Ticketid as int          

as          
          
          
select  distinct 
right(left(docpath,len(docpath)-4), len(left(docpath,len(docpath)-4))-6) as batchid,        
max(recordid) as recordid,
0 as rectype          
from tblhtsnotes inner join tblusers 
on tblhtsnotes.empid=tblusers.employeeid           
where ticketid_fk = @Ticketid 
and letterid_fk = 10        
group by right(left(docpath,len(docpath)-4), len(left(docpath,len(docpath)-4))-6)    

union

select bd.documentbatchid as batchid,
bd.scanbatchid_fk as recordid,
1 as rectype
from tbl_htp_documenttracking_batchtickets bt inner join tbl_htp_documenttracking_scanbatch_detail bd
on bt.batchid_fk=bd.documentbatchid
where bt.verifieddate is not null
and ticketid_fk=@Ticketid
        
      
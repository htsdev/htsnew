SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_ls_search_ticket]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_ls_search_ticket]
GO


CREATE Procedure usp_ls_search_ticket 
(  
@TicketID varchar(20)  
  
  
)  
  
as  
  
Select distinct v.recordid  from tblTicketsviolationsArchive v
inner join tblticketsarchive t on t.recordid = v.recordid
and	v. TicketNumber_pk = @TicketID  
and t.clientflag = 0


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


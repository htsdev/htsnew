SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_All_ScanDocuments_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_All_ScanDocuments_2]
GO








CREATE  PROCEDURE [dbo].[usp_Get_All_ScanDocuments_2] 

@TicketID varchar(30),
@DocType varchar(50)

AS


IF (@DocType="All")
Begin
	SELECT     TrafficDocs.traffic.DOCMAIN.DOC_ID, TrafficDocs.traffic.DOCMAIN.DOCREF, TrafficDocs.traffic.DOCPIC.UPDATEDATETIME,  '0' as DOC_Num, ' ' AS ResetDesc, '' AS RefCaseNumber
	FROM         TrafficDocs.traffic.DOCMAIN LEFT OUTER JOIN
             		         TrafficDocs.traffic.DOCPIC ON TrafficDocs.traffic.DOCMAIN.DOC_ID = TrafficDocs.traffic.DOCPIC.DOC_ID
	WHERE     (TrafficDocs.traffic.DOCMAIN.DOCSOURCE = @TicketID )
	
	UNION
	
	SELECT     dbo.tblTicketsViolations.ScanDocID AS DOC_ID, 'Resets' AS DOCREF, 
                      dbo.tblTicketsViolations.UpdatedDate AS UPDATEDATETIME, dbo.tblTicketsViolations.ScanDocNum AS DOC_Num,
	LTRIM(dbo.tblTicketsViolations.RefCaseNumber)
                      + ' - ' + LTRIM(STR(dbo.tblTicketsViolations.SequenceNumber)) + '  - ' + dbo.tblCourtViolationStatus.ShortDescription + '  - ' + CONVERT(varchar(20), 
                      dbo.tblTicketsViolations.CourtDate, 101) + ' ' +  SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 1, 5) 
                      + ' ' + SUBSTRING(SUBSTRING(CONVERT(char(26), dbo.tblTicketsViolations.CourtDate, 109), 13, 14), 13, 2) 
                      + '  - #' + dbo.tblTicketsViolations.CourtNumber + '  - ' + ISNULL(dbo.tblViolations.Description, '') AS ResetDesc, dbo.tblTicketsViolations.TicketsViolationID
FROM         dbo.tblTicketsViolations LEFT OUTER JOIN
                      dbo.tblViolations ON dbo.tblTicketsViolations.ViolationNumber_PK = dbo.tblViolations.ViolationNumber_PK LEFT OUTER JOIN
                      dbo.tblCourtViolationStatus ON dbo.tblTicketsViolations.CourtViolationStatusID = dbo.tblCourtViolationStatus.CourtViolationStatusID
	WHERE     (dbo.tblTicketsViolations.ScanDocID IS NOT NULL)    AND  TicketID_PK = @TicketID
	Order By dbo.tblTicketsViolations.RefCaseNumber
	
end

ELSE IF (@DocType<> "<choose>")
begin

	SELECT     TrafficDocs.traffic.DOCMAIN.DOC_ID, TrafficDocs.traffic.DOCMAIN.DOCREF, TrafficDocs.traffic.DOCPIC.UPDATEDATETIME,  '0' as DOC_Num, ' '  AS ResetDesc
	FROM         TrafficDocs.traffic.DOCMAIN LEFT OUTER JOIN
             		         TrafficDocs.traffic.DOCPIC ON TrafficDocs.traffic.DOCMAIN.DOC_ID = TrafficDocs.traffic.DOCPIC.DOC_ID
	WHERE     (TrafficDocs.traffic.DOCMAIN.DOCSOURCE = @TicketID )
			And  Lower(TrafficDocs.traffic.DOCMAIN.DOCREF) = Lower(@DocType)
end




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


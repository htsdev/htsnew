SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_ReminderCall_comments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_ReminderCall_comments]
GO











---

CREATE procedure usp_HTS_Get_ReminderCall_comments --102623,736932,'04/05/2006'
           
@ticketid int  ,            
@TicketsViolationID int,   
@courtdate datetime         
      
as            
      
SELECT DISTINCT       
TV.ticketid_pk,TV.TicketsViolationID, T.Firstname, T.Lastname, T.LanguageSpeak, T.BondFlag,       
U.Address + ' ' + U.City + ', ' + 'TX' + ' ' + U.Zip AS courtaddress,tv.ReminderComments as comments,  
tv.ReminderCallStatus as status,
dbo.fn_hts_get_TicketsViolations (@ticketid,@courtdate) as  trialdesc, --calling func in order to get distinct violations
I.FirmAbbreviation,
-- I.Phone, I.Fax,   
-- TV.CourtDateMain, TV.CourtNumbermain,
convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact1),''))+'('+ISNULL(LEFT(Ct1.Description, 1),'')+')' as contact1,            
convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact2),''))+'('+ISNULL(LEFT(Ct2.Description, 1),'')+')' as contact2,            
convert(varchar(20),ISNULL(dbo.formatphonenumbers(T.Contact3),''))+'('+ISNULL(LEFT(Ct3.Description, 1),'')+')' as contact3             

FROM         dbo.tblFirm I RIGHT OUTER JOIN      
                      dbo.tblContactstype CT1 RIGHT OUTER JOIN      
                      dbo.tblTicketsViolations TV INNER JOIN      
                      dbo.tblTickets T ON TV.TicketID_PK = T.TicketID_PK INNER JOIN      
                      dbo.tblCourts U ON TV.CourtID = U.Courtid INNER JOIN      
                      dbo.tblCourtViolationStatus CVS ON TV.CourtViolationStatusID = CVS.CourtViolationStatusID
                --      INNER JOIN dbo.tblDateType F ON CVS.CategoryID = F.TypeID
		      LEFT OUTER JOIN  
                      dbo.tblContactstype ct3 ON T.ContactType3 = ct3.ContactType_PK LEFT OUTER JOIN      
                      dbo.tblContactstype ct2 ON T.ContactType2 = ct2.ContactType_PK ON CT1.ContactType_PK = T.ContactType1 ON I.FirmID = T.FirmID      
WHERE     (TV.TicketsViolationID = @TicketsViolationID) AND (TV.ticketid_pk = @ticketid)       
      
  
-------------
              






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


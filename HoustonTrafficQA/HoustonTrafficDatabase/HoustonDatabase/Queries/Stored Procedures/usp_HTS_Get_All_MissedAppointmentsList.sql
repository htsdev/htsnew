SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_Get_All_MissedAppointmentsList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_Get_All_MissedAppointmentsList]
GO









CREATE PROCEDURE [dbo].[usp_HTS_Get_All_MissedAppointmentsList]     
    
@DateFrom datetime,    
@DateTo datetime    
    
AS    
Declare @intDays int    
    
IF (DATEPART(dw, GETDATE() + 1) =1)     
 set @intDays=3    
else if (DATEPART(dw, GETDATE() + 2) =7)     
 set @intDays=2    
else    
 set @intDays=1    
    
SELECT DISTINCT     
 dbo.tblViolationQuote.QuoteID,   
 CONVERT(varchar(12), dbo.tblViolationQuote.LastContactDate, 101) AS LastContactDate,   
 CONVERT(Int, GETDATE()   - dbo.tblViolationQuote.LastContactDate) AS Days,   
 dbo.tblViolationQuote.AppointmentDate,   
 dbo.tblTickets.Activeflag,     
 dbo.tblUsers.Firstname + ' ' + dbo.tblUsers.Lastname AS EmpName,   
 dbo.tblTickets.Firstname + ' ' + dbo.tblTickets.Lastname AS Customer,     
 CONVERT(decimal(8,0), dbo.tblTickets.calculatedtotalfee) as calculatedtotalfee,   
 dbo.tblViolationQuote.QuoteComments,     
 tblQuoteResult_2.QuoteResultDescription,   
 dbo.tblViolationQuote.FollowUpYN,   
 dbo.tblTickets.TicketID_PK,     
 REPLACE(tblQuoteResult_1.QuoteResultDescription, '---------------------', ' ') AS FollowUpStatus,     
 CONVERT(varchar(12), dbo.tblTicketsViolations.CourtDateMain, 101) AS CourtDate,    
 dbo.tblViolationQuote.CallBackDate,   
 dbo.tblViolationQuote.AppointmentDate,     
 dbo.tblViolationQuote.CallBackTime,   
 dbo.tblViolationQuote.AppointmentTime    
    
FROM    dbo.tblViolationQuote   
LEFT OUTER JOIN    
 dbo.tblQuoteResult tblQuoteResult_1   
ON  dbo.tblViolationQuote.FollowUPID = tblQuoteResult_1.QuoteResultID   
LEFT OUTER JOIN    
 dbo.tblTicketsViolations   
RIGHT OUTER JOIN    
 dbo.tblTickets   
ON  dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK   
ON    dbo.tblViolationQuote.TicketID_FK = dbo.tblTickets.TicketID_PK   
LEFT OUTER JOIN    
 dbo.tblUsers   
ON  dbo.tblTickets.EmployeeID = dbo.tblUsers.EmployeeID   
LEFT OUTER JOIN    
 dbo.tblQuoteResult tblQuoteResult_2   
ON  dbo.tblViolationQuote.QuoteResultID = tblQuoteResult_2.QuoteResultID    
    
WHERE  dbo.tblTickets.Activeflag = 0  
AND  dbo.tblViolationQuote.FollowUpYN = 1  
-- AND (CONVERT(datetime, dbo.tblViolationQuote.AppointmentDate, 101) <= GETDATE())     
-- And  (dbo.tblTicketsViolations.CourtDateMain <> GETDATE()+ @intDays)    
--AND  ( (CONVERT(datetime, dbo.tblViolationQuote.AppointmentDate, 101) >= CONVERT(datetime, @DateFrom, 101) )    
--And  (CONVERT(datetime, dbo.tblViolationQuote.AppointmentDate, 101) <= CONVERT(datetime, (@DateTo+1), 101)) )    
  
-- corrected by Tahir Ahmed to eliminate garbade dates.......  
AND  dbo.tblViolationQuote.AppointmentDate >=  @DateFrom  
And  dbo.tblViolationQuote.AppointmentDate <= @DateTo   
  
  
And  (dbo.tblTicketsViolations.CourtDateMain IN    
                           (  
    SELECT  MAX(dbo.tblTicketsViolations.CourtDateMain)    
                             FROM    dbo.tblTicketsViolations    
                             WHERE dbo.tblTicketsViolations.TicketID_PK = dbo.tblTickets.TicketID_PK)  
    )    
    
  
    
    
    
  
  
  






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


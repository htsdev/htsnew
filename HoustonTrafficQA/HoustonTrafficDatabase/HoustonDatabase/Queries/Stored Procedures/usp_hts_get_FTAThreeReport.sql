ALTER  procedure dbo.USP_HTS_GET_FTAThreeReport  
 (  
  
@FromDate datetime ,  
@ToDate  datetime   
 )  
as  
  
-- CREATING TEMPORARY TABLE TO STORE FILTERED DATA.............  
declare @Archive TABLE  
(  
 [midnumber] [varchar] (20)  ,  
 [maildatefta_three] [datetime] NULL ,  
 [ticketnumber] [varchar] (20)  ,  
 [Courtid] [int]  
)   
  
-- INSERTING FILTERED DATA IN TEMPORARY VARIABLE.................  
insert into @Archive  
select distinct  
 midnumber,  
 -- FIELD USED FOR FTA LETTER.......  
 convert(varchar(10),min(maildateftarm),101),  
  
 -- GETTING MINIMUM  OF TICKET NUMBER IF MORE THAN ONE PROFILE EXISTS.....  
 min(ticketnumber),  
 min(courtid)  
  
from tblticketsarchive  
  
-- RECORDS RELATED TO GIVEN DATE RANGE ONLY........  
where convert(varchar(10),maildateftarm,101) between @fromdate and @todate  
  
-- ONLY INSIDE COURT RECORDS REQUIRED.......  
and courtid in (3001,3002,3003)  
group by  
 midnumber,   
 convert(varchar(10),maildateftarm,101)  
  
  
-- CREATING TEMPORARY TABLE  
declare  @temp1 TABLE (  
 [TicketID_PK] [int] NULL ,  
 [MailDateFTA_THREE] [datetime] NULL ,  
 [ActiveFlag] [tinyint] NULL ,  
 [QuoteDate] [datetime] NULL ,  
 [ChargeAmount] [money] NULL ,  
 [HireDate] [datetime] NULL   
 )  
  
-- INSERTING RECORDS WITH PAYMENT AMOUNT & HIRED DATE IN TEMPORARY TABLE.......  
insert into @temp1  
SELECT  t.TicketID_PK,   
 ta.MailDateFTA_THREE,  
 t.Activeflag as ActiveFlag,   
  
 -- DATE WHEN PROFILE WAS CREATED.....  
 t.Recdate AS QuoteDate,   
  
 -- PAID AMOUNT...........  
 ChargeAmount = (  
  select isnull(sum(isnull(p.chargeamount,0)),0) from tblticketspayment p  
  where p.ticketid = t.ticketid_pk  
  and p.paymenttype not in (99,100)  
  and p.paymentvoid = 0  
  ),  
  
 -- DATE ON WHICH FIRST PAYMENT WAS MADE.............  
 HireDate = (  
  select min(p1.recdate) from tblticketspayment p1  
  where p1.ticketid = t.ticketid_pk  
  )  
  
-- FROM TEMP TABLE AND TBLTICKETS BY RELATING THE MID NUMBER, COURT ID, TICKET NUMBER  
-- AND FTA LETTER DATE SHOULD BE LESS THAN QUOTE DATE BY THREE DAYS...............  
-- GETTING ALL RECORDS FROM THE TEMP TABLE TO GET THE LETTER COUNT  
FROM @Archive ta   
LEFT OUTER JOIN  
 tbltickets t  
ON t.ticketid_pk in (  
  select min(b.ticketid_pk) from tbltickets b inner join tblticketsviolations v  
  on b.ticketid_pk = v.ticketid_pk  
  where b.Midnum = ta.MidNumber   
  AND  v.CourtId = ta.CourtID   
  and datediff(day,ta.maildatefta_three,b.recdate) >=3   
  and v.refcasenumber = ta.ticketnumber  
  )  
  
  
  
-- CREATING TEMPORAY TABLE WHICH WILL STORE THE AGGREGATED VALUES.......  
declare  @temp2 TABLE (  
 [maildatefta_three] [datetime] NULL ,  
 [letters] [int] NULL ,  
 [Quote] [int] NULL ,  
 [Revenue] [money] NOT NULL ,  
 [Hired] [int] NULL   
 )  
  
-- INSERTING INTO NEW TEMPORARY TABLE AFTER GETTING LETTER COUNT, NO. OF QOUTES AND NO. OF CLIENTS PER MAIL DATE.....  
insert into @temp2  
select a.maildatefta_three,       -- DISTINCT MAIL DATE  
 count(*) as letters,       -- COUNT OF LETTERS SENT PER MAIL DATE..  
 Quote = (        -- COUNT OF QUOTES....  
  select count(*) from @temp1 x where x.activeflag = 0  
   and x.maildatefta_three = a.maildatefta_three  
  ),  
 Revenue = isnull(sum(isnull(a.chargeamount,0)),0),   -- SUM OF PAYMENTS RECEIVED....  
 Hired = (        -- COUNT OF HIRED  
  select  count(*) from @temp1 y where y.hiredate is not null  
  and y.maildatefta_three = a.maildatefta_three  
  )   
  
from  @temp1 a  
group by  
 a.maildatefta_three  
  
-- CREATING TEMPORARY TABLE  
declare  @temp3 TABLE (  
 [MailDate] [datetime] NULL ,  
 [LetterCount] [int] NULL ,  
 [Quote] [int] NULL ,  
 [Hired] [int] NULL ,  
 [PostalCost] [numeric](13, 2) NULL ,  
 [Revenue] [money] NOT NULL   
 )  
  
-- INSERTING RECORDS INTO ANOTHER TEMPORARY VARIABLE AFTER CALCULATING THE POSTAL COST.....  
insert into @temp3  
select maildatefta_three as MailDate,  
 letters as LetterCount,  
 Quote,  
 Hired,  
 (case when letters < 500 then 0.39* letters  
  else 0.30 * letters  
 end) as PostalCost,  
 Revenue  
from @temp2   
  
-- CREATING TEMPORARY TABLE.......  
declare @temp4 TABLE  (  
 [MailDate] [datetime] NULL ,  
 [LetterCount] [float] NULL ,  
 [Quote] [float] NULL ,  
 [Hired] [float] NULL ,  
 [Total] [float] null,  
 [PostalCost] [money] NULL ,  
 [Revenue] [money] NOT NULL ,  
 [Profit] [money] NULL ,  
 [TotalReturn] [numeric](10,4) NULL ,  
 [PercentTotal] [numeric] (10,4) NULL ,  
 [PercentHired] [numeric] (10,4) NULL ,  
 [HiresTotalRatio] [varchar] (20) NULL   
 )   
  
-- INSERTING RECORDS IN TEMP. TABLE AFTER CALCULATING PROFIT, RETURN AND DIFF. PERCENTAGES & RATIO....  
insert into @temp4  
select   
 -- DISTINCT MAILER DATE....  
 MailDate,  
   
 -- NO. OF LETTERS SENT....  
 LetterCount,  
   
 -- NO. OF CLIENTS QUOTED.....  
 Quote,  
   
 -- NO. OF CLIENTS HIRED.....  
 Hired,  
   
 -- TOTAL NO. OF CASES DEALT....  
 Hired + Quote,  
  
 -- POSTAL COST PER MAILER DATE......  
 PostalCost,  
  
 -- REVENUE REGENERATED THROUGH CLIENTS HIRED THROUGH  THAT MAILER .....  
 Revenue,  
  
 -- TOTAL PROFIT/LOSS ON THAT MAILER DATE......  
 Revenue - PostalCost  as Profit,  
  
 -- TOTAL RETURN, RETURN ON INVESTMENT....  
 TotalReturn = ((Revenue - PostalCost) / PostalCost) ,  
  
 -- PERCENTAGE OF TOTAL CLIENTS CONTACTED TO THE NO. OF LETTERS SENT......  
 PercentTotal = ((cast(Quote as float) + cast(hired as float))   / cast(LetterCount as float))  * 100  ,  
  
 -- PERCENTAGE OF TOTAL CLIENTS HIRED TO THE NO. OF LETTERS SENT.......  
 PercentHired = ((cast(Hired as float) / cast(LetterCount as float))* 100 ) ,  
  
 -- RATIO OF NO. OF HIRED CLIENTS TO NO. OF TOTAL CLIENTS CONTACTED......  
 HiresTotalRatio = (case (quote + hired )  
    when 0 then '0'  
    else  cast(hired as float) / (cast(Quote as float) + cast(hired as float))        
     end )  
from  @temp3  
  
  
  
-- FINAL OUT PUT...................  
select convert(varchar(10),maildate,101) as maildate,  
 convert(numeric(10,0),LetterCount) as LetterCount,  
 convert(numeric(10,0),total) as Total,  
 convert(numeric(10,0),Quote) as Quote,  
 convert(numeric(10,0),Hired) as Hired,  
 convert(numeric(10,2),postalcost) as postalcost,  
 convert(numeric(10,2),Revenue) as Revenue,  
 convert(numeric(10,2),Profit) as Profit,  
 convert(numeric(10,2),TotalReturn) as TotalReturn,  
 convert(numeric(10,2),PercentTotal) as PercentTotal,  
 convert(numeric(10,2),PercentHired) as PercentHired,  
 convert(numeric(10,2),HiresTotalRatio) as HiresTotalRatio  
from  @temp4  

go 
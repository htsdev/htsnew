﻿ 

/****** Object:  StoredProcedure [dbo].[USP_HTP_GetSummaryReport]    Script Date: 08/01/2008 16:50:37 ******/

 -- Noufil 4432 08/05/2008 html serial number added added for validation report
/** 
	Business Logic : This procedure simply return the summary record from table
	Parameter :
				@report : specify the report type... 1 for alert and 2 for pending reports
	column return:
				Reporttitle
				alertcount
				pendingcount
				totalcount

**/

ALTER procedure [dbo].[USP_HTP_GetSummaryReport]
@report VARCHAR(50) = '1',
@ReportIds VARCHAR(MAX)='',		--SAEED 7791 06/30/2010 comma seperated report ids.
@SessionId VARCHAR(500)='' --SAEED 7791 07/24/2010 uniquley identified session id.
as 
SELECT id, Reporttitle as [Report Name],alertcount as [Alert],pendingcount as [Pending],totalcount as [Total]
  from summaryreport 
-- SAEED 7791 06/30/2010 place a where clause. If @reportsIds is '' then it will return all records otherwise it will return records which has the reportids in parameter @ReportIds for current user sessionid
where (@report <> '' AND report_type = @report AND sessionid=@SessionId ) OR (@ReportIds <> '' AND isscheduled=1 AND sessionid= @SessionId AND reportid in (select svalue from dbo.splitstring(@ReportIds, ',')))

﻿ /**  
* Nasir 5864 06/25/2009  
* Business Logic: This procedure is use to get data for ALR Sub Peona Letter
* 
* List of parameter:
* First Name, 
* Last name,
* Case number,
* AttorenyFirstName,
* AttorneyLastName,
* ALROfficerAddress
* Court Date
* Court Number
* Firm Address

**/    
  
  
alter PROCEDURE dbo.USP_HTP_Get_ALR_Subpeona_Letter
 @TicketID INT  
AS  
 SELECT distinct tt.Firstname  
       ,tt.Lastname  
       ,CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end AS CaseNumber
       ,tt.ALROfficerName
       ,tt.ALROfficerAddress
	   ,tt.ALROfficerCity
       ,(SELECT State FROM tblState  WHERE StateID = tt.ALROfficerStateID) AS ALROfficerstate
       ,tt.ALROfficerZip
       ,ttv.CourtDateMain
       ,ttv.CourtNumbermain
       ,tc.[Address] AS CourtAddress
       ,tc.City AS CourtCity
       ,(SELECT State FROM tblState  WHERE StateID = tc.[State]) as CourtState
       ,tc.Zip AS courtzip
       ,tf.AttorenyFirstName as AttorenyFirstName         
       ,tf.AttorneyLastName as AttorneyLastName  
       ,tf.[Address] AS FirmAddress
       ,tf.City AS FirmCity
       ,(SELECT State FROM tblState  WHERE StateID = tf.[State]) AS Firmstate
       ,tf.Zip    AS FirmZip
       --Waqas 6342 08/13/2009 observing officer
       ,ISNULL(tt.IsALRArrestingObservingSame, '0') AS IsALRArrestingObservingSame
       ,tt.ALRObservingOfficerName
       ,tt.ALRObservingOfficerAddress
	   ,tt.ALRObservingOfficerCity
       ,(SELECT State FROM tblState  WHERE StateID = tt.ALRObservingOfficerState) AS ALRObservingOfficerstate
       ,tt.ALRObservingOfficerZip   
                
 FROM   tblTickets tt  
        INNER JOIN tblTicketsViolations ttv  
             ON  ttv.TicketID_PK = tt.TicketID_PK  
        INNER JOIN tblFirm tf  
             ON  tt.CoveringFirmID = tf.FirmID  
        INNER JOIN tblCourts tc
			ON ttv.CourtID=tc.Courtid
 WHERE tt.TicketID_PK=@TicketID
 AND ttv.ViolationNumber_PK=16159
 
 
 GO
 GRANT EXECUTE ON dbo.USP_HTP_Get_ALR_Subpeona_Letter TO dbr_webuser
 GO
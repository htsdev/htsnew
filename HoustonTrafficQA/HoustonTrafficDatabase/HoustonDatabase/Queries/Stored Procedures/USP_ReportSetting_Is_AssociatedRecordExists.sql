/************************************************************
 * Code formatted by SoftTree SQL Assistant © v4.0.34
 * Time: 9/27/2010 12:35:57 PM
 ************************************************************/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  
* Created By		: SAEED AHMED
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to check weather report setting attributes already been set for the report which is passing as an input parameter.
*					  If report setting attribute is already set this sp will return true otherwise it will return false 
*					: 
* Parameter List  
* @ReportID			: Report ID
* @AttributeId		: Attribute ID
* 
* USP_ReportSetting_Is_AssociatedRecordExists 52,1
*/
CREATE PROC [dbo].[USP_ReportSetting_Is_AssociatedRecordExists]
@ReportID INT,
@AttributeId INT
AS


IF EXISTS(
       SELECT rs.ReportSettingId
       FROM   tblCourts tc
              INNER  JOIN ReportSetting rs
                   ON  tc.Courtid = rs.CourtID
       WHERE  rs.AttributeId = @AttributeId
              AND rs.ReportID = @ReportID
   )
BEGIN
    SELECT 'true'
END
ELSE
BEGIN
    SELECT 'false'
END
GO

GRANT EXECUTE ON [dbo].[USP_ReportSetting_Is_AssociatedRecordExists] TO 
dbr_webuser
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_msmq_insertmessageret]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_msmq_insertmessageret]
GO


CREATE procedure usp_msmq_insertmessageret  
  
@Batchid int,  
@Reset  varchar(100),  
@CauseNo varchar(50),  
@Message varchar(200),  
@ticketid int  
  
as  
  
insert into tbl_webscan_msmqlog(batchid,message,msgdate,scanstatus,reset,causeno,ticketid,trialstatus)  
values(@Batchid,@Message,getdate(),'Sending',@Reset,@CauseNo,@ticketid,'Sending')  
  
select Scope_Identity() as id  
  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


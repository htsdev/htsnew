SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_Mailer_Get_ListDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_Mailer_Get_ListDate]
GO




CREATE procedure [dbo].[USP_Mailer_Get_ListDate]    
    
    
as    
    
declare @temp table (crtdate datetime)

insert into @temp     
select distinct  convert(varchar(10),listdate,101) 
from tblticketsarchive    
where Clientflag = 0  
    

select convert(varchar(10), crtdate,101) as listdate from @temp order by crtdate desc
    
    
    
    
  








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


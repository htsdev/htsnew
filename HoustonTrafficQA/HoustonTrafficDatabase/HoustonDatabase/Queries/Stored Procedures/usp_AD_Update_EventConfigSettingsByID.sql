/****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The Procedure is used to update event config settings by event id.	
				it'll also made an entry in event config setting history 
				
List of Parameters:	
	@EventTypeID	
	@EventTypeDays	
	@CourtID	
	@SettingDateRange	
	@EventState	
	@DialTime	
	@EmployeeID	
	
*******/

CREATE procedure dbo.usp_AD_Update_EventConfigSettingsByID

@EventTypeID	tinyint, 
@EventTypeDays	tinyint, 
@CourtID	int, 
@SettingDateRange	tinyint, 
@EventState	bit, 
@DialTime	datetime,
@EmployeeID	int

as

declare @EventTypeDays_old	tinyint, 
@CourtID_old	int, 
@SettingDateRange_old	tinyint, 
@EventState_old	bit, 
@DialTime_old	datetime,
@Notes varchar(500)

set @Notes=''

select 		
	@EventTypeDays_old=EventTypeDays,
	@CourtID_old=CourtID,
	@SettingDateRange_old=SettingDateRange,
	@EventState_old=EventState,
	@DialTime_old=DialTime	
from AutoDialerEventConfigSettings
where EventTypeID=@EventTypeID

update AutoDialerEventConfigSettings
set EventTypeDays=@EventTypeDays,
	CourtID=@CourtID,
	SettingDateRange=@SettingDateRange,
	EventState=@EventState,
	DialTime=@DialTime,
	LastUpdateDateTime=GetDate(),
	LastUpdatedBy=@EmployeeID
where EventTypeID=@EventTypeID

if(@EventTypeDays_old<>@EventTypeDays)
begin
	set @Notes='Call Days Changed From "'+convert(varchar,@EventTypeDays_old)+'" to "'+convert(varchar,@EventTypeDays)+'"'
	exec usp_AD_Insert_ConfigSettingsHistory @EventTypeID,@EmployeeID,@Notes
end

if(@CourtID_old<>@CourtID)
begin
	declare @CourtName_old varchar(50),
	@CourtName varchar(50)

	if (@CourtID_old=0)
		set @CourtName_old='All Courts'
	else if (@CourtID_old=1)
		set @CourtName_old='HMC Courts'
	else if (@CourtID_old=2)
		set @CourtName_old='Non HMC Courts'
	else
		select @CourtName_old=shortname from tblcourts where courtid=@CourtID_old

	if (@CourtID=0)
		set @CourtName='All Courts'
	else if (@CourtID=1)
		set @CourtName='HMC Courts'
	else if (@CourtID=2)
		set @CourtName='Non HMC Courts'
	else
		select @CourtName=shortname from tblcourts where courtid=@CourtID_old

	set @Notes='Court Changed From "'+@CourtName_old+'" to "'+@CourtName+'"'
	exec usp_AD_Insert_ConfigSettingsHistory @EventTypeID,@EmployeeID,@Notes
end

if(@SettingDateRange_old<>@SettingDateRange)
begin
	set @Notes='Setting Date Range Changed From "'+convert(varchar,@SettingDateRange_old)+'" to "'+convert(varchar,@SettingDateRange)+'"'
	exec usp_AD_Insert_ConfigSettingsHistory @EventTypeID,@EmployeeID,@Notes
end

if(@EventState_old<>@EventState)
begin
	set @Notes='Event State Changed From '+case when @EventState_old=1 then '"Start"' else '"Stop"' end +' to '+case when @EventState=1 then '"Start"' else '"Stop"' end
	exec usp_AD_Insert_ConfigSettingsHistory @EventTypeID,@EmployeeID,@Notes
end

if(convert(varchar,@DialTime_old,108)<>convert(varchar,@DialTime,108))
begin
	set @Notes='Dial Time Changed From "'+convert(varchar,@DialTime_old,108)+'" to "'+convert(varchar,@DialTime,108)+'"'
	exec usp_AD_Insert_ConfigSettingsHistory @EventTypeID,@EmployeeID,@Notes
end

go

grant exec on dbo.usp_AD_Update_EventConfigSettingsByID to dbr_webuser
go
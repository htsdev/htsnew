set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*********

Created By		: Zeeshan Ahmed

Business Logic	: This Procedure is used to get list of child custody case type.

List Of Columns :

		CaseTypeID 
		CaseTypeName

*********/

Create PROCEDURE [dbo].[USP_CC_GET_CaseType]
AS


SELECT * FROM [dbo].[CaseType]
Order by CaseTypeID desc 



Go

grant execute on [dbo].[USP_CC_GET_CaseType] to dbr_webuser

Go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_RPT_GET_PercentageOfQuoteClientsToViolCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_RPT_GET_PercentageOfQuoteClientsToViolCount]
GO


create procedure dbo.USP_RPT_GET_PercentageOfQuoteClientsToViolCount
	(
	@sDate datetime,	
	@eDate datetime
	)

as

--set @sDate = '01/01/2006'
--set @eDate = '12/31/2006'

select @edate = @edate + '23:59:59.999'

declare @totClients int
declare @totQouteClients int

declare @temp table (ticketid int)
declare @tempQoutes table (ticketid int)

declare @tmp table (ticketid_pk int , violcount int)
declare @tmp2 table (violcount int, casecount int)
declare @tmp3 table (violcount varchar(10), casecount int)

declare @tmpQ table (ticketid_pk int , violcount int)
declare @tmp2Q table (violcount int, casecount int)
declare @tmp3Q table (violcount varchar(10), casecount int)


insert into @temp
select distinct ticketid from tblticketspayment
where paymentvoid = 0
and recdate between @sdate and @edate
group by ticketid
having sum(chargeamount) > 0


insert into @tempQoutes
select ticketid_pk from tbltickets where recdate between @sdate and @edate and  activeflag = 0 

select @totClients = count(ticketid) from @temp
select @totQouteClients = count(ticketid) from @tempQoutes


insert into @tmp
select t.ticketid_pk, count(v.ticketsviolationid) as ViolCount
from tbltickets t, tblticketsviolations v, @temp a
where t.ticketid_pk = v.ticketid_pk
and t.ticketid_pk = a.ticketid
and t.activeflag = 1
group by t.ticketid_pk
order by count(v.ticketsviolationid) desc


insert into @tmpQ
select t.ticketid_pk, count(v.ticketsviolationid) as ViolCount
from tbltickets t, tblticketsviolations v, @tempQoutes a
where t.ticketid_pk = v.ticketid_pk
and t.ticketid_pk = a.ticketid
and t.activeflag = 0
group by t.ticketid_pk
order by count(v.ticketsviolationid) desc




insert into @tmp2 
select violcount, count(ticketid_pk)
from @tmp
group by violcount

insert into @tmp3
select violcount, casecount
from @tmp2
where violcount < 9

insert into @tmp3
select '9+'  , sum(casecount)
from @tmp2
where violcount > 8



--=====================
insert into @tmp2Q
select violcount, count(ticketid_pk)
from @tmpQ
group by violcount

insert into @tmp3Q
select violcount, casecount
from @tmp2Q
where violcount < 9

insert into @tmp3Q
select '9+'  , sum(casecount)
from @tmp2Q
where violcount > 8
--=====================

if @totClients > 0 
	select	c.violcount as [Violation Count], 
			c.casecount + q.casecount as [Sum of Qoute and Client],
			c.casecount as [Client Count],
			q.casecount as [Qoute Client Count],
			convert( numeric(15,2) ,
round ((convert(numeric(15,2), c.casecount) 
	/ (convert(numeric(15,2), c.casecount + q.casecount))) * 100  ,2)) as [% of Client Hiring]
	from	@tmp3 C, @tmp3Q Q 
		where c.violcount = q.violcount 
else
	select 'No Record Found.'
	



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Get_SearchCaseInfo_Ver_3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Get_SearchCaseInfo_Ver_3]
GO





  
--  USP_HTS_Get_SearchCaseInfo_Ver_3 2, '' ,   '37573' , 9    ,'' , 9, '',9      
CREATE procedure [dbo].[USP_HTS_Get_SearchCaseInfo_Ver_3]  
(         
 @FilterType int= 0,  -- 0= Client, 1=Quote, 2=NonClient, 3=Archive, 4=JIMS          
 @TicketNumber  varchar(25)='',          
 @SearchKeyWord1 varchar(50)='',           
 @SearchKeyType1 int=null,  -- 0= ticketno, 1= lastname, 2=firstname, 3=midnum, 4=dob, 5=phone no, 6=court dt, 7=drv. license, 8=contact dt. , 9=RecordId, 10=Cause No.           
 @SearchKeyWord2 varchar(50)='',          
 @SearchKeyType2 int=null,   -- 0= ticketno, 1= lastname, 2=firstname, 3=midnum, 4=dob, 5=phone no, 6=court dt, 7=drv. license, 8=contact dt. , 9=RecordId, 10=Cause No.          
 --- Added by Asghar for frmMainNew page  
 @SearchKeyWord3 varchar(50)='',          
 @SearchKeyType3 int=null   -- 0= ticketno, 1= lastname, 2=firstname, 3=midnum, 4=dob, 5=phone no, 6=court dt, 7=drv. license, 8=contact dt. , 9=RecordId, 10=Cause No.          
)          
          
as          
          
SET NOCOUNT ON  -- Do not show number of rows returned by the query.          
SET ROWCOUNT 250 -- Limiting the output of the query to 250 records.          
          
declare @strSQL  nvarchar(max),       
 @strSQL2 nvarchar(max),         
 @strWhere nvarchar(max),          
 @strParamDef nvarchar(1000) ,      
 @isDefault bit,      
 @tempDate datetime      
      
-- PROCEDURE CALLED WITHOUT ANY PARAMETER THAN SETTING THE DEFAULT FLAG....      
if (@filtertype = 0 and @ticketnumber ='' and @SearchKeyWord1 = '' and @SearchKeyWord2 = '' and @SearchKeyWord3 ='')      
 set @isDefault = 1        
      
-- FORMATTING TICKET NUMBER......      
if @TicketNumber <> ''      
 set @TicketNumber = left(@TicketNumber, len(@TicketNumber) - 1) + '%'      
      
if  ( @SearchKeyType1 = 0 and @SearchKeyWord1 <> '')      
 set @SearchKeyWord1 = left(@SearchKeyWord1, len(@SearchKeyWord1) - 1) + '%'       
      
if  ( @SearchKeyType2 = 0 and @SearchKeyWord2 <> '')      
 set @SearchKeyWord2 = left(@SearchKeyWord2, len(@SearchKeyWord2) - 1) + '%'       
  
if  ( @SearchKeyType3 = 0 and @SearchKeyWord3 <> '')      
 set @SearchKeyWord3 = left(@SearchKeyWord3, len(@SearchKeyWord3) - 1) + '%'       
      
      
-- FORMATTING MIDNUMBER IF SEARCHING BY MID NUMBER.......      
if ( @SearchKeyType1 = 3 and @SearchKeyWord1 <> '' )      
      
 set @SearchKeyWord1 = '%' + @SearchKeyWord1       
      
if ( @SearchKeyType2 = 3 and @SearchKeyWord2 <> '' )      
 set @SearchKeyWord2 = '%' + @SearchKeyWord2       
      
 if ( @SearchKeyType3 = 3 and @SearchKeyWord3 <> '' )      
 set @SearchKeyWord3 = '%' + @SearchKeyWord3       
    
-- FORMATTING CAUSE NUMBER INPUT IF SEARCHING BY CAUSE NUMBER OF SearchKeyWord1.............................................................      
if ( @SearchKeyType1 = 10 and @SearchKeyWord1 <> '' )      
  begin    
    
 -- REMOVING SPACES FROM THE SEARCH KEY WORD....    
 set @SearchKeyWord1 = replace(@SearchKeyWord1, ' ', '')    
 declare @temp1 varchar(4),    
  @temp2 varchar(30)    
 set @temp1 = left(@SearchKeyWord1,4)    
    
 -- IF NOT A VALID YEAR THEN GET FIRST TWO CHARACTERS FROM THE LEFT FOR YEAR.....    
 if isnumeric(@temp1) = 0    
     begin    
  set @temp1 = left(@SearchKeyWord1,2)    
  set @temp2 = substring(@SearchKeyWord1, 3, len(@SearchKeyWord1) )    
    end    
     
 -- ELSE GET THE REMAINING PART OF THE CAUSE NUMBER IN TEMP. VARIABLE....    
 else    
    begin    
  set @temp2 = substring(@SearchKeyWord1, 5 , len(@SearchKeyWord1) )    
    end    
    
 set @SearchKeyWord1 = '%' + @temp1 + @temp2 + '%'    
  end    
-- FORMATTING CAUSE NUMBER INPUT IF SEARCHING BY CAUSE NUMBER  OF SearchKeyWord2.............................................................          
if ( @SearchKeyType2 = 10 and @SearchKeyWord2 <> '' )      
  begin    
 -- REMOVING SPACES FROM THE SEARCH KEY WORD....    
 set @SearchKeyWord2 = replace(@SearchKeyWord2, ' ', '')    
 declare @temp3 varchar(4),    
  @temp4 varchar(30)    
    
 -- FIRST GETTING FOUR CHARACTERS FROM LEFT FOR YEAR...    
 set @temp3 = left(@SearchKeyWord2,4)    
    
 -- IF NOT A VALID YEAR THEN GET FIRST TWO CHARACTERS FROM THE LEFT FOR YEAR.....    
 if isnumeric(@temp3) = 0    
     begin    
  set @temp3 = left(@SearchKeyWord2,2)    
  set @temp4 = substring(@SearchKeyWord2, 3, len(@SearchKeyWord2) )    
    end    
    
 -- ELSE GET THE REMAINING PART OF THE CAUSE NUMBER IN TEMP. VARIABLE....    
 else    
    begin    
  set @temp4 = substring(@SearchKeyWord2, 5 , len(@SearchKeyWord2) )    
    end    
    
 set @SearchKeyWord2 = '%' + @temp3 + @temp4 + '%'    
  end    
    
    
-- FORMATTING CAUSE NUMBER INPUT IF SEARCHING BY CAUSE NUMBER  OF SearchKeyWord3.............................................................          
if ( @SearchKeyType3 = 10 and @SearchKeyWord3 <> '' )      
  begin    
 -- REMOVING SPACES FROM THE SEARCH KEY WORD....    
 set @SearchKeyWord3 = replace(@SearchKeyWord3, ' ', '')    
 declare @temp5 varchar(4),    
  @temp6 varchar(30)    
    
 -- FIRST GETTING FOUR CHARACTERS FROM LEFT FOR YEAR...    
 set @temp5 = left(@SearchKeyWord3,4)    
    
 -- IF NOT A VALID YEAR THEN GET FIRST TWO CHARACTERS FROM THE LEFT FOR YEAR.....    
 if isnumeric(@temp5) = 0    
     begin    
  set @temp5 = left(@SearchKeyWord3,2)    
  set @temp6 = substring(@SearchKeyWord3, 3, len(@SearchKeyWord3) )    
    end    
    
 -- ELSE GET THE REMAINING PART OF THE CAUSE NUMBER IN TEMP. VARIABLE....    
 else    
    begin    
  set @temp6 = substring(@SearchKeyWord3, 5 , len(@SearchKeyWord3) )    
    end    
    
 set @SearchKeyWord3 = '%' + @temp5 + @temp6 + '%'    
  end    
  
  
    
-- FORMATTING INPUT DATA........................      
if not ( @SearchKeyType1 = 4 or @SearchKeyType1 = 6 or @SearchKeyType1 = 8 or @SearchKeyType1 = 9 )      
 if  @SearchKeyWord1 <> ''      
  select @SearchKeyWord1 = @SearchKeyWord1 + '%'      
      
if not ( @SearchKeyType2 = 4 or @SearchKeyType2 = 6 or @SearchKeyType2 = 8 or @SearchKeyType2 = 9 )      
 if  @SearchKeyWord2 <> ''      
  select @SearchKeyWord2 = @SearchKeyWord2 + '%'      
      
if not ( @SearchKeyType3 = 4 or @SearchKeyType3 = 6 or @SearchKeyType3 = 8 or @SearchKeyType3 = 9 )      
 if  @SearchKeyWord3 <> ''      
  select @SearchKeyWord3 = @SearchKeyWord3 + '%'     
  
-- FORMATTING INPUT DATA IF SEARCHING BY A DATE FIELD....      
if (@SearchKeyType1 = 6 or @SearchKeyType1 =8 or @SearchKeyType1 =4 )        
 begin      
  set @tempDate = @SearchKeyWord1      
  if isdate(@tempDate) = 1      
   select @SearchKeyWord1 = convert(varchar(10),@tempDate,101)  
 end       
      
if (@SearchKeyType2 = 6 or @SearchKeyType2 =8  or @SearchKeyType2 =4 )       
 begin      
  set @tempDate = @SearchKeyWord2      
  if isdate(@tempDate)= 1      
   select @SearchKeyWord2 = convert(varchar(10),@tempDate,101)  
 end      
   
if (@SearchKeyType3 = 6 or @SearchKeyType3 =8  or @SearchKeyType3 =4 )       
 begin      
  set @tempDate = @SearchKeyWord3      
  if isdate(@tempDate)= 1      
   select @SearchKeyWord3 = convert(varchar(10),@tempDate,101)  
 end         
  
-- IF SEARCHING BY RECORD ID  
-- GET ALL MAILER IDs IN TEMP. TABLE  
create table #MailerIDs (noteid int, recordid int)  
DECLARE @searchpattern int set @searchPattern  = 0  
  
-- GET MAILER IDS FROM TRAFFIC TICKETS IF SEARCHING BY CLIENT/QUOTE/NON-CLIENT/ARCHIVE  
if @FilterType in (0,1,2,4)  
BEGIN  
 if @SearchKeyType1 = 9 and @SearchKeyWord1 <> ''  
  insert into #MailerIDs (noteid, recordid)  
  select n.noteid, n.recordid from tblletternotes n, tblbatchletter b  
  where n.batchid_fk = b.batchid and b.courtid not in (6,11) and  noteid =  @SearchKeyWord1  
  
 if @SearchKeyType2 = 9 and @SearchKeyWord2 <> ''  
  insert into #MailerIDs (noteid, recordid)  
  select n.noteid, n.recordid from tblletternotes n, tblbatchletter b  
  where n.batchid_fk = b.batchid and b.courtid not in (6,11) and  noteid =  @SearchKeyWord2  
  
 if @SearchKeyType3 = 9 and @SearchKeyWord3 <> ''  
  insert into #MailerIDs (noteid, recordid)  
  select n.noteid, n.recordid from tblletternotes n, tblbatchletter b  
  where n.batchid_fk = b.batchid and b.courtid not in (6,11) and  noteid =  @SearchKeyWord3  
END  
  
---- GET MAILER IDS FROM JIMS IF SEARCHING BY JIMS  
--if @FilterType =4  
--BEGIN  
-- if ( @SearchKeyType1 = 9 and @SearchKeyWord1 <> '''' )  
--  insert into #MailerIDs (noteid, recordid)  
--  select n.noteid, n.recordid from tblletternotes n, tblbatchletter b  
--  where n.batchid_fk = b.batchid and b.courtid = 8 and  noteid =  @SearchKeyWord1  
--   
-- if( @SearchKeyType2 = 9 and @SearchKeyWord2 <> '''')  
--  insert into #MailerIDs (noteid, recordid)  
--  select n.noteid, n.recordid from tblletternotes n, tblbatchletter b  
--  where n.batchid_fk = b.batchid and b.courtid = 8 and  noteid =  @SearchKeyWord2  
--   
-- if (@SearchKeyType3 = 9 and @SearchKeyWord3 <> '''')  
--  insert into #MailerIDs (noteid, recordid)  
--  select n.noteid, n.recordid from tblletternotes n, tblbatchletter b  
--  where n.batchid_fk = b.batchid and b.courtid = 8 and  noteid =  @SearchKeyWord3  
--END  
--  
  
      
-- CREATING TEMPORARY TABLE FOR STORING THE SEARCH RESULTS AND WILL BE USED TO GROUP RECORDS ON TICKETNUMBER.....      
set @strSQL ='declare @tempResult table (lastname varchar(50), firstname varchar(50), causenumber varchar(30), ticketnumber varchar(25), sequenceno varchar(5),      
 address1 varchar(200), zipcode varchar(20), address  varchar(200), midno varchar(20), dob datetime, courtdate datetime, casestatus varchar(50),      
 ticketid varchar(20), courtid int, MailerIDFlag int, dbid int )      
insert into @tempResult'      
      
          
set @strWhere=''          
set @strParamDef = '@FilterType int, @TicketNumber varchar(25), @SearchKeyWord1 varchar(50),           
   @SearchKeyType1 int, @SearchKeyWord2 varchar(50), @SearchKeyType2 int, @SearchKeyWord3 varchar(50), @SearchKeyType3 int'            
          
          
--------------------------------------------------------------------------------------------------          
--    CLIENTS / QUOTE          
--------------------------------------------------------------------------------------------------          
if (@FilterType = 0 OR @FilterType =1)          
 begin          
  set  @strSQL=@strSQL + '        
  SELECT t.Lastname,           
   t.Firstname,           
   v.casenumassignedbycourt,  
   v.RefCaseNumber ,      
   convert(varchar(10),isnull(v.SequenceNumber,0)),           
   t.address1 as address1,      
   t.zip as zipcode,      
   t.Address1+'' ''+isnull(t.Address2,'''') as address,           
   t.midnum as MIDNo,          
   convert(varchar(10),t.DOB,101) as DOB,           
   convert(varchar(10), isnull(v.CourtDatemain,''01/01/1900'') ,101) as CourtDate,           
--   isnull(d.Description,''Not Set'') as CaseStatus,          
   isnull(c.shortDescription,''N/A'') as CaseStatus,          
   t.ticketid_pk as TicketId,          
   t.currentcourtloc as CourtId,  
 0 as MailerIDFlag   ,0 as dbid   
  FROM    dbo.tblDateType d           
  INNER JOIN          
   dbo.tblCourtViolationStatus c           
  ON  d.TypeID = c.CategoryID           
  RIGHT OUTER JOIN          
   dbo.tblTickets t           
  INNER JOIN          
   dbo.tblTicketsViolations v           
  ON  t.TicketID_PK = v.TicketID_PK           
  ON  c.CourtViolationStatusID = v.CourtViolationStatusIDmain
  --Added By Ozair, not show cases of violation type 1
  LEFT OUTER JOIN
	dbo.tblViolations vio
  ON  v.violationnumber_pk=vio.violationnumber_pk	           
  --
  where 1=1 
	and vio.violationtype<>1         --Added by Ozair
  '          
            
if @isDefault = 1       
 set @strSQL=@strSQL+ ' and t.calculatedtotalfee > 0  '          
      
      
if @FilterType = 0           
 set @strSQL=@strSQL+ ' and t.activeflag=1  '          
else          
 set @strSQL=@strSQL+ ' and t.activeflag=0'          
          
            
-- SEARCHING WITH TICKETNUMBER.........      
if (@TicketNumber <>'' )          
 set @strWhere = @strWhere +' and v.refcasenumber like @TicketNumber'          
      
-- SEARCHING WITH ONE KEY WORD......      
if (@SearchKeyType1=0 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and v.refcasenumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=1 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord1'          
      
if (@SearchKeyType1=2 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere +' and t.firstname like @SearchKeyWord1'           
      
if (@SearchKeyType1=3 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.midnum like @SearchKeyWord1'          
      
if (@SearchKeyType1=4 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere +' and convert(varchar(10),t.dob,101)  = @SearchKeyWord1'          
      
if (@SearchKeyType1=5 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and ( t.contact1 like @SearchKeyWord1 or  t.contact2 like @SearchKeyWord1 or t.contact3 like @SearchKeyWord1  )'          
      
if (@SearchKeyType1=6 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and convert(varchar(10),v.courtdatemain,101) = @SearchKeyWord1'          
      
if (@SearchKeyType1=7 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=8 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and convert(varchar(10),t.contactdate,101) = @SearchKeyWord1'          
    
if (@SearchKeyType1=9 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(int, @SearchKeyWord1)'          
   --set @strWhere = @strWhere + ' and v.refcasenumber =  @SearchKeyWord1'  
    
      
if (@SearchKeyType1=10 and @SearchKeyWord1 <>'')          
-- set @strWhere = @strWhere + ' and v.CaseNumAssignedByCourt like ''%''+  @SearchKeyWord1 + ''%'''          
 set @strWhere = @strWhere + ' and v.CaseNumAssignedByCourt like   @SearchKeyWord1 '    
       
      
-- SEARCHING WITH 2ND KEY WORD.........      
if (@SearchKeyType2=0 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and v.refcasenumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=1 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.lastname LIKE @SearchKeyWord2'          
      
if (@SearchKeyType2=2 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and t.firstname like @SearchKeyWord2'          
      
if (@SearchKeyType2=3 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and t.midnum like @SearchKeyWord2'          
      
if (@SearchKeyType2=4 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord2'          
      
if (@SearchKeyType2=5 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and ( t.contact1 like @SearchKeyWord2 or  t.contact2 like @SearchKeyWord2 or t.contact3 like @SearchKeyWord2  )'          
      
if (@SearchKeyType2=6 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdatemain,101) like @SearchKeyWord2'          
      
if (@SearchKeyType2=7 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.dlnumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=8 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),t.contactdate,101) like @SearchKeyWord2'         
    
if (@SearchKeyType2=9 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(int, @SearchKeyWord2)'          
 --  set @strWhere = @strWhere + ' and v.refcasenumber =  @SearchKeyWord2'  
      
if (@SearchKeyType2=10 and @SearchKeyWord2 <>'')          
-- set @strWhere = @strWhere + ' and v.CaseNumAssignedByCourt like ''%''+  @SearchKeyWord2 + ''%'''          
   set @strWhere = @strWhere + ' and v.CaseNumAssignedByCourt like  @SearchKeyWord2 '          
  
  
-- SEARCHING WITH 3rd KEY WORD.........      
if (@SearchKeyType3=0 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and v.refcasenumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=1 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.lastname LIKE @SearchKeyWord3'          
      
if (@SearchKeyType3=2 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and t.firstname like @SearchKeyWord3'          
      
if (@SearchKeyType3=3 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and t.midnum like @SearchKeyWord3'          
      
if (@SearchKeyType3=4 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord3'          
      
if (@SearchKeyType3=5 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and ( t.contact1 like @SearchKeyWord3 or  t.contact2 like @SearchKeyWord3 or t.contact2 like @SearchKeyWord3  )'          
      
if (@SearchKeyType3=6 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdatemain,101) like @SearchKeyWord3'          
      
if (@SearchKeyType3=7 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.dlnumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=8 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),t.contactdate,101) like @SearchKeyWord3'         
    
if (@SearchKeyType3=9 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(int, @SearchKeyWord3)'          
 --  set @strWhere = @strWhere + ' and v.refcasenumber =  @SearchKeyWord2'  
      
if (@SearchKeyType3=10 and @SearchKeyWord3 <>'')          
-- set @strWhere = @strWhere + ' and v.CaseNumAssignedByCourt like ''%''+  @SearchKeyWord3 + ''%'''          
   set @strWhere = @strWhere + ' and v.CaseNumAssignedByCourt like  @SearchKeyWord3 '   
                
if (@SearchKeyType1 = 9 or @SearchKeyType2 = 9 or @SearchKeyType3 = 9 )  
 set @strWhere = @strWhere + '  
union  
  SELECT t.Lastname,           
   t.Firstname,           
   v.casenumassignedbycourt,  
   v.RefCaseNumber ,      
   convert(varchar(10),isnull(v.SequenceNumber,0)),           
   t.address1 as address1,      
   t.zip as zipcode,      
   t.Address1+'' ''+isnull(t.Address2,'''') as address,           
   t.midnum as MIDNo,          
   convert(varchar(10),t.DOB,101) as DOB,           
   convert(varchar(10), isnull(v.CourtDatemain,''01/01/1900'') ,101) as CourtDate,           
   isnull(c.shortDescription,''N/A'') as CaseStatus,          
   t.ticketid_pk as TicketId,          
   t.currentcourtloc as CourtId,  
 1 as MailerIDFlag   ,0 as dbid     
  FROM    dbo.tblDateType d           
  INNER JOIN          
   dbo.tblCourtViolationStatus c           
  ON  d.TypeID = c.CategoryID           
  RIGHT OUTER JOIN          
   dbo.tblTickets t           
  INNER JOIN          
   dbo.tblTicketsViolations v           
  ON  t.TicketID_PK = v.TicketID_PK           
  ON  c.CourtViolationStatusID = v.CourtViolationStatusIDmain
  --Added By Ozair, not get cases of violation type 1
  LEFT OUTER JOIN
	dbo.tblViolations vio
  ON  v.violationnumber_pk=vio.violationnumber_pk	           
  --
  inner join #mailerids m on convert(int,m.recordid) = v.recordid        
  where 1=1 
	and vio.violationtype<>1         --Added by Ozair    
           
'  
  
if @FilterType = 0           
 set @strWhere=@strWhere+ ' and t.activeflag=1  '          
else          
 set @strWhere=@strWhere+ ' and t.activeflag=0'          
   
end        
    
--------------------------------------------------------------------------------------------------          
--    NON CLIENT          
--------------------------------------------------------------------------------------------------          
if @FilterType=2          
 begin          
  set  @strSQL=@strSQL + '        
  SELECT t.Lastname,           
   t.Firstname,        
   v.causenumber,     
   v.ticketnumber_pk ,      
   convert(varchar(10),v.violationnumber_pk) ,           
   t.address1 as address1,      
   t.zipcode,      
   t.Address1+'' ''+isnull(t.Address2,'''') as address,           
   t.midnumber as MIDNo,          
   convert(varchar(10),t.DOB,101) as DOB,           
   convert(varchar(10), isnull(v.CourtDate,''01/01/1900'')  ,101) as CourtDate,           
--   isnull(d.Description,''Not Set'') as CaseStatus,          
   isnull(c.shortDescription,''N/A'') as CaseStatus,          
   t.ticketnumber as TicketId,          
   t.courtid as CourtId,  
   0 as MailerIDFlag  , 2 as dbid    
 FROM    dbo.tblDateType d           
  INNER JOIN          
   dbo.tblCourtViolationStatus c           
  ON  d.TypeID = c.CategoryID           
  RIGHT OUTER JOIN          
   dbo.tblTicketsarchive t           
  INNER JOIN          
   dbo.tblTicketsViolationsarchive v           
  ON  t.recordid = v.recordid           
  ON  c.CourtViolationStatusID = v.ViolationStatusID
  --Added By Ozair, not get cases of violation type 1
  LEFT OUTER JOIN
	dbo.tblViolations vio
  ON  v.violationnumber_pk=vio.violationnumber_pk	           
  --  
  where t.clientflag=0 
  and vio.violationtype<>1         --Added by Ozair         
  '          
            
-- SEARCHING BY TICKET NUMBER........      
if (@TicketNumber <>'')          
 set @strWhere = @strWhere +' and v.ticketnumber_pk like @TicketNumber'          
      
-- SEARCHING BY 1ST KEY WORD...............      
if (@SearchKeyType1=0 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and v.ticketnumber_pk like @SearchKeyWord1'          
      
if (@SearchKeyType1=1 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord1'          
      
if (@SearchKeyType1=2 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord1'          
      
if (@SearchKeyType1=3 and @SearchKeyWord1 <>'')          
 set @strWhere =  @strWhere +' and t.midnumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=4 and @SearchKeyWord1 <>'')          
 set @strWhere =@strWhere + ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord1'          
      
if (@SearchKeyType1=5 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.phonenumber like @SearchKeyWord1 '          
      
if (@SearchKeyType1=6 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord1'          
      
if (@SearchKeyType1=7 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=9 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(bigint, @SearchKeyWord1)'          
    
if (@SearchKeyType1=10 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and v.causenummber like  @SearchKeyWord1 '          
    
      
      
-- SEARCHING BY 2ND KEY WORD..........      
if (@SearchKeyType2=0 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and v.ticketnumber_pk like @SearchKeyWord2'          
      
if (@SearchKeyType2=1 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.lastname like @SearchKeyWord2'          
      
if (@SearchKeyType2=2 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and t.firstname like @SearchKeyWord2'          
      
if (@SearchKeyType2=3 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and t.midnumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=4 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord2'          
      
if (@SearchKeyType2=5 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.phonenumber like @SearchKeyWord2 '          
      
if (@SearchKeyType2=6 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord2'          
      
if (@SearchKeyType2=7 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.dlnumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=9 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(bigint,@SearchKeyWord2)'          
    
if (@SearchKeyType2=10 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere + ' and v.CauseNumber like   @SearchKeyWord2 '          
      
  
  
-- SEARCHING BY 3rd KEY WORD..........      
if (@SearchKeyType3=0 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and v.ticketnumber_pk like @SearchKeyWord3'          
      
if (@SearchKeyType3=1 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.lastname like @SearchKeyWord3'          
      
if (@SearchKeyType3=2 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and t.firstname like @SearchKeyWord3'          
      
if (@SearchKeyType3=3 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and t.midnumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=4 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord3'          
      
if (@SearchKeyType3=5 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.phonenumber like @SearchKeyWord3 '          
      
if (@SearchKeyType3=6 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord3'          
      
if (@SearchKeyType3=7 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.dlnumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=9 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(bigint,@SearchKeyWord3)'          
    
if (@SearchKeyType3=10 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere + ' and v.CauseNumber like   @SearchKeyWord3 '    
  
  
if (@SearchKeyType1 = 9 or @SearchKeyType2 = 9 or @SearchKeyType3 = 9 )  
 set @strWhere = @strWhere + '  
union  
SELECT t.Lastname,           
   t.Firstname,        
   v.causenumber,     
   v.ticketnumber_pk ,      
   convert(varchar(10),v.violationnumber_pk) ,           
   t.address1 as address1,      
   t.zipcode,      
   t.Address1+'' ''+isnull(t.Address2,'''') as address,           
   t.midnumber as MIDNo,          
   convert(varchar(10),t.DOB,101) as DOB,           
   convert(varchar(10), isnull(v.CourtDate,''01/01/1900'')  ,101) as CourtDate,           
   isnull(c.shortDescription,''N/A'') as CaseStatus,          
   t.ticketnumber as TicketId,          
   t.courtid as CourtId,  
   1 as MailerIDFlag  , 2 as dbid    
 FROM    dbo.tblDateType d           
  INNER JOIN          
   dbo.tblCourtViolationStatus c           
  ON  d.TypeID = c.CategoryID           
  RIGHT OUTER JOIN          
   dbo.tblTicketsarchive t           
  INNER JOIN          
   dbo.tblTicketsViolationsarchive v           
  ON  t.recordid = v.recordid           
  ON  c.CourtViolationStatusID = v.ViolationStatusID 
  --Added By Ozair, not get cases of violation type 1
  LEFT OUTER JOIN
	dbo.tblViolations vio
  ON  v.violationnumber_pk=vio.violationnumber_pk	           
  --  
  inner join #mailerids m on m.recordid = v.recordid  
  where t.clientflag=0
  and vio.violationtype<>1         --Added by Ozair   

union  
  select distinct c.lastname as lastname,  c.firstname as firstname, c.casenumber, C.casenumber as CaseNumber,
   0 , c.address as address1, c.zip as zipcode, C.Address as Address, L.span as MIDNo, convert(varchar(10),DOB,101) as DOB,          
   convert(varchar(10), isnull(l.bookingdate,''01/01/1900'') ,101) as CourtDate, ''Non Client'' as CaseStatus,          
   c.casenumber as TicketId, 3037 as CourtId, 1 as MailerIDFlag , 4 as dbid
from jims.dbo.criminalcases C inner join jims.dbo.Criminal503 L on C.bookingnumber = L.bookingnumber   
 inner join #mailerids m on convert(int,m.recordid) = convert(int,c.bookingnumber) where  c.clientflag=0 
'  
  
  
end          
          
--------------------------------------------------------------------------------------------------          
--    ARCHIVE          
--------------------------------------------------------------------------------------------------          
if @FilterType=3          
 begin          
  set  @strSQL=@strSQL + '       
 SELECT t.Lastname,           
   t.Firstname,           
   v.causenumber,  
   v.ticketnumber_pk ,       
   convert(varchar(10),v.violationnumber_pk) AS CaseNumber,           
   t.address1 as address1,      
   t.zipcode,      
   t.Address1+'' ''+isnull(t.Address2,'''') as address,           
   t.midnumber as MIDNo,          
   convert(varchar(10),t.DOB,101) as DOB,           
   convert(varchar(10), isnull(v.CourtDate,''01/01/1900'') ,101) as CourtDate,           
   ''Non Client'' as CaseStatus,          
   t.ticketnumber as TicketId,          
   t.courtid as CourtId,  
   0 as MailerIDFlag  , 3 as dbid        
  FROM         
   TrafficTicketsArchive.dbo.tblTicketsarchive t           
  INNER JOIN          
   TrafficTicketsArchive.dbo.tblTicketsViolationsarchive v           
  ON  t.Ticketnumber = v.Ticketnumber_PK 
  --Added By Ozair, not get cases of violation type 1
  LEFT OUTER JOIN
	dbo.tblViolations vio
  ON  v.violationnumber_pk=vio.violationnumber_pk	           
  --            
  where t.clientflag=0 
  and vio.violationtype<>1         --Added by Ozair          
  '          
            
-- SEARCHING BY TICKET NUMBER.............      
if ( @TicketNumber <>'')          
 set @strWhere =@strWhere +' and v.ticketnumber_pk like @TicketNumber'          
      
-- SEARCHING BY 1ST KEY WORD........................      
if (@SearchKeyType1=0 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and v.ticketnumber_pk like @SearchKeyWord1'          
      
if (@SearchKeyType1=1 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and t.lastname like @SearchKeyWord1'          
      
if (@SearchKeyType1=2 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and t.firstname like @SearchKeyWord1'          
      
if (@SearchKeyType1=3 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.midnumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=4 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere +' and convert(varchar(10),t.dob,101)  like @SearchKeyWord1'          
      
if (@SearchKeyType1=5 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.phonenumber like @SearchKeyWord1 '          
      
if (@SearchKeyType1=6 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord1'          
      
if (@SearchKeyType1=7 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.dlnumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=9 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and t.recordid = convert(bigint, @SearchKeyWord1)'       
    
if (@SearchKeyType1=10 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and v.CauseNumber like   @SearchKeyWord1 '           
      
-- SEARCHING BY 2ND KEY WORD...................      
if (@SearchKeyType2=0 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and v.ticketnumber_pk like @SearchKeyWord2'          
      
if (@SearchKeyType2=1 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.lastname like @SearchKeyWord2'          
      
if (@SearchKeyType2=2 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and t.firstname like @SearchKeyWord2'          
      
if (@SearchKeyType2=3 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and t.midnumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=4 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord2'          
      
If (@SearchKeyType2=5 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.phonenumber like @SearchKeyWord2 '          
      
if (@SearchKeyType2=6 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord2'          
      
if (@SearchKeyType2=7 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and t.dlnumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=9 and @SearchKeyWord2 <>'')          
 set @strWhere =  ' and t.recordid = convert(bigint,@SearchKeyWord2)'         
    
if (@SearchKeyType2=10 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere + ' and v.CauseNumber like   @SearchKeyWord2 '         
       
   
  
-- SEARCHING BY 3rd KEY WORD...................      
if (@SearchKeyType3=0 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and v.ticketnumber_pk like @SearchKeyWord3'          
      
if (@SearchKeyType3=1 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.lastname like @SearchKeyWord3'          
      
if (@SearchKeyType3=2 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and t.firstname like @SearchKeyWord3'          
      
if (@SearchKeyType3=3 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and t.midnumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=4 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),t.dob,101)  like @SearchKeyWord3'          
      
If (@SearchKeyType3=5 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.phonenumber like @SearchKeyWord3 '          
      
if (@SearchKeyType3=6 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),v.courtdate,101) like @SearchKeyWord3'          
      
if (@SearchKeyType3=7 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and t.dlnumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=9 and @SearchKeyWord3 <>'')          
 set @strWhere =  ' and t.recordid = convert(bigint,@SearchKeyWord3)'         
    
if (@SearchKeyType3=10 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere + ' and v.CauseNumber like   @SearchKeyWord3 '  
  
         
end          
          
--------------------------------------------------------------------------------------------------          
--    JIMS          
--------------------------------------------------------------------------------------------------          
if @FilterType=4          
 begin          
  set  @strSQL=@strSQL + '      
  select distinct           
 c.lastname as lastname,      
 c.firstname as firstname,      
 c.casenumber,  
   C.casenumber as CaseNumber,       
   0 ,          
   c.address as address1,      
   c.zip as zipcode,      
   C.Address as Address,          
   L.span as MIDNo,          
 convert(varchar(10),DOB,101) as DOB,          
   convert(varchar(10), isnull(l.bookingdate,''01/01/1900'') ,101) as CourtDate,          
   ''Non Client'' as CaseStatus,          
   c.casenumber as TicketId,          
   3037 as CourtId,  
 0 as MailerIDFlag  ,4 as dbid      
  from jims.dbo.criminalcases C          
  inner join          
   jims.dbo.Criminal503 L           
  on C.bookingnumber = L.bookingnumber          
  where  c.clientflag=0'          
          
-- SEARCHING BY TICKET NUMBER.......      
if (@TicketNumber <>'')          
 set @strWhere = @strWhere + ' and C.casenumber like @TicketNumber'          
      
-- SEARCHING BY 1ST KEY WORD...................      
if (@SearchKeyType1=0 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and C.casenumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=1 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and c.lastname like @SearchKeyWord1'          
      
if (@SearchKeyType1=2 and @SearchKeyWord1<>'')          
 set @strWhere = @strWhere + ' and c.firstname like @SearchKeyWord1'          
      
if (@SearchKeyType1=3 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and l.span like @SearchKeyWord1'          
      
if (@SearchKeyType1=4 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere +' and convert(varchar(10),c.dob,101)  like @SearchKeyWord1'          
      
if (@SearchKeyType1=6 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),l.bookingdate,101) like @SearchKeyWord1'          
      
if (@SearchKeyType1=7 and @SearchKeyWord1 <>'')          
 set @strWhere =  ' and c.bookingnumber like @SearchKeyWord1'          
      
if (@SearchKeyType1=9 and @SearchKeyWord1 <>'')          
 set @strWhere = @strWhere + ' and C.casenumber like @SearchKeyWord1'          
  
  
  
-- SEARCHING BY 2ND KEY WORD..................      
if (@SearchKeyType2=0 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and C.casenumber like @SearchKeyWord2'          
      
if (@SearchKeyType2=1 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and c.lastname like @SearchKeyWord2'          
      
if (@SearchKeyType2=2 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and c.firstname like @SearchKeyWord2'          
      
if (@SearchKeyType2=3 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and l.span like @SearchKeyWord2'          
      
if (@SearchKeyType2=4 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),c.dob,101)  like @SearchKeyWord2'          
      
if (@SearchKeyType2=6 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),l.bookingdate,101) like @SearchKeyWord2'          
      
if (@SearchKeyType2=7 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and c.bookingnumber like @SearchKeyWord2'          
  
if (@SearchKeyType2=9 and @SearchKeyWord2 <>'')          
 set @strWhere = @strWhere+  ' and C.casenumber like @SearchKeyWord2'          
      
  
  
-- SEARCHING BY 3rd KEY WORD..................      
if (@SearchKeyType3=0 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and C.casenumber like @SearchKeyWord3'          
      
if (@SearchKeyType3=1 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and c.lastname like @SearchKeyWord3'          
      
if (@SearchKeyType3=2 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and c.firstname like @SearchKeyWord3'          
      
if (@SearchKeyType3=3 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and l.span like @SearchKeyWord3'          
      
if (@SearchKeyType3=4 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and convert(varchar(10),c.dob,101)  like @SearchKeyWord3'          
      
if (@SearchKeyType3=6 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+   ' and convert(varchar(10),l.bookingdate,101) like @SearchKeyWord3'          
      
if (@SearchKeyType3=7 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and c.bookingnumber like @SearchKeyWord3'          
  
if (@SearchKeyType3=9 and @SearchKeyWord3 <>'')          
 set @strWhere = @strWhere+  ' and C.casenumber like @SearchKeyWord3'          
  
  
if (@SearchKeyType1 = 9 or @SearchKeyType2 = 9 or @SearchKeyType3 = 9)      
 set @strWhere = @strWhere+  '  
union  
  select distinct           
 c.lastname as lastname,      
 c.firstname as firstname,      
 c.casenumber,  
   C.casenumber as CaseNumber,       
   0 ,          
   c.address as address1,      
   c.zip as zipcode,      
   C.Address as Address,          
   L.span as MIDNo,          
 convert(varchar(10),DOB,101) as DOB,          
   convert(varchar(10), isnull(l.bookingdate,''01/01/1900'') ,101) as CourtDate,          
   ''Non Client'' as CaseStatus,          
   c.casenumber as TicketId,          
   3037 as CourtId,  
 1 as MailerIDFlag   , 4 as dbid       
  from jims.dbo.criminalcases C          
  inner join          
   jims.dbo.Criminal503 L           
  on C.bookingnumber = L.bookingnumber   
 inner join #mailerids m on convert(int,m.recordid) = convert(int,c.bookingnumber)
  where  c.clientflag=0  
  
'  
  
  
end      
      
          
set @strsql2 = '   
select distinct  a.ticketnumber as casenumber,       
 a.lastname as lastname, a.firstname as firstname, causenumber as causenumber,  address1 as address1, zipcode as zipcode, address as address, ltrim(midno) as midno, dob as dob,       
 a.courtdate as courtdate, casestatus as casestatus, ticketid as ticketid, courtid as courtid, MailerIDFlag as MailerIDFlag , dbid      
from  @tempResult a   
--where (a.ticketnumber <> '''' and a.ticketnumber <> ''0'' and a.ticketnumber not like ''000000%'')      
order by [lastname], [firstname], [midno], [casenumber]      

'      
set @strSQL=@strSQL + @strWhere   +  @strsql2      
EXECUTE sp_executesql @strSQL, @strParamDef, @FilterType,  @TicketNumber, @SearchKeyWord1, @SearchKeyType1, @SearchKeyWord2, @SearchKeyType2, @SearchKeyWord3, @SearchKeyType3    

  
  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



 -- Noufil 5691 04/06/2009 
 /**
 * Business Logic : This procedure returns all HMC clients whose court date has been passed 3 days from today's date and their court date must be in business days.
 *					Their court violation status must not be in 
 *						1.DISPOSED
 *						2.WAITING
 *						3.MISSED COURT
 *						4.DAR/MISSING CASE
 *						5.BOND
 *						6.ARRAIGNMENT WAITING			
						7.BOND WAITING
	Parameter : @showall
 **/
-- USP_HTP_Get_PastCourtDateForHMC 0,1
Alter PROCEDURE [dbo].[USP_HTP_Get_PastCourtDateForHMC] 
@showall BIT,
@ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report
AS
	SELECT DISTINCT 
	       tv.TicketID_PK AS ticketid_pk,
	       tv.RefCaseNumber AS ticketnumber,
	       tv.casenumassignedbycourt AS causenumber,
	       t.Firstname AS fname,
	       t.Lastname AS lname,
	       dbo.fn_DateFormat(tv.CourtDateMain, 24, '/', ':', 1) AS courtdatemain,
	       tv.CourtNumbermain AS courtnumber, -- Afaq 8351 10/05/2010 Remove INT conversion.
	       tcvs.ShortDescription AS STATUS,
	       c.ShortName AS shortname,
	       tv.CourtDateMain AS CourtDate,	       
	       t.PastCourtDateFollowUpDate,
	       tv.CourtID,DATEDIFF(DAY,GETDATE(),ISNULL(t.PastCourtDateFollowUpDate,'01/01/1900')) AS ss
	       
	FROM   tblTickets AS t
	       INNER JOIN tblTicketsViolations AS tv
	            ON  t.TicketID_PK = tv.TicketID_PK
	       INNER JOIN tblCourtViolationStatus AS tcvs
	            ON  tv.CourtViolationStatusIDmain = tcvs.CourtViolationStatusID
	       INNER JOIN tblCourts AS c
	            ON  tv.CourtID = c.Courtid
	WHERE  --Ozair 7791 07/24/2010  where clause optimized  
	       (
	               (@showall = 1 OR @ValidationCategory = 2) -- Saeed 7791 07/09/2010 display all records if showAll=1 or validation category is 'Report'
	               OR (
	                      (@showall = 0 OR @ValidationCategory = 1)
	                      AND DATEDIFF(
	                              DAY,
	                              ISNULL(t.PastCourtDateFollowUpDate, '01/01/1900'),
	                              GETDATE()
	                          ) >= 0
	                  ) -- Saeed 7791 07/09/2010 display  if showAll=0 or validation category is 'Alert' then display records which has 'follow up date' null,past or today.
	       )
	       AND (
	               tcvs.CourtViolationStatusID NOT IN (80, 105, 104, 201, 202, 123, 135)
	       )
	       AND (DATEPART(dw, tv.CourtDateMain) NOT IN (7, 1))
	       AND (tv.CourtDateMain < GETDATE() - 3)
	       AND (tv.CourtDateMain >= '01/01/2007')
	       AND c.CourtID IN (3001, 3002, 3003)
	       AND (t.Activeflag = 1)
	ORDER BY
	       t.PastCourtDateFollowUpDate DESC,
	       ticketid_pk-- Adil Aleem 5857 05/28/2009 Include Ticket ID in order by clause

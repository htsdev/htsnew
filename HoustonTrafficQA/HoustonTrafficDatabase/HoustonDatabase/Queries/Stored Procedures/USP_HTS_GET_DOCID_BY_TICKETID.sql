 set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

  
  
--[dbo].[USP_HTS_GET_DOCID_BY_TICKETID]  '12335'  
  
ALTER procedure  [dbo].[USP_HTS_GET_DOCID_BY_TICKETID]  
(  
@TicketID varchar(30)  
)  
  
as  
  
  
SELECT   distinct top 1   sb.ScanBookID AS DOC_ID,                     
  dt.DocType,                     
  sb.UPDATEDATETIME,  '0' as doc_num,                         
  ResetDesc, ' '  AS RefCaseNumber,                     
  '1' AS RecordType,                    
  sb.doccount AS ImageCount,                          
  0 as did,                      
  isnull(sd.DOC_ID,0) as ocrid,                    
  sp.DocExtension as DocExtension,                  
  u.Abbreviation,        
  Events                       
 FROM         dbo.tblScanBook sb                   
 INNER JOIN                  
              dbo.tblDocType dt                   
 ON  sb.DocTypeID = dt.DocTypeID                   
 INNER JOIN                  
        dbo.tblScanPIC sp                   
ON  sb.ScanBookID = sp.ScanBookID                   
INNER JOIN                  
         dbo.tblUsers u                   
ON  sb.EmployeeID = u.EmployeeID                   
LEFT OUTER JOIN                  
        dbo.tblscandata sd                   
ON  sp.DOCID = sd.docnum AND sb.ScanBookID = sd.doc_id    
                  
 where   sb.TicketID =@TicketID   
and sb.DocTypeID In (8,17)  -- Noufil 3999 05/30/2008 for letter of rep
and isnull(sb.isdeleted,0) = 0   
order by sb.updatedatetime desc   
   
  
  
  
  
  

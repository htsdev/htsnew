﻿USE TrafficTickets
GO  
  /******      
Created by:      Fahad Muhammad Qureshi    
Business Logic :    This procedure is used to Get Data for all active client which have Bad email address.    
TaskId:    6429         
List of Parameters: @ShowAll  :Irrespectively Fetch the records from start and end date.    
     @FromDate : Starting date to get records for the report.    
     @ToDate   : Ending date to get records for the report.    
     USP_HTP_Get_BadEmailAdressReport 0,1  
******/    
ALTER PROCEDURE [dbo].[USP_HTP_Get_BadEmailAdressReport]    
(    
    @ShowAll   BIT = 0,    
    @ValidationCategory int=0 -- Saeed 7791 07/09/2010 @ValidationCategory=1 for alert & @ValidationCategory=2 for report  
    --SAEED 7844 06/03/2010, not using from/to date parameter.    
    --@FromDate  DATETIME = '01/01/1900',    
    --@ToDate    DATETIME = '01/01/1900'    
)    
AS    

--Muhammad Nasir 10071 12/27/2012 Added below IF/ ELSE and new conditions in where clause according to the requirements
IF @ValidationCategory = 1
BEGIN
SELECT * FROM   
(  
SELECT DISTINCT 
	tt.TicketID_PK
	,ttf.FlagID
	,isnull(tt.ReviewedEmailStatusID,0) as ReviewedEmailStatus, -- Muhammad Nasir 10071 12/21/2012 replace ReviewedEmailStatus with ReviewedEmailStatusID column
    tt.Firstname,    
    tt.Lastname,   --Muhammad Muneer 8247 9/16/2010 added the sales rep last name      
    tt.Email,  
    -- Babar Ahmad 8247 04/25/2011 changed RepLastName logic to remove duplicate records.  
    -- Babar Ahmad 8247 05/23/2011 deleted employeeID <> 3992 condition to get 'N/A' as RepLastName whose address has not changed.  
    RepLastName = (  
    SELECT TOP 1 CASE WHEN ttn.EmployeeID = 3992 THEN 'N/A' ELSE ISNULL(tu.Lastname,'N/A') END  
		FROM tblusers tu   
        INNER JOIN tblTicketsNotes ttn ON ttn.EmployeeID = tu.EmployeeID  
    AND ttn.TicketID = tt.TicketID_PK   
    WHERE ttn.[Subject] LIKE ('Email Address Added:%')   
    --AND ttn.EmployeeID <> 3992               
    ORDER BY ttn.Recdate DESC  
    ),           
    --ISNULL(tu.Lastname,'N/A') AS RepLastName,    
    dbo.fn_DateFormat(ttv.CourtDateMain, 25, '/', ':', 1) AS CourtDate,    
    HireDate = (    
    SELECT TOP 1 MIN(dbo.fn_DateFormat(ttp.recdate,5,'/',':',1))    
    FROM   tblticketspayment ttp                       
    WHERE  ttp.TicketID = tt.TicketID_PK    
    AND ISNULL(ttp.PaymentVoid, 0) = 0    
    AND ttp.PaymentType <> 8    
    GROUP BY    
    ttp.RecDate    
    ORDER BY    
    ttp.RecDate    
    )  
    -- Afaq 8162 08/17/2010 Remove report type, Email sent Date column    
    ,isnull(CONVERT(VARCHAR(10),tt.BadEmailAddressFollowupDate,101),'')  AS FollowupDate --7844 SAEED 06/01/2010, Followup Date added.    
    -- Babar Ahmad 9676 11/04/2011 Added Contact Number and general comment fields.  
    ,Contact1 = ISNULL(dbo.formatphonenumbers(tt.contact1), '') + ' ' + CASE tc1.ContactType_PK WHEN 0 THEN ' 'ELSE ISNULL(tc1.[Description], '') END,  
    --dbo.fn_FormatPhoneNumber(),  
    Contact2 = ISNULL(dbo.formatphonenumbers(tt.contact2), '') + ' ' + CASE tc2.ContactType_PK WHEN 0 THEN ' ' ELSE ISNULL(tc2.[Description], '') END,  
    Contact3 =ISNULL(dbo.formatphonenumbers(tt.contact3), '') + ' ' + CASE tc3.ContactType_PK WHEN  0 THEN ' ' ELSE ISNULL(tc3.[Description], '') END,  
    tt.GeneralComments  
    -- Babar Ahmad 9676  
    ,ttv.casenumassignedbycourt AS CauseNumber    --7844 SAEED 06/01/2010, CauseNumber field added.    
    ,ttv.RefCaseNumber, --7844 SAEED 06/01/2010, Added refCaseNumber field    
    --Sabir Khan 8247 03/07/2011 Getting max update date.  
    --,MAX(ttn.Recdate) AS Recdate  --Muhammad Muneer 8247 9/16/2010 added the sales rep last name  
    -- Babar Ahmad 8247 05/23/2011 deleted employeeID <> 3992 condition to get recdate.  
    Recdate1 = (  
    SELECT TOP 1 MAX(ttn2.Recdate)    
    FROM tblTicketsNotes ttn2                       
    WHERE  ttn2.TicketID = tt.TicketID_PK  
    AND ttn2.[Subject] LIKE ('Email Address Added:%') --AND ttn2.EmployeeID <> 3992  
    GROUP BY ttn2.TicketID  
  ),  
    
  BadEmailFlagSetCount = (  
     SELECT count(ttn2.NotesID_PK)  
     FROM tblTicketsNotes ttn2                       
     WHERE  ttn2.TicketID = tt.TicketID_PK  
     AND ttn2.[Subject] LIKE ('Flag -%Bad Email%')   
     GROUP BY ttn2.TicketID)  
 FROM tblTickets tt    
      INNER JOIN tblTicketsFlag ttf ON  ttf.TicketID_PK = tt.TicketID_PK    
      INNER JOIN tblTicketsViolations ttv ON  ttv.TicketID_PK = tt.TicketID_PK      
      LEFT OUTER JOIN tblEmailTrialNotification tetn ON  tetn.Ticketid_pk = tt.Ticketid_pk   
      LEFT OUTER JOIN tblContactstype tc1 ON tc1.ContactType_PK = tt.ContactType1  
	  LEFT OUTER JOIN tblContactstype tc2 ON tc2.ContactType_PK = tt.ContactType2  
	  LEFT OUTER JOIN tblContactstype tc3 ON tc3.ContactType_PK = tt.ContactType3   
  WHERE  tt.Activeflag = 1
  AND ttf.FlagID = 34
  AND ISNULL(ttv.CourtViolationStatusIDmain, 0) 
  NOT IN (SELECT courtviolationstatusid FROM tblCourtViolationStatus tcvs WHERE  ISNULL(tcvs.CategoryID, 0) = 50  
  OR tcvs.CourtViolationStatusID = 105)  
  -----Muhammad Nasir 10071 12/27/2012 Updated according to new requirements
  -----By default show those records having status set to PENDING regardless of follow up date
  -----And when <Show All> is ticketd, show all records having status other than Contacted regardless of follow up date
  AND ((isnull(ReviewedEmailStatusID,0) = 0 AND datediff(day,getDate(),isnull(BadEmailAddressFollowupDate,getDate())) <= 0 ) 
       OR (isnull(ReviewedEmailStatusID, 0) NOT IN (1,7,8) AND datediff(day,getDate(),isnull(BadEmailAddressFollowupDate,getDate())) = 0 ))  
) vw  
WHERE vw.BadEmailFlagSetCount >=1               
ORDER BY vw.Recdate1 DESC --Muhammad Muneer 8247 9/16/2010 added the sales rep last name
END

ELSE
BEGIN  
--Nouman 9644 08/20/2011 Inline view is created to hold records and match later against conditions  
SELECT * FROM   
(  
SELECT DISTINCT 
	tt.TicketID_PK
	,ttf.FlagID
	,isnull(tt.ReviewedEmailStatusID,0) as ReviewedEmailStatus, -- Muhammad Nasir 10071 12/21/2012 replace ReviewedEmailStatus with ReviewedEmailStatusID column
    tt.Firstname,    
    tt.Lastname,   --Muhammad Muneer 8247 9/16/2010 added the sales rep last name      
    tt.Email,  
    -- Babar Ahmad 8247 04/25/2011 changed RepLastName logic to remove duplicate records.  
    -- Babar Ahmad 8247 05/23/2011 deleted employeeID <> 3992 condition to get 'N/A' as RepLastName whose address has not changed.  
    RepLastName = (  
    SELECT TOP 1 CASE WHEN ttn.EmployeeID = 3992 THEN 'N/A' ELSE ISNULL(tu.Lastname,'N/A') END  
		FROM tblusers tu   
        INNER JOIN tblTicketsNotes ttn ON ttn.EmployeeID = tu.EmployeeID  
    AND ttn.TicketID = tt.TicketID_PK   
    WHERE ttn.[Subject] LIKE ('Email Address Added:%')   
    --AND ttn.EmployeeID <> 3992               
    ORDER BY ttn.Recdate DESC  
    ),           
    --ISNULL(tu.Lastname,'N/A') AS RepLastName,    
    dbo.fn_DateFormat(ttv.CourtDateMain, 25, '/', ':', 1) AS CourtDate,    
    HireDate = (    
    SELECT TOP 1 MIN(dbo.fn_DateFormat(ttp.recdate,5,'/',':',1))    
    FROM   tblticketspayment ttp                       
    WHERE  ttp.TicketID = tt.TicketID_PK    
    AND ISNULL(ttp.PaymentVoid, 0) = 0    
    AND ttp.PaymentType <> 8    
    GROUP BY    
    ttp.RecDate    
    ORDER BY    
    ttp.RecDate    
    )  
       -- Afaq 8162 08/17/2010 Remove report type, Email sent Date column    
        ,CONVERT(VARCHAR(10),tt.BadEmailAddressFollowupDate,101)  AS FollowupDate --7844 SAEED 06/01/2010, Followup Date added.    
        -- Babar Ahmad 9676 11/04/2011 Added Contact Number and general comment fields.  
        ,Contact1 = ISNULL(dbo.formatphonenumbers(tt.contact1), '') + ' ' + CASE tc1.ContactType_PK WHEN 0 THEN ' 'ELSE ISNULL(tc1.[Description], '') END,  
     --dbo.fn_FormatPhoneNumber(),  
     Contact2 = ISNULL(dbo.formatphonenumbers(tt.contact2), '') + ' ' + CASE tc2.ContactType_PK WHEN 0 THEN ' ' ELSE ISNULL(tc2.[Description], '') END,  
     Contact3 =ISNULL(dbo.formatphonenumbers(tt.contact3), '') + ' ' + CASE tc3.ContactType_PK WHEN  0 THEN ' ' ELSE ISNULL(tc3.[Description], '') END,  
        tt.GeneralComments  
        -- Babar Ahmad 9676  
        ,ttv.casenumassignedbycourt AS CauseNumber    --7844 SAEED 06/01/2010, CauseNumber field added.    
        ,ttv.RefCaseNumber, --7844 SAEED 06/01/2010, Added refCaseNumber field    
        --Sabir Khan 8247 03/07/2011 Getting max update date.  
  --,MAX(ttn.Recdate) AS Recdate  --Muhammad Muneer 8247 9/16/2010 added the sales rep last name  
  -- Babar Ahmad 8247 05/23/2011 deleted employeeID <> 3992 condition to get recdate.  
  Recdate1 = (  
     SELECT TOP 1 MAX(ttn2.Recdate)    
     FROM tblTicketsNotes ttn2                       
     WHERE  ttn2.TicketID = tt.TicketID_PK  
     AND ttn2.[Subject] LIKE ('Email Address Added:%') --AND ttn2.EmployeeID <> 3992  
     GROUP BY ttn2.TicketID  
  ),  
    
  BadEmailFlagSetCount = (  
        SELECT count(ttn2.NotesID_PK)  
     FROM tblTicketsNotes ttn2                       
     WHERE  ttn2.TicketID = tt.TicketID_PK  
     AND ttn2.[Subject] LIKE ('Flag -%Bad Email%')   
     GROUP BY ttn2.TicketID)  
 FROM   tblTickets tt    
        INNER JOIN tblTicketsFlag ttf    
             ON  ttf.TicketID_PK = tt.TicketID_PK    
        INNER JOIN tblTicketsViolations ttv    
             ON  ttv.TicketID_PK = tt.TicketID_PK      
        LEFT OUTER JOIN tblEmailTrialNotification tetn    
             ON  tetn.Ticketid_pk = tt.Ticketid_pk   
        LEFT OUTER JOIN tblContactstype tc1 ON tc1.ContactType_PK = tt.ContactType1  
  LEFT OUTER JOIN tblContactstype tc2 ON tc2.ContactType_PK = tt.ContactType2  
  LEFT OUTER JOIN tblContactstype tc3 ON tc3.ContactType_PK = tt.ContactType3   
  WHERE  tt.Activeflag = 1
  AND ttf.FlagID = 34
  -- Afaq 8431 10/19/2010 Filter out disposed cases.  
  AND ISNULL(ttv.CourtViolationStatusIDmain, 0) 
  NOT IN (SELECT courtviolationstatusid FROM tblCourtViolationStatus tcvs WHERE  ISNULL(tcvs.CategoryID, 0) = 50  
          --Asad Ali 8482 11/05/2010 add filter of "MISSED COURT"   
		  OR tcvs.CourtViolationStatusID = 105)  
  -----Muhammad Nasir 10071 12/27/2012 Updated according to new requirements
  -----By default show those records having status set to PENDING regardless of follow up date
  -----And when <Show All> is ticketd, show all records having status other than Contacted regardless of follow up date
  AND ((isnull(ReviewedEmailStatusID,0) = 0) OR (isnull(ReviewedEmailStatusID, 0) NOT IN (1,7,8) AND convert(varchar,isnull(BadEmailAddressFollowupDate, '1/1/1900'),101) = 
  case when @ShowAll = 1 then convert(varchar,isnull(BadEmailAddressFollowupDate, '1/1/1900'),101) 
	  when @ShowAll = 0 then convert(varchar,getdate(),101)
	  else convert(varchar,getdate(),101) end))  
) vw  
--Nouman 9644 08/20/2011 Select record when bad email flag is set only once  
WHERE vw.BadEmailFlagSetCount >=1               
ORDER BY vw.Recdate1 DESC --Muhammad Muneer 8247 9/16/2010 added the sales rep last name
END



--go
--[USP_HTP_Get_BadEmailAdressReport] 0, 0
--go
--[USP_HTP_Get_BadEmailAdressReport] 1, 0
--go
--[USP_HTP_Get_BadEmailAdressReport] 0, 1

/*
update tblTickets
SET ReviewedEmailStatusID = 2
,BadEmailAddressFollowupDate = getDate()
where ticketID_PK = 441536
*/ 
--select * from tblReminderStatus
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_TicketsContactByTicketID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_TicketsContactByTicketID]
GO

















CREATE PROCEDURE [dbo].[usp_Get_TicketsContactByTicketID] 
@TicketID_PK varchar(20)
AS

Select * from tblTicketsContact 
Where TicketID_PK = @TicketID_PK













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


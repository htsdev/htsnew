SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_hts_get_consultationcomments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_hts_get_consultationcomments]
GO

create procedure usp_hts_get_consultationcomments  
@TicketID int  
as  
SELECT * from tbl_hts_consultationcomments where ticketid_fk = @TicketID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


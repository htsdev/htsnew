﻿/***    
* Waqas Javed 5771 04/16/2009  
* Business Logic : This procedure is used to associate the contact id of the client    
* 1. Contact Id will be associated in non client database if client was a potential client  
* 2. History note will be added " Contact ID [CID] Associated  - [Rep Name] "  
* 
* Parameters : 
*	@ContactID   
*	@TicketID   
*	@EMPID  
***/    
      
CREATE PROCEDURE dbo.USP_HTP_Associate_ContactID
	@ContactID INT ,
	@TicketID INT,
	@EMPID INT
AS
BEGIN
    UPDATE tbltickets
    SET    ContactID_FK = @ContactID,
           IsContactIDConfirmed = 1
    WHERE  TicketID_PK = @TicketID 
    
    --Getting Rep Name
    DECLARE @Name1 AS VARCHAR(50)    
    SELECT @Name1 = (
               SELECT ISNULL(tu.Firstname, '') + ' ' + ISNULL(tu.Lastname, '')
               FROM   tblUsers tu
               WHERE  tu.EmployeeID = @EMPID
           ) 
    --Getting CASE type ID
    DECLARE @Casetype INT
    SELECT @Casetype = casetypeid
    FROM   tblTickets tt
    WHERE  tt.TicketID_PK = @TicketID
    
    --For Traffic and Criminal 
    IF @Casetype = 1
       OR @Casetype = 2
    BEGIN
        IF EXISTS (
               SELECT TOP 1 ttv.Recordid
               FROM   tblTicketsViolations ttv
               WHERE  ttv.RecordID > 0
                      AND ticketid_pk = @TicketID
           )
        BEGIN
            DECLARE @Recordid INT  
            SELECT @Recordid = (
                       SELECT TOP 1 ttv.Recordid
                       FROM   tblTicketsviolations ttv
                       WHERE  ticketid_pk = @TicketID
                   )  
            
            UPDATE tblticketsarchive
            SET    PotentialContactID_FK = @ContactID
            WHERE  RecordID = @Recordid
        END
        ELSE --For Family and Civil
        IF @Casetype = 4
           OR @Casetype = 3
        BEGIN
            IF EXISTS (
                   SELECT TOP 1 ttv.Recordid
                   FROM   tblTicketsViolations ttv
                   WHERE  ttv.RecordID > 0
                          AND ticketid_pk = @TicketID
               )
            BEGIN
                DECLARE @Partyid INT  
                SELECT @Partyid = (
                           SELECT TOP 1 ttv.Recordid
                           FROM   tblTicketsviolations ttv
                           WHERE  ticketid_pk = @TicketID
                       )  
                
                UPDATE civil.dbo.ChildCustodyParties
                SET    PotentialContactID_FK = @ContactID
                WHERE  ChildCustodyPartyId = @Partyid
            END
        END
    END
    
    --Inserting Note in History  
    INSERT INTO tblticketsnotes
      (
        ticketid,
        SUBJECT,
        employeeid
      )
    VALUES
      (
        @TicketID,
        'Contact ID <a href="javascript:window.open(' +
        '''/ClientInfo/AssociatedMatters.aspx?search=0&caseNumber=' +
        CONVERT(VARCHAR, @TicketID) 
        + ''');void('''');">' + CONVERT(VARCHAR, @ContactID) +
        '</a> Associated - ' 
        +
        @Name1 + ' ',
        @EMPID
      )
END
GO

GRANT EXECUTE ON [dbo].[USP_HTP_Associate_ContactID] TO dbr_webuser

GO

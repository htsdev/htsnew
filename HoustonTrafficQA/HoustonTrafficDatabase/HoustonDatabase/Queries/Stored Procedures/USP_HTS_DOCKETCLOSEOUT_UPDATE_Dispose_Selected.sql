SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected]
GO


CREATE procedure dbo.USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected  
 (  
 @TicketsViolationID int,  
 @EmpID int  
 )  
  
  
as  
  
  
update tblticketsviolations   
set  courtviolationstatusidmain = 80,  
  courtviolationstatusid = 80,  
  courtviolationstatusidscan = 80,  
  courtnumber = courtnumbermain,  
  courtnumberscan = courtnumbermain,  
  courtdate = courtdatemain,  
  courtdatescan = courtdatemain,  
  vemployeeid = @EmpID  
where ticketsviolationid = @TicketsViolationID  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


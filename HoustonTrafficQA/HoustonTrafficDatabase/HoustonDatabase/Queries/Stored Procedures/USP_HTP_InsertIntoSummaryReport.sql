 
/****** Object:  StoredProcedure [dbo].[USP_HTP_InsertIntoSummaryReport]    Script Date: 08/01/2008 14:57:57 ******/

-- Noufil 4432 08/05/2008 

/**
		Bussiness Logic : This report insert summary records into table
		Parameter :
					@reporttitle :title of report
					@ThisTotalCount : Total number of rows 
					@ThisPendingCount : Total number of pending records
					@ThisAlertCount : Total number of alert reocrds
					@report_type :Report type .... 1 for Alert report 2 for pending report
 		Column :
**/


ALTER procedure [dbo].[USP_HTP_InsertIntoSummaryReport]
@reporttitle varchar(max),
@ThisTotalCount varchar(20),
@ThisPendingCount varchar(20),
@ThisAlertCount varchar(20),
@report_type INT,
@reportId INT =NULL,	--Saeed 7791 08/10/2010 report type paramter added.
@seessionID VARCHAR(50) --Saeed 7791 08/10/2010 SessionID paramter added.

as 

--insert into Summaryreport (reporttitle,totalcount,pendingcount,alertcount,report_type,ReportID) values (@reporttitle,@ThisTotalCount,@ThisPendingCount,@ThisAlertCount,@report_type,@reportId)
--Saeed 7791 08/10/2010 checking existance of records based on Current SessionID of the user, if records doesn't exist then insert new entry otherwise update that entry.
if not exists (select * from Summaryreport WHERE ReportID=@reportId AND sessionid=@seessionID)
	begin
			insert into Summaryreport (reporttitle,totalcount,pendingcount,alertcount,report_type,ReportID,SessionID) values (@reporttitle,@ThisTotalCount,@ThisPendingCount,@ThisAlertCount,ISNULL(@report_type, 0),@reportId,@seessionID)
	end 
	else
	  update Summaryreport set reporttitle =@reporttitle , totalcount=@ThisTotalCount , pendingcount=@ThisPendingCount , alertcount=@ThisAlertCount WHERE ReportID=@reportId AND sessionid=@seessionID

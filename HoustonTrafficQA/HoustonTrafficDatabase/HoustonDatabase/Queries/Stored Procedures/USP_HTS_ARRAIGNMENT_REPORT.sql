/******   
Alter by:  Syed Muhammad Ozair  
  
Business Logic : this procedure is used to show All The Reocords that are under the Arraignment Status and under HMC 	  
  
List of Parameters:   
 @TVIDS : ticketsviilationids
 @DocumentBatchID : docuemnt id  
  
List of Columns:    
 ticketid :
 ticketno :  
 lastname : 
 firstname : 
 dob : 
 midnum : 
 causeno : 
 courtdate :  
 status :  
 dlnumber :
 ticketsviolationid :
 Time :  
 bonddate :  
 docid : document id
 FLAGID :
   
******/            
              
ALTER PROCEDURE [dbo].[USP_HTS_ARRAIGNMENT_REPORT]                     
                      
@TVIDS as varchar(max),      
@DocumentBatchID as int      
               
as                
                      
          
DECLARE @TBLTEMPTVID TABLE (TICKETVIOLATIONID INT)         
      
if  @TVIDS=''                  
begin            
	                      
	Select distinct                       
	t.ticketid_pk as Ticketid,                    
	tv.REFCASENUMBER AS TICKETNO,                        
	upper(T.LASTNAME) as lastname,                      
	upper(T.FIRSTNAME) as firstname,                      
	convert(varchar(12),T.DOB,101) as DOB,                        
	T.MIDNUM,                      
	TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,                        
	TV.COURTDATEMAIN AS COURTDATE,                        
    COURTDATEMAIN as CRTDATE, --for sorting..           
	TCV.SHORTDESCRIPTION as status,                      
    t.dlnumber,                                
    tv.ticketsviolationid ,        
	isnull(datediff(day,tvl.recdate,getdate()),0) as Time,      
	@DocumentBatchID as DocID,    
	CASE WHEN TF.FlagID IS NULL THEN '' ELSE 'N' END AS FLAGID                            
	FROM                     
    TBLTICKETS T INNER JOIN                       
    TBLTICKETSVIOLATIONS TV ON                       
    T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                       
    TBLCOURTVIOLATIONSTATUS TCV ON                       
    TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                     
    tblcourts tc on                    
    tv.courtid=tc.courtid left outer join        
	tblticketsviolationlog tvl on         
	tv.ticketsviolationid=tvl.ticketviolationid and        
    tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid)    
	LEFT OUTER   join tblTicketsFlag TF on    
	TV.TICKETID_PK = TF.TICKETID_PK     
	AND TF.FlagID = 17                                                   
	WHERE                      	                       
	tv.courtviolationstatusidmain in (select courtviolationstatusid from tblcourtviolationstatus where CATEGORYID IN (2)) 
	and t.activeflag=1  and tv.courtid in (3001,3002,3003,3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .
	and tv.courtdatemain is not null                            
	AND tV.courtviolationstatusidmain <> 80                        
	AND tV.courtviolationstatusidmain <> 162                                          
	order by  CRTDATE ,lastname,firstname--Fahad 10345 06/25/2012 Fixed Sorting issue              
end      
else      
begin      
	insert into @TBLTEMPTVID select * from dbo.sap_string_param(@TVIDS)                   
	Select                       
	t.ticketid_pk as Ticketid,                    
	tv.REFCASENUMBER AS TICKETNO,                        
	upper(T.LASTNAME) as lastname,                      
	upper(T.FIRSTNAME) as firstname,                      
	convert(varchar(12),T.DOB,101) as DOB,                      
	T.MIDNUM,                      
	TV.CASENUMASSIGNEDBYCOURT AS CAUSENO,                      
	CONVERT(VARCHAR(12),TV.COURTDATEMAIN,101) AS COURTDATE,           
    COURTDATEMAIN as CRTDATE, --for sorting..           
	TCV.SHORTDESCRIPTION as status,                      
    t.dlnumber,                  
    tv.ticketsviolationid ,        
    isnull(datediff(day,tvl.recdate,getdate()),0) as Time ,      
	@DocumentBatchID as DocID                                             
	FROM                     
    TBLTICKETS T INNER JOIN                       
    TBLTICKETSVIOLATIONS TV ON                       
    T.TICKETID_PK=TV.TICKETID_PK INNER JOIN                       
    TBLCOURTVIOLATIONSTATUS TCV ON                       
    TV.COURTVIOLATIONSTATUSIDMAIN=TCV.COURTVIOLATIONSTATUSID inner join                     
    tblcourts tc on                    
    tv.courtid=tc.courtid left outer join        
	tblticketsviolationlog tvl on         
	tv.ticketsviolationid=tvl.ticketviolationid and        
    tvl.recdate=(select max(recdate) from tblticketsviolationlog where ticketviolationid= tvl.ticketviolationid)                                                   
	WHERE                                                 
    TV.TICKETSVIOLATIONID IN (SELECT TICKETVIOLATIONID FROM @TBLTEMPTVID)  
    and tv.courtviolationstatusidmain in (select courtviolationstatusid from tblcourtviolationstatus where CATEGORYID IN (2))
	and t.activeflag=1  and tv.courtid in (3001,3002,3003,3075) -- Rab Nawaz Khan 10327 06/14/2012 Added the HMC-W Court in Arraignment Setting Report. . .                   
	and tv.courtdatemain is not null                            
	AND tV.courtviolationstatusidmain <> 80                        
	AND tV.courtviolationstatusidmain <> 162                                          
	order by  CRTDATE   ,lastname,firstname--Fahad 10345 06/25/2012 Fixed Sorting issue              
 end    
       
    
    
    
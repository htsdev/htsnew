/********
Alter by      : Abbas Qamar 
Task Id	      : 9726
Alter Date    : 11-02-2011
Business Logic:	The store procedure is use to mark the Client as Bad Email Flag either in HTP or DTP
*******/


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




--USP_HTS_LA_Update_NoMailFlag '3/27/2008','3007, 3008', 1
  
ALTER PROCEDURE [dbo].[USP_HTS_LA_Update_NoMailFlag]  
@LoadDate datetime,  
@CourtID varchar(10),
@Database	int=1	 --  1=Traffictickets,2=DallasTraffictickets,3=JIMS
AS   
Declare @JPCourtIDs as varchar(100)

UPDATE Tbl_HTS_LA_Group SET IsProcessed = 1 WHERE DATEDIFF(DAY,Load_Date, GETDATE())=0

If @CourtID = 'JP'
	Begin
		Set @JPCourtIDs = '*'
		Select @JPCourtIDs = @JPCourtIDs + ',' + convert(varchar(10),CourtID) from dallastraffictickets.dbo.TblCourts where CourtCategoryNum = 11
		Set @JPCourtIDs = replace(@JPCourtIDs,'*,','')
		Set @CourtID = @JPCourtIDs
	End

--For Houston  
if @database=1
	update traffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
	     -- Abbas Qamar 9726 11-02-2011 Removing First name Filter 
--		 traffictickets.dbo.tblticketsarchive.firstname = traffictickets.dbo.tbl_hts_donotmail_records.FirstName         
--		 AND        
		 traffictickets.dbo.tblticketsarchive.lastname = traffictickets.dbo.tbl_hts_donotmail_records.LastName         
		 AND        
		 traffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	  AND      
	  traffictickets.dbo.tblticketsarchive.ZipCode LIKE(traffictickets.dbo.tbl_hts_donotmail_records.Zip+'%')  
	 AND  
	 traffictickets.dbo.tblticketsarchive.CourtID in (@CourtID)
	 AND   
	 traffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    

--Abbas Qamar 9726 11-02-2011 Check Clients on Zip and address base.

update traffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
		 traffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	     AND      
	     traffictickets.dbo.tblticketsarchive.ZipCode LIKE(traffictickets.dbo.tbl_hts_donotmail_records.Zip+'%')  
  	     AND   
	     traffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    



--For Dallas  
if @database=2
	update dallastraffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
	   -- Abbas Qamar 9726 11-02-2011 Removing First name Filter   
--		 dallastraffictickets.dbo.tblticketsarchive.firstname = traffictickets.dbo.tbl_hts_donotmail_records.FirstName         
--		 AND        
		 dallastraffictickets.dbo.tblticketsarchive.lastname = traffictickets.dbo.tbl_hts_donotmail_records.LastName         
		 AND        
		 dallastraffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	  AND      
	  dallastraffictickets.dbo.tblticketsarchive.ZipCode LIKE(traffictickets.dbo.tbl_hts_donotmail_records.Zip+'%')  
	 AND  
	 convert(varchar(100),dallastraffictickets.dbo.tblticketsarchive.CourtID) in ( @CourtID )
	 AND   
	 dallastraffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    
	 
	 
	 
	 --Abbas Qamar 9726 11-02-2011 Check Clients on Zip and address base.

update dallastraffictickets.dbo.tblticketsarchive         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecLoadDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
		dallastraffictickets.dbo.tblticketsarchive.address1 = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	     AND      
	      dallastraffictickets.dbo.tblticketsarchive.ZipCode LIKE(traffictickets.dbo.tbl_hts_donotmail_records.Zip+'%')  
  	     AND   
	     dallastraffictickets.dbo.tblticketsarchive.DoNotMailFlag = 0  
	 )    


--For Jims
if  @database=3 
	update jims.dbo.CriminalCases         
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
	             
--		 jims.dbo.CriminalCases.firstname = traffictickets.dbo.tbl_hts_donotmail_records.FirstName         
--		 AND        
		 jims.dbo.CriminalCases.lastname = traffictickets.dbo.tbl_hts_donotmail_records.LastName         
		 AND        
		 jims.dbo.CriminalCases.address = traffictickets.dbo.tbl_hts_donotmail_records.Address      
	  AND      
	  jims.dbo.CriminalCases.Zip LIKE(traffictickets.dbo.tbl_hts_donotmail_records.Zip+'%')  
	 AND  
	 jims.dbo.CriminalCases.DoNotMailFlag = 0  
	 )    

--Abbas Qamar 9726 11-02-2011 Check Clients on Zip and address base.

update jims.dbo.CriminalCases 
	 set DoNotMailFlag =1        
	WHERE         
	 datediff(day,RecDate,@LoadDate) =0         
	 and        
	   exists         
		(select distinct firstname,        
		 lastname,        
		 address,    
	  zip         
	             
		from traffictickets.dbo.tbl_hts_donotmail_records         
		WHERE         
		jims.dbo.CriminalCases.address = traffictickets.dbo.tbl_hts_donotmail_records.Address         
	     AND      
	    jims.dbo.CriminalCases.Zip LIKE(traffictickets.dbo.tbl_hts_donotmail_records.Zip+'%')  
  	     AND   
	    jims.dbo.CriminalCases.DoNotMailFlag = 0
	 )    





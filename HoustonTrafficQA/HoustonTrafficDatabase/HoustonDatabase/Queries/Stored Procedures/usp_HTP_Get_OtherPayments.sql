

--****************************************************************************
--Created By   : Afaq Ahmed
--Creation Data: 11/02/2010
--Task ID :8213
--Parameter List	
--@showAllRecord : If true than show all record. 
--@FromDate : From Date
--@ToDate : To Date
--@ShowPending : If true than show undeleted records.
--Return Column:
--tt.Lastname,tt.Firstname,ttp.ChargeAmount,tt.TicketID_PK,ttp.InvoiceNumber_PK,ttp.isRemoved,generalComments for last 5 days against each case.
--Business Logic : Dispaly cases under following criteria.
--1) IS ACTIVE client.
--2) Payment should NOT be void.
--3) Payment identifier should be other.
--****************************************************************************  
Create PROCEDURE [dbo].[usp_HTP_Get_OtherPayments] 
(
    @ShowAllRecords  BIT = 0,
    @FromDate        DATETIME = '01/01/1900',
    @ToDate          DATETIME = '01/01/1900',
    @ShowPending     BIT = 0
)
AS
	SELECT tt.Lastname,
	       tt.Firstname,
	       '$' + CONVERT(VARCHAR, ttp.ChargeAmount, 1) AS ChargeAmount,
	       tt.TicketID_PK,
	       ttp.InvoiceNumber_PK,
	       ttp.isRemoved,
	       (
	           SELECT REPLACE (SUBJECT + ' ', 'General Notes:', '')
	           FROM   tblticketsnotes
	           WHERE  Fk_CommentID = 1
	                  AND tblticketsnotes.TicketID = tt.TicketID_PK
	                  AND DATEDIFF(DAY, Recdate, GETDATE()) < 5
	           ORDER BY
	                  Recdate DESC FOR XML PATH('')
	       ) AS generalComments
	FROM   tblTickets tt
	       INNER JOIN tblTicketsPayment ttp
	            ON  tt.TicketID_PK = ttp.TicketID
	WHERE  tt.Activeflag = 1 
	       --Payment should not be void
	       AND ttp.PaymentVoid = 0
	           --Payment identifier should be Others
	       AND ttp.paymentIdentifier = 4
	       AND (
	               (@ShowPending = 1 AND ttp.isRemoved = 0)
	               OR (
	                      @ShowAllRecords = 0
	                      AND @ShowPending = 0
	                      AND ttp.RecDate BETWEEN @FromDate AND @ToDate
	                      AND ttp.isRemoved = 0
	                  )
	               OR (@ShowAllRecords = 1)
	           )
	ORDER BY
	       tt.TicketID_PK
GO 
GRANT EXECUTE ON [usp_HTP_Get_OtherPayments] TO dbr_webuser
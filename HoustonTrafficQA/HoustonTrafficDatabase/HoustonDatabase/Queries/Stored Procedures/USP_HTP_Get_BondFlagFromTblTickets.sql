/****** 
* Object:  StoredProcedure [dbo].[USP_HTP_Get_BondFlagFromTblTickets]    Script Date: 05/29/2009 22:12:54 
* created by :Fahad Muhammad Qureshi
* BUsiness Logic : THis method returns bond flag of client.
* ******/

--[USP_HTP_Get_BondFlagFromTblTickets] 248610

CREATE procedure [dbo].[USP_HTP_Get_BondFlagFromTblTickets]
@ticketid_pk int 
as 

select ISNULL(tt.BondFlag,0) from tblTickets tt where tt.TicketID_PK=@ticketid_pk

GO
GRANT EXECUTE ON [dbo].[USP_HTP_Get_BondFlagFromTblTickets] TO dbr_webuser
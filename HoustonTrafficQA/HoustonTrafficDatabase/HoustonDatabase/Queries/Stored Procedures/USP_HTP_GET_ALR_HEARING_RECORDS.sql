/**
* Business Logic : This procedure return ALR HEARING RECRODS base on different criterea.
					05 --> ALR Hearing Request : Is Client and ALR Hearing Request document is not yet scanned
					06 --> ALR DPS Receipt : 1 day after ALR Hearing Request is scanned and ALR DPS Receipt not yet scanned.
					07 --> ALR Hearing Notofication : 21 Days after Hire date and ALR Hearing Notofication document not yet scanned.
					08 --> ALR Discovery : 28 Days Before court date and ALR Discovery document not yet scanned.
					09 --> ALR Subpoena Application : 21 days before court date and ALR Subpoena Application document not yet scanned.
					10 --> ALR Subpoena Pick up Alert : 2 days after ALR Subpoena Application is scanned and ALR Subpoena Pick up Alert document not yet scanned.
					11 --> ALR Subpoena Return : 5 Days Before court date and ALR Subpoena Return documents is not yet scanned.
					12 --> ALR Hearing Disposition : 7 Days after court date and ALR Hearing Disposition documents is not scanned and case is not dispose.
					13 --> ALR Hearing Waiver : Shut off all alerts.
					14 --> ALR Request for Appearance of Witness : 21 Days Before court date and ALR Request for Appearance of Witness not yet scanned.
					15 --> ALR Continuance Request (Mandatory) : 5 Days after ALR Continuance Request Reset (Mandatory) is scanned and ALR Continuance Request (Mandatory) is not yet scanned
					16 --> ALR Continuance Request Reset (Mandatory) : 
					17 --> ALR Continuance Request (Discretionary) : 5 Days after ALR Continuance Request Reset (Discretionary) is scanned and ALR Continuance Request (Mandatory) is not yet scanned
					18 --> ALR Continuance Request Reset (Discretionary) :
					19 --> ALR Continuance Denial : Shut off all continuance alerts.

Dependency : Store procedure "USP_HTP_GET_ALRHEARINGREPORT" is depnedent on this Store procedure.

**/


-- [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 14,1,1,''

Alter PROCEDURE [dbo].[USP_HTP_GET_ALR_HEARING_RECORDS] 
@SubDocTypeID INT,
@Showresult INT = 0,
@showvalidation INT=0,
@SQLReturnQuery VARCHAR(5000) OUTPUT

AS

declare @Title varchar(500)
set @Title = (select SubDocType from tblsubdoctype where SubDocTypeID= @SubDocTypeID)


DECLARE @followupdatecolumnname VARCHAR(200)
DECLARE @Eventdays INT
DECLARE @operator VARCHAR(10)


SET @Eventdays = (SELECT tsdt.Eventdays FROM tblSubDocType tsdt WHERE tsdt.SubDocTypeID=@SubDocTypeID)

IF @Eventdays = 0
	SET @operator = '>='
ELSE
	SET @operator = '>'	

IF @SubDocTypeID = 5
BEGIN
	SET @followupdatecolumnname = 'tt.ALRHearingFollowUpDate'
END
ELSE IF @SubDocTypeID = 6
BEGIN
	SET @followupdatecolumnname = 'tt.ALRDPSReceiptFollowUpDate'
END
ELSE IF @SubDocTypeID = 7
BEGIN
	SET @followupdatecolumnname = 'tt.ALRHearingNtfFollowUpDate'
END
ELSE IF @SubDocTypeID = 8
BEGIN
	SET @followupdatecolumnname = 'tt.ALRDiscoveryFollowUpDate'
END
ELSE IF @SubDocTypeID = 9
BEGIN
	SET @followupdatecolumnname = 'tt.ALRSubpAppFollowUpDate'
END
ELSE IF @SubDocTypeID = 10
BEGIN
	SET @followupdatecolumnname = 'tt.ALRSubpAlertFollowUpDate'
END
ELSE IF @SubDocTypeID = 11
BEGIN
	SET @followupdatecolumnname = 'tt.ALRSubpRetFollowUpDate'
END
ELSE IF @SubDocTypeID = 12
BEGIN
	SET @followupdatecolumnname = 'tt.ALRHearingDispFollowUpDate'
END
ELSE IF @SubDocTypeID = 14
BEGIN
	SET @followupdatecolumnname = 'tt.ALRRAWFollowUpDate'
END
ELSE IF @SubDocTypeID = 15
BEGIN
	SET @followupdatecolumnname = 'tt.ALRContReqMFollowUpDate'
END
ELSE IF @SubDocTypeID = 17
BEGIN
	SET @followupdatecolumnname = 'tt.ALRContReqDFollowUpDate'
END
ELSE
BEGIN
SET @followupdatecolumnname = ' '
END
	


DECLARE @SqlQuery VARCHAR(MAX)
SET @SqlQuery = 
' SELECT DISTINCT 
       tt.TicketID_PK,
       tt.Firstname,
       tt.Lastname,
       (select top 1 isnull(tv.casenumassignedbycourt,'''') from tblTicketsViolations tv
       			where tv.TicketID_PK = tt.TicketID_PK AND tv.CourtDatemain = MIN(ttv.CourtDatemain) AND tv.ViolationNumber_PK = 16159 ) as casenumassignedbycourt,              			
		(
           SELECT TOP 1 ISNULL(tvv.RefCaseNumber, '''')
           FROM   tblTicketsViolations tvv
           WHERE  tvv.TicketID_PK = tt.TicketID_PK
                  AND tvv.CourtDatemain = MIN(ttv.CourtDatemain)
                  AND tvv.ViolationNumber_PK = 16159
       ) AS refcasenumber,
       dbo.fn_DateFormat(MIN(ttv.CourtDatemain),25,''/'','':'',1) as CourtDatemain,       
       dbo.fn_DateFormat(MIN(ttv.CourtDatemain), 26, ''/'', '':'', 1) AS courttime,
       MIN(ttv.CourtNumbermain) as CourtNumbermain,
       MIN(ttp.RecDate) AS hiredate,'''
	  + convert(varchar(500),@Title) +''' as [Title],'
	  + @followupdatecolumnname + ' as Followupdate,'
	  + convert(varchar(8),@SubDocTypeID) + ' as subdoctypeid,
	  (select ALRFollowUpDate from tblSubDocType where SubDocTypeID=' + convert(varchar(8),@SubDocTypeID) + ')  as ALRMaxBusinessDay
	   
FROM   tblTickets tt
       INNER JOIN tblTicketsViolations ttv
            ON  ttv.TicketID_PK = tt.TicketID_PK       
       INNER JOIN tblTicketsPayment ttp
            ON  ttp.TicketID = tt.TicketID_PK
	   INNER JOIN tblViolations tv 
			ON tv.ViolationNumber_PK = ttv.ViolationNumber_PK
	   LEFT OUTER JOIN tblCourtViolationStatus tcvs
			ON tcvs.CourtViolationStatusID = ttv.CourtViolationStatusIDmain
	   LEFT OUTER JOIN tblScanBook tsb
            ON  tsb.TicketID = tt.TicketID_PK
       LEFT OUTER JOIN tblSubDocType tsdt
            ON  tsdt.SubDocTypeID = tsb.SubDocTypeID
            
WHERE
	   ttv.ViolationNumber_PK = 16159       
       AND tt.Activeflag = 1 and ttv.courtid in (SELECT Courtid FROM tblCourts WHERE ALRProcess = 1 )' 
       
IF @showvalidation = 1
BEGIN
	SET @SqlQuery = @SqlQuery + 'AND DATEDIFF(DAY, isnull(' + @followupdatecolumnname + ',''01/01/1900''), GETDATE()) >= 0 '
END
       
       
-- ttv.CourtID IN (3047, 3048, 3070, 3079)
-- 5 - ALR HEARING REQUEST
-- 7 - ALR DPS Receipt  
-- 8 - ALR DISCOVERY
-- 9 - ALR SUBPOENA APPLICATION
-- 10 - ALR SUBPOENA PickUp Alert
-- 11 - ALR SUBPOENA Return
-- 12 - ALR Hearing Disposition 

--IF  @SubDocTypeID =5 OR @SubDocTypeID = 7 OR @SubDocTypeID = 8 OR @SubDocTypeID = 9 OR @SubDocTypeID = 10 OR @SubDocTypeID = 11 OR @SubDocTypeID = 12 or
--	@SubDocTypeID =14 
IF @SubDocTypeID NOT IN (12)
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND tcvs.CategoryID != 50'
END

----Asad Ali 7991 07/13/2010 Seprate Condition of doctype 7,8 and 9,10,11
IF @SubDocTypeID IN (7,8)
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in ('+ CONVERT(VARCHAR(20),@SubDocTypeID)+', 13)AND tsb2.IsDeleted=0)'
END

----Asad Ali 7991 07/13/2010 Seprate Condition of doctype 7,8 and 9,10,11
IF @SubDocTypeID IN (9,10,11)
BEGIN
	----Asad Ali 7991 07/13/2010  Conditition Added (And  ISNULL(IsALRArrestingObservingSame,0)<>1   And ISNULL (AlROfficerName,'''')<>ISNULL(ALRBTOLastName,'''')  AND  ISNULL(ALRBTOLastName,'''')<>ISNULL(ALRObservingOfficerName,'''') ) If BTO, Observing Officer and BTO operator is the same person then don't show the client in 'Subpeona Application' or 'Supeona Pick up Alert' and Subpeona Return' 
	--SET @SqlQuery = @SqlQuery + ' AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in ('+ CONVERT(VARCHAR(20),@SubDocTypeID)+', 13)AND tsb2.IsDeleted=0) And  ISNULL(IsALRArrestingObservingSame,0)<>1   And ISNULL (AlROfficerName,'''')<>ISNULL(ALRBTOLastName,'''')  AND  ISNULL(ALRBTOLastName,'''')<>ISNULL(ALRObservingOfficerName,'''')  ' 
	SET @SqlQuery = @SqlQuery + ' AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in ('+ CONVERT(VARCHAR(20),@SubDocTypeID)+', 13)AND tsb2.IsDeleted=0) 
	AND 
	(
		(ISNULL(IsALRArrestingObservingSame,0)<>1   
		And 
		ISNULL (AlROfficerName,'''') <> ISNULL(ALRBTOLastName,'''')  
		AND  
		ISNULL(ALRBTOLastName,'''') <> ISNULL(ALRObservingOfficerName,'''')
		)
		OR
		(
		ISNULL(IsALRArrestingObservingSame,0)=1   
		And 
		ISNULL (AlROfficerName,'''') <> ISNULL(ALRBTOLastName,'''')  
		AND  
		ISNULL(ALRBTOLastName,'''') <> ISNULL(ALRObservingOfficerName,'''')
		)  
		OR
		(
		ISNULL(IsALRArrestingObservingSame,0)<>1   
		And 
		ISNULL (AlROfficerName,'''') = ISNULL(ALRBTOLastName,'''')  
		AND  
		ISNULL(ALRBTOLastName,'''') <> ISNULL(ALRObservingOfficerName,'''')
		)  
		  
		OR
		(
		ISNULL(IsALRArrestingObservingSame,0)<>1   
		And 
		ISNULL (AlROfficerName,'''') <> ISNULL(ALRBTOLastName,'''')  
		AND  
		ISNULL(ALRBTOLastName,'''') = ISNULL(ALRObservingOfficerName,'''')
		)
		OR
		(
		ISNULL(IsALRArrestingObservingSame,0)<>1   
		And 
		ISNULL (AlROfficerName,'''') = ISNULL(ALRBTOLastName,'''')  
		AND  
		ISNULL(ALRBTOLastName,'''') = ISNULL(ALRObservingOfficerName,'''')
		)
	)
	
	' 	
	
	
END


--Sabir Khan 7730 04/30/2010 Check for breath test officer...
else IF @SubDocTypeID = 14
BEGIN
	----Asad Ali 7991 07/13/2010  Conditition Added AND (ISNULL(tt.ALRIsIntoxilyzerTakenFlag,0) = 1 if IS IntoxilyzerTaken then show this report
	SET @SqlQuery = @SqlQuery + 'AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in ('+ CONVERT(VARCHAR(20),@SubDocTypeID)+', 13)AND tsb2.IsDeleted=0) AND (ISNULL(tt.ALRIsIntoxilyzerTakenFlag,0) = 1) '
END  
else if @SubDocTypeID = 5
BEGIN	
	SET @SqlQuery = @SqlQuery + ' AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in ('+ CONVERT(VARCHAR(20),@SubDocTypeID)+', 13)AND tsb2.IsDeleted=0)' 
	IF @Eventdays >0
	SET @SqlQuery = @SqlQuery + ' AND datediff(day,getdate(),ttp.RecDate)'+@operator+ ' ' + CONVERT(VARCHAR(20),@Eventdays) + ' AND ttp.'
END

-- 15 - ALR Continuance Request (Mandatory)
-- Noufil 6358 08/25/2009 Remove ALR Continuance Request Reset(Mandatory) dependecny
else if @SubDocTypeID =15
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND ISNULL(tsb.SubDocTypeID, 0) = 15 AND tsb.IsDeleted=0 AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in (19,13)AND tsb2.IsDeleted=0)'
END

-- 17 - ALR Continuance Request (Discretionary)
-- Noufil 6358 08/25/2009 Remove ALR Continuance Request Reset(Discretionary) dependecny
else if @SubDocTypeID =17
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND ISNULL(tsb.SubDocTypeID, 0) = 17 AND tsb.IsDeleted=0 AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in (19,13)AND tsb2.IsDeleted=0)'
END

-- ALR DPS Receipt       
IF @SubDocTypeID =6
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND isnull(tsb.SubDocTypeID,0) = 5 AND tt.TicketID_PK NOT IN (select TicketID FROM tblScanBook tsb2 WHERE tsb2.SubDocTypeID in ('+ CONVERT(VARCHAR(20),@SubDocTypeID)+',13)AND tsb2.IsDeleted=0)'
END

IF @SubDocTypeID = 10
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND isnull(tsb.SubDocTypeID,0) = 9 '
END

IF @SubDocTypeID = 12
BEGIN
	SET @SqlQuery = @SqlQuery + ' AND  ttv.TicketsViolationID NOT IN (SELECT  tttv.TicketsViolationID FROM tblticketsviolations tttv
                                  INNER JOIN tblScanBook tsb2 ON tttv.TicketID_PK = tsb2.TicketID                                  
                                  WHERE  tsb2.IsDeleted=0 
	                              GROUP BY tttv.CourtViolationStatusIDmain,tttv.TicketsViolationID,tsb2.TicketID,tsb2.SubDocTypeID 
								  HAVING (tsb2.SubDocTypeID = '+ CONVERT(VARCHAR(20),@SubDocTypeID)+' AND tttv.CourtViolationStatusIDmain = 80) OR (tsb2.SubDocTypeID = 13)) '
END

SET @SqlQuery = @SqlQuery +
' GROUP BY
       tt.TicketID_PK,
       tt.Firstname,
       tt.Lastname,
       ttv.refcasenumber,       
       tsdt.SubDocTypeID,tsdt.ALRFollowUpDate,' + @followupdatecolumnname 


IF @SubDocTypeID =6
BEGIN
	SET @SqlQuery = @SqlQuery + ' HAVING DATEDIFF (DAY,MIN(tsb.UPDATEDATETIME),GETDATE())>= ' + CONVERT(VARCHAR(20),@Eventdays)
END


IF @SubDocTypeID = 7
BEGIN
	SET @SqlQuery = @SqlQuery + ',ttp.PaymentVoid HAVING DATEDIFF (DAY,MIN(ttp.RecDate),GETDATE())>= ' + CONVERT(VARCHAR(20),@Eventdays) + ' AND ttp.PaymentVoid=0'
END


IF @SubDocTypeID = 8
BEGIN
	-- Noufil 6358 08/27/2009 Show only cases having ALR status
	SET @SqlQuery = @SqlQuery + '
       ,ttv.CourtViolationStatusIDmain HAVING DATEDIFF (DAY,GETDATE(),MIN(ttv.CourtDateMain))>= 0 and DATEDIFF (DAY,GETDATE(),MIN(ttv.CourtDateMain)) <= ' + CONVERT(VARCHAR(20),@Eventdays)+ ' AND ttv.CourtViolationStatusIDmain=237'
END

IF @SubDocTypeID = 9
BEGIN
	-- Noufil 6358 08/27/2009 Show only cases having ALR status
	SET @SqlQuery = @SqlQuery + '
        ,ttv.CourtViolationStatusIDmain HAVING DATEDIFF (DAY,GETDATE(),MIN(ttv.CourtDateMain)) >= 0 and DATEDIFF (DAY,GETDATE(),MIN(ttv.CourtDateMain)) <='+ CONVERT(VARCHAR(20),@Eventdays)+ ' AND ttv.CourtViolationStatusIDmain=237'
END
 
-- 10 - ALR SUBPOENA PickUp Alert
IF @SubDocTypeID = 10
BEGIN
	SET @SqlQuery = @SqlQuery + ',
       tsb.UPDATEDATETIME HAVING DATEDIFF(DAY,MIN(tsb.UPDATEDATETIME),GETDATE()) >='+ CONVERT(VARCHAR(20),@Eventdays)
END
 
-- 11 - ALR SUBPOENA Return
IF @SubDocTypeID = 11
BEGIN
	-- Noufil 6358 08/27/2009 Show only cases having ALR status
	SET @SqlQuery = @SqlQuery + ' 
       ,ttv.CourtViolationStatusIDmain HAVING DATEDIFF(DAY,GETDATE(),MIN(ttv.CourtDatemain)) >= 0 AND DATEDIFF(DAY,GETDATE(),MIN(ttv.CourtDatemain)) <='+ CONVERT(VARCHAR(20),@Eventdays)+ ' AND ttv.CourtViolationStatusIDmain=237'
END

-- 12 - ALR Hearing Disposition 
IF @SubDocTypeID = 12
BEGIN
	-- Noufil 6358 08/27/2009 Show only cases having ALR status
	SET @SqlQuery = @SqlQuery + '
       ,ttv.CourtViolationStatusIDmain HAVING DATEDIFF (DAY,MIN(ttv.CourtDatemain),GETDATE()) >= '+ CONVERT(VARCHAR(20),@Eventdays)+ ' AND ttv.CourtViolationStatusIDmain=237'
END

-- 14 - ALR Request for Appearance of Witness 
IF @SubDocTypeID = 14
BEGIN
	-- Noufil 6358 08/27/2009 Show only cases having ALR status
	SET @SqlQuery = @SqlQuery + ' 
       ,ttv.CourtViolationStatusIDmain HAVING DATEDIFF(DAY, GETDATE(),MIN(ttv.CourtDatemain) ) >= 0 AND DATEDIFF(DAY, GETDATE(),MIN(ttv.CourtDatemain)) <= '+ CONVERT(VARCHAR(20),@Eventdays)+ ' AND ttv.CourtViolationStatusIDmain=237'
END

IF @SubDocTypeID = 15
BEGIN
	-- noufil 6358 08/24/2009 Show clients having court date within 30 days and Dont show client if they have "ALR hearing notification" document uploaded after "ALR Continuance Request (Mandatory)".
	SET @SqlQuery = @SqlQuery + ' HAVING DATEDIFF(DAY,GETDATE(),MIN(ttv.CourtDatemain)) >= 0  AND DATEDIFF(DAY,GETDATE(),MIN(ttv.CourtDatemain)) <=30 	
	AND
       (
           (
               SELECT MAX(tsb.UPDATEDATETIME)
               FROM   tblScanBook tsb
               WHERE  tsb.TicketID =  tt.TicketID_PK
                      AND tsb.IsDeleted = 0
                      AND tsb.SubDocTypeID = 15
           ) > (
               SELECT ISNULL (MAX(tsb.UPDATEDATETIME),''01/01/1900'')
               FROM   tblScanBook tsb
               WHERE  tsb.TicketID =  tt.TicketID_PK
                      AND tsb.IsDeleted = 0
                      AND tsb.SubDocTypeID = 7
           )
       )'
END
-- noufil 6358 08/24/2009 Dont show client if they have "ALR hearing notification" document uploaded after "ALR Continuance Request (Discretionary)"
IF @SubDocTypeID = 17
BEGIN
	SET @SqlQuery = @SqlQuery + '
       ,tsb.UPDATEDATETIME  HAVING DATEDIFF(DAY,(
               SELECT MIN(tsb2.UPDATEDATETIME)
               FROM   tblScanBook tsb2
               WHERE  tsb2.TicketID = tt.TicketID_PK
                      AND tsb2.SubDocTypeID = 17
                      AND tsb2.IsDeleted = 0
           ),GETDATE()) >= '+ CONVERT(VARCHAR(20),@Eventdays)+ ' AND
       (
           (
               SELECT MAX(tsb.UPDATEDATETIME)
               FROM   tblScanBook tsb
               WHERE  tsb.TicketID =  tt.TicketID_PK
                      AND tsb.IsDeleted = 0
                      AND tsb.SubDocTypeID = 17
           ) > (
               SELECT ISNULL (MAX(tsb.UPDATEDATETIME),''01/01/1900'')
               FROM   tblScanBook tsb
               WHERE  tsb.TicketID =  tt.TicketID_PK
                      AND tsb.IsDeleted = 0
                      AND tsb.SubDocTypeID = 7
           )
       )'
END

SET @SQLReturnQuery = (@SqlQuery)

IF (@Showresult = 1)
	BEGIN
		PRINT (@SqlQuery)
		EXEC (@SqlQuery)	
	END


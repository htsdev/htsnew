SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_HTS_GetDispositionCaseStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_HTS_GetDispositionCaseStatus]
GO

create procedure dbo.usp_HTS_GetDispositionCaseStatus

as 

Select  CourtViolationStatusID as ID,Description   From tblCourtViolationStatus  
where (statustype =1  or courtviolationstatusid in(201,202))
order by sortorder

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


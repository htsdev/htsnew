 --****************************************************************************
--Created By   : Afaq Ahmed
--Creation Data: 11/02/2010
--Task ID :8213
--Parameter List: N/A
--Return Column:
--TicketID_PK,Lastname,Firstname,TicketNumber,CauseNumber,ViolationDescription,ContinuanceComments,Status,CourtDate,CourtRoom,
--Business Logic :   
--display only those records that meet the following criteria   
--1) Is Active Client   
--2) Case is not disposed   
--3) Continuance is Active   
--4) Trial Status should be Judge, Jury or Pre
--****************************************************************************     
      
Alter  PROCEDURE [dbo].[USP_HTP_GET_Trial_Continuance_REPORT]            
as            
            
SELECT distinct              
 t.TicketID_PK,              
 t.Lastname,             
 t.Firstname,             
 tv.RefCaseNumber AS TicketNumber,             
 tv.casenumassignedbycourt AS CauseNumber,             
 v.Description AS ViolationDescription,             
 t.ContinuanceComments,             
 tblCourtViolationStatus.ShortDescription AS Status,             
 tv.CourtDateMain AS CourtDate,             
 tv.CourtNumbermain AS CourtRoom,           
Flag=  
CASE  
  when tv.UnderlyingBondFlag = 1 AND sb.DocTypeID = 14 then 'B; POA'  
 when tv.UnderlyingBondFlag = 1  then 'B'  
  when sb.DocTypeID = 14   then 'POA'  
 ELSE ''  
END,   
ISNULL(tv.UnderlyingBondFlag,0) UnderlyingBondFlag,  
ISNULL(sb.DocTypeID,0) DocTypeID        
FROM         tblTickets AS t INNER JOIN            
                      tblTicketsViolations AS tv ON t.TicketID_PK = tv.TicketID_PK INNER JOIN            
                      tblViolations AS v ON tv.ViolationNumber_PK = v.ViolationNumber_PK INNER JOIN            
                      tblCourtViolationStatus ON tv.CourtViolationStatusIDmain = tblCourtViolationStatus.CourtViolationStatusID INNER JOIN            
       tblTicketsFlag tf ON t.TicketID_PK = tf.TicketID_PK LEFT OUTER JOIN            
       tblScanBook sb ON t.TicketID_PK = sb.TicketID   
       AND sb.DocTypeID = 14   
         
       AND ISNULL(IsDeleted,0)=0  
           
WHERE           
  isnull(t.Activeflag,0)=1 --Active clients --            
  AND tv.CourtViolationStatusIDmain<>80 --Case is not disposed   
  AND  tf.FlagID =9  
  AND tblCourtViolationStatus.CategoryID IN (3,4,5)  
  AND tf.ShowContinuance = 1 --Afaq 9521 07/25/2011 display record if court date is not changed after the continuance date
        
ORDER BY            
 tv.CourtDateMain, tv.CourtNumbermain   
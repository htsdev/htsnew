set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*  
* Created By		: Saeed Ahmed
* Task ID			: 8101  
* Created Date		: 09/27/2010  
* Business logic	: This procedure is used to insert the new record in the ReportAttribute table if the AttributeID doesn't exist
*                   :If the AttributeID exist then it will update the already available record in the ReportAttribute table
* Parameter List  
* @attributeid 		: This parameter contains the AttributeID
* @attributename    : This parameter contains the AttributeName
* @shortname        : This parameter contains the ShortName
* @description      : This parameter contains the Description
* @insertdate       : This parameter contains the insert date
* @insertby         : This parameter contains the inserted user id
* @lastupdatedate   : This parameter contains the last update date
* @lastupdateby     : This parameter contains the last update user id
*/

CREATE Procedure [dbo].[USP_ReportSetting_InsertUpdate_ReportAttribute]
	(
@attributeid INT,
@attributename VARCHAR(300),
@shortname VARCHAR(50),
@description VARCHAR(500),
@insertdate DATETIME,
@insertby INT,
@lastupdatedate DATETIME,
@lastupdateby  INT
)

AS
BEGIN
	
if @attributeid = 0
INSERT INTO ReportAttribute
(
	AttributeName,
	ShortName,
	[Description],
	InsertDate,
	InsertBy,
	LastUpdateDate,
	LastUpdateBy
)
VALUES
(
@attributename,
@shortname,
@description,
@insertdate,
@insertby,
@lastupdatedate,
@lastupdateby

)

ELSE
UPDATE ReportAttribute
SET
	AttributeName =@attributename ,
	ShortName = @shortname,
	[Description] = @description,
	LastUpdateDate = @lastupdatedate,
	LastUpdateBy = @lastupdateby
WHERE AttributeID=@attributeid

END
GO
GRANT EXECUTE ON [dbo].[USP_ReportSetting_InsertUpdate_ReportAttribute] TO dbr_webuser
GO


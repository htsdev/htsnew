SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USP_HTS_Delete_ViolationsCategory]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[USP_HTS_Delete_ViolationsCategory]
GO










CREATE PROCEDURE USP_HTS_Delete_ViolationsCategory  
(
@CategoryID  int,
@RetStatus int=0 OUTPUT 
)
  
AS  
 
declare @Cint as int ,@Ret as int
set @Cint = (select count(*) from tblViolations where CategoryID = @CategoryID) 
Begin 
if (@Cint = 0)
Begin

	Delete tblViolationCategory
 	where CategoryID = @CategoryID  
 
	set @Ret = 1
End

else
Begin
	set @Ret = 0
End
select @RetStatus = @Ret
End
return







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


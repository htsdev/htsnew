

--  [dbo].[usp_hts_Get_ServiceTicket_Information] 172154, 13290
ALTER procedure [dbo].[usp_hts_Get_ServiceTicket_Information] --126130                      
                      
@Ticketid int,      
@Fid int                      
                      
as                       
select   
TF.ticketid_pk as ticketid_pk,  
TF.Flagid as Flagid,  
TF.RecDate as RecDate,  
isnull(TF.IsPaid,0) as IsPaid,  
isnull(TF.Fid,0) as Fid,  
TF.ContinuanceDate as ContinuanceDate,  
isnull(TF.ContinuanceStatus,0) as ContinuanceStatus,  
isnull(TF.Priority,-1)as Priority,  
isnull(TF.empid,0) as EmpID,  
isnull(TF.ServiceTicketCategory,-1)as ServiceTicketCategory,  
isnull(TF.AssignedTo,0) as AssignedTo,  
isnull(TF.ShowTrailDocket,0) as ShowTrailDocket,  
TF.ClosedDate as ClosedDate,  
isnull(TF.DeleteFlag,0)as DeleteFlag,
isnull ( TF.ShowServiceInstruction ,0) as ShowServiceInstruction,
T.GeneralComments ,
TF.ServiceTicketInstruction as ServiceTicketInstruction,
T.firstname, 
T.lastname,
isnull(TF.ShowGeneralComments,0) as ShowGeneralComments

--Added By Zeeshan Ahmed
,u.LastName + ' ' + u.FirstName as AssignedToName 
,Sc.Description as ServiceTicketCategoryName
,Case When TF.Priority = 2 then 'High' when  TF.Priority = 1 then 'Medium' else 'Low' end as PriorityDescription ,
-- Zahoor 4770
isnull(tf.ContinuanceOption,0) as ContinuanceOption,
--Sabir Khan 5738 06/08/2009 Get Current Service Ticket Follow Up Date...
ISNULL(dbo.fn_dateFormat(TF.ServiceTicketFollowupDate,27,'/',':',1),'N/A') AS servicefollowupdate

from tblticketsflag TF  
join tbltickets T   
on T.TicketId_pk = TF.ticketid_pk   

--Added By Zeeshan Ahmed 
  
left outer join tblusers U
on u.employeeid = Assignedto

left outer join tblserviceticketcategories SC
on Sc.Id = TF.ServiceTicketCategory


 where TF.ticketid_pk = @Ticketid and Fid = @Fid and Flagid = 23 

/****** Object:  UserDefinedFunction [dbo].[testfunction]    Script Date: 01/25/2008 20:29:41 ******/
DROP FUNCTION [dbo].[testfunction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[testfunction]()
returns varchar(100)
as
begin
declare @test varchar(100)
set @test = 'hello'
return @test
end
GO

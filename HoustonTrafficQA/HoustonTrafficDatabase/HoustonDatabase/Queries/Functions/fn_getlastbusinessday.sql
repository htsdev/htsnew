USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getlastbusinessday]    Script Date: 09/10/2008 12:30:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* Business Logic : This function is used to return the past business day. 

Parameter:

			@Date:	Court date
			@numofdays:	total number of business days.  

*/ 



alter  FUNCTION [dbo].[fn_getlastbusinessday] (@Date datetime,@numofdays int)
RETURNS datetime
AS
BEGIN
declare @count int
declare @LastDay datetime

set @count = 1
set @lastday = @Date
WHILE @count <= @numofdays
BEGIN
		Set @LastDay = @LastDay - 1
        SET @LastDay = CASE DATEPART(dw, @LastDay)
        WHEN 1 THEN DATEADD(d, -2, @LastDay) -- Sunday
        WHEN 7 THEN DATEADD(d, -1, @LastDay) -- Saturday
        ELSE @LastDay
        END
	set @count = @count + 1
END

return @LastDay
end

GO

grant execute on [dbo].[fn_getlastbusinessday] to dbr_webuser

GO

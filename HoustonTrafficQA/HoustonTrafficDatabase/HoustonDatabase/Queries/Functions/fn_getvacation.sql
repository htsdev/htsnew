/*********************
Business Logic: this funcation is used to get vacation dates.
**********************/
ALTER function [dbo].[fn_getvacation]()
returns varchar(2000)

as

begin

declare @table table ( rowid int identity, mdate DATETIME, rangeNum int)
DECLARE @table2 TABLE (rangenum INT , mindate DATETIME, maxdate DATETIME)
declare @idx int, @rowcount int
declare @str2 varchar(2000)
DECLARE @tempdate DATETIME
DECLARE @vacdate DATETIME
DECLARE @rangeNum INT


-- tahir 05/27/2009 chagned the format of the vacation line in required format that currently we are
-- in the report file as hard coded vacation line.
insert into @table (mdate)
select DISTINCT CONVERT(VARCHAR(10), vacationdate, 101) vacationdate from tblvacationdates
where isactivevacation = 1 and datediff(day, vacationdate, getdate())<=0 order by vacationdate

select @idx = 1, @str2 = '', @rangeNum = 0, @vacdate = NULL , @tempdate = NULL,  @rowcount = count(*) from @table

while @idx <= @rowcount
	BEGIN
		SELECT @vacdate = mdate FROM @table WHERE rowid = @idx
		IF DATEDIFF(DAY, ISNULL(@tempdate, '1/1/1900'), @vacdate) <> 1
			SET @rangeNum = @rangeNum + 1
		
		UPDATE @table SET rangeNum = @rangenum WHERE rowid  = @idx
		SET @tempdate = @vacdate
		set @idx = @idx+1 
	end

INSERT INTO @table2 SELECT DISTINCT rangenum, MIN(mdate), MAX(mdate) FROM @table GROUP BY rangeNum ORDER BY rangeNum

select @str2 = '', @rangeNum = 1, @rowcount = count(rangenum) from @table2

while @rangeNum <= @rowcount
	BEGIN
		SELECT	@str2 = @str2 + 
		--Yasir Kamal 5948 06/29/2009 put space after comma
				datename(month, mindate) + ' '+ datename(day,mindate) +', ' + datename(year,mindate) +
				CASE WHEN DATEDIFF(DAY, mindate, maxdate) = 0 
					THEN CASE (@rowcount - @rangeNum) WHEN 0 THEN '' WHEN 1 THEN ' and ' ELSE ', '
	                     END 
				    ELSE ' to ' + datename(month, maxdate) + ' '+ datename(day,maxdate) +', ' + datename(year,maxdate) + 
						CASE (@rowcount - @rangeNum) WHEN 0 THEN '' WHEN 1 THEN  ' and ' ELSE ', '  
						END 
				end

		FROM @table2 WHERE rangenum = @rangeNum

		set @rangeNum = @rangeNum + 1 
	end

return @str2

end

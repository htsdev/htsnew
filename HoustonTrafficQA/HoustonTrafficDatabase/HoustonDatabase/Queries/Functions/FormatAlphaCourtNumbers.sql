/****** Object:  UserDefinedFunction [dbo].[FormatAlphaCourtNumbers]    Script Date: 01/25/2008 20:29:12 ******/
DROP FUNCTION [dbo].[FormatAlphaCourtNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from dbo.currentdate
CREATE      Function [dbo].[FormatAlphaCourtNumbers](@CourtNumber as varchar(10))
returns varchar(10)
as
begin
	set @CourtNumber = rtrim(ltrim(@CourtNumber))
	if @CourtNumber = ' ' 
		set @CourtNumber = '0'
	if isnumeric(@CourtNumber) = 1
		set @CourtNumber = convert(varchar(10),convert(int,@CourtNumber))

	return isnull(@CourtNumber,'0')
end
GO

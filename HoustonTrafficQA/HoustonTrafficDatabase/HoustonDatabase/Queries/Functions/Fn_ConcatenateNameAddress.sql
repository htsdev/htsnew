/****** Object:  UserDefinedFunction [dbo].[Fn_ConcatenateNameAddress]    Script Date: 01/25/2008 20:28:38 ******/
DROP FUNCTION [dbo].[Fn_ConcatenateNameAddress]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.Fn_ConcatenateNameAddress('Greg','Sullo','10350 lands end')

CREATE  function [dbo].[Fn_ConcatenateNameAddress](@firstname varchar(20),@lastname varchar(20),@Address varchar(50),@Address2 varchar(50))
returns varchar(100)
as
begin
declare @Retvalue varchar(100)
set @Retvalue = isnull(@firstname,' ')+isnull(@lastname,' ')+isnull(@Address,' ')+isnull(@Address2,' ')
return @Retvalue
end
GO

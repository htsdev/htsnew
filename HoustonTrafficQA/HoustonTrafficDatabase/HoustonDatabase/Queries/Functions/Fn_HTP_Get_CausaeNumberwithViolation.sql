﻿/****** 
Create by		: Fahad Muhammad Qureshi
Created Date	: 01/12/2009 
TasK			: 5098

Business Logic  : This Function simply return the violations with Ticketsno not having violationstatusID 80 and 236 and active flag must be 1(i-e Client)

List of Parameters:	
	@TicketID	: id against which records are retrived
	
Column Return : 
	ViolationDescription
			
******/

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
CREATE function [dbo].[Fn_HTP_Get_CausaeNumberwithViolation] (@ticketid int)  
returns varchar(max)  
as  
begin  
declare @ViolationDescription varchar(max)
set @ViolationDescription = ''  
select @ViolationDescription = @ViolationDescription + CASE LEN(ISNULL(tv.RefCaseNumber,tv.casenumassignedbycourt))WHEN 0 THEN v.[Description]+ ' # ' +ISNULL(tv.RefCaseNumber,tv.casenumassignedbycourt) ELSE v.[Description]+ ' # ' +ISNULL(tv.RefCaseNumber,tv.casenumassignedbycourt)+', 'END 

from 
						dbo.tblTickets t
			INNER JOIN	dbo.tblTicketsViolations tv
					ON	t.TicketID_PK = tv.TicketID_PK  
			INNER JOIN	dbo.tblCourtViolationStatus cvs        
					ON  tv.CourtViolationStatusID = cvs.CourtViolationStatusID         
			INNER JOIN	dbo.tblViolations v        
					ON  tv.ViolationNumber_PK = v.ViolationNumber_PK         
			
where 

				t.Activeflag = 1 
			AND t.Ticketid_pk=@ticketid
			AND cvs.CourtViolationStatusID not IN(80,236) 
			

  return  substring(@ViolationDescription,1,len(@ViolationDescription)-1) 
  
END


go

grant exec on [dbo].[Fn_HTP_Get_CausaeNumberwithViolation] to dbr_webuser



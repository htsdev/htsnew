/****** 
Created by:		Sabir Khan
Business Logic:	This function is used to get slash seperated Business Name of those cases having same address.
				
List of Parameters:
	@Address:		Business Address 
	

List of Columns:	
	@QUERY:		Consist of slash seperated Business Name.	

******/
-- SELECT * FROM Fn_AN_Get_AllCompanyNameWithComma('209 JACKSON ST')
CREATE FUNCTION [dbo].[Fn_AN_Get_AllCompanyNameWithComma] 
(  
 @Address VARCHAR(MAX) 
)  
RETURNs VARCHAR(MAX)  
AS  
BEGIN  
    DECLARE @QUERY VARCHAR(MAX)  
    DECLARE @RtnValue TABLE(Id int  identity(1,1),Data VARCHAR(MAX))
    Declare @Cnt int
	Set @Cnt = 1   
      
    SET @QUERY = ''  
      
    SELECT @QUERY = @QUERY + BusinessName + '/ '  
	FROM  Business b INNER JOIN BusinessOwner bo ON bo.BusinessID = b.BusinessID  
    WHERE bo.Flags in ('Y','D','S')
    AND b.Flags in ('Y','D','S')
    AND b.NoMailflag = 0 and bo.NoMailflag = 0
    AND b.[Address] = @Address 
    AND NOT EXISTS ( 
					select tn.recordid FROM TrafficTickets.dbo.tblletternotes tn
					where tn.lettertype = 47 AND tn.RecordID = b.businessid  )           	
	
	IF(LEN(@QUERY) > 1) 
	INSERT INTO @RtnValue(DATA)
	SELECT DISTINCT DATA FROM [dbo].[Split](@QUERY,'/')
	
	SET @QUERY = ''
	
	SELECT @QUERY = @QUERY + DATA + '/ '  
	FROM  @RtnValue
		 
    SET @QUERY = LEFT(@QUERY, LEN(@QUERY) -2)  
      
    RETURN @QUERY  
END


GO
GRANT EXECUTE ON [dbo].[Fn_AN_Get_AllCompanyNameWithComma] TO dbr_webuser
GO



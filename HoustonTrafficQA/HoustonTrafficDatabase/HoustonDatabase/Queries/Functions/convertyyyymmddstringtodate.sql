/****** Object:  UserDefinedFunction [dbo].[convertyyyymmddstringtodate]    Script Date: 01/25/2008 20:28:34 ******/
DROP FUNCTION [dbo].[convertyyyymmddstringtodate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   Function [dbo].[convertyyyymmddstringtodate](@strdate as varchar(12))
returns datetime
as
begin
	set @strdate = rtrim(ltrim(@strdate))
	declare @tempdate varchar(12)
	if len(@strdate) = 8
	begin
		set @tempdate = convert(datetime,substring(@strdate,5,2) + '/' + right(@strdate,2) + '/' + left(@strdate,4))
	end
	
	else
		set @tempdate = '01/01/1900'
	return @tempdate
end
GO

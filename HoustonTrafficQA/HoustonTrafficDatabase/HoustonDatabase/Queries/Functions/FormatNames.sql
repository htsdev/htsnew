/****** Object:  UserDefinedFunction [dbo].[FormatNames]    Script Date: 01/25/2008 20:29:18 ******/
DROP FUNCTION [dbo].[FormatNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select  dbo.FormatNames('Fahim J Iqbal',2) , dbo.FormatNames('Fahim j Iqbal',1)



CREATE         Function [dbo].[FormatNames](@name as varchar(100),@nameflag as tinyint)
returns varchar(100)
as
begin

	
	declare 
	@lastname varchar(30),
	@firstname varchar(30),
	@pos int,
	@tempname varchar(60),
	@middleinitial varchar(20),
	@endpos int

	if @name is not null
	begin
		if @name <> ''
		begin
		
		set @pos = CHARINDEX(' ', @name)

		if @pos > 0
		begin
		set @lastname = SUBSTRING(@name,1, @pos-1)
		set @firstname = ltrim(SUBSTRING(@name,@pos+1,CHARINDEX(' ', @name)+1))
		set @endpos = len(@name)
		
		set @tempname = rtrim(ltrim(SUBSTRING(@name,@pos+1,@endpos-(@pos-1))))
		if CHARINDEX(' ', @tempname) > 0 
		begin
			set @middleinitial =  ltrim(SUBSTRING(@tempname,CHARINDEX(' ', @tempname),20))
		end
		else
			set @middleinitial = ''
		--set @middleinitial = ltrim(SUBSTRING(@name,CHARINDEX(' ', @name),10))
		if @nameflag = 1 
		begin
			set @tempname = @firstname
		end
		if @nameflag = 2 
		begin
			set @tempname = @lastname
		end
		if @nameflag = 3
		begin
			set @tempname = @middleinitial 
		end
		end
		end
	end
	else
	begin
		set @tempname = null
	end 
	return @tempname 
	
end
GO

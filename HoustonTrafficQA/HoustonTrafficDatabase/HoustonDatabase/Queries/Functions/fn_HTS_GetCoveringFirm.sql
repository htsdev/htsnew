
ALTER function dbo.fn_HTS_GetCoveringFirm
(
	@TicketId_pk int,
	@IsNotForRuiz varchar(5) = '0'
)

Returns varchar(max)

as

begin
	declare @Ret varchar(max)
	set @Ret = ''
	select @ret =  @ret  +  f.firmabbreviation + ','
	from tblfirm f inner join tblticketsviolations v
	on f.firmid = v.coveringfirmid
	where v.ticketid_pk = @TicketId_pk
	--Sabir Khan 9379 06/15/2011 Include only mike monk cases.
	and ((Convert(int,@IsNotForRuiz) = 0) OR (Convert(int,@IsNotForRuiz) = 1 and v.CoveringFirmID = 3041))
	
	declare @temp table ( tempstr varchar(100))
	insert into @temp select distinct * from dbo.splitstring(@ret, ',')
	set @ret = ''
	select @ret = @ret + tempstr + ', ' from @temp

	if len(@Ret) > 0
		select @Ret = left(@Ret, len(rtrim(@Ret)) -1 )
	else
		select @Ret =  isnull(@Ret,'')

	return @Ret
end

go


grant execute on dbo.fn_HTS_GetCoveringFirm to dbr_Webuser
go


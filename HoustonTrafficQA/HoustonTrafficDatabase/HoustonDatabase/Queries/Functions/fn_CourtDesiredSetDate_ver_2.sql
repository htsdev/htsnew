/****** Object:  UserDefinedFunction [dbo].[fn_CourtDesiredSetDate_ver_2]    Script Date: 01/25/2008 20:28:43 ******/
DROP FUNCTION [dbo].[fn_CourtDesiredSetDate_ver_2]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--select * from tblticketsviolations

create     function [dbo].[fn_CourtDesiredSetDate_ver_2] (@ticketsviolationid int)
RETURNS Datetime
AS
BEGIN
declare @arrdate datetime
declare @officerid int
declare @officerday varchar(10)
declare @actdate datetime
declare @count int
select distinct @arrdate=courtdatemain , @officerid=OfficerNumber 
from tbltickets T, tblticketsviolations V
where T.ticketid_pk = V.ticketid_pk
and ticketsviolationid = @ticketsviolationid
IF ISDATE(@arrdate)= 1  
	BEGIN
		SET @actdate= dateadd(day,21,@arrdate)
	    SELECT @officerday = officertype from tblofficer where officernumber_pk = @officerid
			
			
		IF NOT (ISNULL(@officerday,'NODAY') = 'NODAY')
			BEGIN
            WHILE (datename(weekday,@actdate) <> UPPER(@officerday))
				BEGIN
				SET @actdate = dateadd(day,1,@actdate)
				CONTINUE
				END
			END
		ELSE
			BEGIN
				RETURN NULL
				
			END
		
                    
			
			SELECT @count = ISNULL(PeopleAssigned830,0)  from dbo.tblcourtsettings where datediff(dd,CourtDate_PK  ,@actdate) = 0 
				
			WHILE (@Count >= 150 )              
				
				BEGIN
				SET @actdate = dateadd(day,7,@actdate)
				SELECT @count = ISNULL(PeopleAssigned830,0)  from dbo.tblcourtsettings where datediff(dd,CourtDate_PK  ,@actdate) = 0 
				END
		         
				SELECT  @actdate  =      @actdate
		
	END
	
ELSE
	BEGIN
		-- if no courtdate is available , then set it to 21 days out from today
		Select @actdate = Date from dbo.currentdate
	        SET @actdate = dateadd(day,21, @actdate)		
	END
	
	RETURN   @actdate
	
END
GO

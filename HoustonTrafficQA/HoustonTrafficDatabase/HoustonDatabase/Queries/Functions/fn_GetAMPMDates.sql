/****** Object:  UserDefinedFunction [dbo].[fn_GetAMPMDates]    Script Date: 01/25/2008 20:28:55 ******/
DROP FUNCTION [dbo].[fn_GetAMPMDates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.fn_GetAMPMDates('08/01/2005 11:30:00')

CREATE  function [dbo].[fn_GetAMPMDates](@StrDate datetime)
returns varchar(40)
as
begin
declare
@strhour int,
@strmin varchar(2),
@courttime varchar(4),
@strtimestring varchar(10),
@tempcourtdate varchar(40)




--set @courttime = '1030'
--set @CourtDate = '04/12/2004'


set @strhour = datepart(hour,@StrDate)
set @strmin = datepart(minute,@StrDate)

if len(@strmin) = 1 
	set @strmin = '0' + @strmin

set @tempcourtdate = convert(varchar(2),datepart(month,@StrDate)) + '/' + convert(varchar(2),datepart(day,@StrDate)) + '/' + convert(varchar(4),datepart(year,@StrDate))

if @strhour >= 12 
begin
	if @strhour = 12
	begin
		set @strtimestring = convert(varchar(2),@strhour) + ':' +   convert(varchar(2),@strmin) + ' ' + 'PM'
	end 
	else
	begin
		set @strtimestring = convert(varchar(2),@strhour-12) + ':' +   convert(varchar(2),@strmin) + ' ' + 'PM'
	end
end
else
begin
	set @strtimestring = convert(varchar(2),@strhour) + ':' +   convert(varchar(2),@strmin) + ' ' + 'AM'
end
set @tempcourtdate = isnull(@tempCourtDate,'01/01/1900') + ' ' + isnull(@strtimestring,'00:00:00')
return @tempcourtdate

end
GO

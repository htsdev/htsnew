/******     
Create by  : Sabir Khan    
Created Date : 01/16/2009    
Task ID   :     
Business Logic : This function is used to convert the case type of string into Capitalize each word format.
    
Parameters :
------------     
@InputString : having string data which is to be converted into the above format.
******/    
CREATE FUNCTION [dbo].[Fn_htp_get_InitCap] ( @InputString varchar(4000) ) 
RETURNS VARCHAR(4000)
AS
BEGIN

DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END

    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
        IF ((@PrevChar != '''' OR UPPER(@Char) != 'S') AND LEN(@InputString) > 2)
            SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char))
    END

    SET @Index = @Index + 1
END

RETURN @OutputString

END
GO

grant exec on dbo.Fn_htp_get_InitCap	 to dbr_webuser

GO
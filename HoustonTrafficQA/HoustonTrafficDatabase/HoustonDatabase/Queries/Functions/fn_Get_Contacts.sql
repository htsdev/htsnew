/****** Object:  UserDefinedFunction [dbo].[fn_Get_Contacts]    Script Date: 01/25/2008 20:28:52 ******/
DROP FUNCTION [dbo].[fn_Get_Contacts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    function [dbo].[fn_Get_Contacts] ( @TicketId as int)
returns varchar(200)

as 

begin

declare	@strPhone varchar(200)

select	@strPhone =''


	select 	@strPhone= @strphone + left(contact,3)+'-'+substring(contact,4,3)+'-'+substring(contact,7,len(contact))+'('+ ct.description  + '),' 
	from  	tblticketscontact  tc
	inner join
		tblcontactstype ct
	on 	tc.contacttype_pk = ct.contacttype_pk
	where 	ticketid_pk=@ticketid
	and 	contact not like '00000%'
	and 	contact not like '%0000000%'
	and 	contact not like '01000%'
	and	contact not like '111111%'


	if len(@strphone) > 1
		select @strphone= left(@strphone,len(@strphone)-1)

	return (isnull(@strPhone,''))

end
GO

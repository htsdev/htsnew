
  
CREATE FUNCTION dbo.fn_CheckFTAViolation  
(  
 @TicketViolationID int   
)  
RETURNS int  
AS  
BEGIN  
  
if ( Select Count(*)   from tblticketsviolations TV join tblviolations V On TV.ViolationNumber_PK = V.ViolationNumber_PK  
  Where V.ShortDesc like 'FTA'and  TV.TicketsViolationID = @TicketViolationID ) > 0   
return 1  
  
return 0  
  
  
END  

GO


GRANT EXEC ON fn_CheckFTAViolation TO dbr_webuser

GO



/****** Object:  UserDefinedFunction [dbo].[fn_HTS_Get_Contacts]    Script Date: 01/25/2008 20:29:05 ******/
DROP FUNCTION [dbo].[fn_HTS_Get_Contacts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[fn_HTS_Get_Contacts] ( @TicketId as int)  
returns varchar(200)  
  
as   
  
begin  
  
declare @strPhone varchar(200)  
  
select @strPhone =''  

select  @strPhone= @strphone+ left(TT.Contact1,3)+'-'+substring(TT.Contact1,4,3)+'-'+substring(TT.Contact1,7,len(TT.Contact1))+'('+ TC1.Description+'),'+left(TT.Contact2,3)+'-'+substring(TT.Contact2,4,3)+'-'+substring(TT.Contact2,7,len(TT.Contact2))+'('+ TC2.Description+'),'+left(TT.Contact3,3)+'-'+substring(TT.Contact3,4,3)+'-'+substring(TT.Contact3,7,len(TT.Contact3))+'('+ TC3.Description+'),'
FROM         dbo.tblTickets TT INNER JOIN
                      dbo.tblContactstype TC1 ON TT.ContactType1 = TC1.ContactType_PK INNER JOIN
                      dbo.tblContactstype TC2 ON TT.ContactType2 = TC2.ContactType_PK INNER JOIN
                      dbo.tblContactstype TC3 ON TT.ContactType3 = TC3.ContactType_PK
WHERE     (TT.TicketID_PK = @TicketId)

 if len(@strphone) > 1  
  select @strphone= left(@strphone,len(@strphone)-1)  
  
 return (isnull(@strPhone,''))  
end
GO

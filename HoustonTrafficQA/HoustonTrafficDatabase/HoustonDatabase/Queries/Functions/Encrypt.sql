/****** Object:  UserDefinedFunction [dbo].[Encrypt]    Script Date: 01/25/2008 20:28:35 ******/
DROP FUNCTION [dbo].[Encrypt]
GO
CREATE FUNCTION [dbo].[Encrypt](@value [nvarchar](200))
RETURNS [nvarchar](200) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [Encrypting].[Encrypt.CLRClass].[EncryptFunction]
GO

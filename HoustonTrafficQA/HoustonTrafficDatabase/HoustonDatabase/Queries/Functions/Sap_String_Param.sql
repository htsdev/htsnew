/****** Object:  UserDefinedFunction [dbo].[Sap_String_Param]    Script Date: 01/25/2008 20:29:39 ******/
DROP FUNCTION [dbo].[Sap_String_Param]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Sap_String_Param] (@str varchar(8000))  
RETURNS @CaseTypeTab TABLE  
   (  
    MainCat     int  
   )  
AS  
  
BEGIN  
declare @TempString varchar(8000)      
 WHILE CHARINDEX( ',', @str,1) <> 0        
 BEGIN  
    INSERT @CaseTypeTab     Values (convert(int,(rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( ',', @str,1) - 1)))))))  
        SELECT @TempString = SUBSTRING(@str, CHARINDEX( ',', @str) + 1, LEN(@str))        
    SELECT @str = @TempString         
 END        
     
   RETURN  
END
GO

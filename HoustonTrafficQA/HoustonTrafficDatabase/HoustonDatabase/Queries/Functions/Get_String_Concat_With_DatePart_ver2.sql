/****** Object:  UserDefinedFunction [dbo].[Get_String_Concat_With_DatePart_ver2]    Script Date: 01/25/2008 20:29:24 ******/
DROP FUNCTION [dbo].[Get_String_Concat_With_DatePart_ver2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   function [dbo].[Get_String_Concat_With_DatePart_ver2] ( 
	@str as varchar(500),
	@field_to_concat as varchar(200)
	)

returns  varchar (5000)
as


begin 

declare @CaseTypeTab TABLE ( MainCat     Varchar(1200) )

declare @TempString varchar(4000)
if(substring(@str,len(@str),len(@str))<>',')
	set @str=@str+','    

WHILE( (CHARINDEX( ',', @str,1) <> 0))
BEGIN
	INSERT @CaseTypeTab     Values(rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( ',', @str,1) - 1)))))
	SELECT @TempString = SUBSTRING(@str, CHARINDEX( ',', @str) + 1, LEN(@str))      
	SELECT @str = @TempString       
END      

set 	@TempString = '' 
select 	@TempString = @TempString +  'datediff(day, '+ @field_to_concat +','''+ MainCat + ''' ) = 0  or  '  from @casetypetab 

return  '(' +  substring (@tempstring,1,len(@tempstring)-3) + ')'

end
GO

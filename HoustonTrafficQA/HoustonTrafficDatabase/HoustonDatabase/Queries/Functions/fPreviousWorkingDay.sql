/****** Object:  UserDefinedFunction [dbo].[fPreviousWorkingDay]    Script Date: 01/25/2008 20:29:20 ******/
DROP FUNCTION [dbo].[fPreviousWorkingDay]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[fPreviousWorkingDay] (@Start datetime)
RETURNS datetime
AS
BEGIN
DECLARE @NextDay datetime

-- First, add a single day

SET @NextDay = DATEADD(d, 0, @Start)

-- Now, depending on the day of the week, move it forward
-- to the next weekday. Note this assume -- s that the first
-- day of the week is Sunday, the defaul -- t.

SET @NextDay = CASE DATEPART(dw, @NextDay)
        WHEN 1 THEN DATEADD(d, -2, @NextDay)
        WHEN 7 THEN DATEADD(d, -1, @NextDay)
        ELSE @NextDay
        END

-- Now look up the date in the holiday table, looping
-- to the next clear weekday.
/*
WHILE Exists(SELECT Holiday FROM Holidays WHERE Holiday = @NextDay) 
BEGIN
        SET @NextDay = DATEADD(d, 1, @NextDay)
        SET @NextDay = CASE DATEPART(dw, @NextDay)
        WHEN 1 THEN DATEADD(d, 1, @NextDay)
        WHEN 7 THEN DATEADD(d, 2, @NextDay)
        ELSE @NextDay
        END
END

*/

-- Finally, return the next working date

RETURN @NextDay
END
GO

﻿ USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Is_PromoClient_New]    Script Date: 10/04/2013 10:51:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sabir Khan
-- Create date: 09/24/2013
-- Description:	This function is used to check for promo price
-- =============================================
--
CREATE FUNCTION [dbo].[fn_Is_PromoClient]
(
	@TicketID as INT
)
RETURNS  bit
AS
BEGIN

DECLARE @temp TABLE(CauseNumber VARCHAR(30))
INSERT INTO @temp(CauseNumber)
SELECT tva.CauseNumber  FROM tblTickets t 
INNER JOIN tblTicketsViolations tv ON tv.TicketID_PK = t.TicketID_PK
INNER JOIN tblTicketsArchive ta ON ta.MidNumber = t.Midnum
INNER JOIN tblTicketsViolationsArchive tva ON tva.RecordID = ta.RecordID
INNER JOIN tblCourts c ON c.Courtid = tv.CourtID AND c.Courtid = tva.CourtLocation
INNER JOIN tblCourtCategories tc ON tc.CourtCategorynum = c.courtcategorynum AND tc.CourtCategorynum = 1
INNER JOIN tblLetterNotes tn ON tn.RecordID = ta.RecordID AND DATEDIFF(DAY,tn.RecordLoadDate,'10/04/2013')<=0

IF EXISTS(
		  SELECT te.CauseNumber 
		  FROM LoaderFilesArchive.dbo.tblEventExtractTemp te INNER JOIN @temp t ON t.CauseNumber =  replace(te.CauseNumber,' ','')
		  WHERE (te.attorneyname LIKE '%SULLO%' OR te.barnumber LIKE '24026218')
		)
BEGIN
	RETURN 0
END
ELSE IF EXISTS(
     			SELECT te.CauseNumber FROM LoaderFilesArchive.dbo.tblEventExtractTemp te 
     			INNER JOIN @temp t ON t.CauseNumber =  replace(te.CauseNumber,' ','')
     			WHERE (Ltrim(Rtrim(isnull(te.attorneyname,''))) <> '' OR ltrim(rTrim(isnull(te.barnumber,''))) <> '')     		
     			AND DATEDIFF(DAY,te.InsertDate,DATEADD(YEAR,-3,GETDATE()))<=0     			
			  )
     BEGIN
     	RETURN 1
     END
     ELSE
 	 BEGIN
		RETURN 0
 	 END	
 RETURN 0 
END





/****** Object:  UserDefinedFunction [dbo].[CalculateAdditionalPrice]    Script Date: 01/25/2008 20:28:29 ******/
DROP FUNCTION [dbo].[CalculateAdditionalPrice]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    Function [dbo].[CalculateAdditionalPrice](@SeqNumber int,@RefCaseNumber varchar(20),@FineAmount money,@additionaldatabaseprice money)
Returns money
AS
BEGIN
--initialize
Declare @AdditionalPrice money
Select @AdditionalPrice = 0
	IF(@SeqNumber=1 OR @SeqNumber=2)
		
		BEGIN
			IF(Left(@RefCaseNumber,1) ='0' OR Left(@RefCaseNumber,1) ='M')
			BEGIN
				IF(@FineAmount <= 170)
					BEGIN	
						Set @AdditionalPrice = 15
					END
				ELSE
					BEGIN
						Set @AdditionalPrice = 5*ceiling((0.30 * @fineamount)/5)
					END
			END
			ELSE
			BEGIN
				IF @additionaldatabaseprice = 0
					BEGIN
						Set @AdditionalPrice = round((0.35 * @fineamount),0)
					END
				ELSE
					BEGIN
						Set @AdditionalPrice = @additionaldatabaseprice
					END
			END
		END
	ELSE
		BEGIN
			IF @additionaldatabaseprice = 0
				BEGIN
					Set @AdditionalPrice = round((0.35 * @fineamount),0)
				END
			ELSE
				BEGIN
					Set @AdditionalPrice = @additionaldatabaseprice
				END
		END		
	return @AdditionalPrice
END
GO

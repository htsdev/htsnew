/****** Object:  UserDefinedFunction [dbo].[convertdatetostring]    Script Date: 01/25/2008 20:28:33 ******/
DROP FUNCTION [dbo].[convertdatetostring]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    Function [dbo].[convertdatetostring](@date as datetime)
returns varchar(10)
as
begin
	--declare @formatteddate datetime

	declare @tempdate varchar(20),
	@strday varchar(2),
	@strmonth varchar(2)
	if isdate(@date) = 1
	begin
		set @strday = convert(varchar(2),day(@date))
		if len(@strday) = 1 
		begin
			set @strday = '0' + @strday
		end
		set @strmonth = convert(varchar(2),month(@date))
		if len(@strmonth) = 1
		begin
			set @strmonth = '0' + @strmonth
		end
		set @tempdate = @strmonth + @strday +  convert(varchar(4),year(@date))
		if isdate(@tempdate) = 0
			set @tempdate = '01/01/1900'
		

	end
	return @tempdate
end
GO

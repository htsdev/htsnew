﻿ USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HTS_trialDocket_getFlags]    Script Date: 05/18/2013 13:03:33 
* Business Logic: This Function is used to Get all the distinct dates of continuance flag and display on remainder call alert report 
* 
* Created By : Rab Nawaz Khan
* Task :	11027
* Date	:	05/20/2013
* 
* ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[fn_HTS_Get_ContinuanceDatesAgainstClient] ( @TicketId AS INT)
RETURNS VARCHAR(250)
AS 

BEGIN

	DECLARE @str VARCHAR(250)
	-- Declaring Temp table
	DECLARE @Continuance TABLE (
		RowId INT IDENTITY(1,1),
		RecDate VARCHAR(50),
		ContinuanceDate VARCHAR(50)
	)	
	-- Inserting disctinct Countinuance Dates in temp table
	INSERT INTO @Continuance
	SELECT DISTINCT CONVERT(VARCHAR(50), ISNULL(RecDate, '01/01/1900'), 101), CONVERT(VARCHAR(50), ISNULL(f.ContinuanceDate, '01/01/1900'), 101)
	FROM TrafficTickets.dbo.tblticketsflag f INNER JOIN TrafficTickets.dbo.tblEventFlags e ON f.FlagID = e.FlagID_PK
	WHERE ticketid_pk = @TicketId 
	AND flagid = 9 -- Continuance. . . 	
	
	SET @str = ''
	-- If Temp table is not empty. . . 
	IF ((SELECT COUNT(*) FROM @Continuance) > 0)
	BEGIN
		SET @str = 'Continuance ('
		SELECT @str = @str + ContinuanceDate +','
		FROM @Continuance
		
		SET @str = LEFT(@str, LEN(@str) -1)
		SET @str = @str + ')'
	END

	RETURN @str

END
GO
GRANT EXECUTE ON [dbo].[fn_HTS_Get_ContinuanceDatesAgainstClient] TO dbr_webuser
GO
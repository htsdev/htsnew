/****** Object:  UserDefinedFunction [dbo].[Sap_Payment_NewLine]    Script Date: 01/25/2008 20:29:38 ******/
DROP FUNCTION [dbo].[Sap_Payment_NewLine]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[Sap_Payment_NewLine] (@str varchar(4000),@num int)  
RETURNS @CaseTypeTab Table  
   (  
    MainCat     Varchar(5000)  
   )  
AS  
  
BEGIN  
declare @TempString varchar(4000)   
declare @TempStr varchar(100)   
declare @TempStrNew varchar(5000)   
set @TempStrNew=''  
declare @TempCount int     
set @TempCount=1  
set @TempString='' 
 if @num>2
 begin 
 WHILE CHARINDEX( ',', @str,1) <> 0        
 BEGIN  
  Set @TempStr= rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( ',', @str,1) - 1))))  
   SELECT @TempString = SUBSTRING(@str, CHARINDEX( ',', @str) + 1, LEN(@str))          
  if @TempCount=1  
   begin  
    Set @TempStr=@TempStr+'					'
    Set @TempStrNew=@TempStrNew+@TempStr  
    set @TempCount=2  
   end  
  else  
   begin  
    Set @TempStr=@TempStr+char(13)  
    Set @TempStrNew=@TempStrNew+@TempStr  
    set @TempCount=1  
   end  
  SELECT @str = @TempString      
 END  
 INSERT @CaseTypeTab     Values(@TempStrNew)    
  end
else
begin
	WHILE CHARINDEX( ',', @str,1) <> 0        
 BEGIN  
  Set @TempStr= rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( ',', @str,1) - 1))))  
   SELECT @TempString = SUBSTRING(@str, CHARINDEX( ',', @str) + 1, LEN(@str))          
    Set @TempStr=@TempStr+char(13)  
    Set @TempStrNew=@TempStrNew+@TempStr  
    set @TempCount=1  
    SELECT @str = @TempString      
 END  
 INSERT @CaseTypeTab     Values(@TempStrNew)
end
               
     
   RETURN  
END
GO

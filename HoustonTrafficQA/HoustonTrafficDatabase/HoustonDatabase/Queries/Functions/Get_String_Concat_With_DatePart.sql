/****** Object:  UserDefinedFunction [dbo].[Get_String_Concat_With_DatePart]    Script Date: 01/25/2008 20:29:23 ******/
DROP FUNCTION [dbo].[Get_String_Concat_With_DatePart]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   function [dbo].[Get_String_Concat_With_DatePart]( @str as varchar(500),@field_to_concat as varchar(200),@opr as varchar(10) )
returns  varchar (5000)
as
begin 
declare @CaseTypeTab TABLE
   (
    MainCat     Varchar(1200)
   )
/*declare @str varchar(500)
set @str = '01/01/2005,02/02/2005,12/11/2006,'
*/
  declare @TempString varchar(4000)
      if(substring(@str,len(@str),len(@str))<>',')
      begin      
        set @str=@str+','    
   end
	WHILE( (CHARINDEX( ',', @str,1) <> 0))
	BEGIN
	   INSERT @CaseTypeTab     Values(rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( ',', @str,1) - 1)))))
    	   SELECT @TempString = SUBSTRING(@str, CHARINDEX( ',', @str) + 1, LEN(@str))      
	   SELECT @str = @TempString       
	END      

  --select * from @casetypetab 
    set @TempString = '' 
   select @TempString =@TempString + @field_to_concat+'  ' + @opr + '  (''' +  MainCat + ''' ) or  '  from @casetypetab 
--   Select  @tempstring
      
  return  '(' +  substring (@tempstring,1,len(@tempstring)-3) + ')'

end
GO

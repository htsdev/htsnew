/****** Object:  UserDefinedFunction [dbo].[fn_Get_MailerType]    Script Date: 01/25/2008 20:28:54 ******/
DROP FUNCTION [dbo].[fn_Get_MailerType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[fn_Get_MailerType](@ticket as int)
returns varchar(50)
 
as
begin

declare @HireDate datetime 
declare @mailerType varchar(50)
declare @ticketNo varchar(20)

SELECT     @HireDate = MIN(dbo.tblTicketsPayment.RecDate) ,@ticketNo = dbo.tblTickets.TicketNumber_PK
FROM         dbo.tblTicketsPayment INNER JOIN
                      dbo.tblTickets ON dbo.tblTicketsPayment.TicketID = dbo.tblTickets.TicketID_PK INNER JOIN
                      dbo.tblTicketsArchive ON dbo.tblTickets.TicketNumber_PK = dbo.tblTicketsArchive.TicketNumber AND 
                      dbo.tblTickets.Midnum = dbo.tblTicketsArchive.MidNumber AND dbo.tblTickets.CurrentCourtloc = dbo.tblTicketsArchive.CourtID
WHERE     (dbo.tblTickets.TicketID_PK = @ticket)
GROUP BY dbo.tblTickets.TicketNumber_PK

--select @HireDate

SELECT  @mailerType = 
	(case 		
	when isnull(MailDate_DayB4Arraign,'01/01/1900') + 2  < @HireDate then 'Day Before Arrignment' 
	when isnull(MailDateJUDGE,'01/01/1900') + 2  < @HireDate and (not(MailDateJUDGE is  null)) then 'Mail Day Judge' 
	when isnull(MailDateFTA_THREE,'01/01/1900') + 2  < @HireDate and (not(MailDateFTA_THREE is  null)) then 'MailDateFTA_THREE' 
	when isnull(MailDateFTA_TWO,'01/01/1900') + 2  < @HireDate and (not(MailDateFTA_TWO is  null)) then 'MailDateFTA_TWO' 
	when isnull(MailDateFTA,'01/01/1900') + 2  < @HireDate and (not(MailDateFTA is  null)) then 'MailDateFTA' 
	when isnull(MailDateREGULAR_TWO,'01/01/1900') + 2  < @HireDate and (not(MailDateREGULAR_TWO is  null))  then 'MailDateREGULAR_TWO'
	when isnull(MailDateREGULAR,'01/01/1900') + 2  < @HireDate and (not(MailDateREGULAR is  null)) then 'MailDateREGULAR'
	end)
	FROM         dbo.tblTicketsArchive
WHERE     (TicketNumber = @ticketNo)

return @mailerType
end
GO

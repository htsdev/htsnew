/****** Object:  UserDefinedFunction [dbo].[fn_HTS_Get_Client_Count]    Script Date: 01/25/2008 20:29:05 ******/
DROP FUNCTION [dbo].[fn_HTS_Get_Client_Count]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_HTS_Get_Client_Count]      
(      
@FirstName varchar(20),      
@LastName varchar(20),      
@Address1 varchar(50),      
@Midnum varchar(20)      
)      
RETURNS int       
      
AS      
BEGIN      
Declare @ClientCount int      
      
      
       
Set @ClientCount = (select count(*) from tbltickets      
   WHERE activeflag = 1 and  (firstname = @FirstName and lastname = @LastName  and isnull(Address1,'N/A') = isnull(@Address1,'N/A')) or midnum = @midnum)-- isnull(midnum,' ') =isnull(@midnum,' '))      
      
       
Return @ClientCount      
      
END
GO

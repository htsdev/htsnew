/****** Object:  UserDefinedFunction [dbo].[fn_MID_Get_CourtShortName]    Script Date: 01/25/2008 20:29:10 ******/
DROP FUNCTION [dbo].[fn_MID_Get_CourtShortName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fn_MID_Get_CourtShortName] (@CourtId Int )  
RETURNS varchar(35)  AS  
BEGIN 

declare	@CourtShortName	varchar(35)
select @CourtShortName = shortname  from dbo.tblcourts where courtid = @courtid
return  (isnull(@CourtShortName,'-NA-'))
 
END
GO

﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rab Nawaz Khan
-- Create date: 09/03/2013
-- Description:	This Function is used to get the Upper and lower Jail imprisonment time period for Criminal courts.
-- =============================================
CREATE FUNCTION fn_HTP_GetJailTimePeriod 
(
	-- Add the parameters for the function here
	@violationDesc VARCHAR(500),
	@chargeLevel INT
)
RETURNS VARCHAR(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar VARCHAR(100)
	DECLARE @startingLimits VARCHAR(2)
	
	-- Add the T-SQL statements to compute the return value here
	SELECT @startingLimits = LEFT(thi.MinJail, 2) 
	FROM tblHCCCViolationsInfo thi WHERE REPLACE (thi.ViolationDescription, ' ', '') = REPLACE (@violationDesc, ' ', '')
	AND thi.ChargeLevel = @chargeLevel 

	IF (ISNUMERIC(LTRIM(RTRIM(@startingLimits))) = 1)
	BEGIN
		SELECT @ResultVar = LTRIM(RTRIM(@startingLimits)) + ' - ' + LTRIM(RTRIM(thi.MaxJail))  
		FROM tblHCCCViolationsInfo thi WHERE REPLACE (thi.ViolationDescription, ' ', '') = REPLACE (@violationDesc, ' ', '')
		AND thi.ChargeLevel = @chargeLevel 		
	END
	ELSE
	BEGIN
		SELECT @ResultVar = LEFT(LTRIM(RTRIM(thi.MinJail)), 1)  + ' - ' + LTRIM(RTRIM(thi.MaxJail))  
		FROM tblHCCCViolationsInfo thi WHERE REPLACE (thi.ViolationDescription, ' ', '') = REPLACE (@violationDesc, ' ', '')
		AND thi.ChargeLevel = @chargeLevel 
		AND ISNUMERIC(LEFT(LTRIM(RTRIM(thi.MinJail)), 1)) = 1
	END

	-- Return the result of the function
	RETURN @ResultVar

END
GO


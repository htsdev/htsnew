/****** Object:  UserDefinedFunction [dbo].[fn_getviolationstatus]    Script Date: 01/25/2008 20:29:02 ******/
DROP FUNCTION [dbo].[fn_getviolationstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.fn_getofficername(115370)

CREATE     function [dbo].[fn_getviolationstatus](@violationstatus as varchar(100))
returns int
as
begin
declare @status int
select @status = isnull(courtviolationstatusid,0) from tblcourtviolationstatus where description = @violationstatus


return @status
end

--select * from tblofficer
GO

/****** Object:  UserDefinedFunction [dbo].[FTicketsCountbyViolationID]    Script Date: 01/25/2008 20:29:21 ******/
DROP FUNCTION [dbo].[FTicketsCountbyViolationID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select * from tblviolations where description like '%failure to appea%'

/*
	select *
	from tbltickets T, tblticketsviolations V, tblviolations S
	where T.ticketid_pk = V.ticketid_pk
	and V.violationnumber_pk = S.violationnumber_pk
	and (S.description like '%failure to appear%' or S.description like '%fail to appear%')
	and T.ticketid_pk = 88851


select @ticketcount = 

select count(refcasenumber) 
	from tbltickets T, tblticketsviolations V
	where T.ticketid_pk = V.ticketid_pk
	and refcasenumber like 'F%'
	and T.ticketid_pk = 

(select ticketid_pk from tblticketsviolations where
	ticketsviolationid = 699518 )
*/


--select dbo.FTicketsCountbyViolationID(706961)
--select * from tblticketsviolations where ticketsviolationid = 707675

CREATE       Function [dbo].[FTicketsCountbyViolationID](@ViolationID int)
Returns int
AS
BEGIN
	declare @ticketcount int
	select @ticketcount = count(refcasenumber) 
	from tbltickets T, tblticketsviolations V, tblviolations S
	where T.ticketid_pk = V.ticketid_pk
	and V.violationnumber_pk = S.violationnumber_pk
	and (S.description like '%failure to appear%' or S.description like '%fail to appear%')
	--and refcasenumber like 'F%'
	and T.ticketid_pk = (select ticketid_pk from tblticketsviolations where
	ticketsviolationid = @ViolationID )

	return isnull(@ticketcount,0)
END
GO

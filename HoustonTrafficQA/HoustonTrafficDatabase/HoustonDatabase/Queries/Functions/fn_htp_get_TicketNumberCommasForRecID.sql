

/*
* Task : Waqas 5932 06/22/2009
* This function is used to get comma seperated ticket number against record id.
*/
ALTER FUNCTION [dbo].[fn_htp_get_TicketNumberCommasForRecID]
(
	@Recordid INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @QUERY VARCHAR(200)
    SET @QUERY = ''
    SELECT @QUERY = @QUERY + TicketNumber_PK + ','
    FROM   tblTicketsViolationsArchive ttva
    WHERE  ttva.RecordID = @Recordid
    
    Set @Query=Left(@Query,len(@Query)-1)
    RETURN @QUERY
END
GO


GRANT EXECUTE ON dbo.[fn_htp_get_TicketNumberCommasForRecID] TO webuser_hts 
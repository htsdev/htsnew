/****** Object:  UserDefinedFunction [dbo].[fn_FormatContactNumber]    Script Date: 01/25/2008 20:28:47 ******/
DROP FUNCTION [dbo].[fn_FormatContactNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_FormatContactNumber](@ContactNumber as varchar(20), @ContactType as int) 
RETURNS varchar(40) AS  
BEGIN 

Declare @FormatedContactNumber as varchar(40)
Declare @ContactTypeDesc as varchar(10)
Declare @FirstThree as varchar(10)
Declare @SecondThree as varchar(10)
Declare @FirstFour as varchar(10)
Declare @Ext as varchar(10)

Set @FirstThree = '('+ isnull(SubString(@ContactNumber,1,3), 'XXX') + ') '
if len(ltrim(rtrim(@FirstThree)))=2
begin
	Set @FirstThree = ''
end
Set @SecondThree = isnull(SubString(@ContactNumber,4,3), 'XXXX')--7,4 changed by khalid to fix bug 2530
if len(ltrim(rtrim(@SecondThree)))=1                              --khalid 7-1-08  
begin
	Set @SecondThree = ''
end
Set @FirstFour = '-' + isnull(SubString(@ContactNumber,7, 4), 'XXXX')
if len(ltrim(rtrim(@FirstFour)))=0
begin
	Set @FirstFour = ''
end
Set @Ext = ' X-' + isnull(SubString(@ContactNumber,11, 4), 'XXXX')
if len(ltrim(rtrim(@Ext)))=2
begin
	Set @Ext = ''
end
if @ContactType <> 0
begin
	Set @ContactTypeDesc = (Select '(' + isnull(Description,'') + ')' from tblContactsType where ContactType_PK = @ContactType)
end

Set @ContactTypeDesc = isnull(@ContactTypeDesc,'')
if len(ltrim(rtrim(@ContactTypeDesc)))=2
begin
	Set @ContactTypeDesc = ''
end

--Set @FormatedContactNumber = '('+ isnull(SubString(@ContactNumber,1,3), 'XXX') + ') ' + isnull(SubString(@ContactNumber,4,3), 'XXX') + '-' + isnull(SubString(@ContactNumber,7, 4), 'XXXX') + ' X-' + isnull(SubString(@ContactNumber,11, 4), 'XXXX') +  '(' + @ContactTypeDesc + ')'
Set @FormatedContactNumber = @FirstThree + @SecondThree + @FirstFour + @Ext + @ContactTypeDesc

if len(@FormatedContactNumber) < = 7
	begin
			set @FormatedContactNumber= ''
	end 
Return @FormatedContactNumber

END
GO

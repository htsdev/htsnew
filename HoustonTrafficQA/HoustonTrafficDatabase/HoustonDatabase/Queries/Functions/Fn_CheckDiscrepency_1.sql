
/****** 
Altered by		: Sabir Khan
Altered Date	: 11/23/2009
TasK ID			: 7037

Business Logic : This function is used to check the discrepancy in auto and verified information of the client.
				 
				 
Column Return : 
				Int:
-- 0 : No Discrepency Green Check  
-- 1 : Discrepency Red Cross  
-- 2 : Discrepency But Makes A Logical Sense Yellow Check  
-- 3 : Auto and Verified Match But Court Date Is In Past Question Mark  
-- 4 : Yellow Cross ( It will not be treated as a Discrepency )
 
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USp_helptext  Fn_CheckDiscrepency_1
  --  select dbo.Fn_CheckDiscrepency_1(1)  
      
Alter FUNCTION [dbo].[Fn_CheckDiscrepency_1]
(
	@TicketsViolationID AS INT
)
RETURNS INT
AS
BEGIN
    DECLARE @autoCourtDate DATETIME          
    DECLARE @autoCourtViolationstatusid INT          
    DECLARE @autocourtnumber VARCHAR(3)--Afaq 8059 09/24/2010 Change datatype         
    DECLARE @AutoTypeId INT          
    
    DECLARE @VerifiedCourtDate DATETIME          
    DECLARE @VerifiedCourtViolationstatusid INT          
    DECLARE @Verifiedcourtnumber VARCHAR(3)  --Afaq 8059 09/24/2010 Change datatype         
    DECLARE @VerifiedTypeId INT          
    
    
    DECLARE @ScanCourtDate DATETIME          
    DECLARE @ScanCourtViolationstatusid INT          
    DECLARE @Scancourtnumber VARCHAR(3) --Afaq 8059 09/24/2010 Change datatype       
    DECLARE @ScanTypeId INT            
    
    DECLARE @CourtDateMain DATETIME  
    
    SELECT @autoCourtDate = CourtDate,
           @autoCourtViolationstatusid = CourtViolationStatusid,
           @autocourtnumber = CourtNumber,
           @VerifiedCourtDate = CourtDateMain,
           @VerifiedCourtViolationstatusid = CourtViolationStatusidMain,
           @Verifiedcourtnumber = CourtNumberMain,
           @ScanCourtDate = CourtDateScan,
           @ScanCourtViolationstatusid = CourtViolationStatusidscan,
           @Scancourtnumber = CourtNumberscan,
           @CourtDateMain = CourtDateMain
    FROM   tblticketsviolations
    WHERE  ticketsviolationid = @TicketsViolationID 
    
    
    
    -----------------------------------------------------------------------------------------------------------------------
    -- ADDED BY TAHIR DT:4/11/07
    -- DO NOT DISPLAY DISCREPANCY IF CATEGORY ID IS SAME....
    -----------------------------------------------------------------------------------------------------------------------        
    SELECT @AutoTypeId = categoryid
    FROM   tblcourtviolationstatus
    WHERE  courtviolationstatusid = @autoCourtViolationstatusid
    
    SELECT @VerifiedTypeId = categoryid
    FROM   tblcourtviolationstatus
    WHERE  courtviolationstatusid = @VerifiedCourtViolationstatusid
    
    SELECT @ScanTypeId = categoryid
    FROM   tblcourtviolationstatus
    WHERE  courtviolationstatusid = @ScanCourtViolationstatusid        
    
    SELECT @AutoTypeId = ISNULL(@AutoTypeId, 0),
           @VerifiedTypeId = ISNULL(@VerifiedTypeId, 0),
           @ScanTypeId = ISNULL(@ScanTypeId, 0) 
    
    -- DO NOT CONSIDER DISCREPANCY
    -- IF ALL THE THREE STATUSES ARE DISPOSED        
    IF (
           @AutoTypeid = 50
           AND @VerifiedTypeId = 50
           AND @ScanTypeId = 50
       )
        RETURN 0 
    
    --Sabir Khan 7037 11/23/2009
    --DO NOT CONSIDER DISCREPANCY IF BOTH AUTO AND VERIFIED ARE DISPOSED.
    IF (@AutoTypeid = 50 AND @VerifiedTypeId = 50)
        RETURN 0 
        
        -- DO NOT CONSIDER DISCREPANCY
        -- IF AUTO STATUS = 'ARRAIGNMENT' AND VERIFIED STATUS = 'ARRAIGNMENT WAITING'
    ELSE 
    IF (@AutoTypeId = 2)
       AND @VerifiedCourtViolationstatusid = 201
        RETURN 0 
        
        -- DO NOT CONSIDER DISCREPANCY
        -- IF AUTO STATUS = 'FTA' OR 'DLQ' AND VERIFIED STATUS = 'BOND'
    ELSE 
    IF (
           @autoCourtViolationstatusid = 146
           OR @autoCourtViolationstatusid = 43
           OR @autoCourtViolationstatusid = 186
           OR @autoCourtViolationstatusid = 175
       )
       AND @VerifiedCourtViolationstatusid = 135
        RETURN 0 
        
        -- DO NOT CONSIDER DISCREPANCY
        -- IF AUTO STATUS = 'BOND' OR 'DLQ' AND VERIFIED STATUS = 'BOND WAITING'
    ELSE 
    IF (
           @autoCourtViolationstatusid = 135
           OR @autoCourtViolationstatusid = 43
           OR @autoCourtViolationstatusid = 146
           OR @autoCourtViolationstatusid = 175
       )
       AND @VerifiedCourtViolationstatusid = 202
        RETURN 0 
        
        -- ADDED BY ZEESHAN AHMED 3486 04/01/2008
        -- DO NOT CONSIDER DISCREPENCY BUT DISPLAY YELLOW CHECK
        -- IF AUTO STATUS = 'JUR' AUTO COURT DATE IS IN FUTURE AND VERIFIED STATUS = 'A/W'
    ELSE 
    IF (
           @AutoTypeId = 4
           AND DATEDIFF(DAY, GETDATE(), @autoCourtDate) > 0
           AND @VerifiedCourtViolationstatusid = 201
       )
        RETURN 2
        
        -------------------------------------------------------------------------
        -- ADDED BY ZEESHAN 18/4/2007   CHANGED ON 13/11/2007
        -------------------------------------------------------------------------
        -- DO NOT CONSIDER DISCREPANCY BUT DISPLAY YELLOW CHECK
        -- IF AUTO STATUS = 'FTA' AND VERIFIED STATUS = 'MIS'
    ELSE 
    IF @autoCourtViolationstatusid = 186
       AND @VerifiedCourtViolationstatusid = 105
        RETURN 2 
        
        -- DO NOT CONSIDER DISCREPANCY BUT DISPLAY YELLOW CHECK
        -- IF AUTO STATUS = 'DLQ' OR AUTO STATUS = 'MIS' AND VERIFIED STATUS = 'MIS'
    ELSE 
    IF (
           @autoCourtViolationstatusid = 146
           OR @autoCourtViolationstatusid = 175
           OR @VerifiedCourtViolationstatusid = 105
       )
       AND @VerifiedCourtViolationstatusid = 105
        RETURN 2 
        
        -------------------------------------------------------------------------
        -- ADDED BY ZEESHAN 11/6/2007  CHANGED ON 13/6/2007
        -------------------------------------------------------------------------
        -- DO NOT CONSIDER DISCREPANCY
        -- IF AUTO STATUS = 'WAIT' AND VERIFIED STATUS = 'WAIT'
    ELSE 
    IF (
           @autoCourtViolationstatusid = 104
           AND @VerifiedCourtViolationstatusid = 104
       )
        RETURN 0 
        
        -- DO NOT CONSIDER DISCREPANCY BUT DISPLAY YELLOW CHECK
        -- If auto status = NON and verified status = DISPOSED
    ELSE 
    IF (
           @autoCourtViolationstatusid = 15
           OR @autoCourtViolationstatusid = 161
       )
       AND @VerifiedCourtViolationstatusid = 80
        RETURN 2 
        
        -- CHANGED BY Zeeshan Ahmed 04/01/2008 3486
        -- DO NOT CONSIDER DISCREPANCY BUT DISPLAY YELLOW CHECK
        -- if  auto status = DEF and verified status = DISPOSED
    ELSE 
    IF (
           @autoCourtViolationstatusid = 153
           OR @autoCourtViolationstatusid = 154
           OR @autoCourtViolationstatusid = 178
           OR @autoCourtViolationstatusid = 216
       )
       AND @VerifiedCourtViolationstatusid = 80
        RETURN 2 
        
        --------------------------------------------------------------------------
        -- Added By Zeeshan Ahmed On 13/6/2007
        --------------------------------------------------------------------------  
        
        -- DO NOT CONSIDER DISCREPENCY BUT DISPLAY YELLOW CHECK
        -- IF AUTO STATUS = 'FTA' AND VERIFIED STATUS = 'B/W'
    ELSE 
    IF @autoCourtViolationstatusid = 186
       AND @VerifiedCourtViolationstatusid = 202
        RETURN 2 
        
        --------------------------------------------------------------------------  
        
        -- CONSIDER DISCREPANCY
        -- IF VERIFIED & AUTO COURT NUMBERS & COURT DATES ARE NOT SAME
    ELSE 
    IF (
           @autoCourtDate != @VerifiedCourtDate
           OR @autocourtnumber != @Verifiedcourtnumber
       )
       AND @VerifiedCourtViolationstatusid != 105
        RETURN 1 
        
        --------------------------------------------------------------------------
        -- Added By Zeeshan Ahmed On 13/6/2007
        --------------------------------------------------------------------------
        -- DISPLAY QUESTION MARK IF AUTO AND VERIFIED STATUS ARE SAME BUT COURT DATE HAS PASSED
    ELSE 
    IF @AutoTypeid = @VerifiedTypeId
       AND @CourtDateMain < GETDATE()
        RETURN 3 
        --------------------------------------------------------------------------  
        
        --------------------------------------------------------------------------
        -- Added By Asghar Ali On 16/8/2007
        --------------------------------------------------------------------------
        -- DISPLAY Yellow Mark  IF AUTOTYPE IS "NON" AND VERFIED IS "DISPOSE"
    ELSE 
    IF @autoCourtViolationstatusid = 15
       AND @VerifiedCourtViolationstatusid = 80
        RETURN 4 
        --------------------------------------------------------------------------  
        
        -- DO NOT CONSIDER DISCREPANCY
        -- IF AUTO STATUS = VERIFIED STATUS
    ELSE 
    IF @AutoTypeid = @VerifiedTypeId
        RETURN 0 
        
        -- ELSE CONSIDER AS DISCREPANCY
    ELSE
        RETURN 1 
    
    
    RETURN 0
END

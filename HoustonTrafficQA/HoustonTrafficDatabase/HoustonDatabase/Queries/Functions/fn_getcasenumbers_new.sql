/****** Object:  UserDefinedFunction [dbo].[fn_getcasenumbers_new]    Script Date: 01/25/2008 20:28:55 ******/
DROP FUNCTION [dbo].[fn_getcasenumbers_new]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE        function [dbo].[fn_getcasenumbers_new](@TicketNumber as varchar(20))
returns varchar(500)
as
begin
declare @casenumber varchar(500)
select  @casenumber= coalesce(@casenumber + ', ' , '') +  TicketNumber 
from traffictickets.dbo.tblticketsarchive
where firstname+lastname+address1 in 
(select firstname+lastname+address1 from  traffictickets.dbo.tblticketsarchive where TicketNumber =  @TicketNumber 
--and convert(datetime,convert(char(12),listdate,101),101) between  @startdate and @enddate 
)
and courtid = 3013
return @casenumber





end
GO

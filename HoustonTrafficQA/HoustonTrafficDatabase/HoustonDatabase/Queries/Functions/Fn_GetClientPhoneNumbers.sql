/****** Object:  UserDefinedFunction [dbo].[Fn_GetClientPhoneNumbers]    Script Date: 01/25/2008 20:28:56 ******/
DROP FUNCTION [dbo].[Fn_GetClientPhoneNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select dbo.Fn_GetClientPhoneNumbers(90375) 
CREATE          function [dbo].[Fn_GetClientPhoneNumbers](@TicketID int)  
returns varchar(100)  
as
begin  

DECLARE @PhoneNo varchar(100)
set @PhoneNo = ' '
select 	 @PhoneNo = @PhoneNo + dbo.formatphonenumbers(ltrim(rtrim(contact))) + '(' + left(description,1) + ')<br>' 
	from  	tblticketscontact  T, tblContactsType C
	where T.contacttype_pk = C.contacttype_pk
	and 	ticketid_pk=@TicketID
	and 	contact not like '00000%'
	and 	contact not like '%0000000%'
	and 	contact not like '01000%'  
   


return( ltrim(@phoneno))
end
GO

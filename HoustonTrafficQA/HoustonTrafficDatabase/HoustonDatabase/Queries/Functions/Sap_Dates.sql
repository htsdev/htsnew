/****** Object:  UserDefinedFunction [dbo].[Sap_Dates]    Script Date: 01/25/2008 20:29:37 ******/
DROP FUNCTION [dbo].[Sap_Dates]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Sap_Dates] (@str varchar(4000))
RETURNS @CaseTypeTab TABLE
   (
    MainCat     Varchar(12)
   )
AS

BEGIN
declare @TempString varchar(4000)    
	WHILE CHARINDEX( ',', @str,1) <> 0      
	BEGIN
	   INSERT @CaseTypeTab     Values(rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( ',', @str,1) - 1)))))
    	   SELECT @TempString = SUBSTRING(@str, CHARINDEX( ',', @str) + 1, LEN(@str))      
	   SELECT @str = @TempString       
	END      
   
   RETURN
END
GO

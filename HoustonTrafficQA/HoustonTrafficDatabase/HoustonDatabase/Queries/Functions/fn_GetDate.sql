/******
Created by:		Abbas Shahid Khwaja
Business logic : This function is used to get the date and convert the format of the date.
Parameters: 
----------
@month:  It takes month type
This function returns the date in differnet format 
*******/
CREATE Function [dbo].[fn_GetDate] (@month as VARCHAR(30))
Returns DATETIME
As
BEGIN

DECLARE @date DATETIME
DECLARE @monthofyear INT
DECLARE @year INT
DECLARE @mont VARCHAR(10)
SELECT @mont = SUBSTRING(@month,1,CHARINDEX('-',@month,0)-1)
SELECT @year = SUBSTRING(@month,CHARINDEX('-',@month,0)+1,LEN(@month) + 1)
IF @mont = 'Jan'
SET @monthofyear = 1
ELSE IF @mont = 'Feb'
SET @monthofyear = 2
ELSE IF @mont = 'March'
SET @monthofyear = 3
ELSE IF @mont = 'April'
SET @monthofyear = 4
ELSE IF @mont = 'May'
SET @monthofyear = 5
ELSE IF @mont = 'June'
SET @monthofyear = 6
ELSE IF @mont = 'July'
SET @monthofyear = 7
ELSE IF @mont = 'August'
SET @monthofyear = 8
ELSE IF @mont = 'Sep'
SET @monthofyear = 9
ELSE IF @mont = 'Oct'
SET @monthofyear = 10
ELSE IF @mont = 'Nov'
SET @monthofyear = 11
ELSE IF @mont = 'Dec'
SET @monthofyear = 12

SET @date = CONVERT(DATETIME,CONVERT(VARCHAR,@monthofyear)+'/1/'+ CONVERT(VARCHAR,@year))

RETURN @date
END 
GO



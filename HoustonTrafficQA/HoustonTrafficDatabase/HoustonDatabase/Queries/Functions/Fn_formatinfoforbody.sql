/****** Object:  UserDefinedFunction [dbo].[Fn_formatinfoforbody]    Script Date: 01/25/2008 20:28:49 ******/
DROP FUNCTION [dbo].[Fn_formatinfoforbody]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    function [dbo].[Fn_formatinfoforbody](@date as datetime,@courtnum as varchar(2))
returns varchar(30)
as
begin
declare @strhour varchar(2)
declare @strmin varchar(3)
declare @strtimestring varchar(10)
declare @tempdate varchar(30)
declare @strmonth varchar(2)
declare @strday varchar(2)
declare @strtemphour varchar(2)
if isdate(@date) = 1
	begin
		set @strhour = datepart(hour,@date)
		set @strmin = convert(varchar(2),datepart(n,@date))
		set @strtemphour = @strhour
		if len(@strtemphour) = 1
			begin
				set @strtemphour = '0' + @strtemphour
			end
		if len(@strmin) = 1
			begin
				set @strmin = '0' + @strmin
			end
		else
			begin
				set @strmin =  @strmin
			end
		
		if @strhour >= 12 
			begin
				if @strhour = 12
					
					begin
						
						set @strtimestring = convert(varchar(2),@strtemphour) +   @strmin  + 'P'
					end 
				else
					begin
						set @strtemphour= @strhour-12
						if len(@strtemphour) = 1
						begin
							set @strtemphour = '0' + @strtemphour
						end
						set @strtimestring = convert(varchar(2),@strtemphour) + @strmin  + 'P'
					end
			end
		else
			begin
				set @strtimestring = convert(varchar(2),@strtemphour) +    @strmin  + 'A'
			end
		if @courtnum is not null
		begin
			if len(@courtnum) = 1 
			begin
				set @courtnum = '0' + @courtnum 
			end
		end
		else
			begin
				set @courtnum = 'NA'
			end
			
		
			
		
		
		set @tempdate = 'ct#' + @courtnum + '@' + @strtimestring
	end
	
return @tempdate
end
GO

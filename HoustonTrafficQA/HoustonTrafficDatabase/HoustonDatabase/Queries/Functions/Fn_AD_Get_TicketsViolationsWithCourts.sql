﻿ /****** 
Created by:		Syed Muhammad Ozair
Business Logic:	The function is used in Auto Dialer OutCome Details. It returns a string that contains 
				complete violation witn court information (violation description, court date, time, status, room number, court short name) separated
								
				
				
List of Parameters:
	@TicketID:	Ticket ID associated with the case.
	@CourtDate:	Court date associated with the violation.

List of Columns:	
 
*******/

alter function [dbo].[Fn_AD_Get_TicketsViolationsWithCourts] 
	(
		@ticketid int,
		@courtdate datetime
	)          

returns varchar(max)          

as          

begin          
        
declare Cur_Violations cursor for        
select distinct  
	ttv.courtdatemain,
	courtnumbermain = 
		(
		--Ozair 8034 07/19/2010 resolve issue of conversion caused by 7979
		case isnull(ttv.courtnumbermain,'0')
			when '0' then ''  
			else '#'+ttv.courtnumbermain  
		end
		),  
	dt.description,
	c.courtname+' at '+	c.Address + ' ' + c.City + ', Texas ' + c.Zip AS court           
from 
	tblticketsviolations ttv 
inner join         
    tblcourtviolationstatus cvs 
on	ttv.courtviolationstatusidmain=cvs.CourtViolationStatusID 
INNER JOIN                        
    dbo.tblDateType dt 
ON	cvs.CategoryID = dt.TypeID
--ozair 4837 10/08/2008
inner join 
	tblcourts c
on  c.courtid=ttv.courtid          

where	ttv.courtviolationstatusidmain<>80    
and		ticketid_pk=@ticketid     
AND  DATEDIFF(day, ttv.CourtDateMain,@courtdate) between -7 and 0   
        
declare @date as datetime          
declare @cnum as varchar(3)        
declare @description as varchar(50)
declare @court as varchar(500)       
declare @string as varchar(max)              
set @string=' '         
        
OPEN Cur_Violations        
        
-- Get the first row.           
FETCH NEXT FROM Cur_Violations          
INTO @date, @cnum,@description,@court    
        
WHILE (@@FETCH_STATUS = 0)          
begin       
set @string=@string+left(convert(varchar,@date,101),10)+' @'+right (convert(varchar,@date,0),7)+' - '+@court+' '+@cnum+' - '+@description+'<br>'        
        
FETCH NEXT FROM Cur_Violations          
INTO @date, @cnum,@description,@court   
          
END           
          
CLOSE Cur_Violations          
DEALLOCATE Cur_Violations        
        
return @string        
        
end

go 

grant exec on [dbo].[Fn_AD_Get_TicketsViolationsWithCourts] to dbr_webuser
go
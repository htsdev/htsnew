

/*
* Task : Waqas 5932 06/22/2009
* This function is used to get comma seperated ticket number against record id.
*/
CREATE FUNCTION [dbo].[fn_hts_get_TicketNumberCommasForRecID]
(
	@Recordid INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @QUERY VARCHAR(200)
    SET @QUERY = ''
    SELECT @QUERY = @QUERY + TicketNumber_PK + ','
    FROM   tblTicketsViolationsArchive ttva
    WHERE  ttva.RecordID = @Recordid
    
    RETURN @QUERY
END
GO


GRANT EXECUTE ON dbo.[fn_hts_get_TicketNumberCommasForRecID] TO webuser_hts 
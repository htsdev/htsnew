/*
* Created By: Babar Ahmad
* Task: 9660
* Date: 09/07/2011
* 
* Business Logic: This function returns Mobile Number, if not available then Home, if not available then Work, if not available then Other.   
* 
* Input Parameter: @TicketId
* 
* Returns: @PhoneNumber
*/


ALTER function [dbo].[fn_HTP_Get_ContactNumber] ( @TicketId as int)  
returns varchar(200)  
  
as   
  
begin  
  
declare @PhoneNumber varchar(200)  
  
select @PhoneNumber ='' 

SELECT @PhoneNumber =  ISNULL(CASE t.contacttype1 WHEN 4 THEN dbo.formatphonenumbers(t.contact1) 
			  ELSE 
			  CASE t.contacttype2 WHEN 4 THEN dbo.formatphonenumbers(t.contact2)
			  ELSE 
			  CASE t.contacttype3 WHEN 4 THEN dbo.formatphonenumbers(t.contact3) 
			  ELSE 
			  CASE t.contacttype1 WHEN 1 THEN dbo.formatphonenumbers(t.contact1) 
			  ELSE 
			  CASE t.contacttype2 WHEN 1 THEN dbo.formatphonenumbers(t.contact2) 
			  ELSE 
			  CASE t.contacttype3 WHEN 1 THEN dbo.formatphonenumbers(t.contact3)
			  ELSE 
			  CASE t.contacttype1 WHEN 2 THEN dbo.formatphonenumbers(t.contact1) 
			  ELSE 
			  CASE t.contacttype2 WHEN 2 THEN dbo.formatphonenumbers(t.contact2)
			  ELSE 
			  CASE t.contacttype3 WHEN 2 THEN dbo.formatphonenumbers(t.contact3)
			  ELSE CASE WHEN t.contacttype1 = 100 THEN  dbo.formatphonenumbers(t.contact1)
			  WHEN t.contacttype2 = 100 THEN dbo.formatphonenumbers(t.contact2)
			  WHEN  t.contacttype3 =100 THEN  dbo.formatphonenumbers(t.contact3)
			  END															  			  
			  END 
			  END 
			  END 
			  END 
			  END 
			  END 
			  END 
			  END 
			  END			
			,dbo.formatphonenumbers(ISNULL(contact1,ISNULL(contact2,ISNULL(contact3,''))))) 
	
FROM tbltickets t WHERE t.TicketID_PK = @TicketId

 return (isnull(@PhoneNumber,''))  

END


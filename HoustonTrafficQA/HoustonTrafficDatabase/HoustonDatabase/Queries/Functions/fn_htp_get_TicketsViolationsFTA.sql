/******     
Created by:  Yasir Kamal   
TaskId: 5623
Dated:03/25/2009
Business Logic: The function is used in Missed Court Date calls. It returns a string that contains     
    complete case information (court date, time, status, room number) separated    
    by a line break.    
        
List of Parameters:    
 @TicketID: Ticket ID associated with the case.    
 @CourtDate: Court date associated with the violation.    
    

    
*******/    
    
ALTER function [dbo].[fn_htp_get_TicketsViolationsFTA]     
 (    
  @ticketid int,    
  @courtdate datetime    
 )              
    
returns varchar(500)              
    
as              
    
begin                      
declare Cur_Violations cursor for            
select distinct      
 case when len(isnull(ttv.refcasenumber,'')) = 0 then ttv.casenumassignedbycourt else ttv.refcasenumber end ,   
 ttv.courtdatemain,  
 courtnumbermain =     
  (    --Sabir Khan 7979 07/14/2010 check for sugarland court for couurt room number
  case isnull(ttv.courtnumbermain,'0')    
   when '0' then ''      
   else '#'+ttv.courtnumbermain      
  end    
  ),      
 dt.description,    

 c.shortname             
from     
 tblticketsviolations ttv     
inner join             
    tblcourtviolationstatus cvs     
on ttv.courtviolationstatusidmain=cvs.CourtViolationStatusID     
INNER JOIN                            
    dbo.tblDateType dt     
ON cvs.CategoryID = dt.TypeID    
inner join     
 tblcourts c    
on  c.courtid=ttv.courtid              
    
where ttv.courtviolationstatusidmain<>80        
and  ticketid_pk=@ticketid     
and ttv.courtviolationstatusidMAIN in (146,186,105)	    

AND  DATEDIFF(day, ttv.CourtDateMain,@courtdate) = 0      
order by ttv.courtdatemain
            
declare @date as datetime              
declare @cnum as varchar(3)            
declare @description as varchar(50)    
  
declare @court as varchar(9)           
  
declare @string as varchar(500)                  
declare @TicketNumber varchar(500)  
set @string=' '             
            
OPEN Cur_Violations            
            
         
FETCH NEXT FROM Cur_Violations              
INTO @TicketNumber,@date,@cnum,@description,@court     
            
WHILE (@@FETCH_STATUS = 0)              
begin        
set @string=@string+@TicketNumber + ' - ' +left(convert(varchar,@date,101),10)+' @'+right (convert(varchar,@date,0),7)+' - '+@court+' '+@cnum+' - '+@description+'<br>'            
            
FETCH NEXT FROM Cur_Violations              
INTO @TicketNumber,@date, @cnum,@description,@court              
              
END               
              
CLOSE Cur_Violations              
DEALLOCATE Cur_Violations            
            
return @string            
            
end 

go
grant execute on [dbo].[fn_htp_get_TicketsViolationsFTA]  to dbr_webuser
go
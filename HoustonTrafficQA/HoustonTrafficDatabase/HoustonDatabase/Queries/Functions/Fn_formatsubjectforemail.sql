/****** Object:  UserDefinedFunction [dbo].[Fn_formatsubjectforemail]    Script Date: 01/25/2008 20:28:51 ******/
DROP FUNCTION [dbo].[Fn_formatsubjectforemail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    function [dbo].[Fn_formatsubjectforemail](@date as datetime,@officerday as varchar(10),@datetypeflag as int)
returns varchar(30)
as
begin
declare @strhour varchar(2)
declare @strmin varchar(3)
declare @strtimestring varchar(10)
declare @tempdate varchar(30)
declare @strmonth varchar(2)
declare @strday varchar(2)
declare @strtemphour varchar(2)
declare @datetype varchar(3)
if isdate(@date) = 1
	if @datetypeflag = 2
	
		begin
			set @datetype = 'arr'
		end
	if @datetypeflag = 5
		begin
			set @datetype = 'jud'
		end
		
			
		
		set @strmonth = convert(varchar(2),month(@date))
		set @strday = convert(varchar(2),day(@date))
		if len(@strmonth) = 1 
		begin
			set @strmonth = '0' + @strmonth
		end
		if len(@strday) = 1 
		begin
			set @strday = '0' + @strday
		end
		set @tempdate = @datetype + @strmonth + '/' + @strday +  left(@officerday,2) 
return @tempdate
		
end
--end
GO

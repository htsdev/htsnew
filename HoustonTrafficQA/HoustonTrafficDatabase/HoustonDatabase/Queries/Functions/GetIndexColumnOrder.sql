/****** Object:  UserDefinedFunction [dbo].[GetIndexColumnOrder]    Script Date: 01/25/2008 20:29:29 ******/
DROP FUNCTION [dbo].[GetIndexColumnOrder]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Returns whether the column is ASC or DESC 
CREATE FUNCTION [dbo].[GetIndexColumnOrder] 
( 
    @object_id INT, 
    @index_id TINYINT, 
    @column_id TINYINT 
) 
RETURNS NVARCHAR(5) 
AS 
BEGIN 
    DECLARE @r NVARCHAR(5) 
    SELECT @r = CASE INDEXKEY_PROPERTY 
    ( 
        @object_id, 
        @index_id, 
        @column_id, 
        'IsDescending' 
    ) 
        WHEN 1 THEN N' DESC' 
        ELSE N'' 
    END 
    RETURN @r 
END
GO

/****** Object:  UserDefinedFunction [dbo].[fn_getofficername]    Script Date: 01/25/2008 20:28:59 ******/
DROP FUNCTION [dbo].[fn_getofficername]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.fn_getofficername('0900.00')

CREATE     function [dbo].[fn_getofficername](@officerNumber as varchar(20))
returns varchar(100)
as
begin
declare @casenumber varchar(100)
	if isnumeric(@officerNumber) = 1 and charindex('.',@officerNumber) = 0
	begin

	select  @casenumber= firstname + ' ' + lastname
	from traffictickets.dbo.tblofficer where officernumber_pk = convert(int,@officerNumber)
	end
return @casenumber
end

--select * from tblofficer
GO

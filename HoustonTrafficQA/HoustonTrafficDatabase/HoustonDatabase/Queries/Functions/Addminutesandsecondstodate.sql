USE [TrafficTickets]
GO
/****** Object:  UserDefinedFunction [dbo].[Addminutesandsecondstodate]    Script Date: 01/25/2008 20:28:28 ******/
DROP FUNCTION [dbo].[Addminutesandsecondstodate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  Function [dbo].[Addminutesandsecondstodate](@date as datetime,@time as varchar(4))
returns datetime
as
begin
	return dateadd(minute,convert(int,right(@time,2)),dateadd(hour,convert(int,left(@time,2)),@date))
end
GO

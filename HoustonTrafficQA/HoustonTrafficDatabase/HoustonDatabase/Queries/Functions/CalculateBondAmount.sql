/****** Object:  UserDefinedFunction [dbo].[CalculateBondAmount]    Script Date: 01/25/2008 20:28:30 ******/
DROP FUNCTION [dbo].[CalculateBondAmount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE      Function [dbo].[CalculateBondAmount](@SeqNumber int,@RefCaseNumber varchar(20),@FineAmount money,@bonddatabaseamount money)
Returns money
AS
BEGIN
--initialize
Declare @bondamount money
Select @bondamount = 0
	IF(@SeqNumber=1 OR @SeqNumber=2) 
		
		BEGIN
			IF(Left(@RefCaseNumber,1) ='0' OR Left(@RefCaseNumber,1) ='M' OR Left(@RefCaseNumber,1) ='1')
			BEGIN
				IF(@FineAmount <= 125)
					BEGIN	
						Set @bondamount = 150
					END
				ELSE
					BEGIN
						Set @bondamount = @FineAmount + 25
							
						
					END
			END
		END
	else
		BEGIN
			Set @bondamount = @bonddatabaseamount
		END
	RETURN  @bondamount
END
GO

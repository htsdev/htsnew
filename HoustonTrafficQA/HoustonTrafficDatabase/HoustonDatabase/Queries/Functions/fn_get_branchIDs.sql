USE TrafficTickets
GO
/****** Business Logic:
* This function is used to get branch ids if all is selected from branch drop down else return the selected branch id. 
* Task ID: 10920
* Created By Sabir Khan
* Date: 05/27/2013
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_get_branchIDs] (@branchid int)  
RETURNS @tblBranch Table  
   (  
    branchid  int  
   )  
AS  
  
BEGIN  
IF @branchid =1
BEGIN
	INSERT INTO @tblBranch
	SELECT branchid FROM branch WHERE BranchId IN (2,3)
END
ELSE
	BEGIN
		INSERT INTO @tblBranch
		SELECT 	@branchID
	END
               
     
   RETURN  
END


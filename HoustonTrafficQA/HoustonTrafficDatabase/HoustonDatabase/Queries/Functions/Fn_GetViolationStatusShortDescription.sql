/****** Object:  UserDefinedFunction [dbo].[Fn_GetViolationStatusShortDescription]    Script Date: 01/25/2008 20:29:03 ******/
DROP FUNCTION [dbo].[Fn_GetViolationStatusShortDescription]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create    function [dbo].[Fn_GetViolationStatusShortDescription](@violationstatusid as int)
returns varchar(3)
as
begin
	declare @ReturnString varchar(3)

	select @ReturnString = isnull(shortdescription,'N/A') from tblcourtviolationstatus where courtviolationstatusid = @violationstatusid
	return @ReturnString
end


--select * from tblcourtviolationstatus
GO

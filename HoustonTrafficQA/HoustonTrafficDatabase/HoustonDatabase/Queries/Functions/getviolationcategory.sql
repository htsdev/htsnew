/****** Object:  UserDefinedFunction [dbo].[getviolationcategory]    Script Date: 01/25/2008 20:29:34 ******/
DROP FUNCTION [dbo].[getviolationcategory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--select * from tblcourtviolationstatus


--select * from tblcourtviolationstatus where description like 'jury trial%'
--select * from tblcourtviolationstatus where description like 'non jury%'
--select dbo.getcourtstatus(4)

create      Function [dbo].[getviolationcategory](@statusid as int)
returns int
as
begin
	declare @violationcategory int
	
	select @violationcategory = categoryid from tblcourtviolationstatus where courtviolationstatusid  = @statusid
	return @violationcategory
end


--select * from tblcourtviolationstatus where description like '%jury%'
GO

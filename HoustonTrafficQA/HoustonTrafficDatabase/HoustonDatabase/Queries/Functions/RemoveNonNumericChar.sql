USE TrafficTickets
GO

CREATE FUNCTION dbo.RemoveNonNumericChar
(
	@str VARCHAR(500)
)
RETURNS VARCHAR(500)
   
BEGIN
    DECLARE @startingIndex INT  
    SET @startingIndex = Len(@str) - 1  
    DECLARE @IsDone INT
	SET @IsDone = 0 
	set @str = Reverse(@str)   
    WHILE @IsDone = 0
    BEGIN
        SET @startingIndex = PATINDEX('%[^0-9]%', @str)       
        IF @startingIndex = 1
        BEGIN
            SET @str = REPLACE(@str, SUBSTRING(@str, @startingIndex, 1), '')
        END
        ELSE
            SET @IsDone = 1 
        
        ;
    END RETURN REVERSE(@str)
END  

GO



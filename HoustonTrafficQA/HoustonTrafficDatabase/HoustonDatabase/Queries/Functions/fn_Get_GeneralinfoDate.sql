/****** Object:  UserDefinedFunction [dbo].[fn_Get_GeneralinfoDate]    Script Date: 01/25/2008 20:28:53 ******/
DROP FUNCTION [dbo].[fn_Get_GeneralinfoDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_Get_GeneralinfoDate](@text nvarchar(max))    
    
returns  varchar(20)    
    
as    
begin    
    
if @text <> ''    
begin     
 declare @returnText varchar(20)    
 --declare @Text varchar(3000)    
 --set @text = 'Client name provided on public site did not match with court data (5/31/2007 11:58 AM-SYS)'     
    
 declare @chkindex  int    
 declare @filterdata varchar(30)    
 set @filterdata = right(@text, 25)    
 --select @filterdata    
    
  
if  CHARINDEX('-', @filterdata ,1) <> -1 and CHARINDEX('-', @filterdata ,1) !> 4  
select @chkindex  = CHARINDEX('-', @filterdata ,1)+1    
else  
select @chkindex  = CHARINDEX('(', @filterdata ,1)+1    
  
-- select @chkindex  = CHARINDEX('(', @filterdata ,1)+1    
 --select @chkindex     
 set @filterdata=  SUBSTRING(@filterdata,@chkindex, LEN(@TEXT))    
    
 --select @filterdata    
    
 set @returnText =  LEFT(@filterdata, LEN(SUBSTRING(right(@text, 25), @chkindex, LEN(@filterdata)-5)))    
  
set @returnText = SUBSTRING(@returnText,1,CHARINDEX(' ', @filterdata,1))    
    
  
return  LTrim(@returnText)  
end     
     
 return ''    
    
end
GO

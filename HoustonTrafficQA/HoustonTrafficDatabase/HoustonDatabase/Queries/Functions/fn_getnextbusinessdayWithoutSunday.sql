/*
* Noufil 6012 06/12/2009 
* Business Logic : This function returns the next business day. Sunday will not be count as business day.
* Parameter : @Startdate : Start Date
			  @numofdays : Number of business days from start days.
*/

-- drop FUNCTION [dbo].[fn_getnextbusinessdayWithoutSaturday]
CREATE FUNCTION [dbo].[fn_getnextbusinessdayWithoutSunday]
(
	@Startdate  DATETIME,
	@numofdays  INT
)
RETURNS DATETIME
AS

BEGIN
    DECLARE @count INT
    DECLARE @NextDay DATETIME
    SET @count = 1
    SET @nextday = @Startdate
    WHILE @count <= @numofdays
    BEGIN
        SET @NextDay = DATEADD(DAY, 1, @NextDay)
        SET @NextDay = CASE DATEPART(dw, @NextDay)
                            WHEN 1 THEN DATEADD(d, 1, @NextDay)
                            --WHEN 7 THEN DATEADD(d, 2, @NextDay)
                            ELSE @NextDay
                       END
        
        SET @count = @count + 1
    END
    
    RETURN @NextDay
END


GO
GRANT EXECUTE ON [dbo].[fn_getnextbusinessdayWithoutSunday] TO dbr_webuser



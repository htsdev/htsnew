/****** Object:  UserDefinedFunction [dbo].[getnextbusinessday]    Script Date: 01/25/2008 20:29:30 ******/
DROP FUNCTION [dbo].[getnextbusinessday]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tblcourtviolationstatus where description like '%arraignment%'

--sp_getnextdayarraignment '02/03/2005',4
--select dbo.getnextbusinessday '02/03/2005',4
--select DATEPART(dw, '02/04/2004')

CREATE  FUNCTION [dbo].[getnextbusinessday] (@Startdate datetime)
RETURNS datetime
AS
BEGIN
declare @numofdays int
declare @count int
declare @NextDay datetime
set  @numofdays = 4
set @count = 1
set @nextday = @Startdate
WHILE @count <= @numofdays
BEGIN
        SET @NextDay = DATEADD(day, 1, @NextDay)
        SET @NextDay = CASE DATEPART(dw, @NextDay)
        WHEN 1 THEN DATEADD(d, 1, @NextDay)
        WHEN 7 THEN DATEADD(d, 2, @NextDay)
        ELSE @NextDay
        END
	set @count = @count + 1
END

return @NextDay
end
GO

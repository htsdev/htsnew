/******     
Created by:  Yasir Kamal
Task id : 5623
Dated :03/25/2009    
Business Logic: The function is used in Missed Court Date calls. It returns violationids in cvs format.    
           
List of Parameters:    
 @TicketID: Ticket ID associated with the case.    
 @CourtDate: Court date associated with the violation.    
    
 
    
*******/    
    
Create function [dbo].[fn_htp_get_TicketsViolationIdsFTA]     
 (    
  @ticketid int, 
  @courtdate datetime
 )              
    
returns varchar(500)              
    
as              
    
begin              
            
declare @string as varchar(500)            
set @string = ''  
  
select   
  
 @string =  @string + Convert(varchar,ttv.Ticketsviolationid ) + ','  
          
from     
 tblticketsviolations ttv     
inner join             
    tblcourtviolationstatus cvs     
on ttv.courtviolationstatusidmain=cvs.CourtViolationStatusID     
INNER JOIN                            
    dbo.tblDateType dt     
ON cvs.CategoryID = dt.TypeID               
    
where ttv.courtviolationstatusidmain<>80        
and  ticketid_pk=@ticketid         
AND ( DATEDIFF([day], ttv.CourtDateMain,@courtdate) = 0  )  
and ttv.courtviolationstatusidMAIN in (146,186,105)
  
    
       
            
return @string            
            
end 

GO
GRANT EXECUTE ON [dbo].[fn_htp_get_TicketsViolationIdsFTA] TO DBR_WEBUSER
GO
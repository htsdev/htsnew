/****** Object:  UserDefinedFunction [dbo].[fn_getsequnumber]    Script Date: 01/25/2008 20:29:01 ******/
DROP FUNCTION [dbo].[fn_getsequnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  function [dbo].[fn_getsequnumber](@ticketnumber varchar(20))
returns varchar(2)
as
begin
declare @seqnumber varchar(2)
if left(@ticketnumber,1) = 'F'
	set  @seqnumber = '0'
else
	set  @seqnumber = right(@ticketnumber,1)
return @seqnumber
end
GO

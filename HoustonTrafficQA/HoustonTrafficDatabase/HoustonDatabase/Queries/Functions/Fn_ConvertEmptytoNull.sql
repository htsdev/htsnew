/****** Object:  UserDefinedFunction [dbo].[Fn_ConvertEmptytoNull]    Script Date: 01/25/2008 20:28:41 ******/
DROP FUNCTION [dbo].[Fn_ConvertEmptytoNull]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.fn_GetAMPMDates('08/01/2005 11:30:00')

create   function [dbo].[Fn_ConvertEmptytoNull](@Value varchar(100))
returns varchar(100)
as
begin
declare @RetValue varchar(100)

set @RetValue = case when @Value = '' then null
else @Value end



return @RetValue 
end
GO

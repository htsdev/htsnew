/****** Object:  UserDefinedFunction [dbo].[fn_CHECK_Criminal_Case]    Script Date: 01/25/2008 20:28:35 ******/
DROP FUNCTION [dbo].[fn_CHECK_Criminal_Case]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zeeshan Ahmed
-- Create date: 1/4/2008
-- Description:	This function check whether case is a criminal case or not
-- =============================================
CREATE FUNCTION [dbo].[fn_CHECK_Criminal_Case]
(
	@Ticketid as int
)
RETURNS  bit
AS
BEGIN


declare @count int  
  
Select @count =  Count(TC.CourtID)   from tblticketsviolations TV  
join tblcourts TC  
on TV.CourtID = TC.CourtID where iscriminalcourt = 1   
and  ticketid_pk = @Ticketid  
  


if @count > 0   
return 1


return 0

END
GO

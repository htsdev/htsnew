/****** Object:  UserDefinedFunction [dbo].[get_court_violationstatus]    Script Date: 01/25/2008 20:29:22 ******/
DROP FUNCTION [dbo].[get_court_violationstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   Function [dbo].[get_court_violationstatus]
(@datetype as int)
returns int
as
begin

	
declare @courtviolationstatusid int
select @courtviolationstatusid = courtviolationstatusid from tblcourtviolationstatus 
where description in (select description from tbldatetype where typeid = @datetype)
return  @courtviolationstatusid
	
end
GO

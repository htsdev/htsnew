/****** Object:  UserDefinedFunction [dbo].[fn_format_violationnumber]    Script Date: 01/25/2008 20:28:46 ******/
DROP FUNCTION [dbo].[fn_format_violationnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_format_violationnumber](@ticketnumber as varchar(20),@violationnumber as int)
returns varchar(3)
as

begin
declare @retval varchar(3)
if left(@ticketnumber, 1) = 'F'
set @retval = ' '
else
set @retval = '-' + convert(varchar(2),isnull(@violationnumber,0))

return  @retval

end
GO

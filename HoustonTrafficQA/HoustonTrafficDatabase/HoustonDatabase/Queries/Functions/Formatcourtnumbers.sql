/****** Object:  UserDefinedFunction [dbo].[Formatcourtnumbers]    Script Date: 01/25/2008 20:29:14 ******/
DROP FUNCTION [dbo].[Formatcourtnumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create       Function [dbo].[Formatcourtnumbers](@courtnum as varchar(6))
returns varchar(6)
as
begin

	

	if len(@courtnum) = 1
		set @courtnum = '0' + @courtnum
	else
		set @courtnum = @courtnum
	return @courtnum
	
end
GO

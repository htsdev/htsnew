﻿ /******  
* Waqas 5864 07/15/2009
* Business Logic : This function is use to get comma seperated Cause Number
*
* Input Parameters : 
* @TicketID
* @CourtDate  
*
* Returns : @QUERY       
******/  
  
Create FUNCTION dbo.Fn_HTP_Get_CauseNumbersWithComma 
(  
 @TicketID     INT,   
 @CourtDate  DateTime
)  
RETURNs VARCHAR(MAX)  
AS  
BEGIN  
    DECLARE @QUERY VARCHAR(200)  
      
    SET @QUERY = ''  
      
    SELECT @QUERY = @QUERY +  CASE ttv.casenumassignedbycourt WHEN '' THEN ttv.RefCaseNumber ELSE ttv.casenumassignedbycourt end  + ', '  
	FROM   tblTicketsViolations ttv  
    WHERE  ttv.TicketID_PK = @TicketID             
		   AND	ttv.CourtDateMain = @CourtDate
			AND ttv.ViolationNumber_PK=16159
				
			 

      
    IF(LEN(@QUERY) > 1)  
    SET @QUERY = LEFT(@QUERY, LEN(@QUERY) -1)  
      
    RETURN @QUERY  
END  


GO

GRANT EXECUTE ON dbo.Fn_HTP_Get_CauseNumbersWithComma TO dbr_webuser

GO

  
   


/****** Object:  UserDefinedFunction [dbo].[fn_getinfofromunformattedstring]    Script Date: 01/25/2008 20:28:58 ******/
DROP FUNCTION [dbo].[fn_getinfofromunformattedstring]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     Function [dbo].[fn_getinfofromunformattedstring](@courtinfo varchar(100),@fieldpos tinyint)
returns varchar(20)
as
begin


declare @tempinfo varchar(20),
@firstpos tinyint,
@secondpos tinyint,
@thirdpos tinyint,
@fourthpos tinyint
--set @courtinfo = '09172004;1400;ARRAIGNMENT;07'

set @firstpos = charindex(';',@courtinfo)
set @secondpos = charindex(';',@courtinfo,@firstpos+1)
set @thirdpos = charindex(';',@courtinfo,@secondpos+1)


if @fieldpos = 1
	set @tempinfo = left(@courtinfo,@firstpos-1)		
if @fieldpos = 2
	set @tempinfo = substring(@courtinfo,@firstpos+1,@secondpos-@firstpos-1)
if @fieldpos = 3
	set @tempinfo = substring(@courtinfo,@secondpos+1,@thirdpos-@secondpos-1)
if @fieldpos = 4
	set @tempinfo = substring(@courtinfo,@thirdpos+1,len(@courtinfo)-@thirdpos)

return @tempinfo
	





end
GO

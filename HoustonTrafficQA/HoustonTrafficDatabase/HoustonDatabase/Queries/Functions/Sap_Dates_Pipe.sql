
CREATE FUNCTION [dbo].[Sap_Dates_Pipe] (@str varchar(max))      
RETURNS @CaseTypeTab TABLE      
   (      
    MainCat     Varchar(800)      
   )      
AS      
      
BEGIN      
declare @TempString varchar(800)          
 WHILE CHARINDEX( '|', @str,1) <> 0            
 BEGIN      
    INSERT @CaseTypeTab     Values(rtrim(ltrim((SUBSTRING(@str, 1,  CHARINDEX( '|', @str,1) - 1)))))      
        SELECT @TempString = SUBSTRING(@str, CHARINDEX( '|', @str) + 1, LEN(@str))            
    SELECT @str = @TempString             
 END            
         
   RETURN      
END  

GO

grant select on [dbo].[Sap_Dates_Pipe] to dbr_webuser    
go
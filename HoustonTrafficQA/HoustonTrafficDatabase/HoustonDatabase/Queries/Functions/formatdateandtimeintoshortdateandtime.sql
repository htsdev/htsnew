/****** Object:  UserDefinedFunction [dbo].[formatdateandtimeintoshortdateandtime]    Script Date: 01/25/2008 20:29:16 ******/
DROP FUNCTION [dbo].[formatdateandtimeintoshortdateandtime]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    Function [dbo].[formatdateandtimeintoshortdateandtime](@date as datetime)
returns varchar(30)
as
begin
	--declare @formatteddate datetime
	declare @tempdate varchar(30)
	declare @strhour int,
	@strmin varchar(2)
	declare @strtimestring varchar(20)
	if isdate(@date) = 1
	begin
		set @strhour = datepart(hour,@date)
		set @strmin = convert(varchar(2),datepart(n,@date))
		if len(@strmin) = 1
			begin
				set @strmin = '0' + @strmin
			end
		
		if @strhour >= 12 
			begin
				if @strhour = 12
					begin
						set @strtimestring = convert(varchar(2),@strhour) + ':' +   @strmin + ' ' + 'PM'
					end 
				else
					begin
						set @strtimestring = convert(varchar(2),@strhour-12) + ':' +   @strmin + ' ' + 'PM'
					end
			end
		else
			begin
				set @strtimestring = convert(varchar(2),@strhour) + ':' +   @strmin + ' ' + 'AM'
			end
		set @tempdate = convert(varchar(2),month(@date)) + '/' + convert(varchar(2),day(@date)) + '/' + convert(varchar(4),year(@date))  + ' ' + @strtimestring
--+ convert(varchar(2),hour(@date)) + ':' + convert(varchar(2),minute(@date)) + ':' + convert(varchar(2),second(@date))
	end
	else
	set @tempdate = '01/01/1900'
	return @tempdate
end
GO

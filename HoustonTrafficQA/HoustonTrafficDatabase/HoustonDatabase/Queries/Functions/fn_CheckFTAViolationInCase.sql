  
CREATE FUNCTION fn_CheckFTAViolationInCase   
(  
 @TicketID int   
)  
RETURNS int  
AS  
BEGIN  
  
if ( Select Count(*)  from tblticketsviolations TV   
join tblviolations V   
On TV.ViolationNumber_PK = V.ViolationNumber_PK  
Where V.ShortDesc like 'FTA'  
and TV.TicketID_pk = @ticketid  
--and TV.CourtID In (3001,3002,3003)  
) > 0   
return 1  
  
return 0  
  
  
END  

go

grant execute on fn_CheckFTAViolationInCase to dbr_webuser

go 
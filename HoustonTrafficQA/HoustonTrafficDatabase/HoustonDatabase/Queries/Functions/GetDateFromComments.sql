/****** Object:  UserDefinedFunction [dbo].[GetDateFromComments]    Script Date: 01/25/2008 20:29:28 ******/
DROP FUNCTION [dbo].[GetDateFromComments]
GO
CREATE FUNCTION [dbo].[GetDateFromComments](@comments [nvarchar](max))
RETURNS [nvarchar](30) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [StringManipulation].[StringLibrary.StringManipulation].[GetDateFromComments]
GO

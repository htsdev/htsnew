/**
* Business Logic :  This function is used to display time in HH:mm AM/PM format.
**/
ALTER FUNCTION [dbo].[Fn_FormateTime] (@date as datetime)  
RETURNS varchar(10)
AS  
BEGIN 

Declare @temp varchar(10)

	/*Waqas 5653 03/31/2009*/
	/*Waqas 5756 04/07/2009*/
	 SET @temp = dbo.fn_DateFormat(@date,26,'/',':',1) 


	Return @temp


END
GO

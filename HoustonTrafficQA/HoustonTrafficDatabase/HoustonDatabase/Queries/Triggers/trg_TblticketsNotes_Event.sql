﻿-- =============================================
-- Author:		Syed Muhammad Ozair
-- Create date: 05/11/2009
-- Task ID:		5863
-- Description:	To update LastContactDate filed in tbltickets for a case
-- =============================================
ALTER TRIGGER dbo.trg_TblticketsNotes_Event
ON dbo.TblTicketsNotes 
   AFTER INSERT
AS
BEGIN
    -- Noufil 8089 08/18/2010 Apply nested level check to avoid Exception "Maximum stored procedure, function, trigger, or view nesting level exceeded (limit 32)."
    IF ((SELECT TRIGGER_NESTLEVEL(OBJECT_ID('trg_TblticketsNotes_Event'), 'AFTER', 'DML')) = 1)    
    BEGIN
        UPDATE t
        SET    t.LastContactDate = GETDATE(),
               t.employeeidupdate = i.employeeid
        FROM   tblTickets t
               INNER JOIN INSERTED i
                    ON  t.TicketID_PK = i.TicketID
    END
END
GO



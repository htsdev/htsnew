/****** Object:  Trigger [trg_tblCustomerProfileToCreate_CreateProfile]    Script Date: 01/28/2008 12:56:55 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_tblCustomerProfileToCreate_CreateProfile]'))
DROP TRIGGER [dbo].[trg_tblCustomerProfileToCreate_CreateProfile]

go


/****** Object:  Trigger [dbo].[trg_tblCustomerProfileToCreate_CreateProfile]    Script Date: 01/28/2008 12:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[trg_tblCustomerProfileToCreate_CreateProfile] ON [dbo].[tblCustomerProfileToCreate] 
FOR INSERT
AS

set nocount on

declare	@TicketNumber		varchar(30),
	@PaidAmount		money,
	@TicketSource		int,
	@CustomerId		int,
	@MidNumber		varchar(30),
	@Address		varchar(100),
	@Zip			varchar(20),
	@sOutPut		varchar(30),
	@TicketId		int,
	@iFlag			int,
	@temp1			varchar(10),
	@temp2			varchar(10),
	@strInitial		varchar(10)

	
select	@TicketNumber = i.ticketnumber,
	@PaidAmount = i.paidamount,
	@TicketSource = i.ticketsource,
	@CustomerId = i.customerid
from	inserted i



if @TicketSource = 0
	begin
		exec USP_HTS_Insert_CaseInfoFromPublicSite @CustomerId, @sOutPut output
	end


go


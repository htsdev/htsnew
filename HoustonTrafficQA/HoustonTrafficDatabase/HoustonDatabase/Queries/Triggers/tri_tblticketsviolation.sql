/****** Object:  Trigger [tri_tblticketsviolation]    Script Date: 01/28/2008 13:04:50 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tri_tblticketsviolation]'))
DROP TRIGGER [dbo].[tri_tblticketsviolation]

go


/****** Object:  Trigger [dbo].[tri_tblticketsviolation]    Script Date: 01/28/2008 13:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from tblticketsviolations where refcasenumber = 'm00457434' and sequencenumber 




--select * from scanrepos.dbo.docwords where datediff(day,ocrdate,getdate()) = 7
--select * from scanrepos.dbo.docwords where datediff(day,ocrdate,getdate()) = 7


--select * from traffictickets.dbo.tblScannedDocketCourtStatus where datediff(day,recdate,getdate()) <= 8
--0933386371
--m00454335
--delete from traffictickets.dbo.tblScannedDocketCourtStatus


CREATE            TRIGGER [dbo].[tri_tblticketsviolation] ON [dbo].[tblScannedDocketCourtStatus] 
FOR insert
AS


declare @causenumber varchar(20)
declare @ticketnumber varchar(20)

select @causenumber = causenumber,@ticketnumber = ticketnumber from inserted I


--select * from tblScannedDocketCourtStatus
--select * from tblticketsviolations where scanupdatedate is not null
if left(@ticketnumber ,1) = 'F'
begin
	update tblticketsviolations
	set courtdatescan = i.courtdate,
	courtnumberscan = i.courtno,
	courtviolationstatusidscan = i.violationstatus,
	vemployeeid = 3992, --system id
	ScanDocID = doc_id,
	ScanDocnum = docnum,
	scanupdatedate = getdate()
	from inserted I, tblticketsviolations V
	/*
	where I.ticketnumber = V.refcasenumber
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0
	*/
	where I.causenumber = V.casenumassignedbycourt
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0


--select * from tblScannedDocketCourtStatus
	update tblScannedDocketCourtStatus
	set updateflag = 1 where ticketnumber in (
	select refcasenumber
	from inserted I, tblticketsviolations V
	--where I.ticketnumber = V.refcasenumber
	where I.causenumber = V.casenumassignedbycourt)
	and datediff(day,recdate,getdate()) = 0
	and datediff(day,courtdate,'01/01/1900') <> 0
	and violationstatus <> 0

	

end
else
begin


	
	--update by ticketnumber and sequence number and ticket length equal to 10
	update tblticketsviolations
	set courtdatescan = i.courtdate,
	courtnumberscan = i.courtno,
	courtviolationstatusidscan = i.violationstatus,
	vemployeeid = 3992,--system id
	ScanDocID = doc_id,
	ScanDocnum = docnum,
	scanupdatedate = getdate()
	from inserted I, tblticketsviolations V
	where  left(I.ticketnumber,len(I.ticketnumber)-1) = V.refcasenumber
	and  right(I.ticketnumber,1) = sequencenumber
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0
	and isnumeric(right(I.ticketnumber,1)) = 1
	and len(I.ticketnumber) = 10


	--Update by causenumber only
	update tblticketsviolations
	set courtdatescan = i.courtdate,
	courtnumberscan = i.courtno,
	courtviolationstatusidscan = i.violationstatus,
	vemployeeid = 3992,--system id
	ScanDocID = doc_id,
	ScanDocnum = docnum,
	scanupdatedate = getdate()
	from inserted I, tblticketsviolations V
	where (I.causenumber = V.casenumassignedbycourt)
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0

	 --update by ticketnumber only ticket number length greater than 10
	update tblticketsviolations
	set courtdatescan = i.courtdate,
	courtnumberscan = i.courtno,
	courtviolationstatusidscan = i.violationstatus,
	vemployeeid = 3992,
	ScanDocID = doc_id,
	ScanDocnum = docnum,
	scanupdatedate = getdate()
	from inserted I, tblticketsviolations V
	where (I.ticketnumber = V.refcasenumber)
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0
	and len(I.ticketnumber) > 10


	update I
	set updateflag = 1,ScanTicketID = V.ticketid_pk,ScanTicketViolationID = V.ticketsviolationid
	from Inserted S, tblScannedDocketCourtStatus I, tblticketsviolations V
	where  S.recid = I.recid
	and  left(I.ticketnumber,len(I.ticketnumber)-1) = V.refcasenumber
	and  right(I.ticketnumber,1) = sequencenumber
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0
	and isnumeric(right(I.ticketnumber,1)) = 1
	and len(I.ticketnumber) = 10
--select ticketsviolationid from tblticketsviolations
	update I
	set updateflag = 1,ScanTicketID = V.ticketid_pk, ScanTicketViolationID = V.ticketsviolationid
	from Inserted S, tblScannedDocketCourtStatus I, tblticketsviolations V
	where  S.recid = I.recid
	and (I.causenumber = V.casenumassignedbycourt)
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0

	
	update I
	set updateflag = 1,ScanTicketID = V.ticketid_pk,  ScanTicketViolationID = V.ticketsviolationid	
	from Inserted S, tblScannedDocketCourtStatus I, tblticketsviolations V
	where  S.recid = I.recid
	and (I.ticketnumber = V.refcasenumber)
	and datediff(day,i.courtdate,'01/01/1900') <> 0
	and i.violationstatus <> 0
	and len(I.ticketnumber) > 10
	

end


if exists(
	select * from tblScannedDocketCourtStatus where causenumber = @causenumber
	and updateflag = 1 and datediff(day,recdate,getdate()) = 0)
	begin
		update tblScannedDocketCourtStatus set updateflag = 1
		where  causenumber = @causenumber and updateflag = 0
		and datediff(day,recdate,getdate()) = 0
	end


go

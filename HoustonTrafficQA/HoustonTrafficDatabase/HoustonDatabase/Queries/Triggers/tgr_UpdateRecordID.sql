/****** Object:  Trigger [tgr_UpdateRecordID]    Script Date: 01/28/2008 13:10:16 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgr_UpdateRecordID]'))
DROP TRIGGER [dbo].[tgr_UpdateRecordID]

go

/****** Object:  Trigger [dbo].[tgr_UpdateRecordID]    Script Date: 01/28/2008 13:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE      TRIGGER [dbo].[tgr_UpdateRecordID] ON [dbo].[tblTicketsViolationsArchive] 
FOR INSERT

AS

declare @TicketId varchar(20)
declare @RecordID int
declare @CourtId int
declare @midNumber varchar(20),
		@OfficerNumber int,
		@ViolationDate datetime,
		@listdate datetime


set @RecordID = 0

-- Pick ticketID and Court ID from inserted record 
Select @TicketId = inserted.TicketNumber_pk from inserted 
Select @CourtId = isnull(inserted.CourtLocation,0) from inserted 


if @courtid in (3001,3002,3003)
	begin
		-- Pick Midnumber & list date for selection of recordid 
		select	@midNumber = midnumber, 
				@OfficerNumber = officernumber_fk, 
				@violationdate = convert(varchar(10), violationdate,101)
		FROM	tblTicketsArchive 
		where	ticketnumber =  @TicketId 
		and		isnull(CourtID,0)  = @CourtId

		SELECT  top 1  @RecordID = isnull(recordid,0) FROM tblTicketsArchive 
		where	midnumber = @midNumber  
		and		officernumber_fk = @officernumber
		and		datediff(day, violationdate , @violationdate ) = 0

	end
else
	begin
		-- Pick Midnumber & list date for selection of recordid 
		select	@midNumber = midnumber, 
				@listdate = convert(varchar(10),listdate,101)
		FROM	tblTicketsArchive 
		where	ticketnumber =  @TicketId 
		and		isnull(CourtID,0)  = @CourtId

		SELECT  top 1  @RecordID = isnull(recordid,0) FROM tblTicketsArchive 
		where	midnumber = @midNumber  
		and		datediff(day,listdate , @listdate) = 0
	end

--- Update Recordid 
update tblTicketsViolationsArchive set recordID = @RecordID 
where TicketNumber_pk = @TicketId and (isnull(CourtLocation,0) =  @CourtID)




go


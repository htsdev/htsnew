USE [TrafficTickets]
GO
/****** Object:  Trigger [dbo].[TRig_Violations]    Script Date: 11/05/2011 00:01:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER trigger [dbo].[TRig_Violations] ON [dbo].[tblTicketsViolations] 
FOR  insert, UPDATE 
AS

declare @TicketID int, @CourtID int

select @TicketID = ticketid_pk, @CourtID = courtid from inserted

-- IF INSERTING THE RECORD.......
if not exists (select * from deleted)
	begin
		insert 	into tblticketsviolationlog(
			TicketviolationID,
			newverifiedDate,
			oldverifiedDate,
			newverifiedstatus,
			oldverifiedstatus,
			newverifiedRoom,
			oldverifiedRoom,
			newscanDate,
			oldscanDate,
			newscanstatus,
			oldscanstatus,
			newscanroom,
			oldscanroom,
			newautoDate,
			oldautoDate,
			newautostatus,
			oldautostatus,
			newautoroom,
			oldautoroom,
			--Yasir Kamal 6029 06/16/2009 log employee info and old courtId info
			newCourtid,
			oldCourtid,
			EmployeeID
			)
		select 	i.ticketsviolationid,
			isnull(i.courtdatemain,'01/01/1900'),
			'01/01/1900',
			isnull(i.CourtViolationStatusIDmain,0),
			0,
			isnull(i.CourtNumbermain,''),
			'',
			isnull(i.courtdatescan,'01/01/1900'),
			'01/01/1900',
			isnull(i.courtviolationstatusidScan,0),
			0,
			isnull(i.CourtNumberscan,''),
			'',
			isnull(i.courtdate,'01/01/1900'),
			'01/01/1900',
			isnull(i.courtviolationstatusid,0),
			0,
			isnull(i.CourtNumber,''),
			'',
			isnull(i.Courtid,0),
			0,
			ISNULL(i.VEmployeeID,0)
			from inserted i

	--Updating OscarMailFlag if court is Oscar and ActiveFlag = 1
	if(((select activeflag from tbltickets where ticketid_pk = @TicketID)=1) and @CourtID = 3061)
		begin
			if(select isnull(OscarMailFlag,3) from tbltickets where ticketid_pk = @TicketID) = 3
				update tbltickets set OscarMailFlag = 1 where ticketid_pk = @TicketID
		end

	end
-- IF UPDATING THE RECORD......
else  
	begin

		insert 	into tblticketsviolationlog(
			TicketviolationID,
			newverifiedDate,
			oldverifiedDate,
			newverifiedstatus,
			oldverifiedstatus,
			newverifiedRoom,
			oldverifiedRoom,
			newscanDate,
			oldscanDate,
			newscanstatus,
			oldscanstatus,
			newscanroom,
			oldscanroom,
			newautoDate,
			oldautoDate,
			newautostatus,
			oldautostatus,
			newautoroom,
			oldautoroom,
			newCourtid,
			oldCourtid,
			EmployeeID
			)
		select 	i.ticketsviolationid,
			isnull(i.courtdatemain,'01/01/1900'),
			isnull(d.courtdatemain,'01/01/1900'),
			isnull(i.CourtViolationStatusIDmain,0),
			isnull(d.CourtViolationStatusIDmain,0),
			isnull(i.CourtNumbermain,''),
			isnull(d.CourtNumbermain,''),
			isnull(i.courtdatescan,'01/01/1900'),
			isnull(d.courtdatescan,'01/01/1900'),
			isnull(i.courtviolationstatusidScan,0),
			isnull(d.courtviolationstatusidScan,0),
			isnull(i.CourtNumberscan,''),
			isnull(d.CourtNumberscan,''),
			isnull(i.courtdate,'01/01/1900'),
			isnull(d.courtdate,'01/01/1900'),
			isnull(i.courtviolationstatusid,0),
			isnull(d.courtviolationstatusid,0),
			ISNULL(i.courtnumber, ''), --Sabir 9882 10/21/2011 Fixed Court Number Bug.
	        ISNULL(d.courtnumber, ''), --Sabir 9882 11/01/2011 Fixed Court Number Bug.
			isnull(i.courtid,0),
			isnull(d.courtid,0),
			ISNULL(i.VEmployeeID,0)
			
		from	inserted i, deleted d
		where 	i.ticketsviolationid  = d.ticketsviolationid
		and	(
				isnull(i.courtdate,'01/01/1900') != isnull(d.courtdate,'01/01/1900') 
			or	isnull(i.courtdatemain,'01/01/1900') !=	isnull(d.courtdatemain,'01/01/1900') 
			or 	isnull(i.courtdatescan,'01/01/1900') !=isnull(d.courtdatescan,'01/01/1900') 
			or 	isnull(i.courtviolationstatusid,0)!=isnull(d.courtviolationstatusid,0) 
			or 	isnull(i.CourtViolationStatusIDmain,0) !=	isnull(d.CourtViolationStatusIDmain,0) 
			or 	isnull(i.courtviolationstatusidScan,0)!=isnull(d.courtviolationstatusidScan,0) 
			or 	isnull(i.courtid,'0')!=isnull(d.courtid,'0') 
			or 	isnull(i.courtnumbermain,'0')!=isnull(d.courtnumbermain,'0')
			
			)	

	
	--Updating OscarMailFlag if court is Oscar and ActiveFlag = 1
declare @OldCourtID int
select @OldCourtID = courtid from deleted
	if(((select activeflag from tbltickets where ticketid_pk = @TicketID)=1) and (@OldCourtID <> @CourtID))
		if(@CourtID = 3061)
			begin
			if(select isnull(OscarMailFlag,3) from tbltickets where ticketid_pk = @TicketID) = 3
				update tbltickets set OscarMailFlag = 1 where ticketid_pk = @TicketID
			end

	end


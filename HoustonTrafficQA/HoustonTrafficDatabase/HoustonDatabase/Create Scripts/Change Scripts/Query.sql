-------------------- Adding Menu Script---------------------------
go
if exists(select * from tbl_TAB_SUBMENU where Title='Fax Report')
update tbl_TAB_SUBMENU
set URL='/Reports/FaxReport.aspx'
where Title='Fax Report'
else
insert into tbl_TAB_SUBMENU (TITLE,URL,ORDERING,SELECTED,MENUID,active) 
values ('Fax Report','/Reports/FaxReport.aspx',22,0,18,1)

go
if exists(select * from tbl_TAB_SUBMENU where Title='Jp Appearance Fax Report')
update tbl_TAB_SUBMENU
set URL='/Reports/JpAppFaxreport.aspx'
where Title='Jp Appearance Fax Report'
else
insert into tbl_TAB_SUBMENU (TITLE,URL,ORDERING,SELECTED,MENUID,active) 
values ('Jp Appearance Fax Report','/Reports/JpAppFaxreport.aspx',22,0,18,1)
--------------------------------------------------------------------------

---------------------------- Alter Table Script---------------------------
go
alter table tblConfigurationSetting
add extension varchar(20) null
go
alter table tblConfigurationSetting
add docpathforautofax varchar (1000) null
--------------------------------------------------------------------------

---------------------------Insert Script----------------------------------
go
if not exists(select * from tbldoctype where doctype='Fax Letter')
insert into tbldoctype (doctypeid,doctype,sortorder) values (17,'Fax Letter',17)

go
if not exists (select * from tblConfigurationSetting where docpathforautofax='\\SRV-WEB\DOCSTORAGE\ScanDoc\images\' and extension='.pdf')
begin
insert into tblConfigurationSetting (docpathforautofax,extension) values('\\SRV-WEB\DOCSTORAGE\ScanDoc\images\','.pdf')
end
--------------------------------------------------------------------------

 
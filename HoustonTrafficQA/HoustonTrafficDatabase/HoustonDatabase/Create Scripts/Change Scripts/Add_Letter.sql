use traffictickets
go

insert into tblletter (
	lettername, url, courtcategory, isactive, reportfilename, court_spname, data_spname, sortorder,shortname)
values (
	'Criminal New',	',LetterMailer_NewMailer.aspx',	8,	1,	'Crim_New.rpt',	'usp_mailer_Get_HCC_Criminal',	'usp_Mailer_Send_HCC_Criminal',	2,	',CRIM N')

go

update tblletter set isactive = 1 where letterid_pk = 55
go
 
if not exists (select sc.name from sysobjects so inner join syscolumns sc on so.id = sc.id where so.name = 'tblTicketsViolationsArchive' and sc.name = 'HCCC_CDI')
begin
Alter Table tblTicketsViolationsArchive add HCCC_CDI varchar(3)
end

if not exists (select sc.name from sysobjects so inner join syscolumns sc on so.id = sc.id where so.name = 'tblTicketsViolationsArchive' and sc.name = 'HCCC_CST')
begin
Alter Table tblTicketsViolationsArchive add HCCC_CST varchar(3)
end

if not exists (select sc.name from sysobjects so inner join syscolumns sc on so.id = sc.id where so.name = 'tblTicketsViolationsArchive' and sc.name = 'HCCC_DST')
begin
Alter Table tblTicketsViolationsArchive add HCCC_DST varchar(3)
end
 
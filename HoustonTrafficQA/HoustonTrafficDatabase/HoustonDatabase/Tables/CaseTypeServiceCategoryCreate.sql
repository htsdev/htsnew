 /****** Object:  Table [dbo].[CaseTypeServiceCategory]    Script Date: 07/02/2008 21:14:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseTypeServiceCategory](
	[CaseTypeId] [int] NOT NULL,
	[ServiceCategoryId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CaseTypeServiceCategory]  WITH CHECK ADD  CONSTRAINT [FK_CaseTypeServiceCategory_CaseType] FOREIGN KEY([CaseTypeId])
REFERENCES [dbo].[CaseType] ([CaseTypeId])
GO
ALTER TABLE [dbo].[CaseTypeServiceCategory] CHECK CONSTRAINT [FK_CaseTypeServiceCategory_CaseType]
GO
ALTER TABLE [dbo].[CaseTypeServiceCategory]  WITH CHECK ADD  CONSTRAINT [FK_CaseTypeServiceCategory_tblServiceTicketCategories] FOREIGN KEY([ServiceCategoryId])
REFERENCES [dbo].[tblServiceTicketCategories] ([ID])
GO
ALTER TABLE [dbo].[CaseTypeServiceCategory] CHECK CONSTRAINT [FK_CaseTypeServiceCategory_tblServiceTicketCategories]

/****** Object:  Table [dbo].[faxLetter]    Script Date: 06/25/2008 19:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[faxLetter](
	[FaxLetterid] [int] IDENTITY(1,1) NOT NULL,
	[Ticketid] [int] NOT NULL,
	[Employeeid] [int] NULL,
	[EmployeeName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmployeeEmail] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FaxNumber] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Subject] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Body] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FaxDate] [datetime] NULL,
	[Docpath] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_faxLetter] PRIMARY KEY CLUSTERED 
(
	[FaxLetterid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[faxLetter]  WITH CHECK ADD  CONSTRAINT [FK_faxLetter_Tbltickets] FOREIGN KEY([Ticketid])
REFERENCES [dbo].[tblTickets] ([TicketID_PK])
GO
ALTER TABLE [dbo].[faxLetter] CHECK CONSTRAINT [FK_faxLetter_Tbltickets] 
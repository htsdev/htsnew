<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmMainNew.aspx.cs" Inherits="lntechNew.frmMainNew" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="WebControls/ActiveMenu.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>frmMainNew</title>   
    <link href="Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">.style2 { COLOR: #000000 } </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <SCRIPT src="Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
    
    <script type="text/javascript">
     function ChangeCriteria(lnkbtn_type)
     {
        var div1=document.getElementById("dSearchCriteria1");
        var div2=document.getElementById("dSearchCriteria2");
        if(lnkbtn_type == "Advance")
        {
            div1.style.display='none';
            div2.style.display='block';
            
            document.getElementById("txtSearchKeyWord2").value="";
            document.getElementById("ddlSearchKeyType2").selectedIndex=0;
            
            document.getElementById("rdbtnClientad").checked=true;
            document.getElementById("rdbtnClient").checked=false;
            document.getElementById("rdbtnQuote").checked=false;
            document.getElementById("rdbtnNonClient").checked=false;
            document.getElementById("rdbtnJims").checked=false;
            
            document.getElementById("hf_btnpressed").value ="Advance";            
        }
        else
        {
            div2.style.display='none';
            div1.style.display='block';
            
            document.getElementById("txtSearchKeyWord2ad").value="";
            document.getElementById("ddlSearchKeyType2ad").selectedIndex=0;
            document.getElementById("txtSearchKeyWord1").value="";
            document.getElementById("ddlSearchKeyType1").selectedIndex =0;
            
            document.getElementById("rdbtnClient").checked=true;
            document.getElementById("rdbtnClientad").checked=false;
            document.getElementById("rdbtnQuotead").checked=false;
            document.getElementById("rdbtnNonClientad").checked=false;
            document.getElementById("rdbtnJimsad").checked=false;
            
            document.getElementById("hf_btnpressed").value ="NormalMode";
        }
        return false;
      }
      
      function SearchStatus()
      {
        var div1=document.getElementById("dSearchCriteria1");
        var div2=document.getElementById("dSearchCriteria2");
            
        var st =document.getElementById("hf_btnpressed").value;
        
        if(st == "Advance")
        { 
            div1.style.display='none';
            div2.style.display='block';
         } 
        else 
        { 
            div2.style.display='none';
            div1.style.display='block';
        
        }                
       return true;
      }
      
      function ResetControls()
      {
        document.getElementById("txtSearchKeyWord2").value="";
        document.getElementById("txtSearchKeyWord2ad").value="";
        document.getElementById("txtSearchKeyWord1").value="";
        document.getElementById("txtSearchKeyWord3").value="";
        
        document.getElementById("ddlSearchKeyType2").selectedIndex=0;
        document.getElementById("ddlSearchKeyType1").selectedIndex=0;
        document.getElementById("ddlSearchKeyType2ad").selectedIndex=0;
        document.getElementById("ddlSearchKeyType3").selectedIndex=0;
        
         document.getElementById("rdbtnClientad").checked=true;
        document.getElementById("rdbtnClient").checked=true;
        
        document.getElementById("rdbtnQuote").checked=false;
        document.getElementById("rdbtnNonClient").checked=false;
        document.getElementById("rdbtnJims").checked=false;    
            
       
        document.getElementById("rdbtnQuotead").checked=false;
        document.getElementById("rdbtnNonClientad").checked=false;
        document.getElementById("rdbtnJimsad").checked=false;
                      
        if(document.getElementById("dgResult") != null)
            document.getElementById("dgResult").style.display="none";
            
          var div1=document.getElementById("dSearchCriteria1");
          var div2=document.getElementById("dSearchCriteria2");     
            div2.style.display='none';
            div1.style.display='block';
            
        return false;
      }
    </script>
    

    <script type="text/javascript">
		
			//window.history.forward(1);
			
	function KeyDownHandler()
    {
        // process only the Enter key
        if (event.keyCode == 13)
        {
            // cancel the default submit
            event.returnValue=false;
            event.cancel = true;
            // submit the form by programmatically clicking the specified button
            document.getElementById("lnkbtn_reset").click();
        }
    }
			
			
			function ValidateInput()
			{ 
				//error list				
				var error1 = 'Choose one of 3 criterias';
				var error2 = 'Ticket Number must be empty';
				var error3 = 'Choose any criteria from dropdownlist';
				
				 var txtSearchKeyWord1="";
				 var txtSearchKeyWord2="";
				 var txtSearchKeyWord3="";
				 
				 var ddlSearchKeyType1="";
				 var ddlSearchKeyType2="";
				 var ddlSearchKeyType3="";
				    
				if(document.getElementById("hf_btnpressed").value == "Advance")
				{
				    txtSearchKeyWord1 = document.getElementById("txtSearchKeyWord1").value;
				    ddlSearchKeyType1 = document.getElementById("ddlSearchKeyType1");
				    txtSearchKeyWord2 = document.getElementById("txtSearchKeyWord2ad").value;				    
				    ddlSearchKeyType2 = document.getElementById("ddlSearchKeyType2ad");
				    txtSearchKeyWord3 =document.getElementById("txtSearchKeyWord3").value;
				    ddlSearchKeyType3 = document.getElementById("ddlSearchKeyType3");
				}
				else
				{			   
				    var txtSearchKeyWord2 = document.getElementById("txtSearchKeyWord2").value;
				    var ddlSearchKeyType2 = document.getElementById("ddlSearchKeyType2");
				}
				
				
								
				var Message = document.getElementById("Message") ;
				
				
					if ( (txtSearchKeyWord1!="") && (ddlSearchKeyType1.value==-1) )
					{
						Message.innerText = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}
					
					if ( (txtSearchKeyWord2!="") && (ddlSearchKeyType2.value==-1) )
					{
						Message.innerText = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}										
					if ( (txtSearchKeyWord3!="") && (ddlSearchKeyType3.value==-1) )
					{
						Message.innerText = error3;
						document.getElementById("grid").style.display='none';
						return false;
					}				
				
				if(((txtSearchKeyWord3!="") && (ddlSearchKeyType3.value==10)) && ((txtSearchKeyWord2!="") && (ddlSearchKeyType2.value==10)) || ((txtSearchKeyWord1!="") && (ddlSearchKeyType1.value==10)))
				{
					if(document.getElementById("rdbtnJims").checked==true)
					{
						Message.innerText = "Cause Number can not be searched for Jims";
						document.getElementById("grid").style.display='none';						
						return false;
					}
				}
				
				if(((txtSearchKeyWord3!="") && (ddlSearchKeyType3.value==9)) && ((txtSearchKeyWord2!="") && (ddlSearchKeyType2.value==9)) || ((txtSearchKeyWord1!="") && (ddlSearchKeyType1.value==9)))
				{
					if(document.getElementById("rdbtnJims").checked==false)
					{
					    if (txtSearchKeyWord3!="" && isNaN(txtSearchKeyWord3) ) 
					    {
						Message.innerText = "Please enter numeric value.";
						document.getElementById("grid").style.display='none';						
						return false;
						}
						
					    if (txtSearchKeyWord2!="" && isNaN(txtSearchKeyWord2) ) 
					    {
						Message.innerText = "Please enter numeric value.";
						document.getElementById("grid").style.display='none';						
						return false;
						}

					    if (txtSearchKeyWord1!="" && isNaN(txtSearchKeyWord1) ) 
					    {
						Message.innerText = "Please enter numeric value.";
						document.getElementById("grid").style.display='none';						
						return false;
						}

					}
				}
                
                if(ddlSearchKeyType3.value==4 || ddlSearchKeyType3.value==6 || ddlSearchKeyType3.value==8)
				{
					if (txtSearchKeyWord3!="")
					{
						if(chkdate(txtSearchKeyWord3)==false)
						{
							Message.innerText ="Invalid Date";
							document.getElementById("grid").style.display='none';
							return false;
						}						
					}
				}

				if(ddlSearchKeyType2.value==4 || ddlSearchKeyType2.value==6 || ddlSearchKeyType2.value==8)
				{
					if (txtSearchKeyWord2!="")
					{
						if(chkdate(txtSearchKeyWord2)==false)
						{
							Message.innerText ="Invalid Date";
							document.getElementById("grid").style.display='none';
							return false;
						}						
					}
				}
				if(ddlSearchKeyType1.value==4 || ddlSearchKeyType1.value==6 || ddlSearchKeyType1.value==8)
				{
					if(txtSearchKeyWord1!="")
					{
						if(chkdate(txtSearchKeyWord1)==false)
						{
							Message.innerText ="Invalid Date";
							document.getElementById("grid").style.display='none';
							return false;
						}
					}					
				}
				
				document.getElementById("grid").style.display='block';				
				Message.innerText="";
			}
			 
			 function SetHiddenFildValue()
			 {
			  document.getElementById("hf_btnpressed").value ="NormalMode";
			 }
			
		</script>
		
		
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
</head >
<body  onload="SearchStatus()">
    <form id="form1" runat="server" onkeypress="KeyDownHandler();">
    <div>
        	<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD  colSpan="2"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD colSpan="2"></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR >
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center">
					
					<TABLE id="tbleSerchCriteria" cellSpacing="1" cellPadding="0" border="0" style="width:100%">
					<tr>
					<td colSpan="2" align="center">
					
					<div id="dSearchCriteria1" style="display:block" runat="server">	
                    <table id="tblsearch1" cellSpacing="1" cellPadding="0" border="0" width="100%">
                    <tr align="center" class="clsleftpaddingtable">
                    
                     <td align="center" class="clsleftpaddingtable">
					    <TABLE id="tblSearchCriteria1" cellSpacing="1" cellPadding="0" border="0" width="40%">					   
					    <tr class="clsleftpaddingtable" align="center">
					     <td class="clsleftpaddingtable" vAlign="middle" align="center">
					            <SPAN class="clsleftpaddingtable">&nbsp;
								<td  align="center">
								
                                    <asp:radiobutton id="rdbtnClient" runat="server" CssClass="clslabelnew" Text="Client" GroupName="optCriteria"
											Height="16px" Checked="True" tabIndex="6"></asp:radiobutton>						
							    </td>
							    <td align="left"> 						    
										<asp:radiobutton id="rdbtnQuote" runat="server" CssClass="clslabelnew" Text="Quote" GroupName="optCriteria"
											Height="16px" tabIndex="7"></asp:radiobutton></td>
							    <td align="left">        
										<asp:radiobutton id="rdbtnNonClient" runat="server" CssClass="clslabelnew" Text="Non Client" GroupName="optCriteria"
											Height="16px" tabIndex="8"></asp:radiobutton></td>
							    <td align="left">    
										<asp:radiobutton id="rdbtnJims" runat="server" CssClass="clslabelnew" Text="Jims" GroupName="optCriteria"
											Height="16px" tabIndex="11"></asp:radiobutton> &nbsp;&nbsp;</td>
							</SPAN>
							</td>
					        </tr>
					    </TABLE>
					    </td> 
					    
					     </tr>
					     <tr>
					     <td align="center" class="clsleftpaddingtable">
					     
						<TABLE id="tblSearchCriteria2" cellSpacing="1" cellPadding="0" border="0" width="65%">					        
							
							<TR>
								<TD class="clsleftpaddingtable" vAlign="middle"><asp:textbox id="txtSearchKeyWord2" runat="server" Width="300px" CssClass="clsinputadministration"
										tabIndex="2"></asp:textbox>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlSearchKeyType2" runat="server" CssClass="clsinputcombo" tabIndex="3">
										<asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
										<asp:ListItem Value="0">Ticket Number</asp:ListItem>
										<asp:ListItem Value="1">Last Name</asp:ListItem>
										<asp:ListItem Value="2">First Name</asp:ListItem>
										<asp:ListItem Value="3">MID Number</asp:ListItem>
										<asp:ListItem Value="4">DOB</asp:ListItem>
										<asp:ListItem Value="5">Phone No</asp:ListItem>
										<asp:ListItem Value="6">Court Date</asp:ListItem>
										<asp:ListItem Value="7">Driver License</asp:ListItem>
										<asp:ListItem Value="8">Contact Date</asp:ListItem>
										<asp:ListItem Value="9">LID</asp:ListItem>
										<asp:ListItem Value="10">Cause Number</asp:ListItem>
									</asp:dropdownlist></TD>
								
								<td class="clsleftpaddingtable" align="left">
								   <SPAN class="clsleftpaddingtable">
										</SPAN>
                                    <asp:ImageButton ID="imgbtn_searchnor" runat="server" ImageUrl="~/Images/search_go.gif" OnClick="imgbtn_Search_Click" OnClientClick="SetHiddenFildValue()" /></td class="clsleftpaddingtable">                                
                                <td >
                                    &nbsp;
										<asp:button id="btn_Reset" tabIndex="12" runat="server" CssClass="clsbutton" Text="Reset" OnClick="btn_Reset_Click"></asp:button>
                                </td>	
								<TD class="clsLeftPaddingTable" vAlign="middle" align="left">
									<SPAN class="clsleftpaddingtable">
                                        <asp:LinkButton ID="lnkbtn_adnvace" runat="server" Font-Bold="True">Advance</asp:LinkButton>
									</SPAN>
								</TD>
							</TR>
						</TABLE>
                   </td>
					     </tr>
					 </table>  	 
					</div>	
					</td>
					</tr>
					<tr>
					<td colSpan="2" align="center">
					
					<div id="dSearchCriteria2" style="display:none" runat="server" enableviewstate="true">	
					
						
					    
					    <TABLE  cellSpacing="1" cellPadding="0" border="0" width="100%">
					    <tr class="clsleftpaddingtable" vAlign="middle" align="center">
					          <td  align="center">
					          <table id="tblsearch2" cellSpacing="1" cellPadding="0" border="0" width="300px">
								<tr class="clsLeftPaddingTable">
								<td >
                                    <asp:radiobutton id="rdbtnClientad" runat="server" CssClass="clslabelnew" Text="Client" GroupName="optCriteriaad"
											Height="16px" Checked="True" tabIndex="6"></asp:radiobutton>						
							    </td>
							    <td> 						    
										<asp:radiobutton id="rdbtnQuotead" runat="server" CssClass="clslabelnew" Text="Quote" GroupName="optCriteriaad"
											Height="16px" tabIndex="7"></asp:radiobutton>
								</td>
							    <td style="width: 71px">        
										<asp:radiobutton id="rdbtnNonClientad" runat="server" CssClass="clslabelnew" Text="Non Client" GroupName="optCriteriaad"
											Height="16px" tabIndex="8" Width="83px"></asp:radiobutton>
								</td>
							    <td style="width: 61px">    
										<asp:radiobutton id="rdbtnJimsad" runat="server" CssClass="clslabelnew" Text="Jims" GroupName="optCriteriaad"
											Height="16px" tabIndex="11"></asp:radiobutton>
								</td>
								</tr>
								</table>  </td>   
                                        
					        </tr>
					    </TABLE>
					    
					    <TABLE id="tblSearchCriteria4" cellSpacing="1" cellPadding="0" border="0" width="100%">					        
							
							<TR>
								<TD class="clsLeftPaddingTable">
                                    &nbsp;<asp:textbox id="txtSearchKeyWord2ad" runat="server" Width="118px" CssClass="clsinputadministration"
										tabIndex="2"></asp:textbox></TD>
								<TD class="clsleftpaddingtable" vAlign="middle" align="left"><asp:dropdownlist id="ddlSearchKeyType2ad" runat="server" CssClass="clsinputcombo" tabIndex="3">
										<asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
										<asp:ListItem Value="0">Ticket Number</asp:ListItem>
										<asp:ListItem Value="1">Last Name</asp:ListItem>
										<asp:ListItem Value="2">First Name</asp:ListItem>
										<asp:ListItem Value="3">MID Number</asp:ListItem>
										<asp:ListItem Value="4">DOB</asp:ListItem>
										<asp:ListItem Value="5">Phone No</asp:ListItem>
										<asp:ListItem Value="6">Court Date</asp:ListItem>
										<asp:ListItem Value="7">Driver License</asp:ListItem>
										<asp:ListItem Value="8">Contact Date</asp:ListItem>
										<asp:ListItem Value="9">LID</asp:ListItem>
										<asp:ListItem Value="10">Cause Number</asp:ListItem>
									</asp:dropdownlist>&nbsp;</TD>
								
								<td class="clsLeftPaddingTable" vAlign="middle" align="right">
                                    <asp:TextBox ID="txtSearchKeyWord1" runat="server" CssClass="clsinputadministration"
                                        TabIndex="4" Width="118px"></asp:TextBox>&nbsp;</td>
								<td class="clsLeftPaddingTable" vAlign="middle" align="left">
                                    &nbsp;<asp:DropDownList ID="ddlSearchKeyType1" runat="server" CssClass="clsinputcombo"
                                        TabIndex="5">
                                        <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                        <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                        <asp:ListItem Value="1">Last Name</asp:ListItem>
                                        <asp:ListItem Value="2">First Name</asp:ListItem>
                                        <asp:ListItem Value="3">MID Number</asp:ListItem>
                                        <asp:ListItem Value="4">DOB</asp:ListItem>
                                        <asp:ListItem Value="5">Phone No</asp:ListItem>
                                        <asp:ListItem Value="6">Court Date</asp:ListItem>
                                        <asp:ListItem Value="7">Driver License</asp:ListItem>
                                        <asp:ListItem Value="8">Contact Date</asp:ListItem>
                                        <asp:ListItem Value="9">LID</asp:ListItem>
                                        <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                    </asp:DropDownList></td>
								
								<td class="clsLeftPaddingTable" vAlign="middle" align="left">
                                    &nbsp;
                                    <asp:TextBox ID="txtSearchKeyWord3" runat="server" CssClass="clsinputadministration"
                                        TabIndex="4" Width="118px"></asp:TextBox></td>
                                
								<TD class="clsleftpaddingtable" vAlign="middle" align="left">
                                     <asp:DropDownList ID="ddlSearchKeyType3" runat="server" CssClass="clsinputcombo" TabIndex="5">
                                         <asp:ListItem Value="-1">&lt; Choose &gt;</asp:ListItem>
                                         <asp:ListItem Value="0">Ticket Number</asp:ListItem>
                                         <asp:ListItem Value="1">Last Name</asp:ListItem>
                                         <asp:ListItem Value="2">First Name</asp:ListItem>
                                         <asp:ListItem Value="3">MID Number</asp:ListItem>
                                         <asp:ListItem Value="4">DOB</asp:ListItem>
                                         <asp:ListItem Value="5">Phone No</asp:ListItem>
                                         <asp:ListItem Value="6">Court Date</asp:ListItem>
                                         <asp:ListItem Value="7">Driver License</asp:ListItem>
                                         <asp:ListItem Value="8">Contact Date</asp:ListItem>
                                         <asp:ListItem Value="9">LID</asp:ListItem>
                                         <asp:ListItem Value="10">Cause Number</asp:ListItem>
                                     </asp:DropDownList></TD>
                                <td class="clsleftpaddingtable" vAlign="middle" align="left">
                                    <asp:ImageButton ID="imgbtn_Search" runat="server" ImageUrl="~/Images/search_go.gif"
                                        OnClick="imgbtn_Search_Click" />&nbsp;</td>
                               <td class="clsleftpaddingtable" vAlign="middle" align="left">
                                   <asp:LinkButton ID="lnkbtn_reset" runat="server" ForeColor="Desktop">Reset</asp:LinkButton>&nbsp;</td>
												
								
							</TR>
						</TABLE>
					
					</div>	
					
					</td>
					</tr>
					
					</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2" style="height: 19px" >
						<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
							<TR >
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" style="height: 11px">
                                    &nbsp;</TD>
							</TR>
							<tr>
							<TD class="clssubhead" width="100%">
							<asp:Label ID="Message" runat="server" Font-Bold="True"
                                            Font-Names="Verdana" Font-Size="XX-Small" ForeColor="Red"></asp:Label>
                                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="XX-Small"
                                        ForeColor="Red"></asp:Label></TD>
							</tr>
						</TABLE>
						
					</TD>
				</TR>
				
				<TR>
					<TD colSpan="2" >
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR >
								<TD class="clssubhead" width="37%" background="../Images/subhead_bg.gif" height="34" >&nbsp;&nbsp;Results</TD>
								<TD class="clssubhead" align="right" width="25%" background="../Images/subhead_bg.gif">
									<asp:LinkButton id="lnkbtn_AddNewTicket" runat="server" CssClass="clsbutton" tabIndex="12">Add a New Ticket</asp:LinkButton>&nbsp;</TD>
							</TR>
							
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD id="grid" vAlign="top" colSpan="2"><asp:datagrid id="dgResult" runat="server" Width="780px" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
							PageSize="20" BorderColor="DarkGray" BorderStyle="Solid" AllowSorting="True" tabIndex="13" BorderWidth="1px" CellPadding="0" OnItemDataBound="dgResult_ItemDataBound" OnSortCommand="dgResult_SortCommand" OnItemCommand="dgResult_ItemCommand">
							<Columns>
								<asp:TemplateColumn SortExpression="LastName" HeaderText="Last Name">
									<HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="FirstName" HeaderText="First Name">
									<HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label5 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="DOB" HeaderText="DOB">
									<HeaderStyle Width="8%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=Label2 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.DOB", "{0:d}") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="Address" HeaderText="Address" Visible="False">
									<HeaderStyle Width="24%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label6 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
										</asp:Label>
										<asp:Label id=lblAdd1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblZip runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zipcode") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="causenumber" HeaderText="Cause Number">
									<HeaderStyle Width="15%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblcausenumber" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="CaseNumber" HeaderText="Ticket Number">
									<HeaderStyle Width="15%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id=lnkbtnCaseNo runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNumber") %>' CommandName="CaseNo">
										</asp:LinkButton>
										<asp:HyperLink id=HLCaseNo runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNumber") %>' Visible="False">
										</asp:HyperLink>
										<asp:Label id=lblTicketId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketId") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblCourtId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtId") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" SortExpression="MIDNo" HeaderText="MID #">
									<HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id=HLMidNumber runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MIDNo") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="CourtDate" HeaderText="Court Date">
									<HeaderStyle Width="8%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate", "{0:d}") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="CaseStatus" HeaderText="Status">
									<HeaderStyle Width="10%" CssClass="clsaspcolumnheaderblack"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.CaseStatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				
				<TR>
					<TD colSpan="2">
						<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR >
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
						</TABLE>
                        <asp:HiddenField ID="hf_btnpressed" runat="server" />
                        &nbsp;
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			&nbsp;</TABLE>
    </div>
    </form>
					        
</body>
</html>

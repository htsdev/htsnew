<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%--Yasir Kamal 5427 01/31/2009 Pagging functionality Added.--%>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%--Yasir Kamal 5427 end--%>
<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.QuickEntryNew.ReminderCalls"
    CodeBehind="ReminderCalls.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Reminder Call Alert</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="javascript" type="text/javascript">
    // Agha Usman Ahmed for Bug Id 3882 04/28/2008
    
    function refreshContent(){
            //document.Form2.button_name.click();
            document.getElementById("tdData").style.display = 'none';
            //Yasir Kamal 5427 02/06/2009 Pagging functionality Added.
         //document.getElementById("tdWait").style.display = 'none';
            //Yasir Kamal 5427 end.
          __doPostBack('lbRefresh','');
         }
         
    //Agha Usman Ahmed for Task Id 3870 05/01/2008
    function performCheck(check)
    {
        var dt =new Date(); 
        var datestring;         
        var checkbox = document.getElementById("cb_showall");
        //Sabir 4272. set true value for bond and reguler checkbox 
        var chkbond=document.getElementById("chkBondClient");
        var chkReguler=document.getElementById("chkRegulerClient");
        checkbox.checked = check;
        chkbond.checked=true;
        chkbond.disabled=false;   // Add 3977 Zahoor        
        chkReguler.checked=true;
        chkReguler.disabled=false;
        document.getElementById("cal_EffectiveFrom");
        //Sabir 4272. set three days back date for set call option and current date for reminder call option.        
        //ozair 4783 10/16/2008 date undefined issue resolved;  also refactor the code
        if(check)
        {
        dt.setDate(dt.getDate()-3);        
        }       
        dt.setMonth(dt.getMonth());        
        var dd=dt.getDate();
        if(dd<10)
            dd="0"+dd;
        var mm=dt.getMonth()+1;
        if(mm<10)
            mm="0"+mm;
        var yyyy=dt.getFullYear();
        
        datestring= mm + "/" + dd + "/" + yyyy;       
        document.getElementById("cal_EffectiveFrom").value=datestring;
        
        //End here                
        
    }
    
    // Added Zahoor 3977 08/11/2008
    // Description: for Setting FTA Activity report
    function FTACheckSettings(perform)
    {        
        var dt =new Date(); 
        var datestring;         
        var checkbox = document.getElementById("cb_showall");        
        var chkbond=document.getElementById("chkBondClient");
        var chkReguler=document.getElementById("chkRegulerClient");
        checkbox.checked = perform;
        chkbond.checked=false;
        chkbond.disabled=true;
        chkReguler.checked=true;
        chkReguler.disabled=true;
        //ozair 4783 10/16/2008 date undefined issue resolved;  also refactor the code
        dt.setDate(dt.getDate()-2);
        dt.setMonth(dt.getMonth());        
        var dd=dt.getDate();
        if(dd<10)
            dd="0"+dd;
        var mm=dt.getMonth()+1;
        if(mm<10)
            mm="0"+mm;            
        var yyyy=dt.getFullYear();
        
        datestring= mm + "/" + dd + "/" + yyyy;       
        document.getElementById("cal_EffectiveFrom").value=datestring;             
    }
    // ended 3977 Zahoor.
    
    //Sabir 4272. checked reguler client check box if bond client check box is unchecked and vice versa
    function IsCheck(check)
    {
        var chkbond=document.getElementById("chkBondClient");
        var chkReguler=document.getElementById("chkRegulerClient");
       // Added Zahoor 3977
       var cmbbox = document.Form2.rb_calls;  
       
        //if (cmbbox[2].checked != true)
        //{        
       
        if(chkbond.checked==false && check=="1")        
            chkReguler.checked=true;            
        if(chkReguler.checked==false && check=="2")
            chkbond.checked=true;
            
        //}        
               // End 3977
    }
    
     //Sabir Khan 4760  09/10/2008  Display alert in case of past date selected.
     function CheckDate(seldate, tbID)
	    {
	    
//	    if(document.getElementById("rb_calls_0").checked==true)
//        {
//            today=new Date();
//            var diff =Math.ceil(DateDiff(seldate, today));
//	        if (diff < 0)
//	        {
//	        alert("Court Date should not be in past for the reminder calls.");
//	        var datestring;
//	        //ozair 4837 10/08/2008 month issue resolved
//	        if(today.getDate() < 10)
//                datestring=(today.getMonth()+1)  + "/0" + today.getDate() + "/" + today.getFullYear();
//            if((today.getMonth()+1) < 10)
//                datestring="0" + (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getFullYear();
//            if(today.getDate() < 10 && (today.getMonth()+1) < 10)
//                datestring="0" + (today.getMonth()+1) + "/0" + today.getDate() + "/" + today.getFullYear(); 
//            //end ozair 4837
//	        document.getElementById("cal_EffectiveFrom").value=datestring
//            return false;
//            }             
//        }
	    }
        //Sabir Khan 4760  09/10/2008  Return Diffrence b/w the selected date and current date.
        function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
      
         
		//In order to set focus on clicked record 
//		function SetFocusOnGrid()
//		{
//		//a;
//			var id= <%=Session["ReminID"]%>;		
//			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
//			var grdName = "dg_ReminderCalls";		
//			var idx=2;			
//			cnt = cnt+2;
//			for (idx=2; idx < (cnt); idx++)
//			{					
//			var txtbox = grdName+ "__ctl" + idx + "_txt_sno"
//			var txtfocus=document.getElementById(txtbox);							
//			var compare=txtfocus.value;
//			  if (compare==id)
//			  {
//			  //a;			  
//				  txtfocus.focus();
//				  <%Session["ReminID"]=1;%>;				 //initializing to 1 
//				  break;			
//			  }
//			}		
//		}
 
        /// --------------------------------------------------
/// mainScreen object
/// ------------------------------------------------ 
     function validation()
            {
            if (document.getElementById("ddl_rStatus").value=="0")
                {
                    var r= confirm ("Are You Sure You Want To Active This Flag and Send A Letter To The Client Requested Updated Contact Information");
                   if (r==true)
                   {                   
                      
                   }
                   else
                   {
                  
                   }                 
                }                 
             }          
   
       function DGSelectOrUnselectAll(grdid,obj,objlist)
               { 
               var chk
        
        //this function decides whether to check or uncheck all
                if(obj.checked) 
                {
                DGSelectAll(grdid,objlist) 
                }
                else 
                {
                DGUnselectAll(grdid,objlist) 
                }
        } 
//---------- 
 
        function DGSelectAll(grdid,objid)
              
        { 
            //.this function is to check all the items
            var chkbox; 
            var i=2; 
            
            chkbox=document.getElementById(grdid + '_ctl0' + i + '_' + objid); 
            while(chkbox!=null){ 
                chkbox.checked=true; 
                i=i+1;
                if ( i < 10 ) chkbox=document.getElementById(grdid +'_ctl0' + i + '_' + objid); 
                else
                    chkbox=document.getElementById(grdid +'_ctl' + i + '_' + objid); }                 
                   
     

}//-------------- 

    function DGUnselectAll(grdid,objid)
    { 
    //.this function is to uncheckcheck all the items
        var chkbox; 
        var i=2;             
        chkbox=document.getElementById(grdid + '_ctl0' + i + '_' + objid); 
        while(chkbox!=null)
        { 
            chkbox.checked=false; 
            i=i+1; 
                    if ( i < 10 ) chkbox=document.getElementById(grdid +'_ctl0' + i + '_' + objid); 
                    else
                        chkbox=document.getElementById(grdid +'_ctl' + i + '_' + objid); 
        } 
    }



        function SetFocusOnGrid()
		{
		
		    if ( document.getElementById("dg_ReminderCalls")!= null)
		    {
		
			    var id= <%=Session["ReminID"]%>;		
			    var cnt = parseInt(document.getElementById("txt_totalrecords").value);
			    var grdName = "dg_ReminderCalls";	
			    
			    //Yasir Kamal 5427 01/23/2009 Pagging functionality Added.
			    
			    var idx=3;			
			    cnt = cnt+2;
			    for (idx=3; idx < (cnt); idx++)
			    {	
			        var txtbox="";
			            if( idx < 10)
			            {				
			                 txtbox = grdName+ "_ctl0" + idx + "_txt_sno"
			            }
			            else
			                txtbox = grdName+ "_ctl" + idx + "_txt_sno"    
			            var txtfocus=document.getElementById(txtbox);							
			            var compare=txtfocus.value;
			          if (compare==id)
			          {
				          txtfocus.focus();
				          <%Session["ReminID"]=1;%>;				 //initializing to 1 
				          break;			
			          }
			    }		
			}
		var chkbond=document.getElementById("chkBondClient");        
		var chkReguler=document.getElementById("chkRegulerClient");
//        var cmbbox = document.Form2.rb_calls;    
//        //alert ("Before");
//        if (cmbbox[2].checked == true)
//        {           
//            chkbond.disabled = true;
//            chkbond.checked = false;
//            chkReguler.disabled = true;
//            chkReguler.checked = true;            
//        }
			
	}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style2
        {
        }
        .style3
        {
            width: 13px;
        }
        .style5
        {
            width: 100px;
            direction: ltr;
        }
    </style>
</head>
<body onload="SetFocusOnGrid();">
    <form id="Form2" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="height: 9px" width="780" background="../../images/separator_repeat.gif"
                                height="9">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 9px" colspan="4" align="center">
                    <table>
                        <tr>
                            <td style="height: 9px" class="clslabel" align="right">
                                &nbsp; Crt/Set date:
                            </td>
                            <td style="height: 9px" class="clslabel">
                                <ew:CalendarPopup ID="cal_EffectiveFrom" runat="server" EnableHideDropDown="True"
                                    ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                    AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
                                    PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Text=" " Width="80px">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td class="style2" align="right">
                                &nbsp;&nbsp;<span class="clslabel">&nbsp;Call status :</span>
                            </td>
                            <td class="clslabel">
                                &nbsp;
                                <asp:DropDownList runat="server" ID="ddl_rStatus" CssClass="clsInputCombo" OnSelectedIndexChanged="ddl_rStatus_SelectedIndexChanged1">
                                </asp:DropDownList>
                            </td>
                            <td class="clslabel">
                                &nbsp; Language :
                            </td>
                            <td class="style5">
                                <asp:DropDownList ID="dd_language" CssClass="clsInputCombo" runat="server" Width="100">
                                    <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="English" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Spanish" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px" class="clslabel">
                                <asp:CheckBox ID="chkBondClient" runat="server" CssClass="clslabel" Text="Bond Client"
                                    Checked="true" />
                            </td>
                            <td style="height: 9px" align="right" colspan="2">
                                &nbsp;
                                <asp:CheckBox ID="chkRegulerClient" runat="server" CssClass="clslabel" Text="Regular Client"
                                    Checked="true" />
                            </td>
                            <td align="center" colspan="2">
                                &nbsp; &nbsp;
                                <asp:CheckBox ID="cb_showall" runat="server" Text="Show All Cases" CssClass="clslabel" />
                            </td>
                            <td align="right">
                                <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="clsbutton"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="upnlResult" runat="server">
                        <ContentTemplate>
                            <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                border="0">
                                <tr>
                                    <td style="height: 11px" width="780" background="../../images/separator_repeat.gif"
                                        colspan="5" height="11">
                                    </td>
                                </tr>
                                 <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                    <%--Yasir Kamal 5427 01/23/2009 Pagging functionality Added.--%>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="right" valign="middle">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 13px" align="center">
                                        <asp:Label ID="lblMessage" runat="server" Width="320px" CssClass="label" ForeColor="Red"
                                            EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <%--<tr>
                                <td id="tdWait" runat="server" class="clssubhead" valign="middle" align="center"
                                        style="display: none">
                                        <img alt="Please wait" src="/Images/plzwait.gif" />
                                        Please wait While we update information.
                                    </td>
                                    </tr>--%>
                                     <tr>
                                    <td align="center">
                                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" 
                                            AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server" 
                                                    CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td id="tdData" runat="server" valign="top" align="center" colspan="2">
                                        <asp:DataGrid AllowPaging="true" HeaderStyle-HorizontalAlign="Left" 
                                            PageSize="25" ID="dg_ReminderCalls" runat="server" CssClass="clsLeftPaddingTable"
                                            AutoGenerateColumns="False" BorderStyle="Solid" 
                                            onpageindexchanged="dg_ReminderCalls_PageIndexChanged">
                                             <PagerStyle HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <table class="clsleftpaddingtable" id="Table1" cellspacing="1" cellpadding="0" width="780"
                                                            align="center" border="0">
                                                            <tr>
                                                             <td> 
                                                                <asp:Label ID="lblSno" runat="server" CssClass="label" ForeColor="#123160">S No:</asp:Label>&nbsp;<asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid")+"&search=0" %>'
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink></td>
                                                                <td colspan="4">
                                                                    <asp:Label ID="lbl_owes" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.owedamount", "{0:C0}") %>'
                                                                        CssClass="label" ForeColor="Red" Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_Firm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="clsLeftPaddingTable" valign="middle" width="105" bgcolor="#eff4fb" height="20">
                                                                    <asp:Label ID="Label11" runat="server" CssClass="label" ForeColor="#123160">Name:</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20">
                                                                    <asp:HyperLink ID="lnkName" runat="server" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.LastName"),", ",(DataBinder.Eval(Container, "DataItem.FirstName") )) %>'
                                                                        CssClass="label">
                                                                    </asp:HyperLink>
                                                                    <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketID") %>'
                                                                        CssClass="label" ForeColor="#123160" Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_RID" runat="server" CssClass="label" ForeColor="#123160" Visible="False"></asp:Label>
                                                                </td>
                                                                <td valign="top" width="123" bgcolor="#eff4fb" height="20" rowspan="3" align="left">
                                                                    <p>
                                                                        <asp:Label ID="lbl_contact1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'
                                                                            CssClass="label">
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Contact2" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'
                                                                            CssClass="label">
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Contact3" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'
                                                                            CssClass="label">
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Language" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Languagespeak") %>'
                                                                            CssClass="label">
                                                                        </asp:Label><br>
                                                                        <asp:HyperLink ID="lnk_Comments" runat="server">Add Comments </asp:HyperLink>
                                                                        <asp:Label ID="lbl_phone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'
                                                                            Visible="False">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lbl_Fax" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fax") %>'
                                                                            Visible="False">
                                                                        </asp:Label></p>
                                                                </td>
                                                                <td valign="top" bgcolor="#eff4fb" height="20" rowspan="5">
                                                                    <div style="width: 245px; height: 83px; overflow: auto">
                                                                        <asp:Label ID="lbl_comments" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Comments") %>'
                                                                            CssClass="label">
                                                                        </asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" width="105" bgcolor="#eff4fb" height="20">
                                                                    <asp:Label ID="Label12" runat="server" CssClass="label" ForeColor="#123160">Case Status:</asp:Label>
                                                                </td>
                                                                <td class="clsLeftPaddingTable" width="310" bgcolor="#eff4fb" height="20">
                                                                    <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialDesc") %>'
                                                                        CssClass="label" ForeColor="#123160" Font-Bold="True">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_TicketViolationID" runat="server" CssClass="label" ForeColor="#123160"
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.TicketViolationIds") %>' Visible="False"> </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="clsLeftPaddingTable" valign="middle" width="105" bgcolor="#eff4fb" height="20">
                                                                    <asp:Label ID="Label14" runat="server" CssClass="label" ForeColor="#123160">Call Back:</asp:Label>
                                                                    <asp:TextBox ID="txt_sno" runat="server" Width="1px" ForeColor="#EFF4FB" Height="1px"
                                                                        BackColor="#EFF4FB" BorderColor="#EFF4FB" BorderStyle="Solid"></asp:TextBox>
                                                                </td>
                                                                <td align="center" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20">
                                                                    <div align="left">
                                                                        <asp:Label ID="lbl_CallBacks" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                                                            CssClass="label" ForeColor="#123160"></asp:Label>
                                                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" ForeColor="#123160"
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>' Visible="False"> </asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" ForeColor="#123160">Dialer Status:</asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DialerStatus") %>'
                                                                        CssClass="Label" ForeColor="#123160">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#eff4fb" colspan="4" height="20" rowspan="1" valign="top">
                                                                    <asp:Label ID="lbl_BondFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'
                                                                        Visible="False">
                                                                    </asp:Label><asp:Label ID="lbl_Bond" runat="server" Visible="False" Font-Bold="True"
                                                                        BackColor="#FFCC66">Bond<span style="background-color:#eff4fb;">&nbsp;</span></asp:Label><asp:Label
                                                                            ID="lblWrongFlag" runat="server" Visible='<%# Convert.ToBoolean(Eval("WrongNumberFlag")) %>'
                                                                            Font-Bold="True" BackColor="#FFCC66">Wrong Telephone Number<span style="background-color:#eff4fb;">&nbsp;</span></asp:Label><asp:Label
                                                                                ID="lbl_Insurance" runat="server" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"
                                                                                Text='<%# DataBinder.Eval(Container, "DataItem.Insurance") %>' Visible="False"></asp:Label><asp:Label
                                                                                    ID="lbl_Child" runat="server" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"
                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.Child") %>' Visible="False"></asp:Label>
                                                                                    <asp:Label ID="LblContinuance" runat="server" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"
                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.Continuance") %>' Visible="False"></asp:Label>
                                                                    <asp:HiddenField ID="hf_GeneralComments" runat="server" Value="<%# Bind('GeneralComments') %>" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 11px" width="780" background="../../images/separator_repeat.gif"
                                                                    colspan="4" height="11">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" NextPageText="&nbsp; Next &gt;" PrevPageText="&lt; Prev &nbsp;" />
                                        </asp:DataGrid>
                                        <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                         </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableComment" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                        border="0">
                        <tr class="clsleftpaddingtable">
                            <td class="clsaspcolumnheader">
                            </td>
                            <td class="clsaspcolumnheader" colspan="2">
                            </td>
                        </tr>
                        <tr class="clsleftpaddingtable">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td valign="bottom" align="right" colspan="1" rowspan="1">
                            </td>
                        </tr>
                        <tr>
                            <td width="780" background="../../images/separator_repeat.gif" colspan="5" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none">
                                <asp:TextBox ID="txt_totalrecords" runat="server">
                                </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

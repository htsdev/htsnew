using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

namespace lntechNew.QuickEntryNew
{
    public partial class TrialDocketPopup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            

            FileInfo file = new FileInfo(Request.QueryString["path"]);
             
               // Checking if file exists
               if (file.Exists)
               {
                   // Clear the content of the response
                   Response.ClearContent();

                   // LINE1: Add the file name and attachment, which will force the


                   Response.AddHeader("Content-Disposition", "inline; filename=" + file.Name);


                   // Add the file size into the response header
                   Response.AddHeader("Content-Length", file.Length.ToString());


                   // Set the ContentType
                   Response.ContentType = "application/pdf";

                   // Write the file into the response
                   Response.WriteFile(file.FullName);

                   Response.Flush();

                   Response.Close();

                   file.Delete();

                   //System.Net.WebClient client = new System.Net.WebClient();
                   //Byte[] buffer = client.DownloadData(Request.QueryString["path"]);

                   //if (buffer != null)
                   //{
                   //    Response.ContentType = "application/pdf";
                   //    Response.AddHeader("content-length", buffer.Length.ToString());
                   //    Response.BinaryWrite(buffer);
                   //}
               }

        }
    }
}

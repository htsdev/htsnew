using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.QuickEntryNew
{
    public partial class frmPreviewDocs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string docid = Request.QueryString["docid"].ToString();
                ViewState["FilePath"] = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
                string Path = ViewState["FilePath"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 1) + "DOCID_"+docid + ".pdf";
                string sPath = ViewState["FilePath"].ToString() + "DOCID_" + docid + ".pdf";
                if (System.IO.File.Exists(sPath))
                {
                    Response.Redirect("../Docstorage" + Path, false);
                }
                else
                { Response.Write("Document Not Found"); }
            }
        }
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace HTP.QuickEntryNew
{
    public partial class ArrCombined : System.Web.UI.Page
    {
        #region Variables

        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        private DateTime CSDMon;
        private DateTime CSDTue;
        private DateTime CSDWed;
        private DateTime CSDThr;
        private DateTime CSDFri;
        DataView DV;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        //network path
                        ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                        ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH1"].ToString();
                        ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                        ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
                        
                        lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());
                        // Afqa 8039 08/19/2010 use for stop event bubbling.
                        if (Request.QueryString["ReportType"] != null)
                        BindReportType(Request.QueryString["ReportType"].ToString());
                    }
                }
                lnk_bsumm.Attributes.Add("onclick", "return Prompt(" + 4 + ");");
                HPARRSumm.Attributes.Add("onclick", "return Prompt(" + 2 + ");");
                btnARR.Attributes.Add("onclick", "return Prompt(" + 1 + ");");
                btnBoncSumm.Attributes.Add("onclick", "return Prompt(" + 3 + ");");
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dg_ProbReq_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid = ((Label)e.Item.FindControl("lbl_TicketID")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lbl_CauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_WeekDay_SelectedIndexChanged1(object sender, EventArgs e)
        {
            GenerateReport(ddl_WeekDay.SelectedValue);
        }

        protected void dgbond_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid = ((Label)e.Item.FindControl("bticketid")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("bCauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("serialNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((ImageButton)(e.Item.FindControl("ImgBond"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF('" + ticketid + "','0','0','');");
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Afaq 8210 11/01/2010 Add data bound event for continuance data grid (dg_Cont).
        protected void dg_Cont_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid = ((Label)e.Item.FindControl("lbl_TicketID")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lbl_CauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        protected void btnBondSumm_Click(object sender, EventArgs e)
        {
            try
            {
                string sTVIDs = "";
                foreach (DataGridItem Itemx in dgbond.Items)
                {
                    CheckBox chk = (CheckBox)Itemx.FindControl("chk");
                    Label lbl = (Label)Itemx.FindControl("btvids");
                    if (chk.Checked == true)
                    {
                        if (sTVIDs != String.Empty)
                        {
                            sTVIDs = String.Concat(sTVIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            sTVIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }

                }

                Session["sTVIDS"] = sTVIDs;
                if (HD.Value == "1")//for generating docId..
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=1','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');</script>");

                }
                if (HD.Value == "2")//for generating only preview..
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=2','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        protected void ASdg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    ((HyperLink)e.Item.FindControl("AScauseno")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnARR_Click(object sender, EventArgs e)
        {
            try
            {
                string sTVIDs = "";
                foreach (DataGridItem Itemx in ASdg.Items)
                {
                    CheckBox chk = (CheckBox)Itemx.FindControl("chk1");
                    Label lbl = (Label)Itemx.FindControl("ASTVIDS");
                    if (chk.Checked == true)
                    {
                        if (sTVIDs != String.Empty)
                        {
                            sTVIDs = String.Concat(sTVIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            sTVIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }

                }

                Session["ARRTVIDS"] = sTVIDs;

                if (HD.Value == "1")//for generating docId..
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=1','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");

                }
                if (HD.Value == "2")//for generating only preview..
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=2','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindArraignmentWaiting();
        }

        protected void btn_Search_BondWaiting_Click(object sender, EventArgs e)
        {
            if (ddl_WeekDay.SelectedValue == "998")
            {
                BindBondWaiting();
            }
            else if (ddl_WeekDay.SelectedValue == "999")
            {
                BindArraignmentWaiting();
            }
        }

        protected void btn_PrintReport_Click(object sender, EventArgs e)
        {
            string IDs = "";

            foreach (DataGridItem ItemX in dg_AWR.Items)
            {
                CheckBox Chk_Select = (CheckBox)ItemX.FindControl("chk_Select");
                if (Chk_Select.Checked)
                {
                    Label lbl = (Label)ItemX.FindControl("ASTVIDS");
                    IDs += lbl.Text + ",";
                }

            }

            Session["ARRTVIDS"] = IDs;

            if (hf_ArrWaiting_GenIDs.Value == "1")
            {

                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=5','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=6','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
        }

        protected void btn_PrintReport_Bond_Click(object sender, EventArgs e)
        {
            string IDs = "";

            foreach (DataGridItem ItemX in dg_BondWaiting.Items)
            {
                CheckBox Chk_Select = (CheckBox)ItemX.FindControl("chk");
                if (Chk_Select.Checked)
                {
                    Label lbl = (Label)ItemX.FindControl("btvids");
                    IDs += lbl.Text + ",";
                }

            }
            Session["sTVIDS"] = IDs;
            if (hf_ArrWaiting_GenIDs.Value == "1")
            {

                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=5','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=6','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindArraignmentWaiting();
        }

        protected void ASdg_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid(e.SortExpression);
        }       

        protected void dg_AWR_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid_ArrWaiting(e.SortExpression);
        }

        protected void dgbondwaiting_sortcommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid_BondWaiting(e.SortExpression);
        }

        protected void dgbond_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGridBond(e.SortExpression);
        }

        protected void Bondwaiting_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid = ((Label)e.Item.FindControl("bticketid")).Text.ToString();

                    ((HyperLink)e.Item.FindControl("bCauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("serialNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;

                    //Added by ozair  
                    Label Lname = (Label)e.Item.FindControl("blastname");
                    Label Fname = (Label)e.Item.FindControl("bfirstname");
                    if (Lname.Text.Trim().Length > 12)
                    {
                        Lname.ToolTip = Lname.Text;
                        Lname.Text = Lname.Text.Substring(0, 9) + "...";
                    }
                    if (Fname.Text.Trim().Length > 12)
                    {
                        Fname.ToolTip = Fname.Text;
                        Fname.Text = Fname.Text.Substring(0, 9) + "...";
                    }

                    Label con1 = (Label)e.Item.FindControl("lblSc");//Code Added by Azee..
                    ImageButton img1 = (ImageButton)e.Item.FindControl("ImgDoc1");
                    if (con1.Text != "" && Convert.ToInt32(con1.Text) >= 1)
                    {
                        img1.Visible = true;
                        con1.Visible = true;
                        ((ImageButton)e.Item.FindControl("ImgDoc1")).Attributes.Add("onclick", "javascript:return PopUp(" + ticketid.ToString() + ");");
                    }
                    else
                    {
                        img1.Visible = false;
                        con1.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void AWRWaiting_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("AScauseno")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();

                    //Added by ozair  
                    Label Lname = (Label)e.Item.FindControl("ASlastname");
                    Label Fname = (Label)e.Item.FindControl("ASfirstname");
                    if (Lname.Text.Trim().Length > 12)
                    {
                        Lname.ToolTip = Lname.Text;
                        Lname.Text = Lname.Text.Substring(0, 9) + "...";
                    }
                    if (Fname.Text.Trim().Length > 12)
                    {
                        Fname.ToolTip = Fname.Text;
                        Fname.Text = Fname.Text.Substring(0, 9) + "...";
                    }

                    ImageButton img = (ImageButton)e.Item.FindControl("ImgDoc");
                    Label con = (Label)e.Item.FindControl("lblScripts");
                    if (con.Text != "" && Convert.ToInt32(con.Text) >= 1)
                    {
                        img.Visible = true;
                        con.Visible = true;
                        ((ImageButton)e.Item.FindControl("ImgDoc")).Attributes.Add("onclick", "javascript:return PopUp(" + ticketid.ToString() + ");");
                    }
                    else
                    {
                        img.Visible = false;
                        con.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void PrintAllBonds_OnClick(object sender, EventArgs e)
        {
            try
            {
                string TicketIDs = "";
                foreach (DataGridItem Itemx in dg_BondWaiting.Items)
                {
                    CheckBox chk = (CheckBox)Itemx.FindControl("chk");
                    Label lbl = (Label)Itemx.FindControl("bticketid");
                    if (chk.Checked == true)
                    {
                        if (TicketIDs != String.Empty)
                        {
                            TicketIDs = String.Concat(TicketIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            TicketIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }

                }

                Session["TicketID4Bonds"] = TicketIDs;
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/PrintAllBonds.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');</script>");
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }

        }
      
        //noufil 4213 06/16/2008 Report for monday,tuesday,wednesday thursday,friday has been deleted.

        #endregion

        #region Methonds
        // Afqa 8039 08/19/2010
        /// <summary>
        /// It will generate the report after page refresh use for stop event bubbling
        /// </summary>
        /// <param name="reportType">Report Type</param>
        private void BindReportType(string reportType)
        {
            switch (reportType)
            { 
                
                case "1":
                    ddl_WeekDay.SelectedValue = "998";
                    GenerateReport("998");
                    break;
                case "2":
                    ddl_WeekDay.SelectedValue = "999";
                    GenerateReport("999");
                    break;

            }
        }
        // Afqa 8039 08/19/2010
        /// <summary>
        /// It will Generate the report according to the given report type
        /// </summary>
        /// <param name="reportType">Report Type</param>
        private void GenerateReport(string reportType)
        {
            try
            {
                lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());
                clsENationWebComponents clsDB = new clsENationWebComponents();
                lbl_Message.Text = "";
                hp.Visible = false;
                HPARRSumm.Visible = false;
                lnk_bsumm.Visible = false;
                lblSep.Visible = false;
                //txtDocid.Text = "";
                PrintAllBonds.Visible = false;
                btn_PrintReport_Bond.Visible = false;
                ImgCrystalSingle.Visible = false;
                //ImgCrystalAll.Visible = false;

                if (reportType.Equals("100"))
                {
                    longshot.Style["display"] = "";
                    //snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    //tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    DataSet ds_JT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1_Update");

                    if (ds_JT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportJT.DataSource = ds_JT;
                        dg_ReportJT.DataBind();
                        GenerateSerialJT();
                        //imgPrint_Ds_to_Printer.Visible = true;
                        //img_pdfLong.Visible = true;
                        //img_PdfAll.Visible = true;
                        //ImageButton2.Visible = true;
                        ImgCrystalSingle.Visible = true;
                        HF_ArraignmentDocket.Value = ddl_WeekDay.SelectedValue.ToString();
                        //ImgCrystalAll.Visible = true;
                    }
                    else
                    {
                        //imgPrint_Ds_to_Printer.Visible = false;
                        //img_pdfLong.Visible = false;
                        ImgCrystalSingle.Visible = false;
                        //ImgCrystalAll.Visible = false;
                    }
                    GetPR_POAReport();
                    // Afaq 8213 11/01/2010 Calling method for creating Continuance Report.
                    GetContinuance_Report();

                }

                //code added by Azee..for displaying bond summary grid
                else if (reportType.Equals("150"))//150 Means Bond Summary option in dropdown..
                {
                    longshot.Style["display"] = "none";
                    //snapshot.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    BforBond.Style["display"] = "";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    //tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    BindBondGrid();


                }

                else if (reportType.Equals("998"))
                {
                    longshot.Style["display"] = "none";
                    //snapshot.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    BforBond.Style["display"] = "";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "";
                    // tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    BindBondWaiting();
                }
                //code added by Azee for displaying arraignment summary grid
                else if (reportType.Equals("160"))//160 Means Arraignment summary options in dropwon..
                {
                    longshot.Style["display"] = "none";
                    //snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "";
                    Td2.Style["display"] = "";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    //tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    BindArraignmentSummary();


                }
                //

                else if (reportType.Equals("999"))
                {
                    longshot.Style["display"] = "none";
                    //snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    btn_PrintReport.Visible = false;
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "";
                    //tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    BindArraignmentWaiting();

                }

                if (reportType.Equals("-1"))
                {
                    longshot.Style["display"] = "none";
                    //snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    // tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style["Display"] = "none";

                    //imgPrint_Ds_to_Printer.Visible = false;
                    //Imagebutton1.Visible = false;
                    //img_pdfLong.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    ImgCrystalSingle.Visible = false;
                    //ImgCrystalAll.Visible = false;

                    tbl_ArrWaiting.Style["Display"] = "none";

                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenerateSerialJT()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_ReportJT.Items)
                {
                    Label sno1 = (Label)ItemX.FindControl("lbl_sno1");
                    Label tid1 = (Label)ItemX.FindControl("lbl_Ticketid1");
                    if (tid1.Text != "")
                    {
                        if (no == 1)
                        {
                            sno1.Text = no.ToString();
                            ticketid = tid1.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid1.Text)
                            {
                                sno1.Text = Convert.ToString(no);
                                ticketid = tid1.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno1.Text = "";
                            }
                        }
                        Label date = (Label)ItemX.FindControl("lbl_CCDate");
                        Label num = (Label)ItemX.FindControl("lbl_CCNum");
                        if (date.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(date.Text.ToString());

                            date.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year, " @ ", adate.ToShortTimeString(), " # ", num.Text);
                        }
                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenerateSerialBond()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dgbond.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("serialNo");
                    Label tid = (Label)ItemX.FindControl("bticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private void GenerateArraignmentSerial()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in ASdg.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("lblSNo");
                    Label tid = (Label)ItemX.FindControl("ASTIDS");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private void GenerateSerialBondWaiting()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_BondWaiting.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("serialNo");
                    Label tid = (Label)ItemX.FindControl("bticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        //snap short
        private void FindStartDate()
        {
            DateTime strDate = DateTime.Now.AddDays(21);
            int intDay = 0;
            for (intDay = 0; intDay <= 6; intDay++)
            {
                strDate = DateTime.Now.AddDays(intDay); //DateAdd("d",intDay,date())		
                if (strDate.DayOfWeek != DayOfWeek.Saturday && strDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (strDate.DayOfWeek == DayOfWeek.Monday)
                    {
                        CSDMon = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Tuesday)
                    {
                        CSDTue = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        CSDWed = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Thursday)
                    {
                        CSDThr = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Friday)
                    {
                        CSDFri = strDate;
                    }
                }
            }
        }

        private void BindBondGrid()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                //ozair 4073 on 05/21/2008 parameter batchid changed to DocumentBatchID
                string[] key = { "@TVIDS", "@DocumentBatchID", "@updateflag", "@employee" };
                object[] value = { "", 0, 0, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)) };
                DataSet ds_bondSummary = clsDB.Get_DS_BySPArr("usp_HTS_Bond_Report", key, value);
                if (ds_bondSummary.Tables[0].Rows.Count > 0)
                {
                    dgbond.DataSource = ds_bondSummary;
                    DV = new DataView(ds_bondSummary.Tables[0]);//data view paging
                    Session["sDV"] = DV;
                    //   GenerateSerial(ds_bondSummary.Tables[0]);
                    dgbond.DataBind();
                    txtRowCount.Text = dgbond.Items.Count.ToString();
                    GenerateSerialBond();
                    lnk_bsumm.Visible = true;
                    //imgPrint_Ds_to_Printer.Visible = false;
                   // Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    HPARRSumm.Visible = false;
                    btnBoncSumm.Visible = true;
                    hp.Visible = true;
                    lblSep.Visible = true;

                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    //imgPrint_Ds_to_Printer.Visible = false;
                   // Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    HPARRSumm.Visible = false;
                    hp.Visible = false; ;
                    BforBond.Style["display"] = "none";
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindArraignmentSummary()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] key = { "@TVIDS", "@DocumentBatchID" };
                object[] value = { "", 0 };
                DataSet Ds_ARRSummary = clsDB.Get_DS_BySPArr("USP_HTS_ARRAIGNMENT_REPORT", key, value);
                if (Ds_ARRSummary.Tables[0].Rows.Count > 0)
                {
                    ASdg.DataSource = Ds_ARRSummary;
                    DV = new DataView(Ds_ARRSummary.Tables[0]);//data view paging
                    Session["sDV"] = DV;
                    ASdg.DataBind();
                    GenerateArraignmentSerial();
                    // GenerateSerial(Ds_ARRSummary.Tables[0]);                    
                    txtARRSummCount.Text = ASdg.Items.Count.ToString();
                    HPARRSumm.Visible = true;
                    lnk_bsumm.Visible = false;
                    //imgPrint_Ds_to_Printer.Visible = false;
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                   // ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    btnBoncSumm.Visible = false;

                    hp.Visible = true;
                    lblSep.Visible = true;
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    //imgPrint_Ds_to_Printer.Visible = false;
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";

                    HPARRSumm.Visible = false;
                    hp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenerateAWRSerial()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_AWR.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("lblSNo");
                    Label tid = (Label)ItemX.FindControl("ASTIDS");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private DataTable GenerateNOSSerial(DataTable dt)
        {

            try
            {

                if (dt.Rows.Count > 0)
                {
                    int no = 1;
                    string ticketid = "";
                    int sno = 1;

                    ticketid = dt.Rows[0]["TicketID"].ToString();
                    dt.Rows[0]["SNO"] = sno;
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {

                        string tid = dt.Rows[i]["TicketID"].ToString();
                        if (tid != "")
                        {
                            if (ticketid != tid)
                            {
                                no++;
                                dt.Rows[i]["SNO"] = no;
                                ticketid = tid;

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
            return dt;
        }

        private void GenerateSerial(DataTable dt)
        {
            try
            {
                int no = 1;
                string ticketid = "";

                if (dt.Columns["SNO"] == null)
                {
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                else
                {
                    dt.Columns.Remove("SNO");
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                ticketid = dt.Rows[0]["TicketID"].ToString();

                for (int i = 1; i < dt.Rows.Count; i++)
                {

                    string tid = dt.Rows[i]["TicketID"].ToString();
                    if (tid != "")
                    {
                        if (ticketid != tid)
                        {
                            no++;
                            dt.Rows[i]["SNO"] = no;
                            ticketid = tid;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private void BindArraignmentWaiting()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] keys = { "@fromDate", "toDate", "@showAllFlag" };
                object[] values = { cal_From.SelectedDate, cal_To.SelectedDate, Convert.ToInt16(chk_ShowAllRec.Checked) };
                DataSet Ds_ARRSummary = clsDB.Get_DS_BySPArr("USP_HTS_ARRAIGNMENT_WAITING_REPORT", keys, values);
                if (Ds_ARRSummary.Tables[0].Rows.Count > 0)
                {
                    dg_AWR.DataSource = Ds_ARRSummary;
                    DV = new DataView(Ds_ARRSummary.Tables[0]);//data view paging
                    //Session["sDV"] = DV;
                    //Session["AWR"] = DV;
                    GenerateSerial(DV.Table);
                    Session["AWR"] = Ds_ARRSummary.Tables[0];
                    dg_AWR.DataBind();
                    btn_PrintReport.Visible = true;
                    //   GenerateAWRSerial();
                    txtARRSummCount.Text = dg_AWR.Items.Count.ToString();
                    HPARRSumm.Visible = false;
                    lnk_bsumm.Visible = false;
                    //mgPrint_Ds_to_Printer.Visible = false;
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    btnBoncSumm.Visible = false;
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "";
                    hf_Count.Value = Ds_ARRSummary.Tables[0].Rows.Count.ToString();

                    hp.Visible = false;
                    lblSep.Visible = false;
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    //imgPrint_Ds_to_Printer.Visible = false;
                   // Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    btn_PrintReport.Visible = false;

                    HPARRSumm.Visible = false;
                    hp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Cannot find table 0.")
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                else
                    lbl_Message.Text = "No Record Found";
            }
        }

        private void BindBondWaiting()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] keys = { "@fromDate", "toDate", "@showAllFlag" };
                object[] values = { cal_From.SelectedDate, cal_To.SelectedDate, Convert.ToInt16(chk_ShowAllRec.Checked) };
                DataSet Ds_ARRSummary = clsDB.Get_DS_BySPArr("USP_HTS_BOND_WAITING_REPORT", keys, values);
                if (Ds_ARRSummary.Tables[0].Rows.Count > 0)
                {
                    dg_BondWaiting.DataSource = Ds_ARRSummary;
                    DV = new DataView(Ds_ARRSummary.Tables[0]);//data view paging
                    Session["sDV"] = DV;
                    GenerateSerial(Ds_ARRSummary.Tables[0]);
                    dg_BondWaiting.DataBind();
                    btn_PrintReport.Visible = true;
                    //  GenerateSerialBondWaiting();
                    txtARRSummCount.Text = dg_AWR.Items.Count.ToString();
                    HPARRSumm.Visible = false;
                    lnk_bsumm.Visible = false;
                    //imgPrint_Ds_to_Printer.Visible = false;
                    //Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                   // ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    btnBoncSumm.Visible = false;
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "";
                    hp.Visible = false;
                    lblSep.Visible = false;
                    hf_Count.Value = Ds_ARRSummary.Tables[0].Rows.Count.ToString();
                    PrintAllBonds.Visible = true;
                    btn_PrintReport_Bond.Visible = true;

                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    //imgPrint_Ds_to_Printer.Visible = false;
                   // Imagebutton1.Visible = false;
                    //img_pdfShort.Visible = false;
                    //img_PdfAll.Visible = false;
                    //ImageButton2.Visible = false;
                    //img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";

                    HPARRSumm.Visible = false;
                    hp.Visible = false;
                    btn_PrintReport_Bond.Visible = true;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Cannot find table 0.")
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                else
                    lbl_Message.Text = "No Record Found";
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                ASdg.DataSource = DV;
                ASdg.DataBind();
                GenerateArraignmentSerial();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        private void SortGrid_ArrWaiting(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DataView DVAWR = new DataView();
                DVAWR.Table = (DataTable)Session["AWR"];
                DVAWR.Sort = StrExp + " " + StrAcsDec;
                GenerateSerial(DVAWR.Table);
                dg_AWR.DataSource = DVAWR;
                dg_AWR.DataBind();
                // GenerateAWRSerial();

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void SortGrid_BondWaiting(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                GenerateSerial(DV.Table);
                dg_BondWaiting.DataSource = DV;
                dg_BondWaiting.DataBind();
                // GenerateSerialBondWaiting();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void SortGridBond(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                dgbond.DataSource = DV;
                dgbond.DataBind();
                GenerateSerialBond();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }
        
        private void GetPR_POAReport()
        {
            try
            {
                DataTable dtPR = FormatPRReport(ClsDb.Get_DT_BySPArr("USP_HTS_GET_PR_POA_REPORT"));
                dg_ProbReq.DataSource = dtPR;
                dg_ProbReq.DataBind();
                if (dtPR.Rows.Count > 0)
                    tbl_PR_Report.Style[HtmlTextWriterStyle.Display] = "";
                else
                    tbl_PR_Report.Style[HtmlTextWriterStyle.Display] = "none";
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private DataTable FormatPRReport(DataTable dtToFormat)
        {
            DataTable dt = dtToFormat.Copy();
            dt.Columns.Add("SNO", typeof(int));
            int lastTicketID = -1;
            int sno = 1;
            int currentID = -1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)
                {
                    dt.Rows[i]["SNO"] = sno;
                    sno++;
                    lastTicketID = Convert.ToInt32(dt.Rows[i]["TicketID_PK"]);
                    continue;
                }
                else
                {
                    currentID = Convert.ToInt32(dt.Rows[i]["TicketID_PK"]);
                    if (lastTicketID != currentID)
                    {
                        dt.Rows[i]["SNO"] = sno;
                        sno++;
                        lastTicketID = currentID;
                    }
                }
            }
            return dt;
        }
        // Afaq 8213 11/01/2010 This method used to create the Continuance report.
        private void GetContinuance_Report()
        {
            try
            {
                DataTable dt = FormatPRReport(ClsDb.Get_DT_BySPArr("USP_HTP_GET_Trial_Continuance_REPORT"));
                dg_Cont.DataSource = dt;
                dg_Cont.DataBind();
                if (dt.Rows.Count > 0)
                    tbl_Cont_Report.Style[HtmlTextWriterStyle.Display] = "";
                else
                    tbl_Cont_Report.Style[HtmlTextWriterStyle.Display] = "none";
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        #endregion
    }
}

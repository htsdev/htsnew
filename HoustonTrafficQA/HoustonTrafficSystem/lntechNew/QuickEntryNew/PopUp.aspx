<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopUp.aspx.cs" Inherits="HTP.QuickEntryNew.PopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Page</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script language="javascript">
        function PopUp(recordid)
		{
		    var path="../ClientInfo/frmPreview.aspx?RecordID="+recordid;
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
		    return false;
		}
		//ozair 4073 on 05/26/2008 method to show verified scanned document from document tracking system
        function PopUpShowPreviewDT(DBID,SBID)
	    {
		    window.open ("../documenttracking/PreviewMain.aspx?DBID="+ DBID +"&SBID="+ SBID);
		    return false;
	    }
        //end ozair 4073   
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="400">
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgdoc" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                        BorderStyle="Solid" Width="100%" OnItemDataBound="dgdoc_ItemDataBound">
                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                        <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Scan Documents">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HP" runat="server" Text='<%# bind("batchid") %>' NavigateUrl="#"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="recordid" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblrecordid" runat="server" Text='<%# bind("recordid") %>'></asp:Label>
                                    <asp:Label ID="lbl_RecType" runat="server" Text='<%# bind("rectype") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

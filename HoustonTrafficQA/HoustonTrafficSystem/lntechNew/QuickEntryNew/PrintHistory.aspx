<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintHistory.aspx.cs" Inherits="lntechNew.QuickEntryNew.PrintHistory" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Print Arraignment and Bond Summary History</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="Styles.css" type="text/css" rel="stylesheet" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        
        <script language="javascript">
           function PopUpSummaryPreview(filename)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("PreviewPrintHistory.aspx?filename="+ filename,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
				return false;
			}
			
			function Validate()
			{
			    var ddlvalue=document.getElementById("ddlReportType").value;
			    if(ddlvalue == "3")
			    {
			        alert("Please Select Report Type!");
			        document.getElementById("ddlReportType").focus();
			        return false;
			    }
			}
        </script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tr><td >
			    <uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
			</td></tr>
			<TR>
								<TD background="../../images/separator_repeat.gif" Height="11" ></TD>
							</TR>
				<TR>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" height="34">
					    <table border="0" cellpadding="0" cellspacing="0" width="100%">
					    <tr>
					    <td class="clssubhead" style="height: 13px">
					&nbsp;Arraignment and Bond Summary Report History&nbsp;</td>
					<td class="clssubhead" align="right" style="height: 13px">
                        <asp:HyperLink ID="HPBack" runat="server" NavigateUrl="~/QuickEntryNew/ArrCombined.aspx">Back</asp:HyperLink>&nbsp;
					</td>
					</tr>
					</table>
					
					</TD>
					
				</TR>
				<tr>
					<td align="center">
						<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
				</Tr>
				<tr><td>
				    <table border="0" width="100%" cellpadding="0" cellspacing="0">
				    <tr>
				    <td width="30%">
                    Select Report Type
                    <asp:DropDownList ID="ddlReportType" runat="server">
                        <asp:ListItem Value="3">---Choose---</asp:ListItem>
                        <asp:ListItem Value="1">Arraignment </asp:ListItem>
                        <asp:ListItem Value="2">Bond</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                    <td width="5%">From</td>
                    <td width="15%"><ew:calendarpopup id="cal_todate" runat="server" allowarbitrarytext="False" calendarlocation="Bottom"
                            controldisplay="TextBoxImage" culture="(Default)" font-names="Tahoma" font-size="8pt"
                            imageurl="../images/calendar.gif" padsingledigits="True" showgototoday="True"
                            tooltip="Select Court Date Range" upperbounddate="12/31/9999 23:59:00" width="90px"> <TEXTBOXLABELSTYLE CssClass="clstextarea" /><WEEKDAYSTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><MONTHHEADERSTYLE ForeColor="Black" BackColor="Yellow" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><OFFMONTHSTYLE ForeColor="Gray" BackColor="AntiqueWhite" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><GOTOTODAYSTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><TODAYDAYSTYLE ForeColor="Black" BackColor="LightGoldenrodYellow" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><DAYHEADERSTYLE ForeColor="Black" BackColor="Orange" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><WEEKENDSTYLE ForeColor="Black" BackColor="LightGray" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><SELECTEDDATESTYLE ForeColor="Black" BackColor="Yellow" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><CLEARDATESTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /><HOLIDAYSTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" /></ew:calendarpopup>
                            </td>
                            <td width="5%">To</td>
                            <td width="20%"><ew:calendarpopup id="cal_fromDate" runat="server" allowarbitrarytext="False" calendarlocation="Bottom"
                            controldisplay="TextBoxImage" culture="(Default)" font-names="Tahoma" font-size="8pt"
                            imageurl="../images/calendar.gif" padsingledigits="True" showgototoday="True"
                            tooltip="Select Court Date Range" upperbounddate="12/31/9999 23:59:00" width="90px">
                                <TEXTBOXLABELSTYLE CssClass="clstextarea" />
                                <WEEKDAYSTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <MONTHHEADERSTYLE ForeColor="Black" BackColor="Yellow" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <OFFMONTHSTYLE ForeColor="Gray" BackColor="AntiqueWhite" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <GOTOTODAYSTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <TODAYDAYSTYLE ForeColor="Black" BackColor="LightGoldenrodYellow" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <DAYHEADERSTYLE ForeColor="Black" BackColor="Orange" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <WEEKENDSTYLE ForeColor="Black" BackColor="LightGray" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <SELECTEDDATESTYLE ForeColor="Black" BackColor="Yellow" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <CLEARDATESTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                                <HOLIDAYSTYLE ForeColor="Black" BackColor="White" Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" />
                            </ew:CalendarPopup>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsButton" OnClick="btnSearch_Click"
                                    Text="Search" /></td>
                    </tr>
                    </table>
                    </td>
                    
                    </tr>
				<TR>
					<TD vAlign="top" colSpan="2">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td >
									<asp:DataGrid id="dgPrintHistory" runat="server" AutoGenerateColumns="False" Width="100%" OnItemDataBound="dg_Viewbug_ItemDataBound">
										<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn SortExpression="bug_id" HeaderText="id" Visible="False">
											<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblPID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.id") %>'></asp:Label>
													<asp:Label id="lblBatchID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.batchid") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="shortdescription" HeaderText="Print Date">
											<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblPrintDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.printdate") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="TicketNumber" HeaderText="Report Type">
											<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbltype" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.PrintType") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="username" HeaderText="UserName">
											    <HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblusername" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
                                                    &nbsp;<asp:HyperLink ID="HPView" runat="server" NavigateUrl="#">View</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
									</asp:DataGrid>
								</td>
							</tr>
							<tr>
									<td background="../../images/separator_repeat.gif" colSpan="5" height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>

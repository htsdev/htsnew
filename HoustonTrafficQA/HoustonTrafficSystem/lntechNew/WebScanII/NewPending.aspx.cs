using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using FrameWorkEnation.Components;
using System.IO;

namespace HTP.WebScanII
{
    public partial class NewPending : System.Web.UI.Page
    {
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        PendingClass NewPendInsert = new PendingClass();
        clsSession cSession = new clsSession();
        string PicID = "";
        string path = "";
        string dpath = "";
        int pageno = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {

                    if (Request.QueryString["batchid"] != "" & Request.QueryString["batchid"] != null)
                    {
                        ViewState["BatchID"] = Request.QueryString["batchid"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();//get path from web config..
                        BindGrid();

                        //FillCourtLocation();
                        DisplayOCR();
                    }
                    txtCauseNo.Focus();
                }
            }
            btnUpdate.Attributes.Add("onclick", "return Validation();");
            img_delete.Attributes.Add("onclick", "return DeleteConfirm();");
        }

        protected void ddlImgsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Width = 0;
            int Height = 0;
            int FixWidth = 798;
            int FixHeight = 840;
            int ddlsize = 0;
            try
            {

                ddlsize = Convert.ToInt32(ddlImgsize.SelectedValue);
                ddlsize = ddlsize - 150;

                Width = FixWidth / 100 * ddlsize;
                Width = Width + FixWidth;

                Height = FixHeight / 100 * ddlsize;
                Height = Height + FixHeight;

                img_docs.Width = Width;
                img_docs.Height = Height;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void gv_pending_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();
                if (((HyperLink)e.Row.FindControl("verified")).Text == "0")
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "NewVerified.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("discrepancy")).Text == "0")
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = "NewDiscrepancy.aspx?batchid=" + batchid; }

                //if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{

                //    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "";
                //}

                ViewState["TotalCount"] = ((Label)e.Row.FindControl("lblCount")).Text.ToString();

            }
        }

        protected void ImgMoveFirst_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveFirst = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MoveFirst.Tables[0].Rows.Count > 0)
                {
                    ViewState["PicID"] = DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString();

                    lbl_ScanType.Text = DS_MoveFirst.Tables[0].Rows[0]["TypeScan"].ToString();
                    lbl_CrtLoc.Text = DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString();


                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = "1";
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveLast_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveLast = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MoveLast.Tables[0].Rows.Count > 0)
                {
                    int cnt = Convert.ToInt32(DS_MoveLast.Tables[0].Rows.Count);
                    ViewState["PicID"] = DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString();

                    lbl_ScanType.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["TypeScan"].ToString();
                    lbl_CrtLoc.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["Location"].ToString();


                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = cnt.ToString();
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMovePrev_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MovePrevious = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MovePrevious.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MovePrevious.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MovePrevious.Tables[0].Rows[i]["picid"].ToString();

                        if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                        {
                            ViewState["PicID"] = DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString();

                            lbl_ScanType.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["TypeScan"].ToString();
                            lbl_CrtLoc.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["Location"].ToString();

                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno - 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveNext_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();

                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveNext = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MoveNext.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MoveNext.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MoveNext.Tables[0].Rows[i]["picid"].ToString();
                        if (ViewState["PicID"] == null)
                        {
                            ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();

                            lbl_ScanType.Text = DS_MoveNext.Tables[0].Rows[i + 1]["TypeScan"].ToString();
                            lbl_CrtLoc.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString();

                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno + 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;

                        }
                        else
                        {
                            if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                            {

                                ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();
                                lbl_ScanType.Text = DS_MoveNext.Tables[0].Rows[i + 1]["TypeScan"].ToString();
                                lbl_CrtLoc.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString();

                                dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";

                                pageno = Convert.ToInt32(lblpageno.Text);
                                pageno = pageno + 1;
                                lblpageno.Text = pageno.ToString();
                                if (!File.Exists(dpath))
                                {
                                    img_docs.ImageUrl = "~/Images/not-available.gif";
                                    Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                                }
                                else
                                {
                                    img_docs.ImageUrl = dpath;
                                    Session["sBSDApath"] = dpath;
                                }
                                break;
                            }
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            NewInsert();
            txtCauseNo.Focus();
        }

        protected void img_delete_Click(object sender, ImageClickEventArgs e)
        {
            Delete();
        }

        protected void img_flip_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                string fPath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                string dpath = fPath;
                fPath = fPath.Replace("\\\\", "\\");
                System.Drawing.Image img;
                img = System.Drawing.Image.FromFile(fPath);

                img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipXY);

                if (File.Exists(dpath))
                {
                    File.Delete(dpath);
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                img_docs.ImageUrl = fPath;
                Session["sBSDApath"] = fPath;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        /// <summary>
        /// //ozair 4400 07/12/2008 implementing HCJP 5-1 status rule
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_HCJP51Yes_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                InsertUpdate();
                PopupPanel_HCJP51.Style["display"] = "none";                
                DisableDive.Style["display"] = "none";
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Pending = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPending", key, value);
                if (DS_Pending.Tables[0].Rows.Count > 0)
                {
                    gv_pending.DataSource = DS_Pending;
                    gv_pending.DataBind();
                    DisplayImage();
                }
                else
                {
                    lblMessage.Text = "No Record in Pending";
                    lblMessage.Visible = true;
                    gv_pending.DataSource = DS_Pending;
                    gv_pending.DataBind();
                    DisplayImage();
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        private void DisplayOCR()
        {
            try
            {

                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_DisplayOCR = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendOCR", key, value);
                if (DS_DisplayOCR.Tables[0].Rows.Count > 0)
                {
                    lbl_ScanType.Text = DS_DisplayOCR.Tables[0].Rows[0]["TypeScan"].ToString();
                    lbl_CrtLoc.Text = DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString();
                }

            }
            catch
            {

            }
        }

        private void DisplayImage()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Display = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_Display.Tables[0].Rows.Count > 0)
                {
                    PicID = DS_Display.Tables[0].Rows[0]["PicID"].ToString();
                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + PicID + ".jpg";
                    ViewState["PicID"] = PicID;
                    if (!File.Exists(path))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = path;
                        Session["sBSDApath"] = path;
                    }
                    lblpageno.Text = "1";
                    lblCount.Text = DS_Display.Tables[0].Rows.Count.ToString();
                    DisabledImageButtons();
                }
                else
                {
                    lblpageno.Text = "0";
                    lblCount.Text = "0";
                    DisabledImageButtons();
                    img_docs.Visible = false;
                    ddlImgsize.Enabled = false;
                    img_delete.Enabled = false;
                    img_flip.Enabled = false;
                }


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void DisabledImageButtons()
        {
            if (lblpageno.Text == lblCount.Text)
            {
                ImgMoveLast.Enabled = false;
                ImgMoveNext.Enabled = false;
            }
            else
            {
                ImgMoveLast.Enabled = true;
                ImgMoveNext.Enabled = true;
            }
            if (lblpageno.Text == "1")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else if (lblpageno.Text == "0")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else
            {
                ImgMoveFirst.Enabled = true;
                ImgMovePrev.Enabled = true;
            }
        }

        #region Commented
        //private void FillCourtLocation()
        //{
        //    try
        //    {
        //        DataSet ds_Court = clsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
        //        ddlCrtLoc.Items.Clear();


        //        ddlCrtLoc.DataSource = ds_Court.Tables[0];
        //        ddlCrtLoc.DataTextField = "shortname";
        //        ddlCrtLoc.DataValueField = "courtid";
        //        ddlCrtLoc.DataBind();

        //        ddlCrtLoc.Items[0].Text = "---Choose---";
        //        ddlCrtLoc.Items.RemoveAt(1);
        //        ddlCrtLoc.Items.RemoveAt(1);
        //        ddlCrtLoc.Items.RemoveAt(1);

        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Visible = true;
        //        lblMessage.Text = ex.Message;

        //        BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }
        //}
        #endregion

        private void NewInsert()
        {
            lblMessage.Visible = false;
            try
            {
                NewPendInsert.CauseNo = txtCauseNo.Text.ToString().Replace(" ", "").ToUpper();
                //ozair 4400 07/12/2008 implementing HCJP 5-1 status rule
                if (NewPendInsert.CheckForHCJP51() == 1)
                {
                    hf_isUpdateable.Value = "0";
                    string msg = String.Empty;
                    if (ddlstatus.SelectedItem.Text.ToString() == "PRETRIAL")
                    {
                        PopupPanel_HCJP51.Style["display"] = "block";
                        PopupPanel_HCJP51.Style["top"] = "280";
                        DisableDive.Style["height"] = hf_height.Value;
                        DisableDive.Style["width"] = hf_width.Value;
                        DisableDive.Style["display"] = "block";
                        tbl_Alert.Style["display"] = "none";
                        tbl_Confirm.Style["display"] = "block";
                    }
                    else if (ddlstatus.SelectedItem.Text.ToString() != "JURY TRIAL")
                    {
                        PopupPanel_HCJP51.Style["display"] = "block";
                        PopupPanel_HCJP51.Style["top"] = "280";
                        DisableDive.Style["height"] = hf_height.Value;
                        DisableDive.Style["width"] = hf_width.Value;
                        DisableDive.Style["display"] = "block";
                        tbl_Alert.Style["display"] = "block";
                        tbl_Confirm.Style["display"] = "none";
                        hf_isUpdateable.Value = "0";
                    }
                    else
                    { 
                        //ozair 4517 08/06/2008 
                        PopupPanel_HCJP51.Style["display"] = "none";                                                
                        DisableDive.Style["display"] = "none";
                        tbl_Alert.Style["display"] = "none";
                        tbl_Confirm.Style["display"] = "none";
                        //end ozair 4517
                        hf_isUpdateable.Value = "1";
                        InsertUpdate();
                    }
                }
                else
                {
                    hf_isUpdateable.Value = "1";
                    InsertUpdate();
                }
                //end ozair 4400
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void RefreshControls()
        {
            txtCauseNo.Text = "";
            txtdd.Text = "";
            //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
            ddlmm.ClearSelection();
            txtyy.Text = "";
            lbl_ScanType.Text = "";
            lbl_CrtLoc.Text = "";
            ddl_Time.ClearSelection();
            ddlstatus.ClearSelection();
        }

        private void RefreshDDLS()
        {
            try
            {
                ddl_Time.ClearSelection();
                ddlstatus.ClearSelection();
            }
            catch
            {
            }
        }

        private void Delete()
        {
            try
            {
                if (ViewState["TotalCount"] != null && ViewState["TotalCount"].ToString() != "" && ViewState["TotalCount"].ToString() == "1")
                {
                    string[] key1 = { "@BatchID" };
                    object[] value1 = { Convert.ToInt32(ViewState["BatchID"]) };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteBatch", key1, value1);
                    lblMessage.Text = "Record Deleted";
                    lblMessage.Visible = true;
                }
                else
                {



                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    string[] key = { "@PicID" };
                    object[] value = { Convert.ToInt32(ViewState["PicID"]) };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteScanRecord", key, value);
                    lblMessage.Text = "Record Deleted";
                    lblMessage.Visible = true;


                }
                BindGrid();

                RefreshControls();
                DisplayOCR();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        /// <summary>
        /// ozair 4400 07/12/2008 
        /// this method is used to insert/update data
        /// </summary>
        private void InsertUpdate()
        {
            //Farrukh 9451 07/04/2011 Replaced month textbox with dropdown
            string CourtDate = ddlmm.SelectedValue + "/" + txtdd.Text + "/" + txtyy.Text;
            NewPendInsert.PicID = Convert.ToInt32(ViewState["PicID"]);
            NewPendInsert.CauseNo = txtCauseNo.Text.ToString().Replace(" ", "").ToUpper();
            if (ddlstatus.SelectedValue == "104")
            {
                NewPendInsert.CourtDateMain = Convert.ToDateTime("01/01/1900");
                NewPendInsert.Time = "";
                NewPendInsert.CourtDateScan = Convert.ToDateTime("01/01/1900");
            }
            else
            {
                NewPendInsert.CourtDateMain = Convert.ToDateTime(CourtDate);
                NewPendInsert.Time = ddl_Time.SelectedValue.ToString();
                NewPendInsert.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddl_Time.SelectedValue);
            }

            NewPendInsert.CourtNo = 0;
            NewPendInsert.Status = ddlstatus.SelectedItem.Text.ToString();
            NewPendInsert.Location = lbl_CrtLoc.Text.ToString();

            if (hf_isUpdateable.Value == "1")
            {
                int i = NewPendInsert.ComparisonInsert();
                if (i == 3)
                {
                    lblMessage.Text = "Reocord Move to Queued Section";
                    lblMessage.Visible = true;

                }
                if (i == 2)
                {
                    lblMessage.Text = "Record doesnot match with OCR Record and it is Moved to Descripancy Section";
                    lblMessage.Visible = true;

                }
                BindGrid();

                RefreshControls();
                DisplayOCR();
            }
        }

        #endregion


    }
}

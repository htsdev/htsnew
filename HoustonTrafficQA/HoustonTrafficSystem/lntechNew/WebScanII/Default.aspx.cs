using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.WebScanII
{
    public partial class _Default : System.Web.UI.Page
    {
        #region Variables

        Batch NewBatch = new Batch();
        clsGeneralMethods CGeneral = new clsGeneralMethods();
        Picture NewPic = new Picture();
        Log NewLog = new Log();
        OCR NewOCR = new OCR();
        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        DataView DV;
        string ocr_data = String.Empty;
        string picDestination = String.Empty;
        string CourtStatus = String.Empty;
        string causeno = "";
        string courtdate = "";
        string time = "";
        string todaysdate = "";
        string location = "";
        string courtstatus = "";
        string Fname = "";
        string Lname = "";
        string Mname = "";
        string Address = "";
        string City = "";
        string State = "";
        string Zip = "";
        string TempString = "";
        string strServer;
        string searchpath = "";
        string picName = "";
        string picid;
        string courtno = "";

        int BatchID = 0;
        int PicID = 0;
        int Batchid = 0;
        StringBuilder sb = new StringBuilder();
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lblMessage.Visible = false;
                    //FillCourtLocation();
                    BindGrid();//bind Grid With Batch Records..

                    cal_scandate.Text = System.DateTime.Today.ToShortDateString();


                    txtSrv.Text = Request.ServerVariables["SERVER_NAME"].ToString();//for scanning function in javascript.
                    strServer = "http://" + Request.ServerVariables["SERVER_NAME"];//for scanning function in javascript.
                    Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.

                    txtsessionid.Text = Session.SessionID.ToString();//for scanning function in javascript.
                    txtempid.Text = cSession.GetCookie("sEmpID", this.Request).ToString();//for scanning function in javascript.
                    ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp2"].ToString();//path to and get save images
                    ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();//path to save and get images
                    btnScan.Attributes.Add("onclick", "return StartScan();");

                }
            }
        }
        
        private void BindGrid()
        {
            try
            {
                DataSet DS_GetBatch = NewBatch.GetCountsII();//Display Records of current date when page loaded.
                if (DS_GetBatch.Tables[0].Rows.Count > 0)
                {
                    gv_Scan.DataSource = DS_GetBatch;
                    DV = new DataView(DS_GetBatch.Tables[0]);
                    Session["DV"] = DV;
                    gv_Scan.DataBind();
                }
                else
                {
                    lblMessage.Text = "No Record Found";
                    
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        #region SortingLogic 
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        #endregion 

#region commented
        //private void FillCourtLocation()
        //{
        //    try
        //    {
        //        DataSet ds_Court = clsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
        //        ddlCourtLoc.Items.Clear();


        //        ddlCourtLoc.DataSource = ds_Court.Tables[0];
        //        ddlCourtLoc.DataTextField = "shortname";
        //        ddlCourtLoc.DataValueField = "courtid";
        //        ddlCourtLoc.DataBind();

        //        ddlCourtLoc.Items[0].Text = "---Choose---";
        //        ddlCourtLoc.Items.RemoveAt(1);
        //        ddlCourtLoc.Items.RemoveAt(1);
        //        ddlCourtLoc.Items.RemoveAt(1);

        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Visible = true;
        //        lblMessage.Text = ex.Message;

        //        BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }
        //}
#endregion

        private void OCR(int batchid)
        {

            

            try
            {

                DataSet DS=NewPic.GetPicByBatchID(batchid);//get PIcID's of scan pages from database..

                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    try
                    {
                        int TempFlag=0;
                        picid = DS.Tables[0].Rows[i]["PicID"].ToString();
                        string pName = batchid.ToString() + "-" + picid.ToString();
                        picDestination = ViewState["vNTPATHScanImage"].ToString() + pName + ".jpg";//get Image from folder for OCR..
                        picDestination = picDestination.Replace("\\\\", "\\");

                        //try
                        //{
                        //    ocr_data = CGeneral.OcrIt(picDestination);//OCR Process..
                        //}
                        //catch 
                        //{
                            
                            NewOCR.PicID = Convert.ToInt32(picid);
                            NewOCR.CheckStatus = 1;
                            NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                            NewOCR.Status = "--Select--";
                            NewOCR.OCRData = ocr_data;
                            NewOCR.TypeScan = ddl_ScanType.SelectedItem.Text.ToString();
                            NewOCR.InsertPendingOCRII();
                            TempFlag = 1;
                            continue;
                        //}
                        #region commnented for future purpose
                        //if (ocr_data.Length > 0)
                        //{

                        //    TempString = ocr_data.ToUpper();
                        //    TempString = TempString.Replace("\r\n", " ");
                        //    #region Cause No
                        //    try
                        //    {

                        //        if (TempString.Contains("CAUSE NUMBER") == true)
                        //        {

                        //            causeno = TempString.Substring(TempString.LastIndexOf("CAUSE NUMBER") + 13, 17);
                        //            causeno = causeno.Replace(" ", "");
                        //            causeno = causeno.Replace(".", "");
                        //            causeno.Trim();

                        //            if (courtno.IndexOf("O") > -1)
                        //            {
                        //                courtno = courtno.Replace("O", "0");
                        //            }
                        //            else if (courtno.IndexOf("o") > -1)
                        //            {
                        //                courtno = courtno.Replace("o", "0");
                        //            }
                        //            if (courtno.IndexOf("I") > -1)
                        //            {
                        //                courtno = courtno.Replace("I", "1");
                        //            }
                        //            else if (courtno.IndexOf("i") > -1)
                        //            {
                        //                courtno = courtno.Replace("i", "1");
                        //            }

                        //            if (courtno.IndexOf("L") > -1)
                        //            {
                        //                courtno = courtno.Replace("L", "1");
                        //            }
                        //            else if (courtno.IndexOf("l") > -1)
                        //            {
                        //                courtno = courtno.Replace("l", "1");
                        //            }

                        //            if ((causeno.Substring(0, 4) != "2007") & (causeno.Substring(0, 4).LastIndexOf("7") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2007");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2006") & (causeno.Substring(0, 4).LastIndexOf("6") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2006");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2005") & (causeno.Substring(0, 4).LastIndexOf("5") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2005");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2004") & (causeno.Substring(0, 4).LastIndexOf("4") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2004");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2003") & (causeno.Substring(0, 4).LastIndexOf("3") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2003");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2002") & (causeno.Substring(0, 4).LastIndexOf("2") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2002");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2001") & (causeno.Substring(0, 4).LastIndexOf("1") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2001");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "2000") & (causeno.Substring(0, 4).LastIndexOf("0") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "2000");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "1999") & (causeno.Substring(0, 4).LastIndexOf("9") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "1999");
                        //            }
                        //            if ((causeno.Substring(0, 4) != "1998") & (causeno.Substring(0, 4).IndexOf("8") == 3))
                        //            {
                        //                causeno = causeno.Remove(0, 4);
                        //                causeno = causeno.Insert(0, "1998");
                        //            }

                        //            if (causeno.Substring(4, 1) == "T" & causeno.Substring(4, 2) != "TR")
                        //            {
                        //                causeno = causeno.Remove(4, 2);
                        //                causeno = causeno.Insert(4, "TR");
                        //            }

                        //            else if (causeno.Substring(4, 1) == "N" & causeno.Substring(4, 2) != "NT")
                        //            {
                        //                causeno = causeno.Remove(4, 2);
                        //                causeno = causeno.Insert(4, "NT");
                        //            }

                        //            else if (causeno.Substring(4, 1) == "F" & causeno.Substring(4, 3) != "FTA")
                        //            {
                        //                causeno = causeno.Remove(4, 3);
                        //                causeno = causeno.Insert(4, "FTA");
                        //            }
                        //            else if (causeno.Substring(4, 3).IndexOf("A") > -1)
                        //            {
                        //                causeno = causeno.Remove(4, 3);
                        //                causeno = causeno.Insert(4, "FTA");
                        //            }
                        //        }
                        //        else
                        //        {
                        //            causeno = "";
                        //        }

                        //    }
                        //    catch
                        //    {
                        //        causeno = "";
                        //    }
                        //    #endregion
                        //    #region Court No
                        //    try
                        //    {
                        //        if (TempString.Contains("COURT NO") == true)
                        //        {

                        //            courtno = TempString.Substring(TempString.LastIndexOf("COURT NO") + 9, 4);
                                    
                        //            if (courtno.Contains("("))
                        //                courtno = courtno.Replace("(", "");
                        //            if (courtno.Contains(":"))
                        //                courtno = courtno.Replace(":", "");

                        //            if (courtno.IndexOf("O") > -1)
                        //            {
                        //                courtno = courtno.Replace("O", "0");
                        //            }
                        //            else if (courtno.IndexOf("o") > -1)
                        //            {
                        //                courtno = courtno.Replace("o", "0");
                        //            }

                        //            if (courtno.IndexOf("I") > -1)
                        //            {
                        //                courtno = courtno.Replace("I", "1");
                        //            }
                        //            else if (courtno.IndexOf("i") > -1)
                        //            {
                        //                courtno = courtno.Replace("i", "1");
                        //            }

                        //            if (courtno.IndexOf("L") > -1)
                        //            {
                        //                courtno = courtno.Replace("L", "1");
                        //            }
                        //            else if (courtno.IndexOf("l") > -1)
                        //            {
                        //                courtno = courtno.Replace("l", "1");
                        //            }
                        //            if (courtno.IndexOf("(") > -1)
                        //            {
                        //                courtno = courtno.Replace("(", "");
                        //            }

                        //            try
                        //            {
                        //                courtno = Convert.ToInt32(courtno.Trim()).ToString();
                        //            }
                        //            catch
                        //            {
                        //                courtno = "";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            courtno = "";
                        //        }
                        //    }
                        //    catch
                        //    {
                        //        courtno = "";
                        //    }
                        //    #endregion
                        //    #region Court Date
                        //    try
                        //    {
                        //        if (TempString.Contains("COURT DATE") == true)
                        //        {
                        //            courtdate = TempString.Substring(TempString.IndexOf("COURT DATE") + 11, 12);
                        //            courtdate = courtdate.Trim();
                        //            courtdate = courtdate.Replace(" ", "");


                        //            if (courtdate.IndexOf("I") > -1)
                        //            {
                        //                courtdate = TempString.Substring(TempString.IndexOf("COURT DATE") + 11, 13);
                        //                courtdate = courtdate.Replace(" ", "");
                        //                courtdate = courtdate.Replace("I", "1");
                        //            }
                        //            else if (courtdate.IndexOf("i") > -1)
                        //            {
                        //                courtdate = TempString.Substring(TempString.IndexOf("COURT DATE") + 11, 13);
                        //                courtdate = courtdate.Replace(" ", "");
                        //                courtdate = courtdate.Replace("i", "1");
                        //            }

                        //            if (courtdate.IndexOf("O") > -1)
                        //            {
                        //                courtdate = courtdate.Replace("O", "0");
                        //            }
                        //            else if (courtdate.IndexOf("o") > -1)
                        //            {
                        //                courtdate = courtdate.Replace("o", "0");
                        //            }

                        //            if (courtdate.IndexOf("U") > -1)
                        //            {
                        //                courtdate = courtdate.Replace("U", "0");
                        //            }
                        //            else if (courtdate.IndexOf("u") > -1)
                        //            {
                        //                courtdate = courtdate.Replace("u", "0");
                        //            }


                        //            if (courtdate.Substring(2, 1) != "/")
                        //            {
                        //                courtdate = courtdate.Remove(2, 1);
                        //                courtdate = courtdate.Insert(2, "/");
                        //            }
                        //            if (courtdate.Substring(5, 1) != "/")
                        //            {
                        //                courtdate = courtdate.Remove(5, 1);
                        //                courtdate = courtdate.Insert(5, "/");
                        //            }
                        //            courtdate = courtdate.Trim();
                        //            if (courtdate.Length > 10)
                        //            {
                        //                courtdate = courtdate.Substring(0, 10);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            courtdate = "";
                        //        }
                        //    }
                        //    catch
                        //    {
                        //        courtdate = "";
                        //    }
                        //    #endregion
                        //    #region Time
                        //    try
                        //    {
                        //        if (TempString.Contains("TIME") == true)
                        //        {
                        //            time = TempString.Substring(TempString.IndexOf("TIME") + 5, 9);
                        //            if (time.Contains("AND") == true)
                        //            {
                        //                time = TempString.Substring(TempString.LastIndexOf("TIME") + 5, 9);
                        //            }
                        //            if (time.Contains("AND") == true)
                        //            {
                        //                time = "";
                        //            }
                        //            else if (ddlCourtLoc.SelectedValue == "HMC" & ddl_ScanType.SelectedItem.Text.ToString().ToUpper() == "JURY TRIAL")
                        //            {

                        //                if (time.IndexOf("8") > -1)
                        //                {
                        //                    time = "8:00 AM";
                        //                }
                        //                else if (time.IndexOf("9") > -1)
                        //                {
                        //                    time = "9:00 AM";
                        //                }
                        //                else if (time.IndexOf("1") > -1)
                        //                {
                        //                    time = "10:30 AM";
                        //                }
                        //                else if (time.IndexOf("3") > -1)
                        //                {
                        //                    time = "10:30 AM";
                        //                }
                        //                else
                        //                {
                        //                    time = "";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                if (time.IndexOf("7") > -1)
                        //                {
                        //                    if (time.IndexOf("A") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "7:30 AM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "7:00 AM";
                        //                        }
                        //                    }
                        //                    else if (time.IndexOf("P") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "7:30 PM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "7:00 PM";
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "7:00 AM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("8") > -1)
                        //                {
                        //                    if (time.IndexOf("A") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "8:30 AM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "8:00 AM";
                        //                        }
                        //                    }
                        //                    else if (time.IndexOf("P") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "8:30 PM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "8:00 PM";
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "8:00 AM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("9") > -1)
                        //                {
                        //                    if (time.IndexOf("A") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "9:30 AM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "9:00 AM";
                        //                        }
                        //                    }
                        //                    else if (time.IndexOf("P") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "9:30 PM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "9:00 PM";
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "9:00 AM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("10") > -1)
                        //                {
                        //                    if (time.IndexOf("A") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "10:30 AM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "10:00 AM";
                        //                        }
                        //                    }
                        //                    else if (time.IndexOf("P") > -1)
                        //                    {
                        //                        time = "10:00 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "10:00 AM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("11") > -1)
                        //                {
                        //                    if (time.IndexOf("A") > -1)
                        //                    {
                        //                        if (time.IndexOf("3") > -1)
                        //                        {
                        //                            time = "11:30 AM";
                        //                        }
                        //                        else
                        //                        {
                        //                            time = "11:00 AM";
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "11:00 AM";
                        //                    }

                        //                }
                        //                else if (time.IndexOf("12") > -1)
                        //                {
                        //                    if (time.IndexOf("3") > -1)
                        //                    {
                        //                        time = "12:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "12:00 PM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("1") > -1)
                        //                {
                        //                    if (time.IndexOf("3") > -1)
                        //                    {
                        //                        time = "1:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "1:00 PM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("2") > -1)
                        //                {
                        //                    if (time.IndexOf("3") > -1)
                        //                    {
                        //                        time = "2:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "2:00 PM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("3") > -1)
                        //                {
                        //                    if (time.LastIndexOf("3") > time.IndexOf("3"))
                        //                    {
                        //                        time = "3:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "3:00 PM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("4") > -1)
                        //                {
                        //                    if (time.IndexOf("3") > -1)
                        //                    {
                        //                        time = "4:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "4:00 PM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("5") > -1)
                        //                {
                        //                    if (time.IndexOf("3") > -1)
                        //                    {
                        //                        time = "5:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "5:00 PM";
                        //                    }
                        //                }
                        //                else if (time.IndexOf("6") > -1)
                        //                {
                        //                    if (time.IndexOf("3") > -1)
                        //                    {
                        //                        time = "6:30 PM";
                        //                    }
                        //                    else
                        //                    {
                        //                        time = "6:00 PM";
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    time = "";
                        //                }

                        //            }
                        //        }
                        //        else
                        //        {
                        //            time = "";
                        //        }
                        //    }
                        //    catch
                        //    {
                        //        time = "";
                        //    }
                        //    #endregion
                            

                        //    if (ChechForUnsucessOCR(causeno, courtdate, time, courtno) == false)//If all variables are empty then it is bad scan insert it in bad scan.
                        //    {
                                
                        //        //Insertion in tbl_webscan_ocr 
                        //        NewOCR.CauseNo = causeno.Trim();
                        //        NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                        //        NewOCR.NewCourtDate = courtdate.Trim();
                        //        NewOCR.PicID = Convert.ToInt32(picid);
                        //        NewOCR.Time = time.Trim();
                        //        NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                        //        NewOCR.CourtNo = courtno.Trim();
                        //        NewOCR.CheckStatus = 1;
                        //        NewOCR.OCRData = ocr_data;
                        //        NewOCR.InsertInOCR();

                        //    }
                        //    else
                        //    {
                        //        NewOCR.PicID = Convert.ToInt32(picid);
                        //        NewOCR.CheckStatus = 1;
                        //        NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                        //        NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                        //        NewOCR.OCRData = ocr_data;
                        //        NewOCR.InsertPendingOCR();
                        //    }



                        //}
                        //else
                        //{
                        //    if (TempFlag == 1)//If OCR not done sucessfully then insert it into tbl_webscan_log as a bad scan 
                        //    {
                        //        NewOCR.PicID = Convert.ToInt32(picid);
                        //        NewOCR.CheckStatus = 1;
                        //        NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                        //        NewOCR.Status = ddl_ScanType.SelectedItem.Text.ToString();
                        //        NewOCR.OCRData = ocr_data;
                        //        NewOCR.InsertPendingOCR();
                        //    }

                        //}
                        #endregion
                    }
                    catch 
                    {//Insertion in tbl_webscan_ocr 
                        NewOCR.CauseNo = causeno.Trim();
                        NewOCR.Location = ddlCourtLoc.SelectedItem.Text.ToString();
                        NewOCR.NewCourtDate = courtdate.Trim();
                        NewOCR.PicID = Convert.ToInt32(picid);
                        NewOCR.Time = time.Trim();
                        NewOCR.Status = "--Select--";
                        NewOCR.CourtNo = courtno.Trim();
                        NewOCR.CheckStatus = 1;
                        NewOCR.OCRData = ocr_data;
                        NewOCR.TypeScan = ddl_ScanType.SelectedItem.Text.ToString();
                        NewOCR.InsertInOCRII();

                        
                    }
                    
                    }
                    BindGrid();//update grid..
                }
               
             
                
                
            
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        private bool ChechForUnsucessOCR(string CauseNo,string CourtDate,string Time,string CourtNo)
        {

            if (CauseNo == "")
            {
                if (CourtDate == "")
                {
                    if (Time == "")
                    {
                        if (CourtNo == "")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private string[] sortfilesbydate(string[] filename)
        {
            for (int i = 0; i < filename.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(filename[i]);
                for (int j = i + 1; j < filename.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(filename[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = filename[j];
                        filename[j] = filename[i];
                        filename[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return filename;
        }

        protected void btnScan_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;

            try
            {
                searchpath = "*" + Session.SessionID + txtempid.Text + "*.jpg";
                string[] filename = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpath);//get scann images from WebScanDocs..
                if (filename.Length > 1)
                {
                    filename = sortfilesbydate(filename);//sort files by date..
                }
                if (filename.Length >= 1)
                {
                    NewBatch.ScanDate = Convert.ToDateTime(cal_scandate.Text);
                    NewBatch.ScanType = ddl_ScanType.SelectedItem.Text.ToString();
                    NewBatch.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)); 
                    Batchid = NewBatch.InsertInBatch();//Insertion in tbl_webscan_batch..

                    for (int i = 0; i < filename.Length; i++)
                    {
                        NewPic.BatchID = Batchid;
                        PicID = NewPic.InsertInPicture();
                        picName = Batchid.ToString() + "-" + PicID.ToString();
                        //picDestination = Server.MapPath("WebScanDocs/Temp/") + picName + ".jpg";
                        picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg";
                        File.Copy(filename[i].ToString(), picDestination);//save images with batchid and picid like 1-1.jpg.
                        File.Delete(filename[i].ToString());//delete from intial position.

                    }

                }

                //Batchid = 62;
                OCR(Batchid);//OCR process..

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void lnk_View_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewScanDocs.aspx");
        }

        
        protected void gv_Scan_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);//sorting
        }

        protected void gv_Scan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Scan.PageIndex = e.NewPageIndex;//paging
                DV = (DataView)Session["DV"];
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;

            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();//session clear..
                Response.Redirect("Login.aspx");
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void GoodCount(object sender, CommandEventArgs e)
        {
            string batchid = e.CommandArgument.ToString();
            Response.Redirect("ViewGoodScan.aspx?batchid=" + batchid);
        }

        protected void BadCount(object sender, CommandEventArgs e)
        {
            string batchid = e.CommandArgument.ToString();
            Response.Redirect("ViewBadScan.aspx?batchid=" + batchid);
        }

        protected void gv_Scan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();
                if (((HyperLink)e.Row.FindControl("verified")).Text == "0")
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "NewVerified.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("discrepancy")).Text == "0")
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = "NewDiscrepancy.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("pending")).Text == "0")
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = "NewPending.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                //if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{

                //    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "";
                //}
            } 
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
        }

        
    }
}

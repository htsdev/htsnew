using System;
using System.Web.UI;
using lntechNew.Components.ClientInfo;

namespace HTP.WebComponents
{
    public class BasePage : Page
    {
        #region Variables

        //Kazim 3768 4/18/2008 Create Empid property 
        private int _empId;
        //Ozair 7379 03/21/2010 Declareing accesstype variable
        private int _accessType;

        //Session variable declared to get information about the current session of the application
        readonly clsSession _cSession = new clsSession();

        #endregion

        #region Property

        /// <summary>
        /// Employee Id Property of type Get only
        /// </summary>
        public int EmpId
        {
            get
            {
                if (_empId == 0)
                {
                    //Waqas 5057 03/17/2009 Checking employee info in session
                    if ((_cSession.IsValidSession(Request, Response, Session) == false) ||
                        (!int.TryParse(_cSession.GetCookie("sEmpID", Request), out _empId)))
                    {
                        Response.Redirect("~/frmlogin.aspx");
                    }
                }
                return _empId;
            }
        }

        //Ozair 7379 03/21/2010 AccessType property Added
        /// <summary>
        /// Access Type Property of type Get only.
        /// </summary>
        public int AccessType
        {
            get
            {
                if (_accessType == 0)
                {
                    // Checking access type info in session
                    if ((_cSession.IsValidSession(Request, Response, Session) == false) ||
                        (!int.TryParse(_cSession.GetCookie("sAccessType", Request), out _accessType)))
                    {
                        Response.Redirect("~/frmlogin.aspx");
                    }
                }
                return _accessType;
            }
        }

        #endregion

        #region Events

        protected override void OnLoad(EventArgs e)
        {

            //Waqas 5057 03/17/2009 Checking employee info in session
            if (_cSession.IsValidSession(Request, Response, Session) == false)
            {
                Response.Redirect("~/frmlogin.aspx", false);
                goto EndOfMethod;
            }
            base.OnLoad(e);

        EndOfMethod: { }
        }

        #endregion
    }
}

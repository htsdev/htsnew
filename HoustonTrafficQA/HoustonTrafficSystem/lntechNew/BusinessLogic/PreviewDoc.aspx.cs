using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;


namespace HTP.BusinessLogic
{
    public partial class PreviewDoc : System.Web.UI.Page
    {
        
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string docName = Request.QueryString["docname"].ToString();
                    string helpDocPath = ConfigurationManager.AppSettings["NTPATHHelpDoc"].ToString();
                    string docStoragePath = ConfigurationManager.AppSettings["NTPATH"].ToString();
                    //string fullFilePath = "http://"+Request.ServerVariables["SERVER_NAME"]+"/docstorage/"+ helpDocPath.Remove(0,docStoragePath.Length)+"/"+docName;
                    string fullFilePath = "../docstorage/" + helpDocPath.Remove(0, docStoragePath.Length).Replace("\\","") + "/" + docName;
                    Response.Redirect(fullFilePath);
                    
                }
                catch (Exception ex)
                {
                    clsLogger.ErrorLog(ex);
                    
                }
            }
        }

        

 
        #endregion
    }
}

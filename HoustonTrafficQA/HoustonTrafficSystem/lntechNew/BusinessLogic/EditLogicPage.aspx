<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditLogicPage.aspx.cs"
    Inherits="HTP.BusinessLogic.EditLogicPage" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Logic</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 562px;
        }
        .style3
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 55px;
        }
        .style5
        {
            width: 562px;
        }
    </style>
    <script language="javascript" type="text/javascript">
    
//    function CheckEmpty()
//    {
//    debugger;
//         if (document.getElementById("ftbBusinessLogic").value.length==0)
//         {
//            alert("Please insert the Text before Click the Submit button!");
//            return false;
//        }
//        else
//            return false;
//    }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
    <table style="width: 750px">
        <tr style="font-family: Tahoma">
            <td background="../Images/subhead_bg.gif" height="34">
                <table style="width: 750px">
                    <tr>
                        <td align="left" style="width: 60%">
                            <span class="clssubhead">Business Logic Editor</span>
                        </td>
                        <td align="right" style="width: 40%">
                            &nbsp;<asp:HyperLink ID="hlk_BackLogicPage" runat="server" 
                                Font-Underline="True">BACK</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="red"> </asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <FTB:FreeTextBox ID="ftbBusinessLogic" runat="server" Width="750px" 
                    ToolbarLayout="ParagraphMenu,FontFacesMenu,FontSizesMenu,FontForeColorsMenu|Bold,Italic,Underline,Strikethrough;Superscript,Subscript,RemoveFormat|JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;InsertRule|Cut,Copy,Paste;Undo,Redo,Print">
                </FTB:FreeTextBox>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="btnSubmit" runat="server" align="right" CssClass="clsbutton" OnClick="btnupSubmit_Click"
                    Text="Submit" />
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="11">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

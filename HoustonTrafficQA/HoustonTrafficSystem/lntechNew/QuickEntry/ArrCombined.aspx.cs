using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;

namespace lntechNew.QuickEntry
{
    public partial class ArrCombined : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        private DateTime CSDMon;
        private DateTime CSDTue;
        private DateTime CSDWed;
        private DateTime CSDThr;
        private DateTime CSDFri;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        //network path
                        ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                        ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH1"].ToString();
                        //
                        lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());

                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        

        private void GenerateSerial()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_Report.Items)
                {
                    Label sno = (Label)ItemX.FindControl("lbl_sno");
                    Label tid = (Label)ItemX.FindControl("lbl_Ticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                        Label arrdate = (Label)ItemX.FindControl("lbl_arrdate");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                        }

                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenerateSerialJT()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_ReportJT.Items)
                {
                    Label sno1 = (Label)ItemX.FindControl("lbl_sno1");
                    Label tid1 = (Label)ItemX.FindControl("lbl_Ticketid1");
                    if (tid1.Text != "")
                    {
                        if (no == 1)
                        {
                            sno1.Text = no.ToString();
                            ticketid = tid1.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid1.Text)
                            {
                                sno1.Text = Convert.ToString(no);
                                ticketid = tid1.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno1.Text = "";
                            }
                        }
                        Label date = (Label)ItemX.FindControl("lbl_CCDate");
                        Label num = (Label)ItemX.FindControl("lbl_CCNum");
                        if (date.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(date.Text.ToString());

                            date.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year, " @ ", adate.ToShortTimeString(), " # ", num.Text);
                        }
                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void GenerateSerialOT()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_ReportOT.Items)
                {
                    Label sno2 = (Label)ItemX.FindControl("lbl_sno2");
                    Label tid2 = (Label)ItemX.FindControl("lbl_Ticketid2");
                    if (tid2.Text != "")
                    {
                        if (no == 1)
                        {
                            sno2.Text = no.ToString();
                            ticketid = tid2.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid2.Text)
                            {
                                sno2.Text = Convert.ToString(no);
                                ticketid = tid2.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno2.Text = "";
                            }
                        }
                        Label date = (Label)ItemX.FindControl("lbl_CCDate");
                        Label num = (Label)ItemX.FindControl("lbl_CCNum");
                        if (date.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(date.Text.ToString());

                            date.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year, " @ ", adate.ToShortTimeString(), " # ", num.Text);
                        }
                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

       

        //snap short
        private void FindStartDate()
        {
            DateTime strDate = DateTime.Now.AddDays(21);
            int intDay = 0;
            for (intDay = 0; intDay <= 6; intDay++)
            {
                strDate = DateTime.Now.AddDays(intDay); //DateAdd("d",intDay,date())		
                if (strDate.DayOfWeek != DayOfWeek.Saturday && strDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (strDate.DayOfWeek == DayOfWeek.Monday)
                    {
                        CSDMon = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Tuesday)
                    {
                        CSDTue = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        CSDWed = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Thursday)
                    {
                        CSDThr = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Friday)
                    {
                        CSDFri = strDate;
                    }
                }
            }
        }

        //Monday
        private void GenerateSerial0()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Mon.Items)
                {
                    Label tid = (Label)ItemX.FindControl("lblTicketidM");
                    Label no = (Label)ItemX.FindControl("lblNoM");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrM");
                        Label tdt = (Label)ItemX.FindControl("lblTrialM");
                        Label bf = (Label)ItemX.FindControl("lblBondM");
                        Label tc = (Label)ItemX.FindControl("lblTrialCM");
                        Label pt = (Label)ItemX.FindControl("lblPreM");
                        Label pl = (Label)ItemX.FindControl("lblPlusM");

                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_MonC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommM");
                    Label no = (Label)ItemX.FindControl("lblNo1M");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Tuesday
        private void GenerateSerial1()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Tue.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoT");
                    Label tid = (Label)ItemX.FindControl("lblTicketidT");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrT");
                        Label tdt = (Label)ItemX.FindControl("lblTrialT");
                        Label bf = (Label)ItemX.FindControl("lblBondT");
                        Label tc = (Label)ItemX.FindControl("lblTrialCT");
                        Label pt = (Label)ItemX.FindControl("lblPreT");
                        Label pl = (Label)ItemX.FindControl("lblPlusT");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_TueC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommT");
                    Label no = (Label)ItemX.FindControl("lblNo1T");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Wednesday
        private void GenerateSerial2()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Wed.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoW");
                    Label tid = (Label)ItemX.FindControl("lblTicketidW");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrW");
                        Label tdt = (Label)ItemX.FindControl("lblTrialW");
                        Label bf = (Label)ItemX.FindControl("lblBondW");
                        Label tc = (Label)ItemX.FindControl("lblTrialCW");
                        Label pt = (Label)ItemX.FindControl("lblPreW");
                        Label pl = (Label)ItemX.FindControl("lblPlusW");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_WedC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommW");
                    Label no = (Label)ItemX.FindControl("lblNo1W");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Thursday
        private void GenerateSerial3()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Thu.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoTh");
                    Label tid = (Label)ItemX.FindControl("lblTicketidTh");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrTh");
                        Label tdt = (Label)ItemX.FindControl("lblTrialTh");
                        Label bf = (Label)ItemX.FindControl("lblBondTh");
                        Label tc = (Label)ItemX.FindControl("lblTrialCTh");
                        Label pt = (Label)ItemX.FindControl("lblPreTh");
                        Label pl = (Label)ItemX.FindControl("lblPlusTh");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_ThuC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommTh");
                    Label no = (Label)ItemX.FindControl("lblNo1Th");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Friday
        private void GenerateSerial4()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Fri.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoF");
                    Label tid = (Label)ItemX.FindControl("lblTicketidF");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrF");
                        Label tdt = (Label)ItemX.FindControl("lblTrialF");
                        Label bf = (Label)ItemX.FindControl("lblBondF");
                        Label tc = (Label)ItemX.FindControl("lblTrialCF");
                        Label pt = (Label)ItemX.FindControl("lblPreF");
                        Label pl = (Label)ItemX.FindControl("lblPlusF");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_FriC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommF");
                    Label no = (Label)ItemX.FindControl("lblNo1F");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_WeekDay_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());
                clsENationWebComponents clsDB = new clsENationWebComponents();

                if (ddl_WeekDay.SelectedValue == "50")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "block";
                    imgPrint_Ds_to_Printer.Visible = false;
                    img_pdfLong.Visible = false;
                    img_PdfAll.Visible = false;
                    DataSet ds = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapShot_VER_2");
                    #region Moday
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dg_Mon.DataSource = ds.Tables[0];
                        dg_Mon.DataBind();
                        dg_MonC.DataSource = ds.Tables[0];
                        dg_MonC.DataBind();
                        GenerateSerial0();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    #endregion
                    #region Tuesday
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        dg_Tue.DataSource = ds.Tables[1];
                        dg_Tue.DataBind();
                        dg_TueC.DataSource = ds.Tables[1];
                        dg_TueC.DataBind();
                        GenerateSerial1();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    #endregion
                    #region Wednesday
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        dg_Wed.DataSource = ds.Tables[2];
                        dg_Wed.DataBind();
                        dg_WedC.DataSource = ds.Tables[2];
                        dg_WedC.DataBind();
                        GenerateSerial2();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    #endregion
                    #region Thursday
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        dg_Thu.DataSource = ds.Tables[3];
                        dg_Thu.DataBind();
                        dg_ThuC.DataSource = ds.Tables[3];
                        dg_ThuC.DataBind();
                        GenerateSerial3();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    #endregion
                    #region Friday
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        dg_Fri.DataSource = ds.Tables[4];
                        dg_Fri.DataBind();
                        dg_FriC.DataSource = ds.Tables[4];
                        dg_FriC.DataBind();
                        GenerateSerial4();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    #endregion
                }
                else if (ddl_WeekDay.SelectedValue == "100")
                {
                    longshot.Style["display"] = "block";
                    snapshot.Style["display"] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    DataSet ds_JT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1");
                    
                    if (ds_JT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportJT.DataSource = ds_JT;
                        dg_ReportJT.DataBind();
                        GenerateSerialJT();
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    else
                    {
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                    }
                    
                }
                else if (ddl_WeekDay.SelectedValue == "99")
                {
                    longshot.Style["display"] = "block";
                    snapshot.Style["display"] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    DataSet ds_JT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1");
                    DataSet ds_OT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterOutsideArraignment_ver_1");

                    if (ds_JT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportJT.DataSource = ds_JT;
                        dg_ReportJT.DataBind();
                        GenerateSerialJT();
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    else
                    {
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                    }

                    if (ds_OT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportOT.DataSource = ds_OT;
                        dg_ReportOT.DataBind();
                        GenerateSerialOT();
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    else
                    {
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                    }
                }

                if (ddl_WeekDay.SelectedValue != "-1" & ddl_WeekDay.SelectedValue != "50" & ddl_WeekDay.SelectedValue != "100")
                {
                    longshot.Style["display"] = "block";
                    snapshot.Style["display"] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    string[] key1 = { "@ReportWeekDay" };
                    object[] value2 = { Convert.ToInt32(ddl_WeekDay.SelectedValue) };
                    DataSet ds = clsDB.Get_DS_BySPArr("usp_HTS_ArraignmentSnapshotLong_ver_1", key1, value2);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dg_Report.DataSource = ds;
                        dg_Report.DataBind();
                        GenerateSerial();
                        dg_Report.Visible = true;
                        lbl_Message.Visible = false;
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                    }
                    else
                    {
                        dg_Report.Visible = false;
                        lbl_Message.Visible = true;
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                    }
                }
                if (ddl_WeekDay.SelectedValue == "-1")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfLong.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgPrint_Ds_to_Printer_Click1(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.SelectedValue);
        }

        protected void Imagebutton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ArraignmentSnapShot.aspx");
        }

        protected void img_pdf_Click(object sender, ImageClickEventArgs e)
        {
            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShot.aspx?dd=" + DateTime.Now;
            //website end
            GeneratePDF(path);
            RenderPDF();
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.SelectedValue+"&dd="+ DateTime.Now;            
            //website end
            
            GeneratePDF(path);
            RenderPDF();
        }
        private void GeneratePDF(string path)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();
            //theDoc.HtmlOptions.BrowserWidth = 800;
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                //theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }
            string name = Session.SessionID + ".pdf";
            //for website
            ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
            //website end
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();
        }
        private void RenderPDF()
        {
            //for website
            string vpth = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH1"].ToString().Length) - 1);
            string link = "../DOCSTORAGE" + vpth;
            link = link + Session.SessionID + ".pdf";
            //website end
            Response.Redirect(link, false);
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
            for (int i = 0; i < files.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);
                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files[i]);
                }
            }

        }

        protected void img_PdfAll_Click(object sender, ImageClickEventArgs e)
        {
            
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShot.aspx?dd="+DateTime.Now;
            
            GeneratePDF(path,0);
            for (int i = 2; i < ddl_WeekDay.Items.Count; i++)
            {
                path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.Items[i].Value + "&dd="+DateTime.Now; 
                GeneratePDF(path, i);
            }
            Doc theDoc1 = (Doc)Session["vdoc1"];
            theDoc1.Save(ViewState["vFullPath"].ToString());
            theDoc1.Clear();
            Session["vdoc1"] = null;
            RenderPDF();
        }

        private void GeneratePDF(string path, int seq)
        {
            if (seq == 0)
            {
                Doc theDoc1 = new Doc();
                theDoc1.Rect.Inset(35, 45);
                theDoc1.Page = theDoc1.AddPage();
                //theDoc1.HtmlOptions.BrowserWidth = 800;
                int theID;
                theID = theDoc1.AddImageUrl(path);
                while (true)
                {
                    //theDoc1.FrameRect(); // add a black border
                    if (!theDoc1.Chainable(theID))
                        break;
                    theDoc1.Page = theDoc1.AddPage();
                    theID = theDoc1.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc1.PageCount; i++)
                {
                    theDoc1.PageNumber = i;
                    theDoc1.Flatten();
                }
                string name = Session.SessionID + ".pdf";
                //for website
                ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                //website end
                //theDoc1.Save(ViewState["vFullPath"].ToString());
                Session["vdoc1"] = theDoc1;
                //theDoc1.Clear();
            }
            if (seq > 0)
            {
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(35, 45);
                theDoc.Page = theDoc.AddPage();
                //theDoc.HtmlOptions.BrowserWidth = 800;
                int theID;
                theID = theDoc.AddImageUrl(path);
                while (true)
                {
                    //theDoc.FrameRect(); // add a black border
                    if (!theDoc.Chainable(theID))
                        break;
                    theDoc.Page = theDoc.AddPage();
                    theID = theDoc.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc.PageCount; i++)
                {
                    theDoc.PageNumber = i;
                    theDoc.Flatten();
                }
                Doc theDoc1 = (Doc)Session["vdoc1"];
                theDoc1.Append(theDoc);
                Session["vdoc1"] = theDoc1;
                theDoc.Clear();
            }


            //theDoc1.Clear();
        }
        //
    }
}

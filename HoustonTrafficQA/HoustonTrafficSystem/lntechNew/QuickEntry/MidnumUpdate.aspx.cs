using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.QuickEntry
{
    public partial class MidnumUpdate : System.Web.UI.Page
    {
        clsLogger bugTracker = new clsLogger();
        private clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";

        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            try
            {
                string[] keys = { "@CauseNumber", "@Midnum", "@EmpID" };
                object[] values = { txt_CauseNum.Text, txt_MidNum.Text, ClsSession.GetCookie("sEmpID", this.Request).ToString() };
                clsDb.ExecuteSP("USP_HTS_MIDNUMBER_UPDATE", keys, values);
                lbl_Message.Text = "Record updated!";
                txt_MidNum.Text = "";
                txt_CauseNum.Text = "";
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

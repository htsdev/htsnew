using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for temp.
	/// </summary>
	public partial class temp : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.DropDownList ddl_weeks;
		protected System.Web.UI.WebControls.Button btn_refresh;
		protected System.Web.UI.WebControls.Button btn_update;
		protected System.Web.UI.WebControls.DataList dl_result;
		protected System.Web.UI.WebControls.Label lbl_error;
		clsENationWebComponents ClsDB = new clsENationWebComponents();
		clsLogger bugTracker = new clsLogger();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here



			try
			{
			

	
				FillGrid("5/8/2006","7/21/2006");

	
                					
				if ( ! IsPostBack)

				{
					//loadList();
					//GetRecords( Convert.ToInt32(ddl_weeks.SelectedValue));


				}
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lbl_error.Text = ex.ToString();
			}

			


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void loadList()
		{

			
			//  Adding No of Weeks In The Drop Down List
			for( int i=10 ; i <=40 ; i++)
			{

				ListItem ls = new ListItem(i.ToString()+" Weeks" , i.ToString());
				ddl_weeks.Items.Add(ls);
			}

		}
		public double returnDay(string weekDay)
		{

			switch (weekDay)
			{
				
				case "Sunday" : return 1; 
				case "Monday" : return 2; 
				case "Tuesday" : return 3; 
				case "Wednesday" : return 4;
				case "Thursday" : return 5; 
				case "Friday" : return 6;
				case "Saturday" : return 7; 
				default : return 0;
			}
			
		}
		
		public void FillGrid(string sdt , string edt)
        {

			string[] keys = {"@Start_DateTime","@End_DateTime"};
			object[] values = {sdt,edt};
	
			DataSet ds = ClsDB.Get_DS_BySPArr("usp_hts_get_Court_History",keys,values);
                						
			// Bind Records To The DataList Control
			dl_result.DataSource = ds;
			dl_result.DataBind();
		
		
		}
		

		public void GetRecords(int NoOfWeeks)
		{
	
			try
			{	


				// Finding Start Date
				
				DateTime StartDate = DateTime.Now;
				StartDate = StartDate.AddDays(21);
				
				// start from the following monday if today is weekend
				if ( returnDay(StartDate.DayOfWeek.ToString()) ==1)
				{
					//sunday, add one more
					StartDate = StartDate.AddDays(1);

				}
				else if ( returnDay(StartDate.DayOfWeek.ToString())==7)
				{
					//add two if saturday
					StartDate = StartDate.AddDays(2);
				
				}
				else
				{
					//if none, then select monday even if it is under 21 days
					StartDate = StartDate.AddDays( 2 - returnDay(StartDate.DayOfWeek.ToString()));
				}
	
				
				// Getting End Date 
				
				DateTime EndDate = StartDate.AddDays(4+ NoOfWeeks* 7);

								
				
				
			
		FillGrid(StartDate.ToShortDateString(),EndDate.ToShortDateString());
	
		


			}
			catch( Exception ex)
			{
bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lbl_error.Text = ex.ToString();

			}
	




		}

		
		private void btn_refresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				GetRecords( Convert.ToInt32(ddl_weeks.SelectedValue));

			}
			catch ( Exception ex)
			{
bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lbl_error.Text = "Cannot Refresh List" ;
			}
		}

		private void btn_update_Click(object sender, System.EventArgs e)
		{
			try
			{
				
				// Update Each Case Status
				foreach ( DataListItem itm in dl_result.Items)
				{
		
					
					string totalpeople = ((TextBox)(itm.FindControl("tb_pplassign"))).Text ;
					string courtdate =((Label)(itm.FindControl("lblcrtdate"))).Text ;
					string courtNumber =((TextBox)(itm.FindControl("tb_crtnum"))).Text ;
					// Update Court Status
					UpdateRecord(courtdate,courtNumber,totalpeople);
		

				}
			
			
			}
			catch(Exception ex)
			{
			bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lbl_error.Text = "Cannot Update Case Information" +ex.ToString();
			
			}

		}

		public void UpdateRecord(string courtDate , string courtNumber,string totalpeople)
		{
			try
			{
				// Update Court Status Information
				string[] keys={"@CourtDateTime","@CourtNumber","@TotalPeople"};
				string[] values = {courtDate,courtNumber,totalpeople};
				ClsDB.ExecuteSP("sp_UpdateCourtSchedule",keys,values);
		
			}
			catch( Exception ex)
			{
		bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}


		}
	}
}

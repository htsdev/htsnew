using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data.SqlClient;
using lntechNew.Components.ClientInfo;

namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for Officer.
	/// </summary>
	public partial class Officer : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lbl_error;
		protected System.Web.UI.WebControls.DataGrid dg_srchofficer;
		protected System.Web.UI.WebControls.LinkButton lnkbtn_srchofficer;
		protected System.Web.UI.WebControls.LinkButton lnkbtn_addofficer;
		protected System.Web.UI.WebControls.LinkButton lnkbtn_assign;
		protected System.Web.UI.WebControls.Panel pnl_srch;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Panel pnl_add;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Panel pnl_assign;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.Button btn_searchofficer;
		protected System.Web.UI.WebControls.TextBox tb_srchofficerno;
		protected System.Web.UI.WebControls.TextBox tb_srchofficerfname;
		protected System.Web.UI.WebControls.TextBox tb_srchofficerlname;
		protected System.Web.UI.WebControls.TextBox tb_addofficerfname;
		protected System.Web.UI.WebControls.TextBox tb_addofficerlname;
		protected System.Web.UI.WebControls.DropDownList ddl_day;
		protected System.Web.UI.WebControls.DropDownList ddltime;
		protected System.Web.UI.WebControls.TextBox tb_addofficercomments;
		protected System.Web.UI.WebControls.Button btn_addofficer;
		protected System.Web.UI.WebControls.Button btn_assignofficer;
		protected System.Web.UI.WebControls.TextBox tb_assigntktno;
		protected System.Web.UI.WebControls.TextBox tb_assignofficerno;
		protected System.Web.UI.WebControls.Button btn_addreset;
		protected System.Web.UI.HtmlControls.HtmlTableRow offno;
		protected System.Web.UI.WebControls.Label lbl_addofficerno;
		protected System.Web.UI.WebControls.Label lbl_assigntkt;
		clsENationWebComponents ClsDB = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
clsLogger bugTracker = new clsLogger();

		private void Page_Load(object sender, System.EventArgs e)
		{			
		
			try
			{


				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx");
				}
				
				else //To stop page further execution
				{
					string s = Request.QueryString["CaseNumber"];
                    Button4.Attributes.Add("onclick", "javascript: return ClearControls('assaign');");
                    btn_addreset.Attributes.Add("onclick", "javascript: return ClearControls('add');");
					if ( ! IsPostBack)
					{


						if ( s!=null)
						{

							// Check If User Come From Violation Page Than Display Assign Officer Panel
							selectPanel(false , false , true);
							lbl_assigntkt.Text=s;
							tb_assigntktno.Visible = false;
						}
						else
						{
							// Else Display Search Officer Panel
							selectPanel(true,false,false); 
							tb_assigntktno.Visible=true;
							lbl_assigntkt.Visible= false;
						}	

						
						btn_addofficer.Attributes.Add("onClick","return  validateAddPanel();");
						btn_assignofficer.Attributes.Add("onClick","return  validateAssignPanel();");
						btn_searchofficer.Attributes.Add("onClick","return  validateSearch();");
				
					}
	
					// Clear Error Message
					lbl_error.Text  = "";
					// Hide Data Grid
					dg_srchofficer.Visible=false;
				}
			}
			catch( Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}


		
		private void selectPanel ( bool pnl1, bool pnl2 ,bool pnl3 )
		{

		// Set Visibility Property of Search Officer Panel
		pnl_srch.Visible = pnl1;
		// Set Visibility Property of Add Officer Panel
		pnl_add.Visible=pnl2;
		// Set Visibility Property of Assign Officer Panel
		pnl_assign.Visible = pnl3;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.lnkbtn_addofficer.Click += new System.EventHandler(this.lnkbtn_addofficer_Click);
			this.lnkbtn_srchofficer.Click += new System.EventHandler(this.lnkbtn_srchofficer_Click);
			this.lnkbtn_assign.Click += new System.EventHandler(this.lnkbtn_assign_Click);
			this.btn_searchofficer.Click += new System.EventHandler(this.btn_searchofficer_Click);
			this.btn_addofficer.Click += new System.EventHandler(this.btn_addofficer_Click);
			this.btn_addreset.Click += new System.EventHandler(this.btn_addreset_Click);
			this.btn_assignofficer.Click += new System.EventHandler(this.btn_assignofficer_Click);
			this.Button4.Click += new System.EventHandler(this.Button4_Click);
			this.dg_srchofficer.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_srchofficer_ItemCommand);
			this.dg_srchofficer.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_srchofficer_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void lnkbtn_srchofficer_Click(object sender, System.EventArgs e)
		{
			// Displaying Search Officer Panel
			selectPanel(true,false,false);
			emptyControl();
		}

		private void lnkbtn_addofficer_Click(object sender, System.EventArgs e)
		{
			// Displaying ADD Officer Panel
			offno.Visible=false;
			selectPanel(false,true,false);
			emptyControl();
		}

		private void lnkbtn_assign_Click(object sender, System.EventArgs e)
		{
			
			// Displaying Assign Officer Panel
		    selectPanel(false,false,true);
			emptyControl();
		}

	

		private void btn_searchofficer_Click(object sender, System.EventArgs e)
		{
			try
			{

					// Check If OfficerNO TextBox is Empty Then Set It Default Value to 0
					string officerNo = tb_srchofficerno.Text=="" ? "0" :  tb_srchofficerno.Text;
					// Check If First Name TextBox is Empty Then Set It Default Value to %
					string firstName =  tb_srchofficerfname.Text=="" ? "%" :  tb_srchofficerfname.Text;
					// Check If Last Name TextBox is Empty Then Set It Default Value to %
					string lastName = tb_srchofficerlname.Text=="" ? "%" :  tb_srchofficerlname.Text;
					
					
					string[] keys = {"@OfficerNumber","@FirstName","@LastName"};
					object[] values = {Convert.ToInt32(officerNo),firstName,lastName};
       		
					//Getting List of Officers
					DataSet ds = ClsDB.Get_DS_BySPArr("USP_HTS_GET_OFFICERS_INFO",keys,values);
					
					// If Records Found Than Display Them
					if ( ds.Tables[0].Rows.Count > 0)
						{
							dg_srchofficer.DataSource = ds;
							dg_srchofficer.DataBind();
							dg_srchofficer.Visible=true;
						}

					else
						{
							lbl_error.Text="No Search Result Found";
							dg_srchofficer.Visible=false;
						}
	
						
			}
			catch ( Exception ex)
			{
bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lbl_error.Text=ex.ToString();
			
			}
		}

		private void dg_srchofficer_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
		
			try
			{

				
				// Check If User Click The Officer No Then Display Its Information
				if ( e.CommandName =="view" )
				{
		
					lbl_addofficerno.Text= ((LinkButton) e.Item.FindControl("lnkbtn_officerno") ).Text;
					offno.Visible=true;
					//tb_addofficerno.Enabled = false;
					tb_addofficerfname.Text = ((Label) e.Item.FindControl("lbl_fname") ).Text;
					tb_addofficerlname.Text =  ((Label) e.Item.FindControl("lbl_lname") ).Text;
					ddl_day.SelectedValue =  ((Label) e.Item.FindControl("lbl_days") ).Text;
					
					ddltime.SelectedValue ="8:00am";
				
					tb_addofficercomments.Text =  ((Label) e.Item.FindControl("lbl_comments") ).Text;
					// Display Add Officer Panel
					selectPanel(false,true,false);
					
				}

				// Check If User Press Delete Button Than Delete The Officer Record
				if ( e.CommandName == "delete" )
				{
					
					string  officerNo =  ((LinkButton) e.Item.FindControl("lnkbtn_officerno") ).Text;
					string[] keys= {"@OfficerNo"};
					object[] values = {officerNo};
                    				
					//Check if Officer Assign Tickets
					//Change by Ajmal
                    //SqlDataReader dr = ClsDB.Get_DR_BySPArr("USP_HTS_CHECK_OFFICER_ASSIGN_TICKETS",keys,values);

					IDataReader dr = ClsDB.Get_DR_BySPArr("USP_HTS_CHECK_OFFICER_ASSIGN_TICKETS",keys,values);
				
					while ( dr.Read())
					{
	
						//if No Ticket Assign Then Delete The Officer
						if ( Convert.ToInt32(dr[0]) ==0 )
						{
							ClsDB.ExecuteSP("USP_HTS_DELETE_OFFICER",keys,values);
						}
						else
						{
							lbl_error.Text="Officer is currently assigned to one or more tickets in the system, can not delete the officer";
						}
					
					}
				
					dr.Close();
				}
			
			}
			catch ( Exception ex)
			{
bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			lbl_error.Text = ex.ToString();

			}

		}

		public bool checkTicket(string TicketID ,int type)
		{
		
			// Check That Ticket No Is Valid Or Not
            string[] keys = { "@TicketID", "@Type" };
			object[] values = {TicketID,type};
			//Change By Ajmal
		
			IDataReader dr=null;
			
			try
			{
				dr = ClsDB.Get_DR_BySPArr("USP_HTS_CHECK_TICKETS ",keys,values);
			
				while ( dr.Read())
				{

					// Check Ticket Exist or Not
					if ( Convert.ToInt32(dr[0]) >= 1 )
						return true;
					
					else
						return false;
				}
			}

			catch ( Exception ex) 
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				lbl_error.Text=ex.ToString();
			}
			
			finally { dr.Close(); }


			return false;
			
		}


		public bool checkOfficer( string OfficerNo)
		{
			// Check Officer Exist Or Not
			string[] keys= {"@OfficerNo"};
			object[] values = {OfficerNo};
			//Change By Ajmal		
			//SqlDataReader dr=null;
            IDataReader dr = null;
			
			try
			{

				dr = ClsDB.Get_DR_BySPArr("USP_HTS_CHECK_OFFICER",keys,values);
			

				while ( dr.Read())
				{

					// Check Officer Exist or Not
					if ( Convert.ToInt32(dr[0]) >= 1 )
						return true;
					else
						return false;
				}
			}

			catch ( Exception ex) 
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			finally { dr.Close(); }
			
			return false;
		}
		
		
		
		public bool AssignTicket( string ticketID , string officerID , int type)
		{
            
			//Check Ticket Exist
			if ( checkTicket(ticketID,type))
			{
						
				// If Exist Than Check Officer Exist
				if ( ! checkOfficer(officerID))
				{
					lbl_error.Text = "Officer does not exists in the database , please add the officer first";
					return false;
				}
									
				else
				{
					// Both are Valid Update Record	
                    string[] keys = { "@TicketNo", "@OfficerNo", "@Type"};
					object[] values={ticketID,officerID,type};
					ClsDB.ExecuteSP("USP_HTS_ASSIGN_TICKET",keys,values);
					lbl_error.Text="Ticket has been assigned to the officer";
					return true;			
				}

			}
			else
			{
				lbl_error.Text = "Ticket number provided does not exists in client table, officer can not be assigned";
				return false;
			}
				
		
		
		}
		
		
		private void btn_assignofficer_Click(object sender, System.EventArgs e)
		{
			//Assign Ticket Numbers to Officer
			
			try
			{                
                // Save Ticket ID's Or Cause No's  
                string caseids = "";
                
                // Save Type i.e Cause No or TicketID
                int inputtype = 0;

                // Check User Select Cause Number or TicketID 
                if (rbtn_causeno.Checked)
                  caseids = tb_causeno.Text.TrimEnd();
                
                else if ( rbtn_ticketno.Checked)
                {
                    caseids = tb_assigntktno.Text.TrimEnd();
                    inputtype = 1;
                }
                                
                // Check If User Enter More Than 1 Ticket ID 
                if (caseids.IndexOf(",") > 0)
				{
                    string[] TicketIDs = caseids.Split(',');
				    // For Each Ticket ID in Textboxes Assign Them to the Officer
					if ( checkOfficer(tb_assignofficerno.Text))
					{
						foreach ( string s in TicketIDs)
						{
                            bool b = AssignTicket(s, tb_assignofficerno.Text, inputtype);
							if (!b) break;
						}
			    	}
					else
					{
					lbl_error.Text="Officer Doest Not Exist";
					
					}
			
				
				}
                			
				else
				{
			
									
					if ( !tb_assigntktno.Visible)
					{
					
						// Show redirect page
						
						bool chk = false;

						string[] keys= {"@TicketID"};
						object[] values= {lbl_assigntkt.Text};
						string ticketnum;
						DataSet ds = ClsDB.Get_DS_BySPArr("usp_hts_get_refnumber",keys,values);
						
					
						if (ds.Tables.Count > 0 )
						{
							ticketnum =ds.Tables[0].Rows[0][0].ToString();
															
							if ( ticketnum != "")
							{
                                chk = AssignTicket(ticketnum, tb_assignofficerno.Text, inputtype);
							}
									
						}
						
						else
						{

							chk = false;
						}
					
				
						
						if ( chk ) {

                            Response.Redirect("../clientinfo/violationFeeOld.aspx?search=0&caseNumber=" + lbl_assigntkt.Text,false);	
							lbl_error.Text ="Ticket Successfully Assign"; 
						}
						

					}
					else
                        AssignTicket(caseids, tb_assignofficerno.Text, inputtype);

					
					
					//selectPanel(true,false,false);
				
				}
			
				
			}
			catch(Exception ex)
			{
			         bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);       		
			}
		}

		private void btn_addofficer_Click(object sender, System.EventArgs e)
		{
		

			
			// if Update Officer

			try
			{
				
				if ( offno.Visible)
				{
					string[] keys = {"@FirstName","@LastName","@OfficerType","@TrialDateTime","@Comments","@OfficerID"};
					object[] values = {tb_addofficerfname.Text,tb_addofficerlname.Text,ddl_day.SelectedValue,ddltime.SelectedValue,tb_addofficercomments.Text,lbl_addofficerno.Text};
					// Check If Record is Displayed Then Update It
					ClsDB.ExecuteSP("USP_HTS_UPDATE_OFFICER_INFO",keys,values);
				}
				else
				{
					string[] keys = {"@FirstName","@LastName","@OfficerType","@TrialDateTime","@Comments","@OfficerID"};
					object[] values = {tb_addofficerfname.Text,tb_addofficerlname.Text,ddl_day.SelectedValue,ddltime.SelectedValue,tb_addofficercomments.Text, 0};
				// If No Record Display Previously Than Add The New Officer
					int offid  = Convert.ToInt32(  ClsDB.ExecuteScalerBySp( "USP_HTS_ADD_OFFICER_INFO",keys,values));
					lbl_addofficerno.Text = offid.ToString();
				
				}
				
				
				// Display Search Officer Panel 
				tb_srchofficerfname.Text = tb_addofficerfname.Text;
				tb_srchofficerlname.Text=tb_addofficerlname.Text;
				tb_srchofficerno.Text = lbl_addofficerno.Text;
               	selectPanel(true,false,false);
				
				
				// Empty ALL Textboxes in Add Officer Panel	
				tb_addofficercomments.Text = "";
				tb_addofficerfname.Text="";
				tb_addofficerlname.Text="";
				lbl_addofficerno.Text="";
				lbl_addofficerno.Enabled=true;

			
			
			}
			catch(Exception ex)
			{
				lbl_error.Text= ex.ToString();
bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		private void btn_addreset_Click(object sender, System.EventArgs e)
		{
			
			//Display Search Officer Panel
			tb_srchofficerfname.Text ="";
			tb_srchofficerlname.Text="";
			tb_srchofficerno.Text ="";
			selectPanel(false,true,false);
			

			// Empty ALL Textboxes in Add Officer Panel	
			tb_addofficercomments.Text = "";
			tb_addofficerfname.Text="";
			tb_addofficerlname.Text="";
			lbl_addofficerno.Text="";
	
		}

		private void Button4_Click(object sender, System.EventArgs e)
		{
			
			//Display Search Officer Panel
			tb_srchofficerfname.Text ="";
			tb_srchofficerlname.Text="";
			tb_srchofficerno.Text ="";
			selectPanel(false,false,true);

			//  Empty ALL Textboxes in Assign Tickets Panel	
			tb_assignofficerno.Text="";
			tb_assigntktno.Text="";




		}

		private void dg_srchofficer_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if ( e.Item.ItemType == ListItemType.Item ||  e.Item.ItemType == ListItemType.AlternatingItem)
				{
					((LinkButton) e.Item.FindControl("lnkbtn_delete") ).Attributes.Add("onclick","return confirm('Are you sure you want to delete this !');");
				}
			}
			catch ( Exception  ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		public ListItem formatTime (int time)
		{

			int temp=time/ 100;

			if ( temp < 13 )
			return new ListItem( temp.ToString()+" AM",temp.ToString()+"am");				
			
			else
			{
				
				temp = temp - 12;
				return new ListItem( temp.ToString()+" PM",temp.ToString()+"pm");	
			}
		}

		public  void emptyControl()
		{
			
			// Setting Control On Assign Officer Panel
			tb_assignofficerno.Text="";
            tb_causeno.Text = "";
			tb_assigntktno.Text="";
			tb_assigntktno.Visible= true;
			lbl_assigntkt.Visible = false;
			
			// Setting COntrol On Search Officer Panel
			tb_srchofficerfname.Text="";
			tb_srchofficerlname.Text="";
			tb_srchofficerno.Text="";
		
				
		
		}

        protected void btn_searchofficer_Click1(object sender, EventArgs e)
        {

        }

        protected void Button4_Click1(object sender, EventArgs e)
        {

        }

       
	

		
	}
}

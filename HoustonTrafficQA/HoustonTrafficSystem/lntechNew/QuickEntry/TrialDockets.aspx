<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" Codebehind="TrialDockets.aspx.cs" AutoEventWireup="false" Inherits="lntechNew.QuickEntry.TrialDockets" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>HTP Trial Docket</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        
        <script>
		function OpenPopUp(path, Val)  //(var path,var name)
		{
		 window.open(path,'',"height=300,width=400,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		  //return true;
		}
		
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		
		<form id="Form1" method="post" runat="server">
            
	    <TABLE  id = "MainTable" cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 780px">
	    <tbody>
	        <TR>
						<TD style="width: 780px">
                            &nbsp;<uc1:ActiveMenu id="ActiveMenu1" runat="server" />
						</TD>
					</TR>
				<tr>	
						
					</TR>
					<TR >
						<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" 
							height="9">
			</TD>
					</TR>
				    <TR >
			        <TD >
			
			    <TABLE  cellPadding="0"  border="0" style="width: 780px">
				<TR>
					<td><asp:dropdownlist id="DDLCourts" runat="server" CssClass="clsInputCombo" Width="127px">
							<asp:ListItem Value="None">--Choose--</asp:ListItem>
						</asp:dropdownlist></td>
					<td><ew:calendarpopup id="dtpCourtDate" runat="server" Text=" " Width="80px" ImageUrl="../images/calendar.gif"
							SelectedDate="2006-03-14" EnableHideDropDown="True" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
							ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" Nullable="True" UpperBoundDate="12/31/9999 23:59:00"
							PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt">
							<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
							<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></WeekdayStyle>
							<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Yellow"></MonthHeaderStyle>
							<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
								BackColor="AntiqueWhite"></OffMonthStyle>
							<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></GoToTodayStyle>
							<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="LightGoldenrodYellow"></TodayDayStyle>
							<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Orange"></DayHeaderStyle>
							<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="LightGray"></WeekendStyle>
							<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Yellow"></SelectedDateStyle>
							<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></ClearDateStyle>
							<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></HolidayStyle>
						</ew:calendarpopup></td>
					<td><asp:textbox id="TxtCourtNo" runat="server" CssClass="clsinputadministration" Width="30px">0</asp:textbox></td>
					<td><asp:dropdownlist id="DropDownList2" runat="server" CssClass="clsInputCombo">
							<asp:ListItem Value="0">Trial Docket</asp:ListItem>
							<asp:ListItem Value="1">Trial Results</asp:ListItem>
						</asp:dropdownlist></td>
					<td><asp:listbox id="LstCaseStatus" runat="server" CssClass="clsInputCombo" Height="48px">
							<asp:ListItem Value="0" Selected="True">---Choose----</asp:ListItem>
							<asp:ListItem Value="2">Arraignement</asp:ListItem>
							<asp:ListItem Value="3">Pre Trial Setting</asp:ListItem>
							<asp:ListItem Value="4">Jury Trail Setting</asp:ListItem>
							<asp:ListItem Value="5">JudgeTrial</asp:ListItem>
						</asp:listbox></td>
					<td style="width: 102px"><asp:button id="Button_Submit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></td>
				</TR>
			</TABLE>
			</TD>
									</TR>
                                <tr>
                                    <td background="../../images/separator_repeat.gif"  height="9" style="width: 780px;
                                        height: 9px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" border="0" style="width: 780px">
				<TR>
					<TD style="WIDTH: 231px"  align="left">
						<asp:label id="lblRecStatus" runat="server" Font-Size="10px" ForeColor="Red" Visible="False"></asp:label></TD>
					<TD  style="font-size: 10pt" align="right"> 
						
						<asp:imagebutton id="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg" Visible="False" ToolTip="Print" ></asp:imagebutton><asp:imagebutton id="imgExp_Ds_to_Excel" runat="server" ImageUrl="../Images/excel_icon.gif" Visible="False"
							Width="68px" Height="36px" ToolTip="Excel"></asp:imagebutton>
                        <asp:ImageButton ID="img_pdf" runat="server" ImageUrl="../Images/pdf.jpg" OnClick="img_pdf_Click"
                            Visible="False" ToolTip="Print Single" /><asp:ImageButton ID="img_PdfAll" runat="server" ImageUrl="../Images/pdfall.jpg" Visible="False" OnClick="img_PdfAll_Click" ToolTip="Print All" /><asp:ImageButton ID="btnPDFCourts" runat="server" ImageUrl="../Images/pdfall.jpg" OnClick="img_PDFCourt" ToolTip="Print All Court" />
                        <asp:DropDownList ID="ddl_Copies" runat="server">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem Selected="True">5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList></TD>
				</TR>
			</TABLE>
                                    </td>
                                </tr>
                                <tr style="font-size: 10pt">
                                </tr>
                                <tr style="font-size: 10pt">
                                    <td >
			<asp:table id="Table_OutPut" runat="server" Width="780px" Font-Names="Tahoma" EnableViewState="False"
				GridLines="Both" BorderStyle="None" BorderWidth="1px" BorderColor="#404040"></asp:table>
                                    </td>
                                </tr>
                                <tr style="font-size: 10pt">
                                    <td >
			<TABLE cellSpacing="0" cellPadding="0" align="center" border="0" style="font-size: 10pt; width: 780px;">
				<TBODY>
					<TR>
						<TD style="width: 780px; height: 80px;" >
							<TABLE cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 780px">
								<TBODY>
									<TR >
										<TD width="780" background="../images/separator_repeat.gif"  height="11"></TD>
									</TR>
									<TR>
										<TD width="780" >
											<P><uc1:footer id="Footer1" runat="server"></uc1:footer></P>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							
						</TD>
					</TR>
				</TBODY>
			</TABLE>
                                   
		</form>
	</body>
</HTML>

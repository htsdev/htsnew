<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attorneys.aspx.cs" Inherits="lntechNew.QuickEntry.Attorneys" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>

<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   
   <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
   
    <title>Atorneys Email Addresses </title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>
   
   <script>
   
    
    function checkemail()
    {
    
    //tb_email
    
    if(document.getElementById("tb_email").value !="" )
	{			
				if( isEmail(document.getElementById("tb_email").value)== false)
				{
				alert ("Please Enter Email Address In Correct format.");
				document.getElementById("tb_email").focus(); 
				return false;			   
				}
     }	
	else
	{       	
	        alert ("Please Enter Email Address ");
	        document.getElementById("tb_email").focus(); 
	        return false;
	
	}	   
        
    
    }
    
   
   
   
   
   </script>
   
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101;
            left: 88px; position: absolute; top: 16px" width="780">
            <tr>
                <td colspan="4" style="width: 815px">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4" style="width: 816px">
                    <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" colspan="4" valign="top">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" colspan="4" height="11" width="100%">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" rowspan="1" width="100%" >
                                <table width="100%" class="clsleftpaddingtable">
                                    <tr>
                                        <td class="clsleftpaddingtable" style="width: 10px">
                                            <asp:Label ID="Label1" runat="server" CssClass="clssubhead" Text="Enter Email Address :"
                                                Width="132px"></asp:Label></td>
                                        <td  style="width: 181px">
                                            <asp:TextBox ID="tb_email" runat="server"  Width="201px" CssClass="clsinputadministration" Font-Bold="True"></asp:TextBox></td>
                                        <td class="clsleftpaddingtable">
                                            &nbsp;
                                            <asp:Button ID="btn_refresh" runat="server" CommandName="search" CssClass="clsbutton"
                                                OnClick="btn_refresh_Click" Text="Add" Width="72px" OnClientClick="return checkemail();" /></td>
                                        <td class="clsleftpaddingtable" style="width: 100px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" background="../Images/separator_repeat.gif" colspan="4" style="height: 13px">
                            </td>
                        </tr>
                        <tr>
                            <td align="left"  colspan="4" style="width: 100%;">
                                <asp:GridView ID="gv_list" runat="server" AutoGenerateColumns="False" BorderStyle="None" OnRowCommand="gv_list_RowCommand" OnRowDeleting="gv_list_RowDeleting" Width="100%" CssClass="clsleftpaddingtable" OnSelectedIndexChanged="gv_list_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Attorney's Email Addresses" HeaderStyle-HorizontalAlign="center" HeaderStyle-Height="20px">
                                            <HeaderTemplate>
                                                <asp:Label ID="lbl_eadd" runat="server" CssClass="clssubhead" Text=" Attorney's Email Addresses"
                                                    Width="165px"></asp:Label>
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="clsleftpaddingtable" Width="725px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_address" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container,"DataItem.EmailAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="clsleftpaddingtable" Width="100px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btn_delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.Id") %>'
                                                    CssClass="button" CommandName="delete">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" background="../Images/separator_repeat.gif" colspan="4" style="width: 760px;
                                height: 13px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="width: 760px">
                                &nbsp;<asp:Label ID="lbl_error" runat="server" CssClass="clslabel" Font-Bold="True"
                                    ForeColor="Red"></asp:Label></td>
                        </tr>
                    </table>
                    <uc3:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

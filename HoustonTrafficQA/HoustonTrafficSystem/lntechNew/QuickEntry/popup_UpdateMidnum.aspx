<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="popup_UpdateMidnum.aspx.cs" Inherits="lntechNew.QuickEntry.popup_UpdateMidnum" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head runat="server">
    <title>Update Page</title>
    <base target="_self" />
    <base target="_self" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
    function submitform()
    {

        var oldmid = document.getElementById('hf_OldMidnum');
        
        if(oldmid.value == '' || oldmid.value == null)
        {
            window.returnValue = 1;
            window.close();
        }
        
        else
            {
                var conf = window.confirm('Mid # ' +oldmid.value +' already exists for this client. Would you like to overwrite it?');
                if(conf == true)
                {
                window.returnValue = 1;
                }
                else
                {
                window.returnValue = -1;
                }
                window.close();
            }
    }
    
    function cancelform()
    {
        window.returnValue=-1;
        window.close();
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="550px">
    <tr>
    <td width="100%" class="clssubhead"  background="../Images/subhead_bg.gif" colSpan="3" height="34px" >
        Update MidNumber</td>
    </tr>
        <tr>
                <td width="100%"> <asp:HiddenField ID="hf_OldMidnum" runat="server" Value="" /><asp:HiddenField ID="hf_RecordCount" runat="server" Value="" />
                    <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="clsLabel"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 18px" width="100%">
                <asp:Label ID="lbl_Fullname" runat="server" CssClass="clsLabel"></asp:Label></td>
        </tr>
        <tr>
            <td width="100%">
                <asp:Label ID="lbl_MidNum" runat="server" CssClass="clsLabel"></asp:Label></td>
        </tr>
        <tr>
            <td width="100%" class="clsLabel">
                <asp:Label ID="lbl_Info" runat="server" Text="Is this information correct?"></asp:Label>
                </td>
        </tr>
        <tr>
        <td width="100%" background="../Images/separator_repeat.gif"  height="11">
        </td>
        </tr>
        <tr>
            <td >
            <table width="100%">
            <tr id="tr_yesno" runat="server">
            <td align="center">
                <asp:Button ID="btn_Yes" runat="server" CssClass="clsbutton" Text="Yes" Width="50px" OnClientClick="return submitform()" />
            </td>
                <td align="center"> 
                    <asp:Button ID="btn_No" runat="server" CssClass="clsbutton" Text="No" Width="50px" OnClientClick="return cancelform()" /> 
            </td>
            </tr>
            <tr id="tr_cancel" runat="server">
            <td align="center">
            <asp:Button ID="btn_Close" runat="server" CssClass="clsbutton" Text = "Close" OnClientClick="return cancelform()" />
            </td>
            </tr>
                </table>
             </td>
             
        </tr>
    </table>
    
    </div>
    </form>
</body>
</html>

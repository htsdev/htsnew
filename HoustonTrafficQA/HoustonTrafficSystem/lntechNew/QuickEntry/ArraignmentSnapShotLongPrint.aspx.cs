using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for ArraignmentSnapShotLongPrint.
	/// </summary>
	public partial class ArraignmentSnapShotLongPrint : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddl_WeekDay;
		protected System.Web.UI.WebControls.Label lbl_CurrDateTime;
		protected System.Web.UI.WebControls.Label lbl_Message;
		protected System.Web.UI.WebControls.DataGrid dg_Report;
		protected System.Web.UI.WebControls.DataGrid dg_ReportJT;
		protected System.Web.UI.WebControls.DataGrid dg_ReportOT;
		clsLogger bugTracker = new clsLogger();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
					
				lbl_CurrDateTime.Text=String.Concat("Date: ",DateTime.Now.ToString());
				if(!IsPostBack)
				{
                    
                    clsSession ClsSession=new clsSession();
					
					try
					{
						string criteria=Request.QueryString["criteria"].ToString().Trim();
						ddl_WeekDay.SelectedValue=criteria;
                        lbl_DayType.Text = ddl_WeekDay.SelectedItem.Text;

						clsENationWebComponents clsDB=new clsENationWebComponents();

                        if (ddl_WeekDay.SelectedValue == "100")
                        {
                            DataSet ds_JT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1");
                            lbl_Judge.Visible = true;
                            if (ds_JT.Tables[0].Rows.Count > 0)
                            {
                                dg_ReportJT.DataSource = ds_JT;
                                dg_ReportJT.DataBind();
                                GenerateSerialJT();
                            }                            
                        }
                        else if(ddl_WeekDay.SelectedValue=="99")
						{
							DataSet ds_JT=clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1");
							DataSet ds_OT=clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterOutsideArraignment_ver_1");
                            lbl_Judge.Visible = true;
                            lbl_Outside.Visible = true;
							if(ds_JT.Tables[0].Rows.Count>0)
							{
								dg_ReportJT.DataSource=ds_JT;
								dg_ReportJT.DataBind();
								GenerateSerialJT();					
							}
							if(ds_OT.Tables[0].Rows.Count>0)
							{
								dg_ReportOT.DataSource=ds_OT;
								dg_ReportOT.DataBind();
								GenerateSerialOT();
							}
						}

                        if (ddl_WeekDay.SelectedValue != "-1" & ddl_WeekDay.SelectedValue != "100")
						{
							string[] key1    = {"@ReportWeekDay"};
							object[] value2 = {Convert.ToInt32(ddl_WeekDay.SelectedValue)};
							DataSet	ds=clsDB.Get_DS_BySPArr("usp_HTS_ArraignmentSnapshotLong_ver_1",key1,value2);
							if(ds.Tables[0].Rows.Count>0)
							{
								dg_Report.DataSource=ds;
								dg_Report.DataBind();
								GenerateSerial();
								dg_Report.Visible=true;
								lbl_Message.Visible=false;
							}
							else
							{
								dg_Report.Visible=false;
								lbl_Message.Visible=true;
							}	
						}			
					}
					catch(Exception ex)
					{
						bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
					}
				}
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddl_WeekDay.SelectedIndexChanged += new System.EventHandler(this.ddl_WeekDay_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ddl_WeekDay_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
		}

		private void GenerateSerial()
		{
							
			try
			{
				int no=1;
				string ticketid="";
				foreach (DataGridItem ItemX in dg_Report.Items) 
				{ 					
					Label sno=(Label) ItemX.FindControl("lbl_sno");
					Label tid=(Label) ItemX.FindControl("lbl_Ticketid");
					if(tid.Text!="")
					{
						if(no==1)
						{
							sno.Text=no.ToString();
							ticketid=tid.Text;
							no=no+1;
						}
						else
						{
							if(ticketid!=tid.Text)
							{
								sno.Text=Convert.ToString(no);
								ticketid=tid.Text;
								no=no+1;
							}
							else
							{
								sno.Text="";
							}
						}	
						Label arrdate=(Label) ItemX.FindControl("lbl_arrdate");
						if(arrdate.Text!="")
						{
							DateTime adate=Convert.ToDateTime(arrdate.Text.ToString());
							arrdate.Text=String.Concat(adate.Month,"/",adate.Day);
						}
						Label dob=(Label) ItemX.FindControl("lbl_dob");
					
						if(dob.Text!="")
						{
							DateTime adate=Convert.ToDateTime(dob.Text.ToString());
							dob.Text=String.Concat(adate.Month,"/",adate.Day,"/",adate.Year);
						}			
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}
	
		private void GenerateSerialJT()
		{
							
			try
			{
				int no=1;
				string ticketid="";
				foreach (DataGridItem ItemX in dg_ReportJT.Items) 
				{ 					
					Label sno1=(Label) ItemX.FindControl("lbl_sno1");
					Label tid1=(Label) ItemX.FindControl("lbl_Ticketid1");
					if(tid1.Text!="")
					{
						if(no==1)
						{
							sno1.Text=no.ToString();
							ticketid=tid1.Text;
							no=no+1;
						}
						else
						{
							if(ticketid!=tid1.Text)
							{
								sno1.Text=Convert.ToString(no);
								ticketid=tid1.Text;
								no=no+1;
							}
							else
							{
								sno1.Text="";
							}
						}	
						Label date=(Label) ItemX.FindControl("lbl_CCDate");
						Label num=(Label) ItemX.FindControl("lbl_CCNum");
						if(date.Text!="")
						{
							DateTime adate=Convert.ToDateTime(date.Text.ToString());
						
							date.Text=String.Concat(adate.Month,"/",adate.Day,"/",adate.Year," @ ",adate.ToShortTimeString()," # ",num.Text);
						}
						Label dob=(Label) ItemX.FindControl("lbl_dob1");
					
						if(dob.Text!="")
						{
							DateTime adate=Convert.ToDateTime(dob.Text.ToString());
							dob.Text=String.Concat(adate.Month,"/",adate.Day,"/",adate.Year);

						}
					}					
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}
		private void GenerateSerialOT()
		{
							
			try
			{
				int no=1;
				string ticketid="";
				foreach (DataGridItem ItemX in dg_ReportOT.Items) 
				{ 					
					Label sno2=(Label) ItemX.FindControl("lbl_sno2");
					Label tid2=(Label) ItemX.FindControl("lbl_Ticketid2");
					if(tid2.Text!="")
					{
						if(no==1)
						{
							sno2.Text=no.ToString();
							ticketid=tid2.Text;
							no=no+1;
						}
						else
						{
							if(ticketid!=tid2.Text)
							{
								sno2.Text=Convert.ToString(no);
								ticketid=tid2.Text;
								no=no+1;
							}
							else
							{
								sno2.Text="";
							}
						}	
						Label date=(Label) ItemX.FindControl("lbl_CCDate1");
						Label num=(Label) ItemX.FindControl("lbl_CCNum1");
						if(date.Text!="")
						{
							DateTime adate=Convert.ToDateTime(date.Text.ToString());
						
							date.Text=String.Concat(adate.Month,"/",adate.Day,"/",adate.Year," @ ",adate.ToShortTimeString()," # ",num.Text);
						}
						Label dob=(Label) ItemX.FindControl("lbl_dob2");
					
						if(dob.Text!="")
						{
							DateTime adate=Convert.ToDateTime(dob.Text.ToString());
							dob.Text=String.Concat(adate.Month,"/",adate.Day,"/",adate.Year);
						}
					}
				}
			}
			catch(Exception ex)

			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}
	}
}

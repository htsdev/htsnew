<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.QuickEntry.Officer" Codebehind="Officer.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Officer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
		
		function MaxLength()
		{
		if(document.getElementById("txtRemarks").value.length >500)
		{
		alert("The length of character is not in the specified Range");
		document.getElementById("txtRemarks").focus();
		}
		}
		
		function ClearControls(type)
		{
		    if (type == 'assaign') 
		    {
		        document.Form1.tb_assigntktno.value = '';
		        document.Form1.tb_assignofficerno.value = '';
		    }
		    else if (type = 'add')
		    {
		    document.Form1.tb_addofficerfname.value = '';
		    document.Form1.tb_addofficerlname.value = '';  
		    document.Form1.tb_addofficercomments.value = '' ;
		    }
		    return false;
		}
		
		 function ValidateLenght()
	     { 
			var cmts = document.Form1.tb_addofficercomments.value;
			if (cmts.length > 500)
			{
			    event.returnValue=false;
                event.cancel = true;
			}
	    }
	    

        // FUNCTION TO VALIDATE SEARCH CRITERIA......		
		function validateSearch()
		{
		
		    if (isNaN(document.getElementById("tb_srchofficerno").value))
		    {
		        alert("Please provide a Numeric value for search");
		        document.getElementById("tb_srchofficerno").focus();
				return false;
			}
			
			if ( document.getElementById("tb_srchofficerno").value == ""  && document.getElementById("tb_srchofficerfname").value == "" && document.getElementById("tb_srchofficerlname").value == "")
			{
				alert("Please provide a valid criteria for search");
				return false;
			}

			if (document.getElementById("tb_srchofficerno").value.length > 9 )
			{
				alert("You can't enter more than 9 characters.");
				document.getElementById("tb_srchofficerno").focus();
				return false;
			}

			if (document.getElementById("tb_srchofficerfname").value.length > 50 )
			{
				alert("You can't enter more than 50 characters.");
				document.getElementById("tb_srchofficerfname").focus();
				return false;
			}

			if (document.getElementById("tb_srchofficerlname").value.length > 50 )
			{
				alert("You can't enter more than 50 characters.");
				document.getElementById("tb_srchofficerlname").focus();
				return false;
			}
		
		    return true;
		}

        // FUNCTION TO VALIDATE ASSIGN TICKET FUNCTIONALITY....		
		function validateAssignPanel()
		{
		    try
		        {
			        if ( document.getElementById("rbtn_ticketno").checked)
			        {			        
			            if(document.getElementById("tb_assigntktno").value == "")
			            {
				            alert("Please Enter Ticket Number");
				            return false;
			            }
			        }
			        else if ( document.getElementById("rbtn_causeno").checked)
			        {			        
			            if(document.getElementById("tb_causeno").value == "")
			            {
				            alert("Please Enter Cause Number");
				            return false;
			            }
			        }
		        }
		    catch ( e) { }
    			 	
		    if(document.getElementById("tb_assignofficerno").value == "")
		    {
			    alert("Please Enter Officer Number");
			    return false;
		    }	 	
			return true;
		}
		
		
		// FUNCTION TO VALIDATE USER INPUT FOR ADDING OFFICER.....
		function validateAddPanel()
		{ 
		    var fname = document.getElementById("tb_addofficerfname").value;
		    var lname = document.getElementById("tb_addofficerlname").value;
		    var cmt = document.getElementById("tb_addofficercomments").value;
		    
		    
			if(fname == "")
			{
				alert("Please Enter First Name");
				return false;
			}	
			
			if (fname.length > 50 )
			{
				alert("You can't enter more than 50 characters.");
				document.getElementById("tb_srchofficerfname").focus();
				return false;
			}
			
			 	
			if(lname == "")
			{
				alert("Please Enter Last Name");
				return false;
			}	 	


			if (lname.length > 50 )
			{
				alert("You can't enter more than 50 characters.");
				document.getElementById("tb_srchofficerlname").focus();
				return false;
			}

    		if (cmt.length > 500 )
			{
				alert("You can't enter more than 500 characters.");
				document.getElementById("tb_addofficercomments").focus();
				return false;
			}
			return true;
		}
		
		
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" align="center" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
				</TR>
				<TR>
								<TD align="left" background="../Images/separator_repeat.gif" colSpan="4"></TD>
				</TR>
					<TD style="WIDTH: 816px" colSpan="4">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD class="clssubhead" style="HEIGHT: 37px" width="100%" background="../../Images/subhead_bg.gif"
									colSpan="4" height="37"><asp:linkbutton id="lnkbtn_addofficer" runat="server" BackColor="Transparent" CssClass="clssubhead">Add Officer</asp:linkbutton>&nbsp;|
									<asp:linkbutton id="lnkbtn_srchofficer" runat="server" BackColor="Transparent" CssClass="clssubhead">Search Officer</asp:linkbutton>&nbsp;|
									<asp:linkbutton id="lnkbtn_assign" runat="server" BackColor="Transparent" CssClass="clssubhead">Assign Officer to Ticket Number </asp:linkbutton></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="left" colSpan="4"><asp:panel id="pnl_srch" runat="server" Height="10px">
										<TABLE class="clsleftpaddingtable" id="Table2" borderColor="#ffffff" cellSpacing="1" cellPadding="1"
											width="100%" border="1">
											<TR>
												<TD style="WIDTH: 90px" align="right">&nbsp;
													<asp:Label id="Label3" runat="server" CssClass="label">Officer #:</asp:Label></TD>
												<TD>
													<asp:TextBox id="tb_srchofficerno" runat="server" CssClass="clsinputadministration" Width="72px" MaxLength="9"></asp:TextBox></TD>
												<TD>
													<asp:Label id="Label4" runat="server" CssClass="label">First Name</asp:Label>&nbsp;
													<asp:TextBox id="tb_srchofficerfname" runat="server" CssClass="clsinputadministration" Width="160px" MaxLength="50"></asp:TextBox></TD>
												<TD>
													<asp:Label id="Label5" runat="server" CssClass="label">Last  Name</asp:Label>&nbsp;
													<asp:TextBox id="tb_srchofficerlname" runat="server" CssClass="clsinputadministration" Width="160px" MaxLength="50"></asp:TextBox></TD>
												<TD>
													<asp:button id="btn_searchofficer" runat="server" CssClass="clsbutton" Width="104px" Text="Search Officer"
														CommandName="search" ></asp:button></TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="pnl_add" runat="server" Height="140px">
										<TABLE class="clsleftpaddingtable" id="Table3" borderColor="white" cellSpacing="0" cellPadding="0"
											width="100%" border="1">
											<TR id="offno" runat="server">
												<TD style="HEIGHT: 22px" align="left" width="20%">
													<asp:Label id="Label6" runat="server" CssClass="label">Officer #:</asp:Label></TD>
												<TD style="WIDTH: 185px; HEIGHT: 22px">
													<asp:Label id="lbl_addofficerno" runat="server" CssClass="label"></asp:Label></TD>
											</TR>
											<TR>
												<TD align="left" width="20%">
													<asp:Label id="Label2" runat="server" CssClass="label">First Name</asp:Label></TD>
												<TD style="WIDTH: 185px">
													<asp:TextBox id="tb_addofficerfname" runat="server" CssClass="clsinputadministration" Width="160px"
														EnableViewState="False" MaxLength="50"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 143px" align="left">
													<asp:Label id="Label1" runat="server" CssClass="label">Last  Name</asp:Label></TD>
												<TD style="WIDTH: 185px">
													<asp:TextBox id="tb_addofficerlname" runat="server" CssClass="clsinputadministration" Width="160px"
														EnableViewState="False" MaxLength="50"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 143px; HEIGHT: 30px" align="left">
													<asp:Label id="Label7" runat="server" CssClass="label" Width="112px">Trial Day / Time</asp:Label></TD>
												<TD style="WIDTH: 185px; HEIGHT: 30px">
													<asp:DropDownList id="ddl_day" runat="server" CssClass="clsInputCombo" Width="56px">
														<asp:ListItem Value="MONDAY">MON</asp:ListItem>
														<asp:ListItem Value="TUESDAY">TUE</asp:ListItem>
														<asp:ListItem Value="WEDNESDAY">WED</asp:ListItem>
														<asp:ListItem Value="THURSDAY">THU</asp:ListItem>
														<asp:ListItem Value="FRIDAY">FRI</asp:ListItem>
													</asp:DropDownList>
													<asp:DropDownList id="ddltime" runat="server" CssClass="clsInputCombo">
														<asp:ListItem Value="8:00am">8:00 AM</asp:ListItem>
														<asp:ListItem Value="10:30am">10:30 AM</asp:ListItem>
													</asp:DropDownList></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 143px; HEIGHT: 31px" align="left">
													<asp:Label id="Label8" runat="server" CssClass="label" Width="112px">Comments</asp:Label></TD>
												<TD style="WIDTH: 185px; HEIGHT: 31px">
													<asp:TextBox id="tb_addofficercomments" onkeypress="ValidateLenght();" runat="server" CssClass="clsinputadministration" Width="262px"
														EnableViewState="False" TextMode="MultiLine" MaxLength="500"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 143px" align="left"></TD>
												<TD style="WIDTH: 185px">
													<asp:button id="btn_addofficer" runat="server" CssClass="clsbutton" Width="104px" Text="Add Officer"
														CommandName="search"></asp:button>&nbsp;
													<asp:button id="btn_addreset" runat="server" CssClass="clsbutton" Width="56px" Text="Reset"
														CommandName="search"></asp:button></TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="pnl_assign" runat="server" Height="70px">
										<TABLE class="clsleftpaddingtable" id="Table4" borderColor="white" cellSpacing="0" cellPadding="0"
											width="100%" border="1">
											<TR>
												<TD align="left" width="20%">
                                                    <asp:RadioButton ID="rbtn_causeno" runat="server" Checked="True" GroupName="tkt" CssClass="Label" Text="Cause Number :" />
													</TD>
												<TD style="width: 174px">
													<asp:TextBox id="tb_causeno" runat="server" CssClass="clsinputadministration" Width="125px" MaxLength="20"></asp:TextBox></TD>
											</TR>
                                            <tr>
                                                <td align="left" width="20%">
                                                    <asp:RadioButton ID="rbtn_ticketno" runat="server" GroupName="tkt" CssClass="label" Text="Ticket Number :" />
                                                    </td>
                                                <td style="width: 174px">
                                                    <asp:TextBox ID="tb_assigntktno" runat="server" CssClass="clsinputadministration"
                                                        MaxLength="20" Width="125px"></asp:TextBox>
													<asp:Label id="lbl_assigntkt" runat="server" CssClass="label"></asp:Label></td>
                                            </tr>
											<TR>
												<TD style="WIDTH: 107px" align="left">
													<asp:Label id="Label11" runat="server" CssClass="label">Officer # :</asp:Label></TD>
												<TD style="WIDTH: 174px">
													<asp:TextBox id="tb_assignofficerno" runat="server" CssClass="clsinputadministration" Width="88px"
														EnableViewState="False" MaxLength="9"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 107px" align="left"></TD>
												<TD style="WIDTH: 174px">
													<asp:button id="btn_assignofficer" runat="server" CssClass="clsbutton" Width="104px" Text="Assign Officer"
														CommandName="search"></asp:button>&nbsp;
													<asp:button id="Button4" runat="server" CssClass="clsbutton" Width="56px" Text="Reset" CommandName="search" OnClick="Button4_Click1"></asp:button></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 107px" align="left"></TD>
												<TD style="WIDTH: 174px"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%" colSpan="4" height="11"><asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="0"><asp:datagrid id="dg_srchofficer" runat="server" CssClass="clsleftpaddingtable" Width="776px"
										CellPadding="0" AutoGenerateColumns="False" PageSize="1">
										<Columns>
											<asp:TemplateColumn HeaderText="OFFICER #">
												<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id=lnkbtn_officerno runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.officernumber_pk") %>' CommandName="view">
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="FIRST NAME">
												<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_fname runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="LASTNAME">
												<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_lname runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
													</asp:Label>&nbsp;
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DAYS">
												<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_days runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.officertype") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="TIME">
												<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_time runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton id="lnkbtn_delete" runat="server" CommandName="delete">Delete</asp:LinkButton>
													<asp:Label id=lbl_comments runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.comments") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="left" background="../Images/separator_repeat.gif" colSpan="4" height="11" ></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
							</TR>
						</TABLE>
					</TD>
			</TABLE>
		</form>
	</body>
</HTML>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ArrCombined.aspx.cs" Inherits="lntechNew.QuickEntryNew.ArrCombined" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Scheduled Arraignments</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">
         
    <script language="javascript">
		
		function ArraignmentDocketPopUp(flag)
		{
		    var day=document.getElementById("HF_ArraignmentDocket").value;
		    window.open("../Reports/ArraignmentDocket.aspx?day="+day+"&Flag="+flag,'',"toolbar=no,status=no,menubar=no,resizable=yes");
		    return false;
		}
		
	    function PrintAllBondValidate()
	    {
	    	    if(ValidateBondWaiting()==false)
                {
                    alert("Please Select Row for Print Bond.");
		            return false;
                }
	    }
	    
	       function PopUpShowPreviewPDF(TDocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("../Paperless/PreviewMain.aspx?DocID=0&TDocID="+ TDocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				return false;
			}
		
	 function ValidateArraignmentWaiting()
		{ 
		  
		  var rec_count =document.getElementById("hf_Count").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="dg_AWR_ctl02_chk_Select";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="dg_AWR_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk_Select";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk_Select";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
	    
	    
	     function ValidateBondWaiting()
		{ 
		  
		  var rec_count =document.getElementById("hf_Count").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="dg_BondWaiting_ctl02_chk";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="dg_BondWaiting_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
		
		function ShowBndWaitingPopup()
		{
		
		if(ValidateBondWaiting()==false)
		{
		alert('No record Selected');
		return(false);
		}
		var res = confirm('A document ID Number will be generated for this report.');
		if(res==true)
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 1;
		}
		else
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 0;
		}
		return(true);
		}
		
		
		function ShowArrWaitingPopup()
		{
		
		if (ValidateArraignmentWaiting()==false)
		{
		alert('No record Selected');
		return(false);
		}
		var res = confirm('A document ID Number will be generated for this report.');
		if(res==true)
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 1;
		}
		else
		{
		 document.getElementById('hf_ArrWaiting_GenIDs').value = 0;
		}
		return(true);
		}
		
		function Hide()
		{
		
			if( document.getElementById("ddl_WeekDay").value=="-1")
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';	
				document.getElementById("bond").style.display='none';					
				document.getElementById("bondTD").style.display='none';					
			}
			else if(document.getElementById("ddl_WeekDay").value==100)
			{
				document.getElementById("jt").style.display='block';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
				document.getElementById("bond").style.display='none';					
				document.getElementById("bondTD").style.display='none';	
			}
			//code Added by Azwer... for hiding and visible bond report..
			else if(document.getElementById("ddl_WeekDay").value==150)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
				document.getElementById("bond").style.display='block';					
				document.getElementById("bondTD").style.display='block';	
			}
			else if(document.getElementById("ddl_WeekDay").value!=99)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
				document.getElementById("bond").style.display='none';					
				document.getElementById("bondTD").style.display='none';	
			}
			
			else
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='block';
				document.getElementById("ostgrd").style.display='block';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='block';
				document.getElementById("a2").style.display='block';
				document.getElementById("a3").style.display='block';				
			}
			
			
		}
		function OpenBondSummary(path)
		{
		    
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes");
		    
		}
		 function Validate()
		{ 
		  
		  var rec_count =document.getElementById("txtARRSummCount").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="ASdg_ctl02_chk1";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="ASdg_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk1";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk1";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
	     function select_deselectAll(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,10)=='dgbond$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
		
		 function select_deselectAll_ArrWait(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,10)=='dg_AWR$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
			 function select_deselectAll_BondWait(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,18)=='dg_BondWaiting$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
		
		function Prompt(val)
		{
		    
		    if(val== "1")
		    {
		        if(Validate() == false)
		        {
		            alert("Please Select Row for Print");
		            return false;
		        }
		         doyou = confirm("Selected Cases will be changed to waiting status and document ID will be generated? (OK = Yes   Cancel = No)"); 
                 if (doyou == true)
                 {
                    document.getElementById("HD").value="1";//for generating docs
			        return true;
                 }
                 else
                 {
                    document.getElementById("HD").value="2";//for Preview
                    return true;			
                 }
             }
             if(val=="2")    
             {
                doyou = confirm("Selected Cases will be changed to waiting status and document ID will be generated? (OK = Yes   Cancel = No)"); 
                if (doyou == true)
                {
                    OpenBondSummary("../Reports/ArrSummary.aspx?Flag=3"); //print all with docid
                    window.location.reload();
                    return false;
                }
                else
                {
                    OpenBondSummary("../Reports/ArrSummary.aspx?Flag=4");   //print all with out docid
                    return false;
                }
             
             }
             if(val=="3")
             {
                if(ValidateforBond()==false)
                {
                    alert("Please Select Row for Print");
		            return false;
                }
                doyou = confirm("Selected Cases will be changed to waiting status and document ID Number will be generated? (OK = Yes   Cancel = No)"); 
                if (doyou == true)
                {
                     //for generating docs
                     document.getElementById("HD").value="1";
                    return true;
                }
                else
                {
                    document.getElementById("HD").value="2";      //for without docs
                    return true;
                }  
             
             }
             if(val=="4")
             {
                doyou = confirm("Selected Cases will be changed to waiting status and document ID Number will be generated? (OK = Yes   Cancel = No)"); 
                if (doyou == true)
                {
                     //for generating docs
                     OpenBondSummary("../Reports/BondSummary.aspx?Flag=3");//PRINTAAL WITH DOCS
                     window.location.reload();
                    return false;
                }
                else
                {
                    OpenBondSummary("../Reports/BondSummary.aspx?Flag=4");     //print all  without docs
                    return false;
                }  
             }
	   }
	    function ValidateforBond()
		{ 
		  
		  var rec_count =document.getElementById("txtRowCount").value; 
		  var Flag;
		  var j=2;
		  if(rec_count==1)
		  { 
		    var grd="dgbond_ctl02_chk";
		    var chkbox=document.getElementById(grd);
		    if(chkbox.checked == true)
            {
                Flag=true; 
            }
            else
              {
                Flag=false;
              }
		  }
		  else
		  {
          for(i=1;i<=rec_count;i++)
          { 
            var grd="dgbond_";
            if(i<9)
            {
                grd+= "ctl0" +j+"_chk";
                j=j+1;
                
            }
            else
            {
            grd+= "ctl"+j+"_chk";
            j=j+1;
            }
            var chkbox=document.getElementById(grd);
            if(chkbox.checked == true)
            { 
                Flag=true;
                break;
                //alert("Please Select the Row");
                //return false;
            }
            else
            { 
                    Flag=false;
                    //return true;
            }
          }
          }
          if(Flag == false)
            {
                
                return false;
            }
            else
            { 
                return true;
            }
           
	    } 
	    
	    function select_deselectAll2(chkVal, idVal) 
		{ 
		        
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,8)=='ASdg$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAlll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		}
		function Show()
		{
		    
		    var tdScan=document.getElementById("tdScan");
		    var tdDoc=document.getElementById("tdDoc");
		  
		    tdScan.style.display='none';
		    tdDoc.style.display='block';
		  
		    document.getElementById("txtDocid").value="";
		    document.getElementById("txtDocid").focus();
		    
		}
		function Check()
		{
		  
		    var txtDocid=document.getElementById("txtDocid").value;
		    if(txtDocid =="")
		    {
		        alert("Please Enter Document ID");
		        document.getElementById("txtDocid").focus();
		        return false;
		    }
		   
		        else if(isNaN(txtDocid)==true)
		        {
		            alert("Please Enter Valid Document ID");
		            document.getElementById("txtDocid").focus();
		            return false
		        }
		        else if(spaceValidation(txtDocid)==false)
		        {
		            alert("Please Enter Valid Document ID");
		            document.getElementById("txtDocid").focus();
		            return false;
		        }
		        else
		        { 
		            if(StartScan()==false)
		            {
		                return false;
		            }
		            else 
		            {
		                
		                return true;
		            }
		        }
		    
		    
		    
		}
		function PopUp(ticketid)
		{
		    var path="PopUp.aspx?ticketid="+ticketid;
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes,width=450,height=200");
		    return false;
		}
		function DocValidate()
		{
		        
		    var txtdocid=document.getElementById("txtDocid").value;
		    if(txtdocid=="")
		    {
		        alert("Please Enter the DocumentID");
		        document.getElementById("txtDocid").focus();
		        return false;
		    }
		    
		        if(isNaN(txtdocid)==true)
		        {
		            alert("Please Enter Valid Document ID");
		            document.getElementById("txtDocid").focus();
		            return false;
		        }
		        if(spaceValidation(txtdocid)==false)
		        {
		            alert("Please Enter Valid Document ID");
		            document.getElementById("txtDocid").focus();
		            return false;
		        }
		   
		    
		    
		}
		 function spaceValidation(str)
        { 
            
           str = (str.replace(/^\W+/,'')).replace(/\W+$/,'');

            if(str == "")
            { 
                return false;
             }
        }
		
    </script>

   
    
     <%=Convert.ToString(Session["objTwain"])%>
    
    
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    
</head>
<body onload="javascript: Hide();" ms_positioning="GridLayout">
    <form id="Form2" method="post" runat="server">
   <!-- <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0"></aspnew:ScriptManager>-->
    
        <table cellpadding="0" cellspacing="0" border="0" align="center" width="780">
            <tr>
                <td style="height: 549px">
                    <table id="TableMain" cellspacing="0" cellpadding="0" align="center" width="780"
                        border="0">
                        <tr>
                            <td>
                                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="9">
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 18px">
                                <asp:DropDownList ID="ddl_WeekDay" runat="server" AutoPostBack="True" CssClass="clsinputcombo"
                                    OnSelectedIndexChanged="ddl_WeekDay_SelectedIndexChanged1">
                                    <asp:ListItem Value="-1">Select a Day</asp:ListItem>
                                    <asp:ListItem Value="999">Arraignment Waiting</asp:ListItem>
                                    <asp:ListItem Value="998">Bond Waiting</asp:ListItem>
                                </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lbl_CurrDateTime" runat="server" Font-Names="Times New Roman" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbl_Message" runat="server" CssClass="Label" Font-Bold="True" ForeColor="Green"
                                    Visible="False">There is currently no clients arraigned for arraignments</asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 9px" width="100%" background="../../images/separator_repeat.gif"
                                colspan="5">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="display:none">
                               
                                    
                                <asp:HyperLink ID="hp" runat="server" NavigateUrl="~/QuickEntryNew/PrintHistory.aspx"
                                    Visible="False">Show Print History</asp:HyperLink><asp:Label ID="lblSep" runat="server"
                                        ForeColor="Blue" Text="|" Visible="False"></asp:Label>&nbsp;<asp:HyperLink ID="HPARRSumm"
                                            runat="server" NavigateUrl="#" Visible="False">Print All</asp:HyperLink>
                                <asp:HyperLink ID="lnk_bsumm" runat="server" NavigateUrl="#" Visible="False">Print ALL</asp:HyperLink>
                                &nbsp;<asp:ImageButton ID="ImageButton2" OnClick="SendMail" runat="server" ImageUrl="../Images/Email.jpg"
                                    Visible="False" />
                                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../Images/print_02.jpg"
                                    Visible="False" OnClick="Imagebutton1_Click" ToolTip="Print"></asp:ImageButton>&nbsp;<asp:ImageButton
                                        ID="img_pdfShort" runat="server" ImageUrl="../Images/pdf.jpg" Visible="False"
                                        OnClick="img_pdf_Click" ToolTip="Print Single" />
                                <asp:ImageButton ID="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg"
                                    OnClick="imgPrint_Ds_to_Printer_Click1" Visible="False" ToolTip="Print" />&nbsp;<asp:ImageButton
                                        ID="img_pdfLong" runat="server" ImageUrl="../Images/pdf.jpg" Visible="False"
                                        OnClick="ImageButton2_Click" ToolTip="Print Single" />
                                <asp:ImageButton ID="img_PdfAll" runat="server" ImageUrl="../Images/pdfall.jpg" OnClick="img_PdfAll_Click"
                                    ToolTip="Print All" Visible="False" />&nbsp;
                                    
                                    <asp:ImageButton ID="ImgCrystalSingle" runat="server" ImageUrl="../Images/Cr.jpg" ToolTip="Print Single" Visible="False" OnClick="ImgCrystalSingle_Click" OnClientClick="return ArraignmentDocketPopUp(1);" />
                                    
                                    <asp:ImageButton ID="ImgCrystalAll" runat="server" ImageUrl="../Images/Cr.jpg" ToolTip="Print All" Visible="False" OnClientClick="return ArraignmentDocketPopUp(2);" />
                                    
                            </td>
                        </tr>
                        <tr>
                            <td id="td_Title" runat="server" align="right" style="display:none">
                            From:
                                <ew:CalendarPopup id="cal_From" runat="server" Width="86px" ImageUrl="../Images/calendar.gif" ControlDisplay="TextBoxImage">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black"  />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black"  />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray"  />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black"  />
                    </ew:CalendarPopup> To:
                                <ew:CalendarPopup id="cal_To" runat="server" Width="86px" ImageUrl="../Images/calendar.gif" ControlDisplay="TextBoxImage">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black"  />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black"  />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray"  />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black"  />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black"  />
                    </ew:CalendarPopup> <asp:CheckBox ID="chk_ShowAllRec" runat="server" Checked="True" Text="Show all" />
                                        <asp:Button ID="btn_SearchRec" runat="server" CssClass="clsbutton" Text="Search"
                                            OnClick="btn_Search_BondWaiting_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px" width="100%" 
                                colspan="5" >
                            </td>
                        </tr>
                        <tr>
                              <td  width="100%" 
                                colspan="5" >
                                <!-- <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                 <ContentTemplate>-->
                                <asp:Label ID="lblmsg" runat="server" ></asp:Label>
                               <!--</ContentTemplate>
                                <Triggers>
                                <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                                </Triggers>
                                </aspnew:UpdatePanel>-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                    <!--<aspnew:UpdatePanel ID="panel1" runat="server" UpdateMode="Always">
                                        <ContentTemplate>-->
                                       <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                    
                                        <td align="left" width="50%" id="tdDoc" style="display:none" runat="server">DOC ID :&nbsp;   <asp:TextBox ID="txtDocid" runat="server"  CssClass="clstextarea"></asp:TextBox>&nbsp;<asp:Button ID="btnScan" runat="server" Visible="False" Text="Scan Now" CssClass="clsbutton" OnClick="btnScan_Click" /><asp:Button ID="btncheck" runat="server"  Text="Check Document ID" CssClass="clsbutton" OnClick="btnCheck_Click"  /><asp:CheckBox id="Adf"  runat="server"  Visible="false" Enabled="false" Checked="true"  Text="Feeder"></asp:CheckBox>
                                                                                
                                        </td>
                                        <td align="right" width="50%" id="tdScan"  runat="server">
                                        <button id="scan" value="Scan" class="clsbutton" onclick="Show();" style="width: 100px"  runat="server">
                                        
                                            Scan</button><asp:HiddenField ID="HDScan" runat=server />
                                        </td>
                                          </tr>
                                          <tr>
                                          <td colspan="2" align="center">
                                            <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label>
                                          </td>
                                          </tr>
                                          <tr>
                                            <td class="clssubhead" valign="middle" align="center" id="tdplzwait" style="display:none" colspan="2">
                                            <img src="../Images/plzwait.gif" />
                                                Please wait while scanning is in process..
                                            </td>
                                          </tr>
                                </table>
                                     <!--  </ContentTemplate><Triggers>
                                            <aspnew:AsyncPostBackTrigger ControlID="btncheck" EventName="Click" />
                                            <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                                        </Triggers>
                                        </aspnew:UpdatePanel>-->
                                  
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 9px" width="100%" background="../../images/separator_repeat.gif"
                                colspan="5" height="9">
                            </td>
                        </tr>
                        <tr id="tr_NotOnSystem" runat="server" style="display:none">
                            <td width="100%">
                            <table width="100%">
                            <tr>
                            <td class="clslabel">
                            
                            </td>
                            </tr>
                            </table>
                            <table width="100%">
                            <tr>
                            <td class="clslabel">
                                Flag: <asp:DropDownList ID="ddl_Flags" runat="server" CssClass="clsinputcombo" AutoPostBack="true" OnSelectedIndexChanged="ddl_Flags_OnSelectedIndexChanged">
                                </asp:DropDownList> </td>
                            </tr>
                            </table>
                                <asp:DataGrid ID="dg_NotOnSystem" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                    BorderStyle="Solid" Width="100%" OnItemDataBound="ASdg_ItemDataBound" OnSortCommand="dg_NotOnSystem_SortCommand"
                                    AllowSorting="True">
                                    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="ASTIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn Visible="false">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk1" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="CheckAlll" runat="server" onclick="return select_deselectAll2(this.checked, this.id);"
                                                    Checked="false" />
                                            </HeaderTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CAUSE NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="AScauseno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="TICKET NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Asticketno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="ASlastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="FIRST NAME">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="ASfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DL">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDLNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DOB">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="ASDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="ASStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="ASCourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Time">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td id="longshot" runat="server" style="display: none">
                                <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                    border="0">
                                    <tr>
                                        <td style="height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="uaagrd" style="height: 9px" width="100%" colspan="5" height="9">
                                            <asp:DataGrid ID="dg_Report" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                                                BorderColor="Black" BorderStyle="Solid" Width="100%">
                                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="No:">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_sno" runat="server">0</asp:Label>
                                                            <asp:Label ID="lbl_Ticketid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Flags">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Case #">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlnkCaseNo" runat="server" Font-Bold="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                                                Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>'
                                                                Font-Underline="True">
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Seq" Visible="False">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" ></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Last Name">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="First Name">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="DL">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="DOB">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="MID">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MID") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Set Date">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_arrdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="jt" style="height: 14px" >
                                            <strong>Judge Trials</strong></td>
                                    </tr>
                                    <tr>
                                        <td id="a1" style="height: 14px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="jtgrd" style="height: 9px" width="100%" colspan="5" height="9">
                                            <asp:DataGrid ID="dg_ReportJT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                                                BorderColor="Black" BorderStyle="Solid" Width="100%">
                                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="No:">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_sno1" runat="server">0</asp:Label>
                                                            <asp:Label ID="lbl_Ticketid1" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Flags">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label141" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Case #">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlnkCaseNo1" runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                                                Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>'
                                                                Font-Underline="True">
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Seq" Visible="false">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label151" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Last Name">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label161" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="First Name">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label171" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="DL">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label181" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="DOB">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Court Information">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_CCDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_CCNum" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Officer Day">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_OfficerDay" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerDay") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                            <table width="100%" id="tbl_PR_Report" runat="server">
                                             <tr>
                                            <td style="height: 14px" >
                                                <strong>Probation Request Report</strong></td>
                                            </tr><tr>
                                        <td id="Td4" style="height: 14px">
                                        </td>
                                    </tr><tr>
                                        <td>
                                        <asp:DataGrid ID="dg_ProbReq" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                    BorderStyle="Solid" Width="100%" OnItemDataBound="dg_ProbReq_ItemDataBound" >
                                    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TicketID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Last Name" SortExpression="lastname">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_LastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="First Name">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_FirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Cause No">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lbl_CauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Ticket No">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TicketNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Violation Desc">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_VioDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Crt Date">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Crt Time">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:t}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Room">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtRoom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtRoom") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Flags">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_FlagsBF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lbl_PR_Flag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PRFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lbl_POA_Flag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.POAFlag") %>'>
                                                </asp:Label>    
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Trial Comments">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman" Font-Size="XX-Small"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TrialComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                        </td>
                                    </tr>
                                            </table>
                                            
                                            </td>
                                    </tr>
                                   
                                    
                                    
                                    <tr>
                                        <td id="a2" style="height: 16px">
                                        </td>
                                    </tr>
                                    <tr id="trial" runat="server">
                                        <td id="ost" style="height: 16px">
                                            <strong>Outside Trials</strong></td>
                                    </tr>
                                    <tr>
                                        <td id="a3" style="height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="ostgrd" style="height: 9px" width="100%" colspan="5" height="9">
                                            <asp:DataGrid ID="dg_ReportOT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                                                BorderColor="Black" BorderStyle="Solid" Width="100%">
                                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="No:">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_sno2" runat="server">0</asp:Label>
                                                            <asp:Label ID="lbl_Ticketid2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Flags">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label142" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Case #">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlnkCaseNo2" runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                                                Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>'
                                                                Font-Underline="True">
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Seq" Visible="false">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label152" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Last Name">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label162" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="First Name">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label172" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="DL">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label182" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="DOB">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Court Information">
                                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_CCDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="lbl_CCNum" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" style="display: none" id="snapshot" runat="server">
                    <table id="Table1" cellspacing="0" cellpadding="0" width="950" align="left" border="0">
                        <tr>
                            <td style="height: 21px">
                                <strong><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MONDAY</font></strong>
                            </td>
                            <td style="height: 21px">
                                <strong><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TUESDAY</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WEDNESDAY</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THURSDAY</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FRIDAY</font></strong>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="20%">
                                <asp:DataGrid ID="dg_Mon" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    BorderStyle="Solid" BorderColor="Black" Width="80%" EnableViewState="False">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketidM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Com">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrialM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialDateTime") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblBondM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTrialCM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialComments") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblPlusM" runat="server"></asp:Label>
                                                <asp:Label ID="lblPreM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_pretrialstatus") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Arr">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center">
                                            </ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_ViolationDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Name">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlnkNameM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_Name") %>'
                                                    Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.MON_TicketID_PK"),"&amp;search=0") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top" width="20%">
                                <asp:DataGrid ID="dg_Tue" runat="server" AutoGenerateColumns="False" BorderStyle="Solid"
                                    BorderColor="Black" Width="80%" EnableViewState="False">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketidT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Com">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrialT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TrialDateTime") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblBondT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTrialCT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TrialComments") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblPlusT" runat="server"></asp:Label>
                                                <asp:Label ID="lblPreT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_pretrialstatus") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Arr">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center">
                                            </ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_ViolationDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Name">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlnkNameT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_Name") %>'
                                                    Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK"),"&amp;search=0") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top" width="20%">
                                <asp:DataGrid ID="dg_Wed" runat="server" AutoGenerateColumns="False" BorderStyle="Solid"
                                    BorderColor="Black" Width="80%" EnableViewState="False">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketidW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Com">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrialW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialDateTime") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblBondW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTrialCW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialComments") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblPlusW" runat="server"></asp:Label>
                                                <asp:Label ID="lblPreW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_pretrialstatus") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Arr">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center">
                                            </ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_ViolationDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Name">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlnkNameW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_Name") %>'
                                                    Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.WED_TicketID_PK"),"&amp;search=0") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top" width="20%">
                                <asp:DataGrid ID="dg_Thu" runat="server" AutoGenerateColumns="False" BorderStyle="Solid"
                                    BorderColor="Black" Width="80%" EnableViewState="False">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketidTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Com">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrialTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialDateTime") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblBondTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTrialCTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialComments") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblPlusTh" runat="server"></asp:Label>
                                                <asp:Label ID="lblPreTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_pretrialstatus") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Arr">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center">
                                            </ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_ViolationDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Name">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlnkNameTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_Name") %>'
                                                    Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.THR_TicketID_PK"),"&amp;search=0") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top" width="20%">
                                <asp:DataGrid ID="dg_Fri" runat="server" AutoGenerateColumns="False" BorderStyle="Solid"
                                    BorderColor="Black" Width="80%" EnableViewState="False">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketidF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Com">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrialF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TrialDateTime") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblBondF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_BondFlag") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTrialCF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TrialComments") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblPlusF" runat="server"></asp:Label>
                                                <asp:Label ID="lblPreF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_pretrialstatus") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Arr">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman" HorizontalAlign="Center">
                                            </ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_ViolationDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Name">
                                            <HeaderStyle Font-Size="XX-Small" Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlnkNameF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_Name") %>'
                                                    Font-Underline="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK"),"&amp;search=0") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td>
                                <strong><font size="2">Comments:</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">Comments:</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">Comments:</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">Comments:</font></strong>
                            </td>
                            <td>
                                <strong><font size="2">Comments:</font></strong>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dg_MonC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
                                    ShowHeader="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNo1M" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketid1M" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCommM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MON_TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top">
                                <asp:DataGrid ID="dg_TueC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
                                    ShowHeader="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNo1T" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketid1T" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TUE_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCommT" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tue_TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top">
                                <asp:DataGrid ID="dg_WedC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
                                    ShowHeader="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNo1W" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketid1W" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCommW" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WED_TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top">
                                <asp:DataGrid ID="dg_ThuC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
                                    ShowHeader="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNo1Th" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketid1Th" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCommTh" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.THR_TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                            <td valign="top">
                                <asp:DataGrid ID="dg_FriC" runat="server" AutoGenerateColumns="False" EnableViewState="False"
                                    ShowHeader="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNo1F" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SEQ_CustNumber") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblTicketid1F" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FRI_TicketID_PK") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle Font-Size="XX-Small" Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCommF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fri_TrialComments") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" style="display: block" id="BforBond" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td id="bond">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <strong>Bond Settings</strong></td>
                                        <td align="right">
                                            <asp:Button ID="btnBoncSumm" runat="server" Text="Print Selected" OnClick="btnBondSumm_Click"
                                                CssClass="clsButton"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td id="bondTD">
                                <asp:DataGrid ID="dgbond" runat="server" AutoGenerateColumns="False"
                                    BorderColor="Black" BorderStyle="Solid" Width="100%" OnItemDataBound="dgbond_ItemDataBound" AllowSorting="True" OnSortCommand="dgbond_SortCommand">
                                    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="btvids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="bticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="serialNo" runat="server"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="CheckAll" runat="server" onclick="return select_deselectAll(this.checked, this.id);"
                                                    Checked="false" />
                                            </HeaderTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CAUSE NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="bCauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.causeno") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="FLAG">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="TICKET NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bTicketNO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="blastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="FIRST NAME">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DL">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bDL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DOB">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="X-REF">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblcrtdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="BOND D">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblbonddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bonddate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="B Time" SortExpression="Time">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                        
                                         <ItemTemplate>
                                            <asp:ImageButton ID="ImgBond" runat="server" ImageUrl="~/Images/preview.gif" />
                                         </ItemTemplate>
                                        </asp:TemplateColumn>
                                      
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td height="15">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td id="td_BondWaiting" runat="server" style="display:block">
                    <table>
                    <tr>
                   
                    <td align="left" style="display:none">
                    <asp:Button ID="btn_PrintReport_Bond" runat="server" CssClass="clsbutton" OnClick="btn_PrintReport_Bond_Click"
                                    OnClientClick="return ShowBndWaitingPopup()" Text="Print Summary Report" Visible="False" />
                    </td>
                     <td align="right" style="display:none">
                         <asp:LinkButton ID="PrintAllBonds" runat="server" Visible="False" OnClientClick="return PrintAllBondValidate();" OnClick="PrintAllBonds_OnClick">Print All Bonds</asp:LinkButton>
                    </td>
                    </tr>
                        
                        <tr>
                            <td colspan="2">
                                <asp:DataGrid ID="dg_BondWaiting" runat="server" AutoGenerateColumns="False"
                                    BorderColor="Black" BorderStyle="Solid" Width="100%" OnItemDataBound="Bondwaiting_ItemDataBound" AllowSorting="True" OnSortCommand="dgbondwaiting_sortcommand">
                                    <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                    <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="btvids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="bticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="serialNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="CheckAll" runat="server" onclick="return select_deselectAll_BondWait(this.checked, this.id);"
                                                    Checked="false" />
                                            </HeaderTemplate>
                                        </asp:TemplateColumn>                                        
                                        <asp:TemplateColumn HeaderText="CAUSE NO" SortExpression="causeno">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="bCauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.causeno") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="FLAG" SortExpression="BondFlag">
                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label ID="lbl_bond" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="TICKET NO" SortExpression="TICKETNO">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bTicketNO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="blastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="FIRST NAME" SortExpression="firstname">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DL" SortExpression="dlnumber">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bDL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DOB" SortExpression="DOB">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="X-REF" SortExpression="midnum">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status" SortExpression="status">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="bstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="ID" SortExpression="doccount">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True" Width="20%"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <sup><asp:Label ID="lblSc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doccount") %>' Font-Size="8">
                                                </asp:Label></sup><asp:ImageButton ID="ImgDoc1" style="display: inline" runat="server" ImageUrl="~/Images/folder.gif" Height="20px" />
                                                <asp:Label ID="lblshowscanB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.showscan") %>'
                                                            Visible="False"></asp:Label>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Court Date" SortExpression="CourtDate">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="B DATE" SortExpression="BondDate">
                                        <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                            <asp:Label ID="lbl_BondDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondDate","{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="WAIT D" SortExpression="WaitingDate">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_WaitTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WaitingDate","{0:d}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="W Time" SortExpression="Time">
                                            <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="display: none" id="AforArraignment" runat="server" width="780">
                    <table border="0" cellpadding="0" cellspacing="0" width="780">
                        <tr>
                            <td id="Td1">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <strong>Arraignment Settings</strong>
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btnARR" runat="server" Text="Print Selected" CssClass="clsButton"
                                                OnClick="btnARR_Click"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </table>
                        </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td id="Td2" width="780" runat="server" style="height: 165px">
                    <asp:DataGrid ID="ASdg" runat="server" AutoGenerateColumns="False"
                        BorderColor="Black" BorderStyle="Solid" Width="780px" OnItemDataBound="ASdg_ItemDataBound" OnSortCommand="ASdg_SortCommand" AllowSorting="True">
                        <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                        <HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="ASTVIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="ASTIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk1" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="CheckAlll" runat="server" onclick="return select_deselectAll2(this.checked, this.id);"
                                        Checked="false" />
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CAUSE NO">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="AScauseno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLAG">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblArrFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLAGID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TICKET NO">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Asticketno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="ASlastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FIRST NAME">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="ASfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DL">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDLNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DOB">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="ASDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="X-REF">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="ASmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Status">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="ASStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="ASCourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Time">
                                <HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
                                <ItemStyle Font-Names="Times New Roman"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td height="15">
                </td>
            </tr>
        
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="width: 946px">
                            <table id="tbl_ArrWaiting" width="100%" style="display: none" runat="server">
                                <tr>
                                    <td align="right" colspan="5" height="9" style="height: 9px" width="100%">
                                        &nbsp;</td>
                                </tr>
                                 <tr>
                                    <td style="display:none">
                                        <asp:Button ID="btn_PrintReport" runat="server" CssClass="clsbutton" OnClick="btn_PrintReport_Click"
                                            OnClientClick="return ShowArrWaitingPopup()" Text="Print Summary Report" Visible="false" />
                                        <asp:HiddenField ID="hf_ArrWaiting_GenIDs" runat="server" Value="0" />
                                        <asp:HiddenField ID="hf_BondWaiting_GenIDs" runat="server" Value="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dg_AWR" runat="server" AutoGenerateColumns="False" BorderColor="Black"
                                            BorderStyle="Solid" OnItemDataBound="AWRWaiting_ItemDataBound"
                                            Width="780px" AllowSorting="True" OnSortCommand="dg_AWR_SortCommand">
                                            <ItemStyle Font-Names="Times New Roman" />
                                            <HeaderStyle BackColor="#FFCC00" ForeColor="Black" />
                                            <Columns>                                                
                                                <asp:TemplateColumn HeaderText="ticketviolationid" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASTVIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="ticketid" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASTIDS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="NO">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblSNo" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                 <HeaderTemplate>
                                                <asp:CheckBox ID="CheckAll" runat="server" onclick="return select_deselectAll_ArrWait(this.checked, this.id);"
                                                    Checked="false" />
                                            </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chk_Select" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="CAUSE NO" SortExpression="CAUSENO">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="AScauseno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENO") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="FLAG" SortExpression="FLAGID">
                                                <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArrWFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLAGID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="TICKET NO" SortExpression="TICKETNO">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Asticketno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNO") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="LAST NAME" SortExpression="lastname">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASlastname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="FIRST NAME" SortExpression="firstname">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASfirstname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                   <asp:TemplateColumn HeaderText="DL" SortExpression="dlnumber">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_DL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="DOB" SortExpression="DOB">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="X-REF" SortExpression="midnum">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASmidnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Status" SortExpression="status">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="ID" SortExpression="doccount">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                                <sup><asp:Label ID="lblScripts" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doccount") %>' Font-Size="8"></asp:Label></sup><asp:ImageButton ID="ImgDoc" style="display: inline" runat="server" ImageUrl="~/Images/folder.gif" />
                                                                <asp:Label ID="lblshowscan" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.showscan") %>'
                                                            Visible="False"></asp:Label>                                                            
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="CRT D" SortExpression="COURTDATE">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="ASCourtDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:d}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="WAIT D" SortExpression="WaitingDate">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_WaitTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WaitingDate","{0:d}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Time" SortExpression="Time">
                                                    <HeaderStyle Font-Bold="True" Font-Names="Times New Roman" />
                                                    <ItemStyle Font-Names="Times New Roman" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="btime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                               
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../images/separator_repeat.gif" height="9" style="width: 946px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 946px">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: none; width: 946px;">
                            <asp:TextBox ID="txtRowCount" runat="server"></asp:TextBox><asp:TextBox ID="txtARRSummCount"
                                runat="server"></asp:TextBox><asp:TextBox ID="txtbID" runat="server"></asp:TextBox><asp:textbox id="txtempid" runat="server"></asp:textbox><asp:textbox id="txtsessionid" runat="server"></asp:textbox><asp:textbox id="txtScan" runat="server">0</asp:textbox><asp:textbox id="textButton" runat="server">0</asp:textbox></td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        <asp:HiddenField ID="HD" runat="server" />
        <asp:HiddenField ID="HF_ArraignmentDocket" runat="server" />
        <asp:HiddenField ID="hf_Count" runat="server" Value="0" />
       
        
    </form>
     <SCRIPT language="JavaScript">
function StartScan()
{
   
	
   
    var sid= document.getElementById("txtsessionid").value;
	var eid= document.getElementById("txtempid").value;
	var path='<%=ViewState["vNTPATHScanTemp"]%>';
	var Type='network';		
	var AutoFeed=1;
	
	
	
	var sel=OZTwain1.SelectScanner();
	if(sel=="Success")
	{
	     if (document.Form2.Adf.checked == true)
	     {
	        AutoFeed=-1;
	     }
	    document.getElementById("tdplzwait").style.display="block";
	    OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
	    //document.getElementById("ddl_WeekDay").value="-1";
	    /*if (document.Form2.Adf.checked == false)
	    {
	        var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		    if(x)
		    {
			    StartScan(); 
			   
		    }	
		    else
		    {
		       document.Form2.Adf.checked == true
        	    
		    }	
	    }*/
    }
    else if(sel=="Cancel")
    {
        alert("Operation canceled by user!");
        return false;
    }
    else 
    {
        alert("Scanner not installed.");
	    return false;
    }
    
}
		</SCRIPT>
</body>
</html>

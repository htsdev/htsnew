using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Activities
{
	/// <summary>
	/// Summary description for CallBackNotes.
	/// </summary>
	public partial class PartialPayNotes : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.Label lbl_name;
		protected System.Web.UI.WebControls.Label lbl_appdate;
		protected System.Web.UI.WebControls.Button btn_submit;
		clsENationWebComponents clsdb = new clsENationWebComponents();
		clsSession cSession	 =new clsSession();
		clsLogger clog = new clsLogger();
		int ticketid;
		int empid;
		
        protected System.Web.UI.WebControls.Label lbl_oweamount;		
		protected System.Web.UI.WebControls.Label lbl_telephone1;
		protected System.Web.UI.WebControls.Label lbl_telephone3;
		protected System.Web.UI.WebControls.Label lbl_telephone2;
		protected eWorld.UI.CalendarPopup PayDueDate;
		int RecID;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
            //Waqas 5057 03/17/2009 Checking employee info in session               
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
		//	empid=3992;
			//Adding Attribute For Save button
			btn_submit.Attributes.Add("onclick","return Validate();");
			if(!IsPostBack)
			{
				ticketid =Convert.ToInt32(Request.QueryString["casenumber"]);			
				RecID=Convert.ToInt32(Request.QueryString["RecID"]);
				empid=Convert.ToInt32(cSession.GetCookie("sEmpID",this.Request));
                ViewState["vTicketID"] = ticketid;
                ViewState["vRecID"] = RecID;
                ViewState["vEmpID"] = empid;

				
				GetInfo();
                //Nasir 6098 08/21/2009 general comment initialize overload method
                WCC_GeneralComments.Initialize(ticketid, empid, 1, "Label", "clsinputadministration", "clsbutton","Label");
                WCC_GeneralComments.btn_AddComments.Visible = false;
                WCC_GeneralComments.TextBox_Visible = false;
			}

		}
		//GetInfo Based on Schedule ID, 
		private void GetInfo()
		{
			try
			{
				DataRow[] dr;
				//filtering record on ScheduleID
				DataSet ds_notes=(DataSet) Session["DS"];
				ticketid =Convert.ToInt32(Request.QueryString["casenumber"]);			
				string filterstr= "scheduleid="+RecID + " and TicketID_PK="+ticketid ;
				dr=ds_notes.Tables[0].Select(filterstr);
				if (dr.Length>0)
				{					
					lbl_name.Text=dr[0]["clientname"].ToString();					
					lbl_telephone1.Text = dr[0]["contact1"].ToString();				
					lbl_telephone2.Text = dr[0]["contact2"].ToString();	
					lbl_telephone3.Text = dr[0]["contact3"].ToString();	
					lbl_appdate.Text    = dr[0]["courtdate"].ToString();	
					//lbl_appdate.Text    = lbl_appdate.Text.Substring(0,lbl_appdate.Text.IndexOf(" "));
					lbl_oweamount.Text  = dr[0]["owes"].ToString();					
					lbl_oweamount.Text=lbl_oweamount.Text.Substring(0,lbl_oweamount.Text.IndexOf("."));
					string payduedate   = dr[0]["paymentdate"].ToString();							
					//payduedate=payduedate.Substring(0,payduedate.IndexOf(" "));
					if (payduedate!="1/1/1900")
					{
						PayDueDate.SelectedDate=Convert.ToDateTime(dr[0]["paymentdate"].ToString());	
					}			
								
				}			
				
				//Eliminating ( in telephone number
				if (lbl_telephone1.Text.StartsWith("("))
				{
					lbl_telephone1.Visible=false;
				}
				if (lbl_telephone2.Text.StartsWith("("))
				{
					lbl_telephone2.Visible=false;
				}
				if (lbl_telephone3.Text.StartsWith("("))
				{
					lbl_telephone3.Visible=false;
				}				
				
			}
			catch(Exception ex)
			{
				string a = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		//In order to save the information
		private void btn_submit_Click(object sender, System.EventArgs e)
		{		
			try
			{
                // Fahad 2804 ( 2-8-08 )
                // Remove Text box from popup & implement comment control on Partial Pay Notes
				string [] key1 =    {"@ScheduleID","@ticketid","@empid","@DueDate","@amount"} ;
                object[] value1 =  { Convert.ToInt32(ViewState["vRecID"]),   Convert.ToInt32(ViewState["vTicketID"]), Convert.ToInt32(ViewState["vEmpID"]), PayDueDate.SelectedDate, Convert.ToDouble(lbl_oweamount.Text) };
				clsdb.ExecuteSP("USP_HTS_UPDATE_PartialPayNotes",key1,value1);		
				Session["new"]="in";
                if (WCC_GeneralComments.TextBox_Text.Length > 0)
                {
                    WCC_GeneralComments.AddComments();
                }
				//In order to close the popup
				HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");	
		        //End Fahad
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		
		}
	}
}

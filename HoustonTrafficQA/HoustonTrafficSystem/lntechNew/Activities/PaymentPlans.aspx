<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentPlans.aspx.cs" Inherits="HTP.Activities.PaymentPlans"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Due Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript ">
     function ValidateCommentControls()
		{ 
            
	      //Comments Textboxes
		  var dateLenght = 0;
		  var newLenght = 0;
		  
          newLenght = document.getElementById("wctl_GeneralComments_txt_comments").value.length
		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

		  if (document.getElementById("wctl_GeneralComments_txt_comments").value.length + document.getElementById("wctl_GeneralComments_lbl_comments").innerText.length > (2000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
		  {
		    alert("Sorry You cannot type in more than 2000 characters in General comments box")
			return false;		  
		  }
    }
    
    function Showfollowup()
    {
        if (document.getElementById("lbl_Paymentfollowupdate") != "block")
        {        
            document.getElementById("lbl_Paymentfollowupdate").style.display="none";
            document.getElementById("cal_followup").style.display="block";            
        }
         
    }
    
    function CheckDate(seldate, tbID)
    {
          var weekday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
          var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth()+1)+"/"+Date.parseInvariant(seldate).getDate() + "/"+Date.parseInvariant(seldate).getFullYear(),"MM/dd/yyyy");                
          newseldate = Date.parseInvariant(newseldatestr,"MM/dd/yyyy");
          // Noufil 5802 06/03/2009 Allow followupdate to set on saturday
          if (weekday[newseldate.getDay()] == "Sunday") 
          {
              alert("Please select any business day");
              return false;
          }
    }
    
    function Validate()
    {
        var followupdate = document.getElementById("cal_followup").value;
        var weekday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
          //var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth()+1)+"/"+Date.parseInvariant(seldate).getDate() + "/"+Date.parseInvariant(seldate).getFullYear(),"MM/dd/yyyy");                
          newseldate = Date.parseInvariant(followupdate,"MM/dd/yyyy");
          // Noufil 5802 06/03/2009 Allow followupdate to set on saturday
          if (weekday[newseldate.getDay()] == "Sunday" ) 
          {
              alert("Please select any business day");
              return false;
          }
    }
    
    function ShowFollowupdate()
    {    
        if (document.getElementById("td_followcal").style.display == "block")
        {
            document.getElementById("td_followcal").style.display="none";
            document.getElementById("td_followlabel").style.display="block";
             document.getElementById("hf_showhide").value ="0";
        }
        else
        {
            document.getElementById("td_followcal").style.display="block";
            document.getElementById("td_followlabel").style.display="none";
            document.getElementById("hf_showhide").value ="1";
            
        }
        return false;
    }
    
    function cc()
    {
        document.getElementById("td1").style.display="block";
        return false;
    }
    </script>

    <script language="JavaScript" type="text/javascript">

     var Page;
     var postBackElement;
     
     function pageLoad()
     {
          Page = Sys.WebForms.PageRequestManager.getInstance(); 
          Page.add_beginRequest(OnBeginRequest);
          Page.add_endRequest(endRequest);
     }

    function OnBeginRequest(sender, args)
    {   
	    var IpopTop = (document.body.clientHeight - document.getElementById("IMGDIV").offsetHeight) / 2;
        var IpopLeft = (document.body.clientWidth - document.getElementById("IMGDIV").offsetWidth) / 2;            
        document.getElementById("IMGDIV").style.left=IpopLeft + document.body.scrollLeft;
        document.getElementById("IMGDIV").style.top=IpopTop + document.body.scrollTop;            
        document.getElementById("IMGDIV").style.display = "block";
        $get("IMGDIV").style.display="";
    } 

    function endRequest(sender, args)
    {
            
            $get("IMGDIV").style.display="none";
            
            //Nasir 6049 07/16/2009 move down when client select
            var val=sender._postBackSettings.panelID;                                    
            if(val.indexOf("lnk_clientname")!=-1)
            {
                var el=$get("lbl_clientname");
                el.scrollIntoView(true);
            }
            
    }
    //Nasir 6098 08/22/20009 hide iscomplaint check box
     function HideCheckBox()
        {
                document.getElementById("wctl_GeneralComments_chk_Complaint").style.display="none";
                document.getElementById("wctl_GeneralComments_lbl_Complaint").style.display="none";
        }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <div id="IMGDIV" style="display: none; height: 60px; width: 200px; position: absolute;
        vertical-align: middle; background-color: White; z-index: 40; margin-top: auto;
        margin-left: auto; border: 0; border-top: black thin solid; border-left: black thin solid;
        border-bottom: black thin solid; border-right: black thin solid;">
        <%--top: 90%; left: 45%;--%>
        <table id="Table1" width="100%">
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <img alt="" src="../images/plzwait.gif" />&nbsp;
                    <asp:Label ID="lbl1" runat="server" CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <aspnew:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="800px">
                    <tr>
                        <td style="width: 827px; height: 14px">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Partial Pays&nbsp; </font></STRONG>
									</td>
								</tr>-->
                                <tr>
                                    <td background="../../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="clssubhead">
                                                                Court Type :&nbsp;&nbsp;
                                                                <asp:DropDownList ID="ddlCourtLocation" runat="server" CssClass="clsInputCombo" Width="120px">
                                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                                    <asp:ListItem Value="1">Traffic</asp:ListItem>
                                                                    <asp:ListItem Value="2">Criminal</asp:ListItem>
                                                                    <asp:ListItem Value="4">Family Law</asp:ListItem>
                                                                    <asp:ListItem Value="3">Civil</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="clssubhead">
                                                                Criteria :&nbsp;&nbsp;
                                                                <asp:DropDownList ID="ddl_criteria" runat="server" CssClass="clsInputCombo" Width="140px">
                                                                    <asp:ListItem Value="1">Payment Reminder Calls</asp:ListItem>
                                                                    <asp:ListItem Value="4">Past Dues Calls</asp:ListItem>
                                                                    <asp:ListItem Value="2">Payment Defaults</asp:ListItem>
                                                                    <asp:ListItem Value="3">Waived Payments</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="clssubhead">
                                                                Start Date :&nbsp;&nbsp;
                                                                <ew:CalendarPopup ID="sdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" SelectedDate="2006-05-26"
                                                                    ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                                    Width="65px">
                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>
                                                            </td>
                                                            <td class="clssubhead">
                                                                End Date :&nbsp;&nbsp;
                                                                <ew:CalendarPopup ID="edate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" SelectedDate="2006-05-26"
                                                                    ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                                    Width="65px">
                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>
                                                            </td>
                                                            <td class="clssubhead">
                                                                <asp:CheckBox ID="chk_pplan" runat="server" CssClass="clssubhead" Checked="true"
                                                                    Text="Show All" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="clssubhead" style="width: 100%" align="right">
                                                                <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSubmit_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    <%--<asp:DropDownList ID="ddl_Pageno" runat="server" CssClass="clsInputCombo" Visible="False"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddl_Pageno_SelectedIndexChanged">
                                                </asp:DropDownList>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table id="TblGrid" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                    <asp:GridView ID="gv_paymentplan" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" AllowPaging="True" AllowSorting="True" PageSize="30"
                                                        OnPageIndexChanged="gv_paymentplan_PageIndexChanged" OnSorting="gv_paymentplan_Sorting"
                                                        OnPageIndexChanging="gv_paymentplan_PageIndexChanging" OnRowCommand="gv_paymentplan_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.no">
                                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" VerticalAlign="Top"
                                                                    Width="3%" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlnk_Sno" runat="server" Text='<%# Eval("sno") %>' NavigateUrl='<%# "../clientinfo/newpaymentinfo.aspx?casenumber=" + Eval("TicketID_PK")+"&search=0" %>'></asp:HyperLink>
                                                                    <asp:Label ID="lbl_ticketid" runat="server" CssClass="label" Text='<%# Eval("TicketID_PK") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="ClientName">
                                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" VerticalAlign="Top"
                                                                    Width="20%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnk_clientname" runat="server" CommandName="paymentdetail" Text='<%# Eval("ClientName") %>'
                                                                        CommandArgument='<%# Eval("TicketID_PK") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Crt</u>" SortExpression="crt">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                                <ItemStyle VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt" Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCrt" runat="server" Text='<%# Eval("crt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Crt Type</u>" SortExpression="CrtType">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                                <ItemStyle VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt" Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCrtType" runat="server" Text='<%# Eval("CrtType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Crt Date</u>" SortExpression="CourtDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                                                <ItemStyle VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt" Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_contactdate" runat="server" Text='<%# Eval("CourtDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Total Fee</u>" SortExpression="TotalFeeCharged">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFee" runat="server" Text='<%# Eval("TotalFeeCharged","{0:C0}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Amt Due</u>" SortExpression="Owes">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblowes" runat="server" Text='<%# Eval("Owes", "{0:C0}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Payment Due</u>" SortExpression="PaymentDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPayplandate" runat="server" Text='<%# Eval("PaymentDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Follow up</u>" SortExpression="PaymentFollowUpDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFollowupdate" runat="server" Text='<%# Eval("PaymentFollowUpDate","{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField SortExpression="Pastdays">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPastdays" runat="server" Text='<%# Eval("Pastdays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" style="width: 100%">
                                        <a name="client"></a>
                                        <table runat="server" id="tbl_paymentdetail" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11" width="100%">
                                                    </td>
                                                </tr>
                                                <tr class="clsLeftPaddingTable">
                                                    <td height="11px" colspan="7" align="right" style="width: 800px">
                                                        <asp:LinkButton ID="lnkbtn_First" runat="server" Text=" << First Record " OnClick="lnkbtn_First_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkbtn_Previous" runat="server" Text=" < Previous " OnClick="lnkbtn_Previous_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkbtn_Next" runat="server" Text="Next > " OnClick="lnkbtn_Next_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkbtn_Last" runat="server" Text=" Last Record >>" OnClick="lnkbtn_Last_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="clsLeftPaddingTable">
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td height="11px" colspan="7" style="font-family: Tahoma; font-size: 18px;">
                                                                    <asp:Label ID="lbl_clientname" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td style="width: 10px">
                                                                </td>
                                                                <td valign="middle" align="right">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 33%; border: 1; border-color: Black; border-right-width: thin"
                                                                                align="left">
                                                                                <asp:Label ID="lblContact1" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 33%; border: 1; border-color: Black;" align="left">
                                                                                <asp:Label ID="lblContact2" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 33%; border: 1; border-color: Black;" align="left">
                                                                                <asp:Label ID="lblContact3" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                                        <table>
                                                            <tr>
                                                                <td align="right" class="clssubhead">
                                                                    Payment Details
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <asp:GridView ID="GV_paymentdetail" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            CssClass="clsLeftPaddingTable">
                                                            <Columns>
                                                                <asp:BoundField HeaderText="Payment Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="15%" DataField="paymentdate" DataFormatString="{0:MM/dd/yyyy}"
                                                                    ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Rep" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="5%" DataField="rep" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Amount" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="10%" DataField="amount" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Type" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="25%" DataField="types" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Response" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="30%" DataField="response" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("TicketID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" height="11">
                                                    </td>
                                                </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table runat="server" id="tbl_paymplan" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="800px">
                                                        <tr>
                                                            <td align="left" valign="middle" class="clssubhead" style="width: 20%">
                                                                &nbsp;Payment Plans
                                                            </td>
                                                            <td style="width: 80%;" align="center">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="clssubhead" align="center" style="width: 21%">
                                                                            Owes$:&nbsp;<asp:Label ID="lbl_owes" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="center" class="clssubhead" style="width: 21%">
                                                                            Plan$:&nbsp;<asp:Label ID="lbl_plan" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="center" class="clssubhead" style="width: 24%">
                                                                            Difference$:&nbsp;<asp:Label ID="lbl_defrence" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="right" class="clssubhead" style="width: 15%">
                                                                            Follow-Up Date :
                                                                        </td>
                                                                        <td align="left" style="width: 15%; display: block;" valign="middle" id="td_followlabel"
                                                                            runat="server">
                                                                            <asp:Label ID="lbl_Paymentfollowupdate" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td id="td_followcal" runat="server" style="display: none; width: 15%" align="left">
                                                                            <ew:CalendarPopup ID="cal_followup" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                                                ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                                ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                                                ToolTip="Select Follow-Up Date" UpperBoundDate="12/31/9999 23:59:00" Width="65px"
                                                                                JavascriptOnChangeFunction="CheckDate">
                                                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                            </ew:CalendarPopup>
                                                                        </td>
                                                                        <td style="width: 4%" align="left">
                                                                            <asp:ImageButton ID="ImgBtn_Followup" runat="server" ImageUrl="~/Images/Rightarrow.gif"
                                                                                ToolTip="Payment Follow-Up Date" OnClientClick="return ShowFollowupdate();" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <asp:GridView ID="GV_paymplan" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" OnRowDataBound="GV_paymplan_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Amount($)">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="33%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_amount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Amount") %>'
                                                                        CssClass="clsLeftPaddingTable"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Payment Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="33%" DataField="paymentdate" DataFormatString="{0:MM/dd/yyyy}"
                                                                ItemStyle-CssClass="clsLeftPaddingTable" />
                                                            <asp:BoundField HeaderText="Days Left" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="33%" DataField="daysleft" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                            <%--<asp:TemplateField HeaderText="Payment Date">
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="33%" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_rep" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"paymentdate","{0:d}") %>'
                                                                    CssClass="clsLeftPaddingTable"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                            <%--<asp:TemplateField HeaderText="Days Left">
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="33%" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_amout" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"daysleft") %>'
                                                                    CssClass="clsLeftPaddingTable"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <td align="center" valign="top">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../../images/separator_repeat.gif" height="11">
                                                </td>
                                                <td background="../../images/separator_repeat.gif" height="11">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" valign="top">
                                        <table runat="server" id="tbl_generalcomments" cellpadding="0" cellspacing="0" border="0"
                                            width="100%">
                                            <tr>
                                                <td height="25px" colspan="7">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="clssubhead">
                                                    General Comments
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="11px" colspan="7">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" width="100%">
                                                    <cc1:WCtl_Comments ID="wctl_GeneralComments" runat="server" Width="753px"></cc1:WCtl_Comments>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../../images/separator_repeat.gif" height="11" width="800px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="Button1" runat="server" CssClass="clsbutton" Text="Update" OnClick="Button1_Click"
                                                        OnClientClick="return Validate();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:Footer ID="Footer1" runat="server" />
                                        <asp:HiddenField ID="hf_showhide" runat="server" Value="0" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_First" />
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_Previous" />
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_Next" />
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_Last" />
                <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" />
                <aspnew:AsyncPostBackTrigger ControlID="Button1" />
            </Triggers>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

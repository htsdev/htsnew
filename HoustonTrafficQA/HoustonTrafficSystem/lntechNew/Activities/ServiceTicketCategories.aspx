<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceTicketCategories.aspx.cs"
    Inherits="HTP.Activities.ServiceTicketCategories" EnableEventValidation="false" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Service Ticket Categories</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
    
    function ValidateAddCategory()
    {
        var txt = document.getElementById('txtNewCategory');
        var cblist = document.getElementById('Cblist_casetype');
        var ddlAdd = document.getElementById('ddlUserAdd');
        if(txt.value == '')
        {
            alert('Please enter some text for the new category');
            txt.focus();
            return(false);
        }
         if(ddlAdd.value == '0')
        {
            alert('Please select user to assign the new category');
            ddlAdd.focus();
            return(false);
        }
        
        
        if ((! document.getElementById("Cblist_casetype_0").checked) &&  (! document.getElementById("Cblist_casetype_1").checked) && (! document.getElementById("Cblist_casetype_2").checked)&& (! document.getElementById("Cblist_casetype_3").checked))
        {
            alert("Please select Case type for Category");
            return false;
        }
        // sadaf Aziz 10235 12/05/2012 validating Category name 
        
        var alpha = '^[a-zA-Z]+[a-zA-Z (\\)/`��.]*$';
        if(!txt.value.match(alpha))
        {
        alert('invalid Characters');
        txt.focus();
        return(false);
       
      
               }
        
    }
    
    function ValidateUpdateCategory()
    {
        var txt = document.getElementById('txtCategoryUpdate');
         var ddlUpdate = document.getElementById('ddlUserUpdate');
        if(txt.value == '')
        {
            alert('Please enter some text for the new category');
            txt.focus();
            return(false);
        }
         if(ddlUpdate.value == '0')
        {
            alert('Please select any User to assign the new category');
            ddlUpdate.focus();
            return(false);
        }                
        if ((! document.getElementById("cblist_updatecasetype_0").checked) &&  (! document.getElementById("cblist_updatecasetype_1").checked) && (! document.getElementById("cblist_updatecasetype_2").checked)&& (! document.getElementById("cblist_updatecasetype_3").checked) )
        {
            alert("Please select Case type for Category");
            return false;
        }
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="v_Add" runat="server" OnActivate="v_Add_Activate">
                            <table width="780px%">
                                <tr>
                                    <td background="../Images/separator_repeat.gif" height="9" width="750px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 103%; height: 100%">
                                            <tr style="background-color: #EFF4FB">
                                                <td align="center" style="width: 819px">
                                                    <table style="width: 622px">
                                                        <tr>
                                                            <td style="width: 234px;" colspan="3">
                                                                <asp:Label ID="Lbl_newcategory" runat="server" CssClass="clsSubhead" Text="New Category Name :"
                                                                    Width="123px"></asp:Label>
                                                            </td>
                                                            <td style="width: 239px">
                                                                &nbsp;
                                                                <asp:TextBox ID="txtNewCategory" runat="server" CssClass="clsinputadministration" MaxLength="50"
                                                                    Width="200px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 234px;" colspan="3">
                                                                <asp:Label ID="Label4" runat="server" CssClass="clsSubhead" Text="Open Email To:"
                                                                    Width="123px"></asp:Label>
                                                            </td>
                                                            <td style="width: 239px">
                                                                &nbsp;
                                                                <asp:DropDownList ID="ddlUserAdd" runat="server" Width="200px" CssClass="clsInputCombo">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 266px" align="left">
                                                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                                                    OnClientClick="return ValidateAddCategory()" Text="Add" Width="60px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 234px;" colspan="3">
                                                                <asp:Label ID="lbl_casetype" runat="server" CssClass="clsSubhead" Text="Case Type :"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:CheckBoxList ID="Cblist_casetype" runat="server" RepeatColumns="7" DataTextField="CaseTypeName"
                                                                    DataValueField="CaseTypeId">
                                                                </asp:CheckBoxList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../Images/separator_repeat.gif" height="9" style="width: 819px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px;
                                                    width: 819px;">
                                                    &nbsp;Service Ticket Categories
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 819px">
                                                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 819px">
                                                    <asp:GridView ID="gvServiceTicketCategories" runat="server" AutoGenerateColumns="False"
                                                        CssClass="clsLeftPaddingTable" OnRowCommand="gvServiceTicketCategories_RowCommand"
                                                        OnRowDeleting="gvServiceTicketCategories_RowDeleting" CellPadding="3" CellSpacing="0">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SNo">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSno" runat="server" CssClass="clssubhead" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbtn_Desc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SNo") %>' CommandName="Select"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hf_CategoryID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Case Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_casetype" runat="server" Width="100px" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.casetypename") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open Email To">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label6" runat="server" Width="100px" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.EmpName") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_AssignedUser" runat="server" Width="100px" Visible="false" CssClass="clsLabel"
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.AssignTo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/remove2.gif"
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>' CommandName="Delete" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <td align="center">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="v_Update" runat="server" OnActivate="v_Update_Activate">
                            <table width="100%" style="background-color: #EFF4FB; border-color: #EFF4FB;">
                                <tr>
                                    <td background="../Images/separator_repeat.gif" height="9">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="Label3" runat="server" CssClass="clssubhead" Text="Update Category"></asp:Label>:
                                        <asp:Label ID="lblCategory" runat="server" CssClass="clsLabel"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table>
                                            <tr>
                                                <td class="style1">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 117px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="clsSubhead" Text="New Category Name:"></asp:Label>
                                                </td>
                                                <td style="width: 213px">
                                                    &nbsp;
                                                    <asp:TextBox ID="txtCategoryUpdate" runat="server" CssClass="clsinputadministration"
                                                        Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 117px">
                                                    <asp:Label ID="Label5" runat="server" CssClass="clsSubhead" Text="Assigned To :"
                                                        Width="110px"></asp:Label>
                                                </td>
                                                <td style="width: 213px">
                                                    &nbsp;
                                                    <asp:DropDownList ID="ddlUserUpdate" runat="server" Width="200px" CssClass="clsInputCombo">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" OnClick="btnUpdate_Click"
                                                        Text="Update" OnClientClick="return ValidateUpdateCategory()" />
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" OnClick="btnCancel_Click"
                                                        Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="clsSubhead" Text="Case Type :"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:CheckBoxList ID="cblist_updatecasetype" CssClass="clsLabel" runat="server" RepeatColumns="7"
                                                        DataTextField="CaseTypeName" DataValueField="CaseTypeId">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

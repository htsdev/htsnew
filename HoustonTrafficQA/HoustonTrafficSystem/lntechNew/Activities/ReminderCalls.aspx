<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.ReminderCalls" Codebehind="ReminderCalls.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReminderCalls</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
		//In order to set focus on clicked record 
//		function SetFocusOnGrid()
//		{
//		//a;
//			var id= <%=Session["ReminID"]%>;		
//			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
//			var grdName = "dg_ReminderCalls";		
//			var idx=2;			
//			cnt = cnt+2;
//			for (idx=2; idx < (cnt); idx++)
//			{					
//			var txtbox = grdName+ "__ctl" + idx + "_txt_sno"
//			var txtfocus=document.getElementById(txtbox);							
//			var compare=txtfocus.value;
//			  if (compare==id)
//			  {
//			  //a;			  
//				  txtfocus.focus();
//				  <%Session["ReminID"]=1;%>;				 //initializing to 1 
//				  break;			
//			  }
//			}		
//		}
        function SetFocusOnGrid()
		{
		//a;
			var id= <%=Session["ReminID"]%>;		
			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
			var grdName = "dg_ReminderCalls";		
			var idx=2;			
			cnt = cnt+2;
			for (idx=2; idx < (cnt); idx++)
			{	
			var txtbox="";
			    if( idx < 10)
			    {				
			         txtbox = grdName+ "_ctl0" + idx + "_txt_sno"
			    }
			    else
			        txtbox = grdName+ "_ctl" + idx + "_txt_sno"    
			var txtfocus=document.getElementById(txtbox);							
			var compare=txtfocus.value;
			  if (compare==id)
			  {
			  //a;			  
				  txtfocus.focus();
				  <%Session["ReminID"]=1;%>;				 //initializing to 1 
				  break;			
			  }
			}		
		}
		</script>
	</HEAD>
	<body onload="SetFocusOnGrid();" >
		<FORM id="Form2" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" 
									height="9"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 9px"><ew:calendarpopup id="cal_EffectiveFrom" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
							CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True"
							UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
							ImageUrl="../images/calendar.gif" Text=" " Width="80px">
							<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
							<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></WeekdayStyle>
							<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Yellow"></MonthHeaderStyle>
							<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
								BackColor="AntiqueWhite"></OffMonthStyle>
							<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></GoToTodayStyle>
							<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="LightGoldenrodYellow"></TodayDayStyle>
							<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Orange"></DayHeaderStyle>
							<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="LightGray"></WeekendStyle>
							<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Yellow"></SelectedDateStyle>
							<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></ClearDateStyle>
							<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></HolidayStyle>
						</ew:calendarpopup><asp:dropdownlist id="ddl_rStatus" runat="server" AutoPostBack="True" CssClass="clsInputCombo">
						</asp:dropdownlist>&nbsp;
						<asp:button id="btn_update1" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_update1_Click1"></asp:button></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 13px"><asp:label id="lblMessage" runat="server" Width="320px" CssClass="label" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
							<tr >
								<td style="HEIGHT: 11px" width="780" background="../../images/separator_repeat.gif"
									colSpan="5" height="11"></td>
							</tr>
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="dg_ReminderCalls" runat="server" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<TABLE class="clsleftpaddingtable" id="Table1" cellSpacing="1" cellPadding="0" width="780"
														align="center" border="0">
														<TR>
															<TD colSpan="5" rowSpan="1">
																<asp:Label id=lbl_owes runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.owedamount", "{0:C0}") %>' CssClass="label" ForeColor="Red" Visible="False">
																</asp:Label>
																<asp:Label id=lbl_Firm runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>' Visible="False">
																</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															</TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" vAlign="middle" width="105" bgColor="#eff4fb" height="20">
																<asp:Label id="Label11" runat="server" CssClass="label" ForeColor="#123160">Name:</asp:Label></TD>
															<TD class="clsLeftPaddingTable" bgColor="#eff4fb" colSpan="2" height="20">
																<asp:HyperLink id=lnkName runat="server" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.LastName"),", ",(DataBinder.Eval(Container, "DataItem.FirstName") )) %>' CssClass="label">
																</asp:HyperLink>
																<asp:Label id=lbl_TicketID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketID") %>' CssClass="label" ForeColor="#123160" Visible="False">
																</asp:Label>
																<asp:Label id="lbl_RID" runat="server" CssClass="label" ForeColor="#123160" Visible="False"></asp:Label></TD>
															<TD vAlign="top" width="123" bgColor="#eff4fb" height="20" rowSpan="4">
																<P>
																	<asp:Label id=lbl_contact1 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>' CssClass="label">
																	</asp:Label><BR>
																	<asp:Label id=lbl_Contact2 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>' CssClass="label">
																	</asp:Label><BR>
																	<asp:Label id=lbl_Contact3 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>' CssClass="label">
																	</asp:Label><BR>
																	<asp:Label id=lbl_Language runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Languagespeak") %>' CssClass="label">
																	</asp:Label><BR>
																	<asp:HyperLink id="lnk_Comments" runat="server">Add Comments </asp:HyperLink>
																	<asp:Label id=lbl_phone runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>' Visible="False">
																	</asp:Label>
																	<asp:Label id=lbl_Fax runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fax") %>' Visible="False">
																	</asp:Label></P>
															</TD>
															<TD vAlign="top" width="205" bgColor="#eff4fb" height="20" rowSpan="4">
																<asp:Label id=lbl_comments runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Comments") %>' CssClass="label">
																</asp:Label></TD>
														</TR>
														<TR>
															<TD vAlign="middle" width="105" bgColor="#eff4fb" height="20">
																<asp:Label id="Label12" runat="server" CssClass="label" ForeColor="#123160">Case Status:</asp:Label></TD>
															<TD class="clsLeftPaddingTable" width="350" bgColor="#eff4fb" colSpan="2" height="20">
																<asp:Label id=lbl_Status runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialDesc") %>' CssClass="label" ForeColor="#123160" Font-Bold="True">
																</asp:Label></TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" vAlign="middle" bgColor="#eff4fb" height="20">
																<asp:Label id="Label13" runat="server" CssClass="label" ForeColor="#123160">Location:</asp:Label></TD>
															<TD class="clsLeftPaddingTable" bgColor="#eff4fb" colSpan="2" height="20">
																<asp:Label id=lbl_Location runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtAddress") %>' CssClass="label" ForeColor="#123160">
																</asp:Label>
																<asp:Label id=lbl_TicketViolationID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketviolationID") %>' CssClass="label" ForeColor="#123160" Visible="False">
																</asp:Label></TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" vAlign="middle" width="105" bgColor="#eff4fb" height="20">
																<asp:Label id="Label14" runat="server" CssClass="label" ForeColor="#123160">Call Back:</asp:Label>
																<asp:TextBox id="txt_sno" runat="server" Width="1px" ForeColor="#EFF4FB" Height="1px" BackColor="#EFF4FB"
																	BorderColor="#EFF4FB" BorderStyle="Solid"></asp:TextBox></TD>
															<TD class="clsLeftPaddingTable" width="151" bgColor="#eff4fb" height="20">
																<asp:Label id=lbl_CallBacks runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>' CssClass="label" ForeColor="#123160"></asp:Label></TD>
															<TD class="clsLeftPaddingTable" align="center" width="125" bgColor="#eff4fb" height="20">
																<DIV align="center"></DIV>
																<asp:Label id=lbl_BondFlag runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>' Visible="False">
																</asp:Label><BR>
																<asp:Label id="lbl_Bond" runat="server" Visible="False" Font-Bold="True" BackColor="#FFCC66">Bond</asp:Label></TD>
														</TR>
														<TR >
															<TD width="780" background="../../images/separator_repeat.gif" colSpan="5" height="11"></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="TableComment" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
							<TR class="clsleftpaddingtable">
								<TD class="clsaspcolumnheader"></TD>
								<TD class="clsaspcolumnheader" colSpan="2"></TD>
							</TR>
							<TR class="clsleftpaddingtable">
								<TD></TD>
								<TD></TD>
								<TD vAlign="bottom" align="right" colSpan="1" rowSpan="1"></TD>
							</TR>
							<TR >
								<TD width="780" background="../../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
							<TR>
								<TD style="display : none">
									<asp:TextBox id="txt_totalrecords" runat="server"></asp:TextBox></TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>

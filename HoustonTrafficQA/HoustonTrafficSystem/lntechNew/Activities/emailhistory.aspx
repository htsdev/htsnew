<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.emailhistory"
    CodeBehind="emailhistory.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Text Message</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            width: 139px;
        }
    </style>

    <script type="text/javascript">
    
    //Yasir Kamal 6064 07/01/2009 do not allow to search for past dates.
    
        function validate()
		{
		    var strCalFrom =  document.getElementById("calFrom").value;
		    var strCalEnd =  document.getElementById("calTo").value;
		    var dd_sendsmsto =  document.getElementById("dd_sendsmsto").value;
		    var sdate = Date.parse(strCalFrom);
		    var edate = Date.parse(strCalEnd);
		    today = new Date(); 
            var curr = Date.parse(today.toDateString());
             
	        if (sdate > curr || edate > curr)
            {
            alert("Please select today's or past date");
            return false;
            }
            
            // Noufil 5884 07/06/2009 Javascript message added.
            if (dd_sendsmsto == "-1")
            {
                alert("Please select message category.");
                return false;
            }
		}
		    
		    // Noufil 5884 07/07/2009 Check Selected Records
		    function CheckSelectedRecords()
		    {
		        var checkdate = "0";
		        var gv = document.getElementById("gvresult");
		        document.getElementById("hf_isRecordCheck").value = "";
                var gridRowCount = gv.rows.length;
                for (var i=1; i < gridRowCount; i++)
                {
                    if (gv.rows[i].cells[0].innerText.trim() != "Next >   Last Page >>")
                    {
                        if (parseInt(gv.rows[i].cells[6].childNodes.length) >1 && parseInt(gv.rows[i].cells[6].childNodes.length)>0)
                        {   
                            if ((gv.rows[i].cells[6].all[1].type== "checkbox") && (gv.rows[i].cells[6].all[1].checked))
                            {
                                if (document.getElementById("hf_isRecordCheck").value == "")
                                    document.getElementById("hf_isRecordCheck").value = i +",";
                                else
                                    document.getElementById("hf_isRecordCheck").value += i +",";
                            }
                        }
                    }
                }
                if (document.getElementById("hf_isRecordCheck").value == "")
                {
                    alert("Please select records to resend.");
                    return false;
                }   
                else 
                    return true;
		        
		    }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="upnlResult" runat="server">
        <ContentTemplate>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tr>
                    <td style="width: 815px" colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td style="width: 816px" colspan="4">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" colspan="4"
                                    height="10">
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="4">
                                    <table id="Table4" style="width: 788px" cellspacing="1" cellpadding="1" border="0">
                                        <tr>
                                            <td width="16%">
                                                <span class="clssubhead">Message Sent Date :</span>&nbsp;
                                            </td>
                                            <td width="20%">
                                                <span class="clssubhead">From</span>&nbsp;
                                                <ew:CalendarPopup ID="calFrom" runat="server" DisableTextboxEntry="true" ToolTip="Select Report Date Range"
                                                    PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)"
                                                    AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                                    Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif" Width="90px">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td width="19%">
                                                <span class="clssubhead">To</span>&nbsp;
                                                <ew:CalendarPopup ID="calTo" runat="server" ToolTip="Select Report Date Range" PadSingleDigits="True"
                                                    UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
                                                    ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                                    Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif" Width="90px"
                                                    DisableTextboxEntry="true">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td width="15%">
                                                <span class="clssubhead">Message Category :</span>&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dd_sendsmsto" runat="server" CssClass="clsInputCombo" Width="120px">
                                                    <asp:ListItem Selected="True" Text="--- Choose ---" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Sent to Attorney" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Sent to Client" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="10%" align="right">
                                                <asp:Button ID="btn_submit" runat="server" OnClientClick="return validate();" CssClass="clsbutton"
                                                    Text="Search" OnClick="btn_submit_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" colspan="4"
                                    height="10">
                                </td>
                            </tr>
                            <tr>
                                <td style="background-image: url(../Images/subhead_bg.gif)" class="clssubhead" align="left"
                                    height="34px">
                                    <table style="width: 100%">
                                        <tr>
                                            <td class="clssubhead">
                                                History
                                            </td>
                                            <td align="right" valign="middle">
                                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                            </td>
                                            <td align="right">
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="nexttelemail.aspx?sMenu=26"
                                                    BackColor="Transparent">Global Settings</asp:HyperLink>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="background-image: url(../images/separator_repeat.gif)"
                                    height="11">
                                </td>
                            </tr>
                            <%--       <tr>
                        <td height="39px" width="100%" style="background-image: url(../Images/subhead_bg.gif)"
                            colspan="4">
                            <table id="Table3" style="width: 100%; height: 20px" cellspacing="1" cellpadding="1"  border="0">
                                <tr>
                                    <td class="clssubhead" style="width: 233px">
                                        &nbsp;History
                                    </td>
                                    <td align="right">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="nexttelemail.aspx" BackColor="Transparent">Global Settings</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="100%">
                                    <table id="Table2" style="height: 24px; display: none" cellspacing="1" cellpadding="1"
                                        width="100%" border="0">
                                        <tr>
                                            <td width="5%" style="height: 22px">
                                                <asp:Label ID="Label1" runat="server" CssClass="clsaspcolumnheader">S.No</asp:Label>
                                            </td>
                                            <td width="12%" style="height: 22px">
                                                <asp:Label ID="Label2" runat="server" CssClass="clsaspcolumnheader">To</asp:Label>
                                            </td>
                                            <td width="17%" style="height: 22px">
                                                <asp:Label ID="Label3" runat="server" CssClass="clsaspcolumnheader">Client</asp:Label>
                                            </td>
                                            <td width="33%" style="height: 22px">
                                                <asp:Label ID="Label5" runat="server" CssClass="clsaspcolumnheader">Information</asp:Label>
                                            </td>
                                            <td width="20%" style="height: 22px">
                                                <asp:Label ID="Label6" runat="server" CssClass="clsaspcolumnheader">Send</asp:Label>
                                            </td>
                                            <td align="right" width="15%" style="height: 22px">
                                                <asp:Button ID="btn_send" runat="server" CssClass="clsbutton" Text="Resend" CommandName="resend">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                                        <ProgressTemplate>
                                            <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:GridView ID="gvresult" runat="server" BorderColor="White" CssClass="clsLeftPaddingTable"
                                        Width="100%" AutoGenerateColumns="False" BorderStyle="None" ShowFooter="True"
                                        CellPadding="0" OnRowCommand="gvresult_RowCommand" AllowPaging="True" OnPageIndexChanging="gvresult_PageIndexChanging"
                                        PageSize="50">
                                        <PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="S.No">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SNo" runat="server" Text='<%# Eval("SNo") %>' CssClass="Label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader" Width="100px">
                                                </HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_emailto" runat="server" CssClass="Label" Text='<%# Eval("emailto") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Subject">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_client" runat="server" CssClass="Label" Text='<%# Eval("client") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Information">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_subject" runat="server" CssClass="Label" Text='<%# Eval("subject") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Send">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_dates" runat="server" CssClass="Label" Text='<%# Eval("dates") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Resend By">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Resend" runat="server" CssClass="Label" Text='<%# Eval("resendby") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" Font-Bold="False"
                                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False">
                                                </HeaderStyle>
                                                <HeaderTemplate>
                                                    <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Resend" CommandName="resend"
                                                        OnClientClick="return CheckSelectedRecords();"></asp:Button>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbox_resend" runat="server" CssClass="Label"></asp:CheckBox>
                                                    <asp:Label ID="lbl_id" runat="server" Text='<%# Eval("id") %>' Visible="False">
                                                    </asp:Label>
                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("ticketid") %>' />
                                                    <asp:HiddenField ID="hf_phonenumber" runat="server" Value='<%# Eval("PhoneNumber") %>' />
                                                    <asp:HiddenField ID="hf_TicketNumber" runat="server" Value='<%# Eval("TicketNumber") %>' />
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# Eval("CourtDate") %>' />
                                                    <asp:HiddenField ID="hf_CourtNumber" runat="server" Value='<%# Eval("CourtNumber") %>' />
                                                    <asp:HiddenField ID="hf_Client" runat="server" Value='<%# Eval("Clientname") %>' />
                                                    <asp:HiddenField ID="hf_courtname" runat="server" Value='<%# Eval("shortname") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" colspan="4"
                                    height="11">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="left" colspan="4">
                                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hf_isRecordCheck" runat="server" Value="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

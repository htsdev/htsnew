﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using HTP.WebComponents;
using HTP.Components;

namespace HTP.Activities
{
    public partial class LegalConsultationReport : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 5533 02/25/2008  grouping variables
        #region Variables

        ValidationReports VReport = new ValidationReports();
        clscalls LegalHostonReport = new clscalls();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        string ticketID;

        #endregion


        // Fahad Muhammad Qureshi 5533 02/25/2008  grouping Event Handlers
        #region EventHandlers
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {


                    if (Page.IsPostBack != true)
                    {
                        dtSDate.SelectedDate = DateTime.Today; //setting the date
                        dtEDate.SelectedDate = DateTime.Today;

                        GetReminderStatus(); //fills reminder status drop down list
                        fillgrid();

                    }

                    // Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_records;
                }
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }



        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    fillgrid();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }





        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("LnkBtnUpdt")).CommandArgument = e.Row.RowIndex.ToString();
                    ((Label)e.Row.FindControl("lblcomments")).Text = ((Label)e.Row.FindControl("lblcomments")).Text.Replace("<", "&lt;");
                    ((Label)e.Row.FindControl("lblcomments")).Text = ((Label)e.Row.FindControl("lblcomments")).Text.Replace(">", "&gt;");
                    //pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }


        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string ClientName = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_Clientname")).Value);
                    string Contact1 = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_contactno1")).Value);
                    string Contact2 = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_contactno2")).Value);
                    string Contact3 = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_contactno3")).Value);
                    ticketID = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_ticketno")).Value);
                    ViewState["ticketID"] = ticketID;
                    //string courtname = (((HiddenField)gv_Result.Rows[RowID].Cells[13].FindControl("hf_loc")).Value);
                    string commentdate = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_commentdate")).Value);
                    string comments = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_comments")).Value);
                    string StatusID = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_StatusId")).Value);
                    string StatusDesc = (((HiddenField)gv_records.Rows[RowID].Cells[1].FindControl("hf_StatusDescription")).Value);
                    ViewState["StatusDesc"] = StatusDesc;
                    //cCase.TicketID = Convert.ToInt32(ticketno);
                    //string comm = cCase.GetGeneralCommentsByTicketId();
                    //string follow = string.Empty;
                    //ozair 4442 07/22/2008 ALR Courts (Houston,Fort Bend, Conroe)
                    //int Days = (court == "3047" || court == "3048" || court == "3070") ? 7 : 14;
                    //follow = DateTime.Today.AddDays(Days).ToShortDateString();
                    //UpdateFollowUpInfo2.Freezecalender = true;
                    //WCC_ConsultationComments.Initialize(Convert.ToInt32(ticketno), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 7, "Label", "clsinputadministration", "clsbutton");
                    //WCC_ConsultationComments.Button_Visible = false;
                    binddate(ClientName, Contact1, Contact2, Contact3, ticketID, commentdate, comments, StatusID, StatusDesc);
                    ModalPopupExtender1.Show();
                    Pagingctrl.Visible = true;

                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                gv_records.PageIndex = 0;
                fillgrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Nasir 5256 12/12/2008 this function save the Consultaion comment and status 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string NewComments = tb_ConsultationComments.Text.Trim();
                NewComments = NewComments.Replace("<", "&lt;");
                NewComments = NewComments.Replace(">", "&gt;");
                string StatusDesc = ViewState["StatusDesc"].ToString();
                string LegalConsultationComments = lblComments.Text + tb_ConsultationComments.Text;
                LegalConsultationComments = LegalConsultationComments.Replace("<", "&lt;");
                LegalConsultationComments = LegalConsultationComments.Replace(">", "&gt;");
                LegalHostonReport.UpdateLegalConsultationComments(Convert.ToInt32(ViewState["ticketID"]), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), LegalConsultationComments, Convert.ToInt32(ddl_Status.SelectedValue));

                if (NewComments != string.Empty)
                {
                    clog.AddNote(Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "Call Back Notes: " + NewComments, "", Convert.ToInt32(ViewState["ticketID"]));
                }
                if (ddl_Status.SelectedItem.Text != StatusDesc)
                {

                    clog.AddNote(Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "Call Back Status:Changed from " + StatusDesc + " To " + ddl_Status.SelectedItem.Text, "", Convert.ToInt32(ViewState["ticketID"]));
                }

                fillgrid();


            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion


        // Fahad Muhammad Qureshi 5533 02/25/2008  grouping Methods
        #region Methods

        //Nasir 5256 12/12/2008 this function bind data in ajax screen in legal consultation page
        public void binddate(string ClientName, string Contact1, string Contact2, string Contact3, string ticketID, string commentdate, string comments, string StatusID, string StatusDesc)
        {
            //if (freezecalender == true)
            //{
            //  ViewState["freestate"] = freezecalender;
            if (tb_ConsultationComments.Text != string.Empty)
            {
                tb_ConsultationComments.Text = "";
            }


            lblClientName.Text = ClientName;
            lblContactNumber1.Text = Contact1;
            lblContactNumber2.Text = Contact2;
            lblContactNumber3.Text = Contact3;
            //lblTicketNumber.Text = ticketno;

            this.hf_ticid.Value = ticketID;
            this.hf_ticketid.Value = ticketID;
            ddl_Status.SelectedValue = StatusID;

            lblComments.Text = comments;

            //pnl_control.Style["display"] = "block";
            //pnl_control.Style["position"] = "";
            //chk_UpdateAll.Visible = false;


            //this.hf_courtname.Value = courtname;
            //btnCancel.Attributes.Remove("onClick");
            //lbtn_close.Attributes.Remove("onClick");

            //btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            //lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");

        }
        private void GetReminderStatus()
        {
            DataTable dtStatus = LegalHostonReport.GetReminderStatus();

            if (dtStatus.Rows.Count > 0)
            {

                ddl_Status.Items.Clear();
                ddlCallBackStatus.Items.Clear();
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                    ddl_Status.Items.Add(item);
                    ddlCallBackStatus.Items.Add(item);
                }
                //Fahad 5533 02/25/2009 Set value for all
                ddlCallBackStatus.Items.Add(new ListItem("All", "-1"));

            }
        }

        public void fillgrid()
        {
            DataTable dt = LegalHostonReport.GetLegalConsultationData(dtSDate.SelectedDate, dtEDate.SelectedDate, Int32.Parse(ddlCallBackStatus.SelectedValue), chkShowAll.Checked); //  .GetVisitorReport();

            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No Records Found";
                Pagingctrl.Visible = false;
            }
            else
            {
                lblMessage.Text = "";
                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["TicketID_PK"].ToString() != dt.Rows[i]["TicketID_PK"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }
                Pagingctrl.Visible = true;
                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();
                Pagingctrl.GridView = gv_records;
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            //else
            //{
            //    Pagingctrl.PageCount = 0;
            //    Pagingctrl.PageIndex = 0;
            //    Pagingctrl.SetPageIndex();
            //    lbl_Message.Text = "No Records Found";
            //    gv_SearchBatch.Visible = false;
            //}
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }
        /// FAHAD 5533 02/25/2009 Page Size Change method added.
        /// 
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gv_records.PageIndex = 0;
                    gv_records.PageSize = pageSize;
                    gv_records.AllowPaging = true;
                }
                else
                {
                    gv_records.AllowPaging = false;
                }
                fillgrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion


    }

}

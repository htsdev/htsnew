<%@ Page Language="C#" AutoEventWireup="true" Codebehind="NewPartialPay.aspx.cs"
    Inherits="lntechNew.Activities.NewPartialPay" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Partial Pays</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
         function OpenEditWin(ticketid,RecID)
	      {
	       
	          var PDFWin
		      PDFWin = window.open("PartialPayNotes.aspx?casenumber="+ticketid+"&RecID="+RecID,"","fullscreen=no,toolbar=no,width=422,height=331,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=no");				
		      return false;				      
		      //&"RecID="+RecID
	       } 
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 98px;
        }
        .style2
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 24px;
            width: 98px;
        }
        .style3
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 30px;
            width: 98px;
        }
        .style7
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 91px;
        }
        .style8
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 24px;
            width: 91px;
        }
        .style9
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 30px;
            width: 91px;
        }
        .style10
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 86px;
        }
        .style11
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 24px;
            width: 86px;
        }
        .style12
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 30px;
            width: 86px;
        }
        .style13
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 418px;
        }
        .style14
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 24px;
            width: 418px;
        }
        .style15
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            height: 30px;
            width: 418px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                width="780">
                <tbody>
                    <tr>
                        <td style="width: 827px; height: 14px">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Partial Pays&nbsp; </font></STRONG>
									</td>
								</tr>-->
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="clsleftpaddingtable">
                                                        <tr>
                                                            <td class="style10">
                                                            </td>
                                                            <td class="style7">
                                                            </td>
                                                            <td class="style1">
                                                                &nbsp;</td>
                                                            <td class="style13">
                                                            </td>
                                                            <td class="clssubhead" align="right">
                                                                <asp:DropDownList ID="ddl_Pageno" runat="server" CssClass="clsinputcombo" Visible="False"
                                                        OnSelectedIndexChanged="ddl_Pageno_SelectedIndexChanged" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style11">
                                                                Court Location
                                                            </td>
                                                            <td class="style8">
                                                                Plan
                                                                Status
                                                            </td>
                                                            <td class="style2">
                                                                Case Status</td>
                                                            <td class="style14">
                                                                Due Date Range
                                                            </td>
                                                            <td class="clssubhead" style="height: 24px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style12">
                                                                <asp:DropDownList ID="ddlCourtLocation" runat="server" CssClass="clsinputcombo" 
                                                                    Width="80px">
                                                                </asp:DropDownList></td>
                                                            <td class="style9">
                                                                <asp:DropDownList ID="ddl_partialstatus" runat="server" 
                                                                    CssClass="clsinputcombo" Width="90px">
                                                                    <asp:ListItem Value="0">Active Plans</asp:ListItem>
                                                                    <asp:ListItem Value="1">InActive Plans</asp:ListItem>
                                                                    <asp:ListItem Value="2">Default Plans</asp:ListItem>
                                                                    <asp:ListItem Value="3">Waived Plans</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                            <td class="style3">
                                                                <asp:DropDownList ID="ddl_CaseStatus" runat="server" CssClass="clsinputcombo" 
                                                                    Width="95px">
                                                                    <asp:ListItem Value="0" Selected="True">All Cases</asp:ListItem>
                                                                    <asp:ListItem Value="1">Open Cases</asp:ListItem>
                                                                    <asp:ListItem Value="2">Disposed Cases</asp:ListItem>
                                                                </asp:DropDownList>
                                                                </td>
                                                            <td class="style15">
                                                                <ew:CalendarPopup ID="sduedate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" SelectedDate="2006-05-26"
                                                                    ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                                    Width="75px">
                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>
                                                                &nbsp;<ew:CalendarPopup ID="eduedate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" SelectedDate="2006-05-26"
                                                                    ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                                    Width="75px">
                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>
                                                                <asp:CheckBox ID="chk_pastdues" runat="server" CssClass="clslabelnew" Text="Show all past dues"
                                                                    Checked="True" />
                                                                <asp:CheckBox ID="chk_AllowPaging" runat="server" Checked="True" CssClass="clslabelnew"
                                                                    Text="Allow Paging" />
                                                                </td>
                                                            <td class="clssubhead" style="height: 30px" align="center">
                                                                <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit"
                                                                    OnClick="btnSubmit_Click" />&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" colspan="5">
                                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                                             </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table id="TblGrid" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dg_partial" runat="server" AllowPaging="True" AllowSorting="True"
                                                        AutoGenerateColumns="False" BackColor="#EFF4FB" BorderColor="White" CssClass="clsLeftPaddingTable"
                                                        PageSize="30" Width="100%" OnPageIndexChanged="dg_partial_PageIndexChanged" OnItemDataBound="dg_partial_ItemDataBound">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="S#">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="3%" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                                    <asp:Label ID="lbl_ticketid" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"TicketID_PK") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Client">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="20%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lnkb_clientname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ClientName") %>'
                                                                        CssClass="label"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Phone Number">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="20%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <table id="tbl_contacts" border="0" cellpadding="1" cellspacing="1" class="clsleftpaddingtable"
                                                                        width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbl_contact1" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Contact1") %>'>
                                                                                </asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbl_contact2" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contact2") %>'>
                                                                                </asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbl_contact3" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contact3") %>'>
                                                                                </asp:Label></td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Court">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="8%" />
                                                                <ItemStyle VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCrt" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"crt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Court Date">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="12%" />
                                                                <ItemStyle VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_contactdate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"CourtDate","{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Fee">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFee" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.TotalFeeCharged","{0:C0}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Owes">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblowes" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Owes", "{0:C0}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Payment Plan">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" VerticalAlign="Top"
                                                                    Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td border="1">
                                                                                <asp:Label ID="lblPayplandate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"PaymentDate","{0:d}") %>'></asp:Label>
                                                                            </td>
                                                                            <td border="1">
                                                                                <asp:Label ID="lblPayplanamount" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.Amount","{0:C0}")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle Width="3%" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImgComment" runat="server" ImageUrl="~/Images/Add.gif" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " />
                                                    </asp:DataGrid></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:Footer ID="Footer1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>

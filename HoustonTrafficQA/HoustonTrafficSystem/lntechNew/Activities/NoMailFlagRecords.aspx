<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoMailFlagRecords.aspx.cs"
    Inherits="HTP.Reports.NoMailFlagRecords" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TrafficMenuMain.ascx" TagName="TrafficMenuMain"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No mail flag records</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
    
function ClearForm()
{
//Clear all values
document.getElementById('txt_FirstName').value='';
document.getElementById('txt_LastName').value='';
document.getElementById('txt_Address').value='';
document.getElementById('txt_ZipCode').value='';
document.getElementById('lbl_Message').innerText = '';
document.getElementById('txt_City').value='';
//Yasir Kamal 6100 07/03/2009 reset calendar control date.
document.getElementById('chk_useRange').checked=true;
document.getElementById('ddl_States').value = 0;
document.getElementById('td_grid').style.display='none';
document.getElementById('tblPaging').style.display='none';

var date='<%=ViewState["date"]%>';
document.form1.calFrom.value = date;
document.form1.calTo.value = date;

return(false);
}


function useRange()
		{		
		 
		    if (document.getElementById("chk_useRange").checked)
		    {
		      document.getElementById("calFrom").disabled =false;
		      document.getElementById("calTo").disabled =false;
		    }
		    else
		    {
	        document.getElementById("calFrom").disabled =true;
	        document.getElementById("calTo").disabled =true;
		    }
		}	

function ValidateForm()
{

  /*  var var1 = document.getElementById('txt_FirstName');
    var var2 = document.getElementById('txt_LastName');
    var var3 = document.getElementById('txt_Address');
    var var4 = document.getElementById('txt_ZipCode');

    if(var1.value == '' && var2.value == '' && var3.value == '' && var4.value == '')
    {
        alert('Please enter a value to search');
        var1.focus();
        return(false);
    }
    
    if(var4.value != '' && (var1.value == '' && var2.value == '' && var3.value == ''))
    {
        alert('To search by zip code, please supply either the first name, last name or address with the zip code');
        var1.focus();
        return(false);
    }
*/
    return(true);
}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" method="post" runat="server">
    <div>
        <table cellspacing="0" cellpadding="0" width="820 px" align="center" border="0">
            <tr>
                <td>
                    <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/subhead_bg.gif)" height="34" class="clssubhead">
                    <table width="100%">
                        <tr>
                            <td class="clssubhead" width="50%">
                                Search
                            </td>
                            <td class="clssubhead" width="50%" align="right">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="DoNotMail.aspx?sMenu=121">Back</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table align="center" class="" width="100%">
                        <tr>
                            <td align="right">
                                <asp:Label ID="label1" runat="server" Text="First Name:" CssClass="clsLabel"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_FirstName" runat="server" CssClass="clsInputadministration"
                                    Width="150px"></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Label ID="label2" runat="server" Text="Last Name:" CssClass="clsLabel"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_LastName" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Label ID="label5" runat="server" Text="City:" CssClass="clsLabel"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_City" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click"
                                    Width="50px" OnClientClick="return ValidateForm();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="label3" runat="server" Text="Address:" CssClass="clsLabel"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_Address" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Label ID="label4" runat="server" Text="Zip Code:" CssClass="clsLabel"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_ZipCode" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Label ID="label6" runat="server" Text="State:" CssClass="clsLabel"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_States" runat="server" CssClass="clsInputCombo">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:Button ID="btn_Reset" runat="server" CssClass="clsbutton" Text="Reset" OnClientClick="return ClearForm();"
                                    Width="50px"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" width="100%">
                                <table id="Table4" style="width: 100%">
                                    <tr>
                                        <td style="width: 260px">
                                            <span class="clsLabel">&nbsp;&nbsp; From Date:</span>
                                            <ew:CalendarPopup ID="calFrom" runat="server" DisableTextboxEntry="true" ToolTip="Select Report Date Range"
                                                PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)"
                                                AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                                Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif" Width="90px">
                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></WeekdayStyle>
                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGray"></WeekendStyle>
                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></HolidayStyle>
                                            </ew:CalendarPopup>
                                        </td>
                                        <td style="width: 262px">
                                            <span class="clsLabel">&nbsp; To Date:</span>
                                            <ew:CalendarPopup ID="calTo" runat="server" ToolTip="Select Report Date Range" PadSingleDigits="True"
                                                UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
                                                ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                                Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif" Width="90px"
                                                DisableTextboxEntry="true">
                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></WeekdayStyle>
                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGray"></WeekendStyle>
                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></HolidayStyle>
                                            </ew:CalendarPopup>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chk_useRange" runat="server" onclick="useRange()" CssClass="clsLabelNew"
                                                Checked="true" Text="Use Date Range" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/separator_repeat.gif)" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/subhead_bg.gif)" height="34" class="clssubhead"
                    align="right">
                    <table id="tblPaging" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc4:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/separator_repeat.gif)" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td id="td_grid">
                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        Width="100%" PageSize="20" OnRowCommand="gv_Records_RowCommand" AllowPaging="True"
                        OnPageIndexChanging="gv_Records_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="S. No">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_sno" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Address" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_city" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_State" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zip">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Zip" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.Zip") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Insert Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_insertdate" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.insertdate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Update Source">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_updatSource" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.UpdateSource") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="clssubhead" Width="3%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.FirstName") +"," +DataBinder.Eval(Container, "DataItem.LastName") +"," + DataBinder.Eval(Container, "DataItem.Address") + "," + DataBinder.Eval(Container, "DataItem.Zip") %>'
                                        ImageUrl="~/Images/cross.gif" CommandName="Remove" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

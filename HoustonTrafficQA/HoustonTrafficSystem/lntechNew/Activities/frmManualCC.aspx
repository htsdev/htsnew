<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmManualCC.aspx.cs" Inherits="lntechNew.Activities.frmManualCC" %>

<%@ Register TagName="ActiveMenu" TagPrefix="uc3" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manual CC Processing</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />    
    <SCRIPT src="../Scripts/validatecreditcard.js" type="text/javascript"></SCRIPT>
    
    <script type="text/jscript"> 
     		
     var txtCardSatus=1;
     function GetFocus()
     {
        if(txtCardSatus !="0")
        {
         document.getElementById("txt_creditcard").focus();
        }
        return;
     }
     
     function VerifyCard()
        {	
            var carddata =document.getElementById("txt_creditcard").value;
            
            if(carddata !=null || carddata.length > 2)
            {
            
                var CardHolderName;
                var CardNo;
                var ExpDate;                         
                 var FirstName;
                var LastName;
                var temp; 
                var tIndex;                     
                
                CardHolderName =carddata.substring(carddata.indexOf("^")+1,carddata.lastIndexOf("^"));
                // swap first name with last name and replace "/" with empty space.
                tIndex = CardHolderName.indexOf("/");
                if (tIndex != 0 && tIndex != -1 )
                {
                    LastName = CardHolderName.substring(0, tIndex);
                    FirstName =  CardHolderName.substring(tIndex+1,CardHolderName.length);
                    CardHolderName =  trimAll(FirstName) +" "+ LastName;
                }
                document.getElementById("lbl_cardholdername").innerText =CardHolderName.toUpperCase(); 
                
                ExpDate =carddata.substring(carddata.lastIndexOf("^")+1,carddata.lastIndexOf("^")+5);            
                document.getElementById("lbl_expmonth").innerText =ExpDate.substring(2,4);
                document.getElementById("lbl_expyear").innerText =ExpDate.substring(0,2);            
                
                document.getElementById("lbl_cardno").innerText =carddata.substring(2,carddata.indexOf("^"));     
               
            }
        }
     
     function FillCreditInfo()
     {
        if(document.getElementById("lbl_cardholdername").innerText == "" || document.getElementById("lbl_expyear").length ==0)
        {
            alert("Please swap the card.");
             document.getElementById("txt_creditcard").focus();  
            return false;
        }
        document.getElementById("txt_nameoncard").value =document.getElementById("lbl_cardholdername").innerText;
        document.getElementById("txt_ccnumber").value =document.getElementById("lbl_cardno").innerText;
        document.getElementById("ddl_month").value =document.getElementById("lbl_expmonth").innerText;
        document.getElementById("ddl_year").value =document.getElementById("lbl_expyear").innerText;
        closereaderpopup("0");
        
     }
      function ShowReaderPopup() 
		{
	      txtCardSatus="1";  
		   
		  document.getElementById("PopupPanel").style.display = 'block';
          document.getElementById("PopupPanel").style.top = 455;			
	      
	      document.getElementById("DisableDive").style.height = document.body.offsetHeight * 2 ;
		  document.getElementById("DisableDive").style.width = document.body.offsetWidth;
		  document.getElementById("DisableDive").style.display = 'block';
		   document.getElementById("lbl_cardholdername").innerText = "";
          document.getElementById("lbl_expmonth").innerText="";
          document.getElementById("lbl_cardno").innerText="";
          document.getElementById("lbl_expyear").innerText="";
          document.getElementById("txt_creditcard").innerText="";
		  document.getElementById("txt_creditcard").focus();  
		  return false;
		}
		
		function closereaderpopup(w)
		{
		  txtCardSatus=0;
		  document.getElementById("PopupPanel").style.display = 'none';
		  document.getElementById("DisableDive").style.display = 'none';
		  if(w=="1")
		  {
		    document.getElementById("txt_nameoncard").value="";
		    document.getElementById("txt_ccnumber").value ="";
		    document.getElementById("ddl_month").selectedIndex="0";
		    document.getElementById("ddl_year").selectedIndex="0";		   		 
		    txtCardSatus=0;
		   
		  }
		  if(w == "0")
		  {
		     document.getElementById("txt_ccnumber").focus();
		  }
		  document.getElementById("txt_creditcard").value="";
		  return false;
		}
    </script>
    
    
  <script language="javascript">
    function FillNameOnCard()
    {
      if((document.getElementById("txt_fname") !="") || (document.getElementById("txt_lname") !="" ))
      {
        var nameoncard =value=document.getElementById("txt_fname").value +" "+ document.getElementById("txt_lname").value;       
        document.getElementById("txt_nameoncard").value =nameoncard;              
      }
    }
    
    function FormValidation()
    {   
        var fname = document.getElementById("txt_fname").value;
        if(fname.length <= 0)
         {
            alert("Please Enter First Name");
            document.getElementById("txt_fname").focus();
            return false;
         }
       var lname =document.getElementById("txt_lname").value;
       if(lname.length <= 0)
       {
       //document.getElementById("txt_lname").style.background="yellow";
        alert("Please Enter Last Name");              
       document.getElementById("txt_lname").focus();
        return false
       }
       var nameoncard =document.getElementById("txt_nameoncard").value;
       if(nameoncard.length <= 0)
       {
        alert("Please Enter NameOnCard");
        document.getElementById("txt_nameoncard").focus();
        return false
       }
       ///////////// Credit card cheking ///////
     //  CheckManualCardNumber(form)
        var ccnumber =document.getElementById("txt_ccnumber").value;
       if(ccnumber.length <= 0)
       {
        alert("Please Enter CC Number");
        document.getElementById("txt_ccnumber").focus();
        return false
       }  
       //////
//       var cin =document.getElementById("txt_cin").value;
//       if(cin.length <= 0)
//       {
//        alert("Please Enter CIN");
//        document.getElementById("txt_cin").focus();
//        return false
//       }
       var amount =document.getElementById("txt_amount").value;
       if(amount.length <= 0)
       {
        alert("Please Enter Amount");
        document.getElementById("txt_amount").focus();
        return false
       }
       var clienttype =document.getElementById("ddl_clienttype").value;
       if(clienttype <= 0)
       {
        alert("Please Select ClienType")
        document.getElementById("ddl_clienttype").focus();
        return false;
       }
       ///Checking Valid amount    
       if(isNaN(document.getElementById("txt_amount").value))
       {
         alert("This is not Valid amount");
         document.getElementById("txt_amount").focus();
         return false;
       }                               
       
       //CALLING FUNCTION TO VALIDATE CARD NUMBER			   
	     if (CheckManualCardNumber(document.forms[0])==false)		   
		   return false; 	
     
//       var intamount=0;
//		intamount=document.getElementById("txt_amount").value;
//		if (isNaN(document.getElementById("txt_amount").value) == false && intamount!="" && intamount !=0  )
//		{
//		            alert ("Incorrect payment amount.");
//					document.getElementById("txt_amount").focus(); 
//					return false;							
//		}

        ///////// Checking Transaction mode////
        var doyou;
	    if(document.getElementById("hf_transactionmode").value =="1")
	     {
	        doyou =confirm("This transaction is running in test mode. Are you sure you want to process it further?");
	        if(doyou == false)
	            return false;
	                
	     }		
			     
        plzwait();        
        
    }
     function plzwait()
		{
		
		document.getElementById("tbl_plzwait").style.display = 'block';
		document.getElementById("allcreditcard").style.display = 'none';
		
		}
		
    function ClearControls()
    {
        document.getElementById("txt_fname").value="";
        document.getElementById("txt_lname").value="";            
        document.getElementById("txt_nameoncard").value="";
        document.getElementById("txt_ccnumber").value="";
        document.getElementById("txt_cin").value="";
        document.getElementById("txt_amount").value="";
        document.getElementById("txt_description").value="";
        document.getElementById("ddl_clienttype").value=0;        
        document.getElementById("ddl_month").value=01;                     
        //document.getElementById("ddl_year").value=07;
        return false;
    }
    
    //In order to select appropriate card by entering num		
		function SelectCard()
		{	
			var cardnum=document.getElementById("txt_ccnumber").value;
			var ddl_cardtype=document.getElementById("ddl_cardtype");
			
			if (cardnum.charAt(0)==4)
			{
				ddl_cardtype.value=	1			
			}	
			if (cardnum.charAt(0)==5)
			{
				ddl_cardtype.value=	2			
			}		
			if (cardnum.charAt(0)==3)
			{
				ddl_cardtype.value=	3			
			}	
			if (cardnum.charAt(0)==6)
			{
				ddl_cardtype.value=	4			
			}	
		}
//    function ResetColor(x)
//    {      
//      document.getElementById("txt_lname").style.background="white";
//      return false;
//    }
  </script>
    

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
</head>

<body>
    <form id="form1" runat="server">
    <div id="DisableDive" style="display :none;position: absolute;left :1 ;top : 1;height : 1px;background-color :Silver; filter : alpha(opacity=50)" >
        <table width="100%" height="100%">                                                
        <tr>
            <td  style="width : 100%;height :100%" > 
            </td>
         </tr>
        </table>
    </div>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server"/>
       
    <TABLE id="Table2" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD colSpan="7">
                        &nbsp;<uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </TD>
				</TR>
				<TR>
					<TD colSpan="5">
						</TD>
					</TR>
					
					<TR>
						<TD style="HEIGHT: 1px" background="../Images/separator_repeat.gif" colSpan="4"></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="4" style="height: 300px">
                            <div style="text-align: center">
                            
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            <table id="allcreditcard" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable" style="width: 780px">
                                
                                <TR>
						        <td style="HEIGHT: 1px" background="../Images/separator_repeat.gif" colSpan="4"></td>
					            </TR>
					            <tr>
						        <td align="center" colSpan="4">
                                     <asp:Label ID="lbl_transmode" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label>
							    </td>
					            </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                <asp:label id="lblMessage" runat="server" Width="704px" Height="13px" ForeColor="Red"></asp:label></td>
                                </tr>
                            <tr>
                            <td style="height: 155px" align="right" valign="top" rowspan="">
                                <table id="tblcreditcard" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable" style="width: 318px">
                                    <tr class="clsLeftPaddingTable">
                                        <td class="clsaspcolumnheader" valign="top" style="width: 138px">
                                        </td>
                                        <td valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" height="20" valign="top" style="width: 138px">
                                            First
                                            Name</td>
                                        <td colspan="" height="20" rowspan="" valign="top">
                                            <asp:TextBox ID="txt_fname" runat="server" CssClass="clsinputadministration" MaxLength="100"
                                               Width="200px" TabIndex="2"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" height="20" style="width: 138px" valign="top">
                                            Last Name</td>
                                        <td colspan="1" height="20" rowspan="1" valign="top">
                                            <asp:TextBox ID="txt_lname" runat="server" CssClass="clsinputadministration" MaxLength="50"
                                             Width="200px" TabIndex="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" height="20" style="width: 138px" valign="top">
                                            Name On Card</td>
                                        <td colspan="1" height="20" rowspan="1" valign="top">
                                            <asp:TextBox ID="txt_nameoncard" runat="server" CssClass="clsinputadministration"
                                             onfocus="FillNameOnCard()" MaxLength="100" Width="200px" TabIndex="5"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" height="20" style="width: 138px" valign="top">
                                            CC Number</td>
                                        <td height="20" valign="top">
                                            <asp:TextBox ID="txt_ccnumber" runat="server" onblur="SelectCard();" CssClass="clsinputadministration" MaxLength="16"
                                                Width="200px" TabIndex="6"></asp:TextBox>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" valign="top" style="width: 138px; height: 20px">
                                            Expiry date</td>
                                        <td valign="top" style="height: 20px">
                                            <asp:DropDownList ID="ddl_month" runat="server" CssClass="clsinputcombo" Width="60px" TabIndex="7">
                                                <asp:ListItem Value="1">01</asp:ListItem>
                                                <asp:ListItem Value="2">02</asp:ListItem>
                                                <asp:ListItem Value="3">03</asp:ListItem>
                                                <asp:ListItem Value="4">04</asp:ListItem>
                                                <asp:ListItem Value="5">05</asp:ListItem>
                                                <asp:ListItem Value="6">06</asp:ListItem>
                                                <asp:ListItem Value="7">07</asp:ListItem>
                                                <asp:ListItem Value="8">08</asp:ListItem>
                                                <asp:ListItem Value="9">09</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            /
                                            <asp:DropDownList ID="ddl_year" runat="server" CssClass="clsinputcombo" Width="60px" TabIndex="8">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="clsleftpaddingtable" height="20" style="display: none; width: 138px;" valign="top">
                                            Card Type</td>
                                        <td height="20" style="display: none" valign="top">
                                            &nbsp;
                                            <asp:DropDownList ID="ddl_cardtype" runat="server" CssClass="clsinputcombo" Width="176px">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" height="21" valign="top" style="width: 138px">
                                            CIN</td>
                                        <td height="21">
                                            <asp:TextBox ID="txt_cin" runat="server" CssClass="clsinputadministration" MaxLength="4"
                                                 Width="76px" TabIndex="9"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="clsleftpaddingtable" height="1" style="width: 138px;" valign="top" align="left">
                                            Amount</td>
                                        <td height="1" style="height: 10px">
                                            <asp:TextBox ID="txt_amount" runat="server" CssClass="clsinputadministration" MaxLength="5"
                                                Width="76px" TabIndex="10"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="height: 155px" valign="top"><table id="Table1" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable" style="width: 330px">
                                <tr class="clsLeftPaddingTable">
                                    <td class="clsaspcolumnheader" valign="top" style="width: 138px">
                                    </td>
                                    <td valign="top">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="clsleftpaddingtable" height="20" style="display: none; width: 138px;" valign="top">
                                        Card Type</td>
                                    <td height="20" style="display: none" valign="top">
                                        &nbsp;
                                        <asp:DropDownList ID="DropDownList3" runat="server" CssClass="clsinputcombo" Width="176px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="clsleftpaddingtable" height="1" style="height: 10px; width: 138px;" valign="top" align="left">
                                            Client type</td>
                                    <td height="1" style="height: 10px">
                                        <asp:DropDownList ID="ddl_clienttype" runat="server" CssClass="clsinputcombo" Width="244px" TabIndex="11">
                                            <asp:ListItem Selected="True" Value="0">------------------ Select ----------------</asp:ListItem>
                                            <asp:ListItem Value="1">Traffic</asp:ListItem>
                                            <asp:ListItem Value="2">Oscar Client</asp:ListItem>
                                            <asp:ListItem Value="3">Lisa Client</asp:ListItem>
                                            <asp:ListItem Value="4">Oher</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                    <tr>
                                        <td align="left" class="clsleftpaddingtable" height="1" valign="top" style="width: 138px">
                                            Description<br />
                                            <br />
                                            <br />
                                            <table border="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 100px">
                                                        <asp:ImageButton ID="imgbtn_creditcard" runat="server" Height="21px" ImageUrl="~/Images/creditcard1.jpg"
                                                            Width="21px" TabIndex="4" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td height="1" valign="top">
                                            <asp:TextBox ID="txt_description" runat="server" CssClass="clsinputadministration"
                                                Height="107px" MaxLength="500" TextMode="MultiLine"
                                                Width="244px" TabIndex="12"></asp:TextBox></td>
                                    </tr>
                            </table>
                            
                            </td>
                           </tr>
                                <tr>
                                    <td align="center" valign="top">
							</td>
                                    <td align="center" valign="top">
                                        <asp:Button ID="btn_submit" runat="server" CssClass="clsbutton" Text="Submit"
                                            Width="80px" OnClick="btn_submit_Click" TabIndex="13" />&nbsp;<asp:Button ID="btn_reset" runat="server" CssClass="clsbutton"
                                                EnableViewState="False" Text="Reset" Width="46px" TabIndex="14" /></td>
                                </tr>
                            </table>
                                <table id="tbl_plzwait" class="clssubhead" style="display: none; height: 60px" width="100%">
                                    <tr>
                                        <td align="center" class="clssubhead" valign="middle">
                                            <img src="../Images/plzwait.gif" />
                                            &nbsp; Please wait while we process your request.
                                        </td>
                                    </tr>
                                </table>
                                
                          </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btn_submit" EventName="Click" />
            </Triggers>
   </asp:UpdatePanel>
                            </div>
                            <table>
                                <tr>
                                    <td width="780">
                                        <br />
                                        <uc2:Footer ID="Footer1" runat="server" />
                                        <asp:HiddenField ID="hf_transactionmode" runat="server" />
                                   <asp:Panel ID="PopupPanel" runat="server" Height="50px" Width="125px" style="position :absolute;top :1000; left : 400px;display :none" >
                                        <table id="tbl_telnos" cellpadding="0"  cellspacing="1"  style="width: 348px;background-color :White; border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;" border="0">
                                            <tr >
                                                <td class="clsLeftPaddingTable" colspan="2" style="height: 34px; background-image: url(../Images/subhead_bg.gif);" background="../images/headbar_headerextend.gif" valign="middle">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td class="clssubhead">
                                                               <strong> Credit Card Reader </strong></td>
                                                            <td align="right">
                                                                &nbsp;<asp:LinkButton ID="lnkbtnpclose" runat="server" OnClientClick='return closereaderpopup("1");'>X</asp:LinkButton>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                   <strong> Card Holder Name:</strong></td>
                                                <td style="height: 20px" class="clsLeftPaddingTable">
                                            <asp:Label ID="lbl_cardholdername" runat="server" Width="151px" CssClass="clsinputadministration"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                   <strong> Card Number: </strong></td>
                                                <td style="height: 24px" class="clsLeftPaddingTable">
                                                    <asp:Label ID="lbl_cardno" runat="server" Width="151px" CssClass="clsinputadministration"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 28px;" class="clsLeftPaddingTable">
                                                  <strong> Expiration Date:</strong></td>
                                                <td class="clsLeftPaddingTable" style="width: 220px; height: 28px">
                                                   <strong> Month</strong>
                                            <asp:Label ID="lbl_expmonth" runat="server" Width="47px" CssClass="clsinputadministration"></asp:Label>&nbsp;
                                                  <strong>  Year </strong>
                                                    <asp:Label ID="lbl_expyear" runat="server" Width="47px" CssClass="clsinputadministration"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                </td>
                                                <td id="td_txtcreditcard" runat="server" style="width: 200px; height: 36px; display:block" class="clsLeftPaddingTable" valign="bottom">
                                                    <asp:Button ID="btn_popupCancel" runat="server" Text="Cancel" CssClass="clsButton" Width="54px" />
                                                    <asp:Button ID="btn_popupok" runat="server" Text="Ok" CssClass="clsButton" Width="54px" /><asp:TextBox ID="txt_creditcard" runat="server" Height="1px" Width="1px" CssClass="clsinputadministration"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                </td>
                                                <td class="clsLeftPaddingTable" style="width: 200px; height: 10px">
                                        </td>
                                            </tr>
                                        </table>
                             </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </TD>
					</TR>
				</TABLE>
   
    </form>
    
</body>
</html>

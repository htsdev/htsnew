﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Activities;
// Yasir Kamal 5427 02/06/2009 Pagging added for dataGrid.
using lntechNew.WebControls;


namespace HTP.Activities
{
    /// <summary>
    /// Summary description for setcalls.
    /// </summary>
    public partial class SetCalls : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clscalls clscall = new clscalls();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        string SessionRecid;
        private DataTable dtReminderStatus;



        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                chkBondClient.Attributes.Add("onclick", "IsCheck(1)");
                chkRegulerClient.Attributes.Add("onclick", "IsCheck(2)");

                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    SessionRecid = Convert.ToString(Session["ReminID"]);

                    if (Page.IsPostBack != true)
                    {
                        cal_EffectiveFrom.SelectedDate = DateTime.Today.Subtract(new TimeSpan(3, 0, 0, 0)); //setting the date
                        if (SessionRecid == "")					 //If not redirecting from Notes Page
                            Session["ReminID"] = "1";		 //initialize Session RecID	
                        GetReminderStatus(); //fills reminder status drop down list

                    }
                    // Yasir Kamal 5427 01/23/2009 Pagging functionality Added.
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    PagingControl.grdType = GridType.DataGrid;
                    Pagingctrl.DataGrid = dg_ReminderCalls;
                    // Yasir Kamal 5427  end.
                }

                Pagingctrl.Visible = true;

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddl_rStatus.SelectedIndexChanged += new System.EventHandler(this.ddl_rStatus_SelectedIndexChanged);
            this.btn_update1.Click += new System.EventHandler(this.btn_update1_Click);
            this.lbRefresh.Click += new EventHandler(this.btn_update1_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        private void FillGrid() // Filling the Grid
        {
            try
            {
                int Mode = 0;
                if (this.chkBondClient.Checked && (!(this.chkRegulerClient.Checked)))
                    Mode = 1;  //1 for only bond clients.
                else if (this.chkRegulerClient.Checked && (!(this.chkBondClient.Checked)))
                    Mode = 2;  //2 for only reguler clients.
                else if (this.chkBondClient.Checked && this.chkRegulerClient.Checked)
                    Mode = 3; //3 for all clients.                               
                DataSet ds = clscall.remindercallgrid(cal_EffectiveFrom.SelectedDate, Convert.ToInt32(ddl_rStatus.SelectedValue), cb_showall.Checked, 1, Convert.ToInt32(dd_language.SelectedValue.ToString()), Mode);

                //Yasir Kamal 5499 01/23/2009 Add S.No to Set Call Report.

                DataColumn dc = new DataColumn("SNo");
                dc.DataType = typeof(Int32);
                ds.Tables[0].Columns.Add(dc);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["Sno"] = i + 1;
                }
                //Yasir Kamal 5427 01/23/2009 Pagging added.
                dg_ReminderCalls.DataSource = ds;

                try
                {
                    dg_ReminderCalls.DataBind();
                }
                catch
                {
                    if (dg_ReminderCalls.CurrentPageIndex > dg_ReminderCalls.PageCount - 1)
                    {
                        dg_ReminderCalls.CurrentPageIndex = dg_ReminderCalls.PageCount - 1;
                        dg_ReminderCalls.DataBind();

                    }
                }

                Pagingctrl.PageCount = dg_ReminderCalls.PageCount;
                Pagingctrl.PageIndex = dg_ReminderCalls.CurrentPageIndex;
                Pagingctrl.SetPageIndex();

                BindReport();

                if (dg_ReminderCalls.Items.Count == 0)
                {
                    lblMessage.Text = "No Records";
                    dg_ReminderCalls.Visible = false;
                }
                else
                {
                    dg_ReminderCalls.Visible = true;

                    if (dg_ReminderCalls.PageSize >= ds.Tables[0].Rows.Count)
                    {
                        dg_ReminderCalls.PagerStyle.Visible = false;

                    }
                    else
                    {
                        dg_ReminderCalls.PagerStyle.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {


                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);

            }
        }
        //Method To Generate Serial Number
        private void BindReport()
        {
            long sNo = (dg_ReminderCalls.CurrentPageIndex) * (dg_ReminderCalls.PageSize);
            try
            {
                foreach (DataGridItem ItemX in dg_ReminderCalls.Items)
                {
                    sNo += 1;

                    ((TextBox)(ItemX.FindControl("txt_sno"))).Text = sNo.ToString();
                }
                //To assign total num of records
                // Yasir Kamal 5427 01/31/2009 Pagging functionality Added.
                txt_totalrecords.Text = dg_ReminderCalls.Items.Count.ToString();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        //
        private void SetUrl() // setting the grid
        {
            try
            {
                foreach (DataGridItem items in dg_ReminderCalls.Items)
                {
                    // setting hyperlink
                    //((HyperLink)(items.FindControl("lnkName"))).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" + ((Label)(items.FindControl("lbl_TicketID"))).Text;

                    //setting Callbacks
                    Label lblCalls = (Label)(items.FindControl("lbl_CallBacks"));
                    GetAllReminderStatuses();
                    // Fahad 5503 03/02/2009 add if-else clause added
                    if (lblCalls.Text != null)
                    {
                        // Rab Nawaz 10914 07/01/2013 Added SMS Reply confirmed Status. . . 
                        if (lblCalls.Text == "0" || lblCalls.Text == "1" || lblCalls.Text == "2" || lblCalls.Text == "3" || lblCalls.Text == "4" || lblCalls.Text == "5" || lblCalls.Text == "6" || lblCalls.Text == "8")
                        {
                            DataRow dr = dtReminderStatus.Rows.Find(Convert.ToInt32(lblCalls.Text));
                            ((Label)(items.FindControl("lbl_CallBacks"))).Text = dr["Description"].ToString();
                        }
                    }
                    else
                    {
                        lblCalls.Text = "0";
                        DataRow dr = dtReminderStatus.Rows.Find(Convert.ToInt32(lblCalls.Text));
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = dr["Description"].ToString();
                    }

                    // tahir 5762 04/06/2009 added scroll bars..
                    ((HyperLink)(items.FindControl("lnk_Comments"))).NavigateUrl = "javascript:window.open('../QuickEntryNew/remindernotes.aspx?casenumber=" + ((Label)(items.FindControl("lbl_TicketID"))).Text + "&violationID=" + ((Label)(items.FindControl("lbl_TicketViolationID"))).Text + "&searchdate=" + ((Label)(items.FindControl("lbl_CourtDate"))).Text + "&Recid=" + ((TextBox)(items.FindControl("txt_sno"))).Text + "&calltype=" + "1" + "&genComments=" + (HiddenField)(items.FindControl("hf_GeneralComments")) + "','','status=yes,width=530,height=410,scrollbars=yes');void('');";

                    if (((Label)(items.FindControl("lbl_Comments"))).Text == "")
                    {
                        ((Label)(items.FindControl("lbl_Comments"))).Text = "No Comments";
                    }

                    // Firm setting
                    if (((Label)(items.FindControl("lbl_Firm"))).Text != "SULL")
                    {
                        ((Label)(items.FindControl("lbl_contact1"))).Text = ((Label)(items.FindControl("lbl_Firm"))).Text;
                        ((Label)(items.FindControl("lbl_contact1"))).Font.Bold = true;
                        ((Label)(items.FindControl("lbl_contact2"))).Visible = false; ;
                        ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                    }

                    // Contact no setting
                    else if
                        (((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("(") &&
                        ((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("(") &&
                        ((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("("))
                    {
                        ((Label)(items.FindControl("lbl_contact1"))).Text = "No contact";
                        ((Label)(items.FindControl("lbl_contact2"))).Visible = false; ;
                        ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                    }
                    else
                    {
                        if (((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact1"))).Visible = false;
                        }
                        if (((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact2"))).Visible = false;
                        }
                        if (((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                        }
                    }
                    // Bond flag setting
                    if (((Label)items.FindControl("lbl_BondFlag")).Text == "1")
                    {
                        ((Label)items.FindControl("lbl_Bond")).Visible = true;
                    }
                    // Owes setting
                    if (((Label)(items.FindControl("lbl_owes"))).Text.StartsWith("$0") == false)
                    {
                        ((Label)(items.FindControl("lbl_owes"))).Text = "(" + ((Label)(items.FindControl("lbl_owes"))).Text + ")";
                        ((Label)(items.FindControl("lbl_owes"))).Visible = true;
                    }
                    //Insurance settings
                    if (((Label)items.FindControl("lbl_Insurance")).Text.Length > 0)
                    {
                        ((Label)items.FindControl("lbl_Insurance")).Visible = true;
                    }
                    //child belt settings
                    if (((Label)items.FindControl("lbl_Child")).Text.Length > 0)
                    {
                        ((Label)items.FindControl("lbl_Child")).Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        private void ddl_rStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        private void btn_update1_Click(object sender, System.EventArgs e)
        {
            try
            { //Yasir Kamal 5427 02/06/2009 Pagging functionality Added.
                if (sender == btn_update1)
                {
                    dg_ReminderCalls.CurrentPageIndex = 0;
                }
                
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
                tdData.Style.Add("display", "block");
                //tdWait.Style.Add("display", "none");
                // Yasir 5427 end

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        private void GetReminderStatus()
        {
            DataTable dtStatus = clscall.GetReminderStatus();

            if (dtStatus.Rows.Count > 0)
            {
                ddl_rStatus.Items.Clear();
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                    ddl_rStatus.Items.Add(item);
                }
                ListItem itm = new ListItem("All", "5");
                ddl_rStatus.Items.Add(itm);
            }
        }

        private void GetAllReminderStatuses()
        {
            dtReminderStatus = clscall.GetReminderStatuses();
            dtReminderStatus.Constraints.Add("PrimaryKeyConstraint", dtReminderStatus.Columns[0], true);
        }

        protected void ddl_rStatus_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }


        //Yasir Kamal 5427 01/23/2009 Pagging functionality Added.
        /// <summary>
        /// Method to handle Pagging Control pageIndexChange Event.
        /// </summary>

        protected void Pagingctrl_PageIndexChanged()
        {
            dg_ReminderCalls.CurrentPageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
            SetUrl();


        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>

        protected void dg_ReminderCalls_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_ReminderCalls.CurrentPageIndex = e.NewPageIndex;
                    FillGrid();
                    SetUrl();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="pageSize"></param>

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_ReminderCalls.CurrentPageIndex = 0;
                dg_ReminderCalls.PageSize = pageSize;
                dg_ReminderCalls.AllowPaging = true;

            }
            else
            {
                dg_ReminderCalls.AllowPaging = false;
            }

            FillGrid();
            SetUrl();

        }

        // 5427 end
    }
}


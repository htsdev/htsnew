using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Activities
{
    public partial class ServiceTicketCategories : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        ServiceTicketComponent STcategories = new ServiceTicketComponent();

        protected void Page_Load(object sender, EventArgs e)
        {

            //Waqas 5057 03/17/2009 Checking employee info in session               
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            lbl_Message.Text = "";
            try
            {
                //Sabir Khan 6431 08/21/2009 To stop page further execution for secondary user...
                if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        BindUser();
                        GetRecords();
                        MultiView1.SetActiveView(v_Add);
                        // Noufil 4632 09/18/2008 Filling the check boxes with case type
                        Fillcheckbox(Cblist_casetype);
                        Fillcheckbox(cblist_updatecasetype);
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void GetRecords()
        {
            try
            {
                // Noufil 4632 09/18/2008 using class to get records
                DataTable dt = STcategories.GetServiceTicketsWithCasetype();
                if (dt.Rows.Count > 0)
                {
                    gvServiceTicketCategories.DataSource = dt;
                    gvServiceTicketCategories.DataBind();
                }
                else
                    lbl_Message.Text = "No Records Found!";
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void gvServiceTicketCategories_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    // Noufil 4632 09/18/2008 using class to get records
                    DataTable temp = STcategories.CheckServiceticket(e.CommandArgument.ToString());
                    if (Convert.ToInt32(temp.Rows[0]["RecCount"]) == 0)
                    {
                        // Noufil 4632 09/18/2008 using class to delete record
                        STcategories.DeleteServiceTicketCategory(e.CommandArgument.ToString());
                        ViewState["UpdateFlag"] = 0;
                        GetRecords();
                    }
                    else
                    {
                        lbl_Message.Text = "Some records with this category exist in the database. The category can not be deleted.";
                    }
                }
                if (e.CommandName == "Select")
                {
                    GridView gv = (GridView)sender;
                    gv.SelectedIndex = Convert.ToInt32(e.CommandArgument) - 1;
                    ViewState["UpdateFlag"] = 1;
                    ViewState["CategoryID"] = ((HiddenField)gv.SelectedRow.FindControl("hf_CategoryID")).Value;
                    ViewState["Description"] = ((LinkButton)gv.SelectedRow.FindControl("lbtn_Desc")).Text;
                    ViewState["AssignedUser"] = ((Label)gv.SelectedRow.FindControl("lbl_AssignedUser")).Text;//Fahad 6054 07/25/2009 Add AssignTo field through dropdown selected value
                    MultiView1.SetActiveView(v_Update);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                // Sadaf Aziz 10233 12/05/2012 Restrict to add same category name

                if (STcategories.IsServiceTicketCategoryExist(txtNewCategory.Text) == true)
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "jscript",
                                                           "<script language=\"javascript\">alert(\"Category name already exists, please try different.\");</script>");
                    lbl_Message.Text = "Category name already exists, please try different.";
                    return;
                }
                foreach (ListItem i in Cblist_casetype.Items)
                {
                    if (i.Selected)
                    {
                        STcategories.AddServiceCategory(txtNewCategory.Text, Convert.ToInt32(i.Value), Convert.ToInt32(ddlUserAdd.SelectedValue));
                    }
                }
                ClearTextBoxes();
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gvServiceTicketCategories_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void v_Update_Activate(object sender, EventArgs e)
        {
            ClearTextBoxes();
            lblCategory.Text = Convert.ToString(ViewState["Description"]);
            txtCategoryUpdate.Text = Convert.ToString(ViewState["Description"]);
            //Nasir 6451 11/11/2009 implement Check if empty select choose
            ddlUserUpdate.SelectedValue = !(ViewState["AssignedUser"].ToString() == "" || ViewState["AssignedUser"] == null) ? Convert.ToString(ViewState["AssignedUser"]) : "0";//Fahad 6054 07/25/2009 Add AssignTo field through dropdown selected value
            DataTable dt = STcategories.GetCaseTypeByCategory(Convert.ToString(ViewState["CategoryID"]));
            foreach (DataRow r in dt.Rows)
            {
                foreach (ListItem lstitem in cblist_updatecasetype.Items)
                {
                    if (lstitem.Value == r[0].ToString())
                        lstitem.Selected = true;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTextBoxes();
                MultiView1.SetActiveView(v_Add);

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void v_Add_Activate(object sender, EventArgs e)
        {
            try
            {
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // Sadaf Aziz 10233 12/05/2012 Restrict to add same category name
                // faique Ali 10233 01/24/2013 change viewstate varialbe,old veriable was not defined properly therefor that was giving null values..
                if (ViewState["Description"].ToString().ToLower().Trim().Equals(txtCategoryUpdate.Text.ToLower().Trim()))
                    if (STcategories.IsServiceTicketCategoryExist(txtCategoryUpdate.Text) == true)
                    {
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "jscript",
                                                               "<script language=\"javascript\">alert(\"Category name already exists, please try different.\");</script>");
                        lbl_Message.Text = "Category name already exists, please try different.";
                        return;
                    }
                // Noufil 4782 Family law case type updated
                //Fahad 6054 07/25/2009 Fahad 6054 07/25/2009 Add AssignTo field through dropdown selected value parameter in the end
                STcategories.UpdateServiceTicketCategories(Convert.ToString(ViewState["CategoryID"]), txtCategoryUpdate.Text, Convert.ToInt32(cblist_updatecasetype.Items[0].Selected), Convert.ToInt32(cblist_updatecasetype.Items[1].Selected), Convert.ToInt32(cblist_updatecasetype.Items[2].Selected), Convert.ToInt32(cblist_updatecasetype.Items[3].Selected), Convert.ToInt32(ddlUserUpdate.SelectedValue));
                ClearTextBoxes();
                MultiView1.SetActiveView(v_Add);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void ClearTextBoxes()
        {
            txtNewCategory.Text = "";
            txtCategoryUpdate.Text = "";
            ddlUserAdd.SelectedValue = "0";
            ddlUserUpdate.SelectedValue = "0";
            foreach (ListItem list in cblist_updatecasetype.Items)
            {
                list.Selected = false;
            }
            foreach (ListItem list in Cblist_casetype.Items)
            {
                list.Selected = false;
            }

        }

        // filling the check box with case type
        private void Fillcheckbox(CheckBoxList CBlist)
        {
            CaseType Ctype = new CaseType();
            CBlist.DataSource = Ctype.GetAllCaseType();
            CBlist.DataBind();
        }
        //Fahad 6054 07/22/2009 populate user DropDown list by adding all Users 
        private void BindUser()
        {
            clsUser objClsUsers = new clsUser();
            DataSet dsUser = objClsUsers.getAllUsersList();
            ddlUserAdd.DataSource = dsUser;
            ddlUserAdd.DataTextField = dsUser.Tables[0].Columns["UserName"].ToString();
            ddlUserAdd.DataValueField = dsUser.Tables[0].Columns["Employeeid"].ToString();
            ddlUserAdd.DataBind();
            ddlUserAdd.Items.Insert(0, new ListItem("--Choose--", "0"));

            ddlUserUpdate.DataSource = dsUser;
            ddlUserUpdate.DataTextField = dsUser.Tables[0].Columns["UserName"].ToString();
            ddlUserUpdate.DataValueField = dsUser.Tables[0].Columns["Employeeid"].ToString();
            ddlUserUpdate.DataBind();
            ddlUserUpdate.Items.Insert(0, new ListItem("--Choose--", "0"));
        }


    }
}

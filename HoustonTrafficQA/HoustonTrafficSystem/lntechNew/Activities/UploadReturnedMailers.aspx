<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadReturnedMailers.aspx.cs"
    Inherits="HTP.Activities.UploadReturnedMailers" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Upload Returned Mailers</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
    function Validate()
    {
        // Sabir Khan 6232 07/28/2009 Validtion for Mail Type...
        if(document.getElementById("ddl_uploadmailer").value == "-1")
         {
            alert("Please select a returned mail type.");
            document.getElementById("ddl_uploadmailer").focus();
            return false;
         }
        // if no file is selected........
        if ((document.getElementById("FileUpload").value=="") || (document.getElementById("FileUpload").value ==null) )
         {
            alert("Please Select file for uploading.");
            document.getElementById("FileUpload").focus();
            return false;
         }
         
         // tahir 4417 07/17/2008
         // validating file type.. 
         // only text files with ".txt" are allowed....
         var sFileName =  document.getElementById("FileUpload").value;
         if (sFileName.lastIndexOf(".txt") == -1)
         {
                alert("Only text files with TXT extensions are allowed.");
                Reset();
                document.getElementById("FileUpload").focus();
                return false;
         }
         
         
    }


    // tahir 4417 07/17/2008
    // clear controls and reset the page....
    function Reset()
    {
        var who=document.getElementsByName("FileUpload")[0]; 
        var who2= who.cloneNode(false); 
        who2.onchange= who.onchange; 
        who.parentNode.replaceChild(who2,who); 
        document.getElementById("ddl_uploadmailer").value = "-1";  //sabir Khan  6232 07/28/2009 Reset Mail Type ....
        document.getElementById("lblMessage").innerText = "";
        return false;
    }
    
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" width="780" cellspacing="0" cellpadding="0" align="center"
            border="0">
            <tbody>
                <tr>
                    <td>
                        <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" background="../Images/separator_repeat.gif" style="width: 100%;
                                    height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td class="clsLeftPaddingTable" align="center" colspan="2" style="height: 100px">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td  class="clssubhead">
                                            Returned mail type
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_uploadmailer" runat="server"  CssClass="clsInputCombo">
                                            <asp:ListItem Text="---- Choose ----" Selected="True" Value = "-1"></asp:ListItem>
                                            <asp:ListItem Text="Returned Mail - Regular"  Value = "0"></asp:ListItem>
                                            <asp:ListItem Text="Returned Mail - USPS IMB"  Value = "2"></asp:ListItem>
                                            </asp:DropDownList>      
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                        <tr>
                                            <td class="clssubhead">
                                                <asp:Label ID="Label1" runat="server" Text="Upload File to Mailing List"></asp:Label>
                                                &nbsp; &nbsp;
                                            </td>
                                            <td align="right">
                                                <asp:FileUpload ID="FileUpload" runat="server" CssClass="clsInputadministration"
                                                    ToolTip="Select a file" Width="350px" BorderWidth="1px" Height="20px" />
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:Button ID="btnsave" runat="server" OnClick="btnsave_Click" Text="Upload" CssClass="clsbutton" />
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="clsbutton" 
                                                    onclientclick="return Reset();" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="clsLeftPaddingTable" align="center" colspan="2">
                                    &nbsp;
                                    <asp:Label ID="lblMessage" runat="server" Width="735px" Height="16px" 
                                        ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" background="../Images/separator_repeat.gif" style="width: 100%;
                                    height: 10px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="5" class="Label">
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for Dockets1.
	/// </summary>
	public partial class Dockets1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lbl_error;
		protected eWorld.UI.CalendarPopup calstartdate;
		protected eWorld.UI.CalendarPopup calenddate;
		protected System.Web.UI.WebControls.DataGrid dgresult;
		protected System.Web.UI.WebControls.DropDownList ddl_attorney;
	
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
        clsENationWebComponents ClsDB = new clsENationWebComponents("returnDockets");
			//clsENationWebComponents ClsDB = new clsENationWebComponents();
		
		
		private void Page_Load(object sender, System.EventArgs e)

		{
			// Fill Data				
			try
			{	
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
				
					lbl_error.Text="";
				
					if ( ! IsPostBack )
					{
						DataSet ds = new DataSet();
		
						// Getting List of Attorneys To Be Filled In DropDown List
						ds = ClsDB.Get_DS_BySP("USP_HTS_list_of_Atorneys");

						ddl_attorney.DataSource=ds;
				
						ddl_attorney.DataTextField="IMPORTANCE";
						ddl_attorney.DataValueField="IMPORTANCE";
						ddl_attorney.DataBind();					
				
						ListItem ls = new ListItem("Select All","%" );
						ddl_attorney.Items.Insert(0,ls);
						//btnSearch.Attributes.Add("OnClick","return CalendarValidation();");
						btnSearch.Attributes.Add("OnClick", "return validate();");							
					}
				}
			}
			catch(Exception ex)
			{
				lbl_error.Text= ex.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.dgresult.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgresult_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		private void GenerateSerial()
		{
			// Procedure for writing serial numbers in "S.No." column in data grid.......	
			
			long sNo=(dgresult.CurrentPageIndex)*(dgresult.PageSize);									
			try
			{
				// looping for each row of record.............
				foreach (DataGridItem ItemX in dgresult.Items) 
				{ 					
					sNo +=1 ;
					// setting text of hyper link to serial number after bouncing of hyper link...
					((Label)(ItemX.FindControl("lbl_sno"))).Text  = (string) Convert.ChangeType(sNo,typeof(string));
				 
					Label docid=(Label)ItemX.FindControl("lbl_docid");
				
					//((LinkButton)ItemX.FindControl("lnkbtn_date")).Attributes.Add("OnClick", "return PopUpCallBack(" + docid.Text +"  );");
					((LinkButton)ItemX.FindControl("lnkbtn_docdate")).Attributes.Add("OnClick", "return PopUpCallBack(" + docid.Text +"  );");
					((Label)(ItemX.FindControl("lbl_pageno"))).Text += "  Pages";			
				
				}
			}
			catch(Exception ex)

			{
				lbl_error.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}	
		
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			try
			{
	

				string[] keys={ "@stdate","@enddate","@StrAtoreny"};
				object[] values={calstartdate.SelectedDate.ToShortDateString(),calenddate.SelectedDate.ToShortDateString(),ddl_attorney.SelectedValue};
							
					

				string test	= ddl_attorney.SelectedValue;

				// Getting List of Dockets	
	
				DataSet ds =ClsDB.Get_DS_BySPArr("USP_HTS_List_Docket",keys,values);
			
				if (ds.Tables.Count > 0 )
				{
		
					if ( ds.Tables[0].Rows.Count > 0)
					{
						dgresult.DataSource = ds.Tables[0];
						dgresult.DataBind();
						GenerateSerial();
					}
					else
					{
						dgresult.DataSource="";
						dgresult.DataBind();	
						lbl_error.Text="No Record Found";
					}
	
				}
				else
					lbl_error.Text="No Record Found";	
			}

			catch ( Exception ex)
			{
				lbl_error.Text= ex.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}	

		private void dgresult_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				// Highlight Current DataGrid Row
				
				if (e.Item.ItemType == ListItemType.Header) 
				{
		
				}
				else
				{
					if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
					{
						e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
					}
					
					if (e.Item.ItemType == ListItemType.Item)
					{
						e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
					}
					else
					{
						e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
					}
				}
			}
			catch (Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
	}
}

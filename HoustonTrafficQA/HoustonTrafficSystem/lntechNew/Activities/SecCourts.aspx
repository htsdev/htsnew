<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.SecCourts" Codebehind="SecCourts.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Courts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			function Submit() 
			{

				if (frmcourts.txt_CourtName.value == "") 
				{
					alert("Please specify Court Name");
					frmcourts.txt_CourtName.focus();
					return false;
				}

				if (frmcourts.txt_JudgeName.value == "") 
				{
					alert("Please specify Judge Name");
					frmcourts.txt_JudgeName.focus();
					return false;
				}
			
				if (frmcourts.txt_ShortName.value == "") 
				{
					alert("Please specify Court Short Name");
					frmcourts.txt_ShortName.focus();
					return false;
				}
				
				if (frmcourts.txt_Address1.value == "") 
				{
					alert("Please specify your Address");
					frmcourts.txt_Address1.focus();
					return false;
				}
				
				if (frmcourts.txt_City.value == "") 
				{
					alert("Please specify Court City");
					frmcourts.txt_City.focus();
					return false;
				}	

				if (frmcourts.ddl_States.value == 0) 
				{
					alert("Please specify Court State");
					frmcourts.ddl_States.focus();
					return false;
				}

				if (frmcourts.txt_ZipCode.value == "") 
				{
					alert("Please specify Court Zip");
					frmcourts.txt_ZipCode.focus();
					return false;
				}	
				
				if (frmcourts.txt_DDName.value == "") 
				{
					alert("Please specify Drop Down Name");
					frmcourts.txt_DDName.focus();
					return false;
				}
				
				var intphonenum1 = frmcourts.txt_CC11.value;
				var intphonenum2 = frmcourts.txt_CC12.value;
				var intphonenum3 = frmcourts.txt_CC13.value;
				var intphonenum4 = frmcourts.txt_CC14.value;
				
				if  ((intphonenum1 == "") || (intphonenum2 == "") || (intphonenum3 == "")) 
				{
					alert("Invalid Phone Number. Please don't use any dashes or space.");
					frmcourts.txt_CC11.focus();
					return false;
				}

				if (isNaN(intphonenum1) == true )
				{
					alert("Invalid Phone Number. Please don't use any dashes or space");
					frmcourts.txt_CC11.focus();
					return false;
				}
				if (isNaN(intphonenum2) == true )
				{
					alert("Invalid Phone Number. Please don't use any dashes or space");
					frmcourts.txt_CC12.focus();
					return false;
				}
				if (isNaN(intphonenum3) == true )
				{
					alert("Invalid Phone Number. Please don't use any dashes or space");
					frmcourts.txt_CC13.focus();
					return false;
				}
				/*if (isNaN(intphonenum4) == true )
				{
					alert("Invalid Phone Number. Please don't use any dashes or space");
					frmcourts.txt_CC14.focus();
					return false;
				}*/
							
				var intphonenum1 = frmcourts.txt_CC1.value;
				var intphonenum2 = frmcourts.txt_CC2.value;
				var intphonenum3 = frmcourts.txt_CC3.value;
								
				if (isNaN(intphonenum1) == true )
				{
					alert("Invalid Fax Number. Please don't use any dashes or space");
					frmcourts.txt_CC1.focus();
					return false;
				}
				if (isNaN(intphonenum2) == true )
				{
					alert("Invalid Fax Number. Please don't use any dashes or space");
					frmcourts.txt_CC2.focus();
					return false;
				}
				if (isNaN(intphonenum3) == true )
				{
					alert("Invalid Fax Number. Please don't use any dashes or space");
					frmcourts.txt_CC3.focus();
					return false;
				}
				
				if (frmcourts.ddl_SettingRequest.value == 0) 
				{
					alert("Please specify Setting Request");
					return false;
				}

				if (isNaN(frmcourts.txt_VisitCharges.value) == true ) 
				{
					alert("Sorry Invalid Additional Amount");
					return false;
				}
			}
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<FORM id="frmcourts" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<TR>
					<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<tr>
				    <td width="780" background="../../images/separator_repeat.gif"  height="11" align="center" id="lbl_Message">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
				</tr>
				<tr>
					<td>
						<table class="clsleftpaddingtable" width="100%" border="0">
							<tr>
								<td class="clsLabelNew" width="180">Court Name:
								</td>
								<td width="210"><asp:textbox id="txt_CourtName" runat="server" Width="200" CssClass="clsinputadministration" MaxLength="25"></asp:textbox></td>
								<td class="clsLabelNew" width="180">Court Contact:
								</td>
								<td width="210">&nbsp;
									<asp:textbox id="txt_CourtContact" runat="server" Width="194px" CssClass="clsinputadministration" MaxLength="15"></asp:textbox></td>
							</tr>
							<tr>
								<td class="clsLabelNew" style="HEIGHT: 20px">Judge Name:
								</td>
								<td style="WIDTH: 215px; HEIGHT: 20px"><asp:textbox id="txt_JudgeName" runat="server" Width="200px" CssClass="clsinputadministration" MaxLength="20"></asp:textbox></td>
								<td class="clsLabelNew" style="HEIGHT: 20px">Telephone:
								</td>
								<td style="HEIGHT: 20px">&nbsp;
									<asp:textbox id="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px"
										CssClass="clsinputadministration"></asp:textbox>-
									<asp:textbox id="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px"
										CssClass="clsinputadministration"></asp:textbox>-
									<asp:textbox id="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server" Width="32px"
										CssClass="clsinputadministration"></asp:textbox>x
									<asp:textbox id="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server" Width="40px"
										CssClass="clsinputadministration"></asp:textbox></td>
							</tr>
							<tr>
								<td class="clsLabelNew" style="HEIGHT: 21px">Short Name:
								</td>
								<td style="WIDTH: 215px; HEIGHT: 21px"><asp:textbox id="txt_ShortName" runat="server" Width="200px" CssClass="clsinputadministration" MaxLength="20"></asp:textbox></td>
								<td class="clsLabelNew" style="HEIGHT: 21px">Fax:
								</td>
								<td style="HEIGHT: 21px">&nbsp;
									<asp:textbox id="txt_CC1" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px"
										CssClass="clsinputadministration"></asp:textbox>-
									<asp:textbox id="txt_CC2" onkeyup="return autoTab(this, 3, event)" runat="server" Width="32px"
										CssClass="clsinputadministration"></asp:textbox>-
									<asp:textbox id="txt_CC3" onkeyup="return autoTab(this, 4, event)" runat="server" Width="32px"
										CssClass="clsinputadministration"></asp:textbox></td>
							</tr>
							<tr>
								<td class="clsLabelNew" style="HEIGHT: 21px">Address #1:
								</td>
								<td style="WIDTH: 215px; HEIGHT: 21px"><asp:textbox id="txt_Address1" runat="server" Width="200px" CssClass="clsinputadministration" MaxLength="100"></asp:textbox></td>
								<td class="clsLabelNew" style="HEIGHT: 21px">Setting Request:
								</td>
								<td style="HEIGHT: 21px">&nbsp;
									<asp:dropdownlist id="ddl_SettingRequest" runat="server" Width="128px" CssClass="clsinputcombo"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="clsLabelNew" style="HEIGHT: 17px">Address #2:
								</td>
								<td style="WIDTH: 215px; HEIGHT: 17px"><asp:textbox id="txt_Address2" runat="server" Width="200px" CssClass="clsinputadministration" MaxLength="100"></asp:textbox></td>
								<td class="clsLabelNew" style="HEIGHT: 17px">Bond Type:
								</td>
								<td style="HEIGHT: 17px">&nbsp;
									<asp:dropdownlist id="ddl_BondType" runat="server" Width="128px" CssClass="clsinputcombo"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="clsLabelNew" style="HEIGHT: 23px">City,State,Zip:
								</td>
								<td style="WIDTH: 215px; HEIGHT: 23px"><asp:textbox id="txt_City" runat="server" Width="80px" CssClass="clsinputadministration" Height="19" MaxLength="15"></asp:textbox><asp:dropdownlist id="ddl_States" runat="server" Width="48px" CssClass="clsinputcombo" Height="19"></asp:dropdownlist><asp:textbox id="txt_ZipCode" runat="server" Width="40px" CssClass="clsinputadministration" Height="19"
										MaxLength="5"></asp:textbox></td>
								<td class="clsLabelNew" style="HEIGHT: 23px">Additional:
								</td>
								<td style="HEIGHT: 23px">&nbsp; $
									<asp:textbox id="txt_VisitCharges" runat="server" Width="42px" CssClass="clsinputadministration" MaxLength="5"></asp:textbox>&nbsp;
									<asp:checkbox id="chkb_InActive" runat="server" CssClass="clslabelnew" Text="InActive"></asp:checkbox></td>
							</tr>
                            <tr>
                                <td class="clsLabelNew">
                                    Drop Down Name
                                </td>
                                <td style="width: 215px">
                                    <asp:textbox id="txt_DDName" runat="server" Width="200px" CssClass="clsinputadministration" MaxLength="20"></asp:textbox></td>
                                <td class="clsLabelNew">
                                    Letter Of Rep</td>
                                <td>
                                    &nbsp;<ew:CalendarPopup ID="dtpCourtDate" runat="server" AllowArbitraryText="False"
                                        CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                        Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                        PadSingleDigits="True" SelectedDate="2006-03-14" ShowGoToToday="True" Text=" "
                                        ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00" Width="80px" LowerBoundDate="">
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                            </tr>
							<tr>
								<td class="clsLabelNew">
								</td>
								<td style="WIDTH: 215px"></td>
								<td><asp:button id="btn_Submit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button><asp:button id="btn_Delete" runat="server" CssClass="clsbutton" Text="Delete" Enabled="False"></asp:button></td>
								<td><asp:label id="lblcourtID" runat="server" CssClass="label" Visible="False">0</asp:label><asp:CheckBox Runat="server" ID="chkcriminal" Text="Is Criminal Court" CssClass="clslabelnew"></asp:CheckBox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="lbl_Msg" runat="server" CssClass="label" ForeColor="Red"></asp:label></td>
				</tr>
				<TR>
					<TD background="../../images/separator_repeat.gif" height="10"></TD>
				</TR>
				<TR>
					<TD style="height: 144px">
						<TABLE id="tblgrid" cellSpacing="0" cellPadding="0"  border="0">
							<TR>
								<TD style="width:100%" ><asp:datagrid id="dg_Result" runat="server"  CssClass="clsleftpaddingtable" BorderStyle="None"
										PageSize="20" AutoGenerateColumns="False" BorderColor="White" OnItemDataBound="dg_Result_ItemDataBound" AllowSorting="True" OnSortCommand="dg_Result_SortCommand" Width="780px">
										<Columns>
											<asp:TemplateColumn HeaderText="Court Name" SortExpression="courtname">
												<HeaderStyle CssClass="clsaspcolumnheader"  ></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id=lnkbtnCourtNo runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ccourtname") %>' CommandName="CourtNo"></asp:LinkButton>
													<asp:Label id=lbl_CourtID runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Drop Down Name">
												<HeaderStyle CssClass="clsaspcolumnheader"  ></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=Label5 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.cshortcourtname") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Short Name">
												<HeaderStyle CssClass="clsaspcolumnheader" ></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblsname runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Setting Request">
												<HeaderStyle  CssClass="clsaspcolumnheader" ></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=Label2 runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.setreqdes") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Active">
												<HeaderStyle CssClass="clsaspcolumnheader" ></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblInactive runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.inactiveflag") %>'>
													</asp:Label>
												</ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Criminal">
												<HeaderStyle CssClass="clsaspcolumnheader"  ></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_IsCriminalCourt runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.IsCriminalCourt") %>'>
													</asp:Label>
												</ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
											</asp:TemplateColumn>
											
											<asp:TemplateColumn HeaderText="Rep'sLetterDate">
												<HeaderStyle CssClass="clsaspcolumnheader"  ></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_letterofrep runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.RepDate","{0:d}") %>'>
													</asp:Label>
												</ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
											</asp:TemplateColumn>
											
										</Columns>
										<PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif"  height="11"></TD>
				</TR>
				<TR>
					<TD style="height: 48px" ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>

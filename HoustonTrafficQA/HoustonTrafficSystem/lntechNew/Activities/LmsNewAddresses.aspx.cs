﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;


namespace lntechNew.Activities
{
    // Rab Nawaz Khan 10431 10/02/2013 Added the LMS Address Check Report.
    /// <summary>
    /// LMS Address Check Report
    /// </summary>
    public partial class LmsNewAddresses : System.Web.UI.Page
    {
        readonly clsSession _clsSession = new clsSession();
        readonly ClsNonClientsManager _clsNonclients = new ClsNonClientsManager();

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    ViewState["vEmpID"] = _clsSession.GetCookie("sEmpID", this.Request);
                    if (Page.IsPostBack != true)
                    {
                        cal_EffectiveFrom.SelectedDate = DateTime.Today.Date; //setting the date 
                        cal_EffectiveTo.SelectedDate = DateTime.Today.Date;
                        ddl_letterType.DataSource = _clsNonclients.FillLetterTypes();
                        ddl_letterType.DataTextField = "LetterName";
                        ddl_letterType.DataValueField = "ID";
                        ddl_letterType.DataBind();
                        if (Session["lmsData"] != null)
                            Session["lmsData"] = null;
                    }
                    updateBadMailer.PageMethod += new lntechNew.WebControls.PageMethodHandler(updateBadMailer_PageMethod);
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    PagingControl.grdType = GridType.GridView;
                    Pagingctrl.GridView = dg_lmsAddresses;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        
        /// <summary>
        /// This methoed is used to Fill the Grid View
        /// </summary>
        private void FillGrid()
        {
            DataTable dt;
            try
            {
                if (Session["lmsData"] == null)
                {
                    dt = _clsNonclients.GetBadMarkedRecords(ddl_letterType.SelectedValue, cal_EffectiveFrom.SelectedDate,
                        cal_EffectiveTo.SelectedDate, chk_showAll.Checked, false);
                    Session["lmsData"] = dt.DefaultView;
                }
                else
                {
                    DataView dw = Session["lmsData"] as DataView;
                    dt = dw.ToTable();
                }
                if (dt.Rows.Count > 0)
                {
                    dg_lmsAddresses.DataSource = dt;
                    dg_lmsAddresses.DataBind();
                    dg_lmsAddresses.Visible = true;
                    Pagingctrl.Visible = true;
                    Pagingctrl.PageCount = dg_lmsAddresses.PageCount;
                    Pagingctrl.PageIndex = dg_lmsAddresses.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    dg_lmsAddresses.Visible = false;
                    Pagingctrl.Visible = false;
                    lblMessage.Text = @"No Records Found";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle Pagging Control pageIndexChange Event.
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                dg_lmsAddresses.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_PageIndexChanged(object source, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_lmsAddresses.PageIndex = e.NewPageIndex;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_lmsAddresses.PageIndex = 0;
                dg_lmsAddresses.PageSize = pageSize;
                dg_lmsAddresses.AllowPaging = true;

            }
            else
            {
                dg_lmsAddresses.AllowPaging = false;
            }

            FillGrid();
        }

        /// <summary>
        /// Submit Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            if (Session["lmsData"] != null)
                Session["lmsData"] = null;
            FillGrid();
        }

        /// <summary>
        /// Grid View Page Index changing methoed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_lmsAddresses.PageIndex = e.NewPageIndex;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Row Data Bound Event of Grid View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "border:1px solid DarkGray");
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "border:1px solid DarkGray");
                    }
                    if (((LinkButton) e.Row.FindControl("lnk_newAddress1")).Text.Equals("N/A"))
                        ((LinkButton) e.Row.FindControl("lnk_newAddress1")).Text = "";
                    if (((LinkButton)e.Row.FindControl("lnk_newAddress2")).Text.Equals("N/A"))
                        ((LinkButton)e.Row.FindControl("lnk_newAddress2")).Text = "";


                    Label lbllastName = ((Label) e.Row.FindControl("lbl_lastName"));
                    if (lbllastName.Text.Length > 14)
                    {

                        lbllastName.ToolTip = lbllastName.Text;
                        lbllastName.Text = lbllastName.Text.Substring(0, 14) + @"...";
                    }

                    Label lblFirstName = ((Label)e.Row.FindControl("lbl_firstName"));
                    if (lblFirstName.Text.Length > 10)
                    {
                        lblFirstName.ToolTip = lblFirstName.Text;
                        lblFirstName.Text = lblFirstName.Text.Substring(0, 10) + @"...";
                    }

                    Label lblexistingAddress = ((Label)e.Row.FindControl("lbl_existingAddress"));
                    if (lblexistingAddress.Text.Length > 35)
                    {
                        lblexistingAddress.ToolTip = lblexistingAddress.Text;
                        lblexistingAddress.Text = lblexistingAddress.Text.Substring(0, 35) + @"...";
                    }

                    HiddenField hfIsNoNewAdd = e.Row.FindControl("hf_IsNoNewAddressMarked") as HiddenField;
                    if (Convert.ToBoolean(hfIsNoNewAdd.Value))
                    {
                        ((LinkButton) e.Row.FindControl("lnk_newAddress1")).Text = @"No New Address";
                        ((LinkButton) e.Row.FindControl("lnk_newAddress2")).Text = @"No New Address";
                        ((Label)e.Row.FindControl("lbl_newAddress1")).Visible = false;
                        ((Label)e.Row.FindControl("lbl_newAddress2")).Visible = false;
                        ((LinkButton)e.Row.FindControl("lnk_newAddress1")).CommandArgument = e.Row.RowIndex.ToString(CultureInfo.InvariantCulture);
                        ((LinkButton)e.Row.FindControl("lnk_newAddress2")).CommandArgument = e.Row.RowIndex.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        ((LinkButton) e.Row.FindControl("lnk_newAddress1")).Visible = false;
                        ((LinkButton)e.Row.FindControl("lnk_newAddress2")).Visible = false;
                        Label lblnewAddress1 = ((Label)e.Row.FindControl("lbl_newAddress1"));
                        if (lblnewAddress1.Text.Length > 22)
                        {
                            HiddenField hf_adrStatus1 = e.Row.FindControl("hf_addressStatus1") as HiddenField;
                            lblnewAddress1.ToolTip = lblnewAddress1.Text;
                            lblnewAddress1.Text = lblnewAddress1.Text.Substring(0, 22) + @"...";
                            if ((!lblnewAddress1.Text.Equals("N/A")) && (!hf_adrStatus1.Value.Equals("N")))
                                ((Image)e.Row.FindControl("imgAddressVerify1")).Visible = true;
                        }

                        Label lblnewAddress2 = ((Label)e.Row.FindControl("lbl_newAddress2"));
                        if (lblnewAddress2.Text.Length > 22)
                        {
                            HiddenField hf_adrStatus2 = e.Row.FindControl("hf_addressStatus2") as HiddenField;
                            lblnewAddress2.ToolTip = lblnewAddress2.Text;
                            lblnewAddress2.Text = lblnewAddress2.Text.Substring(0, 22) + @"...";
                            if ((!lblnewAddress2.Text.Equals("N/A")) && (!hf_adrStatus2.Value.Equals("N")))
                            ((Image)e.Row.FindControl("imgAddressVerify2")).Visible = true;
                        }
                    }
                    ((LinkButton)e.Row.FindControl("lnk_History")).Attributes.Add("OnClick", "return show_popup('" + ((HiddenField)e.Row.FindControl("hf_recordId")).Value + "')");
                    ((LinkButton)e.Row.FindControl("lnk_edit")).CommandArgument = e.Row.RowIndex.ToString(CultureInfo.InvariantCulture);
                    ((ImageButton)e.Row.FindControl("img_cross")).CommandArgument = e.Row.RowIndex.ToString(CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Edit Row Event of Grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_RowEditing(object sender, GridViewEditEventArgs e) { }

        /// <summary>
        /// Row Command Event of Grid View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    int rowId = Convert.ToInt32(e.CommandArgument);
                    string mailerDate = ((Label) dg_lmsAddresses.Rows[rowId].FindControl("lbl_mailDate")).Text.Trim();
                    int letterId =
                        Convert.ToInt32(((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_letterId")).Value);
                    int recordId =
                        Convert.ToInt32(((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_recordId")).Value);
                    string newAddress1 =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_newAddress1")).Value.Trim();
                    string newCity1 =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NewCity1")).Value.Trim();
                    int newState1 =
                        Convert.ToInt32(((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NewState1")).Value);
                    string newZipcode1 =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NewZipCode1")).Value.Trim();
                    string newAddress2 =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_newAddress2")).Value.Trim();
                    string newCity2 =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NewCity2")).Value.Trim();
                    int newState2 =
                        Convert.ToInt32(((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NewState2")).Value);
                    string newZipcode2 =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NewZipCode2")).Value.Trim();
                    string lastName =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_LastName")).Value.Trim();
                    string firstName =
                        ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_FirstName")).Value.Trim();
                    string existingAddress =
                        ((HiddenField)dg_lmsAddresses.Rows[rowId].FindControl("hf_ExistingAddress")).Value.Trim();
                    int noteId =
                        Convert.ToInt32(((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_NoteId")).Value);
                    int empId = Convert.ToInt32(ViewState["vEmpID"].ToString());
                    bool isNewAddress2Updated =
                        Convert.ToBoolean(
                            ((HiddenField) dg_lmsAddresses.Rows[rowId].FindControl("hf_IsNewAddress2Updated")).Value);
                    string causeNumbers =
                        ((HiddenField)dg_lmsAddresses.Rows[rowId].FindControl("hf_CauseNumbers")).Value.Trim();
                    DataTable dt = _clsNonclients.GetStates();

                    updateBadMailer.CallPopUp(mailerDate, letterId, recordId, newAddress1, newCity1, newState1,
                        newZipcode1,
                        newAddress2, newCity2, newState2, newZipcode2, lastName, firstName, existingAddress, noteId, dt,
                        ModalPopupExtender2.ClientID, empId, isNewAddress2Updated, causeNumbers);
                    ModalPopupExtender2.Show();
                    Pagingctrl.Visible = true;
                }

                if (e.CommandName.Equals("No New Address"))
                {
                    int rowId = Convert.ToInt32(e.CommandArgument);
                    int recordId =
                        Convert.ToInt32(((HiddenField)dg_lmsAddresses.Rows[rowId].FindControl("hf_recordId")).Value);
                    string lastName =
                        ((HiddenField)dg_lmsAddresses.Rows[rowId].FindControl("hf_LastName")).Value.Trim();
                    string firstName =
                        ((HiddenField)dg_lmsAddresses.Rows[rowId].FindControl("hf_FirstName")).Value.Trim();
                    int empId = Convert.ToInt32(ViewState["vEmpID"].ToString());
                    UpdateNoNewAddress(recordId, lastName, firstName, empId);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// This Method is used to Mark the record as "No New Address"
        /// </summary>
        /// <param name="recordid">int recordId</param>
        /// <param name="lastName">string lastName</param>
        /// <param name="firstname">string firstName</param>
        /// <param name="empid">int employeeId</param>
        protected void UpdateNoNewAddress(int recordid, string lastName, string firstname, int empid)
        {
            try
            {
                if (Session["lmsData"] != null)
                    Session["lmsData"] = null;
                _clsNonclients.UpdateNoNewAddress(recordid, lastName, firstname, empid);
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Delegate use to update the Grid on page
        /// </summary>
        void updateBadMailer_PageMethod()
        {
            Session["lmsData"] = null;
            FillGrid();
        }
    }
}

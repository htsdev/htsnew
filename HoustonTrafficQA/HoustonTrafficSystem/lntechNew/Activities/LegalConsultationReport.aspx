﻿<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="True" CodeBehind="LegalConsultationReport.aspx.cs"
    Inherits="HTP.Activities.LegalConsultationReport" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Legal Consultation</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="javascript" type="text/javascript">
    
    function checkcomments()
        {       
        
            var Comment=document.getElementById("tb_ConsultationComments").value.length;
            if(Comment == 0)
             {
                    alert("Please Enter Comments")            
                    return false;
             }
        
    
        }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <aspnew:UpdatePanel ID="PnlMain" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="780">
                    <tbody>
                        <tr>
                            <td colspan="4" style="height: 14px">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="7" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 790">
                                <table cellspacing="0" cellpadding="0" width="790" border="0" align="left">
                                    <tr>
                                        <td style="width: 75px" align="left">
                                            <asp:Label ID="lblStartDate" runat="server" CssClass="clssubhead">Start Date :</asp:Label>
                                        </td>
                                        <td align="left">
                                            <ew:CalendarPopup ID="dtSDate" runat="server" Width="80px" EnableHideDropDown="True"
                                                Font-Names="Tahoma" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                PadSingleDigits="True" ToolTip="Select start date" ImageUrl="../images/calendar.gif"
                                                Font-Size="8pt">
                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></WeekdayStyle>
                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGray"></WeekendStyle>
                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></HolidayStyle>
                                            </ew:CalendarPopup>
                                        </td>
                                        <td style="width: 75px" align="left">
                                            &nbsp;<asp:Label ID="lblEndDate" runat="server" CssClass="clssubhead">End Date :</asp:Label>
                                        </td>
                                        <td align="left">
                                            <ew:CalendarPopup ID="dtEDate" runat="server" Width="80px" EnableHideDropDown="True"
                                                Font-Names="Tahoma" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                PadSingleDigits="True" ToolTip="Select start date" ImageUrl="../images/calendar.gif"
                                                Font-Size="8pt">
                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></WeekdayStyle>
                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGray"></WeekendStyle>
                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></HolidayStyle>
                                            </ew:CalendarPopup>
                                        </td>
                                        <td style="width: 67px" align="left">
                                            <asp:Label ID="lblPCCR" runat="server" CssClass="clssubhead" Width="100px" Text="Call 
                                Back Status:"></asp:Label>
                                        </td>
                                        <td align="left" style="width: 170px">
                                            <asp:DropDownList ID="ddlCallBackStatus" runat="server" CssClass="clsInputCombo"
                                                Width="155px">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 90px" align="center">
                                            <asp:CheckBox ID="chkShowAll" runat="server" CssClass="clsaspcolumnheader" Checked="true"
                                                Text="Show All" />
                                        </td>
                                        <td align="right" colspan="">
                                            <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btnSubmit_Click">
                                            </asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="7" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                                <table style="width: 750;">
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: right;">
                                            <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="7" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <%-- <td  align="center" valign="top" colspan="2" style="height: 144px">--%>
                            <td align="center" valign="top" colspan="2">
                                <aspnew:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpnlGvw">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="UpnlGvw" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="4"
                                            PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="True" OnRowCommand="gv_records_RowCommand"
                                            OnRowDataBound="gv_records_RowDataBound" CellSpacing="0">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#" HeaderStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hl_Sno" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            CssClass="clssubhead" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LnkBtnUpdt" runat="server" CommandName="btnclick" Text='<%# DataBinder.Eval(Container,"DataItem.ClientName") %>'></asp:LinkButton>
                                                        <asp:HiddenField ID="hf_Clientname" runat="server" Value='<%#Eval("ClientName") %>' />
                                                        <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                        <asp:HiddenField ID="hf_contactno1" runat="server" Value='<%#Eval("contact1") %>' />
                                                        <asp:HiddenField ID="hf_contactno2" runat="server" Value='<%#Eval("contact2") %>' />
                                                        <asp:HiddenField ID="hf_contactno3" runat="server" Value='<%#Eval("contact3") %>' />
                                                        <asp:HiddenField ID="hf_commentdate" runat="server" Value='<%#Eval("recievedate_sort") %>' />
                                                        <asp:HiddenField ID="hf_comments" runat="server" Value='<%#Eval("Comments") %>' />
                                                        <asp:HiddenField ID="hf_StatusId" runat="server" Value='<%#Eval("CallBackStatus") %>' />
                                                        <asp:HiddenField ID="hf_StatusDescription" runat="server" Value='<%#Eval("Description") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="20%" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comments">
                                                    <ItemTemplate>
                                                        <div id="div1" style="overflow: auto; height: 45px; width: 450px;">
                                                            <asp:Label ID="lblcomments" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("Comments")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="Comments" HeaderText="Comments" HtmlEncode="false">
                                                                <itemstyle cssclass="GridItemStyle" />
                                                                <headerstyle cssclass="clssubhead" horizontalalign="Left" />
                                                            </asp:BoundField>--%>
                                                <%--<asp:TemplateField HeaderText="Contact Info">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcontact1" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact1")%>'></asp:Label><br />
                                                        <asp:Label ID="lblcontact2" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact2")%>'></asp:Label><br />
                                                        <asp:Label ID="lblcontact3" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact3")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="18%" />
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Contact Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCallBackStatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Description")%>'></asp:Label><br />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="14%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Record Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRecieveDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RecieveDate")%>'></asp:Label><br />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="10%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="PnlLegalConsultation" runat="server" Style="display: none; width: 600px">
                                            <div id="pnl_control" runat="server">
                                                <table border="3" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                                                    width: 600px">
                                                    <tr>
                                                        <td background="../Images/subhead_bg.gif" class="clssubhead" valign="bottom">
                                                            <table border="0" width="600px">
                                                                <tr>
                                                                    <td class="clssubhead" style="height: 26px">
                                                                        <asp:Label ID="lbl_head" Text="Call Back Information" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td align="right">
                                                                        &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable">
                                                                <tr>
                                                                    <td class="clssubhead" style="width: 125px">
                                                                        Client Name :
                                                                    </td>
                                                                    <td style="height: 30px; width: 475px">
                                                                        <asp:Label ID="lblClientName" runat="server" CssClass="clsLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="clssubhead" style="width: 125px">
                                                                        Contact Number :
                                                                    </td>
                                                                    <td style="height: 30px; width: 475px">
                                                                        <asp:Label ID="lblContactNumber1" runat="server" CssClass="clsLabel"></asp:Label><br />
                                                                        <asp:Label ID="lblContactNumber2" runat="server" CssClass="clsLabel"></asp:Label><br />
                                                                        <asp:Label ID="lblContactNumber3" runat="server" CssClass="clsLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="clssubhead" style="width: 125px">
                                                                        Call Back Status :
                                                                    </td>
                                                                    <td style="height: 30px; width: 475px">
                                                                        <asp:DropDownList ID="ddl_Status" runat="server" CssClass="clsInputCombo">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="clssubhead" style="width: 125px">
                                                                        Comments :
                                                                    </td>
                                                                    <td style="height: 30px; width: 475px">
                                                                        <div id="divcomment" runat="server" style="overflow: auto; height: 80px; width: 475px;">
                                                                            <asp:Label ID="lblComments" runat="server" CssClass="clsLabel"></asp:Label>
                                                                        </div>
                                                                        <asp:TextBox ID="tb_ConsultationComments" runat="server" Height="70px" TextMode="MultiLine"
                                                                            Width="475px"></asp:TextBox>
                                                                        <asp:HiddenField ID="hf_ticketid" runat="server" Value="0" />
                                                                        <asp:HiddenField ID="hf_ticid" runat="server" />
                                                                        <asp:HiddenField ID="hf_todaydate" runat="server" />
                                                                        <asp:HiddenField ID="Hf_field" runat="server" Value="0" />
                                                                        <asp:HiddenField ID="hf_FollowUpType" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" style="height: 20px">
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Button ID="btnsave" runat="server" CssClass="clsbutton" OnClientClick="return checkcomments();"
                                                                            OnClick="btnsave_Click" Text="Save" Width="53px" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                                                                            PopupControlID="PnlLegalConsultation" BackgroundCssClass="modalBackground" CancelControlID="lbtn_close">
                                                                        </ajaxToolkit:ModalPopupExtender>
                                                                        <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>
    </aspnew:UpdatePanel>
    <asp:HiddenField ID="hf_StatusDesc" runat="server" />
    </form>
</body>
</html>

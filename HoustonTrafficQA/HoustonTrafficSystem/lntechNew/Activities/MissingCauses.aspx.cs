using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace lntechNew.Activities
{
    public partial class MissingCauses : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        DataTable dtRecords = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                if (!IsPostBack)
                {
                    GetRecords();
                    if (dtRecords.Rows.Count == 0)
                        lbl_Message.Text = "No Records!";
                    else
                    {
                        gv_Data.DataSource = dtRecords;
                        gv_Data.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void GetRecords()
        {
            dtRecords = ClsDb.Get_DT_BySPArr("USP_HTS_Get_MissingCauseNumbers_New");

        }

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex >= 0)
                {
                    HyperLink hlnk = (HyperLink)e.Row.FindControl("hl_SNo");
                    //hlnk.Text = Convert.ToString(e.Row.RowIndex + 1);

                    HiddenField hf = (HiddenField)e.Row.FindControl("hf_TicketID");
                    hlnk.NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&caseNumber=" + hf.Value;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Data_PageIndexChanged(object sender, EventArgs e)
        {
           
            
        }

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            GetRecords();
            gv_Data.DataSource = dtRecords;
            gv_Data.PageIndex = e.NewPageIndex;
            gv_Data.DataBind();
        }

        
    }
}

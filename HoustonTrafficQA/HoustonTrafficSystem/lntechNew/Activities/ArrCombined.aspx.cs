using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace lntechNew.QuickEntryNew
{
    public partial class ArrCombined : System.Web.UI.Page
    {
       

        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        private DateTime CSDMon;
        private DateTime CSDTue;
        private DateTime CSDWed;
        private DateTime CSDThr;
        private DateTime CSDFri;
        DataView DV;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        tdScan.Style["display"] = "none";
                        //network path
                        ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                        ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH1"].ToString();
                        ViewState["FilePath"] = ConfigurationSettings.AppSettings["NPathSummaryReports"].ToString();
                        ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();
                        //
                        lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());
                        //for Scanning..
                        ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                        ViewState["vNTPATHCaseSummary"] = ConfigurationSettings.AppSettings["NTPATHCaseSummary"].ToString();
                        txtempid.Text = ClsSession.GetCookie("sEmpID", this.Request);
                        txtsessionid.Text = Session.SessionID;
                        
                        #region For Scanning Purpose
                        string strServer;
                        strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                        Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.
                        #endregion
                    }
                }
                lnk_bsumm.Attributes.Add("onclick", "return Prompt(" + 4 + ");");
                btnBoncSumm.Attributes.Add("onclick", "return Validate();");
                HPARRSumm.Attributes.Add("onclick", "return Prompt("+2+");");
                btnARR.Attributes.Add("onclick","return Prompt("+1+");");
                btnBoncSumm.Attributes.Add("onclick", "return Prompt(" + 3 + ");");
                btnScan.Attributes.Add("onclick", "return Check();");
                btncheck.Attributes.Add("onclick", "return DocValidate();");
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        

        private void GenerateSerial()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_Report.Items)
                {
                    Label sno = (Label)ItemX.FindControl("lbl_sno");
                    Label tid = (Label)ItemX.FindControl("lbl_Ticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                        Label arrdate = (Label)ItemX.FindControl("lbl_arrdate");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                        }

                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GenerateSerialJT()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_ReportJT.Items)
                {
                    Label sno1 = (Label)ItemX.FindControl("lbl_sno1");
                    Label tid1 = (Label)ItemX.FindControl("lbl_Ticketid1");
                    if (tid1.Text != "")
                    {
                        if (no == 1)
                        {
                            sno1.Text = no.ToString();
                            ticketid = tid1.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid1.Text)
                            {
                                sno1.Text = Convert.ToString(no);
                                ticketid = tid1.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno1.Text = "";
                            }
                        }
                        Label date = (Label)ItemX.FindControl("lbl_CCDate");
                        Label num = (Label)ItemX.FindControl("lbl_CCNum");
                        if (date.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(date.Text.ToString());

                            date.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year, " @ ", adate.ToShortTimeString(), " # ", num.Text);
                        }
                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void GenerateSerialOT()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_ReportOT.Items)
                {
                    Label sno2 = (Label)ItemX.FindControl("lbl_sno2");
                    Label tid2 = (Label)ItemX.FindControl("lbl_Ticketid2");
                    if (tid2.Text != "")
                    {
                        if (no == 1)
                        {
                            sno2.Text = no.ToString();
                            ticketid = tid2.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid2.Text)
                            {
                                sno2.Text = Convert.ToString(no);
                                ticketid = tid2.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno2.Text = "";
                            }
                        }
                        Label date = (Label)ItemX.FindControl("lbl_CCDate");
                        Label num = (Label)ItemX.FindControl("lbl_CCNum");
                        if (date.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(date.Text.ToString());

                            date.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year, " @ ", adate.ToShortTimeString(), " # ", num.Text);
                        }
                        Label dob = (Label)ItemX.FindControl("lbl_dob");

                        if (dob.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(dob.Text.ToString());
                            dob.Text = String.Concat(adate.Month, "/", adate.Day, "/", adate.Year);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void GenerateSerialBond()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dgbond.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("serialNo");
                    Label tid = (Label)ItemX.FindControl("bticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
              }
        }

        private void GenerateArraignmentSerial()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in ASdg.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("lblSNo");
                    Label tid = (Label)ItemX.FindControl("ASTIDS");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }
        private void GenerateSerialBondWaiting()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_BondWaiting.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("serialNo");
                    Label tid = (Label)ItemX.FindControl("bticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }


       

        //snap short
        private void FindStartDate()
        {
            DateTime strDate = DateTime.Now.AddDays(21);
            int intDay = 0;
            for (intDay = 0; intDay <= 6; intDay++)
            {
                strDate = DateTime.Now.AddDays(intDay); //DateAdd("d",intDay,date())		
                if (strDate.DayOfWeek != DayOfWeek.Saturday && strDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (strDate.DayOfWeek == DayOfWeek.Monday)
                    {
                        CSDMon = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Tuesday)
                    {
                        CSDTue = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        CSDWed = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Thursday)
                    {
                        CSDThr = strDate;
                    }
                    if (strDate.DayOfWeek == DayOfWeek.Friday)
                    {
                        CSDFri = strDate;
                    }
                }
            }
        }

        //Monday
        private void GenerateSerial0()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Mon.Items)
                {
                    Label tid = (Label)ItemX.FindControl("lblTicketidM");
                    Label no = (Label)ItemX.FindControl("lblNoM");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrM");
                        Label tdt = (Label)ItemX.FindControl("lblTrialM");
                        Label bf = (Label)ItemX.FindControl("lblBondM");
                        Label tc = (Label)ItemX.FindControl("lblTrialCM");
                        Label pt = (Label)ItemX.FindControl("lblPreM");
                        Label pl = (Label)ItemX.FindControl("lblPlusM");

                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_MonC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommM");
                    Label no = (Label)ItemX.FindControl("lblNo1M");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Tuesday
        private void GenerateSerial1()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Tue.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoT");
                    Label tid = (Label)ItemX.FindControl("lblTicketidT");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrT");
                        Label tdt = (Label)ItemX.FindControl("lblTrialT");
                        Label bf = (Label)ItemX.FindControl("lblBondT");
                        Label tc = (Label)ItemX.FindControl("lblTrialCT");
                        Label pt = (Label)ItemX.FindControl("lblPreT");
                        Label pl = (Label)ItemX.FindControl("lblPlusT");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_TueC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommT");
                    Label no = (Label)ItemX.FindControl("lblNo1T");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Wednesday
        private void GenerateSerial2()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Wed.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoW");
                    Label tid = (Label)ItemX.FindControl("lblTicketidW");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrW");
                        Label tdt = (Label)ItemX.FindControl("lblTrialW");
                        Label bf = (Label)ItemX.FindControl("lblBondW");
                        Label tc = (Label)ItemX.FindControl("lblTrialCW");
                        Label pt = (Label)ItemX.FindControl("lblPreW");
                        Label pl = (Label)ItemX.FindControl("lblPlusW");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_WedC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommW");
                    Label no = (Label)ItemX.FindControl("lblNo1W");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Thursday
        private void GenerateSerial3()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Thu.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoTh");
                    Label tid = (Label)ItemX.FindControl("lblTicketidTh");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrTh");
                        Label tdt = (Label)ItemX.FindControl("lblTrialTh");
                        Label bf = (Label)ItemX.FindControl("lblBondTh");
                        Label tc = (Label)ItemX.FindControl("lblTrialCTh");
                        Label pt = (Label)ItemX.FindControl("lblPreTh");
                        Label pl = (Label)ItemX.FindControl("lblPlusTh");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_ThuC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommTh");
                    Label no = (Label)ItemX.FindControl("lblNo1Th");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Friday
        private void GenerateSerial4()
        {

            try
            {
                foreach (DataGridItem ItemX in dg_Fri.Items)
                {
                    Label no = (Label)ItemX.FindControl("lblNoF");
                    Label tid = (Label)ItemX.FindControl("lblTicketidF");
                    if (tid.Text != "")
                    {
                        Label arrdate = (Label)ItemX.FindControl("lblArrF");
                        Label tdt = (Label)ItemX.FindControl("lblTrialF");
                        Label bf = (Label)ItemX.FindControl("lblBondF");
                        Label tc = (Label)ItemX.FindControl("lblTrialCF");
                        Label pt = (Label)ItemX.FindControl("lblPreF");
                        Label pl = (Label)ItemX.FindControl("lblPlusF");
                        if (arrdate.Text != "")
                        {
                            DateTime adate = Convert.ToDateTime(arrdate.Text.ToString());
                            arrdate.Text = String.Concat(adate.Month, "/", adate.Day);
                            DateTime curr = DateTime.Now;
                            int dif = adate.Day - curr.Day;
                            if (dif >= 0 && dif <= 2 && curr.Month == adate.Month && curr.Year == adate.Year)
                            {
                                pl.Text = "+";
                            }
                            else
                            {
                                pl.Visible = false;
                            }
                        }
                        if (tdt.Text.ToUpper() == "10:30AM")
                        {
                            tdt.Text = "t";
                        }
                        else
                        {
                            tdt.Visible = false;
                        }
                        if (Convert.ToInt32(bf.Text) > 0)
                        {
                            bf.Text = "b";
                        }
                        else
                        {
                            bf.Visible = false;
                        }
                        if (tc.Text != "")
                        {
                            tc.Text = "*";
                        }
                        else
                        {
                            tc.Visible = false;
                        }
                        if (pt.Text != "P")
                        {
                            pt.Visible = false;
                        }
                    }
                }
                foreach (DataGridItem ItemX in dg_FriC.Items)
                {
                    Label com = (Label)ItemX.FindControl("lblCommF");
                    Label no = (Label)ItemX.FindControl("lblNo1F");
                    if (com.Text == "")
                    {
                        no.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_WeekDay_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                btncheck.Visible = true;
                btnScan.Visible = false;
                lbl_CurrDateTime.Text = String.Concat("Date: ", DateTime.Now.ToString());
                clsENationWebComponents clsDB = new clsENationWebComponents();
                lbl_Message.Text = "";
                lblMessage.Text = "";
                hp.Visible = false;
                HPARRSumm.Visible = false;
                lnk_bsumm.Visible = false;
                lblSep.Visible = false;
                //txtDocid.Text = "";
                PrintAllBonds.Visible = false;
                btn_PrintReport_Bond.Visible = false;
                ImgCrystalSingle.Visible = false;
                ImgCrystalAll.Visible = false;
                
                if (ddl_WeekDay.SelectedValue == "50")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "block";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    imgPrint_Ds_to_Printer.Visible = false;
                    img_pdfLong.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    DataSet ds = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapShot_VER_2_Update");
                    #region Moday
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dg_Mon.DataSource = ds.Tables[0];
                        dg_Mon.DataBind();
                        dg_MonC.DataSource = ds.Tables[0];
                        dg_MonC.DataBind();
                        GenerateSerial0();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        ImgCrystalAll.Visible = true;
                        HF_ArraignmentDocket.Value = "50";
                        ImgCrystalSingle.Visible = true;
                    }
                    #endregion
                    #region Tuesday
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        dg_Tue.DataSource = ds.Tables[1];
                        dg_Tue.DataBind();
                        dg_TueC.DataSource = ds.Tables[1];
                        dg_TueC.DataBind();
                        GenerateSerial1();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        ImgCrystalAll.Visible = true;
                        HF_ArraignmentDocket.Value = "50";
                        ImgCrystalSingle.Visible = true;
                    }
                    #endregion
                    #region Wednesday
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        dg_Wed.DataSource = ds.Tables[2];
                        dg_Wed.DataBind();
                        dg_WedC.DataSource = ds.Tables[2];
                        dg_WedC.DataBind();
                        GenerateSerial2();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        ImgCrystalAll.Visible = true;
                        HF_ArraignmentDocket.Value = "50";
                        ImgCrystalSingle.Visible = true;
                    }
                    #endregion
                    #region Thursday
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        dg_Thu.DataSource = ds.Tables[3];
                        dg_Thu.DataBind();
                        dg_ThuC.DataSource = ds.Tables[3];
                        dg_ThuC.DataBind();
                        GenerateSerial3();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        ImgCrystalAll.Visible = true;
                        HF_ArraignmentDocket.Value = "50";
                        ImgCrystalSingle.Visible = true;
                    }
                    #endregion
                    #region Friday
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        dg_Fri.DataSource = ds.Tables[4];
                        dg_Fri.DataBind();
                        dg_FriC.DataSource = ds.Tables[4];
                        dg_FriC.DataBind();
                        GenerateSerial4();
                        Imagebutton1.Visible = true;
                        img_pdfShort.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        ImgCrystalAll.Visible = true;
                        HF_ArraignmentDocket.Value = "50";
                        ImgCrystalSingle.Visible = true;
                    }
                    #endregion
                }
                else if (ddl_WeekDay.SelectedValue == "100")
                {
                    longshot.Style["display"] = "block";
                    snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    DataSet ds_JT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1_Update");
                    
                    if (ds_JT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportJT.DataSource = ds_JT;
                        dg_ReportJT.DataBind();
                        GenerateSerialJT();
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        ImgCrystalSingle.Visible = true;
                        HF_ArraignmentDocket.Value = ddl_WeekDay.SelectedValue.ToString();
                        ImgCrystalAll.Visible = true;
                    }
                    else
                    {
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                        ImgCrystalSingle.Visible = false;
                        ImgCrystalAll.Visible = false;
                    }
                    GetPR_POAReport();
                    
                }
                else if (ddl_WeekDay.SelectedValue == "99")
                {
                    longshot.Style["display"] = "block";
                    snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                   // DataSet ds_JT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1_Update");
                    DataSet ds_OT = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterOutsideArraignment_ver_1_Update");

                    /*if (ds_JT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportJT.DataSource = ds_JT;
                        dg_ReportJT.DataBind();
                        GenerateSerialJT();
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                    }
                    else
                    {
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                    }*/

                    if (ds_OT.Tables[0].Rows.Count > 0)
                    {
                        dg_ReportOT.DataSource = ds_OT;
                        dg_ReportOT.DataBind();
                        GenerateSerialOT();
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        trial.Style["Display"] = "block";
                        ImgCrystalSingle.Visible = true;
                        HF_ArraignmentDocket.Value = ddl_WeekDay.SelectedValue.ToString();
                        ImgCrystalAll.Visible = true;
                    }
                    else
                    {
                        ImgCrystalSingle.Visible = false;
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                        trial.Style["Display"] = "none";
                        ImgCrystalAll.Visible = false;
                    }
                    tbl_PR_Report.Style[HtmlTextWriterStyle.Display] = "none";
                }
                //code added by Azee..for displaying bond summary grid
                else if (ddl_WeekDay.SelectedValue == "150")//150 Means Bond Summary option in dropdown..
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    BforBond.Style["display"] = "block";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    BindBondGrid();
                    

                }

                else if (ddl_WeekDay.SelectedValue == "0")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "block";
                    tdScan.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    FillDDLWithFlags();
                    ddl_Flags.SelectedIndex = 0;
                    DisplayNotOnSystem(Convert.ToInt32(ddl_Flags.Items[0].Value));
                }

                else if (ddl_WeekDay.SelectedValue == "998")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    BforBond.Style["display"] = "block";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "block";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    tdScan.Style["display"] = "block";
                    tdDoc.Style["display"] = "none";
                    BindBondWaiting();
                }
                //code added by Azee for displaying arraignment summary grid
                else if (ddl_WeekDay.SelectedValue == "160")//160 Means Arraignment summary options in dropwon..
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "block";
                    Td2.Style["display"] = "block";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    BindArraignmentSummary();


                }
                //

                else if (ddl_WeekDay.SelectedValue == "999")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    btn_PrintReport.Visible = false;
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "block";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["display"] = "block";
                    BindArraignmentWaiting();

                }
                if (ddl_WeekDay.SelectedValue != "-1" & ddl_WeekDay.SelectedValue != "50" & ddl_WeekDay.SelectedValue != "100" & ddl_WeekDay.SelectedValue != "150" & ddl_WeekDay.SelectedValue != "160" & ddl_WeekDay.SelectedValue != "999" & ddl_WeekDay.SelectedValue != "998")
                {
                    longshot.Style["display"] = "block";
                    snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    td_Title.Style[HtmlTextWriterStyle.Display] = "none";
                    if (ddl_WeekDay.SelectedValue != "0")
                        tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    else
                        tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "block";
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    tdScan.Style["display"] = "none";
                    string[] key1 = { "@ReportWeekDay" };
                    object[] value2 = { Convert.ToInt32(ddl_WeekDay.SelectedValue) };
                    DataSet ds = clsDB.Get_DS_BySPArr("usp_HTS_ArraignmentSnapshotLong_ver_1_Update", key1, value2);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dg_Report.DataSource = ds;
                        dg_Report.DataBind();
                        GenerateSerial();
                        dg_Report.Visible = true;
                        lbl_Message.Visible = false;
                        imgPrint_Ds_to_Printer.Visible = true;
                        img_pdfLong.Visible = true;
                        img_PdfAll.Visible = true;
                        ImageButton2.Visible = true;
                        HF_ArraignmentDocket.Value = ddl_WeekDay.SelectedValue.ToString();
                        ImgCrystalSingle.Visible = true;
                        ImgCrystalAll.Visible = true;
                    }
                    else
                    {
                        dg_Report.Visible = false;
                        lbl_Message.Visible = true;
                        imgPrint_Ds_to_Printer.Visible = false;
                        img_pdfLong.Visible = false;
                        ImgCrystalSingle.Visible = false;
                        ImgCrystalAll.Visible = false;
                        
                    }
                }
                if (ddl_WeekDay.SelectedValue == "-1")
                {
                    longshot.Style["display"] = "none";
                    snapshot.Style["display"] = "none";
                    BforBond.Style["display"] = "none";
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                    tdDoc.Style["display"] = "none";
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_NotOnSystem.Style[HtmlTextWriterStyle.Display] = "none";
                    tdScan.Style["Display"] = "none";
                    td_Title.Style["Display"] = "none";

                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfLong.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    ImgCrystalSingle.Visible = false;
                    ImgCrystalAll.Visible = false;

                    tbl_ArrWaiting.Style["Display"] = "none";
                    
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgPrint_Ds_to_Printer_Click1(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.SelectedValue);
        }

        protected void Imagebutton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ArraignmentSnapShot.aspx");
        }

        protected void img_pdf_Click(object sender, ImageClickEventArgs e)
        {
            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/ArraignmentSnapShot.aspx?date="+DateTime.Now.ToString();
            //website end
            GeneratePDF(path,true);
            RenderPDF();
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.SelectedValue + "&date=" + DateTime.Now.ToString();
            //website end
            GeneratePDF(path,false);
            RenderPDF();
        }
        private void GeneratePDF(string path ,bool isfirst)
        {
            Doc theDoc = new Doc();
            if (isfirst)
            {

                // apply a rotation transform
                double w = theDoc.MediaBox.Width;
                double h = theDoc.MediaBox.Height;
                double l = theDoc.MediaBox.Left;
                double b = theDoc.MediaBox.Bottom;
                theDoc.Transform.Rotate(90, l, b);
                theDoc.Transform.Translate(w, 0);
                // rotate our rectangle
                theDoc.Rect.Width = h;
                theDoc.Rect.Height = w;

            }
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();

            


            
            //Added By Zeeshan Ahmed
           //theDoc.HtmlOptions.BrowserWidth = 950;
            
            
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }
            string name = Session.SessionID + ".pdf";
            //for website
            ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
            //website end
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();
        }
        private void RenderPDF()
        {
            //for website
            string vpth = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH1"].ToString().Length) - 1);
            string link = "../DOCSTORAGE" + vpth;
            link = link + Session.SessionID + ".pdf";
            //website end
            Response.Redirect(link, false);
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
            for (int i = 0; i < files.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);
                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files[i]);
                }
            }

        }

        protected void img_PdfAll_Click(object sender, ImageClickEventArgs e)
        {

            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/ArraignmentSnapShot.aspx?date=" + DateTime.Now.ToString();
            GeneratePDF(path,0);
            for (int i = 7; i < ddl_WeekDay.Items.Count; i++)
            {
                path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.Items[i].Value + "&date=" + DateTime.Now.ToString();
                GeneratePDF(path, i);
            }
            Doc theDoc1 = (Doc)Session["vdoc1"];
            theDoc1.Save(ViewState["vFullPath"].ToString());
            theDoc1.Clear();
            Session["vdoc1"] = null;
            RenderPDF();
        }

        private void GeneratePDF(string path, int seq)
        {
            if (seq == 0)
            {
                Doc theDoc1 = new Doc();
                
                //------Added By Zeeshan Ahmed to Display PDF in landscape---//
                                double w = theDoc1.MediaBox.Width;
                                double h = theDoc1.MediaBox.Height;
                                double l = theDoc1.MediaBox.Left;
                                double b = theDoc1.MediaBox.Bottom;
                                theDoc1.Transform.Rotate(90, l, b);
                                theDoc1.Transform.Translate(w, 0);
                                // rotate our rectangle
                                theDoc1.Rect.Width = h;
                                theDoc1.Rect.Height = w;
                //-----------------------------------------------------------//
                
                theDoc1.Rect.Inset(35, 45);
                theDoc1.Page = theDoc1.AddPage();
                //theDoc1.HtmlOptions.BrowserWidth = 950;
                int theID;
                theID = theDoc1.AddImageUrl(path);
                while (true)
                {
                    theDoc1.FrameRect(); // add a black border
                    if (!theDoc1.Chainable(theID))
                        break;
                    theDoc1.Page = theDoc1.AddPage();
                    theID = theDoc1.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc1.PageCount; i++)
                {
                    theDoc1.PageNumber = i;
                    theDoc1.Flatten();
                }
                string name = Session.SessionID + ".pdf";
                //for website
                ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                //website end
                //theDoc1.Save(ViewState["vFullPath"].ToString());
                Session["vdoc1"] = theDoc1;
                //theDoc1.Clear();
            }
            if (seq > 0)
            {
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(35, 45);
                theDoc.Page = theDoc.AddPage();
                //theDoc.HtmlOptions.BrowserWidth = 800;
                int theID;
                theID = theDoc.AddImageUrl(path);
                while (true)
                {
                    theDoc.FrameRect(); // add a black border
                    if (!theDoc.Chainable(theID))
                        break;
                    theDoc.Page = theDoc.AddPage();
                    theID = theDoc.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc.PageCount; i++)
                {
                    theDoc.PageNumber = i;
                    theDoc.Flatten();
                }
                Doc theDoc1 = (Doc)Session["vdoc1"];
                theDoc1.Append(theDoc);
                Session["vdoc1"] = theDoc1;
                theDoc.Clear();
            }


            //theDoc1.Clear();
        }
        //

       

        protected void SendMail(object sender, ImageClickEventArgs e)
        {

            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/ArraignmentSnapShot.aspx";

            GeneratePDF(path, 0);
            for (int i = 2; i < ddl_WeekDay.Items.Count; i++)
            {
                path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentrynew/ArraignmentSnapShotLongPrint.aspx?criteria=" + ddl_WeekDay.Items[i].Value; ;
                GeneratePDF(path, i);
            }
            Doc theDoc1 = (Doc)Session["vdoc1"];
            theDoc1.Save(ViewState["vFullPath"].ToString());
            theDoc1.Clear();
            
            lntechNew.Components.MailClass.SendMailToAttorneys("Arrignment Report", "Arrignment Report", ViewState["vFullPath"].ToString());
        }

        protected void dgbond_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid=((Label)e.Item.FindControl("bticketid")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("bCauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("serialNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((ImageButton)(e.Item.FindControl("ImgBond"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF('" + ticketid + "','0','0','');");
              

                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnBondSumm_Click(object sender, EventArgs e)
        {
            try
            {   string sTVIDs="";
                foreach (DataGridItem Itemx in dgbond.Items)
                {
                    CheckBox chk = (CheckBox)Itemx.FindControl("chk");
                    Label lbl = (Label)Itemx.FindControl("btvids");
                        if (chk.Checked == true)
                        {
                            if (sTVIDs != String.Empty)
                            {
                                sTVIDs = String.Concat(sTVIDs, lbl.Text.ToString().Trim(), ",");
                            }
                            else
                            {
                                sTVIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                            }
                        }

                    }


                    Session["sTVIDS"] = sTVIDs;
                    if (HD.Value == "1")//for generating docId..
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=1','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');</script>");
                       
                    }
                    if (HD.Value == "2")//for generating only preview..
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=2','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
                    }

              
               
                   
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }
        private void BindBondGrid()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] key ={ "@TVIDS" ,"@BatchID", "@updateflag","@employee"};
                object[] value ={ "", 0, 0, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)) };
                DataSet ds_bondSummary = clsDB.Get_DS_BySPArr("usp_HTS_Bond_Report", key, value);
                if (ds_bondSummary.Tables[0].Rows.Count > 0)
                {
                    dgbond.DataSource = ds_bondSummary;
                    DV = new DataView(ds_bondSummary.Tables[0]);//data view paging
                    Session["sDV"] = DV;
                 //   GenerateSerial(ds_bondSummary.Tables[0]);
                    dgbond.DataBind();
                    txtRowCount.Text = dgbond.Items.Count.ToString();
                    GenerateSerialBond();
                    lnk_bsumm.Visible = true;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    HPARRSumm.Visible = false;
                    btnBoncSumm.Visible = true;
                    hp.Visible = true;
                    lblSep.Visible = true;
                    
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false ;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    HPARRSumm.Visible = false;
                    hp.Visible = false; ;
                    BforBond.Style["display"] = "none";
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace); 
            }
        }
        private void BindArraignmentSummary()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] key ={ "@TVIDS", "@BatchID" };
                object[] value ={ "",0};
                DataSet Ds_ARRSummary = clsDB.Get_DS_BySPArr("USP_HTS_ARRAIGNMENT_REPORT",key,value);
                if (Ds_ARRSummary.Tables[0].Rows.Count > 0)
                {
                    ASdg.DataSource = Ds_ARRSummary;
                    DV = new DataView(Ds_ARRSummary.Tables[0]);//data view paging
                    Session["sDV"] = DV;
                    ASdg.DataBind();
                    GenerateArraignmentSerial();
                   // GenerateSerial(Ds_ARRSummary.Tables[0]);                    
                    txtARRSummCount.Text = ASdg.Items.Count.ToString();                   
                    HPARRSumm.Visible = true;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    btnBoncSumm.Visible = false;
                   
                    hp.Visible = true;
                    lblSep.Visible = true;
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    AforArraignment.Style["display"] = "none";
                    Td2.Style["display"] = "none";
                   
                    HPARRSumm.Visible = false;
                    hp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace); 
            }
        }

        protected void ASdg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
              
                    ((HyperLink)e.Item.FindControl("AScauseno")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void HPARRSumm_Click(object sender, EventArgs e)
        {

            try
            {
                //string TicketID = "";
                //foreach (DataGridItem Itemx in ASdg.Items)
                //{
                    
                //        Label lbl = (Label)Itemx.FindControl("ASTIDS");
                    
                //        if (TicketID != String.Empty)
                //        {
                //            TicketID = String.Concat(TicketID, lbl.Text.ToString().Trim(), ",");
                //        }
                //        else
                //        {
                //            TicketID = String.Concat(lbl.Text.ToString().Trim(), ",");
                //        }
                    

                //}

                //Session["ArrTicketID"] = TicketID;

                //HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
               



            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        protected void btnARR_Click(object sender, EventArgs e)
        {
            try
            {
                string sTVIDs = "";
                foreach (DataGridItem Itemx in ASdg.Items)
                {
                    CheckBox chk = (CheckBox)Itemx.FindControl("chk1");
                    Label lbl = (Label)Itemx.FindControl("ASTVIDS");
                    if (chk.Checked == true)
                    {
                        if (sTVIDs != String.Empty)
                        {
                            sTVIDs = String.Concat(sTVIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            sTVIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }

                }


                Session["ARRTVIDS"] = sTVIDs;

                if (HD.Value == "1")//for generating docId..
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=1','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
                    
                }
                if (HD.Value == "2")//for generating only preview..
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=2','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
                }

                


            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private void GenerateAWRSerial()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_AWR.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("lblSNo");
                    Label tid = (Label)ItemX.FindControl("ASTIDS");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private void GenerateNOSSerial()
        {
            try
            {
                int no = 1;
                string ticketid = "";
                foreach (DataGridItem ItemX in dg_NotOnSystem.Items)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("lblSNo");
                    Label tid = (Label)ItemX.FindControl("ASTIDS");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
        }

        private DataTable GenerateNOSSerial(DataTable dt)
        {

            try
            {

                if (dt.Rows.Count > 0)
                {
                    int no = 1;
                    string ticketid = "";
                    int sno = 1;

                    ticketid = dt.Rows[0]["TicketID"].ToString();
                    dt.Rows[0]["SNO"] = sno;
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {

                        string tid = dt.Rows[i]["TicketID"].ToString();
                        if (tid != "")
                        {
                            if (ticketid != tid)
                            {
                                no++;
                                dt.Rows[i]["SNO"] = no;
                                ticketid = tid;

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
            return dt;
        }

        private void GenerateSerial(DataTable dt)
        {           
          
            try
            {


                int no = 1;
                string ticketid = "";
                int sno = 1;
                if (dt.Columns["SNO"] == null)
                {
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                else
                {
                    dt.Columns.Remove("SNO");
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                ticketid = dt.Rows[0]["TicketID"].ToString();
                
                for (int i = 1; i < dt.Rows.Count; i++ )
                {
                                      
                    string tid = dt.Rows[i]["TicketID"].ToString();
                    if (tid != "")
                    {
                            if (ticketid != tid)
                            {
                                no++;
                                dt.Rows[i]["SNO"] =no;
                                ticketid = tid;
                                
                            }                                                    
                     }
                }               
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
            }
          

        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindArraignmentWaiting();
        }

        private void BindArraignmentWaiting()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] keys = { "@fromDate", "toDate", "@showAllFlag" };
                object[] values = { cal_From.SelectedDate, cal_To.SelectedDate, Convert.ToInt16(chk_ShowAllRec.Checked) };
                DataSet Ds_ARRSummary = clsDB.Get_DS_BySPArr("USP_HTS_ARRAIGNMENT_WAITING_REPORT", keys, values);
                if (Ds_ARRSummary.Tables[0].Rows.Count > 0)
                {
                    dg_AWR.DataSource = Ds_ARRSummary;
                    DV = new DataView(Ds_ARRSummary.Tables[0]);//data view paging
                    //Session["sDV"] = DV;
                    //Session["AWR"] = DV;
                    GenerateSerial(DV.Table);
                    Session["AWR"] = Ds_ARRSummary.Tables[0];
                    dg_AWR.DataBind();
                    btn_PrintReport.Visible = true;
                 //   GenerateAWRSerial();
                    txtARRSummCount.Text = dg_AWR.Items.Count.ToString();
                    HPARRSumm.Visible = false;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    btnBoncSumm.Visible = false;
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "block";
                    hf_Count.Value = Ds_ARRSummary.Tables[0].Rows.Count.ToString();

                    hp.Visible = false;
                    lblSep.Visible = false;
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    tbl_ArrWaiting.Style[HtmlTextWriterStyle.Display] = "none";
                    btn_PrintReport.Visible = false;

                    HPARRSumm.Visible = false;
                    hp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Cannot find table 0.")
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                else
                    lbl_Message.Text = "No Record Found";
            }
        }

        private void BindBondWaiting()
        {
            try
            {
                clsENationWebComponents clsDB = new clsENationWebComponents();
                string[] keys = { "@fromDate", "toDate", "@showAllFlag" };
                object[] values = { cal_From.SelectedDate, cal_To.SelectedDate, Convert.ToInt16(chk_ShowAllRec.Checked) };
                DataSet Ds_ARRSummary = clsDB.Get_DS_BySPArr("USP_HTS_BOND_WAITING_REPORT", keys, values);
                if (Ds_ARRSummary.Tables[0].Rows.Count > 0)
                {
                    dg_BondWaiting.DataSource = Ds_ARRSummary;
                    DV = new DataView(Ds_ARRSummary.Tables[0]);//data view paging
                    Session["sDV"] = DV;
                    GenerateSerial(Ds_ARRSummary.Tables[0]);
                    dg_BondWaiting.DataBind();
                    btn_PrintReport.Visible = true;
                  //  GenerateSerialBondWaiting();
                    txtARRSummCount.Text = dg_AWR.Items.Count.ToString();
                    HPARRSumm.Visible = false;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    lbl_Message.Visible = false;
                    btnBoncSumm.Visible = false;
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "block";
                    hp.Visible = false;
                    lblSep.Visible = false;
                    hf_Count.Value = Ds_ARRSummary.Tables[0].Rows.Count.ToString();
                    PrintAllBonds.Visible = true;
                    btn_PrintReport_Bond.Visible = true;
                    
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                    lblSep.Visible = false;
                    lbl_Message.Visible = true;
                    lnk_bsumm.Visible = false;
                    imgPrint_Ds_to_Printer.Visible = false;
                    Imagebutton1.Visible = false;
                    img_pdfShort.Visible = false;
                    img_PdfAll.Visible = false;
                    ImageButton2.Visible = false;
                    img_pdfLong.Visible = false;
                    btnBoncSumm.Visible = false;
                    td_BondWaiting.Style[HtmlTextWriterStyle.Display] = "none";

                    HPARRSumm.Visible = false;
                    hp.Visible = false;
                    btn_PrintReport_Bond.Visible = true;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Cannot find table 0.")
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                else
                    lbl_Message.Text = "No Record Found";
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindArraignmentWaiting();
        }

        protected void btn_Search_BondWaiting_Click(object sender, EventArgs e)
        {
            if (ddl_WeekDay.SelectedValue == "998")
            {
                BindBondWaiting();
            }
            else if (ddl_WeekDay.SelectedValue == "999")
            {
                BindArraignmentWaiting();
            }
        }

        protected void btn_PrintReport_Click(object sender, EventArgs e)
        {
            string IDs = "";

            foreach (DataGridItem ItemX in dg_AWR.Items)
            {
                CheckBox Chk_Select = (CheckBox)ItemX.FindControl("chk_Select");
                if (Chk_Select.Checked)
                {
                    Label lbl = (Label)ItemX.FindControl("ASTVIDS");
                    IDs += lbl.Text + ",";
                }

            }

            Session["ARRTVIDS"] = IDs;

            if (hf_ArrWaiting_GenIDs.Value == "1")
            {
                
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=5','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/ArrSummary.aspx?Flag=6','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
        }

        protected void btn_PrintReport_Bond_Click(object sender, EventArgs e)
        {
            string IDs = "";

            foreach (DataGridItem ItemX in dg_BondWaiting.Items)
            {
                CheckBox Chk_Select = (CheckBox)ItemX.FindControl("chk");
                if (Chk_Select.Checked)
                {
                    Label lbl = (Label)ItemX.FindControl("btvids");
                    IDs += lbl.Text + ",";
                }

            }
            Session["sTVIDS"] = IDs;
            if (hf_ArrWaiting_GenIDs.Value == "1")
            {

                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=5','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/BondSummary.aspx?Flag=6','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes') ;</script>");
            }
            
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                ASdg.DataSource = DV;
                ASdg.DataBind();
                GenerateArraignmentSerial();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        private void SortGrid_ArrWaiting(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DataView DVAWR = new DataView();
                DVAWR.Table = (DataTable)Session["AWR"];
                DVAWR.Sort = StrExp + " " + StrAcsDec;
                GenerateSerial(DVAWR.Table);
                dg_AWR.DataSource = DVAWR;
                dg_AWR.DataBind();
               // GenerateAWRSerial();
              
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void SortGrid_NotOnSystem(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DataView dv_NOS = new DataView();
                dv_NOS.Table = (DataTable)Session["dv_NOS"];
                dv_NOS.Sort = StrExp + " " + StrAcsDec;
                GenerateNOSSerial();
                dg_NotOnSystem.DataSource = dv_NOS;
                dg_NotOnSystem.DataBind();
                // GenerateAWRSerial();

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void SortGrid_BondWaiting(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                GenerateSerial(DV.Table);
                dg_BondWaiting.DataSource = DV;
                dg_BondWaiting.DataBind();
               // GenerateSerialBondWaiting();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }
        private void SortGridBond(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["sDV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                dgbond.DataSource = DV;
                dgbond.DataBind();
                GenerateSerialBond();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        protected void ASdg_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        protected void dg_NotOnSystem_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid_NotOnSystem(e.SortExpression);
        }

        protected void dg_AWR_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid_ArrWaiting(e.SortExpression);
        }

        protected void dgbondwaiting_sortcommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid_BondWaiting(e.SortExpression);
        }

        protected void dgbond_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGridBond(e.SortExpression);
        }

        protected void btnScan_Click(object sender, EventArgs e)
        {
            try
            {
               btnScan.Visible = false;
               lbl_Message.Visible = false;
               lblMessage.Text = "";
               lbl_Message.Text = "";
               btncheck.Visible = true;
               string Docid = txtDocid.Text.ToString().Trim();
               
               int docid = Convert.ToInt32(Docid);
               txtDocid.Text = "";              
                string[] key ={ "@docid" };
                object[] value ={ docid };

                //string sessionid = "cjp2dy552rru4n45pqwpfyrd";
                
                DataSet DS = ClsDb.Get_DS_BySPArr("usp_hts_GetDocid", key, value);
                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblMessage.Text = "Document ID doest not Exist in the System! Please Scan again with Valid Dcoument ID";
                    
                    string searchpat = "*" + Session.SessionID.ToString() + txtempid.Text + "*.jpg";
                    
                    //string searchpat = "*" + sessionid + txtempid.Text + "*.jpg";
                    
                    string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);
                    if(fileName.Length >= 1)
                    {
                        for (int i = 0; i < fileName.Length; i++)
                        {
                            System.IO.File.Delete(fileName[i].ToString());
                        }
                    }
                }
                else
                {
                    string searchpat = "*" + Session.SessionID.ToString() + txtempid.Text + "*.jpg";
                    //string searchpat = "*" + sessionid + txtempid.Text + "*.jpg";

                    string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);
                    //
                    if (fileName.Length > 1)
                    {
                        fileName = sortfilesbydate(fileName);
                    }
                    string picName, picDestination;
                    //picDestination = ViewState["vNTPATHScanImage"].ToString() + "DOCID_" + docid + ".jpg";
                    for (int i = 0; i < fileName.Length; i++)
                    {


                        jpeg2pdf(fileName[i].ToString(), i);
                        System.IO.File.Delete(fileName[i].ToString());

                    }

                    string sPath = "*" + "TempDOC_" + "*.pdf";
                    int empid = Convert.ToInt32(txtempid.Text);
                    string[] FileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), sPath);

                    //string Path = ViewState["vNTPATHScanImage"].ToString() + "DOCID_" + docid + ".pdf";
                    string Path = ViewState["vNTPATHCaseSummary"].ToString() + "DOCID_" + docid + ".pdf";
                    
                    string[] key1 ={  "@DOCFlag", "@BatchID", "@employee" };
                    object[] value1 ={  1, docid, empid };
                    ClsDb.InsertBySPArr("usp_hts_update_tblbatchsummary", key1, value1);

                    string name = "DOCID_" + docid + ".pdf";

                    string[] keys ={ "@DOCID", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath", "@RecordID" };
                    object[] values ={ docid, System.DateTime.Now, System.DateTime.Now, 1, 10, Convert.ToInt32(txtempid.Text), name ,""};

                    string Recordid=(string)ClsDb.InsertBySPArrRet("usp_InsertForScan", keys, values);


                    Doc doc1 = new Doc();
                    Doc doc2 = new Doc();

                    //if document exist with the same doc id then append..
                    //string ExistPath = ViewState["vNTPATHScanImage"].ToString() + "DOCID_" + docid + ".pdf";

                    string ExistPath = ViewState["vNTPATHCaseSummary"].ToString() + "DOCID_" + docid + ".pdf";

                    if (File.Exists(ExistPath))
                    {
                         doc1.Read(ExistPath);
                        string TempPath = ViewState["vNTPATHScanTemp"].ToString() + "DOCID_" + docid + ".pdf";
                        if (FileName.Length >= 1)
                        {
                            for (int j = 0; j < FileName.Length; j++)
                            {
                                FileName = sortfilesbydate(FileName);
                                doc2.Read(FileName[j].Replace("\\\\", "\\").ToString());
                                doc1.Append(doc2);
                            }
                            doc1.Save(TempPath.Replace("\\\\", "\\"));
                            doc1.Dispose();
                            doc2.Dispose();
                            
                            File.Delete(ExistPath);
                            File.Copy(TempPath, Path);
                            File.Delete(TempPath);

                            for (int j = 0; j < FileName.Length; j++)
                            {
                                System.IO.File.Delete(FileName[j].ToString());
                            }
                        }
                    }
                    else
                    {
                        if (FileName.Length >= 1)
                        {
                            for (int i = 0; i < FileName.Length; i++)
                            {
                                FileName = sortfilesbydate(FileName);

                                doc2.Read(FileName[i].Replace("\\\\", "\\").ToString());
                                doc1.Append(doc2);



                            }
                            if (System.IO.File.Exists(Path))
                            {
                                System.IO.File.Delete(Path);
                                doc1.Save(Path);
                            }
                            else
                            {
                                doc1.Save(Path);
                            }
                            doc2.Dispose();
                            doc1.Dispose();
                            for (int j = 0; j < FileName.Length; j++)
                            {
                                System.IO.File.Delete(FileName[j].ToString());
                            }
                        }
                        

                        
                    }
                
                    
                    
                    //RegisterStartupScript("script","language='javascript'> function NewPopUp(docid){ var path='PreviewDocs.aspx?docid=docid'; window.open(path,'',,'fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');} NewPopUp('" + docid + "');");
                    HttpContext.Current.Response.Write("<script language='javascript'> window.open('../ClientInfo/frmPreview.aspx?RecordID=" + Recordid + "','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');</script>");

                    tdScan.Style["Display"] = "block";
                    lblMessage.Text = "Document Scanned Successfully";
                    Adf.Visible = false;
                    if (ddl_WeekDay.SelectedValue.ToString() == "999")
                    {
                        BindArraignmentWaiting();
                    }
                    if (ddl_WeekDay.SelectedValue.ToString() == "998")
                    {
                        BindBondWaiting();
                    }
                }
                // txtDocid.Text = "";
               // ddl_WeekDay.SelectedIndex=0;
                //temp();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void temp()
        {
            try
            {
                string sPath = "*" + "TempDOC_" + "*.pdf";
                string ExistPath = ViewState["vNTPATHScanImage"].ToString() + "DOCID_" + 145 + ".pdf";
                string Path = ViewState["vNTPATHScanImage"].ToString() + "DOCID_" + 145 + ".pdf";
                string[] FileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), sPath);
                string TempPath = ViewState["vNTPATHScanTemp"].ToString() + "DOCID_" + 145 + ".pdf";
                Doc doc1 = new Doc();
                Doc doc2 = new Doc();
                if (File.Exists(ExistPath))
                    {
                        doc1.Read(ExistPath.Replace("\\\\", "\\").ToString());
                        if (FileName.Length >= 1)
                        {
                            for (int j = 0; j < FileName.Length; j++)
                            {
                                FileName = sortfilesbydate(FileName);
                                doc2.Read(FileName[j].Replace("\\\\", "\\").ToString());
                                
                                doc1.Append(doc2);
                            }
                            
                            doc1.Save(TempPath.Replace("\\\\", "\\"));
                            doc1.Dispose();
                            doc2.Dispose();
                            File.Delete(ExistPath);
                            File.Copy(TempPath, Path);
                            File.Delete(TempPath);

                           
                        }
                    }
                   

            }
            catch (Exception ex)
            { 
            
            }
        }
        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }
        private void jpeg2pdf(string path,int i)
        {
            try
            {
                string Path = ViewState["vNTPATHScanTemp"].ToString()+"TempDOC_"+i+".pdf";
                Document documentx = new Document();
                PdfWriter.getInstance(documentx, new FileStream(Path, FileMode.Create));
                documentx.Open();
                iTextSharp.text.Image jpeg = iTextSharp.text.Image.getInstance(path);
                jpeg.scalePercent(20, 21);
                jpeg.scaleToFit(PageSize.A4.Width - 16, PageSize.A4.Height);
                documentx.Add(jpeg);
                documentx.Close();
                
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);  
            }
        }

        protected void Bondwaiting_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid = ((Label)e.Item.FindControl("bticketid")).Text.ToString();
                   
                    ((HyperLink)e.Item.FindControl("bCauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("serialNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    
                    //Added by ozair  
                    Label Lname = (Label)e.Item.FindControl("blastname");
                    Label Fname = (Label)e.Item.FindControl("bfirstname");
                    if (Lname.Text.Trim().Length > 12)
                    {
                        Lname.ToolTip = Lname.Text;
                        Lname.Text = Lname.Text.Substring(0, 9) + "...";
                    }
                    if (Fname.Text.Trim().Length > 12)
                    {
                        Fname.ToolTip = Fname.Text;
                        Fname.Text = Fname.Text.Substring(0, 9) + "...";
                    }
                    
                    /*Label showscan1 = (Label)e.Item.FindControl("lblshowscanB");//Ozee Bhai i have comment this code(Azwer)..
                    ImageButton img1 = (ImageButton)e.Item.FindControl("ImgDoc1");
                    Label con1 = (Label)e.Item.FindControl("lblSc");
                    if (showscan1.Text == "1")
                    {
                        img1.Visible = true;
                        con1.Visible = true;
                        ((ImageButton)e.Item.FindControl("ImgDoc1")).Attributes.Add("onclick", "javascript:return PopUp(" + ticketid.ToString() + ");");
                    }
                    else
                    {
                        img1.Visible = false;
                        con1.Visible = false;
                    }*/
                    Label con1 = (Label)e.Item.FindControl("lblSc");//Code Added by Azee..
                    ImageButton img1 = (ImageButton)e.Item.FindControl("ImgDoc1");
                    if (con1.Text != "" && Convert.ToInt32(con1.Text) >=1)
                    {
                        img1.Visible = true;
                        con1.Visible = true;
                        ((ImageButton)e.Item.FindControl("ImgDoc1")).Attributes.Add("onclick", "javascript:return PopUp(" + ticketid.ToString() + ");");
                    }
                    else
                    {
                        img1.Visible = false;
                        con1.Visible = false;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void AWRWaiting_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid=((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("AScauseno")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((Label)e.Item.FindControl("ASTIDS")).Text.ToString();
                    
                    //Added by ozair  
                    Label Lname = (Label)e.Item.FindControl("ASlastname");
                    Label Fname = (Label)e.Item.FindControl("ASfirstname");
                    if (Lname.Text.Trim().Length > 12)
                    {
                        Lname.ToolTip = Lname.Text;
                        Lname.Text = Lname.Text.Substring(0, 9) + "...";
                    }
                    if (Fname.Text.Trim().Length > 12)
                    {
                        Fname.ToolTip = Fname.Text;
                        Fname.Text = Fname.Text.Substring(0, 9) + "...";
                    }

                    /*Label showscan = (Label)e.Item.FindControl("lblshowscan");//code comment by azwar
                    ImageButton img = (ImageButton)e.Item.FindControl("ImgDoc");
                    Label con = (Label)e.Item.FindControl("lblScripts");
                    if (showscan.Text == "1")
                    {
                        img.Visible = true;
                        con.Visible = true;
                        ((ImageButton)e.Item.FindControl("ImgDoc")).Attributes.Add("onclick", "javascript:return PopUp(" + ticketid.ToString() + ");");
                    }
                    else
                    {
                        img.Visible = false;
                        con.Visible = false;
                    }*/
                    //
                    ImageButton img = (ImageButton)e.Item.FindControl("ImgDoc");
                    Label con = (Label)e.Item.FindControl("lblScripts");
                    if(con.Text !="" && Convert.ToInt32(con.Text) >=1)
                    {
                        img.Visible = true;
                        con.Visible = true;
                        ((ImageButton)e.Item.FindControl("ImgDoc")).Attributes.Add("onclick", "javascript:return PopUp(" + ticketid.ToString() + ");");
                    }
                    else
                    {
                        img.Visible = false;
                        con.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            try
            {
                int rpttype = Convert.ToInt32(ddl_WeekDay.SelectedValue);
                lbl_Message.Text = "";
                lblMessage.Text = "";
                int docid = Convert.ToInt32(txtDocid.Text);

                //added by Ozair to check for arraignment or bond
                int type = 0;
                if (rpttype==998)
                {
                    type = 202;
                }
                else if (rpttype == 999)
                {
                    type = 201;
                }
                //
                //modified by Ozair
                string[] key ={ "@docid","@type" };
                object[] value ={ docid,type };
                DataSet DS = ClsDb.Get_DS_BySPArr("usp_hts_GetDocid_Check", key, value);

                if (DS.Tables[1].Rows.Count <= 0 && DS.Tables[0].Rows.Count <= 0)
                //
                {
                    lblMessage.Text = "Document ID doest not Exist in the System! Please Scan again with Valid Dcoument ID";
                    lblMessage.Visible = true;
                    //txtDocid.Text = "";
                    return;
                }
                else if(DS.Tables[1].Rows.Count > 0 && DS.Tables[0].Rows.Count <= 0)
                {
                    if (rpttype == 999)
                    {
                        if (DS.Tables[1].Rows[0]["status"].ToString() != "" )//for Arraignment waiting check for documentID
                        {
                            lblMessage.Text = "DocumentID Does Not Exist in the Arraigment Waiting Report.";
                            lblMessage.Visible = true;
                            return;
                        }
                        
                    }
                    else if (rpttype == 998)
                    {
                        if (DS.Tables[1].Rows[0]["status"].ToString() != "" )//for bond waiting check for documentID..
                        {
                            lblMessage.Text = "DocumentID Does Not Exist in the Bond Waiting Report.";
                            lblMessage.Visible = true;
                            return;
                        }
                    }
                }
                //
                if (rpttype == 999 )
                {
                    if (DS.Tables[0].Rows[0]["status"].ToString() != "" )//for Arraignment waiting check for documentID
                    {
                        if (DS.Tables[0].Rows[0]["Docpicflag"].ToString() != "")
                        {
                            if (Convert.ToInt32(DS.Tables[0].Rows[0]["Docpicflag"]) > 0)
                            {
                                lblMessage.Text = "Document ID already Scanned in the system.It will append your new documents with old one";
                                lblMessage.Visible = true;
                                
                                //txtDocid.Text = txtDocid.Text;
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "DocumentID Does Not Exist in the Arraigment Waiting Report.";
                        lblMessage.Visible = true;
                        return;
                    }
                }
                else if (rpttype == 998)
                {
                    if (DS.Tables[0].Rows[0]["status"].ToString() != "")//for bond waiting check for documentID..
                    {
                        if (DS.Tables[0].Rows[0]["Docpicflag"].ToString() != "")
                        {
                            if (Convert.ToInt32(DS.Tables[0].Rows[0]["Docpicflag"]) > 0)
                            {
                                lblMessage.Text = "Document ID already Scanned in the system.It will append your new documents with old one";
                                lblMessage.Visible = true;
                                
                                //txtDocid.Text = txtDocid.Text;
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "DocumentID Does Not Exist in the Bond Waiting Report.";
                        lblMessage.Visible = true;
                        return;
                    }

                }
                
                btnScan.Visible = true;
                btncheck.Visible = false;
                tdDoc.Style.Add("Display", "block");
                Adf.Visible = true;
                //scan.Visible = false;
                tdScan.Style["Display"] = "none";
                
                HDScan.Value = docid.ToString();
            }

            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void PrintAllBonds_OnClick(object sender, EventArgs e)
        {
            try
            {
                string TicketIDs= "";
                foreach (DataGridItem Itemx in dg_BondWaiting.Items)
                {
                    CheckBox chk = (CheckBox)Itemx.FindControl("chk");
                    Label lbl = (Label)Itemx.FindControl("bticketid");
                    if (chk.Checked == true)
                    {
                        if (TicketIDs != String.Empty)
                        {
                            TicketIDs = String.Concat(TicketIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            TicketIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }

                }

                Session["TicketID4Bonds"] = TicketIDs;
                HttpContext.Current.Response.Write("<script language='javascript'> window.open('../Reports/PrintAllBonds.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');</script>");
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }

        }

        private void DisplayNotOnSystem(int FlagID)
        {
            try
            {
                string[] key ={ "@FlagID" };
                object[] value ={ FlagID };
                DataSet DS = ClsDb.Get_DS_BySPArr("USP_HTS_NOTONSYSTEM_REPORT", key, value);
                
                DataTable dt_NOS = DS.Tables[0].Copy();
                dt_NOS.Columns.Add("SNO", typeof(Int32));
                //dt_NOS.Columns["SNO"].AutoIncrement = true;
                //dt_NOS.Columns["SNO"].SetOrdinal(0);
                dt_NOS = GenerateNOSSerial(dt_NOS);
                Session["dv_NOS"] = dt_NOS;
                dg_NotOnSystem.DataSource = dt_NOS;
                dg_NotOnSystem.DataBind();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void FillDDLWithFlags()
        {
            try
            {
                DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_GET_ALL_FLAGS");
                ddl_Flags.DataTextField = "Description";
                ddl_Flags.DataValueField = "FlagID_PK";
                ddl_Flags.DataSource = dt;
                ddl_Flags.DataBind();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        protected void ddl_Flags_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DisplayNotOnSystem(Convert.ToInt32(ddl_Flags.SelectedValue));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private void GetPR_POAReport()
        {
            try
            {
                DataTable dtPR = FormatPRReport(ClsDb.Get_DT_BySPArr("USP_HTS_GET_PR_POA_REPORT"));
                dg_ProbReq.DataSource = dtPR;
                dg_ProbReq.DataBind();
                if (dtPR.Rows.Count > 0)
                    tbl_PR_Report.Style[HtmlTextWriterStyle.Display] = "block";
                else
                    tbl_PR_Report.Style[HtmlTextWriterStyle.Display] = "none";
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        private DataTable FormatPRReport(DataTable dtToFormat)
        {
            DataTable dt = dtToFormat.Copy();
            dt.Columns.Add("SNO", typeof(int));
            int lastTicketID = -1;
            int sno = 1;
            int currentID = -1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)
                {
                    dt.Rows[i]["SNO"] = sno;
                    sno++;
                    lastTicketID = Convert.ToInt32(dt.Rows[i]["TicketID_PK"]);
                    continue;
                }
                else
                {
                    currentID = Convert.ToInt32(dt.Rows[i]["TicketID_PK"]);
                    if (lastTicketID != currentID)
                    {
                        dt.Rows[i]["SNO"] = sno;
                        sno++;
                        lastTicketID = currentID;
                    }
                }
            }
            return dt;
        }

        protected void dg_ProbReq_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ticketid = "";
                    ticketid = ((Label)e.Item.FindControl("lbl_TicketID")).Text.ToString();
                    ((HyperLink)e.Item.FindControl("lbl_CauseNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                    ((HyperLink)e.Item.FindControl("lblSNo")).NavigateUrl = "../clientinfo/ViolationFeeold.aspx?search=0&caseNumber=" + ticketid;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ImgCrystalSingle_Click(object sender, ImageClickEventArgs e)
        {

        }

    }
}

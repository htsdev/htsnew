using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;

using System.IO;
using System.Configuration;
using FrameWorkEnation.Components;



namespace Chap0601
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		string strCnn = (string)ConfigurationSettings.AppSettings["returndocconn"];
		int DOCNO;

		clsENationWebComponents ClsDB = new clsENationWebComponents("ReturnDockets");
		

		
		
		private void Page_Load(object sender, System.EventArgs e)
		{	
			DOCNO = (Int16) Convert.ChangeType(Request.QueryString["docid"].ToString(),typeof(Int16)) ;
			File.Delete(Server.MapPath("") + "/" + Session.SessionID.ToString() + DOCNO.ToString() + ".pdf" );
			GeneratFile();
			String docNm = DOCNO.ToString() + ".pdf";
			string FILEPATH	= Server.MapPath("") + "/temp/" + Session.SessionID.ToString() + DOCNO + ".pdf";
		}
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Unload += new System.EventHandler(this.WebForm1_Unload);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void WebForm1_Unload(object sender, System.EventArgs e)
		{
		//File.Delete(Server.MapPath("") + "/" + DOCNO.ToString() + ".pdf" );
		}

		public void GeneratFile()
		{
			DOCNO = (Int16) Convert.ChangeType(Request.QueryString["docid"].ToString(),typeof(Int16)) ;
			Session["DocID"]=DOCNO.ToString();


			//Change by Ajmal
            //SqlDataReader sqlDR;
			IDataReader sqlDR;

			string[] key = { "@DOCNO"};
			object[] values={DOCNO.ToString()};

			sqlDR = ClsDB.Get_DR_BySPArr("USP_GET_ALL_DOCPIC",key,values);
					//
			string s = Server.MapPath("") ;

						
	
			
			Document documentx = new Document();
			String ImgNm;
			try 
			{
				PdfWriter.getInstance(documentx,new FileStream(Server.MapPath("") + "/temp/" + Session.SessionID.ToString() + DOCNO + ".pdf", FileMode.Create));
				documentx.Open();
				while (sqlDR.Read())
				{
					ImgNm = sqlDR.GetValue(0).ToString() + sqlDR.GetValue(1).ToString() + ".jpg";
					Response.Write(ImgNm.ToString() + "<br>");
					iTextSharp.text.Image jpeg = iTextSharp.text.Image.getInstance((byte[]) sqlDR.GetValue(2));
					jpeg.scaleToFit(PageSize.A4.Width - 16 , PageSize.A4.Height);
					documentx.Add(jpeg);
				}
			}

			catch(DocumentException de) 
			{
				Response.Write (de.Message);
			}

			catch(IOException ioe) 
			{
				Response.Write (ioe.Message);
			}
			finally
			{
				sqlDR.Close();
				documentx.Close();
				documentx = null;
			}

			Session["DocNm"] = Session.SessionID.ToString() + DOCNO.ToString() + ".pdf";
			

			Response.Redirect("frame.aspx",false);
				
		}

		public void LoadPDF(string strFileName )
		{
			Response.ContentType = "Application/pdf";			
			Response.WriteFile(strFileName);
			Response.End();
		}
	}
}

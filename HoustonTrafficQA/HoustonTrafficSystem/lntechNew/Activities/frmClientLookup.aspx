<%@ Page Language="c#" Inherits="HTP.Activities.frmClientLookup" CodeBehind="frmClientLookup.aspx.cs"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Taffic Alerts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script language="javascript">

	
	function DateValidation()
	{
	
	 var datDate1= Date.parse(document.getElementById('dtFrom').value);
     var datDate2= Date.parse(document.getElementById('dtTo').value);
     var dif=((datDate2-datDate1)/(24*60*60*1000))
     if(dif>31)
     {
     alert('Please Enter date range of within 30 days');
     return(false);
     }
     if(dif<0)
     {
     alert('Please Enter From date which is greater than to date');
     return(false);
     }
     document.getElementById('lblMessage').innerText = '';
     document.getElementById('tbl_wait').style.display='block';
     document.getElementById('td_main').style.display='none';
     document.getElementById("tblClientGrid").style.display='none' ;
	 document.getElementById("tblPageNavigation").style.display='none';
	 if(document.getElementById("chkAll")!=null)
	 {
	    document.getElementById("chkAll").style.display='none';
	    document.getElementById("lblAll").style.display='none';
	 
	 }
	
	 
	 document.getElementById("tblNewInfoDetail").style.display='none';
	 document.getElementById("tblNewInfoDetailGrid").style.display='none';
	 document.getElementById("tblPrevInfoGrid").style.display='none';
	 document.getElementById('tblClientNavigation').style.display='none';
     
	return (true);
	}	
	
	function RefreshPage()
	{
	    if(document.getElementById('hf_Refresh').value=="1")
	    {
	        document.getElementById('hf_Refresh').value="0";
	        document.Form1.submit();
	    }
	    document.getElementById('td_main').style.display='block';
	    
	    if(document.getElementById('hf_rec_count').value=="0" /*|| document.getElementById('hf_Hide_flag').value=="1"*/)
	    {
	     ShowHidePageNavigation();
	     document.getElementById("tblNewInfoDetail").style.display = 'none' ;
		 document.getElementById("tblNewInfoDetailGrid").style.display = 'none'  ;
		 document.getElementById("tblPrevInfoGrid").style.display = 'none'  ;
	    }
	    else
	    {
	    document.getElementById('tblNewInfoDetail').style.display='block';
	     document.getElementById('tblPrevInfoGrid').style.display='block';
	     document.getElementById("tblPrevInfoGrid").style.display = 'block';
	    }
	}
	
	
		function SignUp()
		{  
			var caseNo = document.getElementById("txtNewCaseNo");
			var status = document.getElementById("ddlUpdateStatus");
			var status2 = document.getElementById("txtStatus");
			var profile = document.getElementById("hf_ProfileExists").value;
			
						
			/*if (status.selectedIndex == 0)
				{
				alert("Please first update the quote status.");
				status.focus();
				return false;
				}*/
				
				if(document.getElementById('chkNoContactList').checked==true)
				{
				    alert('Sorry, No contact box selected. The client can not sign up, please use update button to move client to no contact group.');
				    return(false);
				}
				
				if(status.options[status.selectedIndex].text != status2.value)
				{
				alert("Sorry, The alert status was changed and the new status selected was not updated. Please update the status first.");
				status.focus();
				return false;
				}
				
				var validate = DoValidation();
				if(validate == false)
				{
				return(false);
				}
				
/*			if (caseNo.value == '')
			{ 
				alert("To sign up the client for an FTA Ticket, please use case number hyperlink.");
				return false;
			}
*/			
		}
	
		function TogglePageNavigation()
		{
			var tbl =  document.getElementById("tblPageNavigation").style;
			
			if (tbl.display == 'block')
				tbl.display = 'none';
			else
				tbl.display = 'block';
		}

		function ShowHidePageNavigation()
		{ 
			var tbl =document.getElementById("tblPageNavigation").style;
			var	el  =document.getElementById("tblClientGrid").style;
			var	e2  =document.getElementById("tblNewInfoDetailGrid").style;
			var	e3  =document.getElementById("tblPrevInfoGrid").style;
			var	e4  =document.getElementById("tblNewInfoDetail").style;
			var	e5  =document.getElementById("tblClientNavigation").style ;
			var e6  =document.getElementById("tblClientNavigation").style ;
			if(document.getElementById ("chkAll")!=null)
			{
			    var chk = document.getElementById("chkAll").style ;
		        var lbl = document.getElementById("lblAll").style;
			}
			
	
			var lbl2 =  document.getElementById("lblPageSize").style;
			var ddl = document.getElementById("ddlPageSize").style;
					
			if (document.getElementById("txtRecCount").value == 0 ) 
				{
				tbl.display='none';		
				el.display ='none';
				document.Form1.imgClient.src = "../images/grdexpand.bmp";
				e2.display ='none';				
				document.Form1.imgNew.src  = "../images/grdexpand.bmp";
				e3.display ='none';
				e4.display ='none';
				e5.display = 'none';
				e6.display = 'none';
				if(document.getElementById ("chkAll")!=null)
				{
				    chk.display = 'none';	
				    lbl.display = 'none';
				}
				
				
				//lbl2.display = 'none';
				//ddl.display = 'none';			
				}
			else
				{
				if(document.getElementById ("chkAll")!=null)
				{
				if (document.getElementById("chkAll").checked == true)
					{
					tbl.display = 'none';
					//lbl2.display ='none';
					//ddl.display ='none';
					}
				else
					{
					tbl.display='block';	
					//lbl2.display ='block';
					//ddl.display ='block';
					}
				}	
				el.display = 'block';
				document.Form1.imgClient.src = "../images/grdcollapse.bmp";
				e2.display =  'block';
				document.Form1.imgNew.src  = "../images/grdcollapse.bmp";
				e3.display = 'block';
				e4.display ='block';
				e5.display = 'block';
				e6.display = 'block';	
				if(document.getElementById ("chkAll")!=null)
				{
				    chk.display = 'block';	
				    lbl.display = 'block';		
				}
															
				}
		}
		
		function ShowHideClientGrid()
		{
			var client = document.getElementById("tblClientGrid").style ;
			var tbl2   = document.getElementById("tblPageNavigation").style;
			if(document.getElementById ("chkAll")!=null)
			{
			    var chk = document.getElementById("chkAll");
			    var lbl = document.getElementById("lblAll").style;
			
			}
			

			if ( client.display == 'none' )
				{
				client.display = 'block';
				if(document.getElementById ("chkAll")!=null)
				{
				    if (chk.checked == false )
					    tbl2.display = 'block';
				}
				if (document.getElementById("txtRecCount").value != '0'  ) 
				{
					if(document.getElementById ("chkAll")!=null)
					{
					    chk.style.display = 'block';
					    lbl.display = 'block';
					}
					
				}
				document.Form1.imgClient.src = "../images/grdcollapse.bmp";
				}
			else
				{
				client.display = 'none';
				tbl2.display = 'none';
				if(document.getElementById ("chkAll")!=null)
			    {
				    chk.style.display = 'none';
				    lbl.display = 'none';
				}
					
				document.Form1.imgClient.src = "../images/grdexpand.bmp";
				}
				
		}


		function ShowHideNewInfo()
		{
			var elemnt1 = document.getElementById("tblNewInfoDetail").style ;
			var elemnt2 = document.getElementById("tblNewInfoDetailGrid").style ;
			var elemnt3 = document.getElementById("tblPrevInfoGrid").style ;

			if ( elemnt1.display == 'none' )
				{
				elemnt1.display = 'block';
				elemnt3.display = 'block';
				document.Form1.imgNew.src  = "../images/grdcollapse.bmp";
				}
			else
				{
				elemnt1.display = 'none';	
				elemnt3.display = 'none';	
				document.Form1.imgNew.src  = "../images/grdexpand.bmp";
				}
				
			if (elemnt2.display == 'none' )
				elemnt2.display = 'block';
			else
				elemnt2.display = 'none';	
				
				
		}

		function ShowHidePrevInfo()
		{
			var elemnt3 = document.getElementById("tblPrevInfoGrid").style ;


			if ( elemnt3.display == 'none' )
				{
				elemnt3.display = 'block';
				}
			else
				{
				elemnt3.display = 'none';		
				}
		}


		function DoValidation()
		{
			/*if ( document.Form1.ddlUpdateStatus.selectedIndex == 0 )
				{
				alert("Please select status.");
				document.Form1.ddlUpdateStatus.focus();
				return false;
				}*/
				
				var profile = document.getElementById("hf_ProfileExists").value;
			
			if(profile=="1")
			{
			alert('Sorry, Profile already exists for this record');
			return(false);
			}
				
				var pending = '<% = ViewState["PendingStatusID"] %>';
				
				if(document.getElementById('ddlUpdateStatus').value == pending && document.getElementById('chkNoContactList').checked == true)
				{
				    alert('Sorry, A record with pending status can not be sent to no contact list. Please select a valid status');
				    return(false);
				}
				
			var str = document.Form1.txtComments.value;
			if (str.length > 1980)
				{
				alert("Please insert only 2000 characters as comments!");
				document.Form1.txtComments.focus();
				return false;
				}
				
			var cdlY = document.getElementById("optCDLYes");
			var cdlN = document.getElementById("optCDLNo");
			
			if (cdlY.checked == false && cdlN.checked == false)
				{
				alert("Please select an option for CDL.");
				cdlY.focus();
				return false;
				}
				
			var accY =  document.getElementById("optAccYes");
			var accN = document.getElementById("optAccNo");
			
			if (accY.checked == false && accN.checked== false)
			{
				alert("Please select an option for Accident.");
				accY.focus();
				return false;
			}
			
			document.getElementById('tbl_plzwait1').style.display='block';
			document.getElementById('tblNewInfoDetail').style.display='none';

		}	
		


    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </aspnew:ScriptManager>
    <table id="tblMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
        <tbody>
            <tr>
                <td style="width: 800px">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 800px">
                                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server">
                                    </uc1:ActiveMenu>
                                </td>
                            </tr>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 800px">
                                    <table class="clsmainhead" id="tblHeader" cellspacing="0" cellpadding="0" width="100%"
                                        align="left" border="0">
                                        <tr>
                                            <td class="producthead" align="center" width="25" height="20">
                                            </td>
                                            <td class="producthead" width="593" height="20">
                                                <font color="#ffffff">Uploaded Previous Client Report: Filters</font>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td id="td_main" style="width: 800px">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                        <tr>
                                            <td valign="middle" height="27">
                                                &nbsp;
                                                <asp:Label ID="lblFromDate" runat="server" Width="103px" Height="16px" CssClass="frmtd"> Upload Range:</asp:Label>
                                            </td>
                                            <td style="width: 150px" valign="middle">
                                                <ew:CalendarPopup ID="dtFrom" runat="server" Width="73px" EnableHideDropDown="True"
                                                    Font-Names="Tahoma" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                    AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                    PadSingleDigits="True" ToolTip="Select starting list date" Font-Size="8pt" ImageUrl="../images/calendar.gif">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                                &nbsp; &nbsp;
                                                <asp:Label ID="Label2" runat="server" Width="38px" CssClass="frmtd">To:</asp:Label>
                                            </td>
                                            <td style="width: 113px">
                                                &nbsp;
                                                <ew:CalendarPopup ID="dtTo" runat="server" Width="73px" EnableHideDropDown="True"
                                                    Font-Names="Tahoma" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                    AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                    PadSingleDigits="True" ToolTip="Select ending list date" Font-Size="8pt" ImageUrl="../images/calendar.gif">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td style="width: 182px">
                                                <asp:Label ID="lblCaseType" runat="server" Width="152px" CssClass="frmtd">Uploaded Case Status:</asp:Label>
                                            </td>
                                            <td style="width: 43px" align="left">
                                                <asp:DropDownList ID="cmbCaseType" runat="server" Width="204px" CssClass="label">
                                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                                    <asp:ListItem Value="2" Selected="True">ARRAIGNMENT OR BLANK</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" height="25">
                                                &nbsp;
                                                <asp:Label ID="lblClientType" runat="server" Width="96px" CssClass="frmtd"> Type:</asp:Label>
                                            </td>
                                            <td style="width: 150px" valign="middle">
                                                <asp:DropDownList ID="cmbClientType" runat="server" Width="148px" Height="16px" CssClass="label">
                                                    <%--tahir  7028 11/19/2009--%> 
                                                    <%--<asp:ListItem Value="2">ALL</asp:ListItem>--%>
                                                    <asp:ListItem Value="1" Selected="True">PREVIOUS CLIENT</asp:ListItem>
                                                    <%--tahir  7028 11/19/2009--%> 
                                                    <%--<asp:ListItem Value="0">PREVIOUS QUOTE</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 113px">
                                                &nbsp;
                                            </td>
                                            <td style="width: 182px">
                                                <asp:Label ID="lblPCCR" runat="server" Width="88px" CssClass="frmtd">Alert Status:</asp:Label>
                                            </td>
                                            <td colspan="1" align="left">
                                                <asp:DropDownList ID="cmbPCCR" runat="server" Width="204px" CssClass="label">
                                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                                    <asp:ListItem Value="1" Selected="True">NO ACTION</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px" width="111">
                                                &nbsp;
                                                <asp:Label ID="lblCourtType" runat="server" Width="104px" CssClass="frmtd">Court Location:</asp:Label>
                                            </td>
                                            <td style="width: 150px; height: 13px">
                                                <asp:DropDownList ID="cmbCourtType" runat="server" Width="147px" CssClass="label">
                                                    <asp:ListItem Value="0" Selected="True">ALL</asp:ListItem>
                                                    <asp:ListItem Value="1">INSIDE COURTS</asp:ListItem>
                                                    <asp:ListItem Value="2">OUTSIDE COURTS</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 113px; height: 13px">
                                                &nbsp;
                                            </td>
                                            <td style="width: 182px; height: 13px" align="left" valign="middle">
                                                <asp:Label ID="Label8" runat="server" Width="167px" CssClass="frmtd">Only Future Court Dates:</asp:Label>
                                            </td>
                                            <td style="width: 45px; height: 13px" align="left" width="45" valign="middle">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:RadioButton ID="optFutureYes" runat="server" CssClass="label" GroupName="FutureCourtDate"
                                                                Text="Yes" Width="47px"></asp:RadioButton>
                                                        </td>
                                                        <td style="width: 100px">
                                                            <asp:RadioButton ID="optFutureNo" runat="server" CssClass="label" GroupName="FutureCourtDate"
                                                                Text="No" Checked="True" Width="47px"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 6px" width="111">
                                            </td>
                                            <td style="width: 150px; height: 6px">
                                            </td>
                                            <td style="width: 113px; height: 6px">
                                            </td>
                                            <td align="right" style="width: 182px; height: 6px">
                                            </td>
                                            <td align="left" style="width: 45px; height: 6px" width="45">
                                                <table>
                                                    <tr>
                                                        <td style="width: 61px">
                                                            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="../Images/grdsearch.gif"
                                                                AlternateText="Search" OnClientClick="return DateValidation()"></asp:ImageButton>
                                                        </td>
                                                        <td style="width: 100px">
                                                            <asp:ImageButton ID="btn_Reset" runat="server" ImageUrl="../Images/grdreset.gif">
                                                            </asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="pnl_MainGrid" runat="server" UpdateMode="Conditional" Visible="true">
                        <ContentTemplate>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <table id="tbl_wait" width="100%" style="display: none">
                                                <tr>
                                                    <td class="clssubhead" valign="middle" align="center">
                                                        <img src="../Images/plzwait.gif" />
                                                        Please wait while your request is being processed..
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="5" height="25">
                                            <asp:Label ID="lblMessage" runat="server" Width="248px" Height="13px" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TDHeadingnew" height="15" style="width: 800px">
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="TDHeading" width="40%" style="height: 33px">
                                                        <table class="clsmainhead" id="tblClientList" cellspacing="0" cellpadding="0" width="100%"
                                                            align="left" border="0">
                                                            <tr>
                                                                <td align="center" width="10%" height="20">
                                                                    <img id="imgClient" onclick="ShowHideClientGrid();" src="../images/grdcollapse.bmp"
                                                                        name="imgClient">
                                                                </td>
                                                                <td class="producthead" align="left" width="90%" height="20">
                                                                    <font color="#ffffff">Client List:</font>&nbsp;
                                                                    <asp:Label ID="lblRecCount" runat="server" Width="160px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                        ForeColor="White" Visible="False"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="TDHeading" align="right" width="52%" style="height: 33px">
                                                        <table id="tblPageNavigation" cellspacing="0" cellpadding="0" width="100%" align="right"
                                                            border="0">
                                                            <tr>
                                                                <td align="right">
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPageSize" runat="server" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White">Rec/Page</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlPageSize" runat="server" CssClass="frmtd" Font-Size="Smaller"
                                                                            AutoPostBack="True">
                                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                                            <asp:ListItem Value="15">15</asp:ListItem>
                                                                            <asp:ListItem Value="20">20</asp:ListItem>
                                                                            <asp:ListItem Value="25">25</asp:ListItem>
                                                                            <asp:ListItem Value="30" Selected="True">30</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCurrPage" runat="server" Width="88px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White" Visible="False">Current Page :</asp:Label>&nbsp;
                                                                        <asp:Label ID="lblPNo" runat="server" Width="30px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White" Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblGoto" runat="server" Width="16px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White" Visible="False">Goto</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cmbPageNo" runat="server" CssClass="frmtd" Font-Size="Smaller"
                                                                            Visible="False" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="TDHeading" align="right" width="3%" style="height: 33px">
                                                        <!--Modified by kazim task-id:2665
                                                            Set the visible property of chkall to false,b/c it should not be visible when no record found
                                                            within the specific range of date.
                                                            -->
                                                        <asp:CheckBox ID="chkAll" runat="server" Font-Names="Verdana" Font-Size="8.5pt" ForeColor="White"
                                                            AutoPostBack="True" Visible="False"></asp:CheckBox>
                                                    </td>
                                                    <td class="TDHeading" align="left" width="5%" style="height: 33px">
                                                        <asp:Label ID="lblAll" runat="server" Font-Names="Verdana" Font-Size="8.5pt" ForeColor="White"
                                                            Visible="False">ALL</asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="width: 800px">
                                            <table id="tblClientGrid" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <asp:DataGrid ID="dgClient" runat="server" Width="796px" Font-Names="Verdana" Font-Size="2px"
                                                            AllowSorting="True" AutoGenerateColumns="False" PageSize="30" AllowPaging="True">
                                                            <SelectedItemStyle BackColor="#FFFFCC"></SelectedItemStyle>
                                                            <ItemStyle BackColor="White"></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="grdheader" VerticalAlign="Middle">
                                                            </HeaderStyle>
                                                            <FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                            </FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="S. No">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        &nbsp;
                                                                        <asp:LinkButton ID="btnSerial" runat="server" CommandName="GetDetails"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hf_TicketNumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="ClientName" HeaderText="Client Name">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="language" HeaderText="Language">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:TemplateColumn SortExpression="listdate" HeaderText="Upload Date">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label6" runat="server" CssClass="grdlabel" Text='<%# DataBinder.Eval(Container, "DataItem.listdate", "{0:MM/dd/yyyy}") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="midnumber" HeaderText="MID Number">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="courtdate" HeaderText="Court Date" DataFormatString="{0:MM/dd/yyyy}"
                                                                    SortExpression="courtdate">
                                                                    <HeaderStyle CssClass="TaskGrid_Head" HorizontalAlign="Left"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TicketNumber" HeaderText="TicketNumber" Visible="False">
                                                                </asp:BoundColumn>
                                                                 <asp:BoundColumn DataField="StatusDescription" HeaderText="Alert Status">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                            <PagerStyle VerticalAlign="Middle" Visible="False" NextPageText="Next" Font-Size="XX-Small"
                                                                PrevPageText="Previous" HorizontalAlign="Right" ForeColor="Black" BackColor="#999999">
                                                            </PagerStyle>
                                                        </asp:DataGrid>
                                                        <asp:HiddenField ID="hf_Rec_count" runat="server" Value="0" />
                                                        <asp:HiddenField ID="hf_ProfileExists" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 800px">
                                            <table id="tblNewTckInfo" cellspacing="0" cellpadding="0" width="100%" align="left"
                                                border="0">
                                                <tr>
                                                    <td class="TDHeadingnew" align="right">
                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="TDHeading" width="80%" height="20">
                                                                    <table class="clsmainhead" id="tblNewInfoHeading" cellspacing="0" cellpadding="0"
                                                                        align="left" border="0">
                                                                        <tr>
                                                                            <td align="center" width="31" height="20">
                                                                                <img id="imgNew" onclick="ShowHideNewInfo();" src="../images/grdcollapse.bmp" name="imgNew">
                                                                            </td>
                                                                            <td class="producthead" width="593" height="20">
                                                                                <font color="#ffffff">Alert Status</font> : <a name="details" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="TDHeading" align="right" width="20%">
                                                                    <table id="tblClientNavigation" cellspacing="0" cellpadding="0" width="130" align="right"
                                                                        border="0">
                                                                        <tr>
                                                                            <td class="TDHeading" valign="middle" align="right">
                                                                                <asp:LinkButton ID="btnPrevClient" runat="server" Font-Names="Verdana" Font-Size="8.5pt"
                                                                                    ForeColor="White"> Previous </asp:LinkButton>|
                                                                            </td>
                                                                            <td class="TDHeading" valign="middle" align="left">
                                                                                <asp:LinkButton ID="btnNextClient" runat="server" Width="32px" Font-Names="Verdana"
                                                                                    Font-Size="8.5pt" ForeColor="White">Next</asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <aspnew:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>

                                                                <script type="text/javascript" language="javascript">
                                                                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                                                                    prm.add_pageLoaded(pageLoaded);
                                                                    prm.add_beginRequest(beginRequest);
                                                                    var postbackElement; 

                                                                    function beginRequest(sender, args) {
                                                                        postbackElement = args.get_postBackElement();
                                                                    }

                                                                    function pageLoaded(sender, args) {
                                                                        var updatedPanels = args.get_panelsUpdated();
                                                                        if (typeof(postbackElement) == 'undefined') {
                                                                            return;
                                                                        }
                                                                          RefreshPage();
                                                                          navigateToRecord(); 
                                                                        
                                                                     }
                                                                </script>

                                                                <table id="tbl_plzwait1" style="display: none" width="800px">
                                                                    <tr>
                                                                        <td class="clssubhead" valign="middle" align="center">
                                                                            <img src="../Images/plzwait.gif" />
                                                                            Please wait while your request is being processed..
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table id="tblNewInfoDetail" runat="server" style="display: block" cellspacing="0"
                                                                    cellpadding="0" width="100%" border="0">
                                                                    <tr id="trNewInfoClient">
                                                                        <td valign="top" width="35%" height="120">
                                                                            <table id="tblNewInfoClient" style="border-collapse: collapse" cellspacing="0" cellpadding="0"
                                                                                rules="all" width="100%" border="1">
                                                                                <tr>
                                                                                    <td class="frmtd" width="101" height="20">
                                                                                        &nbsp;Alert Status:
                                                                                    </td>
                                                                                    <td width="60%" height="20">
                                                                                        <asp:DropDownList ID="ddlUpdateStatus" runat="server" Width="160px" CssClass="frmtd">
                                                                                            <asp:ListItem Value="1">NO ACTION</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" width="101" height="20">
                                                                                        &nbsp;Name:
                                                                                    </td>
                                                                                    <td width="60%" height="20">
                                                                                        <asp:Label ID="lblName" runat="server" Width="152px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Language:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:Label ID="lblLanguage" runat="server" Width="120px" Height="16" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;CDL:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:RadioButton ID="optCDLYes" runat="server" Width="17px" Height="16px" CssClass="grdlabel"
                                                                                            GroupName="cdl" Text="Yes"></asp:RadioButton><asp:RadioButton ID="optCDLNo" runat="server"
                                                                                                Width="24px" Height="16px" CssClass="grdlabel" GroupName="cdl" Text="No">
                                                                                        </asp:RadioButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Accident:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:RadioButton ID="optAccYes" runat="server" Width="17px" Height="16px" CssClass="grdlabel"
                                                                                            GroupName="ACC" Text="Yes"></asp:RadioButton><asp:RadioButton ID="optAccNo" runat="server"
                                                                                                Width="24px" Height="16px" CssClass="grdlabel" GroupName="ACC" Text="No">
                                                                                        </asp:RadioButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Estimated Fee:&nbsp;
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:Label ID="lblFee" runat="server" Width="88px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td valign="top" width="35%" height="120">
                                                                            <table id="tblContacts" style="border-collapse: collapse" cellspacing="0" cellpadding="0"
                                                                                rules="all" width="100%" border="1">
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Contact&nbsp;1:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:Label ID="lblContact1" runat="server" Width="176px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Contact&nbsp;2:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:Label ID="lblContact2" runat="server" Width="184px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Contact&nbsp;3:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:Label ID="lblContact3" runat="server" Width="184px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" width="83" height="21">
                                                                                        &nbsp;Contact&nbsp;4:
                                                                                    </td>
                                                                                    <td height="21">
                                                                                        <asp:Label ID="lblContact4" runat="server" Width="184px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" width="83" height="21">
                                                                                        &nbsp;Contact&nbsp;5:
                                                                                    </td>
                                                                                    <td height="21">
                                                                                        <asp:Label ID="lblContact5" runat="server" Width="184px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="frmtd" height="20">
                                                                                        &nbsp;Contact&nbsp;6:
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        <asp:Label ID="lblContact6" runat="server" Width="184px" Height="16px" CssClass="grdlabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td valign="top" width="30%" height="120">
                                                                            <table id="tblComments" style="border-collapse: collapse" cellspacing="0" cellpadding="0"
                                                                                rules="all" width="100%" border="1">
                                                                                <tr>
                                                                                    <td valign="top" width="100%" height="120">
                                                                                        <asp:TextBox ID="txtComments" runat="server" Width="100%" Height="120px" TextMode="MultiLine"
                                                                                            Rows="10" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <table style="border-collapse: collapse; display: none" cellspacing="0" cellpadding="0"
                                                                                rules="all" width="100%" border="1">
                                                                                <tr>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkPriceShopping" runat="server" CssClass="frmtd" Text="Price Shopping">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkHiredAnother" runat="server" CssClass="frmtd" Text="Hired Another">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkHandleMySelf" runat="server" CssClass="frmtd" Text="Handle Myself">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkPaidTickets" runat="server" CssClass="frmtd" Text="Paid Ticket">
                                                                                        </asp:CheckBox>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkDSCProb" runat="server" CssClass="frmtd" Text="DSC / Prob">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkNoMoney" runat="server" CssClass="frmtd" Text="No Money"></asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkTooExpensive" runat="server" CssClass="frmtd" Text="Too Expensive">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkPoorService" runat="server" CssClass="frmtd" Text="Poor Service">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <asp:HiddenField ID="hf_UpdateFlag" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hf_Refresh" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hf_Hide_flag" runat="server" Value="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr bgcolor="#eeeeee">
                                                                        <td class="frmtd">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="middle" align="right" colspan="2">
                                                                            <asp:CheckBox ID="chkNoContactList" runat="server" Width="135px" Height="16px" CssClass="frmtd"
                                                                                Text="No Contact List"></asp:CheckBox>&nbsp;
                                                                            <asp:ImageButton ID="btnSignUp" runat="server" ImageUrl="../Images/signup.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                                ID="btnUpdateComments" runat="server" ImageUrl="../Images/grdupdate.gif" AlternateText="Update">
                                                                            </asp:ImageButton>&nbsp;
                                                                        </td>
                                                                        <td style="display: none; visibility: hidden">
                                                                            <asp:TextBox ID="txtRecCount" runat="server" Width="15px" Height="15px"></asp:TextBox><asp:TextBox
                                                                                ID="txtNewCaseNo" runat="server"></asp:TextBox><asp:TextBox ID="txtStatus" runat="server"></asp:TextBox><asp:TextBox
                                                                                    ID="txtFTATicket" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <aspnew:AsyncPostBackTrigger ControlID="btnUpdateComments" EventName="Click" />
                                                            </Triggers>
                                                        </aspnew:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr id="trdgViolNew">
                                                    <td valign="top" align="left">
                                                        <table id="tblNewInfoDetailGrid" style="display: block" cellspacing="0" cellpadding="0"
                                                            width="100%" border="0">
                                                            <tr>
                                                                <td id="td_title" class="producthead" style="height: 20px" runat="server" visible="false">
                                                                    NON-HIRED CASES
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:DataGrid ID="dgViolNew" runat="server" Width="795px" Font-Names="Verdana" Font-Size="2px"
                                                                        AutoGenerateColumns="False">
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                        </HeaderStyle>
                                                                        <FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                        </FooterStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderText="Case Number">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:HyperLink ID="lnkNewCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNo") %>'>
                                                                                    </asp:HyperLink>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="ViolDesc" HeaderText="Violation Description">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="250px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CaseStatus" HeaderText="Case Status">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="135px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CourtName" HeaderText="Court Name">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CourtDate" HeaderText="Court Date" DataFormatString="{0:d}">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="RecordId">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recordid") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recordid") %>'>
                                                                                    </asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="Midnum">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblMidNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="address">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="zip">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zip") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="CourtDate" DataFormatString="{0:t}" HeaderText="Court Time">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CourtNumber" HeaderText="Court Room">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="FineAmount" HeaderText="Fine Amount" DataFormatString="{0:C}">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle VerticalAlign="Middle" NextPageText="Next Page" Font-Size="XX-Small"
                                                                            PrevPageText="Previous Page" HorizontalAlign="Right" ForeColor="Black" BackColor="#999999">
                                                                        </PagerStyle>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="tblPrevInfoMain" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td>
                                                                    <table class="clsmainhead" id="tblPrevInfoHeading" cellspacing="0" cellpadding="0"
                                                                        width="100%" align="left" border="0">
                                                                        <tr bgcolor="#eeeeee">
                                                                            <td id="td_title_hired" runat="server" class="producthead" style="height: 20px">
                                                                                &nbsp; HIRED CASES
                                                                            </td>
                                                                            <td class="producthead">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table id="tblPrevInfoGrid" style="display: block" cellspacing="0" cellpadding="0"
                                                                        width="100%" border="0">
                                                                        <tr>
                                                                            <td style="width: 787px">
                                                                                <asp:DataGrid ID="dgViolPrevious" runat="server" Width="795px" Font-Names="Verdana"
                                                                                    Font-Size="2px" AutoGenerateColumns="False" PageSize="5" ShowFooter="True" OnItemDataBound="dgViolPrevious_ItemDataBound">
                                                                                    <FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                                    </FooterStyle>
                                                                                    <ItemStyle BackColor="White"></ItemStyle>
                                                                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                                    </HeaderStyle>
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderText="Case Number">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnPrevCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNo") %>'
                                                                                                    CommandName="cmdPrevCaseNo">
                                                                                                </asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="ViolDesc" HeaderText="Violation Description">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="250px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CaseStatus" HeaderText="Case Status">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="135px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:TemplateColumn HeaderText="Viol Outcome">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnViolOutcome" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolOutCome") %>'
                                                                                                    CommandName="cmdViolOutcome">
                                                                                                </asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolOutCome") %>'>
                                                                                                </asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="CourtName" HeaderText="Court Name">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CourtDate" HeaderText="Court Date" DataFormatString="{0:d}">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CourtDate" DataFormatString="{0:t}" HeaderText="Court Time">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CourtNumber" HeaderText="Court Room">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="50px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="AmountPaid" HeaderText="Amount Paid">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Firm" HeaderText="Firm">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:TemplateColumn Visible="False" HeaderText="ticketid">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblTicketId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'
                                                                                                    Visible="False" />
                                                                                                <asp:Label ID="lbl_CDLFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CDLFlag") %>'
                                                                                                    Visible="False" />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="TextBox9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketit") %>'>
                                                                                                </asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="ibtn_GC" runat="server" ImageUrl="~/Images/note04.gif" OnClientClick="return(false)"
                                                                                                    ToolTip='<%# DataBinder.Eval(Container, "DataItem.Generalcomments") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                    </Columns>
                                                                                    <PagerStyle VerticalAlign="Middle" NextPageText="Next Page" Font-Size="XX-Small"
                                                                                        PrevPageText="Previous Page" HorizontalAlign="Right" ForeColor="Black" BackColor="#999999">
                                                                                    </PagerStyle>
                                                                                </asp:DataGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                    <script language="javascript"> ShowHidePageNavigation();  </script>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" height="11">
                                                        <asp:HiddenField ID="hf_FocusFlag" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <uc1:Footer ID="Footer1" runat="server">
                                                        </uc1:Footer>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                        </Triggers>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
        </tbody>
    </table>

    <script type="text/javascript">
        function navigateToRecord()
        {
			var flag_focus = document.getElementById('hf_FocusFlag').value;
			
			if(flag_focus=="1")
			{
			window.location="#details";
			}
	    }
			
    </script>

    </form>
</body>
</html>

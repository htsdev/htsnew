using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.Common;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for Courts.
	/// </summary>
	public partial class SecCourts : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btn_Delete;
		protected System.Web.UI.WebControls.Button btn_Submit;
	clsLogger clog = new clsLogger();
		clsSession ClsSession=new clsSession();
		clsCourts ClsCourts =new clsCourts();
        clsPricingPlans ClsPricingPlans = new clsPricingPlans();
        //khalid
        DataView DV;
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        //
		protected System.Web.UI.WebControls.DataGrid dg_Result;
		protected System.Web.UI.WebControls.DropDownList ddl_SettingRequest;
		protected System.Web.UI.WebControls.DropDownList ddl_BondType;
		protected System.Web.UI.WebControls.DropDownList ddl_States;
		protected System.Web.UI.WebControls.TextBox txt_CourtName;
		protected System.Web.UI.WebControls.TextBox txt_CourtContact;
		protected System.Web.UI.WebControls.TextBox txt_JudgeName;
		protected System.Web.UI.WebControls.TextBox txt_ShortName;
		protected System.Web.UI.WebControls.TextBox txt_Address1;
		protected System.Web.UI.WebControls.TextBox txt_Address2;
		protected System.Web.UI.WebControls.TextBox txt_City;
		protected System.Web.UI.WebControls.TextBox txt_ZipCode;
		protected System.Web.UI.WebControls.TextBox txt_DDName;
		protected System.Web.UI.WebControls.TextBox txt_CC11;
		protected System.Web.UI.WebControls.TextBox txt_CC12;
		protected System.Web.UI.WebControls.TextBox txt_CC13;
		protected System.Web.UI.WebControls.TextBox txt_CC14;
		protected System.Web.UI.WebControls.TextBox txt_CC1;
		protected System.Web.UI.WebControls.TextBox txt_CC2;
		protected System.Web.UI.WebControls.TextBox txt_CC3;
		protected System.Web.UI.WebControls.TextBox txt_VisitCharges;
		protected System.Web.UI.WebControls.CheckBox chkb_InActive;
		protected System.Web.UI.WebControls.Label lblcourtID;
		protected System.Web.UI.WebControls.Label lbl_Msg;
		protected System.Web.UI.WebControls.CheckBox chkcriminal;
		clsENationWebComponents ClsDB=new clsENationWebComponents();

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				// Check for valid Session if not then redirect to login page
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
                    //Kazim 3276 2/28/2008  Remove Response.End to solve bug
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2" && ClsSession.GetCookie("sUserID", this.Request).ToString() != "Cathia")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                    }
                    else //To stop page further execution
                    {
                        if (!IsPostBack)
                        {
                            //Commented by Ozair
                            //dtpCourtDate.SelectedDate = DateTime.Now.Date;
                            //Added by Ozair
                            dtpCourtDate.Clear();
                            //
                            btn_Submit.Attributes.Add("OnClick", "javascript:return Submit();");
                            FillSettingRequest();
                            FillBondType();
                            FillStates();
                            FillGrid();
                        }
                    }
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                lbl_Msg.Text = ex.Message;
			}
		}
			
		public void FillSettingRequest()
		{
			ddl_SettingRequest.Items.Clear();
			DataSet ds=ClsDB.Get_DS_BySP("usp_HTS_Get_CourtSettingRequestType");
			ddl_SettingRequest.DataSource=ds;
			ddl_SettingRequest.DataTextField=ds.Tables[0].Columns[1].ColumnName;
			ddl_SettingRequest.DataValueField=ds.Tables[0].Columns[0].ColumnName;
			ddl_SettingRequest.DataBind();
			ddl_SettingRequest.SelectedValue="4";
		}
		public void FillBondType()
		{
			ddl_BondType.Items.Clear();
			DataSet ds=ClsDB.Get_DS_BySP("usp_hts_list_all_bondtype");
			ddl_BondType.DataSource=ds;
			ddl_BondType.DataTextField=ds.Tables[0].Columns[1].ColumnName;
			ddl_BondType.DataValueField=ds.Tables[0].Columns[0].ColumnName;
			ddl_BondType.DataBind();
		}
		public void FillStates()
		{
			ddl_States.Items.Clear();
			DataSet ds=ClsDB.Get_DS_BySP("USP_HTS_Get_State");
			ddl_States.DataSource=ds;
			ddl_States.DataTextField=ds.Tables[0].Columns[1].ColumnName;
			ddl_States.DataValueField=ds.Tables[0].Columns[0].ColumnName;
			ddl_States.DataBind();
			ddl_States.SelectedValue="45";
		}
		public void FillGrid()
		{
            try
            {
                DataSet DS = ClsCourts.GetCourtInfo();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DV = new DataView(DS.Tables[0]);
                    Session["DS"] = DV;
                    dg_Result.DataSource = DS;
                    dg_Result.DataBind();
                   
                }
            }
            catch (Exception e)
            {
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   			
			this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
			this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
			this.dg_Result.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Result_ItemCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dg_Result_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				lbl_Msg.Text="";
				if(e.CommandName=="CourtNo")
				{
					Label courtid=(Label) e.Item.FindControl("lbl_CourtID");
					DataSet ds=ClsCourts.GetCourtInfo(Convert.ToInt32(courtid.Text));
					lblcourtID.Text=ds.Tables[0].Rows[0]["courtid"].ToString();
					txt_Address1.Text=ds.Tables[0].Rows[0]["address"].ToString();
					txt_Address2.Text =ds.Tables[0].Rows[0]["address2"].ToString();
					string fax=ds.Tables[0].Rows[0]["fax"].ToString().Trim();
					if(fax.Length>0)
					{
						txt_CC1.Text=fax.Substring(0,3);
						txt_CC2.Text=fax.Substring(3,3);
						txt_CC3.Text=fax.Substring(6,4);
					}
					string phone=ds.Tables[0].Rows[0]["phone"].ToString().Trim();
					if(phone.Length>0)
					{
						txt_CC11.Text=phone.Substring(0,3);
						txt_CC12.Text=phone.Substring(3,3);
						txt_CC13.Text=phone.Substring(6,4);
						if (phone.Length>10)
						{
							txt_CC14.Text=phone.Substring(10);
						}
					}
					txt_City.Text =ds.Tables[0].Rows[0]["city"].ToString();
					txt_CourtContact.Text=ds.Tables[0].Rows[0]["courtcontact"].ToString();
					txt_CourtName.Text=ds.Tables[0].Rows[0]["courtname"].ToString();
					txt_DDName.Text=ds.Tables[0].Rows[0]["shortcourtname"].ToString();
					txt_JudgeName.Text=ds.Tables[0].Rows[0]["judgename"].ToString();
					txt_ShortName.Text=ds.Tables[0].Rows[0]["shortname"].ToString();
					txt_VisitCharges.Text=(Convert.ToInt32(ds.Tables[0].Rows[0]["visitcharges"])).ToString();
					if (txt_VisitCharges.Text=="0")
					{
						txt_VisitCharges.Text ="";
					}
					txt_ZipCode.Text=ds.Tables[0].Rows[0]["zip"].ToString();
					ddl_States.SelectedValue=ds.Tables[0].Rows[0]["state"].ToString();
					ddl_BondType.SelectedValue=ds.Tables[0].Rows[0]["bondtype"].ToString();
					ddl_SettingRequest.SelectedValue=ds.Tables[0].Rows[0]["settingrequesttype"].ToString();
					
					if(ds.Tables[0].Rows[0]["inactiveflag"].ToString() == "1")
					{
						chkb_InActive.Checked=true;
					}
					else
					{
						chkb_InActive.Checked=false;
					}
					
					if(Convert.ToBoolean(ds.Tables[0].Rows[0]["IsCriminalCourt"])==true)
					{ 
						chkcriminal.Checked=true;
					}
					else
					{ 
						chkcriminal.Checked=false;
					}
                    //added by ozair
                    if (Convert.ToDateTime(ds.Tables[0].Rows[0]["repdate"]).ToShortDateString() != "1/1/1900")
                    {
                        dtpCourtDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["repdate"]);
                    }
                    else
                    {
                        dtpCourtDate.Clear();
                    }
                    //
					btn_Delete.Enabled=true;
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                lbl_Msg.Text = ex.Message;
			}
		}

		private void btn_Delete_Click(object sender, System.EventArgs e)
		{
			try
			{
				lbl_Msg.Text ="";
				int ccount=ClsCourts.DeleteCourt(Convert.ToInt32(lblcourtID.Text));
				if (ccount==0)
				{
					lbl_Msg.Text= "Court Deleted";
					FillGrid();
					ClearValues();
				}
				else
				{
					lbl_Msg.Text="Cannot delete this court; clients are assigned to this court";
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void btn_Submit_Click(object sender, System.EventArgs e)
		{
			try
			{
				lbl_Msg.Text ="";
				if(lblcourtID.Text=="0")
				{

                    DbTransaction trans = ClsDB.DBBeginAndReturnTransaction();

					SetValues();
					int court=ClsCourts.AddCourt(trans);
					if (court==0)
					{
						lbl_Msg.Text="Court Name Already Exist";


					}
					else
					{
                        if (SetPriceValue(court, trans) != false)
                        {
                            FillGrid();
                            ClearValues();

                        }
                        else
                        {
                            lbl_Msg.Text = "An Error Occured during this operation.";
                        }
					}
				}
				else
				{
					SetValues();
					if(ClsCourts.UpdateCourt()==true)
					{
						FillGrid();
						ClearValues();
					}				
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                lbl_Msg.Text = "An Error Occured during this operation.";
			}
		}
		
		public void ClearValues()
		{
			try
			{
				txt_Address1.Text="";
				txt_Address2.Text="";
				txt_CC1.Text="";
				txt_CC11.Text="";
				txt_CC12.Text="";
				txt_CC13.Text="";
				txt_CC14.Text="";
				txt_CC2.Text="";
				txt_CC3.Text="";
				txt_City.Text="";
				txt_CourtContact.Text="";
				txt_CourtName.Text="";
				txt_DDName.Text="";
				txt_JudgeName.Text="";
				txt_ShortName.Text="";
				txt_VisitCharges.Text="";
				txt_ZipCode.Text="";
				ddl_States.SelectedValue="45";
				ddl_BondType.ClearSelection();
				ddl_SettingRequest.SelectedValue="4";
				lblcourtID.Text="0";
				btn_Delete.Enabled=false;
                //Commented by Ozair
                //dtpCourtDate.SelectedDate = DateTime.Now.Date;
                //Added by Ozair
                dtpCourtDate.Clear();
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                lbl_Msg.Text = ex.Message;
			}
		}
		public void SetValues()
		{
			try
			{
				if(lblcourtID.Text!="0")
				{
					ClsCourts.CourtID=Convert.ToInt32(lblcourtID.Text);
				}
				ClsCourts.CourtName=txt_CourtName.Text.Trim();
				ClsCourts.JudgeName=txt_JudgeName.Text.Trim();
				ClsCourts.ShortName=txt_ShortName.Text.Trim();
				ClsCourts.Address1=txt_Address1.Text.Trim();
				ClsCourts.Address2=txt_Address2.Text.Trim();
				ClsCourts.City=txt_City.Text.Trim();
				ClsCourts.StateID=Convert.ToInt32(ddl_States.SelectedValue);
				ClsCourts.ZipCode=txt_ZipCode.Text.Trim();
				ClsCourts.CourtDropDownName=txt_DDName.Text.Trim();
				ClsCourts.CourtContact=txt_CourtContact.Text.Trim();
				ClsCourts.Phone= txt_CC11.Text+txt_CC12.Text+txt_CC13.Text+txt_CC14.Text;
				ClsCourts.Fax=txt_CC1.Text+txt_CC2.Text+txt_CC3.Text;
				ClsCourts.SettingRequestType=Convert.ToInt32(ddl_SettingRequest.SelectedValue);
				ClsCourts.BondType=Convert.ToInt32(ddl_BondType.SelectedValue);
                //added/modified by Ozair 
                if (dtpCourtDate.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    ClsCourts.LetterOfRep = dtpCourtDate.SelectedDate;
                }
                else
                {
                    ClsCourts.LetterOfRep = Convert.ToDateTime("1/1/1900");
                }
                //
				if(txt_VisitCharges.Text=="")
				{
					ClsCourts.VisitCharges=0;
				}
				else
				{
					ClsCourts.VisitCharges=Convert.ToDouble(txt_VisitCharges.Text.Trim());
				}
				if(chkb_InActive.Checked)
				{
					ClsCourts.InActiveFlag=1;
				}
				else
				{
					ClsCourts.InActiveFlag=0;
				}
				if(chkcriminal.Checked)
				{ 
					ClsCourts.IsCriminalCourt=1;
				}
				else
				{  
					ClsCourts.IsCriminalCourt=0;
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
                lbl_Msg.Text = ex.Message;
			}
		}

        //Added by Usman
        protected bool SetPriceValue(int court, DbTransaction trans)
        {
            try
            {
                //init transaction
                const int templateId = 45;

                //Getting values from template
                DataSet dsTemplate = ClsPricingPlans.GetPlan(templateId);
                DataSet dsTemplateDetail = ClsPricingPlans.GetPlanDetails(templateId);

                if (dsTemplate.Tables.Count > 0)
                {
                    DataTable dtTemplate = dsTemplate.Tables[0];
                    DataTable dtTemplateDetail = ClsPricingPlans.GetPlanDetails(templateId).Tables[0];
                    //Setting Values
                    ClsPricingPlans.PlanDescription = txt_CourtName.Text + "-default plan";


                    ClsPricingPlans.PlanShortName = txt_ShortName.Text + "_plan";

                    ClsPricingPlans.EffectiveDate = DateTime.Now;

                    ClsPricingPlans.EndDate = DateTime.Now.AddYears(2);

                    ClsPricingPlans.CourtID = court;

                    ClsPricingPlans.ShowAll = 0;

                    ClsPricingPlans.PlanID = ClsPricingPlans.AddPlanInfo(trans);

                    if (ClsPricingPlans.PlanID == 0)
                    {
                        ClsDB.DBTransactionRollback();
                        return false;
                    }

                    if (dsTemplateDetail.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dtTemplateDetail.Rows)
                        {
                            ClsPricingPlans.CategoryID = Convert.ToInt32(dr["CategoryId"]);
                            ClsPricingPlans.BasePrice = Convert.ToDouble(dr["BasePrice"]);
                            ClsPricingPlans.SecondaryPrice = Convert.ToDouble(dr["SecondaryPrice"]);
                            ClsPricingPlans.BasePercentage = Convert.ToDouble(dr["BasePercentage"]);
                            ClsPricingPlans.SecondaryPercentage = Convert.ToDouble(dr["SecondaryPercentage"]);
                            ClsPricingPlans.BondBase = Convert.ToDouble(dr["BondBase"]);
                            ClsPricingPlans.BondSecondary = Convert.ToDouble(dr["BondSecondary"]);
                            ClsPricingPlans.BondBasePercentage = Convert.ToDouble(dr["BondBasePercentage"]);
                            ClsPricingPlans.BondSecondaryPercentage = Convert.ToDouble(dr["BondSecondaryPercentage"]);
                            ClsPricingPlans.BondAssumtion = Convert.ToDouble(dr["BondAssumption"]);
                            ClsPricingPlans.FineAssumption = Convert.ToDouble(dr["FineAssumption"]);

                            if (ClsPricingPlans.UpdatePlanDetails(trans) == false)
                            {
                                ClsDB.DBTransactionRollback();
                                return false;
                            }
                        }
                    }

                    ClsDB.DBTransactionCommit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
                return false;
            }

        }

        //Added by ozair
        protected void dg_Result_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                if (((Label)e.Item.FindControl("lbl_letterofrep")).Text == "1/1/1900")
                {
                    ((Label)e.Item.FindControl("lbl_letterofrep")).Visible = false;
                }
            }
        }

        //added khalid 27-9-07
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;

                dg_Result.DataSource = DV;
                dg_Result.DataBind();
                //SerialNumber();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }


        protected void dg_Result_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        
     

	}
}

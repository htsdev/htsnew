using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data.SqlClient; 
using lntechNew.Components.ClientInfo;

namespace HTP.Activities
{
	/// <summary>
	/// Summary description for QuoteCallBackDetail.
	/// </summary>
	public partial class QuoteCallBackDetail : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label lblName;
        protected System.Web.UI.WebControls.Label lblCourtDates;
        protected System.Web.UI.WebControls.Label lblAmount;
        protected System.Web.UI.WebControls.Label lblContactNo;
        protected System.Web.UI.WebControls.DropDownList cmbOQR;
        protected System.Web.UI.WebControls.Label lblOQRDate;
        protected System.Web.UI.WebControls.DropDownList cmbFollowUp;
        protected System.Web.UI.WebControls.Label lblFollowUpDate;
        protected System.Web.UI.WebControls.RadioButtonList optFollowUp;
        
        protected eWorld.UI.CalendarPopup calQCallback;
        protected eWorld.UI.TimePicker tpQCallBack;
        protected eWorld.UI.CalendarPopup calApptDate;
        protected eWorld.UI.TimePicker tpApptTime;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.DropDownList cmbCallBackTime;
        protected System.Web.UI.WebControls.DropDownList cmbApptTime;
        protected System.Web.UI.WebControls.TextBox txtAppointmentDate;
        protected System.Web.UI.WebControls.TextBox txtCallBackDate;
        protected System.Web.UI.WebControls.TextBox txtFlagUpdated;
        protected System.Web.UI.WebControls.TextBox txtCurrentDate;
        protected System.Web.UI.WebControls.Label lblMessage;


		string CourtDateStatus;
		string ContactNo;
		int GQouteID;
		int GTicketID;
	    clsENationWebComponents ClsDb=new clsENationWebComponents();
		clsLogger clog = new clsLogger();
        clsSession cSession = new clsSession();
        //Yasir Kamal 5744 04/06/2009 Quote call back changes.
        clsCaseDetail ClsCaseDetail = new clsCaseDetail();

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			try
			{
				//string strTemp;

				//btnClose.Attributes.Add("OnClick", "CloseWindow();")
				int QuoteID = (int) Convert.ChangeType(Request.QueryString["QuoteID"], typeof(int));
				int TicketID = (int) Convert.ChangeType(Request.QueryString["TicketID"], typeof(int));
				GQouteID = QuoteID;
				GTicketID = TicketID;
				btnSubmit.Attributes.Add("OnClick", "return ValidateControl();");

				if (!IsPostBack)
				{
                   
                    clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Initiated", "", GTicketID);

					//intTicketID_PK = Session("TicketID")
                    //EDITED BY FAHAD 
               
					txtCurrentDate.Text = String.Format("{0:MM/dd/yyyy}",DateTime.Now.ToShortDateString());
					FillControlsByTicketID(QuoteID,TicketID);
                    string[] key = {"@QuoteID" };
                    object[] value = { QuoteID };
                    DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_Get_QuoteCallBackByQuoteID",key,value);
                    DataSet ds_CaseDetail = ClsCaseDetail.GetCaseDetail(TicketID);
                    dgDetailInfo.DataSource = ds_CaseDetail;
                    dgDetailInfo.DataBind();


                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //    lbldesc.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    //}
					//strTemp = DateTime.Now.Date.ToString();
                    //khalid 3249  3/3/08 To add comments Control to the page
                    //Nasir 6098 08/21/2009 general comment initialize overload method
                    WCC_GeneralComments.Initialize (TicketID, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), 1, "Label", "clsinputadministration", "clsbutton","Label",true);
                    WCC_GeneralComments.Button_Visible = false;
                    

                    ViewState["cmbFollowUpSelected"] = cmbFollowUp.SelectedValue;
                    ViewState["cmbFollowUpSelectedText"] = cmbFollowUp.SelectedItem.Text ;

                    ViewState["optionSelected"] = optFollowUp.SelectedItem.Text;
                    ViewState["CallBackDateSelected"] = CallBackDate.SelectedDate.ToShortDateString();

                      
				}

			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
	}



        private void dgDetailInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label desc = (Label)e.Row.FindControl("Label4");

                    if (desc.Text.Length > 20)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 20).ToLowerInvariant() + "...";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

         }


		private void FillControlsByTicketID(int QuoteID, int TicketID)
		{
            //Change by Ajmal
            //SqlDataReader DRControl = null;
			IDataReader DRControl=null;
			try
			{
				string[] key1={"@QuoteID"};
				object[] value1={QuoteID};
				DRControl = ClsDb.Get_DR_BySPArr("usp_HTS_Get_QuoteCallBackByQuoteID",key1,value1);
				
				DataSet ds_cmbOQR=ClsDb.Get_DS_BySP("usp_HTS_Get_AllQuoteResult");	
				//Bind Quote Result with DropDown
				cmbOQR.DataSource=ds_cmbOQR;
				cmbOQR.DataTextField=ds_cmbOQR.Tables[0].Columns[1].ColumnName;
				cmbOQR.DataValueField=ds_cmbOQR.Tables[0].Columns[0].ColumnName;
				cmbOQR.DataBind();	
				
				DataSet ds_cmbFollowUp=ClsDb.Get_DS_BySP("usp_HTS_Get_AllQuoteResult");	
				//Bind Followup with DropDown
				cmbFollowUp.DataSource=ds_cmbFollowUp;
				cmbFollowUp.DataTextField=ds_cmbFollowUp.Tables[0].Columns[1].ColumnName;
				cmbFollowUp.DataValueField=ds_cmbFollowUp.Tables[0].Columns[0].ColumnName;
				cmbFollowUp.DataBind();		
				
				DataSet ds_optFollowUp=ClsDb.Get_DS_BySP("usp_HTS_Get_All_tblChoice");	
				//Bind Followup with DropDown
				optFollowUp.DataSource=ds_optFollowUp;
				optFollowUp.DataTextField=ds_optFollowUp.Tables[0].Columns[1].ColumnName;
				optFollowUp.DataValueField=ds_optFollowUp.Tables[0].Columns[0].ColumnName;
				optFollowUp.DataBind();		
				
				
				
				
				if (DRControl.Read()) //  Read data in DataReader
				{   
					lblName.Text = DRControl["Customer"].ToString(); 
					//lblCourtDates.Text = Get_CourtDates(TicketID);
                    lblAmount.Text = String.Format("{0:C2}", Convert.ToDouble(DRControl["calculatedtotalfee"]));
					lblContactNo.Text = Get_ContactNo(TicketID);
                    // Noufil 4487 08/04/2008 Courtlocation added
                    // lblcrt.Text = DRControl["crt"].ToString();

					cmbOQR.SelectedValue = DRControl["QuoteResultID"] + "";
					lblOQRDate.Text = DRControl["QuoteResultDate"] + "";
					cmbFollowUp.SelectedValue = DRControl["FollowUPID"].ToString();
					//lblFollowUpDate.Text = DRControl["FollowUPDate"] + "";

                    DateTime dtFollowUpDate = Convert.ToDateTime("01/01/1900");
                    try { dtFollowUpDate = Convert.ToDateTime(DRControl["FollowUPDate"]); }
                    catch { dtFollowUpDate = Convert.ToDateTime("01/01/1900"); }

                    if (dtFollowUpDate.ToShortDateString() == Convert.ToDateTime("1/1/1900").ToShortDateString())
                    {
                        lblFollowUpDate.Text = "1/1/1900";
                        lblFollowUpDate.Visible = false;
                    }
                    else
                    {
                        lblFollowUpDate.Text = dtFollowUpDate.ToShortDateString();
                        lblFollowUpDate.Visible = true;
                    }

					optFollowUp.SelectedValue = DRControl["FollowUpYN"] + "";
					
                        
					// callback date
					if (Convert.ToDateTime(DRControl["CallBackDate"]).ToShortDateString() == "1/1/1900")
					{
						//calQCallback.Clear();
                        //txt_cb_Day.Text = "";
                        //txt_cb_Month.Text = "";
                        //txt_cb_Year.Text = "";
                        CallBackDate.SelectedDate = DateTime.Now;
					}
					else
					{
                        DateTime callback = Convert.ToDateTime(DRControl["CallBackDate"].ToString().Trim());
                        //txt_cb_Year.Text = callback.Year.ToString().Substring(2, 2); ;
                        //txt_cb_Month.Text = callback.Month.ToString();
                        //txt_cb_Day.Text = callback.Day.ToString();
                        CallBackDate.SelectedDate = callback;

						//calQCallback.SelectedDate =Convert.ToDateTime(DRControl["CallBackDate"].ToString().Trim());
						//calQCallback.VisibleDate = calQCallback.SelectedDate;

					}
				
					//callback time
                    //if (DRControl["CallBackTime"].ToString().Trim() == "" || DRControl["CallBackTime"].ToString().Trim() == "--")
                    //{
                    //    //tpQCallBack.Clear();
                    //    ddl_cb_time.SelectedValue = "--";
                    //}
                    //else
                    //{
                    //    //tpQCallBack.SelectedTime = Convert.ToDateTime(DRControl["CallBackTime"].ToString().Trim());
                    //    ddl_cb_time.SelectedValue = DRControl["CallBackTime"].ToString().Trim();
                    //}
                        
						
                    //if (Convert.ToDateTime(DRControl["AppointmentDate"]).ToShortDateString() == "1/1/1900" )
                    //{
                    //    //calApptDate.Clear();
                    //   // txt_App_Day.Text = "";
                    //    //txt_App_Month.Text = "";
                    //    //txt_App_Year.Text = "";
                    //}
                    //else
                    //{
                    //    DateTime AppDate = Convert.ToDateTime(DRControl["AppointmentDate"].ToString().Trim());
                    //    txt_App_Year.Text = AppDate.Year.ToString().Substring(2, 2);
                    //    txt_App_Month.Text = AppDate.Month.ToString();
                    //    txt_App_Day.Text = AppDate.Day.ToString();
                        
                    //    //calApptDate.SelectedDate = Convert.ToDateTime(DRControl["AppointmentDate"].ToString().Trim());
                    //    //calApptDate.VisibleDate  = calApptDate.SelectedDate;
                    //}
                    //if (DRControl["AppointmentTime"].ToString().Trim() == "" || DRControl["AppointmentTime"].ToString().Trim() == "--")
                    //{
                    //    //tpApptTime.Clear();
                    //    ddl_ap_time.SelectedValue = "--";
                    //}
                    //else
                    //{
						
                    //    //tpApptTime.SelectedTime = Convert.ToDateTime(DRControl["AppointmentTime"].ToString().Trim());
                    //    ddl_ap_time.SelectedValue = DRControl["AppointmentTime"].ToString().Trim();
						
                    //}

                    
				
				
				}
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			finally
			{
				DRControl.Close();
			}
		}


		private string Get_ContactNo(int TicketId)
		{								
			try
			{
				DataSet dsContactNo = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_TicketsContactByTicketID","@TicketID_PK",TicketId);
		
				foreach (DataRow  dr in  dsContactNo.Tables[0].Rows)
				{
					if (dr["phno"].ToString().Trim() == "" )
					{
						ContactNo += dr["phno"].ToString();
					}
					else
					{
                        ContactNo += FormatPhoneNumber(dr["phno"].ToString()) + "(" + dr["contacttype"].ToString() + ")";
						ContactNo += "<BR />";
					}
				}																						  

	
			}

			catch (Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		return ContactNo;										 
		}									 
	

		private string Get_CourtDates(int TicketId)
		{		
			try
			{
				DataSet dsCDate=ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_TicketsCourtDates","@TicketID_PK",TicketId);
		 
				//  if structResult.intCode = 1 Then 
				//With structResult.dstResult.Tables("TicketsCourtDates")
		
				foreach (DataRow  dr in  dsCDate.Tables[0].Rows)
				{
				//CourtDateStatus += dr["CourtDatenStatus"].ToString();
                    CourtDateStatus += String.Format("{0:d}", Convert.ToDateTime(dr["CourtDate"])) + " " + String.Format("{0:t}", Convert.ToDateTime(dr["CourtDate"])) + "<b> " + Convert.ToString(dr["ViolationDescription"]) + "</b> <BR />";
				}
				
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			return CourtDateStatus;
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);
            this.dgDetailInfo.RowDataBound += new GridViewRowEventHandler(this.dgDetailInfo_RowDataBound);
          

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{   //khalid 2/23/08 task  //to add acomments control
         //WCC_GeneralComments.AddComments();
			Update(GQouteID,GTicketID);
		}

		private void Update(int QuoteID,int TicketID)
		{
			try
			{
                WCC_GeneralComments.AddComments();
                

                DateTime callback = CallBackDate.SelectedDate;
                DateTime appDate = Convert.ToDateTime("1/1/1900");
                //try { callback = Convert.ToDateTime(txt_cb_Month.Text + "/" + txt_cb_Day.Text + "/" + txt_cb_Year.Text); }
                //catch { callback = Convert.ToDateTime("1/1/1900"); }
               // try { appDate = Convert.ToDateTime(txt_App_Month.Text + "/" + txt_App_Day.Text + "/" + txt_App_Year.Text); }
             //   catch { appDate = Convert.ToDateTime("1/1/1900"); }
					txtFlagUpdated.Text = "1";
                    
                    //string[] key={"@QuoteID","@TicketID_FK","@QuoteResultID","@QuoteResultDate","@FollowUPID","@FollowUPDate","@FollowUpYN","@QuoteComments","@AppointmentDate","@CallBackDate","@CallBackTime","@AppointmentTime"};
                    //object[] value1 ={ QuoteID, TicketID, cmbOQR.SelectedValue, lblOQRDate.Text, cmbFollowUp.SelectedValue, lblFollowUpDate.Text, optFollowUp.SelectedValue, txtComments.Text, appDate.ToString("MM/dd/yyyy"), callback.ToString("MM/dd/yyyy"), ddl_cb_time.SelectedValue, ddl_ap_time.SelectedValue };
                    string[] key ={ "@QuoteID", "@TicketID_FK", "@QuoteResultID", "@QuoteResultDate", "@FollowUPID", "@FollowUPDate", "@FollowUpYN","@CallBackDate","@CallBackTime", "@EmpID" };
                    object[] value1 ={ QuoteID, TicketID, cmbOQR.SelectedValue, lblOQRDate.Text, cmbFollowUp.SelectedValue, lblFollowUpDate.Text, optFollowUp.SelectedValue,callback.ToString("MM/dd/yyyy"), CallBackDate.SelectedDate.TimeOfDay.ToString() ,Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request))};

                    //if (txt_cb_Month.Text == "" || txt_cb_Day.Text == "" || txt_cb_Year.Text == "")
                    //    value1[9] = DBNull.Value;
               //     if (txt_App_Year.Text == "" || txt_App_Month.Text == "" || txt_App_Day.Text == "")
                 //       value1[8] = DBNull.Value;
			
					ClsDb.ExecuteSP("usp_HTS_Update_tblViolationQuote",key,value1);	
					//Response.Redirect("QuoteCallBackMain.aspx",false);
                    string selectedDate = CallBackDate.SelectedDate.ToShortDateString();

                    if (ViewState["CallBackDateSelected"].ToString() != selectedDate)
                    {
                        clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: call back date changed from [" + ViewState["CallBackDateSelected"].ToString()+ "] to [" + selectedDate + "]", "", GTicketID);
                    }
                    if (cmbFollowUp.SelectedItem.Text != ViewState["cmbFollowUpSelectedText"].ToString())
                    {
                        if (cmbFollowUp.SelectedValue == "1" || cmbFollowUp.SelectedValue == "5" || cmbFollowUp.SelectedValue == "14")
                        {
                            clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up call status changed from [" + ViewState["cmbFollowUpSelectedText"].ToString() + "] to [----------]", "", GTicketID);
                        }
                        else if (ViewState["cmbFollowUpSelected"].ToString() == "1" || ViewState["cmbFollowUpSelected"].ToString() == "5" || ViewState["cmbFollowUpSelected"].ToString() == "14")
                        {
                            clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up call status changed from [----------] to [" + cmbFollowUp.SelectedItem.Text +"]" , "", GTicketID);
                        }
                        else
                        {
                            clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up call status changed from [" + ViewState["cmbFollowUpSelectedText"].ToString() + "] to [" + cmbFollowUp.SelectedItem.Text + "]" , "", GTicketID);
                        }
                    }
                    if (ViewState["optionSelected"].ToString() != optFollowUp.SelectedItem.Text)
                    {
                        clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up changed from ["+ ViewState["optionSelected"].ToString() + "] to [" + optFollowUp.SelectedItem.Text + "]", "", GTicketID);
                    }

					HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
			}			
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}

		}

        private string FormatPhoneNumber(string number)
        {
            string temp = String.Empty;
            if (number.Length == 10)
            {
                temp = "(" + number.Substring(0, 3) + ") " + number.Substring(3, 3) + "-" + number.Substring(6, 4);

            }
            else if (number.Length > 10)
            {
                temp = "(" + number.Substring(0, 3) + ") " + number.Substring(3, 3) + "-" + number.Substring(6, 4) + "x" + number.Substring(10, number.Length - 10);
            }
            else if (number == String.Empty)
            {
                temp = "N/A";
            }
            else
                temp = number;
            return temp;
        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {

        }
	
	}
}

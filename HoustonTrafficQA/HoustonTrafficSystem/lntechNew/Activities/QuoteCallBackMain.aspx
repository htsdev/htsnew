<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.QuoteCallBackMain"
    MaintainScrollPositionOnPostback="true" CodeBehind="QuoteCallBackMain.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Navigation.ascx" TagName="Navigation" TagPrefix="uc6" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Quote CallBack</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #TableDates
        {
            width: 776px;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="HideBox();ShowDateBoxes();">

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/cBoxes.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
		
		function ShowHidePageNavigation()
		{
//			var rec = document.getElementById("lblRecCount").innerText * 1;
//			var tbl =  document.getElementById("tblPageNavigation").style;
//					
//			if (rec == '0')
//				tbl.display = 'none';
//			else
//				tbl.display= 'block';
		
		}
		
		function SplitDate()
			{
		       var time=new Date();
               var date=time.getDate();
               var year=time.getYear();
               if (year < 2000)    // Y2K Fix, Isaac Powell
               year = year + 1900; // http://onyx.idbsu.edu/~ipowell
		       
		       var d1 = new dateObj(document.Form1.dMonth, document.Form1.dDay, document.Form1.dYear);			   
			   
			   if ((document.Form1.txtFrom.value != "") || (document.Form1.txtFrom.value != (eval(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex].value) + "/" +  eval(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value)))
			   {			   		   
					var strDOB = new String(document.Form1.txtFrom.value)
					var stringArray = strDOB.split("/"); 
					initDates(2002 ,  year, stringArray[2], stringArray[1], stringArray[0], d1);
			   }			   
			   var d2 = new dateObj(document.Form1.ToMonth,document.Form1.ToDay, document.Form1.ToYear);
			   
			   if ((document.Form1.txtTo.value != "") || (document.Form1.txtTo.value != (document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex].value + "/" +  document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value + "/" + document.Form1.ToYear.options[document.Form1.ToYear.selectedIndex].value)))
			   {
					var strAppt = new String(document.Form1.txtTo.value)
					var stringArray = strAppt .split("/"); 					
					initDates(2002, year, stringArray[2], stringArray[1], stringArray[0], d2);	           
			   }			   
			   
			} 
			
			function BindDate()
			{								
				if (document.Form1.dMonth.value!=1 && document.Form1.dDay.value!=0 && document.Form1.dYear.value!=0)				
				{
					if (Len(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value) == 1 )				
					{				
						strTemp= "0" + document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value;				
					}
					else
					{	strTemp = document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value;}
				
					if (Len(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) == 1 )				
					{				
						strTemp= strTemp + "/0" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;				
					}
					else
					{	strTemp = strTemp + "/" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;}								
				
					strTemp = strTemp + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value;
					document.Form1.txtFrom.value = strTemp;
				}
				else
				{
					document.Form1.txtFrom.value = '';
				}
												
				//*******************
				if (document.Form1.ToMonth.value!=1 && document.Form1.ToDay.value!=0 && document.Form1.ToYear.value!=0)
				{
					if (Len(document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex-1].value) == 1 )				
					{				
						strTemp= "0" + document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex-1].value;				
					}
					else
					{	strTemp = document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex-1].value;}
				
					if (Len(document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value) == 1 )				
					{				
						strTemp= strTemp + "/0" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value;				
					}
					else
					{	strTemp = strTemp + "/" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value;}								
				
					strTemp = strTemp + "/" + document.Form1.ToYear.options[document.Form1.ToYear.selectedIndex].value;
				
					document.Form1.txtTo.value = strTemp;				
				}				
				else
				{
					document.Form1.txtTo.value = '';
				}				
			}
		
		//Yasir Kamal 5744 04/06/2009 Quote Call back chages. 
			function PopUpCallBack(TicketID, QuoteID)
			{							
				document.getElementById("txtCallBackFlag").value=1;
				window.open ("QuoteCallBackDetail.aspx?TicketID="+ TicketID + "&QuoteID=" + QuoteID ,"QuoteCallBack", 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=yes,resizable=no,left=150,Top=100,height=520,width=685' );
				return false;
			
			}
			
			function ShowViolationFee(TicketID)
			{			
				var strURL= "//ClientInfo/violationsfees.asp?caseNumber=" & TicketID;
				window.navigate(strURL);				
			}
			
			function GoBack()
			{				
				window.navigate('../../Reports/BatchPrintSingle.asp');
			}
			
					function ShowDateBoxes()
			{			
			
				element = document.getElementById('TableDates').style; 				
				if (document.Form1.rlstQuoteType[0].checked == true)					 

				{
				
				var date='<%=ViewState["date"]%>';
				 document.Form1.CallDateCalender.value = date;
							
					element.display='block';		
					element.margin = '1.5px 0px';
										
						element1 = document.getElementById('tdRpt1').style											
						element1.display='block';		
						element1.margin = '1.5px 0px';
                        element1_1 = document.getElementById('td_days').style											
						element1_1.display='none';		
						element1_1.margin = '1.5px';
						element3 = document.getElementById('tdRpt3').style					
						element3.display='none';		
						element3.margin = '0px';					
						
						element1_1 = document.getElementById('td_days').style											
						element1_1.display='none';		
						element1_1.margin = '1.5px';
				}
					
				else if (document.Form1.rlstQuoteType[1].checked == true)
					{	
					var from='<%=ViewState["from"]%>';
					var to='<%=ViewState["to"]%>';
                    
                    document.Form1.calFrom1.value = from;
                    document.Form1.calTo1.value = to;
					
 			        element.display='block';		
					element.margin = '1.5px 0px';
						
						element1 = document.getElementById('tdRpt1').style											
						element1.display='none';		
						element1.margin = '1.5px';
						element1_1 = document.getElementById('td_days').style											
						element1_1.display='block';		
			       		element1_1.margin = '0px';
						element3 = document.getElementById('tdRpt3').style					
						element3.display='none';		
						element3.margin = '0px';
		
					}	
					
		        else
		        {
					element.display='none';		
					element.margin = '0px';	    	        
		        }		                  			
		            
             
			}
			
			
			function  SearchValidation()
			{
			    
				if (document.Form1.rlstQuoteType[0].checked == true )					 
				{				
					if (document.Form1.calFrom1.value =="")
					{
						alert("Please enter valid Date!");
						document.Form1.calFrom1.focus(); 
						return false;
					}
				
					if (isDate(document.Form1.calFrom1.value,'MM/dd/yyyy')==false)
					{
						alert("Please enter valid Date format (mm/dd/yyyy)!");
						document.Form1.calFrom1.focus(); 
						return false;
					}									
					if (document.Form1.calTo1.value =="")
					{
						alert("Please enter valid Date!");
						document.Form1.calTo1.focus(); 
						return false;
					}									
					
					if (isDate(document.Form1.calTo1.value,'MM/dd/yyyy')==false)
					{	
						alert("Please enter valid Date format (mm/dd/yyyy)!");
						document.Form1.calTo1.focus(); 
						return false;
					}
					
					if (IsDatesEqualOrGrater(document.Form1.calTo1.value,'MM/dd/yyyy',document.Form1.calFrom1.value,'MM/dd/yyyy')==false)
					{
						alert("Please enter valid date, To-Date must be grater then or equal to From-Date");
						document.Form1.calTo1.focus(); 
						return false;
					}										
				}			
				
				
			}
			
		function show( divid )
		{
			divid.style.display = 'block';
			divid.style.margin = '1.5px 0px';
		}

		function hide( divid )
		{
			divid.style.display = 'none';
			divid.style.margin = '0px';
		}
		
		function DefaultLoad()
		{			
			
		}
		
		function HideBox()
		{			
			element = document.getElementById('txtCallBackFlag').style;
		    element.display='none';	
		    element = document.getElementById('txtFrom').style;
		    element.display='none';	
		    element = document.getElementById('txtTo').style;
		    element.display='none';	
		    element = document.getElementById('tdHide').style;
		    element.display='none';			    		    
		}
		
		function RefreshGrid()
		{
			if (document.getElementById("txtCallBackFlag").value==1)
			{				
				document.getElementById("txtCallBackFlag").value=0;			
			}			
		}
		function ShowBoxes()
		{
		
		    var tdRpt1 =document.getElementById('tdRpt1').style;
		    var td_days =document.getElementById('td_days').style;
		    var tdRpt3 =document.getElementById('tdRpt3').style;
		    if (document.Form1.rlstQuoteType[1].checked == true)
			{
				td_days.display="block";
				tdRpt1.display="none";
				tdRpt3.display="none";					
			}	
		    
		    		    
		}
		
		// Fahad 6934 12/17/2009 Hide Quote Call Back control modal popup
        function HidePopup() {
            var modalPopupBehavior = $find('mpeQuoteCall');
            modalPopupBehavior.hide();
            //return false;
        }
		
				
    </script>

    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
        border="0">
        <tbody>
            <tr>
                <td colspan="4">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" colspan="4">
                                <table id="Table2" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="width: 258px; height: 23px" valign="top">
                                            <asp:RadioButtonList ID="rlstQuoteType" runat="server" Font-Names="Tahoma" Font-Size="6pt"
                                                CssClass="clssubhead" onclick="ShowDateBoxes();" Width="264px" DataTextField="QueryTypeDesc"
                                                DataValueField="QueryTypeID">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td align="left" valign="top">
                                            <table id="TableDates" border="0" cellpadding="0" cellspacing="0" style="display: block;
                                                width: 432px" width="432">
                                                <tr id="trRpt1">
                                                    <td id="tdRpt1" style="height: 23px">
                                                        <asp:Label ID="Label6" runat="server" CssClass="clsLabel">From</asp:Label>&nbsp;
                                                        <ew:CalendarPopup ID="calFrom1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Court Date" Width="80px">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>
                                                        &nbsp;
                                                        <asp:Label ID="Label7" runat="server" CssClass="clsLabel">To</asp:Label>&nbsp;
                                                        <ew:CalendarPopup ID="calTo1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Court Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 23px">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="tr_days">
                                                    <td id="td_days" style="height: 23">
                                                        <asp:Label ID="Label12" runat="server" CssClass="clsLabel">Date</asp:Label>&nbsp;
                                                        <ew:CalendarPopup ID="CallDateCalender" runat="server" AllowArbitraryText="False"
                                                            CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                                            ShowClearDate="True" ShowGoToToday="True" ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td id="tdRpt3" style="height: 23px">
                                                        <asp:Label ID="Label8" runat="server" CssClass="clsLabel">From</asp:Label>
                                                        &nbsp;<ew:CalendarPopup ID="calFrom3" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>
                                                        &nbsp;
                                                        <asp:Label ID="Label9" runat="server" CssClass="clsLabel">To</asp:Label>&nbsp;
                                                        <ew:CalendarPopup ID="calTo3" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="tdHide">
                                                        <asp:Label ID="Label2" runat="server" CssClass="clsLabel">From</asp:Label>
                                                        <select id="dMonth" class="clsInputCombo" name="dMonth" onblur="BindDate()">
                                                        </select>
                                                        <select id="dDay" class="clsInputCombo" name="dDay" onblur="BindDate()">
                                                        </select>
                                                        <select id="dYear" class="clsInputCombo" name="dYear" onblur="BindDate()">
                                                        </select>
                                                        &nbsp;
                                                        <asp:Label ID="Label5" runat="server" CssClass="clsLabel">To</asp:Label>&nbsp;<select
                                                            id="ToMonth" class="clsInputCombo" name="ToMonth" onblur="BindDate()"></select><select
                                                                id="ToDay" class="clsInputCombo" name="ToDay" onblur="BindDate()"></select><select
                                                                    id="ToYear" class="clsInputCombo" name="ToYear" onblur="BindDate()"></select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtFrom" runat="server" CssClass="clstextarea" Width="5px"></asp:TextBox>&nbsp;
                                                        <asp:TextBox ID="txtTo" runat="server" CssClass="clstextarea" Width="5px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="780px">
                                            <tr>
                                                <td class="clsLeftPaddingTable" colspan="4" style="height: 30px">
                                                    &nbsp;
                                                    <asp:Label ID="lbl_case" runat="server" CssClass="label" Text="Case Type"></asp:Label>&nbsp;
                                                    &nbsp;<asp:DropDownList ID="dd_casetype" runat="server" Width="98px" CssClass="clsInputCombo"
                                                        DataTextField="CaseTypeName" DataValueField="CaseTypeId">
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search"
                                                        OnClick="btnSearch_Click1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clssubhead" style="height: 34px" valign="middle" align="left" width="50%"
                                                    background="../Images/subhead_bg.gif" colspan="3">
                                                    &nbsp; Quote Client Details
                                                </td>
                                                <td class="clssubhead" style="height: 34px" valign="middle" align="right" width="50%"
                                                    background="../Images/subhead_bg.gif">
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
                                                        DisplayAfter="2">
                                                        <ProgressTemplate>
                                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                                CssClass="clsLabel"></asp:Label>
                                                        </ProgressTemplate>
                                                    </aspnew:UpdateProgress>
                                                    <asp:Label ID="lblMessage" runat="server" Font-Names="Arial" Font-Size="X-Small"
                                                        Width="538px" ForeColor="Red"></asp:Label>
                                                    <asp:GridView ID="gv_Quote" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnRowCommand="gv_Quote_RowCommand"
                                                        OnPageIndexChanging="gv_Quote_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                                        Text='<%# Eval("sno") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Customer" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <%--  <asp:HyperLink ID="lnkCustomer" runat="server" CssClass="GridItemStyle" NavigateUrl="#"
                                                    Text='<%# Eval("Customer") %>'>															
                                                </asp:HyperLink>--%>
                                                                    <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Customer") %>'
                                                                        CommandName="Click" CommandArgument='<%# Eval("TicketID_PK") %>'>									
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Contact" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatus" runat="server" CssClass="GridItemStyle" Text='<%# Eval("LastContactDate")+ "("+ Eval("Days") +")" %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Court Date" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" CssClass="GridItemStyle" Text='<%# Eval("CourtDate") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Crt.Loc" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_crt" runat="server" CssClass="GridItemStyle" Text='<%# Eval("crt") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Quote" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblQuote" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Calculatedtotalfee") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Follow Up Status" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFollowUpStatus" runat="server" CssClass="GridItemStyle" Text='<%# Eval("FollowUpStatus") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Call Back Date" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCallbackDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CallBackDate", "{0:MM/dd/yyyy}") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeQuoteCall" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlControl" TargetControlID="Button1">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="Button1" runat="server" Style="display: none" />
                                        <asp:Panel ID="pnlControl" runat="server" Style="display: none;">
                                            <uc6:Navigation ID="Navigation1" runat="server" ControlTitle="Quote Call Client Details" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 760px" align="center" colspan="4">
                                <asp:TextBox ID="txtCallBackFlag" runat="server" Width="23px" CssClass="clstextarea"
                                    AutoPostBack="True">0</asp:TextBox>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 760px" align="center" colspan="4">
                                <asp:Label ID="lblRecCount" runat="server" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    </form>

    <script language="javascript" type="text/javascript">
		
				var time=new Date();
				var date=time.getDate();
				var year=time.getYear();
				if (year < 2000)    // Y2K Fix, Isaac Powell
					year = year + 1900; // http://onyx.idbsu.edu/~ipowell
				//var d1 = new dateObj(document.Form1.dDay,document.Form1.dMonth, document.Form1.dYear);
				var d1 = new dateObj(document.Form1.dMonth,document.Form1.dDay, document.Form1.dYear);
				initDates(2002, year, '--','--','--', d1);
				
				var d2 = new dateObj(document.Form1.ToMonth,document.Form1.ToDay, document.Form1.ToYear);				
				initDates(2002, year, '--','--','--', d2);
				
				ShowHidePageNavigation();
				
				 document.getElementById("Navigation1_CallBackDate_div").style.zIndex = 100003;
        document.getElementById("Navigation1_CallBackDate_monthYear").style.zIndex = 100004;
		
    </script>

</body>
</html>

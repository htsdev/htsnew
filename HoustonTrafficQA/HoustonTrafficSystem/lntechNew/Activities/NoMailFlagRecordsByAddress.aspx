﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoMailFlagRecordsByAddress.aspx.cs" Inherits="HTP.Reports.NoMailFlagRecordsByAddress" %>
                    
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TrafficMenuMain.ascx" TagName="TrafficMenuMain"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>No mail flag records By Addresses</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
    <form id="form1" method="post" runat="server">
    <div>
        <table cellspacing="0" cellpadding="0" width="820 px" align="center" border="0">
            <tr>
                <td>
                    <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/subhead_bg.gif)" height="34" class="clssubhead">
                    <table width="100%">
                        <tr>
                            <td class="clssubhead" width="50%">
                                Search
                            </td>
                            <td class="clssubhead" width="50%" align="right">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="DoNotMail.aspx?sMenu=121">Back</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" height="11">
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/subhead_bg.gif)" height="34" class="clssubhead"
                    align="right">
                    <table id="tblPaging" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc4:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/separator_repeat.gif)" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td id="td_grid">
                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        Width="100%" PageSize="20" OnRowCommand="gv_Records_RowCommand" AllowPaging="True"
                        OnPageIndexChanging="gv_Records_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="S. No">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_sno" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Address" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_city" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_State" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zip">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Zip" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.Zip") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Insert Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_insertdate" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.insertdate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField>
                                <HeaderStyle CssClass="clssubhead" Width="3%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.Address") + ","+ DataBinder.Eval(Container, "DataItem.City") + ","+ DataBinder.Eval(Container, "DataItem.Zip") %>'
                                        ImageUrl="~/Images/cross.gif" CommandName="Remove" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for CallBackNotes.
	/// </summary>
	public partial class CallBackNotes : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.DropDownList ddl_callstatus;
		protected System.Web.UI.WebControls.Label lbl_name;
		protected System.Web.UI.WebControls.Label lbl_telephone;
		protected System.Web.UI.WebControls.Label lbl_appdate;
		protected System.Web.UI.WebControls.Button btn_submit;
		clsENationWebComponents clsdb = new clsENationWebComponents();
		clsSession cSession	 =new clsSession();
		clsLogger clog = new clsLogger();
		int ticketid;
		int empid;
		protected eWorld.UI.CalendarPopup CallBackDate;
		int callid;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				
				ticketid =Convert.ToInt32(Request.QueryString["casenumber"]);
				//	Session["employeeid"] = (int) Convert.ChangeType(Request.Cookies["employeeid"].Value,typeof(int));
				empid=Convert.ToInt32(cSession.GetCookie("sEmpID",this.Request));
				//	empid=3992;
				if(!IsPostBack)
				{
					GetInfo();
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		//GetInfo Based on Ticket  Num
		private void GetInfo()
		{
            //Change by Ajmal
			//System.Data.SqlClient.SqlDataReader sqldr=null;
            IDataReader sqldr = null;
			try
			{
				string [] key1 =  {"@ticketid"} ;
				object [] value1 =  {ticketid} ;

				sqldr = clsdb.Get_DR_BySPArr("USP_HTS_GET_CallBackNotes",key1,value1);
				sqldr.Read();
				lbl_name.Text = sqldr[0].ToString();
				lbl_telephone.Text = sqldr[1].ToString();
				lbl_appdate.Text = sqldr[2].ToString();				
				string calldate=sqldr[3].ToString();
				if (calldate!="")
				{
					CallBackDate.SelectedDate=Convert.ToDateTime(calldate);						
				}				
				//txt_calldate.Text=sqldr[3].ToString();					
				txtComments.Text = sqldr[4].ToString();
				int callid=Convert.ToInt32(sqldr[5]);
				ddl_callstatus.SelectedValue=callid.ToString();
				
			}
			catch(Exception ex)
			{
				string a = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			finally
			{
				sqldr.Close();
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		//In order to save the information
		private void btn_submit_Click(object sender, System.EventArgs e)
		{		
			try
			{
				string comments = txtComments.Text.Trim() ;
				callid=Convert.ToInt32(ddl_callstatus.SelectedValue);
				DateTime dt=Convert.ToDateTime("1/1/0001"); 

				string date=CallBackDate.SelectedDate.ToString();


				if (CallBackDate.SelectedDate== dt.Date)
				{
					CallBackDate.SelectedDate=Convert.ToDateTime("01/01/1900");
				}
				string [] key1 =  {"@ticketid","@empid","@comments","@CallBackDate","@CallStatus"} ;
				object [] value1 =  {ticketid,empid,comments,CallBackDate.SelectedDate,callid} ;
				clsdb.ExecuteSP("USP_HTS_UPDATE_CallBackNotes",key1,value1);		
				//In order to close the popup
				HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");			
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		
		}
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;



namespace lntechNew
{
	/// <summary>
	/// Summary description for Cleanup.
	/// </summary>
	public partial class Cleanup : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
		
				//File.Delete(Request.ServerVariables["SERVER_NAME"].ToString()+"/lntechNew/Reports/temp/"+Session["PDFImage"].ToString());
				File.Delete(Server.MapPath("").ToString()+"/reports/temp/"+Session["PDFImage"].ToString());
			}
			catch
			{}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaymentInfo.FinancialRepsPreview" Codebehind="FinancialRepsPreview.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Financial Reps. Report Preview</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script type="text/javascript">		
		
		function Back()
		{
			window.history.back(1);
		
		}		
		
		function ShowViolationFee(TicketID)
		{
			//a;
				//strURL = "../../ClientInfo/violationsfees.asp?caseNumber=" & intTicketID
			var strURL= "//ClientInfo/violationsfees.asp?caseNumber=" & TicketID;
			window.navigate(strURL);				
		}
			
			
		function show( divid )
		{
			divid.style.display = 'block';
			divid.style.margin = '1.5px 0px';
		}

		function hide( divid )
		{
			divid.style.display = 'none';
			divid.style.margin = '0px';
		}
		
		function DefaultLoad()
		{		
			//document.getElementById("txtPC").style.display='none'
			//document.getElementById("txtPR").style.display='none'
			//a;
			//if (document.getElementById("txtPC").value == 1)			
			//{
			//	show(HA2);show(HA3);hide(HA1);			
			//}
			//else
			//{
			//	hide(HA2);hide(HA3);show(HA1);		
			//}
			
			//if (document.getElementById("txtPR").value == 1)
			//{
			//	show(HB2);show(HB3);hide(HB1);			
			//}
			//else
			//{
			//	hide(HB2);hide(HB3);show(HB1);		
			//}
			
			//show(HA2);show(HA3);hide(HA1);			
			//hide(HB2);hide(HB3);show(HB1);		
			//show(HB2);show(HB3);hide(HB1);	
			//a;						
		}
		
		function SetFlag(Control,Value)
		{
			document.getElementById(Control).value = Value ;		
		}
		
		
		function ValidateMe(txtbox)
		{
			//a;
			//var AmountValue= document.Form1.TextBox3.value;
			var AmountValue= txtbox.value; 
			if (AmountValue !="")
			{
				if (!(isFloat(AmountValue) || isPositiveInteger(AmountValue)))
				{
					alert("Please Enter actual amount in textboxes");				
					return false;
				}					
			}
			return true;		
		}
		
		
		function PopUpShowPreviewPDF()
		{
				//a;
				//strURL = "../../ClientInfo/violationsfees.asp?caseNumber=" & intTicketID
				//var strURL= "PreviewPDF.aspx?DocID=" & DocID;
				//window.open(strURL);				
				
				//window.open ("QuoteCallBackDetail.aspx?TicketID="+ TicketID + "&QuoteID=" + QuoteID ,"QuoteCallBack", 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=no,resizable=no,left=150,Top=100,height=430,width=500' );				
				//window.open ('PreviewPDF.aspx?DocID='+ DocID,'toolbar=yes,location=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,left=150,Top=100,height=430,width=500' );
				//window.open ("PreviewMain.aspx?DocID="+ DocID);
				window.open ("PreviewMain.aspx");
		}
		
		</script>
		<style type="text/css">TABLE { FONT: 11px verdana,arial,helvetica,sans-serif }
	DIV.divHidden { DISPLAY: none; MARGIN: 0px }
	DIV { MARGIN: 0px }
	</style>
</HEAD>
	<body onload="DefaultLoad();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE width="780" align="left" border="0" cellPadding="0" cellSpacing="0" id="TableMain">
				<TR>
					<TD colSpan="4"><IMG alt="" src="../Images/lnlogo.jpg">&nbsp;
						<table id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="TopHeading" style="HEIGHT: 13px" align="center" colSpan="4" height="13">PAYMENT 
									DETAIL REPORT</td>
							</tr>
							<TR>
								<TD class="TopHeading" style="HEIGHT: 13px" align="center" colSpan="4" height="13">
									For The Period&nbsp;
									<asp:label id="lblDate" runat="server" CssClass="TopHeading"></asp:label>&nbsp;To
									<asp:label id="lblDateTo" runat="server" CssClass="TopHeading"></asp:label></TD>
							</TR>
							<TR>
								<TD class="frmtd" style="HEIGHT: 5px" width="20" height="5"></TD>
								<TD class="frmtd" style="HEIGHT: 5px" width="100" height="5"></TD>
								<TD style="DISPLAY: none;  HEIGHT: 5px" height="5"><ew:calendarpopup id="calQueryDate" runat="server" AutoPostBack="True" Font-Size="8pt" Font-Names="Tahoma"
										ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
										PadSingleDigits="True" ToolTip="Select Report Date" Width="90px" ImageUrl="../images/calendar.gif">
										<textboxlabelstyle cssclass="clstextarea"></textboxlabelstyle>
										<weekdaystyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="White"></weekdaystyle>
										<monthheaderstyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="Yellow"></monthheaderstyle>
										<offmonthstyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Gray"
											backcolor="AntiqueWhite"></offmonthstyle>
										<gototodaystyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="White"></gototodaystyle>
										<todaydaystyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="LightGoldenrodYellow"></todaydaystyle>
										<dayheaderstyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="Orange"></dayheaderstyle>
										<weekendstyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="LightGray"></weekendstyle>
										<selecteddatestyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="Yellow"></selecteddatestyle>
										<cleardatestyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="White"></cleardatestyle>
										<holidaystyle font-size="XX-Small" font-names="Verdana,Helvetica,Tahoma,Arial" forecolor="Black"
											backcolor="White"></holidaystyle>
									</ew:calendarpopup>&nbsp;
									<ew:calendarpopup id="calTo" runat="server" ImageUrl="../images/calendar.gif" Width="90px" ToolTip="Select Report Date"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
										ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Names="Tahoma"
										Font-Size="8pt" AutoPostBack="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
								<TD style="HEIGHT: 5px" vAlign="bottom" align="right" height="5">&nbsp;&nbsp;
									<asp:HyperLink id="lnkBack" onClick="Back();" runat="server" CssClass="NormalLink" NavigateUrl="#">Back</asp:HyperLink></TD>
							</TR>
							<TR>
								<TD class="frmtd" align="center" colSpan="4">
									<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></TD>
							</TR>
						</table>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">&nbsp;
						<asp:textbox id="Textbox7" runat="server" CssClass="ProductHead" ReadOnly="True">Category Type Summary</asp:textbox>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<tr>
					<td colSpan="5" width="100%">
						<asp:datagrid id="dgCategoryType" runat="server" Font-Size="2px" Font-Names="Verdana" Width="100%"
							AutoGenerateColumns="False" ShowFooter="True">
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<FooterStyle CssClass="GrdFooter"></FooterStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Category Type">
									<HeaderStyle HorizontalAlign="Left" Width="64%" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" CssClass="ProductGrdLinks" Width="20px" Visible="False"></asp:Label>
										<asp:Label id=lblCategoryType runat="server" CssClass="ProductGrdLinks" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryType") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:Label id="Label5" runat="server" CssClass="grdheader">Total</asp:Label>
									</FooterTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Count">
									<HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblCount2 runat="server" CssClass="GrdLbl" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'>
										</asp:Label>
										<asp:Label id=lblDisplayCol runat="server" CssClass="GrdLbl" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.displaycol") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Center"></FooterStyle>
									<FooterTemplate>
										<asp:Label id="lbl_SumCount" runat="server" CssClass="GrdLbl" Font-Bold="True"></asp:Label>
									</FooterTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Amount">
									<HeaderStyle HorizontalAlign="Center" Width="21%" CssClass="GrdHeader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lblAmount2 runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalAmount", "{0:C}") %>'>
										</asp:Label>&nbsp;&nbsp;&nbsp;
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
									<FooterTemplate>
										<asp:Label id="lbl_SumAmount" runat="server" CssClass="GrdLbl" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;
									</FooterTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				<!-- credit summary section starts here.... -->
				<TR>
					<TD colSpan="5" height="18"></TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">&nbsp;
						<asp:textbox id="Textbox3" runat="server" CssClass="ProductHead" ReadOnly="True">Credit Summary</asp:textbox>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<tr>
					<td width="100%" colSpan="5">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><asp:datagrid id="dgCredit" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px" AutoGenerateColumns="False"
										ShowFooter="True" Visible="true">
<AlternatingItemStyle BackColor="#EEEEEE">
</AlternatingItemStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
</HeaderStyle>

<FooterStyle CssClass="GrdFooter">
</FooterStyle>

<Columns>
<asp:TemplateColumn HeaderText="Credit Type">
<HeaderStyle HorizontalAlign="Left" Width="65%" CssClass="GrdHeader">
</HeaderStyle>

<ItemTemplate>
<asp:Label id=Label6 runat="server" CssClass="ProductGrdLinks" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryType") %>'>
													</asp:Label>
</ItemTemplate>

<FooterTemplate>
<asp:Label id=Label7 runat="server" CssClass="grdheader">Total</asp:Label>
</FooterTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Count">
<HeaderStyle HorizontalAlign="Center" Width="15%" CssClass="GrdHeader">
</HeaderStyle>

<ItemStyle HorizontalAlign="Center">
</ItemStyle>

<ItemTemplate>
<asp:Label id=Label8 runat="server" CssClass="GrdLbl" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'>
													</asp:Label>
</ItemTemplate>

<FooterStyle HorizontalAlign="Center">
</FooterStyle>

<FooterTemplate>
<asp:Label id=Label9 runat="server" CssClass="GrdLbl"></asp:Label>
</FooterTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Amount">
<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="GrdHeader">
</HeaderStyle>

<ItemStyle HorizontalAlign="Right">
</ItemStyle>

<ItemTemplate>
<asp:Label id=Label10 runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalAmount", "{0:C}") %>'>
													</asp:Label>&nbsp;&nbsp;&nbsp; 
</ItemTemplate>

<FooterStyle HorizontalAlign="Right">
</FooterStyle>

<FooterTemplate>
<asp:Label id=Label11 runat="server" CssClass="GrdLbl"></asp:Label>&nbsp;&nbsp;&nbsp; 
</FooterTemplate>
</asp:TemplateColumn>
</Columns>

<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
</PagerStyle>
									</asp:datagrid></td>
							</tr>
							<TR>
								<TD style="HEIGHT: 20px" colSpan="5"></TD>
							</TR>
						</table>
					</td>
				</tr>
				<TR>
					<TD colSpan="5" height="2"></TD>
				</TR>
				<!-- credit summary section ends here....  -->
				<TR>
					<TD colSpan="5" height="18"></TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HA1">&nbsp;&nbsp;<asp:textbox id="lblHeading1" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Type Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<TR>
					<TD width="100%" colSpan="5">
						<div id="HA3">
							<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0" width="100%">
								<TR>
									<TD vAlign="top" align="left" style="width: 929px">
										<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD colSpan="5"><asp:datagrid id="dgrdPayType" runat="server" Font-Size="2px" Font-Names="Verdana" Width="100%"
														OnItemCommand="DoGetQueryString" AutoGenerateColumns="False" ShowFooter="True">
														<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
														<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
														<FooterStyle CssClass="GrdFooter"></FooterStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="Payment Method">
																<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=lblPayTypeID runat="server" CssClass="GrdLbl" Visible="False" Text='<%# DataBinder.Eval(Container, &#13;&#13;&#10;"DataItem.PaymentType_PK") %>'>
																	</asp:Label>
																	<asp:Image id="imgParent" ImageUrl="../Images/folderopen.gif" Visible="false" Runat="server"></asp:Image>
																	<asp:Image id="imgLink" ImageUrl="../Images/T.gif" Visible="false" Runat="server"></asp:Image>
																	<asp:Image id="ImageChild" ImageUrl="../Images/folder.gif" Visible="false" Runat="server"></asp:Image>
																	<asp:Label id=lblDocumentID runat="server" CssClass="ProductGrdLinks" Visible="true" Text='<%# DataBinder.Eval(Container, &#13;&#13;&#10;"DataItem.Description") %>'>
																	</asp:Label>
																	<asp:Label id=lblPayType runat="server" CssClass="GrdLbl" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Count">
																<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lblCount runat="server" CssClass="GrdLbl" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") &#13;&#13;&#10;%>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Amount">
																<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblAmount" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
																	</asp:Label>&nbsp;&nbsp;&nbsp;
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD colSpan="5" height="20">&nbsp;</TD>
											</TR>
											<TR>
												<TD colSpan="5" class="TDHeading">&nbsp;&nbsp;
													<asp:textbox id="Textbox2" runat="server" CssClass="ProductHead" Width="168px" ReadOnly="True">Payment Type Summary</asp:textbox></TD>
											</TR>
											<TR>
												<TD colSpan="5" height="7"></TD>
											</TR>
											<TR>
												<TD class="frmtd" style="HEIGHT: 9px" vAlign="top" colSpan="5">
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD class="frmtd" style="BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid" vAlign="top"
																align="left" width="18%" bgColor="#eeeeee" colSpan="2" height="18">Rep</TD>
															<TD class="frmtd" style="BORDER-TOP: 1px solid" align="center" width="25%" bgColor="#eeeeee"
																height="18">Cash</TD>
															<TD class="frmtd" style="BORDER-TOP: 1px solid" align="center" width="25%" bgColor="#eeeeee"
																height="18">Check</TD>
															<TD class="frmtd" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: 1px solid" align="center"
																width="25%" bgColor="#eeeeee" height="18">Total</TD>
														</TR>
														<TR>
															<TD width="100%" colSpan="6"><asp:datagrid id="dgrdPayByRep" runat="server" Font-Size="2px" Font-Names="Verdana" Width="100%"
																	OnItemCommand="DoGetQueryString" AutoGenerateColumns="False">
																	<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
																	<FooterStyle CssClass="GrdFooter"></FooterStyle>
																	<Columns>
																		<asp:TemplateColumn>
																			<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="GrdHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label id="lblEmployeeID" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepID") %>'>
																				</asp:Label>
																				<asp:Label id="lblEmployee" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepName") %>'>
																				</asp:Label>
																				<asp:Label id="lblRepID" CssClass="ProductGrdLinks" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RepName") %>'>
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="System">
																			<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label id="lblSystemCash" CssClass="GrdLbl" runat="server" Visible="True" Text=''></asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Actual">
																			<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label id="lblActualCash" CssClass="GrdLbl" runat="server" Visible="false" Text=''></asp:Label>
																				<asp:TextBox ID="txtActualCash" CssClass="TextBox" BorderStyle="None" Runat="server" Visible="True" 
 Width="50" MaxLength="6" ReadOnly="True"></asp:TextBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="System">
																			<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label id="lblSystemCheck" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Actual">
																			<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label id="lblActualCheck" CssClass="GrdLbl" runat="server" Visible="false" Text=''></asp:Label>
																				<asp:TextBox ID="txtActualCheck" CssClass="TextBox" BorderStyle="None" Runat="server" Visible="True" 
 Width="50" ReadOnly="True" MaxLength="6"></asp:TextBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="System">
																			<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label id="lblSystemTotal" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Actual">
																			<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label id="lblActualTotal" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																	<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
																</asp:datagrid></TD>
														</TR>
														<TR>
															<TD class="frmtd" style="BORDER-RIGHT: gray 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: black 1px solid"
																colSpan="5">
																<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD class="frmtd" width="22%" height="25">Total</TD>
																		<TD width="14%" height="25">
																			<asp:label id="lblTotalCashSys" runat="server" CssClass="Label"></asp:label></TD>
																		<TD width="13%" height="25">
																			<asp:label id="lblTotalCashActual" runat="server" CssClass="Label"></asp:label></TD>
																		<TD width="15%" height="25">
																			<asp:label id="lblTotalCheckSys" runat="server" CssClass="Label"></asp:label></TD>
																		<TD width="12%" height="25">
																			<asp:label id="lblTotalCheckActual" runat="server" CssClass="Label"></asp:label></TD>
																		<TD width="13%">
																			<asp:label id="lblTotalSystem" runat="server" CssClass="Label"></asp:label></TD>
																		<TD>
																			<asp:label id="lblTotalActual" runat="server" CssClass="Label" DESIGNTIMEDRAGDROP="4631"></asp:label></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD class="frmtd" colSpan="5">&nbsp;</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD class="frmtd" style="HEIGHT: 9px" vAlign="top" colSpan="5"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 2px" vAlign="top" colSpan="5"><asp:textbox id="txtRemarks" runat="server" ToolTip="Notes" Width="100%" CssClass="TextArea"
														Height="88px" TextMode="MultiLine" ReadOnly="True" Visible="False"></asp:textbox></TD>
											</TR>
											<tr>
												<td colspan="5" height="20"></td>
											</tr>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HC1">&nbsp; &nbsp;<asp:textbox id="Textbox5" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:textbox></div>
					</TD>
				</TR>
				<TR>
					<TD colSpan="5" height="7"></TD>
				</TR>
				<TR>
					<TD width="100%" colSpan="5">
						<div id="HC3">
							<TABLE id="Table61" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD vAlign="top" align="left">
										<TABLE id="Table21" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD colSpan="5" height="20"><asp:datagrid id="dgrdCourt" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
														ShowFooter="True" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString">
														<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
														<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
														<FooterStyle CssClass="GrdFooter"></FooterStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="Court Location">
																<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id="lblCourtID" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
																	</asp:Label>
																	<asp:Label id="lblCourtName" CssClass="ProductGrdLinks" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Transactions">
																<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblTrans" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Trans") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Fee Amount">
																<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblFee" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
																	</asp:Label>&nbsp;&nbsp;&nbsp;
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 20px" colSpan="5"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD class="TDHeading" colSpan="5">
						<div id="HB1">&nbsp;&nbsp;&nbsp;<asp:textbox id="Textbox1" runat="server" CssClass="ProductHead" ReadOnly="True">Transaction Details</asp:textbox></div>
					</TD>
				</TR>
				<tr>
					<TD colSpan="5" height="7"></TD>
				</tr>
				<TR>
					<TD align="center" colSpan="5" height="10" style="DISPLAY: none"><asp:dropdownlist id="cmbRep" runat="server" AutoPostBack="True" Width="150px" CssClass="frmtd"></asp:dropdownlist>&nbsp;
						<asp:dropdownlist id="cmbPayType" runat="server" AutoPostBack="True" Width="250px" CssClass="frmtd"></asp:dropdownlist>&nbsp;
						<asp:dropdownlist id="cmbCourt" runat="server" CssClass="frmtd" Width="150px" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD colSpan="5" height="3">
						<div id="HB3">
							<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid"
										align="center" bgColor="#eeeeee" colSpan="5" height="25">
										<asp:Label id="lblRep" runat="server" CssClass="frmtd"></asp:Label>&nbsp;&nbsp;&nbsp;
										<asp:Label id="Label1" runat="server" CssClass="frmtd"></asp:Label>&nbsp;&nbsp;
										<asp:Label id="lblCourt" runat="server" CssClass="frmtd"></asp:Label>
									</TD>
								</TR>
								<TR>
									<TD><asp:datagrid id="dgrdPayDetail" runat="server" Font-Size="2px" Font-Names="Verdana" Width="100%"
											OnItemCommand="DoGetQueryString" AutoGenerateColumns="False" PageSize="100" AllowCustomPaging="True">
											<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No.">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblTicketID" CssClass="GrdLbl" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'>
														</asp:Label>
														<asp:Label id="lblCardTypeID" CssClass="GrdLbl" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>'>
														</asp:Label>
														<asp:Label id="lblNo" CssClass="GrdLbl" runat="server" Visible="True" Text=''></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Date">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label2" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Time">
													<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblTime" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Client Name">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="lblCustomer" CssClass="ProductGrdLinks" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<HeaderTemplate>
														Rep
													</HeaderTemplate>
													<ItemTemplate>
														<asp:Label id="Label3" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Bond">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="lblBond" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Paid">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="lblPaidAmount" CssClass="GrdLbl" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount","{0:C0}") %>'></asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Adj1">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                    
                                                </asp:TemplateColumn>
                                                
                                                <asp:TemplateColumn HeaderText="Adj2">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                   
                                                </asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<HeaderTemplate>
														Payment Type
													</HeaderTemplate>
													<ItemTemplate>
														<asp:Label id=lblPayTypeFR runat="server" CssClass="GrdLbl" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'>
														</asp:Label>
														<asp:Label id=lblPTypeId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Card">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="lblCardType" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Court">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label4" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="List">
													<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="lblListDate" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.ListDate") %>'>
														</asp:Label>
													</ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Source">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"  />
                                                    <ItemTemplate>
                                                        <asp:Label id="lbl_MailType" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>' runat="server" CssClass="GrdLbl" Visible="true">
														</asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Date">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"  />
                                                    <ItemTemplate>
                                                        <asp:Label id="lbl_MailDate" Text='<%# DataBinder.Eval(Container, "DataItem.MailDate") %>' runat="server" CssClass="GrdLbl" Visible="true">
														</asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colspan="5" height="20"></td>
								</tr>
							</TABLE>
						</div>
					</TD>
				</TR>
				<TR>
					<TD  class="TDHeading" colSpan="5"> &nbsp; &nbsp;&nbsp;
                        <asp:TextBox ID="Textbox4" runat="server" CssClass="ProductHead" ReadOnly="True">Void Transactions</asp:TextBox></TD>
				</TR>
				<TR>
					<TD colSpan="5" style="height: 16px">
                        <asp:DataGrid ID="DG_voidTransactions" runat="server" AllowCustomPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="2px" OnItemCommand="DoGetQueryString"
                            PageSize="100" Width="100%">
                            <FooterStyle CssClass="GrdFooter" />
                            <PagerStyle HorizontalAlign="Center" NextPageText="Next" PrevPageText="Previous" />
                            <AlternatingItemStyle BackColor="#EEEEEE" />
                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="No">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
												
                                      <ItemTemplate>
                                        <asp:Label ID="lblTicketID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                            Visible="false">
														</asp:Label>
                                        <asp:Label ID="lblCardTypeID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>'
                                            Visible="false">
														</asp:Label>
                                        <asp:Label ID="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Date">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                     <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Time">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime","{0:t}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Client Name">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomer" runat="server" CssClass="ProductGrdLinks" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                            >
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rep">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                   <ItemTemplate>
                                        <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj1">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj2">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Payment Type">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                            Visible="true">
														</asp:Label><asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                            Visible="False">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Card">
                                   <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lblCardType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Court">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									
                                    <ItemTemplate>
                                        <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid></TD>
				</TR>
				<tr>
									<td colspan="5" height="20"></td>
								</tr>
					<TR>
					<TD  class="TDHeading" colSpan="5"> &nbsp; &nbsp;&nbsp;
                        <asp:TextBox ID="Textbox8" runat="server" CssClass="ProductHead" ReadOnly="True">Attorney Credit Payments</asp:TextBox></TD>
				</TR>
				<TR>
					<TD colSpan="5" align="left" valign="top">
					
                        <asp:datagrid id="DG_AttorneyCredit" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False"  AllowCustomPaging="True" PageSize="100"
											AllowSorting="True" OnItemCommand="DoGetQueryString" >
                            <FooterStyle CssClass="GrdFooter" />
                            <PagerStyle HorizontalAlign="Center" NextPageText="Next" PrevPageText="Previous" />
                            <AlternatingItemStyle BackColor="#EEEEEE" />
                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="No">
                               <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													
                                
                                 <ItemTemplate>
                                <asp:Label id=lblTicketID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' Visible="false">
														</asp:Label>
														<asp:Label id=lblCardTypeID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>' Visible="false">
														</asp:Label>
														<asp:Label id="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
														
														</ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Date">
                                <HeaderStyle  HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Time">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime","{0:t}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Client Name" SortExpression="CustomerName">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                            Visible="False"></asp:Label><asp:LinkButton ID="lnkCustomer" runat="server" CommandName="DoGetCustomer"
                                            CssClass="ProductGrdLinks" ForeColor="black">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
														</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rep" SortExpression="Lastname">
                               <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" SortExpression="ChargeAmount">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj1">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj2">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Payment Type" SortExpression="PaymentType">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                            Visible="true">
														</asp:Label><asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                              
                                <asp:TemplateColumn HeaderText="Court">
                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        
                        </TD>
				</TR>
				<TR>
					<TD  class="TDHeading" colSpan="5"> &nbsp; &nbsp;&nbsp;
                        <asp:TextBox ID="Textbox9" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Waiver</asp:TextBox></TD>
				</TR>
				
					<TR>
					<TD colSpan="5" align="left" valign="top">
					
                        <asp:datagrid id="DG_AttorneyCredit2" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
											AutoGenerateColumns="False"  AllowCustomPaging="True" PageSize="100"
											AllowSorting="True" OnItemCommand="DoGetQueryString" >
                            <FooterStyle CssClass="GrdFooter" />
                            <PagerStyle HorizontalAlign="Center" NextPageText="Next" PrevPageText="Previous" />
                            <AlternatingItemStyle BackColor="#EEEEEE" />
                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="No">
                               <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
													
                                
                                 <ItemTemplate>
                                <asp:Label id=lblTicketID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' Visible="false">
														</asp:Label>
														<asp:Label id=lblCardTypeID runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>' Visible="false">
														</asp:Label>
														<asp:Label id="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
														
														</ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Date">
                                <HeaderStyle  HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Time">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime","{0:t}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Client Name" SortExpression="CustomerName">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                            Visible="False"></asp:Label><asp:LinkButton ID="lnkCustomer" runat="server" CommandName="DoGetCustomer"
                                            CssClass="ProductGrdLinks" ForeColor="black">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
														</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rep" SortExpression="Lastname">
                               <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" SortExpression="ChargeAmount">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount", "{0:C0}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj1">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj2">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lb_adj2" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Payment Type" SortExpression="PaymentType">
                                <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
													
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                            Visible="true">
														</asp:Label><asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                              
                                <asp:TemplateColumn HeaderText="Court">
                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                
                                    <ItemTemplate>
                                        <asp:Label ID="lblCourt" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'
                                            Visible="true">
														</asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    
                        </TD>
				</TR>
				
				<tr>
				<td>
				<asp:textbox id="txtCSS" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtCT" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtPC" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtRS" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtCS" runat="server" Width="0px" Height="32px">1</asp:textbox><asp:textbox id="txtPR" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtCC" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="iCount" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="Textbox6" runat="server" Width="0px" Height="18px"></asp:textbox><asp:textbox id="txtCheck" runat="server" Width="0px" Height="18px">0</asp:textbox><asp:textbox id="txtVP" runat="server" Width="0px" Height="18px">1</asp:textbox><asp:textbox id="txtAtt" runat="server" Width="0px" Height="18px">1</asp:textbox>
				</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

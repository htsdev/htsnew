using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components; 

namespace lntechNew.PaperLess
{
	
	/// <summary>
	/// Summary description for PreviewScanDocument.
	/// </summary>
	public partial class PreviewScanDocument : System.Web.UI.Page
	{
		public string strR;
		ScnImage CurrentImage;
		protected System.Web.UI.WebControls.ImageButton imgPreviouse;
		protected System.Web.UI.WebControls.ImageButton imgNext;
		protected System.Web.UI.WebControls.ImageButton ImageButton1;
		protected System.Web.UI.WebControls.ImageButton ImageButton2;
		protected System.Web.UI.WebControls.ImageButton imgOK;
		protected System.Web.UI.WebControls.TextBox txtData;
		ArrayList aList;
		private void Page_Load(object sender, System.EventArgs e)
		{
			
				//string to load image into Image control
				strR = "<SCRIPT language='javascript'>document.frm1.izo.src='" + Request["img"].ToString().Trim()    + "'</script>";
			
				txtData.Visible = false;
			
				//find out the properties of image loaded
				string fName = Request["img"].ToString().Trim();
				fName = fName.Substring(fName.LastIndexOf("/")+1); 
			       
				aList = (ArrayList) Session["ILIST"];
					
					 
				IEnumerator en =  aList.GetEnumerator(); 

				while(en.MoveNext())
				{
					ScnImage i = (ScnImage)	en.Current;
					if(i.ClientName == fName)
					{
						//TextBox1.Text = i.ServerName + "  " + i.include.ToString() + "  " + i.count.ToString()  ;
						CurrentImage = i;
					
						// i.include = false; 
												
					}
				}
										 
			 

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgPreviouse.Click += new System.Web.UI.ImageClickEventHandler(this.imgPreviouse_Click);
			this.imgNext.Click += new System.Web.UI.ImageClickEventHandler(this.imgNext_Click);
			this.ImageButton1.Click += new System.Web.UI.ImageClickEventHandler(this.ImageButton1_Click);
			this.ImageButton2.Click += new System.Web.UI.ImageClickEventHandler(this.ImageButton2_Click);
			this.imgOK.Click += new System.Web.UI.ImageClickEventHandler(this.imgOK_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnRemove_Click(object sender, System.EventArgs e)
		{  
			
		}

		private void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			
			string Nimg = nextImage();
 			string Pimg = PreImage(); 
			
		//Remove Image from Book and back to 
			int Dcount;
		//		ArrayList aList;
		//		aList = (ArrayList) Session["ILIST"];
		//	   aList.Remove(CurrentImage);
			//CurrentImage.include = false;
			aList.Remove(CurrentImage);
			Session["ILIST"] =aList;
			Dcount= (int) Convert.ChangeType(Session["DOCCOUNT"].ToString(),typeof(int));
			Dcount--;
			Session["DOCCOUNT"] = Dcount;

			//check if any other document exist in this book 
			if(Nimg != "")//if next image exists then redirect to that
				 Response.Redirect("PreviewScanDocument.aspx?img=c:/" + Nimg,true);
			else if(Pimg != "")//if previouse image exists then redirect to that
				Response.Redirect("PreviewScanDocument.aspx?img=c:/" + Pimg,true);
			else //finally go back
			  Response.Redirect("DoScanDocument.aspx?BID=" + Session["BookID"].ToString().Trim() + "&Dcount=" + Session["DOCCOUNT"].ToString() + "&PicName=N"  );
			
		}

		private void imgNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			string nImg=nextImage();
			if (nImg != ""){
               Response.Redirect("PreviewScanDocument.aspx?img=c:/" + nImg,true);
			}
			
		}

		private string nextImage()
		{
			
			IEnumerator en =  aList.GetEnumerator(); 
			string strRed="";//string using for return next image name
			while(en.MoveNext())
			{
				ScnImage img = (ScnImage)	en.Current;
				if(img.ClientName ==  CurrentImage.ClientName)
				{
					
					if(en.MoveNext())//has next Image
					{
						img = (ScnImage)	en.Current;
						strRed  =  img.ClientName; 		
						break;
					}
					else
					{
						strRed="";
					}
			 												
			    }
			}
	
			return strRed; 
		}

		private string PreImage()
		{
			IEnumerator en =  aList.GetEnumerator(); 
			string pF="";//previous Image
			while(en.MoveNext())
			{
				ScnImage i = (ScnImage)	en.Current;
				if(i.ClientName ==  CurrentImage.ClientName)
				{
					break;	//Response.Redirect("PreviewScanDocument.aspx?img=c:/" + pF  ); 		
				}
				pF = i.ClientName; 
			}
			return pF;
		}

		private void imgPreviouse_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			string nImg=PreImage();
			if (nImg != "")
			{
				Response.Redirect("PreviewScanDocument.aspx?img=c:/" + nImg,true);
			}
		}

		private void imgOK_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{//sending back
		Response.Redirect("DoScanDocument.aspx?BID=" + Session["BookID"].ToString().Trim() + "&Dcount=" + Session["DOCCOUNT"].ToString() + "&PicName=N" ,true );
		}

		private void btnOCR_Click(object sender, System.EventArgs e)
		{ 
						
		}

		private void ImageButton2_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			
			clsGeneralMethods CGeneral = new clsGeneralMethods();
			txtData.Visible = true;
			txtData.Text = CGeneral.OcrIt(Server.MapPath("../temp/") + CurrentImage.ServerName ); 
	

		}

	}
}

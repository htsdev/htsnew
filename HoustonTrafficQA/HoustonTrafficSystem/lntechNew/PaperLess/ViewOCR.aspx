<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaperLess.ViewOCR" Codebehind="ViewOCR.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ViewOCR</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table style="WIDTH: 640px">
				<tr>
					<td width="40%">Document Type:<asp:Label id="lblDocType" runat="server" CssClass="label"></asp:Label></td>
					<td width="60%">Description:<asp:Label id="lblDesc" runat="server" CssClass="label"></asp:Label></td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:TextBox id="txtOCRData" runat="server" TextMode="MultiLine" Width="640px" Height="416px"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right">
						<asp:Button id="btn_Update" runat="server" Text="Update" CssClass="clsbutton"></asp:Button>&nbsp;
						<asp:Button id="btnCancel" runat="server" CssClass="clsbutton" Text="Cancel"></asp:Button>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="HTP.PaperLess.Documents" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register Assembly="Microsoft.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/WebControls/faxcontrol.ascx" TagName="Faxcontrol" TagPrefix="Fc" %>
<%@ Register Src="../WebControls/ReadNotes.ascx" TagName="ReadNotes" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Documents</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript">
    //ozair 3643 on 05/12/2008 method to show verified scanned document from document tracking system
    function PopUpShowPreviewDT(DBID,SBID)
	{
		window.open ("../documenttracking/PreviewMain.aspx?DBID="+ DBID +"&SBID="+ SBID);
		return false;
	}
    //end ozair 3643
    
    function checkGComments()
		{
//		   var dateLenght = 0;
//		  var newLenght = 0;
//		  
//          newLenght = document.frmgeneralinfo.WCC_GeneralComments_txt_Comments.value.length
//		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

//		  if (document.frmgeneralinfo.WCC_GeneralComments_txt_Comments.value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerText.length > (2000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
//		  {
//		    alert("Sorry You cannot type in more than 2000 characters in General comments box")
//			return false;		  
//		  }

//		    //General Comments Check
//			if (document.getElementById("txtGeneralComments").value.length > 2000-27) //-27 bcoz to show last time partconcatenated with comments
//			{
//				alert("You Cannot type more than 2000 characters ");
//				document.getElementById("txtGeneralComments").focus();
//				return false;
//			}
//		
}
		
		
		
		
        function OpenPopUpNew(path)  //(var path,var name)
		{
		    window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		    return false;
		}
        
        function HideNShow()
        {
            var ddlOperation=document.getElementById("ddlOperation").value;
            
            var tdfile=document.getElementById("fpupload");
            //Nasir 6181 07/31/2009 remove for above td
            var tdScan=document.getElementById("Scan");
            //Nasir 6181 07/31/2009 hide button
            var tdsubmit=document.getElementById("btnSubmit");
            var ddldocType=document.getElementById("ddlDocType").value;
            
            if(ddlOperation=="3")
            {
                document.getElementById("nonDoc").style.display='none';
                document.getElementById("GeDoc").style.display='block';
            }
            else 
            {
                document.getElementById("nonDoc").style.display='block';
                document.getElementById("GeDoc").style.display='none';
                if(ddlOperation== "2")
                {
                
                    tdfile.style.display='block';
                    //Nasir 6181 07/31/2009 remove for above td
                    tdScan.style.display='none'   ;
                    tdsubmit.style.display='block';    
                    
                }
                else
                {
                
                    tdfile.style.display='none';   
                    //Nasir 6181 07/31/2009 remove for above td
                    tdScan.style.display='block'   ;
                    tdsubmit.style.display='none';    
                }
            }
            
        }
        function check()
        {   
            var ddldocType=document.getElementById("ddlDocType").value;
            var ddlOperation=document.getElementById("ddlOperation").value;
            
            var tdfile=document.getElementById("fpupload");
            
            if(tdfile.style.display=="block")
            {
                if(ddldocType=="1")
                {
                    alert("Please Select the Document Type");
                    document.getElementById("ddlDocType").focus();
                    return false;
                }
                // Noufil 6358 08/13/2009 IF ALR case is not dispose then Display javascript message
                else if (ddldocType == "20" && document.getElementById("ddl_SubDocType").value == "13" && document.getElementById("hf_IsALRDispoaseCase").value == "0")
                {
                    alert("Please dispose the ALR case.");
                    return false;
                }
            }
            if (ValidateSubDoctype()==false)
                return false;
           
        }
        function hide()
        {
            
            var tdfile=document.getElementById("fpupload");
            //Nasir 6181 07/31/2009 remove for above td and btnSubmit
            var tdsubmit=document.getElementById("btnSubmit");
            
            tdfile.style.display='none';
            //Nasir 6181 07/31/2009 remove for above td and 
            tdsubmit.style.display='none';
        }
        function PopUpShowPreviewMSG(DocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt,'',"height=10,width=10,resizable=no, status=no,toolbar=no,scrollbars=no,menubar=no,location=no");
				return false;
			}
        function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
			{
			    //Ozair 8039 07/20/2010 check implemented for ARR/Bond Summary report view (reftype 4)
				if(refType==4)
				{
				    window.open ("../QuickEntryNew/PreviewPrintHistory.aspx?filename="+ DocID);
				}
				else
				{
				    window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				}
				return false;
			}
			function DeleteCheck(id)
			{
			    //Noufil 5819 07/05/5009 Don't allow depenedent document to delete
			    var SubDocTypeValue = document.getElementById(id).name;
		        var SubdoctypeInGrid = document.getElementById("hf_Subdoctype").value; 
		        var IsALRDPSRecieptDoc = "0";
		        var IsALRSubpoenaPickupDOC = "0";
		        var IsALRContRequestResetManDOC = "0";
		        var IsALRContRequestResetDISDOC = "0";
    		    
		        SubdoctypeInGrid = SubdoctypeInGrid.split(',');
    		    
		        for ( i = 0 ; i < SubdoctypeInGrid.length; i++)
		        {
		            if (SubdoctypeInGrid[i] == "6")
		                IsALRDPSRecieptDoc = "1";
		            if (SubdoctypeInGrid[i] == "10")
		                IsALRSubpoenaPickupDOC = "1";
		            // Noufil 6358 08/24/2009 Code Comment
                    //	if (SubdoctypeInGrid[i] == "16")
                    //	    IsALRContRequestResetManDOC = "1";
                    //	if (SubdoctypeInGrid[i] == "18")
                    //	   IsALRContRequestResetDISDOC = "1";
		        }
		    
		        if (SubDocTypeValue == 5 && IsALRDPSRecieptDoc == "1")
		        {
		            alert("Please delete ALR DPS RECIEPT document first then delete ALR HEARING REQUEST Document.");
		            return false;
		        }		        
		        if (SubDocTypeValue == 9 && IsALRSubpoenaPickupDOC == "1")
		        {
		            alert("Please delete ALR SUBPOENA PICKUP ALERT document first then delete ALR SUBPOENA APPLICATION document.");
		            return false;
		        }		        
		        
		        // Noufil 6358 08/24/2009 Code Comment
                // if (SubDocTypeValue == 15 && IsALRContRequestResetManDOC == "1")
                // {
                //		            alert("Please delete ALR CONTINUANCE REQUEST RESET (MANDATORY) document first then delete ALR CONTINUANCE REQUEST (MANDATORY) document.");
                //		            return false;
                //		        }		       
                //		        if (SubDocTypeValue == 17 && IsALRContRequestResetDISDOC == "1")
                //		        {
                //		            alert("Please delete ALR CONTINUANCE REQUEST RESET (DISCRETIONARY) document first then delete ALR CONTINUANCE REQUEST (DISCRETIONARY) document.");
                //		            return false;
                //		        }
			    
				var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
				if(x)
				{
				}
				else
				{
					return false;
				}
			}
		
		//added by khalid bug 2526  	
		function OpenPopUpSmall(path)  //(var path,var name)
		{
		 window.open(path,'',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");
		}
		
		// Abid Ali 5397 1/16/2009 for LOR certified
		function OpenReport(filePath)
        {
            
            window.open( filePath ,"");
            return false;
        }
					
		function PrintLetter( flag)
		{   
		    //var flag=document.getElementById("ddlLetterType").value;
	//a;
		    if(flag== "-1")
		    {
		        alert("PLease Select Letter Type!");
		        document.getElementById("ddlLetterType").focus();
		        return false;
		    }
		  /*if(flag=="1")
		  {
			//alert(flag)			
			OpenPopUp("../ClientInfo/frmCaseSummary.aspx?casenumber=<%=ViewState["vTicketId"]%>&search=<%=ViewState["vSearch"]%>");	
			return false;	
		  }*/
		  //for EmailTrial leter..
		  if(flag == "9")
		  { 
		    var retBatch=false; 
		    
		   OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
		   return false;
				
		  }
		  if(flag=="2")
		  {	 
		  //var retBatch=false;
		  var retBatch=false;
		  var option = confirm("Trial Noticiation Letter generated. Would you like to 'PRINT NOW' or 'SEND TO BATCH'?\n Click [OK] to print now or click [CANCEL] to send to batch."); 
		  if (option)
		    retBatch =false;
     	  else
		    retBatch=true;
		  var courtid = document.getElementById("lbl_court").innerText;
	            var flg_split = document.getElementById("lbl_IsSplit").innerText;
	            var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerText;
		  //changes made by khalid  bug 2526 date 22-1-08
		     var PrintContinue;
	       	if (retBatch == true)
				 {  
		           if(flg_print==1)
	                    {
	                        PrintContinue = confirm("Case number "+courtid +" already exists in batch print queue. Would you like to override the existing letter with this new letter");
	                    }
	                if(PrintContinue==false)
	                // Afaq 8105 08/16/2010 Add date parameter in url.
	                    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=0"+"&Date=<%=DateTime.Now.ToFileTime().ToString()%>");	
	                else
				        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=1"+"&Date=<%=DateTime.Now.ToFileTime().ToString()%>");	
				 
				}
				 else 
				 {
			//	alert("here");
				 OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");	
				 //location.reload(true);
				
				}			 
		   
		  
		  //end 2526
		  //OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&CourtID="+courtid+"&flg_split="+flg_split+"&flg_print="+flg_print );	
		  
		   return false;
		  }						
			if(flag=="3")
		    {	
			
				window.open("../Reports/word_receipt.aspx?casenumber=<%=ViewState["vTicketId"]%>",'',"left=150,top=50,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");	
				

			
			return false;	
			}		
			if(flag=="4")
			{	
			         //Nasir 6483 10/06/2009 logic for bond report for sec user
			         var paymenttype=document.getElementById("hf_CheckBondReportForSecUser").value;
        	        
	                if(paymenttype!="0")
	                {
	                    if(paymenttype == 7)
	                    {
	                        alert("Bonds can only be printed within 2 months of last payment.");
	                    }
	                    else
	                    {
	                        alert("Bonds can only be printed within 3 Weeks of last payment.");
	                    }
	                    return false;
	                }
        			
			        var courtid=document.getElementById("lbl_courtid").innerText;
				        if (courtid==3001 ||courtid==3002 || courtid==3003)
				        {
				          OpenPopUp("../Reports/Word_Bond.aspx?CourtType=0" + "&casenumber=<%=ViewState["vTicketId"]%>");	
				        }
				        else
				        {
				          OpenPopUp("../Reports/Word_Bond.aspx?CourtType=1" + "&casenumber=<%=ViewState["vTicketId"]%>");	
				        }		
			        return false;	
			}			
			if(flag=="5")
		    {	 
				OpenPopUp("../Reports/FrmMainCONTINUANCE.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>");	
				
			return false;	
			}
			

			if(flag=="6")
		    {	 
		       
		        
		        //Nasir 6181 07/22/2009 if all cases disposed.Dont Allow to print LOR 
              var isnotDisposed=document.getElementById("hf_IsnotDisposed").value;
        
                if(isnotDisposed==0)
                {
                       alert("All cases disposed. LOR not available");
                       return false;
                }
		         // Sabir Khan 5763 05/01/2009 Check for LOR Subpoena Letter and LOR Motion of Discovery...
	            //----------------------------------------------------------------------------------------
	            if(document.getElementById("hf_IsLORSubpoena").value == "1")
	            {
	                if(document.getElementById("hf_HasSameCourtdate").value == "0")
	                {
	                    alert("The LOR for this court house contains Subpeona Letter and it requires to have same court date/time for all violations.\n Please first fix this issue and then print LOR.");
	                    return false;
	                }
	            }
	            if(document.getElementById("hf_IsLORMOD").value == "1")
                {
	                if(document.getElementById("hf_HasMoreSpeedingviol").value == "0")
	                {
	                    alert("The LOR for this court house contains Motion For Discovery and it requires to have only one speeing violation.\n This client has more than one speeding violation. Please first fix this issue and then print LOR.");
	                    return false;
	                }
	            }
    		    //----------------------------------------------------------------------------------------  
		      
		       //Nasir 6181 07/22/2009 if LOR method is fax
		       //Ozair 6460 08/25/2009 included check for bond flag
		        if(document.getElementById("hfLORMethodCount").value != '0' && document.getElementById("hf_IsBond").value=="0") 
                {               
                    return true;            
                }
		        
		        //Nasir 6013 07/08/2009 remove message
		       
			    
			    var flg_print = document.getElementById("lbl_IsLORAlreadyInBatchPrint").innerHTML;
	         //Nasir 6181 07/27/2009 remove retbatch
	            if ( flg_print == 1)
                {
			        if(!confirm("Case number <%=CauseNumber%> already exists in batch print queue. Would you like to override the existing letter with this new letter"))
			        {
			            return false;
			        }			    		    
                }
                                                    
                      //Nasir 6181 07/27/2009 remove retbatch condition and print letter url            
                    //Ozair 6460 08/25/2009 small popup method called
                    OpenPopUpSmall("../Reports/frmSendLetterOfRepBatch.aspx?Batch=true&lettertype=6&casenumber=<%=ViewState["vTicketId"]%>");					    	           
                                           	  
			    //Nasir 6013 07/10/2009 replace doyou to false
			    return false;	
			}
			
			if(flag=="8")
		    {				    		    
			OpenPopUp("../../reports/Payment_Receipt_Contract.asp?TicketID=<%=ViewState["vTicketid"]%>");	
			return false;	
			}
			  
		}
		function OpenPopUp(path)  //(var path,var name)
		{
		
		 window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		 
		}
		
		function ValidateSubDoctype()
		{		
		    //Noufil 5819 07/05/5009 Don't allow child document to upload without parent document
		    var SubDocTypeValue = document.getElementById("ddl_SubDocType").value;
		    var SubdoctypeInGrid = document.getElementById("hf_Subdoctype").value; 
		    var IsALRHearingDoc = "0";
		    var IsALRSubpoenaApplicationDOC = "0";
		    var IsALRContRequestManDOC = "0";
		    var IsALRContRequestDISDOC = "0";
		    var IsALRhearingNotDOC = "0";
		    
		    SubdoctypeInGrid = SubdoctypeInGrid.split(',');
		    
		    for ( i = 0 ; i < SubdoctypeInGrid.length; i++)
		    {
		        if (SubdoctypeInGrid[i] == "5")
		        {
		            IsALRHearingDoc = "1";
		        }   
		        
		        if (SubdoctypeInGrid[i] == "9")
		        {
		            IsALRSubpoenaApplicationDOC = "1";
		        }
		            
		        if (SubdoctypeInGrid[i] == "15")
		        {
		            IsALRContRequestManDOC = "1";
		        }		        
		        else if (SubdoctypeInGrid[i] == "17")
		        {
		            IsALRContRequestDISDOC = "1";
		        }		        
		        else if (SubdoctypeInGrid[i] == "7")
		        {
		            IsALRhearingNotDOC = "1";
		        }
		    }		    
		   
		    if (SubDocTypeValue == 6 && IsALRHearingDoc == "0")
		    {
		        alert("Please add ALR HEARING REQUEST document first then Add ALR DPS RECIEPT document.");
		        return false;
		    }
		    
		    if (SubDocTypeValue == 10 && IsALRSubpoenaApplicationDOC == "0")
		    {
		        alert("Please add ALR SUBPOENA APPLICATION document first then Add ALR SUBPOENA PICKUP ALERT document.");
		        return false;
		    }
		    
		    
		    if (IsALRContRequestManDOC =="1" || IsALRContRequestDISDOC=="1")
		    {
		        // Noufil 6358 08/24/2009 ALR CONTINUANCE REQUEST (MANDATORY) document can only be added after ALR HEARING NOTIFICATION or no ALR CONTINUANCE REQUEST (MANDATORY) and ALR CONTINUANCE REQUEST (DISCRETIONARY) document present.
		        if (SubDocTypeValue == 15)
		        {
		            alert("Please add ALR HEARING NOTIFICATION document first then Add ALR CONTINUANCE REQUEST (MANDATORY)");
		            return false;
		        }
		        // Noufil 6358 08/24/2009 ALR CONTINUANCE REQUEST (DISCRETIONARY) document can only be added after ALR HEARING NOTIFICATION or no ALR CONTINUANCE REQUEST (MANDATORY) and ALR CONTINUANCE REQUEST (DISCRETIONARY) document present.
		        else if(SubDocTypeValue == 17)
		        {
		            alert("Please add ALR HEARING NOTIFICATION document first then Add ALR CONTINUANCE REQUEST (DISCRETIONARY)");
		            return false;
		        }		        
		    }		    
		}
		
		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
		
    </script>

    <!-- Do Not Delete This. initialozing the scannig object. -->
    <%=Session["objTwain"].ToString()%>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="hide();">
    <form id="form1" runat="server" enctype="multipart/form-data">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tbody>
                <tr>
                    <td colspan="5">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" style="height: 33px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td width="80%">
                                                <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                                <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                                <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>)
                                                <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink><a href="#"></a>
                                            </td>
                                            <td align="right" width="14%">
                                            </td>
                                            <td align="right" width="6%">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMessage" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td id="read" runat="server" style="width: 100%;" colspan="2">
                                    <uc2:ReadNotes ID="ReadNotes1" runat="server" />
                                </td>
                            </tr>
                            <!-- next section starts -->
                            <tr style="font-family: Tahoma">
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px;
                                                width: 50%;">
                                                &nbsp; Scan/Upload
                                            </td>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px;
                                                width: 50%;" align="right">
                                                <asp:DropDownList ID="ddlOperation" runat="server" CssClass="clsInputCombo">
                                                    <asp:ListItem Selected="True" Value="0">Scan-Feeder</asp:ListItem>
                                                    <asp:ListItem Value="1">Scan-Flatbed</asp:ListItem>
                                                    <asp:ListItem Value="2">Upload</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="font-family: Tahoma">
                                <td colspan="2" valign="top">
                                    <table id="tbl_contact" border="0" cellpadding="0" cellspacing="1" width="780">
                                        <tr>
                                            <td valign="top" width="780px" id="nonDoc">
                                                <table border="0" cellpadding="0" cellspacing="1" width="780">
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" style="height: 20px; width: 140px;">
                                                            &nbsp;Document Type
                                                        </td>
                                                        <td id="sdt" runat="server" class="clsLeftPaddingTable" style="height: 20px; width: 140px;">
                                                            Sub Type
                                                        </td>
                                                        <td class="clsLeftPaddingTable" colspan="4" style="height: 20px; width: 500px;">
                                                            Description
                                                        </td>
                                                        <%--  <td class="clsLeftPaddingTable" style="height: 20px" id="fpupload1">
                                                        </td>
                                                        <td class="clsLeftPaddingTable" style="height: 20px;width:160px">
                                                        </td>--%>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" style="height: 20px; width: 140px;">
                                                            &nbsp;<asp:DropDownList ID="ddlDocType" runat="server" CssClass="clsInputCombo" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="sdt1" runat="server" class="clsLeftPaddingTable" style="height: 20px; width: 140px;">
                                                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" EnableViewState="true">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddl_SubDocType" runat="server" CssClass="clsInputCombo">
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <aspnew:AsyncPostBackTrigger ControlID="ddlDocType" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </aspnew:UpdatePanel>
                                                        </td>
                                                        <td id="txtdescrip" class="clsLeftPaddingTable" style="height: 20px; width: 140px">
                                                            <asp:TextBox ID="txtDescription" runat="server" CssClass="clsInputcommentsfield"
                                                                Width="150px" MaxLength="100">
                                                            </asp:TextBox>
                                                        </td>
                                                        <td class="clsLeftPaddingTable" colspan="2" style="height: 20px; width: 360px">
                                                            <table class="clsLeftPaddingTable">
                                                                <tr>
                                                                    <td class="clsLeftPaddingTable" id="fpupload">
                                                                        <asp:FileUpload ID="FPUpload" runat="server" CssClass="clsInputcommentsfield" Width="200px" />
                                                                    </td>
                                                                    <td id="Scan" class="clsLeftPaddingTable" style="height: 20px; width: 60px;">
                                                                        <asp:Button ID="btnScan" runat="server" CssClass="clsbutton" Text="Scan" OnClick="btnScan_Click"
                                                                            Width="60px" />
                                                                    </td>
                                                                    <td class="clsLeftPaddingTable" id="submit" style="height: 20px; width: 60px;">
                                                                        <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSubmit_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" id="GeDoc" style="display: none">
                                                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" height="20">
                                                            &nbsp;<asp:DropDownList ID="ddlLetterType" runat="server" CssClass="clsInputCombo">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="clsLeftPaddingTable" align="right">
                                                            &nbsp;
                                                            <asp:Button ID="btnGenerateDocs" runat="server" Text="Generate Documents" CssClass="clsbutton"
                                                                OnClick="btnGenerateDocs_Click" />&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="font-family: Tahoma">
                                <td colspan="6">
                                    <table width="100%" cellpadding="0" cellspacing="0" style="left: 0px; position: relative;
                                        top: 0px">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" height="34">
                                                &nbsp; Generate Documents
                                            </td>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" height="34" align="right">
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="1" width="100%" style="left: 1px;
                                        position: relative; top: 0px">
                                        <tr>
                                            <td class="clsLeftPaddingTable" id="Td7" style="height: 13px">
                                                <asp:DataList ID="dl_generatedocs" runat="server" RepeatDirection="Horizontal" Style="position: relative"
                                                    Width="100%" OnItemDataBound="dl_generatedocs_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lb_docs" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.value") %>'
                                                            OnClick="btnGenerateDocs_Click" Style="position: relative; left: 0px;" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                            OnCommand="lb_docs_Command"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- next section starts -->
                            <tr style="font-family: Tahoma">
                                <td background="../../images/headbar_headerextend.gif" colspan="2" style="height: 5px">
                                </td>
                            </tr>
                            <tr style="font-family: Tahoma">
                                <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" height="34">
                                    &nbsp; Document History&nbsp;
                                </td>
                            </tr>
                            <tr style="font-family: Tahoma">
                                <td colspan="2" valign="top">
                                    <table id="tbl_comments" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="top" width="50%">
                                                <table id="tbl_comm_1" border="0" cellpadding="0" cellspacing="1" width="100%">
                                                    <tr class="clsLeftPaddingTable">
                                                        <td height="20">
                                                            <asp:DataGrid ID="dgrdDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                Font-Names="Verdana" Font-Size="2px" PageSize="20" Width="100%" OnItemCommand="dgrdDoc_ItemCommand">
                                                                <FooterStyle CssClass="GrdFooter" />
                                                                <PagerStyle CssClass="GrdFooter" Font-Names="Arial" HorizontalAlign="Center" Mode="NumericPages"
                                                                    NextPageText="" PageButtonCount="50" PrevPageText="" />
                                                                <AlternatingItemStyle BackColor="#EEEEEE" />
                                                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Date">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDateTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATEDATETIME") %>'
                                                                                Font-Size="Smaller"></asp:Label>&nbsp; &nbsp;
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Event">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEvent" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Events") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Document Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOCREF") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lbldid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblocrid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocNum" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Document Description">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocDescription" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ResetDesc") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocExtension" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Rep">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAbb1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'
                                                                                Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="View">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif"
                                                                                Width="19" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Pages / Docs">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocCount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ImageCount") %>'
                                                                                Width="32px" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblRecType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>'
                                                                                Visible="False" Width="10px" Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Options">
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkbtn_Delete" Name='<%#Eval("subdoctype") %>' runat="server"
                                                                                CommandName="Delete" CommandArgument='<%#Eval("subdoctype") %>' Font-Size="Smaller">Delete,</asp:LinkButton>&nbsp;
                                                                            <asp:LinkButton ID="lnkbtn_OCR" runat="server" CommandName="OCR" Font-Size="Smaller">OCR</asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkbtn_VOCR" runat="server" CommandName="View" Visible="False"
                                                                                Font-Size="Smaller">View OCR</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- next section starts -->
                            <tr>
                                <td colspan="2">
                                    <table id="FlagsE" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="3" height="34">
                                                &nbsp;General Comments&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table id="tbl_flagdetail" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr class="clsLeftPaddingTable">
                                            <td height="20">
                                                <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Width="765px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="3" rowspan="1" style="height: 20px">
                                    <asp:Button ID="btnupdate" runat="server" CssClass="clsbutton" OnClick="btnupdate_Click"
                                        Text="Update" />
                                </td>
                            </tr>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <uc1:Footer ID="Footer1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="visibility: hidden" colspan="2">
                                    <asp:TextBox ID="txtImageCount" runat="server"></asp:TextBox><asp:TextBox ID="txtQuery"
                                        runat="server"></asp:TextBox><asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox
                                            ID="txtsessionid" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtbID" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox>
                                    <asp:Label ID="lbl_courtid" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsSplit" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_court" runat="server"></asp:Label>
                                    <asp:HiddenField ID="hf_TicketID" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORSubpoena" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORMOD" runat="server" />
                                    <asp:HiddenField ID="hf_HasSameCourtdate" runat="server" />
                                    <asp:HiddenField ID="hf_HasMoreSpeedingviol" runat="server" />
                                    <asp:HiddenField ID="hf_IsnotDisposed" runat="server" />
                                    <asp:HiddenField ID="hfLORMethodCount" runat="server" />
                                    <asp:HiddenField ID="hf_Subdoctype" runat="server" />
                                    <asp:HiddenField ID="hf_IsALRDispoaseCase" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_IsBond" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_CheckBondReportForSecUser" Value="0" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <asp:Label ID="lbl_IsLORAlreadyInBatchPrint" Style="display: none;" runat="server"></asp:Label>
        <asp:HiddenField ID="hf_letterofrap" runat="server" />
        <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                    PopupControlID="panel1" BackgroundCssClass="modalBackground" HideDropDownList="false"
                    CancelControlID="img1">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="panel1" runat="server" Width="483px" Height="400px">
                    <table id="Table1" bgcolor="white" border="0" style="border-top: black thin solid;
                        border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                        height: 400px;">
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                <table>
                                    <tr>
                                        <td style="width: 500px">
                                            <span class="clssubhead">Compose Fax</span>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="img1" runat="server" ImageUrl="../Images/remove2.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Fc:Faxcontrol ID="Faxcontrol1" runat="server" Recall="true" />
                                <asp:Button ID="Button1" runat="server" Text="Cancel" Style="display: none;" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>

    <script language="JavaScript">
function StartScan()
{
       
	if(document.getElementById("ddlDocType").value=="1" || document.getElementById("ddlDocType").value=="1")
	{
		alert("Plese Select valid document type to scan");
		document.getElementById("ddlDocType").focus();
		return false;
	}
	if (ValidateSubDoctype()==false)
        return false;
    var ddlOperation=document.getElementById("ddlOperation").value;
    //Ozair 5546 02/17/2009 included ticketid with session id
    var sid= document.getElementById("txtsessionid").value + document.getElementById("hf_TicketID").value;
	var eid= document.getElementById("txtempid").value;
	var path='<%=ViewState["vNTPATHScanTemp"]%>';
	var Type='network';		
	var AutoFeed=1;
	var sel=OZTwain1.SelectScanner();
	if(sel=="Success")
	{
	    if (ddlOperation == "0")
	    {
	        AutoFeed=-1;
	    }	    
	    
	    OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
	    //
	    if (ddlOperation == "1")
	      {
		    var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		    if(x)
		    {
			    //OZTwain1.Acquire(sid,eid,path,Type);
			    StartScan();
		    }	
		    else
		    {
		        document.getElementById("txtbID").value="";
        	    //document.Form1.Adf.checked = true;
		    }		
	      }
    }
    else if(sel=="Cancel")
    {
        alert("Operation canceled by user!");
        return false;
    }
    else 
    {
        alert("Scanner not installed.");
	    return false;
    }
    
}
    </script>

</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;


namespace lntechNew.PaperLess
{
	/// <summary>
	/// Summary description for UploadScanDocument.
	/// </summary>
	public partial class UploadScanDocument : System.Web.UI.Page
	{

		
		clsENationWebComponents ClsDb = new clsENationWebComponents();

		private void Page_Load(object sender, System.EventArgs e)
		{
			 if (Request.Files.Count == 0) return;
			
			//check book ID
			int BookId = (int) Convert.ChangeType(Request["Book"].ToString() ,typeof(int));
			if(BookId == 0)
			 {Random  r = new Random(); 

				BookId =  (int) (r.NextDouble() * 30000) + 1 ; 
			} 


			///Counting Documents
			int DocCount=0;
			//get old value
			DocCount =(int) Convert.ChangeType(Request["Count"].ToString(),typeof(int));
			DocCount += 1;
			string onlyPic = Session.SessionID + BookId + "-" + DocCount + ".JPG";
			//update new value	
			
			///
								
			//Save the file to the server.
			string fileName = Server.MapPath("../temp/") + onlyPic;
			Request.Files[0].SaveAs(fileName);
			
	
					
			
			
			//Convert Dollar sign back to Spaces
			string bType;
			bType= Request["Type"].ToString();
			bType = bType.Replace("$"," "); 
			//

			
			
			//tell new book ID and Doc Count to calling script
			Response.Write("?BID=+" + BookId + "&Dcount=" + DocCount+ "&PicName=" + onlyPic + "&OCR=" + Request["OCR"].ToString() );  
			Response.End(); 

			
			
			//call OCR			 
			//string strOCR = OcrIt(fileName);
			
			////

					
						
		}


		private string OcrIt(string filePath)
		{
			MODI.Document miDoc;
			 
			miDoc = new MODI.Document();
			miDoc.Create(filePath);
  
			miDoc.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH,true,true);
  
			MODI.Image image = (MODI.Image) miDoc.Images[0];
			MODI.Layout layout = image.Layout;
			return layout.Text; 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
/*
   if (Request.Files.Count == 0) return;
			
								
			//Save the file to the server.
			string fileName = Server.MapPath("../temp/") + Session.SessionID + "TT.JPG";
			Request.Files[0].SaveAs(fileName);
			

			///Load Document into byte array
			//int len = Request.Files[0].ContentLength;      
	//		byte[] pic = new byte[len];      
	//		Request.Files[0].InputStream.Read (pic, 0, len);
			///
			
			///Counting Documents
int DocCount=0;
			 //get old value
DocCount =(int) Convert.ChangeType(Request["Count"].ToString(),typeof(int));
DocCount += 1;
			 //update new value	
Session["DocCount"] = DocCount;
			///
			
			
int empid= (int) Convert.ChangeType(Request["Employee"].ToString() ,typeof(int));; //
int TicketID = (int) Convert.ChangeType(Request["Ticket"].ToString() ,typeof(int));
int BookId = (int) Convert.ChangeType(Request["Book"].ToString() ,typeof(int));
			
			//Convert Dollar sign back to Spaces
string bType;
bType= Request["Type"].ToString();
bType = bType.Replace("$"," "); 
			//

			///Query Work
			///
string picName,onlyPic;
string[] key  = {"@updated","@extension","@DocType","@Employee","@TicketID","@Count","@Book","@BookID"};
object[] value1 = {DateTime.Now,"JPG",bType,empid,TicketID,DocCount,BookId,""};
			//call sP and get the book ID back from sP
picName = ClsDb.InsertBySPArrRet("usp_Add_ScanImage",key,value1).ToString() ;  
Session["BookID"] = picName.Split('-')[0]; 
			//Session["BookID"] 
			
			
			//Move file
				//picName= Server.MapPath("images/")+ picName + ".jpg"  ; 
picName= Server.MapPath("../temp/")+ picName + ".jpg"  ; 
System.IO.File.Copy(fileName,picName);
System.IO.File.Delete(fileName);
			 
			//
			//tell new book ID and Doc Count to calling script
Response.Write("?BID=" + Session["BookID"].ToString() + "&Dcount=" + DocCount+ "& PicName=" + onlyPic);  
Response.End(); 

			
			
			//call OCR			 
			//string strOCR = OcrIt(fileName);
			
			////

  */

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.PaperLess.PreviewScanDocument" Codebehind="PreviewScanDocument.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PreviewScanDocument</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Style.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form method="post" id="frm1" runat="server">
			<!--webbot bot="SaveResults" u-file="fpweb:///_private/form_results.csv" s-format="TEXT/CSV" s-label-fields="TRUE" -->
			<P>
			</P>
			<P></P>
			<P></P>
			<P>&nbsp;</P>
			<P>
				<TABLE class="clsLeftPaddingTable" id="Table1" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 0px"
					cellSpacing="1" cellPadding="1" width="100%" border="0">
					<TR>
						<TD align="left" colSpan="3" bgColor="#ffffff">&nbsp;
						</TD>
					</TR>
					<TR class="clssubhead">
						<TD class="clssubhead" align="left" background="../Images/subhead_bg.gif" colSpan="3">Preview 
							Scan Document</TD>
					</TR>
					<TR>
						<TD vAlign="middle" align="left" colSpan="3"><SPAN>&nbsp;<A class="StyleSubMenu" href="javascript:zoom_in()"><IMG height="24" src="../Images/Zoomin.gif" width="24" border="0">&nbsp;
								</A><SPAN>&nbsp;<A href="javascript:zoom_out()"><IMG loop="0" src="../Images/zoomOut.gif" border="0"></A></SPAN>&nbsp;</SPAN>&nbsp;
							<asp:ImageButton id="imgPreviouse" runat="server" ToolTip="Show Previouse" Height="24px" Width="24px"
								ImageUrl="../Images/previouse.gif"></asp:ImageButton>&nbsp;&nbsp;
							<asp:ImageButton id="imgNext" runat="server" ToolTip="Show Next" Height="24px" Width="24px" ImageUrl="../Images/next.gif"></asp:ImageButton>&nbsp;&nbsp;
							<asp:ImageButton id="ImageButton1" runat="server" ToolTip="Remove this document" Height="24px" Width="24px"
								ImageUrl="../Images/remove.gif"></asp:ImageButton>&nbsp;&nbsp;
							<asp:ImageButton id="ImageButton2" runat="server" ToolTip="Perform OCR" Height="24px" Width="24px"
								ImageUrl="../Images/ocr.gif"></asp:ImageButton>&nbsp;&nbsp;
							<asp:ImageButton id="imgOK" runat="server" ToolTip="Go Back" Height="24px" Width="24px" ImageUrl="../Images/done.gif"></asp:ImageButton></TD>
					</TR>
					<TR>
						<TD vAlign="middle" align="left" colSpan="3">
							<asp:TextBox id="txtData" runat="server" Height="88px" Width="615px" TextMode="MultiLine"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD vAlign="middle" align="left" colSpan="3">
							<DIV id="izo_div" style="LEFT: 1px; POSITION: absolute; TOP: 205px"><IMG style="CURSOR: hand" height="360" alt="Image Zoom Out" width="240" border="3" name="izo"
									bordercolor="#000000"></DIV>
						</TD>
					</TR>
				</TABLE>
			</P>
		</form>
		<SCRIPT language="javascript">

// (c) 2002 Premshree Pillai
// http://www.qiksearch.com
// premshree@hotmail.com
// Loaction of this script :
// http://www.qiksearch.com/javascripts/image_zoom_out.htm
// Visit http://www.qiksearch.com/javascripts.htm for more FREE scripts

//document.write('<div id="izo_div" style="position:absolute; top:1; left:200"><img src="fmx_loadjpg.zip"  border="3" bordercolor="#000000" name="izo" alt="Image Zoom Out" style="cursor:hand"></div>');
 //document.write("<div id='izo_div' style='position:absolute; top:140; left:1'><img  border='3' bordercolor='#000000' name='izo' alt='Image Zoom Out' style='cursor:hand'></div>");
  
//img_act_width = izo.width;
//img_act_height = izo.height;
//izo.width=240;
//izo.height= 360;
//izo.src="images/64-111.jpg";
//document.write(img_act_width);
i=1;

function loadPIC()
{
//var c = document.getElementById("txtImageCount").value; 
//	 izo.src= c
}


function zoom_out()
{ 
 if(document.frm1.izo.width==0)
  {
   document.frm1.izo.border=0;
  }
 if(document.frm1.izo.width>240 && document.frm1.izo.height>360 )
  {
   document.frm1.izo.width-=(document.frm1.izo.width*0.1);
   document.frm1.izo.height-=(document.frm1.izo.height*0.1);
   i+=1;
  }  
} 

function zoom_in()
{
 if(document.frm1.izo.width==0)
  {
   document.frm1.izo.border=0;
  }
 if(document.frm1.izo.width<2300 || document.frm1.izo.height<3300 )
  {
   document.frm1.izo.width+=(document.frm1.izo.width*0.1);
   document.frm1.izo.height+=(document.frm1.izo.height*0.1);
   i+=1;
  }  
 }
  
  function Move_Left()
{
   document.frm1.izo.left+= 10; //(izo.width*0.1);
   
 

} 

		</SCRIPT>
		<%=strR%>
	</body>
</HTML>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="lntechNew.PaperLess.uploadNsave" Codebehind="uploadNsave.aspx.cs" %>
<HTML>
	<HEAD>
		<title>File upload in ASP.NET</title>
	</HEAD>
	<body bgcolor="#ffffff" style="FONT:8pt verdana">
		<script language="C#" runat="server">
void btnUploadTheFile_Click(object Source, EventArgs evArgs) 
{
	string strFileNameOnServer = txtServername.Value;
	string strBaseLocation = "c:\\temp\\";
	
	if ("" == strFileNameOnServer) 
	{
		txtOutput.InnerHtml = "Error - a file name must be specified.";
		return;
	}

	if (null != uplTheFile.PostedFile) 
	{
		try 
		{
			uplTheFile.PostedFile.SaveAs(strBaseLocation+strFileNameOnServer);
			txtOutput.InnerHtml = "File <b>" + 
				strBaseLocation+strFileNameOnServer+"</b> uploaded successfully";
		}
		catch (Exception e) 
		{
			txtOutput.InnerHtml = "Error saving <b>" + 
				strBaseLocation+strFileNameOnServer+"</b><br>"+ e.ToString();
		}
	}
}
		</script>
		<table>
			<form enctype="multipart/form-data" runat="server">
				<TBODY>
					<tr>
						<td>Select file:</td>
						<td><input id="uplTheFile" type="file" runat="server"></td>
					</tr>
					<tr>
						<td>Name on server:</td>
						<td><input id="txtServername" type="text" runat="server"></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="button" id="btnUploadTheFile" value="Upload" OnServerClick="btnUploadTheFile_Click"
								runat="server">
						</td>
					</tr>
			</form>
			</TBODY>
		</table>
		<span id="txtOutput" style="FONT: 8pt verdana" runat="server" />
	</body>
</HTML>

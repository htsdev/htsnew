using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo ;
using lntechNew.Components;
using lntechNew.WebControls;
using System.Configuration;

namespace lntechNew.PaperLess
{
	/// <summary>
	/// Summary description for ScanOnly.
	/// </summary>
	public partial class ScanOnly : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList cmbDocType1;
		protected System.Web.UI.WebControls.TextBox txtDescription;
		protected System.Web.UI.WebControls.DataGrid dgrdDoc;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.TextBox txtImageCount;
		protected System.Web.UI.WebControls.TextBox txtQuery;
	
		protected System.Web.UI.WebControls.TextBox txtempid;
		protected System.Web.UI.WebControls.TextBox txtsessionid;
		
		protected System.Web.UI.WebControls.TextBox txtSrv;
		protected System.Web.UI.WebControls.TextBox txtbID;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnScn;
		protected System.Web.UI.HtmlControls.HtmlInputCheckBox Adf;
	
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsSession ClsSession = new clsSession();
		protected string objTwain;
		protected string Qstr;

		private void Page_Load(object sender, System.EventArgs e)
		{
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx");
                }
                else //To stop page further execution
                {

                    if (!(Page.IsPostBack))
                    {
                        ViewState["vTicketId"] = Request.QueryString["casenumber"];
                        ViewState["vSearch"] = Request.QueryString["search"];
                        //network path
                        ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                        //

                        int tID = Convert.ToInt32(ViewState["vTicketId"]);
                        if (tID == 0)
                        {
                            Response.Redirect("../ClientInfo/ViolationFeeold.aspx?newt=1");
                        }

                        string strServer;
                        strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                        txtSrv.Text = strServer;
                        Session["objTwain"] = "<OBJECT id='VSTwain1' codeBase='" + strServer + "/VintaSoft.Twain.dll#version=1,6,1,1' height='1' width='1' classid='" + strServer + "/VintaSoft.Twain.dll#VintaSoft.Twain.VSTwain' VIEWASTEXT> </OBJECT>";

                        
                        BindControls();
                        txtsessionid.Text = Session.SessionID.ToString();
                        txtempid.Text = ClsSession.GetCookie("sEmpID", this.Request);
                    }
                    
                    BindGrid(ViewState["vTicketId"].ToString());
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
		}

		//object BindControls() 
        void BindControls()
        {
            try
            {
                DataSet ds = ClsDb.Get_DS_BySP("usp_Get_All_DocType");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string Description = ds.Tables[0].Rows[i]["DocType"].ToString().Trim();
                    string ID = ds.Tables[0].Rows[i]["DocTypeID"].ToString().Trim();
                    if (i == 0)
                    {
                        cmbDocType1.Items.Add(new ListItem("Select", "-1"));
                    }
                    else
                    {
                        cmbDocType1.Items.Add(new ListItem(Description, ID));
                    }

                }

            }
            catch { }
        }

        void BindGrid(string Criteria)
        {
            int intTotalRows;
            DataTable dtlTemp1;
            DataSet ds;

            try
            {
                string[] key = { "@TicketID", "@DocType" };

                object[] value1 = { Criteria, cmbDocType1.SelectedItem.Text };
                ds = ClsDb.Get_DS_BySPArr("usp_Get_All_ScanDocuments_New_Ver_1", key, value1);




                dgrdDoc.Visible = true;
                if (ds.Tables[0].Rows.Count > 0)
                {

                    dtlTemp1 = ds.Tables[0];

                    DataView dtvtemp1 = new DataView(dtlTemp1);
                    if (dtlTemp1.Rows.Count > 0)
                    {
                        intTotalRows = dtlTemp1.Rows.Count;

                        dgrdDoc.DataSource = dtvtemp1;
                        dgrdDoc.DataBind();
                        lblMessage.Text = "";
                        BindLinks();
                    }
                    else
                    {
                        lblMessage.Text = "No record(s) found";
                        ClsDb.InitGrid(dgrdDoc, "ScanDocView", 4);

                        DisableLinks();

                        dgrdDoc.PagerStyle.Visible = false;
                    }
                }
                else //If sP not return any data
                {
                    lblMessage.Text = "No record(s) found";
                    dgrdDoc.Visible = false;
                    dgrdDoc.PagerStyle.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.Source + ex.StackTrace;
            }
        }

        //
        public void DoGetQueryString(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string lngDocID;
            string lngRecordType;
            string strURL;
            string lngDocNum;
            try
            {

                if (e.CommandName == "DoGetScanDocx")
                {
                    lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                    lngDocNum = (((Label)(e.Item.FindControl("lblDocNum"))).Text);
                    lngRecordType = (((Label)(e.Item.FindControl("lblRecType"))).Text);
                    strURL = "PreviewPDF.aspx?DocID=" + lngDocID + "&RecType=" + lngRecordType + "&DocNum=" + lngDocNum;

                    Response.Redirect(strURL);
                }
                if (e.CommandName == "Delete")
                {
                    try
                    {
                        lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                        int picid = Convert.ToInt32(((Label)(e.Item.FindControl("lbldid"))).Text);
                        string[] key = { "@docid" };
                        object[] value1 = { Convert.ToInt32(lngDocID) };
                        ClsDb.ExecuteSP("usp_HTS_Delete_ScanDoc", key, value1);
                        BindGrid(ViewState["vTicketId"].ToString());
                    }
                    catch (Exception)
                    {
                        lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
                    }
                }
                if (e.CommandName == "OCR")
                {
                    try
                    {
                        lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                        string did = (((Label)(e.Item.FindControl("lbldid"))).Text);
                        string picDestination;
                        string[] key3 ={ "@sbookid" };
                        object[] value3 ={ Convert.ToInt32(lngDocID) };
                        DataSet ds_bid = ClsDb.Get_DS_BySPArr("usp_hts_get_totalpicsbybookid", key3, value3);
                        for (int i = 0; i < ds_bid.Tables[0].Rows.Count; i++)
                        {
                            did = ds_bid.Tables[0].Rows[i]["docid"].ToString();
                            string picName = lngDocID + "-" + did;
                            //picDestination = Server.MapPath("images/") + picName + ".jpg"; //DestinationImage
                            picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage
                            clsGeneralMethods CGeneral = new clsGeneralMethods();
                            string ocr_data = CGeneral.OcrIt(picDestination);
                            string[] key2 = { "@doc_id", "@docnum_2", "@data_3" };
                            object[] value2 = { Convert.ToInt32(lngDocID), Convert.ToInt32(did), ocr_data };
                            //call sP and get the book ID back from sP
                            ClsDb.InsertBySPArr("usp_scan_insert_tblscandata", key2, value2);
                        }
                        BindGrid(ViewState["vTicketId"].ToString());
                    }
                    catch (Exception)
                    {
                        lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
                    }
                }
                if (e.CommandName == "View")
                {
                    ViewState["sdoc_id"] = (((Label)(e.Item.FindControl("lblDocID"))).Text);
                    ViewState["sdocnum"] = (((Label)(e.Item.FindControl("lbldid"))).Text);
                    ViewState["sdoctypedesc"] = (((Label)(e.Item.FindControl("lblDocType"))).Text);
                    ViewState["sdocdesc"] = (((Label)(e.Item.FindControl("lblDocDescription"))).Text);
                    Response.Redirect("ViewOCR.aspx?search=" + ViewState["vSearch"].ToString() + "&casenumber=" + ViewState["vTicketId"].ToString() + "&sdoc_id=" + ViewState["sdoc_id"].ToString() + "&sdocnum=" + ViewState["sdocnum"].ToString() + "&sdoctypedesc=" + ViewState["sdoctypedesc"].ToString() + "&sdocdesc=" + ViewState["sdocdesc"].ToString());
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }


        void DisableLinks()
        {
            try
            {
                foreach (DataGridItem dgItem in dgrdDoc.Items)
                {
                    ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Visible = false;
                }
            }
            catch { }
        }


        void BindLinks()
        {
            long lngDocID;
            long RecType;
            long lngDocNum;
            string DocExt;
            try
            {
                foreach (DataGridItem dgItem in dgrdDoc.Items)
                {
                    lngDocID = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocID"))).Text);
                    lngDocNum = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocNum"))).Text);
                    RecType = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblRecType"))).Text);
                    DocExt = System.Convert.ToString(((Label)(dgItem.FindControl("lblDocExtension"))).Text);
                    ((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + lngDocID.ToString() + "," + RecType + "," + lngDocNum.ToString() + ",'" + DocExt + "'" + ");");
                    
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion





        private void btnUpload_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("uploadfile.aspx?casenumber=" + ViewState["vTicketId"].ToString() + "&search=" + ViewState["vSearch"].ToString());
        }

        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }

		private void btncancel_ServerClick(object sender, System.EventArgs e)
		{
		
		}

        protected void btnScn_ServerClick1(object sender, EventArgs e)
        {
            try
            {
                ViewState["ScanDocType"] = cmbDocType1.SelectedItem.Text;
                ViewState["BookID"] = 0;
                ViewState["DocCount"] = 0;

                if (txtbID.Text.Length > 0)
                {
                    ViewState["sSDDocCount"] = ViewState["sSDDocCount"].ToString();
                }
                else
                {
                    ViewState["sSDDocCount"] = 1;
                }
                ViewState["sSDDesc"] = txtDescription.Text.Trim().ToUpper();

                //Perform database Uploading Task

                string searchpat = "*" + Session.SessionID + txtempid.Text + "img.jpg";
                //string[] fileName = Directory.GetFiles(Server.MapPath("tempimages/"), searchpat);
                string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);
                //
                if (fileName.Length > 1)
                {
                    fileName = sortfilesbydate(fileName);
                }
                //
                string Ticket = ViewState["vTicketId"].ToString();

                int empid = Convert.ToInt32(txtempid.Text); //
                int TicketID = (int)Convert.ChangeType(ViewState["vTicketId"].ToString().ToString(), typeof(int));

                string bType = ViewState["ScanDocType"].ToString().Trim();
                int BookId = 0, picID = 0;
                if (txtbID.Text.Length > 0)
                {
                    BookId = Convert.ToInt32(txtbID.Text);
                }
                string picName, picDestination;

                int DocCount = Convert.ToInt32(ViewState["sSDDocCount"].ToString());
                for (int i = 0; i < fileName.Length; i++)
                {
                    string[] key = { "@updated", "@extension", "@Description", "@DocType", "@Employee", "@TicketID", "@Count", "@Book", "@BookID" };
                    object[] value1 = { DateTime.Now, "JPG", ViewState["sSDDesc"].ToString().ToUpper(), bType, empid, TicketID, DocCount, BookId, "" };
                    //call sP and get the book ID back from sP
                    picName = ClsDb.InsertBySPArrRet("usp_Add_ScanImage", key, value1).ToString();
                    string BookI = picName.Split('-')[0];
                    string picI = picName.Split('-')[1];
                    BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                    picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                    if (Adf.Checked == false)
                    {
                        txtbID.Text = BookId.ToString();
                    }
                    DocCount = DocCount + 1;
                    ViewState["sSDDocCount"] = Convert.ToString(DocCount);

                    //Move file
                    //picDestination = Server.MapPath("images/") + picName + ".jpg"; //DestinationImage
                    picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage

                    System.IO.File.Copy(fileName[i].ToString(), picDestination);

                    System.IO.File.Delete(fileName[i].ToString());
                }
                if (Adf.Checked)
                {

                    txtbID.Text = "";
                    txtDescription.Text = "";
                    //Adf.Checked = false;
                }
                BindGrid(ViewState["vTicketId"].ToString());

                //
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;
using System.Configuration;
using lntechNew.WebControls;

namespace lntechNew.PaperLess
{
	/// <summary>
	/// Summary description for UploadFile.
	/// </summary>
	public partial class UploadFile : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtsessionid;
		protected System.Web.UI.WebControls.TextBox txtempid;
		protected System.Web.UI.HtmlControls.HtmlInputFile File1;
		protected System.Web.UI.WebControls.Button CmdUpload;
		protected System.Web.UI.WebControls.TextBox txtDesc;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.DropDownList cmbDocType;
		clsENationWebComponents ClssDB=new clsENationWebComponents();
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		clsSession ClsSession = new clsSession();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				// Put user code to initialize the page here
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx");
				}
				else //To stop page further execution
				{
					//int tID = Convert.ToInt32(ClsSession.GetCookie("sTicketID" ,this.Request));
					ViewState["vTicketID"]=Request.QueryString["casenumber"];
					ViewState["vSearch"]=Request.QueryString["search"];
					int tID = Convert.ToInt32(ViewState["vTicketID"]);
					if (tID==0)
					{
						Response.Redirect("../ClientInfo/ViolationFeeold.aspx?newt=1");
					}
					if(!IsPostBack)
					{
						CmdUpload.Attributes.Add("OnClick","return Check();");
						//txtempid.Text=Session["sEmpID"].ToString();
						txtempid.Text = ClsSession.GetCookie("sEmpID",this.Request);
						txtsessionid.Text=Session.SessionID;
                        //network path
                        ViewState["vNTPATHScanTemp"] = ConfigurationSettings.AppSettings["NTPATHScanTemp"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage"].ToString();
                        //
						BindControls();

					}
				}
                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");
                txt1.Text = ViewState["vTicketID"].ToString();
                TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                txt2.Text = ViewState["vSearch"].ToString();
			}
			catch{}
		}
		void BindControls() 
		{
			try
			{
				cmbDocType.Items.Clear();
				cmbDocType.Items.Add(new ListItem("Select" ,"-1"));					
				DataSet ds=ClssDB.Get_DS_BySP("usp_Get_All_DocType");
				for(int i=1;i<ds.Tables[0].Rows.Count;i++)
				{
					string Description=ds.Tables[0].Rows[i]["DocType"].ToString().Trim();
					string ID=ds.Tables[0].Rows[i]["DocTypeID"].ToString().Trim();
					cmbDocType.Items.Add(new ListItem(Description ,ID));					
				}
				
			}
			catch{}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.CmdUpload.Click += new System.EventHandler(this.CmdUpload_Click);
			this.LinkButton1.Click += new System.EventHandler(this.LinkButton1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void CmdUpload_Click(object sender, System.EventArgs e)
		{
			try
			{
				//Event Handler for the upload button
			
				if (File1.PostedFile !=null) //Checking for valid file
				{		
					// Since the PostedFile.FileNameFileName gives the entire path we use Substring function to rip of the filename alone.
					string StrFileName = File1.PostedFile.FileName.Substring(File1.PostedFile.FileName.LastIndexOf("\\") + 1) ;
					string StrFileType = File1.PostedFile.ContentType ;
					int IntFileSize =File1.PostedFile.ContentLength;
					//Checking for the length of the file. If length is 0 then file is not uploaded.
					if (IntFileSize <=0)
						Response.Write(" <font color='Red' size='2'>Uploading of file " + StrFileName + " failed </font>");
					else
					{
						string sid= txtsessionid.Text;
						string eid= txtempid.Text;
						string StrFileName1=sid+eid+StrFileName;
						//File1.PostedFile.SaveAs(Server.MapPath(".\\tempimages\\" + StrFileName1));
                        File1.PostedFile.SaveAs(ViewState["vNTPATHScanTemp"].ToString() + StrFileName1);
						//Response.Write( "<font color='green' size='2'>Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully</font>");
						lblMessage.Text="Your file " + StrFileName + " of type " + StrFileType + " and size " + IntFileSize.ToString() + " bytes was uploaded successfully";

						//Session["ScanDocType"] = cmbDocType.SelectedItem.Text;
						ViewState["ScanDocType"] = cmbDocType.SelectedItem.Text;
						//Session["BookID"]= 0;
						ViewState["BookID"]= 0;
						//Session["DocCount"]= 0;
						ViewState["DocCount"]= 0;
				
						
						//Perform database Uploading Task
				
						//string searchpat="*"+ Session.SessionID + Session["sEmpID"].ToString() +"*.*";
						string searchpat="*"+ Session.SessionID + eid +"*.*";
						//string[] fileName=Directory.GetFiles(Server.MapPath("tempimages/"),searchpat);	
                        string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);	
					
						//string Ticket = Session["sTicketID"].ToString();
						//int empid= (int) Convert.ChangeType(Session["sEmpID"].ToString() ,typeof(int)); //
						//int TicketID = (int) Convert.ChangeType(Session["sTicketID"].ToString() ,typeof(int));
						string Ticket = ViewState["vTicketID"].ToString();//ClsSession.GetCookie("sTicketID",this.Request);
						int empid=  Convert.ToInt32(eid);
						int TicketID = Convert.ToInt32(Ticket);
						//string bType=Session["ScanDocType"].ToString().Trim(); 
						string bType=ViewState["ScanDocType"].ToString().Trim(); 
						int BookId = 0,picID=0;
						string picName,picDestination;
						int DocCount=1;
						for(int i=0;i<fileName.Length;i++)
						{
							string exten=fileName[i].Substring(fileName[i].LastIndexOf(".")+1);
							string[] key  = {"@updated","@extension","@Description", "@DocType","@Employee","@TicketID","@Count","@Book","@BookID"};
							object[] value1 = {DateTime.Now,exten.ToUpper(),"(Uploaded File) "+ txtDesc.Text.Trim().ToUpper(),bType,empid,TicketID,DocCount,BookId,""};
							//call sP and get the book ID back from sP
							picName = ClssDB.InsertBySPArrRet("usp_Add_ScanImage",key,value1).ToString() ;  
							string BookI = picName.Split('-')[0]; 
							string picI = picName.Split('-')[1]; 
							BookId =(int) Convert.ChangeType(BookI  ,typeof(int));; //
							picID =(int) Convert.ChangeType(picI  ,typeof(int));; //
							//Move file
							//picDestination= Server.MapPath("images/")+ picName + "." + exten; //DestinationImage
                            picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + "." + exten; //DestinationImage
				
							System.IO.File.Copy(fileName[i].ToString(),picDestination);

							System.IO.File.Delete(fileName[i].ToString());	
						}
						txtDesc.Text="";
						cmbDocType.ClearSelection();
						Response.Redirect("ScanDocument.aspx?casenumber="+ViewState["vTicketID"].ToString()+"&search="+ViewState["vSearch"].ToString(),false);
					}
				}
			
								
			}
			catch (Exception ex) 
			{ 
				lblMessage.Text = ex.Message; 
			}
		}

		private void LinkButton1_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("ScanDocument.aspx?casenumber="+ViewState["vTicketID"].ToString()+"&search="+ViewState["vSearch"].ToString(),false);
		}
	}
}

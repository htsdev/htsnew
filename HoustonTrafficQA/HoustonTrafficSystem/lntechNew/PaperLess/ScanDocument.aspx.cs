using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo ;
using lntechNew.Components;
using lntechNew.WebControls;

namespace lntechNew.PaperLess
{
	/// <summary>
	/// Summary description for ScanDocument.
	/// </summary>
	public partial class ScanDocument : System.Web.UI.Page
	{
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		//clsWeb objDS1 = new clsWeb(); 
		//clsDB.structResult structResult; 
		protected System.Web.UI.WebControls.DropDownList cmbDocType;
		protected System.Web.UI.WebControls.DataGrid dgrdDoc;
		protected System.Web.UI.WebControls.Button btnScan;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnScn;
		
		clsSession ClsSession = new clsSession();
		//added by ozair
		
		
		protected System.Web.UI.WebControls.DropDownList cmbDocType1;
		
		protected System.Web.UI.WebControls.TextBox txtImageCount;
		protected System.Web.UI.WebControls.TextBox txtQuery;
		protected string objTwain;
		protected System.Web.UI.WebControls.TextBox txtDescription;
		protected System.Web.UI.WebControls.TextBox txtShow;
		protected System.Web.UI.WebControls.TextBox txtempid;
		protected System.Web.UI.WebControls.TextBox txtsessionid;
		protected System.Web.UI.HtmlControls.HtmlInputButton btncancel;
		protected System.Web.UI.WebControls.TextBox txtcmb;
		protected System.Web.UI.WebControls.TextBox txtSrv;
		protected System.Web.UI.HtmlControls.HtmlInputCheckBox Adf;
		protected System.Web.UI.WebControls.TextBox txtbID;
		protected System.Web.UI.WebControls.Button btnUpload;
		//protected lntechNew.WebControls.ActiveMenu ActiveMenu1;
		protected string Qstr;
		//
		private void Page_Load(object sender, System.EventArgs e)
		{
			try 
			{ 
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx");
				}
				else //To stop page further execution
				{
				
					if (!(Page.IsPostBack)) 
					{
                        if (Request.QueryString.Count>=2)
						{
                            ViewState["vTicketId"] = Request.QueryString["casenumber"];
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
						else
						{
							Response.Redirect("../frmMain.aspx",false);
                            goto SearchPage;
                        }
						//ViewState["vTicketId"] = ClsSession.GetCookie("sTicketID",this.Request);

						int tID = Convert.ToInt32(ViewState["vTicketId"]);
						if (tID==0)
						{
							Response.Redirect("../ClientInfo/ViolationFeeold.aspx?newt=1");
						}

						btnScan.Attributes.Add("OnClick","return ShowHide()");
						cmbDocType1.Attributes.Add("OnClick","return onSelectionChanged()");
						cmbDocType.Attributes.Add("OnClick","return onSelectionChanged()");
						//btnUpload.Attributes.Add("OnClick","return openUpload()");
						string strServer;
						strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
						txtSrv.Text = strServer;
						//Session["objTwain"]= "<OBJECT id='VSTwain1' codeBase='" + strServer + "/lntechNew/VintaSoft.Twain.dll#version=1,3,1,1' height='1' width='1' classid='" + strServer + "/lntechnew/VintaSoft.Twain.dll#VintaSoft.Twain.VSTwain' VIEWASTEXT> </OBJECT>"; 
						//
						//	Session["objTwain"]= "<OBJECT id='VSTwain1' codeBase='" + strServer + "/VintaSoft.Twain.dll#version=1,6,1,1' height='1' width='1' classid='" + strServer + "/lntechnew/VintaSoft.Twain.dll#VintaSoft.Twain.VSTwain' VIEWASTEXT> </OBJECT>"; 
					
						//Session["EmpID"]= Convert.ToInt32(ClsSession.GetCookie("sEmpID",this.Request));//(int)Convert.ChangeType(Request.Cookies["employeeid"].Value,typeof(int));
						//Session["TicketID"] = Convert.ToInt32(Session["sTicketID"].ToString());//Request.Cookies["casenumber"].Value; //(int) Convert.ChangeType(Request.Cookies["casenumber"],typeof(int));
						//Session["TicketID"] = tID;
						
						BindControls(); 
						txtsessionid.Text=Session.SessionID.ToString();
						//txtempid.Text=Session["EmpID"].ToString();
						txtempid.Text=ClsSession.GetCookie("sEmpID",this.Request);
					} 
					//Session["ScanDocType"] = cmbDocType.SelectedItem.Text; //Enable on live
					//cmbDocType1.SelectedIndex = cmbDocType1.Items.IndexOf(cmbDocType1.Items.FindByValue(cmbDocType.SelectedValue));
				
				
				
					BindGrid(ViewState["vTicketId"].ToString() );
                    
				}
                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");
                txt1.Text = ViewState["vTicketId"].ToString();
                TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                txt2.Text = ViewState["vSearch"].ToString();
                SearchPage:
                { }
			}
			catch (Exception ex) 
			{ 
				lblMessage.Text = ex.Message; 
			}
		}


		//object BindControls() 
		void BindControls() 
		{
			try
			{
				//ClsDb.FetchValuesInWebControlBysp(cmbDocType,"usp_Get_All_DocType","DocType","DocTypeID");
				cmbDocType.Items.Clear();
				//cmbDocType.Items.Add(new ListItem("Select","-1"));
				DataSet ds=ClsDb.Get_DS_BySP("usp_Get_All_DocType");
				for(int i=0;i<ds.Tables[0].Rows.Count;i++)
				{
					string Description=ds.Tables[0].Rows[i]["DocType"].ToString().Trim();
					string ID=ds.Tables[0].Rows[i]["DocTypeID"].ToString().Trim();
					cmbDocType.Items.Add(new ListItem(Description ,ID));
					if(i==0)
					{
						cmbDocType1.Items.Add(new ListItem("Select","-1"));
					}
					else
					{
						cmbDocType1.Items.Add(new ListItem(Description ,ID));
					}
					
				}
				
			}
			catch{}
		}

		void BindGrid(string Criteria) 
		{ 
			int intTotalRows; 
			DataTable  dtlTemp1; 
			DataSet ds;
		
			try 
			{ 
				string[] key    = {"@TicketID","@DocType"};
				if(txtcmb.Text!="-1" && txtcmb.Text!="1" && txtcmb.Text!="0")
				{
					object[] value1 = {Criteria,cmbDocType1.SelectedItem.Text};
					ds = ClsDb.Get_DS_BySPArr("usp_Get_All_ScanDocuments_New_Ver_1",key,value1);
				}
				else
				{
					object[] value1 = {Criteria,cmbDocType.SelectedItem.Text};
					ds = ClsDb.Get_DS_BySPArr("usp_Get_All_ScanDocuments_New_Ver_1",key,value1);
				}
				
				
				dgrdDoc.Visible=true;
				if (ds.Tables[0].Rows.Count > 0) 
				{ 
			
					dtlTemp1 = ds.Tables[0];
					
					DataView dtvtemp1 = new DataView(dtlTemp1); 
					if (dtlTemp1.Rows.Count > 0) 
					{ 
						intTotalRows = dtlTemp1.Rows.Count; 
						
						dgrdDoc.DataSource = dtvtemp1; 
						dgrdDoc.DataBind(); 
						lblMessage.Text = ""; 
						/*if (intTotalRows < dgrdDoc.PageSize) 
						{ 
							dgrdDoc.PagerStyle.Visible = false; 
						} 
						else 
						{ 
							dgrdDoc.PagerStyle.Visible = true; 
						} */
						BindLinks(); 
						} 
					else 
					{ 
						lblMessage.Text = "No record(s) found"; 
						ClsDb.InitGrid(dgrdDoc, "ScanDocView", 4); 
						
						DisableLinks(); 
				
						dgrdDoc.PagerStyle.Visible = false; 
					} 
				} 
				else //If sP not return any data
				{ 
					lblMessage.Text = "No record(s) found"; 
					dgrdDoc.Visible=false;
					dgrdDoc.PagerStyle.Visible = false; 
				}
			} 
			catch (Exception ex) 
			{ 
				lblMessage.Text = ex.Message + ex.Source + ex.StackTrace; 
			} 
		}

//
		public void DoGetQueryString(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{ 
			
			string  lngDocID; 
			string  lngRecordType;
			string  strURL; 
			string lngDocNum;
            string lngDocExt;
			try 
			{ 
				
				if (e.CommandName == "DoGetScanDocx") 
				{ 
					lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text); 
					lngDocNum = (((Label)(e.Item.FindControl("lblDocNum"))).Text); 
					lngRecordType = (((Label)(e.Item.FindControl("lblRecType"))).Text); 
					//strURL = "PreviewPDF.aspx?DocID=" + lngDocID + "&RecType=" + lngRecordType ; 
					strURL = "PreviewPDF.aspx?DocID=" + lngDocID + "&RecType=" + lngRecordType  + "&DocNum=" + lngDocNum; 

					Response.Redirect(strURL); 
				}
				if(e.CommandName=="Delete")
				{
					try
					{
                        lngDocID = ((Label)(e.Item.FindControl("lblDocID"))).Text;
                        lngDocNum = ((Label)(e.Item.FindControl("lblDocNum"))).Text;
                        lngRecordType = ((Label)(e.Item.FindControl("lblRecType"))).Text;
                        lngDocExt = ((Label)(e.Item.FindControl("lblDocExtension"))).Text; 
                        int picid=Convert.ToInt32(((Label)(e.Item.FindControl("lbldid"))).Text);
						string[] key    = {"@docid","@RecType","@DocNum","@DocExt", "@empid"};
                        object[] value1 = { Convert.ToInt32(lngDocID), Convert.ToInt32(lngRecordType), Convert.ToInt32(lngDocNum), lngDocExt, Convert.ToInt32(txtempid.Text) };
						ClsDb.ExecuteSP("usp_HTS_Delete_ScanDoc",key,value1);
						/*
                        // Deleting Files From HDD
						string searchpat=lngDocID + "*.*";
						string[] fileName=Directory.GetFiles(Server.MapPath("images/"),searchpat);	
						for(int i=0;i<fileName.Length;i++)
						{
							System.IO.File.Delete(fileName[i].ToString());	
						}
						//
                        */
						BindGrid(ViewState["vTicketId"].ToString());
					}
					catch(Exception)
					{
						lblMessage.Text="Operation not completed successfully: Plz Perform this action again.";
					}
				}
				if(e.CommandName=="OCR")
				{
					try
					{										  
						lngDocID = (((Label)(e.Item.FindControl("lblDocID"))).Text); 
						string did=(((Label)(e.Item.FindControl("lbldid"))).Text); 
						string picDestination;
						string[] key3={"@sbookid"};
						object [] value3={Convert.ToInt32(lngDocID)};
						DataSet ds_bid=ClsDb.Get_DS_BySPArr("usp_hts_get_totalpicsbybookid",key3,value3);
						for(int i=0;i<ds_bid.Tables[0].Rows.Count;i++)
						{
							did=ds_bid.Tables[0].Rows[i]["docid"].ToString();
							string picName=lngDocID+"-"+did;
							picDestination= Server.MapPath("images/")+ picName + ".jpg"  ; //DestinationImage
							clsGeneralMethods  CGeneral = new clsGeneralMethods();
							string ocr_data = CGeneral.OcrIt(picDestination);
							string[] key2  = {"@doc_id","@docnum_2","@data_3"};
							object[] value2 = {Convert.ToInt32(lngDocID),Convert.ToInt32(did),ocr_data};
							//call sP and get the book ID back from sP
							ClsDb.InsertBySPArr("usp_scan_insert_tblscandata",key2,value2); 
						}
						BindGrid(ViewState["vTicketId"].ToString() ); 
					}
					catch(Exception)
					{
						lblMessage.Text="Operation not completed successfully: Plz Perform this action again.";
					}
				}
				if(e.CommandName=="View")
				{
					ViewState["sdoc_id"]=(((Label)(e.Item.FindControl("lblDocID"))).Text);
					ViewState["sdocnum"]=(((Label)(e.Item.FindControl("lbldid"))).Text); 
					ViewState["sdoctypedesc"]=(((Label)(e.Item.FindControl("lblDocType"))).Text);
					ViewState["sdocdesc"]=(((Label)(e.Item.FindControl("lblDocDescription"))).Text);
					Response.Redirect("ViewOCR.aspx?search="+ ViewState["vSearch"].ToString() +"&casenumber="+ ViewState["vTicketId"].ToString() +"&sdoc_id="+ ViewState["sdoc_id"].ToString() +"&sdocnum="+ ViewState["sdocnum"].ToString() +"&sdoctypedesc="+ ViewState["sdoctypedesc"].ToString() +"&sdocdesc="+ViewState["sdocdesc"].ToString());
				}
				
			} 
			catch (Exception ex) 
			{ 
				lblMessage.Text = ex.Message; 
			} 
		}

		
		void DisableLinks() 
		{ 
			try
			{
				foreach (DataGridItem dgItem in dgrdDoc.Items ) 
				{ 
					((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Visible = false; 
				}
			}
			catch{}
		}


		void BindLinks() 
		{ 	
			long lngDocID; 
			long RecType;
			long lngDocNum;
			string DocExt;
			try 
			{ 
				foreach (DataGridItem dgItem in dgrdDoc.Items) 
				{ 
					lngDocID = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocID"))).Text); 
					lngDocNum = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblDocNum"))).Text); 
					RecType = System.Convert.ToInt64(((Label)(dgItem.FindControl("lblRecType"))).Text); 
					DocExt= System.Convert.ToString(((Label)(dgItem.FindControl("lblDocExtension"))).Text); 
					//(ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "return PopUpShowPreviewPDF(" + lngDocID.ToString()+ "," + RecType  + ");"); 
					((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + lngDocID.ToString()+ "," + RecType  + "," + lngDocNum.ToString() + ",'" + DocExt + "'"+ ");"); 
					//((ImageButton)(dgItem.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", "return PopUpShowPreviewPDF(" + lngDocID.ToString()+ "," + RecType  + "," + lngDocNum.ToString() + ");"); 
					
					//if (ClsSession.GetSessionVariable("sAccessType",this.Session).ToString()!="2")
					{
						if(((Label)dgItem.FindControl("lblDocType")).Text.Trim()=="Resets")
						{
							((LinkButton) dgItem.FindControl("lnkbtn_Delete")).Visible=false;
							((LinkButton) dgItem.FindControl("lnkbtn_OCR")).Visible=false;
							((LinkButton) dgItem.FindControl("lnkbtn_VOCR")).Visible=false;
						}
					}

					DateTime recDate=Convert.ToDateTime(((Label) dgItem.FindControl("lblDateTime")).Text);
					DateTime CurDate=DateTime.Now;
					int a=CurDate.Day-recDate.Day; 
					if(a > 1)
					{
						((LinkButton) dgItem.FindControl("lnkbtn_Delete")).Visible=false;
					}
					else
					{
						((LinkButton) dgItem.FindControl("lnkbtn_Delete")).Attributes.Add("OnClick","return DeleteCheck();");
					}
					
					if(((Label) dgItem.FindControl("lblocrid")).Text!="0")
					{
						((LinkButton) dgItem.FindControl("lnkbtn_OCR")).Visible=false;
						((LinkButton) dgItem.FindControl("lnkbtn_VOCR")).Visible=true;
					}
					if(((Label) dgItem.FindControl("lblDocExtension")).Text!="000" && ((Label) dgItem.FindControl("lblDocExtension")).Text!="JPG")
					{
						((LinkButton) dgItem.FindControl("lnkbtn_OCR")).Visible=false;
						((LinkButton) dgItem.FindControl("lnkbtn_VOCR")).Visible=false;
					}
				} 
			} 
			catch (Exception ex) 
			{ 
				lblMessage.Text = ex.Message; 
			} 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
			this.btnScn.ServerClick += new System.EventHandler(this.btnScn_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	

		/*private void btnScan_Click(object sender, System.EventArgs e)
		{
			try
			{
				txtShow.Text="1";
				cmbDocType1.Items.Add(cmbDocType.SelectedItem.Text);
				Session["ScanDocType"] = cmbDocType.SelectedItem.Text;
				//cmbDocType1.Enabled = false; 
				txtcmb.Text=cmbDocType.SelectedValue;
			}
			catch{}
		}*/
		
		
		private void btnScn_ServerClick(object sender, System.EventArgs e)
		{
			try
			{
				txtShow.Text="1";
				//Session["ScanDocType"] = cmbDocType1.SelectedItem.Text;
				ViewState["ScanDocType"] = cmbDocType1.SelectedItem.Text;
				txtcmb.Text=cmbDocType1.SelectedValue;
				
				//Session["BookID"]= 0;
				ViewState["BookID"]= 0;
				//Session["DocCount"]= 0;
				ViewState["DocCount"]= 0;
									
				if(txtbID.Text.Length>0)
				{
					//Session["sSDDocCount"]=Session["sSDDocCount"].ToString();
					ViewState["sSDDocCount"]=Session["sSDDocCount"].ToString();
				}
				else
				{
					//Session["sSDDocCount"]=1;
					ViewState["sSDDocCount"]=1;
				}
				//Session["sSDDesc"]=txtDescription.Text.Trim();
				ViewState["sSDDesc"]=txtDescription.Text.Trim();
				
				//Perform database Uploading Task
				
				//string searchpat="*"+ Session.SessionID + Session["EmpID"].ToString() +"img.jpg";
				string searchpat="*"+ Session.SessionID + txtempid.Text +"img.jpg";
				string[] fileName=Directory.GetFiles(Server.MapPath("tempimages/"),searchpat);	
				//
				if(fileName.Length>1)
				{
					fileName=sortfilesbydate(fileName);
				}
				//
				string Ticket = ViewState["vTicketId"].ToString();
				//int empid= (int) Convert.ChangeType(Session["EmpID"].ToString() ,typeof(int)); //
				int empid= Convert.ToInt32(txtempid.Text); //
				int TicketID = (int) Convert.ChangeType(ViewState["vTicketId"].ToString().ToString() ,typeof(int));
				//string bType=Session["ScanDocType"].ToString().Trim(); 
				string bType=ViewState["ScanDocType"].ToString().Trim(); 
				int BookId = 0,picID=0;
				if(txtbID.Text.Length>0)
				{
					BookId=Convert.ToInt32(txtbID.Text);
				}
				string picName,picDestination;
				//int DocCount=Convert.ToInt32(Session["sSDDocCount"].ToString());
				int DocCount=Convert.ToInt32(ViewState["sSDDocCount"].ToString());
				for(int i=0;i<fileName.Length;i++)
				{
					string[] key  = {"@updated","@extension","@Description", "@DocType","@Employee","@TicketID","@Count","@Book","@BookID"};
					//object[] value1 = {DateTime.Now,"JPG",Session["sSDDesc"].ToString().ToUpper(),bType,empid,TicketID,DocCount,BookId,""};
					object[] value1 = {DateTime.Now,"JPG",ViewState["sSDDesc"].ToString().ToUpper(),bType,empid,TicketID,DocCount,BookId,""};
					//call sP and get the book ID back from sP
					picName = ClsDb.InsertBySPArrRet("usp_Add_ScanImage",key,value1).ToString() ;  
					string BookI = picName.Split('-')[0]; 
					string picI = picName.Split('-')[1]; 
					BookId =(int) Convert.ChangeType(BookI  ,typeof(int));; //
					picID =(int) Convert.ChangeType(picI  ,typeof(int));; //
					if(Adf.Checked==false)
					{
						txtbID.Text=BookId.ToString();
					}
					DocCount=DocCount+1;
					//Session["sSDDocCount"]=Convert.ToString(DocCount);
					ViewState["sSDDocCount"]=Convert.ToString(DocCount);

					//Move file
					picDestination= Server.MapPath("images/")+ picName + ".jpg"  ; //DestinationImage
				
					//System.IO.File.Move(fileName[i].ToString(),picDestination);
					System.IO.File.Copy(fileName[i].ToString(),picDestination);

					System.IO.File.Delete(fileName[i].ToString());	
				}
				if(Adf.Checked)
				{
					txtcmb.Text="1";
					txtbID.Text="";
					cmbDocType.SelectedIndex=0;
					txtDescription.Text="";
					Adf.Checked=false;
				}			
				BindGrid(ViewState["vTicketId"].ToString());
				
				//
			}
			catch (Exception ex) 
			{ 
				lblMessage.Text = ex.Message; 
			} 
			
		}

		private void btnUpload_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UploadFile.aspx?casenumber="+ ViewState["vTicketId"].ToString()+"&search="+ViewState["vSearch"].ToString());
		}

		private string[] sortfilesbydate(string[] fileName)
		{
			for(int i=0;i<fileName.Length;i++)
			{
				DateTime cDateTime= File.GetCreationTime(fileName[i]);				
				for(int j=i+1;j<fileName.Length;j++)
				{
					DateTime cDateTime1=File.GetCreationTime(fileName[j]);
					if(DateTime.Compare(cDateTime1,cDateTime)<0)
                    {
						string fname=fileName[j];
						fileName[j]=fileName[i];
						fileName[i]=fname;
						i=-1;
						break;
					}
				}
			}
			return fileName;
		}
	}
}

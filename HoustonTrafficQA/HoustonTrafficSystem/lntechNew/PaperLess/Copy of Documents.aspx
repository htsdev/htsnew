<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Documents.aspx.cs" Inherits="lntechNew.PaperLess.Documents" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Assembly="Microsoft.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI" TagPrefix="asp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Documents</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript">
    
    
    
    function checkGComments()
		{
		    //General Comments Check
			if (document.getElementById("txtGeneralComments").value.length > 2000-27) //-27 bcoz to show last time partconcatenated with comments
			{
				alert("You Cannot type more than 2000 characters ");
				document.getElementById("txtGeneralComments").focus();
				return false;
			}
		}
        function OpenPopUpNew(path)  //(var path,var name)
		{
		    window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		    return false;
		}
        
        function HideNShow()
        {
            var ddlOperation=document.getElementById("ddlOperation").value;
            
            var tdfile=document.getElementById("fpupload");
            var tdfile1=document.getElementById("fpupload1");
            var tdScan=document.getElementById("Scan");
            var tdsubmit=document.getElementById("submit");
            var ddldocType=document.getElementById("ddlDocType").value;
            
            if(ddlOperation=="3")
            {
                document.getElementById("nonDoc").style.display='none';
                document.getElementById("GeDoc").style.display='block';
            }
            else 
            {
                document.getElementById("nonDoc").style.display='block';
                document.getElementById("GeDoc").style.display='none';
                if(ddlOperation== "2")
                {
                
                    tdfile.style.display='block';
                    tdfile1.style.display='block';
                    tdScan.style.display='none'   ;
                    tdsubmit.style.display='block';    
                    
                }
                else
                {
                
                    tdfile.style.display='none';   
                    tdfile1.style.display='none';   
                    tdScan.style.display='block'   ;
                    tdsubmit.style.display='none';    
                }
            }
            
        }
        function check()
        {
            var ddldocType=document.getElementById("ddlDocType").value;
            var ddlOperation=document.getElementById("ddlOperation").value;
            
            var tdfile=document.getElementById("fpupload");
            
            if(tdfile.style.display=="block")
            {
                    if(ddldocType=="1")
                    {
                        alert("Please Select the Document Type");
                        document.getElementById("ddlDocType").focus();
                        return false;
                    }
            }
           
        }
        function hide()
        {
            
            var tdfile=document.getElementById("fpupload");
            var tdfile1=document.getElementById("fpupload1");
            var tdsubmit=document.getElementById("submit");
            
            tdfile.style.display='none';
            tdfile1.style.display='none';
            tdsubmit.style.display='none';
        }
        function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				return false;
			}
			function DeleteCheck()
			{
				var x=confirm("Are you sure you want to delete this document.[OK] Yes [Cancel] No");
				if(x)
				{
				}
				else
				{
					return false;
				}
			}
			
		function PrintLetter( flag)
		{   
		    //var flag=document.getElementById("ddlLetterType").value;
	//a;
		    if(flag== "-1")
		    {
		        alert("PLease Select Letter Type!");
		        document.getElementById("ddlLetterType").focus();
		        return false;
		    }
		  /*if(flag=="1")
		  {
			//alert(flag)			
			OpenPopUp("../ClientInfo/frmCaseSummary.aspx?casenumber=<%=ViewState["vTicketId"]%>&search=<%=ViewState["vSearch"]%>");	
			return false;	
		  }*/
		  //for EmailTrial leter..
		  if(flag == "9")
		  { 
		    var retBatch=false; 
		    
		   OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
		   return false;
				
		  }
		  if(flag=="2")
		  {	 
		  //var retBatch=false;
		  var retBatch=false;
		  var option = confirm("Trial Noticiation Letter generated. Would you like to 'PRINT NOW' or 'SEND TO BATCH'?\n Click [OK] to print now or click [CANCEL] to send to batch."); 
		  if (option  )
		    retBatch =false;
     	  else
		    retBatch="";
		  var courtid = document.getElementById("lbl_court").innerText;
	            var flg_split = document.getElementById("lbl_IsSplit").innerText;
	            var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerText;
		  OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&CourtID="+courtid+"&flg_split="+flg_split+"&flg_print="+flg_print );	
		  
		   return false;
		  }						
			if(flag=="3")
		    {	
			
				window.open("../Reports/word_receipt.aspx?casenumber=<%=ViewState["vTicketId"]%>",'',"left=150,top=50,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");	
				

			
			return false;	
			}		
			if(flag=="4")
			{	
			var courtid=document.getElementById("lbl_courtid").innerText;
				if (courtid==3001 ||courtid==3002 || courtid==3003)
				{
				  OpenPopUp("../Reports/Word_Bond.aspx?CourtType=0" + "&casenumber=<%=ViewState["vTicketId"]%>");	
				}
				else
				{
				  OpenPopUp("../Reports/Word_Bond.aspx?CourtType=1" + "&casenumber=<%=ViewState["vTicketId"]%>");	
				}		
			return false;	
			}			
			if(flag=="5")
		    {	 
				OpenPopUp("../Reports/FrmMainCONTINUANCE.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>");	
				
			return false;	
			}
			

			if(flag=="6")
		    {	 
				//OpenPopUp("../Reports/FrmMainLetterofRep.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>");	
				  OpenPopUp("../Reports/FrmMainLetterofRep.aspx?Batch=" + retBatch + "&lettertype=6&casenumber=<%=ViewState["vTicketId"]%>");					    
				  
			return false;	
			}
			
			if(flag=="8")
		    {				    		    
			OpenPopUp("../../reports/Payment_Receipt_Contract.asp?TicketID=<%=ViewState["vTicketid"]%>");	
			return false;	
			}
			  
		}
		function OpenPopUp(path)  //(var path,var name)
		{
		
		 window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		 
		}
    </script>

</head>
<body onload="hide();">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        </aspnew:ScriptManager>
        <div>
            <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
                <tbody>
                    <tr>
                        <td colspan="5">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="height: 33px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="80%">
                                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="Label" Width="32px"></asp:Label>,
                                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                                    <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>)
                                                    <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink><a href="#"></a></td>
                                                <td align="right" width="14%">
                                                </td>
                                                <td align="right" width="6%">
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="lblMessage" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            ForeColor="Red"></asp:Label></td>
                                </tr>
                                <!-- next section starts -->
                                <tr style="font-family: Tahoma">
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" height="34">
                                                    &nbsp; Scan/Upload</td>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" height="34" align="right">
                                                    <asp:DropDownList ID="ddlOperation" runat="server" CssClass="clsinputcombo">
                                                        <asp:ListItem Selected="True" Value="0">Scan-Feeder</asp:ListItem>
                                                        <asp:ListItem Value="1">Scan-Flatbed</asp:ListItem>
                                                        <asp:ListItem Value="2">Upload</asp:ListItem>
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Tahoma">
                                    <td colspan="2" valign="top">
                                        <table id="tbl_contact" border="0" cellpadding="0" cellspacing="1" width="100%">
                                            <tr>
                                                <td valign="top" width="50%" id="nonDoc">
                                                    <table border="0" cellpadding="0" cellspacing="1" width="100%">
                                                        <tr>
                                                            <td class="clsLeftPaddingTable" style="height: 20px">
                                                                &nbsp;Document Type</td>
                                                            <td id="sdt" runat="server" class="clsLeftPaddingTable" style="height: 20px;">
                                                                Sub Type</td>
                                                            <td class="clsLeftPaddingTable" style="height: 20px">
                                                                Description</td>
                                                            <td class="clsLeftPaddingTable" style="height: 20px" id="fpupload1">
                                                            </td>
                                                            <td class="clsLeftPaddingTable" style="height: 20px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="clsLeftPaddingTable" height="20">
                                                                &nbsp;<asp:DropDownList ID="ddlDocType" runat="server" CssClass="clsinputcombo" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                                </asp:DropDownList></td>
                                                            <td id="sdt1" runat="server" class="clsLeftPaddingTable" height="20">
                                                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" EnableViewState="true">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddl_SubDocType" runat="server" CssClass="clsinputcombo">
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <aspnew:AsyncPostBackTrigger ControlID="ddlDocType" EventName="SelectedIndexChanged" />
                                                                    </Triggers>
                                                                </aspnew:UpdatePanel>
                                                            </td>
                                                            <td id="txtdescrip" class="clsLeftPaddingTable">
                                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="clsInputcommentsfield"
                                                                    Width="150px" MaxLength="100">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="clsLeftPaddingTable" id="fpupload">
                                                                <asp:FileUpload ID="FPUpload" runat="server" CssClass="clsInputcommentsfield" Width="200px" />
                                                            </td>
                                                            <td id="Scan" class="clsLeftPaddingTable">
                                                                <asp:Button ID="btnScan" runat="server" CssClass="clsbutton" Text="Scan" OnClick="btnScan_Click"
                                                                    Width="60px" /></td>
                                                            <td class="clsLeftPaddingTable" id="submit">
                                                                <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSubmit_Click" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" id="GeDoc" style="display: none">
                                                    <table border="0" cellpadding="0" cellspacing="1" width="100%">
                                                        <tr>
                                                            <td class="clsLeftPaddingTable" height="20">
                                                                &nbsp;<asp:DropDownList ID="ddlLetterType" runat="server" CssClass="clsinputcombo">
                                                                </asp:DropDownList></td>
                                                            <td class="clsLeftPaddingTable" align="right">
                                                                &nbsp;
                                                                <asp:Button ID="btnGenerateDocs" runat="server" Text="Generate Documents" CssClass="clsbutton"
                                                                    OnClick="btnGenerateDocs_Click" />&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Tahoma">
                                    <td  colspan="6">
                                        <table width="100%" cellpadding="0" cellspacing="0" style="left: 0px; position: relative;
                                            top: 0px">
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" height="34">
                                                    &nbsp; Generate Documents
                                                </td>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" height="34" align="right">
                                                </td>
                                            </tr>
                                        </table>
                                       
                                        <table border="0" cellpadding="0" cellspacing="1" width="100%" style="left: 1px;
                                            position: relative; top: 0px">
                                            <tr>
                                                <td class="clsLeftPaddingTable" id="Td7" style="height: 13px">
                                                    <asp:DataList ID="dl_generatedocs" runat="server" RepeatDirection="Horizontal" Style="position: relative"
                                                        Width="100%" OnItemDataBound="dl_generatedocs_ItemDataBound">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lb_docs" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.value") %>'
                                                                OnClick="btnGenerateDocs_Click" Style="position: relative; left: 0px;" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                                OnCommand="lb_docs_Command"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:DataList></td>
                                            </tr>
                                        </table>
                                       
                                    </td>
                                </tr>
                                <!-- next section starts -->
                                <tr style="font-family: Tahoma">
                                    <td background="../../images/headbar_headerextend.gif" colspan="2" style="height: 5px">
                                    </td>
                                </tr>
                                <tr style="font-family: Tahoma">
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" height="34">
                                        &nbsp; Document History&nbsp;</td>
                                </tr>
                                <tr style="font-family: Tahoma">
                                    <td colspan="2" valign="top">
                                        <table id="tbl_comments" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" width="50%">
                                                    <table id="tbl_comm_1" border="0" cellpadding="0" cellspacing="1" width="100%">
                                                        <tr class="clsLeftPaddingTable">
                                                            <td height="20">
                                                                <asp:DataGrid ID="dgrdDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Font-Names="Verdana" Font-Size="2px" PageSize="20" Width="100%" OnItemCommand="dgrdDoc_ItemCommand">
                                                                    <FooterStyle CssClass="GrdFooter" />
                                                                    <PagerStyle CssClass="GrdFooter" Font-Names="Arial" HorizontalAlign="Center" Mode="NumericPages"
                                                                        NextPageText="" PageButtonCount="50" PrevPageText="" />
                                                                    <AlternatingItemStyle BackColor="#EEEEEE" />
                                                                    <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="Date">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDateTime" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATEDATETIME") %>'
                                                                                    Font-Size="Smaller"></asp:Label>&nbsp; &nbsp;
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" Width="160px" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Event">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEvent" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Events") %>'
                                                                                    Font-Size="Smaller"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Document Type">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDocID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>'
                                                                                    Visible="False" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblDocType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOCREF") %>'
                                                                                    Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lbldid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>'
                                                                                    Visible="False" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblocrid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>'
                                                                                    Visible="False" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblDocNum" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>'
                                                                                    Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Document Description">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDocDescription" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ResetDesc") %>'
                                                                                    Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblDocExtension" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>'
                                                                                    Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Rep">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAbb1" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'
                                                                                    Font-Size="Smaller"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="View">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif"
                                                                                    Width="19" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Pages / Docs">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDocCount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ImageCount") %>'
                                                                                    Width="32px" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblRecType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>'
                                                                                    Visible="False" Width="10px" Font-Size="Smaller"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Options">
                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkbtn_Delete" runat="server" CommandName="Delete" Font-Size="Smaller">Delete,</asp:LinkButton>&nbsp;
                                                                                <asp:LinkButton ID="lnkbtn_OCR" runat="server" CommandName="OCR" Font-Size="Smaller">OCR</asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbtn_VOCR" runat="server" CommandName="View" Visible="False"
                                                                                    Font-Size="Smaller">View OCR</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller" />
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- next section starts -->
                                <tr>
                                    <td colspan="2">
                                        <table id="FlagsE" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="3" height="34">
                                                    &nbsp;General Comments&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table id="tbl_flagdetail" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr class="clsLeftPaddingTable">
                                                <td height="20">
                                                    <asp:TextBox ID="txtGeneralComments" runat="server" CssClass="clsInputcommentsfield"
                                                        Width="790px" TextMode="MultiLine" Height="100px" MaxLength="2000"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" rowspan="1" style="height: 20px">
                                        <asp:Button ID="btnUpdate" OnClientClick="return checkGComments();" runat="server"
                                            CssClass="clsbutton" Text="Update" Width="60px" OnClick="btnUpdate_Click" />&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <uc1:Footer ID="Footer1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="visibility: hidden" colspan="2">
                                        <asp:TextBox ID="txtImageCount" runat="server"></asp:TextBox><asp:TextBox ID="txtQuery"
                                            runat="server"></asp:TextBox><asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox
                                                ID="txtsessionid" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtbID" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox>
                                        <asp:Label ID="lbl_courtid" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_IsSplit" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_court" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>

    <script language="JavaScript">
function StartScan()
{
     a;  
	if(document.getElementById("ddlDocType").value=="1" || document.getElementById("ddlDocType").value=="1")
	{
		alert("Plese Select valid document type to scan");
		document.getElementById("ddlDocType").focus();
		return false;
	}
    var ddlOperation=document.getElementById("ddlOperation").value;
    var sid= document.getElementById("txtsessionid").value;
	var eid= document.getElementById("txtempid").value;
	var path='<%=ViewState["vNTPATHScanTemp"]%>';
	var Type='network';		
	var AutoFeed=1;
	var sel=OZTwain1.SelectScanner();
	if(sel=="Success")
	{
	    if (ddlOperation == "0")
	    {
	        AutoFeed=-1;
	    }	    
	    
	    OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
	    //
	    if (ddlOperation == "1")
	      {
		    var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		    if(x)
		    {
			    //OZTwain1.Acquire(sid,eid,path,Type);
			    StartScan();
		    }	
		    else
		    {
		        document.getElementById("txtbID").value="";
        	    //document.Form1.Adf.checked = true;
		    }		
	      }
    }
    else if(sel=="Cancel")
    {
        alert("Operation canceled by user!");
        return false;
    }
    else 
    {
        alert("Scanner not installed.");
	    return false;
    }
    
}
    </script>

</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;

namespace test1
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Button Button1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button2.Click += new System.EventHandler(this.Button2_Click);
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Button3.Click += new System.EventHandler(this.Button3_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Button1_Click(object sender, System.EventArgs e)
		{

			DataSet ds=new DataSet();
			SqlConnection con=new SqlConnection("Server=server;uid=traffic;pwd=tickets;database=traffictickets");
			SqlCommand cmd=new SqlCommand("usp_hts_getcaseinfo",con);
			cmd.CommandType=CommandType.StoredProcedure;
			cmd.Parameters.Add(11579);

			con.Open();
            //Change By Ajmal

			IDataReader dr=cmd.ExecuteReader();
			DataGrid1.DataSource=dr;
			DataGrid1.DataBind();
			con.Close();			
			
		}

		private void Button2_Click(object sender, System.EventArgs e)
		{
            //using(SqlConnection con=new SqlConnection("Server=server;uid=traffic;pwd=tickets;database=traffictickets"))
            //{	
            //    SqlCommand cmd=new SqlCommand("usp_hts_getcaseinfo",con);
            //    cmd.CommandType=CommandType.StoredProcedure;
            //    cmd.Parameters.Add("@ticketid",11579);
            //    con.Open();
            //    //Change By Ajmal
            //    IDataReader dr=cmd.ExecuteReader();
            //    DataGrid1.DataSource=dr;
            //    DataGrid1.DataBind();

				
            //}
		}

		private void Button3_Click(object sender, System.EventArgs e)
		{
			clsENationWebComponents clsdb=new clsENationWebComponents();
			DataGrid1.DataSource= clsdb.Get_DS_BySPByOneParmameter("usp_hts_getcaseinfo","@ticketid",11579);
			DataGrid1.DataBind();			
		}
	}
}

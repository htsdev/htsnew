<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.IDScreen.CauseNumberReport" Codebehind="CauseNumberReport.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CauseNumberReport</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="HEIGHT: 27px" colSpan="2"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD colSpan="2"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 35px" colSpan="2">
						<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR background="../images/separator_repeat.gif">
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="tblSearchCriteria" cellSpacing="1" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" width="17%"><font color="#3366cc"><STRONG>Court Date From:</STRONG></font>
								</TD>
								<TD class="clsLeftPaddingTable"><ew:calendarpopup id="cal_todate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
								<TD class="clsLeftPaddingTable"><font color="#3366cc"><STRONG>To:</STRONG></font>
								</TD>
								<TD class="clsleftpaddingtable"><ew:calendarpopup id="cal_fromDate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
								<td class="clsleftpaddingtable"><font color="#3366cc"><strong>Court Location :</strong></font>
								<td>
								<td class="clsleftpaddingtable"><asp:dropdownlist id="ddl_CourtLocation" runat="server" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
										CssClass="clinputcombo">
										<asp:ListItem Value="0">All</asp:ListItem>
										<asp:ListItem Value="1">HMC</asp:ListItem>
										<asp:ListItem Value="2">Outside Court</asp:ListItem>
									</asp:dropdownlist></td>
								<TD class="clsleftpaddingtable"><asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Search"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD background="../Images/subhead_bg.gif" height ="34px" ><IMG height="34" src="../Images/subhead_bg.gif" style="width: 1px"></TD>
				</TR>
				<tr>
					<td class="clsleftpaddingtable" align="right"><asp:label id="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" Font-Bold="True"
							ForeColor="#3366cc" Height="8px" Font-Names="Tahoma">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Width="9px" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
							Height="10px" Font-Names="Tahoma">a</asp:label>&nbsp;<asp:label id="lblGoto" runat="server" Width="16px" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
							Height="7px" Font-Names="Tahoma">Goto</asp:label>&nbsp;
						<asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
							CssClass="clinputcombo" AutoPostBack="True"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<TR>
					<td align="center"><asp:label id="lbl_Msg" runat="server" ForeColor="Red" CssClass="normalfont"></asp:label></td>
				<tr>
					<td></td>
				</tr>
				<TR>
					<TD id="grid" vAlign="top" colSpan="2"><asp:datagrid id="DG_CauseReport" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
							PageSize="20" AllowPaging="True" ShowFooter="True">
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<FooterStyle CssClass="GrdFooter"></FooterStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Sno">
									<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id="hp_sno" runat="server"></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Cause No">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id=HLCauseno runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "casenumassignedbycourt") %>' Height="17px" Target="_self">
										</asp:HyperLink>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtcauseno runat="server" Width="115px" CssClass="TextBox" Text='<%# DataBinder.Eval(Container.DataItem, "casenumassignedbycourt") %>' MaxLength="15"></asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Ticket No">
									<HeaderStyle HorizontalAlign="Left" Width="19%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id=HLTicketno runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumber") %>' Height="17px" Target="_self">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="Fullname" HeaderText="Name">
									<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead" Width="25%"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label6 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Fullname") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="D.O.B">
									<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>' NAME="Label1">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Mid">
									<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label5 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Midnum") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="Description" HeaderText="Description">
									<HeaderStyle HorizontalAlign="Left" Width="35%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Court">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtLoc") %>' cssclass="label">
										</asp:Label>
										<asp:HyperLink id=hlkFulnm runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>' Visible="False">
										</asp:HyperLink>
										<asp:HyperLink id=hlkPhone runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumber") %>' Visible="False">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="BondFlag" HeaderText="Bond">
									<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lbl_BondFlag runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' NAME="Label1">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="TicketID_PK">
									<ItemTemplate>
										<asp:HyperLink id=HlkTktKey runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>'>
										</asp:HyperLink>
										<asp:HyperLink id=HLkTicketID runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Courtid">
									<ItemTemplate>
										<asp:Label id=lbl_courtid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Courtid") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:EditCommandColumn UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
							</Columns>
							<PagerStyle NextPageText=" Next &amp;gt;" Font-Size="XX-Small" PrevPageText="&amp;lt; Previous "
								HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colSpan="2">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR background="../images/separator_repeat.gif">
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

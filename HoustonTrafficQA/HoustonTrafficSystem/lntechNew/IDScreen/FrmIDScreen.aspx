<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>


<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.IDScreen.FrmIDScreen" Codebehind="FrmIDScreen.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ID Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 792px">
			<tbody>
				<tr>
					<td style="HEIGHT: 14px; width: 793px;" >
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
				</tr>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				
				<TR>
					<TD style="HEIGHT: 25px; width: 793px;" align="center" >
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0" style="width: 100%">
							<TR>
								<TD style="WIDTH: 92px" align="right"><asp:label id="label_fname" runat="server" CssClass="label" Font-Bold="True" Height="16px"
										Width="52px">F Name</asp:label>&nbsp;
								</TD>
								<TD style="WIDTH: 109px"><asp:textbox id="txt_fname" runat="server" CssClass="TextBox" Width="90px"></asp:textbox></TD>
								<TD style="WIDTH: 103px" align="right"><asp:label id="Label_LName" runat="server" CssClass="label" Font-Bold="True" Height="16px"
										Width="52px">L Name</asp:label>&nbsp;
								</TD>
								<TD style="WIDTH: 109px"><asp:textbox id="txt_lname" runat="server" CssClass="TextBox" Width="90px"></asp:textbox></TD>
								<TD style="WIDTH: 87px" align="right"><asp:label id="Label_TicketNo" runat="server" CssClass="label" Font-Bold="True" Height="16px"
										Width="64px">Ticket No</asp:label>&nbsp;
								</TD>
								<TD style="WIDTH: 114px"><asp:textbox id="txt_ticketno" runat="server" CssClass="TextBox" Width="90px"></asp:textbox></TD>
								
								<TD style="WIDTH: 87px" align="right"><asp:label id="Label3" runat="server" CssClass="label" Font-Bold="True" Height="16px"
										Width="64px">Cause No</asp:label>&nbsp;
								</TD>
								<TD style="WIDTH: 114px"><asp:textbox id="txt_causenum" runat="server" CssClass="TextBox" Width="90px"></asp:textbox></TD>
								
								<TD><asp:imagebutton id="Btn_search" runat="server" ImageUrl="../Images/search.jpg"></asp:imagebutton></TD>
								<TD><asp:imagebutton id="btn_Reset" runat="server" ImageUrl="../Images/reset.jpg"></asp:imagebutton></TD>
							</TR>
						</TABLE>
                        &nbsp;<asp:Label ID="lblerror" runat="server" CssClass="label" Font-Bold="True" ForeColor="#C00000"></asp:Label>
						<asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Display="Dynamic" ErrorMessage="Please Enter Tickets ID (ID123445)."
							ControlToValidate="txt_ticketno" ValidationExpression="ID.*|id.*"></asp:RegularExpressionValidator>
                        </TD>
				</TR>
				<TR>
					<td class="TDHeadingnew" style="HEIGHT: 22px; width: 793px;" >
						<table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="185" align="left"
							border="0">
							<tr>
								<td width="31" align="center"><img height="18" src="../Images/screen.jpg" width="17"></td>
								<td class="TDHeadingnew" vAlign="baseline" width="140"><font color="#666666">&nbsp;ID 
										Screen</font>
								</td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td style="HEIGHT: 1px; width: 793px;" background="../Images/separator_repeat.gif" ></td>
				</tr>
				<tr>
					<td align="center"  style="width: 793px"><asp:datagrid id="DG_IDScreen" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
							PageSize="20" AllowPaging="True" Font-Size="2px" Font-Names="Verdana" ShowFooter="True">
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<FooterStyle CssClass="GrdFooter"></FooterStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Sno">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblSno" runat="server" CssClass="GrdLbl"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Ticket No">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id=HLTicketno runat="server" Width="56px" Height="17px" CssClass="GrdHeader" ForeColor="Black" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumber") %>' Target="_self">
										</asp:HyperLink>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtTktNo runat="server" Width="95px" CssClass="TextBox" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumber") %>' MaxLength="15"></asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Cause No">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink id=HLcauseno runat="server" Width="56px" Height="17px" CssClass="GrdHeader" ForeColor="Black" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "casenumassignedbycourt") %>' Target="_self">
										</asp:HyperLink>										
                                    </ItemTemplate>     
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtcaseno" runat="server" Width="95px" CssClass="TextBox" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container.DataItem, "casenumassignedbycourt") %>' MaxLength="15"></asp:TextBox>
                                    </EditItemTemplate>                               
                                </asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="Fullname" HeaderText="Name">
									<HeaderStyle HorizontalAlign="Left" ForeColor="#006699"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label6 runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.Fullname") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="D.O.B">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Mid">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label5 runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Midnum") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="Description" HeaderText="Description">
									<HeaderStyle HorizontalAlign="Left" ForeColor="#006699"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 CssClass="GrdLbl" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>' Font-Size="XX-Small">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="CourtNumber" HeaderText="Underlying">
									<HeaderStyle HorizontalAlign="Left" ForeColor="#006699"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label2 runat="server" CssClass="GrdLbl" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.oldno") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Court">
									<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.CourtLoc") %>'>
										</asp:Label>
										<asp:HyperLink id=hlkFulnm runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>' Visible="False">
										</asp:HyperLink>
										<asp:HyperLink id=hlkPhone runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Phone") %>' Visible="False">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:EditCommandColumn UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
								<asp:TemplateColumn Visible="False" HeaderText="TicketID_PK">
									<ItemTemplate>
										<asp:HyperLink id=HlkTktKey runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>'>
										</asp:HyperLink>
										<asp:HyperLink id=HLkTicketID runat="server" Font-Size="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Font-Size="XX-Small" HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
									<td background="../../images/separator_repeat.gif" height="11"></td>
								</tr>
					<TR>
					<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
					</TR>
				</tbody>
			</table>
		</form>
	</body>
</HTML>

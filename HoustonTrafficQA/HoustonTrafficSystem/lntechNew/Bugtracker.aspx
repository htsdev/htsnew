<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Bugtracker" Codebehind="Bugtracker.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Bugtracker</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		var refreshinterval=60
		var displaycountdown="yes"
		var starttime
		var nowtime
		var reloadseconds=0
		var secondssinceloaded=0

		function starttime() {
			starttime=new Date()
			starttime=starttime.getTime()
			countdown()
		}

		function countdown() {
			nowtime= new Date()
			nowtime=nowtime.getTime()
			secondssinceloaded=(nowtime-starttime)/1000
			reloadseconds=Math.round(refreshinterval-secondssinceloaded)
			if (refreshinterval>=secondssinceloaded) {
				var timer=setTimeout("countdown()",1000)
				if (displaycountdown=="yes") {
					window.status="Page refreshing in "+reloadseconds+ " seconds"
				}
			}
			else {
				clearTimeout(timer)
				window.location.reload(true)
			} 
		}
		window.onload=starttime
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="780" align="center" border="1">
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:label id="lblCurrPage" runat="server" CssClass="cmdlinks" Width="83px" Font-Size="Smaller"
							ForeColor="#3366cc" Height="8px" Font-Bold="True">Current Page :</asp:label>
						<asp:label id="lblPNo" runat="server" CssClass="cmdlinks" Width="9px" Font-Size="Smaller" ForeColor="#3366cc"
							Height="10px" Font-Bold="True">a</asp:label>
						<asp:label id="lblGoto" runat="server" CssClass="cmdlinks" Width="16px" Font-Size="Smaller"
							ForeColor="#3366cc" Height="7px" Font-Bold="True">Goto</asp:label>
						<asp:dropdownlist id="cmbPageNo" runat="server" CssClass="clinputcombo" Font-Size="Smaller" ForeColor="#3366cc"
							Font-Bold="True" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="dg_bug" runat="server" Width="100%" AllowPaging="True" PageSize="20" AutoGenerateColumns="False">
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Bug ID.">
									<HeaderStyle HorizontalAlign="Center" Width="1%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lbl_bugid runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"eventid") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Date/Time">
									<HeaderStyle HorizontalAlign="Center" Width="9%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lbl_datetime runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Timestamp") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Message">
									<HeaderStyle HorizontalAlign="Center" Width="5%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lbl_message runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"message") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Source">
									<HeaderStyle HorizontalAlign="Center" Width="5%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lbl_source runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "source") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Target Site">
									<HeaderStyle HorizontalAlign="Center" Width="10%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lbl_targetsite runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"targetsite") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Stack Trace">
									<HeaderStyle HorizontalAlign="Center" Width="70%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id=lbl_statetrace runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.stacktrace") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " HorizontalAlign="Center"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

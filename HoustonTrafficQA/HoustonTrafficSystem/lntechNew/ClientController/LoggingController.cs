﻿using System.Collections.Generic;
using Lntech.Logging.MessageObjects;
using Lntech.Logging.Service.Client;
using System;

namespace HTP.ClientController
{
    // Noufil 7897 06/17/2010 Class added
    /// <summary>
    ///     This class serves as the bridge from where web application with communicate with the service methods.
    /// </summary>
    internal class LoggingController
    {
        /// <summary>
        ///     Returns all logs that exisit in our system with respect to searching critirea provided.
        /// </summary>
        /// <param name="loggingSearchCriteria">loggingSearchCriteria objec</param>
        /// <returns>list of LoggingReportDto</returns>
        internal List<LoggingReportDto> GetAllLogs(LoggingSearchCriteria loggingSearchCriteria)
        {
            // Create object of service client
            LoggerClient client = new LoggerClient();
            // Open client to communicate with service
            client.Open();
            // Delcare Request object
            LoggingRequest loggingRequest = new LoggingRequest { LoggingSearchCriteria = loggingSearchCriteria };
            // Declare repsone object to get all Logs by calling Service method
            LogginResponse logginResponse = client.GetLogs(loggingRequest);
            // Close client to end all communication
            client.Close();
            // Check if no validation error message occurs
            if (!string.IsNullOrEmpty(logginResponse.ValidationMessage))
                // Throw validation error message as application exception
                throw new ApplicationException(logginResponse.ValidationMessage);
            // Check if no Exception occurs
            else if (logginResponse.Exception != null)
                // Throw exception
                throw logginResponse.Exception;
            // return list of all logs
            return logginResponse.LoggingReportDto;
        }

        /// <summary>
        ///     Returns all available applications from where exception has been logged
        /// </summary>
        /// <param name="applicationDto">applicationDto with property 'ApplicationNameToSearch' having application name to search. Provide null to get all</param>
        /// <returns>List of ApplicationDto</returns>
        internal List<ApplicationDto> GetAllApplication(ApplicationDto applicationDto)
        {
            // Create object of service client
            LoggerClient client = new LoggerClient();
            // Open client to communicate with service
            client.Open();
            // Delcare Request object
            LoggingRequest loggingRequest = new LoggingRequest { ApplicationDto = applicationDto };
            // Declare repsone object to get all Logs by calling Service method
            LogginResponse logginResponse = client.GetAllApplications(loggingRequest);
            // Close client to end all communication
            client.Close();
            // Check if no validation error message occurs
            if (!string.IsNullOrEmpty(logginResponse.ValidationMessage))
                // Throw validation error message as application exception
                throw new ApplicationException(logginResponse.ValidationMessage);
            // Check if no Exception occurs
            else if (logginResponse.Exception != null)
                // Throw exception
                throw logginResponse.Exception;
            // return list of applications
            return logginResponse.ListApplicationDto;
        }
    }
}

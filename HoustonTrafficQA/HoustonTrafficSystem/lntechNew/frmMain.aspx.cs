using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.Configuration;
using HTP.Components.Services;


namespace HTP
{
    /// <summary>
    /// Summary description for frmMain.
    /// </summary>
    public partial class frmMain : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.DataGrid dgResult;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase ClsCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        DataSet ds_Result;
        DataView dv_Result;
        clsUser ccUser = new clsUser();
        OpenServiceTickets GetAlert = new OpenServiceTickets();

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        int mailerID = 0;
        
        //ozair 4625 08/27/2008 removed classic search option 
        #region Variables

        private static string Lidad = string.Empty;
        private static string Causenumberad = string.Empty;
        private static string SearchKeyWordad1 = String.Empty;
        private static int SearchKeyTypead1;
        private static string SearchKeyWordad2 = String.Empty;
        private static int SearchKeyTypead2;
        private static string SearchKeyWordad3 = String.Empty;
        private static int SearchKeyTypead3;
        private static int CourtId = 0;
        private static string TicketNo = String.Empty;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.Label Message;
        protected System.Web.UI.WebControls.LinkButton lnkbtn_AddNewTicket;
        
        
        private static int FilterType = 0;

        #endregion
        //ozair 4625 08/27/2008 removed classic search option      
        private void Page_Load(object sender, System.EventArgs e)
        {                   
            try
            {              

                // Check for valid Session if not then redirect to login page					
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("frmLogin.aspx", false);

                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {

                        if (ClsSession.GetCookie("SearchType", this.Request) != null)
                        {
                            dSearchCriteria2.Visible = true;                            
                        }
                        //page validation
                        btnSearchAdvance.Attributes.Add("OnClick", "return ValidateInputAdvance();");
                        
                        //Mid# Link Functionality			
                        //ozair 4625 08/26/2008 Mid number search from matter page advance search
                        if (Request.QueryString.Count == 3)
                        {
                            txtSearchKeyWord2ad.Text = Request.QueryString["lstcriteriaValue3"];
                            ddlSearchKeyType2ad.SelectedValue = Request.QueryString["lstcriteria3"];
                            int search = Convert.ToInt32(Request.QueryString["search"]);
                            if (search == 0 || search == 1)//client/quote
                            {   rdbtnQuotead.Checked = true;
                                rdbtnNonClientad.Checked = false;
                                rdbtnJimsad.Checked = false;
                                rdbtnCivilad.Checked = false;
                                rdbtnClientad.Checked = false;
                            }                            
                            else if (search == 2)//non client
                            {
                                rdbtnQuotead.Checked = false;
                                rdbtnNonClientad.Checked = true;
                                rdbtnJimsad.Checked = false;
                                rdbtnCivilad.Checked = false;
                                rdbtnClientad.Checked = false;
                            }                            
                            else if (search == 4)//jims
                            {
                                rdbtnQuotead.Checked = false;
                                rdbtnNonClientad.Checked = false;
                                rdbtnJimsad.Checked = true;
                                rdbtnCivilad.Checked = false;
                                rdbtnClientad.Checked = false;
                            }
                            else if (search == 5)//civil
                            {
                                rdbtnQuotead.Checked = false;
                                rdbtnNonClientad.Checked = false;
                                rdbtnJimsad.Checked = false;
                                rdbtnCivilad.Checked = true;
                                rdbtnClientad.Checked = false;
                            }
                            
                            SetAdvanceParameters();                            
                            GetAdvanceResult();
                        }
                        //end ozair 4625
                        ServiceTicketAlert();
                        FillCourts();
                        GetCaseType();
                        
                        //Waqas 5933 05/28/2009 deleting files from temp
                        if (Global.tempDeleteDate.CompareTo(Convert.ToDateTime(DateTime.Now.ToShortDateString())) < 0)
                        {
                            clsGeneralMethods GMethod = new clsGeneralMethods();
                            GMethod.DeleteTempFiles(@"C:\Temp\", "*.*");
                            Global.tempDeleteDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                        }
                    }
                }
                btnResetAdvance.Attributes.Add("OnClick", "return ResetControls();");                
            }
            catch (Exception ex)
            {
                //Kamran 3585 03/09/08 remove double error message
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        //ozair 4625 08/27/2008 removed classic search option      
        private void FillCourts()
        {
            try
            {
                DataSet dsCourts = ClsCase.GetCourtsForSearch();
                // advance search .....
                ddlCourtsAd.DataTextField = "courtname";
                ddlCourtsAd.DataValueField = "courtid";
                ddlCourtsAd.DataSource = dsCourts;
                ddlCourtsAd.AppendDataBoundItems = true;
                ddlCourtsAd.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        //ozair 4625 08/27/2008 removed classic search option      
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>        
        private void InitializeComponent()
        {
            this.lnkbtn_AddNewTicket.Click += new System.EventHandler(this.lnkbtn_AddNewTicket_Click);
            this.dgResult.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgResult_ItemCommand);
            this.dgResult.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgResult_SortCommand);
            this.dgResult.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgResult_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
        
        //ozair 4625 08/27/2008 removed classic search option      
        private void dgResult_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header)
                {

                }
                else
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
                    }

                    if (e.Item.ItemType == ListItemType.Item)
                    {
                        e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                    }
                    else
                    {
                        e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                    }

                }

                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv == null) return;

                string strCaseNumber = drv["CaseNumber"].ToString();
                
                //Status Fore Color
                Label Status = (Label)e.Item.FindControl("Label1");
                string strStatus = drv["CaseStatus"].ToString().Trim();
                switch (strStatus)
                {
                    case "Non Client":
                        Status.ForeColor = System.Drawing.Color.Black;
                        break;
                    case "Arraignment":
                        Status.ForeColor = System.Drawing.Color.Green;
                        break;
                    case "Jury Trial Setting":
                        Status.ForeColor = System.Drawing.Color.Green;
                        break;
                    case "Pre Trial":
                        Status.ForeColor = System.Drawing.Color.Green;
                        break;
                    case "Judge Trial":
                        Status.ForeColor = System.Drawing.Color.Green;
                        break;
                    case "PreTrial Setting":
                        Status.ForeColor = System.Drawing.Color.Green;
                        break;
                    case "Return":
                        Status.ForeColor = System.Drawing.Color.Green;
                        break;
                    case "Closed":
                        Status.ForeColor = System.Drawing.Color.Brown;
                        break;
                    case "Missed Court":
                        Status.ForeColor = System.Drawing.Color.Brown;
                        break;
                    case "DISPOSED":
                        Status.ForeColor = System.Drawing.Color.Purple;
                        break;
                    case "Quote":
                        Status.ForeColor = System.Drawing.Color.Blue;
                        break;
                    case "---Choose----":
                        Status.Text = "Choose";
                        Status.ForeColor = System.Drawing.Color.Orange;
                        break;
                    default:
                        Status.ForeColor = System.Drawing.Color.Orange;
                        break;
                }


                //Zeeshan 3709 05/15/2008 Display Matter In Tool Tip If Length Greatat Than 23 Charcters
                string Matter = e.Item.Cells[5].Text;

                if (Matter.Length > 23)
                {
                    e.Item.Cells[5].ToolTip = Matter;
                    e.Item.Cells[5].Text = Matter.Substring(0, 23);
                }

                //Muhammad Muneer 8455 10/22/2010 adding the new functionality for the Null tickets
                string TicketNumberN = (drv["casenumber"].ToString());
                if (TicketNumberN == "")
                {
                    e.Item.Cells[1].Text = "<a href=" + "ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=" + drv["ticketid"].ToString() + ">N/A</a>";
                }
               

                //Noufil 4369 07/17/2008 Grid Color Change according to Client type
                // Agha 4480 08/01/2008 - Change ActiveFlag to IsActive
                string activeflag = Convert.ToInt32(drv["isActive"]).ToString().Trim();
                if (activeflag == "1")
                {
                    // Noufil 4637 08/18/2008 mouse over color change
                    e.Item.BackColor = ColorTranslator.FromHtml("#A9F5A1");
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#ADDFFF'");
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#A9F5A1'");

                }
                else if (activeflag == "0")
                {

                    e.Item.BackColor = ColorTranslator.FromHtml("#EFF4FB");
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");
                }
                else
                {
                    e.Item.BackColor = ColorTranslator.FromHtml("#FFF2D9");
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#EFF4FB'");
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFF2D9'");
                }

            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void dgResult_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            //lblMessage.Text = "";
            Message.Text = "";
            SortGrid(e.SortExpression);
        }

        private void SortGrid(string SortExp)
        {
            try
            {

                if (ClsSession.GetSessionObject("dv_Result", this.Session) != null)
                {
                    SetAcsDesc(SortExp);
                    dv_Result = (DataView)(ClsSession.GetSessionObject("dv_Result", this.Session));
                    dv_Result.Sort = StrExp + " " + StrAcsDec;
                    dgResult.DataSource = dv_Result;
                    dgResult.DataBind();
                }
                else
                {
                    Response.Redirect("frmlogin.aspx", false);
                }

            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                //StrExp = Session["StrExp"].ToString();
                //StrAcsDec = Session["StrAcsDec"].ToString();
                StrExp = ViewState["StrExp"].ToString();
                StrAcsDec = ViewState["StrAcsDec"].ToString();

            }
            catch { }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    //Session["StrAcsDec"] = StrAcsDec ;
                    ViewState["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    //Session["StrAcsDec"] = StrAcsDec ;
                    ViewState["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                //Session["StrExp"] = StrExp ;
                //Session["StrAcsDec"] = StrAcsDec ;
                ViewState["StrExp"] = StrExp;
                ViewState["StrAcsDec"] = StrAcsDec;

            }
        }

        //ozair 4625 08/27/2008 removed classic search option      
        private void dgResult_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "CaseNo")
                {
                    string mailerIDFlag = ((HiddenField)e.Item.FindControl("hf_MailerIDFlag")).Value;
                    string strCaseNumber = ((LinkButton)e.Item.FindControl("lnkbtnCaseNo")).Text;
                    string ticketid = ((Label)e.Item.FindControl("lblTicketId")).Text;
                    string courtid = ((Label)e.Item.FindControl("lblCourtId")).Text;
                    string zipcode = ((Label)e.Item.FindControl("lblZip")).Text;
                    string Add1 = ((Label)e.Item.FindControl("lblAdd1")).Text;
                    string MidNum = ((HyperLink)e.Item.FindControl("HLMidNumber")).Text;
                    int dbid = Convert.ToInt32(((Label)(e.Item.FindControl("lbl_dbid"))).Text);

                    // Added by Zeeshan Ahmed **
                    // Fix bug of Advance Search

                    bool quoteclient, nonclient, archive, jims, civil;

                    quoteclient = rdbtnQuotead.Checked;
                    nonclient = rdbtnNonClientad.Checked;
                    archive = false;
                    jims = rdbtnJimsad.Checked;
                    civil = rdbtnCivilad.Checked;
                    // 25/06/08 4227 Added By Adil
                    if (jims == true && dbid == 2)
                    {
                        jims = false;
                        nonclient = true;
                    }

                    if (quoteclient)//Client/Quote
                    {    //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu                                                                                                                                                               
                        Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=" + ticketid, false);
                    }
                    else if (nonclient)//NonClient
                    {
                        if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                        {
                            if (strCaseNumber.IndexOf('-') != -1)// -1 means '-' is found in strCaseNumber
                            {
                                strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                            }

                            //
                            try
                            {
                                if (mailerIDFlag == "1")
                                    mailerID = Convert.ToInt32(Session["MailerID"]);
                                else
                                    mailerID = 0;

                            }
                            catch { mailerID = 0; }
                            
                            //===========================================================================
                            // EDITED BY TAHIR AHMED DT:6/27/07
                            // TO MERGE THE RESULTS OF NON-CLIENT & JIMS SEARCH
                            //===========================================================================
                            ////int dbid = Convert.ToInt32(((Label)(e.Item.FindControl("lbl_dbid"))).Text);
                            if (dbid == 2)
                            {
                                string[] ticketidFlag = new string[2] { "0", "-1" };

                                //Kazim 3938 6/10/2008 pass 0 for Non Traffic Client 

                                ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(999, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), Add1, zipcode, mailerID, MidNum, 0);
                                if (ticketidFlag[0] == "")
                                {//added by khalid bug 2545, 15-1-08
                                    ticketidFlag = new string[2] { "0", "-1" };
                                }
                                if (ticketidFlag[1] != "2")
                                {
                                    HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                    //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                                }
                                else
                                { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                    Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                                }
                            }
                            else if (dbid == 4)
                            {
                                if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                                {
                                    if (strCaseNumber.IndexOf('-') != -1) // -1 means '-' is found in strCaseNumber
                                    {
                                        strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                                    }
                                    //kazim 3938 6/10/2008 pass zero for non traffic clients 
                                    string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(3, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "", "", 0, "", 0);
                                    if (ticketidFlag[1] != "2")
                                    {
                                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                        //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                                    }
                                    else
                                    { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                        Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                                    }
                                    //
                                }
                            }
                            //===========================================================================
                            // END CHANGES....
                            //===========================================================================
                        }
                    }
                    else if (archive)//Archive
                    {
                        if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                        {
                            if (strCaseNumber.IndexOf('-') != -1)// -1 means '-' is found in strCaseNumber
                            {
                                strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                            }

                            //kazim 3938 6/10/2008 pass zero for non traffic client
                            string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(5, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), Add1, zipcode, 0, MidNum, 0);
                            if (ticketidFlag.Length == 2)
                            {
                                if (ticketidFlag[1] != "2")
                                {
                                    HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                    //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + ticketidFlag[0],false);
                                }
                                else
                                { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                    //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                                    Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                                }
                                //
                            }
                            else
                            { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                            }

                            //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=5&caseNumber=" + strCaseNumber,false);													
                        }
                    }
                    else if (jims)//Jims
                    {
                        if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                        {
                            if (strCaseNumber.IndexOf('-') != -1) // -1 means '-' is found in strCaseNumber
                            {
                                strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                            }
                            //
                            try
                            {
                                if (mailerIDFlag == "1")
                                    mailerID = Convert.ToInt32(Session["MailerID"]);
                                else
                                    mailerID = 0;

                            }
                            catch { mailerID = 0; }
                            //kazim 3938 6/10/2008 pass zero for non traffic clients
                            string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(3, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "", "", mailerID, "", 0);
                            if (ticketidFlag.Length == 2)
                            {
                                if (ticketidFlag[1] != "2")
                                {
                                    HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                    //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                                }
                                else
                                { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                    Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                                }
                            }
                            else
                            { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                            }
                            //
                        }
                    }
                    else if (civil)//Civil 
                    {
                        if (strCaseNumber.StartsWith("F") != true) // if caseNo starts with F (Failure to appear), has no link
                        {
                            if (strCaseNumber.IndexOf('-') != -1) // -1 means '-' is found in strCaseNumber
                            {
                                strCaseNumber = strCaseNumber.Substring(0, strCaseNumber.IndexOf('-'));
                            }
                            //
                            try
                            {
                                if (mailerIDFlag == "1")
                                    mailerID = Convert.ToInt32(Session["MailerID"]);
                                else
                                    mailerID = 0;

                            }
                            catch { mailerID = 0; }

                            //kazim 3938 6/10/2008 pass zero for non traffic clients
                            //Zeeshan Ahmed 4304 6/26/2008 Send Party Id In Mid Number Field To The Profile Migration Procedure
                            string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(6, strCaseNumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "", "", mailerID, MidNum, 0);
                            if (ticketidFlag.Length == 2)
                            {
                                if (ticketidFlag[1] != "2")
                                {
                                    HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists. Please search this case in CLIENT or QUOTE By Ticket Number or by Cause Number.'); void(0); </script>");
                                    //Response.Redirect("ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&caseNumber=" + ticketidFlag[0],false);
                                }
                                else
                                { //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                    Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                                }
                            }
                            else
                            {//Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                                Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&search=1&caseNumber=" + ticketidFlag[0], false);
                            }
                            //
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void lnkbtn_AddNewTicket_Click(object sender, System.EventArgs e)
        {//Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
            Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&newt=1&casenumber=0&search=-1", false);
        }

        protected void ActiveMenu1_Load(object sender, EventArgs e)
        {

        }

        //ozair 4625 08/27/2008 removed classic search option      
        private void ResetControls()
        {
            rdbtnQuotead.Checked = false;
            rdbtnNonClientad.Checked = false;
            rdbtnJimsad.Checked = false;
            rdbtnClientad.Checked = true;

            txtSearchKeyWord2ad.Text = "";
            txtSearchKeyWord1ad.Text = "";
            txtSearchKeyWord3ad.Text = "";
            ddlSearchKeyType2ad.SelectedIndex = 2;
            ddlSearchKeyType1ad.SelectedIndex = 3;
            ddlSearchKeyType3ad.SelectedIndex = 5;

            dgResult.Visible = false;

        }
        private void SetAdvanceParameters()
        {

            FilterType = 0;
            TicketNo = String.Empty;
            TicketNo = txtTicketNumberad.Text.Trim();
            Lidad = txtLidad.Text.Trim();
            Causenumberad = txtcausenumberad.Text.Trim();
            SearchKeyWordad1 = txtSearchKeyWord1ad.Text.Trim();
            SearchKeyTypead1 = Convert.ToInt32(ddlSearchKeyType1ad.SelectedValue);
            SearchKeyWordad2 = txtSearchKeyWord2ad.Text.Trim();
            SearchKeyTypead2 = Convert.ToInt32(ddlSearchKeyType2ad.SelectedValue);
            CourtId = Convert.ToInt32(ddlCourtsAd.SelectedValue);
            SearchKeyWordad3 = txtSearchKeyWord3ad.Text.Trim();
            SearchKeyTypead3 = Convert.ToInt32(ddlSearchKeyType3ad.SelectedValue);

            // tahir 4580 08/13/2008 
            // fixed the LID search bug related to profile migration 
            // with the letter credit to the search LID....
            //ozair 4625 08/23/2008 fixed lid search input string error
            if (Lidad.Length > 0)
            {
                mailerID = Convert.ToInt32(Lidad);
            }
            else if (SearchKeyTypead1 == 9)
            {
                if (SearchKeyWordad1.Length > 0)
                {
                    mailerID = Convert.ToInt32(SearchKeyWordad1);
                }
            }
            else if (SearchKeyTypead2 == 9)
            {
                if (SearchKeyWordad2.Length > 0)
                {
                    mailerID = Convert.ToInt32(SearchKeyWordad2);
                }
            }
            else if (SearchKeyTypead3 == 9)
            {
                if (SearchKeyWordad3.Length > 0)
                {
                    mailerID = Convert.ToInt32(SearchKeyWordad3);
                }
            }
            else
            {
                mailerID = 0;
            }
            //end ozair 4625

            Session["MailerID"] = Convert.ToString(mailerID);
            // end 4580

            //Zeeshan Ahmed 3979 04/29/2008 Add Civil Cases In Search
            //if (rdbtnClientad.Checked)
            //    FilterType = 0;

            if (rdbtnQuotead.Checked)
                FilterType = 0;

            if (rdbtnNonClientad.Checked)
                FilterType = 2;            

            if (rdbtnJimsad.Checked)
                FilterType = 4;

            if (rdbtnCivilad.Checked)
                FilterType = 5;

        }
        private void GetAdvanceResult()
        {
            try
            {
                // Noufil 3478 03/24/2008 dropdown control
                if (SearchKeyTypead2 == 1 && SearchKeyWordad2 == "")
                {
                    SearchKeyTypead2 = -1;
                }

                if (SearchKeyTypead1 == 2 && SearchKeyWordad1 == "")
                {
                    SearchKeyTypead1 = -1;
                }

                if (SearchKeyTypead3 == 4 && SearchKeyWordad3 == "")
                {
                    SearchKeyTypead3 = -1;
                }

                //////// Checking IF given Lettid ID range is greater than int
                if (SearchKeyTypead1 == 9 && SearchKeyWordad1.Length > 0)
                {
                    try
                    {
                        Int32.Parse(SearchKeyWordad1);
                    }
                    catch (Exception)
                    {
                        //lblMessage.Text = "The given LetterID is not correct. ";
                        Message.Text = "The given LetterID is not correct. ";
                        txtSearchKeyWord1ad.Focus();
                        return;
                    }
                }

                if (SearchKeyTypead2 == 9 && SearchKeyWordad2.Length > 0)
                {
                    try
                    {
                        Int32.Parse(SearchKeyWordad2);
                    }
                    catch (Exception)
                    {
                        //lblMessage.Text = "The given LetterID is not correct. ";
                        Message.Text = "The given LetterID is not correct. ";
                        txtSearchKeyWord2ad.Focus();
                        return;
                    }
                }
                if (SearchKeyTypead3 == 9 && SearchKeyWordad3.Length > 0)
                {
                    try
                    {
                        Int32.Parse(SearchKeyWordad3);
                    }
                    catch (Exception)
                    {
                        //lblMessage.Text = "The given LetterID is not correct. ";
                        Message.Text = "The given LetterID is not correct. ";
                        // txtSearchKeyWord3ad.Focus();
                        return;
                    }
                }

                ds_Result = ClsCase.SearchCaseInfo(TicketNo, SearchKeyWordad1, SearchKeyTypead1, SearchKeyWordad2, SearchKeyTypead2, Lidad, Causenumberad, FilterType, CourtId, SearchKeyWordad3, SearchKeyTypead3);

                if (ds_Result.Tables.Count > 0)
                {
                    //if sp return one or more rows
                    if (ds_Result.Tables[0].Rows.Count > 0)
                    {
                        // changed by tahir ahmed....
                        // to implement sorting on grid....
                        //dgResult.DataSource=ds_Result;

                        // getting data view from data set for sorting purpose...
                        dv_Result = ds_Result.Tables[0].DefaultView;

                        // storing data view in session....
                        ClsSession.SetSessionVariable("dv_Result", dv_Result, this.Session);

                        // binding grid with the data view....
                        dgResult.DataSource = dv_Result;
                        dgResult.DataBind();
                        dgResult.Visible = true;
                        //lblMessage.Text = "";
                        Message.Text = "";
                        lblResults.Visible = true;
                        tblColorCode.Visible = true;
                    }
                    else
                    {
                        //lblMessage.Text = "No record found.";
                        Message.Text = "No record found.";
                        dgResult.DataBind();
                        dgResult.Visible = false;
                        lblResults.Visible = false;
                        tblColorCode.Visible = false;
                    }
                }
                else
                {
                    //lblMessage.Text = "No record found.";
                    Message.Text = "No record found.";
                    dgResult.DataBind();
                    dgResult.Visible = false;
                    lblResults.Visible = false;
                    tblColorCode.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void ServiceTicketAlert()
        {
            try
            {

                string refurl = Convert.ToString(Request.UrlReferrer);


                if (refurl != "" && refurl.ToUpper().Contains("FRMLOGIN.ASPX") || (Convert.ToString(Session["fromLoginPage"]) == "1"))
                {
                    int cnt = 0;
                    DateTime str = DateTime.Today;


                    object[] value = { Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)) };

                    GetAlert.Values = value;

                    DataSet ds = GetAlert.GetServiceTicketAlerts();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        cnt = Convert.ToInt32(ds.Tables[0].Rows[0]["servicecount"]);
                        str = Convert.ToDateTime(ds.Tables[0].Rows[0]["serviceticketalertdate"]);
                    }

                    if ((cnt > 0) && (str.ToShortDateString() != DateTime.Now.ToShortDateString()))
                        Response.Write("<script language='javascript'> window.open('../Reports/ServiceTicketAlert.aspx','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no,height=120,width=475');</script>");
                    Session["fromLoginPage"] = "0";
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearchAdvance_Click(object sender, EventArgs e)
        {
            try
            {
                //lblMessage.Text = "";
                Message.Text = "";
                SetAdvanceParameters();
                GetAdvanceResult();
                dgResult.Visible = true;
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnResetAdvance_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmmain.aspx", false);
        }

        private void GetCaseType()
        {
            ddlCaseType.Items.Clear();
            ddlCaseType.DataSource = ClsCase.GetCaseType();
            ddlCaseType.DataBind();
            ddlCaseType.Items.Insert(0, new ListItem("---Choose---", "0"));

            //Muhammad Muneer 8178  21/08/2010  adding a new item in the case drop down 
            ddlCaseType.Items.Add(new ListItem("POLM", "POLM"));

            ddlCaseType.SelectedValue = "1";

        }

        protected void btnAddCase_Click(object sender, EventArgs e)
        {
            try
            {
                //Muhammad Muneer 8178 21/08/2010   checking for the POLM condition
                if (ddlCaseType.SelectedValue != "POLM")
                {
                    //Nasir 5902 06/11/2009 for business logic issue pass query string sMenu
                    Response.Redirect("ClientInfo/ViolationFeeold.aspx?sMenu=61&newt=1&casenumber=0&search=-1&casetype=" + ddlCaseType.SelectedValue, false);
                }                
                else
                {
                    ConfigurationKeys configurationKey = new ConfigurationKeys();
                    string webaddress = configurationKey.WebisteAddress;
                    webaddress = "http://" + webaddress + "/view/AddProspect.aspx";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "opnewin", "window.open('" + webaddress + "','null'); void(0);", true);

                }
            }
            catch (Exception ex)
            {               
                Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

    }
}

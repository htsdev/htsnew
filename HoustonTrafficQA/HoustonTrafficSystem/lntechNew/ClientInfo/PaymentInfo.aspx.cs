using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using System.IO;
using System.Configuration;
using WebSupergoo.ABCpdf5;

namespace lntechNew.ClientInfo
{
	/// <summary>
	/// Form For Procesing Payments
	/// </summary>
	public partial class PaymentInfo : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lbl_totalfee;
		protected System.Web.UI.WebControls.Label lbl_paid;
		protected System.Web.UI.WebControls.Label lbl_owes;
		protected System.Web.UI.WebControls.TextBox txt_amount;
		protected eWorld.UI.CalendarPopup date_payment;
		protected System.Web.UI.WebControls.DropDownList ddl_paymentmethod;
		protected System.Web.UI.WebControls.TextBox txt_name;
		protected System.Web.UI.WebControls.TextBox txt_cardno;
		protected System.Web.UI.WebControls.DropDownList ddl_cardtype;
		protected System.Web.UI.WebControls.TextBox txt_cin;
		protected System.Web.UI.WebControls.DropDownList ddl_expmonth;
		protected System.Web.UI.WebControls.DropDownList ddl_expyear;
		protected System.Web.UI.WebControls.Button btn_process;
		protected System.Web.UI.WebControls.RadioButtonList rbtn_trial;
		protected System.Web.UI.WebControls.Label lblMessage;
		int TicketID;
		clsPayments ClsPayments = new clsPayments();
		clsSession cSession = new clsSession();
		clsLogger	clog= new clsLogger();	
		protected System.Web.UI.WebControls.Label lbl_pretrialflag;
		protected System.Web.UI.WebControls.Button btn_emailrep;
		protected System.Web.UI.WebControls.Button btn_trialLetter;
		protected System.Web.UI.WebControls.Button btn_coversheet;
		protected System.Web.UI.WebControls.Button btn_Receipt;
		protected System.Web.UI.WebControls.Button btn_letterofRep;
		protected System.Web.UI.WebControls.Button btn_contuance;
		protected System.Web.UI.WebControls.Button btn_polm;
		protected System.Web.UI.WebControls.Button btn_update;
		protected System.Web.UI.WebControls.Label lbl_lockflag;
		protected System.Web.UI.WebControls.Label lbl_scheduleid;
		protected System.Web.UI.WebControls.DataGrid dg_creditcard;
		protected System.Web.UI.WebControls.Button btn_next;
		protected System.Web.UI.WebControls.Label lbl_trialbuttonflag;
		protected System.Web.UI.WebControls.Label lbl_courtid;
		protected System.Web.UI.WebControls.Button btn_bond;
		protected System.Web.UI.WebControls.HyperLink hlnk_MidNo;
		protected System.Web.UI.WebControls.Label lbl_FirstName;
		protected System.Web.UI.WebControls.Label lbl_LastName;
		protected System.Web.UI.WebControls.Label lbl_CaseCount;
		protected System.Web.UI.WebControls.DataGrid dg_PaymentDetail;
		protected System.Web.UI.WebControls.DataGrid dg_Schedule;
		protected System.Web.UI.WebControls.TextBox txt_paycomments;
		protected System.Web.UI.WebControls.TextBox txt_certified;
		protected System.Web.UI.WebControls.LinkButton lnkb_Viol;
		protected System.Web.UI.WebControls.TextBox txt_schedulesum;
		protected System.Web.UI.WebControls.Label lbl_contactcheck;
		protected System.Web.UI.WebControls.Label lbl_addresscheck;
		protected MagicAjax.UI.Controls.AjaxPanel Ajaxpanel3;
		protected System.Web.UI.WebControls.DataGrid dg_Print;
		protected System.Web.UI.WebControls.Label lbl_yds;
		protected System.Web.UI.WebControls.Label lbl_dlstate;
		protected System.Web.UI.WebControls.Label lbl_activeflag;
		protected System.Web.UI.WebControls.Label lbl_schcount;
		//protected lntechNew.WebControls.ActiveMenu ActiveMenu1;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.TextBox txtTimeStemp;
        private clsENationWebComponents cls_db = new clsENationWebComponents();
		int PretrialStatusFlag;	
		


	
		private void Page_Load(object sender, System.EventArgs e)
		{		
			try
			{	
			
				if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //to stop page further execution
				{
                    ViewState["vEmpID"] = cSession.GetCookie("sEmpID", this.Request);
                    //commented by ozair
					//cSession.SetSessionVariable("sMoved","True",this.Session);	
					//
					//Show Credit Detail When Payment Method is CreditCard
					ddl_paymentmethod.Attributes.Add("onclick","ShowHideCreditDetail();");								

					//In order to select card when cardnum is entered
					txt_cardno.Attributes.Add("onclick","SelectCard()");

					// Adding Attributes To Button for Opening Report
					//btn_coversheet.Attributes.Add("onclick"," return PrintLetter(" + 1 + ");");
					btn_trialLetter.Attributes.Add("onclick"," return PrintLetter(" + 2 + ");");
					btn_Receipt.Attributes.Add("onclick"," return PrintLetter(" + 3 + ");");
					btn_bond.Attributes.Add("onclick"," return PrintLetter(" + 4 + ");");			
					btn_contuance.Attributes.Add("onclick"," return PrintLetter(" + 5 + ");");			
					btn_letterofRep.Attributes.Add("onclick"," return PrintLetter(" + 6 + ");");
					btn_emailrep.Attributes.Add("onclick"," return PrintLetter(" + 8 + ");");
                    btn_EmailTrial.Attributes.Add("onclick", " return PrintLetter(" + 9 + ");");
                    btn_Limine.Attributes.Add("onclick", " return PrintLetter(" + 11 + ");");
                    btn_Discovery.Attributes.Add("onclick", " return PrintLetter(" + 12 + ");");
                    
					//btn_polm.Attributes.Add("onclick"," return PrintLetter(" + 9 + ");");			
			
					//Adding Attribute to process button
					btn_process.Attributes.Add("onclick","return ProcessAmount();");

					//Adding Attribute To Update Button
					btn_update.Attributes.Add("onclick"," return ValidateForm()");
					
					lblMessage.Text="";									

					txtTimeStemp.Text = System.DateTime.Now.TimeOfDay.ToString();
					if(!IsPostBack)
					{

                        if (Request.QueryString.Count >= 2)
                        {
                            ViewState["vTicketId"] = Request.QueryString["casenumber"];
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }
						
                        btn_EmailTrial.Visible = false;//code Added by Azee..
                        Session["TimeStemp"] = txtTimeStemp.Text;

						
						TicketID=Convert.ToInt32(ViewState["vTicketId"]);				
						ClsPayments.EmpID= Convert.ToInt32(ViewState["vEmpID"]);
						//	TicketID=11103;
						//	ClsPayments.EmpID=3992;
						if (TicketID==0)
						{
							Response.Redirect("ViolationFeeold.aspx?newt=1",false);
						}
                        //for oscar court
                        string[] key_1 ={ "@ticketid" };
                        object[] value_1 ={ TicketID };
                        DataSet ds_os = cls_db.Get_DS_BySPArr("usp_hts_check_activeflag", key_1, value_1);
                        if (ds_os.Tables[0].Rows.Count > 0)
                        {
                            lbl_OscarActiveFlag.Text = ds_os.Tables[0].Rows[0]["activeflag"].ToString();
                            lbl_OscarCourtID.Text = ds_os.Tables[0].Rows[0]["courtid"].ToString();
                        }
                        else
                        {
                            lbl_OscarActiveFlag.Text = "";
                            lbl_OscarCourtID.Text = "";
                        }
                        //
						ClsPayments.TicketID=TicketID;

						PretrialStatusFlag=0;
						date_payment.SelectedDate=DateTime.Now;
						//Payment Type Dropdown				
						DataSet ds_paymenttype= ClsPayments.GetPaymentType();
						ddl_paymentmethod.DataSource=ds_paymenttype;				
						ddl_paymentmethod.DataTextField=ds_paymenttype.Tables[0].Columns[1].ColumnName;
						ddl_paymentmethod.DataValueField=ds_paymenttype.Tables[0].Columns[0].ColumnName;
						ddl_paymentmethod.DataBind();

                        //Check for records in BatchPrint
                        IsAlreadyInBatchPrint();

                        //Check for split cases 
                        CheckSpilitCases();

						//CardType DropDown
						DataSet ds_cardtype=ClsPayments.GetCardType();				
						ddl_cardtype.DataSource=ds_cardtype;					
						ddl_cardtype.DataTextField =ds_cardtype.Tables[0].Columns[1].ColumnName;
						ddl_cardtype.DataValueField=ds_cardtype.Tables[0].Columns[0].ColumnName;
						ddl_cardtype.DataBind();				
						ddl_cardtype.Items.Insert(0,"--Choose--");

						//Populating year Combo
						int lbound=DateTime.Now.Date.Year;
						string lb=lbound.ToString();
						int ubound=DateTime.Now.AddYears(5).Date.Year;
						string ub=ubound.ToString();
						lb=lb.Substring(2);
						ub=ub.Substring(2);
			
						for(int i=Convert.ToInt32(lb);i<= Convert.ToInt32(ub);i++)
						{
							if(i<10)
							{
								//string year =""i.ToString()
								ddl_expyear.Items.Add(String.Format ("0{0}",i.ToString()));
							}
							else
							{
								ddl_expyear.Items.Add( i.ToString());
							}
						}
						//Method To Call Function(s)						
						GetPayments();	
						//Session["FromSchedule"]="";
						cSession.CreateCookie("FromSchedule","",this.Request, this.Response);
						//In order to redirect to frmMain when midnumber is clicked
						//string casetype=cSession.GetCookie("sSearch",this.Request);
						ViewState["vSearch"]=Request.QueryString["search"];
						string casetype=ViewState["vSearch"].ToString();

						switch (casetype)
						{
							case "0":
								hlnk_MidNo.NavigateUrl="../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
								break;
							case "1":
								hlnk_MidNo.NavigateUrl="../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
								break;							
						}

					}	
					//When coming from Schedule Window
					//Method To Call Function(s)										 
					//if (Session["FromSchedule"].ToString()=="yes")
					if (  cSession.GetCookie("FromSchedule",this.Request) == "yes")
					{
						//Session["FromSchedule"]="no";
						cSession.CreateCookie("FromSchedule","no",this.Request, this.Response );
						GetPayments();		
					}
					//In order to redirect to frmMain when midnumber is clicked
				}
				ActiveMenu am = (ActiveMenu) this.FindControl("ActiveMenu1");
				TextBox txt1 = (TextBox) am.FindControl("txtid");
				txt1.Text = ViewState["vTicketId"].ToString();
				TextBox txt2 = (TextBox) am.FindControl("txtsrch");
				txt2.Text = ViewState["vSearch"].ToString();
            SearchPage:
                { }
			}			

			catch(Exception e_pgload)
			{
				lblMessage.Text=e_pgload.Message;
				clog.ErrorLog(e_pgload.Message,e_pgload.Source,e_pgload.TargetSite.ToString(),e_pgload.StackTrace);			
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
			this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
			this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
			this.dg_PaymentDetail.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_PaymentDetail_EditCommand);
			this.dg_PaymentDetail.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_PaymentDetail_ItemDataBound);
			this.dg_Schedule.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Schedule_ItemCommand);
			this.dg_Schedule.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Schedule_DeleteCommand);
			this.dg_Schedule.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_Schedule_ItemDataBound);
			this.dg_Print.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_Print_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

        private void SendMail()
        {
            try
            {
                string toUser = (string)ConfigurationSettings.AppSettings["OsEmailTo"];
                string ccUser = (string)ConfigurationSettings.AppSettings["OsEmailCC"];
                lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.", "An Oscar client hired.", ViewState["vFullPath"].ToString(), toUser, ccUser);
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Email not sent";
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GeneratePDF(string path, string name)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                //theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }

            ViewState["vFullPath"] = Server.MapPath("\\PDF\\" + name);
            ViewState["vPath"] = Server.MapPath("\\PDF\\");
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), name);
            for (int i = 0; i < files.Length; i++)
            {
                System.IO.File.Delete(files[i]);
            }
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();
            //
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();
            string[] files1 = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
            for (int i = 0; i < files1.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files1[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);
                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files1[i]);
                }
            }
            //
        }
        private void IsAlreadyInBatchPrint()
        {
            lbl_court.Text = Request.QueryString["casenumber"].ToString();
            string[] keys = { "@TicketID" };
            object[] values = { Request.QueryString["casenumber"] };
            DataTable dtBatchPrint = new DataTable();
            dtBatchPrint = cls_db.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT",keys,values);
            if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                lbl_IsAlreadyInBatchPrint.Text = "1";
            else
                lbl_IsAlreadyInBatchPrint.Text = "0";
        }

        private void CheckSpilitCases()
        {
            string[] keys = { "@ticketid" };
            object[] values = { Request.QueryString["casenumber"] };
            DataTable dtSplit = new DataTable();
            dtSplit = cls_db.Get_DT_BySPArr("USP_HTS_PAYMENTINFO_GET_SPLITCaseCount", keys, values);

            if (Convert.ToInt32(dtSplit.Rows[0][0].ToString()) > 0)
                lbl_IsSplit.Text = "1";
            else
                lbl_IsSplit.Text = "0";
        }

		//Method to populate form calling all the sub functions
		private void GetPayments()
		{
			try
			{
				//int datetypeflag=0;	
                ClsPayments.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
                ClsPayments.EmpID = Convert.ToInt32(ViewState["vEmpID"]);
				if(ClsPayments.GetPaymentInfo(Convert.ToInt32(ViewState["vTicketId"])))
				{
                    //added by ozair for andrew's modification point 2 
                    lbl_ToProcess.Text  = ClsPayments.ToProcess.ToString();
                    //
                    lbl_totalfee.Text= ClsPayments.Totalfee.ToString();
					lbl_paid.Text=ClsPayments.ChargeAmount.ToString();
					lbl_owes.Text=ClsPayments.Owes.ToString();					
					lbl_pretrialflag.Text=Convert.ToString(ClsPayments.PreTrialStatus);
					txt_paycomments.Text=ClsPayments.PaymentsComments;
					txt_certified.Text=ClsPayments.CertifiedMail;
					PretrialStatusFlag=ClsPayments.PreTrialStatus;	
					//	datetypeflag=ClsPayments.MainStatus;
					//In order to select pretrial radio button
					if(PretrialStatusFlag!=0)
					{
						switch(PretrialStatusFlag)
						{
							case 1:
								rbtn_trial.SelectedValue="1";
								break;
							
							case 2:
								rbtn_trial.SelectedValue="2";
								break;
							case 3:
								rbtn_trial.SelectedValue="3";
								break;
							case 4:
								rbtn_trial.SelectedValue="4";
								break;
							case 5:
								rbtn_trial.SelectedValue="5";
								break;
						}
					}
				}
				else
				{					
					lblMessage.Text="Information Not Retrived";
				}
				//Filling Payments Grid
				DataSet ds_paymentdetail=ClsPayments.GetPaymentDetail(Convert.ToInt32(ViewState["vTicketId"]));
				dg_PaymentDetail.DataSource= ds_paymentdetail;
				dg_PaymentDetail.DataBind();
				//filling Schedule Grid
				DataSet ds_schedulepayment=ClsPayments.GetSchedulePaymentInfo(Convert.ToInt32(ViewState["vTicketId"]));
				dg_Schedule.DataSource= ds_schedulepayment;
				dg_Schedule.DataBind();  
				lbl_schcount.Text=dg_Schedule.Items.Count.ToString();
			
				//To get the sum of schedule amount
				if (dg_Schedule.Items.Count>0)
				{
					double sumamt=ScheduleAmountSum();	
					txt_schedulesum.Text=sumamt.ToString();
				}
				else
				{
					txt_schedulesum.Text="0";
				}

				clsCase ClsCase= new clsCase();		
				DataSet ds_case=ClsCase.GetCase(Convert.ToInt32(ViewState["vTicketId"]));
				if(ds_case.Tables[0].Rows.Count>0)
				{
					//Session["sEmail"]=ds_case.Tables[0].Rows[0]["email"].ToString().Trim(); //To send report to client
					cSession.CreateCookie("sEMail",  ds_case.Tables[0].Rows[0]["email"].ToString().Trim() ,this.Request, this.Response ); //To send report to client
				
					//When Schedule Payment is clicked
					double owes=Convert.ToDouble(lbl_owes.Text)-Convert.ToDouble(txt_schedulesum.Text);
					int schdid=0;
					lnkb_Viol.Attributes.Add("onclick","return OpenSchedule(" + owes + "," + schdid + "," +ds_case.Tables[0].Rows[0] ["ActiveFlag"]+  ");"); 	
					//When To Edit SchedulePayment
					EditSchedulePayment(owes);

					//Filling Credit Card Grid
					DataSet ds_creditcard=ClsPayments.GetCreditCardInfo();
					dg_creditcard.DataSource=ds_creditcard;
					dg_creditcard.DataBind();

					//Filling Batch Print Grid
					dg_Print.DataSource = ClsPayments.GetBatchPrintInfo();
					dg_Print.DataBind();
				
					//setting flags
				
					lbl_FirstName.Text=ds_case.Tables[0].Rows[0]["FirstName"].ToString().Trim();
					lbl_LastName.Text=ds_case.Tables[0].Rows[0]["Lastname"].ToString().Trim();
					hlnk_MidNo.Text=ds_case.Tables[0].Rows[0]["midnum"].ToString().Trim();
					lbl_CaseCount.Text=ds_case.Tables[0].Rows[0]["Casecount"].ToString().Trim();
					//Checking flags for displaying report buttons
					lbl_activeflag.Text=ds_case.Tables[0].Rows[0]["activeflag"].ToString();
                    //for oscar
                    lbl_OscarActiveFlag.Text = ds_case.Tables[0].Rows[0]["activeflag"].ToString();
                    //
					lbl_dlstate.Text=ds_case.Tables[0].Rows[0]["dlstate"].ToString();
					lbl_lockflag.Text=ds_case.Tables[0].Rows[0]["LockFlag"].ToString();
					lbl_addresscheck.Text=ds_case.Tables[0].Rows[0]["Addressconfirmflag"].ToString(); 				
					//In order to prompt for conflict
					//				lbl_trialbuttonflag.Text=ClsCase.CheckCourtStatus(TicketID).ToString();	
					//Checking if bondflag=1 then client must have three contact numbers
				
				
					//  Added By Zeeshan Jur
					lbl_yds.Text=ds_case.Tables[0].Rows[0]["YDS"].ToString();
					//Newly add if lock then get owed anount in amount textbox
					if(lbl_lockflag.Text=="True")
					{
						txt_amount.Text=lbl_owes.Text;
						txt_name.Text=lbl_FirstName.Text+" "+lbl_LastName.Text;
					}

				
				
					if (Convert.ToInt32(ds_case.Tables[0].Rows[0]["Bondflag"])==1) //main
					{
						if (ClsCase.CheckClientContact(Convert.ToInt32(ViewState["vTicketId"]))==true)
						{
							lbl_contactcheck.Text="True";
						}
						else
						{
							lbl_contactcheck.Text="False";
						}
					}

                    if (Convert.ToInt32(ds_case.Tables[0].Rows[0]["ActiveFlag"]) == 1)
                    {
                        //Only active flag should be 1 
                        btn_coversheet.Visible = true;
                        btn_Receipt.Visible = true;
                        btn_polm.Visible = true;
                        //Button Display new logic
                        DataSet ds_underlying = ClsCase.GetUnderlyingCaseInfo(Convert.ToInt32(ViewState["vTicketId"]));
                        for (int i = 0; i < ds_underlying.Tables[0].Rows.Count; i++)
                        {
                            //checking for BOND button //Bond flag set to 1 & status in Waiting or Arr
                            if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2))
                            {
                                btn_bond.Visible = true;
                                lbl_courtid.Text = ds_underlying.Tables[0].Rows[i]["CourtId"].ToString();
                            }
                            else
                            {
                                if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 12)
                                {
                                    btn_bond.Visible = true;
                                    lbl_courtid.Text = ds_underlying.Tables[0].Rows[i]["CourtId"].ToString();
                                }

                            }
                            //checking for TRIAL BUTTON 
                            //Outside court and status in Arr,Pre,Jury,Judge today or future courtdate
                            DateTime dcourt = Convert.ToDateTime(ds_underlying.Tables[0].Rows[i]["CourtDateMain"]);
                            if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 3 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 4 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3003 ))
                            {
                                if (ClsCase.ValidateTrialLetterPrintOption(TicketID))
                                    btn_trialLetter.Visible = true;
                                //code Added By Azee..for enable emai trial letter button if valid email exist
                                if (ds_case.Tables[0].Rows[0]["email"].ToString() != "")
                                {
                                    // Following line was commented by Farhan Sabir to address bug#1721
                                    //btn_EmailTrial.Visible = true;
                                    ViewState["emailid"] = ds_case.Tables[0].Rows[0]["email"].ToString();

                                }
                                else
                                {
                                    ViewState["emailid"] = "";

                                }
                            }
                            else
                                //Inside court status in Arr
                                if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 4 && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtId"]) == 3001 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtId"]) == 3002 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtId"]) == 3003))
                                {
                                    if (ClsCase.ValidateTrialLetterPrintOption(TicketID))
                                        btn_trialLetter.Visible = true;
                                    //code Added By Azee..for enable emai trial letter button if valid email exist
                                    if (ds_case.Tables[0].Rows[0]["email"].ToString() != "")
                                    {
                                        // Following line was commented by Farhan Sabir to address bug#1721
                                        //btn_EmailTrial.Visible = true;
                                        ViewState["emailid"] = ds_case.Tables[0].Rows[0]["email"].ToString();

                                    }
                                    else
                                    {
                                        ViewState["emailid"] = "";

                                    }
                                }
                            // Checking for LETTER OF REP //status in Waiting,Arr,Waiting2nd,Bond and court not to be Lubbok/Mykawa

                            if ((Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 1 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 2 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 11 || Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["categoryid"]) == 12) && (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) != 3003))
                            {
                                btn_letterofRep.Visible = true;
                                if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["CourtID"]) == 3060)
                                {
                                    btn_Discovery.Visible = true;
                                    btn_Limine.Visible = true;
                                }
                            }

                            if (Convert.ToInt32(ds_underlying.Tables[0].Rows[i]["ContinuanceAmount"]) > 0)
                            {
                                btn_contuance.Visible = true;
                            }
                            //if(ds_underlying.Tables[0].Rows[i]["Email"].ToString()!="")
                            //{
                            //    //../reports/payment_receipt_contract.asp?TicketID=<%=intticketid 
                            //    btn_emailrep.Visible=true;
                            //}						
                        }
                    }
                    else
                    {
                        btn_bond.Visible = false;
                        btn_contuance.Visible = false;
                        btn_coversheet.Visible = false;
                        btn_emailrep.Visible = false;
                        btn_EmailTrial.Visible = false;
                        btn_letterofRep.Visible = false;
                        btn_polm.Visible = false;
                        btn_Receipt.Visible = false;
                        btn_trialLetter.Visible = false;
                        btn_Limine.Visible = false;
                        btn_Discovery.Visible = false;
                        
                    }
				}
                //---Adil
                if (lbl_activeflag.Text == "1")
                {
                    btn_ESignature.Enabled = true;
                    btn_ESignature.Attributes.Add("OnClick", "return OpenDocumentSelector('" + Request.QueryString["casenumber"].ToString() + "-" + ViewState["vEmpID"].ToString() + "');");
                }
                else
                {
                    btn_ESignature.Enabled = false;
                }
                //---Adil
			}
			catch(Exception ex_payment)
			{				
				lblMessage.Text=ex_payment.Message + ex_payment.Source;
				clog.ErrorLog(ex_payment.Message,ex_payment.Source,ex_payment.TargetSite.ToString(),ex_payment.StackTrace);				
			}
		}
		//When update button is clicked
		private void btn_update_Click(object sender, System.EventArgs e)
		{
			UpdateInfo();
		}
		//In order to update the payment information
		private void UpdateInfo()
		{
			try
			{
				ClsPayments.TicketID=Convert.ToInt32(ViewState["vTicketId"]);
				ClsPayments.PaymentsComments=txt_paycomments.Text;
				ClsPayments.CertifiedMail=txt_certified.Text;
				ClsPayments.PreTrialStatus=Convert.ToInt32(rbtn_trial.SelectedValue);
				//ClsPayments.EmpID=Convert.ToInt32(cSession.GetCookie("sEmpID",this.Request));
				ClsPayments.EmpID = Convert.ToInt32( ViewState["vEmpID"]	);
				if (ClsPayments.UpdatePaymentInfo()==false)
				{
					lblMessage.Text="Problem in Updating Database";
				}
				else
				{
					//In order to get the updated info
					GetPayments();
				}
				ViewState["vTicketId"]=ClsPayments.TicketID;
			}
			catch(Exception	ex)
			{				
				lblMessage.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
		//When process button is clicked
		private void btn_process_Click(object sender, System.EventArgs e)
		{
			try
			{	
				string strReq = Request.Form["txtTimeStemp"].ToString();
				if(Session["TimeStemp"].ToString() == strReq.ToString())
				{
					txtTimeStemp.Text = System.DateTime.Now.TimeOfDay.ToString();
					Session["TimeStemp"] = txtTimeStemp.Text;				
					//mandatory	
					ClsPayments.TicketID = Convert.ToInt32( ViewState["vTicketId"]	);
					ClsPayments.EmpID = Convert.ToInt32( ViewState["vEmpID"]	);
					ClsPayments.ChargeAmount=Convert.ToDouble(txt_amount.Text);
					ClsPayments.RecDate=date_payment.SelectedDate;
					ClsPayments.PaymentType=Convert.ToInt32(ddl_paymentmethod.SelectedValue);
					//if credit card selected
					ClsPayments.CardHolderName=txt_name.Text.ToUpper();				
					ClsPayments.CardNumber=txt_cardno.Text;
					ClsPayments.CardType=ddl_cardtype.SelectedValue;
					ClsPayments.Cin=txt_cin.Text;
					ClsPayments.PaymentsComments = txt_paycomments.Text.Trim();
					ClsPayments.CertifiedMail = txt_certified.Text.Trim();
					ClsPayments.CardExpirationDate=ddl_expmonth.SelectedValue+"/"+ddl_expyear.SelectedValue;	
					if (lbl_scheduleid.Text=="")
					{
						lbl_scheduleid.Text="0";
					}
					ClsPayments.Scheduleid=Convert.ToInt32(lbl_scheduleid.Text);
					//Settings parameters for process flag
					//if 'payment type is refund' or 'not processing a schedule amt' or 'no schedule payments' 
					if (ClsPayments.PaymentType==8 || ClsPayments.Scheduleid!=0 || dg_Schedule.Items.Count==0 )
					{
						ClsPayments.Processflag=0;
					}
					else
					{
						ClsPayments.Processflag=1;
					}	

					//Calling Method to process payment
					if(ClsPayments.ProcessPayment())
					{
						ClsPayments.UpdatePaymentInfo();
						txt_amount.Text="";
						txt_amount.Enabled=true;
						lbl_scheduleid.Text="0";
						ddl_paymentmethod.SelectedValue="0";					
						//btn_process. ="clsbutton";
						//Initializing credit card section
						txt_name.Text="";
						txt_cardno.Text="";
						ddl_cardtype.SelectedIndex=0;
						txt_cin.Text="";
						ddl_expmonth.SelectedValue="01";
                        //for oscar court
                        string[] key_1 ={ "@ticketid" };
                        object[] value_1 ={ Convert.ToInt32(ViewState["vTicketId"]) };
                        DataSet ds_os = cls_db.Get_DS_BySPArr("usp_hts_check_activeflag", key_1, value_1);
                        if (ds_os.Tables[0].Rows.Count > 0)
                        {
                            if (lbl_OscarCourtID.Text == "3061" & lbl_OscarActiveFlag.Text == "0" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1" & ds_os.Tables[0].Rows[0]["courtid"].ToString() == "3061")
                            {
                                string name = ViewState["vTicketId"].ToString()+DateTime.Today.Day.ToString() + ".pdf";//Session.SessionID + DateTime.Now.ToFileTime() + ".pdf";
                                string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/clientinfo/frmCaseSummary.aspx?cdate=" + DateTime.Now.ToFileTime() + "+&pdf=1&casenumber=" + ViewState["vTicketId"].ToString() + "&search=" + ViewState["vSearch"].ToString();
                                GeneratePDF(path, name);
                                SendMail();
                            }
                            lbl_OscarActiveFlag.Text = ds_os.Tables[0].Rows[0]["activeflag"].ToString();
                            lbl_OscarCourtID.Text = ds_os.Tables[0].Rows[0]["courtid"].ToString();
                        }
                        else
                        {
                            lbl_OscarActiveFlag.Text = "";
                            lbl_OscarCourtID.Text = "";
                        }
                        //
						GetPayments();	
						//	btn_process.CssClass="clsbutton";
						//		btn_process.BackColor=System.Drawing.Color.FromName("#ffcc66");    
						//			Page.Response.Redirect(Page.Request.Url.ToString(), true);		
						//			btn_process.Font.Bold=true;
						//			btn_process.Font.Size=System.Web.UI.WebControls.FontUnit.Point(8);
						//			btn_process.BorderColor= System.Drawing.Color.FromName("#ffcc66");    
						//			btn_process.ForeColor= System.Drawing.Color.FromName("#ffcc66");    
						//			btn_process.BackColor=System.Drawing.Color.FromName("#ffcc66");

						//lblMessage.Text="zee";
					}
					else
					{
						lblMessage.Text="Payment cannot be processed";
						//btn_process.CssClass="clsbutton";
						//btn_process.BackColor=System.Drawing.Color.Red;
					}
				}
				else 
				{
					GetPayments();


				}
			}
			catch(Exception ex)
			{
				if (txt_cardno.Text!="")
				{
					//Filling Credit Card Grid to show credit error
					DataSet ds_creditcard=ClsPayments.GetCreditCardInfo();
					dg_creditcard.DataSource=ds_creditcard;
					dg_creditcard.DataBind();
				}
				lblMessage.Text=ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}		
		}
		
		private void dg_PaymentDetail_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				try
				{
					LinkButton lnkb_void=((LinkButton)e.Item.FindControl("lnkb_edit")); 
					Label lbl_paydate=((Label)e.Item.FindControl("lbl_paydate"));
					Label lbl_voidflag= ((Label)e.Item.FindControl("lbl_vflag"));
					DateTime chkdt=Convert.ToDateTime(lbl_paydate.Text);		
					//If refund then mark as red
					if(((Label)e.Item.FindControl("lbl_paymethod")).Text=="Refund")
					{
						((Label)e.Item.FindControl("lbl_emp")).ForeColor=System.Drawing.Color.Red;
						((Label)e.Item.FindControl("lbl_amount")).ForeColor=System.Drawing.Color.Red;
						((Label)e.Item.FindControl("lbl_paydate")).ForeColor=System.Drawing.Color.Red;
						((Label)e.Item.FindControl("lbl_paymethod")).ForeColor=System.Drawing.Color.Red;				
					}		
				

					//When trans is done on Todays Date then Enable the void button				
					
					int vflag=Convert.ToInt32(lbl_voidflag.Text);
					if (vflag==1)
					{					
						lnkb_void.Enabled=false;
					}
					else
					{
						lnkb_void.Text="";
					}
					if (DateTime.Now.Date==chkdt.Date && vflag==0)
					{
						lnkb_void.Enabled=true;
						lnkb_void.Text="Void";
					}
					if (lnkb_void.Enabled==true)
					{
						lnkb_void.Attributes.Add("onclick","return PromptVoid();");					
					}
					string lbl_amount= ((Label)e.Item.FindControl("lbl_amount")).Text;
					if(lbl_amount.StartsWith("-"))
					{
						lbl_amount=lbl_amount.Substring(1);
						((Label)e.Item.FindControl("lbl_amount")).Text=lbl_amount;
					}
					//Transaction num
					string s_trans= ((Label)e.Item.FindControl("lbl_transnum")).Text;
					if (s_trans !="")// && s_trans!="0")
					{
						Label lbl_trans= ((Label)e.Item.FindControl("lbl_transnum"));
						lbl_trans.Text="("+s_trans+")";
					}
				}
				catch(Exception ex)
				{
					clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);					
				}
			}
		}
		//When Schedule Amount is clicked
		private void dg_Schedule_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{			
			if(e.CommandName=="AmountClicked")
			{
				LinkButton lnkb_amount= ((LinkButton)e.Item.FindControl("lnkb_amount"));
				txt_amount.Text= lnkb_amount.Text;						
				txt_amount.Enabled=false;
				date_payment.SelectedDate=DateTime.Now.Date;
				lbl_scheduleid.Text= ((Label)e.Item.FindControl("lbl_schd_id")).Text;	

			}
		}
		//Redirect to next page
		private void btn_next_Click(object sender, System.EventArgs e)
		{
			int tid=Convert.ToInt32(ViewState["vTicketId"]);
			string search=ViewState["vSearch"].ToString();
			UpdateInfo();
			//tid=ClsPayments.TicketID;
			Response.Redirect("CaseHistory.aspx?casenumber="+ tid + "&search="+ search,false);
		}
		//When void is clicked
		private void dg_PaymentDetail_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
            {
                ClsPayments.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
                ClsPayments.EmpID = Convert.ToInt32(ViewState["vEmpID"]);			
				int invoicenum=Convert.ToInt32(((Label)e.Item.FindControl("lbl_invoicenum")).Text);				
				string Paymentmethod=((Label)e.Item.FindControl("lbl_paymethod")).Text;
				ClsPayments.VoidPayment(invoicenum,Paymentmethod);	
				GetPayments();
			}
			catch(Exception ebound)
			{
				lblMessage.Text=ebound.Message;	
				clog.ErrorLog(ebound.Message,ebound.Source,ebound.TargetSite.ToString(),ebound.StackTrace);				
			}
		}
		//To get the sum of Schedule Amount
		private double ScheduleAmountSum()
		{
			try
			{
				double sum=0;				
				foreach (DataGridItem ItemX in dg_Schedule.Items) 
				{ 	      
					sum+=Convert.ToDouble(((LinkButton)(ItemX.FindControl("lnkb_amount"))).Text);											
				}
				return sum;
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
				return 0;
			}	
		}

		//In order to Edit the schedule payment
		private void EditSchedulePayment(double owes)
		{
			try
			{
				int schdid=0;
				double amount=0;
				foreach (DataGridItem ItemX in dg_Schedule.Items) 
				{ 	
					//binding scheduleid 
					schdid=Convert.ToInt32(((Label)ItemX.FindControl("lbl_schd_id")).Text);					
					amount=Convert.ToDouble(((LinkButton)ItemX.FindControl("lnkb_amount")).Text);										
					//Function to open window to Edit Schedule Amt
					((LinkButton) ItemX.FindControl("lnkb_editschedule")).Attributes.Add("OnClick","return OpenSchedule(" + owes + " , " + schdid + ");"); 													
				}

			}
			catch(Exception	ex)
			{
				lblMessage.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}


		//Inactive the selected schedule payment
		private void dg_Schedule_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{			  
				int schdid=0;
				schdid= Convert.ToInt32(((Label)e.Item.FindControl("lbl_schd_id")).Text);
				ClsPayments.InactiveSchedulePayment(schdid);	
				//filling Schedule Grid
				DataSet ds_schedulepayment=ClsPayments.GetSchedulePaymentInfo(Convert.ToInt32(ViewState["vTicketId"]));
				dg_Schedule.DataSource= ds_schedulepayment;
				dg_Schedule.DataBind();  
				GetPayments();
			}
			
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
		
		
		
		private void dg_Schedule_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				//to prompt
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					LinkButton btnButton = (LinkButton)e.Item.Cells[4].Controls[0]; 
					btnButton.Attributes.Add("onclick","return PromptDelete();");		
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
		//Letter Batch View
		private void dg_Print_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					string lnk = "javascript: return OpenPopUp('frmPreview.aspx?RecordID=" + ((Label) e.Item.FindControl("lbl_recid")).Text + "')";
					((LinkButton) e.Item.FindControl("lnkbtn_File")).Attributes.Add("OnClick",lnk);
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
		}
        //When cover sheet button is clicked
        protected void btn_coversheet_Click1(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("frmCaseSummary.aspx?casenumber=" + ViewState["vTicketId"].ToString() + "&search=" + ViewState["vSearch"].ToString(),false);
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);				
            } 
        }
	}
}

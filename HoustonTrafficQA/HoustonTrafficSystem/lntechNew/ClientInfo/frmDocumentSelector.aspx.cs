using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.ClientInfo
{
    public partial class frmDocumentSelector : System.Web.UI.Page
    {
        private clsENationWebComponents cls_DataBase = new clsENationWebComponents();
        // bool ContLetter_Flag = false, Bond_Flag = false, TrialLetter_Flag = false, LetterRep_Flag = false;
        //string ParameterString = "";

        // Noufil 5931 05/21/2009 Code Code commented. All code shift to javascript.
        protected void Page_Load(object sender, EventArgs e)
        {
            ////this.Visible = false;
            //string SessionedID = "", ExePath = "";
            //DataTable dt_Config = new DataTable();
            //dt_Config = cls_DataBase.Get_DS_BySP("USP_HTS_ESignature_Get_Configuration").Tables[0];
            //ExePath = dt_Config.Rows[0]["ExePath"].ToString();
            //ExePath = ExePath.Replace("\\", "\\\\");
            //hf_ExePath.Value = ExePath;
            //SessionedID = Session.SessionID + System.DateTime.Now;
            //SessionedID = SessionedID.Replace("/", "");
            //SessionedID = SessionedID.Replace(":", "");
            //SessionedID = SessionedID.Replace(" ", "");
            //hf_SessionId.Value = SessionedID;
            //hf_TicktEmployeeId.Value = Request.QueryString["ticketid"].ToString();
            ////Response.Write("<script type=\"text/javascript\">OpenESignature('" + Request.QueryString["ticketid"].ToString() + "', '" + SessionedID + "', '" + ExePath + "');</script>");
            //this.Dispose();
            //Attributes.Add("OnLoad", "OpenESignature('" + Request.QueryString["ticketid"].ToString() + "', '" + SessionedID + "', '" + ExePath + "');");
        }
        //private void EnableCheckButtons()
        //{
        //    try
        //    {
        //        chk_Bond.Enabled = false ;
        //        chk_LetterContinuance.Enabled = false ;
        //        chk_LetterOfRep.Enabled = false ;
        //        chk_LetterTrial.Enabled = false ;

        //        string[] parameterKeys = { "@TicketID" };
        //        object[] values = { Request.QueryString["ticketid"].ToString().Substring(0,Request.QueryString["ticketid"].IndexOf("-") )   };
        //        DataTable dt_EnabledCheckButtons = new DataTable();

        //        dt_EnabledCheckButtons = cls_DataBase.Get_DT_BySPArr("USP_HTS_Get_PAYMENTINFO_BUTTONSTATUS", parameterKeys, values);

        //        for (int i = 0; i < dt_EnabledCheckButtons.Rows.Count; i++)
        //        {
        //            if ((Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 1 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 2 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 11 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 12) && (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtID"]) != 3003))
        //            {
        //                LetterRep_Flag = true;
        //            }

        //            DateTime dcourt = Convert.ToDateTime(dt_EnabledCheckButtons.Rows[i]["CourtDateMain"]);
        //            if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 2 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 3 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 4 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtID"]) != 3003))
        //            {
        //                TrialLetter_Flag = true;
        //            }
        //            else
        //            {
        //                //Inside court status in Arr
        //                if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 4 && (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtId"]) == 3001 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtId"]) == 3002 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["CourtId"]) == 3003))
        //                {
        //                    TrialLetter_Flag = true;
        //                }
        //            }

        //            if (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 1 || Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 2))
        //            {
        //                Bond_Flag = true;
        //            }
        //            else
        //            {
        //                if (Convert.ToInt32(dt_EnabledCheckButtons.Rows[i]["categoryid"]) == 12)
        //                {
        //                    Bond_Flag = true;
        //                }
        //            }

        //            if ((Convert.ToInt32(dt_EnabledCheckButtons.Rows[0]["ContinuanceAmount"]) > 0))
        //            {
        //                ContLetter_Flag = true;
        //            }
        //        }

        //        chk_Bond.Checked = Bond_Flag; 
        //        chk_Bond.Enabled = Bond_Flag;
        //        //chk_LetterContinuance.Checked = ContLetter_Flag; 
        //        //chk_LetterContinuance.Enabled = ContLetter_Flag;
        //        //chk_LetterOfRep.Checked = LetterRep_Flag;
        //        //chk_LetterOfRep.Enabled = LetterRep_Flag;
        //        //chk_LetterTrial.Checked = TrialLetter_Flag; 
        //        //chk_LetterTrial.Enabled = TrialLetter_Flag;
        //    }
        //    catch (Exception ex)
        //    { 

        //    }
        //}
    }
}
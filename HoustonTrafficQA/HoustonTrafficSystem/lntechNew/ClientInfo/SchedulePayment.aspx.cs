using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.ClientInfo
{
	/// <summary>
	/// Summary description for SchedulePayment.
	/// </summary>
	public partial class SchedulePayment : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btn_submit;
		clsENationWebComponents ClsDb= new clsENationWebComponents();
        //Sabir khan 5560 02/24/2009 Important System Notice ...
        NewClsPayments Clspayments = new NewClsPayments();
        //clsPayments Clspayments= new clsPayments();		
		clsSession cSession = new clsSession();
		clsLogger clog = new clsLogger();
		protected System.Web.UI.WebControls.TextBox txt_amount;
		protected eWorld.UI.CalendarPopup date_payment;		
		protected System.Web.UI.WebControls.Label lbl_owes;
		int scheduleid=0;
		protected System.Web.UI.WebControls.Label lbl_activeflag;
		double owes=0;
		
		
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			try
			{
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else
				{

					//Adding Attribute For Save button
					btn_submit.Attributes.Add("onclick","return ValidateForm();");
             
					//Clspayments.TicketID=Convert.ToInt32(cSession.GetCookie("sTicketID",this.Request));			
					Clspayments.TicketID=Convert.ToInt32(Request.QueryString["casenumber"]);	
					Clspayments.EmpID=Convert.ToInt32(cSession.GetCookie("sEmpID",this.Request));			
					//	Clspayments.TicketID=97211;
					//	Clspayments.EmpID=3992;
					scheduleid=Convert.ToInt32(Request["schid"]);
					owes=Convert.ToDouble(Request["owes"]);	
					lbl_activeflag.Text=Request["activeflag"].ToString();
					if(!IsPostBack)
					{
						if (scheduleid==0)
						{
							lbl_owes.Text=owes.ToString(); 
							date_payment.SelectedDate=DateTime.Now.Date.AddDays(15);
						}
						else
						{
                            //Change By Ajmal
							//System.Data.SqlClient.SqlDataReader dr=null;
                            IDataReader dr = null;
							dr= Clspayments.GetScheduleAmount(scheduleid);
							dr.Read();
							txt_amount.Text=dr["amount"].ToString();
							date_payment.SelectedDate=Convert.ToDateTime(dr["paymentdate"]);
							double owessum=owes+Convert.ToDouble(dr["amount"]);
							lbl_owes.Text=owessum.ToString(); dr.Close();
						}			
						date_payment.LowerBoundDate=DateTime.Now.Date;
						//date_payment.SelectedDate=DateTime.Now.Date;			
					}
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		//when submit button is clicked
		private void btn_submit_Click(object sender, System.EventArgs e)
		{
			InsertEditSchedulePayment();
			//in order to call getpayments on paymentinfo page
			Session["FromSchedule"]="yes";
			//In order to close the popup			
		//	HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");			
		//	HttpContext.Current.Response.Write("<script language='javascript'> window.opener.document.forms[0].submit(); self.close();   </script>");			
            HttpContext.Current.Response.Write("<script language='javascript'> opener.location.href = opener.location; self.close();   </script>");			

		}
		private void InsertEditSchedulePayment()
		{				
			double amount=Convert.ToDouble(txt_amount.Text);
			Clspayments.AddSchedulePayment(amount,date_payment.SelectedDate,scheduleid);            			
		}
	}
}

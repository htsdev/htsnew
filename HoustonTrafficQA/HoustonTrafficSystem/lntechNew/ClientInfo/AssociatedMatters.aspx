﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssociatedMatters.aspx.cs"
    Inherits="HTP.ClientInfo.AssociatedMatters" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagName="Info" TagPrefix="Contact" Src="~/WebControls/ContactInfo.ascx" %>
<%@ Register TagName="LookUp" TagPrefix="Contact" Src="~/WebControls/ContactIDLookUp.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Associated Matters</title>

    <script type="text/javascript">
    
       function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
       
       function Confirmation(recordid,CaseTypeID)
        {

           var ans= confirm("Removing CID. Would you like to continue?");
            if(ans)
            {
        
            var CID = gup('cid');                       
                if (CID != "")
                {
                    window.location="AssociatedMatters.aspx?type=UnLink&RecordID="+recordid + "&cid="+CID+"&CaseType="+CaseTypeID;
                }
                else
                {
                var casenumber= gup( 'casenumber' );                
                window.location="AssociatedMatters.aspx?type=UnLink&RecordID="+recordid + "&casenumber="+casenumber+"&CaseType="+CaseTypeID;
                }
                
            }
   
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td align="left" width="80%" style="height: 20px">
                                <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                <asp:Label ID="lbl_CaseCount" runat="server" CssClass="Label"></asp:Label>)
                                <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink><a href="#"></a>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="80%" style="height: 20px">
                                <asp:Label ID="lblMessage" runat="server" CssClass="Label"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TreeView ID="TreeView1" runat="server">
                    </asp:TreeView>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

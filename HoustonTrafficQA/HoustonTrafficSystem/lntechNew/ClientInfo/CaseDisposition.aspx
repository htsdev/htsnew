<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ClientInfo.CaseDisposition"
    SmartNavigation="False" CodeBehind="CaseDisposition.aspx.cs" %>

<%@ Register Src="../WebControls/UpdateViolationSRV.ascx" TagName="UpdateViolationSRV"
    TagPrefix="uc4" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%--<%@ Register Src="../WebControls/UpdateViolation.ascx" TagName="UpdateViolation"
    TagPrefix="uc2" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/ReadNotes.ascx" TagName="ReadNotes" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Disposition</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript">
		
		// ********* Scripts for Update Panel *********//
		
        //Added By Zeeshan Ahmed
        function DisplayTogglePa(ddlname,imagename,labelname)
		{	
			
			    
            if(document.getElementById(ddlname).style.display=='none' )
            {
                document.getElementById(ddlname).style.display='block';
                document.getElementById(labelname).style.display='none';
                
                
            }
            else
            {
             document.getElementById(ddlname).style.display='none';   
             document.getElementById(labelname).style.display='block';
            
            }
            
             
                       
            return false;
		    /*var temp =   document.getElementById("ddl_Coverage").style.display;
			document.getElementById("ddl_Coverage").style.display = document.getElementById("lbl_Coverage").style.display;
		    document.getElementById("lbl_Coverage").style.display = temp;*/
		    
		    
		}		
		
//		function MaxLength()
//		{
//		if(document.getElementById("txt_GeneralComments").value.length >2000-27)//-27 bcoz to show last time partconcatenated with comments
//		{
//		alert("Please Enter the character in the specified Range");
//		document.getElementById("txt_GeneralComments").focus();
//        return false;    				
//		}
//				
//		}
		
//		 function ValidateLenght()
//	     { 
//			var cmts = document.getElementById("txt_GeneralComments").value;
//			if (cmts.length > 2000-27)//-27 bcoz to show last time partconcatenated with comments
//			{
//			    event.returnValue=false;
//                event.cancel = true;
//			}
//	    }
	    
//	     function TValidateLenght()
//	     { 
//			var cmts = document.getElementById("txt_TrialComments").value;
//			if (cmts.length > 500)
//			{
//			    event.returnValue=false;
//                event.cancel = true;
//			}
//	    }
		
		
		function OpenNewWindow(txtUrl)
		{
		
		var w = window.open(txtUrl,"","status=yes,left=20,top=20, width=850,height=600,scrollbars=yes");
		return false;
		}
		
		
		function setDurText(ddlmon,ddlday,ddlyrs,txtDur)
		{
		var mon = document.getElementById(ddlmon);
		var day = document.getElementById(ddlday);
		var yrs = document.getElementById(ddlyrs);
		var txt = document.getElementById(txtDur);
		
		
		if (mon.selectedIndex == 0 || day.selectedIndex == 0 || yrs.selectedIndex == 0 )  
			{
			
			}
			else
			{
			//myDate.setFullYear(2010,0,14)
				var objDate1 = new Date() ; 
				var dd = day.selectedIndex;
				
				var Mon = mon.selectedIndex ;
				Mon  = Mon - 1 ; 
				//dd.selectedIndex = D.getDate();
				//yy.selectedvalue = D.getFullYear();
				
				//for (var i =0 ; i < yy.length ; i++)
				//{
				var yy = yrs.options[yrs.selectedIndex].value * 1
				
				objDate1.setFullYear(yy,Mon,dd);				
				
				var objDate2 = new Date(); with (objDate2) setDate(getDate());
				var Tdur = Math.ceil(((objDate1.getTime()-objDate2.getTime())/86400)/1000);
				
					if (Tdur <= 0)
					{
						alert ("Please select the future Date");
						txt.value = "";
					} 
					else
					{
						txt.value = Tdur;
					}
				
				
			}
		
		}
		
		function ValidateInput()
		{ //a;
				var rbol =true;
		var inbol = true;
		rbol = ValidateInput1();
			if (rbol == true)
			{/*
			inbol = ValidateInput2();
				if (inbol == false)
				{
				return false;
				}
			*/}
			else if(rbol == false)
			{
			return false;
			}
//		if(document.getElementById("txt_GeneralComments").value.length >2000-27)//-27 bcoz to show last time partconcatenated with comments
//		{
//		alert("Sorry You Cannot type more than 2000 characters ");
//		document.getElementById("txt_GeneralComments").focus();
//        return false;    				
//		}

	      //Comments Textboxes
	      // General Comments
		  var dateLenght = 0;
		  var newLenght = 0;
		  
//          newLenght = document.getElementById("WCC_GeneralComments_txt_comments").value.length
//		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

//		  if (document.getElementById("WCC_GeneralComments_txt_comments").value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerText.length > (5000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
//		  {
//		    alert("Sorry You cannot type in more than 5000 characters in General comments box")
//			return false;		  
//		  }
	      //Comments Textboxes
	      //Trial Comments
          newLenght = document.getElementById("WCC_TrialComments_txt_comments").value.length
		  if(newLenght > 0){dateLenght =  27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

		  if (document.getElementById("WCC_TrialComments_txt_comments").value.length + document.getElementById("WCC_TrialComments_lbl_Comments").innerText.length >1000 - dateLenght)
		  {
		    alert("Sorry You cannot type in more than 1000 characters in Trial comments box")
			return false;		  
		  }


		plzwait('TableComment','plz_wait2');
		return rbol ;
		}
		
		function ValidateInput1()
		{	
			//a;			
			var error = "Please correct value.Only Integer value is allowed";
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_Details";
			var idx=2;
			var bol = true;
			//alert("asli");
			cnt = cnt + 2;
			for (idx=2; idx < (cnt); idx++)
			{
			var ctlPending1 = "";
			var casevio1 = "";
			
            var ddlcoveringfirm="";
			var hffirmid="";
			
			if( idx < 10)
			{
			 ctlPending1 = grdName+ "_ctl0" + idx + "_ddl_Pending";
			 casevio1 = grdName+ "_ctl0" + idx + "_lbl_CrtVio";
			 
            ddlcoveringfirm=grdName+ "_ctl0" + idx + "_ddlcoveringfirms";
			 hffirmid=grdName+ "_ctl0" + idx + "_HFCoveringFirmID";   
			}
			else
			{
			 ctlPending1 = grdName+ "_ctl" + idx + "_ddl_Pending";
			 casevio1 = grdName+ "_ctl" + idx + "_lbl_CrtVio";
			 
			 ddlcoveringfirm=grdName+ "_ctl" + idx + "_ddlcoveringfirms";
			 hffirmid=grdName+ "_ctl" + idx + "_HFCoveringFirmID";   
			
			}
			
			
			
            //-------------M.Azwar Alam on 27 August 2007 for TAsk no 1126--------------------------------------
			
			if(document.getElementById(ddlcoveringfirm).style.display=="block")
			{
			    document.getElementById(hffirmid).value=document.getElementById(ddlcoveringfirm).value;//for updating tblticketsviolations coveringfirmid
			    
			  
			    
			 }
			
			   document.getElementById("HFMainFirmID").value=document.getElementById(hffirmid).value;//for updating tbltickets coveringfirmid..
			 //-------------------------------------------------------------------------------------------------

			
			
			var ddl_Pending1 = document.getElementById(ctlPending1);	
			var txtCStatus1 = document.getElementById(casevio1);
			
			//alert(txtCStatus1.innerText);
				if (txtCStatus1.innerText == "50")
				{										
					// user can't select 'Pending' as outcome if case is disposed.
					//if ((ddl_Pending1.value == 1) || (ddl_Pending1.value == 16))
					if ((ddl_Pending1.value == 1) )
					{
					alert ("Sorry, It is not an appropriate violation outcome");
					ddl_Pending1.focus();
					return false;
					}
				}
				
				else if(txtCStatus1.innerText !="50")
				{
				    //Comment By Zeeshan Ahmed
					/*if ((ddl_Pending1.value !=1) && (ddl_Pending1.value !=16))
					{
						alert ("It is not an appropriate violation outcome");
						ddl_Pending1.focus();
						return false;
					}*/
					
					if ((txtCStatus1.innerText == "10") || (txtCStatus1.innerText == "8") || (txtCStatus1.innerText == "6") || (txtCStatus1.innerText == "0"))
					{
						var sr = idx-1 ;
						alert("Invalid Court status. Please update Violation Sr# " + sr);
						return false;
					}	
				}
				bol = ValidateInput2(idx);
				if (bol == false)
				{
				return false;
				}
			}	
		
		
		
				
		
		return bol;
		}
		
		function ValidateInput2(idx)
		{	//a;
			var error = "Please correct value.Only Positive Integer value is allowed";
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_Details";
			var idx=2;
			cnt = cnt + 2;
			var bol = true;
			for (idx=2; idx < (cnt); idx++)
			{
			
		//	alert(cnt);
		//	alert(idx);
				var ctlPending ="";
				var ctlAmount = "";
				var ctlTDuration = "";
				var ctlPDuration = "";
				
				var casevio =""; 
				//alert(ddl_Pending.value);
				if( idx < 10)
				{
				 ctlPending = grdName+ "_ctl0" + idx + "_ddl_Pending";
				 ctlAmount = grdName+ "_ctl0" + idx + "_txt_TAmount";
				 ctlTDuration = grdName+ "_ctl0" + idx + "_txt_TDuration";
				 ctlPDuration = grdName+ "_ctl0" + idx + "_txt_PDuration";
				 casevio = grdName+ "_ctl0" + idx + "_lbl_CrtVio";
				}
				else
				{
				ctlPending = grdName+ "_ctl" + idx + "_ddl_Pending";
				 ctlAmount = grdName+ "_ctl" + idx + "_txt_TAmount";
				 ctlTDuration = grdName+ "_ctl" + idx + "_txt_TDuration";
				 ctlPDuration = grdName+ "_ctl" + idx + "_txt_PDuration";
				 casevio = grdName+ "_ctl" + idx + "_lbl_CrtVio";
				}
				
					
				var ddl_Pending = document.getElementById(ctlPending);	
				var txt_TAmount =document.getElementById(ctlAmount);	
				var txt_TDuration = document.getElementById(ctlTDuration);	
				var txt_PDuration = document.getElementById(ctlPDuration);	
				
				var txtCStatus = document.getElementById(casevio);
				
				var ctlPAMonth =""; 
				var ctlPADay = "";
				var ctlPAYear = "";
				
				var ctlPTMonth = "";
				var ctlPTDay = "";
				var ctlPTYear = "";
				
				var ctlCauseMonth = "";
				var ctlCauseDay = "";
				var ctlCauseYear = "";
				var ctlCauseTime = "";
				var ctlCauseCrtNo = "";
				
				var ctlShowMonth = "";
				var ctlShowDay = "";
				var ctlShowYear = "";
				var ctlShowTime = "";
				var ctlShowCrtNo =""; 
				
				var ctlChkRet ="";
				var ctlChkMisc ="";
				var ctlChkDsc ="";
				var ctlChkShow ="";
				
				var ctrlDscDay1 ="";
				var ctrlDscDay2 ="";
				
				var txtMisc ="";
				
				if( idx < 10)
				{
				 ctlPAMonth = grdName+ "_ctl0" + idx + "_ddl_PAMonth";
				 ctlPADay = grdName+ "_ctl0" + idx + "_ddl_PADay";
				 ctlPAYear = grdName+ "_ctl0" + idx + "_ddl_PAYear";
				
				 ctlPTMonth = grdName+ "_ctl0" + idx + "_ddl_PTMonth";
				 ctlPTDay = grdName+ "_ctl0" + idx + "_ddl_PTDay";
				 ctlPTYear = grdName+ "_ctl0" + idx + "_ddl_PTYear";
				
				 ctlCauseMonth = grdName+ "_ctl0" + idx + "_ddl_CauseMonth";
				 ctlCauseDay = grdName+ "_ctl0" + idx + "_ddl_CauseDay";
				 ctlCauseYear = grdName+ "_ctl0" + idx + "_ddl_CauseYear";
				 ctlCauseTime = grdName+ "_ctl0" + idx + "_ddl_CauseTime";
				 ctlCauseCrtNo = grdName+ "_ctl0" + idx + "_txt_CauseCrtNo";
				
				 ctlShowMonth = grdName+ "_ctl0" + idx + "_ddl_ShowMonth";
				 ctlShowDay = grdName+ "_ctl0" + idx + "_ddl_ShowDay";
				 ctlShowYear = grdName+ "_ctl0" + idx + "_ddl_ShowYear";
				 ctlShowTime = grdName+ "_ctl0" + idx + "_ddl_ShowTime";
				 ctlShowCrtNo = grdName+ "_ctl0" + idx + "_txt_ShowCrtNo";
				
				 ctlChkRet =grdName+ "_ctl0" + idx + "_chk_Ret";
				 ctlChkMisc =grdName+ "_ctl0" + idx + "_chk_Misc";
				 ctlChkDsc =grdName+ "_ctl0" + idx + "_chk_Dsc";
				 ctlChkShow =grdName+ "_ctl0" + idx + "_chk_ShowCause";
				
				 ctrlDscDay1 =grdName+ "_ctl0" + idx + "_txt_DSCDay1";
				 ctrlDscDay2 =grdName+ "_ctl0" + idx + "_txt_DSCDay2";
				
				 txtMisc =grdName+ "_ctl0" + idx + "_txt_Misc";
				
				}
				else
				{
				ctlPAMonth = grdName+ "_ctl" + idx + "_ddl_PAMonth";
				 ctlPADay = grdName+ "_ctl" + idx + "_ddl_PADay";
				 ctlPAYear = grdName+ "_ctl" + idx + "_ddl_PAYear";
				
				 ctlPTMonth = grdName+ "_ctl" + idx + "_ddl_PTMonth";
				 ctlPTDay = grdName+ "_ctl" + idx + "_ddl_PTDay";
				 ctlPTYear = grdName+ "_ctl" + idx + "_ddl_PTYear";
				
				 ctlCauseMonth = grdName+ "_ctl" + idx + "_ddl_CauseMonth";
				 ctlCauseDay = grdName+ "_ctl" + idx + "_ddl_CauseDay";
				 ctlCauseYear = grdName+ "_ctl" + idx + "_ddl_CauseYear";
				 ctlCauseTime = grdName+ "_ctl" + idx + "_ddl_CauseTime";
				 ctlCauseCrtNo = grdName+ "_ctl" + idx + "_txt_CauseCrtNo";
				
				 ctlShowMonth = grdName+ "_ctl" + idx + "_ddl_ShowMonth";
				 ctlShowDay = grdName+ "_ctl" + idx + "_ddl_ShowDay";
				 ctlShowYear = grdName+ "_ctl" + idx + "_ddl_ShowYear";
				 ctlShowTime = grdName+ "_ctl" + idx + "_ddl_ShowTime";
				 ctlShowCrtNo = grdName+ "_ctl" + idx + "_txt_ShowCrtNo";
				
				 ctlChkRet =grdName+ "_ctl" + idx + "_chk_Ret";
				 ctlChkMisc =grdName+ "_ctl" + idx + "_chk_Misc";
				 ctlChkDsc =grdName+ "_ctl" + idx + "_chk_Dsc";
				 ctlChkShow =grdName+ "_ctl" + idx + "_chk_ShowCause";
				
				 ctrlDscDay1 =grdName+ "_ctl" + idx + "_txt_DSCDay1";
				 ctrlDscDay2 =grdName+ "_ctl" + idx + "_txt_DSCDay2";
				
				 txtMisc =grdName+ "_ctl" + idx + "_txt_Misc";
				
				}
				
				var ddl_PAMonth =  document.getElementById(ctlPAMonth);
				var ddl_PADay =  document.getElementById(ctlPADay);
				var ddl_PAYear =  document.getElementById(ctlPAYear) ;
				
				var ddl_PTMonth = document.getElementById(ctlPTMonth);
				var ddl_PTDay = document.getElementById(ctlPTDay);
				var ddl_PTYear = document.getElementById(ctlPTYear);
				
				var ddl_CauseMonth = document.getElementById(ctlCauseMonth);
				var ddl_CauseDay = document.getElementById(ctlCauseDay);  
				var ddl_CauseYear = document.getElementById(ctlCauseYear);
				var ddl_CauseTime = document.getElementById(ctlCauseTime);
				var txt_CauseCrtNo = document.getElementById(ctlCauseCrtNo);
				
				var ddl_ShowMonth = document.getElementById(ctlShowMonth);
				var ddl_ShowDay = document.getElementById(ctlShowDay);  
				var ddl_ShowYear = document.getElementById(ctlShowYear);
				var ddl_ShowTime = document.getElementById(ctlShowTime);
				var txt_ShowCrtNo = document.getElementById(ctlShowCrtNo);
				
				var txtDscDay1 = document.getElementById(ctrlDscDay1);
				var txtDscDay2 = document.getElementById(ctrlDscDay2);
				
				var chk_Ret = document.getElementById(ctlChkRet);
				var chk_Misc = document.getElementById(ctlChkMisc);
				var chk_Dsc = document.getElementById(ctlChkDsc);
				var chk_Show = document.getElementById(ctlChkShow);
				
				var txt_Misc = document.getElementById(txtMisc);
				
				
				
				bol = true;
							 
				 if(ddl_Pending.value == 18)
				{ 
			
					bol = ValidationInteger(txt_TAmount);
					if (bol==false)
						{
							return false;
						}
					/*var vAmount = txt_TAmount.value;  //for amount greater then zero 
					if (vAmount <=0)
						{
							alert("Amount must be greater then zero.");
							txt_TAmount.focus();
							return false;
						}*/
					bol = ValidationInteger(txt_TDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					bol = ValidationInteger(txt_PDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					
					bol = IntegerLength(txt_TDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					bol = IntegerLength(txt_PDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
						
					bol = ChkDateCombo(ddl_PAMonth);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PADay);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PAYear);
					if (bol==false)
						{
							//return false;
						}
						
					bol = chkDatefun(ddl_PAMonth,ddl_PADay,ddl_PAYear);
					if (bol==false)
						{
							return false;
						}
						
					bol = CompareTodayDate(ddl_PAMonth,ddl_PADay,ddl_PAYear);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PTMonth);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PTDay);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PTYear);
					if (bol==false)
						{
							//return false;
						}
						
					bol = chkDatefun(ddl_PTMonth,ddl_PTDay,ddl_PTYear);
					if (bol==false)
						{
							return false;
						}
					bol = CompareTodayDate(ddl_PTMonth,ddl_PTDay,ddl_PTYear);
					if (bol==false)
						{
							//return false;
						}
					
					if (chk_Dsc.checked == true)
						{
						bol = ValidationInteger(txtDscDay1);
							if (bol==false)
							{
								//alert(error)
								return false;
							}
						bol = ValidationInteger(txtDscDay2);
							if (bol==false)
							{
								//alert(error)
								return false;
							}
						bol = IntegerGreaterThenZero(txtDscDay1);
						if (bol==false)
							{
								//alert(error)
								//return false;
							}
						bol = IntegerGreaterThenZero(txtDscDay2);
						if (bol==false)
							{
								//alert(error)
								//return false;
							}
						}
					if (chk_Ret.checked == true)
						{
							
							bol = ChkDateCombo(ddl_CauseMonth);
							if (bol==false)
							{
								//return false;
							}
							bol = ChkDateCombo(ddl_CauseDay);
							if (bol==false)
							{
								//return false;
							}
							bol = ChkDateCombo(ddl_CauseYear);
							if (bol==false)
							{
								//return false;
							}
							bol = ChkDateCombo(ddl_CauseTime);
							if (bol==false)
							{
								//return false;
							}
							bol = chkDatefun(ddl_CauseMonth,ddl_CauseDay,ddl_CauseYear);
							if (bol==false)
							{
								return false;
							}
							bol = CompareTodayDate(ddl_CauseMonth,ddl_CauseDay,ddl_CauseYear);
							if (bol==false)
							{
								//return false;
							}
							bol = ValidationInteger(txt_CauseCrtNo);
							if (bol==false)
							{
								//alert(error)
								return false;
							}
							/*bol = IntegerGreaterThenZero(txt_CauseCrtNo);
							if (bol==false)
							{
								//alert(error)
								return false;
							}*/
							
						}
					if (chk_Show.checked == true)
						{
							
							bol = ChkDateCombo(ddl_ShowMonth);
							if (bol==false)
							{
								//return false;
							}
							bol = ChkDateCombo(ddl_ShowDay);
							if (bol==false)
							{
								//return false;
							}
							bol = ChkDateCombo(ddl_ShowYear);
							if (bol==false)
							{
								//return false;
							}
							bol = ChkDateCombo(ddl_ShowTime);
							if (bol==false)
							{
								//return false;
							}
							bol = chkDatefun(ddl_ShowMonth,ddl_ShowDay,ddl_ShowYear);
							if (bol==false)
							{
								return false;
							}
							bol = CompareTodayDate(ddl_ShowMonth,ddl_ShowDay,ddl_ShowYear);
							if (bol==false)
							{
								//return false;
							}
							bol = ValidationInteger(txt_ShowCrtNo);
							if (bol==false)
							{
								//alert(error)
								return false;
							}
							/*bol = IntegerGreaterThenZero(txt_ShowCrtNo);
							if (bol==false)
							{
								//alert(error)
								return false;
							}*/
							
						}
					
					if (chk_Misc.checked == true)
					{
						if (txt_Misc.value == "")
						{
							//alert("Please enter details of other things required by the judge in the MISC comments box.");
							//txt_Misc.focus();
							//return false;
						}
						
					}
				}
				
				
			else if(ddl_Pending.value == 9)
				{ 
			
					bol = ValidationInteger(txt_TAmount);
					if (bol==false)
						{
							return false;
						}
					bol = ValidationInteger(txt_TDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					var vAmount = txt_TAmount.value;
					if (vAmount <=0)
						{
							//alert("Amount must be greater then zero.");
							//txt_TAmount.focus();
							//return false;
						}
					bol = ValidationInteger(txt_PDuration);
					if (bol==false)
						{
							//alert(error);
							return false;
						}
					bol = IntegerLength(txt_TDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					bol = IntegerLength(txt_PDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					bol = ChkDateCombo(ddl_PAMonth);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PADay);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PAYear);
					if (bol==false)
						{
							//return false;
						}
					bol = chkDatefun(ddl_PAMonth,ddl_PADay,ddl_PAYear);
					if (bol==false)
						{
							return false;
						}
					bol = ChkDateCombo(ddl_PTMonth);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PTDay);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PTYear);
					if (bol==false)
						{
							//return false;
						}
					bol = chkDatefun(ddl_PTMonth,ddl_PTDay,ddl_PTYear);
					if (bol==false)
						{
							return false;
						}	
					bol = CompareTodayDate(ddl_PAMonth,ddl_PADay,ddl_PAYear);
					if (bol==false)
						{
							//return false;
						}
					bol = CompareTodayDate(ddl_PTMonth,ddl_PTDay,ddl_PTYear);
					if (bol==false)
						{
							//return false;
						}
				}
			else if (ddl_Pending.value == 19)
			{
				
				bol = ValidationInteger(txt_TAmount);
					if (bol==false)
						{
							return false;
						}
					var vAmount = txt_TAmount.value;
					if (vAmount <=0)
						{
							//alert("Amount must be greater then zero.");
							//txt_TAmount.focus();
							//return false;
						}
					bol = ValidationInteger(txt_TDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
						bol = IntegerLength(txt_TDuration);
					if (bol==false)
						{
							//alert(error)
							return false;
						}
					
					bol = ChkDateCombo(ddl_PAMonth);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PADay);
					if (bol==false)
						{
							//return false;
						}
					bol = ChkDateCombo(ddl_PAYear);
					if (bol==false)
						{
							//return false;
						}
					bol = chkDatefun(ddl_PAMonth,ddl_PADay,ddl_PAYear);
					if (bol==false)
						{
							return false;
						}
					bol = CompareTodayDate(ddl_PAMonth,ddl_PADay,ddl_PAYear);
					if (bol==false)
						{
							//return false;
						}
				}
				
				}	
			
			return bol;
		}
		
		function IntegerLength(TextBox)
		{
		var error = "Duration is too long";
		var txtval = TextBox.value;
			if(txtval.length > 3)
				{
					alert(error);
					TextBox.focus();
					return false;
				}
			else
					{
						lblMessage.innerText = "";
						return true;
					}
		}
		
		function IntegerGreaterThenZero(TextBox)
		{
		var error = "It cannot be equal to zero";
		var txtval = TextBox.value;
			if(txtval <= 0)
				{
					//alert(error);
					//TextBox.focus();
					//return false;
				}
			else
					{
						lblMessage.innerText = "";
						return true;
					}
		}
		function ValidationInteger(TextBox)
		{
				//alert(TextBox.value);
				var error = "Please correct value. Only Positive Integer value is allowed";
				//var error = "Please correct value. Only Whole number is allowed";
				var lblMessage = document.getElementById("lblMessage");

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
					if (isInteger(TextBox.value)==false) 
					{
						//if -ve and not float
						alert(error);
						lblMessage.innerText = error;
						TextBox.focus();
						
						return false;
					}
					else
					{
						lblMessage.innerText = "";
						return true;
					}
				}
				
		}
		function ValidationFloat(TextBox)
		{
				//alert(TextBox.value);
				
				var error = "Please correct value. Only float value is allowed";
				var lblMessage = document.getElementById("lblMessage");

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
					if (isFloat(TextBox.value)==false) 
					{
						//if -ve and not float
						lblMessage.innerText = error;
						alert(error);
						TextBox.focus();
						
						return false;
					}
					else
					{
						lblMessage.innerText = "";
						return true;
					}
				}
				
			}
		
		function ChkDateCombo(selCombo)
			{
			
			//var selCombo = document.getElementById(cboDate);
			var error = "Please Select correct Date";
			var lblMessage = document.getElementById("lblMessage");			
				//alert (selCombo.selectedIndex);
				if (selCombo.selectedIndex == 0 )  
				{
				
					lblMessage.innerText = error;
					//alert(error);
					//selCombo.focus();
					//return false;
				}
				else
				{
					lblMessage.innerText = "";
					return true;
				}
				
						
			}
		function chkDatefun(mm,dd,yy)
		{
			var error = "Invalid Date";
			var mMonth = mm.selectedIndex;
			var mDay = dd.selectedIndex;
			var mYear = yy.selectedIndex;
			var mDate = mm.options[mMonth].value + "/" + dd.options[mDay].value + "/" + yy.options[mYear].value;
			if (chkdate(mDate)==false && mDate != "---/--/----" )
			{
				alert(error + "  " +mDate);
				//mm.focus();
				return false;
			}
			else
			{
						
				return true;
			}
			
		}
		
		
		function CompareTodayDate(ddlmon1,ddlday1,ddlyrs1)
		{
		var mon = ddlmon1;
		var day = ddlday1;
		var yrs =ddlyrs1;
	
		
		if (mon.selectedIndex == 0 || day.selectedIndex == 0 || yrs.selectedIndex == 0 )  
			{
			
			}
		else
			{
			
				var objDateNow = new Date(); with (objDateNow) setDate(getDate());
				var dd = day.selectedIndex;
				
				var Mon = mon.selectedIndex ;
				Mon  = Mon - 1 ; 
				var yy = yrs.options[yrs.selectedIndex].value * 1
				var objDateCompare = new Date();
				objDateCompare.setFullYear(yy,Mon,dd);				
				
				
				var TdurHidden = Math.ceil(((objDateCompare.getTime()-objDateNow.getTime())/86400)/1000);
				
				if (TdurHidden <= 0)
				{
					//alert(TdurHidden);
					//alert ("Please select the future Date");
					//mon.focus();
					//return false;
				} 
			}
		}
		
    </script>

    <script language="javascript">
		
		
		function restoreControls(ctrlref)
		{
			
				var ctlAmount = ctrlref + "_txt_TAmount";
				var ctlTDuration = ctrlref + "_txt_TDuration";
				var ctlPDuration = ctrlref + "_txt_PDuration";
				
				
				var ctlPAMonth = ctrlref + "_ddl_PAMonth";
				var ctlPADay = ctrlref + "_ddl_PADay";
				var ctlPAYear = ctrlref + "_ddl_PAYear";
				
				var ctlPTMonth = ctrlref + "_ddl_PTMonth";
				var ctlPTDay = ctrlref + "_ddl_PTDay";
				var ctlPTYear = ctrlref + "_ddl_PTYear";
				
				var ctlCauseMonth = ctrlref + "_ddl_CauseMonth";
				var ctlCauseDay = ctrlref + "_ddl_CauseDay";
				var ctlCauseYear = ctrlref + "_ddl_CauseYear";
				var ctlCauseTime = ctrlref + "_ddl_CauseTime";
				var ctlCauseCrtNo = ctrlref + "_txt_CauseCrtNo";
				
				var ctlShowMonth = ctrlref + "_ddl_ShowMonth";
				var ctlShowDay = ctrlref + "_ddl_ShowDay";
				var ctlShowYear = ctrlref + "_ddl_ShowYear";
				var ctlShowTime = ctrlref + "_ddl_ShowTime";
				var ctlShowCrtNo = ctrlref + "_txt_ShowCrtNo";
				
				var ctlChkRet =ctrlref + "_chk_Ret";
				var ctlChkIns =ctrlref + "_chk_Ins";
				var ctlChkMisc =ctrlref + "_chk_Misc";
				var ctlChkDsc =ctrlref + "_chk_Dsc";
				var ctlChkShow =ctrlref + "_chk_ShowCause";
				
				var ctrlDscDay1 =ctrlref + "_txt_DSCDay1";
				var ctrlDscDay2 =ctrlref + "_txt_DSCDay2";
				
				var txtMisc = ctrlref + "_txt_Misc";
				
				var txt_TAmount =document.getElementById(ctlAmount);	
				var txt_TDuration = document.getElementById(ctlTDuration);	
				var txt_PDuration = document.getElementById(ctlPDuration);	
						
				
				var ddl_PAMonth =  document.getElementById(ctlPAMonth);
				var ddl_PADay =  document.getElementById(ctlPADay);
				var ddl_PAYear =  document.getElementById(ctlPAYear) ;
				
				var ddl_PTMonth = document.getElementById(ctlPTMonth);
				var ddl_PTDay = document.getElementById(ctlPTDay);
				var ddl_PTYear = document.getElementById(ctlPTYear);
				
				var ddl_CauseMonth = document.getElementById(ctlCauseMonth);
				var ddl_CauseDay = document.getElementById(ctlCauseDay);  
				var ddl_CauseYear = document.getElementById(ctlCauseYear);
				var ddl_CauseTime = document.getElementById(ctlCauseTime);
				var txt_CauseCrtNo = document.getElementById(ctlCauseCrtNo);
				
				var ddl_ShowMonth = document.getElementById(ctlShowMonth);
				var ddl_ShowDay = document.getElementById(ctlShowDay);  
				var ddl_ShowYear = document.getElementById(ctlShowYear);
				var ddl_ShowTime = document.getElementById(ctlShowTime);
				var txt_ShowCrtNo = document.getElementById(ctlShowCrtNo);
				
				var txtDscDay1 = document.getElementById(ctrlDscDay1);
				var txtDscDay2 = document.getElementById(ctrlDscDay2);
				
				var chk_Ret = document.getElementById(ctlChkRet);
				var chk_Ins = document.getElementById(ctlChkIns);
				var chk_Misc = document.getElementById(ctlChkMisc);
				var chk_Dsc = document.getElementById(ctlChkDsc);
				var chk_Show = document.getElementById(ctlChkShow);
				
				var T4 = document.getElementById(ctrlref + "_tdMisc" );
			
				var txt_Misc = document.getElementById(txtMisc);
				
				 txt_TAmount.value = "";
				 txt_TDuration.value = "";
				 txt_PDuration.value = ""; 
						
				
				 ddl_PAMonth.selectedIndex =0 ;
				 ddl_PADay.selectedIndex =0 ;
				 ddl_PAYear.selectedIndex =0 ;
				
				 ddl_PTMonth.selectedIndex =0 ;
				 ddl_PTDay.selectedIndex =0 ;
				 ddl_PTYear.selectedIndex =0 ;
				
				 ddl_CauseMonth.selectedIndex =0 ;
				 ddl_CauseDay.selectedIndex =0 ;
				 ddl_CauseYear.selectedIndex =0 ; 
				 ddl_CauseTime.selectedIndex =0 ; 
				 txt_CauseCrtNo.value = ""; 
				
				ddl_ShowMonth.selectedIndex =0 ;
				ddl_ShowDay.selectedIndex =0 ;
				ddl_ShowYear.selectedIndex =0 ;
				ddl_ShowTime.selectedIndex =0 ;
				txt_ShowCrtNo.value = "";
				
				txtDscDay1.value =0; 
				txtDscDay2.value =0; 
				
				chk_Ret.checked = false;
				chk_Ins.checked = false;
				chk_Misc.checked = false; 
				chk_Dsc.checked = false; 
				chk_Show.checked = false; 
				
				txt_Misc.value= ""; 
				T4.style.display = 'none';
		}

		function setTableRows(crtlname)
		{
		var ctrl = document.getElementById(crtlname);
		
		//var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_Details";
			var idx=2;
			var bol = true;
			//alert("asli");
			InStr = crtlname.length - 12;
				
				//var txtctrl = grdName+ "__ctl" + idx;
				var txtctrl = crtlname.substring(0,InStr);
				var tt1 = txtctrl + "_t1";
				var tt2 = txtctrl + "_t2";
				var tt3 = txtctrl + "_t3";
				var tt4 = txtctrl + "_t4";
				var tt5 = txtctrl + "_t5";
				var tt6 = txtctrl + "_t6";
				var casevio = txtctrl + "_lbl_CrtVio";
					//	alert(txtctrl);
					
				restoreControls(txtctrl);
		//yah per
		var T1 = document.getElementById(tt1);
		var T2 = document.getElementById(tt2);
		var T3 = document.getElementById(tt3);
		var T4 = document.getElementById(tt4);
		var T5 = document.getElementById(tt5);
		var T6 = document.getElementById(tt6);
		var txtCStatus = document.getElementById(casevio);
		
		//alert(casevio);
		//alert(ctrl.value);
		//alert(txtCStatus.innerText);
		//var T1 = document.getElementById(crtlname.substring(0,17) + "t1");
		//var T2 = document.getElementById(crtlname.substring(0,17) + "t2");
		//var T3 = document.getElementById(crtlname.substring(0,17) + "t3");
		//var T4 = document.getElementById(crtlname.substring(0,17) + "t4");
		

			if( ctrl.value == 18)
			{ //alert("1");
				T1.style.display = 'block' ;
				T2.style.display = 'block' ;
				T3.style.display = 'block' ;
				T4.style.display = 'none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
				
				var T4_1 = document.getElementById(txtctrl + "_T1lb_Amount" );
				T4_1.innerText = "   Probation Amount and Time";
				var T4_2 = document.getElementById(txtctrl + "_T2lb_Pay" );
				T4_2.innerText = "   Probation Time To Pay";		
							
			}
			else if (ctrl.value == 19)
			{
			// alert("2");
				T1.style.display = 'block' ;
				T2.style.display = 'none' ;
				T3.style.display = 'none' ;
				T4.style.display = 'none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
				//T1.height = 19;
				var T4_1 = document.getElementById(txtctrl + "_T1lb_Amount" );
				//T4_1.innerText = "    Fine Amount and Time";	
				T4_1.innerText = "   Fine Amount and Time";	
										
	
				
				var T4_Misc = document.getElementById(txtctrl + "_tdMisc" );	
				T4_Misc.style.display = 'none';
			}
			else if (ctrl.value == 9)
			{
			 //alert("3");
				T1.style.display = 'block' ;
				T2.style.display = 'block' ;
				T3.style.display = 'none' ;
				T4.style.display = 'none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
				
				var T4_1 = document.getElementById(txtctrl + "_T1lb_Amount" );
				T4_1.innerText = "   DSC Amount and Time";	
				
				var T4_2 = document.getElementById(txtctrl + "_T2lb_Pay" );
				T4_2.innerText = "   DSC Time To Pay";					
			
			}/*
			else if (((ctrl.value == 24) || (ctrl.value == 16)) && (txtCaseStatus.innerText == "50") )
			{
			 alert("4");
				alert ("It is not an appropriate violation outcome");
				T1.style.display = 'none' ;
				T2.style.display = 'none' ;
				T3.style.display = 'none' ;
				T4.style.display  ='none' ;
			}
			else if ((txtCaseStatus.innerText == "10") || (txtCaseStatus.innerText == "8") || (txtCaseStatus.innerText == "6") || (txtCaseStatus.innerText == "0"))
			{
			 alert("5");
				alert ("Select Case Status");
				T1.style.display = 'none' ;
				T2.style.display = 'none' ;
				T3.style.display = 'none' ;
				T4.style.display  ='none' ;
			}*/
			else
			{ 
			// alert("6");
				T1.style.display = 'none' ;
				T2.style.display = 'none' ;
				T3.style.display = 'none' ;
				T4.style.display  ='none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
			}
			
			
			
		
		}
		
		function setTableRows_OnLoad(crtlname)
		{
		var ctrl = document.getElementById(crtlname);
		
		//var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_Details";
			var idx=2;
			var bol = true;
			//alert("asli");
			InStr = crtlname.length - 12;
				
				//var txtctrl = grdName+ "__ctl" + idx;
				var txtctrl = crtlname.substring(0,InStr);
				var tt1 = txtctrl + "_t1";
				var tt2 = txtctrl + "_t2";
				var tt3 = txtctrl + "_t3";
				var tt4 = txtctrl + "_t4";
				var tt5 = txtctrl + "_t5";
				var tt6 = txtctrl + "_t6";
				var casevio = txtctrl + "_lbl_CrtVio";

		var T1 = document.getElementById(tt1);
		var T2 = document.getElementById(tt2);
		var T3 = document.getElementById(tt3);
		var T4 = document.getElementById(tt4);
		var T5 = document.getElementById(tt5);
		var T6 = document.getElementById(tt6);
		var txtCStatus = document.getElementById(casevio);
		
		
		

			if( ctrl.value == 18)
			{ //alert("1");
				T1.style.display = 'block' ;
				T2.style.display = 'block' ;
				T3.style.display = 'block' ;
				T4.style.display = 'none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
				
				var T4_1 = document.getElementById(txtctrl + "_T1lb_Amount" );
				T4_1.innerText = "   Probation Amount and Time";
				var T4_2 = document.getElementById(txtctrl + "_T2lb_Pay" );
				T4_2.innerText = "   Probation Time To Pay";		
							
			}
			else if (ctrl.value == 19)
			{
			// alert("2");
				T1.style.display = 'block' ;
				T2.style.display = 'none' ;
				T3.style.display = 'none' ;
				T4.style.display = 'none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
				//T1.height = 19;
				var T4_1 = document.getElementById(txtctrl + "_T1lb_Amount" );
				//T4_1.innerText = "    Fine Amount and Time";	
				T4_1.innerText = "   Fine Amount and Time";	
										
	
				
				var T4_Misc = document.getElementById(txtctrl + "_tdMisc" );	
				T4_Misc.style.display = 'none';
			}
			else if (ctrl.value == 9)
			{
			 //alert("3");
				T1.style.display = 'block' ;
				T2.style.display = 'block' ;
				T3.style.display = 'none' ;
				T4.style.display = 'none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
				
				var T4_1 = document.getElementById(txtctrl + "_T1lb_Amount" );
				T4_1.innerText = "   DSC Amount and Time";	
				
				var T4_2 = document.getElementById(txtctrl + "_T2lb_Pay" );
				T4_2.innerText = "   DSC Time To Pay";					
			
			}
			else
			{ 
			// alert("6");
				T1.style.display = 'none' ;
				T2.style.display = 'none' ;
				T3.style.display = 'none' ;
				T4.style.display  ='none' ;
				T5.style.display = 'none' ;
				T6.style.display = 'none' ;
			}
			
			
			
		
		}
		
		function setTableRowCauseDate_2 (chkCtrl)
		{ 
		var chkRet = document.getElementById(chkCtrl);
		var InStr = chkCtrl.length - 7;
		
		var T4 = document.getElementById(chkCtrl.substring(0,InStr) + "t4");
		var ddlMon = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_CauseMonth");
		var ddlDay = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_CauseDay");
		var ddlYear = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_CauseYear");
		var ddlTime = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_CauseTime");
		var txtCrtNo = document.getElementById(chkCtrl.substring(0,InStr) + "txt_CauseCrtNo");
				
		if (chkRet.checked == false )
				{
				ddlMon.selectedIndex = 0;
				ddlDay.selectedIndex = 0; 
				ddlYear.selectedIndex = 0; 
				ddlTime.selectedIndex = 0;
				txtCrtNo.value = 0;
				 T4.style.display = 'none';
				 }
		
		else 
				{
				// alert("To select RET, the case status should be SHOW CAUSE. If ok, please enter show cause date.");
				 T4.style.display = 'block';
				 }
		}
		
		function setTableRowCauseDate(chkCtrl)
		{
		var chkRet = document.getElementById(chkCtrl);
		var InStr = chkCtrl.length - 7;
		
		var T4 = document.getElementById(chkCtrl.substring(0,InStr) + "t4");
		
		if (chkRet.checked == false )
				{
				 T4.style.display = 'none';
				 }
		
		else 
				{
				 //alert("To select RET, the case status should be SHOW CAUSE. If ok, please enter show cause date.");
				 T4.style.display = 'block';
				 }
		
		}
		
		function setTableRowDSC_2 (chkCtrl)
		{ //setTableRowCauseDate_2
		var chkRet = document.getElementById(chkCtrl);
		var InStr = chkCtrl.length - 7;
		
		var T4 = document.getElementById(chkCtrl.substring(0,InStr) + "t5");
		var txtDscDay1 =document.getElementById(chkCtrl.substring(0,InStr) + "txt_DSCDay1");
		var txtDscDay2 =document.getElementById(chkCtrl.substring(0,InStr) + "txt_DSCDay2");
				
		if (chkRet.checked == false )
				{
				txtDscDay1.value = 0;
				txtDscDay2.value = 0;
				 T4.style.display = 'none';
				 }
		
		else 
				{
				// alert("To select DSC, the case status should be SHOW CAUSE. If ok, please enter show cause date.");
				 T4.style.display = 'block';
				 }
		}
		
		function setTableRowDSC(chkCtrl)
		{
		var chkRet = document.getElementById(chkCtrl);
		var InStr = chkCtrl.length - 7;
		
		var T4 = document.getElementById(chkCtrl.substring(0,InStr) + "t5");
		
		if (chkRet.checked == false )
				{
				 T4.style.display = 'none';
				 }
		
		else 
				{
				 //alert("To select RET, the case status should be SHOW CAUSE. If ok, please enter show cause date.");
				 T4.style.display = 'block';
				 }
		
		}
		function setTableRowShowCause_2 (chkCtrl)
		{ 
		var chkRet = document.getElementById(chkCtrl);
		var InStr = chkCtrl.length - 13;
		
		var T4 = document.getElementById(chkCtrl.substring(0,InStr) + "t6");
		var ddlMon = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_ShowMonth");
		var ddlDay = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_ShowDay");
		var ddlYear = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_ShowYear");
		var ddlTime = document.getElementById(chkCtrl.substring(0,InStr) + "ddl_ShowTime");
		var txtCrtNo = document.getElementById(chkCtrl.substring(0,InStr) + "txt_ShowCrtNo");
		
		if (chkRet.checked == false )
				{
				ddlMon.selectedIndex = 0;
				ddlDay.selectedIndex = 0; 
				ddlYear.selectedIndex = 0; 
				ddlTime.selectedIndex = 0;
				txtCrtNo.value = 0;
				 T4.style.display = 'none';
				 }
		
		else 
				{
				// alert("To select RET, the case status should be SHOW CAUSE. If ok, please enter show cause date.");
				 T4.style.display = 'block';
				 }
		}
		
		function setTableRowShowCause(chkCtrl)
		{
		var chkRet = document.getElementById(chkCtrl);
		var InStr = chkCtrl.length - 13;
		
		var T4 = document.getElementById(chkCtrl.substring(0,InStr) + "t6");
		
		if (chkRet.checked == false )
				{
				 T4.style.display = 'none';
				 }
		
		else 
				{
				 //alert("To select RET, the case status should be SHOW CAUSE. If ok, please enter show cause date.");
				 T4.style.display = 'block';
				 }
		
		}
		
	
		
		function setTableRowMiscComments_2(chkMiscCtrl)
		{
		var chkMisc = document.getElementById(chkMiscCtrl);
		var InStr = chkMiscCtrl.length - 8;
		var T4 = document.getElementById(chkMiscCtrl.substring(0,InStr) + "tdMisc" );
		var txtMisc = document.getElementById(chkMiscCtrl.substring(0,InStr) + "txt_Misc" );	
		if (chkMisc.checked == false )
				{
				txtMisc.value = "";
				 T4.style.display = 'none';
				 }
		else 
				{
				 alert("Please enter details of other things required by the judge in the MISC comments box.");				
				 T4.style.display = 'block';
				 txtMisc.focus();
				 }
		}
		
		function setTableRowMiscComments(chkMiscCtrl)
		{
		var chkMisc = document.getElementById(chkMiscCtrl);
		var InStr = chkMiscCtrl.length - 8;
		var T4 = document.getElementById(chkMiscCtrl.substring(0,InStr) + "tdMisc" );
			
		if (chkMisc.checked == false )
				{
				 T4.style.display = 'none';
				 }
		else 
				{
				 //alert("Please enter details of other things required by the judge in the MISC comments box.");				
				 T4.style.display = 'block';
				 }
		}
		
		function  setTableRowDate(lbDuration,ddlMM,ddlDD,ddlYY,ChkType,CrtDay,CrtMonth,CrtYear)
		{
		
		var lbDays = document.getElementById(lbDuration);
		
		var mm = document.getElementById(ddlMM);
		var dd = document.getElementById(ddlDD);
		var yy = document.getElementById(ddlYY);
		
		// Added By Zeeshan Ahmed
		// Getting Court Date     
        var d = new Date();
        d.setDate(CrtDay);
        d.setMonth(parseInt(CrtMonth)-1);
        d.setFullYear(parseInt(CrtYear));
               					
		var dur = lbDays.value ;
			
		if (dur >= 0) 
			{
				//var D = new Date() ; with (D) setDate(getDate() + dur * 1 );
				//Added By Zeeshan Ahmed
				var D = new Date() ;
				D.setMonth( d.getMonth());
			    D.setFullYear(d.getFullYear());
				D.setDate( d.getDate() + dur * 1 );  
			
				mm.selectedIndex = D.getMonth() + 1 ; 
				dd.selectedIndex = D.getDate();
				//yy.selectedvalue = D.getFullYear();
			
				for (var i =0 ; i < yy.length ; i++)
				{
					if ((yy.options[i].value * 1) == D.getFullYear())
					{
					yy.selectedIndex = i;
					
					break;
					}
				}
			}
			else if (dur == "" )
			{
				chkDatefun(mm,dd,yy);
				mm.selectedIndex = 0; 
				dd.selectedIndex = 0;
				yy.selectedIndex = 0;
			}
				
	}
		
    </script>

    <script language="javascript">
		function ChangeBGColor(pCell, pMouseStatus)
			{
				if (pMouseStatus == 'in')
					pCell.style.backgroundColor = '#FFF2D9';
				else
					pCell.style.backgroundColor = '#EFF4FB';
				
			}	
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="checkAdmin()" onkeypress="pressKey();">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <div id="Disable" style="display: none; filter: alpha(opacity=50); left: 1px; width: 15px;
        position: absolute; top: 1px; height: 1px; background-color: silver">
        <table height="100%" width="100%">
            <tr>
                <td style="width: 100%; height: 100%">
                </td>
            </tr>
        </table>
    </div>
    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table id="MainTable" cellspacing="0" cellpadding="0" width="770" align="center"
                border="0">
                <tbody>
                    <tr>
                        <td bgcolor="white" colspan="4">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="width: 800px" background="../images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="test" runat="server" ForeColor="Red" CssClass="label" Font-Bold="True"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btn_UpdateInfo" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="btn_UpdateDetails" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="Click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table4" cellspacing="0" cellpadding="0" width="100%" bgcolor="white" border="0">
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <aspnew:UpdatePanel ID="UpdatePanel122" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red" Visible="False" CssClass="Label"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aspnew:AsyncPostBackTrigger ControlID="btn_UpdateInfo" EventName="Click" />
                                                    <aspnew:AsyncPostBackTrigger ControlID="btn_UpdateDetails" EventName="Click" />
                                                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="Click" />
                                                </Triggers>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px">
                                            &nbsp;<asp:Label ID="lb_LastName" runat="server" CssClass="Label"></asp:Label>&nbsp;,<asp:Label
                                                ID="lb_firstName" runat="server" CssClass="Label"></asp:Label>&nbsp;, (<asp:Label
                                                    ID="lbl_CaseCount" runat="server" CssClass="label" ForeColor="Black"></asp:Label>)
                                            , &nbsp;
                                            <asp:HyperLink ID="hlk_MIDNo" runat="server"></asp:HyperLink>
                                        </td>
                                        <td style="width: 10%; height: 20px" align="right">
                                            <asp:Button ID="btnNext" runat="server" Width="55px" CssClass="clsbutton" Text="Next"
                                                Visible="False"></asp:Button>
                                        </td>
                                        <td style="width: 10%; height: 20px" align="right">
                                            <asp:Button ID="btn_UpdateInfo" runat="server" CssClass="clsbutton" Text="Update">
                                            </asp:Button>&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="read" colspan="2" runat="server">
                            <uc2:ReadNotes ID="ReadNotes1" runat="server"></uc2:ReadNotes>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" style="border-color: Red;" width="100%">
                                <tr>
                                    <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                        &nbsp;General Setting Information
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="display: none" id="Table6" class="clsleftpaddingtable" bordercolor="white"
                                cellspacing="0" cellpadding="0" width="100%" bgcolor="white" border="1">
                                <tbody>
                                    <tr class="clsLeftPaddingTable">
                                        <td id="CS" class="clsaspcolumnheader" width="150" style="height: 20px">
                                            Case Status
                                        </td>
                                        <td id="CD" class="clsaspcolumnheader" width="180" style="height: 20px">
                                            Crt Date
                                        </td>
                                        <td id="CT" class="clsaspcolumnheader" width="100" style="height: 20px">
                                            Crt Time
                                        </td>
                                        <td id="CN" class="clsaspcolumnheader" width="80" style="height: 20px">
                                            Crt No
                                        </td>
                                    </tr>
                                    <tr class="clsleftpaddingtable">
                                        <td id="CS1" style="height: 20px">
                                            <asp:Label ID="lbl_CaseStatus" runat="server" CssClass="label" ForeColor="#123160"></asp:Label>
                                        </td>
                                        <td id="CD1" style="height: 20px">
                                            <asp:Label ID="lbl_CrtDate" runat="server" CssClass="label" ForeColor="#123160"></asp:Label>
                                        </td>
                                        <td id="CT1" style="height: 20px">
                                            <asp:Label ID="lbl_Crttime" runat="server" CssClass="label" ForeColor="#123160"></asp:Label>
                                        </td>
                                        <td id="CN1" style="height: 20px">
                                            <asp:Label ID="lbl_CrtNo" runat="server" CssClass="label" ForeColor="#123160"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 11px" width="780" background="../../images/separator_repeat.gif"
                                            colspan="5">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <script type="text/javascript">
				    
	       
       
        var postbackElement;
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
        
        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
        }
        function pageLoaded(sender, args) {
            setTableData();
                 
        }
		    
				    
                            </script>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableGrid" cellspacing="0" cellpadding="0" width="780" bgcolor="white"
                                border="0">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <aspnew:UpdatePanel ID="uppnl_gd_details" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table id="tbl_violations">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table class="clsleftpaddingtable" bordercolor="white" cellspacing="0" cellpadding="0"
                                                                        width="100%" border="1" id="table_name" runat="server">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="clsaspcolumnheader" width="120" height="19" id="td_ticketname" runat="server">
                                                                                    Ticket No
                                                                                </td>
                                                                                <td class="clsaspcolumnheader" width="100" height="19">
                                                                                    Cause Number
                                                                                </td>
                                                                                <td class="clsaspcolumnheader" width="151" height="19">
                                                                                    Matter &nbsp;Description
                                                                                </td>
                                                                                <td class="clsaspcolumnheader" width="200" height="19">
                                                                                    Case Info
                                                                                </td>
                                                                                <td class="clsaspcolumnheader" align="left" width="120" height="19">
                                                                                    Violation Outcome&nbsp; &nbsp;
                                                                                </td>
                                                                                <td align="center" class="clsaspcolumnheader" height="19" width="110">
                                                                                    Coverage
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <asp:DataGrid ID="dg_Details" runat="server" Width="800px" CssClass="clsleftpaddingtable"
                                                                        BorderColor="White" BorderStyle="None" AutoGenerateColumns="False" ShowHeader="False">
                                                                        <Columns>
                                                                            <asp:TemplateColumn>
                                                                                <ItemTemplate>
                                                                                    <table id="Table2" runat="server" bordercolor="white" cellspacing="1" cellpadding="0"
                                                                                        width="780" border="0">
                                                                                        <tr class="clsleftpaddingtable" onmouseover="ChangeBGColor(this, 'in');" onmouseout="ChangeBGColor(this, 'out');"
                                                                                            height="19">
                                                                                            <td width="110" height="19" id="td_ticket" runat="server">
                                                                                                <asp:LinkButton ID="lnkbtn_TicketNo" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNum") %>'
                                                                                                    CausesValidation="False">
                                                                                                </asp:LinkButton>
                                                                                            </td>
                                                                                            <td align="left" width="100" id="td_causeno" runat="server">
                                                                                                <asp:Label ID="lbl_CauseNo" runat="server" CssClass="label" ForeColor="#123160" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'>
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td width="110" height="19" id="td_lcausenumber" runat="server">
                                                                                                <asp:LinkButton ID="lnkcausenumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'
                                                                                                    CausesValidation="False">
                                                                                                </asp:LinkButton>
                                                                                            </td>
                                                                                            <td align="left" width="126" height="19" id="td_desc" runat="server">
                                                                                                <asp:Label ID="lb_VDesc" runat="server" CssClass="label" ForeColor="#123160" Text='<%# DataBinder.Eval(Container, "DataItem.descriptions") %>'>
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td align="left" width="350" colspan="2" height="19" id="td_status" runat="server">
                                                                                                <asp:Label ID="lb_CrtStatus" runat="server" CssClass="label" ForeColor="#123160"
                                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.CourtViolationStatusID") %>'>
                                                                                                </asp:Label>&nbsp;
                                                                                                <asp:Label ID="lb_CrtInfo" runat="server" CssClass="label" ForeColor="#123160" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate", "{0: MM/dd/yy}") %>'
                                                                                                    BorderStyle="None">
                                                                                                </asp:Label><font color="#123160">@</font>
                                                                                                <asp:Label ID="Label2" runat="server" CssClass="label" ForeColor="#123160" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDate", "{0:hh:mm tt}") %>'>
                                                                                                </asp:Label><font color="#123160">#</font>
                                                                                                <asp:Label ID="lb_CrtNo" runat="server" CssClass="label" ForeColor="#123160" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNum") %>'>
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td align="left" width="195" height="19">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:DropDownList ID="ddl_pending" runat="server" CssClass="clsinputcombo" Width="100px">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td align="right">
                                                                                                            <asp:DropDownList ID="ddlcoveringfirms" runat="server" CssClass="clsinputcombo">
                                                                                                            </asp:DropDownList>
                                                                                                            <asp:Label ID="lblfirmname" runat="server" Visible="false" CssClass="clslabel"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="right">
                                                                                                            <img id="Imgtoggle" runat="server" alt="" src="../Images/Rightarrow.gif" visible="false" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="clsleftpaddingtable" id="t1" onmouseover="ChangeBGColor(this, 'in');"
                                                                                            onmouseout="ChangeBGColor(this, 'out');" height="19" runat="server">
                                                                                            <td class="clslabel" id="T1lb_Amount" width="251" colspan="2" height="19" runat="server"
                                                                                                cssclass="Label">
                                                                                                &nbsp;&nbsp;
                                                                                                <asp:Label ID="lb_Amount" runat="server" CssClass="clslabel">Probation Amount & Time: </asp:Label>
                                                                                            </td>
                                                                                            <td align="right" width="305" colspan="3" height="19">
                                                                                                $
                                                                                                <asp:TextBox ID="txt_TAmount" runat="server" CssClass="clsinputadministration" Width="45px"
                                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.DSCProbationAmount","{0:G6}") %>'
                                                                                                    Height="18px">
                                                                                                </asp:TextBox>
                                                                                                <asp:TextBox ID="txt_TDuration" runat="server" CssClass="clsinputadministration"
                                                                                                    Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.DSCProbationTime") %>'
                                                                                                    MaxLength="3" Height="18px">
                                                                                                </asp:TextBox>
                                                                                                <asp:DropDownList ID="ddl_PAMonth" runat="server" CssClass="clsinputcombo" Width="48px"
                                                                                                    Height="22px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_PADay" runat="server" CssClass="clsinputcombo" Width="48px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_PAYear" runat="server" CssClass="clsinputcombo" Width="51px">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td class="clslabel" id="tdMisc" valign="top" align="left" width="220" height="19"
                                                                                                rowspan="2" runat="server">
                                                                                                Misc Comments:
                                                                                                <asp:TextBox ID="txt_Misc" runat="server" CssClass="clstextarea" Width="200px" Text='<%# DataBinder.Eval(Container, "DataItem.MiscComments") %>'
                                                                                                    TextMode="MultiLine" Height="43px">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="clsleftpaddingtable" id="t2" onmouseover="ChangeBGColor(this, 'in');"
                                                                                            onmouseout="ChangeBGColor(this, 'out');" height="19" runat="server">
                                                                                            <td class="clslabel" id="T2lb_Pay" width="251" colspan="2" runat="server" cssclass="Label">
                                                                                                &nbsp;&nbsp;
                                                                                                <asp:Label ID="lb_Pay" runat="server" CssClass="clslabel">Probation Time to Pay: </asp:Label>
                                                                                            </td>
                                                                                            <td align="right" width="305" colspan="3">
                                                                                                <asp:TextBox ID="txt_PDuration" runat="server" CssClass="clsinputadministration"
                                                                                                    Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.TimeToPay") %>' MaxLength="3"
                                                                                                    Height="18px">
                                                                                                </asp:TextBox>
                                                                                                <asp:DropDownList ID="ddl_PTMonth" runat="server" CssClass="clsinputcombo" Width="48px"
                                                                                                    Height="22px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_PTDay" runat="server" CssClass="clsinputcombo" Width="48px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_PTYear" runat="server" CssClass="clsinputcombo" Width="51px">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="clsleftpaddingtable" id="t3" onmouseover="ChangeBGColor(this, 'in');"
                                                                                            onmouseout="ChangeBGColor(this, 'out');" height="19" runat="server">
                                                                                            <td class="clslabel" width="251" colspan="2" height="36">
                                                                                                &nbsp;&nbsp;
                                                                                                <asp:Label ID="lb_Terms" runat="server" CssClass="clslabel" Height="18px">Terms :</asp:Label>
                                                                                                <asp:Label ID="lb_HidPayDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TimeToPayDate") %>'
                                                                                                    Height="18px" Visible="False">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td align="right" width="305" colspan="3" height="36">
                                                                                                <asp:CheckBox ID="chk_Dsc" runat="server" CssClass="clslabel" Text="DSC" Checked='<%# DataBinder.Eval(Container, "DataItem.IsDSCSelected") %>'>
                                                                                                </asp:CheckBox>
                                                                                                <asp:CheckBox ID="chk_Ins" runat="server" CssClass="clslabel" Text="INS" Checked='<%# DataBinder.Eval(Container, "DataItem.IsInsSelected") %>'>
                                                                                                </asp:CheckBox>
                                                                                                <asp:CheckBox ID="chk_Ret" runat="server" CssClass="clslabel" Text="RET" Checked='<%# DataBinder.Eval(Container, "DataItem.IsRETSelected") %>'>
                                                                                                </asp:CheckBox>
                                                                                                <asp:CheckBox ID="chk_ShowCause" runat="server" CssClass="clslabel" Text="SHOW CAUSE"
                                                                                                    Checked='<%# DataBinder.Eval(Container, "DataItem.IsShowCauseSelected") %>'>
                                                                                                </asp:CheckBox>
                                                                                                <asp:CheckBox ID="chk_Misc" runat="server" CssClass="clslabel" Text="MISC" Checked='<%# DataBinder.Eval(Container, "DataItem.IsMISCSelected") %>'>
                                                                                                </asp:CheckBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lb_HidCauseDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.RetDate") %>'
                                                                                                    Height="18px" Visible="False">
                                                                                                </asp:Label>
                                                                                                <asp:Label ID="lbl_Ulinks" runat="server" CssClass="label" Visible="False"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="clsleftpaddingtable" id="t5" onmouseover="ChangeBGColor(this, 'in');"
                                                                                            onmouseout="ChangeBGColor(this, 'out');" height="19" runat="server">
                                                                                            <td align="right" width="556" colspan="5">
                                                                                                <asp:Label ID="Label3" runat="server" CssClass="clslabel">Complete Course by (Days) / Driving Record by (Days): </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <asp:TextBox ID="txt_DSCDay1" runat="server" CssClass="clsinputadministration" Width="45px"
                                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.CourseCompletionDays") %>' Height="18px">
                                                                                                </asp:TextBox>/
                                                                                                <asp:TextBox ID="txt_DSCDay2" runat="server" CssClass="clsinputadministration" Width="45px"
                                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.DrivingRecordsDays") %>' Height="18px">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td width="220">
                                                                                                <asp:HyperLink ID="hyp_TicketNo" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNum") %>'
                                                                                                    Visible="False">
                                                                                                </asp:HyperLink>
                                                                                                <asp:Label ID="lbl_HidShowDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ShowCauseDate") %>'
                                                                                                    Visible="False">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="clsleftpaddingtable" id="t4" onmouseover="ChangeBGColor(this, 'in');"
                                                                                            onmouseout="ChangeBGColor(this, 'out');" height="19" runat="server">
                                                                                            <td class="clslabel" width="251" colspan="2" cssclass="Label">
                                                                                                &nbsp;&nbsp;
                                                                                                <asp:Label ID="lb_ShowCause" runat="server" CssClass="clslabel">Return to Court Date / Time / Court Number:</asp:Label>
                                                                                            </td>
                                                                                            <td align="right" width="305" colspan="3">
                                                                                                <asp:DropDownList ID="ddl_CauseMonth" runat="server" CssClass="clsinputcombo" Width="48px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_CauseDay" runat="server" CssClass="clsinputcombo" Width="48px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_CauseYear" runat="server" CssClass="clsinputcombo" Width="51px">
                                                                                                </asp:DropDownList>
                                                                                                /
                                                                                                <asp:DropDownList ID="ddl_CauseTime" runat="server" CssClass="clsinputcombo" Width="70px">
                                                                                                </asp:DropDownList>
                                                                                                /
                                                                                                <asp:TextBox ID="txt_CauseCrtNo" runat="server" CssClass="clsinputadministration"
                                                                                                    Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.RetCourtNumber") %>'
                                                                                                    Height="18px">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td style="visibility: hidden;" width="220" rowspan="1">
                                                                                                <asp:Label ID="lbl_CrtVio" runat="server" CssClass="label" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.CrtViolationtype") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:Label ID="lb_TicketsViolationID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>'
                                                                                                    Visible="False">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="clsleftpaddingtable" id="t6" onmouseover="ChangeBGColor(this, 'in');"
                                                                                            onmouseout="ChangeBGColor(this, 'out');" height="19" runat="server">
                                                                                            <td width="251" colspan="2">
                                                                                                &nbsp;
                                                                                                <asp:Label ID="Label4" runat="server" CssClass="clslabel">Show Cause Date / Time / Court Number:</asp:Label>
                                                                                            </td>
                                                                                            <td align="right" width="305" colspan="3">
                                                                                                <asp:DropDownList ID="ddl_ShowMonth" runat="server" CssClass="clsinputcombo" Width="48px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_ShowDay" runat="server" CssClass="clsinputcombo" Width="48px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:DropDownList ID="ddl_ShowYear" runat="server" CssClass="clsinputcombo" Width="51px">
                                                                                                </asp:DropDownList>
                                                                                                /
                                                                                                <asp:DropDownList ID="ddl_ShowTime" runat="server" CssClass="clsinputcombo" Width="70px">
                                                                                                </asp:DropDownList>
                                                                                                /
                                                                                                <asp:TextBox ID="txt_ShowCrtNo" runat="server" CssClass="clsinputadministration"
                                                                                                    Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.ShowCauseCourtNumber") %>'
                                                                                                    Height="18px">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td width="220">
                                                                                                <asp:Label ID="Label1" runat="server" CssClass="label" Width="24px" Text='<%# DataBinder.Eval(Container, "DataItem.violationstatusid") %>'
                                                                                                    Visible="False">
                                                                                                </asp:Label>
                                                                                                <asp:Label ID="lb_HidAmountDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DSCProbationDate") %>'
                                                                                                    Visible="False">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>' />
                                                                                    <asp:HiddenField ID="hf_bondamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.Bondamount","{0:F0}") %>' />
                                                                                    <asp:HiddenField ID="hf_fineamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.FineAmount","{0:F0}") %>' />
                                                                                    <asp:HiddenField ID="hf_courtloc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.shortname") %>' />
                                                                                    <asp:HiddenField ID="hf_priceplan" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' />
                                                                                    <asp:HiddenField ID="hf_bondflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' />
                                                                                    <asp:HiddenField ID="hf_violationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>' />
                                                                                    <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>' />
                                                                                    <asp:HiddenField ID="hf_courtviolationstatusid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtViolationStatusidmain") %>' />
                                                                                    <asp:HiddenField ID="hf_categoryid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' />
                                                                                    <asp:HiddenField ID="hf_ticketsviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>' />
                                                                                    <asp:HiddenField ID="hf_courtid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' />
                                                                                    <asp:HiddenField ID="hf_OscareCourt" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.OscarCourtDetail") %>' />
                                                                                    <asp:HiddenField ID="hf_ticketNumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refcasenum") %>' />
                                                                                    <asp:HiddenField ID="hf_courtdate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:d}") %>' />
                                                                                    <asp:HiddenField ID="hf_crm" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' />
                                                                                    <asp:HiddenField ID="hf_ctime" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>' />
                                                                                    <asp:HiddenField ID="hf_VDesc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Descriptions") %>' />
                                                                                    <asp:HiddenField ID="hf_courtstatus" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.VerCourtDesc") %>' />
                                                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ticketid_pk") %>' />
                                                                                    <asp:HiddenField ID="HFCoveringFirmID" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CoveringFirmID") %>' />
                                                                                    <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.iscriminalcourt") %>' />
                                                                                    <asp:HiddenField ID="hf_chargelevel" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.chargelevel") %>' />
                                                                                    <asp:HiddenField ID="hf_cdi" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.cdi") %>' />
                                                                                    <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.isFTAViolation") %>' />
                                                                                    <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasFTAViolations") %>' />
                                                                                    <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value='<%# Bind("coveringFirmId") %>' />
                                                                                    <asp:HiddenField ID="hf_arrestdate" runat="server" Value='<%# Bind("arrestdate","{0:d}") %>' />
                                                                                    <asp:HiddenField ID="hf_casetype" runat="server" Value='<%# Bind("casetypeid") %>' />
                                                                                    <asp:HiddenField ID="hf_missedcourttype" runat="server" Value='<%# Bind("missedcourttype") %>' />
                                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="display: none" id="tbl_plzwait1" width="805">
                                                        <tbody>
                                                            <tr>
                                                                <td class="clssubhead" valign="middle" align="center">
                                                                    <img src="../Images/plzwait.gif" />
                                                                    Please wait While we update your Violation information.
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click">
                                                    </aspnew:AsyncPostBackTrigger>
                                                </Triggers>
                                            </aspnew:UpdatePanel>
                                            <div style="display: none; left: 20px; width: 250px; position: absolute; top: 1400px;
                                                background-color: transparent" id="zee">
                                                &nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center" colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" background="../../images/separator_repeat.gif" colspan="5" height="11">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableComment" bordercolor="white" cellspacing="0" style="height: 180px"
                                cellpadding="0" width="100%" border="1">
                                <tbody>
                                    <tr class="clsleftpaddingtable" style="height: 20px">
                                        <td class="clsaspcolumnheader" style="width: 458px">
                                            General Comments
                                        </td>
                                        <td class="clsaspcolumnheader" colspan="2">
                                            Trial Comments
                                        </td>
                                    </tr>
                                    <tr class="clsleftpaddingtable" style="height: 265px">
                                        <td style="width: 458px; height: 100%" valign="top">
                                            &nbsp;<cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Width="327px" Height="160px" />
                                        </td>
                                        <td style="width: 408px; height: 100%" valign="top">
                                            &nbsp;<cc2:WCtl_Comments ID="WCC_TrialComments" runat="server" Width="327px" Height="160px" />
                                        </td>
                                        <td valign="middle" align="center" width="170" colspan="1" rowspan="1" style="height: 100%"
                                            valign="top">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table style="display: none; height: 80px" id="plz_wait2" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="clssubhead" valign="middle" align="center">
                                            <img src="../Images/plzwait.gif" />
                                            Please wait While we update your Violation information.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="width: 800px" background="../images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                            &nbsp;Auto Info History
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <aspnew:UpdatePanel ID="upnlAutoHistory" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:DataGrid ID="dg_autohistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                        BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                                        <Columns>
                                                            <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyle"
                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm tt}"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Ticket No" DataField="refcasenumber" ItemStyle-CssClass="GridItemStyle"
                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Auto Status" DataField="Description" ItemStyle-CssClass="GridItemStyle"
                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Court Date" DataField="NewAutoDate" ItemStyle-CssClass="GridItemStyle"
                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Court Time" DataField="NewAutoDate" ItemStyle-CssClass="GridItemStyle"
                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                DataFormatString="{0:t}"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Court Number" DataField="NewAutoRoom" ItemStyle-CssClass="GridItemStyle"
                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click">
                                                    </aspnew:AsyncPostBackTrigger>
                                                </Triggers>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 800px" background="../images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="height: 22px">
                                            <asp:Button ID="btn_UpdateDetails" runat="server" CssClass="clsbutton" Text="Update">
                                            </asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 800px" background="../images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                        </td>
                    </tr>
                    </td> </tr>
                    <tr>
                        <td style="display: none">
                            <asp:Label ID="lblMessage" runat="server" Width="320px" CssClass="label" ForeColor="White"
                                EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="display: none; visibility: hidden" id="CtrlEnd" runat="server">
                                <input id="txtHidden" type="hidden" name="Hidden1" runat="server" />
                                <asp:TextBox ID="lbl_Court" runat="server"></asp:TextBox>
                                <asp:Label ID="lblAdmin" runat="server" ForeColor="Black">0</asp:Label><asp:HiddenField
                                    ID="HFMainFirmID" runat="server"></asp:HiddenField>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </tbody>
            </table>
            </tbody> </table>
            <uc4:UpdateViolationSRV ID="UpdateViolationSRV1" runat="server" GridField="hf_ticketviolationid"
                DivID="tbl_plzwait1" IsCaseDisposition="true" GridID="dg_Details" />
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btn_UpdateInfo" EventName="Click"></aspnew:AsyncPostBackTrigger>
            <aspnew:AsyncPostBackTrigger ControlID="btn_UpdateDetails" EventName="Click"></aspnew:AsyncPostBackTrigger>
        </Triggers>
    </aspnew:UpdatePanel>

    <script language="javascript">
		
		function setTableData()
		{
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_Details";
			var idx=2;
			
			for (idx=2; idx < (cnt+2); idx++)
			{
				
				if(idx < 10)
				{
				
				setTableRows_OnLoad(grdName+ "_ctl0" + idx + "_ddl_Pending");
				var FormStr = grdName+ "_ctl0" + idx;
				setTableRowCauseDate(FormStr + "_chk_Ret");
				setTableRowDSC(FormStr + "_chk_DSC");
				setTableRowShowCause(FormStr + "_chk_ShowCause");
				setTableRowMiscComments(FormStr + "_chk_Misc")
				}
				else
				{
				setTableRows_OnLoad(grdName+ "_ctl" + idx + "_ddl_Pending");
				var FormStr = grdName+ "_ctl" + idx;
				setTableRowCauseDate(FormStr + "_chk_Ret");
				setTableRowDSC(FormStr + "_chk_DSC");
				setTableRowShowCause(FormStr + "_chk_ShowCause");
				setTableRowMiscComments(FormStr + "_chk_Misc")
				
				}
				
			}
		}
		
		//setTableData();
		
		function  pressKey()
		{
		        if ( document.getElementById("Disable").style.display == 'block' )
		        {		            
		            if (event.keyCode == 13)
		            {
		                event.returnValue=false;
				        event.cancel = true;
		            
		                document.getElementById("btn_popup").click();
		                
		            }
		        }
		}
		
		
    </script>

    </form>

    <script language="javascript">
		function checkAdmin()
		{
			if (lblAdmin.innerText=="0")
			{
				document.getElementById("CS").style.display='none';
				document.getElementById("CD").style.display='none';
				document.getElementById("CT").style.display='none';
				document.getElementById("CN").style.display='none';
				
				document.getElementById("CS1").style.display='none';
				document.getElementById("CD1").style.display='none';
				document.getElementById("CT1").style.display='none';
				document.getElementById("CN1").style.display='none';
				
				
			}
		}
    </script>

</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailerInfo.aspx.cs" Inherits="lntechNew.ClientInfo.MailerInfo" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Violation Data Information</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>
    <table width="350px">
        <tr>
            <td style="height: 21px">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="label"
                    ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolkit:TabContainer ID="Tabs" runat="server">
                    <ajaxToolkit:TabPanel ID="pnl_ByDescription" runat="server" HeaderText="General"
                        TabIndex="0">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td style="width: 166px" valign="middle" height="20" class="clsLabel">
                                        Officer Name:
                                    </td>
                                    <td width="520" colspan="2" height="20">
                                        <font color="#3366cc">
                                            <asp:Label ID="lbl_Name" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label></font>
                                    </td>
                                </tr>
                                <tr id="trMailDate" runat="server">
                                    <td style="width: 166px" valign="middle" height="20" class="clsLabel">
                                        Mail Date:
                                    </td>
                                    <td width="520" colspan="2" height="20">
                                        <font color="#3366cc">
                                            <asp:Label ID="lbl_MailerDate" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label></font>
                                    </td>
                                </tr>
                                <tr id="trMailerId" runat="server">
                                    <td class="clsLabel" style="width: 166px; height: 20px;" valign="middle">
                                        Mailer ID:
                                    </td>
                                    <td colspan="2" style="height: 20px">
                                        <font color="#3366cc">
                                            <asp:Label ID="lbl_MailerID" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label></font>
                                    </td>
                                </tr>
                                <tr id="trLetterType" runat="server">
                                    <td class="clsLabel" style="width: 166px; height: 20px;" valign="middle">
                                        Letter Type:
                                    </td>
                                    <td colspan="2" style="height: 20px">
                                        <asp:Label ID="lbl_LetterType" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                    </td>
                                </tr>
                                </tr>
                                <tr>
                                    <td class="clsLabel" style="width: 166px" valign="top">
                                        Upload Date:
                                    </td>
                                    <td width="520" colspan="2" valign="top">
                                        <font color="#3366cc">
                                            <asp:Label ID="lbl_UploadDate" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label></font>
                                    </td>
                                </tr>
                                <tr id="trTrafficAlert" runat="server" style="display: none">
                                    <td style="width: 100%; height: 20px" colspan="2">
                                        <asp:Label ID="lblTrafficAlert" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="pnl_ByDescriptione" runat="server" HeaderText="Mailer"
                        TabIndex="1">
                        <ContentTemplate>
                            <table>
                                <tr runat="server">
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" height="32px"
                                        colspan="2" width="300px">
                                        <strong>&nbsp;&nbsp;Mailer Information</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:GridView ID="gv_ShowAllLettres" runat="server" AutoGenerateColumns="False" BackColor="#EFF4FB"
                                            BorderColor="White" Width="300">
                                            <Columns>
                                                <asp:BoundField HeaderText="Mail Date" DataField="MailDate" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="Left" />
                                                <asp:BoundField HeaderText="Mailer Type" DataField="MailerType" ItemStyle-CssClass="GridItemStyle"
                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="Left" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" style="height: 40px">
                <asp:Button ID="btn_Close" runat="server" CssClass="clsbutton" Text="Close" OnClientClick="window.close();">
                </asp:Button>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="HTP.ClientInfo.Comments"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comments</title>
    <link href="../Styles.css" type="text/css" rel="Stylesheet" />

    <script type="text/javascript" language="javascript">
    function CommentPopUp()
    {       
       if(document.getElementById("WCC_Comments_lbl_comments")!=null)
       {
            document.getElementById("WCC_Comments_lbl_comments").innerHTML="";
           
     
        }
    }
    function MaintainGrid()
    {
        
            document.getElementById("tblDateWise1").style.display='none';
            document.getElementById("tblDateWise2").style.display='none';
            document.getElementById("trCommWise").style.display='block';
       
           }
    
    function ChangeGrid()
    {
        //document.getElementById("trdate").style.display ='block';
        if(document.getElementById("lnkComm").innerHTML=="Date Time View")
        {
            document.getElementById("lnkComm").innerHTML="Comment Type Wise";
            document.getElementById("tblDateWise1").style.display='none';
            document.getElementById("tblDateWise2").style.display='none';
            document.getElementById("trCommWise").style.display='block';
        }
        else
        {
            document.getElementById("lnkComm").innerHTML="Date Time View";
            document.getElementById("tblDateWise1").style.display='block';
            document.getElementById("tblDateWise2").style.display='block';
            document.getElementById("trCommWise").style.display='none';
        }
        
        return false;
    }
    
    
    function Check()
    {        
        var select =document.getElementById("ddlCommentType");
        var select =document.getElementById("ddlCommentType");
        if(select.options[select.selectedIndex].value==-1)
        {
            alert("Please Select Comment Type");            
            select.focus();
            return false;
            
        }
        else if(document.getElementById("WCC_Comments_txt_Comments").value == "")   //SAEED 7859 06/08/2010, Change innerHtml with value attribute. Because google chrome doesn't support innerHtml.
        {        
            alert("Please Enter Comments");            
            document.getElementById("WCC_Comments_txt_Comments").focus();
            return false;            
        }
        
        document.getElementById("btnSubmit").style.display='none';
        document.getElementById("btnDummy").style.display='block';        
        return true;
        //Sys.WebForms.PageRequestManager.getInstance()._updateControls(['tupd_panel','tupnl_Repeter'], ['btnSubmit'], [], 90);
    }
    
    function CheckForSelect()
    {
    
        var select =document.getElementById("ddlCommentType");
        if(select.options[select.selectedIndex].value==-1)
        {
            return false;
        } 
    return true;
      
    }
    
    function HideCheckBox()
    {
        var select =document.getElementById("ddlCommentType");   
        var Chk_Complaint = document.getElementById("WCC_Comments_chk_Complaint");    
        if(select.options[select.selectedIndex].value == 1  || select.options[select.selectedIndex].value == 3)
        {
            Chk_Complaint.style.display="block";
            document.getElementById("WCC_Comments_lbl_Complaint").style.display="block";
        }
        if(Chk_Complaint!=null)
        {
           Chk_Complaint.checked=false;    
        }
    }
    
     
     function HidePopup() 
     {
            var modalPopupBehavior = $find('MPEComments');
            modalPopupBehavior.hide();
            return false;
     }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="upd_panel" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
                    <tbody>
                        <tr>
                            <td colspan="5">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td background="../../images/separator_repeat.gif" colspan="6" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 33px">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="80%">
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                                        <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>)
                                                        <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink><a href="#"></a>
                                                    </td>
                                                    <td align="right" width="14%">
                                                    </td>
                                                    <td align="right" width="6%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table id="Table1" cellspacing="0" cellpadding="0" width="800" border="0">
                                                <tr>
                                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                                    </td>
                                                </tr>
                                                <tr class="clsLeftPaddingTable">
                                                    <td align="right">
                                                        <table>
                                                            <tr>
                                                                <td align="right" class="clssubhead">
                                                                    <%--<span class="clssubhead">Date Time View</span>--%>
                                                                    <asp:LinkButton ID="lnkComm" runat="server" OnClick="lnkComm_Click" Text="Date Time View"></asp:LinkButton>
                                                                    |
                                                                </td>
                                                                <td align="right">
                                                                    <asp:LinkButton ID="lnk_AddComments" OnClick="btn_lnkAddComm" runat="server" Text="Add Comments"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" background="../images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table id="tblDateWise1" runat="server" width="100%">
                                                <tr>
                                                    <td align="right" background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <span class="clssubhead">Comments</span>
                                                                </td>
                                                                <td align="right">
                                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" background="../images/separator_repeat.gif" height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lbl_Message" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                            ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="tblDateWise2" runat="server" width="100%">
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" CssClass="clsLabel"
                                                        Text="Please Wait ......"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gv_Records" runat="server" AllowPaging="True" AllowSorting="True"
                                                        AutoGenerateColumns="False" CssClass="clsLeftPaddingTable" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                                        OnSorting="gv_Records_Sorting" PageSize="20" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="&lt;u&gt;Date Time&lt;/u&gt;"
                                                                SortExpression="sortCommDate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CommDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Day">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Days" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommDay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Comment Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CommType" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderText="Comment">
                                                                <ItemTemplate>
                                                                    <div style="height: 40px; width: 500px; overflow: auto">
                                                                        <asp:Label ID="lbl_Comm" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Comm") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderText="&lt;u&gt;Rep&lt;/u&gt;" SortExpression="Rep">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Rep" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                            Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                                </Triggers>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<ajaxToolkit:ModalPopupExtender ID="MPELoding" runat="server" BackgroundCssClass="modalBackground"
                                    PopupControlID="Loading" TargetControlID="btndummynone">
                                </ajaxToolkit:ModalPopupExtender>
                                <div id="Loading">
                                    <img alt="Please wait" src="../Images/page_loading_ani.gif" />
                                </div>--%>
                                <ajaxToolkit:ModalPopupExtender ID="MPEComments" runat="server" BackgroundCssClass="modalBackground"
                                    CancelControlID="lbtn_cmn_close" PopupControlID="pnlAddComments" TargetControlID="btndummynone">
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Button ID="btndummynone" runat="server" Style="display: none" />
                            </td>
                        </tr>
                        <tr id="trCommWise" runat="server">
                            <td>
                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="upnl_Repeter">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl2" runat="server" CssClass="clsLabel"
                                            Text="Please Wait ......"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnl_Repeter" runat="server">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptComments" runat="server" OnItemDataBound="rptComments_ItemDataBound">
                                            <ItemTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right" background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <span class="clssubhead">
                                                                            <asp:Label ID="lblComm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CommentType") %>'></asp:Label>
                                                                        </span>
                                                                    </td>
                                                                    <td align="right">
                                                                        <uc3:PagingControl ID="Pagingctrl_typewise" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td background="../../images/separator_repeat.gif" height="11">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="100%">
                                                            <asp:GridView ID="gv_CommRecords" runat="server" AllowPaging="True" AllowSorting="True"
                                                                AutoGenerateColumns="False" CssClass="clsLeftPaddingTable" OnPageIndexChanging="gv_Data_PageIndexChanging"
                                                                OnSorting="gv_Sorting" PageSize="10" Width="100%">
                                                                <EmptyDataTemplate>
                                                                    <table align="center" class="clsLeftPaddingTable">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbl_Emptydata" runat="server" ForeColor="Red" Text="No Records Found"
                                                                                    Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="&lt;u&gt;Date Time&lt;/u&gt;"
                                                                        SortExpression="sortCommDate">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_CommDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommDate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Day">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Days" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommDay") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                        HeaderText="Comment">
                                                                        <ItemTemplate>
                                                                            <div style="height: 40px; width: 500px; overflow: auto">
                                                                                <asp:Label ID="lbl_Comm" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Comm") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                        HeaderText="&lt;u&gt;Rep&lt;/u&gt;" SortExpression="Rep">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Rep" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <PagerSettings Mode="NextPreviousFirstLast" Visible="false" NextPageText="&nbsp;Next &gt;"
                                                                    PreviousPageText="&lt; Previous" FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;"
                                                                    LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr class="clsLeftPaddingTable">
                                                        <td align="center">
                                                            <asp:LinkButton ID="lnkFirst" runat="server" OnClick="lbk_First_Click" Text="&lt;&lt; First Page"></asp:LinkButton>
                                                            &nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lbk_previous_Click" Text="&lt; Previous"></asp:LinkButton>
                                                            &nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnkNext" runat="server" OnClick="lbk_next_Click" Text="Next &gt;"></asp:LinkButton>
                                                            &nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnkLast" runat="server" OnClick="lbk_Last_Click" Text="Last Page &gt;&gt;"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td background="../../images/separator_repeat.gif" height="11">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlAddComments" runat="server" Width="455px">
                                    <table border="2" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
                                        border-top-color: navy; border-collapse: collapse; border-right-color: navy"
                                        width="455px" class="clsLeftPaddingTable">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" valign="bottom">
                                                <table border="0" style="height: 26px" width="100%">
                                                    <tr>
                                                        <td class="clssubhead" style="height: 26px">
                                                            Add Comments
                                                        </td>
                                                        <td align="right">
                                                            <asp:LinkButton ID="lbtn_cmn_close" runat="server">X</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" class="clsLeftPaddingTable " width="100%">
                                                    <tr>
                                                        <td class="clssubhead" colspan="2">
                                                            Comments Type :
                                                            <asp:DropDownList ID="ddlCommentType" AutoPostBack="true" runat="server" DataTextField="CommentType"
                                                                DataValueField="CommentID" OnSelectedIndexChanged="ddlCommentType_SelectedIndexChanged"
                                                                CssClass="clsInputCombo">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" width="100%">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <cc2:WCtl_Comments ID="WCC_Comments" runat="server" Width="420px" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" width="100%">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td align="right" width="100%">
                                                                                    <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" OnClientClick="return Check();"
                                                                                        OnClick="btnSubmit_Click" Width="50px" Text="Add" />
                                                                                    <asp:Button ID="btnDummy" runat="server" Text="Add" Width="50px" Style="display: none" disabled="true"
                                                                                        CssClass="clsbutton" /> 
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btn_cancel" runat="server" CssClass="clsbutton" OnClientClick="return HidePopup();" Width="50px"
                                                                                        Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hfLORCourtId" runat="server" Value="test" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

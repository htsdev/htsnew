using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Configuration;


namespace lntechNew.ClientInfo
{
	/// <summary>
	/// Preview Printed Letters
	/// </summary>
	public partial class frmPreview : System.Web.UI.Page
	{
			clsENationWebComponents ClsDb = new clsENationWebComponents();	
		  clsLogger cLog = new clsLogger();
		private void Page_Load(object sender, System.EventArgs e)
		{
			string lnk = String.Empty ;
			try
			{
				// Put user code to initialize the page here
				string RecordID =	Request.QueryString["RecordID"].ToString();
				string[] keys = {"@RecordID"};
				object[] values = {RecordID};
				DataSet ds_Print =   ClsDb.Get_DS_BySPArr("USP_HTS_GET_HTSNOTES",keys,values);
				
				//To get the address of file
				//commented by ozair
                //lnk=Server.MapPath("");
                lnk = ConfigurationSettings.AppSettings["NTPATHBatchPrinte"].ToString();
				
				lnk=lnk+ds_Print.Tables[0].Rows[0]["DocPath"].ToString();	
		        
				Response.ClearContent(); 
				Response.ClearHeaders(); 
				Response.ContentType = "application/pdf"; 
				Response.WriteFile(lnk);
				Response.Flush(); 
				Response.Close(); 
			}
			catch (Exception ex)
			{
				if ( !File.Exists(lnk))
					Response.Write("The specified file has been moved or deleted from the server.");
				else
					cLog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString() ,ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPaymentInfo.aspx.cs"
    Inherits="HTP.ClientInfo.NewPaymentInfo" ValidateRequest="false" EnableEventValidation="true" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="~/WebControls/faxcontrol.ascx" TagName="Faxcontrol" TagPrefix="Fc" %>
<%@ Register Src="../WebControls/ReadNotes.ascx" TagName="ReadNotes" TagPrefix="uc2" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Billing</title>

    <script src="../Scripts/NewValidateCC.js" type="text/javascript"></script>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalbackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script type="text/jscript">

        var txtCardSatus = 1;
        function GetFocus() {
            if (txtCardSatus != "0") {
                document.getElementById("txt_creditcard").focus();
            }
            return;
        }
        //        function test()
        //        {debugger;
        //            var a = document.getElementById('hf_continuancecount').value;
        //            var b = document.getElementById('HFOwes').value;
        //            var c = document.getElementById('hf_owesdiff').value;
        //        }

        // Abid Ali 5138 11/17/2008 -- update multiple if on same variable into else if conditions
        function VerifyCard() {
            if (event.keyCode == 13 && document.getElementById("ddlPaymentMethod").value == 5) {
                // cancel the default submit
                event.returnValue = false;
                event.cancel = true;
                // submit the form by programmatically clicking the specified button
                //document.getElementById("btn_popupok").click();
                return false;
            }

            var carddata = document.getElementById("txt_creditcard").value;
            document.getElementById("lbl_cardholdername").innerHTML = "";
            document.getElementById("lbl_expmonth").innerHTML = "";
            document.getElementById("lbl_cardno").innerHTML = "";

            if (carddata != null || carddata.length > 2) {

                var CardHolderName;
                var CardNo;
                var ExpDate;
                var FirstName;
                var LastName;
                var temp;
                var tIndex;

                CardHolderName = carddata.substring(carddata.indexOf("^") + 1, carddata.lastIndexOf("^"));
                // swap first name with last name and replace "/" with empty space.
                tIndex = CardHolderName.indexOf("/");
                if (tIndex != 0) {
                    LastName = CardHolderName.substring(0, tIndex);
                    FirstName = CardHolderName.substring(tIndex + 1, CardHolderName.length);
                    CardHolderName = trimAll(FirstName) + " " + LastName;
                }
                document.getElementById("lbl_cardholdername").innerHTML = CardHolderName.toUpperCase();
                ExpDate = carddata.substring(carddata.lastIndexOf("^") + 1, carddata.lastIndexOf("^") + 5);
                document.getElementById("lbl_expmonth").innerHTML = ExpDate.substring(2, 4);
                document.getElementById("txt_expyear").innerHTML = ExpDate.substring(0, 2);
                document.getElementById("lbl_cardno").innerHTML = carddata.substring(2, carddata.indexOf("^"));

            }
        }

        function trimAll(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }

        function FillCreditInfo() {
            if (document.getElementById("lbl_cardholdername").innerHTML == "" || document.getElementById("txt_expyear").length == 0) {
                alert("Please swap the card.");
                document.getElementById("txt_creditcard").focus();
                return false;
            }
            document.getElementById("txtCardholder").value = document.getElementById("lbl_cardholdername").innerHTML;
            document.getElementById("txtCardNumber").value = document.getElementById("lbl_cardno").innerHTML;
            document.getElementById("ddl_expmonth").value = document.getElementById("lbl_expmonth").innerHTML;
            document.getElementById("ddlYear").value = document.getElementById("txt_expyear").innerHTML;
            closereaderpopup("0");

        }
        function ShowReaderPopup() {
            txtCardSatus = "1";

            document.getElementById("PopupPanel").style.display = '';
            document.getElementById("PopupPanel").style.top = 450 + "px";

            document.getElementById("DisableDive").style.height = (document.body.offsetHeight * 2) + "px";
            document.getElementById("DisableDive").style.width = document.body.offsetWidth + "px";
            document.getElementById("DisableDive").style.display = '';
            document.getElementById("lbl_cardholdername").innerHTML = "";
            document.getElementById("lbl_expmonth").innerHTML = "";
            document.getElementById("lbl_cardno").innerHTML = "";
            document.getElementById("txt_expyear").innerHTML = "";
            document.getElementById("txt_creditcard").innerHTML = "";
            document.getElementById("txt_creditcard").focus();
            return false;
        }

        function closereaderpopup(w) {
            txtCardSatus = 0;
            document.getElementById("PopupPanel").style.display = 'none';
            document.getElementById("DisableDive").style.display = 'none';
            if (w == "1") {
                document.getElementById("txtCardholder").value = "";
                document.getElementById("txtCardNumber").value = "";
                document.getElementById("ddl_expmonth").selectedIndex = "0";
                document.getElementById("ddlYear").selectedIndex = "0";
                document.getElementById("ddlPaymentMethod").selectedIndex = "0";
                HideShow();
                txtCardSatus = 0;
            }
            document.getElementById("txt_creditcard").value = "";
            return false;
        }


        function ShowContinuanceCommentPopup() {
            document.getElementById("Pnl_ContinuanceComments").style.display = '';
            document.getElementById("Pnl_ContinuanceComments").style.top = 450 + "px";
            document.getElementById("DisableDive").style.height = (document.body.offsetHeight * 2) + "px";
            document.getElementById("DisableDive").style.width = document.body.offsetWidth + "px";
            document.getElementById("DisableDive").style.display = '';
            return false;
        }
        function CloseContinuanceCommentPopup() {
            //ozair 3871 on 04/25/2008 validatiing the payment void comments.
            var gComments = document.getElementById("WCC_ContinuanceComments_txt_Comments");
            gComments.value = trim(gComments.value);
            if (gComments.value.length == 0) {
                alert("Please enter Comments.");
                gComments.focus();
                return false;
            }
            //end ozair 3871		  		  
            document.getElementById("pnl_VoidComments").style.display = 'none';
            document.getElementById("DisableDive").style.display = 'none';
            return true;
        }
        //end ozair 3856
        //Fahad 5807 05/14/2009 Display Panels according to Requirements

        function GetXmlHttpObject() {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                return new XMLHttpRequest();
            }
            if (window.ActiveXObject) {
                // code for IE6, IE5
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
            return null;
        }

        //Noufil 5807 05/29/2009 check XMLHTTP state
        function state_Change() {
            if (xmlhttp.readyState == 4) {
                // 4 = "loaded"
                if (xmlhttp.status == 200) {
                    // 200 = OK
                    // ...our code here...
                }
                else {
                    alert("Problem retrieving XML data");
                }
            }
        }

        //Noufil 5807 05/29/2009 Display grid.
        var interval = 0;
        var norecordsfound = 0;
        function Display() {
            document.getElementById("tr_Process").style.display = 'none';
            document.getElementById("tr_nextbutton").style.display = 'none';
            document.getElementById("trgv_Payments").style.display = '';
            document.getElementById("trPaymentMsg").style.display = '';
            document.getElementById("tr_pleasewait").style.display = '';
            document.getElementById("lbl_showwaitmessage").innerHtml = "Please wait while we check bond amount from court website and load it in our system.";
            document.getElementById("btnDone").style.display = 'none';
            setTimeout("CheckBondfromCourt()", 800);
            return false;
        }

        //Noufil 5807 05/29/2009 Method to hit court website to get data
        function CheckBondfromCourt() {
            var grid = document.getElementById("gv_Payments");
            var ticketid = '<%= this.TicketId %>';
            var empid = '<%= this.EmpId %>';
            var randomnumber = Math.floor(Math.random() * 101)
            var url = "BondInformationFromCourt.aspx?Ticketid=" + ticketid + '-' + randomnumber + "&Empid=" + empid;

            xmlhttp = GetXmlHttpObject();
            if (xmlhttp == null) {
                alert("Your browser does not support AJAX!");
                return;
            }

            xmlhttp.onreadystatechange = state_Change;
            xmlhttp.open("GET", url, false);
            xmlhttp.send(null);
            var responseText = xmlhttp.responseText;
            //xmlhttp.dispose();
            xmlhttp.abort();
            xmlhttp = null;

            for (i = 1; i < grid.rows.length; i++) {
                var intervalset = 800 * i;
                var rowresponsetext = "";
                var oneplus = i + 1;
                var Ticketnumber = grid.rows[i].cells[1].innerHTML;
                try {
                    if (rowresponsetext.indexOf("Error:") < 0) {
                        rowresponsetext = responseText.substring(responseText.indexOf("Div_Literal") + 13, responseText.indexOf("Done"));
                        rowresponsetext = rowresponsetext.trim().split('$');
                        if (parseInt(rowresponsetext[i - 1].substring(rowresponsetext[i - 1].indexOf(":") + 1).trim()) > 0) {
                            interval = oneplus;
                            setTimeout("ChangeImage(" + oneplus + ")", 3000 + intervalset);
                        }
                    }
                    else {
                        norecordsfound = 1
                        alert("An error occurs in connecting to website.");
                    }
                }
                catch (err) {
                }
            }
            if (interval == 0 && norecordsfound == 0) {
                document.getElementById("tr_pleasewait").style.display = 'none';
                document.getElementById("btnDone").style.display = '';
            }

            void (0);
            return false;
        }

        //Noufil 5807 05/29/2009 method to change image
        function ChangeImage(oneplus) {
            document.getElementById("gv_Payments_ctl0" + oneplus + "_img_bond").src = "../Images/btnOK.ico";
            if (interval == oneplus) {
                document.getElementById("tr_pleasewait").style.display = 'none';
                document.getElementById("btnDone").style.display = '';
            }
            return false;
        }

        //Fahad 5807 05/14/2009 to Close the Modal Pop-Up
        function closeModalPopup(popupid) {
            var modalPopupBehavior = $find(popupid);
            modalPopupBehavior.hide();
            return false;
        }
        //Fahad 6054 07/21/2009 to shif Control to Server Side
        function VerifyModalPopUp() {
            return true;
        }


        function OpenPopUpSmall(path)  //(var path,var name)
        {
            window.open(path, '', "fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");
        }
		
		
    </script>

    <script language="javascript" type="text/javascript">
       
     function checkGComments()
		{
//		 var dateLenght = 0;
//		  var newLenght = 0;
//		  
//          newLenght = document.frmpayment.WCC_GeneralComments_txt_Comments.value.length
//		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

//		  if (document.frmpayment.WCC_GeneralComments_txt_Comments.value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerHTML.length > (5000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
//		  {
//		    alert("Sorry You cannot type in more than 5000 characters in General comments box")
//			return false;		  
//		  }
		  //if (document.frmgeneralinfo.txt_settingnotes.value!=document.frmgeneralinfo.txt_oldset.value)
		  //{

          
		    //General Comments Check
			//if (document.getElementById("txtGeneralComments").value.length > 2000-27) //-27 bcoz to show last time partconcatenated with comments
			//{
			//	alert("Sorry You Cannot type more than 2000 characters ");
			//	document.getElementById("txtGeneralComments").focus();
			//	return false;
			//}
		
		
		}
    function PrintLetter(flag)
    {
    	if(flag==1)
        {
	        //alert(flag)			
	        OpenPopUp("CaseSummaryNew.aspx?casenumber=<%=this.TicketId%>&search=<%=ViewState["vSearch"]%>");	
	        return false;	
        }
        //for EmailTrial leter..
        else if(flag == 9)
        { 
	        var retBatch=false; 
    		
	        OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=this.TicketId%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
	        return false;
    			
        }
        else if(flag==2)
        {	 
	        //code Added by Azwar if no letter flag then restrict to print
    	  
	        var checkFlag=document.getElementById("lblTrailNotFlag").innerHTML;
	        if(checkFlag == 0 )
	        {
		        alert("The 'No Letters' flag is active.Please remove this flag to send a trial notification letter");
		        return false;
	        }
	        else
	        {
		        var retBatch=false;
		        var option = confirm("Trial Noticiation Letter generated. Would you like to 'PRINT NOW' or 'SEND TO BATCH'?\n Click [OK] to print now or click [CANCEL] to send to batch."); 
		        if (option  )
			        retBatch =false;
		        else
			        retBatch=true;
    		
        //		  var courtid = document.getElementById("lbl_court").innerHTML;
        //	            var flg_split = document.getElementById("lbl_IsSplit").innerHTML;
        //	            var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerHTML;
        //		  
        //		  if ( option)
        //		  OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=this.TicketId%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&CourtID="+courtid+"&flg_split="+flg_split+"&flg_print="+flg_print );	
        //		 
          /*
		        var WinSettings = "center:yes;resizable:no;dialogHeight:220px;dialogwidth:350px;scroll:no";
		        var Arg = 1;*/
    			
	        //  var retBatch = BatchLetter();
    		
        //		if (document.getElementById("lbl_trialbuttonflag").innerHTML==0)
        //		{	


		        var courtid = document.getElementById("lbl_court").innerHTML;
		        var flg_split = document.getElementById("lbl_IsSplit").innerHTML;
		        var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerHTML;
		        var SplitContinue; //= true;
		        var PrintContinue;// = true;
    		   
    				   
		        if(flg_split==1)
		        {
			        SplitContinue = confirm("The Trial Notification Letter you are printing has a split court date, time or room number. Would you like to continue?");
		        }
    		   
		        if(SplitContinue==false)
			        return;
    			
    		  
    		
		        if (retBatch == true)
		        {  
    			 
			        if(flg_print==1)
			        {
				        PrintContinue = confirm("Case number "+courtid +" already exists in batch print queue. Would you like to override the existing letter with this new letter");
			        }
			        if(PrintContinue==false)
			        {
				        //ozair 4660 08/23/2008 if user opts not to overide the already existed letter then do nothing
				        return false;
			        }
			        else
			        {
			            // Afaq 8105 08/16/2010 Add date parameter in url.
				        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=this.TicketId%>" +"&Exists=1"+"&Date=<%=DateTime.Now.ToFileTime().ToString()%>");	
			        }
    			 
		        }
		        else 
		        {
		        //	alert("here");
			        OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=this.TicketId%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");	
			        //location.reload(true);
    			
		        }			 
	          //End
    		 
    		 
	         /*
	         var WinSettings = "center:yes;resizable:no;dialogHeight:220px;dialogwidth:350px;scroll:no";
    			
		        var Arg = 1;
		        var url = "Popup_Options.aspx?LetterType=TrialNotification&SplitFlag="+flg_split+"&PrintFlag="+flg_print;
    			
		        var conf = window.showModalDialog(url,Arg,WinSettings);
    			
		        if(conf == 1)
		        {
		        OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=false&casenumber=<%=this.TicketId%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
		        }
		        if(conf == 2)
		        {
		        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=" + flag + "&casenumber=<%=this.TicketId%>" +"&Exists=0");	
		        }
		        if(conf == 3)
		        {
		        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=" + flag + "&casenumber=<%=this.TicketId%>" +"&Exists=1");	
		        }
		        if(conf == 4)
		        {
		        OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=false&casenumber=<%=this.TicketId%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&EmailFlag=1");
		        }
	           */ 
    		   
        //		}
        /*
	        else
	        {
	         var x=confirm("There are conflicting court dates with this Trial Letter. Please verify this is correct. Press [ OK ] to Continue");
		        if (x)
		        {
			        if (retBatch == true)
			        {  
			        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
			        }
			        else 
			        {
			        OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch);	
		        //	location.reload(true);
			        }			 
		        }
           }
           */
		        return false;
	        }
        }						
        else if(flag==3)
        {	
        //	var retBatch = BatchLetter();
        //	if (retBatch == true)
        //	{  
        //	OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
        //	}	
        //	else 
        //	{  
    				  
	        //OpenPopUp("../Reports/word_receipt.aspx?casenumber=<%=this.TicketId%>");
	        window.open("../Reports/word_receipt.aspx?casenumber=<%=this.TicketId%>",'',"left=150,top=50,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");	
    		

        //	}
	        //End
           /* Commented by Farhan Sabir
	        var WinSettings = "center:yes;resizable:no;dialogHeight:220px;dialogwidth:350px;scroll:no";
	        var Arg = 1;
	        var conf = window.showModalDialog("Popup_Options.aspx?LetterType=Receipt",Arg,WinSettings);
	        if(conf == 1)
	        {
	        OpenPopUp("../Reports/word_receipt.aspx?casenumber=" + <%=this.TicketId%>+ "&EmailFlag=0");
	        }
	        if(conf == 2)
	        {
	        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=" + flag + "&casenumber=<%=this.TicketId%>" );	
	        }
	        if(conf == 3)
	        {
	        OpenPopUp("../Reports/word_receipt.aspx?casenumber="+ <%=this.TicketId%>+ "&EmailFlag=1");
	        }
	        */ //End
	        return false;	
        }		
        else if(flag==4)
        {	
	        var courtid=document.getElementById("lbl_courtid").innerHTML;

	        // tahir 4477 07/29/2008
	        // added validation on bond letter printing &
	        // fixed fixed the bug.
	        
	        //Nasir 6483 10/06/2009 logic for bond report for sec user
	        var paymenttype=document.getElementById("hf_CheckBondReportForSecUser").value;
	        
	        if(paymenttype!="0")
	        {
	            if(paymenttype == 7)
	            {
	                alert("Bonds can only be printed within 2 months of last payment.");
	            }
	            else
	            {
	                alert("Bonds can only be printed within 3 Weeks of last payment.");
	            }
	            return false;
	        }
	        

	        var canPrintBonds = document.getElementById("hf_AllowBondLetter").value;	        	        	        

	        if (canPrintBonds != "1")
	        {
		        alert("Bond Letters can only be printed for ARR, WAIT & BOND cases.");
		        return false;
	        }
	        // end 4477

                //Muhammad Nasir 10531 11/06/2012 added ID (3075) of "Houston Municipal- W. Montgomery"
		        if (courtid==3001 ||courtid==3002 || courtid==3003 || courtid==3075)
		        {
		            //Yasir Kamal 5627 03/06/2009 Bond Letter Changes.
		        // if (document.getElementById("lblBondConfirmed").innerHTML == "0" )
			    //   {
				//  alert("Please confirm the bond amounts before printing bonds");		       
			    //    }
			    //    else 
			    //    {
				        OpenPopUp("../Reports/Word_Bond.aspx?CourtType=0" + "&casenumber=<%=this.TicketId%>");	
			    //    }
			    //5627 end
		        }
		        else
		        {
		          // TAHIR AHMED 4281 06/20/2008
			        // DISPLAY AN ALERT IF CAUSE NUMBER IS INVALID OR MISSING FOR 
			        // HCJP COURT.....
			        var nos = document.getElementById("hf_nos").value;
			        var InvalidCauseNo= document.getElementById("hf_InvalidCauseNo").value;
			        nos = nos.toUpperCase();
    				
			        if (nos == "FALSE" && InvalidCauseNo == "0")
			        {
				        doyou =confirm("There is an invalid or no cause number associated with one or more bond.\n\rWould you like to continue? Press [Ok] for Yes and [Cancel] for No.");
    					
				        if (doyou)
					        {
					        OpenPopUp("../Reports/Word_Bond.aspx?CourtType=1" + "&casenumber=<%=this.TicketId%>");	
					        }
			        }
			        // END 4281
			        else 
			        {
				        OpenPopUp("../Reports/Word_Bond.aspx?CourtType=1" + "&casenumber=<%=this.TicketId%>");	
			        }
		        }		
	        return false;	
        }			
        else if(flag==5)
        {	 /*var retBatch = BatchLetter();
	        //alert(flag)
	        if (retBatch == true)
	        {  
	        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
	        }	
	        else 
	        {  */
	        OpenPopUp("../Reports/FrmMainCONTINUANCE.aspx?Batch=" + retBatch + "&casenumber=<%=this.TicketId%>");	
	        //}
	        return false;	
        }
        //	if(flag==6)
          //  {	
	        //alert(flag)		//letter of rep
    //			OpenPopUp("../Reports/main.aspx");	
    //			return false;	
    //			}
        else if(flag==6)
        {
        
             //Sabir Khan 9941 12/14/2011 Check for PMC LOR Dates for Maxout and for Past
             if(document.getElementById("hf_IsLORDateInPastOrFull").value == "1")
             {               
               alert("All available LOR dates are in the past or full, Please request the Administrator to change the LOR dates.");
               return true;
             }
             // Zeeshan Haider 10699 04/18/2013 PMC Cases for LOR
             if(document.getElementById("hf_IsLORDateInFuture").value == "0")
             {
                alert("The system is unable to generate and send a LOR because there are no available court setting dates. Please request the system administrator to update the LOR setting request date. This case will remain in its current status until a LOR is sent.");
                return true;
             }
           
            //Yasir Kamal 5948 06/04/2009 if all cases disposed.Dont Allow to print LOR 
              var isnotDisposed=document.getElementById("hf_IsnotDisposed").value;
        
                if(isnotDisposed==0)
                {
                       alert("All cases Disposed or No Hire. LOR not available");
                       return false;
                }
              //Sabir Khan 7110 12/09/2009 Check for Oscar case...
              var courtid = document.getElementById("hf_court").value;
              var coveringFirm = document.getElementById("hf_CoveringFirm").value;
              if(courtid == 3061 || coveringFirm == "0")
              {
                alert("LOR printing not allowed for Oscar case.");
                return false;
              }
                            
        	 //var retBatch = BatchLetter();
	        /*if (retBatch == true)
	        {  
	        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
	        }
	        else 
	        {	*/
	        // Sabir Khan 5763 04/16/2009 Check for LOR Subpoena Letter and LOR Motion of Discovery...
	        //----------------------------------------------------------------------------------------
	        if(document.getElementById("hf_IsLORSubpoena").value == "1")
	        {
	            if(document.getElementById("hf_HasSameCourtdate").value == "0")
	            {
	                alert("The LOR for this court house contains Subpeona Letter and it requires to have same court date/time for all violations.\n Please first fix this issue and then print LOR.");
	                return false;
	            }
	        }
	        if(document.getElementById("hf_IsLORMOD").value == "1")
            {
	            if(document.getElementById("hf_HasMoreSpeedingviol").value == "0")
	            {
	                //Yasir Kamal 5915 05/20/2009 spelling in alert message fixed.
	                alert("The LOR for this court house contains Motion For Discovery and it requires to have only one speeding violation.\n This client has more than one speeding violation. Please first fix this issue and then print LOR.");
	                return false;
	            }
	        }
    		//----------------------------------------------------------------------------------------   	
    		// Abid Ali 5359 12/30/2008 LOR to send batch and optimez the code	
    		//Nasir 6013 07/08/2009 remove message	        
			
			//Nasir 6181 07/22/2009 if LOR method is fax
			//Ozair 6460 08/25/2009 included check for bond flag
			 var bondFlag = <%=ViewState["BondFlag"]%>;
			  //Yasir Kamal 6480 08/28/2009 Allow Faxing LOR even if bond is true. 
	         if(document.getElementById("hfLORMethodCount").value != '0' && (bondFlag=="0" || bondFlag=="1" && document.getElementById("hffaxFlag").value == "True" && document.getElementById("hfIsHCJP").value == "True")) 
            {               
                return true;            
            }
             
						
			var courtid = document.getElementById("lbl_court").innerHTML;
	        var flg_print = document.getElementById("lbl_IsLORAlreadyInBatchPrint").innerHTML;
	        //Nasir 6181 07/27/2009 remove retbatch
	        if ( flg_print == 1)
            {
			    if(!confirm("Case number "+courtid +" already exists in batch print queue. Would you like to override the existing letter with this new letter"))
			    {
			        return false;
			    }		    
            }
            
            // Abid Ali 5359 pritn letter of rep
      
            
            //Nasir 6181 07/27/2009 remove retbatch condition and print letter url
                //Ozair 6460 08/25/2009 small popup method called
                OpenPopUpSmall("../Reports/frmSendLetterOfRepBatch.aspx?Batch=true&lettertype=6&casenumber=<%=this.TicketId%>");					    	           
            
		    
	            //Nasir 6013 07/10/2009 replace doyou to false
	            return false;
        }
    		
        else if(flag==11)
        {	
            //Muhammad Muneer BUG 6153 08/23/2010  removing the retbatch variable that is undefined and passing Batch=false 
	        OpenPopUp("../Reports/FrmMainLetterofRep.aspx?lettertype=7&casenumber=<%=this.TicketId%>");	
	        return false;	
        }
        else if(flag==12)
        {
        
        	        //Muhammad Muneer BUG 6153 08/23/2010  removing the retbatch variable that is undefined and passing Batch=false
	        OpenPopUp("../Reports/FrmMainLetterofRep.aspx?lettertype=8&casenumber=<%=this.TicketId%>");	
	        return false;	
        }
        else if(flag==8)
        {				    		    
	        OpenPopUp("../../reports/Payment_Receipt_Contract.asp?TicketID=<%=this.TicketId%>");	
	        return false;	
        }
        // Abid Ali 5138 11/17/2008 New report buttons added and intended the function
        else if(flag==16)
        {
	        OpenPopUp("../reports/TrialNotificationDL.aspx?casenumber=<%=this.TicketId%>&reportnumber=1");	
	        return false;
        }
        else if(flag==17)
        {
	        OpenPopUp("../reports/TrialNotificationDL.aspx?casenumber=<%=this.TicketId%>&reportnumber=2");	
	        return false;
        }
         // Fahad 5098 11/29/2008 New report buttons added and intended the function for AG Contract
         else if(flag>=18 && flag<=25)
        {
            //Sabir Khan 7313 Don't print AG Contract when all cases are disposed or no hire...
              if(flag == 18)
              {
                    var isnotDisposed=document.getElementById("hf_IsnotDisposed").value;
                    if(isnotDisposed==0)
                    {
                        alert("All cases Disposed or No Hire. AG Contract not available");
                        return false;
                    }
               }
                var myWindow;
		        var width = 780+"px";
                var height = 780+"px";
                var left = parseInt((screen.availWidth/2) - (width/2))+"px";
                var top = parseInt((screen.availHeight/2) - (height/2))+"px";
                var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;      
                myWindow =OpenPopUp("../Reports/ContractMain.aspx?lettertype="+flag+"&casenumber=<%=this.TicketId%>","subWind",windowFeatures);
	            return false;
        }        
        
        //Waqas 5864 07/03/2009
        //flag from 26 to 31 used for ALR Letters, thanks.
	       
    }
		//Waqas 5864 07/03/2009 ALR Checks
		function CheckALRCheckReq()
		{
		    if(document.getElementById("hf_ALROfficerNameAlert").value == '1')
			{
		        	alert("Please enter Officer Name under Post Hire question on Matter Page");
			        return false;
			}
			//waqas 6342 08/13/2009 for observing officer
			if(document.getElementById("hf_ALROBSOfficerNameAlert").value == '1')
			{
		        	alert("Please enter Observing officer name under Post Hire question on Matter Page");
			        return false;
			}
		    OpenPopUp("../Reports/ALRLetters.aspx?lettertype=26&casenumber=<%=this.TicketId%>");	
	        return false;	
		}
		
		function CheckALRContinuance()
		{
		    if(document.getElementById("hf_ALRAttorneyNameAlert").value == '1')
		    {
		        alert("Please enter Attorney First/Last name on Outside Firm Interface");
		        return false;
		    }	
		    
		    if(document.getElementById("hf_ALRAttorneyBarCardAlert").value == '1')
		    {
		        alert("Please enter Attorney Bar Card Number on Outside Firm Interface");
		        return false;
		    }
		    
		    OpenPopUp("../Reports/ALRLetters.aspx?lettertype=27&casenumber=<%=this.TicketId%>");	
	        return false;        
		    
		}
		
		function CheckALRHearingRequest()
		{
		    if(document.getElementById("hf_ALRArrestingAgencyAlert").value == '1')
		    {
		        alert("Please enter Arresting Agency under Post Hire question on Matter Page");
		        return false;
		    }	
		    
		    if(document.getElementById("hf_ALROfficerNameAlert").value == '1')
		    {
		        alert("Please enter Officer Name under Post Hire question on Matter Page");
		        return false;
		    }
		    
		     if(document.getElementById("hf_ALRAttorneyNameAlert").value == '1')
		    {
		        alert("Please enter Attorney First/Last name on Outside Firm Interface");
		        return false;
		    }
		    
		    OpenPopUp("../Reports/ALRLetters.aspx?lettertype=28&casenumber=<%=this.TicketId%>");	
	        return false;	
		}
		
		function CheckALRMandatoryContinuance()
		{
		    if(document.getElementById("hf_ALRAttorneyNameAlert").value == '1')
		    {
		        alert("Please enter Attorney First/Last name on Outside Firm Interface");
		        return false;
		    }	
		    
		    if(document.getElementById("hf_ALRAttorneyBarCardAlert").value == '1')
		    {
		        alert("Please enter Attorney Bar Card Number on Outside Firm Interface");
		        return false;
		    }
		    
		    OpenPopUp("../Reports/ALRLetters.aspx?lettertype=29&casenumber=<%=this.TicketId%>");	
	        return false;	
		}
		
		function CheckALRMotionToRequestBTOBTS()
		{
		    if(document.getElementById("hf_ALRAttorneyNameAlert").value == '1')
		    {
		        alert("Please enter Attorney First/Last name on Outside Firm Interface");
		        return false;
		    }	
		    
		    if(document.getElementById("hf_ALRAttorneyBarCardAlert").value == '1')
		    {
		        alert("Please enter Attorney Bar Card Number on Outside Firm Interface");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALRIntoxilizerAlert").value == '1')
		    {
		        alert("Intoxilizer was not taken by the client. Please set to �Yes� on Matters page if you would like to Print this request");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALRIntoxilizerAlert2").value == '1')
		    {
		        alert("Intoxilizer Result was taken by the client to 'Fail'. Please set to 'Pass' on Matters page if you would like to Print this request");
		        return false;
		    }			    
		    
		    OpenPopUp("../Reports/ALRLetters.aspx?lettertype=30&casenumber=<%=this.TicketId%>");	
	        return false;	
	        
		}
		
		function CheckALRSubpoena()
		{
		    if(document.getElementById("hf_ALROfficerNameAlert").value == '1')
		    {
		        alert("Please enter Officer Name under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    //Waqas 6342 08/13/2009 check for officer address and state added 
		    if(document.getElementById("hf_ALROfficerAddressAlert").value == '1')
		    {
		        alert("Please enter Officer address under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROfficerCityAlert").value == '1')
		    {
		        alert("Please enter Officer's City under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROfficerStateAlert").value == '1')
		    {
		        alert("Please select Officer's state under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROfficerZipAlert").value == '1')
		    {
		        alert("Please enter Officer's Zip Code under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    //Waqas 6342 08/13/2009 for observing officer
		    if(document.getElementById("hf_ALROBSOfficerNameAlert").value == '1')
		    {
		        alert("Please enter Observing officer name under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROBSOfficerAddressAlert").value == '1')
		    {
		        alert("Please enter Observing officer address under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROBSOfficerCityAlert").value == '1')
		    {
		        alert("Please enter Observing officer's City under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROBSOfficerStateAlert").value == '1')
		    {
		        alert("Please select Observing officer's state under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    if(document.getElementById("hf_ALROBSOfficerZipAlert").value == '1')
		    {
		        alert("Please enter Observing officer's Zip Code under Post Hire question on Matter Page");
		        return false;
		    }
		    
		    
		    if(document.getElementById("hf_ALRAttorneyNameAlert").value == '1')
		    {
		        alert("Please enter Attorney First/Last name on Outside Firm Interface");
		        return false;
		    }	
		    
		    OpenPopUp("../Reports/ALRLetters.aspx?lettertype=31&casenumber=<%=this.TicketId%>");	
	        return false;	
		}
		
		//--Adil
		function OpenDocumentSelector(ticketid,ExePath,SessionId)
        {
        //alert(ticketid);       
        //Fahad 5806 04/27/2009 Window witdth and height changed
        // Noufil 5931 05/21/2009  passing values in QueryString
        window.open ('frmDocumentSelector.aspx?ticketid=' + ticketid +'&exepath='+ExePath+'&sessionid='+SessionId, '', 'status=no,right=0,top=0,height=30,width=30,location=right,scrollbars=no'); 
        return false;
        }
        //--Adil
		
		
		function OpenPopUp(path)  //(var path,var name)
		{
		
		 window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		  //return true;
		}
		// Fahad 6054 07/25/2009 Method to check allow only alpha numeric character
		function isAlphabetNumeric(strData)
		{
		    var iCount,iDataLen;
		    var strCompare;
		    var bFlag = true;
		    strCompare = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		    iDataLen = strData.length;
		    if (iDataLen >0)
		    {
		        for (iCount=0; iCount<=iDataLen; iCount++)
		        {
		            var cData = strData.charAt(iCount);
		            if (strCompare.indexOf(cData) < 0 )
		            {
		                alert("Please enter valid bar card number!")
		                document.getElementById("txtBarCardNumber").value="";
		                return false;
		            } 
		         
		         }
		   }
        }
    
    
    
        function HideShow()
         {
            var DDLPaymentMethod=document.getElementById("ddlPaymentMethod").value;
            var td_CH=  document.getElementById("td_CH");
            var tdCH=  document.getElementById("tdCH");
            var td_CN=document.getElementById("td_CN");
            var tdCN=document.getElementById("tdCN");
            var tdCCR =document.getElementById("tdCCR");
            var td_EX=document.getElementById("td_EX");
            var tdEX=document.getElementById("tdEX");
            var td_CIN=document.getElementById("td_CIN");
            var tdCIN=document.getElementById("tdCIN");
            var td_BC=document.getElementById("td_BC");
            var tdBC=document.getElementById("tdBC");
            var td_OF=document.getElementById("td_OF");
            var tdOF=document.getElementById("tdOF");
            var td_RN=document.getElementById("td_RN");
            var tdRN=document.getElementById("tdRN");                           
           
         
            //Fahad 6054 07/20/2009 Change the Case of if and else aand make it Re-Useable for future use.
            if(DDLPaymentMethod == "5")
            {
                td_CH.style.display='';
                tdCH.style.display='';
                td_CN.style.display='';
                tdCN.style.display='';
                td_EX.style.display='';
                tdEX.style.display='';
                td_CIN.style.display='';
                tdCIN.style.display='';     
                tdCCR.style.display=''; 
                td_BC.style.display='none';
                tdBC.style.display='none';
                td_OF.style.display='none';
                tdOF.style.display='none';
                td_RN.style.display='none';
                tdRN.style.display='none';
           
            }
            //Fahad 6054 07/20/2009 Case for Partner Firm Credit
            else if (DDLPaymentMethod == "104")
            {
                td_OF.style.display='';
                tdOF.style.display='';
                td_CH.style.display='none';
                tdCH.style.display='none';
                td_CN.style.display='none';
                tdCN.style.display='none';
                td_EX.style.display='none';
                tdEX.style.display='none';
                td_CIN.style.display='none';
                tdCIN.style.display='none';
                tdCCR.style.display='none';
                td_BC.style.display='none';
                tdBC.style.display='none';
                td_RN.style.display='none';
                tdRN.style.display='none';
            
            
            }
             //Fahad 6054 07/20/2009 Case for Attorney Credit
            else if (DDLPaymentMethod == "9")
            {
                td_BC.style.display='';
                tdBC.style.display='';
                td_CH.style.display='none';
                tdCH.style.display='none';
                td_CN.style.display='none';
                tdCN.style.display='none';
                td_EX.style.display='none';
                tdEX.style.display='none';
                td_CIN.style.display='none';
                tdCIN.style.display='none';
                tdCCR.style.display='none';
                td_OF.style.display='none';
                tdOF.style.display='none';
                td_RN.style.display='none';
                tdRN.style.display='none';
            
            
            }
             //Fahad 6054 07/20/2009 Case for Friend Credit
            else if (DDLPaymentMethod == "11")
            {
                td_RN.style.display='';
                tdRN.style.display='';
                td_BC.style.display='none';
                tdBC.style.display='none';
                td_CH.style.display='none';
                tdCH.style.display='none';
                td_CN.style.display='none';
                tdCN.style.display='none';
                td_EX.style.display='none';
                tdEX.style.display='none';
                td_CIN.style.display='none';
                tdCIN.style.display='none';
                tdCCR.style.display='none';
                td_OF.style.display='none';
                tdOF.style.display='none';
              
            
            
            }
            //Abbas Qamar  9957 10/02/2011 Case for Fee Waived
            else if (DDLPaymentMethod == "102")
            {
                td_RN.style.display='';
                tdRN.style.display='';
                td_BC.style.display='none';
                tdBC.style.display='none';
                td_CH.style.display='none';
                tdCH.style.display='none';
                td_CN.style.display='none';
                tdCN.style.display='none';
                td_EX.style.display='none';
                tdEX.style.display='none';
                td_CIN.style.display='none';
                tdCIN.style.display='none';
                tdCCR.style.display='none';
                td_OF.style.display='none';
                tdOF.style.display='none';
              
            
            
            }
            
            else
            {
                td_OF.style.display='none';
                tdOF.style.display='none';
                td_RN.style.display='none';
                tdRN.style.display='none';
                td_BC.style.display='none';
                tdBC.style.display='none';
                td_CH.style.display='none';
                tdCH.style.display='none';
                td_CN.style.display='none';
                tdCN.style.display='none';
                td_EX.style.display='none';
                tdEX.style.display='none';
                td_CIN.style.display='none';
                tdCIN.style.display='none';
                tdCCR.style.display='none';
                         
            }
            
         }
         function Hide()
         {//Hide Cardholder Cardnumber expiration and cin number on pageload..
            var td_CH=  document.getElementById("td_CH");
            var tdCH=  document.getElementById("tdCH");
            var td_CN=document.getElementById("td_CN");
            var tdCN=document.getElementById("tdCN");
            var td_EX=document.getElementById("td_EX");
            var tdEX=document.getElementById("tdEX");
            var td_CIN=document.getElementById("td_CIN");
            var tdCIN=document.getElementById("tdCIN");
            var td_BC=document.getElementById("td_BC");
            var tdBC=document.getElementById("tdBC");
            var td_OF=document.getElementById("td_OF");
            var tdOF=document.getElementById("tdOF");
            var td_RN=document.getElementById("td_RN");
            var tdRN=document.getElementById("tdRN");         
            
                td_OF.style.display='none';
                tdOF.style.display='none';
                td_RN.style.display='none';
                tdRN.style.display='none';
                td_BC.style.display='none';
                tdBC.style.display='none';
                td_CH.style.display='none';
                tdCH.style.display='none';
                td_CN.style.display='none';
                tdCN.style.display='none';
                td_EX.style.display='none';
                tdEX.style.display='none';
                td_CIN.style.display='none';
                tdCIN.style.display='none';
                tdCCR.style.display='none';
         }
         
         /////////////////////////////CREDIT CARD////////////////////////////////////////		
		//To prompt for void
		//ozair 3856 on 04/25/2008 included the invoice number and payment method parameters
		function PromptVoid(invNum,payMethod)
		{
		    doyou = confirm("Are you sure you want to void this transaction? (OK = Yes   Cancel = No)"); 
            if (doyou == true)
            {
                //setting the hidden fields 
                document.getElementById("hf_InvNum").value=invNum;
                document.getElementById("hf_PayMethod").value=payMethod;
                //dipalaying the void comments popup
                return ShowVoidCommentsPopup();                        
            }
            else
            {
                return false;			
            }
         //end ozair 3856
		}
		//ozair 3856 on 04/25/2008 
        //To display the pop up for entering General Comments for Payment Void
        function ShowVoidCommentsPopup() {
            document.getElementById("pnl_VoidComments").style.display = '';
            document.getElementById("pnl_VoidComments").style.top = 350+"px";
            document.getElementById("DisableDive").style.height = (document.body.offsetHeight * 2)+"px";
            document.getElementById("DisableDive").style.width = document.body.offsetWidth+"px";
            document.getElementById("DisableDive").style.display = '';
            return false;
        }
        //to hide the General Comments for Payment Void pop up on update button.
        function CloseVoidCommentsPopup() {
            //ozair 3871 on 04/25/2008 validatiing the payment void comments.
            var gComments = document.getElementById("WCC_VoidGeneralComments_txt_Comments");
            gComments.value = trim(gComments.value);
            if (gComments.value.length == 0) {
                alert("Please enter payment void reason.");
                if (gComments.style.display != "none")
                gComments.focus();
                return false;
            }
            //end ozair 3871		  		  
            document.getElementById("pnl_VoidComments").style.display = 'none';
            document.getElementById("DisableDive").style.display = 'none';
            return true;
        }
		function Validate()
		{
		    var ddlPaymentMethod=document.getElementById("ddlPaymentMethod").value;
		    var Cardholder=document.getElementById("txtCardholder").value;
		    var CardNo=document.getElementById("txtCardNumber").value;
		    var CIN=document.getElementById("txtCIN").value;
		    
		    if(ddlPaymentMethod == "5")
		    {
		        if(Cardholder =="")
		        {
		            alert("PLease Enter the Cardholder Name");
		            document.getElementById("txtCardholder").focus();
		            return false;
		        }
		        if(CardNo == "")
		        {
		            alert("PLease Enter the Credit Card Number");
		            document.getElementById("txtCardNumber").focus();
		            return false;
		        }
		        
		    }
		}
		
			function OpenSchedule(owes,schid,activeflag)
		{
			if (document.getElementById("lbl_lockflag").innerHTML=="False")
			{
				alert("Please 'Lock' the current estimated owed amount or make an adjustment on the Matter page");
				return false;			
			}			
			if (document.getElementById("lbl_contactcheck").innerHTML=="False")
			{
			 //Yasir Kamal 5362 12/30/2008 page name changed from "General Info" to "Contact" Page
			 
				alert("Please first specify client Contact Num(s) on Contact page");
				return false;			
			}

//          // commented .... as we have implemented 'No DL' option on general info page.				
//			if (document.getElementById("lbl_dlstate").innerHTML=="0" && document.getElementById("lbl_activeflag").innerHTML=="0" )
//			{
//				alert("Please Specify DL State on General Info Page ");	
//				return false;			
//			}
			
			if (document.getElementById("lbl_addresscheck").innerHTML=="0")
			{
				//Waqas 5864 07/06/2009 General to contact
				alert("Please verify the Address Check Box on Contact Page");	
				return false;			
			}	
		
				var PDFWin
			    PDFWin = window.open("SchedulePayment.aspx?owes="+owes+"&schid="+schid+"&activeflag="+activeflag+"&casenumber=<%=this.TicketId%>",'',"fullscreen=no,toolbar=no,width=370,height=165,left=100,top=300,status=no,menubar=no,scrollbars=yes,resizable=yes");				
				//window.history.back(); 					
				return false;
		}
		
		//Checking fields required for process amount 
		function ProcessAmount()
		{	
		var ShowModal="1";
		            
		            
		             ///-------------------------------------------------------------------------
		             //Asad Ali 8502 11/04/2010 Remove Validation as per requiremnet 
//                //Sabir Khan 8058 07/23/2010 Check for valid PMC Ticket Number
//			    if((document.getElementById("hf_court").value == "3004") && ((document.getElementById("hf_IsInvalidPMCCausenumber").value != "0") && (document.getElementById("hf_isInvalidPMCTicket").value != "0")))
//			    {
//			          //Muhammad Muneer 8276 9/15/2010 making nessary changes to the alert.
//			            alert(" Please provide either a valid ticket number or cause number for PMC according to the following rules:\n \n Cause Number length must be exactly 7 characters long and it should start with 1.\n \n Ticket number: \n 1. If start with 6 or 7 then length must be exactly 6 characters long \n 2. If start with E then length must be exactly 8 characters long \n 3.If Cause Number is valid, then Ticket Number should be valid or it will be blank");
//                        return false;
//			    }    
//			   if((document.getElementById("hf_court").value == "3004") && (document.getElementById("hf_IsInvalidPMCCausenumber").value != "0"))
//			   {
//			       alert("PMC Cause Number should be only 7 characters");			           
//			       return false; 
//			   }
            
            ///-------------------------------------------------------------------------
		
		        
		        //Asad Ali 8153 09/20/2010 if AL hearing is required and No DL is checked then restrict user to do the payment 
		        var hf_ActiveFlag = document.getElementById('<%=lbl_OscarActiveFlag.ClientID %>');
		        var hf_NoDL = document.getElementById('<%=hf_NoDL.ClientID %>');
		        var hf_IsALRHearingRequired = document.getElementById('<%=hf_IsALRHearingRequired.ClientID %>');
		        if(hf_ActiveFlag.innerHTML=="0" && hf_NoDL.value=="1" && hf_IsALRHearingRequired.value=="true")
		        {
		            alert("Please Provide Driver License No. On Contact Page");
		            return false;
		        }
		
		
		
                //Yasir Kamal 6109 07/17/2009 do not allow hiring if no court is assigned to one or more violations
                // Noufil 6109 08/08/2009 Add space after full stop
                    if(document.getElementById("hf_IsNoCourtAssigned").value == "True")
                    {
                        alert("One or more violations are not associated with any court house. Please first associate violations with a court house.");
			            document.getElementById("ddlPaymentMethod").selectedIndex=0;
			            return false;
                    }


                    //Waqas 5771 04/17/2009
                    if(document.getElementById("hf_DisplayCIDMsg").value == 1)
                    {
                        alert("CID has not been associated, Please associate any CID before process payment.");
			            document.getElementById("ddlPaymentMethod").selectedIndex=0;
			            return false;
                    }
		        	
		        	if(document.getElementById("hf_DisplayConfirmedMsg").value == 1)
                    {
                        alert("CID has not been confirmed, Please confirm CID before process payment.");
			            document.getElementById("ddlPaymentMethod").selectedIndex=0;
			            return false;
                    }
                    
                    //Noufil 5819 05/15/2009 Check arrest date for ALR violation.
                    //Waqas 5864 06/23/2009 Message changed.
                    if(document.getElementById("hf_checkALRnullArrestdate").value == "True")
                    {
                        alert("Please enter arrest date on Matter's page to hire the client");
			            document.getElementById("ddlPaymentMethod").selectedIndex=0;
			            return false;
                    }
                    
                    if(document.getElementById("hf_checkALRArrestdateRange").value == "True")
                    {
                        alert("15 day deadline has passed.");
			            document.getElementById("ddlPaymentMethod").selectedIndex=0;
			            return false;
                    }
                                       
                    	                            
		          SelectCard();
    			    // Sabir Khan 4636 08/21/2008 check for association.
			       if(document.getElementById("hf_AssociatedCaseType").value == 1)
			       {
			        alert("Please first change all court locations that not belongs to selected case type on Matter's Page");
			        document.getElementById("ddlPaymentMethod").selectedIndex=0;
			        return false;
			       }
			        
			        //Zeeshan Ahmed 04/08/2008
			        var bondflag = <%=ViewState["BondFlag"]%>;
                    var hasFTATickets = <%=ViewState["HasFTATickets"]%>;
                    var isInsideCase = <%=ViewState["IsInsideCase"]%>;  
                     // Noufil 4324 07/11/2008 if amount enter is greater then  amount owes then show a error
		            intamount=document.getElementById("txtAMT").value;
			        intowes=document.getElementById("lbl_owes").innerHTML.substring(1);		
			        var paymentType = document.getElementById("ddlPaymentMethod").value;			        
			        //Zeeshan 4444 08/13/2008 Fixed Bounce Check & Refund Payment Issue
			        if ( (paymentType !=8 && paymentType !=103) && parseInt(intamount) > parseInt(intowes))
			        {
			            alert("Sorry Incorrect payment amount. A client cannot pay more than he owes");
			            return false;
			        }
			        
			        if ( bondflag == "1" && isInsideCase == "1" && hasFTATickets== "0" && document.getElementById("lbl_activeflag").innerHTML == "0" )
			        {
			            alert("Sorry Payment cannot be processed!. Please change bond flag to No or Add an FTA ticket to process the payment.");
			            document.getElementById("txtAMT").focus();
			            return false;
			        }
                    
			       // Added by Asghar for checking No Check Flag druing payment
			       if((document.getElementById("hf_nocheck").value =="True") && (document.getElementById("ddlPaymentMethod").value =="2"))
			       {
			        alert("The �No Check� flag is activated. This payment cannot be processed with a check.");
			        document.getElementById("ddlPaymentMethod").selectedIndex=0;
			        return false;
			       }
			       
			       if(document.getElementById("hf_nos").value == "FALSE" && document.getElementById ("hf_InvalidCauseNo").value == 0)
			       {
			        alert("Please enter a 'Cause Number' for HCJP court that starts with 'TR', 'BC' or 'CR' or select 'Not On System' flag if cause number is unavailable");
			        document.getElementById("ddlPaymentMethod").selectedIndex=0;
			        return false;
			        }
			       //Yasir Kamal 7748 05/25/2010  treat M.O./CashierCheck same as cash
//			       if((document.getElementById("hf_nocheck").value =="True") && (document.getElementById("ddlPaymentMethod").value =="4"))
//			       {
//			        alert("The �No Check� flag is activated. This payment cannot be processed with a M.O./CashierCheck.");
//			        document.getElementById("ddlPaymentMethod").selectedIndex=0;
//			        return false;
//			       }
			    
				// Added By Zeeshan Ahmed
				
				if ( document.getElementById("hf_owesdiff").value == "1")
				{
				    alert("The owed price does not equal the estimated owed price on the violation fees page. Please recalculate the price on the violation fees page.");
				    return false;				
				}
				
				//Added by Azwar
				
				if (document.getElementById("HF_Courtid").value == "0")
			    {
				    alert("Please first specify the court location on violation fee page.");	
				    return false;			
			    }
			    
			    //Yasir Kamal 6194 07/23/2009 do not allow hiring if court date in past and criminal case.	
			    //Waqas 6342 08/17/2009 Now only ALR case will not allow past court dates, otherwise all other criminal case or DWi can hv past court dates.		    
				if(document.getElementById("hf_CourtDateMain").value == "0" && document.getElementById("hf_isCriminalCase").value == "True" && document.getElementById("hf_ALRProcess").value == "1")
		        {
		            alert("System not allow to hire a client of ALR case with past court date");
		                return false;
		        }
				// Babar Ahmad 9243 05/12/2011 if user belongs to HCJP 2-1, 2-2, 3-2, 6-1, 6-2, 7-1 court and is not an active client, the system will restrict the payment.
				
				//Sabir Khan 9243 11/29/2011 Change the hard coded court ids into active/inactive court.
				if ((document.getElementById("lbl_activeflag").innerHTML=="0") && (document.getElementById("hfIsHCJP").value != "0") && (document.getElementById("hf_ActiveInactiveCourt").value == "1") )
				{
				    alert("Currently we are not dealing cases in this court");
				    return false;
				
				}
			    				    			    
			    //Sabir Khan 5338 12/26/2008 Check if past court date and without bond...			   
				//if(document.getElementById("hf_CourtDateMain").value == "0" && bondflag == "0" && document.getElementById("hf_isCriminalCase").value == "False")
				// Noufil 6164 08/11/2009 Remove Sabir Check and add case type for Traffic only
				if(document.getElementById("hf_CourtDateMain").value == "0" && bondflag == "0" && document.getElementById("hf_casetype").value == "1")
		        {
		            alert("Can not hire client with past court date without bonds.");
		            return false;
		        }
				//added by Azwar
				
				if(document.getElementById("lblFTACase").innerHTML != "0")
				{
				    alert("Cannot process with case status of FTA.Please select another case status.")
				    return false;
				}
				
				//Added By Ozair
				if(document.getElementById("lbl_ToProcess").innerHTML!="0")
				{
				    alert("Sorry Payment cannot be processed! Case status of underlying violation(s) has not been set.")
				    return false;
				}
			<%Session["TimeStemp"]=txtTimeStemp.Text;%>;
			//
			if (document.getElementById("lbl_lockflag").innerHTML=="False")
			{
				alert("Please 'Lock' the current estimated owed amount or make an adjustment on the Matter Page");
				return false;			
			}			
			if (document.getElementById("lbl_contactcheck").innerHTML=="False")
			{
			    //Yasir Kamal 5362 12/30/2008 page name changed from "General Info" to "Contact" Page
			 
				alert("Please first specify client Contact Num(s) on Contact page");
				return false;			
			}

//            // commented .... as we have implemented 'No DL' option on general info page.				
//			if (document.getElementById("lbl_dlstate").innerHTML=="0" && document.getElementById("lbl_activeflag").innerHTML=="0" )
//			{
//				alert("Please Specify DL State on General Info Page ");	
//				return false;			
//			}
			
			if (document.getElementById("lbl_addresscheck").innerHTML=="0")
			{
				//Waqas 5864 07/06/2009 General to contact
				alert("Please verify the Address Check Box on Contact Page");	
				return false;			
			}

			//Amount Related
			var intamount=0;
			var intowes=0;
			var totalfee=0;
			var paidamount=0;
			
			intamount=document.getElementById("txtAMT").value;
			intowes=(document.getElementById("lbl_owes").innerHTML)*1;			
			totalfee=document.getElementById("HFFee").value;
			
			 var owes=document.getElementById("HFOwes").value;
			paidamount=totalfee-owes;
			
			if (isNaN(document.getElementById("txtAMT").value) == false && intamount!="" && intamount !=0  )
			{			
				if( intamount > intowes &&(document.getElementById("ddlPaymentMethod").value!=8) )
				{ 
					alert ("Sorry Incorrect payment amount. A client cannot pay more than he owes");
					document.getElementById("txtAMT").focus(); 
					return false;				
				}
				if(intamount<0)
				{
					alert ("Sorry Incorrect payment amount. A client cannot owe a negative");
					document.getElementById("txtAMT").focus(); 
					return false;				
				}				
			}
			else
			{
					alert ("Sorry incorrect payment amount.");
					document.getElementById("txtAMT").focus(); 
					return false;							
			}
			//Farrukh 9398 09/15/2011 validation applied for PaymentIdentifier
			if (document.getElementById("ddlPaymentMethod").value=='0' || document.getElementById("ddlPaymentMethod").value=='10')
			{
				alert ("Please Specify your payment method.");
				document.getElementById("ddlPaymentMethod").focus(); 
				return false;				
			}
			if (document.getElementById("td_PaymentIdentifier").style.display == '' && document.getElementById("ddl_PaymentIdentifier").value == '0' )
			{
				alert ("Please Specify your payment type.");
				document.getElementById("ddl_PaymentIdentifier").focus(); 
				return false;				
			}
			var paid=(document.getElementById("lbl_Paid").innerHTML)*1;		
			if (document.getElementById("ddlPaymentMethod").value==8 && paid < intamount )
			{
				alert ("Sorry, Cannot Refund more than the amount paid.");
				document.getElementById("txtAMT").focus(); 
				return false;				
			}
			
			if (document.getElementById("ddlPaymentMethod").value==8 &&  intamount > paidamount )
			{
				alert ("Sorry, Cannot Refund more than the amount paid.");
				document.getElementById("txtAMT").focus(); 
				return false;				
			}
			
			//Added By Zeeshan Ahmed On 1/18/2008
			//Bounce Check Validation Implemented
			
			var paid=(document.getElementById("lbl_Paid").innerHTML)*1;		
			var paymentType =document.getElementById("ddlPaymentMethod").value
			
			if (paymentType==103 && paid < intamount )
			{
				alert ("Bounce check amount cannot be greater than the amount paid.");
				document.getElementById("txtAMT").focus(); 
				return false;				
			}
			
			if (paymentType==103  &&  intamount > paidamount )
			{
				alert ("Bounce check amount cannot be greater than the amount paid.");
				document.getElementById("txtAMT").focus(); 
				return false;				
			}
								
			//credit card related
			///////// Checking Transaction mode////
			    if((document.getElementById("hf_transactionmode").value =="1") && (document.getElementById("ddlPaymentMethod").value =="5"))
			     {
			        doyou =confirm("This transaction is running in test mode. Are you sure you want to process it further?");
			        if(doyou == false)
			            return false;
			                
			     }		
			     
			if (document.getElementById("ddlPaymentMethod").value==5)
			{
			   if(document.getElementById("txtCardholder").value=="")
			   {
				alert ("Please enter the credit card holder's name")		
				document.getElementById("txtCardholder").focus(); 
				return false;				
			   }
			   		   
			   
			   if(document.getElementById("txtCardNumber").value=="")
			   {
				alert ("Please enter your credit card number.")
				document.getElementById("txtCardNumber").focus(); 
				return false;				
			   }
//			    if(document.getElementById("txtCIN").value=="")
//			   {
//				alert ("Please enter your CIN number.")
//				document.getElementById("txtCIN").focus(); 
//				return false;				
//			   } 
			   if(isNaN(document.getElementById("txtCIN").value)==true)
			   {
			        alert ("Please enter valid cinn number.")
				    document.getElementById("txtCIN").focus(); 
				    return false;				
			   }
			   
			   //CALLING FUNCTION TO VALIDATE CARD NUMBER
			  
			   if (CheckCardNumber(document.forms[0])==false)		   
			   return false; 			   
			    //added by ozair
			   //if(document.getElementById("ddlPaymentMethod").value!='5')
					//HideCreditDetail();
			
			   if (document.getElementById("lbl_yds").innerHTML=="N")
				{			
					doyou =confirm("The address provided is not correct.\nWould you like to continue with the payment?");
					if (doyou == true)		
					{
					//Yasir Kamal 6464 08/25/2009 Payment bug fixed.
						//document.getElementById("Showbtn").disabled = true;
					// if(document.getElementById("ddlPaymentMethod").value!=9 && document.getElementById("ddlPaymentMethod").value!= 11 && document.getElementById("ddlPaymentMethod").value!=104)
					// {
						document.getElementById("Showbtn").style.display = 'none';
						document.getElementById("hidebtn").style.display = '';
						//document.getElementById("hidebtn").disabled = true;
					//	}
		             // Afaq 8213 11/27/2010 Calling function
			         return chkPaymentIdentifier();				
					}					
					return false;				
				}
							
			}//credit card process ends
			
			//Fahad 6054 07/20/2009 Case Added for Attorney Credit Payment Type
			if ((document.getElementById("ddlPaymentMethod").value==8 && document.getElementById("hf_CheckPaymentType").value =="1")||(document.getElementById("ddlPaymentMethod").value==103 ))
			{
			    alert ("Payment refund and Bounce check options are not allowed in case of attorney credit, friend credit or partner firm credit")		
				document.getElementById("ddlPaymentMethod").focus(); 
				return false;				
			   
			}
			
			//Fahad 6054 07/20/2009 Case Added for Attorney Credit Payment Type
			if (document.getElementById("ddlPaymentMethod").value==9)
			{

			 var txtbox=document.getElementById("txtBarCardNumber").value;
			   if(txtbox=="")
			   {
				alert ("Please enter the bar card number")		
				document.getElementById("txtBarCardNumber").focus(); 
				return false;				
			   }
			   else if(txtbox!="")
			   {

			         if(isAlphabetNumeric(txtbox)==false)
                        {
//                            alert("Please enter valid bar card number!");
//                            return false;
//Fahad 6054 07/20/2009 Validate User input 
                            ShowModal="0";
                            return false;
                        }	
			   }
			}
			//Fahad 6054 07/20/2009 Case Added for Attorney Credit Payment Type
			if ((document.getElementById("ddlPaymentMethod").value==11 || document.getElementById("ddlPaymentMethod").value==102))
			{
			   if(document.getElementById("ddlRepsName").value==0)
			   {
				alert ("Please select the user from the user list")		
				document.getElementById("ddlRepsName").focus(); 
				return false;				
			   }
			}
			//Fahad 6054 07/20/2009 Case Added for Attorney Credit Payment Type
			if (document.getElementById("ddlPaymentMethod").value==104)
			{
			   if(document.getElementById("ddlOutsideFirm").value==0)
			   {
				alert ("Please select the firm from the firm list")		
				document.getElementById("ddlOutsideFirm").focus(); 
				return false;				
			   }
			}
			
			
			if(document.getElementById("lbl_WalkFlag").innerHTML =="")	
			{
			    //Waqas 5864 07/06/2009 General to Contact
			    alert("Please select Walk in Client Flag on Contact Page.");
			    return false;
			}
//            //Sabir Khan 7181 12/24/2009 Do'nt allow hiring client for PMC if ticketno is empty or has pmc in ticketno....
//            if((document.getElementById("hf_isInvalidPMCTicket").value != "0") && (document.getElementById("lbl_activeflag").innerHTML == "0"))
//            { 
//               alert("Ticket number is not correct. Please first update the ticket number");
//               return false;
//            }
           			
			if(document.getElementById("HFNoTrialLetter").value == "1")
			{
			    doyou =confirm("Trial Notification Letter will not be sent do you want to continue?");
			    if(doyou==true)
			    {
			        //return true;
			    }
			    else
			    {
			        return false;
			    }
			    
			}
			
			//Nasir 10/07/2009 
			if (document.getElementById("lbl_yds").innerHTML=="N")
            {                                              
                doyou =confirm("The address provided is not correct.\nWould you like to continue with the payment?");
                if (doyou == true)                            
                                {
                                    document.getElementById("Showbtn").style.display = 'none';
                                    document.getElementById("hidebtn").style.display = '';
                                }
                else
                    {                                              
                         return false;                                                       
                    }
            }

			
            if (ShowModal=="1")
            {
			//Fahad 6054 07/22/2009 Add the Case
			    if(document.getElementById("ddlPaymentMethod").value==9 ||document.getElementById("ddlPaymentMethod").value== 11 ||document.getElementById("ddlPaymentMethod").value==104)
			    {

			        if(document.getElementById("hf_CheckClientonSameDate").value == "0")
			        {

			            var modalPopupBehavior = $find('Modal_Message');
                        modalPopupBehavior.show();
                        return false;            
        			
			        }
			    }
			} 
			// Afaq 8213 11/27/2010 Calling function
			return chkPaymentIdentifier();
		}	
		// Afaq 8213 11/27/2010 Function to apply check on the basis of payment identifier.
		function chkPaymentIdentifier()
		{
		    //Afaq 8213 11/01/2010 if Continuance flag is added and continuance is selected as a payment identifier then Continuance comment popup will show.
			var isTrue = true;  
		    if (document.getElementById('hf_continuancecount').value>0 && document.getElementById("ddl_PaymentIdentifier").value == 1)
		    {
		        return ShowContinuanceCommentPopup();
		    }
		    //Afaq 8213 11/01/2010 if Continuance flag is not added and continuance is selected as a payment identifier then alert will display.
		    else if (document.getElementById('hf_continuancecount').value==0 && document.getElementById("ddl_PaymentIdentifier").value == 1)
		    {
		        alert('"Continuance Flag is not present.�There must be a "Continuance" flag added before�you can process his payment." When rep would add continuance flag manually then continuance amount would automatically appear in AMT.');
		        isTrue = false;
		        
		    }
		    //Afaq 8213 11/01/2010 if bond flag is not added and bond is selected as payment identifier than alert will dispaly.
		    else if (<%=ViewState["BondFlag"]%>=="0" && document.getElementById("ddl_PaymentIdentifier").value == 3)
		    {
		        alert("Client doesn't appear to be in bond status. He must be in bonds before you can process his payment");
                 isTrue = false;
                           
		    }
			
			if (!isTrue)
			{
			    document.getElementById("Showbtn").style.display = '';
                document.getElementById("hidebtn").style.display = 'none';			    
			}
			
			return isTrue;
		}
			// noufil 4444 07/22/2008 This function checks the whether the input are alphanumeric or not
                function alphanumeric(alphane)
                {                
	                var numaric = alphane;
	                for(var j=0; j<numaric.length; j++)
		                {
		                      var alphaa = numaric.charAt(j);
		                      var hh = alphaa.charCodeAt(0);		                      
		                      //ASCII code for aphabhets and 32 for space bar
		                      if (!((hh > 64 && hh<91) || (hh > 96 && hh<123)|| hh==32))
		                      {
		                        return false;
		                      }    
		                }
	            }
		
		
		function SelectCard()
		{
		//a;
			var cardnum=document.getElementById("txtCardNumber").value;
			var ddl_cardtype=document.getElementById("ddl_cardtype");
			
			if (cardnum.charAt(0)==4)
			{
				ddl_cardtype.value=	1			
			}	
			if (cardnum.charAt(0)==5)
			{
				ddl_cardtype.value=	2			
			}		
			if (cardnum.charAt(0)==3)
			{
				ddl_cardtype.value=	3			
			}	
			if (cardnum.charAt(0)==6)
			{
				ddl_cardtype.value=	4			
			}	
		}
		function PromptDelete()
		{	
		
		 doyou = confirm("Are you sure you want to Delete the Schedule Amount? (OK = Yes   Cancel = No)"); 
         if (doyou == true)
         {
			//	document.getElementById("lbl_schcount").innerHTML=="1" && 
			if(document.getElementById("lbl_Paid").innerHTML=="0")
			{
				if((document.getElementById("txt_schedulesum").value)*1==(document.getElementById("lbl_owes").innerHTML)*1)		
				{	
					abc=confirm("This will move the Client to Quote. Click (OK = Yes  Cancel = No) ");
					if (abc==true)
					{
						return true;
					}
					return false;
				}
			}			
			//
			return true;
         }
         return false;			
		}		
		
		//Zeeshan Ahmed 3948 05/14/2008
		function JuryTrialAlert()
		{
		    //ozair 4191 06/05/2008 ".... should not be in ...." changed to ".... should be in ...."
		    alert("Court dates have been passed for all the referenced tickets OR Case should be in jury, judge or pre trial status. Please set a future court date OR Change case status then the trial letter will be available to you.");
		    return false;
		}
		
		//
		//Ozair 4967 10/15/2008 Trial Letter Alert for Non Traffic Case Types
		function TrialCaseTypeAlert()
		{
		    alert("Trial Notification letters can only be printed for Traffic case types.");
		    return false;
		}
		//Fahad 5908 05/15/2009 Function to Open E-signature Exe
        function OpenESignature()
        {
           var getdate = new Date();
           var ticketid= document.getElementById("hf_TicktEmployeeId").value;
           var sessionid=document.getElementById("hf_SessionId").value;
           sessionid = sessionid + getdate.getHours() + getdate.getMinutes() + getdate.getSeconds();
           var exepath=document.getElementById("hf_ExePath").value;
		   var oShell = new ActiveXObject("Shell.Application");
		   var paramShell = ticketid +"-"+ "TTTTT" +"."+ sessionid;
		   oShell.ShellExecute(exepath+"ESignatures.exe", paramShell, "", "open", "1");
        }	
        
        // Noufil 6052 10/01/2009 Show confirmation box on deleting
        function AskConfirmation()
        {
            if (confirm("Are you sure you want to remove the Attorney payout amount?"))
                return true;
            else
                return false;            
        }
        
        
    </script>

</head>
<body onload="Hide()">
    <form id="frmpayment" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <div id="DisableDive" style="display: none; position: absolute; left: 1; top: 1;
        height: 1px; background-color: Silver; filter: alpha(opacity=50)">
        <table width="100%" height="100%">
            <tr>
                <td style="width: 100%; height: 100%">
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tr>
                <td colspan="5">
                    <aspnew:UpdatePanel ID="UpdPnlActiveMenu" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnl" runat="server">
                                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                        </Triggers>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td id="td_yds">
                    <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="780">
                        <tbody>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="2" style="height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" height="20px">
                                    <asp:Label ID="lbl_BranchName" runat="server" Font-Bold="True" Font-Size="X-Large"
                                        ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" style="height: 20px">
                                    <table id="tblname" border="0" cellpadding="0" cellspacing="0" style="width: 897px">
                                        <tr>
                                            <td style="height: 19px" width="100%">
                                                <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                                <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                                <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>),
                                                <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink>
                                                <a href="#"></a>
                                            </td>
                                            <td align="right" style="width: 19%; height: 19px">
                                                &nbsp;
                                            </td>
                                            <td align="right" style="height: 19px" width="6%">
                                                <asp:Button ID="btn_ESignature" runat="server" CssClass="clsbutton" Text="Sign Documents"
                                                    Width="103px" TabIndex="1" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lbl_transactionmode" runat="server" Font-Bold="True" Font-Size="Larger"
                                        ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr id = "tr_errorMessages" runat= "server" style="display: none;" >
                                <td align="left" colspan="2" rowspan="1" style="height: 16px">
                                    <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always" EnableViewState="true">
                                        <ContentTemplate>
                                            <asp:Label ID="lblMessage" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                ForeColor="Red"></asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="btnCIDInCorrect" EventName="click"></aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                                        </Triggers>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" rowspan="1">
                                    <asp:Label ID="lbl_flag" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" rowspan="1">
                                    <asp:Label ID="lblPasadenaJudge" runat="server" ForeColor="Red" Font-Bold="true"
                                        Font-Size="14px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" rowspan="1">
                                    <asp:Label ID="lbl_cause" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table runat="server" id="tbl_CIDConfirmation" style="display: none" cellspacing="0"
                                                cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td style="height: 30px">
                                                        <asp:Label ID="lblCIDCorrectConfirmation" runat="server" ForeColor="Red" Font-Names="Verdana"
                                                            Font-Bold="True">Please confirm that the following CID information is 
                                                        correct?</asp:Label>
                                                        <asp:Button ID="btnCIDCorrect" runat="server" CssClass="clsbutton" Text="Correct"
                                                            OnClick="btnCIDCorrect_Click"></asp:Button>
                                                        &nbsp;<asp:Button ID="btnCIDInCorrect" runat="server" CssClass="clsbutton" Text="Incorrect"
                                                            OnClick="btnCIDInCorrect_Click"></asp:Button>
                                                        <asp:HiddenField ID="hf_DisplayCIDMsg" runat="server"></asp:HiddenField>
                                                        <asp:HiddenField ID="hf_DisplayConfirmedMsg" runat="server"></asp:HiddenField>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                            <tr id = "tr_InfoMessages" runat = "server" style =" display:none;">
                                <td align="left" colspan="2" rowspan="1" style="height: 16px">
                                    <asp:Label ID="lblbadnumber" runat="server" ForeColor="Red" Font-Names="Verdana"
                                        Font-Size="X-Small" Visible="False" Font-Bold="True">Telephone Number Alert : Please update client contact information.</asp:Label>
                                    <br />
                                    <asp:Label ID="lblbademail" runat="server" ForeColor="Red" Font-Names="Verdana" Font-Size="X-Small"
                                        Visible="False" Font-Bold="True">Bad Email</asp:Label>
                                    <br />
                                    <asp:Label ID="lblProblemClient" runat="server" ForeColor="Red" Font-Names="Verdana"
                                        Visible="False" Font-Bold="True">This is a problem client.</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td id="read" runat="server" style="width: 100%;" colspan="2">
                                    <uc2:ReadNotes ID="ReadNotes1" runat="server" />
                                </td>
                            </tr>
                            <tr style="color: #000000">
                                <td background="../Images/subhead_bg.gif" class="clssubhead" height="34" width="38%">
                                    &nbsp; Fee Information
                                </td>
                                <td align="right" background="../Images/subhead_bg.gif" class="clssubhead" height="34">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblNoCheckflag" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"
                                                    CssClass="Label" Font-Names="Arial" Font-Size="Medium">NO CHECK</asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="clsInputCombo">
                                                    <asp:ListItem Value="1">Active Plan</asp:ListItem>
                                                    <asp:ListItem Value="2">Default Plan</asp:ListItem>
                                                    <asp:ListItem Value="3">Waived Plan</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table id="tblTotalfee" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr class="clsLeftPaddingTable">
                                            <td class="clsLeftPaddingTable" height="20" valign="top">
                                                Fee
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top">
                                                Paid
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top">
                                                Owes
                                            </td>
                                            <td height="20" valign="top" class="clsLeftPaddingTable">
                                                AMT
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top" id="td_lbl_PaymentIdentifier"
                                                runat="server">
                                                Payment Type
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top">
                                                &nbsp;Method
                                            </td>
                                            <td height="20" valign="top" class="clsLeftPaddingTable" id="td_BC">
                                                Bar Card Number
                                            </td>
                                            <td height="20" valign="top" class="clsLeftPaddingTable" id="td_RN">
                                                Reps Name
                                            </td>
                                            <td height="20" valign="top" class="clsLeftPaddingTable" id="td_OF">
                                                Outside Firms
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top">
                                            </td>
                                            <td height="20" valign="top" class="clsLeftPaddingTable" id="td_CH">
                                                Cardholder
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top" id="td_CN">
                                                Card Number
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top" id="td_EX">
                                                Expiration
                                            </td>
                                            <td class="clsLeftPaddingTable" height="20" valign="top" id="td_CIN">
                                                CIN
                                            </td>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px">
                                                <asp:Label ID="lbl_Fee" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px">
                                                <asp:Label ID="lbl_Paid" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px">
                                                <asp:Label ID="lbl_owes" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                            <td valign="top" class="clsLeftPaddingTable" style="height: 20px">
                                                $<asp:TextBox ID="txtAMT" runat="server" Width="30px" MaxLength="8" CssClass="clsInputadministration"></asp:TextBox>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px" id="td_PaymentIdentifier"
                                                runat="server">
                                                <asp:DropDownList ID="ddl_PaymentIdentifier" runat="server" CssClass="clsInputCombo"
                                                    Width="84px">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px">
                                                &nbsp;<asp:DropDownList ID="ddlPaymentMethod" runat="server" CssClass="clsInputCombo">
                                                </asp:DropDownList>
                                            </td>
                                            <td id="tdBC" valign="top" class="clsLeftPaddingTable" style="height: 20px">
                                                <asp:TextBox ID="txtBarCardNumber" runat="server" CssClass="clsInputadministration"
                                                    Width="83px" MaxLength="10"> </asp:TextBox>
                                            </td>
                                            <td id="tdOF" class="clsLeftPaddingTable" valign="top" style="height: 20px">
                                                &nbsp;<asp:DropDownList ID="ddlOutsideFirm" runat="server" CssClass="clsInputCombo">
                                                </asp:DropDownList>
                                            </td>
                                            <td id="tdRN" class="clsLeftPaddingTable" valign="top" style="height: 20px">
                                                &nbsp;<asp:DropDownList ID="ddlRepsName" runat="server" CssClass="clsInputCombo">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="clsLeftPaddingTable" style="height: 20px; display: none" valign="top"
                                                id="tdCCR">
                                                <asp:ImageButton ID="imgbtn_creditcard" runat="server" Height="18px" ImageUrl="~/Images/creditcard1.jpg"
                                                    Width="21px" />
                                            </td>
                                            <td valign="top" class="clsLeftPaddingTable" style="height: 20px" id="tdCH">
                                                <asp:TextBox ID="txtCardholder" runat="server" CssClass="clsInputadministration"
                                                    Width="83px"></asp:TextBox>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px" id="tdCN">
                                                <asp:TextBox ID="txtCardNumber" runat="server" CssClass="clsInputadministration"
                                                    Width="83px" MaxLength="25"></asp:TextBox>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px" id="tdEX">
                                                <asp:DropDownList ID="ddl_expmonth" runat="server" CssClass="clsInputCombo" Width="48px">
                                                    <asp:ListItem Selected="True" Value="01">01</asp:ListItem>
                                                    <asp:ListItem Value="02">02</asp:ListItem>
                                                    <asp:ListItem Value="03">03</asp:ListItem>
                                                    <asp:ListItem Value="04">04</asp:ListItem>
                                                    <asp:ListItem Value="05">05</asp:ListItem>
                                                    <asp:ListItem Value="06">06</asp:ListItem>
                                                    <asp:ListItem Value="07">07</asp:ListItem>
                                                    <asp:ListItem Value="08">08</asp:ListItem>
                                                    <asp:ListItem Value="09">09</asp:ListItem>
                                                    <asp:ListItem Value="10">10</asp:ListItem>
                                                    <asp:ListItem Value="11">11</asp:ListItem>
                                                    <asp:ListItem Value="12">12</asp:ListItem>
                                                </asp:DropDownList>
                                                /
                                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="clsInputCombo" Width="52px">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="clsLeftPaddingTable" valign="top" style="height: 20px" id="tdCIN">
                                                <asp:TextBox ID="txtCIN" runat="server" CssClass="clsInputadministration" Width="30px"></asp:TextBox>
                                            </td>
                                            <td id="Showbtn" class="clsLeftPaddingTable">
                                                <asp:Button ID="btnProcess" runat="server" CssClass="clsbutton" Text="Process" OnClick="btnProcess_Click" />
                                            </td>
                                            <td id="hidebtn" class="clsLeftPaddingTable">
                                                <asp:Button ID="Button1" runat="server" CssClass="clsbutton" Text="Process" Enabled="False">
                                                </asp:Button>
                                            </td>
                                            <td class="clsLeftPaddingTable">
                                                <asp:Image ID="imgclienttrue" runat="server" ImageUrl="~/Images/right.gif" Visible="False"
                                                    ToolTip="Client" />
                                                <asp:Image ID="imgclientfalse" runat="server" ImageUrl="~/Images/cross.gif" Visible="False"
                                                    ToolTip="Quote" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="clsLeftPaddingTable" style="display: none" valign="top" height="20">
                                                Card Type
                                            </td>
                                            <td style="display: none" valign="top" height="20">
                                                &nbsp;
                                                <asp:DropDownList ID="ddl_cardtype" runat="server" CssClass="clsInputCombo" Width="176px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="font-size: 10pt; color: #ff0000">
                                <td background="../../images/separator_repeat.gif" colspan="2" style="height: 11px">
                                </td>
                            </tr>
                            <!-- next section starts -->
                            <tr style="font-size: 10pt; color: #ff0000">
                                <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" height="34">
                                    &nbsp;Payment Details
                                </td>
                            </tr>
                            <tr class="clsLeftPaddingTable">
                                <td width="100%" colspan="2">
                                    <asp:DataGrid ID="dg_PaymentDetail" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                        CssClass="clsLeftPaddingTable" PageSize="2" Width="100%" OnItemDataBound="dg_PaymentDetail_ItemDataBound">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Payment Date &amp; Time">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="138px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_paydate" runat="server" CssClass="Label" Text='<%# bind("recdate","{0:MM/dd/yyyy @hh:mm tt}")  %>'
                                                        Font-Size="Smaller"></asp:Label>&nbsp;
                                                    <asp:HiddenField ID="hf_paydate" runat="server" Value='<%# bind("recdate","{0:MM/dd/yyyy}")  %>'>
                                                    </asp:HiddenField>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Rep">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="23px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_emp" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="AMT">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="8%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_amount" runat="server" CssClass="Label" Text='<%# "$" +  Eval("paid","{0:F2}") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lbl_invoicenum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.invoicenumber_pk") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lbl_vflag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.voidflag") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Type">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="160px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_paymethod" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'
                                                        Font-Size="Smaller"></asp:Label>&nbsp;
                                                    <asp:Label ID="lbl_transnum" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.transnum") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Payment Identifier" ItemStyle-CssClass="label">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="98px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentIdentifier" runat="server" CssClass="Label" Text=' <%#Eval("PaymentIdentifier") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Card Name" Visible="False">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="130px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCardName" runat="server" CssClass="Label" Text='<%# bind("NAME") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                    <asp:Label ID="lblrowid" runat="server" CssClass="Label" Text='<%# bind("rowid") %>'
                                                        Visible="False" Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Card #" Visible="False">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="40px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCardNo" runat="server" CssClass="Label" Text='<%# bind("ccnum") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="CIN" Visible="False">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="6px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCINN" runat="server" CssClass="Label" Text='<%# bind("cin") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Exp" Visible="False">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="6px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExpDate" runat="server" CssClass="Label" Text='<%# bind("expdate") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auth" Visible="False">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="8px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAuthCode" runat="server" CssClass="Label" Text='<%# bind("auth_code") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Response" Visible="False">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" Width="15px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblResponse" runat="server" CssClass="Label" Text='<%# bind("response_reason_text") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Branch" Visible="True">
                                                <HeaderStyle CssClass="clsaspcolumnheader" Font-Size="Smaller" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranchName" runat="server" CssClass="label" Text='<%# bind("branchName") %>'
                                                        Font-Size="Smaller"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkb_edit" runat="server" CausesValidation="false" CommandName="Edit"
                                                        Enabled="False" Text="E">Void</asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    &nbsp;
                                                </EditItemTemplate>
                                                <ItemStyle Width="25px" />
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle NextPageText="&amp;gt; Next" PrevPageText="Previous &amp;lt; " />
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <!--aa-->
                            <!--main-->
                            <tr id="tr_attorneyseperator" runat="server" style="display: none;">
                                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                                </td>
                            </tr>
                            <tr id="tr_attorneysubhead" runat="server" style="font-size: 10pt; color: #ff0000;
                                display: none;">
                                <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" height="34">
                                    &nbsp;Attorney payout
                                </td>
                            </tr>
                            <tr id="tr_attorneygrid" runat="server" style="display: none;">
                                <td width="100%" colspan="2">
                                    <asp:GridView ID="gv_AttorneyPayouthistory" runat="server" AutoGenerateColumns="False"
                                        Width="100%" CssClass="clsLeftPaddingTable" PageSize="20" CellPadding="0" CellSpacing="0"
                                        OnRowCommand="gv_AttorneyPayouthistory_RowCommand" OnRowDeleting="gv_AttorneyPayouthistory_RowDeleting"
                                        OnRowDataBound="gv_AttorneyPayouthistory_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Cause Number" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_causenumber" runat="server" CssClass="GridItemStyle" Text='<%# Eval("casenumassignedbycourt") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Violation Description" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_desc" runat="server" CssClass="GridItemStyle" Text='<%# Eval("[Description]") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Charge Level" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_clevel" runat="server" CssClass="GridItemStyle" Text='<%# Eval("LevelCode") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_pdate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("AttorneyPaidDate","{0:MM/dd/yyyy}") + " @ " + Eval("AttorneyPaidDate","{0:hh:mm tt}")  %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Amount" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Pamount" runat="server" CssClass="GridItemStyle" Text='<%#"$ " + Convert.ToInt32(Eval("AttorneyPaidAmount"))  %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rep's Name" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_rname" runat="server" CssClass="GridItemStyle" Text='<%# Eval("EmpID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Case Disposition date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_cdDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("dispositiondate","{0:MM/dd/yyyy}") + " @ " + Eval("dispositiondate","{0:hh:mm tt}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandName="delete"
                                                        OnClientClick="return AskConfirmation();" />
                                                    <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# Eval("TicketsViolationID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle CssClass="clssubhead" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                                </td>
                            </tr>
                            <!-- next section starts -->
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" height="34">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td height="34" class="clssubhead">
                                                &nbsp;Payment Plans
                                            </td>
                                            <td>
                                                Owes:<asp:Label ID="lblOwe" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                Plan:<asp:Label ID="lblplan" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                Difference:<asp:Label ID="lbldifference" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                Follow-Up Date:
                                                <asp:Label ID="lblFollowupdate" runat="server"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:LinkButton ID="lnkb_Viol" runat="server" BackColor="#EEC75E">Schedule Payment</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!--cutted-->
                            <tr>
                                <td colspan="2" width="100%">
                                    <table id="tblschedulepay" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:DataGrid ID="dg_Schedule" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                    CssClass="clsLeftPaddingTable" PageSize="2" Width="100%" OnItemCommand="dg_Schedule_ItemCommand"
                                                    OnItemDataBound="dg_Schedule_ItemDataBound">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="AMT ">
                                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lnkb_amount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.amount") %>'>
                                                                </asp:Label>
                                                                <asp:Label ID="lbl_schd_id" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.scheduleid") %>'
                                                                    Visible="False" Width="12px">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Payment Date">
                                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_schd_date" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.paymentdate","{0:MM/dd/yyyy}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Days Left">
                                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_dayleft" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.daysleft") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkb_editschedule" runat="server">Edit</asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/cross.gif" CommandName="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle NextPageText="&amp;gt; Next" PrevPageText="Previous &amp;lt; " />
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!--cutted-->
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="2" style="height: 12px">
                                </td>
                            </tr>
                            <!-- next section starts -->
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" height="34">
                                    &nbsp;General Comments
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
                                    <table id="tblbottomcredit1" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Width="900px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 15px">
                                    <asp:Panel ID="PopupPanel" runat="server" Height="50px" Width="125px" Style="position: absolute;
                                        top: 1070px; left: 198px; display: none">
                                        <table id="tbl_telnos" cellpadding="0" cellspacing="1" style="width: 348px; background-color: White;
                                            border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                            border-bottom: black thin solid;" border="0">
                                            <tr>
                                                <td class="clsLeftPaddingTable" colspan="2" style="height: 34px; background-image: url(../Images/subhead_bg.gif);"
                                                    valign="middle">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td class="clssubhead">
                                                                Card Reader
                                                            </td>
                                                            <td align="right">
                                                                &nbsp;<asp:LinkButton ID="lnkbtnpclose" runat="server" OnClientClick='return closereaderpopup("1");'>X</asp:LinkButton>&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                    <strong>Card Holder Name:</strong>
                                                </td>
                                                <td style="height: 20px" class="clsLeftPaddingTable">
                                                    <asp:Label ID="lbl_cardholdername" runat="server" Width="151px" CssClass="clsInputadministration"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                    <strong>Card Number: </strong>
                                                </td>
                                                <td style="height: 24px" class="clsLeftPaddingTable">
                                                    <asp:Label ID="lbl_cardno" runat="server" Width="151px" CssClass="clsInputadministration"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 28px;" class="clsLeftPaddingTable">
                                                    <strong>Expiration Date:</strong>
                                                </td>
                                                <td class="clsLeftPaddingTable" style="width: 220px; height: 28px">
                                                    <strong>Month</strong>
                                                    <asp:Label ID="lbl_expmonth" runat="server" Width="47px" CssClass="clsInputadministration"></asp:Label>&nbsp;
                                                    <strong>Year </strong>
                                                    <asp:Label ID="txt_expyear" runat="server" Width="47px" CssClass="clsInputadministration"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                </td>
                                                <td id="td_txtcreditcard" runat="server" style="width: 200px; height: 36px; display: block"
                                                    class="clsLeftPaddingTable" valign="bottom">
                                                    <asp:Button ID="btn_popupCancel" runat="server" Text="Cancel" CssClass="clsbutton"
                                                        Width="54px" />
                                                    <asp:Button ID="btn_popupok" runat="server" Text="Ok" CssClass="clsbutton" Width="54px" /><asp:TextBox
                                                        ID="txt_creditcard" runat="server" Height="1px" Width="1px" CssClass="clsInputadministration"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                </td>
                                                <td class="clsLeftPaddingTable" style="width: 200px; height: 10px">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="pnl_VoidComments" runat="server" Style="position: absolute; top: 1070px;
                                        left: 198px; display: none">
                                        <table id="tbl_VoidComments" cellpadding="0" cellspacing="0" style="background-color: White;
                                            border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                            border-bottom: black thin solid;" border="0">
                                            <tr>
                                                <td class="clsLeftPaddingTable" style="height: 34px; background-image: url(../Images/subhead_bg.gif);"
                                                    valign="middle">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td class="clssubhead">
                                                                Payment Void Comments
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" style="width: 600px">
                                                    <cc2:WCtl_Comments ID="WCC_VoidGeneralComments" runat="server" Width="575px" Height="186px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" align="center">
                                                    <asp:Button ID="btn_VoidUpdate" runat="server" Text="Update" CssClass="clsbutton"
                                                        OnClick="btn_VoidUpdate_Click" OnClientClick="return CloseVoidCommentsPopup();" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPdadingTable" style="height: 8px">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="Pnl_ContinuanceComments" runat="server" Style="position: absolute;
                                        top: 1070px; left: 198px; display: none">
                                        <table id="Table2" cellpadding="0" cellspacing="0" style="background-color: White;
                                            border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                            border-bottom: black thin solid;" border="0">
                                            <tr>
                                                <td class="clsLeftPaddingTable" style="height: 34px; background-image: url(../Images/subhead_bg.gif);"
                                                    valign="middle">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td class="clssubhead">
                                                                Continuance Comments
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable">
                                                    <cc2:WCtl_Comments ID="WCC_ContinuanceComments" runat="server" Width="500px" Height="186px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" align="center">
                                                    <asp:Button ID="btn_ContinuanceComments" runat="server" Text="Update" CssClass="clsbutton"
                                                        OnClick="btnProcess_Click" OnClientClick="return CloseContinuanceCommentPopup();" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPdadingTable" style="height: 8px">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="15">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 20px">
                                    <asp:Button ID="btnUpdate" OnClientClick="return checkGComments();" runat="server"
                                        CssClass="clsbutton" Text="Update" OnClick="btnUpdate_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn_coversheet" runat="server" CssClass="clsbutton" Text="Cover Sheet"
                                                    Visible="False" OnClick="btn_coversheet_Click1"></asp:Button><asp:Button ID="btn_Receipt"
                                                        runat="server" CssClass="clsbutton" Text="Contract/Receipt" Visible="False">
                                                </asp:Button><asp:Button ID="btn_emailrep" runat="server" CssClass="clsbutton" Text="Email Receipt"
                                                    Visible="False"></asp:Button><asp:Button ID="btn_bond" runat="server" CssClass="clsbutton"
                                                        Text="Bonds" Visible="False"></asp:Button><asp:Button ID="btn_letterofRep" runat="server"
                                                            CssClass="clsbutton" Text="Letter of Rep" Visible="False" OnClick="btn_letterofRep_Click" /><asp:Button
                                                                ID="btn_TrialSuspensionDL" runat="server" CssClass="clsbutton" Text="Trial DL Suspension"
                                                                Visible="False" /><asp:Button ID="btn_TrialOccupationDL" runat="server" CssClass="clsbutton"
                                                                    Text="Trial Occupation DL" Visible="False" /><asp:Button ID="btn_contuance" runat="server"
                                                                        CssClass="clsbutton" Text="Continuance" Visible="False">
                                                </asp:Button>
                                                <asp:Button ID="btnAGContract" runat="server" CssClass="clsbutton" Text="AG Contract"
                                                    Visible="False"></asp:Button>
                                                <asp:Button ID="btnALRContract" runat="server" CssClass="clsbutton" Text="ALR Contract"
                                                    Visible="False"></asp:Button>
                                                <asp:Button ID="btnDWIContract" runat="server" CssClass="clsbutton" Text="DWI Contract"
                                                    Visible="False"></asp:Button>
                                                <asp:Button ID="btnPreTrialContract" runat="server" CssClass="clsbutton" Text="Pre-Trial  Contract"
                                                    Visible="False"></asp:Button>
                                                <asp:Button ID="btnTrialContract" runat="server" CssClass="clsbutton" Text="Trial Contract"
                                                    Visible="False" OnClientClick=""></asp:Button>
                                                <asp:Button ID="btnDLSuspensionContract" runat="server" CssClass="clsbutton" Text="DL Suspension Contract"
                                                    Visible="False"></asp:Button>
                                                <asp:Button ID="btnOccupLicContract" runat="server" CssClass="clsbutton" Text="OccupLic Contract"
                                                    Visible="False"></asp:Button>
                                                <asp:Button ID="btn_trialLetter" CssClass="clsbutton" Text="Trial Letter" Visible="True"
                                                    runat="server"></asp:Button>
                                                <asp:Button ID="btn_EmailTrial" runat="server" CssClass="clsbutton" Text="EmailTrial"
                                                    Visible="False" />
                                                <asp:Button ID="btn_Limine" runat="server" CssClass="clsbutton" Text="Motion in Limine"
                                                    Visible="False" Width="108px" />
                                                <asp:Button ID="btn_Discovery" runat="server" CssClass="clsbutton" Text="Motion for Discovery"
                                                    Visible="False" Width="127px" />
                                                <%--Waqas 5864 07/01/2009 --%>
                                                <asp:Button ID="btn_ALRCheckRequest" OnClientClick="return CheckALRCheckReq();" runat="server"
                                                    CssClass="clsbutton" Text="ALR Check Request" Visible="False" Width="127px" />
                                                <asp:Button ID="btn_ALRContinuance" OnClientClick="return CheckALRContinuance();"
                                                    runat="server" CssClass="clsbutton" Text="ALR Continuance" Visible="False" Width="127px" />
                                                <asp:Button ID="btn_ALRHearingRequest" OnClientClick="return CheckALRHearingRequest();"
                                                    runat="server" CssClass="clsbutton" Text="ALR Hearing Request" Visible="False"
                                                    Width="140px" />
                                                <asp:Button ID="btn_ALRMandatoryContinuance" OnClientClick="return CheckALRMandatoryContinuance();"
                                                    runat="server" CssClass="clsbutton" Text="ALR Mandatory Continuance" Visible="False"
                                                    Width="190px" />
                                                <asp:Button ID="btn_ALRMotionToRequestBTOBTS" OnClientClick="return CheckALRMotionToRequestBTOBTS();"
                                                    runat="server" CssClass="clsbutton" Text="Motion To Request (BTO-BTS)" Visible="False"
                                                    Width="190px" />
                                                <asp:Button ID="btn_ALRSubpoena" OnClientClick="return CheckALRSubpoena();" runat="server"
                                                    CssClass="clsbutton" Text="ALR Subpoena" Visible="False" Width="115px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td background="../../images/separator_repeat.gif" colspan="2" style="height: 11px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="visibility: hidden; width: 780px; height: 40px;" valign="top" align="right"
                                    colspan="2">
                                    <asp:TextBox ID="txtTimeStemp" runat="server" Width="89px"></asp:TextBox><asp:Label
                                        ID="lbl_schcount" runat="server"></asp:Label><asp:Label ID="lbl_activeflag" runat="server">active</asp:Label><asp:Label
                                            ID="lbl_dlstate" runat="server">dlstate</asp:Label><asp:Label ID="lbl_yds" runat="server">yds</asp:Label><asp:Label
                                                ID="lbl_trialbuttonflag" runat="server">trialbtnf</asp:Label><asp:Label ID="lbl_addresscheck"
                                                    runat="server"></asp:Label><asp:Label ID="lbl_contactcheck" runat="server">concheck</asp:Label>&nbsp;<asp:TextBox
                                                        ID="txt_schedulesum" runat="server" Width="3px"></asp:TextBox>&nbsp;<asp:Label ID="lbl_courtid"
                                                            runat="server"></asp:Label><asp:Label ID="lbl_scheduleid" runat="server"></asp:Label><asp:Label
                                                                ID="lbl_pretrialflag" runat="server">pretrialflag</asp:Label><asp:Label ID="lbl_lockflag"
                                                                    runat="server"></asp:Label>
                                    <asp:HiddenField ID="hf_transactionmode" runat="server" />
                                    <asp:Label ID="lbl_WalkFlag" runat="server"></asp:Label>
                                    <asp:Label ID="lblBondConfirmed" runat="server"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lbl_OscarActiveFlag" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_OscarCourtID" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_ToProcess" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_court" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsSplit" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsLORAlreadyInBatchPrint" runat="server"></asp:Label>
                                    <asp:HiddenField ID="hf_continuancecount" runat="server" />
                                    <asp:Label ID="lblFTACase" runat="server"></asp:Label>
                                    <asp:Label ID="lblTrailNotFlag" runat="server"></asp:Label>
                                    <asp:HiddenField ID="HFOwes" runat="server" />
                                    <asp:HiddenField ID="HFFee" runat="server" />
                                    <asp:HiddenField ID="hf_owesdiff" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_nocheck" runat="server" />
                                    &nbsp;
                                    <asp:HiddenField ID="hf_court" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_nos" runat="server" />
                                    <asp:HiddenField ID="HFNoTrialLetter" runat="server" />
                                    <asp:HiddenField ID="HF_Courtid" runat="server" />
                                    <asp:HiddenField ID="hf_letterofrap" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORSubpoena" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORMOD" runat="server" />
                                    <asp:HiddenField ID="hf_HasSameCourtdate" runat="server" />
                                    <asp:HiddenField ID="hf_HasMoreSpeedingviol" runat="server" />
                                    <asp:HiddenField ID="hf_AllowChecks" runat="server" />
                                    <asp:HiddenField ID="hf_InvalidCauseNo" runat="server" />
                                    <asp:HiddenField ID="hf_InvNum" runat="server" />
                                    <asp:HiddenField ID="hf_PayMethod" runat="server" />
                                    <asp:HiddenField ID="hf_AllowBondLetter" runat="server" />
                                    <asp:HiddenField ID="hf_AssociatedCaseType" runat="server" />
                                    <asp:HiddenField ID="hf_CourtDateMain" runat="server" />
                                    <asp:HiddenField ID="hf_checkALRnullArrestdate" runat="server" />
                                    <asp:HiddenField ID="hf_checkALRArrestdateRange" runat="server" />
                                    <asp:HiddenField ID="hf_IsnotDisposed" runat="server" />
                                    <asp:HiddenField ID="hfLORMethodCount" runat="server" />
                                    <asp:HiddenField ID="hf_ALRAttorneyNameAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALRAttorneyBarCardAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALRIntoxilizerAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALRIntoxilizerAlert2" runat="server" />
                                    <asp:HiddenField ID="hf_ALRArrestingAgencyAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROfficerNameAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROfficerCityAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROfficerZipAlert" runat="server" />
                                    <asp:HiddenField ID="hf_IsNoCourtAssigned" runat="server" />
                                    <asp:HiddenField ID="hf_CheckClientonSameDate" runat="server" />
                                    <asp:HiddenField ID="hf_CheckPaymentType" runat="server" />
                                    <asp:HiddenField ID="hf_isCriminalCase" runat="server" />
                                    <asp:HiddenField ID="hf_casetype" runat="server" />
                                    <%--Waqas 6342 08/13/2009 hidden fields added for subpoena--%>
                                    <asp:HiddenField ID="hf_ALROfficerAddressAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROfficerStateAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROBSOfficerNameAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROBSOfficerAddressAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROBSOfficerCityAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROBSOfficerStateAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALROBSOfficerZipAlert" runat="server" />
                                    <asp:HiddenField ID="hf_ALRProcess" runat="server" />
                                    <asp:HiddenField ID="hffaxFlag" runat="server" />
                                    <asp:HiddenField ID="hfIsHCJP" runat="server" />
                                    <asp:HiddenField ID="hf_CheckBondReportForSecUser" Value="0" runat="server" />
                                    <asp:HiddenField ID="hf_CoveringFirm" runat="server" />
                                    <%--<asp:HiddenField ID="hf_isInvalidPMCTicket" runat="server" />--%>
                                    <%--<asp:HiddenField ID="hf_IsInvalidPMCCausenumber" runat="server" />--%>
                                    <asp:HiddenField ID="hf_NoDL" runat="server" />
                                    <asp:HiddenField ID="hf_IsALRHearingRequired" runat="server" />
                                    <asp:HiddenField ID="hf_ActiveInactiveCourt" runat="server" />
                                    <asp:HiddenField ID="hf_IsLORDateInPastOrFull" runat="server" />
                                    <asp:HiddenField ID="hf_PrventingPMCIntimationCallonPageLoad" Value="0" runat="server" />
                                    <%--Zeeshan Haider 10699 04/18/2013 CaseCourt Date Validation with LOR Dates PMC--%>
                                    <asp:HiddenField ID="hf_IsLORDateInFuture" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 48px" colspan="2">
                                    <uc1:Footer ID="Footer1" runat="server" />
                                </td>
                            </tr>

                            <script language="javascript">                                document.getElementById("hidebtn").style.display = "none";	 
                            </script>

                        </tbody>
                    </table>
                    <ajaxToolkit:ModalPopupExtender ID="mp_msg" runat="server" CancelControlID="btncancel"
                        TargetControlID="button12" PopupControlID="pnl_msg" BackgroundCssClass="modalbackground"
                        HideDropDownList="false">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Button ID="button12" runat="server" Text="button" Style="display: none;" />
                    <asp:Panel ID="pnl_msg" runat="server">
                        <table id="tblsub" width="300" class="clsLeftPaddingTable" height="60" align="center"
                            cellpadding="0" cellspacing="1" bgcolor="white" border="0" style="border-top: black thin solid;
                            border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid">
                            <tr>
                                <td>
                                    <asp:Label ID="lblcourterror" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td background="../images/subhead_bg.gif" class="clssubhead" align="center" style="height: 34px">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="clssubhead">
                                                <strong>Payment Information</strong>
                                            </td>
                                            <td align="right">
                                                <%-- <asp:LinkButton ID="linkbutton1" OnClientClick="return closeModalPopup('mp_msg')"
                                                    runat="server">x</asp:LinkButton>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="tr_Process" runat="server">
                                <td align="left" class="clssubhead">
                                    Payment processed successfully, Press Next button to get the updated Bond amount
                                    from Court website.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr id="tr_pleasewait" runat="server">
                                <td align="center" class="clssubhead">
                                    <img alt="" src="../images/plzwait.gif" />
                                    &nbsp;
                                    <asp:Label ID="lbl_showwaitmessage" runat="server"></asp:Label>
                                    Please wait while we check bond amount from court website...
                                </td>
                            </tr>
                            <tr id="trPaymentMsg" runat="server">
                                <td align="left">
                                    <asp:GridView Width="300px" ID="gv_Payments" CssClass="clsLeftPaddingTable" runat="server"
                                        AutoGenerateColumns="false" BorderColor="Black">
                                        <Columns>
                                            <asp:TemplateField HeaderText="S No">
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" BorderColor="Black"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Sno" runat="server" Text='<%# Eval("SNo") %>' CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" BorderColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cause Number">
                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" BorderColor="Black"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="ticktno" runat="server" Text='<%# Eval("TicketNumber") %>' CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" BorderColor="Black" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <img id="img_bond" runat="server" src="~/Images/btnClose.gif" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" BorderColor="Black" />
                                                <HeaderStyle BorderColor="Black" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="tr_nextbutton" runat="server">
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="btnpaymentnext" runat="server" Text="Next" OnClientClick="return Display();"
                                                    CssClass="clsbutton"></asp:Button>
                                                <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="clsbutton" OnClientClick="return closeModalPopup('mp_msg')">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="trgv_Payments" runat="server">
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="btnDone" runat="server" Text="Done" CssClass="clsbutton" OnClientClick="return closeModalPopup('mp_msg')">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <%--Fahad 6054 07/21/2009 Modal PopUp Added--%>
        <ajaxToolkit:ModalPopupExtender ID="Modal_Message" runat="server" TargetControlID="btnok"
            PopupControlID="Panel1" BackgroundCssClass="modalbackground" CancelControlID="btnNo">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Button ID="btnok" runat="server" Text="Yes" Style="display: none" />
        <asp:Panel ID="Panel1" runat="server" Style="display: none;">
            <table id="tablepopup" width="400px" height="150px" style="border-top: black thin solid;
                border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid"
                cellpadding="0" cellspacing="0">
                <tr>
                    <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                        style="width: 353px">
                        <table width="400px" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="clssubhead" align="left" style="height: 16px">
                                        &nbsp;Confirmation Box
                                    </td>
                                    <td align="right" style="height: 16px">
                                        &nbsp;<asp:LinkButton ID="lnkbtncancelpopup" OnClientClick="return closeModalPopup('Modal_Message')"
                                            runat="server">X</asp:LinkButton>&nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr bgcolor="#eff4fb">
                    <td align="center">
                        <table width="400px" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblmessageshow" runat="server" BackColor="#eff4fb" ForeColor="#3366CC"
                                        Text="The firm currently does not have clients at courthouse on the entered date and therefore no attorney is assigned to that courthouse.  Please get authorization to process this case.  Click Yes to Continue or No to cancel this transaction"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <br />
                                    <asp:Button ID="btnYes" runat="server" OnClientClick="return VerifyModalPopUp()"
                                        OnClick="btnYes_Click" Text="Yes" CssClass="clsbutton" Height="22px" Width="80px" />&nbsp;
                                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="clsbutton" Height="22px"
                                        Width="80px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button2"
                    PopupControlID="panel2" BackgroundCssClass="modalBackground" HideDropDownList="false"
                    CancelControlID="img1">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="panel2" runat="server" Width="483px" Height="400px">
                    <table id="Table1" bgcolor="white" border="0" style="border-top: black thin solid;
                        border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                        height: 400px;">
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                <table>
                                    <tr>
                                        <td style="width: 500px">
                                            <span class="clssubhead">Compose Fax</span>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="img1" runat="server" ImageUrl="../Images/remove2.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Fc:Faxcontrol ID="Faxcontrol1" runat="server" Recall="true" />
                                <asp:Button ID="Button2" runat="server" Text="Cancel" Style="display: none;" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Page language="c#" EnableEventValidation="false" AutoEventWireup="false" Inherits="lntechNew.ClientInfo.UpdateViolation" Codebehind="UpdateViolation.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UpdateViolation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<base target="_self" />
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		
		function ToggleStatus()
		{
			var ddl =   document.getElementById("ddlStatus").style;
			var lbl =	document.getElementById("lblStatus").style;
				
			if (document.getElementById("txtStatus").value==0)
			{
					ddl.display = 'block'; 
					lbl.display = 'none'; 
					document.getElementById("txtStatus").value = 1;
			}
			else 
			{
					ddl.display = 'none'; 
					lbl.display = 'block'; 	
					document.getElementById("txtStatus").value = 0;
			}
		}
		
		function DisplayToggle()
		{
			var ddl =   document.getElementById("ddl").style;
			var lbl =	document.getElementById("lbl").style;
				
			if (document.getElementById("TxtValue").value==0)
			{
					ddl.display = 'block'; 
					lbl.display = 'none'; 
					document.getElementById("TxtValue").value = 1;
			}
			else 
			{
					ddl.display = 'none'; 
					lbl.display = 'block'; 	
					document.getElementById("TxtValue").value = 0;
			}
		}
				
		function checkAdmin()
		{  
			if (lblAdmin.innerText=="0")
			{
				document.getElementById("PricePlan").style.visibility='hidden';
				document.getElementById("PricePlan1").style.visibility='hidden';					
			}
		/*
			if(lblDispo.innerText=="1")
			{
				document.UpdateViolation.ddl_Status.style.display='block';
				lbl_Status.style.display='none';							
			}
			else if (document.UpdateViolation.ddl_Status.value==80)
			{
					document.UpdateViolation.ddl_Status.style.display='none';
					lbl_Status.style.display='block';							
			}
			else
			{
				document.UpdateViolation.ddl_Status.style.display='block';
				lbl_Status.style.display='none';							
			}		
         */			
		}
		
		function select_deselectAll(chkVal, idVal) 
		{ 
			var frm = document.forms[0];
			// Loop through all elements
			for (i=0; i<frm.length; i++) 
			{
				if(frm.elements[i].name.substring(0,19)=='dgViolationInfo$ctl' )
				{
					// Look for our Header Template's Checkbox
					if (idVal.indexOf ('CheckAll') != -1) 
					{
						// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
						if(chkVal == true) 
						{
							
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = true;
							}
						} 
						else 
						{
							if(frm.elements[i].disabled==true)
							{
							}
							else
							{
								frm.elements[i].checked = false;
							}
						}
					}         
				}
			}
		} 
 
		function MakeVerfiedDateSameAsAutodate() 
		{
			document.UpdateViolation.ddl_Status.value =document.getElementById("lbl_AutoStatusID").innerText;
			document.UpdateViolation.txt_Month.value =document.getElementById("lbl_AutoCourtDate").innerText.substring(0,2);
			document.UpdateViolation.txt_Day.value =  document.getElementById("lbl_AutoCourtDate").innerText.substring(3,5);
			document.UpdateViolation.txt_Year.value = document.getElementById("lbl_AutoCourtDate").innerText.substring(6,10);
			document.UpdateViolation.ddl_Time.value = document.getElementById("lbl_AutoTime").innerText;
			document.UpdateViolation.txt_CourtNo.value =document.getElementById("lbl_AutoNo").innerText;
		}
		
		//
		function submitForm() 
		{ 
		   
			//Check For Ticket No
			var casenumber = document.UpdateViolation.txt_TicketNo.value;
			var refcasenumber = casenumber;
			
			if (refcasenumber == "" ) 
			{
				alert("Invalid Ticket Number.");
				document.UpdateViolation.txt_TicketNo.focus();
				return false;
			}
			if (refcasenumber.length<5)
			{
				alert("Ticket Number must be greater then four characters.");
				document.UpdateViolation.txt_TicketNo.focus();
				return false;
			}
			if (casenumber.indexOf(" ") != -1 ) 
			{
				alert("Please make sure that there are no spaces in the ticketnumber input box"); 
				document.UpdateViolation.txt_TicketNo.focus();
				return false;
			}
			//Check For SeqNo
			//var sequnumber = document.UpdateViolation.txt_SequenceNo.value;
			//var seqnumber = sequnumber;
			/*if (UpdateViolation.ddl_ViolationDescription.value!=50)
			{
				if (sequnumber.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the SequenceNumber input box");
					document.UpdateViolation.txt_SequenceNo.focus();
					return false;
				}
			}*/
			/*if (isNaN(sequnumber)) 
			{
				alert("Please enter only number in Sequence No.");
				document.UpdateViolation.txt_SequenceNo.focus();
				return false;
			}*/
			
			
			var samedateflag = false;
			var courtid;
			if(UpdateViolation.txtinactive.value!="1")
			{
				courtid = UpdateViolation.ddl_CourtLocation.value;
			}
			else
			{
				courtid= lblinactiveCourtID.innerText;
			}
			var courtnum = UpdateViolation.txt_CourtNo.value ;
			var datetype = UpdateViolation.ddl_Status.value;
			selectedindex = UpdateViolation.ddl_Status.selectedIndex;
			var strdatetype =UpdateViolation.ddl_Status.value;
			varhours = UpdateViolation.ddl_Time.value;
			var strdate = UpdateViolation.txt_Month.value + "/" + UpdateViolation.txt_Day.value + "/2" + UpdateViolation.txt_Year.value;
			var strdatewithtime = strdate + " " + varhours;
			var courtstatusauto = document.getElementById("lbl_AutoStatusID").innerText;
			var courtnumauto =document.getElementById("lbl_AutoNo").innerText;
			var courtdateauto = document.getElementById("lbl_AutoCourtDate").innerText + " " + document.getElementById("lbl_AutoTime").innerText;
			//Violation Description Check
			
			if(document.getElementById("TxtValue").value == 1)
			{
			   
			    var ddltext=document.UpdateViolation.ddl_ViolationDescription.options[document.UpdateViolation.ddl_ViolationDescription.selectedIndex].text;
				if (UpdateViolation.ddl_ViolationDescription.value==0)
				{
					alert("Please Select ViolationDescription.");
					UpdateViolation.ddl_ViolationDescription.focus();
					return false;
				}
				if(ddltext.substring(0,3)== "---")
				{ 
				    alert("Please Select ViolationDescription.");
					UpdateViolation.ddl_ViolationDescription.focus();
					return false;
				}
			}
			
			if(document.getElementById("TxtValue").value == 0)
			{   
			    var violid = document.getElementById('lbl_ViolationID');
			    var ddlViol = document.getElementById('ddl');
			    var lblViol = document.getElementById('lbl');
				if (violid.innerText == '0' && lblViol.style.display == 'block' )
				{
					alert("Please Select Violation Description.");
					lblViol.style.display = 'none';
					ddlViol.style.display = 'block';
					UpdateViolation.TxtValue.value = '1';
					UpdateViolation.ddl_ViolationDescription.focus();
					return false;
				}
			}						
			
			//Fine AMount Check
			var fineamount = document.UpdateViolation.txt_FineAmount.value;
			if (fineamount=="") 
			{
				alert("Please enter Fine Amount."); 
				UpdateViolation.txt_FineAmount.focus();
				return false;
			}
			if (fineamount.indexOf(" ") != -1) 
			{
				alert("Please make sure that there are no spaces in the Fine Amount input box"); 
				UpdateViolation.txt_FineAmount.focus();
				return false;
			}
			if(isNaN(fineamount))
			{
				alert("Invalid Fine Amount.");
				UpdateViolation.txt_FineAmount.focus();
				return false;
			}
			//Bond AMount Check
			if(isNaN(UpdateViolation.txt_BondAmount.value))
			{
				alert("Invalid Bond Amount.");
				UpdateViolation.txt_BondAmount.focus();
				return false;
			}
			var ValidChars = "-";
			var Char;
			var sText=UpdateViolation.txt_FineAmount.value;
			
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid fine amount");
					UpdateViolation.txt_FineAmount.focus();
					return false;
				}
			}
			var sText=UpdateViolation.txt_BondAmount.value;
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid bond amount");
					UpdateViolation.txt_BondAmount.focus();
					return false;
				}
			}
			//Status Check
/*			if (UpdateViolation.ddl_Status.value==0)
			{
				alert("Please Select Case Status.");
				UpdateViolation.ddl_Status.focus();
				return false;
			}
*/
            if (UpdateViolation.txtStatus.value == 1)
            {
                if (UpdateViolation.ddl_Status.value==0)
			    {
				alert("Please Select Case Status.");
				UpdateViolation.ddl_Status.focus();
				return false;
			    }
            }
            else
            {   
                var statusid = document.getElementById("lblStatusId").innerText;
    			var ddl =   document.getElementById("ddlStatus").style;
	    		var lbl =	document.getElementById("lblStatus").style;
                
                if ( statusid  == '0')
			    {
				alert("Please Select Case Status.");
				ddl.display = 'block';
				lbl.display = 'none';
				UpdateViolation.txtStatus.value = '1';
				UpdateViolation.ddl_Status.focus();
				return false;
			    }
            
            }


			
			//Check For Court Date
			var bflag=0;
			if(document.UpdateViolation.chkb_Bond.checked)
			{
				bflag = 1;
			}
			else
			{
				bflag = 0;
			}
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
			
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					UpdateViolation.txt_Month.focus();
					return false;			
				}
			}
			
			
			if (DateDiff(strdatewithtime, courtdateauto) == 0) 
			{
				samedateflag  = true;
			}
			
			var doyou;
			if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
			{
				if (courtstatusauto == strdatetype) 
				{
					if ((samedateflag == false) || (parseInt(courtnumauto) != parseInt(courtnum))) 
					{
						doyou = confirm("The verified court status and auto load courts status are the same but have conflicting setting information.  Would you like to proceed? (OK = Yes   Cancel = No)");
						if (doyou == true) 
						{
						}
						else
						{
							return false;
						}
					}
				}

				if ((refcasenumber.substring(0,1) == "0" ) || (refcasenumber.substring(0,1) == "M") || (refcasenumber.substring(0,1) == "A")) 
				{
					/*if (seqnumber == "" ) 
					{
						if (UpdateViolation.ddl_ViolationDescription.value!=50)
						{
							alert("Invalid Sequence Number.");
							UpdateViolation.txt_SequenceNo.focus();
							return false;
						}
					}*/
/*
					if (refcasenumber.length != 9) 
					{
						alert("It requires 9 characters If Case Number starts with '0' or 'M' or 'A'");
						UpdateViolation.txt_TicketNo.focus();
						return false;
					}
*/	
				}
/*
				if (refcasenumber.substring(0,1) == "F" ) 
				{
					if (refcasenumber.length != 8) 
					{
						alert("It requires 8 characters if Case Number starts with 'F'");
						UpdateViolation.txt_TicketNo.focus();
						return false;
					}
			
					if ((seqnumber != "" ) && (seqnumber != 0)) 
					{
						alert("F Tickets don't have any sequence number.");
						UpdateViolation.txt_SequenceNo.focus();
						return false;
					}
				}
				
				
				
				
				
*/
		if(courtid == "3001")
		{ 
			var courtno = courtnum * 1;
			if ((courtno < 0) || (courtno > 17)||(courtno > 12 && courtno < 15))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
		}
		
				if((courtid == "3002")||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}

				if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00AM") && (varhours != "9:00AM") && (varhours != "10:30AM") && (varhours != "10:00AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							UpdateViolation.ddl_Time.focus();
							return false;
						}
						
						
						
						var bodflag=0;
						if(document.UpdateViolation.chkb_Bond.checked)
						{
							bodflag = 1;
						}
						else
						{
							bodflag = 0;
						}
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
							if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
							{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								UpdateViolation.txt_CourtNo.focus();
								return false;
							}
						}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						UpdateViolation.ddl_Status.focus();
						return false;
					}
				}
				
				var bdflag=0;
				if(document.UpdateViolation.chkb_Bond.checked)
				{
					bdflag = 1;
				}
				else
				{
					bdflag = 0;
				}
				if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (courtid != "3003") 
					{
						if ((courtnum == "18") ) 
						{
							alert("This court number can only be set for Dairy Ashford Court");
							UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					if (courtid == "3003") 
					{
						if ((courtnum != "18") ) 
						{
							alert("Court number 18 can only be set for Dairy Ashford Court");
							UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa");
							UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
				}
			}	
			
			
			//var strdate = UpdateViolation.ddl_Date_Month.value + "/" + UpdateViolation.ddl_Date_Day.value + "/" + UpdateViolation.ddl_Date_Year.value; 
			var strdate = UpdateViolation.txt_Month.value + "/" + UpdateViolation.txt_Day.value + "/20" + UpdateViolation.txt_Year.value; 
			var strtime = UpdateViolation.ddl_Time.value;
			var bbflags=0;
			if(document.UpdateViolation.chkb_Bond.checked)
			{
				bbflags = 1;
			}
			else
			{
				bbflags = 0;
			}
			if (bbflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
				if (strtime == "--") 
				{
					alert("Invalid Court Time. ");
					document.UpdateViolation.ddl_Time.focus();
					return false;
				}
				if (strtime == "11") 
				{
					if (datetype != 80 )
					{
						selectedindex1 = document.UpdateViolation.ddl_ViolationDescription.selectedIndex;
						var strviolationnew = trimAll(document.UpdateViolation.ddl_ViolationDescription.options[selectedindex1].text);
					
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Time. ");
							UpdateViolation.ddl_Time.focus();
							return false;
						}
					}
				}
			}
		
			//Check For Court No
			var bndflag=0;
			var datetype1 = UpdateViolation.ddl_Status.value;
			if(document.UpdateViolation.chkb_Bond.checked)
			{
				bndflag = 1;
			}
			else
			{
				bndflag = 0;
			}
			if (bndflag ==0 && datetype1 != 104 && datetype1 != 105 && datetype1 != 135)
			{
				var courtnumber = document.UpdateViolation.txt_CourtNo.value;
				if (courtnumber.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the CourtNumber input box"); 
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
				if (courtnumber=="") 
				{
					alert("Please Enter Court Number.");
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
/*				if (courtnumber=="0") 
				{
					alert("Invalid Court Number.");
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
*/				
				var ValidChars = "0123456789";
				var Char;
				for (j = 0; j < courtnumber.length; j++) 
				{ 
					Char = courtnumber.charAt(j); 
					if (ValidChars.indexOf(Char) == -1) 
					{
						alert("Invalid Court Number");
						document.UpdateViolation.txt_CourtNo.focus();
						return false;
					}
				}
			}
			
			//Check for Court Location
			if(UpdateViolation.txtinactive.value!="1")
			{
				if(UpdateViolation.ddl_CourtLocation.value==0)
				{	
					alert("Please Select Court Location.");
					document.UpdateViolation.ddl_CourtLocation.focus();
					return false;
				}
			}
			if (strdate == "0/--/2005" )   
			{
				var bflags=0;
				if(document.UpdateViolation.chkb_Bond.checked)
				{
					bflags = 1;
				}
				else
				{
					bflags = 0;
				}
				if (bflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (datetype != 80) 
					{
						selectedindex1 = document.UpdateViolation.ddl_ViolationDescription.selectedIndex;
						var strviolationnew = trimAll(document.UpdateViolation.ddl_ViolationDescription.options[selectedindex1].text);		
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Date. ");
							UpdateViolation.txt_Month.focus();
							return false;
						}
					}
				}
			}
			else 
			{
				//alert(launchCheck(strdate))
				var fticketsflag1 = UpdateViolation.txt_FTicketCount.value;
				//var bondflag = UpdateViolation.txt_BondFlag.value;
				var bondflag = 0;
				if(document.UpdateViolation.chkb_Bond.checked)
				{
					bondflag = 1;
				}
				else
				{
					bondflag = 0;
				}
				
				if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
				{
					if (bondflag == 0) 
					{
						if (datetype != 80 && datetype != 104 && datetype != 105 && datetype != 135)  // Not Disposed
						{
							if (launchCheck(strdate) == false )  // if date is in the past
							{
								alert("Court Date has already passed. Please specify a future Date. ");
								UpdateViolation.txt_Month.focus();
								return false;
							}
						}
					//}
						//a;
						var bondflagT = UpdateViolation.txt_BondFlag.value;
/*						if(bondflagT == 0)
						{
							if ( fticketsflag1 == 0 & UpdateViolation.txt_IsNew.value=="0" & datetype != 135 & launchCheck(strdate) == false ) 
							{
								alert("Please Enter at least one Fticket to process this client. ");
								return false;
							}
						}
*/						
					}	
				}
				if (i != 3 ) 
				{
					var violationnumber;
					var fticketflag = false;
					if (launchCheck(strdate) == false ) 
					{
						//alert(strdate)
						for (var i=0; i<=UpdateViolation.ddl_ViolationDescription.length-1;i++) 
						{
							violationnumber = UpdateViolation.ddl_ViolationDescription[i].value;
							if ((violationnumber == "50") || (violationnumber == "448") || (violationnumber == "2821") || (violationnumber == "2822") || (violationnumber == "2824") || (violationnumber == "4337") || (violationnumber == "4999") || (violationnumber == "5103")) 
							{
								fticketflag = true;
								break;
							}
						}

						if (fticketflag == false)
						{
							if (datetype != 80) 
							{
								//alert("Court Date has already passed. Please specify a different Date or Click Insert Violation link. ");
								//return false;
							}
						}
					}
				}		
			}

			//alert(fticketflag)
			//return;
			
			document.UpdateViolation.submit()
		}

		function trimAll(sString) 
		{
			while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}

			while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		}

		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}

		function launchCheck(strdate) 
		{
			var today = new Date(); // today
			var date = new Date(strdate); // your launch date
			//alert(date.getTime())
			//alert(today.getTime())
			var oneday = 1000*60*60*24
			//alert(Math.ceil(today.getTime()-date.getTime())/oneday)
			if (Math.ceil(today.getTime()-date.getTime())/oneday >= 1 ) 
			//if ( date < today )
			{
				return false;
			}
 
			//if (today.getTime()  > date.getTime()) { // on or after launch date
				//return false;
			//}
			return true;
		}
		//
		</script>
	</HEAD>
	<body dir="ltr" MS_POSITIONING="GridLayout">
		<form id="UpdateViolation" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<TR>
						<TD style="WIDTH: 783px; height: 16px;"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 783px">
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="780" border="0">
								<TR>
									<TD><IMG height="17" src="../images/head_icon.gif" width="30">&nbsp;<STRONG>Violation 
											Update Screen </STRONG>
									</TD>
								</TR>
								<TR>
									<TD width="780" background="../../images/separator_repeat.gif" height="11"></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="tbl_name" cellSpacing="0" cellPadding="0" width="780" border="0">
											<TR>
												<TD height="20" width="70%">&nbsp;
													<asp:label id="lbl_LastName" runat="server" CssClass="Label" Width="32px"></asp:label>,&nbsp;
													<asp:label id="lbl_FirstName" runat="server" CssClass="Label"></asp:label>(
													<asp:label id="lbl_CaseCount" runat="server"></asp:label>) ,
													<asp:label id="lbl_MidNo" runat="server" ForeColor="#3366CC"></asp:label></TD>
												<TD align="right" height="20">
													<asp:button id="btn_Update1" runat="server" CssClass="clsbutton" Text="Update Ticket(s)"></asp:button></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD width="780" background="../../images/separator_repeat.gif" height="11"></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="tbl_ticket" cellSpacing="0" cellPadding="0" width="780" border="0">
											<TR class="clsLeftPaddingTable">
												<TD class="clsaspcolumnheader" style="WIDTH: 83px" height="20">Cause Number</TD>
												
												<TD class="clsaspcolumnheader" style="WIDTH: 82px" height="20">Ticket Number</TD>
												<TD class="clsaspcolumnheader" style="WIDTH: 273px" height="20">Violation 
													Description</TD>
												<TD class="clsaspcolumnheader" height="20">Fine Amount</TD>
												<TD class="clsaspcolumnheader" height="20">Bond Amount</TD>
												<TD class="clsaspcolumnheader" height="20">Bond</TD>
											</TR>
											<TR class="clsLeftPaddingTable">
												<TD style="WIDTH: 83px" height="20"><asp:textbox id="txt_CauseNo" runat="server" CssClass="clsinputadministration" Width="80px" MaxLength="15"></asp:textbox></TD>
												
												<TD style="WIDTH: 83px" height="20"><asp:textbox id="txt_TicketNo" runat="server" CssClass="clsinputadministration" Width="80px"></asp:textbox></TD>
												<ajax:ajaxpanel id="Ajaxpanel3" runat="server">
													<TD style="WIDTH: 273px" height="20">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="90%">
																	<DIV id="ddl" ms_positioning="FlowLayout">
																		<asp:dropdownlist id="ddl_ViolationDescription" runat="server" CssClass="clsinputcombo"></asp:dropdownlist></DIV>
																	<DIV id="lbl" ms_positioning="FlowLayout">
																		<asp:label id="lbl_ViolationDescription" runat="server" CssClass="label"></asp:label></DIV>
																</TD>
																<TD width="10%"><IMG onclick="DisplayToggle()" alt="" src="../Images/Rightarrow.gif"></TD>
															</TR>
														</TABLE>
													</TD>
												</ajax:ajaxpanel><TD height="20"><asp:textbox id="txt_FineAmount" runat="server" CssClass="clsinputadministration" Width="64px" MaxLength="7"></asp:textbox></TD>
												<TD height="20"><asp:textbox id="txt_BondAmount" runat="server" CssClass="clsinputadministration" Width="60px" MaxLength="7"></asp:textbox></TD>
												<TD height="20"><asp:checkbox id="chkb_Bond" runat="server" CssClass="clslabelnew"></asp:checkbox></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD width="780" background="../../images/separator_repeat.gif" height="11"></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="tbl_status" cellSpacing="1" cellPadding="0" width="780" border="0">
											<TR class="clsLeftPaddingTable">
												
												<TD class="clsaspcolumnheader" style="WIDTH: 78px; height: 20px;">Status</TD>
												<TD class="clsaspcolumnheader" style="WIDTH: 92px; height: 20px;">Court Date</TD>
												<TD class="clsaspcolumnheader" style="width: 75px; height: 20px;">Court Time</TD>
												<TD class="clsaspcolumnheader" style="width: 57px; height: 20px;">Court No</TD>
												<td class="clsaspcolumnheader" style="height: 20px">
                                                    Court Location</td>
												<td class="clsaspcolumnheader" id="PricePlan" style="height: 20px">
                                                    Pricing Plan</td>
											</TR>
											<TR class="clsLeftPaddingTable">
												<!--  1borntowin    -->
												<TD style="WIDTH: 120px" height="20">
												    <table border="0" cellpadding="0" cellspacing="0" width="100%">
												        <tr>
												            <td style="width:90%; height: 32px;">
												                <div id="ddlStatus" ms_positioning="FlowLayout"><asp:dropdownlist id="ddl_Status" runat="server" CssClass="clsinputcombo"></asp:dropdownlist></div>
												                <div id="lblStatus" ms_positioning="FlowLayout"><asp:label id="lbl_Status" runat="server" CssClass="label"></asp:label></div>
												            </td>
												            <td style="width:10%; height: 32px;"><IMG onclick="ToggleStatus()" alt="" src="../Images/Rightarrow.gif"></td>
												            <td style="display:none"><asp:Label ID="lblStatusId" runat="server"></asp:Label></td>
												        </tr>
												   </table>
												</TD>
												<TD style="WIDTH: 92px" height="20">
                                                    <asp:TextBox ID="txt_Month" onkeyup="return autoTab(this, 2, event)" runat="server" CssClass="clsinputadministration" MaxLength="2"
                                                        Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_Day" runat="server" onkeyup="return autoTab(this, 2, event)" CssClass="clsinputadministration"
                                                            MaxLength="2" Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_Year" runat="server" onkeyup="return autoTab(this, 2, event)"
                                                                CssClass="clsinputadministration" MaxLength="2" Width="25px"></asp:TextBox></TD>
												<TD height="20" style="width: 75px"><asp:dropdownlist id="ddl_Time" runat="server" CssClass="clsinputcombo" Width="71px">
														<asp:ListItem Value="--">--</asp:ListItem>
														<asp:ListItem Value="8:00AM">8:00AM</asp:ListItem>
														<asp:ListItem Value="8:15AM">8:15AM</asp:ListItem>
														<asp:ListItem Value="8:30AM">8:30AM</asp:ListItem>
														<asp:ListItem Value="8:45AM">8:45AM</asp:ListItem>
														<asp:ListItem Value="9:00AM">9:00AM</asp:ListItem>
														<asp:ListItem Value="9:15AM">9:15AM</asp:ListItem>
														<asp:ListItem Value="9:30AM">9:30AM</asp:ListItem>
														<asp:ListItem Value="9:45AM">9:45AM</asp:ListItem>
														<asp:ListItem Value="10:00AM">10:00AM</asp:ListItem>
														<asp:ListItem Value="10:15AM">10:15AM</asp:ListItem>
														<asp:ListItem Value="10:30AM">10:30AM</asp:ListItem>
														<asp:ListItem Value="10:45AM">10:45AM</asp:ListItem>
														<asp:ListItem Value="11:00AM">11:00AM</asp:ListItem>
														<asp:ListItem Value="11:15AM">11:15AM</asp:ListItem>
														<asp:ListItem Value="11:30AM">11:30AM</asp:ListItem>
														<asp:ListItem Value="11:45AM">11:45AM</asp:ListItem>
														<asp:ListItem Value="12:00PM">12:00PM</asp:ListItem>
														<asp:ListItem Value="12:15PM">12:15PM</asp:ListItem>
														<asp:ListItem Value="12:30PM">12:30PM</asp:ListItem>
														<asp:ListItem Value="12:45PM">12:45PM</asp:ListItem>
														<asp:ListItem Value="1:00PM">1:00PM</asp:ListItem>
														<asp:ListItem Value="1:15PM">1:15PM</asp:ListItem>
														<asp:ListItem Value="1:30PM">1:30PM</asp:ListItem>
														<asp:ListItem Value="1:45PM">1:45PM</asp:ListItem>
														<asp:ListItem Value="2:00PM">2:00PM</asp:ListItem>
														<asp:ListItem Value="2:15PM">2:15PM</asp:ListItem>
														<asp:ListItem Value="2:30PM">2:30PM</asp:ListItem>
														<asp:ListItem Value="2:45PM">2:45PM</asp:ListItem>
														<asp:ListItem Value="3:00PM">3:00PM</asp:ListItem>
														<asp:ListItem Value="3:15PM">3:15PM</asp:ListItem>
														<asp:ListItem Value="3:30PM">3:30PM</asp:ListItem>
														<asp:ListItem Value="3:45PM">3:45PM</asp:ListItem>
														<asp:ListItem Value="4:00PM">4:00PM</asp:ListItem>
														<asp:ListItem Value="4:15PM">4:15PM</asp:ListItem>
														<asp:ListItem Value="4:30PM">4:30PM</asp:ListItem>
														<asp:ListItem Value="4:45PM">4:45PM</asp:ListItem>
														<asp:ListItem Value="5:00PM">5:00PM</asp:ListItem>
														<asp:ListItem Value="5:15PM">5:15PM</asp:ListItem>
														<asp:ListItem Value="5:30PM">5:30PM</asp:ListItem>
														<asp:ListItem Value="5:45PM">5:45PM</asp:ListItem>
														<asp:ListItem Value="6:00PM">6:00PM</asp:ListItem>
														<asp:ListItem Value="6:15PM">6:15PM</asp:ListItem>
														<asp:ListItem Value="6:30PM">6:30PM</asp:ListItem>
														<asp:ListItem Value="6:45PM">6:45PM</asp:ListItem>
														<asp:ListItem Value="7:00PM">7:00PM</asp:ListItem>
														<asp:ListItem Value="7:15PM">7:15PM</asp:ListItem>
														<asp:ListItem Value="7:30PM">7:30PM</asp:ListItem>
														<asp:ListItem Value="7:45PM">7:45PM</asp:ListItem>
														<asp:ListItem Value="8:00PM">8:00PM</asp:ListItem>
														<asp:ListItem Value="8:15PM">8:15PM</asp:ListItem>
														<asp:ListItem Value="8:30PM">8:30PM</asp:ListItem>
														<asp:ListItem Value="8:45PM">8:45PM</asp:ListItem>
														<asp:ListItem Value="9:00PM">9:00PM</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD height="20" style="width: 57px"><asp:textbox id="txt_CourtNo" runat="server" CssClass="clsinputadministration" Width="46px" MaxLength="3"></asp:textbox></TD>
												<ajax:ajaxpanel id="AjaxPanel1" runat="server">
												<td><asp:dropdownlist id="ddl_CourtLocation" runat="server" CssClass="clsinputcombo" AutoPostBack="True"></asp:dropdownlist>
														<asp:Label id="lblinactiveCourtName" runat="server" CssClass="label" Visible="False"></asp:Label></td>
													</ajax:ajaxpanel><ajax:ajaxpanel id="Ajaxpanel2" runat="server">		
												<td id="PricePlan1"><asp:dropdownlist id="ddl_PricePlan" runat="server" CssClass="clsinputcombo"></asp:dropdownlist></td>
												</ajax:ajaxpanel></TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD background="../../images/separator_repeat.gif" height="11"></TD>
								</TR>
								<TR>
									<TD class="clsLeftPaddingTable">
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 718px" background="../../images/headbar_headerextend.gif" colSpan="4"
										height="5"></TD>
								</TR>
								<TR  >
									<TD class="clssubhead" vAlign="middle" background="../Images/subhead_bg.gif" colSpan="3" height="34px">
                                        <table width="780px">
                                            <tr>
                                                <td class="clssubhead" style="width: 426px" >
                                                    <span>Case Update Information</span></td>
                                                <td style="text-align: right" >
                                        <asp:linkbutton id="lnkb_InsertViolation" runat="server" BackColor="#EEC75E">Insert Violation</asp:linkbutton></td>
                                            </tr>
                                        </table>
                                    </TD>
								</TR>
								<TR>
									<TD width="780">
										<TABLE id="vioDetail" cellSpacing="0" cellPadding="0" width="736" border="0">
											<TR>
												<TD width="100%" ><asp:datagrid id="dgViolationInfo" runat="server" CssClass="clsleftpaddingtable" Width="780px"
														BorderColor="Gray" BorderStyle="Solid" AutoGenerateColumns="False">
														<Columns>
															<asp:TemplateColumn>
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<HeaderTemplate>
																	<asp:CheckBox id="CheckAll" onclick="return select_deselectAll(this.checked, this.id);" runat="server"
																		CssClass="clslabelnew" Checked="False"></asp:CheckBox>
																</HeaderTemplate>
																<ItemTemplate>
																	<asp:checkbox id="Checkbox1" runat="server" CssClass="clslabelnew" Checked="False"></asp:checkbox>
																</ItemTemplate>
															</asp:TemplateColumn>
															
															<asp:TemplateColumn HeaderText="Cause Number">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_CauseNo" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Ticket No">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=lblTicketNo runat="server" CssClass="label" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>'>
																	</asp:Label>
																	<asp:LinkButton id=lbl_tno runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>' CommandName="TicketClick">
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Case/Court No">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	&nbsp;
																	<asp:Label id="Label4" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Violation Description">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=Label1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Fine Amount">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=Label2 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Fineamount", "{0:c}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Bond Amount">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=Label3 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Bondamount", "{0:c}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Bond">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_bond runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Price Plan">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_plan" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.PlanDescription") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Officer Name">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_OfficerName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Day">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Day" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerDay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
                                <tr>
                                    <td >
                                        <table id="tbl_autoscan_header" runat="server" width="780px" cellSpacing="0" cellPadding="0">
                                        <tr>
                                    <td colspan=4 background="../../images/separator_repeat.gif" height="11" width="780px">
                                        </td>
                                </tr>
                                            <tr>
                                                <td class="clssubhead"  background="../Images/subhead_bg.gif" colSpan="3" height="34px"  >
                                                Case Statuses
                                                </td>
                                                <td class="clssubhead" align="right"  background="../Images/subhead_bg.gif" colSpan="3" height="34px" >
                                                <A href="javascript:MakeVerfiedDateSameAsAutodate()">Verified=Auto</A>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="11">
										<TABLE id="tbl_autoscan" cellSpacing="0" cellPadding="0" width="780" border="0" runat="server">
											<TR class="clsLeftPaddingTable">
												<TD class="clsaspcolumnheader"></TD>
												<TD class="clsaspcolumnheader">Status</TD>
												<TD class="clsaspcolumnheader">Court Date</TD>
												<TD class="clsaspcolumnheader">Time</TD>
												<TD class="clsaspcolumnheader">No</TD>
											</TR>
											<TR>
												<TD class="clsLeftPaddingTable">AUTO</TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_AutoStatus" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable" ><asp:label id="lbl_AutoCourtDate" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_AutoTime" runat="server" CssClass="LABEL"></asp:label>&nbsp;
												</TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_AutoNo" runat="server" CssClass="LABEL"></asp:label>&nbsp;</TD>
											</TR>
											<TR>
												<TD class="clsLeftPaddingTable">SCAN</TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_ScanStatus" runat="server" CssClass="label"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_ScanCourtDate" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_ScanTime" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_ScanNo" runat="server" CssClass="label"></asp:label></TD>
											</TR>
											<TR>
												<TD class="clsLeftPaddingTable">MAIN</TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_MainStatus" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_MainCourtDdate" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_MainTime" runat="server" CssClass="LABEL"></asp:label></TD>
												<TD class="clsLeftPaddingTable"><asp:label id="lbl_MainNo" runat="server" CssClass="LABEL"></asp:label></TD>
											</TR>
											<TR>
												<TD class="clsLeftPaddingTable" colSpan="1" rowSpan="1"></TD>
												<TD class="clsLeftPaddingTable"></TD>
												<TD class="clsLeftPaddingTable"></TD>
												<TD class="clsLeftPaddingTable"></TD>
											</TR>
										</TABLE>
                                    </td>
                                </tr>
								<TR>
									<TD background="../../images/separator_repeat.gif" height="11">
									</TD>
								</TR>
								<TR>
									<TD align="right"><asp:button id="btn_Update" runat="server" CssClass="clsbutton" Text="Update Ticket(s)"></asp:button></TD>
								</TR>
								<TR>
									<TD background="../../images/separator_repeat.gif" height="11"></TD>
								</TR>
							</TABLE>
							<asp:label id="lbl_Message" runat="server" CssClass="Label" ForeColor="Red" Visible="False"></asp:label></TD>
					</TR>
					<TR>
						<TD style="VISIBILITY: hidden"><asp:textbox id="txt_FTicketCount" runat="server" Width="32px"></asp:textbox><asp:textbox id="txt_BondFlag" runat="server" Width="24px"></asp:textbox><asp:label id="lbl_AutoStatusID" runat="server" CssClass="LABEL" Width="56px" Height="8px"></asp:label><asp:textbox id="txt_IsNew" runat="server" Width="48px">0</asp:textbox><asp:label id="lbldetailcount" runat="server" CssClass="label"></asp:label><asp:checkbox id="chkb_Main" runat="server" CssClass="clslabelnew" Checked="True"></asp:checkbox><asp:label id="lblAdmin" runat="server" CssClass="label"></asp:label><asp:label id="lbl_ViolationID" runat="server" CssClass="label"></asp:label><asp:label id="lbldisplay" runat="server" CssClass="Label"></asp:label><asp:textbox id="TxtValue" runat="server" Width="46px"></asp:textbox><asp:label id="lblDispo" runat="server">0</asp:label>
							<asp:textbox id="txtinactive" runat="server" Width="46px"></asp:textbox>
							<asp:Label id="lblinactiveCourtID" runat="server" CssClass="label"></asp:Label>
                            <asp:TextBox ID="txtStatus" runat="server" Width="41px">1</asp:TextBox>
                            </TD>
					</TR>
				</tbody>
			</TABLE>
			<script language="javascript">checkAdmin();DisplayToggle(); ToggleStatus();  </script>
		</form>
	</body>
</HTML>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="~/WebControls/Footer.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ClientInfo.CaseHistory"
    SmartNavigation="False" CodeBehind="CaseHistory.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>History</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
		
			function DoValidate()
			{
				var txt = document.getElementById("txtSubject");
				if (txt.value == '')
					{
						alert("Please enter some note.");
						txt.focus();
						return false;
					}
			}
		
			function ChangeBGColor(pCell, pMouseStatus)
			{
				if (pMouseStatus == 'in')
					pCell.style.backgroundColor = '#FFF2D9';
				else
					pCell.style.backgroundColor = '#EFF4FB';
			}
			
			
			
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepnlpaging" runat="server">
        <ContentTemplate>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tbody>
                    <tr>
                        <td bgcolor="white" colspan="5">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tbody>
                                    <tr style="background-image: url(../../images/separator_repeat.gif">
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            colspan="5" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="Label" Width="32px"></asp:Label>,
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                            <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>)
                                            <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#eff4fb" style="visibility: hidden">
                            <table id="TableNote" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td align="center" bgcolor="#eff4fb">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td bgcolor="aliceblue">
                                            <asp:TextBox ID="txtSubject" runat="server" CssClass="clsInputadministration" Width="320px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnInsertNote" runat="server" CssClass="clsbutton" Width="80px" Font-Bold="True"
                                                Text="Insert Note"></asp:Button>
                                        </td>
                                        <td>
                                            &nbsp;<br />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td width="780" style="background-image: url(./../images/separator_repeat.gif)" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="User Activities:" Width="170px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <asp:DataGrid ID="dg_caseHistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                                <Columns>
                                                    <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyleBig"
                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                        DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}" HeaderStyle-Width="25%">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="User" DataField="lastname" ItemStyle-CssClass="GridItemStyleBig"
                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Description" DataField="subject" ItemStyle-CssClass="GridItemStyleBig"
                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <table border="0" cellpadding="0" cellspacing="0" runat="server" id="tbl_statushistory"
                                                width="100%">
                                                <tr>
                                                    <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                                        height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="100%" style="font-weight: bold; height: 13px;" class="clsLeftPaddingTable">
                                                        Auto Info History:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                                        height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100%">
                                                        <asp:DataGrid ID="dg_autohistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                            BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                                            <Columns>
                                                                <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                    DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}"></asp:BoundColumn>
                                                                <%--Yasir Kamal 6029 06/17/2009 show user lastname and court location--%>
                                                                <asp:BoundColumn HeaderText="User" DataField="lastname" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Cause Number" DataField="casenumassignedbycourt" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Ticket No" DataField="refcasenumber" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Auto Status" DataField="Description" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Court Date" DataField="NewAutoDate" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                    DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Court Time" DataField="NewAutoDate" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                    DataFormatString="{0:t}"></asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Court Number" DataField="NewAutoRoom" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Court Location" DataField="ShortName" ItemStyle-CssClass="GridItemStyleBig"
                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" id="forprimaryuser" runat="server"
                                                            style="display: none">
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                                <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                                                    height="11">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="100%" style="font-weight: bold" class="clsLeftPaddingTable">
                                                                    Status History:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                                                    height="11">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%">
                                                                    <asp:DataGrid ID="dg_statushistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                                        BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                                                        <Columns>
                                                                            <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyleBig"
                                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                                DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}" ItemStyle-Font-Size="7pt">
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn HeaderText="Ticket No" DataField="refcasenumber" ItemStyle-CssClass="GridItemStyleBig"
                                                                                HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                                ItemStyle-Font-Size="7pt"></asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="New Ver. Status">
                                                                                <HeaderStyle Width="13%" HorizontalAlign="left" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                    VerticalAlign="Top"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <itemstyle wrap="False" />
                                                                                    <asp:Label ID="lb_verifiedstatus" Font-Size="7pt" runat="server" CssClass="Label"
                                                                                        Text='<%# Eval("shortname") +" " + Eval("newverifiedstatus") +" "+ Eval("newverifiedDate","{0:d}")+" "+ Eval("newverifiedDate","{0:t}") + " " + Eval("NewverifiedRoom") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Old Ver. Status">
                                                                                <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                    VerticalAlign="Top"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lbllastname" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("oldverifiedstatus") +" "+ Eval("oldverifiedDate","{0:d}")+" "+ Eval("oldverifiedDate","{0:t}") + " " + Eval("oldverifiedRoom")  %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="New Scan Status">
                                                                                <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                    VerticalAlign="Top"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lb_note" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("newscanstatus") +" "+ Eval("newscandate","{0:d}")+" "+ Eval("newscandate","{0:t}")  + " " + Eval("NewscanRoom")  %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Old Scan Status">
                                                                                <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                    VerticalAlign="Top"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lb_note" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("oldscanstatus") +" "+ Eval("oldscandate","{0:d}")+" "+ Eval("oldscandate","{0:t}")  + " " + Eval("oldscanRoom") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="New Auto Status">
                                                                                <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                    VerticalAlign="Top"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lb_note" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("newautostatus") +" "+ Eval("newautodate","{0:d}")+" "+ Eval("newautodate","{0:t}")  + " " + Eval("NewautoRoom") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Old Auto Status">
                                                                                <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                    VerticalAlign="Top"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lb_note1" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("oldautostatus") +" "+ Eval("oldautodate","{0:d}")+" "+ Eval("oldautodate","{0:t}")   + " " + Eval("oldautoRoom") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                                        height="11">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Court Activities:" Width="162px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DataGrid ID="dg_SystemHistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                                OnItemDataBound="dg_SystemHistory_ItemDataBound">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Date &amp; Time">
                                                        <HeaderStyle Width="25%" HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top">
                                                        </HeaderStyle>
                                                        <ItemTemplate>
                                                            <itemstyle wrap="False" />
                                                            &nbsp;
                                                            <asp:Label ID="lb_Date" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.recdate") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="User">
                                                        <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top">
                                                        </HeaderStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbllastname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Description">
                                                        <ItemTemplate>
                                                            &nbsp;
                                                            <asp:Label ID="lb_note" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.subject") %>'>
                                                            </asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="75%" CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                            <asp:Label ID="lblTicklerHistory" runat="server" Font-Bold="True" Text="Tickler Court History:"
                                                Width="162px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gv_tickler" runat="server" Width="100%" BorderColor="DarkGray"
                                                BorderStyle="Solid" AllowSorting="true" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                                OnSorting="gv_tickler_Sorting">
                                                <Columns>
                                                    <asp:TemplateField SortExpression="updatedate" HeaderText="<u>Date &amp; Time</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lb_Date" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.updatedate","{0:MM/dd/yyyy @ HH:mm:ss tt}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="casenumassignedbycourt" HeaderText="<u>Cause Number</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lb_CauseNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.casenumassignedbycourt") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="ticklerevent" HeaderText="<u>Tickler Event</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lb_ticklerevent" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.ticklerevent") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                            <asp:Label ID="lblSMSHistory" runat="server" Font-Bold="True" Text="SMS History:"
                                                Width="162px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_SMSHistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Date &amp; Time">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_SMSDate" runat="server" CssClass="Label" Text='<%# Eval("recdate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="70px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cause No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_SMSTicketNo" runat="server" CssClass="Label" Text='<%# Eval("TicketNumber") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Mobile Number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lb_SMSMnumber" runat="server" CssClass="Label" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Message">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_SMSMessage" runat="server" CssClass="Label" Text='<%# Eval("SentInfo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Alert Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_SmsAlertType" runat="server" CssClass="Label" Text='<%# Eval("SmsCallType") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sent/Received">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblSendReceived" runat="server" CssClass="Label" Text='<%# Eval("SendRecieved") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" style="background-image: url(../../images/separator_repeat.gif)"
                                            height="11">
                                            <asp:HiddenField ID="hf_CheckBondReportForSecUser" Value="0" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

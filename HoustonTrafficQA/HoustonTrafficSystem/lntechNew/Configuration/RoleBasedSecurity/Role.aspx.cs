﻿using System;
using System.Web.UI.WebControls;
using HTP.ClientController;
using lntechNew.Components.ClientInfo;
using RoleBasedSecurity.DataTransferObjects;

namespace HTP.Configuration.RoleBasedSecurity
{
    /// <summary>
    /// This class represents all information about the Roles.
    /// </summary>
    public partial class Role : WebComponents.BasePage
    {
        #region Variables

        //Variable Declared for role based security controller class
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                {
                    //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                    var tpuserList = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });

                    //Setting Userid in View State for further Use.
                    ViewState["LoginEmployeeId"] = tpuserList[0].UserId;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    lbl_Message.Text = string.Empty;
                    switch (btnAdd.Text)
                    {
                        case "Add":
                            if (Add())
                            {
                                SetEmptyControl();
                                FillGrid();
                                lbl_Message.Text = "Record added successfully.";
                            }
                            else
                                lbl_Message.Text = "Record not added successfully.";
                            break;
                        case "Update":
                            if (Update())
                            {
                                SetEmptyControl();
                                FillGrid();
                                lbl_Message.Text = "Record updated successfully.";
                            }
                            else
                                lbl_Message.Text = "Record not updated successfully.";
                            break;
                    }
                }
                else
                {
                    rfvValue.ErrorMessage = "Controls are not validated properly. Please try again.";
                    ValidationSummary1.ShowSummary = true;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {

            try
            {
                SetEmptyControl();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("LnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((ImageButton)e.Row.FindControl("ImgDelete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvRole_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                switch (e.CommandName)
                {
                    case "image":
                        {
                            var isAssociated = Convert.ToBoolean((((HiddenField)gvRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfIsAssociated")).Value));
                            //Checking If case is not associated then Delete process will perform.
                            if (!isAssociated)
                            {
                                var id =
                                    Convert.ToInt32(
                                        (((HiddenField)
                                          gvRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).
                                            Value));
                                if (_roleBasedSecurityController.DeleteRole(new RoleDto { RoleId = id }))
                                {
                                    SetEmptyControl();
                                    lbl_Message.Text = "Record deleted successfully";
                                    FillGrid();
                                }
                                else
                                    lbl_Message.Text = "Record not deleted successfully";
                            }
                            else
                                lbl_Message.Text = "Role cannot be deleted as it is associated with User(s)/Right(s).";
                        }
                        break;
                    case "lnkbutton":
                        txtRole.Text = ((LinkButton)gvRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("LnkbtnValue")).Text;
                        ViewState["ValueID"] = ((HiddenField)gvRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfIsActive")).Value);
                        chkAllowAdmin.Checked = Convert.ToBoolean(((HiddenField)gvRole.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfAllowAdmin")).Value);
                        btnAdd.Text = "Update";
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            txtRole.Text = string.Empty;
            chkIsActive.Checked = false;
            chkAllowAdmin.Checked = false;
            lbl_Message.Text = string.Empty;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;
            btnAdd.Text = "Add";
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            //Call the Role Based Security Controller method to get all Roles and assigned to the List.
            var roleist = _roleBasedSecurityController.GetAllRole(new RoleDto { IsActive = null });

            //Checking if list is not empty then bind with drop down list.
            if (roleist != null)
            {
                gvRole.DataSource = roleist;
                gvRole.DataBind();
            }
            else
            {
                gvRole.DataSource = null;
                gvRole.DataBind();
            }
        }

        /// <summary>
        /// This method is used to Add/Insert New Role.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Add()
        {
            //Call the Role Based Security Controller method to Add Role.
            return _roleBasedSecurityController.AddRole(new RoleDto
            {
                RoleName = txtRole.Text.Trim(),
                AllowRightsAdmin = chkAllowAdmin.Checked,
                IsActive = chkIsActive.Checked,
                InsertedBy = Convert.ToInt32(ViewState["LoginEmployeeId"])
            });
        }

        /// <summary>
        /// This Method used to Update the existing Role.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            //Call the Role Based Security Controller method to Update Role.
            return _roleBasedSecurityController.UpdateRole(new RoleDto
            {
                RoleId = Convert.ToInt32(ViewState["ValueID"]),
                RoleName = txtRole.Text.Trim(),
                IsActive = chkIsActive.Checked,
                LastUpdatedBy = Convert.ToInt32(ViewState["LoginEmployeeId"]),
                AllowRightsAdmin = chkAllowAdmin.Checked
            });


        }

        #endregion

    }
}

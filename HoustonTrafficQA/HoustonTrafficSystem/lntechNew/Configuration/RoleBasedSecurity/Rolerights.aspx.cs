﻿using System;
using System.Data;
using System.Web;
using HTP.ClientController;
using lntechNew.Components.ClientInfo;
using RoleBasedSecurity.DataTransferObjects;
using System.Web.UI.WebControls;

namespace HTP.Configuration.RoleBasedSecurity
{
    /// <summary>
    /// This class represents all information about the Rolerights.
    /// </summary>
    public partial class Rolerights : WebComponents.BasePage
    {
        #region Variables

        //Variable Declared for role based security controller class
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                {
                    //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                    var tpuserList = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });

                    //Setting Userid in View State for further Use.
                    ViewState["LoginEmployeeId"] = tpuserList[0].UserId;
                    Populateddl();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblMenuMessage.Text = string.Empty;
            lblProcessMessage.Text = string.Empty;
            FillMenuGrid();
            FillProcessGrid();
            //Setting Role Id in View State at the time of Search for further Use.
            ViewState["RoleID"] = ddlRole.SelectedValue;
            //Enabling Update Button
            btnUpdate.Enabled = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //Updating Menu Role Rights
                UpdateMenuRoleRights();

                //Updating Process Role Rights
                UpdateProcessRoleRights();
                
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblProcessMessage.Text = ex.Message;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method is used to Fill Data into the Grid of Menue.
        /// </summary>
        private void FillMenuGrid()
        {
            lblMenuMessage.Text = string.Empty;

            //Call the Role Based Security Controller method to get all Menu Role Rights and assigned to the List.
            var menuRoleRightsList = _roleBasedSecurityController.GetAllMenuRoleRights(new UserAccessRightsDto
            {
                RoleId = Convert.ToInt32(ddlRole.SelectedValue),
                ApplicationId = Convert.ToInt32(ddlApplication.SelectedValue)

            });

            //Checking if list is not empty then bind with drop down List.
            if (menuRoleRightsList.Count > 0)
            {
                gvMenuRights.DataSource = menuRoleRightsList;
                gvMenuRights.DataBind();
                CheckMenuHeaderRow();
            }
            else
            {
                lblMenuMessage.Text = "No Record Found";
                gvMenuRights.DataSource = null;
                gvMenuRights.DataBind();
            }

        }


        /// <summary>
        /// This method is used to Fill Data into the Grid of Process.
        /// </summary>
        private void FillProcessGrid()
        {
            lblProcessMessage.Text = string.Empty;

            //Call the Role Based Security Controller method to get all Process Role Rights and assigned to the List.
            var processRoleRightsList = _roleBasedSecurityController.GetAllProcessRoleRights(new UserAccessRightsDto
            {
                RoleId = Convert.ToInt32(ddlRole.SelectedValue),
                ApplicationId = Convert.ToInt32(ddlApplication.SelectedValue)

            });

            //Checking if list is not empty then bind with drop down List.
            if (processRoleRightsList.Count > 0)
            {
                gvProcessRights.DataSource = processRoleRightsList;
                gvProcessRights.DataBind();
                //Check Header Row All check box
                CheckProcessHeaderRow();
            }
            else
            {
                lblProcessMessage.Text = "No Record Found";
                gvProcessRights.DataSource = null;
                gvProcessRights.DataBind();
            }

        }


        /// <summary>
        /// This method is using for populating the drop down list of Role and Application.
        /// </summary>
        private void Populateddl()
        {
            //Call the Role Based Security Controller method to get all Roles and assigned to the List.
            var rolelist = _roleBasedSecurityController.GetAllRole(new RoleDto { IsActive = true });

            //Checking if Role list is not empty then bind with drop down List.
            if (rolelist.Count > 0)
            {
                ddlRole.DataSource = rolelist;
                ddlRole.DataTextField = "RoleName";
                ddlRole.DataValueField = "RoleId";
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("--Select--", "0"));
            }

            //Call the Role Based Security Controller method to get all Applications and assigned to the List.
            var applicationlist = _roleBasedSecurityController.GetAllApplications(new ApplicationDto { IsActive = true });

            //Checking if Application list is not empty then bind with drop down List.
            if (applicationlist.Count <= 0) return;
            ddlApplication.DataSource = applicationlist;
            ddlApplication.DataTextField = "ApplicationName";
            ddlApplication.DataValueField = "ApplicationId";
            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// This Method updates the information related to Role Rights.
        /// </summary>
        /// <param name="canView">User can View the record or Not.</param>
        /// <param name="canAdd">User can Add the record or Not.</param>
        /// <param name="canEdit">User can Edit the record or Not.</param>
        /// <param name="canDelete">User can Delete the record or Not.</param>
        /// <param name="rightsId">Id of rights.</param>
        /// <param name="menuId">Id of the menu.</param>
        /// <returns></returns>
        private bool UpdateRoleRights(bool canView, bool canAdd, bool canEdit, bool canDelete, int rightsId, int menuId)
        {
            //Call the Role Based Security Controller method to Update Menu Role Rights.
            return _roleBasedSecurityController.UpdateMenuRoleRights(new UserAccessRightsDto
            {
                CanView = canView,
                CanAdd = canAdd,
                CanEdit = canEdit,
                CanDelete = canDelete,
                RightsId = rightsId,
                MenuId = menuId,
                RoleId = Convert.ToInt32(ViewState["RoleID"]),
                UserId = Convert.ToInt32(ViewState["LoginEmployeeId"])
            });
        }

        /// <summary>
        /// This Method updates the information related to Process Rights.
        /// </summary>
        /// <param name="canExecute">User can Execute the process or Not.</param>
        /// <param name="rightsId">Id of rights.</param>
        /// <param name="processId">Id of Process.</param>
        /// <returns></returns>
        private bool UpdateProcessRights(bool canExecute, int rightsId, int processId)
        {
            //Call the Role Based Security Controller method to Update Process Role Rights.
            return _roleBasedSecurityController.UpdateProcessRoleRights(new UserAccessRightsDto
            {
                CanExecute = canExecute,
                RightsId = rightsId,
                ProcessId = processId,
                RoleId = Convert.ToInt32(ViewState["RoleID"]),
                UserId = Convert.ToInt32(ViewState["LoginEmployeeId"])
            });
        }

        /// <summary>
        /// This Method is used to Update Menu Role Rights against Application and Role from grid
        /// </summary>
        private void UpdateMenuRoleRights()
        {
            foreach (GridViewRow dr in gvMenuRights.Rows)
            {
                var rightsId = ((HiddenField)dr.FindControl("HfRightsId"));
                var canView = ((CheckBox)dr.FindControl("ChkView"));
                var canAdd = ((CheckBox)dr.FindControl("Chkadd"));
                var canEdit = ((CheckBox)dr.FindControl("Chkedit"));
                var canDelete = ((CheckBox)dr.FindControl("Chkdelete"));
                var menuId = ((HiddenField)dr.FindControl("HfMenuId"));

                //Checking Rights Id is not null nor empty and also not equal to zero.
                if (!string.IsNullOrEmpty(rightsId.Value) && rightsId.Value != "0")
                {
                    UpdateRoleRights(canView.Checked, canAdd.Checked, canEdit.Checked, canDelete.Checked,
                                     Convert.ToInt32(rightsId.Value), Convert.ToInt32(menuId.Value));
                    //Fill Menu Grid
                    FillMenuGrid();
                    lblMenuMessage.Text = "Record Updated Successfully";
                }
                else
                {
                    //Checking if any one Or all are checked then update the Role rights
                    if (canView.Checked || canAdd.Checked || canEdit.Checked || canDelete.Checked)
                    {
                        UpdateRoleRights(canView.Checked, canAdd.Checked, canEdit.Checked, canDelete.Checked,
                                         rightsId.Value == "" ? 0 : Convert.ToInt32(rightsId.Value),
                                         Convert.ToInt32(menuId.Value));
                        //Fill Menu Grid
                        FillMenuGrid();
                        lblMenuMessage.Text = "Record Updated Successfully";
                    }
                }
            }
        }

        /// <summary>
        /// This Method is used to Update Process Role Rights against Application and Role from grid
        /// </summary>
        private void UpdateProcessRoleRights()
        {
            foreach (GridViewRow dr in gvProcessRights.Rows)
            {
                var rightsId = ((HiddenField)dr.FindControl("HfRightId"));
                var canExecute = ((CheckBox)dr.FindControl("ChkExecute"));
                var processId = ((HiddenField)dr.FindControl("HfProcessId"));

                //Checking Rights Id is not null nor empty and also not equal to zero.
                if (!string.IsNullOrEmpty(rightsId.Value) && rightsId.Value != "0")
                {
                    UpdateProcessRights(canExecute.Checked, Convert.ToInt32(rightsId.Value),
                                        Convert.ToInt32(processId.Value));
                    //Fill Process Grid
                    FillProcessGrid();
                    lblProcessMessage.Text = "Record Updated Successfully";

                }
                else
                {
                    //Checking if CanExecute checked
                    if (canExecute.Checked)
                    {
                        UpdateProcessRights(canExecute.Checked,
                                            rightsId.Value == "" ? 0 : Convert.ToInt32(rightsId.Value),
                                            Convert.ToInt32(processId.Value));
                        //Fill Process Grid
                        FillProcessGrid();
                        lblProcessMessage.Text = "Record Updated Successfully";
                    }
                }
            }
        }

        /// <summary>
        /// This Method is used to checked the Execute All Check Box if all items are checked in Process Grid
        /// </summary>
        private void CheckProcessHeaderRow()
        {
            //getting total rows count
            var count = gvProcessRights.Rows.Count;

            //decalring count variable
            var chkCount = 0;
            //traversing through Grid Rows
            foreach (GridViewRow dr in gvProcessRights.Rows)
            {
                //Getting Control from Row
                var chk = ((CheckBox)dr.FindControl("ChkExecute"));

                //Incrementing Count if Execute is Checked
                if (chk.Checked)
                    chkCount++;
            }
            //Settign Header Select All checkbox checked if rows count equals to Checked Execute checkboxes count
            if (count == chkCount)
            {
                ((CheckBox)gvProcessRights.HeaderRow.FindControl("ChkExecuteAll")).Checked = true;
            }
        }

        /// <summary>
        /// This Method is used to checked the View/Add/Edit and Delete All Check Box if all items are checked in Menu Grid
        /// </summary>
        private void CheckMenuHeaderRow()
        {
            //getting total rows count
            var count = gvMenuRights.Rows.Count;
            //decalring count variables
            var chkViewCount = 0;
            var chkAddCount = 0;
            var chkEditCount = 0;
            var chkDeleteCount = 0;
            //traversing through Grid Rows
            foreach (GridViewRow dr in gvMenuRights.Rows)
            {
                //Getting Controls from Rows
                var chkView = ((CheckBox)dr.FindControl("ChkView"));
                var chkEdit = ((CheckBox)dr.FindControl("Chkedit"));
                var chkAdd = ((CheckBox)dr.FindControl("Chkadd"));
                var chkDelete = ((CheckBox)dr.FindControl("Chkdelete"));

                //Incrementing Count if View is Checked
                if (chkView.Checked)
                    chkViewCount++;

                //Incrementing Count if Edit is Checked
                if (chkEdit.Checked)
                    chkEditCount++;

                //Incrementing Count if Add is Checked
                if (chkAdd.Checked)
                    chkAddCount++;

                //Incrementing Count if Delete is Checked
                if (chkDelete.Checked)
                    chkDeleteCount++;
            }

            //Settign Header Select All checkbox checked if rows count equals to Checked View checkboxes count
            if (count == chkViewCount)
            {
                ((CheckBox)gvMenuRights.HeaderRow.FindControl("ChkViewAll")).Checked = true;
            }

            //Settign Header Select All checkbox checked if rows count equals to Checked View checkboxes count
            if (count == chkEditCount)
            {
                ((CheckBox)gvMenuRights.HeaderRow.FindControl("ChkEditAll")).Checked = true;
            }

            //Settign Header Select All checkbox checked if rows count equals to Checked View checkboxes count
            if (count == chkAddCount)
            {
                ((CheckBox)gvMenuRights.HeaderRow.FindControl("ChkAddAll")).Checked = true;
            }

            //Settign Header Select All checkbox checked if rows count equals to Checked View checkboxes count
            if (count == chkDeleteCount)
            {
                ((CheckBox)gvMenuRights.HeaderRow.FindControl("ChkDeleteAll")).Checked = true;
            }
        }

        #endregion
    }
}

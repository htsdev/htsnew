﻿using System;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.ClientController;
using RoleBasedSecurity.DataTransferObjects;

namespace HTP.Configuration.RoleBasedSecurity
{
    /// <summary>
    /// This class represents all information about the User.
    /// </summary>
    public partial class Users : WebComponents.BasePage
    {
        # region Variables

        //Variable Declared for role based security controller class
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();

        //clsLogger variable declared to log the error or exception
        readonly clsLogger _clog = new clsLogger();

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        Populateddl();
                        //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                        var tpuserList = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });

                        //Setting Userid in View State for further Use.
                        ViewState["LoginEmployeeId"] = tpuserList[0].UserId;

                        try
                        {
                            //View State has been set empty for new user...
                            ViewState["EmployeeID"] = "";
                            // Display User Infomation In The Grid
                            FillGrid();
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = "Cannot Find Record";
                            _clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                    }


                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = dg_results;

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void dg_results_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Highlight Current DataGrid Row
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("LnkbtnLname")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                    ((LinkButton)e.Row.FindControl("LnkbtnResetPassword")).CommandArgument = Convert.ToString(e.Row.RowIndex);
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);

            }

        }

        protected void dg_results_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "click":
                        {
                            //Hiding Password, Role and Comapany Rows
                            trPassword.Style["display"] = "none";
                            trRC.Style["display"] = "none";
                            // If Selected Than Display Informaiton In The TextBoxs
                            var eId = (HiddenField)dg_results.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfUserId");
                            //set employee id into view state ...
                            ViewState["EmployeeID"] = eId.Value;
                            var userList = _roleBasedSecurityController.GetUserById(new UserDto { UserId = Convert.ToInt32(eId.Value) });
                            //using trim to remove spaces
                            //Displays Information In Text Boxes
                            tb_fname.Text = userList[0].FirstName.Trim();
                            tb_lname.Text = userList[0].LastName.Trim();
                            tb_abbrev.Text = userList[0].Abbreviation.Trim();
                            tb_uname.Text = userList[0].UserName.Trim();
                            txt_email.Text = userList[0].Email.Trim();
                            txt_NTUserID.Text = !string.IsNullOrEmpty(userList[0].NtUserId) ? userList[0].NtUserId.Trim() : "";

                            ddlStatus.SelectedValue = Convert.ToInt32(userList[0].IsActive).ToString();

                            btn_update.Text = "Update";
                            mpeConfigAdd.Show();

                        }
                        break;
                    case "reset":
                        {
                            // If Selected Than Display Informaiton In The TextBoxs
                            var eId = (HiddenField)dg_results.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfUserId");
                            //set employee id into view state ...
                            ViewState["EmployeeID"] = eId.Value;
                            mpeResetPassword.Show();

                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void dg_results_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dg_results.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void rbl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void DdlSortSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = string.Empty;
                switch (btn_update.Text)
                {
                    case "Add":
                        if (Add())
                        {
                            ClearValues();
                            FillGrid();
                            lblMessage.Text = "Record added successfully.";
                        }
                        else
                            lblMessage.Text = "Record not added successfully.";
                        break;
                    case "Update":
                        if (Update())
                        {
                            ClearValues();
                            FillGrid();
                            lblMessage.Text = "Record updated successfully.";
                        }
                        else
                            lblMessage.Text = "Record not updated successfully.";
                        break;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_clear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearValues();
                //Calling the Show method again to persist the display of popup
                mpeConfigAdd.Show();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnk_AddNewRecord_Click(object sender, EventArgs e)
        {
            try
            {
                ClearValues();
                btn_update.Text = "Add";
                //Display Password, Role and Comapany Rows
                trPassword.Style["display"] = "";
                trRC.Style["display"] = "";
                mpeConfigAdd.Show();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnClearResetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                ClearResetPasswordValues();
                //Calling the Show method again to persist the display of popup
                mpeResetPassword.Show();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnResetPassword_Click1(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = string.Empty;

                if (ResetPassword())
                {
                    ClearResetPasswordValues();
                    FillGrid();
                    lblMessage.Text = "Password reset successfully.";
                }
                else
                    lblMessage.Text = "Password not reset successfully.";

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion

        # region Methods

        /// <summary>
        /// This method is using for populating the drop down lists of Role, Company and User.
        /// </summary>
        private void Populateddl()
        {
            //Call the Role Based Security Controller method to get all Roles and assigned to the List.
            var rolelist = _roleBasedSecurityController.GetAllRole(new RoleDto { IsActive = true });

            //Checking if Role list is not empty then bind with drop down List.
            if (rolelist.Count > 0)
            {
                ddlRole.DataSource = rolelist;
                ddlRole.DataTextField = "RoleName";
                ddlRole.DataValueField = "RoleId";
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("--Select--", "0"));
            }

            //Call the Role Based Security Controller method to get all Companies and assigned to the List.
            var compnaylist = _roleBasedSecurityController.GetAllCompany(new CompanyDto { IsActive = true });

            //Checking if Company list is not empty then bind with drop down List.
            if (compnaylist.Count <= 0) return;
            ddlCompany.DataSource = compnaylist;
            ddlCompany.DataTextField = "CompanyName";
            ddlCompany.DataValueField = "CompanyId";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {

            // Getting List of All Users 
            lblMessage.Text = "";
            var userStatus = false;
            if (rbl_status.SelectedValue == "1")
                userStatus = true;

            //Call the Role Based Security Controller method to get all Users and assigned to the List.
            var userList = _roleBasedSecurityController.GetAllUser(new UserDto { IsActive = userStatus });

            //Checking if list is not empty then bind with drop down List.
            if (userList != null)
            {
                dg_results.DataSource = userList;
                dg_results.DataBind();
                Pagingctrl.GridView = dg_results;
                Pagingctrl.PageCount = dg_results.PageCount;
                Pagingctrl.PageIndex = dg_results.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                lblMessage.Text = "No Record Found";
                dg_results.DataSource = null;
                dg_results.DataBind();
            }


        }

        /// <summary>
        /// Use to empty the all Controls on the page.
        /// </summary>
        private void ClearValues()
        {
            // Clearing ALL Text Boxes
            tb_fname.Text = "";
            tb_lname.Text = "";
            tb_abbrev.Text = "";
            tb_password.Text = "";
            tb_uname.Text = "";
            //Fahad 7783 05/10/2010 Comment the code where we are set empty into Employee Id's  
            //ViewState["EmployeeID"] = "";
            txt_email.Text = "";
            txt_NTUserID.Text = "";

            ddlStatus.SelectedIndex = 0;
            //Checking for Role & Company row display property, and setting selected index 
            if (trRC.Style["display"] != "none")
            {
                ddlCompany.SelectedIndex = 0;
                ddlRole.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Use to empty the all reset pasword controls on the page.
        /// </summary>
        private void ClearResetPasswordValues()
        {
            // Clearing ALL Text Boxes
            //Fahad 7783 05/10/2010 Comment the code where we are set empty into Employee Id's  
            //ViewState["EmployeeID"] = "";
            txtNewPassword.Text = "";
            txtReTypeNewPassword.Text = "";
        }

        /// <summary>
        /// Method runs when page index changed.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            dg_results.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }

        /// <summary>
        /// Method runs when page size changed.
        /// </summary>
        /// <param name="pageSize">No of records per page.</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_results.PageIndex = 0;
                dg_results.PageSize = pageSize;
                dg_results.AllowPaging = true;
            }
            else
            {
                dg_results.AllowPaging = false;
            }
            FillGrid();
        }

        /// <summary>
        /// This method is used to Add/Insert New User.
        /// </summary>
        /// <returns>True or False</returns>
        private bool Add()
        {
            //Call the Role Based Security Controller method to Add User.
            return _roleBasedSecurityController.AddUser(new UserDto
            {
                UserName = tb_uname.Text.Trim(),
                Password = tb_password.Text.Trim(),
                FirstName = tb_fname.Text.Trim(),
                LastName = tb_lname.Text.Trim(),
                IsActive = Convert.ToBoolean(Convert.ToInt32(ddlStatus.SelectedValue)),
                InsertedBy = Convert.ToInt32(ViewState["LoginEmployeeId"]),
                Abbreviation = tb_abbrev.Text.Trim(),
                Email = txt_email.Text.Trim(),
                NtUserId = txt_NTUserID.Text.Trim(),
                RoleId = Convert.ToInt32(ddlRole.SelectedValue),
                CompnyId = Convert.ToInt32(ddlCompany.SelectedValue)
            });
        }

        /// <summary>
        /// This Method used to Update the existing User.
        /// </summary>
        /// <returns>True Or False</returns>
        private bool Update()
        {
            var empid = ViewState["EmployeeID"].ToString();
            if (empid == "")
                empid = "0";

            //Call the Role Based Security Controller method to Update User.
            return _roleBasedSecurityController.UpdateUser(new UserDto
            {
                UserId = Convert.ToInt32(empid),
                UserName = tb_uname.Text.Trim(),
                FirstName = tb_fname.Text.Trim(),
                LastName = tb_lname.Text.Trim(),
                IsActive = Convert.ToBoolean(Convert.ToInt32(ddlStatus.SelectedValue)),
                LastUpdatedBy = Convert.ToInt32(ViewState["LoginEmployeeId"]),
                Abbreviation = tb_abbrev.Text.Trim(),
                Email = txt_email.Text.Trim(),
                NtUserId = txt_NTUserID.Text.Trim()
            });
        }

        /// <summary>
        /// This Method used to Reset the password of existing User.
        /// </summary>
        /// <returns>True Or False</returns>
        private bool ResetPassword()
        {
            var empid = ViewState["EmployeeID"].ToString();
            if (empid == "")
                empid = "0";

            //Call the Role Based Security Controller method to Reset the Password of User.
            return _roleBasedSecurityController.ResetPassword(new UserLoginDto
                                                                  {
                                                                      LoginUserId = Convert.ToInt32(empid),
                                                                      LoginUserPassword = txtNewPassword.Text.Trim()

                                                                  });
        }
        #endregion

    }
}
﻿using System;
using System.Web.UI.WebControls;
using HTP.ClientController;
using lntechNew.Components.ClientInfo;
using RoleBasedSecurity.DataTransferObjects;


namespace HTP.Configuration.RoleBasedSecurity
{
    /// <summary>
    /// This class represents all information about the Process.
    /// </summary>
    public partial class Process : WebComponents.BasePage
    {
        #region Variables

        //Variable Declared for role based security controller class
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                //Checking Access Type if Not Primary then redirecting to Login Error Page
                if (AccessType != 2)
                {
                    Response.Redirect("~/LoginAccesserror.aspx", false);
                }
                else if (!IsPostBack)
                {
                    //Call Role Based Security Controller method to get User by Traffic Program ID and assigned to the List.
                    var tpuserList = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });

                    //Setting Userid in View State for further Use.
                    ViewState["LoginEmployeeId"] = tpuserList[0].UserId;
                    //Populating the drop down list having active values
                    Populateddl(true);
                    FillGrid();
                }
                //Assigned all methods to paging Control.
                Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                Pagingctrl.GridView = gvProcess;
                treditprocess.Visible = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    lbl_Message.Text = string.Empty;

                    if (Update())
                    {
                        SetEmptyControl();
                        FillGrid();
                        lbl_Message.Text = "Record updated successfully.";
                    }
                    else
                        lbl_Message.Text = "Record not updated successfully.";
                }
                else
                {
                    rfvProcess.ErrorMessage = "Controls are not validated properly. Please try again.";
                    ValidationSummary1.ShowSummary = true;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvProcess_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("LnkbtnValue")).CommandArgument = Convert.ToString(e.Row.RowIndex);                    
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvProcess_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = string.Empty;
                treditprocess.Visible = true;

                switch (e.CommandName)
                {
                    case "lnkbutton":
                        //Re-Populating the drop down list having all active and inactive values
                        Populateddl(null);
                        txtProcess.Text = ((LinkButton)gvProcess.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("LnkbtnValue")).Text;
                        ViewState["ValueID"] = ((HiddenField)gvProcess.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfValueId")).Value;
                        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)gvProcess.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfIsactive")).Value);
                        ddlApplication.SelectedValue = (((HiddenField)gvProcess.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("HfApplicationId")).Value);                    
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gvProcess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvProcess.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// This method used to set the default view of all the controls on the page.
        /// </summary>
        private void SetEmptyControl()
        {
            //Re-Populating the drop down list having active values
            Populateddl(true);
            txtProcess.Text = String.Empty;
            chkIsActive.Checked = false;
            ddlApplication.SelectedValue = "0";
            lbl_Message.Text = String.Empty;
            if (ViewState != null) ViewState["ValueID"] = String.Empty;

        }

        /// <summary>
        /// This method is used to Fill Data into the Grid.
        /// </summary>
        private void FillGrid()
        {
            //Call the Role Based Security Controller method to get all Users and assigned to the List.
            var processlist = _roleBasedSecurityController.GetAllProcess(new ProcessDto { IsActive = null });

            //Checking if list is not empty then bind with Grid.
            if (processlist != null)
            {
                gvProcess.DataSource = processlist;
                gvProcess.DataBind();
                Pagingctrl.Visible = true;
                Pagingctrl.GridView = gvProcess;
                Pagingctrl.PageCount = gvProcess.PageCount;
                Pagingctrl.PageIndex = gvProcess.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                Pagingctrl.Visible = false;
                gvProcess.DataSource = null;
                gvProcess.DataBind();
            }
        }
       
        /// <summary>
        /// This Method used to Update the existing Process.
        /// </summary>
        /// <returns>True/False</returns>
        private bool Update()
        {
            //Call the Role Based Security Controller method to Update the Process.
            return _roleBasedSecurityController.UpdateProcess(new ProcessDto
            {
                ProcessId = Convert.ToInt32(ViewState["ValueID"]),
                ProcessName = txtProcess.Text.Trim(),
                IsActive = chkIsActive.Checked,
                LastUpdatedBy = Convert.ToInt32(ViewState["LoginEmployeeId"]),
                ApplicationId = Convert.ToInt32(ddlApplication.SelectedValue)
            });


        }

        /// <summary>
        /// This method populating the Application drop dwon containg all Active Application.
        /// </summary>
        private void Populateddl(bool? isActive)
        {
            //Call the Role Based Security Controller method to get all Users and assigned to the List.
            var applicationlist = _roleBasedSecurityController.GetAllApplications(new ApplicationDto { IsActive = isActive });

            //Checking if list is not empty then bind with drop down List.
            if (applicationlist == null) return;
            ddlApplication.DataSource = applicationlist;
            ddlApplication.DataTextField = "ApplicationName";
            ddlApplication.DataValueField = "ApplicationId";
            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// Method runs when page index changed.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gvProcess.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }

        /// <summary>
        /// Method runs when page size changed.
        /// </summary>
        /// <param name="pageSize">No of records per page.</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gvProcess.PageIndex = 0;
                gvProcess.PageSize = pageSize;
                gvProcess.AllowPaging = true;
            }
            else
            {
                gvProcess.AllowPaging = false;
            }
            FillGrid();
        }

        #endregion



    }
}


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidationReport.aspx.cs"
    Inherits="HTP.Configuration.ValidationReport" EnableEventValidation="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ValidationReprotSetting" Src="../WebControls/ValidationReprotSetting.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Validation Report Settings</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="0" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js"  Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>   
    <uc1:ValidationReprotSetting ID="ValidationReportSettings" runat="server" />    
    </form>
</body>
</html>

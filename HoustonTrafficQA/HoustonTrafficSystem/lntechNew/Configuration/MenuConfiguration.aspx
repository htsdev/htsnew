﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MenuConfiguration.aspx.cs"
    Inherits="HTP.Configuration.MenuConfiguration" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagName="MenuConfig" TagPrefix="PrefMenuConfig" Src="~/WebControls/MenuConfigControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Configuration</title>
    <link href="../Styles.css" type="text/css" rel="Stylesheet" />

    <script language="javascript" type="text/javascript">
        //Waqas 7077 12/15/2009 
        //Show popup control to add menu item by calling server side method
        function ShowAddControl(levelid, menuid) {

            document.getElementById("MenuControl1_hf_LevelID").value = levelid;
            document.getElementById("MenuControl1_hf_MenuID").value = menuid;
            document.getElementById("MenuControl1_btnShowAdd").click();

            document.getElementById("lbl_Message").value = "";
            return false;

        }

        //Show popup control to update menu item by calling server side method
        function ShowUpdateControl(levelid, menuid) {

            document.getElementById("MenuControl1_hf_LevelID").value = levelid;
            document.getElementById("MenuControl1_hf_MenuID").value = menuid;
            document.getElementById("MenuControl1_btnShowUpdate").click();
            document.getElementById("lbl_Message").value = "";
            return false;

        }

        function closePopup() {


            $find("ModalPopup1").hide();

            return false;
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="DivMenu" style="z-index: 0; display: none; position: absolute; left: 0;
                top: 0; height: 1px; background-color: Silver; filter: alpha(opacity=50)">
            </div>
            <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td height="500px" valign="top">
                        <asp:Label ID="lbl_Message" runat="server" ForeColor="Red" Visible="True" CssClass="Label"></asp:Label>
                        <br />
                        <br />
                        <asp:TreeView ID="MenuTree" runat="server" CollapseImageToolTip="Collapse" ExpandImageToolTip="Expand">
                        </asp:TreeView>
                        <asp:Panel ID="pnlMenu" runat="server" Width="450px" Style="display: none">
                            <PrefMenuConfig:MenuConfig ID="MenuControl1" runat="server" />
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="ModalPopup1" runat="server" TargetControlID="btn_abc"
                            PopupControlID="pnlMenu" BackgroundCssClass="modalBackground" DropShadow="false">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="btn_abc" runat="server" Text="Up" Width="50" CssClass="clsbutton"
                            Style="display: none" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="MenuControl1$btnAdd" EventName="click"></aspnew:AsyncPostBackTrigger>
            <aspnew:AsyncPostBackTrigger ControlID="MenuControl1$btnUpdate" EventName="click">
            </aspnew:AsyncPostBackTrigger>
            <aspnew:AsyncPostBackTrigger ControlID="MenuControl1$btn_Up" EventName="click"></aspnew:AsyncPostBackTrigger>
        </Triggers>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

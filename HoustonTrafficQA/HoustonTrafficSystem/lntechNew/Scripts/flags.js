
// **********Added By : Zeeshan Ahmed ************//
//   Added For Hiding and Showing Drop Down List  //
//************************************************//

var  _backgroundElement = null;
var _foregroundElement = null;
var _saveTabIndexes = new Array();
var _saveDesableSelect = new Array();
 var _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');

function disableTab()  {
                
                
        _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');
              
        var i = 0;
        var tagElements;
        var tagElementsInPopUp = new Array();
        
        Array.clear(_saveTabIndexes);
        
         _foregroundElement = document.getElementById("pnl_serviceticket");

       //Save all popup's tag in tagElementsInPopUp
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = _foregroundElement.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                tagElementsInPopUp[i] = tagElements[k];
                i++;
            }
        }

        i = 0;
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = document.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                if (Array.indexOf(tagElementsInPopUp, tagElements[k]) == -1)  {
                    _saveTabIndexes[i] = {tag: tagElements[k], index: tagElements[k].tabIndex};
                    tagElements[k].tabIndex="-1";
                    i++;
                }
            }
        }

        //IE6 Bug with SELECT element always showing up on top
       
        i = 0;
            //Save SELECT in PopUp
             var tagSelectInPopUp = new Array();
            for (var j = 0; j < _tagWithTabIndex.length; j++) 
            {
                tagElements = _foregroundElement.getElementsByTagName('SELECT');
                for (var k = 0 ; k < tagElements.length; k++) {
                    tagSelectInPopUp[i] = tagElements[k];
                    i++;
                }
            }

            i = 0;
            Array.clear(_saveDesableSelect);
            tagElements = document.getElementsByTagName('SELECT');
            for (var k = 0 ; k < tagElements.length; k++) 
              {
                if (Array.indexOf(tagSelectInPopUp, tagElements[k]) == -1)  
                {
                    _saveDesableSelect[i] = {tag: tagElements[k], visib: getCurrentStyle(tagElements[k], 'visibility')} ;
                    tagElements[k].style.visibility = 'hidden';
                    i++;
                }
             }
        
        
    }
           
    

    function restoreTab()
     {
        
        for (var i = 0; i < _saveTabIndexes.length; i++) {
            _saveTabIndexes[i].tag.tabIndex = _saveTabIndexes[i].index;
        }

        for (var k = 0 ; k < this._saveDesableSelect.length; k++) {
                _saveDesableSelect[k].tag.style.visibility = _saveDesableSelect[k].visib;
            
        }
    }


        function getCurrentStyle (element, attribute, defaultValue) {
    

        var currentValue = null;
        if (element) {
            if (element.currentStyle) {
                currentValue = element.currentStyle[attribute];
            } else if (document.defaultView && document.defaultView.getComputedStyle) {
                var style = document.defaultView.getComputedStyle(element, null);
                if (style) {
                    currentValue = style[attribute];
                }
            }
            
            if (!currentValue && element.style.getPropertyValue) {
                currentValue = element.style.getPropertyValue(attribute);
            }
            else if (!currentValue && element.style.getAttribute) {
                currentValue = element.style.getAttribute(attribute);
            }       
        }
        
        if ((!currentValue || currentValue == "" || typeof(currentValue) === 'undefined')) {
            if (typeof(defaultValue) != 'undefined') {
                currentValue = defaultValue;
            }
            else {
                currentValue = null;
            }
        }   
        return currentValue;  
    }


//************************************************//

// Set Popup Control Location
 function setpopuplocation()
		{
	
	        var top  = 400;
	        var left = 400;
	        var height = document.body.offsetHeight;
	        var width  = document.body.offsetWidth
            
            // Setting Panel Location
	        if ( width > 1100 || width <= 1280) left = 575;
	        if ( width < 1100) left = 500;
    	    
		    // Setting popup display
	        document.getElementById("pnl_serviceticket").style.top =top;
		    document.getElementById("pnl_serviceticket").style.left =left;
    						
		    if (  document.body.offsetHeight > document.body.scrollHeight )
			    document.getElementById("Disable").style.height = document.body.offsetHeight;
		    else
		    document.getElementById("Disable").style.height = document.body.scrollHeight ;
    			
		    document.getElementById("Disable").style.width = document.body.offsetWidth;
		    document.getElementById("Disable").style.display = 'block'
		    document.getElementById("pnl_serviceticket").style.display = 'block'
		    disableTab();
		
		}    

function submitPopup1()
{
        closepopup("pnl_serviceticket", "Disable");
        return false;
        
}

function validateControl()
{
    
    if (  document.getElementById("ddl_category2").selectedIndex == 0)
    {
        alert("Please select service ticket category");
        document.getElementById("ddl_category2").focus();
        return false;
    }
    
     if (  document.getElementById("ddl_priority").selectedIndex == 0)
    {
        alert("Please select service ticket priority.");
        document.getElementById("ddl_priority").focus();
        return false;
    }

    submitPopup1();

}


function closepopup(ctrl1,ctrl2)
	    {	    
	         document.getElementById(ctrl1).style.display = 'none';
	         document.getElementById(ctrl2).style.display = 'none';	
	         restoreTab();
	         return false;
        }
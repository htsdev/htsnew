//functions are used to check/uncheck checkboxes in deletion processes of datagrids.

function select_deselectAll(chkVal, idVal) 
{ 
    var frm = document.forms[0];
    // Loop through all elements
    for (i=0; i<frm.length; i++) 
    {
		if(frm.elements[i].name != 'chkSentMark')
		{
			// Look for our Header Template's Checkbox
			if (idVal.indexOf ('CheckAll') != -1) 
			{
				// Check if main checkbox is checked, then select or deselect datagrid checkboxes 
				if(chkVal == true) 
				{
					frm.elements[i].checked = true;
				} 
				else 
				{
					frm.elements[i].checked = false;
				}
			}         
         
            // Work here with the Item Template's multiple checkboxes
			else if (idVal.indexOf ('ChkDelete') != -1) 
			{
				// Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
				if(frm.elements[i].checked == false) 
				{
					frm.elements[1].checked = false; //Uncheck main select all checkbox
				}
			}
		}
    }

} 
 
 


//used with toggle button
function selectdeselectAll() 
{ 
    var frm = document.forms[0];
    // Loop through all elements
    for (i=0; i<frm.length; i++) 
       {		
			if(frm.elements[i].name != 'chkSentMark')
			{
				if(frm.elements[i].checked == true)
				{
					frm.elements[i].checked=false 
				}			
				else
				{
					frm.elements[i].checked=true
				}
			}
        } 

} 
 
 


function confirmDelete(frm) 
{ 
    // loop through all elements
    for (i=0; i<frm.length; i++) 
    {
        // Look for our checkboxes only
        if (frm.elements[i].name.indexOf("ChkDelete") !=-1) 
        {
            // If any are checked then confirm alert, otherwise nothing happens
            if(frm.elements[i].checked) 
            {
                return confirm ('Are you sure you want to delete?')
                }
        }
    }
alert('Select the record you want to delete')
return false;
}


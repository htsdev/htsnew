﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WebSupergoo.ABCpdf6;
using FrameWorkEnation.Components;
using HTP.Components;


namespace HTP
{
    public partial class SharePointManager : System.Web.UI.Page
    {        
        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int items = SharePointWebWrapper.DeleteAllItems();
            lblMessage.Text = items + " items deleted.";
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            int items = SharePointWebWrapper.AddAllViolationsToCalendar();
            lblMessage.Text = items + " items added.";
        }

        protected void btn_UpdateAll_Click(object sender, EventArgs e)
        {
            int items = 0;
            clsENationWebComponents clsDB = new clsENationWebComponents();
            DataTable dtViolations = clsDB.Get_DS_BySP("USP_HTP_GetAllCriminalViolationsForSharePoint").Tables[0];

            foreach (DataRow dr in dtViolations.Rows)
            {
                SharePointWebWrapper.UpdateAllViolationsInCalendar(Convert.ToInt32(dr["TicketId"]));
                items++;
            }
            lblMessage.Text = items + " items added/updated.";
        }
    }
}

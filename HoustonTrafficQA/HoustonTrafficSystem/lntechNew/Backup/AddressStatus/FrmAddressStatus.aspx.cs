using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.AddressStatus
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
    public partial class FrmAddressStatus : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Button btnSearch;
		clsENationWebComponents ClsDb=new clsENationWebComponents();
	
		
		String Chk = "asd";
		protected System.Web.UI.WebControls.Repeater Repeater1;
		protected System.Web.UI.WebControls.Table oTable;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected eWorld.UI.CalendarPopup calFrom;
		protected eWorld.UI.CalendarPopup calTo;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.WebControls.TextBox txtSdate;
		Int16 clrval = 0;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
		clsSession ClsSession=new clsSession();

		private void Page_Load(object sender, System.EventArgs e)
		{
			//Added by Ozair
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx");
			}
			if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
			{
				Response.Redirect("../LoginAccesserror.aspx");
				Response.End();
			}
			//
			if (!IsPostBack)
			{
				btnSearch.Attributes.Add("OnClick","return ValidateMe();");
 
				DateTime  dt = new DateTime( DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
				//dt = dt.AddDays(-45);
				DateTime  dt1 = new DateTime( DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
				txtSdate.Text =(string) dt.ToString("MM/dd/yyyy") ;
				txtEndDate.Text = (string) dt1.ToString("MM/dd/yyyy");
				calFrom.SelectedDate= dt;
				calFrom.VisibleDate =calFrom.SelectedDate;
				calTo.SelectedDate = dt1;
				calTo.VisibleDate =calTo.SelectedDate;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			txtSdate.Text = calFrom.SelectedDate.ToString("MM/dd/yyyy");
			txtEndDate.Text = calTo.SelectedDate.ToString("MM/dd/yyyy");
			FillTable();
		}
		private void FillTable()
		{	
			try
			{
				oTable.CellPadding=0;
				oTable.CellSpacing=0;	
				oTable.GridLines=GridLines.Both;
				

				TableRow row=new TableRow();
				row.CssClass ="GrdHeader";	
				row.ForeColor =  Color.FromArgb(51,102,204);  //3366cc;

				row.HorizontalAlign = HorizontalAlign.Center ;

				TableCell cell7=new TableCell();
				cell7.RowSpan =2;
				cell7.Text="List Date";
				cell7.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell7);
					
				TableCell cell8=new TableCell();
				cell8.RowSpan =2;
				cell8.Text="Court Name";
				cell8.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell8);

						
				TableCell cell1=new TableCell();
				cell1.ColumnSpan = 3;
				cell1.Text="Y";
				cell1.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell1);
					
				TableCell cell2=new TableCell();
				cell2.ColumnSpan = 3;
				cell2.Text="D";
				cell2.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell2);

				TableCell cell3=new TableCell();
				cell3.ColumnSpan = 3;
				cell3.Text="S";
				cell3.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell3);

				TableCell cell4=new TableCell();
				cell4.ColumnSpan = 3;
				cell4.Text="N";
				cell4.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell4);
				
				TableCell cell5=new TableCell();
				cell5.ColumnSpan = 3;
				cell5.Text="Others";
				cell5.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell5);

				TableCell cell6=new TableCell();
				cell6.ColumnSpan = 3;
				cell6.Text="Total";
				cell6.ForeColor =  Color.FromArgb(51,102,204); 
				row.Cells.Add(cell6);

				oTable.Rows.Add(row);
					
				TableRow row1=new TableRow();
				row1.CssClass ="GrdHeader";
				row1.HorizontalAlign = HorizontalAlign.Center ;

				
				for (int a =1;a<=6;a++)
				{
					TableCell cell9=new TableCell();
					cell9.Text="Total";
					cell9.ForeColor =  Color.FromArgb(51,102,204); 
					row1.Cells.Add(cell9);
					
					TableCell cell10=new TableCell();
					cell10.Text="Hire";
					cell10.ForeColor =  Color.FromArgb(51,102,204); 
					row1.Cells.Add(cell10);

					TableCell cell11=new TableCell();
					cell11.Text="%";
					cell11.ForeColor =  Color.FromArgb(51,102,204); 
					row1.Cells.Add(cell11);
				}
				oTable.Rows.Add(row1);

				DataSet ds;
				
				string[] key1    = {"@sdate", "@edate"};

				object[] value1 = {txtSdate.Text,  txtEndDate.Text};

				ds = ClsDb.Get_DS_BySPArr("SP_Get_Address_Status_Report",key1, value1);
					
				for(int IdxRow=0;IdxRow<=ds.Tables[0].Rows.Count-1;IdxRow++)
				{
					TableRow row2=new TableRow();
					
					if (Chk.ToString() != ds.Tables[0].Rows[IdxRow]["ListDate"].ToString())
					{
						if (clrval == 0)
						{
							row2.BackColor =  Color.White;
							clrval =1;
							Chk = ds.Tables[0].Rows[IdxRow]["ListDate"].ToString();	
						}
						else
						{	
							//(() i.FindControl("trM")).BackColor = Color.FromArgb(238,238,238); //Color.WhiteSmoke;
						
							clrval =0;
							Chk = ds.Tables[0].Rows[IdxRow]["ListDate"].ToString();
						}
					}
					else 
						Chk = ds.Tables[0].Rows[IdxRow]["ListDate"].ToString();
					if (clrval == 0)
					{
						row2.BackColor = Color.White;
						clrval =0;
					}
					else
					{
						row2.BackColor = Color.FromArgb(238,238,238);
						clrval =1;
					}
	
					//for(int IdxCol=0;IdxCol<=ds.Tables[0].Rows.Count-1;IdxCol++)
					//{
						TableCell cell13=new TableCell();
						cell13.Text=ds.Tables[0].Rows[IdxRow]["ListDate"].ToString();
						row2.Cells.Add(cell13);
						
						TableCell cell14=new TableCell();
						cell14.Text=ds.Tables[0].Rows[IdxRow]["CourtName"].ToString();
						row2.Cells.Add(cell14);

						TableCell cell15=new TableCell();
						cell15.Text=ds.Tables[0].Rows[IdxRow]["No of Y"].ToString() + "&nbsp";
						row2.Cells.Add(cell15);

						TableCell cell16=new TableCell();
						cell16.Text=ds.Tables[0].Rows[IdxRow]["HiredY"].ToString() + "&nbsp";
						row2.Cells.Add(cell16);

						TableCell cell17=new TableCell();
						cell17.Text=ds.Tables[0].Rows[IdxRow]["HiredYage"].ToString() + "&nbsp";
						row2.Cells.Add(cell17);

						TableCell cell18=new TableCell();
						cell18.Text=ds.Tables[0].Rows[IdxRow]["No of D"].ToString() + "&nbsp";
						row2.Cells.Add(cell18);

						TableCell cell19=new TableCell();
						cell19.Text=ds.Tables[0].Rows[IdxRow]["HiredD"].ToString() + "&nbsp";
						row2.Cells.Add(cell19);
						
						TableCell cell20=new TableCell();
						cell20.Text=ds.Tables[0].Rows[IdxRow]["HiredDage"].ToString() + "&nbsp";
						row2.Cells.Add(cell20);

						TableCell cell21=new TableCell();
						cell21.Text=ds.Tables[0].Rows[IdxRow]["No of S"].ToString() + "&nbsp";
						row2.Cells.Add(cell21);

						TableCell cell22=new TableCell();
						cell22.Text=ds.Tables[0].Rows[IdxRow]["HiredS"].ToString() + "&nbsp";
						row2.Cells.Add(cell22);

						TableCell cell23=new TableCell();
						cell23.Text=ds.Tables[0].Rows[IdxRow]["HiredSage"].ToString() + "&nbsp";
						row2.Cells.Add(cell23);

						TableCell cell24=new TableCell();
						cell24.Text=ds.Tables[0].Rows[IdxRow]["No of N"].ToString() + "&nbsp";
						row2.Cells.Add(cell24);

						TableCell cell25=new TableCell();
						cell25.Text=ds.Tables[0].Rows[IdxRow]["HiredN"].ToString() + "&nbsp";
						row2.Cells.Add(cell25);
						
						TableCell cell26=new TableCell();
						cell26.Text=ds.Tables[0].Rows[IdxRow]["HiredNage"].ToString() + "&nbsp";
						row2.Cells.Add(cell26);
						
						TableCell cell27=new TableCell();
						cell27.Text=ds.Tables[0].Rows[IdxRow]["No of other"].ToString() + "&nbsp";
						row2.Cells.Add(cell27);
						
						TableCell cell28=new TableCell();
						cell28.Text=ds.Tables[0].Rows[IdxRow]["Hired0thers"].ToString() + "&nbsp";
						row2.Cells.Add(cell28);
						
						TableCell cell29=new TableCell();
						cell29.Text=ds.Tables[0].Rows[IdxRow]["Hired0thersage"].ToString() + "&nbsp";
						row2.Cells.Add(cell29);

						TableCell cell30=new TableCell();
						cell30.Text=ds.Tables[0].Rows[IdxRow]["totals"].ToString() + "&nbsp";
						row2.Cells.Add(cell30);
						
						TableCell cell31=new TableCell();
						cell31.Text=ds.Tables[0].Rows[IdxRow]["TotalHired"].ToString() + "&nbsp";
						row2.Cells.Add(cell31);
						
						TableCell cell32=new TableCell();
						cell32.Text=ds.Tables[0].Rows[IdxRow]["HiredTotalage"].ToString() + "&nbsp";
						row2.Cells.Add(cell32);
					//}
					oTable.Rows.Add(row2);
				}
			}
			catch(Exception ex)
			{
				TableRow row=new TableRow();
				TableCell cell=new TableCell();
				cell.Text="<h1><font color='red'>Invalid Value entered for Table Properties:</h1><br>" + ex.ToString();
				row.Cells.Add(cell);
				oTable.Rows.Add(row);
			}
		}
		
	}
}

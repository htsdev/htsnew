<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchDetail.aspx.cs" Inherits="HTP.DocumentTracking.BatchDetail" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Batch Detail</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function OpenScanMissing(dbid,sbid)
        {
            window.open('ScanMissing.aspx?DBID='+ dbid + '&SBID='+sbid,'scanmissing','height=240, width=450, resizable=no, status=no,toolbar=no,scrollbars=no,menubar=no');
            return false;
        }
        
        function OpenLetterDetail(dbid,sbid)
        {
            window.open('LetterDetail.aspx?DBID='+ dbid + '&SBID='+sbid,'letterdetail','height=500, width=450, resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no');
            return false;
        }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                &nbsp;Batch Detail
                            </td>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34" align="right">
                                &nbsp;<asp:HyperLink ID="hlk_Search" runat="server">Search</asp:HyperLink>&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="clsLeftPaddingTable">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable" id="tdProcess" style="display: none">
                    <asp:Label ID="lblProess" runat="server" Text="Please Wait Scanning and OCR is in Process..."
                        ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="height: 144px">
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="100%">
                                <asp:GridView ID="gv_BatchDetail" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AllowPaging="True" AllowSorting="True" PageSize="15" OnPageIndexChanging="gv_BatchDetail_PageIndexChanging"
                                    OnRowDataBound="gv_BatchDetail_RowDataBound" OnSorting="gv_BatchDetail_Sorting">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Batch ID" SortExpression="BatchID">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_BatchID" runat="server" Text='<%# bind("BatchID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Document ID" SortExpression="LetterID">
                                            <ItemTemplate>
                                                    <%--Fahad 5167/5168 03/05/2009 Comment and add following Line --%>
                                                <%--<asp:Label ID="lbl_LetterID" CssClass="Label" runat="server" Text='<%# bind("LetterID") %>'></asp:Label>--%>
                                                <asp:HyperLink ID="hlk_LetterID" CssClass="Label" runat="server" Text='<%# bind("LetterID") %>'>View</asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Document Type" SortExpression="DocumentType">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_DocumentType" CssClass="Label" runat="server" Text='<%# bind("DocumentType") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Pages" SortExpression="PageCount">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PageCount" CssClass="Label" runat="server" Text='<%# bind("PageCount") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Missing" SortExpression="Missing">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Missing" CssClass="Label" runat="server" Text='<%# bind("Missing") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Verified" SortExpression="Verified">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Verified" CssClass="Label" runat="server" Text='<%# bind("Verified") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlk_View" ForeColor="RoyalBlue" runat="server">View</asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlk_Scan" ForeColor="RoyalBlue" runat="server">Scan</asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtn_verify" runat="server" CommandName="Verify" OnCommand="lnkbtn_verify_Command">Verify</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td style="visibility: hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="display: none">
                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server" Text="Label"></asp:Label><asp:Label
                        ID="lbl_IsSplit" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

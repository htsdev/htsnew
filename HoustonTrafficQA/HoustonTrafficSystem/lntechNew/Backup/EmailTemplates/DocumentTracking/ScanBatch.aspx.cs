using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components;
using System.Text.RegularExpressions;


namespace HTP.DocumentTracking
{
    public partial class ScanBatch : System.Web.UI.Page
    {
        #region Variables

        clsGeneralMethods CGeneral = new clsGeneralMethods();
        clsDocumentTracking clsDT = new clsDocumentTracking();
        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();

        DataView DV;
        string ocr_data = String.Empty;
        string picDestination = String.Empty;
        string CourtStatus = String.Empty;
        string strServer;
        string searchpath = "";
        string picName = "";
        int scanBatchDetailID = 0;
        int scanBatchID = 0;
        string tempString = String.Empty;
        string StrAcsDec = String.Empty;
        string StrExp = String.Empty;
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (cSession.IsValidSession(this.Request) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lbl_Message.Visible = false;
                    if (Request.QueryString["sMenu"] != "" & Request.QueryString["sMenu"] != null)
                    {
                        ViewState["vSMenuID"] = Request.QueryString["sMenu"].ToString();
                        hlk_Search.NavigateUrl = "~/DocumentTracking/SearchBatch.aspx?sMenu=" + ViewState["vSMenuID"];
                    }
                    BindGrid();
                    cal_ScanDate.Text = System.DateTime.Today.ToShortDateString();
                    txtSrv.Text = Request.ServerVariables["SERVER_NAME"].ToString();//for scanning function in javascript.
                    strServer = "http://" + Request.ServerVariables["SERVER_NAME"];//for scanning function in javascript.
                    Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.
                    txtsessionid.Text = Session.SessionID.ToString();//for scanning function in javascript.
                    txtempid.Text = cSession.GetCookie("sEmpID", this.Request).ToString();//for scanning function in javascript.
                    ViewState["vNTPATHScanTemp"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingTemp"].ToString();//path to and get save images
                    ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHDocumentTrackingImage"].ToString();//path to save and get images
                    btnScan.Attributes.Add("onclick", "return StartScan();");
                }
            }
        }
        
        protected void btnScan_Click(object sender, EventArgs e)
        {
            lbl_Message.Visible = false;

            try
            {
                searchpath = "*" + Session.SessionID + txtempid.Text + "*.jpg";
                string[] filename = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpath);//get scann images
                if (filename.Length > 1)
                {
                    filename = clsDT.SortFilesByDate(filename);//sort files by date..
                }
                if (filename.Length >= 1)
                {
                    clsDT.ScanBatchDate = DateTime.Now;
                    clsDT.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                    scanBatchID = clsDT.GetSBIDByInsertingScanBatch();
                    if (scanBatchID > 0)
                    {
                        for (int i = 0; i < filename.Length; i++)
                        {
                            clsDT.ScanBatchID = scanBatchID;
                            clsDT.DocumentBatchID = 0;
                            clsDT.DocumentBatchPageCount = 0;
                            clsDT.DocumentBatchPageNo = 0;

                            scanBatchDetailID = clsDT.GetSBDIDByInsertingScanBatchDetail();

                            picName = scanBatchID.ToString() + "-" + scanBatchDetailID.ToString();
                            picDestination = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg";
                            File.Copy(filename[i].ToString(), picDestination);//save images with batchid and picid like 1-1.jpg.
                            File.Delete(filename[i].ToString());//delete from intial position.
                        }
                    }
                }

                //scanBatchID = 62;
                OCR(scanBatchID);//OCR process..

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
            finally
            {
                Response.Redirect("~/DocumentTracking/ScanBatch.aspx?sMenu=" + ViewState["vSMenuID"], true);
            }
        }

        protected void gv_Scan_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);//sorting
        }

        protected void gv_Scan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Scan.PageIndex = e.NewPageIndex;//paging
                DV = (DataView)Session["DV"];
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;

            }
        }

        protected void gv_Scan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string SBID = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hlk_BatchID = (HyperLink)e.Row.FindControl("hlk_BatchID");
                HyperLink hlk_NoLetterID = (HyperLink)e.Row.FindControl("hlk_NoLetterID");
                
                SBID = hlk_BatchID.Text;

                if (hlk_NoLetterID.Text == "0")
                {
                    hlk_BatchID.NavigateUrl = "~/DocumentTracking/BatchDetail.aspx?sMenu=" + ViewState["vSMenuID"] + "&SBID=" + SBID;
                }
                else
                {
                    hlk_NoLetterID.NavigateUrl = "~/DocumentTracking/UpdateBatch.aspx?sMenu=" + ViewState["vSMenuID"] + "&SBID=" + SBID;
                }
            }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                clsDT.CheckStatus = 1;
                DataSet DS_GetBatch = clsDT.GetSearchBatch();

                if (DS_GetBatch.Tables[0].Rows.Count > 0)
                {
                    gv_Scan.DataSource = DS_GetBatch;
                    DV = new DataView(DS_GetBatch.Tables[0]);
                    Session["DV"] = DV;
                    gv_Scan.DataBind();
                }
                else
                {
                    gv_Scan.Visible = false;
                    lbl_Message.Visible = true;
                    lbl_Message.Text = "No Record(s) Found.";                    
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }            

        }

        #region SortingLogic 
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DV"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gv_Scan.DataSource = DV;
                gv_Scan.DataBind();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        #endregion 

        private void OCR(int SBID)
        {
            try
            {
                string picPath = String.Empty;
                string picPathNew = String.Empty;
                int documentBatchID = 0;
                int pageCount = 0;
                int pageNo = 0;
                clsDT.ScanBatchID = SBID;
                DataSet ds = clsDT.GetScanDocByScanBatchID();//get scanbatchdetailID's of scan pages from database..
                
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        scanBatchDetailID = Convert.ToInt32(ds.Tables[0].Rows[i]["ScanBatchDetailID"]);
                        string pName = SBID.ToString() + "-" + scanBatchDetailID.ToString();
                        picPath = ViewState["vNTPATHScanImage"].ToString() + pName + ".jpg";//get Image from folder for OCR..
                        picPath = picPath.Replace("\\\\", "\\");

                        try
                        {
                            ocr_data = CGeneral.OcrIt(picPath);//OCR Process..
                        }
                        catch
                        {
                            continue;
                        }
                       
                        if (ocr_data.Length > 0)
                        {

                            tempString = ocr_data.Trim();
                            char[] lit = { '\r', '\n' };
                            string[] data = tempString.Split(lit);
                            
                            try
                            {
                                for (int j = data.Length - 1; j >= 0; j--)
                                {
                                    tempString = data[j].ToString().Trim();
                                    tempString = tempString.Replace(" ", "");

                                    Regex pat = new Regex(@"^\d*-\d*-\d*$");
                                    Match m = pat.Match(tempString);
                                    
                                    if (m.Success)
                                    {
                                        //ozair 4504 08/26/2008 exceptional handling incuded to avoid entry in error log  
                                        try
                                        {
                                            tempString = data[j].ToString().Trim();
                                            documentBatchID = Convert.ToInt32(tempString.Substring(0, tempString.IndexOf("-")));
                                            
                                            tempString = tempString.Substring(tempString.IndexOf("-") + 1);
                                            pageNo = Convert.ToInt32(tempString.Substring(0, tempString.IndexOf("-")));

                                            tempString = tempString.Substring(tempString.IndexOf("-") + 1);
                                            pageCount = Convert.ToInt32(tempString);

                                            clsDT.DocumentBatchID = documentBatchID;
                                            clsDT.DocumentBatchPageCount = pageCount;
                                            clsDT.DocumentBatchPageNo = pageNo;
                                            clsDT.ScanBatchDetailID = scanBatchDetailID;
                                            clsDT.OcrData = ocr_data;

                                            clsDT.UpdateScanBatchDetail();

                                            picName = SBID.ToString() + "-" + scanBatchDetailID.ToString() + "-" + documentBatchID.ToString();
                                            picPathNew = ViewState["vNTPATHScanImage"].ToString() + picName + ".jpg";
                                            File.Copy(picPath.ToString(), picPathNew);//save images with batchid and picid like 1-1.jpg.
                                            File.Delete(picPath.ToString());//delete from intial position.
                                        }
                                        catch
                                        {
                                            try
                                            {
                                                clsDT.DocumentBatchID = 0;
                                                clsDT.DocumentBatchPageCount = 0;
                                                clsDT.DocumentBatchPageNo = 0;
                                                clsDT.ScanBatchDetailID = scanBatchDetailID;
                                                clsDT.OcrData = ocr_data;

                                                clsDT.UpdateScanBatchDetail();
                                            }
                                            catch (Exception ex)
                                            {
                                                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                                            }
                                        }
                                        //end ozair 4504
                                        break;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            clsDT.DocumentBatchID = 0;
                                            clsDT.DocumentBatchPageCount = 0;
                                            clsDT.DocumentBatchPageNo = 0;
                                            clsDT.ScanBatchDetailID = scanBatchDetailID;
                                            clsDT.OcrData = ocr_data;

                                            clsDT.UpdateScanBatchDetail();
                                        }
                                        catch (Exception ex)
                                        {
                                            BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                                        }
                                    }
                                }
                            }
                            catch(Exception ex)
                            {
                                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);                                
                                try
                                {
                                    clsDT.DocumentBatchID = 0;
                                    clsDT.DocumentBatchPageCount = 0;
                                    clsDT.DocumentBatchPageNo = 0;
                                    clsDT.ScanBatchDetailID = scanBatchDetailID;
                                    clsDT.OcrData = ocr_data;

                                    clsDT.UpdateScanBatchDetail();
                                }
                                catch(Exception exc)
                                {
                                    BugTracker.ErrorLog(exc.Message, exc.Source, exc.TargetSite.ToString(), exc.StackTrace);                                                                        
                                }
                            }
                        }
                    }
                    catch
                    {

                    }
                }                
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Message.Text = ex.Message;
                lbl_Message.Visible = true;
            }
        }
        
        #endregion
    }
}

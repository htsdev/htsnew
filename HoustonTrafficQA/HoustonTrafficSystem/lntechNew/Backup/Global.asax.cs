using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using FrameWorkEnation.Components;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using LNHelper;

//Waqas 5933 05/28/2009 name space updated
namespace HTP
{
    /// <summary>
    /// Summary description for Global.
    /// </summary>
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        //Waqas 5933 05/28/2009
        public static DateTime tempDeleteDate;

        public Global()
        {
            InitializeComponent();
        }

        protected void Application_Start(Object sender, EventArgs e)
        {
            //Waqas 5933 05/28/2009
            tempDeleteDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        }

        protected void Session_Start(Object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {

        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {

        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            //Get reference to the source of the exception chain
            Logger.Instance.LogError(Server.GetLastError().GetBaseException());
        }

        protected void Session_End(Object sender, EventArgs e)
        {
            //Noufil 4707 09/19/2008 Remove Directory When User's Session End
            //string DirectoryPath = Server.MapPath("") + "\\Reports\\Temp\\" + this.Session.SessionID;

            //if (Directory.Exists(DirectoryPath))
            //    Directory.Delete(DirectoryPath, true);
        }

        protected void Application_End(Object sender, EventArgs e)
        {

        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
        }
        #endregion
    }
}


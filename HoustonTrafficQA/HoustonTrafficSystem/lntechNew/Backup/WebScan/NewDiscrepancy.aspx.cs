using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.Components;

namespace HTP.WebScan
{
    public partial class NewDiscrepancy : System.Web.UI.Page
    {
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        Discrepancy DiscInsert = new Discrepancy();
        clsSession cSession = new clsSession();
        string PicID = "";
        string path = "";
        string dpath = "";
        int pageno = 0;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    lbl_ocrdata.Attributes.Add("OnMouseOver", "StateTracePoupup('hf_ocrdata');");
                    lbl_ocrdata.Attributes.Add("OnMouseOut", "CursorIcon2()");

                    if (Request.QueryString["batchid"] != "" & Request.QueryString["batchid"] != null)
                    {
                        ViewState["BatchID"] = Request.QueryString["batchid"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();//get path from web config..
                        BindGrid();

                        FillCourtLocation();
                        DisplayOCR();
                    }
                    txtCauseNo.Focus();
                }
                if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    OCRData.Style["display"] = "none";
                }
            }
            btnUpdate.Attributes.Add("onclick", "return Validation();");
            img_delete.Attributes.Add("onclick", "return DeleteConfirm();");

        }
        
        protected void ddlImgsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Width = 0;
            int Height = 0;
            int FixWidth = 798;
            int FixHeight = 840;
            int ddlsize = 0;
            try
            {

                ddlsize = Convert.ToInt32(ddlImgsize.SelectedValue);
                ddlsize = ddlsize - 150;

                Width = FixWidth / 100 * ddlsize;
                Width = Width + FixWidth;

                Height = FixHeight / 100 * ddlsize;
                Height = Height + FixHeight;

                img_docs.Width = Width;
                img_docs.Height = Height;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        
        protected void ImgMoveFirst_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveFirst = clsDb.Get_DS_BySPArr("Usp_WebScan_GetDiscrepancyOCR", key, value);
                if (DS_MoveFirst.Tables[0].Rows.Count > 0)
                {

                    ViewState["PicID"] = DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString();
                    lblCauseno.Text = DS_MoveFirst.Tables[0].Rows[0]["CauseNo"].ToString();
                    lblcrtdate.Text = DS_MoveFirst.Tables[0].Rows[0]["NewCourtDate"].ToString();
                    lblcrttime.Text = DS_MoveFirst.Tables[0].Rows[0]["Time"].ToString();
                    lblcrtroom.Text = DS_MoveFirst.Tables[0].Rows[0]["CourtNo"].ToString();
                    lblstatus.Text = DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString();
                    lblcrtloc.Text = DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString();


                    // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    ddlstatus.Items.FindByText(DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString()).Selected = true;
                    if (DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString().ToUpper() == "HMC")
                    {
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = "1";
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveLast_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveLast = clsDb.Get_DS_BySPArr("Usp_WebScan_GetDiscrepancyOCR", key, value);
                if (DS_MoveLast.Tables[0].Rows.Count > 0)
                {
                    int cnt = Convert.ToInt32(DS_MoveLast.Tables[0].Rows.Count);
                    ViewState["PicID"] = DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString();


                    lblCauseno.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["CauseNo"].ToString();
                    lblcrtdate.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["NewCourtDate"].ToString();
                    lblcrttime.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["Time"].ToString();
                    lblcrtroom.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["CourtNo"].ToString();
                    lblstatus.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString();
                    lblcrtloc.Text = DS_MoveLast.Tables[0].Rows[cnt - 1]["Location"].ToString();


                    // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    ddlstatus.Items.FindByText(DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString()).Selected = true;
                    if (DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveLast.Tables[0].Rows[cnt - 1]["Location"].ToString().ToUpper() == "HMC")
                    {
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = cnt.ToString();
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMovePrev_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MovePrevious = clsDb.Get_DS_BySPArr("Usp_WebScan_GetDiscrepancyOCR", key, value);
                if (DS_MovePrevious.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MovePrevious.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MovePrevious.Tables[0].Rows[i]["picid"].ToString();

                        if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                        {
                            ViewState["PicID"] = DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString();
                            lblCauseno.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["CauseNo"].ToString();
                            lblcrtdate.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["NewCourtDate"].ToString();
                            lblcrttime.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["Time"].ToString();
                            lblcrtroom.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["CourtNo"].ToString();
                            lblstatus.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString();
                            lblcrtloc.Text = DS_MovePrevious.Tables[0].Rows[i - 1]["Location"].ToString();

                            // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                            //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                            ddlstatus.Items.FindByText(DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString()).Selected = true;
                            if (DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MovePrevious.Tables[0].Rows[i - 1]["Location"].ToString().ToUpper() == "HMC")
                            {
                                ddltime.Visible = true;
                                ddl_Time.Visible = false;
                            }
                            else
                            {
                                ddltime.Visible = false;
                                ddl_Time.Visible = true;
                            }
                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno - 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveNext_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveNext = clsDb.Get_DS_BySPArr("Usp_WebScan_GetDiscrepancyOCR", key, value);
                if (DS_MoveNext.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MoveNext.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MoveNext.Tables[0].Rows[i]["picid"].ToString();
                        if (ViewState["PicID"] == null)
                        {
                            ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();
                            lblCauseno.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CauseNo"].ToString();
                            lblcrtdate.Text = DS_MoveNext.Tables[0].Rows[i + 1]["NewCourtDate"].ToString();
                            lblcrttime.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Time"].ToString();
                            lblcrtroom.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CourtNo"].ToString();
                            lblstatus.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString();
                            lblcrtloc.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString();

                            // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                            //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                            ddlstatus.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString()).Selected = true;
                            if (DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString().ToUpper() == "HMC")
                            {
                                ddltime.Visible = true;
                                ddl_Time.Visible = false;
                            }
                            else
                            {
                                ddltime.Visible = false;
                                ddl_Time.Visible = true;
                            }
                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno + 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;

                        }
                        else
                        {
                            if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                            {
                                ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();

                                lblCauseno.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CauseNo"].ToString();
                                lblcrtdate.Text = DS_MoveNext.Tables[0].Rows[i + 1]["NewCourtDate"].ToString();
                                lblcrttime.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Time"].ToString();
                                lblcrtroom.Text = DS_MoveNext.Tables[0].Rows[i + 1]["CourtNo"].ToString();
                                lblstatus.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString();
                                lblcrtloc.Text = DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString();

                                //ddlCrtLoc.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString()).Selected = true;
                                ddlstatus.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString()).Selected = true;
                                if (DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString().ToUpper() == "HMC")
                                {
                                    ddltime.Visible = true;
                                    ddl_Time.Visible = false;
                                }
                                else
                                {
                                    ddltime.Visible = false;
                                    ddl_Time.Visible = true;
                                }

                                dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";
                                pageno = Convert.ToInt32(lblpageno.Text);
                                pageno = pageno + 1;
                                lblpageno.Text = pageno.ToString();
                                if (!File.Exists(dpath))
                                {
                                    img_docs.ImageUrl = "~/Images/not-available.gif";
                                    Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                                }
                                else
                                {
                                    img_docs.ImageUrl = dpath;
                                    Session["sBSDApath"] = dpath;
                                }
                                break;
                            }
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateDiscrepancy();
            txtCauseNo.Focus();
        }
        
        protected void gv_discrepancy_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();
                if (((HyperLink)e.Row.FindControl("verified")).Text == "0")
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "NewVerified.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("pending")).Text == "0")
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("pending")).NavigateUrl = "NewPending.aspx?batchid=" + batchid; }
                //if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{

                //    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "";
                //}
                ViewState["TotalCount"] = ((Label)e.Row.FindControl("lblCount")).Text.ToString();
            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }

            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void img_delete_Click(object sender, ImageClickEventArgs e)
        {

        }
        
        protected void img_delete_Click1(object sender, ImageClickEventArgs e)
        {
            Delete();
        }
       
        protected void img_flip_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                string fPath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                string dpath = fPath;
                fPath = fPath.Replace("\\\\", "\\");
                System.Drawing.Image img;
                img = System.Drawing.Image.FromFile(fPath);

                img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipXY);

                if (File.Exists(dpath))
                {
                    File.Delete(dpath);
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                img_docs.ImageUrl = fPath;
                Session["sBSDApath"] = fPath;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Rab Nawaz Khan 10007 01/31/2012 Court location added for the HMC courts
            if (ddlstatus.SelectedItem.Text == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
            {
                ddltime.Visible = true;
                ddl_Time.Visible = false;
            }
            else
            {
                ddltime.Visible = false;
                ddl_Time.Visible = true;
            }
        }
        
        #endregion

        #region Methods
        
        private void DisplayOCR()
        {
            try
            {

                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_DisplayOCR = clsDb.Get_DS_BySPArr("Usp_WebScan_GetDiscrepancyOCR", key, value);
                if (DS_DisplayOCR.Tables[0].Rows.Count > 0)
                {
                    lblCauseno.Text = DS_DisplayOCR.Tables[0].Rows[0]["CauseNo"].ToString();
                    lblcrtdate.Text = DS_DisplayOCR.Tables[0].Rows[0]["NewCourtDate"].ToString();
                    lblcrttime.Text = DS_DisplayOCR.Tables[0].Rows[0]["Time"].ToString();
                    lblcrtroom.Text = DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString();
                    lblstatus.Text = DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString();
                    lblcrtloc.Text = DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString();
                    hf_ocrdata.Value = DS_DisplayOCR.Tables[0].Rows[0]["ocrdata"].ToString();

                    lbl_ocrdata.Attributes.Add("OnMouseOver", "StateTracePoupup('hf_ocrdata');");
                    lbl_ocrdata.Attributes.Add("OnMouseOut", "CursorIcon2()");

                    // Rab Nawaz Khan 10007 01/31/2012 FindByText replaced with FindByValue
                    // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                    if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                        ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                    else if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                        ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                    else if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                        ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                    else if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                        ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                    else
                        ddlCrtLoc.Items.FindByText(DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                    
                    ddlstatus.Items.FindByText(DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString()).Selected = true;
                    if (DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString().ToUpper() == "HMC")
                    {
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                }
                else
                {
                    ddltime.Visible = true;
                    ddl_Time.Visible = false;
                }

            }
            catch
            {

            }
        }
        
        private void FillCourtLocation()
        {
            try
            {
                DataSet ds_Court = clsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
                ddlCrtLoc.Items.Clear();
                if(ds_Court.Tables[0].Rows[0]["Courtid"].ToString() == "0")
                {   
                    ds_Court.Tables[0].Rows[0].Delete();
                }

                ddlCrtLoc.DataSource = ds_Court.Tables[0];
                ddlCrtLoc.DataTextField = "shortname";
                ddlCrtLoc.DataValueField = "courtid";
                ddlCrtLoc.DataBind();
                // Rab Nawaz Khan 10007 01/31/2012 Comment the code to display HMC courts individualy 
                //ddlCrtLoc.Items[0].Text = "---Choose---";
                //ddlCrtLoc.Items[1].Text = "HMC";
                //ddlCrtLoc.Items[1].Value = "HMC";
                //ddlCrtLoc.SelectedValue = "HMC";
                //ddlCrtLoc.Items.RemoveAt(2);
                //ddlCrtLoc.Items.RemoveAt(2);

            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void BindGrid()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Discrepancy = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPending", key, value);
                //comments by khalid for fahad task 2617  dated 24-1-08
                if (DS_Discrepancy.Tables.Count > 0)
                {
                    if (DS_Discrepancy.Tables[0].Rows.Count > 0)
                    {
                        gv_discrepancy.DataSource = DS_Discrepancy;
                        gv_discrepancy.DataBind();
                        DisplayImage();
                    }
                    else
                    {
                        lblMessage.Text = "No Record in Discrepancy";
                        gv_discrepancy.DataSource = DS_Discrepancy;
                        gv_discrepancy.DataBind();
                        lblMessage.Visible = true;
                        DisplayImage();
                    }
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }
        
        private void DisplayImage()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Display = clsDb.Get_DS_BySPArr("Usp_WebScan_GetDiscrepancyDetail", key, value);
                if (DS_Display.Tables[0].Rows.Count > 0)
                {
                    PicID = DS_Display.Tables[0].Rows[0]["PicID"].ToString();
                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + PicID + ".jpg";
                    ViewState["PicID"] = PicID;
                    if (!File.Exists(path))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = path;
                        Session["sBSDApath"] = path;
                    }
                    lblpageno.Text = "1";
                    lblCount.Text = DS_Display.Tables[0].Rows.Count.ToString();
                    DisabledImageButtons();
                }
                else
                {
                    lblpageno.Text = "0";
                    lblCount.Text = "0";
                    DisabledImageButtons();
                    img_docs.Visible = false;
                    ddlImgsize.Enabled = false;
                    lblCauseno.Text = "";
                    lblcrtdate.Text = "";
                    lblcrtloc.Text = "";
                    lblcrtroom.Text = "";
                    lblcrttime.Text = "";
                    lblstatus.Text = "";
                    img_delete.Enabled = false;
                    img_flip.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        
        private void DisabledImageButtons()
        {
            if (lblpageno.Text == lblCount.Text)
            {
                ImgMoveLast.Enabled = false;
                ImgMoveNext.Enabled = false;
            }
            else
            {
                ImgMoveLast.Enabled = true;
                ImgMoveNext.Enabled = true;
            }
            if (lblpageno.Text == "1")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else if (lblpageno.Text == "0")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else
            {
                ImgMoveFirst.Enabled = true;
                ImgMovePrev.Enabled = true;
            }
        }
                
        private void UpdateDiscrepancy()
        {
            lblMessage.Visible = false;
            //Farrukh 9451 07/04/2011 replaced month textbox with dropdown
            string CourtDate = ddlmm.SelectedValue + "/" + txtdd.Text + "/" + txtyy.Text;
            try
            {
                DiscInsert.PicID = Convert.ToInt32(ViewState["PicID"]);
                DiscInsert.CauseNo = txtCauseNo.Text.ToString().Replace(" ", "").ToUpper();
                DiscInsert.CourtDateMain = Convert.ToDateTime(CourtDate);
                // Rab Nawaz Khan 10007 01/31/2012 Replace the drop down selected Text to Selected Value and added the HMC court ids 
                if (ddlstatus.SelectedItem.Text.ToString().ToUpper() == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
                {
                    DiscInsert.Time = ddltime.SelectedValue.ToString();
                    DiscInsert.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddltime.SelectedValue);
                }
                else
                {
                    DiscInsert.Time = ddl_Time.SelectedValue.ToString();
                    DiscInsert.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddl_Time.SelectedValue);
                }
                //ozair 4387 07/11/2008 Input string format issue handled in java script too
                DiscInsert.CourtNo = Convert.ToInt32(txtRoomNo.Text.Trim());
                //end ozair 4387
                DiscInsert.Status = ddlstatus.SelectedItem.Text.ToString();
                // Rab Nawaz Khan 10007 01/31/2012 Insert HMC in database as court location insted of seprate HMC court Names
                if (ddlstatus.SelectedItem.Text.ToString().ToUpper() == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
                    DiscInsert.Location = "HMC";
                else
                    DiscInsert.Location = ddlCrtLoc.SelectedItem.Text.ToString();
                // END 10007    

                int i = DiscInsert.ComparisonInsert();
                if (i == 3)
                {
                    lblMessage.Text = "Record Move to Queued Section";
                    lblMessage.Visible = true;
                    //HttpContext.Current.Response.Write("<script language='javascript'>alert('Record Move to Queued Section')</script>");
                    //Response.Redirect("NewQueued.aspx?batchid=" + ViewState["BatchID"].ToString());

                }
                if (i == 1)
                {
                    //HttpContext.Current.Response.Write("<script language='javascript'>alert('Record doesnot match with OCR Record and its id move to Pending Section')</script>");
                    //Response.Redirect("NewPending.aspx?batchid=" + ViewState["BatchID"].ToString());
                    lblMessage.Text = "Record doesnot match with OCR Record and it is Moved to Pending Section";
                    lblMessage.Visible = true;
                }
                BindGrid();
                RefreshControls();
                DisplayOCR();


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
        
        private void RefreshControls()
        {
            try
            {
                txtCauseNo.Text = "";
                txtdd.Text = "";
                //Farrukh 9451 07/04/2011 replaced month textbox with dropdown
                ddlmm.ClearSelection();
                txtRoomNo.Text = "";
                txtyy.Text = "";
                ddlCrtLoc.ClearSelection();
                ddltime.ClearSelection();
                ddl_Time.ClearSelection();
                ddlstatus.ClearSelection();
            }
            catch
            {
            }
        }
        
        private void RefreshDDLS()
        {
            try
            {

                ddlCrtLoc.ClearSelection();
                ddltime.ClearSelection();
                ddl_Time.ClearSelection();
                ddlstatus.ClearSelection();
            }
            catch
            {
            }
        }
               
        private void Delete()
        {
            try
            {
                if (ViewState["TotalCount"] != null && ViewState["TotalCount"].ToString() != "" && ViewState["TotalCount"].ToString() == "1")
                {
                    string[] key1 = { "@BatchID" };
                    object[] value1 = { Convert.ToInt32(ViewState["BatchID"]) };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteBatch", key1, value1);
                    lblMessage.Text = "Record Deleted";
                    lblMessage.Visible = true;
                }
                else
                {
                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    string[] key = { "@PicID" };
                    object[] value = { Convert.ToInt32(ViewState["PicID"]) };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteScanRecord", key, value);
                    lblMessage.Text = "Record Deleted";
                    lblMessage.Visible = true;
                }
                BindGrid();
                RefreshControls();
                DisplayOCR();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        #endregion
    }
}

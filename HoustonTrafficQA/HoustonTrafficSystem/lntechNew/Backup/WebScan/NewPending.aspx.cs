using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using FrameWorkEnation.Components;
using System.IO;

namespace HTP.WebScan
{
    public partial class NewPending : System.Web.UI.Page
    {
        #region Variables

        clsLogger BugTracker = new clsLogger();
        clsENationWebComponents clsDb = new clsENationWebComponents();
        PendingClass NewPendInsert = new PendingClass();
        clsSession cSession = new clsSession();
        string PicID = "";
        string path = "";
        string dpath = "";
        int pageno = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Waqas 5057 03/17/2009 Checking employee info in session
            if (cSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {

                    if (Request.QueryString["batchid"] != "" & Request.QueryString["batchid"] != null)
                    {
                        ViewState["BatchID"] = Request.QueryString["batchid"].ToString();
                        ViewState["vNTPATHScanImage"] = ConfigurationSettings.AppSettings["NTPATHScanImage2"].ToString();//get path from web config..
                        BindGrid();

                        FillCourtLocation();
                        DisplayOCR();
                    }
                    txtCauseNo.Focus();
                }
            }
            btnUpdate.Attributes.Add("onclick", "return Validation();");
            img_delete.Attributes.Add("onclick", "return DeleteConfirm();");
        }
       
        protected void ddlImgsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Width = 0;
            int Height = 0;
            int FixWidth = 798;
            int FixHeight = 840;
            int ddlsize = 0;
            try
            {

                ddlsize = Convert.ToInt32(ddlImgsize.SelectedValue);
                ddlsize = ddlsize - 150;

                Width = FixWidth / 100 * ddlsize;
                Width = Width + FixWidth;

                Height = FixHeight / 100 * ddlsize;
                Height = Height + FixHeight;

                img_docs.Width = Width;
                img_docs.Height = Height;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void gv_pending_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string batchid = "";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                batchid = ((Label)e.Row.FindControl("lblbatchid")).Text.ToString();
                if (((HyperLink)e.Row.FindControl("verified")).Text == "0")
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "NewVerified.aspx?batchid=" + batchid; }
                if (((HyperLink)e.Row.FindControl("queued")).Text == "0")
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "NewQueued.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("nocause")).Text == "0")
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "NewNoCause.aspx?batchid=" + batchid; }

                if (((HyperLink)e.Row.FindControl("discrepancy")).Text == "0")
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = ""; }
                else
                { ((HyperLink)e.Row.FindControl("discrepancy")).NavigateUrl = "NewDiscrepancy.aspx?batchid=" + batchid; }

                //if (cSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{

                //    ((HyperLink)e.Row.FindControl("queued")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("verified")).NavigateUrl = "";
                //    ((HyperLink)e.Row.FindControl("nocause")).NavigateUrl = "";
                //}

                ViewState["TotalCount"] = ((Label)e.Row.FindControl("lblCount")).Text.ToString();

            }
        }

        protected void ImgMoveFirst_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveFirst = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MoveFirst.Tables[0].Rows.Count > 0)
                {
                    ViewState["PicID"] = DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString();

                    // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                    if (DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_MoveFirst.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_MoveFirst.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                        ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                    else if (DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveFirst.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                        ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                    else if (DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveFirst.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                        ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                    else if (DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                        ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                    else
                        ddlCrtLoc.Items.FindByText(DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                    ddlstatus.Items.FindByText(DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString()).Selected = true;
                    if (DS_MoveFirst.Tables[0].Rows[0]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveFirst.Tables[0].Rows[0]["Location"].ToString().ToUpper() == "HMC")
                    {
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveFirst.Tables[0].Rows[0]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = "1";
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveLast_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveLast = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MoveLast.Tables[0].Rows.Count > 0)
                {
                    int cnt = Convert.ToInt32(DS_MoveLast.Tables[0].Rows.Count);
                    ViewState["PicID"] = DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString();

                    // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                    if (DS_MoveLast.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_MoveLast.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_MoveLast.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                        ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                    else if (DS_MoveLast.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveLast.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                        ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                    else if (DS_MoveLast.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveLast.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                        ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                    else if (DS_MoveLast.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                        ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                    else
                        ddlCrtLoc.Items.FindByText(DS_MoveLast.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                    ddlstatus.Items.FindByText(DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString()).Selected = true;
                    if (DS_MoveLast.Tables[0].Rows[cnt - 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveLast.Tables[0].Rows[cnt - 1]["Location"].ToString().ToUpper() == "HMC")
                    {
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                    dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveLast.Tables[0].Rows[cnt - 1]["picid"].ToString() + ".jpg";

                    if (!File.Exists(dpath))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = dpath;
                        Session["sBSDApath"] = dpath;
                    }
                    lblpageno.Text = cnt.ToString();
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMovePrev_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MovePrevious = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MovePrevious.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MovePrevious.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MovePrevious.Tables[0].Rows[i]["picid"].ToString();

                        if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                        {
                            ViewState["PicID"] = DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString();

                            // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                            //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                            // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                            if (DS_MovePrevious.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_MovePrevious.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_MovePrevious.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                                ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                            else if (DS_MovePrevious.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MovePrevious.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                                ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                            else if (DS_MovePrevious.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MovePrevious.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                                ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                            else if (DS_MovePrevious.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                                ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                            else
                                ddlCrtLoc.Items.FindByText(DS_MovePrevious.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                            ddlstatus.Items.FindByText(DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString()).Selected = true;
                            if (DS_MovePrevious.Tables[0].Rows[i - 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MovePrevious.Tables[0].Rows[i - 1]["Location"].ToString().ToUpper() == "HMC")
                            {
                                ddltime.Visible = true;
                                ddl_Time.Visible = false;
                            }
                            else
                            {
                                ddltime.Visible = false;
                                ddl_Time.Visible = true;
                            }

                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MovePrevious.Tables[0].Rows[i - 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno - 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }

        protected void ImgMoveNext_Click(object sender, ImageClickEventArgs e)
        {
            lblMessage.Visible = false;
            try
            {
                RefreshDDLS();

                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_MoveNext = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_MoveNext.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS_MoveNext.Tables[0].Rows.Count; i++)
                    {
                        string pid = DS_MoveNext.Tables[0].Rows[i]["picid"].ToString();
                        if (ViewState["PicID"] == null)
                        {
                            ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();
                            // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                            //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                            // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                            if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                                ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                            else if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                                ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                            else if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                                ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                            else if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                                ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                            else
                                ddlCrtLoc.Items.FindByText(DS_MoveNext.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                            ddlstatus.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString()).Selected = true;
                            if (DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString().ToUpper() == "HMC")
                            {
                                ddltime.Visible = true;
                                ddl_Time.Visible = false;
                            }
                            else
                            {
                                ddltime.Visible = false;
                                ddl_Time.Visible = true;
                            }
                            dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";
                            pageno = Convert.ToInt32(lblpageno.Text);
                            pageno = pageno + 1;
                            lblpageno.Text = pageno.ToString();
                            if (!File.Exists(dpath))
                            {
                                img_docs.ImageUrl = "~/Images/not-available.gif";
                                Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                            }
                            else
                            {
                                img_docs.ImageUrl = dpath;
                                Session["sBSDApath"] = dpath;
                            }
                            break;

                        }
                        else
                        {
                            if (Convert.ToInt32(pid) == Convert.ToInt32(ViewState["PicID"]))
                            {

                                ViewState["PicID"] = DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString();
                                // Rab Nawaz Khan 10007 01/31/2012 FindByText Replaced with FindByValue
                                //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                                // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                                if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                                    ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                                else if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                                    ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                                else if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_MoveNext.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                                    ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                                else if (DS_MoveNext.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                                    ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                                else
                                    ddlCrtLoc.Items.FindByText(DS_MoveNext.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                                ddlstatus.Items.FindByText(DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString()).Selected = true;
                                if (DS_MoveNext.Tables[0].Rows[i + 1]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_MoveNext.Tables[0].Rows[i + 1]["Location"].ToString().ToUpper() == "HMC")
                                {
                                    ddltime.Visible = true;
                                    ddl_Time.Visible = false;
                                }
                                else
                                {
                                    ddltime.Visible = false;
                                    ddl_Time.Visible = true;
                                }
                                dpath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + DS_MoveNext.Tables[0].Rows[i + 1]["picid"].ToString() + ".jpg";

                                pageno = Convert.ToInt32(lblpageno.Text);
                                pageno = pageno + 1;
                                lblpageno.Text = pageno.ToString();
                                if (!File.Exists(dpath))
                                {
                                    img_docs.ImageUrl = "~/Images/not-available.gif";
                                    Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                                }
                                else
                                {
                                    img_docs.ImageUrl = dpath;
                                    Session["sBSDApath"] = dpath;
                                }
                                break;
                            }
                        }
                    }
                }
                DisabledImageButtons();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;

            }
        }
        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            NewInsert();
            txtCauseNo.Focus();
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }

            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }
       
        protected void img_delete_Click(object sender, ImageClickEventArgs e)
        {
            Delete();
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void img_flip_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                string fPath = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                string dpath = fPath;
                fPath = fPath.Replace("\\\\", "\\");
                System.Drawing.Image img;
                img = System.Drawing.Image.FromFile(fPath);

                img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipXY);

                if (File.Exists(dpath))
                {
                    File.Delete(dpath);
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    img.Save(fPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                img_docs.ImageUrl = fPath;
                Session["sBSDApath"] = fPath;
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Rab Nawaz Khan 10007 01/31/2012 Court location added for the HMC courts
            if (ddlstatus.SelectedItem.Text == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
            {
                ddltime.Visible = true;
                ddl_Time.Visible = false;
            }
            else
            {
                ddltime.Visible = false;
                ddl_Time.Visible = true;
            }
        }

        protected void ddlCrtLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsAlloCourtNumbers(ddlCrtLoc.SelectedValue);
            
        }


        // Rab Nawaz Khan 8997 10/18/2011 Added method for allow or restrice the court Numbers for the outside courts
        /// <summary>
        /// This method is used to check the allo court room numbers status and return boolean value
        /// </summary>
        /// <param name="courtId">courtId</param>
        /// <returns>Boolean</returns>
        protected bool IsAlloCourtNumbers(string courtId)
        {
            clsCourts ClsCourts = new clsCourts();
            bool isAllowCourtNumber = false;
            // Rab Nawaz Khan 10007 01/31/2012 Added HMC Court Ids
            if (courtId == "3001" || courtId == "3002" || courtId == "3003")
            {
                crt_Room_txt_td.Style["Display"] = "";
                crt_Room_td.Style["Display"] = "";
                isAllowCourtNumber = true;
            }
            else
            {
                isAllowCourtNumber = ClsCourts.GetAllowedCourtRoomNumbers(Convert.ToInt32(courtId));
                if (isAllowCourtNumber)
                {
                    crt_Room_txt_td.Style["Display"] = "";
                    crt_Room_td.Style["Display"] = "";
                }
                else
                {
                    crt_Room_txt_td.Style["Display"] = "none";
                    crt_Room_td.Style["Display"] = "none";
                }
            }
            return isAllowCourtNumber;
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Pending = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPending", key, value);
                if (DS_Pending.Tables[0].Rows.Count > 0)
                {
                    gv_pending.DataSource = DS_Pending;
                    gv_pending.DataBind();
                    DisplayImage();
                }
                else
                {
                    lblMessage.Text = "No Record in Pending";
                    lblMessage.Visible = true;
                    gv_pending.DataSource = DS_Pending;
                    gv_pending.DataBind();
                    DisplayImage();
                }
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        private void DisplayOCR()
        {
            try
            {

                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_DisplayOCR = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendOCR", key, value);
                if (DS_DisplayOCR.Tables[0].Rows.Count > 0)
                {
                    // Haris Ahmed 10361 07/05/2012 Select Location in DropDownList
                    if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC" && (DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "13" || DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "14"))
                        ddlCrtLoc.Items.FindByValue("3002").Selected = true;
                    else if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "18")
                        ddlCrtLoc.Items.FindByValue("3003").Selected = true;
                    else if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC" && DS_DisplayOCR.Tables[0].Rows[0]["CourtNo"].ToString() == "20")
                        ddlCrtLoc.Items.FindByValue("3075").Selected = true;
                    else if (DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString() == "HMC")
                        ddlCrtLoc.Items.FindByValue("3001").Selected = true;
                    else
                        ddlCrtLoc.Items.FindByText(DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString()).Selected = true;
                    //ddlCrtLoc.Items.FindByValue("0").Selected = true;
                    ddlstatus.Items.FindByText(DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString()).Selected = true;
                    if (DS_DisplayOCR.Tables[0].Rows[0]["Status"].ToString().ToUpper() == "JURY TRIAL" & DS_DisplayOCR.Tables[0].Rows[0]["Location"].ToString().ToUpper() == "HMC")
                    {
                        ddltime.Visible = true;
                        ddl_Time.Visible = false;
                    }
                    else
                    {
                        ddltime.Visible = false;
                        ddl_Time.Visible = true;
                    }
                }
                else
                {
                    ddltime.Visible = true;
                    ddl_Time.Visible = false;
                }

            }
            catch
            {

            }
        }

        private void DisplayImage()
        {
            try
            {
                string[] key = { "@BatchID" };
                object[] value = { Convert.ToInt32(ViewState["BatchID"]) };
                DataSet DS_Display = clsDb.Get_DS_BySPArr("Usp_WebScan_GetPendingDetail", key, value);
                if (DS_Display.Tables[0].Rows.Count > 0)
                {
                    PicID = DS_Display.Tables[0].Rows[0]["PicID"].ToString();
                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + PicID + ".jpg";
                    ViewState["PicID"] = PicID;
                    if (!File.Exists(path))
                    {
                        img_docs.ImageUrl = "~/Images/not-available.gif";
                        Session["sBSDApath"] = Server.MapPath("/Images/") + "not-available.gif";
                    }
                    else
                    {
                        img_docs.ImageUrl = path;
                        Session["sBSDApath"] = path;
                    }
                    lblpageno.Text = "1";
                    lblCount.Text = DS_Display.Tables[0].Rows.Count.ToString();
                    DisabledImageButtons();
                }
                else
                {
                    lblpageno.Text = "0";
                    lblCount.Text = "0";
                    DisabledImageButtons();
                    img_docs.Visible = false;
                    ddlImgsize.Enabled = false;
                    img_delete.Enabled = false;
                    img_flip.Enabled = false;
                }


            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void DisabledImageButtons()
        {
            if (lblpageno.Text == lblCount.Text)
            {
                ImgMoveLast.Enabled = false;
                ImgMoveNext.Enabled = false;
            }
            else
            {
                ImgMoveLast.Enabled = true;
                ImgMoveNext.Enabled = true;
            }
            if (lblpageno.Text == "1")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else if (lblpageno.Text == "0")
            {
                ImgMoveFirst.Enabled = false;
                ImgMovePrev.Enabled = false;
            }
            else
            {
                ImgMoveFirst.Enabled = true;
                ImgMovePrev.Enabled = true;
            }
        }

        private void FillCourtLocation()
        {
            try
            {
                DataSet ds_Court = clsDb.Get_DS_BySP("usp_HTS_GetShortCourtName");
                ddlCrtLoc.Items.Clear();
                if(ds_Court.Tables[0].Rows[0]["Courtid"].ToString() == "0")
                {   
                    ds_Court.Tables[0].Rows[0].Delete();
                }                
                ddlCrtLoc.DataSource = ds_Court.Tables[0];
                ddlCrtLoc.DataTextField = "shortname";
                ddlCrtLoc.DataValueField = "courtid";
                ddlCrtLoc.DataBind();

                // Rab Nawaz Khan 10007 01/31/2012 Comment the code to display HMC courts individualy 
                //ddlCrtLoc.Items[0].Text = "---Choose---";
                //ddlCrtLoc.Items[1].Text = "HMC";
                //ddlCrtLoc.Items[1].Value = "HMC";
                //ddlCrtLoc.SelectedValue = "HMC";
                //ddlCrtLoc.Items.RemoveAt(2);
                //ddlCrtLoc.Items.RemoveAt(2);
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void NewInsert()
        {
            lblMessage.Visible = false;
            //Farrukh 9451 07/04/2011 replaced month textbox with dropdown
            string CourtDate = ddlmm.SelectedValue + "/" + txtdd.Text + "/" + txtyy.Text;
            try
            {
                NewPendInsert.PicID = Convert.ToInt32(ViewState["PicID"]);
                NewPendInsert.CauseNo = txtCauseNo.Text.ToString().Replace(" ", "").ToUpper();
                NewPendInsert.CourtDateMain = Convert.ToDateTime(CourtDate);
                // Rab Nawaz Khan 10007 01/31/2012 Replace the drop down selected Text to Selected Value and added the HMC court ids 
                if (ddlstatus.SelectedItem.Text.ToString().ToUpper() == "JURY TRIAL" & ((ddlCrtLoc.SelectedItem.Value == "3001") || (ddlCrtLoc.SelectedItem.Value == "3002") || (ddlCrtLoc.SelectedItem.Value == "3003")))
                {
                    NewPendInsert.Time = ddltime.SelectedValue.ToString();
                    NewPendInsert.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddltime.SelectedValue);
                }
                else
                {
                    NewPendInsert.Time = ddl_Time.SelectedValue.ToString();
                    NewPendInsert.CourtDateScan = Convert.ToDateTime(CourtDate + " " + ddl_Time.SelectedValue);
                }
                //ozair 4387 07/11/2008 Input string format issue handled in java script too
                // Rab Nawaz Khan 8997 08/02/2011 Added for allow or restrice the court Numbers for the outside courts
                if (IsAlloCourtNumbers(ddlCrtLoc.SelectedValue))
                {
                    NewPendInsert.CourtNo = int.Parse(txtRoomNo.Text.Trim());
                }
                //end ozair 4387
                NewPendInsert.Status = ddlstatus.SelectedItem.Text.ToString();
                // Rab Nawaz Khan 10007 01/31/2012 Insert HMC in database as court location insted of seprate HMC court Names
                if (ddlCrtLoc.SelectedItem.Value == "3001" || ddlCrtLoc.SelectedItem.Value == "3002" || ddlCrtLoc.SelectedItem.Value == "3003")
                    NewPendInsert.Location = "HMC";
                else
                    NewPendInsert.Location = ddlCrtLoc.SelectedItem.Text.ToString();
                // END 10007    

                int i = NewPendInsert.ComparisonInsert();
                if (i == 3)
                {
                    lblMessage.Text = "Reocord Move to Queued Section";
                    lblMessage.Visible = true;

                }
                if (i == 2)
                {
                    lblMessage.Text = "Record doesnot match with OCR Record and it is Moved to Descripancy Section";
                    lblMessage.Visible = true;

                }
                BindGrid();

                RefreshControls();
                DisplayOCR();

            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;

                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        private void RefreshControls()
        {
            txtCauseNo.Text = "";
            txtdd.Text = "";
            //Farrukh 9451 07/04/2011 replaced month textbox with dropdown
            ddlmm.ClearSelection();
            txtRoomNo.Text = "";
            txtyy.Text = "";
            ddlCrtLoc.ClearSelection();
            ddltime.ClearSelection();
            ddl_Time.ClearSelection();
            ddlstatus.ClearSelection();
        }
                
        private void RefreshDDLS()
        {
            try
            {

                ddlCrtLoc.ClearSelection();
                ddltime.ClearSelection();
                ddl_Time.ClearSelection();
                ddlstatus.ClearSelection();
            }
            catch
            {
            }
        }
                
        private void Delete()
        {
            try
            {
                if (ViewState["TotalCount"] != null && ViewState["TotalCount"].ToString() != "" && ViewState["TotalCount"].ToString() == "1")
                {
                    string[] key1 = { "@BatchID" };
                    object[] value1 = { Convert.ToInt32(ViewState["BatchID"]) };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteBatch", key1, value1);
                    lblMessage.Text = "Record Deleted";
                    lblMessage.Visible = true;
                }
                else
                {



                    path = ViewState["vNTPATHScanImage"].ToString() + ViewState["BatchID"].ToString() + "-" + ViewState["PicID"].ToString() + ".jpg";
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    string[] key = { "@PicID" };
                    object[] value = { Convert.ToInt32(ViewState["PicID"]) };
                    clsDb.InsertBySPArr("Usp_WebScan_DeleteScanRecord", key, value);
                    lblMessage.Text = "Record Deleted";
                    lblMessage.Visible = true;


                }
                BindGrid();

                RefreshControls();
                DisplayOCR();
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }

        }

        #endregion

    }
}

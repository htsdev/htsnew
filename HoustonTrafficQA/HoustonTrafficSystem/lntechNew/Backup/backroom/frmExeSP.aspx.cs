using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom.SqlSrvMrg
{
	/// <summary>
	/// Summary description for frmExeSP.
	/// </summary>
	public partial class frmExeSP : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid Dg_Output;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.Button Button1;
		string	ConStr = ConfigurationSettings.AppSettings["connString"].ToString();
		clsSession cSession = new clsSession();
		
		string sp_name ;


		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			// Check Session 
			if (cSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				if (cSession.GetCookie("sAccessType",this.Request).ToString()!="2")
				{
					Response.Redirect("../LoginAccesserror.aspx",false);
					Response.End();
				}
				else //To stop page further execution
				{	
			
					sp_name = Request.QueryString["spnm"].ToString();

					if  (IsPostBack!=true)
					{	
						ListParameters(sp_name);
					}
				}
			}
		}

		private void ListParameters(string sp_name)
		{
			DataSet ds =  SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"sp_help", new SqlParameter("@objname",sp_name ));
			if (ds.Tables.CanRemove(ds.Tables["table1"])) 
			{
				ds.Tables.Remove("Table");
			}
			else
			{
				lblmsg.Text = "No Parameter";
				return;
			}

			Dg_Output.DataSource = ds ;
			Dg_Output.DataBind();
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.DataGrid1.SelectedIndexChanged += new System.EventHandler(this.DataGrid1_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Button1_Click(object sender, System.EventArgs e)
		{

			SqlParameter [] arParms = new SqlParameter[Dg_Output.Items.Count];
			int idx=0;

			foreach(DataGridItem ItemX in Dg_Output.Items)
			{
				string name = ((Label) (ItemX.FindControl("lblParamName"))).Text ;
				string type = ((Label) (ItemX.FindControl("lblParamType"))).Text ;
				string len	= ((Label)  (ItemX.FindControl("lblParamLength"))).Text ;
				object sValue =  ((TextBox) (ItemX.FindControl("txtParamValue"))).Text ;
		
				
				//	System.Data.SqlDbType  Type = (System.Data.SqlDbType)GetDBType(type);
				
				int length = ((int) (Convert.ChangeType(len,typeof(int))));

				arParms[idx] = new SqlParameter(name, sValue); 
				idx++;
			}

			
			DataSet ds = SqlHelper.ExecuteDataset(ConStr, CommandType.StoredProcedure,sp_name,arParms ); 
			// new SqlParameter("@court_Name",TxtCountNm.Text),new SqlParameter("@Court_desc",TxtCountDesc.Text),new SqlParameter("@File_type",DdlFileType.SelectedItem.Text), new SqlParameter("@court_Id",SqlDbType.Int,4,ParameterDirection.Output );
			DataGrid1.DataSource = ds ;
			DataGrid1.DataBind();
		
		}

		private System.Data.DbType GetDBType(string strType) 
		{ 
			switch(strType.Trim())
			{		
				
				case "Binary":
					return System.Data.DbType.Binary; 
					
				case "Boolean":
					return System.Data.DbType.Boolean; 
					
				case "Byte":
					return System.Data.DbType.Byte; 
					
				case "Currency":
					return System.Data.DbType.Currency; 
					
				case "Date":
					return System.Data.DbType.Date; 
					
				case "DateTime":
					return System.Data.DbType.DateTime; 
					
				case "Decimal":
					return System.Data.DbType.Decimal; 
					
				case "Double":
					return System.Data.DbType.Double; 
					
				case "Int16":
					return System.Data.DbType.Int16; 
					
				case "Int32":
					return System.Data.DbType.Int32; 
					
				case "Int64":
					return System.Data.DbType.Int64; 
					
				case "SByte":
					return System.Data.DbType.SByte; 
					
				case "Single":
					return System.Data.DbType.Single; 
					
				case "String":
					return System.Data.DbType.String; 
					
				case "StringFixedLength":
					return System.Data.DbType.StringFixedLength; 
					
				case "Time":
					return System.Data.DbType.Time; 
					
				case "UInt16":
					return System.Data.DbType.UInt16; 
					
				case "UInt32":
					return System.Data.DbType.UInt32; 
					
				case "UInt64":
					return System.Data.DbType.UInt64; 
					
				case "VarNumeric":
					return System.Data.DbType.VarNumeric; 
				case "Byte[]":
					return System.Data.DbType.Object; 
					//return (System.Data.DbType)(System.Data.SqlDbType.Image);   
				default:
					return System.Data.DbType.Int64; 
			}
		}

		private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for closingstats.
	/// </summary>
	public partial class closingstats : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lbl_error;
		protected eWorld.UI.CalendarPopup cal_from;
		protected eWorld.UI.CalendarPopup cal_to;
		protected System.Web.UI.WebControls.Button btnsubmit;
		protected System.Web.UI.WebControls.DataGrid dgresult;
		clsENationWebComponents ClsDB = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
	

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (ClsSession.IsValidSession(this.Request)==false)
			{
				Response.Redirect("../frmlogin.aspx",false);
			}
			else //To stop page further execution
			{
				if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
				{
					Response.Redirect("../LoginAccesserror.aspx",false);
					Response.End();
				}
				else //To stop page further execution
				{
					if ( ! IsPostBack)
					{
			
						cal_from.SelectedDate = DateTime.Now;
						cal_to.SelectedDate= DateTime.Now;
						btnsubmit.Attributes.Add("OnClick", "return validate();");

					}
					lbl_error.Text="";
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnsubmit.Click += new System.EventHandler(this.btnsubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnsubmit_Click(object sender, System.EventArgs e)
		{
			try
			{

				// Searching Records
			
				if ( cal_from.SelectedDate.ToString() !="1/1/1" && cal_to.SelectedDate.ToString() !="1/1/1")
				{
					string[] keys = {"@startdate","@enddate"};
					object[] values = {cal_from.SelectedDate.ToShortDateString(),cal_to.SelectedDate.ToShortDateString()};
					DataSet ds = ClsDB.Get_DS_BySPArr("USP_HTS_list_all_cases_disposition_status",keys,values);

					
					if ( ds.Tables.Count > 0 ) 
					{
						
						if ( ds.Tables[0].Rows.Count > 0)
						{
							dgresult.DataSource=ds;
							dgresult.DataBind();
							setPercentages();
							dgresult.Visible = true;
						}

						else
						{
							lbl_error.Text="No Record Found";
							dgresult.Visible=false;
						}

						//	dgresult.ShowHeader = false;
					
					}
					else
					{
						dgresult.Visible = false;
					}
				}
				
                
			}
			catch( Exception ex)
			{
				lbl_error.Text= ex.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				
			}
		}
		private void setPercentages()
		{
		
			try
			{
            	
				// Generating No of Percentages of Case Categories

				int i=1;
				double total=0;

				
				foreach ( DataGridItem item in dgresult.Items)
				{
				
					Label lbl= (Label) item.FindControl("lbl_violcount");
				
					if ( i ==1 )
					{
				
						// Save Total Cases For The First Time
						total = Convert.ToDouble(lbl.Text);
						i=0;

					}
					
					else
					{
						// Calculate in % how many cases are in different categories
						((Label) item.FindControl("lbl_result")).Text = Convert.ToString( Math.Round(( Convert.ToDouble(lbl.Text)/total)*100,2))+ "%";
					
					}
				}
		
			}
			catch  ( Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);			
			}

		
		
		}
	}
}

<%@ Register TagPrefix="uc1" TagName="Footer" Src="~/WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.AddCourtStatusCategory" Codebehind="AddCourtStatusCategory.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Case Categories</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script lang="javascript">
		function isUnassignedSelected()
		{
			var listUnassigned = document.getElementById("listUnassigned");
			if( listUnassigned.selectedIndex<0 )
			{
				alert("Please select atleast one value from Unassigned list");
				return false; 
			}
		}

		function isAssignedSelected()
		{
			var listAssigned = document.getElementById("listAssigned");
			if( listAssigned.selectedIndex<0 )
			{
				alert("Please select atleast one value from Assigned list");
				return false; 
			}

        	if (listAssigned.options[listAssigned.selectedIndex].value == "3" || listAssigned.options[listAssigned.selectedIndex].value == "135" || listAssigned.options[listAssigned.selectedIndex].value == "101" || listAssigned.options[listAssigned.selectedIndex].value == "103" || listAssigned.options[listAssigned.selectedIndex].value == "26" || listAssigned.options[listAssigned.selectedIndex].value == "104" || listAssigned.options[listAssigned.selectedIndex].value == "161" || listAssigned.options[listAssigned.selectedIndex].value == "27" || listAssigned.options[listAssigned.selectedIndex].value == "105" || listAssigned.options[listAssigned.selectedIndex].value == "147" || listAssigned.options[listAssigned.selectedIndex].value == "80" )
			    {
                alert("This is a primary case status and you can not\n\runassign this case status.");
                return false;
                }

		}

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
				</TR>
				<TR>
					<TD style="HEIGHT: 464px">
						<TABLE id="tblsub" cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 99%">
							<TR>
								<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
							</TR>
							<TR>
								<TD background="../../images/separator_repeat.gif" Height="11"></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable">
									<TABLE id="tblheader" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="clsLeftPaddingTable" width="13%" align="right" style="height: 19px">Select Category</TD>
											<TD class="clsLeftPaddingTable" style="height: 19px"><asp:dropdownlist id="ddlCategory" runat="server" CssClass="clsinputcombo" Width="128px" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
													<asp:ListItem Value="-1">--Choose--</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR vAlign="top">
								<TD class="clsLeftPaddingTable" vAlign="top">
									<TABLE id="tbl_list" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="clsLeftPaddingTable" vAlign="top" align="center" colSpan="2" rowSpan="1">UnAssigned&nbsp;Case
											</TD>
											<TD class="clsLeftPaddingTable" align="center" colSpan="3" rowSpan="1">Assigned 
												Case</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="right" width="40%" colSpan="3"><asp:listbox id="listUnassigned" runat="server" CssClass="clstextarea" Width="328px" Rows="14"
													SelectionMode="Multiple" Height="272px"></asp:listbox></TD>
											<TD vAlign="top" align="center" width="7%">
												<P>&nbsp;</P>
												<P>&nbsp;</P>
												<P><asp:imagebutton id="imgbtnAdd" runat="server" AlternateText=">" ImageUrl="../Images/rightarrow-crop.gif"
														ToolTip="Add selected violations to category"></asp:imagebutton></P>
												<P><asp:imagebutton id="imgbtnDel" runat="server" AlternateText="<" ImageUrl="../Images/Arrow-crop.gif"
														ToolTip="Delete violations From category"></asp:imagebutton></P>
												<P>&nbsp;</P>
											</TD>
											<TD vAlign="top" align="left" width="40%"><asp:listbox id="listAssigned" runat="server" CssClass="clstextarea" Width="328px" Rows="14"
													SelectionMode="Multiple" Height="272px"></asp:listbox></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="left" width="442" colSpan="3"></TD>
											<TD vAlign="top" align="center" width="51"></TD>
											<TD vAlign="top" align="right"></TD>
										</TR>
										<TR>
											<TD class="frmtd" align="center" ><asp:label id="lblMessage" runat="server" Width="175px" ForeColor="Red"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD background="../../images/separator_repeat.gif" height="11"></TD>
							</TR>
							<TR>
								<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom
{
    public partial class ViolationList : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                        Response.End();
                    }
                    else //To stop page further execution
                    {
                         
                        imgAdd.Attributes.Add("onclick", "return AddViol();");
                        imgEdit.Attributes.Add("onclick", "return EditViol();");
                        imgMoveDown.Attributes.Add("onclick", "return MoveDown();");
                        imgMoveUp.Attributes.Add("onclick", "return MoveUp();");
                        imgRemove.Attributes.Add("onclick", "return RemoveViol();");
                        lnkInsertSeparator.Attributes.Add("onclick", "return AddSeparator();");
                        btnUpdateViolation.Attributes.Add("onclick","return Validation();");
                        lstViolations.Attributes.Add("onclick","return SetValues();");
                        
                        //btnCancelViol.Attributes.Add("onclick","return Clear();");                        
                        this.btnUpdateViolation.Enabled = false;
                        this.btnCancelViol.Enabled = false;
                        if (!IsPostBack)
                        {
                            

                            Fill_Violations();
                            Fill_Category();
                        }
                    }
                }
                txtEditClicked.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        private void Fill_Category()
        {
            string[] key1 =  { "@CategoryId" };
            object[] value1 = { 0 };
            DataSet ds = ClsDb.Get_DS_BySPArr  ("usp_HTS_GetAllCategories",key1, value1);
            ddlCategory.DataTextField = ds.Tables[0].Columns["categoryname"].ToString();
            ddlCategory.DataValueField = ds.Tables[0].Columns["categoryid"].ToString();
            ddlCategory.DataSource = ds;
            ddlCategory.DataBind();
            ListItem lst = new ListItem("--Choose--", "0");
            ddlCategory.Items.Insert(0, lst);
        }

        private void Fill_Violations()
        {
            DataSet ds =   ClsDb.Get_DS_BySP("USP_HTS_Get_ViolationList");
            lstViolations.DataTextField = ds.Tables[0].Columns["description"].ToString();
            lstViolations.DataValueField = ds.Tables[0].Columns["violationnumber_pk"].ToString();
            lstViolations.DataSource = ds;
            lstViolations.DataBind();

            ddlPriview.DataTextField = ds.Tables[0].Columns["description"].ToString();
            ddlPriview.DataValueField = ds.Tables[0].Columns["violationnumber_pk"].ToString();
            ddlPriview.DataSource = ds;
            ddlPriview.DataBind();
            ddlPriview.Items.Insert(0, "---Choose---");
        }

        protected void btnUpdateViolation_Click(object sender, EventArgs e)
        {
            try
            {
                string[] key1 =  { "@ViolationNumber_pk", "@Description","@ShortDesc", "@Sequence" , "@Categoryid"};
                object[] value1 = { Convert.ToInt32(txtViolId.Text), txtViolation.Text,txtShortDesc.Text, Convert.ToInt32(txtSequence.Text) , Convert.ToInt32(ddlCategory.SelectedValue)};
                ClsDb.ExecuteSP("USP_HTS_Insert_VioaltionList", key1, value1);
                Fill_Violations();
                Clear_Values();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void imgMoveUp_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (lstViolations.SelectedIndex == 0)
                    return;

                int currIndex = lstViolations.SelectedIndex;
                string currText = lstViolations.Items[lstViolations.SelectedIndex].Text;
                int currValue = Convert.ToInt32(lstViolations.SelectedValue);

                int NewIndex = currIndex - 1;
                string NewText = lstViolations.Items[NewIndex].Text;
                int NewValue = Convert.ToInt32(lstViolations.Items[NewIndex].Value);

                string tempText = lstViolations.Items[NewIndex].Text;
                int tempValue = Convert.ToInt32(lstViolations.Items[NewIndex].Value);

                lstViolations.Items[NewIndex].Text = currText;
                lstViolations.Items[NewIndex].Value = currValue.ToString();

                lstViolations.Items[currIndex].Text = tempText;
                lstViolations.Items[currIndex].Value = tempValue.ToString();
                lstViolations.SelectedIndex = NewIndex;

                Update_Sequence(currValue, NewValue, currIndex, NewIndex);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }


        }

        protected void imgMoveDown_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (lstViolations.SelectedIndex == lstViolations.Items.Count - 1)
                    return;

                int currIndex = lstViolations.SelectedIndex;
                string currText = lstViolations.Items[lstViolations.SelectedIndex].Text;
                int currValue = Convert.ToInt32(lstViolations.SelectedValue);

                int NewIndex = currIndex + 1;
                string NewText = lstViolations.Items[NewIndex].Text;
                int NewValue = Convert.ToInt32(lstViolations.Items[NewIndex].Value);

                string tempText = lstViolations.Items[NewIndex].Text;
                int tempValue = Convert.ToInt32(lstViolations.Items[NewIndex].Value);

                lstViolations.Items[NewIndex].Text = currText;
                lstViolations.Items[NewIndex].Value = currValue.ToString();

                lstViolations.Items[currIndex].Text = tempText;
                lstViolations.Items[currIndex].Value = tempValue.ToString();
                lstViolations.SelectedIndex = NewIndex;
                Update_Sequence(currValue, NewValue, currIndex, NewIndex);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
            
        }
        private void Update_Sequence(int currValue, int NewValue, int currIndex, int NewIndex)
        {
            string[] key1 = { "@CurrValue", "@NewValue", "@currindex","@newindex"};
            object[] value1 = { currValue, NewValue, currIndex, NewIndex};
            ClsDb.ExecuteSP("USP_HTS_Update_ViolationSequence", key1, value1);
            Fill_Violations();
            //Clear_Values();

        }

        protected void imgRemove_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string[] key1 = { "@Violid"};
                object[] value1 = { Convert.ToInt32(txtViolId.Text)};

                ClsDb.ExecuteSP("USP_HTS_Delete_ViolationList", key1, value1);
                Fill_Violations();
                Clear_Values();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        protected void btnCancelViol_Click(object sender, EventArgs e)
        {
            //this.btnUpdateViolation.Enabled = false;
            //this.btnCancelViol.Enabled = false;           
            Clear_Values();
        }
        private void Clear_Values()
        {
            txtViolId.Text = "";
            txtViolation.Text = "";
            lstViolations.SelectedIndex = -1;
            ddlCategory.SelectedIndex = 0;
            txtShortDesc.Text = "";

        }

        protected void imgEdit_Click(object sender, ImageClickEventArgs e)
        {        
            
        }

        protected void lnkInsertSeparator_Click(object sender, EventArgs e)
        {
            try
            {
                string[] key1 =  { "@ViolationNumber_pk", "@Description", "@ShortDesc", "@Sequence", "@Categoryid" };
                object[] value1 = { 0, txtViolation.Text,txtShortDesc.Text ,Convert.ToInt32(txtSequence.Text), 21 };
                ClsDb.ExecuteSP("USP_HTS_Insert_VioaltionList", key1, value1);
                Fill_Violations();
                Clear_Values();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void txtViolation_TextChanged(object sender, EventArgs e)
        {

        }

        protected void lstViolations_SelectedIndexChanged(object sender, EventArgs e)
        {
           // this.imgEdit_Click(sender,new ImageClickEventArgs(0,0));

            int result;

            // Noufil 3504 03/24/2008 replace convert.toint with int.tryparse
            try
            {
                if (int.TryParse(txtViolId.Text, out result))
                {
                    string[] key1 = { "@ViolId" };
                    object[] value1 = { Convert.ToInt32(txtViolId.Text) };
                    DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_SelectedViolation", key1, value1);
                    txtViolId.Text = ds.Tables[0].Rows[0]["violationnumber_pk"].ToString();
                    txtViolation.Text = ds.Tables[0].Rows[0]["description"].ToString();
                    ddlCategory.SelectedValue = ds.Tables[0].Rows[0]["categoryid"].ToString();
                    txtShortDesc.Text = ds.Tables[0].Rows[0]["ShortDesc"].ToString();
                    txtEditClicked.Text = "1";
                    this.btnUpdateViolation.Enabled = true;
                    this.btnCancelViol.Enabled = true;
                }
                else
                {
                    lblMessage.Text = "Cannot Find VOilent Id";
                }

            }


            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

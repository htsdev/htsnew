<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmBatchTrialLetter.aspx.cs" Inherits="lntechNew.backroom.frmBatchTrialLetter" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Trial Letter Batch Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="HEIGHT: 27px" colSpan="2"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD colSpan="2"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 35px" colSpan="2">
						<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR background="../images/separator_repeat.gif">
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="tblSearchCriteria" cellSpacing="1" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" style="width: 12%"><font color="#3366cc"><STRONG> Date From:</STRONG></font>
								</TD>
								<TD class="clsLeftPaddingTable" style="width: 137px"><ew:calendarpopup id="cal_todate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
								<TD class="clsLeftPaddingTable" style="width: 62px"><font color="#3366cc"><STRONG>To:</STRONG></font>
								</TD>
								<TD class="clsleftpaddingtable" style="width: 176px"><ew:calendarpopup id="cal_fromDate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
															
								<TD class="clsleftpaddingtable"><asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click"></asp:button></TD>
								<td class="clsleftpaddingtable" align="right">
                                    <asp:HyperLink ID="hyperlink2" NavigateUrl="~/backroom/frmEmailedTrialLetter.aspx"  runat="server">Trial Letter Email Report</asp:HyperLink></td>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD background="../Images/subhead_bg.gif" height ="34px" ><IMG height="34" src="../Images/subhead_bg.gif" style="width: 1px"></TD>
				</TR>
				<tr>
					<td class="clsleftpaddingtable" align="right"><asp:label id="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" Font-Bold="True"
							ForeColor="#3366cc" Height="8px" Font-Names="Tahoma">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Width="9px" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
							Height="10px" Font-Names="Tahoma">a</asp:label>&nbsp;<asp:label id="lblGoto" runat="server" Width="16px" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
							Height="7px" Font-Names="Tahoma">Goto</asp:label>&nbsp;
						<asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" Font-Bold="True" ForeColor="#3366cc"
							CssClass="clinputcombo" AutoPostBack="True" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<TR>
					<td align="center"><asp:label id="lbl_Msg" runat="server" ForeColor="Red" CssClass="normalfont" Font-Bold="True"></asp:label></td>
				<tr>
					<td></td>
				</tr>
				<TR>
					<TD id="grid" vAlign="top" colSpan="2"><asp:datagrid id="DG_TrialLetterBatch" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
							PageSize="20" AllowPaging="True" ShowFooter="True" OnItemDataBound="DG_TrialLetterBatch_ItemDataBound" OnPageIndexChanged="DG_TrialLetterBatch_PageIndexChanged" OnSortCommand="DG_TrialLetterBatch_SortCommand">
							<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
							<FooterStyle CssClass="GrdFooter"></FooterStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Sno">
									<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id="hp_sno" runat="server"></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>								
								<asp:TemplateColumn HeaderText="Ticket No">
									<HeaderStyle HorizontalAlign="Left" Width="19%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id="HLTicketno" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumber") %>' Height="17px" Target="_self">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="ClientName" HeaderText="Name">
									<HeaderStyle HorizontalAlign="Left" CssClass="clssubhead" Width="25%"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbl_clientname" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientName") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>	
								<asp:TemplateColumn HeaderText="Email">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:HyperLink id="HLCauseno" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>' Height="17px" Target="_self">
										</asp:HyperLink>
									</ItemTemplate>									
								</asp:TemplateColumn>							
								<asp:TemplateColumn SortExpression="CourtStatus" HeaderText="Status">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbl_midnum" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtStatus") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>															
								<asp:TemplateColumn SortExpression="CourtLoc" HeaderText="Court">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbl_courtloc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtLoc") %>' cssclass="label">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="courtdatemain" HeaderText="Court Date">
									<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbl_courtdate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:d}") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="courtdatemain" HeaderText="Court Time">
									<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbl_courttime" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:t}") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="BatchDate" HeaderText="Batch Date">
									<HeaderStyle HorizontalAlign="Left" Width="5%" CssClass="clssubhead"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbl_emaildate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BatchDate","{0:d}") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								
							</Columns>
							<PagerStyle NextPageText=" Next &amp;gt;" Font-Size="XX-Small" PrevPageText="&amp;lt; Previous "
								HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colSpan="2">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR background="../images/separator_repeat.gif">
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
    </div>
    </form>
</body>
</html>

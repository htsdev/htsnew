<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViolationList.aspx.cs" Inherits="lntechNew.backroom.ViolationList" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head >
    <title>Manage Violations</title>
    <LINK href="../Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
    
            function CheckDescripkAction()
            {
                //var vFlag = document.form1.txtEditClicked.value;
//                if (vFlag = '1')
//               {
                document.form1.btnUpdateViolation.disabled=false;
                document.form1.btnCancelViol.disabled=false;
//                }
//                else 
//                {
//                document.form1.btnUpdateViolation.disabled=true;
//                document.form1.btnCancelViol.disabled=true;
//                }
            }
                      
            
           /*  function EnableControl(obj)
            {               
                obj.disabled=false;               
            }*/
            
        function whichElement(e)
        { //a;
            var ev ;
            if (!e)
                 ev = window.event;
            else 
                ev = e;
            var target = ev.target;
            var button = ev.button;
            var iLeft = ev.offsetX + 130;
            var iTop = ev.offsetY + 300;
            
             if (ev.target)
                target = ev.target;
            else if (e.srcElement)
                target = e.srcElement;
            
            
            if (target.id == "lstViolations" && button == "2")
            { 
                ev.cancel;
                document.getElementById("rMenu").style.left = iLeft;
                document.getElementById("rMenu").style.top = iTop;
                document.getElementById("rMenu").style.display = 'block';
            }
            else
            {
                document.getElementById("rMenu").style.display = 'none';
            }
        }



 
    
    function ccp()
    {
        document.getElementById("tbl_plzwait").style.display = 'block';
    }
    
    function start()
    {
    document.getElementById("tbl_plzwait").style.display = 'none';
    }

    
        function SetValues()
        {   
            var violid = document.getElementById("txtViolId");
            var seq = document.getElementById("txtSequence");
            violid.value = form1.lstViolations.options[form1.lstViolations.selectedIndex].value;
            seq.value = form1.lstViolations.selectedIndex;
            return false;
        }

        function AddViol()
        {  
            if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the location\n\rwhere you want to add new violation.");
                form1.lstViolations.focus();
                start();
                return false;
            }

            form1.txtViolation.focus();
            form1.txtViolId.value = "0";
            form1.txtSequence.value = form1.lstViolations.selectedIndex;
            CheckDescripkAction();
            ChangeValuesInTextBoxes();
            return false;            
        }      
        function ChangeValuesInTextBoxes()
        {
            form1.ddlCategory.selectedIndex = 0;
            form1.txtShortDesc.value="";          
            form1.txtViolation.value ="";           
            form1.txtViolId.value="0";
            return false;
        }
              
        function MoveDown()
        {  
            
            if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the violation\n\rthat you want to re-arrange.");
                form1.lstViolations.focus();
                start();
                return false;
            }
            
            if (form1.lstViolations.selectedIndex == form1.lstViolations.length - 1 )
                {
                start();
                return false;
                }
        }
        
        function MoveUp()
        {  
            if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the violation\n\rthat you want to re-arrange.");
                form1.lstViolations.focus();
                start();
                return false;
            }

            if (form1.lstViolations.selectedIndex == 0)
                {
                start();
                return false;
                }
        }

        function RemoveViol()
        {
           if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the violation\n\rthat you want to remove.");
                form1.lstViolations.focus();
                start();
                return false;
            }
            
            var doyou = confirm("Are you sure you want to remove the selected violation?\n\rTo yes click [OK], for No click [CANCEL].");
            if (doyou== true)
             {
                form1.txtViolId.value = form1.lstViolations.options[form1.lstViolations.selectedIndex].value;
                CheckDescripkAction();
             }
             else
             {
                start();
                return false;
             }
           
        }


        function EditViol()
        {
           if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the violation\n\rthat you want to edit.");
                form1.lstViolations.focus();
                //start();
                return false;
            }            
            form1.txtViolation.focus(); 
            start();   
            return false;
           // CheckDescripkAction()        
        }
        
            
        function AddSeparator()
        { 
           if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the location\n\rwhere you want to add separator.");
                form1.lstViolations.focus();
                start();
                return false;
            }
            var doyou = confirm("Are you sure you want to add a separator below the selected violation?\n\rFor yes click [OK], for No click [CANCEL].");
            if (doyou== true)
            {
                form1.txtViolation.value = "-------------------------";
                form1.txtViolId.value = "0";
                form1.txtSequence.value = form1.lstViolations.selectedIndex;
            }
            else
            {
                start();
                return false;
            }

        }
        
    
    function Validation()
    { 
            if (form1.lstViolations.selectedIndex == -1)
            {
                alert("Please first select the location where you want to add new violation\n\ror select a violation that you want to edit.");
                form1.lstViolations.focus();
                start();
                return false;
            }
    
            var desc = form1.txtViolation.value;
            if (desc.length == 0)
            {
                alert("Please enter violation description.");
                form1.txtViolation.focus();
                start();
                return false;
            }

           if (form1.ddlCategory.selectedIndex == -1  || form1.ddlCategory.selectedIndex == 0 )
            {
                alert("Please first select the violation category.");
                form1.ddlCategory.focus();
                start();
                return false;
            }            
    }    
          
        
    </script>
    

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
</head>
<body id="ViolationList" onload="start();">
    <form id="form1" runat="server">
            <table align="center" id="tblMain" cellpadding="0" cellspacing="0" style="width: 780px" border="0">
                <tr>
                    <td style="width: 100%">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" background="../images/separator_repeat.gif"  height="11">
                    </td>
                </tr>
                                
				<tr>
					<td background="../images/headbar_headerextend.gif"  height="5"></td>
				</tr>
				<tr>
					<td class="clssubhead" height=34  background="../Images/subhead_bg.gif"
						 align="left" >&nbsp;Violations</td>
					
				</tr>
                <tr>
                    <td style="width: 100%" align="left">
                    <aspnew:ScriptManager ID="ScriptManager1" runat="server"></aspnew:ScriptManager>
                    <aspnew:UpdatePanel ID="pnl_Violations" runat="server"  UpdateMode="Conditional">
										<ContentTemplate>
                        <table id="tblViolations" border="0" cellpadding="0" cellspacing="0" width="100%" >
                            <tr>
                                <td align="left" class="clsleftpaddingtable" style="width: 50%">
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="imgAdd" runat="server" AlternateText="Add New" ImageUrl="../Images/add.gif"
                                        ToolTip="Add New Violation"/>
                                    <asp:ImageButton ID="imgEdit" runat="server" AlternateText="Edit Violation" ImageUrl="~/Images/edit.gif"
                                        ToolTip="Edit Selected Violation" OnClick="imgEdit_Click" OnClientClick="ccp();" />
                                    <asp:ImageButton ID="imgRemove" runat="server" AlternateText="Remove Violation" ImageUrl="~/Images/cross.gif"
                                        ToolTip="Remove Selected Violation" OnClick="imgRemove_Click" OnClientClick="ccp();" /></td>
                                <td width="5%" class="clsleftpaddingtable" >
                                    &nbsp;</td>
                                <td class="clsleftpaddingtable" width="45%">
                                    </td>
                            </tr>
                            <tr>
                                <td style="border-top: gray 1px solid; border-left: gray 1px solid; border-right: gray 1px solid; border-bottom: gray 1px solid; width: 50%; height: 248px;"  class="clsleftpaddingtable" valign="top">
                                    <asp:ListBox ID="lstViolations" runat="server" Height="320px" Width="400px" CssClass="clsinputcombo" OnSelectedIndexChanged="lstViolations_SelectedIndexChanged" AutoPostBack="True"></asp:ListBox>
                                    
                                    </td>
                                <td width = "5%" align="left" style=" border-top: gray 1px solid; border-bottom: gray 1px solid; height: 248px;" class="clsleftpaddingtable" >
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                       
                                        <tr>
                                            <td style="width: 100%">
                                               <asp:ImageButton ID="imgMoveUp" runat="server" AlternateText="Move Up" ImageUrl="~/Images/MoveUp.gif"
                                                    ToolTip="Move Selected Violation Up one level" Width="30px" OnClick="imgMoveUp_Click" OnClientClick="ccp();" /></td>
                                        </tr>
                                        <tr>
                                            <td >
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <asp:ImageButton ID="imgMoveDown" runat="server" AlternateText="Move Down"
                                                    ImageUrl="~/Images/Movedown.gif" ToolTip="Move Selected Violation Down one level" Width="30px" OnClick="imgMoveDown_Click" OnClientClick="ccp();" /></td>
                                        </tr>
                                       
                                    </table>
                                </td>
                                <td width="45%" align="left" valign="top" style="border-top: gray 1px solid; border-left: gray 1px solid; border-bottom: gray 1px solid; border-right: gray 1px solid; height: 248px;" id="tdAddEdit" class="clsleftpaddingtable">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblAddEdit">
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td style="height: 16px"><p><strong>&nbsp;Add/Edit Violation</strong></p></td></tr>
                                        <tr>
                                            <td style="width: 100%; height: 42px;">
                                               <table border="0" style="width: 100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="20%" class="clsleftpaddingtable" style="height: 22px"><asp:Label ID="Label2" runat="server" Text="Description"></asp:Label></td>
                                                        <td class="clsleftpaddingtable" style="height: 22px; width: 57%;"><asp:TextBox ID="txtViolation" runat="server" CssClass="clsinputadministration" Width="150px" MaxLength="100" OnTextChanged="txtViolation_TextChanged"></asp:TextBox></td>
                                                        <td width="20%" class="clsleftpaddingtable" style="height: 22px"><asp:LinkButton ID="lnkInsertSeparator" runat="server" OnClick="lnkInsertSeparator_Click" OnClientClick="ccp();">Separator</asp:LinkButton>&nbsp;</td>
                                                    </tr>
                                                   <tr>
                                                       <td class="clsleftpaddingtable" style="height: 22px" width="20%">
                                    <asp:Label ID="Label3" runat="server" Text="Short Desc"></asp:Label></td>
                                                       <td class="clsleftpaddingtable" style="width: 57%; height: 22px">
                                    <asp:TextBox ID="txtShortDesc" runat="server" Width="150px" CssClass="clsinputadministration" MaxLength="20"></asp:TextBox></td>
                                                       <td class="clsleftpaddingtable" style="height: 22px" width="20%">
                                                       </td>
                                                   </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%;">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td style="width: 20%; height: 25px;" class="clsleftpaddingtable"><asp:Label ID="Label1" runat="server" Text="Category"></asp:Label></td>
                                                        <td style="width: 60%; height: 25px;" class="clsleftpaddingtable"><asp:DropDownList ID="ddlCategory" runat="server" CssClass="clsinputcombo" Width="150px"></asp:DropDownList></td>
                                                        <td style="width: 20%; height: 25px;" class="clsleftpaddingtable">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="width: 100%;" valign="top">
                                                
                                                <table border="0" style="width: 100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right" style="width: 50%; height: 13px;" class="clsleftpaddingtable">&nbsp;</td>
                                                        <td align="right" style="width: 50%; height: 13px;" class="clsleftpaddingtable">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 50%" class="clsleftpaddingtable"><asp:Button ID="btnUpdateViolation" runat="server" CssClass="clsbutton" Text="Update" ToolTip="Update Violation" OnClick="btnUpdateViolation_Click" OnClientClick="ccp();" Enabled="False" />
                                                        <asp:Button ID="btnCancelViol" runat="server" CssClass="clsbutton" Text="Cancel" OnClick="btnCancelViol_Click" OnClientClick="ccp();" Enabled="False" />
                                                        </td>
                                                        <%--<td align="left" style="width: 50%" class="clsleftpaddingtable" ><asp:Button ID="btnCancelViol" runat="server" CssClass="clsbutton" Text="Cancel" OnClick="btnCancelViol_Click" OnClientClick="ccp();" Enabled="False" /></td>--%>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td style="height: 16px"><p><strong>&nbsp;Preview Violations</strong></p></td></tr>
                                        <tr><td>
                                            &nbsp;<asp:DropDownList ID="ddlPriview" runat="server" CssClass="clsinputcombo" Width="218px">
                                            </asp:DropDownList></td></tr>
                                        
                                    </table>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" width="100%" >
                                    <div id="tbl_plzwait" runat="server" style="display :none" class="clssubhead">
	    								<img src="../Images/plzwait.gif" />Update in progress....</div>
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
    				    </ContentTemplate>
					    <Triggers>
					        <aspnew:AsyncPostBackTrigger ControlID="imgMoveUp" EventName="Click" />
                            <aspnew:AsyncPostBackTrigger ControlID="imgMoveUp" EventName="Click" />
                            <aspnew:AsyncPostBackTrigger ControlID="imgRemove" EventName="Click" />
                            <aspnew:AsyncPostBackTrigger ControlID="btnUpdateViolation" EventName="Click" />
                            <aspnew:AsyncPostBackTrigger ControlID="btnCancelViol" EventName="Click" />
                            <aspnew:AsyncPostBackTrigger ControlID="imgEdit" EventName="Click" />
                            <aspnew:AsyncPostBackTrigger ControlID="lnkInsertSeparator" EventName="Click" />
					    </Triggers>
				    </aspnew:UpdatePanel>
                   </td>
                </tr>
                <tr>
                    <td style="width: 100%; visibility: hidden;" align="left">
                        &nbsp;<asp:TextBox ID="txtViolId" runat="server" Width="57px"></asp:TextBox><asp:TextBox ID="txtSequence" runat="server" Width="51px"></asp:TextBox>
                        <asp:TextBox ID="txtIsChanged" runat="server">0</asp:TextBox>
                        <asp:TextBox ID="txtEditClicked" runat="server" Width="56px">1</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 100%" align="left">
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
               
            </table>
    </form>
</body>
</html>

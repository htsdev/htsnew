<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.WebForm1" Codebehind="PrincingMain.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Price Settings</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		
		function validate()
		{
									
			//a;
			var d1 = document.getElementById("cal_EffectiveFrom").value
			var d2 = document.getElementById("cal_EffectiveTo").value			
			
			if (d1 == d2)
				return true;
			var d3 = document.getElementById("cal_EndFrom").value
			var d4 = document.getElementById("cal_EndTo").value			
			
			if (d3 == d4)
				return true;
			
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false || compareDates(d4,'MM/dd/yyyy',d3,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("cal_EffectiveTo").focus(); 
					return false;
				}
				return true;
		}

		function ActiveDate(calobj)
		{
		var objdt = document.getElementById(calobj);
			alert(objdt.value.length);
			if ((objdt.value.length == 2) || (objdt.value.length ==null))
			{
				calobj.value = calobj.value & "/" ;
			}
		}
		
		function validation()
		{
		var ctrlEffFrom = document.getElementById("cal_EffectiveFrom");
		var ctrlEffTo = document.getElementById("cal_EffectiveTo");
		
		var ctrlEndFrom = document.getElementById("cal_EndFrom");
		var ctrlEndTo = document.getElementById("cal_EndTo");
		
		dtEffFrom = new Date(ctrlEffFrom.value);
		dtEffTo = new Date(ctrlEffTo.value);
		dtEndFrom = new Date(ctrlEndFrom.value);
		dtEndTo = new Date(ctrlEndTo.value);
		
		var ddlEffFrom = document.getElementById("ddl_EffectiveFrom");
		var ddlEffTo = document.getElementById("ddl_EffectiveTo");
		
		var ddlEndFrom = document.getElementById("ddl_EndFrom");
		var ddlEndTo = document.getElementById("ddl_EndTo");
		
		var bol = true; 
		
		bol = chkDateCreatria(dtEffFrom,dtEffTo,ddlEffTo.value,ddlEffFrom.value);
		if (bol == false)
		{
			return false;
		}
	
		bol = chkDateCreatria(dtEndFrom,dtEndTo,ddlEndTo.value,ddlEndFrom.value);
		if (bol == false)
		{
			return false;
		}
		
		bol = chkDateCreatria(dtEffTo,dtEndTo,ddlEndTo.value,ddlEffTo.value);
		if (bol == false)
		{
			return false;
		}
		
	return bol;
		
		}
		
		function chkDateCreatria(dtFrom,dtTo,ddlTo,ddlFrom)
		{
		
		if ((dtTo == "NaN" ) || (dtFrom == "NaN"))
		{
		alert("Select Date Range");
			 
			return false;
		}
		else if ((ddlTo == "00:00" ) || (ddlFrom == "00:00"))
		{
		alert("Select Time Range");
			 
			return false;
		}
		else if ((dtTo <= dtFrom))
		{	
			
			if ((dtTo < dtFrom))
			{
			alert("Invalid Date Range");
			 
			return false;
			}	
			else if(dtFrom <= dtTo)
			{
				if (ddlTo <= ddlFrom)
					{
					
					if (ddlTo < ddlFrom)
					{
						alert("Invalid Time Range");
						return false;
					}
					else if(ddlTo <= ddlFrom)
					{
					var timeFrom = ddlFrom;
					var lnTimeFrom = timeFrom.length - 3;
					var IndexTimeFrom = timeFrom.indexOf(':');
				
					addHFrom = timeFrom.substring(0,lnTimeFrom);
					addMFrom = timeFrom.substring(IndexTimeFrom +1,timeFrom.length);
				
				
					var timeTo = ddlTo;
					var lnTimeTo = timeTo.length - 3;
					var IndexTimeTo = timeTo.indexOf(':');
		
					addHTo = timeTo.substring(0,lnTimeTo);
					addMTo = timeTo.substring(IndexTimeTo+1,timeTo.length);
				
					
					if (addMTo <= addMFrom)
						{
							if (addMTo < addMFrom)
							{
							alert("Invalid Time Range");
							return false;
							}
							else if (addMTo <= addMFrom || addMTo == addMFrom )
							{
							
							return true;
							}
							
					
						}
					}}
				}
			else
			{
			return false;
			}
		}
		}
		
		
		</script>
        
		

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        
		
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" width=780 cellSpacing="0" cellPadding="0"
				align="center" border="0">
				<tbody>
				<tr>
						<tr>
						<td style="HEIGHT: 14px;" >
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
						</td>
					</tr>
	
				<tr>
					<td background="../../images/separator_repeat.gif"  style="height: 11px; "></td>
				</tr>
				<TR>
					<TD  >
						<TABLE class="clsLeftPaddingTable" id="Table2" cellSpacing="0" cellPadding="0"
							border="0" width=100%>
							<TR>
								<TD class="clsLeftPaddingTable" style="width: 292px; height: 7px" >Effective Date & Time From:
									</td>
									<td style="width: 165px; height: 7px"><ew:calendarpopup id="cal_EffectiveFrom" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
										ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 196px; height: 7px;"><asp:dropdownlist id="ddl_EffectiveFrom" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 7px">To:</td>
								<td style="width: 179px; height: 7px"><ew:calendarpopup id="cal_EffectiveTo" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
										CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 155px; height: 7px;"><asp:dropdownlist id="ddl_EffectiveTo" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 292px">
								End Date & Time From:</td>
								<td style="width: 165px">
									<ew:calendarpopup id="cal_EndFrom" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
										ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 196px"><asp:dropdownlist id="ddl_EndFrom" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable">To:</td>
								<td style="width: 179px"><ew:calendarpopup id="cal_EndTo" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False"
										ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 155px"><asp:dropdownlist id="ddl_EndTo" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist>
							</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 292px">Court 
									Location</td>
									<td style="width: 165px"><asp:dropdownlist id="ddl_crtloc" runat="server" Width="145px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 196px" ><asp:checkbox id="Chkb_activeplan" runat="server" Text="Show All Active Plans" Width="127px" cssclass="clsLeftPaddingTable"></asp:checkbox></TD>
								<TD class="clsLeftPaddingTable" align="right" >&nbsp;</TD>
								<TD class="clsLeftPaddingTable" align="right" style="width: 179px" >&nbsp;</TD>
								<td class="clsLeftPaddingTable" align="right" style="width: 155px"><asp:button id="btn_SearchPricing" runat="server" Text="Search Pricing Plan " CssClass="clsbutton" Width="149px"></asp:button></td>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td background="../../images/headbar_headerextend.gif" ></td>
				</tr>
				<tr>
				    <td>
				        <table cellSpacing="0" cellPadding="0" border = "0" width=100%>
						    <tr>
				            	<td class="clssubhead" background="../../Images/subhead_bg.gif" width=80% height=34>&nbsp;Plans
				            	</td>
					            <td class="clssubhead" align="right" background="../../Images/subhead_bg.gif" ><A href="frmAddPricePlan.aspx">Add New Plan</A>&nbsp;
					            </td>
				            </tr>
				        </table>
				    </td>
				</tr>
				
				<TR>
					<td >
						<table cellSpacing="0" width=100% cellPadding="0" border="0" >
							<tr>
								<TD align="center" ><asp:datagrid id="dgViolInfo" runat="server" Width="100%" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Description">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:HyperLink id=lnk_plandesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlanDescription") %>'>
													</asp:HyperLink>
													<asp:Label id=lbl_hidPlanID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Short Name">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_ShortName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlanShortName") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Effective Date">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_effectiveDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.effectiveDate", "{0: MM/dd/yyyy hh:mm tt}") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="End Date">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_EndDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.endDate", "{0: MM/dd/yyyy hh:mm tt}") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Location">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_courtloc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Status">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblStatus runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsActivePlan") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
				</tbody>
			</TABLE>
			</form>
	</body>
	
	
</HTML>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for FrmDelCategory.
	/// </summary>
	public partial class FrmDelCategory : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lbl_addcategory;
		protected System.Web.UI.WebControls.Label lblMessage;
		
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		clsViolationCategory ClsViolationCategory=new clsViolationCategory();
		protected System.Web.UI.WebControls.Label txt_CategoryName;
		protected System.Web.UI.WebControls.Label txt_CategoryDescription;
		protected System.Web.UI.WebControls.Label Label1;
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
				}
				if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
				{
					Response.Redirect("../LoginAccesserror.aspx",false);
					Response.End();
				}
				if(! IsPostBack)
				{
					
					if (Request.QueryString.Count!=0 )
					{
						int intCategoryId = Convert.ToInt32(Request.QueryString["CategoryID"].ToString().Trim ());
						if (intCategoryId>0)
						{
					
							string[] key    = {"@CategoryID","@RetStatus"};
				
							object[] value1 = {intCategoryId,0};
			
							object retObj = ClsDb.InsertBySPArrRet("USP_HTS_Delete_ViolationsCategory",key,value1);
							int IretObj = Convert.ToInt16(retObj);

							if (IretObj == 1)
							{
								lblMessage.Text = "Category Name '"+ txt_CategoryName.Text + "' is deleted";
								HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
							}
							else if (IretObj == 0)
							{
								//lblMessage.ForeColor=System.Drawing.Color.Blue;
								lblMessage.Text = "Category Name '"+ txt_CategoryName.Text + "' cannot be deleted.It contain Violations.";
							
							}

						}
					}	
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		

		
		}
	}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.backroom
{
	/// <summary>
	/// Summary description for AddViolationCategory.
	/// </summary>
	public partial class AddViolationCategory : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ImageButton imgbtnAdd;
		protected System.Web.UI.WebControls.ImageButton imgbtnDel;
		protected System.Web.UI.WebControls.Label lblMessage;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		//DataSet ds_assigned;
		protected System.Web.UI.WebControls.DropDownList ddlCategory;
		//DataSet ds_unassigned;
		clsViolationCategory ClsViolationCategory=new clsViolationCategory();
		protected System.Web.UI.WebControls.ListBox listUnassigned;
		protected System.Web.UI.WebControls.ListBox listAssigned;
		clsViolations ClsViolation = new clsViolations();
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
	
	


		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else
					{
						imgbtnAdd.Attributes.Add("OnClick","return isUnassignedSelected();"); 
						imgbtnDel.Attributes.Add("OnClick","return isAssignedSelected();"); 


						if (!IsPostBack)
						{
							GetCategory();
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		public void GetCategory()
		{
			try
			{
				DataSet ds = ClsViolationCategory.GetCategory();
				ddlCategory.DataSource  = ds;
			
				ddlCategory.DataValueField = ds.Tables[0].Columns[0].ColumnName; // value=CategoryID
				ddlCategory.DataTextField = ds.Tables[0].Columns[1].ColumnName;	 // Text=CategoryName	
				ddlCategory.DataBind();
			
				//First item would be "--Choose--"
				ListItem listitem = new ListItem("--Choose--","-1");
				ddlCategory.Items.Add(listitem);
				ddlCategory.SelectedValue = "-1";
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlCategory.SelectedIndexChanged += new System.EventHandler(this.ddlCategory_SelectedIndexChanged);
			this.imgbtnAdd.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtnAdd_Click);
			this.imgbtnDel.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtnDel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void imgbtnAdd_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				if (listUnassigned.SelectedIndex > -1 )
				{
					string strViolationIDs=""; //e.g, "2,5,7," violationIds to be assigned to Category

					foreach(ListItem li in listUnassigned.Items )
					{
						if(li.Selected==true)
						{
							strViolationIDs += li.Value + "," ;
						}
					}

					int intCategoryID = Convert.ToInt32(ddlCategory.SelectedValue); //update unassigned Violations to intCategoryID
					ClsViolation.UpdateViolationToCategoryID(intCategoryID,strViolationIDs); 
					FillListBox(); // fill list Box to refresh

				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void imgbtnDel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				if (listAssigned.SelectedIndex > -1 )
				{
					string strViolationIDs=""; //e.g, "2,5,7," violationIds to be Unassigned from Category

					foreach(ListItem li in listAssigned.Items )
					{
						if(li.Selected==true)
						{
							strViolationIDs += li.Value + "," ;
						}
					}

					ClsViolation.UpdateViolationToCategoryID(0,strViolationIDs); 
					FillListBox(); // fill list Box to refresh

				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void imgbtnSave_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
		
		}

		private void ddlCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillListBox();
		}

		private void FillListBox()
		{
			try
			{
				int intCategoryID = ((int)Convert.ChangeType(ddlCategory.SelectedItem.Value,typeof(int)));
				DataSet ds =  ClsViolation.GetViolationsByCategoryID(intCategoryID);
				listAssigned.DataSource = ds;
				listAssigned.DataValueField = ds.Tables[0].Columns[0].ColumnName;	//ViolationNumber_PK
				listAssigned.DataTextField = ds.Tables[0].Columns[1].ColumnName;	//Description
				listAssigned.DataBind();


				DataSet ds1 =  ClsViolation.GetViolationsByCategoryID(0);			 //0 means unassigned
				listUnassigned.DataSource = ds1;
				listUnassigned.DataValueField = ds1.Tables[0].Columns[0].ColumnName; //ViolationNumber_PK
				listUnassigned.DataTextField = ds1.Tables[0].Columns[1].ColumnName;  //Description
				listUnassigned.DataBind();

			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		
	}
}

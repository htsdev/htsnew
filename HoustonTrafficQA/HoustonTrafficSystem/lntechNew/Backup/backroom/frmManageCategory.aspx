<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmManageCategory.aspx.cs" Inherits="lntechNew.backroom.frmManageCategory" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Category </title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
    function addItem()
	{	
		
		 if ( document.form1.lstassigned.length == 1 )
		 {
		  document.form1.addtext.value = "";
		 }
		 	
		 if(document.getElementById("ddl_reportcategory").selectedIndex == 0)
		  {
		    alert("Please select Report Category. ");
		    document.getElementById("ddl_reportcategory").focus();
		    return false;
		  }  
		  
		if (document.form1.lstunassigned.selectedIndex == -1)
		{
		 alert("Please select any Report for Unassigned.");
		 document.getElementById("lstunassigned").focus();
		 return false;
		//document.form1.lstunassigned.selectedIndex != 0 && 
//		var txt = document.form1.lstunassigned[document.form1.lstunassigned.selectedIndex].text;
//		var val = document.form1.lstunassigned[document.form1.lstunassigned.selectedIndex].value;		
//		document.form1.lstassigned.options[document.form1.lstassigned.options.length] = new Option(txt, val);
//		document.form1.lstunassigned.options[document.form1.lstunassigned.selectedIndex]=null;				
//		var adtxt=txt+",";
//		var advalue=val+",";
//		document.form1.addtext.value+=adtxt;
//		document.form1.addvalue.value+=advalue;					
		}		
		
	}
	
	function removeItem()
	{		
	    
	     if(document.getElementById("ddl_reportcategory").selectedIndex == 0)
		   {
		    alert("Please select Report Category. ");
		    document.getElementById("ddl_reportcategory").focus();
		    return false;
		   }  
		    	
	    if (document.form1.lstassigned.selectedIndex == -1)
	     {
	       alert("Please select any report for Assigned to Category");
	       document.getElementById("lstassigned").focus();
	       return false; 
	      //&& document.form1.lstassigned.selectedIndex != 0	     
//		    var txt = document.form1.lstassigned[document.form1.lstassigned.selectedIndex].text
//		    var val = document.form1.lstassigned[document.form1.lstassigned.selectedIndex].value		
//		    document.form1.lstunassigned.options[document.form1.lstunassigned.options.length] = new Option(txt, val);
//		    document.form1.lstassigned.options[document.form1.lstassigned.selectedIndex]=null
//		    var Unassigned=val+",";	
//		    document.form1.addUnassignedvalue.value +=Unassigned;    		 
	    }	
	   
	
	}
    </script>
    

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 9px" align="left" background="../../images/separator_repeat.gif"></TD>
								</TR>                                
                                <tr>
                                    <td class="clsleftpaddingtable" align="center">
                                                 <asp:DropDownList ID="ddl_reportcategory" runat="server" Width="372px" AutoPostBack="True" OnSelectedIndexChanged="ddl_reportcategory_SelectedIndexChanged">
                                                 </asp:DropDownList></td>
                                </tr>
								
								<tr>
								  <td class="clsleftpaddingtable" style="height:22px;" align="right" valign="top">
                                      <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/backroom/frmReportCategory.aspx">Go Back to Report Category</asp:HyperLink>
                                      &nbsp;
								  </td>
								</tr>
								<TR>
									<TD class="clsleftpaddingtable" style="HEIGHT: 14px">
                                     
                                     <table class="clsleftpaddingtable" cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                        <tr>
                                            <td align="center" class="clsleftpaddingtable" valign="top" style="width:50%;height:20px; font-weight:bold;">
                                                Un Assigned</td>
                                                <td style="width:10%" align="left"> 
                                             </td>
                                            <td align="center" class="clsleftpaddingtable" valign="top" style="width:50%; height:20px; font-weight:bold;">
                                                Assigned
                                            </td>
                                        </tr>
                                         <tr>
                                             <td class="clsleftpaddingtable" style="width: 40%">
                                                 <asp:ListBox ID="lstunassigned" runat="server" CssClass="clsinputcombo"
                                                     Height="400px" Width="400px">
                                                 </asp:ListBox></td>
                                             <td class="clsleftpaddingtable" style="width:10%"  align="left"> 
                                             
                                                <table class="clsleftpaddingtable" cellpadding="0" cellspacing="0" border="0" align="left" style="height: 100px;  ">
                                                    <tr>
                                                        <td>                                                           
                                                            <asp:ImageButton ID="imgbtn_unassigned" runat="server" AlternateText="Move Up" ImageUrl="~/Images/Leftarrow.gif"
                                                                ToolTip="Move Selected  Up one level"
                                                                Width="30px" OnClick="imgbtn_unassigned_Click" />
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="imgbtn_assigned" runat="server" AlternateText="Move Up" ImageUrl="~/Images/rightArrow-crop.gif"
                                                                ToolTip="Move Selected  Up one level"
                                                                Width="30px" BackColor="White" OnClick="imgbtn_assigned_Click" />
                                                        </td>
                                                    </tr>
                                                  </table>
                                             
                                             </td>
                                             
                                             
                                             <td style="width: 100%" align="left">
                                                 <asp:ListBox ID="lstassigned" runat="server" CssClass="clsinputcombo"
                                                     Height="400px" Width="400px">
                                                 </asp:ListBox></td>
                                         </tr>
                                     </table>
                                                                             
                                    </TD>
								</TR>
								<TR>
									<td class="clsleftpaddingtable" vAlign="top" width="100%" style="height: 15px" align="right">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="clsbutton"
                                            Text="Submit" OnClick="btnsubmit_Click" Visible="False" />
                                        &nbsp; &nbsp;
                                     </td>
								</TR>
								<tr>
                                    <td class="clsleftpaddingtable" align="left" background="../../images/separator_repeat.gif" style="height: 9px">
                                        <input name="addtext" type="hidden" />
                                        <input name="addvalue" type="hidden" />
                                        <input name="addUnassignedvalue" type="hidden" />
                                        </td>
                                </tr>
								<TR>
									<TD align="center" width="800" style="height: 13px"><asp:label id="lblmsg" runat="server" CssClass="cmdlinks" ForeColor="Red" Visible="False" Font-Bold="True"></asp:label></TD>
								</TR>
							</TBODY>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
    </div>
    </form>
</body>
</html>

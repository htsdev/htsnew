using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.backroom
{
    public partial class frmReportCategory : System.Web.UI.Page
    {
        clsLogger clog = new clsLogger();      
        Components.ReportCategory cat = new lntechNew.Components.ReportCategory();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                btnsubmit.Attributes.Add("OnClick","return Validate();");
                btndelete.Attributes.Add("OnClick", "return DeletCategoryFunction();");

                if (!IsPostBack)
                {
                    GetCategory();
                    lblmsg.Text = "";
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void GetCategory()
        {
            try
            {
                DataSet ds = cat.GetCategory();
                
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GV_Category.DataSource = ds;
                        GV_Category.DataBind();
                        GenrateSerial();
                    }
                    else
                    {
                        GV_Category.DataSource = ds;
                        GV_Category.DataBind();
                    }
                   
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                lblmsg.Text = "";
                cat.CategoryName = txtcategory.Text;

                if (cat.InsertCategory() == false)
                    lblmsg.Text ="Could not Insert Category Name " + txtcategory.Text;
               
                GetCategory();
                txtcategory.Text = "";
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                lblmsg.Text = "";
                cat.CategoryID = Convert.ToInt32(hf_categoryid.Value);
                DataSet ds = cat.DeleteCategory();

                if (ds.Tables[0].Rows[0][0].ToString() == "0")
                {
                    lblmsg.Text = "Could not Delete Category " + txtcategory.Text + " It is being used by Reports";
                    lblmsg.Visible = true;
                }
                
                GetCategory();
                txtcategory.Text = "";
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
        }

        private void GenrateSerial()
        {
            try
            {
                int sno =0;

                foreach (GridViewRow ItemX in  GV_Category.Rows)
                {
                    Label lblsno = (Label)ItemX.FindControl("lbl_sno");

                    //if (lblsno.Text != "")
                    sno++;
                    lblsno.Text = sno.ToString(); ;
                }
            }
            catch(Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void GV_Category_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Footer))
                {
                    HiddenField hfcategory = (HiddenField)e.Row.FindControl("hfcategoryname");

                    HyperLink hlcategory = (HyperLink)e.Row.FindControl("hl_categoryname");
                    if(hlcategory != null)
                        hlcategory.Attributes.Add("OnClick", "SetCategoryName('"+hlcategory.Text +"','"+hfcategory.Value+"');");
                }
            }
            catch (Exception ex)
            {
               lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
                
    }
}

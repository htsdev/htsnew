﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using System.Data.SqlClient;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
    // Noufil 4338 07/10/2008 Get and update Case Status
    public partial class Casetype : System.Web.UI.Page
    {
        clsUser cUser = new clsUser();
        CaseType ctype = new CaseType();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnl_casetype.Style[HtmlTextWriterStyle.Display] = "none";
                bindgrid();
            }
        }

        public void bindgrid()
        {            
            pnl_casetype.Style[HtmlTextWriterStyle.Display] = "none";
            ctype.CaseTypeId = -1;
            dd_Ename.DataSource = cUser.getAllUsersList();            
            dd_Ename.DataBind();            
            //Get All case type
            DataTable dt = ctype.GetCaseTypeInfo();
            gv_records.DataSource = dt;
            gv_records.DataBind();
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "click")
            {
                // show modal popup and set case type values
                modalpopup1.Show();
                pnl_casetype.Style[HtmlTextWriterStyle.Display] = "block";
                ctype.CaseTypeId = Convert.ToInt32(e.CommandArgument);
                ViewState["casetypeid"] = ctype.CaseTypeId;
                DataTable dtt = ctype.GetCaseTypeInfo();
                txt_name.Text = dtt.Rows[0]["CaseTypeName"].ToString();
                dd_Ename.SelectedValue = dtt.Rows[0]["ServiceTicketEmpId"].ToString();
                txt_openmail.Text = dtt.Rows[0]["ServiceTicketEmailAlert"].ToString();               
                txt_closemail.Text = dtt.Rows[0]["ServiceTicketEmailClose"].ToString();
                txt_angryopenemail.Text = dtt.Rows[0]["ServiceTicketAngryEmailOpen"].ToString();
                txt_angrycloseemail.Text = dtt.Rows[0]["ServiceTicketAngryEmailClose"].ToString();
                txt_UpdateEmail.Text = dtt.Rows[0]["ServiceTicketUpdateEmail"].ToString();
                if (txt_openmail.Text.Contains("N/A") == true) { txt_openmail.Text = ""; }
                if (txt_closemail.Text.Contains("N/A") == true) { txt_closemail.Text = ""; }
                if (txt_angryopenemail.Text.Contains("N/A") == true) { txt_angryopenemail.Text = ""; }
                if (txt_angrycloseemail.Text.Contains("N/A") == true) { txt_angrycloseemail.Text = ""; }
                // Zahoor 4757 09/09/2008 for Update Email
                if (txt_UpdateEmail.Text.Contains("N/A") == true) { txt_UpdateEmail.Text = ""; }
            }
        }

        protected void btn_update_Click(object sender, EventArgs e)
        {
            //update case type values
            ctype.CaseTypeName = txt_name.Text;
            ctype.CaseTypeId = Convert.ToInt32(ViewState["casetypeid"].ToString());
            ctype.ServiceTicketEmpId = Convert.ToInt32(dd_Ename.SelectedValue.ToString());
            ctype.ServiceTicketEmailAlert=txt_openmail.Text;
            ctype.ServiceTicketEmailClose= txt_closemail.Text ;
            ctype.ServiceTicketAngryEmailOpen=txt_angryopenemail.Text;
            ctype.ServiceTicketAngryEmailClose=txt_angrycloseemail.Text;
            // Zahoor 4757 09/09/2008 for Update Email
            ctype.ServiceTicketUpdateEmail = txt_UpdateEmail.Text;
            ctype.UpdateCaseType();            
            bindgrid();
        }

    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PSReport2.aspx.cs" Inherits="lntechNew.backroom.PSReport2" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Public Site Visitors Report 2</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <TABLE id="TableMain"
				cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 816px" colSpan="4">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="10"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 46px" colSpan="4">
									<TABLE id="Table2" style="WIDTH: 784px; HEIGHT: 14px" cellSpacing="1" cellPadding="1" width="784"
										border="0">
										<TR>
											<TD class="clsLeftPaddingTable" style="WIDTH: 238px; HEIGHT: 23px"><STRONG>Search By Date</STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 76px; HEIGHT: 23px"></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 178px; HEIGHT: 23px"></TD>
											<TD class="clsLeftPaddingTable" style="HEIGHT: 23px" align="right">
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Publicsitevisitors.aspx?sMenu=111">Public Site Visitors</asp:HyperLink></TD>
										</TR>
										<TR>
											<TD class="clsLeftPaddingTable" style="WIDTH: 238px"><STRONG>Start Date</STRONG>&nbsp;:&nbsp;
												<ew:calendarpopup id="calstartdate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
													ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
													ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Width="99px" Font-Size="8pt" Font-Names="Tahoma">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 76px"><STRONG>End Date:</STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 178px"><ew:calendarpopup id="calenddate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True" ShowClearDate="True"
													Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
													Width="102px" Font-Size="8pt" Font-Names="Tahoma">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable"><asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSearch_Click"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 11px" align="left" background="../Images/separator_repeat.gif" colSpan="4">&nbsp;
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="4"><asp:datagrid id="dgresult" runat="server" Width="784px" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
										BorderColor="White" BorderStyle="None" OnItemDataBound="dgresult_ItemDataBound">
										<Columns>
											<asp:TemplateColumn HeaderText="S.No">
												<HeaderStyle HorizontalAlign="Left" Width="25px" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno" runat="server" CssClass="label"></asp:Label>
                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' />
                                                      <asp:HiddenField ID="hf_activeflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ActiveFlag") %>' />
												</ItemTemplate>
											</asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ticket No.">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtn_clientname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:LinkButton>
                                                    <asp:Label ID="lbltktno" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="180px" CssClass="clsaspcolumnheader" />                                                
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Client Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcname" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientName") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="180px" CssClass="clsaspcolumnheader" /> 
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Email">
                                                <ItemTemplate>
                                                    <asp:Label id="lblemail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserEmail") %>' CssClass="clslabel"></asp:Label>
                                                </ItemTemplate>            
                                                <HeaderStyle HorizontalAlign="Center" Width="120px" CssClass="clsaspcolumnheader" />                                    
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Website quote">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblwquote" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SiteQuote","{0:C0}") %>' CssClass="clslabel"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"  CssClass="clsaspcolumnheader" />                                                
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Call in quote">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcallquote" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.callquote","{0:C0}") %>' CssClass="clslabel"></asp:Label>
                                                </ItemTemplate>                
                                                <HeaderStyle HorizontalAlign="Center"  CssClass="clsaspcolumnheader" />                                
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="VCount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltviolation" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VCount") %>' CssClass="clslabel"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"  CssClass="clsaspcolumnheader" />                                                
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="FTA">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FTA") %>' CssClass="clslabel"></asp:Label>
                                                </ItemTemplate>          
                                                <HeaderStyle HorizontalAlign="Center"  CssClass="clsaspcolumnheader" />                                                                                      
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Hire Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhirestatus" runat="server"  CssClass="label"></asp:Label>
                                                    <br />
                                                    <asp:HiddenField ID="hf_status" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.processflag") %>' />
                                                </ItemTemplate>                 
                                                <HeaderStyle HorizontalAlign="Center" Width="125px" CssClass="clsaspcolumnheader" />                               
                                            </asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%" colSpan="4" height="11">
									<asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="4">
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
    </div>
    </form>
</body>
</html>

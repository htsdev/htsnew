<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidationEmail.aspx.cs"
    Inherits="HTP.backroom.ValidationEmail" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ValidationEmail</title>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
    
        
        function ValidationEmail()
		{		
		var email = form1.tb_emailaddress;		
		if (  email.value != "")
		{
		    if( isEmail(email.value)== false)
			{
			    alert ("Please enter Email Address in Correct format.");
			    email.focus(); 
			    return false;			   
			}		
		}		
		
		 var doyou = confirm("Are you sure you want to run Validation Email Report (OK = Yes   Cancel = No)"); 
         if (doyou == true)
            {return true;}         
         return false;			         
		}
		
		
		function showbobbittAddress()
		{		
		    document.form1.tb_emailaddress.value='<%= ViewState["ValidationEmailOthers"] %>';
		}
		
		
		function ShowWait()
		{		
		    document.getElementById('tr_PleaesWait').style.display = "none";    //Saeed 6255 07/14/2010 hide please wait message.
		    document.getElementById("tdWait").style.display = ""
		    document.getElementById("tdcontrols").style.display = "none"
		    return true;
		}
		//Saeed 6255 07/14/2010 method use to display 'please wait..' message when click on Send email button.
		function ShowProggress()
		{
		    document.getElementById('tr_PleaesWait').style.display = "";
		    return true;
		}
		
		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <%--<asp:ScriptManager ID="smValidationEmail" runat="server" AsyncPostBackTimeout="900"></asp:ScriptManager>     --%>
    <aspnew:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="0" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js"  Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    <aspnew:UpdatePanel ID="upValidationEmail" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                                            
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                        
                            <tr>
                                <td style="background:url(../images/separator_repeat.gif) repeat;"  colspan="7" height="11">                                
                                </td>
                            </tr>
                            <tr>
                                <td style="background:url(../Images/subhead_bg.gif) repeat;"    height="34" class="clssubhead" align="right">
                                    <asp:CheckBox ID="chkDetail" Text="Detail" runat="server" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr id="tr_PleaesWait" style="display:none" >
                        <td align="center">
                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upValidationEmail">
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lblWait" runat="server" Text="Please wait... while system process the request."
                                        CssClass="clsLabel"></asp:Label>
                                </ProgressTemplate>
                       </aspnew:UpdateProgress>
                       </td></tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" id="tdcontrols" runat="server">
                                    <table style="width: 100%;">
                                        <tr>                                        
                                            <td align="left" class="clslabel" style="width: 72%">
                                                <asp:RadioButton ID="rb_demail" runat="server" Checked="True" GroupName="Email" Text="hvalidation@sullolaw.com" />&nbsp;
                                                <asp:RadioButton ID="rb_other" runat="server" GroupName="Email" Text="Others" onclick="showbobbittAddress()" />
                                                <asp:TextBox ID="tb_emailaddress" runat="server" CssClass="clsInputadministration"
                                                    Width="250px"></asp:TextBox>
                                                <asp:Button ID="btn_email" runat="server" CssClass="clsbutton" OnClientClick="return ShowProggress();" OnClick="LinkButton1_Click"
                                                    Text="Send Email" />
                                            </td>
                                            <td align="left" style="width: 28%" valign="middle">
                                                <asp:Button ID="btn_displayreport" runat="server" CssClass="clsbutton" OnClick="btn_displayreport_Click"
                                                    Text="Generate Validation  Report" OnClientClick="return ShowWait();" Width="205px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="background:url(../images/separator_repeat.gif) repeat;" style="height: 12px">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_result" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td align="center" class="clssubhead" id="tdWait" runat="server" style="display: none">                                
                                    Please wait...<br /><br />
                                    <img id="imgProgressbar" runat="server" src="../Images/Progressbar.gif" alt="Please wait.."  />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" id="tdReport" runat="server" style="display:inline">
                                    <asp:Panel ID="pnl_report" runat="server" Height="600px" ScrollBars="Both" Visible="False"
                                        Width="100%" Wrap="False">
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td background="../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:Footer ID="Footer1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>                    
                    </ContentTemplate>
                    </aspnew:UpdatePanel>                    
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.FrmAddNewCategory" Codebehind="FrmAddNewCategory.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >



<HTML>
	<HEAD>
		<title>Add New Category</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
		
		function MaxLength()
		{
		if(document.getElementById("txt_CategoryDescription").value.length >100)
		{
		alert("The length of character is not in the specified Range");
		document.getElementById("txt_CategoryDescription").focus();
		}
		}
		 function ValidateLenght()
	     { 
			var cmts = document.getElementById("txt_CategoryDescription").value;
			if (cmts.length > 100)
			{
			    event.returnValue=false;
                event.cancel = true;
                
			}
	    }
	    
		function saveClick()
		{
			var alert01 = "Please enter CategoryName to save";
			var txt_CategoryName = document.getElementById("txt_CategoryName");
			if(txt_CategoryName.value=="")
			{
				alert(alert01);
				return false;
			}
			else
			{
				return true;
			}
		}
	   function closewin()
	    {
			opener.location.reload();
			self.close();	   
	    }	    
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; LEFT: 64px; WIDTH: 357px; POSITION: absolute; TOP: 16px; HEIGHT: 128px"
				cellSpacing="0" cellPadding="0" width="357" align="center" border="0">
				<TR>
					<TD class="clsLeftPaddingTable" vAlign="top">
						<asp:label id="lbl_addcategory" Runat="server" CssClass="clssubhead" BackColor="#EEC75E">Add/Edit Category</asp:label></TD>
				</TR>
				<TR>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="tblsub" height="20" cellSpacing="1" cellPadding="0" width="350" align="center"
							border="0">
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<P><STRONG>Category Name</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<asp:textbox id="txt_CategoryName" runat="server" CssClass="clsinputadministration" MaxLength="100"
										Width="276px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<P><STRONG>Short Description</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<asp:textbox id="txt_shortdesc" runat="server" CssClass="clsinputadministration" MaxLength="10"
										Width="276px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 31px" vAlign="top">
									<P><STRONG>Category Description</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 31px" vAlign="top">
									<asp:textbox id="txt_CategoryDescription" runat="server" CssClass="clsinputadministration" MaxLength="200"
										Width="276px" TextMode="MultiLine" onkeypress="ValidateLenght();"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top" align="right" colSpan="2">
									<asp:button id="btn_submit" runat="server" CssClass="clsbutton" Width="87px" Text="Save" OnClientClick ="MaxLength();" ></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<asp:label id="lblMessage" runat="server" Width="272px" Font-Size="XX-Small" Font-Names="Verdana"
							ForeColor="Red" Font-Bold="True"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

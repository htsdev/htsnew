﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="changeEmpPassword.aspx.cs"
    Inherits="lntechDallasNew.backroom.changeEmpPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Login Password</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function CheckName(name)
    {       
        for ( i = 0 ; i < name.length ; i++)
        {
            var asciicode =  name.charCodeAt(i)
            //If not valid alphabet 
            if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
            return false;
       }
        return true;
    }
    
    function Checkpasswordlenght()
    {
    
        var Plenght= document.getElementById("txt_password").value.length;
        var nPlenght= document.getElementById("txt_newpassword").value.length;
        var nPvalue= document.getElementById("txt_newpassword").value;
        var cvalue= document.getElementById("txt_confrimpasswrod").value;
        
        if (Plenght ==0)
        {
            alert ("Please enter all Fields");
            document.getElementById("txt_password").focus();
            return false;
        }
        
        if (nPlenght ==0)
        {
            alert ("Please enter all Fields");
            document.getElementById("txt_newpassword").focus();
            return false;
        }
        
        if (document.getElementById("txt_confrimpasswrod").value.length==0)
        {
            alert ("Please enter all Fields");
            document.getElementById("txt_confrimpasswrod").focus();
            return false;
        }
        
        if (Plenght < 4 || Plenght > 20)
        {
            alert ("Please enter old password greater then 3 and less then 20 digits");
            document.getElementById("txt_password").focus();
            return false;
        }
        
        if (nPlenght < 4 || nPlenght > 20)
        {
            alert ("Please enter new password greater then 3 and less then 20 digits");
            document.getElementById("txt_newpassword").focus();
            return false;
        }
        
        if (nPvalue != cvalue)
        {
            alert ("Both passwords are not same");
            document.getElementById("txt_confrimpasswrod").focus();
            return false;            
        }
        
        if (CheckName(document.getElementById("txt_password").value)==false)
        {
            alert ("Old password must only contain alphabets and numbers");
            document.getElementById("txt_password").focus();
            return false;
        }
        
        if (CheckName(nPvalue)==false)
        {
            alert ("New password must only contain alphabets and numbers");
            document.getElementById("txt_newpassword").focus();
            return false;
        }
        
        if (CheckName(cvalue)==false)
        {
            alert ("Confrim new password must only contain alphabets and numbers");
            document.getElementById("txt_confrimpasswrod").focus();
            return false;
        }
        
        
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101">
            <tbody>
                <tr>
                    <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="left">
                        &nbsp;Change Password
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="right">
                                    <span class="clssubhead">Old Password&nbsp;</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_password" runat="server" CssClass="clsInputadministration" Width="150px"
                                        Text="" TextMode="Password" MaxLength="20"></asp:TextBox>
                                </td>
                                <td>
                                    <span style="color: Red">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <span class="clssubhead">New Password&nbsp;</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_newpassword" runat="server" CssClass="clsInputadministration"
                                        Width="150px" Text="" TextMode="Password" MaxLength="20"></asp:TextBox>
                                </td>
                                <td>
                                    <span style="color: Red">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <span class="clssubhead">Confrim New Password&nbsp;</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_confrimpasswrod" runat="server" CssClass="clsInputadministration"
                                        Width="150px" Text="" TextMode="Password" MaxLength="20"></asp:TextBox>
                                </td>
                                <td>
                                    <span style="color: Red">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <asp:Button runat="server" ID="btn_Submit" Text="Submit" CssClass="clsbutton" ValidationGroup="RFV"
                                        OnClick="btn_Submit_Click" OnClientClick="return Checkpasswordlenght();" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="5" height="11">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

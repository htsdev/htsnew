<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.frmAddPricePlan" Codebehind="frmAddPricePlan.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>frmAddPricePlan</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script language="javascript">

		function ValidateInput()
		{	
			var error = "Please enter correct value.Only float and numeric value are allowed";
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_PlanDetail";
			var idx=2;
			var bol = true;
			//alert("asli");
			
			for (idx=2; idx < (cnt+2); idx++)
			{
//				var ctlBasePrice = grdName+ "__ctl" + idx + "_tb_BasePrice";
//				var ctlsecprice = grdName+ "__ctl" + idx + "_tb_secprice";
//				var ctlbasepcent = grdName+ "__ctl" + idx + "_tb_basepcent";
//				var ctlsecpcent = grdName+ "__ctl" + idx + "_tb_secpcent";
//				var ctlbondbase = grdName+ "__ctl" + idx + "_tb_bondbase";
//				var ctlbondsec = grdName+ "__ctl" + idx + "_tb_bondsec";
//				var ctlbondbasepcent = grdName+ "__ctl" + idx + "_tb_bondbasepcent";
//				var ctlbondsecpcent = grdName+ "__ctl" + idx + "_tb_bondsecpcent";
//				var ctlbondassump = grdName+ "__ctl" + idx + "_tb_bondassump";
//				var ctlfineassump = grdName+ "__ctl" + idx + "_tb_fineassump";
//				

				/* testing for changing color
				txt_BasePrice.runtimeStyle.backgroundColor
				txt_BasePrice.currentStyle.color
					color	"#123160"	Variant
				txt_BasePrice.style.backgroundColor
				*/
				var ctlBasePrice = ""; 
				var ctlsecprice = "";
				var ctlbasepcent = "";
				var ctlsecpcent = "";
				var ctlbondbase = "";
				var ctlbondsec = "";
				var ctlbondbasepcent = "";
				var ctlbondsecpcent = "";
				var ctlbondassump = "";
				var ctlfineassump = "";
				
				if( idx < 10)
				{
				 ctlBasePrice = grdName+ "_ctl0" + idx + "_tb_BasePrice";
				 ctlsecprice = grdName+ "_ctl0" + idx + "_tb_secprice";
				 ctlbasepcent = grdName+ "_ctl0" + idx + "_tb_basepcent";
				 ctlsecpcent = grdName+ "_ctl0" + idx + "_tb_secpcent";
				 ctlbondbase = grdName+ "_ctl0" + idx + "_tb_bondbase";
				 ctlbondsec = grdName+ "_ctl0" + idx + "_tb_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl0" + idx + "_tb_bondbasepcent";
				 ctlbondsecpcent = grdName+ "_ctl0" + idx + "_tb_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl0" + idx + "_tb_bondassump";
				 ctlfineassump = grdName+ "_ctl0" + idx + "_tb_fineassump";
				
				
				}
				else
				{
				 ctlBasePrice = grdName+ "_ctl" + idx + "_tb_BasePrice";
				 ctlsecprice = grdName+ "_ctl" + idx + "_tb_secprice";
				 ctlbasepcent = grdName+ "_ctl" + idx + "_tb_basepcent";
				 ctlsecpcent = grdName+ "_ctl" + idx + "_tb_secpcent";
				 ctlbondbase = grdName+ "_ctl" + idx + "_tb_bondbase";
				 ctlbondsec = grdName+ "_ctl" + idx + "_tb_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl" + idx + "_tb_bondbasepcent";
				 ctlbondsecpcent = grdName+ "_ctl" + idx + "_tb_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl" + idx + "_tb_bondassump";
				 ctlfineassump = grdName+ "_ctl" + idx + "_tb_fineassump";
				
				
				}
				
				var txt_BasePrice = document.getElementById(ctlBasePrice);	
				var txt_secprice =document.getElementById(ctlsecprice);	
				var txt_basepcent = document.getElementById(ctlbasepcent);	
				var txt_secpcent = document.getElementById(ctlsecpcent);	
				var txt_bondbase =document.getElementById(ctlbondbase);	
				var txt_bondsec =document.getElementById(ctlbondsec);	
				var txt_bondbasepcent = document.getElementById(ctlbondbasepcent);	
				var txt_bondsecpcent = document.getElementById(ctlbondsecpcent);	
				var txt_bondassump = document.getElementById(ctlbondassump);	
				var txt_fineassump =document.getElementById(ctlfineassump);	
				
				bol = true;
				
				bol = ValidationFloat(txt_BasePrice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secprice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_basepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbase);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsec);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbasepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsecpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_fineassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}
			}
			
			
				bol = NullValidation();
				if (bol==false)
				{
					
					return false;
				}
				
				bol = NullValidationShort();
				if (bol==false)
				{
					
					return false;
				}
				
				bol = ddlValidation();
				if (bol==false)
				{
					
					return false;
				}
				
				var calfrm = "calFrom";
				bol = calValidation(calfrm);
				if (bol==false)
				{
					
					return false;
				}
				calT = "calTo";
				bol = calValidation(calT);
				if (bol==false)
				{
					
					return false;
				}
			return bol;
		}
		
		function ValidationFloat(TextBox)
		{
				//alert(TextBox.value);
				//var error = "Please correct value. Only float value is allowed";
				var lblMessage = document.getElementById("lblMessage");

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
					if (isFloat(TextBox.value)==false) 
					{
						//if -ve and not float
						//lblMessage.innerText = error;
						TextBox.focus();
						
						return false;
					}
					else
					{
						lblMessage.innerText = "";
						return true;
					}
				}
				
		}
		
		function NullValidation()
		{
			
			var lblMessage = document.getElementById("lblMessage");

			var txt = document.getElementById("txtPlanName");
			
			
			if (txt.value == "")
				{		
						var error = "Please enter Plan Name";
						//lblMessage.innerText = error;
						alert(error);
						txt.focus();
						
						return false;
				}
			else
				{
						lblMessage.innerText = "";
						return true;
				}
			}
			
			function NullValidationShort()
		{
			
			var lblMessage = document.getElementById("lblMessage");

			var txt = document.getElementById("txt_ShortName");
			
			
			if (txt.value == "")
				{		
						var error = "Please enter Plan Short Name";
						//lblMessage.innerText = error;
						alert(error);
						txt.focus();
						
						return false;
				}
			else
				{
						lblMessage.innerText = "";
						return true;
				}
			}
			function ddlValidation()	
			{
			var lblMessage = document.getElementById("lblMessage");

			var ddlcrtloc = document.getElementById("ddl_crtloc");
			if (ddlcrtloc.selectedIndex == "0")
			
			{	
			//alert(crtloc.value);
			var error = "Please select Court Location";
			//lblMessage.innerText = error;
			alert(error);
			ddlcrtloc.focus();
						
			return false;
		}
		else
		{
			lblMessage.innerText = "";
			return true;
		} 
		}
		function calValidation(cal)
		{
			
			var lblMessage = document.getElementById("lblMessage");

			var txt = document.getElementById(cal);
			
			
			if (txt.value == "")
				{		
						var error = "Please enter Date";
						//lblMessage.innerText = error;
						alert(error);
						txt.focus();
						
						return false;
				}
			else
				{
						lblMessage.innerText = "";
						return true;
				}
			}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<TR>
					<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="TableHeader" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 10px" colSpan="1" rowSpan="1"><STRONG></STRONG></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 10px" colSpan="1">&nbsp;</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px; HEIGHT: 10px"></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 10px" align="right" colSpan="3"><asp:hyperlink id="HyperLink1" runat="server" NavigateUrl="PrincingMain.aspx">Back to Pricing Main</asp:hyperlink></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 18px">Name</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 18px"><asp:textbox id="txtPlanName" runat="server" Width="176px" CssClass="clsinputadministration"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px"></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 133px" colSpan="2"></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 14px">Start&nbsp;Date</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 14px"><ew:calendarpopup id="calFrom" runat="server" Width="80px" EnableHideDropDown="True" Font-Size="8pt"
										Font-Names="Tahoma" ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup><asp:dropdownlist id="ddl_TimeFrom" runat="server" Width="70px" CssClass="clsinputcombo"></asp:dropdownlist><textboxlabelstyle cssclass="clstextarea"></TEXTBOXLABELSTYLE><WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></WEEKDAYSTYLE><MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></MONTHHEADERSTYLE><OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
										BackColor="AntiqueWhite"></OFFMONTHSTYLE><GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></GOTOTODAYSTYLE><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGoldenrodYellow"></TODAYDAYSTYLE><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Orange"></DAYHEADERSTYLE><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGray"></WEEKENDSTYLE><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></SELECTEDDATESTYLE><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></CLEARDATESTYLE><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></HOLIDAYSTYLE></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px; HEIGHT: 14px">Court Location</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 144px; HEIGHT: 14px"><TEXTBOXLABELSTYLE CssClass="clstextarea"><asp:dropdownlist id="ddl_crtloc" runat="server" CssClass="clsinputcombo"></asp:dropdownlist>
									</TEXTBOXLABELSTYLE><WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></WEEKDAYSTYLE><MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></MONTHHEADERSTYLE><OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
										BackColor="AntiqueWhite"></OFFMONTHSTYLE><GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></GOTOTODAYSTYLE><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGoldenrodYellow"></TODAYDAYSTYLE><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Orange"></DAYHEADERSTYLE><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="LightGray"></WEEKENDSTYLE><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="Yellow"></SELECTEDDATESTYLE><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></CLEARDATESTYLE><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
										BackColor="White"></HOLIDAYSTYLE></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 14px" align="right"><asp:checkbox id="chkb_savetemplate" runat="server" CssClass="clslabelnew" Text="Save As Template"></asp:checkbox>&nbsp;</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px">End Date</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px"><ew:calendarpopup id="calTo" runat="server" Width="80px" EnableHideDropDown="True" Font-Size="8pt"
										Font-Names="Tahoma" ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup><asp:dropdownlist id="ddl_TimeTo" runat="server" Width="70px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px"></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 144px"><asp:radiobutton id="rdo_Active" runat="server" CssClass="clslabelnew" Text="Active" GroupName="Planstat"></asp:radiobutton><asp:radiobutton id="rdo_Inactive" runat="server" CssClass="clslabelnew" Text="Inactive" GroupName="Planstat"></asp:radiobutton></TD>
								<TD class="clsLeftPaddingTable" align="right"><asp:button id="btn_Save" runat="server" CssClass="clsbutton" Text="Save Changes"></asp:button>&nbsp;</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 75px; HEIGHT: 18px">Short Name</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 172px; HEIGHT: 18px"><asp:textbox id="txt_ShortName" runat="server" Width="176px" CssClass="clsinputadministration"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 83px"></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 133px" colSpan="2"></TD>
							</TR>
						</TABLE>
						<asp:label id="lblMessage" runat="server" Width="320px" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<TR>
					<TD background="../../images/headbar_headerextend.gif" height="5"></TD>
				</TR>
				<TR>
					<TD  class="clssubhead" background="../../Images/subhead_bg.gif" height="34px" style="width: 883px">&nbsp;
						<asp:label id="Label1" runat="server" Width="72px" CssClass="clssubhead" BackColor="#EEC75E"
							BorderColor="Transparent">Plan Name</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:dropdownlist id="ddl_templetes" runat="server" Width="152px" CssClass="clsinputcombo" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD  class="clssubhead" background="../../Images/subhead_bg.gif" height="34px" style="width: 883px"></TD>
				</TR>
				<TR>
					<TD background="../../images/headbar_footerextend.gif" height="10"></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="tblgrid" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TR>
								<TD align="center"><asp:datagrid id="dg_PlanDetail" runat="server" Width="780px" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Violation Category">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_voilcategory runat="server" CssClass="label" Width="116px" Text='<%# DataBinder.Eval(Container, "DataItem.categoryname") %>'>
													</asp:Label>
													<asp:Label id=lbl_hidCategoryID runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_baseprice runat="server" CssClass="clsinputadministration" Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.BasePrice", "{0:F}") %>'>
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_secprice runat="server" CssClass="clsinputadministration" Width="45px" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPrice", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_basepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_secpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondbase runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBase", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondsec runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondary", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondbasepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondsecpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_bondassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondAssumption", "{0:F}")%>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Fine Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tb_fineassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.FineAssumption", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="right"></TD>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif"  height="11"></TD>
				</TR>
				<TR>
					<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
			<div><INPUT id="txtHidden" type="hidden" runat="server"><asp:label id="test" runat="server" Height="8px" Visible="False">Label</asp:label></div>
		</form>
	</body>
</HTML>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" src="~/WebControls/ActiveMenu.ascx"%>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.SqlSrvMrg.StoredProceduresList" Codebehind="StoredProceduresList.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="~/WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Stored Procedure List</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">

		// Enabling and Disabling Dg_Output(datagrid) Controls(Combobox and textbox) 
		function ShowHide(d1,t1,d2)
		{
			
			var ddl1 = document.getElementById(d1 );
			var ddl2 = document.getElementById(d2 );
			var txt = document.getElementById(t1 );

			if (ddl1.selectedIndex == 0)
				{
				ddl2.disabled = true;
				txt.disabled = true;
				}
			else
				{
				ddl2.disabled = false;
				txt.disabled = false;
				}

		}

		// Hide the table	
		function ShowHidePageNavigation()
		{
		
			var tbl =document.getElementById("tblhide").style;
			var grd = document.getElementById("Dg_Output");
			var grd2 = document.getElementById("DgSqlProcedure");
			var msg = document.getElementById("txtNoParam") ;
			var msg2 = document.getElementById("txtchk") ;
		   if ( msg.value == '1' || msg2.value == '1')
			{
		   		tbl.display='none';		// means hidden  table  
		   	}
		   else
			{
				tbl.display ='block';	// show table
			}
		 }

			// Validating the Controls
    	 function DoValidate()
		{
			var cnt = (document.Form1.TextBox2.value) * 1 ;
			cnt= cnt + 2;
			
			var grdName = "Dg_Output";
			var idx=2;
		
			var reportname = document.getElementById("txtreportname");
			if (reportname.value == "")
			{
					alert("Please type Report name where you want to save parameters field.");
					return false;
			} 
			for (idx=2; idx < cnt; idx++)
			{
				var ctlName = "";
				var ctext = "";
				var calias = "";
				
				if(idx < 10)
				{
				 ctlName = grdName+ "_ctl0" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl0" + idx + "_txtCmd";
				 calias = grdName+ "_ctl0" + idx + "_txtParamAlias";
				}
				else
				{
				ctlName = grdName+ "_ctl" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl" + idx + "_txtCmd";
				 calias = grdName+ "_ctl" + idx + "_txtParamAlias";
				} 
				
				
				var elemnt = document.getElementById(ctlName);	
				var etext = document.getElementById(ctext);
				var ealias = document.getElementById(calias);
				if ((elemnt.selectedIndex == 1  && etext.value == "") )
					{
					alert("Please type SQL query or specify procedure name.");
					etext.focus();
					return false;
					}
				if (ealias.value =="")
				{
					alert("Please type Parameter Alias.");
					return false;
				}
				
				
			}
		}
		 
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" >
		<form id="Form1" method="post" runat="server">
			<table id="tblMain" borderColor="#e4e2e2" cellSpacing="0" cellPadding="0" width="764" align="center"
				border="0">
				<tr>
					<td vAlign="top">
						<!-- For Header -->
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></td>
				</tr>
				<TR>
					<TD class="tableHeader" vAlign="top" align="right"></TD>
				</TR>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TR>
								<TD align="left" style="HEIGHT: 17px"><STRONG class="clsmainhead"><IMG height="17" src="../../images/head_icon.gif" width="30">Report&nbsp;Management</STRONG></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 17px" align="left" background="../../images/separator_repeat.gif"></TD>
							</TR>
							<tr>
								<td align="left" style="HEIGHT: 2px" class="clsleftpaddingtable">
									<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD align="center" width="75%" class="clsleftpaddingtable">&nbsp;
												<asp:label id="lblNoRec" runat="server" CssClass="cmdlinks" Width="184px" Visible="False" ForeColor="Red"></asp:label></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
							<tr> <!-- tahir   -->
								<td align="left" width="100%" vAlign="bottom" style="HEIGHT: 26px" class="clsleftpaddingtable"><STRONG><FONT face="Verdana" color="#0066cc" class="clsleftpaddingtable">
											<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TBODY>
													<TR>
														<TD width="912" style="WIDTH: 912px" class="clsleftpaddingtable">&nbsp;
															<asp:Label id="Label4" runat="server" CssClass="label" Width="104px" Font-Size="Smaller">Stored Procedure</asp:Label>
															<asp:textbox id="txtSearchSp" CssClass="FrmTDLetter" Runat="server"></asp:textbox>&nbsp;
															<asp:button id="cmdSearchSp" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
														<TD style="WIDTH: 172px" align="left" class="clsleftpaddingtable"></TD>
														<TD align="right" style="WIDTH: 185px">&nbsp;<asp:label id="lblCurrPage" runat="server" CssClass="label" Font-Size="Smaller">Current Page :</asp:label></TD>
														<TD align="right" style="WIDTH: 5px">&nbsp;<asp:label id="lblPNo" runat="server" Width="16px" CssClass="label" Height="10px" Font-Size="Smaller">a</asp:label></TD>
														<TD align="right" style="WIDTH: 31px">&nbsp;<asp:label id="lblGoto" runat="server" Width="16px" CssClass="label" Font-Size="Smaller">Goto</asp:label></TD>
														<TD align="right">&nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" CssClass="clsinputcombo" Font-Size="Smaller" AutoPostBack="True"></asp:dropdownlist></TD>
													</TR>
												</TBODY>
											</TABLE>
										</FONT></STRONG>
								</td>
							</tr>
							<TR>
								<TD style="HEIGHT: 16px" vAlign="bottom" align="left" width="100%" background="../../images/separator_repeat.gif"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 38px" vAlign="middle" align="left" width="100%" background="../../Images/subhead_bg.gif"
									class="clssubhead">List of Stored Procedures</TD>
							</TR>
							<tr>
								<td valign="top" align="center" width="100%">&nbsp;
									<asp:datagrid id="DgSqlProcedure" runat="server" Width="780px" AllowPaging="True" AutoGenerateColumns="False"
										CellPadding="0" CssClass="clsleftpaddingtable">
										<Columns>
											<asp:TemplateColumn HeaderText="Procedure Name">
												<HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:HyperLink id=HLSpnm runat="server" CssClass="cmdLinks2" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'>
													</asp:HyperLink>
													<asp:LinkButton id=lnkSPName runat="server" CssClass="cmdLinks2" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' CommandName="ShowParam">
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Create Date">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=Label1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.crdate") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=TextBox1 runat="server" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.crdate") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
									</asp:datagrid>
									<asp:label id="lblmsg" runat="server" CssClass="cmdlinks" Visible="False" ForeColor="Red"></asp:label>
								</td>
							</tr>
							<TR>
								<TD align="center" width="800" style="HEIGHT: 45px">
									<TABLE id="tblhide" cellSpacing="0" cellPadding="0" width="780" border="0">
										<TR>
											<TD class="clssubhead" style="HEIGHT: 37px" align="left" vAlign="middle" background="../../Images/subhead_bg.gif">
												Report Descriptions</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<tr>
								<TD align="left" vAlign="top" class="clsleftpaddingtable" style="HEIGHT: 32px">
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD style="WIDTH: 144px">&nbsp;<asp:label id="lblreportname" runat="server" Visible="False" Width="80px" CssClass="label"
													Height="8px" Font-Size="Smaller">Report Name</asp:label></TD>
											<TD style="WIDTH: 154px">&nbsp;<asp:textbox id="txtreportname" runat="server" Visible="False" CssClass="clsinputadministration"></asp:textbox></TD>
											<TD style="WIDTH: 80px">&nbsp;<asp:label id="Lblloc" runat="server" Visible="False" Width="56px" CssClass="label" Height="8px"
													Font-Size="Smaller">Location</asp:label></TD>
											<TD class="clsleftpaddingtable">&nbsp;<asp:dropdownlist id="dlistloc" runat="server" Visible="False" Width="96px" CssClass="clsinputcombo">
													<asp:ListItem Value="0">ALL</asp:ListItem>
													<asp:ListItem Value="1">Houston</asp:ListItem>
													<asp:ListItem Value="2">Dallas</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
									</TABLE>
								</TD>
							</tr>
							<tr>
								<TD vAlign="top" align="left" style="HEIGHT: 82px">
									<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD style="WIDTH: 141px; HEIGHT: 74px" vAlign="top" class="clsleftpaddingtable">&nbsp;<asp:label id="lblreportdesc" runat="server" Visible="False" CssClass="label" ForeColor="Black"
													Height="8px" Font-Size="Smaller">Report Description</asp:label></TD>
											<TD style="HEIGHT: 74px" class="clsleftpaddingtable">&nbsp;&nbsp;<asp:textbox id="txtreportdesc" runat="server" Visible="False" Width="328px" CssClass="clsinputadministration"
													Height="56px" TextMode="MultiLine"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
							</tr>
							<TR>
								<TD><asp:datagrid id="Dg_Output" runat="server" Width="780px" CellPadding="0" AutoGenerateColumns="False"
										CssClass="clsleftpaddingtable">
										<Columns>
											<asp:TemplateColumn HeaderText="Parameter Name">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblParamName runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Parameter_name") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Parameter Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblParamType runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Type") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Parameter Alias">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:TextBox id="txtParamAlias" CssClass="clsinputadministration" Width="90px" Visible="True"
														Runat="server" EnableViewState="True"></asp:TextBox>
												</ItemTemplate>
												<EditItemTemplate>
													Alias
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Parameter Length">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblParamLength runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Default Value">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:TextBox id="txtParamDef" runat="server" CssClass="clsinputadministration"></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Visibility">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox id="chkVisibility" runat="server" CssClass="clslabel"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Object Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:DropDownList id="DDLObjType" runat="server" CssClass="clsinputcombo" Width="100px">
														<asp:ListItem Value="1">Text Box</asp:ListItem>
														<asp:ListItem Value="2">Drop Down Box</asp:ListItem>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Command Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:DropDownList id="DDLCmdType" runat="server" CssClass="clsinputcombo" Width="120px" Enabled="False">
														<asp:ListItem Value="1">Text</asp:ListItem>
														<asp:ListItem Value="2">Stored Procedure</asp:ListItem>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Command Text">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:TextBox id="txtCmd" runat="server" CssClass="clsinputadministration" Enabled="False"></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="right" style="HEIGHT: 18px">
									<asp:button id="btnReset" runat="server" CssClass="clsbutton" Text="Submit"></asp:button>&nbsp;
									<asp:button id="btnsubmit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
							</TR>
							<TR>
								<TD style="VISIBILITY: hidden; HEIGHT: 20px" align="right">
									<asp:textbox id="txtSpName" runat="server" Visible="False" DESIGNTIMEDRAGDROP="1460"></asp:textbox><asp:textbox id="txtchk" runat="server">0</asp:textbox><asp:textbox id="txtNoParam" runat="server">0</asp:textbox><asp:textbox id="TextBox2" runat="server"></asp:textbox></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
			<script language="javascript"> ShowHidePageNavigation()</script>
		</form>
	</body>
</HTML>

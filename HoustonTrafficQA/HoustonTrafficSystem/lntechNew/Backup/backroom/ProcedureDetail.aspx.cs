using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.Data.SqlClient;
using System.Data.Common;


namespace SPUtility
{
    /// <summary>
    /// Summary description for ProcedureDetail.
    /// </summary>
    public partial class ProcedureDetail : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.TextBox txt_StoreProcedure;
        protected System.Web.UI.WebControls.Button btn_Rollback;
        protected System.Web.UI.WebControls.Label lbl_Msg;
        protected System.Web.UI.WebControls.Label lbl_SPName;
        protected System.Web.UI.WebControls.Label lbl_Date;
        protected System.Web.UI.WebControls.Label lbl_Time;
        protected System.Web.UI.WebControls.Label lbl_username;

        clsENationWebComponents clsDB = new clsENationWebComponents();

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                lbl_Msg.Text = "";
                lbl_Msg.Visible = false;
                txt_StoreProcedure.Attributes.Add("onclick", "return Not();");
                int SPID = Convert.ToInt32(Request.QueryString["SPID"]);
                BindControls(SPID);

            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Rollback.Click += new System.EventHandler(this.btn_Rollback_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
        private void BindControls(int SPID)
        {
            try
            {
                string spcontents = "";
                string[] key1 ={ "@SP_ID" };
                object[] value1 ={ SPID };
                //Change by Ajmal
                //SqlDataReader Reader=clsDB.Get_DR_BySPArr("USP_SP_GET_Procedure",key1,value1);
                IDataReader Reader = clsDB.Get_DR_BySPArr("USP_SP_GET_Procedure", key1, value1);

                while (Reader.Read())
                {
                    spcontents += Reader["SPContents"];
                    //spcontents.Replace(" "+"go"+" "," ");
                    txt_StoreProcedure.Text = spcontents;
                    lbl_username.Text = Reader["UserName"].ToString();
                    lbl_SPName.Text = Reader["SP_Name"].ToString();
                    lbl_Date.Text = String.Format("{0:D}", Reader["Date"]);
                    lbl_Time.Text = String.Format("{0:t}", Reader["Date"]);

                }

                /*txt_username.Text=Reader["UserName"].ToString();	
                txt_SP.Text=Reader["SP_Name"].ToString();
                txt_date.Text=Reader["SourceDate"].ToString();*/

                Reader.Close();

            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;

            }
        }

        private void btn_Search_Click(object sender, System.EventArgs e)
        {

        }
        private void btn_Rollback_Click(object sender, System.EventArgs e)
        {
            try
            {
                lbl_Msg.Text = "";
                lbl_Msg.Visible = false;
                string SPname = "";
                string StoreProcedure = "";


                SPname = lbl_SPName.Text.ToString();
                StoreProcedure = txt_StoreProcedure.Text.ToString();
                if (SPname != "" && StoreProcedure != "")
                {
                    
                    clsDB.ExecuteQuery("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" + SPname + "]') and OBJECTPROPERTY(id,N'Isprocedure') = 1)" + " " + "drop procedure [dbo].[" + SPname + "]");
                    clsDB.ExecuteQuery(StoreProcedure);
                    lbl_Msg.Visible = true;
                    lbl_Msg.Text = "Stored Procedure Roll Back Sucessfully";
                }
            }
            catch (Exception ex)
            {
                lbl_Msg.Visible = true;
                lbl_Msg.Text = ex.Message;
            }
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.Data.Common;
using System.Configuration;
using HTP.Components.ClientInfo;
using HTP.WebComponents;
using HTP.Components.Services;
using HTP.Components.Entities;
namespace HTP.backroom
{
    /// <summary>
    /// Summary description for Courts.
    /// </summary>
    public partial class Courts : MatterBasePage
    {
        protected System.Web.UI.WebControls.Button btn_Delete;
        protected System.Web.UI.WebControls.Button btn_Submit;
        clsLogger clog = new clsLogger();
        clsSession ClsSession = new clsSession();
        clsCourts ClsCourts = new clsCourts();
        clsPricingPlans ClsPricingPlans = new clsPricingPlans();
        CourtsFiles files = new CourtsFiles();
        CountyService countyservice = new CountyService();

               
        //khalid
        DataView DV;
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        //

        clsENationWebComponents ClsDB = new clsENationWebComponents();
        //Yasir Kamal 7150 01/06/2010 display court popup
        public delegate void ButtonClickHandler(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e);

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Check for valid Session if not then redirect to login page
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                //ozair 4411 07/14/2008 removed secondary user check for activities section
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {



                        try
                        {
                            //Zeeshan Ahmed 3697 05/07/2008
                            //Add For File Upload Control To Work In Update Panel
                            ScriptManager1.RegisterPostBackControl(FileUpload1);
                            //ScriptManager1.Controls.
                        }
                        catch
                        { }

                        //Commented by Ozair
                        //dtpCourtDate.SelectedDate = DateTime.Now.Date;
                        //Added by Ozair
                        dtpCourtDate.Clear();
                        //
                        btn_Submit.Attributes.Add("OnClick", "javascript:return Submit();");
                        FillSettingRequest();
                        FillStates();
                        FillGrid();
                        FillCaseType();
                        //Sabir Khan 5941 07/24/2009 Fill county drop down list...
                        FillCountyName();
                        //Asad Ali 8153 09/20/2010 fill ALR Associated courts
                        FillAssociatedCourt();
                        //Yasir Kamal 7150 01/06/2010 display court popup
                        if (Request.QueryString["ALRCourt"] != null)
                        {

                            ButtonClickHandler handler = new ButtonClickHandler(dg_Result_ItemCommand);
                            handler(null, null);
                            
                        }


                        // ClearValues();
                    }
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
            }
        }

        public void FillSettingRequest()
        {
            ddl_SettingRequest.Items.Clear();
            DataSet ds = ClsDB.Get_DS_BySP("usp_HTS_Get_CourtSettingRequestType");
            ddl_SettingRequest.DataSource = ds;
            ddl_SettingRequest.DataTextField = ds.Tables[0].Columns[1].ColumnName;
            ddl_SettingRequest.DataValueField = ds.Tables[0].Columns[0].ColumnName;
            ddl_SettingRequest.DataBind();
            ddl_SettingRequest.SelectedValue = "4";
        }

        public void FillStates()
        {
            ddl_States.Items.Clear();
            DataSet ds = ClsDB.Get_DS_BySP("USP_HTS_Get_State");
            ddl_States.DataSource = ds;
            ddl_States.DataTextField = ds.Tables[0].Columns[1].ColumnName;
            ddl_States.DataValueField = ds.Tables[0].Columns[0].ColumnName;
            ddl_States.DataBind();
            ddl_States.SelectedValue = "45";
        }
        //Noufil 4237 06/28/2008 Get case type
        public void FillCaseType()
        {
            ddl_casetype.Items.Clear();
            DataSet ds_case = ClsDB.Get_DS_BySP("USP_HTP_GET_ALL_CASETYPE");
            ddl_casetype.DataSource = ds_case;
            ddl_casetype.DataTextField = ds_case.Tables[0].Columns[1].ColumnName;
            ddl_casetype.DataValueField = ds_case.Tables[0].Columns[0].ColumnName;
            ddl_casetype.DataBind();
            ddl_casetype.SelectedValue = "1";
        }


        public void FillGrid()
        {   
                DataSet DS = ClsCourts.GetCourtInfo();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DV = new DataView(DS.Tables[0]);
                    Session["DS"] = DV;
                    dg_Result.DataSource = DS;
                    dg_Result.DataBind();
                }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //Kamran 3536 04/09/08 this event genrate a double click because client side also call this button Onclick=btn_Submit_Click
            //this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            this.dg_Result.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Result_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void dg_Result_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                lbl_Msg.Text = "";
                //Yasir Kamal 7150 01/06/2010 display court popup
                if (Request.QueryString["ALRCourt"] != null && e==null)
                {
                    displayCourtPopUp(Request.QueryString["ALRCourt"].ToString());

                }
                else if (e.CommandName == "CourtNo")
                {
                    Label courtid = (Label)e.Item.FindControl("lbl_CourtID");
                    displayCourtPopUp(courtid.Text);
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
            }
        }

        private void btn_Delete_Click(object sender, System.EventArgs e)
        {
            try
            {
                lbl_Msg.Text = "";
                int ccount = ClsCourts.DeleteCourt(Convert.ToInt32(lblcourtID.Text));
                if (ccount == 0)
                {
                    FillCourtFilesGrid();
                    lbl_Msg.Text = "Court Deleted";
                    FillGrid();
                    ClearValues();
                }
                else
                {
                    lbl_Msg.Text = "Cannot delete this court; clients are assigned to this court";
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_Submit_Click(object sender, System.EventArgs e)
        {
            try
            {
                lbl_Msg.Text = "";

                //If New Court
                if (hfCourtid.Value == "0")
                {
                    //Save Court Information
                    hfCourtid.Value = "1";
                    DbTransaction trans = ClsDB.DBBeginAndReturnTransaction();
                    //Set Parameters
                    SetValues();
                    //Save Court Information
                    int court = ClsCourts.AddCourt(trans);
                    lblcourtID.Text = Convert.ToString(court);
                    //If Court Already Exist
                    if (court == 0)
                        lbl_Msg.Text = "Court Name Already Exist";
                    else
                    {
                        //Set Price Plan 
                        if (SetPriceValue(court, trans) != false)
                            FillGrid();
                        else
                            lbl_Msg.Text = "An Error Occured during this operation.";
                    }
                }
                else
                {
                    //Update Court Information
                    SetValues();
                    if (ClsCourts.UpdateCourt() == true)
                        FillGrid();

                }
                //Upload Files Related To Court
                if (FileUpload1.HasFile)
                {
                    files.CourtId = Convert.ToInt32(lblcourtID.Text);
                    files.Rep = this.EmpId;
                    if (files.UploadFile(FileUpload1))
                    {
                        FillCourtFilesGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = "An Error Occured during this operation.";
            }
        }

        protected bool SetPriceValue(int court, DbTransaction trans)
        {
            try
            {
                //init transaction
                const int templateId = 45;

                //Getting values from template
                DataSet dsTemplate = ClsPricingPlans.GetPlan(templateId);
                DataSet dsTemplateDetail = ClsPricingPlans.GetPlanDetails(templateId);

                if (dsTemplate.Tables.Count > 0)
                {
                    DataTable dtTemplate = dsTemplate.Tables[0];
                    DataTable dtTemplateDetail = ClsPricingPlans.GetPlanDetails(templateId).Tables[0];
                    //Setting Values
                    ClsPricingPlans.PlanDescription = txt_CourtName.Text + "-default plan";


                    ClsPricingPlans.PlanShortName = txt_ShortName.Text + "_plan";

                    ClsPricingPlans.EffectiveDate = DateTime.Now;

                    ClsPricingPlans.EndDate = DateTime.Now.AddYears(2);

                    ClsPricingPlans.CourtID = court;

                    ClsPricingPlans.ShowAll = 0;

                    ClsPricingPlans.PlanID = ClsPricingPlans.AddPlanInfo(trans);

                    if (ClsPricingPlans.PlanID == 0)
                    {
                        ClsDB.DBTransactionRollback();
                        return false;
                    }

                    if (dsTemplateDetail.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dtTemplateDetail.Rows)
                        {
                            ClsPricingPlans.CategoryID = Convert.ToInt32(dr["CategoryId"]);
                            ClsPricingPlans.BasePrice = Convert.ToDouble(dr["BasePrice"]);
                            ClsPricingPlans.SecondaryPrice = Convert.ToDouble(dr["SecondaryPrice"]);
                            ClsPricingPlans.BasePercentage = Convert.ToDouble(dr["BasePercentage"]);
                            ClsPricingPlans.SecondaryPercentage = Convert.ToDouble(dr["SecondaryPercentage"]);
                            ClsPricingPlans.BondBase = Convert.ToDouble(dr["BondBase"]);
                            ClsPricingPlans.BondSecondary = Convert.ToDouble(dr["BondSecondary"]);
                            ClsPricingPlans.BondBasePercentage = Convert.ToDouble(dr["BondBasePercentage"]);
                            ClsPricingPlans.BondSecondaryPercentage = Convert.ToDouble(dr["BondSecondaryPercentage"]);
                            ClsPricingPlans.BondAssumtion = Convert.ToDouble(dr["BondAssumption"]);
                            ClsPricingPlans.FineAssumption = Convert.ToDouble(dr["FineAssumption"]);

                            if (ClsPricingPlans.UpdatePlanDetails(trans) == false)
                            {
                                ClsDB.DBTransactionRollback();
                                return false;
                            }
                        }
                    }

                    ClsDB.DBTransactionCommit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
                return false;
            }

        }

        public void ClearValues()
        {
            try
            {
                txt_Address1.Text = "";
                txt_Address2.Text = "";
                txt_CC1.Text = "";
                txt_CC11.Text = "";
                txt_CC12.Text = "";
                txt_CC13.Text = "";
                txt_CC14.Text = "";
                txt_CC2.Text = "";
                txt_CC3.Text = "";
                txt_City.Text = "";
                txt_CourtContact.Text = "";
                txt_CourtName.Text = "";
                txt_DDName.Text = "";
                txt_JudgeName.Text = "";
                txt_ShortName.Text = "";
                txt_VisitCharges.Text = "";
                txt_ZipCode.Text = "";
                txt_min.Text = "00";
                txt_hour.Text = "00";
                dd_time.SelectedIndex = 0;
                //kamran 3536 04/15/08 reset ddl 
                ddl_States.SelectedIndex = 0;
                //ddl_States.SelectedValue="45";
                ddl_SettingRequest.SelectedIndex = 0;
                //ddl_SettingRequest.SelectedValue="4";

                ddl_BondType.ClearSelection();
                lblcourtID.Text = "0";
                btn_Delete.Enabled = false;
                //Commented by Ozair
                //dtpCourtDate.SelectedDate = DateTime.Now.Date;
                //Added by Ozair
                dtpCourtDate.Clear();
                //added by khalid --1-2-08
                chk_duplicateCauseno.Checked = false;
                //kamran 3536 04/11/08 reset ddl status
                ddl_status.SelectedIndex = 0;

                //Kazim 3697 5/6/2008 Clear the newly added fields.  

                ddlCaseIdentification.SelectedValue = "-1";
                ddlAcceptAppeals.SelectedIndex = -1;
                txtAppealLocation.Text = "";
                ddlInitialSetting.SelectedValue = "-1";
                ddlLOR.SelectedValue = "-1";
                txtAppearanceHire.Text = "";
                txtJugeTrial.Text = "";
                txtJuryTrial.Text = "";
                rblHandwriting.SelectedIndex = -1;
                rblFTAIssues.SelectedIndex = -1;
                rblGracePeriod.SelectedIndex = -1;
                //Nasir 5310 12/24/2008 clear value default
                rblALRProcess.SelectedIndex = 1;
                txtContinuance.Text = "";
                rblSupportingDoc.SelectedIndex = -1;
                txtComments.Text = "";
                txt_min.Text = "00";
                txt_hour.Text = "00";
                dd_time.SelectedValue = "AM";
                //Sabir Khan 5941 07/24/2009 clear county name drop down...
                ddl_CountyName.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
            }
        }

        public void SetValues()
        {
            try
            {
                if (lblcourtID.Text != "0")
                {
                    ClsCourts.CourtID = Convert.ToInt32(lblcourtID.Text);
                }
                ClsCourts.CourtName = txt_CourtName.Text.Trim();
                ClsCourts.JudgeName = txt_JudgeName.Text.Trim();
                ClsCourts.ShortName = txt_ShortName.Text.Trim();
                ClsCourts.Address1 = txt_Address1.Text.Trim();
                ClsCourts.Address2 = txt_Address2.Text.Trim();
                ClsCourts.City = txt_City.Text.Trim();
                ClsCourts.StateID = Convert.ToInt32(ddl_States.SelectedValue);
                ClsCourts.ZipCode = txt_ZipCode.Text.Trim();
                ClsCourts.CourtDropDownName = txt_DDName.Text.Trim();
                ClsCourts.CourtContact = txt_CourtContact.Text.Trim();
                ClsCourts.Phone = txt_CC11.Text + txt_CC12.Text + txt_CC13.Text + txt_CC14.Text;
                ClsCourts.Fax = txt_CC1.Text + txt_CC2.Text + txt_CC3.Text;
                ClsCourts.SettingRequestType = Convert.ToInt32(ddl_SettingRequest.SelectedValue);
                ClsCourts.BondType = Convert.ToInt32(ddl_BondType.SelectedValue);
                ClsCourts.Casetype = Convert.ToInt32(ddl_casetype.SelectedValue);

                //Kazim 3697 5/6/2008 Set the values newly added fields.  

                ClsCourts.CaseIdentifiaction = Convert.ToInt32(ddlCaseIdentification.SelectedValue);
                ClsCourts.AcceptAppeals = Convert.ToInt32(ddlAcceptAppeals.SelectedValue);
                ClsCourts.AppealLocation = txtAppealLocation.Text;
                ClsCourts.InitialSetting = Convert.ToInt32(ddlInitialSetting.SelectedValue);
                ClsCourts.LOR = Convert.ToInt32(ddlLOR.SelectedValue);
                ClsCourts.AppearnceHire = txtAppearanceHire.Text;
                ClsCourts.JudgeTrialHire = txtJugeTrial.Text;
                ClsCourts.JuryTrialHire = txtJuryTrial.Text;
                //Sabir Khan 5941 07/24/2009 Get selected county id...
                ClsCourts.County.CountyID = Convert.ToInt32(ddl_CountyName.SelectedValue);
                
                if (rblHandwriting.SelectedValue == "")
                {
                    ClsCourts.HandwritingAllowed = -1;
                }
                else
                {
                    ClsCourts.HandwritingAllowed = Convert.ToInt32(rblHandwriting.SelectedValue);
                }
                if (rblFTAIssues.SelectedValue == "")
                {
                    ClsCourts.AdditionalFTAIssued = -1;
                }
                else
                {
                    ClsCourts.AdditionalFTAIssued = Convert.ToInt32(rblFTAIssues.SelectedValue);
                }
                if (rblGracePeriod.SelectedValue == "")
                {
                    ClsCourts.GracePeriod = -1;
                }
                else
                {
                    ClsCourts.GracePeriod = Convert.ToInt32(rblGracePeriod.SelectedValue);
                }
                //Nasir 5310 12/24/2008 set property ALRProcess to insert
                ClsCourts.ALRProcess = Convert.ToInt32(rblALRProcess.SelectedValue);
                //Asad Ali 8153 09/20/2010 set Associated ALR Court ID
                if (ddl_casetype.SelectedValue == "2" && ClsCourts.ALRProcess == 0)
                {
                    ClsCourts.AssociatedALRCourtID = Convert.ToInt32(ddl_AssociatedCourts.SelectedValue);
                }
                else
                {
                    ClsCourts.AssociatedALRCourtID = 0;
                }

                ClsCourts.Continuance = txtContinuance.Text;

                if (rblSupportingDoc.SelectedValue == "")
                {
                    ClsCourts.SupportingDocument = -1;
                }
                else
                {
                    ClsCourts.SupportingDocument = Convert.ToInt32(rblSupportingDoc.SelectedValue);
                }

                ClsCourts.Comments = txtComments.Text;

                ClsCourts.IsLetterofRep = Convert.ToInt32(cb_rep.Checked);

                //Sabir Khan 5763 04/10/2009 
                ClsCourts.IsLORSubpoena = Convert.ToInt32(chkIsLorSubpoena.Checked);
                ClsCourts.IsLORMOD = Convert.ToInt32(chkIsLorMod.Checked);

                // Rab Nawaz Khan 8997 10/18/2011 Added the logic of allow or restrict the court room numbers for the outside courts
                if (chkAllowCourtNumbers.Checked)
                    ClsCourts.AllowCourtNumbers = true;
                else
                    ClsCourts.AllowCourtNumbers = false;
                

                //added/modified by Ozair 
                if (cb_rep.Checked == true && dtpCourtDate.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    // Noufil 4945 10/14/2008 set time
                    ClsCourts.LetterOfRep = Convert.ToDateTime(dtpCourtDate.SelectedDate.ToShortDateString() + " " + txt_hour.Text + ":" + txt_min.Text + dd_time.SelectedValue.ToString());
                }
                else
                {
                    ClsCourts.LetterOfRep = Convert.ToDateTime("1/1/1900" + " " + "00" + ":" + "00" + "AM");
                }

                //Muhammad Muneer 8227 09/27/2010 add the new functionality for the LOR Dates
                if (cb_rep.Checked == true && dtpCourtDate2.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    ClsCourts.LORRepDate2 = Convert.ToDateTime(dtpCourtDate2.SelectedDate.ToShortDateString() + " " + txt_hour2.Text + ":" + txt_min2.Text + dd_time2.SelectedValue.ToString());
                }
                else
                {
                    ClsCourts.LORRepDate2 = Convert.ToDateTime("1/1/1900" + " " + "00" + ":" + "00" + "AM");
                }
                //Muhammad Muneer 8227 09/27/2010 add the new functionality for the LOR Dates
                if (cb_rep.Checked == true && dtpCourtDate3.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    ClsCourts.LORRepDate3 = Convert.ToDateTime(dtpCourtDate3.SelectedDate.ToShortDateString() + " " + txt_hour3.Text + ":" + txt_min3.Text + dd_time3.SelectedValue.ToString());
                }
                else
                {
                    ClsCourts.LORRepDate3 = Convert.ToDateTime("1/1/1900" + " " + "00" + ":" + "00" + "AM");
                }

                //Muhammad Muneer 8227 09/27/2010 add the new functionality for the LOR Dates
                if (cb_rep.Checked == true && dtpCourtDate4.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    ClsCourts.LORRepDate4 = Convert.ToDateTime(dtpCourtDate4.SelectedDate.ToShortDateString() + " " + txt_hour4.Text + ":" + txt_min4.Text + dd_time4.SelectedValue.ToString());
                }
                else
                {
                    ClsCourts.LORRepDate4 = Convert.ToDateTime("1/1/1900" + " " + "00" + ":" + "00" + "AM");
                }

                //Muhammad Muneer 8227 09/27/2010 add the new functionality for the LOR Dates
                if (cb_rep.Checked == true && dtpCourtDate5.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    ClsCourts.LORRepDate5 = Convert.ToDateTime(dtpCourtDate5.SelectedDate.ToShortDateString() + " " + txt_hour5.Text + ":" + txt_min5.Text + dd_time5.SelectedValue.ToString());
                }
                else
                {
                    ClsCourts.LORRepDate5 = Convert.ToDateTime("1/1/1900" + " " + "00" + ":" + "00" + "AM");
                }

                //Muhammad Muneer 8227 09/27/2010 add the new functionality for the LOR Dates
                if (cb_rep.Checked == true && dtpCourtDate6.SelectedDate.ToShortDateString() != "1/1/0001")
                {
                    ClsCourts.LORRepDate6 = Convert.ToDateTime(dtpCourtDate6.SelectedDate.ToShortDateString() + " " + txt_hour6.Text + ":" + txt_min6.Text + dd_time6.SelectedValue.ToString());
                }
                else
                {
                    ClsCourts.LORRepDate6 = Convert.ToDateTime("1/1/1900" + " " + "00" + ":" + "00" + "AM");
                }


                //
                if (txt_VisitCharges.Text == "")
                {
                    ClsCourts.VisitCharges = 0;
                }
                else
                {
                    ClsCourts.VisitCharges = Convert.ToDouble(txt_VisitCharges.Text.Trim());
                }

                ClsCourts.InActiveFlag = Convert.ToInt32(this.ddl_status.SelectedValue);
                //if(chkb_InActive.Checked)
                //{
                //    ClsCourts.InActiveFlag=1;
                //}
                //else
                //{
                //    ClsCourts.InActiveFlag=0;
                //}
                if (ddl_casetype.SelectedValue == "2")
                {
                    ClsCourts.IsCriminalCourt = 1;

                }
                else
                {
                    ClsCourts.IsCriminalCourt = 0;
                    
                }
                //added by khalid for  Duplicate cause no field
                if (chk_duplicateCauseno.Checked)
                {
                    ClsCourts.DuplicateCauseno = true;
                }
                else
                {
                    ClsCourts.DuplicateCauseno = false;

                }
                //Asad Ali 8153 09/20/2010 comment code b/c this task in requirement
                //Nasir 7369 02/19/2010 added AllowOnlineSignUp
                //ClsCourts.AllowOnlineSignUp = Convert.ToBoolean(Convert.ToInt32(rbtAllowOnlineSignUp.SelectedValue));


            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
            }
        }
        //Added by ozair

        protected void dg_Result_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                string LetterOfRep = ((Label)e.Item.FindControl("lbl_letterofrep")).Text;

                if (LetterOfRep == "1/1/1900" || LetterOfRep == "01/01/1900" || LetterOfRep == "")
                    ((Label)e.Item.FindControl("lbl_letterofrep")).Text = "N/A";
            }
        }

        //added khalid 27-9-07
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;

                dg_Result.DataSource = DV;
                dg_Result.DataBind();
                //SerialNumber();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        protected void dg_Result_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        protected void btnResetAdvance_Click(object sender, EventArgs e)
        {
            Response.Redirect("Courts.aspx", false);
        }

        protected void gvCourtFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Footer))
            {
                string lnk = "javascript: return OpenPopUpNew('../PaperLess/frmPreviewDocuments.aspx?fielid=" + ((HiddenField)e.Row.FindControl("hfFileId")).Value + "')";
                ((ImageButton)(e.Row.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", lnk);
            }

        }

        //Kazim 3697 5/6/2008 Method is used to fill the uploaded files of Selected court. 

        private void FillCourtFilesGrid()
        {
            files.CourtId = Convert.ToInt32(lblcourtID.Text);
            DataTable dt = files.GetUploadedFiles();

            //Kazim 4087 5/21/2008 Set the height of Courtfile div

            if (dt.Rows.Count > 0)
            {

                trgridline.Visible = true;
                divDocumentDetails.Style[HtmlTextWriterStyle.Height] = "90px";
                CBECaseIdentification.ExpandedSize = 400;
            }
            else
            {
                trgridline.Visible = false;
                divDocumentDetails.Style[HtmlTextWriterStyle.Height] = "10px";
                CBECaseIdentification.ExpandedSize = 270;
            }
            gvCourtFiles.DataSource = dt;
            gvCourtFiles.DataBind();

        }

        protected void gvCourtFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    files.DeleteFile(Convert.ToInt32(e.CommandArgument));
                    FillCourtFilesGrid();
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_Msg.Text = ex.Message;
            }
        }

        protected void gvCourtFiles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        //Sabir Khan 5941 07/24/2009 Method for binding county drop down...
        public void FillCountyName()
        {

            ddl_CountyName.Items.Clear();
            DataSet ds_County = countyservice.GetCounties(true);
            DataRow dr = ds_County.Tables[0].NewRow();
            dr[0] = 0;
            dr[1] = "---- Choose ----";
            ds_County.Tables[0].Rows.InsertAt(dr, 0);
            ddl_CountyName.DataSource = ds_County;
            ddl_CountyName.DataTextField = ds_County.Tables[0].Columns[1].ColumnName;
            ddl_CountyName.DataValueField = ds_County.Tables[0].Columns[0].ColumnName;
            ddl_CountyName.DataBind();                        
            ddl_CountyName.SelectedValue = "0";
            
        }
        //Asad Ali 8153 09/20/2010 get All Associated court
        public void FillAssociatedCourt()
        {

            ddl_AssociatedCourts.Items.Clear();
            DataSet ds_AssociatdCourt = ClsCourts.GetAssociatedCourts(); ;
            DataRow dr = ds_AssociatdCourt.Tables[0].NewRow();
            ddl_AssociatedCourts.DataSource = ds_AssociatdCourt;
            ddl_AssociatedCourts.DataTextField = ds_AssociatdCourt.Tables[0].Columns[1].ColumnName;
            ddl_AssociatedCourts.DataValueField = ds_AssociatdCourt.Tables[0].Columns[0].ColumnName;
            ddl_AssociatedCourts.DataBind();
            

        }

        //Yasir Kamal 7150 01/06/2010 code moved from item command event to method to display court popup for setting county.
        private void displayCourtPopUp(string Court)
        {
            //Asad Ali 8153 29/09/2010 fill drop down again 
            FillAssociatedCourt();
            MPECourtDetailPopup.Show();

            //Kazim 4071 05/19/2008 Remove bondtype and continuance panels

            CBECaseIdentification.ClientState = "true";
            CBECaseIdentification.Collapsed = true;
            CBEGeneralInfo.ClientState = "false";
            CBEGeneralInfo.Collapsed = false;
            ClearValues();

            //Kazim 4071 5/22/2008 Set the value of hfcourtid  

           
            hfCourtid.Value = Court;

            DataSet ds = ClsCourts.GetCourtInfo(Convert.ToInt32(Court));
            lblcourtID.Text = ds.Tables[0].Rows[0]["courtid"].ToString();
            txt_Address1.Text = ds.Tables[0].Rows[0]["address"].ToString();
            txt_Address2.Text = ds.Tables[0].Rows[0]["address2"].ToString();
            string fax = ds.Tables[0].Rows[0]["fax"].ToString().Trim();
            if (fax.Length > 0)
            {

                txt_CC1.Text = (fax.Length >= 3) ? fax.Substring(0, 3) : fax;
                txt_CC2.Text = (fax.Length >= 6) ? fax.Substring(3, 3) : fax.Substring(3);
                txt_CC3.Text = (fax.Length >= 10) ? fax.Substring(6, 4) : fax.Substring(6);
                //txt_CC1.Text = fax.Substring(0, 3);
                //txt_CC2.Text = fax.Substring(3, 3);
                //txt_CC3.Text = fax.Substring(6, 4);

            }
            string phone = ds.Tables[0].Rows[0]["phone"].ToString().Trim();
            if (phone.Length > 0 && phone != "N/A")
            {
                txt_CC11.Text = (phone.Length >= 3) ? phone.Substring(0, 3) : phone;
                txt_CC12.Text = (phone.Length >= 6) ? phone.Substring(3, 3) : phone.Substring(3);
                txt_CC13.Text = (phone.Length >= 10) ? phone.Substring(6, 4) : phone.Substring(6);

                //txt_CC11.Text = phone.Substring(0, 3);
                //txt_CC12.Text = phone.Substring(3, 3);
                //txt_CC13.Text = phone.Substring(6, 4);
                if (phone.Length > 10)
                {
                    txt_CC14.Text = phone.Substring(10);
                }
                else
                    //kamran 3536 04/15/08 extention problam
                    txt_CC14.Text = "";
            }
            txt_City.Text = ds.Tables[0].Rows[0]["city"].ToString();
            txt_CourtContact.Text = ds.Tables[0].Rows[0]["courtcontact"].ToString();
            txt_CourtName.Text = ds.Tables[0].Rows[0]["courtname"].ToString();
            txt_DDName.Text = ds.Tables[0].Rows[0]["shortcourtname"].ToString();
            txt_JudgeName.Text = ds.Tables[0].Rows[0]["judgename"].ToString();
            txt_ShortName.Text = ds.Tables[0].Rows[0]["shortname"].ToString();
            txt_VisitCharges.Text = (Convert.ToInt32(ds.Tables[0].Rows[0]["visitcharges"])).ToString();
            if (txt_VisitCharges.Text == "0")
            {
                txt_VisitCharges.Text = "";
            }
            txt_ZipCode.Text = ds.Tables[0].Rows[0]["zip"].ToString();
            ddl_States.SelectedValue = ds.Tables[0].Rows[0]["state"].ToString();
            ddl_BondType.SelectedValue = ds.Tables[0].Rows[0]["bondtype"].ToString();
            ddl_SettingRequest.SelectedValue = ds.Tables[0].Rows[0]["settingrequesttype"].ToString();
            ddl_casetype.SelectedValue = ds.Tables[0].Rows[0]["CaseTypeId"].ToString();  //Noufil 4237 06/28/2008 Get case type
            this.ddl_status.SelectedValue = ds.Tables[0].Rows[0]["inactiveflag"].ToString();
            //Sabir Khan 5941 07/23/2009 Set county name...
            ddl_CountyName.SelectedValue = ds.Tables[0].Rows[0]["CountyId"].ToString();

            
            //if(ds.Tables[0].Rows[0]["inactiveflag"].ToString() == "1")
            //{

            //    chkb_InActive.Checked = true;
            //}
            //else
            //{
            //    chkb_InActive.Checked=false;
            //}

            //if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsCriminalCourt"]) == true)
            //{
            //    chkcriminal.Checked = true;
            //}
            //else
            //{
            //    chkcriminal.Checked = false;
            //}
            //added by ozair
            //kamran 3536 04/15/08 change date formate
            //Asad Ali 8153 09/20/2010 set is ALR Process value in hidden field 
            hf_IsALRCourt.Value =(Convert.ToBoolean(ds.Tables[0].Rows[0]["ALRProcess"])==true?"1":"0");
            //Asad Ali 8153 09/20/2010 Set associated court
            if (ds.Tables[0].Rows[0]["CaseTypeId"].ToString().Equals("2") == true && hf_IsALRCourt.Value=="0")
            {
                if (ds.Tables[0].Rows[0]["AssociatedALRCourtID"] != null && ds.Tables[0].Rows[0]["AssociatedALRCourtID"].ToString() != "0")
                {
                    ddl_AssociatedCourts.SelectedValue = ds.Tables[0].Rows[0]["AssociatedALRCourtID"].ToString();
                }
                tr_AssociatedALRCourt.Style[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                tr_AssociatedALRCourt.Style[HtmlTextWriterStyle.Display] = "none";
            }
            
            if (ds.Tables[0].Rows[0]["RepDate"].ToString() != "")
            {
                if (Convert.ToDateTime(ds.Tables[0].Rows[0]["RepDate"]).ToShortDateString() != "1/1/1900")
                {
                    dtpCourtDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["RepDate"]);
                    // Noufil 4945 10/14/2008 set time
                    string ShortTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["rdate"]).ToShortTimeString();
                    txt_hour.Text = ShortTime.Substring(0, (ShortTime.IndexOf(":")));
                    txt_min.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["rdate"]).Minute.ToString();
                    if ((txt_hour.Text.Length == 1))
                        txt_hour.Text = "0" + txt_hour.Text;
                    if ((txt_min.Text.Length == 1))
                        txt_min.Text = "0" + txt_min.Text;
                    dd_time.SelectedValue = (ds.Tables[0].Rows[0]["rdate"].ToString().Contains("AM") ? "AM" : "PM");
                }
                else
                {
                    dtpCourtDate.SelectedDate = DateTime.Today;
                    //Ozair 4945 10/14/2008 set default time
                    txt_hour.Text = "01";
                    txt_min.Text = "00";
                    dd_time.SelectedValue = "PM";
                }
            }
            else
            {
                dtpCourtDate.SelectedDate = DateTime.Now;
                //Ozair 4945 10/14/2008 set default time
                txt_hour.Text = "01";
                txt_min.Text = "00";
                dd_time.SelectedValue = "PM";
            }

           
            //Muhammad Muneer 8227 09/27/2010 Add the functionality for the LORRepDate
            if (lblcourtID.Text == "3004")
            {
                if (ds.Tables[0].Rows[0]["LORRepDate2"].ToString() != "")
                {
                    if (Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate2"]).ToShortDateString() != "1/1/1900")
                    {
                        dtpCourtDate2.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate2"]);
                        // Noufil 4945 10/14/2008 set time
                        string ShortTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate2"]).ToShortTimeString();
                        txt_hour2.Text = ShortTime.Substring(0, (ShortTime.IndexOf(":")));
                        txt_min2.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate2"]).Minute.ToString();
                        if ((txt_hour2.Text.Length == 1))
                            txt_hour2.Text = "0" + txt_hour2.Text;
                        if ((txt_min2.Text.Length == 1))
                            txt_min2.Text = "0" + txt_min2.Text;
                        dd_time2.SelectedValue = (ds.Tables[0].Rows[0]["LORRepDate2"].ToString().Contains("AM") ? "AM" : "PM");
                    }
                    else
                    {
                        dtpCourtDate2.SelectedDate = DateTime.Today;
                        //Ozair 4945 10/14/2008 set default time
                        txt_hour2.Text = "01";
                        txt_min2.Text = "00";
                        dd_time2.SelectedValue = "PM";
                    }
                }
                else
                {
                    dtpCourtDate2.SelectedDate = DateTime.Now;
                    //Ozair 4945 10/14/2008 set default time
                    txt_hour2.Text = "01";
                    txt_min2.Text = "00";
                    dd_time2.SelectedValue = "PM";
                }

                //Muhammad Muneer 8227 09/27/2010 Add the functionality for the LORRepDate
                if (ds.Tables[0].Rows[0]["LORRepDate3"].ToString() != "")
                {
                    if (Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate3"]).ToShortDateString() != "1/1/1900")
                    {
                        dtpCourtDate3.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate3"]);
                        // Noufil 4945 10/14/2008 set time
                        string ShortTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate3"]).ToShortTimeString();
                        txt_hour3.Text = ShortTime.Substring(0, (ShortTime.IndexOf(":")));
                        txt_min3.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate3"]).Minute.ToString();
                        if ((txt_hour3.Text.Length == 1))
                            txt_hour3.Text = "0" + txt_hour3.Text;
                        if ((txt_min3.Text.Length == 1))
                            txt_min3.Text = "0" + txt_min3.Text;
                        dd_time3.SelectedValue = (ds.Tables[0].Rows[0]["LORRepDate3"].ToString().Contains("AM") ? "AM" : "PM");
                    }
                    else
                    {
                        dtpCourtDate3.SelectedDate = DateTime.Today;
                        //Ozair 4945 10/14/2008 set default time
                        txt_hour3.Text = "01";
                        txt_min3.Text = "00";
                        dd_time3.SelectedValue = "PM";
                    }
                }
                else
                {
                    dtpCourtDate3.SelectedDate = DateTime.Now;
                    //Ozair 4945 10/14/2008 set default time
                    txt_hour3.Text = "01";
                    txt_min3.Text = "00";
                    dd_time3.SelectedValue = "PM";
                }



                //Muhammad Muneer 8227 09/27/2010 Add the functionality for the LORRepDate
                if (ds.Tables[0].Rows[0]["LORRepDate4"].ToString() != "")
                {
                    if (Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate4"]).ToShortDateString() != "1/1/1900")
                    {
                        dtpCourtDate4.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate4"]);
                        // Noufil 4945 10/14/2008 set time
                        string ShortTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate4"]).ToShortTimeString();
                        txt_hour4.Text = ShortTime.Substring(0, (ShortTime.IndexOf(":")));
                        txt_min4.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate4"]).Minute.ToString();
                        if ((txt_hour4.Text.Length == 1))
                            txt_hour4.Text = "0" + txt_hour4.Text;
                        if ((txt_min4.Text.Length == 1))
                            //Asad Ali 8227 12/23/2010 fix time exception issue
                            txt_min4.Text = "0" + txt_min4.Text;
                        dd_time4.SelectedValue = (ds.Tables[0].Rows[0]["LORRepDate4"].ToString().Contains("AM") ? "AM" : "PM");
                    }
                    else
                    {
                        dtpCourtDate4.SelectedDate = DateTime.Today;
                        //Ozair 4945 10/14/2008 set default time
                        txt_hour4.Text = "01";
                        txt_min4.Text = "00";
                        dd_time4.SelectedValue = "PM";
                    }
                }
                else
                {
                    dtpCourtDate4.SelectedDate = DateTime.Now;
                    //Ozair 4945 10/14/2008 set default time
                    txt_hour4.Text = "01";
                    txt_min4.Text = "00";
                    dd_time4.SelectedValue = "PM";
                }


                //Muhammad Muneer 8227 09/27/2010 Add the functionality for the LORRepDate
                if (ds.Tables[0].Rows[0]["LORRepDate5"].ToString() != "")
                {
                    if (Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate5"]).ToShortDateString() != "1/1/1900")
                    {
                        dtpCourtDate5.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate5"]);
                        // Noufil 4945 10/14/2008 set time
                        string ShortTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate5"]).ToShortTimeString();
                        txt_hour5.Text = ShortTime.Substring(0, (ShortTime.IndexOf(":")));
                        txt_min5.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate5"]).Minute.ToString();
                        if ((txt_hour5.Text.Length == 1))
                            txt_hour5.Text = "0" + txt_hour5.Text;
                        if ((txt_min5.Text.Length == 1))
                            txt_min5.Text = "0" + txt_min5.Text;
                        dd_time5.SelectedValue = (ds.Tables[0].Rows[0]["LORRepDate5"].ToString().Contains("AM") ? "AM" : "PM");
                    }
                    else
                    {
                        dtpCourtDate5.SelectedDate = DateTime.Today;
                        //Ozair 4945 10/14/2008 set default time
                        txt_hour5.Text = "01";
                        txt_min5.Text = "00";
                        dd_time5.SelectedValue = "PM";
                    }
                }
                else
                {
                    dtpCourtDate5.SelectedDate = DateTime.Now;
                    //Ozair 4945 10/14/2008 set default time
                    txt_hour5.Text = "01";
                    txt_min5.Text = "00";
                    dd_time5.SelectedValue = "PM";
                }




                //Muhammad Muneer 8227 09/27/2010 Add the functionality for the LORRepDate
                if (ds.Tables[0].Rows[0]["LORRepDate6"].ToString() != "")
                {
                    if (Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate6"]).ToShortDateString() != "1/1/1900")
                    {
                        dtpCourtDate6.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate6"]);
                        // Noufil 4945 10/14/2008 set time
                        string ShortTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate6"]).ToShortTimeString();
                        txt_hour6.Text = ShortTime.Substring(0, (ShortTime.IndexOf(":")));
                        txt_min6.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["LORRepDate6"]).Minute.ToString();
                        if ((txt_hour6.Text.Length == 1))
                            txt_hour6.Text = "0" + txt_hour6.Text;
                        if ((txt_min6.Text.Length == 1))
                            txt_min6.Text = "0" + txt_min6.Text;
                        dd_time6.SelectedValue = (ds.Tables[0].Rows[0]["LORRepDate6"].ToString().Contains("AM") ? "AM" : "PM");
                    }
                    else
                    {
                        dtpCourtDate6.SelectedDate = DateTime.Today;
                        //Ozair 4945 10/14/2008 set default time
                        txt_hour6.Text = "01";
                        txt_min6.Text = "00";
                        dd_time6.SelectedValue = "PM";
                    }
                }
                else
                {
                    dtpCourtDate6.SelectedDate = DateTime.Now;
                    //Ozair 4945 10/14/2008 set default time
                    txt_hour6.Text = "01";
                    txt_min6.Text = "00";
                    dd_time6.SelectedValue = "PM";
                }

            }


            //added by khalid 2-1-08 for duplicate cause no check box
            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["CausenoDuplicate"]) == true)
            {
                chk_duplicateCauseno.Checked = true;
            }
            else
            {
                chk_duplicateCauseno.Checked = false;
            }

            ddlCaseIdentification.SelectedValue = ds.Tables[0].Rows[0]["CaseIdentifiaction"].ToString();
            ddlAcceptAppeals.SelectedValue = ds.Tables[0].Rows[0]["AcceptAppeals"].ToString();
            txtAppealLocation.Text = ds.Tables[0].Rows[0]["AppealLocation"].ToString();
            ddlInitialSetting.SelectedValue = ds.Tables[0].Rows[0]["InitialSetting"].ToString();
            ddlLOR.SelectedValue = ds.Tables[0].Rows[0]["LOR"].ToString();
            txtAppearanceHire.Text = ds.Tables[0].Rows[0]["AppearnceHire"].ToString();
            txtJugeTrial.Text = ds.Tables[0].Rows[0]["JudgeTrialHire"].ToString();
            txtJuryTrial.Text = ds.Tables[0].Rows[0]["JuryTrialHire"].ToString();
            rblHandwriting.SelectedValue = ds.Tables[0].Rows[0]["HandwritingAllowed"].ToString();
            rblFTAIssues.SelectedValue = ds.Tables[0].Rows[0]["AdditionalFTAIssued"].ToString();
            rblGracePeriod.SelectedValue = ds.Tables[0].Rows[0]["GracePeriod"].ToString();
            //Nasir 5310 12/24/2008 Bind text to radio ALRProcess
            rblALRProcess.SelectedValue = Convert.ToInt32(ds.Tables[0].Rows[0]["ALRProcess"]).ToString();
            txtContinuance.Text = ds.Tables[0].Rows[0]["Continuance"].ToString();
            rblSupportingDoc.SelectedValue = ds.Tables[0].Rows[0]["SupportingDocument"].ToString();
            txtComments.Text = ds.Tables[0].Rows[0]["Comments"].ToString();
            cb_rep.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsletterofRep"]);
            //Sabir Khan 5763 04/10/2009 
            chkIsLorSubpoena.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsLORSubpoena"]);
            chkIsLorMod.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsLORMOD"]);
            //Asad Ali 8153 09/21/2010 rollback task 7369 as per ozair 
            //Nasir 7369 02/19/2010 added AllowOnlineSignUp
            //rbtAllowOnlineSignUp.SelectedValue = Convert.ToInt32(ds.Tables[0].Rows[0]["AllowOnlineSignUp"]).ToString(); ;
            //Muhammad Muneer 8227 09/28/2010 Added the new functionality to support LOR Dates

            // Rab Nawaz Khan 8997 10/18/2011 Allow court room numbers logic
            DataTable dt_test = ClsCourts.CheckCourtAllowanceStatus(Convert.ToInt32(ds.Tables[0].Rows[0]["courtid"].ToString()));
            chkAllowCourtNumbers.Checked = Convert.ToBoolean(dt_test.Rows[0]["AllowCourtNumbers"].ToString());
            chkAllowCourtNumbers.Visible = Convert.ToBoolean(dt_test.Rows[0]["ShowCourtRoomNumbers"].ToString());


            if (lblcourtID.Text != "3004")
            {
                if (cb_rep.Checked == true)
                {
                    tr_rep.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_rep2.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep3.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep4.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep5.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep6.Style[HtmlTextWriterStyle.Display] = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    tr_rep1.Style[HtmlTextWriterStyle.Display] = "block";
                    // End 8870
                }
                else
                {
                    tr_rep.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep2.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep3.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep4.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep5.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep6.Style[HtmlTextWriterStyle.Display] = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    tr_rep1.Style[HtmlTextWriterStyle.Display] = "none";
                    // End 8870


                }
            }
            else
            {
                if (cb_rep.Checked == true)
                {
                    tr_rep.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_rep2.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_rep3.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_rep4.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_rep5.Style[HtmlTextWriterStyle.Display] = "block";
                    tr_rep6.Style[HtmlTextWriterStyle.Display] = "block";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    tr_rep1.Style[HtmlTextWriterStyle.Display] = "block";
                    // End 8870
                
                }
                else
                {
                    tr_rep.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep2.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep3.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep4.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep5.Style[HtmlTextWriterStyle.Display] = "none";
                    tr_rep6.Style[HtmlTextWriterStyle.Display] = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    tr_rep1.Style[HtmlTextWriterStyle.Display] = "none";
                    // End 8870
                }
            }

                FillCourtFilesGrid();
            
            btn_Delete.Enabled = true;
        
        }
    }
}

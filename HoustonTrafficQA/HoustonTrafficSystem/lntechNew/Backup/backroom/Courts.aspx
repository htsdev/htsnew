<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.Courts" CodeBehind="Courts.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="gw" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html>
<head>
    <title>Courts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script language="javascript" type="text/javascript">

        //Kazim 4071 5/22/2008 Write the code to handle the enter key 

        function KeyDownHandler() {
            // process only the Enter key
            if (event.keyCode == 13) {

                event.returnValue = false;
                event.cancel = true;
                document.getElementById("btn_submit").click();
            }
        }

        //Kazim 3697 5/6/2008 Used to open popup 

        function OpenPopUpNew(path)  //(var path,var name)
        {

            window.open(path, '', "height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
            return false;
        }

        function Submit() {

            var ctrl = null;

            // Get control.

            ctrl = $find('CBEGeneralInfo');
            if (!ctrl)
                return;

            if (frmcourts.txt_CourtName.value.trim() == "") {

                alert("Please specify Court Name");
                ctrl._doOpen();
                frmcourts.txt_CourtName.focus();
                return false;
            }

            if (frmcourts.txt_JudgeName.value.trim() == "") {
                alert("Please specify Judge Name");
                ctrl._doOpen();
                frmcourts.txt_JudgeName.focus();
                return false;
            }

            if (frmcourts.txt_ShortName.value.trim() == "") {
                alert("Please specify Court Short Name");
                ctrl._doOpen();
                frmcourts.txt_ShortName.focus();
                return false;
            }

            if (frmcourts.txt_Address1.value.trim() == "") {
                alert("Please specify your Address");
                ctrl._doOpen();
                frmcourts.txt_Address1.focus();
                return false;
            }

            if (frmcourts.txt_City.value.trim() == "") {
                alert("Please specify Court City");
                ctrl._doOpen();
                frmcourts.txt_City.focus();
                return false;
            }

            if (frmcourts.ddl_States.value == 0) {
                alert("Please specify Court State");
                ctrl._doOpen();
                frmcourts.ddl_States.focus();
                return false;
            }

            if (frmcourts.txt_ZipCode.value.trim() == "") {
                alert("Please specify Court Zip");
                ctrl._doOpen();
                frmcourts.txt_ZipCode.focus();
                return false;
            }

            if (frmcourts.txt_DDName.value.trim() == "") {
                alert("Please specify Drop Down Name");
                ctrl._doOpen();
                frmcourts.txt_DDName.focus();
                return false;
            }

            //		if (document.getElementById("txt_hour").value.trim()=="" )
            //		{
            //		    alert("Please add LOR Time");
            //		    document.getElementById("txt_hour").focus();
            //		    ctrl._doOpen();
            //			return false;
            //		}
            //		if (document.getElementById("txt_min").value.trim()=="")
            //		{
            //		    alert("Please add LOR Minutes ");
            //		    document.getElementById("txt_min").focus();
            //		    ctrl._doOpen();
            //			return false;
            //		}


            //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina 
            if (document.getElementById("txt_CourtName").value == "Pasadena Municipal Court") {
                //Muhammad ALi 8227 10/06/2010 Add Validation for "." cout time and hours 
                if ((isNaN(document.getElementById("txt_hour").value.trim()) == true) || ((document.getElementById("txt_hour").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour6").value.trim()) == true) || ((document.getElementById("txt_hour6").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour5").value.trim()) == true) || ((document.getElementById("txt_hour5").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour4").value.trim()) == true) || ((document.getElementById("txt_hour4").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour3").value.trim()) == true) || ((document.getElementById("txt_hour3").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_hour2").value.trim()) == true) || ((document.getElementById("txt_hour2").value).indexOf(".") != -1)) {
                    alert("Invalid Time. Please use numbers");
                    ctrl._doOpen();
                    return false;
                }
                if ((isNaN(document.getElementById("txt_min").value.trim()) == true) || ((document.getElementById("txt_min").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min6").value.trim()) == true) || ((document.getElementById("txt_min6").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min5").value.trim()) == true) || ((document.getElementById("txt_min5").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min4").value.trim()) == true) || ((document.getElementById("txt_min4").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min3").value.trim()) == true) || ((document.getElementById("txt_min3").value).indexOf(".") != -1) || (isNaN(document.getElementById("txt_min2").value.trim()) == true) || ((document.getElementById("txt_min2").value).indexOf(".") != -1)) {
                    alert("Invalid Time. Please use numbers");
                    ctrl._doOpen();
                    return false;
                }
                //End Muhammad Ali 8227

                if (document.getElementById("cb_rep").checked == true) {


                    if (document.getElementById("dtpCourtDate").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        document.getElementById("dtpCourtDate").focus();
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate2").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate3").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate4").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate5").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }

                    else if (document.getElementById("dtpCourtDate6").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        ctrl._doOpen();
                        return false;
                    }
                    else
                    { }

                    if ((document.getElementById("txt_hour").value.trim() > 12) || (document.getElementById("txt_hour").value.trim() == 0) || (document.getElementById("txt_hour").value.trim() < 0) && (document.getElementById("dtpCourtDate").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour2").value.trim() > 12) || (document.getElementById("txt_hour2").value.trim() == 0) || (document.getElementById("txt_hour2").value.trim() < 0) && (document.getElementById("dtpCourtDate2").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour3").value.trim() > 12) || (document.getElementById("txt_hour3").value.trim() == 0) || (document.getElementById("txt_hour3").value.trim() < 0) && (document.getElementById("dtpCourtDate3").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour4").value.trim() > 12) || (document.getElementById("txt_hour4").value.trim() == 0) || (document.getElementById("txt_hour4").value.trim() < 0) && (document.getElementById("dtpCourtDate4").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour5").value.trim() > 12) || (document.getElementById("txt_hour5").value.trim() == 0) || (document.getElementById("txt_hour5").value.trim() < 0) && (document.getElementById("dtpCourtDate5").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_hour6").value.trim() > 12) || (document.getElementById("txt_hour6").value.trim() == 0) || (document.getElementById("txt_hour6").value.trim() < 0) && (document.getElementById("dtpCourtDate6").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        ctrl._doOpen();
                        return false;
                    }
                    else
                    { }

                    if ((document.getElementById("txt_min").value.trim() > 59) || (document.getElementById("txt_min").value.trim() < 0) || (document.getElementById("txt_min").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min2").value.trim() > 59) || (document.getElementById("txt_min2").value.trim() < 0) || (document.getElementById("txt_min2").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min3").value.trim() > 59) || (document.getElementById("txt_min3").value.trim() < 0) || (document.getElementById("txt_min3").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min4").value.trim() > 59) || (document.getElementById("txt_min4").value.trim() < 0) || (document.getElementById("txt_min4").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min5").value.trim() > 59) || (document.getElementById("txt_min5").value.trim() < 0) || (document.getElementById("txt_min5").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("txt_min6").value.trim() > 59) || (document.getElementById("txt_min6").value.trim() < 0) || (document.getElementById("txt_min6").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        ctrl._doOpen();
                        return false;
                    }
                    else
                    { }

                    if ((document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;

                    }
                    else if ((document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate2").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate3").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate5").value.trim()) || (document.getElementById("dtpCourtDate4").value.trim() == document.getElementById("dtpCourtDate6").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate6").value.trim()) || (document.getElementById("dtpCourtDate5").value.trim() == document.getElementById("dtpCourtDate2").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else if ((document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate2").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate3").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate4").value.trim()) || (document.getElementById("dtpCourtDate6").value.trim() == document.getElementById("dtpCourtDate5").value.trim())) {
                        alert("Date cannot be same. Please check all the dates");
                        ctrl._doOpen();
                        return false;
                    }
                    else
                    { }



                    //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina 
                    //Muhammad Ali 8227  10/06/2010 Correct Alert Msg Language...
                    if (document.getElementById("txt_CourtName").value == "Pasadena Municipal Court") {
                        if (validateIsWorkingDay('dtpCourtDate') == false) {
                            alert("Please select any business day for LOR date");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate2') == false) {
                            alert("Please select any business day for LOR date 2");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate3') == false) {
                            alert("Please select any business day for LOR date 3");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate4') == false) {
                            alert("Please select any business day for LOR date 4");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate5') == false) {
                            alert("Please select any business day for LOR date 5");
                            return false;
                        }
                        if (validateIsWorkingDay('dtpCourtDate6') == false) {
                            alert("Please select any business day for LOR date 6");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate') == false) {
                            alert("Please select LOR date in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate2') == false) {
                            alert("Please select LOR date 2 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate3') == false) {
                            alert("Please select LOR date 3 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate4') == false) {
                            alert("Please select LOR date 4 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate5') == false) {
                            alert("Please select LOR date 5 in future");
                            return false;
                        }
                        if (validateIsFutureDate('dtpCourtDate6') == false) {
                            alert("Please select LOR date 6 in future");
                            return false;
                        }
                    }
                }

            }
            else {
                if (isNaN(document.getElementById("txt_hour").value.trim()) == true) {
                    alert("Invalid Time. Please use numbers");
                    document.getElementById("txt_hour").focus();
                    ctrl._doOpen();
                    return false;
                }
                if (isNaN(document.getElementById("txt_min").value.trim()) == true) {
                    alert("Invalid Time. Please use numbers");
                    document.getElementById("txt_min").focus();
                    ctrl._doOpen();
                    return false;
                }

                //		if ((document.getElementById("txt_hour").value.trim()>0)||(document.getElementById("txt_hour").value.trim()<0)&&(document.getElementById("dtpCourtDate").value ==""))
                //		{
                //		    alert("Invalid Time. Please make time 0 if no letter of Rep date selected");
                //		    document.getElementById("txt_hour").focus();
                //		    ctrl._doOpen();
                //			return false;
                //		}


                if (document.getElementById("cb_rep").checked == true) {
                    if (document.getElementById("dtpCourtDate").value.trim() == "") {
                        alert("Invalid Date. Please enter date.");
                        document.getElementById("dtpCourtDate").focus();
                        ctrl._doOpen();
                        return false;
                    }

                    if ((document.getElementById("txt_hour").value.trim() > 12) || (document.getElementById("txt_hour").value.trim() == 0) || (document.getElementById("txt_hour").value.trim() < 0) && (document.getElementById("dtpCourtDate").value != "")) {
                        alert("Invalid Time. Hour should between 1 and 12.");
                        document.getElementById("txt_hour").focus();
                        ctrl._doOpen();
                        return false;
                    }

                    if ((document.getElementById("txt_min").value.trim() > 59) || (document.getElementById("txt_min").value.trim() < 0) || (document.getElementById("txt_min").value.trim() == "")) {
                        alert("Invalid Time. Minutes should between 0 and 59.");
                        document.getElementById("txt_min").focus();
                        ctrl._doOpen();
                        return false;
                    }
                }
            }
            var intphonenum1 = frmcourts.txt_CC11.value;
            var intphonenum2 = frmcourts.txt_CC12.value;
            var intphonenum3 = frmcourts.txt_CC13.value;
            var intphonenum4 = frmcourts.txt_CC14.value;

            if ((intphonenum1 == "") || (intphonenum2 == "") || (intphonenum3 == "")) {
                alert("Invalid Phone Number. Please don't use any dashes or space.");
                ctrl._doOpen();
                frmcourts.txt_CC11.focus();
                return false;
            }

            if (isNaN(intphonenum1) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC11.focus();
                return false;
            }
            if (isNaN(intphonenum2) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC12.focus();
                return false;
            }
            if (isNaN(intphonenum3) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC13.focus();
                return false;
            }
            if (intphonenum4 != "" && isNaN(intphonenum4) == true) {
                alert("Invalid Phone Number. Please don't use any dashes or space");
                return false;
            }

            var phonelength = intphonenum1.length + intphonenum2.length + intphonenum3.length + intphonenum4.length

            if (phonelength < 10) {
                alert("Invalid Phone Number");
                ctrl._doOpen();
                frmcourts.txt_CC11.focus();
                return false;
            }

            var intphonenum1 = frmcourts.txt_CC1.value;
            var intphonenum2 = frmcourts.txt_CC2.value;
            var intphonenum3 = frmcourts.txt_CC3.value;

            if (isNaN(intphonenum1) == true) {
                alert("Invalid Fax Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC1.focus();
                return false;
            }
            if (isNaN(intphonenum2) == true) {
                alert("Invalid Fax Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC2.focus();
                return false;
            }
            if (isNaN(intphonenum3) == true) {
                alert("Invalid Fax Number. Please don't use any dashes or space");
                ctrl._doOpen();
                frmcourts.txt_CC3.focus();
                return false;
            }

            var faxlength = intphonenum1.length + intphonenum2.length + intphonenum3.length;

            if (faxlength < 10) {
                alert("Invalid Fax Number");
                ctrl._doOpen();
                frmcourts.txt_CC1.focus();
                return false;
            }

            if (frmcourts.ddl_SettingRequest.value == 0) {
                alert("Please specify Setting Request");
                ctrl._doOpen();
                frmcourts.ddl_SettingRequest.focus();
                return false;
            }

            if (frmcourts.ddl_BondType.value == 0) {
                alert("Please specify Bond Type");
                ctrl._doOpen();
                frmcourts.ddl_BondType.focus();
                return false;
            }

            if (isNaN(frmcourts.txt_VisitCharges.value) == true) {
                alert("Sorry Invalid Additional Amount");
                frmcourts.txt_VisitCharges.focus();
                ctrl._doOpen();
                return false;
            }
            if (frmcourts.ddl_status.value == -1) {
                alert("Please specify Status");
                frmcourts.ddl_status.focus();
                ctrl._doOpen();
                return false;
            }

            //Sabir Khan 6000 06/04/2009 Validation for court name...				
            var regex = /^[0-9a-zA-Z\s()-.:,]+$/;
            if (!(regex.test(document.getElementById("txt_CourtName").value))) {
                alert("Court name can only contain alpha numeric and following special characters:\n(  ) : . -  , ");
                document.getElementById("txt_CourtName").focus();
                return false;
            }
            else {
                var crtname = document.getElementById("txt_CourtName").value;
                var count = crtname.length;
                for (var i = 0; i < count; i++) {
                    if (crtname.charAt(i) == '+' || crtname.charAt(i) == '*') {
                        alert("Court name can only contain alpha numeric and following special characters:\n(  ) : . -  , ");
                        document.getElementById("txt_CourtName").focus();
                        return false;
                    }
                }
            }

            //Sabir Khan 5941 07/24/2009 check for county Name.
            if (frmcourts.ddl_CountyName.value == 0) {
                alert("Please specify County Name");
                ctrl._doOpen();
                frmcourts.ddl_CountyName.focus();
                return false;
            }




            //		
            /*
            ctrl = $find('CBECaseIdentification');   
            if (!ctrl)
            return;
		
		if (frmcourts.rblSupportingDoc_0.checked !=true && frmcourts.rblSupportingDoc_1.checked !=true) 
            {
            alert("Please specify Supporting Document");
            ctrl._doOpen();
            frmcourts.rblSupportingDoc_0.focus();
            return false;
            }
		
		if (frmcourts.rblFTAIssues_0.checked !=true && frmcourts.rblFTAIssues_1.checked !=true) 
            {
            alert("Please specify Additonal FTA Isuues");
            ctrl._doOpen();
            frmcourts.rblFTAIssues_0.focus();
            return false;
            }		
		         
            if (frmcourts.rblHandwriting_0.checked !=true && frmcourts.rblHandwriting_1.checked !=true) 
            {
            alert("Please specify HandWriting Allowed");
            ctrl._doOpen();
            frmcourts.rblHandwriting_0.focus();
            return false;
            }
		
		if (frmcourts.rblGracePeriod_0.checked !=true && frmcourts.rblGracePeriod_1.checked !=true) 
            {
            alert("Please specify Grace Period");
            ctrl._doOpen();
            frmcourts.rblGracePeriod_0.focus();
            return false;
            }
		
		if (frmcourts.ddlCaseIdentification.value == -1) 
            {
            alert("Please specify Case Identification");
            ctrl._doOpen();
            frmcourts.ddlCaseIdentification.focus();
            return false;
            }
		
		if (frmcourts.ddlAcceptAppeals.value == -1) 
            {
            alert("Please specify Accept Appeals");
            ctrl._doOpen();
            frmcourts.ddlAcceptAppeals.focus();
            return false;
            }
		
		if (frmcourts.ddlInitialSetting.value == -1) 
            {
            alert("Please specify Initial Setting");
            ctrl._doOpen();
            frmcourts.ddlInitialSetting.focus();
            return false;
            }
		
		if (frmcourts.ddlLOR.value == -1) 
            {
            alert("Please specify LOR Method");
            ctrl._doOpen();
            frmcourts.ddlLOR.focus();
            return false;
            }*/


        }

        function CommentsLength() {
            if (frmcourts.txtComments.value.length > 1000) {
                alert("Sorry You cannot type in more than 1000 characters in Comments");
                return false;
            }

        }



        function validateIsFutureDate(var_date) {
            var txtLORDate = document.getElementById(var_date);
            var today = new Date();
            var LORDate = new Date(txtLORDate.value);

            if (LORDate < today) {

                txtLORDate.focus();
                return false;
            }

        }

        function validateIsWorkingDay(var_date) {
            var txtLORDate = document.getElementById(var_date).value;
            var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
            var newseldatestr = formatDate((Date.parseInvariant(txtLORDate).getMonth() + 1) + "/" + Date.parseInvariant(txtLORDate).getDate() + "/" + Date.parseInvariant(txtLORDate).getFullYear(), "MM/dd/yyyy");
            newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
            if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {

                return false;
            }

        }

        function OpenGeneralInfoSectionPanel() {

            ctrl = $find('CBEGeneralInfo');
            if (!ctrl)
                return;
            ctrl._doOpen();



            ctrl = $find('CBECaseIdentification');
            if (!ctrl)
                return;
            ctrl._doClose();

        }

        function ResetControls(type) {

            document.getElementById("divDocumentDetails").style.height = "10px";
            OpenGeneralInfoSectionPanel();
            document.getElementById("txt_CourtName").value = "";
            document.getElementById("txt_JudgeName").value = "";
            document.getElementById("txt_ShortName").value = "";
            document.getElementById("txt_Address1").value = "";
            document.getElementById("txt_Address2").value = "";
            document.getElementById("txt_City").value = "";
            document.getElementById("ddl_States").selectedIndex = 0;
            document.getElementById("txt_ZipCode").value = "";
            document.getElementById("txt_DDName").value = "";
            document.getElementById("dd_time").selectedIndex = 0;
            document.getElementById("chk_duplicateCauseno").checked = false;
            document.getElementById("txt_CourtContact").value = "";
            document.getElementById("txt_CC11").value = "";
            document.getElementById("txt_CC12").value = "";
            document.getElementById("txt_CC13").value = "";
            document.getElementById("txt_CC14").value = "";
            document.getElementById("txt_CC1").value = "";
            document.getElementById("txt_CC2").value = "";
            document.getElementById("txt_CC3").value = "";
            document.getElementById("ddl_SettingRequest").selectedIndex = 0;
            document.getElementById("ddl_BondType").selectedIndex = 0;
            document.getElementById("txt_VisitCharges").value = "";
            document.getElementById("ddl_status").value = -1;
            document.getElementById("cb_rep").checked = false;
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            curr_month++;
            var curr_year = d.getFullYear();
            document.getElementById("dtpCourtDate").value = curr_month + "/" + curr_date + "/" + curr_year;
            // Noufil 4945 10/16/2008 Set hour and minutes values
            document.getElementById("txt_hour").value = "01";
            document.getElementById("txt_min").value = "00";
            document.getElementById("dd_time")[1].selected = true;
            document.getElementById("tr_rep").style.display = "none";
            // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
            document.getElementById("tr_rep1").style.display = "none";
            // End 8870
            document.getElementById("tr_rep2").style.display = "none";
            document.getElementById("tr_rep3").style.display = "none";
            document.getElementById("tr_rep4").style.display = "none";
            document.getElementById("tr_rep5").style.display = "none";
            document.getElementById("tr_rep6").style.display = "none";
            document.getElementById("lbl_Msg").innerText = "";
            document.getElementById("ddlCaseIdentification").value = -1;
            document.getElementById("ddlAcceptAppeals").value = -1;
            document.getElementById("ddlInitialSetting").value = -1;
            document.getElementById("ddlLOR").value = -1;
            document.getElementById("rblHandwriting_0").checked = false;
            document.getElementById("rblHandwriting_1").checked = false;
            document.getElementById("rblFTAIssues_0").checked = false;
            document.getElementById("rblFTAIssues_1").checked = false;
            document.getElementById("rblGracePeriod_0").checked = false;
            document.getElementById("rblGracePeriod_1").checked = false;
            document.getElementById("rblSupportingDoc_0").checked = false;
            document.getElementById("rblSupportingDoc_1").checked = false;
            document.getElementById("txtAppealLocation").value = "";
            document.getElementById("txtAppearanceHire").value = "";
            document.getElementById("txtJugeTrial").value = "";
            document.getElementById("txtJuryTrial").value = "";
            document.getElementById("txtContinuance").value = "";
            document.getElementById("txtComments").value = "";

            //Sabir Khan 5763 04/22/2009 reset LOR Subpoena and LOR Motion for discovery.
            document.getElementById("chkIsLorSubpoena").checked = false;
            document.getElementById("chkIsLorMod").checked = false;

            if (document.getElementById("gvCourtFiles") != null) {
                document.getElementById("gvCourtFiles").style.display = "none";
            }
            document.getElementById("hfCourtid").value = "0";
            document.getElementById("btn_Delete").disabled = true;

            var modalPopupBehavior = $find('CBECaseIdentification');
            //Kazim 4087 5/21/2008 Set the expanded size of collapsible panel 
            modalPopupBehavior._expandedSize = "270";
            //Sabir Khan 5941 07/31/2009 Reset county drop down...
            document.getElementById("ddl_CountyName").selectedIndex = 0;
            //Asad Ali 8153 09/20/2010  check condition on reset
            ddl_CaseType_IndexChange();
            return false;
        }

        function showrep() {

            //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina
            if (document.getElementById("txt_CourtName").value != "Pasadena Municipal Court") {
                if (document.getElementById("cb_rep").checked == true) {
                    document.getElementById("tr_rep").style.display = "block";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "block";
                    // End 8870
                }
                else {
                    document.getElementById("tr_rep").style.display = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "none";
                    // End 8870
                }
            }
            else  //Muhammad Muneer 8227 09/27/2010 adding the new functionality for LOR Pasedina
            {
                if (document.getElementById("cb_rep").checked == true) {
                    document.getElementById("tr_rep").style.display = "block";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "block";
                    //End 8870
                    document.getElementById("tr_rep2").style.display = "block";
                    document.getElementById("tr_rep3").style.display = "block";
                    document.getElementById("tr_rep4").style.display = "block";
                    document.getElementById("tr_rep5").style.display = "block";
                    document.getElementById("tr_rep6").style.display = "block";
                    //tr_rep2
                }
                else {
                    document.getElementById("tr_rep").style.display = "none";
                    // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers
                    document.getElementById("tr_rep1").style.display = "none";
                    // End 8870
                    document.getElementById("tr_rep2").style.display = "none";
                    document.getElementById("tr_rep3").style.display = "none";
                    document.getElementById("tr_rep4").style.display = "none";
                    document.getElementById("tr_rep5").style.display = "none";
                    document.getElementById("tr_rep6").style.display = "none";

                }
                //  alert('This is Pasedena');
            }


        }
        //Asad Ali 8153 09/20/2010 on Case type dropdown index change 
        function ddl_CaseType_IndexChange() {
            var ddlCasetype = document.getElementById('<%=ddl_casetype.ClientID%>');
            var tr_AssociatedCourts = document.getElementById('<%=tr_AssociatedALRCourt.ClientID%>');
            var rblALRProcess = document.getElementById('rblALRProcess_0');
            // 8997
            var tr_allowCourtNumbers = document.getElementById('<%=tr_allowCourtNumbers.ClientID%>');

            if (ddlCasetype.value == "2" && rblALRProcess.checked == false) {
                tr_AssociatedCourts.style.display = "block";
            }
            else {
                tr_AssociatedCourts.style.display = "none";
            }
             // 8997 
             if (ddlCasetype.value != "1")
                tr_allowCourtNumbers.style.display = "none";
             else
                tr_allowCourtNumbers.style.display = "block";

        }
    
 
    
    
    
    
    
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onkeypress="KeyDownHandler()">
    <form id="frmcourts" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
        border="0">
        <tr>
            <td>
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" TabIndex="40"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_Msg" runat="server" CssClass="Label" ForeColor="Red" TabIndex="27"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="10">
            </td>
        </tr>
        <tr>
            <td>
                <aspnew:UpdatePanel ID="upnlcourt" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="clssubhead" valign="middle" align="left" background="../../images/subhead_bg.gif"
                                    style="height: 32px">
                                    <table style="width: 100%; height: 100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="clssubhead" style="width: 100px">
                                                &nbsp;Court Details
                                            </td>
                                            <td class="clssubhead" align="right">
                                                <asp:LinkButton ID="lbtnAddNewCourt" runat="server" Text="Add New Court" CssClass="clsbutton"
                                                    OnClientClick="return ResetControls();" />&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 144px">
                                    <table id="tblgrid" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:DataGrid ID="dg_Result" runat="server" CssClass="clsLeftPaddingTable" BorderStyle="None"
                                                    PageSize="20" AutoGenerateColumns="False" BorderColor="White" OnItemDataBound="dg_Result_ItemDataBound"
                                                    AllowSorting="True" OnSortCommand="dg_Result_SortCommand" Width="820px" TabIndex="92">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Court Name&lt;/u&gt;" SortExpression="courtname">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkbtnCourtNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ccourtname") %>'
                                                                    CommandName="CourtNo" TabIndex="93"></asp:LinkButton>
                                                                <asp:Label ID="lbl_CourtID" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.courtid") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Drop Down Name&lt;/u&gt;" SortExpression="shortcourtname">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label5" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.cshortcourtname")  %>'
                                                                    TabIndex="94"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Short Name&lt;/u&gt;" SortExpression="shortname">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblsname" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                                    TabIndex="95">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Setting Request&lt;/u&gt;" SortExpression="setreqdes">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.setreqdes") %>'
                                                                    TabIndex="96">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;InActive&lt;/u&gt;" SortExpression="inactiveflag">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInactive" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.inactiveflag") %>'
                                                                    TabIndex="97">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Case Type&lt;/u&gt;" SortExpression="casetypename">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_IsCriminalCourt" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.casetypename") %>'
                                                                    TabIndex="98">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="&lt;u&gt;Rep'sLetterDate&lt;/u&gt;" SortExpression="RepDate">
                                                            <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_letterofrep" runat="server" CssClass="Labelfrmmain" Text='<%# DataBinder.Eval(Container, "DataItem.RepDate","{0:d}") %>'
                                                                    TabIndex="99">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center">
                                                    </PagerStyle>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="pnlModalPopUp" runat="server">
                                                    <table border="1" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
                                                        border-top-color: navy; border-collapse: collapse; border-right-color: navy">
                                                        <tr>
                                                            <td valign="top">
                                                                <table class="clsLeftPaddingTable" width="700px">
                                                                    <tr>
                                                                        <td class="clssubhead" background="../images/subhead_bg.gif" style="height: 35px;">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="clssubhead">
                                                                                        &nbsp;Court Detail
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        &nbsp; &nbsp;<asp:LinkButton ID="lnkbtnCancelPopUp" runat="server">X</asp:LinkButton>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ajaxToolkit:CollapsiblePanelExtender ID="CBECaseIdentification" runat="Server" TargetControlID="pnlCaseIdentification"
                                                                                CollapsedSize="0" ExpandedSize="265" Collapsed="false" ExpandControlID="panelTitleCaseIdentification"
                                                                                CollapseControlID="panelTitleCaseIdentification" AutoCollapse="False" AutoExpand="False"
                                                                                CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgCaseInfo"
                                                                                CollapsedImage="../Images/collapse.jpg" ExpandedImage="../Images/expand.jpg" />
                                                                            <ajaxToolkit:CollapsiblePanelExtender ID="CBEGeneralInfo" runat="Server" TargetControlID="pnlGeneralInfo"
                                                                                CollapsedSize="0" ExpandedSize="250" Collapsed="True" ExpandControlID="panelTitleGeneralInfo"
                                                                                CollapseControlID="panelTitleGeneralInfo" AutoCollapse="False" AutoExpand="False"
                                                                                CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgGInfo"
                                                                                ExpandDirection="Vertical" CollapsedImage="../Images/collapse.jpg" ExpandedImage="../Images/expand.jpg" />
                                                                            <ajaxToolkit:ModalPopupExtender ID="MPECourtDetailPopup" runat="server" BackgroundCssClass="modalBackground"
                                                                                CancelControlID="lnkbtnCancelPopUp" PopupControlID="pnlModalPopUp" TargetControlID="lbtnAddNewCourt">
                                                                            </ajaxToolkit:ModalPopupExtender>
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                            <td>
                                                                            <asp:Panel ID="panelTitleGeneralInfo" runat="server" Height="0px" BackImageUrl="~/images/subhead_bg.gif"
                                                                                CssClass="clssubhead" Width="100%">
                                                                                <table style="height: 100%" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="clssubhead" valign="middle">
                                                                                            &nbsp;<asp:Image ID="imgGInfo" runat="server" ImageUrl="../images/collapse.jpg" Style="cursor: pointer;" />
                                                                                            General Information
                                                                                        </td>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="pnlGeneralInfo" runat="Server" Width="100%" Style="display: inherit;">
                                                                                <table class="clsLeftPaddingTable" width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                <td colspan = "4">&nbsp;</td>
                                                                                </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 125px; height: 25px;">
                                                                                            Court Name
                                                                                        </td>
                                                                                        <td style="width: 219px; height: 25px;">
                                                                                            <%--Yasir Kamal 5915 05/20/2009 Courtname textbox length increased. --%>
                                                                                            &nbsp;<asp:TextBox ID="txt_CourtName" runat="server" Width="200" CssClass="clsInputadministration"
                                                                                                MaxLength="50" TabIndex="1"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" style="width: 118px">
                                                                                            Court Contact
                                                                                        </td>
                                                                                        <td width="210" style="height: 25px">
                                                                                            <asp:TextBox ID="txt_CourtContact" runat="server" Width="188px" CssClass="clsInputadministration"
                                                                                                MaxLength="50" TabIndex="13"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px">
                                                                                            Judge Name
                                                                                        </td>
                                                                                        <td style="width: 219px">
                                                                                            &nbsp;<asp:TextBox ID="txt_JudgeName" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="50" TabIndex="2"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" style="width: 118px">
                                                                                            Telephone
                                                                                        </td>
                                                                                        <td style="height: 20px">
                                                                                            <asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                                Width="32px" CssClass="clsInputadministration" TabIndex="14"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                                Width="32px" CssClass="clsInputadministration" TabIndex="15"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                                Width="32px" CssClass="clsInputadministration" TabIndex="16"></asp:TextBox>
                                                                                            x
                                                                                            <asp:TextBox ID="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                                Width="40px" CssClass="clsInputadministration" TabIndex="17"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px">
                                                                                            Short Name
                                                                                        </td>
                                                                                        <td style="width: 219px">
                                                                                            &nbsp;<asp:TextBox ID="txt_ShortName" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="20" TabIndex="3"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" style="width: 118px">
                                                                                            Fax
                                                                                        </td>
                                                                                        <td style="height: 21px">
                                                                                            <asp:TextBox ID="txt_CC1" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                                Width="32px" CssClass="clsInputadministration" TabIndex="18"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txt_CC2" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                                Width="32px" CssClass="clsInputadministration" TabIndex="19"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txt_CC3" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                                Width="32px" CssClass="clsInputadministration" TabIndex="20"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px">
                                                                                            Address #1
                                                                                        </td>
                                                                                        <td class="style15" style="width: 219px">
                                                                                            &nbsp;<asp:TextBox ID="txt_Address1" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="100" TabIndex="4"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style40" style="width: 118px">
                                                                                            Setting Request
                                                                                        </td>
                                                                                        <td style="height: 21px">
                                                                                            <asp:DropDownList ID="ddl_SettingRequest" runat="server" Width="132px" CssClass="clsInputCombo"
                                                                                                TabIndex="21">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px">
                                                                                            Address #2
                                                                                        </td>
                                                                                        <td class="style16" style="width: 219px">
                                                                                           &nbsp;<asp:TextBox ID="txt_Address2" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="100" TabIndex="5"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style40" style="width: 118px">
                                                                                            Bond Type
                                                                                        </td>
                                                                                        <td style="height: 17px">
                                                                                            <asp:DropDownList ID="ddl_BondType" runat="server" Width="132px" CssClass="clsInputCombo"
                                                                                                Height="19px" TabIndex="22">
                                                                                                <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Original</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Bonds</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px">
                                                                                            City,State,Zip
                                                                                        </td>
                                                                                        <td class="style17" style="width: 219px">
                                                                                            &nbsp;<asp:TextBox ID="txt_City" runat="server" Width="76px" CssClass="clsInputadministration"
                                                                                                MaxLength="12" TabIndex="6"></asp:TextBox>&nbsp;
                                                                                            <asp:DropDownList ID="ddl_States" runat="server" Width="42px" CssClass="clsInputCombo"
                                                                                                Height="17px" TabIndex="7">
                                                                                            </asp:DropDownList>
                                                                                            &nbsp;
                                                                                            <asp:TextBox ID="txt_ZipCode" runat="server" Width="58px" CssClass="clsInputadministration"
                                                                                                MaxLength="12" TabIndex="8"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style40" style="width: 118px">
                                                                                            Status
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:DropDownList ID="ddl_status" runat="server" CssClass="clsInputCombo" Width="132px"
                                                                                                Height="19px" TabIndex="23">
                                                                                                <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                                                                <asp:ListItem Value="0">Active</asp:ListItem>
                                                                                                <asp:ListItem Value="1">InActive</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px; height: 25px;">
                                                                                            Drop Down Name
                                                                                        </td>
                                                                                        <td class="style13" style="width: 219px; height: 25px;">
                                                                                            &nbsp;<asp:TextBox ID="txt_DDName" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="50" TabIndex="9"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style40" style="width: 118px">
                                                                                            Case Type
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:DropDownList ID="ddl_casetype" runat="server" CssClass="clsInputCombo" Width="132px"
                                                                                                Height="19px" TabIndex="23" onchange="ddl_CaseType_IndexChange();" >
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px; height: 25px;">
                                                                                            <asp:CheckBox ID="cb_rep" runat="server" CssClass="Label" Text="Is Letter of Rep"
                                                                                                Width="130px" onclick="showrep();" />
                                                                                        </td>
                                                                                        <td style="width: 219px">
                                                                                            <asp:CheckBox ID="chk_duplicateCauseno" runat="server" CssClass="Label" TabIndex="12"
                                                                                                Text="Duplicate CauseNo" Width="135px" />
                                                                                        </td>
                                                                                        <td class="Label" style="width: 118px;" class="Label">
                                                                                            Additional ($)
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txt_VisitCharges" runat="server" CssClass="clsInputadministration"
                                                                                                MaxLength="5" TabIndex="24" Width="127px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px; height: 25px;">
                                                                                            
                                                                                        </td>
                                                                                        <td style="width: 219px">
                                                                                            
                                                                                        </td>
                                                                                       <%-- <td class="Label" style="width: 118px;" class="Label">
                                                                                            
                                                                                        </td>--%>
                                                                                        <td colspan = "2" id = "tr_allowCourtNumbers" runat = "server" >
                                                                                            <asp:CheckBox ID = "chkAllowCourtNumbers" runat = "server" CssClass ="Label" Text= "Allow Court Numbers" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%-- // Rab Nawaz Khan 8870 02/09/2011 included due to alignment issue in chrome and safari browsers --%>
                                                                                    <tr>
                                                                                        <td colspan="4" align="left">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr id = "tr_rep1" runat ="server" style="display: none;" align="left">

                                                                                        <td style="width: 130px;" class="Label" >
                                                                                            <asp:CheckBox ID="chkIsLorSubpoena" runat="server" CssClass="Label" Text="Is LOR Subpoena"
                                                                                               />
                                                                                        </td>
                                                                                        <td class="Label" style ="width : 220px;">
                                                                                            <asp:CheckBox ID="chkIsLorMod" runat="server" CssClass="Label" Text="Is LOR Motion for Discovery"/>
                                                                                        </td>
                                                                                        <td>
                                                                                            
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        </tr>
                                                                                       <tr id="tr_rep" runat="server" style="display: none;"  align="left">
                                                                                        <td style="width: 130px;" class="Label" >
                                                                                            Letter Of Rep
                                                                                        </td>
                                                                                        <td class="Label">
                                                                                            <div>
                                                                                                &nbsp;<ew:CalendarPopup ID="dtpCourtDate" runat="server" AllowArbitraryText="False"
                                                                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                                                    Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                                                    TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                                                                    Width="80px" ShowClearDate="True">
                                                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                </ew:CalendarPopup>
                                                                                                <asp:Label ID="lblcourtID" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                                                                <asp:TextBox ID="txt_hour" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                                                <asp:TextBox ID="txt_min" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <asp:DropDownList ID="dd_time" runat="server" CssClass="clsInputCombo" Height="18px">
                                                                                                    <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        </tr>
                                                                                       <tr id="tr_rep2" runat="server" style="display: none;"  align="left">
                                                                                        <td style="width: 130px;" class="Label">
                                                                                            Letter Of Rep 2
                                                                                        </td>
                                                                                        <td class="Label">
                                                                                            <div>
                                                                                                &nbsp;<ew:CalendarPopup ID="dtpCourtDate2" runat="server" AllowArbitraryText="False"
                                                                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                                                    Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                                                    TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                                                                    Width="80px">
                                                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                </ew:CalendarPopup>
                                                                                                <asp:Label ID="lblcourtID2" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                                                                <asp:TextBox ID="txt_hour2" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                                                <asp:TextBox ID="txt_min2" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <asp:DropDownList ID="dd_time2" runat="server" CssClass="clsInputCombo" Height="18px">
                                                                                                    <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        </tr>
                                                                                       <tr id="tr_rep3" runat="server" style="display: none;"  align="left">
                                                                                        <td style="width: 130px;" class="Label">
                                                                                            Letter Of Rep 3
                                                                                        </td>
                                                                                        <td class="Label">
                                                                                            <div>
                                                                                                &nbsp;<ew:CalendarPopup ID="dtpCourtDate3" runat="server" AllowArbitraryText="False"
                                                                                                    CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                                                    Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                                                    TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                                                                    Width="80px">
                                                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                </ew:CalendarPopup>
                                                                                                <asp:Label ID="lblcourtID3" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                                                                <asp:TextBox ID="txt_hour3" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                                                <asp:TextBox ID="txt_min3" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <asp:DropDownList ID="dd_time3" runat="server" CssClass="clsInputCombo" Height="18px">
                                                                                                    <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                       <tr id="tr_rep4" runat="server" style="display: none;">
                                                                                        <td style="width: 130px;" class="Label">
                                                                                            Letter Of Rep 4
                                                                                        </td>
                                                                                        <td class="Label">
                                                                                            <div>
                                                                                                &nbsp;<ew:CalendarPopup ID="dtpCourtDate4" runat="server" AllowArbitraryText="False"
                                                                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                                                    Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                                                    TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                                                                    Width="80px">
                                                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                </ew:CalendarPopup>
                                                                                                <asp:Label ID="lblcourtID4" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                                                                <asp:TextBox ID="txt_hour4" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                                                <asp:TextBox ID="txt_min4" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <asp:DropDownList ID="dd_time4" runat="server" CssClass="clsInputCombo" Height="18px">
                                                                                                    <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                       <tr id="tr_rep5" runat="server" style="display: none;">
                                                                                        <td style="width: 130px;" class="Label">
                                                                                            Letter Of Rep 5
                                                                                        </td>
                                                                                        <td class="Label">
                                                                                            <div>
                                                                                                &nbsp;<ew:CalendarPopup ID="dtpCourtDate5" runat="server" AllowArbitraryText="False"
                                                                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                                                    Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                                                    TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                                                                    Width="80px">
                                                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                </ew:CalendarPopup>
                                                                                                <asp:Label ID="lblcourtID5" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                                                                <asp:TextBox ID="txt_hour5" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                                                <asp:TextBox ID="txt_min5" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <asp:DropDownList ID="dd_time5" runat="server" CssClass="clsInputCombo" Height="18px">
                                                                                                    <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                       <tr id="tr_rep6" runat="server" style="display: none;">
                                                                                        <td style="width: 130px;" class="Label">
                                                                                            Letter Of Rep 6
                                                                                        </td>
                                                                                        <td class="Label">
                                                                                            <div>
                                                                                                &nbsp;<ew:CalendarPopup ID="dtpCourtDate6" runat="server" AllowArbitraryText="False"
                                                                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                                                                    Font-Size="8pt" ImageUrl="../images/calendar.gif" LowerBoundDate="1900-01-01"
                                                                                                    Nullable="True" PadSingleDigits="True"  ShowGoToToday="True"
                                                                                                    TabIndex="10" Text=" " ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                                                                    Width="80px">
                                                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                        ForeColor="Black" />
                                                                                                </ew:CalendarPopup>
                                                                                                <asp:Label ID="lblcourtID6" runat="server" CssClass="Label" Style="display: none">0</asp:Label>
                                                                                                <asp:TextBox ID="txt_hour6" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <span class="clssubhead">&nbsp;:&nbsp;</span>
                                                                                                <asp:TextBox ID="txt_min6" runat="server" CssClass="clsInputadministration" MaxLength="2"
                                                                                                    Width="15px"></asp:TextBox>
                                                                                                <asp:DropDownList ID="dd_time6" runat="server" CssClass="clsInputCombo" Height="18px">
                                                                                                    <asp:ListItem Selected="True" Text="AM" Value="AM"></asp:ListItem>
                                                                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                       &nbsp;
                                                                                        </td>
                                                                                      </tr>
                                                                                    </table> 
                                                                                        </td>
                                                                                    </tr>
                                                                                   
                                                                                    <tr>
                                                                                        <td style="width: 128px" class="Label">
                                                                                            County Name
                                                                                        </td>
                                                                                        <td>
                                                                                            &nbsp;<asp:DropDownList ID="ddl_CountyName" runat="server" CssClass="clsInputCombo" Width="132px"
                                                                                                Height="19px" TabIndex="23">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td colspan="2"> 
                                                                                       
                                                                                    <%-- End 8870 --%>
                                                                                    
                                                                                   
                                                                                        <table>
                                                                                        <tr runat="server" id="tr_AssociatedALRCourt">
                                                                                        <td class="Label">
                                                                                        <asp:Label ID="lblAssociatedALRCourts" Text="Associated ALR Court" CssClass="Label" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td >
                                                                                         <asp:DropDownList ID="ddl_AssociatedCourts" runat="server" CssClass="clsInputCombo" Height="19px" >
                                                                                            </asp:DropDownList>  
                                                                                            
                                                                                        </td>
                                                                                        <td style="height: 20px">
                                                                                           
                                                                                        </td>
                                                                                    </tr>
                                                                                        </table>
                                                                                           <%-- <asp:Label ID="lblAllowOnlineSignUp" Text="Allow Online SignUp" CssClass="Label" runat="server"></asp:Label>--%>
                                                                                            
                                                                                        </td>
                                                                                        <%--<td>
                                                                                        <asp:RadioButtonList ID="rbtAllowOnlineSignUp" runat="server" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="yes" Value="1"></asp:ListItem>
                                                                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>--%>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" style="width: 128px; height: 20px;">
                                                                                        </td>
                                                                                        <td class="clsLabelNew" colspan="2" rowspan="2" style="height: 20px">
                                                                                            <asp:HiddenField ID="hfCourtid" runat="server" Value="1" />
                                                                                            <asp:HiddenField ID="hf_IsALRCourt" runat="server" Value="0" />
                                                                                        </td>
                                                                                        <td style="height: 20px">
                                                                                            &nbsp;&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>  <!-- End pnlGeneralInfo -->
                                                                            <asp:Panel ID="panelTitleCaseIdentification" runat="server" Height="30px" BackImageUrl="~/images/subhead_bg.gif"
                                                                                CssClass="clssubhead" Width="100%">
                                                                                <table style="height: 100%" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="clssubhead" valign="middle">
                                                                                            &nbsp;<asp:Image ID="imgCaseInfo" runat="server" ImageUrl="../images/collapse.jpg"
                                                                                                Style="cursor: pointer;" />
                                                                                            Case Processing Detail
                                                                                        </td>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="pnlCaseIdentification" runat="Server" Width="100%">
                                                                                <table class="clsLeftPaddingTable" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="Label" class="style54" style="width: 128px">
                                                                                            Appeal Location
                                                                                        </td>
                                                                                        <td class="style52" style="width: 218px">
                                                                                            &nbsp;<asp:TextBox ID="txtAppealLocation" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="200" TabIndex="25"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style56" style="width: 118px">
                                                                                            Appearnace Hire
                                                                                        </td>
                                                                                        <td style="height: 24px">
                                                                                            <asp:TextBox ID="txtAppearanceHire" runat="server" CssClass="clsInputadministration"
                                                                                                MaxLength="200" TabIndex="34" Width="195px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style55" style="width: 128px">
                                                                                            Judge Trial Hire
                                                                                        </td>
                                                                                        <td class="style53" style="width: 218px">
                                                                                            &nbsp;<asp:TextBox ID="txtJugeTrial" runat="server" Width="200px" CssClass="clsInputadministration"
                                                                                                MaxLength="200" TabIndex="26"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style57" style="width: 118px">
                                                                                            Jury Trial hire
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:TextBox ID="txtJuryTrial" runat="server" Width="195px" CssClass="clsInputadministration"
                                                                                                MaxLength="200" TabIndex="35"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style55" style="width: 128px">
                                                                                            Continuance
                                                                                        </td>
                                                                                        <td class="style53" style="width: 218px">
                                                                                            &nbsp;<asp:TextBox ID="txtContinuance" runat="server" CssClass="clsInputadministration"
                                                                                                MaxLength="200" TabIndex="27" Width="200px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="Label" class="style57" style="width: 118px">
                                                                                            Case Identification
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:DropDownList ID="ddlCaseIdentification" runat="server" CssClass="clsInputCombo"
                                                                                                Height="19px" TabIndex="36" Width="140px">
                                                                                                <asp:ListItem Selected="True" Value="-1">--Choose--</asp:ListItem>
                                                                                                <asp:ListItem Value="0">Ticket</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Cause</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style55" style="width: 128px">
                                                                                            Supporting Docs
                                                                                        </td>
                                                                                        <td class="style53" style="width: 218px">
                                                                                            <asp:RadioButtonList ID="rblSupportingDoc" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="28">
                                                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                        <td class="Label" class="style57" style="width: 118px">
                                                                                            Accept Appeals
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:DropDownList ID="ddlAcceptAppeals" runat="server" CssClass="clsInputCombo" Height="19px"
                                                                                                TabIndex="37" Width="140px">
                                                                                                <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Ticket &amp; Cause No</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style55" style="width: 128px">
                                                                                            Additinal FTA issued
                                                                                        </td>
                                                                                        <td class="style53" style="width: 218px">
                                                                                            <asp:RadioButtonList ID="rblFTAIssues" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="29">
                                                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                        <td class="Label" class="style57" style="width: 118px">
                                                                                            Initial Setting
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:DropDownList ID="ddlInitialSetting" runat="server" CssClass="clsInputCombo"
                                                                                                Height="19px" TabIndex="38" Width="140px">
                                                                                                <asp:ListItem Selected="True" Value="-1">--Choose--</asp:ListItem>
                                                                                                <asp:ListItem Value="0">Appearance</asp:ListItem>
                                                                                                <asp:ListItem Value="1">PreTrial</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Jury Trial</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style55" style="width: 128px">
                                                                                            Handwriting Allowed
                                                                                        </td>
                                                                                        <td class="style53" style="width: 218px">
                                                                                            <asp:RadioButtonList ID="rblHandwriting" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="30">
                                                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                        <td class="Label" class="style57" style="width: 118px">
                                                                                            LOR Method
                                                                                        </td>
                                                                                        <td style="height: 23px">
                                                                                            <asp:DropDownList ID="ddlLOR" runat="server" CssClass="clsInputCombo" Height="19px"
                                                                                                TabIndex="39" Width="140px">
                                                                                                <asp:ListItem Selected="True" Value="-1">--Choose--</asp:ListItem>
                                                                                                <asp:ListItem Value="0">Fax</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Certified Mail</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Hand Delivery</asp:ListItem>
                                                                                                <asp:ListItem Value="3">Mail</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style55" style="width: 128px">
                                                                                            Grace Period
                                                                                        </td>
                                                                                        <td class="style53" style="width: 218px">
                                                                                            <asp:RadioButtonList ID="rblGracePeriod" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="31">
                                                                                                <asp:ListItem Value="1">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                                                <asp:ListItem Value="0">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                        <td class="Label" class="style57" style="width: 128px">
                                                                                            ALR Process
                                                                                        </td>
                                                                                        <td style="width: 218px">
                                                                                            <asp:RadioButtonList ID="rblALRProcess" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="31">
                                                                                                <asp:ListItem Value="1" onclick="ddl_CaseType_IndexChange();">&lt;span class=&quot;Label&quot;&gt;Yes&lt;/span&gt;</asp:ListItem>
                                                                                                <asp:ListItem Value="0" Selected="True" onclick="ddl_CaseType_IndexChange();">&lt;span class=&quot;Label&quot;&gt;No&lt;/span&gt;</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table class="clsLeftPaddingTable" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="Label" class="style45" style="width: 128px">
                                                                                            Upload File
                                                                                        </td>
                                                                                        <td style="height: 25px">
                                                                                            &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" Width="254px" BorderWidth="1px"
                                                                                                CssClass="clsInputadministration" TabIndex="32" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="Label" class="style46" style="width: 128px">
                                                                                            Comments
                                                                                        </td>
                                                                                        <td style="height: 35px">
                                                                                            &nbsp;<asp:TextBox ID="txtComments" runat="server" Width="250px" CssClass="clsInputadministration"
                                                                                                MaxLength="1000" TabIndex="33" TextMode="MultiLine" Height="62px" onkeyup="return CommentsLength();"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table width="100%">
                                                                                    <tr id="trgridline" runat="server" visible="false">
                                                                                        <td align="center" background="../Images/separator_repeat.gif" style="width: 100%;
                                                                                            height: 10px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="clssubhead">
                                                                                            DocumentDetails
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center" background="../Images/separator_repeat.gif" style="width: 100%;
                                                                                            height: 10px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div id="divDocumentDetails" runat="server" style="height: 90px; overflow: auto">
                                                                                                <asp:GridView ID="gvCourtFiles" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                    Font-Names="Verdana" Font-Size="2px" OnRowCommand="gvCourtFiles_RowCommand" OnRowDataBound="gvCourtFiles_RowDataBound"
                                                                                                    OnRowDeleting="gvCourtFiles_RowDeleting" Width="670px">
                                                                                                    <FooterStyle CssClass="GrdFooter" />
                                                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                                                    <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField HeaderText="Uploaded Date">
                                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="lblDateTime" runat="server" CssClass="GrdLbl" Font-Size="Smaller"
                                                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.Uploaded Date") %>'></asp:Label>
                                                                                                                &nbsp; &nbsp;
                                                                                                            </ItemTemplate>
                                                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField HeaderText="Document Name">
                                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="lblDocumentName" runat="server" CssClass="GrdLbl" Font-Size="Smaller"
                                                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'></asp:Label>
                                                                                                                &nbsp; &nbsp;
                                                                                                            </ItemTemplate>
                                                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField HeaderText="Rep">
                                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Font-Size="Smaller" Text='<%# DataBinder.Eval(Container, "DataItem.rep") %>'></asp:Label>
                                                                                                                &nbsp; &nbsp;
                                                                                                            </ItemTemplate>
                                                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField HeaderText="View">
                                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:ImageButton ID="imgPreviewDoc" runat="server" ImageUrl="../images/Preview.gif"
                                                                                                                    Width="19" />
                                                                                                                <asp:HiddenField ID="hfFileId" runat="server" Value='<%# bind("id") %>' />
                                                                                                            </ItemTemplate>
                                                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField>
                                                                                                            <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# bind("id") %>'
                                                                                                                    CommandName="Delete" Text="Delete" />
                                                                                                            </ItemTemplate>
                                                                                                            <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" />
                                                                                                        </asp:TemplateField>
                                                                                                    </Columns>
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" background="../Images/separator_repeat.gif" style="width: 896px;
                                                                            height: 11px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" class="style8" width="100%">
                                                                                <tr>
                                                                                    <td align="right" class="style9" colspan="1">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClick="btn_Submit_Click"
                                                                                            TabIndex="40" Text="Submit" />
                                                                                        &nbsp;
                                                                                        <asp:Button ID="btn_Delete" runat="server" CssClass="clsbutton" Enabled="False" TabIndex="41"
                                                                                            Text="Delete" />
                                                                                        &nbsp;
                                                                                        <asp:Button ID="btnResetAdvance" runat="server" CssClass="clsbutton" Height="20px"
                                                                                            OnClick="btnResetAdvance_Click" OnClientClick="return ResetControls();" TabIndex="42"
                                                                                            Text="Reset" Width="55px" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td background="../images/separator_repeat.gif" height="10">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel> <!-- end pnlModalPopUp -->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <aspnew:PostBackTrigger ControlID="btn_Submit" />
                    </Triggers>
                </aspnew:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>

    <script>
    
    // Rab Nawaz 8870 03/10/2011 zindex has been added due to google chrom and safari browsers alignment issues 
        document.getElementById("dtpCourtDate_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate_monthYear").style.zIndex = 100004;                
        document.getElementById("dtpCourtDate2_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate2_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate3_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate3_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate4_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate4_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate5_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate5_monthYear").style.zIndex = 100004;
        document.getElementById("dtpCourtDate6_div").style.zIndex = 100003;
        document.getElementById("dtpCourtDate6_monthYear").style.zIndex = 100004;
   // end 8870
    </script>

</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JuryOutCome.aspx.cs" Inherits="HTP.backroom.JuryOutCome" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Jury OutCome</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript"> 
    
    //Tahir 5910 05/18/2009 allow user to display printable version.
    function OpenPrintVersion()
    {
        var docket = document.getElementById("cal_Date").value;
        window.open("juryoutcome.aspx?IsPrint=True&date="+ docket  ,"_blank","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width=825, height=800");
        //alert (docket);
        return false;
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" runat="server" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr id="trSelectionCriteria" runat="server">
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="width: 50%" align="left">
                                <asp:Label ID="Label1" CssClass="label" Text="Docket Date:" runat="server"></asp:Label>
                                <ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif"
                                    Width="86px" EnableHideDropDown="True">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clsinputadministration" />
                                </ew:CalendarPopup>
                                <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_Submit_Click" />
                            </td>
                            <td style="width: 50%" align="right">
                                <asp:ImageButton ID="imgPrint" runat="server" Text="Print" OnClientClick="return OpenPrintVersion();"
                                    ImageUrl="../Images/PrintNew1.jpg" AlternateText="Print" ToolTip="Printer friendly version">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trPrintHeader" runat="server" visible="false">
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="width: 100%; font-weight: bold;" align="left">
                                Sullo & Sullo LLP
                                <br />
                                Jury Out Come Report
                                <br />
                                Docket Date:
                                <asp:Label ID="lblDocketDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                        border="0">
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr style="text-align: center">
                            <td>
                                <asp:Label ID="lbl_Message" CssClass="Label" ForeColor="Red" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px;
                                            width: 743px;" align="left">
                                            &nbsp;Docket Summary
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" background="../Images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                CssClass="clsleftpaddingtable">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Total Clients">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_total" runat="server" Text='<%# Eval("total")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Warrant Clients">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_warrant" runat="server" Text='<%# Eval("warrant")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reset Clients">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_reset" runat="server" Text='<%# Eval("reset")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Unresulted">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_unresulted" runat="server" Text='<%# Eval("unresulted")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Disposed Clients">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_disposed" runat="server" Text='<%# Eval("disposed")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Clients DSC/DADJ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_dsc_dadj" runat="server" Text='<%# Eval("dsc_dadj")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="18px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Clients With MOVERS PLEAD(Plea Outs)">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="110px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_plead_part" runat="server" Text='<%# Eval("plead_part")+ "(" +Eval("plead_all")+")"%>'
                                                                Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="% of Clients Disposed">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="65px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_plead_all" runat="server" Text='<%# Eval("perc_client_disp","{0:f2}")%>'
                                                                Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="% of Clients with DSC/DADJ/Movers Plea">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" HorizontalAlign="Left" Font-Size="XX-Small"
                                                            Width="130px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_perc_client_disp" runat="server" Text='<%# Eval("perc_dsc_dadj_plead","{0:f2}")%>'
                                                                Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="Label" Visible="false" ForeColor="Red" ID="Lbl_Doc_NoRec" Text="No records found"
                                                runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px;
                                            width: 743px;" align="left">
                                            &nbsp;Officer Summary
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gv_records_2" runat="server" AutoGenerateColumns="False" Width="100%"
                                                CssClass="clsleftpaddingtable" OnRowDataBound="gv_records_2_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S.No">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="25px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Sno" runat="server" Visible="false" Text='<%# Eval("SNO")%>' Font-Size="XX-Small"></asp:Label>
                                                            <asp:HyperLink ID="hl_sno" Font-Size="XX-Small" runat="server" Target="_blank" Text='<%# Eval("SNo") %>'
                                                                NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" +Eval("ticketid_pk") %>'></asp:HyperLink>
                                                            <asp:HiddenField ID="hf_rowtype" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "RowType") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Client">
                                                        <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_client" runat="server" Text='<%# Eval("client")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CDL/BOND">
                                                        <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="50px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bondflag" runat="server" Text='<%#  Eval("cdlflag") + " " + Eval("bondflag") %>'
                                                                Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt No">
                                                        <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="40px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtnum" runat="server" Text='<%# Eval("crtnum")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Time">
                                                        <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crttime" runat="server" Text='<%# Eval("crttime")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Violations">
                                                        <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_viol" runat="server" Text='<%# Eval("viol")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Outcome">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_outcome" runat="server" Text='<%# Eval("outcome")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" Font-Size="XX-Small" Width="18px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="Label" Visible="false" ForeColor="Red" ID="Lbl_officer_NoRec"
                                                Text="No records found" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px;
                                            width: 743px;" align="left">
                                            &nbsp;Court Room Summary
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gv_records_3" runat="server" AutoGenerateColumns="False" Width="100%"
                                                CssClass="clsleftpaddingtable" OnRowDataBound="gv_records_3_RowDataBound" ShowFooter="True">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Crt No">
                                                        <HeaderStyle BackColor="#EFF4FB" HorizontalAlign="Left" CssClass="clssubhead" Font-Size="XX-Small"
                                                            Width="50%" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtnum" runat="server" Text='<%# Eval("crtnum")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="DSC/DADJ/Pleas (non-pleaouts)">
                                                        <HeaderStyle BackColor="#EFF4FB" HorizontalAlign="Left" CssClass="clssubhead" Font-Size="XX-Small"
                                                            Width="50%" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_dsc_dadj_plea" runat="server" Text='<%# Eval("dsc_dadj_plea")%>'
                                                                Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="Label" Visible="false" ForeColor="Red" ID="Lbl_Court_NoRec"
                                                Text="No records found" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px;
                                            width: 743px;" align="left">
                                            &nbsp;Un-Resulted Cases
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gv_records_4" runat="server" AutoGenerateColumns="False" Width="100%"
                                                CssClass="clsleftpaddingtable">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S.No">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="25px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_sno" Visible="false" runat="server" Text='<%# Eval("SNO")%>' Font-Size="XX-Small"></asp:Label>
                                                            <asp:HyperLink ID="hl_sno" Font-Size="XX-Small" Target="_blank" runat="server" Text='<%# Eval("SNo") %>'
                                                                NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" +Eval("ticketid_pk") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Client">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="8px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_client" runat="server" Text='<%# Eval("client")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Officer">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_officer" runat="server" Text='<%# Eval("officer")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt No">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="40px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtnum" runat="server" Text='<%# Eval("crtnum")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Matter">
                                                        <HeaderStyle BackColor="#EFF4FB" CssClass="clssubhead" Font-Size="XX-Small" Width="8px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_viol" runat="server" Text='<%# Eval("viol")%>' Font-Size="XX-Small"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="Label" Visible="false" ForeColor="Red" ID="Lbl_UnRslt_NoRec"
                                                Text="No records found" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
    public partial class JuryOutCome : System.Web.UI.Page
    {
        #region declarations

        private DataSet dsRecords;
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        int sum = 0;

        #endregion


        #region Events

        /// <summary>
        /// Page Load Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";

            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                    Response.End();
                }
                else //To stop page further execution
                {

                   if (!IsPostBack)
                    {
                        try
                        {
                            //Tahir 5910 05/18/2009 allow user to display printable version.

                            bool IsPrintVersion = false;
                            

                            if (Request.QueryString["IsPrint"] != null && Request.QueryString["IsPrint"].Length > 0)
                            {
                                IsPrintVersion = Convert.ToBoolean(Request.QueryString["IsPrint"]);
                            }
                            else
                            {
                                IsPrintVersion = false;
                            }

                            if (Request.QueryString["date"] != null && Request.QueryString["date"].Length > 0)
                            {
                                cal_Date.SelectedDate = Convert.ToDateTime(Request.QueryString["date"]);
                            }
                            else
                            {
                                if (Convert.ToString(DateTime.Now.DayOfWeek) == "Monday")
                                {
                                    cal_Date.SelectedDate = DateTime.Now.AddDays(-3);
                                }
                                else if (Convert.ToString(DateTime.Now.DayOfWeek) == "Sunday")
                                {
                                    cal_Date.SelectedDate = DateTime.Now.AddDays(-2);
                                }
                                else
                                {
                                    cal_Date.SelectedDate = DateTime.Now.AddDays(-1);
                                }
                            }


                            if (IsPrintVersion)
                            {
                                tblMain.Align = "left";
                                ActiveMenu1.Visible = false;
                                trPrintHeader.Visible = true;
                                trSelectionCriteria.Visible = false;
                                lblDocketDate.Text = cal_Date.SelectedDate.ToShortDateString();
                            }
                            else
                            {
                                tblMain.Align = "center";
                                ActiveMenu1.Visible = true;
                                trPrintHeader.Visible = false;
                                trSelectionCriteria.Visible = true;
                            }


                            btn_Submit_Click(null, null);
                        }
                        catch (Exception ex)
                        {
                            lbl_Message.Text = ex.Message;
                            bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Submit Button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Grid RowdataBound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField rowtype = (HiddenField)e.Row.FindControl("hf_rowtype");

                    if (rowtype.Value == "3")
                    {
                        for (int i = (e.Row.Cells.Count - 1); i > 0; i--)

                            e.Row.Cells.Remove(e.Row.Cells[1]);

                        e.Row.Cells[0].Style.Add("height", "20px");
                        e.Row.Cells[0].ColumnSpan = 7;

                        Label lbl = (Label)e.Row.FindControl("lbl_sno");

                        if (lbl.Text != "")
                        {
                            lbl.ForeColor = System.Drawing.Color.White;
                            e.Row.Cells[0].BackColor = System.Drawing.Color.Black;
                            e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
                            e.Row.Cells[0].Font.Bold = true;
                            lbl.Visible = true;
                            ((HyperLink)e.Row.FindControl("hl_sno")).Visible = false; ;

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Grid RowdataBound Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    sum = sum + Convert.ToInt16(((DataRowView)e.Row.DataItem)["dsc_dadj_plea"].ToString());
                }
                else if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[0].Text = "Total";
                    e.Row.Cells[0].Font.Bold = true;
                    e.Row.Cells[1].Text = sum.ToString();
                    e.Row.Cells[1].Font.Bold = true;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

       

        #endregion


        #region Methods

        /// <summary>
        ///This Method will be used to Get records Into the Dataset.
        /// </summary>
        private void GetRecords()
        {
            try
            {
                ClsJuryOutCome juryOutComeReport = new ClsJuryOutCome();
                dsRecords = juryOutComeReport.GetJuryOutComeReport(cal_Date.SelectedDate);
                fillGrid();

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        /// <summary>
        /// This Method will be used to Display Records in the Grids.
        /// </summary>
        private void fillGrid()
        {
            try
            {

                if (dsRecords.Tables[0].Rows.Count > 0)
                {

                    gv_records.DataSource = dsRecords.Tables[0];
                    GenerateSerial(dsRecords.Tables[0]);
                    Lbl_Doc_NoRec.Visible = false;
                }
                else
                {
                    Lbl_Doc_NoRec.Visible = true;
                }

                if (dsRecords.Tables[1].Rows.Count > 0)
                {
                    formatOfficerDataSet(dsRecords, 1);
                    gv_records_2.DataSource = dsRecords.Tables[1];
                    Lbl_officer_NoRec.Visible = false;
                }
                else
                {
                    Lbl_officer_NoRec.Visible = true;
                }

                if (dsRecords.Tables[2].Rows.Count > 0)
                {
                    gv_records_3.DataSource = dsRecords.Tables[2];
                    GenerateSerial(dsRecords.Tables[2]);
                    Lbl_Court_NoRec.Visible = false;
                }
                else
                {
                    Lbl_Court_NoRec.Visible = true;
                }

                if (dsRecords.Tables[3].Rows.Count > 0)
                {
                    formatUnresultedDataSet(dsRecords, 3);
                    gv_records_4.DataSource = dsRecords.Tables[3];
                    Lbl_UnRslt_NoRec.Visible = false;
                }
                else
                {
                    Lbl_UnRslt_NoRec.Visible = true;
                }

                gv_records.DataBind();
                gv_records_2.DataBind();
                gv_records_4.DataBind();
                gv_records_3.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        /// <summary>
        /// Method to Format officer Summary Dataset.
        /// </summary>
        /// <param name="ds_records"></param>
        /// <param name="tableno"></param>
        private void formatOfficerDataSet(DataSet ds_records, int tableno)
        {
            int sno = 1;

            DataColumn dc = new DataColumn("RowType");
            dc.DefaultValue = 1;

            ds_records.Tables[tableno].Columns.Add("SNO");
            ds_records.Tables[tableno].Columns.Add(dc);
            ds_records.Tables[tableno].Rows[0]["SNO"] = sno;
            DataRow dr1 = ds_records.Tables[tableno].NewRow();
            dr1["SNO"] = ds_records.Tables[tableno].Rows[0]["officer"].ToString();
            dr1["RowType"] = 3;
            ds_records.Tables[tableno].Rows.InsertAt(dr1, 0);
            sno++;

            for (int i = 1; i < ds_records.Tables[tableno].Rows.Count; i++)
            {

                if (ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"] != null && ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"].ToString().Length > 0)
                {
                    try
                    {

                        if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["ticketid_pk"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"]))
                        {
                            ds_records.Tables[tableno].Rows[i]["SNO"] = sno;
                            sno++;

                        }

                        if (ds_records.Tables[tableno].Rows[i]["officer"].ToString() != ds_records.Tables[tableno].Rows[i - 1]["officer"].ToString())
                        {
                            DataRow dr = ds_records.Tables[tableno].NewRow();
                            dr["RowType"] = 3;
                            dr["SNO"] = ds_records.Tables[tableno].Rows[i]["officer"].ToString();
                            ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                        }

                    }
                    catch (Exception ex)
                    {
                        lbl_Message.Text = ex.Message;
                        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    }
                }

            }

        }

        /// <summary>
        /// Method to format Unresulted Dataset.
        /// </summary>
        /// <param name="ds_records"></param>
        /// <param name="tableno"></param>
        private void formatUnresultedDataSet(DataSet ds_records, int tableno)
        {
            int sno = 1;

            ds_records.Tables[tableno].Columns.Add("SNO");
            ds_records.Tables[tableno].Rows[0]["SNO"] = sno;
            sno++;

            for (int i = 1; i < ds_records.Tables[tableno].Rows.Count; i++)
            {

                if (ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"] != null)
                {
                    try
                    {

                        if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["ticketid_pk"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"]))
                        {
                            ds_records.Tables[tableno].Rows[i]["SNO"] = sno;
                            sno++;

                        }

                    }
                    catch (Exception ex)
                    {
                        lbl_Message.Text = ex.Message;
                        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    }
                }

            }

        }

        /// <summary>
        /// Method to Generate Serial No's.
        /// </summary>
        /// <param name="DT"></param>
        private void GenerateSerial(DataTable DT)
        {

            try
            {
                int sno = 1;
                if (!DT.Columns.Contains("sno"))
                    DT.Columns.Add("sno");


                if (DT.Rows.Count >= 1)
                    DT.Rows[0]["sno"] = 1;

                if (DT.Rows.Count >= 2)
                {
                    for (int i = 1; i < DT.Rows.Count; i++)
                    {

                        DT.Rows[i]["sno"] = ++sno;

                    }
                }

            }

            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

    }
}

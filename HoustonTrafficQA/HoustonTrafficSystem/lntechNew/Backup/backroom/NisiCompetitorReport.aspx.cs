using System;
using System.Data;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.backroom
{
    ///<summary>
    ///</summary>
    public partial class NisiCompetitorReport : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        //DataTable dtRecords;
        ///<summary>
        ///</summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                    {
                        Response.Redirect("../LoginAccesserror.aspx", false);
                        Response.End();
                    }
                    else //To stop page further execution
                    {
                        lbl_message.Text = "";
                        if (!IsPostBack)
                        {
                            calQueryFrom.SelectedDate = DateTime.Today;
                            calQueryTo.SelectedDate = DateTime.Today;
                            btn_submit.Attributes.Add("OnClick", "return validate();");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

     
        /// <summary>
        /// Repeater item data bond event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repNisiCompititor_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DateTime startDate = (DateTime)((DataRowView)e.Item.DataItem)["StartDate"];
                    DateTime endDate = (DateTime)((DataRowView)e.Item.DataItem)["EndDate"];
                    Label lblStartDate = (Label)e.Item.FindControl("lblStartDate");
                    if (ddlBreakDown.SelectedValue == "Monthly")
                    {
                        lblStartDate.Text = startDate.ToString("MMM yyyy");
                    }
                    else if (ddlBreakDown.SelectedValue == "Quarterly")
                    {
                        lblStartDate.Text = "Quarter 0" + GetQuarter(startDate) + "-" + startDate.ToString("yyyy");
                    }
                    else if (ddlBreakDown.SelectedValue == "Yearly")
                    {
                        lblStartDate.Text = startDate.ToString("yyyy");   
                    }
                    string[] key = { "@sDate", "@eDate", "@breakDown", "@Top" };
                    object[] value1 = { startDate, endDate, ddlBreakDown.SelectedValue, ddlNoOfRecords.SelectedValue };
                    DataTable dtMonthRecords = ClsDb.Get_DT_BySPArr("USP_HTP_NISICompititor", key, value1);
                    GenerateSerialNo(dtMonthRecords);

                    DataGrid dgNisiCompitoter = (DataGrid)e.Item.FindControl("dgNisiCompitoter");
                    dgNisiCompitoter.DataSource = dtMonthRecords;
                    dgNisiCompitoter.DataBind();
                }
                catch (Exception exp)
                {
                    clsLogger.ErrorLog(exp);
                }
            }
        }
        /// <summary>
        /// Submit button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else 
                {
                    DateTime startDate = new DateTime(calQueryFrom.SelectedDate.Year, calQueryFrom.SelectedDate.Month, 1);
                    DateTime endDate = calQueryTo.SelectedDate;

                    DataTable dtDateRange = new DataTable("DateRange");
                    dtDateRange.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
                    dtDateRange.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));

                    DateTime tempStartDate, tempEndDate;
                    tempStartDate = startDate;
                    //making a data table of dates by breaking the date range in months.
                    while (tempStartDate < endDate)
                    {
                        if (ddlBreakDown.SelectedValue == "Monthly")
                        {
                            tempEndDate = GetLastDate(tempStartDate.Year, tempStartDate.Month);
                            DataRow dRow = dtDateRange.NewRow();
                            dtDateRange.Rows.Add(dRow);
                            dRow["StartDate"] = tempStartDate;
                            if(endDate.Month == tempEndDate.Month && endDate.Year == tempEndDate.Year)
                            {
                                dRow["EndDate"] = endDate;
                            }
                            else
                            {
                                dRow["EndDate"] = tempEndDate;    
                            }
                            
                            tempStartDate = tempStartDate.AddMonths(1);
                        }
                        else if (ddlBreakDown.SelectedValue == "Quarterly")
                        {
                            tempEndDate = GetQuarterEndDate(tempStartDate.Year, tempStartDate.Month);
                            DataRow dRow = dtDateRange.NewRow();
                            dtDateRange.Rows.Add(dRow);
                            dRow["StartDate"] = tempStartDate;
                            dRow["EndDate"] = endDate < tempEndDate ? endDate : tempEndDate;
                            tempStartDate = tempStartDate.AddMonths(3);
                        }
                        else if (ddlBreakDown.SelectedValue == "Yearly")
                        {
                            tempEndDate = new DateTime(tempStartDate.Year,12,31);
                            DataRow dRow = dtDateRange.NewRow();
                            dtDateRange.Rows.Add(dRow);
                            dRow["StartDate"] = tempStartDate;
                            dRow["EndDate"] = endDate < tempEndDate ? endDate : tempEndDate;
                            tempStartDate = tempStartDate.AddYears(1); 
                        }
                    }
                    DataView dv = dtDateRange.DefaultView;
                    dv.Sort = "EndDate desc";
                    DataTable sortedDT = dv.ToTable();

                    repNisiCompititor.DataSource = sortedDT;
                    repNisiCompititor.DataBind();
                    repNisiCompititor.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        ///<summary>
        /// This method is used to get quarter from the provided date.
        ///</summary>
        ///<param name="date"></param>
        ///<returns>Quarter</returns>
        public int GetQuarter(DateTime date)
        {
            if (date.Month >= 1 && date.Month <= 3)
            {return 1;}
            else if (date.Month >= 4 && date.Month <= 6)
            { return 2;}
            else if (date.Month >= 7 && date.Month <= 9)
            {return 3;}
            else
            { return 4;}

        }
        ///// <summary>
        ///// This method is used to generate serial no 
        ///// </summary>
        ///// <description>
        /////This method automatically add the serial number column if a table does not have, and also fill the serial number
        ///// with each record
        ///// </description>
        ///// <example >
        /////DataTable dt=assignatable;
        /////GenerateSerialNo()
        ///// </example>
        //public void generateSerialNo()
        //{
        //    if (dtRecords.Columns.Contains("sno") == false)
        //    {
        //        dtRecords.Columns.Add("sno");
        //        int sno = 1;

        //        //added By Aziz to check if rows existed
        //        if (dtRecords.Rows.Count >= 1)
        //            dtRecords.Rows[0]["sno"] = 1;

        //        if (dtRecords.Rows.Count >= 2)
        //        {
        //            for (int i = 1; i < dtRecords.Rows.Count; i++)
        //            {
        //                dtRecords.Rows[i]["sno"] = ++sno;
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Year">Year part of the date</param>
        /// <param name="Month">Month part of the date</param>
        /// <returns>Date having last day of the given month/year</returns>
        private DateTime GetLastDate(int Year, int Month)
        {
            return new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month));
        }
        private DateTime GetQuarterEndDate(int Year, int Month)
        {
            return new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)).AddMonths(2);
        }
        private DateTime GetYearEndDate(int Year, int Month)
        {
            return new DateTime(Year, Month, GetDaysInAYear(Year));
        }

        private int GetDaysInAYear(int year)
        {

            int days = 0;
            for (int i = 1; i <= 12; i++)
            {
                days += DateTime.DaysInMonth(year, i);
            }
            return days;
        }


        /// <summary>
        /// Add serial no column in the data table
        /// </summary>
        /// <param name="dtRecords">Reference of the Data Table</param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
                int sno = 1;
                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
        }
    }

}

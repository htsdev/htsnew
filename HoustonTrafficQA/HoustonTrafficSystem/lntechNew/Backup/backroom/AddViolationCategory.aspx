<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.AddViolationCategory" Codebehind="AddViolationCategory.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Violation Category</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script lang="javascript">
		function isUnassignedSelected()
		{
			var listUnassigned = document.getElementById("listUnassigned");
			if( listUnassigned.selectedIndex<0 )
			{
				alert("Please select atleast one value from Unassigned list");
				return false; 
			}
		}

		function isAssignedSelected()
		{
			var listAssigned = document.getElementById("listAssigned");
			if( listAssigned.selectedIndex<0 )
			{
				alert("Please select atleast one value from Assigned list");
				return false; 
			}
		}

		</script>
	</HEAD>
	<BODY leftMargin="0" topMargin="0" ms_positioning="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" style="Z-INDEX: 101; LEFT: 96px; POSITION: absolute; TOP: 16px" cellSpacing="0"
				cellPadding="0" width="780" align="center" border="0">
				<TR>
				</TR>
				<TR>
					<TD style="HEIGHT: 464px">
						<TABLE id="tblsub" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
							</TR>
							<TR>
								<TD background="../../images/separator_repeat.gif"  height="11"></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable">
									<TABLE id="tblheader" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="clsLeftPaddingTable" width="13%" align="right">Select Category</TD>
											<TD class="clsLeftPaddingTable"><asp:dropdownlist id="ddlCategory" runat="server" CssClass="clsinputcombo" Width="128px" AutoPostBack="True">
													<asp:ListItem Value="-1">--Choose--</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR vAlign="top">
								<TD class="clsLeftPaddingTable" vAlign="top">
									<TABLE id="tbl_list" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="clsLeftPaddingTable" vAlign="top" align="center" colSpan="2" rowSpan="1">UnAssigned 
												Violations
											</TD>
											<TD class="clsLeftPaddingTable" align="center" colSpan="3" rowSpan="1">Assigned 
												Violations</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="right" width="40%" colSpan="3"><asp:listbox id="listUnassigned" runat="server" Width="328px" Rows="14" SelectionMode="Multiple"
													Height="272px" CssClass="clstextarea"></asp:listbox></TD>
											<TD vAlign="top" align="center" width="7%">
												<P>&nbsp;</P>
												<P>&nbsp;</P>
												<P><asp:imagebutton id="imgbtnAdd" runat="server" AlternateText=">" ImageUrl="../Images/rightarrow-crop.gif"
														ToolTip="Add selected violations to category"></asp:imagebutton></P>
												<P><asp:imagebutton id="imgbtnDel" runat="server" AlternateText="<" ImageUrl="../Images/Arrow-crop.gif"
														ToolTip="Delete violations From category"></asp:imagebutton></P>
												<P>&nbsp;</P>
											</TD>
											<TD vAlign="top" align="left" width="40%"><asp:listbox id="listAssigned" runat="server" Width="328px" Rows="14" SelectionMode="Multiple"
													Height="272px" CssClass="clstextarea"></asp:listbox></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="left" width="442" colSpan="3"></TD>
											<TD vAlign="top" align="center" width="51"></TD>
											<TD vAlign="top" align="right"></TD>
										</TR>
										<TR>
											<TD class="frmtd" align="center" ><asp:label id="lblMessage" runat="server" Width="175px" ForeColor="Red"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD background="../../images/separator_repeat.gif"  height="11"></TD>
							</TR>
							<TR>
								<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</BODY>
</HTML>

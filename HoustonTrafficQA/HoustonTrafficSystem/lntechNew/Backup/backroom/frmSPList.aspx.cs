using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data.SqlClient;
using lntechNew.Components.ClientInfo;

namespace lntechNew.backroom.SqlSrvMrg
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	/// 
	

	public partial class frmSPList : System.Web.UI.Page
	{
		string  ConStr = (string)ConfigurationSettings.AppSettings["connString"];
	protected System.Data.SqlClient.SqlConnection conn;
		protected System.Web.UI.WebControls.TextBox txtchk;
		protected System.Web.UI.WebControls.TextBox txtNoParam;
		protected System.Web.UI.WebControls.TextBox TextBox2;
		protected System.Web.UI.WebControls.TextBox txtSpName;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected System.Web.UI.WebControls.DataGrid DgSqlProcedure;
		protected System.Web.UI.WebControls.Label lblNoRec;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.TextBox txtSearchSp;
		protected System.Web.UI.WebControls.Button ImageButton1;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;    
		clsSession cSession = new clsSession();
		clsLogger clog = new clsLogger();
				 
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (cSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{
						conn= new SqlConnection(ConStr);
						conn.Open(); 		
				
						if (IsPostBack!=true)
						{
					
							//	string QueryText = "usp_SQLMGR_Get_Procedures" + txtSearchSp.Text.Trim() ;
							//	DataSet	Ds1= SqlHelper.ExecuteDataset(conn,CommandType.Text,QueryText);
				
							SqlParameter [] Sparam = new SqlParameter[1];
							Sparam[0] = new SqlParameter("@ProcName",txtSearchSp.Text.Trim());
							DataSet DsSearch = SqlHelper.ExecuteDataset(ConStr,CommandType.StoredProcedure,"usp_SQLMGR_Get_Procedures",Sparam);   
							fill_Grid(DsSearch);
							txtNoParam.Text="1";
						}
					}
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
	}

		private void fill_Grid(DataSet Ds1)
		{
			try
			{
				int a= Ds1.Tables[0].Rows.Count;
				DgSqlProcedure.DataSource = Ds1;	
				DgSqlProcedure.DataBind();
				FillPageList();
				SetNavigation();
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ImageButton1.Click += new System.EventHandler(this.ImageButton1_Click);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
			this.DgSqlProcedure.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DgSqlProcedure_ItemCommand);
			this.DgSqlProcedure.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DgSqlProcedure_PageIndexChanged);
			this.DgSqlProcedure.SelectedIndexChanged += new System.EventHandler(this.DgSqlProcedure_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		// Go to the Next Page or Selected Page
		private void GotoPage(int intNewPageNo) 
		{
			try
			{	
				lblmsg.Text  ="";
				DgSqlProcedure.CurrentPageIndex = intNewPageNo ;// Comment By Zeeshan Jur
				string QueryText = "usp_SQLMGR_Get_Procedures ";// + txtSearchSp.Text.Trim() ;
				
				
				DataSet Ds1;


				if (txtSearchSp.Text=="")
				{
					Ds1= SqlHelper.ExecuteDataset(conn,CommandType.Text,QueryText);	
				}
				else
				{
					Ds1= SqlHelper.ExecuteDataset(conn,CommandType.StoredProcedure,QueryText,new SqlParameter("@ProcName",txtSearchSp.Text));
			
				}
					
				
				
				fill_Grid(Ds1);
				DgSqlProcedure.SelectedIndex=-1;
				
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
		// Procedure call when new page is come in the Grid
		private void DgSqlProcedure_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
		visible();
		GotoPage(e.NewPageIndex);
		}
					
		//fill dropdownlist with no of pages in grid
		private void FillPageList()
		{
			try
			{
				Int16 idx =0;
				cmbPageNo.Items.Clear();
				for(idx=1; idx<=DgSqlProcedure.PageCount;idx++ )
				{
					cmbPageNo.Items.Add("Page - " + idx.ToString());
					cmbPageNo.Items[idx-1].Value = idx.ToString();
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		 }	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(DgSqlProcedure.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;
				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				cmbPageNo.SelectedIndex =DgSqlProcedure.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Procedure for setting the grid page number as per selected from page no. combo.......
		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				visible();				
				
				GotoPage(cmbPageNo.SelectedIndex);
				txtNoParam.Text="1";
			}
			catch(Exception ex)
			{
				lblmsg.Text  = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		// Fires when an event is generated within the datagrid
		private void DgSqlProcedure_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{

				if (e.CommandName == "ShowParam")
				{
					lblNoRec.Visible = false;
					lblmsg.Visible =true;
					txtNoParam.Text="0";
					txtchk.Text = "0";
					txtSpName.Text = ((LinkButton)( e.Item.FindControl("lnkSPName"))).Text;
					DgSqlProcedure.SelectedIndex = e.Item.ItemIndex;
					Response.Redirect("frmReportDesc.aspx?spName=" + txtSpName.Text + "&prm=" + txtNoParam.Text,false); 
				}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		
			
				
		//Invisible Controls
		private void visible()
		{
			lblNoRec.Visible = false;
			
		}

		// For searching the storedProcedure
		private void cmdSearchSp_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			
		}

		private void DgSqlProcedure_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void ImageButton1_Click(object sender, System.EventArgs e)
		{
			try
			{
				visible();
				
				
				DgSqlProcedure.CurrentPageIndex = 0;
				string QueryText = "usp_SQLMGR_Get_Procedures";
				DataSet	Ds1= SqlHelper.ExecuteDataset(conn,CommandType.StoredProcedure,QueryText,new SqlParameter("@ProcName",txtSearchSp.Text));
				DgSqlProcedure.SelectedIndex=-1;
				fill_Grid(Ds1);
				if (DgSqlProcedure.Items.Count == 0)
				{
					lblNoRec.Visible = true;
					lblNoRec.Text = "No Record Found";
					lblmsg.Text ="";
					txtNoParam.Text="1";
					txtchk.Text = "1";
				}
				
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		
		
		
		
		

		
		


	

		
		

		

	

		
		
	}
}

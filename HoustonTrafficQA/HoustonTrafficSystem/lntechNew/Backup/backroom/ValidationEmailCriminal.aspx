﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidationEmailCriminal.aspx.cs" Inherits="HTP.backroom.ValidationEmailCriminal" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ValidationEmail</title>
 <script src="../Scripts/Validationfx.js" type="text/javascript"></script>
    <script type="text/javascript">
    
        
        function ValidationEmail()
		{		
		var email = form1.tb_emailaddress;		
		if (  email.value != "")
		{
		    if( isEmail(email.value)== false)
			{
			    alert ("Please enter Email Address in Correct format.");
			    email.focus(); 
			    return false;			   
			}		
		}		
		
		 var doyou = confirm("Are you sure you want to run Validation Email Report (OK = Yes   Cancel = No)"); 
         if (doyou == true)
            {return true;}         
         return false;			         
		}
		
		
		function showbobbittAddress()
		{		
		    document.form1.tb_emailaddress.value='<%= ViewState["ValidationEmailOthers"] %>';
		}
		
		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                width="820px">
                <tbody>
                    <tr>
                        <td colspan="4" style="height: 14px">
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                     <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="left" class="clsLabel" style="width: 72%">
                                                    <asp:RadioButton ID="rb_demail" runat="server" Checked="True" GroupName="Email" Text="hcriminal@sullolaw.com" />&nbsp;
                                                    <asp:RadioButton ID="rb_other" runat="server" GroupName="Email" Text="Others" onclick="showbobbittAddress()" />
                                                    <asp:TextBox ID="tb_emailaddress" runat="server" CssClass="clsinputadministration"
                                                        Width="250px"></asp:TextBox>
                                                    <asp:Button ID="btn_email" runat="server" CssClass="clsbutton" OnClick="LinkButton1_Click"
                                                        Text="Send Email" /></td>
                                                <td align="left" style="width: 28%" valign="middle">
                                                    <asp:Button ID="btn_displayreport" runat="server" CssClass="clsbutton" OnClick="btn_displayreport_Click"
                                                        Text="Generate Validation  Report" Width="205px" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width" align="center" background="../images/separator_repeat.gif" style="height: 12px">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" >
                                        <asp:Label ID="lbl_result" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Panel ID="pnl_report" runat="server" Height="600px" ScrollBars="Both" Visible="False"
                                            Width="100%" Wrap="False">
                                        </asp:Panel>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td background="../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <uc1:Footer ID="Footer1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>


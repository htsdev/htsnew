<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.frmTemplateDetail" Codebehind="frmTemplateDetail.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Template Detail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
		<script language="javascript">

		function ValidateInput()
		{	
			var error = "Please enter correct value.Only float and numeric value is allowed";
			var error1 = "Please type Template Name";
		
			
			var lblMessage = document.getElementById("lblMessage");
			
			var txtTemplateName = document.getElementById("txtTemplateName");
			if (txtTemplateName.value=="")
			{
				//lblMessage.innerText = error1;
				alert(error1)
				txtTemplateName.focus();
				return false;
			}
			
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var grdName = "dg_newplan";
			var idx=2;
			var bol = true;
			//alert("asli");
			for (idx=2; idx < (cnt+2); idx++)
			{
				var ctlBasePrice = "";
				var ctlsecprice = "";
				var ctlbasepcent = "";
				var ctlsecpcent = "";
				var ctlbondbase = "";
				var ctlbondsec = "";
				var ctlbondbasepcent= ""; 
				var ctlbondsecpcent = "";
				var ctlbondassump = "";
				var ctlfineassump = "";
				
				if( idx < 10)
				{
				 ctlBasePrice = grdName+ "_ctl0" + idx + "_txt_BasePrice";
				 ctlsecprice = grdName+ "_ctl0" + idx + "_txt_secprice";
				 ctlbasepcent = grdName+ "_ctl0" + idx + "_txt_basepcent";
				 ctlsecpcent = grdName+ "_ctl0" + idx + "_txt_secpcent";
				 ctlbondbase = grdName+ "_ctl0" + idx + "_txt_bondbase";
				 ctlbondsec = grdName+ "_ctl0" + idx + "_txt_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl0" + idx + "_txt_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl0" + idx + "_txt_bondassump";
				 ctlfineassump = grdName+ "_ctl0" + idx + "_txt_fineassump";
				}
				else
				{
				ctlBasePrice = grdName+ "_ctl" + idx + "_txt_BasePrice";
				 ctlsecprice = grdName+ "_ctl" + idx + "_txt_secprice";
				 ctlbasepcent = grdName+ "_ctl" + idx + "_txt_basepcent";
				 ctlsecpcent = grdName+ "_ctl" + idx + "_txt_secpcent";
				 ctlbondbase = grdName+ "_ctl" + idx + "_txt_bondbase";
				 ctlbondsec = grdName+ "_ctl" + idx + "_txt_bondsec";
				 ctlbondbasepcent = grdName+ "_ctl" + idx + "_txt_bondsecpcent";
				 ctlbondassump = grdName+ "_ctl" + idx + "_txt_bondassump";
				 ctlfineassump = grdName+ "_ctl" + idx + "_txt_fineassump";
				}

				/* testing for changing color
				txt_BasePrice.runtimeStyle.backgroundColor
				txt_BasePrice.currentStyle.color
					color	"#123160"	Variant
				txt_BasePrice.style.backgroundColor
				*/
				var txt_BasePrice = document.getElementById(ctlBasePrice);	
				var txt_secprice =document.getElementById(ctlsecprice);	
				var txt_basepcent = document.getElementById(ctlbasepcent);	
				var txt_secpcent = document.getElementById(ctlsecpcent);	
				var txt_bondbase =document.getElementById(ctlbondbase);	
				var txt_bondsec =document.getElementById(ctlbondsec);	
				var txt_bondbasepcent = document.getElementById(ctlbondbasepcent);	
				var txt_bondsecpcent = document.getElementById(ctlbondsecpcent);	
				var txt_bondassump = document.getElementById(ctlbondassump);	
				var txt_fineassump =document.getElementById(ctlfineassump);	
				
				bol = true;
				
				bol = ValidationFloat(txt_BasePrice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secprice);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_basepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_secpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbase);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsec);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondbasepcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondsecpcent);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_bondassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}

				bol = ValidationFloat(txt_fineassump);
				if (bol==false)
				{
					alert(error)
					return false;
				}
			
				if (bol==false)
				{
					alert(error)
					return false;
				}
			}
			return bol;
		}
		
		function ValidationFloat(TextBox)
		{
				//alert(TextBox.value);
				//var error = "Please correct value. Only float value is allowed";
				var lblMessage = document.getElementById("lblMessage");

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
					if (isFloat(TextBox.value)==false) 
					{
						//if -ve and not float
						//lblMessage.innerText = error;
						TextBox.focus();
						
						return false;
					}
					else
					{
						lblMessage.innerText = "";
						return true;
					}
				}
				
		}


/*

			function ValidateBasePrice(crtl_BasePrice)
			{
				var error = "Please correct value";
				var txt_BasePrice = document.getElementById(crtl_BasePrice);
				var lblMessage = document.getElementById("lblMessage");
			
				if(txt_BasePrice.value!="")
				{

					if (isFloat(txt_BasePrice.value)==false) //if -ve
					{
						lblMessage.innerText = error;
						txt_BasePrice.focus();
					}
					else
					{
						lblMessage.innerText = "";
					}
					
				}
				else
				{
					txt_BasePrice.value = 0;
				}
			}
		
			function ValidateSecPrice()
			{
			var error = "Please correct value";
				var txt_BasePrice = document.getElementById(crtl_BasePrice);
				var lblMessage = document.getElementById("lblMessage");
			
				if(txt_BasePrice.value!="")
				{

					if (isFloat(txt_BasePrice.value)==false) //if -ve
					{
						lblMessage.innerText = error;
						txt_BasePrice.focus();
					}
					
				}
				else
				{
					txt_BasePrice.value = 0;
				}

			}
			function ValidateBasePcent()
			{
				var error = "Please correct value";
				var txt_BasePrice = document.getElementById(crtl_BasePrice);
				var lblMessage = document.getElementById("lblMessage");
			
				if(txt_BasePrice.value!="")
				{


					if (isNonnegativeInteger(txt_BasePrice.value)==false)
					{
						lblMessage.innerText = error;
						txt_BasePrice.focus();
						alert(error);
					}
					
				}
				else
				{
					txt_BasePrice.value = 0;
				}
			}
		*/
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<TR>
					<TD><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="TableHeader" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 140px">Template Name:</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 227px">&nbsp;
									<asp:textbox id="txtTemplateName" runat="server" CssClass="clsinputadministration" Width="144px"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable" align="left">&nbsp;
									<asp:hyperlink id="HyperLink1" runat="server" NavigateUrl="TemplateMain.aspx">Back to Template Main</asp:hyperlink></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 140px">Template Description:</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 227px">&nbsp;
									<asp:textbox id="txtTemplateDesc" runat="server" CssClass="clsinputadministration" Width="144px"
										TextMode="MultiLine"></asp:textbox></TD>
								<TD class="clsLeftPaddingTable">&nbsp;</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 140px">&nbsp;</TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 227px">&nbsp;</TD>
								<TD class="clsLeftPaddingTable" align="left"><asp:button id="btnSave" runat="server" CssClass="clsbutton" Text="Save Changes"></asp:button></TD>
							</TR>
						</TABLE>
						<asp:label id="lblMessage" runat="server" Width="320px" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<tr>
					<td background="../../images/headbar_headerextend.gif" height="5"></td>
				</tr>
				<tr>
					<td class="clssubhead" background="../../images/headbar_midextend.gif" height="13"></td>
					<td class="clssubhead" background="../../images/headbar_midextend.gif" height="13"></td>
				</tr>
				<tr>
					<td background="../../images/headbar_footerextend.gif" height="10"></td>
				</tr>
				<tr>
					<td style="HEIGHT: 172px">
						<table id="tblgrid" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TR>
								<TD align="center"><asp:datagrid id="dg_newplan" runat="server" CssClass="clsLeftPaddingTable" Width="780px" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Violation Category">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_voilcategory runat="server" Width="116px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.categoryname") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="DetailID">
												<ItemTemplate>
													<asp:Label id=lbl_DetailID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DetailID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="TemplateID">
												<ItemTemplate>
													<asp:Label id=lbl_TemplateID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="CategoryID">
												<ItemTemplate>
													<asp:Label id=lbl_CategoryID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_BasePrice runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BasePrice", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary Price">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_secprice runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPrice", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_basepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_secpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.SecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondbase runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBase", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondsec runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondary", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Base%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondbasepcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondBasePercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Secondary%">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondsecpcent runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondSecondaryPercentage", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Bond Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_bondassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.BondAssumption", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Fine Assumption">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=txt_fineassump runat="server" Width="45px" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.FineAssumption", "{0:F}") %>' MaxLength="5">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</table>
					</td>
				</tr>
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="6" height="11"></td>
				</tr>
				<TR>
					<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
			<INPUT id="txtHidden" type="hidden" runat="server">
		</form>
	</body>
</HTML>

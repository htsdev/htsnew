﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components.Services;
using lntechNew.Components;

namespace HTP.backroom
{
    ///<summary>
    ///</summary>
    public partial class AdminLog : System.Web.UI.Page
    {
        #region variables

        private DataView _dvCom = new DataView();
        bool _isSearch;
        readonly clsSession _cSession = new clsSession();

        #endregion

        #region Properites

        ///<summary>
        /// Property for sort direction
        ///</summary>
        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Descending; //Sabir Khan 9934 12/02/2011 Change defualt sort direction to Descending order
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        ///<summary>
        /// Property for sort expression
        ///</summary>
        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_cSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        cal_FromDateFilter.SelectedDate = DateTime.Today;
                        cal_ToDateFilter.SelectedDate = DateTime.Today;
                        FillddlReport();                        
                    }

                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_Records;


                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                clsLogger.ErrorLog(ex);
                lbl_Message.Text = ex.ToString();
            }
        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                BindGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                BindGrid();
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }        

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _isSearch = true;
                GridViewSortExpression = "RecDate";              
                BindGrid();
                _isSearch = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }                                               

        #endregion

        #region Method

        /// <summary>
        /// Fill firm drop down of attorneys
        /// </summary>
        private void FillddlReport()
        {
            try
            {
                
                ReportCategory rc=new ReportCategory();
                
                DataTable dt = rc.GetReportsName(true);                
                
                if (dt.Rows.Count > 0)
                {                    
                    ddlReport.DataTextField = "TITLE";
                    ddlReport.DataValueField = "ID";
                    ddlReport.DataSource = dt;
                    ddlReport.DataBind();
                }
                ddlReport.Items.Insert(0, new ListItem("---- Choose ----", "-1"));
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
            }
        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                if (Session["_dvCom"] != null)
                {
                    gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// get data and bind grid for comment report
        /// </summary>
        private void BindGrid()
        {
            DataTable dt = new DataTable();
            ShowSettingHistoryService sshs = new ShowSettingHistoryService();
            if (_isSearch)
            {
                dt = sshs.GetShowSettingHistory(cal_FromDateFilter.SelectedDate, cal_ToDateFilter.SelectedDate,Convert.ToInt32(ddlReport.SelectedValue),chk_ShowAll.Checked);
                Session["_dvCom"] = dt.DefaultView;
                gv_Records.PageSize = 20;
                gv_Records.PageIndex = 0;
                _isSearch = false;
            }

            if (Session["_dvCom"] != null && dt.Rows.Count == 0)
            {
                _dvCom = (DataView)Session["_dvCom"];
                dt = _dvCom.ToTable();

            }
            if (dt.Rows.Count > 0)
            {
                
                _dvCom = dt.DefaultView;
                _dvCom.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                dt = _dvCom.ToTable();
                
                _dvCom = dt.DefaultView;
                Session["_dvCom"] = _dvCom;
                gv_Records.DataSource = _dvCom;
                gv_Records.DataBind();

                Pagingctrl.Visible = true;
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                gv_Records.Visible = true;
                lbl_Message.Text = "";
            }
            else
            {
                
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Visible = true;
                lbl_Message.Text = "No Records Found";
                gv_Records.Visible = false;
            }

        }

        #endregion
        
    }
}

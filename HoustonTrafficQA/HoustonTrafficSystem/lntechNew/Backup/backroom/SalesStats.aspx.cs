using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Backroom
{
	
	public partial class SalesStats : System.Web.UI.Page
	{
		
		protected System.Web.UI.WebControls.DataGrid dg_valrep;
		protected System.Web.UI.WebControls.Button btn_submit;
		DataSet ds_val;
		protected System.Web.UI.WebControls.Label lbl_message;
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.DropDownList DdlCity;
		protected eWorld.UI.CalendarPopup calQueryFrom;
		protected eWorld.UI.CalendarPopup calQueryTo;
		protected System.Web.UI.WebControls.DropDownList ddlEmployee;
		protected System.Web.UI.WebControls.DropDownList ddl_rpttype;
		clsSession ClsSession=new clsSession();
		clsLogger clog= new clsLogger();
		
		private void Page_Load(object sender, System.EventArgs e)
		{		
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (ClsSession.GetCookie("sAccessType",this.Request).ToString()!="2")
					{
						Response.Redirect("../LoginAccesserror.aspx",false);
						Response.End();
					}
					else //To stop page further execution
					{

						lbl_message.Text ="";

			
						if(!IsPostBack)
						{
							
							
                            //calQueryFrom.SelectedDate = DateTime.Parse("2/12/1984");
                            //calQueryTo.SelectedDate=DateTime.Parse("8/2/2006");
                            calQueryFrom.SelectedDate = DateTime.Today;
                            calQueryTo.SelectedDate = DateTime.Today;
                
							ddlEmployee.SelectedIndex = 2;
							DdlCity.SelectedIndex = 1;
							ddl_rpttype.SelectedIndex = 1;

							Fill_Employee_In_DropDownBox();							
							btn_submit.Attributes.Add("OnClick", "return validate();");
						}
			
					}
				}
			}
			catch ( Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

			//Comment By Zeeshan 
			//if(dg_valrep.Items.Count<1)
			{
				//lblCurrPage.Visible=false;
				//lblGoto.Visible=false;
				//lblPNo.Visible=false;
				//cmbPageNo.Visible=false;
			}			
		}
		private void Fill_Employee_In_DropDownBox()
		{
            //Change by Ajmal
            //SqlDataReader Dr_employee = null;
			IDataReader Dr_employee=null;
			try
			{
				if(DdlCity.SelectedValue =="0")
					Dr_employee = ClsDb.Get_DR_BySP("USP_HTS_list_all_users");
				else 
					Dr_employee =  ClsDb.Get_DR_BySP("USP_HTS_list_all_Dallas_users");

				ddlEmployee.DataTextField = "username";
				ddlEmployee.DataValueField = "employeeid";
				ddlEmployee.DataSource = Dr_employee;
				ddlEmployee.DataBind();	
			
				// Adding ALL Option

				ListItem item= new ListItem("All","0");
				ddlEmployee.Items.Insert(0,item);

			}
			catch (Exception ex) 
			{
				lbl_message.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

			}
			finally
			{
				Dr_employee.Close();
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_submit.Click += new System.EventHandler(this.Button1_Click);
			this.dg_valrep.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_valrep_PageIndexChanged);
			this.dg_valrep.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_valrep_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	
		public void calculateSummary ( DataSet ds )
		{
			
			// This function Calculate Summary Information and Disp

			try
			{
                	
				// Create a row for storing Summary Information
				DataRow dr_Summary = ds.Tables[0].NewRow();
				
				double totalClicks=0 , totalquotes=0,totalclients=0,totalrevg=0,totalpotrev=0;
			
		
			
			
				foreach ( DataRow dr in ds.Tables[0].Rows )
				{
	

					totalClicks+= Convert.ToDouble(dr[8]);
					totalquotes+= 	Convert.ToDouble(dr[4]);			
					totalclients+=Convert.ToDouble(dr[3]);
					totalrevg+=Convert.ToDouble(dr[5]);
					totalpotrev+=Convert.ToDouble(dr[6]);
			
				}

						
				// Save Summary Information
				dr_Summary[1]= "0";
				dr_Summary[2]= "Total";
				dr_Summary[3]= 	totalclients.ToString();
				dr_Summary[4]= totalquotes.ToString();
				dr_Summary[7]= "0";
				dr_Summary[6]= totalpotrev.ToString();
				dr_Summary[5]= totalrevg.ToString();
				dr_Summary[8]= totalClicks.ToString();
			
				// Add Summary Row to the Dataset
				ds.Tables[0].Rows.Add(dr_Summary);

		
			}
			catch(Exception ex)
			{
				string str = ex.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		
		}
		
		
		
		private void Button1_Click(object sender, System.EventArgs e)
		{
			// Comments By Zeeshan Jur
			//dg_valrep.CurrentPageIndex=0;
            FillGrid();		
		}

		private void FillGrid()
		{
			
			
			
			string[] key={"@startdate","@enddate","@employeeid","@reporttype"};
			object[] value1={calQueryFrom.SelectedDate,calQueryTo.SelectedDate,ddlEmployee.SelectedValue,ddl_rpttype.SelectedValue};
			string Sp_Name=null;

			if (DdlCity.SelectedValue =="0")
				Sp_Name = "USP_HTS_GetRetentionReport_VER1";
			else 
				Sp_Name = "usp_hts_GetDallasRetentionReport_VER1";
			

			try
			{
			
				ds_val=ClsDb.Get_DS_BySPArr(Sp_Name.ToString(),key,value1);			
            
				lbl_message.Text="";
				if(ds_val.Tables[0].Rows.Count<1)
				{
					lbl_message.Text="No Record Found";		
					dg_valrep.Visible=false;
				}

				else
				{
				
					calculateSummary(ds_val);
					dg_valrep.Visible=true;
				
					dg_valrep.DataSource=ds_val;							
					dg_valrep.DataBind();
					
					for (int i=0 ; i < 8 ; i++)
					dg_valrep.Items[dg_valrep.Items.Count -1].Cells[i].BackColor= System.Drawing.ColorTranslator.FromHtml("#EEEEEE");
		
					// Comments By Zeeshan Jur
					//FillPageList();
					//SetNavigation();
					 
				}
			}
			catch( Exception ex)
			{	
				lbl_message.Text=ex.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		
	
		#region Navigation

		//----------------------------------Navigational LOGIC-----------------------------------------//
		
		// Procedure for filling page numbers in page number combo........
	/*	private void FillPageList()
		{
			Int16 idx;

			cmbPageNo.Items.Clear();
			for (idx=1; idx<=dg_valrep.PageCount;idx++)
			{
				cmbPageNo.Items.Add ("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value=idx.ToString();
			}
		}	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(dg_valrep.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;

				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();

				cmbPageNo.SelectedIndex =dg_valrep.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lbl_message.Text = ex.Message ;
			}
		}

		// Procedure for setting the grid page number as per selected from page no. combo.......
		private void cmbPageNo_SelectedIndexChanged_1(object sender, System.EventArgs e)
		{
			try
			{				
				dg_valrep.CurrentPageIndex= cmbPageNo.SelectedIndex ;												
				FillGrid();
			}
			catch (Exception ex)
			{
				lbl_message.Text=ex.Message;
			}
		}*/
#endregion

		private void dg_valrep_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg_valrep.CurrentPageIndex=e.NewPageIndex;
			FillGrid();
		}

		private void dg_valrep_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
                //Noufil 3932 05/21/2008 ListItemType declare for handling null values in header
                if (e.Item.ItemType == ListItemType.Item)
                {
                    int totalClient = Convert.ToInt32(((Label)e.Item.FindControl("lbl_totalclient")).Text);
                    int totalQuote = Convert.ToInt32(((Label)e.Item.FindControl("lbl_totalquote")).Text);
                    double retention = 0;

                    if ((totalClient + totalQuote) != 0 && totalClient != 0)
                    {
                        retention = (double)(totalClient * 100) / (totalClient + totalQuote);
                    }

                    ((Label)e.Item.FindControl("lbl_totalcalls")).Text = Convert.ToString((totalClient + totalQuote));
                    ((Label)e.Item.FindControl("lbl_retention")).Text = retention.ToString("#00.00") + "%";
                }						
			}
			catch( Exception ex)
			{
                lbl_message.Text = ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);			
			}
		}
		


		
	}
}

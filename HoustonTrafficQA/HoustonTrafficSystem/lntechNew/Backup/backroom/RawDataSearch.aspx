<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RawDataSearch.aspx.cs"
    Inherits="HTP.backroom.RawDataSearch" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Raw Data Search</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script language="javascript">
function ValidateInput()
{    
    // Validating ticket number field
    //Farrukh 10304 08/23/2012 added check for Tarrant County Criminal Loaders
    var rb_TicketNumber = document.getElementById("rb_TicketNumber").checked;        
    var loaderId = document.getElementById('ddlLoader').value;	
    var rb_LastNameDOB =  document.getElementById("rb_LastName_DOB").checked;
    var txtLastName = trim(document.getElementById("txt_LastName").value);
    var txtFirstName = trim(document.getElementById("txt_FirstName").value);
    var txtMiddleName = trim(document.getElementById("txt_MiddleName").value);
    var txtDOB = trim(document.getElementById("txt_DOB").value);
    var txtCauseNumber = trim(document.getElementById("txt_CauseNumber").value);
    var txtFileDate = document.getElementById("txtFileDate").value;
    var txt_CrtDate = document.getElementById("txt_CrtDate").value;      
         
    if(loaderId == 63 || loaderId == 64)
    {   
        if(txtLastName == "" && txtFirstName == "" && txtFileDate == "" && txt_CrtDate == "")
        {    
              alert("Please enter 'Name'");
             return false;
        }
        else
            return true;
    }
    else if(loaderId == 61 || loaderId == 62)
    {
        if(txtLastName == "" && txtFirstName == "" && txtCauseNumber == "" && txtFileDate == "")
        {
            alert("Please enter cause number");
             return false;
        }
        else
            return true;
    }
     
    if(rb_TicketNumber == true)
    {
        // Noufil 5835 05/06/2009 Use trim method
        var txtTicketNumber = trim(document.getElementById("txt_TicketNumber").value);        
        if(txtTicketNumber=="") {
                alert("Please enter a ticket number");                
                document.getElementById("txt_TicketNumber").focus();
                return false;
            }
          else
            return true;    
    }    
    //Validating CauseNumber field    
    var rb_CauseNumber = document.getElementById("rb_CauseNumber").checked;  
    
    
    if(rb_CauseNumber == true)
    {
    
        // Noufil 5835 05/06/2009 Use trim method
        
        if(txtCauseNumber=="")
            {
                var radioButton= document.getElementById("rb_CauseNumber")
                var arrLabel= radioButton.parentNode.getElementsByTagName('label');
                alert("Please enter a " + arrLabel[0].innerHTML);
               document.getElementById("txt_CauseNumber").focus();
                return false;
             }
             else
            return true;    
    }    
    //Validating Last Name & DOB
        // Noufil 5835 05/06/2009 Use trim method
       
    
    if(rb_LastNameDOB == true)
    {
        //Last name and DOB both are empty
        if(txtLastName == "" && txtFirstName == "" && txtMiddleName == "" && txtDOB=="")// (txtMonth =="" && txtDay == "" && txtYear ==""))
        {
            if (td_DOB.style.display == "none")
            {
                alert("Please enter 'Name'");
            }
            else
            {
                alert("Please enter either any of the 'Name' or 'Date of Birth'");
            }
                document.getElementById("txt_LastName").focus();
                return false;
        }

        //Invalid Characters in Last Name        
        if (checkName(txtLastName) == false) 
        {
            var radioButton= document.getElementById("rb_LastName_DOB")
            var arrLabel= radioButton.parentNode.getElementsByTagName('label');
            alert(arrLabel[0].innerHTML + " cannot contain Digits and any special characters");
                document.getElementById("txt_LastName").focus();
                return false;
        }
        //Invalid Characters in First Name                
        if (checkName(txtFirstName) == false) 
        {
            alert(document.getElementById("lbl_FirstName").innerText + " cannot contain Digits and any special characters");
                document.getElementById("txt_FirstName").focus();
                return false;
        }
        
        
        if (checkName(txtMiddleName) == false) 
        {
            alert("M cannot contain Digits and any special characters");
                document.getElementById("txt_MiddleName").focus();
                return false;
        }
        

        
        
        

        
         // Last name is not empty and DOB is empty
        if(txtLastName != "" && txtDOB=="")//(txtMonth =="" && txtDay == "" && txtYear ==""))
            {return true;}
        
         // First name is not empty and DOB is empty
        if(txtFirstName != "" && txtDOB=="")//(txtMonth =="" && txtDay == "" && txtYear ==""))
             {return true;}
        
         // Middle name is not empty and DOB is empty
        if(txtMiddleName != "" && txtDOB=="")//(txtMonth =="" && txtDay == "" && txtYear ==""))
             {return true;}
        
        
         // Last, first, middle name is empty but DOB is not empty
        if(txtLastName == "" && txtFirstName == "" && txtMiddleName == "" && txtDOB!="")// (txtMonth !="" || txtDay != "" || txtYear !=""))
        {         
		    if (!MMDDYYYYDate(txtDOB))
		    {			  
		        document.getElementById("txt_DOB").focus();
		        return(false);
		    }
        }
        
        if(txtLastName != "" && txtDOB!="")//(txtMonth !="" || txtDay != "" || txtYear !=""))
        {
		    if (!MMDDYYYYDate(txtDOB))
		    {			  
		        document.getElementById("txt_DOB").focus();
		        return(false);
		    }	
        }
        
        if(txtFirstName != "" && txtDOB!="")//(txtMonth !="" || txtDay != "" || txtYear !=""))
        {
		    if (!MMDDYYYYDate(txtDOB))
		    {			  
		        document.getElementById("txt_DOB").focus();
		        return(false);
		    }	
        }
        
         if(txtMiddleName != "" && txtDOB!="")//(txtMonth !="" || txtDay != "" || txtYear !=""))
        {
		    if (!MMDDYYYYDate(txtDOB))
		    {			  
		        document.getElementById("txt_DOB").focus();
		        return(false);
		    }	
        }
        
        
    }
    
    //Validating DL Number    
    var rbFileDate =  document.getElementById("rbFileDate").checked;    
    if(rbFileDate==true)
    {         
         if(!MMDDYYYYDate(txtFileDate))
         {            
            document.getElementById("txtFileDate").focus();
            return false;
         }
         else{return true;}
    } 
    
    //Validating DL Number    
    var rb_CrtDate =  document.getElementById("rb_CrtDate").checked;    
    if(rb_CrtDate==true)
    {         
         if(!MMDDYYYYDate(txt_CrtDate))
         {            
            document.getElementById("txt_CrtDate").focus();
            return false;
         }
         else{return true;}
    }                  
  }
    
function ResetControls()
{
    //5835 Fahad 05/29/2009 Add following reset controls
    trName.style.display='block';
    tdrbCrtDate.style.display='block';
    tdtxtCrtDate.style.display='block';
    td_DOBname.style.display="block";
    td_DOB.style.display="block";
    td_MiddleName.style.display="Block";
    td_txtMiddleName.style.display="Block";
    tdBlank.style.display= "block";
    tdrbTicktNo.style.display= "block";
    tdtxtTicketNo.style.display= "block";
    tdrbCauseNo.style.display= "Block";
    tdtxtCauseNo.style.display= "Block";
    
    document.getElementById("rb_TicketNumber").checked=true;    
    document.getElementById("txt_TicketNumber").value="";
    document.getElementById("txt_CauseNumber").value="";    
    document.getElementById("txt_LastName").value="";   
    document.getElementById("txt_FirstName").value="";   
    document.getElementById("txt_MiddleName").value="";   
    document.getElementById("txt_DOB").value="";
    document.getElementById("txtFileDate").value="";        
    document.getElementById("txt_CrtDate").value="";        
    document.getElementById("ddlLoader").value=1;
    document.getElementById("lblMessage").innerText="";
    
    if (document.getElementById("divPaging") != null) 
    {
        document.getElementById("divPaging").style.display ="none";    
    }
    
    if (document.getElementById("divExport") != null) 
    {
        document.getElementById("divExport").style.display ="none";    
    }
    
    if (document.getElementById("divPgDropDown") != null) 
    {
        document.getElementById("divPgDropDown").style.display ="none";    
    }
    
    
    //Saeed 8127 08/07/2010 comment out calling of OnLoaderChange.
    //OnLoaderChange();
    
    var chk = document.getElementById("gv_Details");
    if ( chk !=null)
    {
        document.getElementById("gv_Details").style.display='none';
    }
    return false;
}
    
function KeyDownHandler()
{
	// process only the Enter key
	if (event.keyCode == 13)
	{
		// cancel the default submit
		event.returnValue=false;
		event.cancel = true;
		// submit the form by programmatically clicking the specified button
		document.getElementById("btn_Submit").click();
	}
}
	
function OnLoaderChange()
{
    var loaderId = document.getElementById('ddlLoader').value;	    
    var trName = document.getElementById('trName');
    var tdrbCrtDate = document.getElementById('tdrbCrtDate');
    var tdtxtCrtDate = document.getElementById('txt_CrtDate');

    //Farrukh 10447 12/19/2012 Added New fileds for Texas Dept of Transportation Loaders
    var tdrbCrashId = document.getElementById('tdrbCrashId');
    var tdtxtCrashId = document.getElementById('tdtxtCrashId');
    var tdrbCaseId = document.getElementById('tdrbCaseId');
    var tdtxtCaseId = document.getElementById('tdtxtCaseId');
    
    //Saeed 8127 08/06/2010 initially hide all search criterias.
    trName.style.display='none';
    tdrbCrtDate.style.display='none';
    tdtxtCrtDate.style.display='none';
    td_DOBname.style.display="none";
    td_DOB.style.display="none";
    tdBlank.style.display= "none";
    tdrbTicktNo.style.display= "none";
    tdtxtTicketNo.style.display= "none";
    tdrbCauseNo.style.display= "none";
    tdtxtCauseNo.style.display= "none";
    td_MiddleName.style.display="none";
    td_txtMiddleName.style.display="none";
    //Saeed 8127 08/06/2010 END
    var radioButton= document.getElementById("rb_CauseNumber")
    var arrLabel= radioButton.parentNode.getElementsByTagName('label');
    if(arrLabel.length>0)
    {
        arrLabel[0].innerHTML="Cause Number";
    }
    radioButton= document.getElementById("rb_LastName_DOB")
    arrLabel= radioButton.parentNode.getElementsByTagName('label');
    if(arrLabel.length>0)
    {
        arrLabel[0].innerHTML="Last Name";
    }
    radioButton= document.getElementById("rb_CrtDate")
    arrLabel= radioButton.parentNode.getElementsByTagName('label');
    if(arrLabel.length>0)
    {
        arrLabel[0].innerHTML="Crt Date";
    }
    document.getElementById("lbl_FirstName").innerText = 'First Name';
    
    switch(loaderId)
    {
        // Saeed 8127 08/06/2010 javascript to show/hide search criteria for 'HMC-Arraignment' & 'HMC-DLQ'
        case "2":
        case "3":
            trName.style.display='';
            tdrbCrtDate.style.display='';
            tdtxtCrtDate.style.display='';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            document.getElementById("rb_TicketNumber").checked=true;
            break;
        
        case "4":
            trName.style.display='none';
            tdrbCrtDate.style.display='';
            tdtxtCrtDate.style.display='';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            document.getElementById("rb_TicketNumber").checked='true';
            
            break;
            
        
        case "5":
            trName.style.display='none';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            document.getElementById("rb_TicketNumber").checked='true';
            break;
        // Noufil 5335 12/19/2008 javascript to hide DOB in sugerland report    
        case "6":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            document.getElementById("rb_TicketNumber").checked='true';
            break;
        // Adil 5374 1/1/2009 HMC Dispossed cases raw data search
        case "7":
            trName.style.display='none';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            document.getElementById("rb_TicketNumber").checked='true';
            break;
         //FAHAD 5581 02/27/2009 FORT WORTH CASES RAW SEARCH
         case "8":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
            //FAHAD 5835 22/05/2009 DMC Arraginment CASES RAW SEARCH
         case "9":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
            //FAHAD 5835 22/05/2009 DMC-Bonds CASES RAW SEARCH
         case "10":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
             //FAHAD 5835 22/05/2009 DCJP CASES RAW SEARCH
         case "12":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="Block";
            td_txtMiddleName.style.display="Block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_CauseNumber").checked='true';
            break;
            //FAHAD 5835 22/05/2009 DCCC-PTH CASES RAW SEARCH
         case "13":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
            //FAHAD 5835 22/05/2009 DCCC-BI CASES RAW SEARCH
         case "14":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
             //FAHAD 5835 22/05/2009 Irving Arraginment CASES RAW SEARCH
         case "15":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="Block";
            td_txtMiddleName.style.display="Block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
             //FAHAD 5835 22/05/2009 Irving Bonds CASES RAW SEARCH
         case "16":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="Block";
            td_txtMiddleName.style.display="Block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_TicketNumber").checked='true';
            break;
             //FAHAD 5835 22/05/2009 HCJP(Disposed,Entered,Event) CASES RAW SEARCH
         case "17":
         case "18":
         case "19":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="Block";
            td_txtMiddleName.style.display="Block";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            
            document.getElementById("rb_CauseNumber").checked=true;
            break;
        //FAHAD 5835 22/05/2009 HCCC CASES RAW SEARCH
        case "20":
        case "69": //Farrukh 10438 11/16/2012 Cases added HCCC Criminal Filings Daily, Monthly & Disposed-Monthly
        case "70":
        case "73":               
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            
            document.getElementById("rb_CauseNumber").checked=true;
            break;
        case "85": // Rab Nawaz 11277 09/20/2013 HCCC Scraper Loaders Added. . .               
            trName.style.display = 'block';
            tdrbCrtDate.style.display = 'none';
            tdtxtCrtDate.style.display = 'none';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            td_MiddleName.style.display = "none";
            td_txtMiddleName.style.display = "none";
            tdBlank.style.display = "block";
            tdrbTicktNo.style.display = "block";
            tdtxtTicketNo.style.display = "block";

            document.getElementById("rb_CauseNumber").checked = true;
            break;  
        case "71": //Farrukh 10438 11/16/2012 Cases added HCCC Criminal JIMS009 & JIMS203
        case "72":        
            trName.style.display = 'block';
            tdrbCrtDate.style.display = 'block';
            tdtxtCrtDate.style.display = 'block';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            td_MiddleName.style.display = "none";
            td_txtMiddleName.style.display = "none";
            tdBlank.style.display = "block";
            tdrbTicktNo.style.display = "block";
            tdtxtTicketNo.style.display = "block";

            document.getElementById("rb_CauseNumber").checked = true;
            break;
             //FAHAD 5835 22/05/2009 PMC Arraginment CASES RAW SEARCH
         case "21":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "None";
            tdrbCauseNo.style.display= "None";
            tdtxtCauseNo.style.display= "None";
            tdrbTicktNo.style.display= "None";
            tdtxtTicketNo.style.display= "None";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            break;
             //FAHAD 5835 22/05/2009 PMC Warrant CASES RAW SEARCH
         case "22":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "None";
            tdrbCauseNo.style.display= "None";
            tdtxtCauseNo.style.display= "None";
            tdrbTicktNo.style.display= "None";
            tdtxtTicketNo.style.display= "None";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            break;
         //Sabir Khan 5963 06/02/2009 Tickler Extract Data Search...
         //---------------------------------------------------------
         case "23":
            trName.style.display='None';
            tdrbCrtDate.style.display='None';
            tdtxtCrtDate.style.display='None';
            td_DOBname.style.display="None";
            td_DOB.style.display="None";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "None";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            document.getElementById("rb_CauseNumber").checked = true;
            document.getElementById("txt_CauseNumber").focus();
            
            break;
         //-----------------------------------------------------------    
         //FAHAD 6061 06/17/2009 Dallas Events CASES RAW SEARCH
         case "24":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdBlank.style.display= "None";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            
            document.getElementById("rb_TicketNumber").checked = true;
            document.getElementById("txt_TicketNumber").focus();
            break;
            //Adil 6592 10/08/2009 Arlington arraignment CASES RAW SEARCH
         case "25":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "None";
            tdtxtCauseNo.style.display= "None";
            
            document.getElementById("rb_TicketNumber").checked='true';
            document.getElementById("txt_TicketNumber").focus();
            break;
            //Adil 6592 10/08/2009 Arlington Warrant CASES RAW SEARCH
         case "26":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "None";
            tdtxtCauseNo.style.display= "None";
            
            document.getElementById("rb_TicketNumber").checked='true';
            document.getElementById("txt_TicketNumber").focus();
            break;
            //Adil 6848 10/27/2009 DCCC CBF raw data search added
        case "27":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_TicketNumber").checked='true';
            document.getElementById("txt_TicketNumber").focus();
            break;
            //Adil 6850 11/04/2009 DCCC BN raw data search added
        case "28":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "none";
            tdtxtTicketNo.style.display= "none";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
        //Waqas 6791 11/18/2009 for montgomery loader
        case "29":
            tdrbTicktNo.style.display = 'block';
            tdtxtTicketNo.style.display = 'block';
            tdrbCauseNo.style.display = 'block';
            tdtxtCauseNo.style.display = 'block';
            trName.style.display = 'block';
            td_DOBname.style.display = 'block';
            td_DOB.style.display = 'block';
            td_MiddleName.style.display = 'none';
            td_txtMiddleName.style.display = 'none';
            tdrbCrtDate.style.display = 'block';
            tdtxtCrtDate.style.display = 'block';
            tdBlank.style.display = 'block';
            document.getElementById("rb_LastName_DOB").checked = 'true';
            document.getElementById("txt_LastName").focus();
            break; 
            //Adil 7105 12/14/2009 Fort Worth raw data search added
        case "30":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7205 01/05/2010 Grand Prairie raw data search added
       case "31":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7235 01/20/2010 Grand Prairie raw data search added
       case "32":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7353 01/29/2010 Plano Arraignment raw data search added
       case "33":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7362 02/09/2010 Plano Warrant raw data search added
       case "34":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "none";
            tdtxtTicketNo.style.display= "none";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7364 02/17/2010 Stafford Arraignment raw data search added
       case "35":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "none";
            tdtxtCauseNo.style.display= "none";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7420 02/22/2010 Stafford Warrant raw data search added
       case "36":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7988 07/22/2010 SugarLand DL raw data search added
       case "37":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7989 07/22/2010 SugarLand Dispose raw data search added
       case "38":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
            // Adil 7990 07/22/2010 SugarLand Events raw data search added            
       case "39":   //Saeed 8127 08/06/2010 SugarLand Events id is 39.
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
       case "40":   //Adil 8117 08/24/2010 FW DLQ.
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
       case "41":   //Adil 8349 10/18/2010 Stafford Events.
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
        case "42":   //Kashif Jawed 8981 03/18/2011 PMC Events.
            trName.style.display='block';
            
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
       // Muhammad Nadir Siddiqui 9303 05/24/2011 added SOHO-Entered Loader             
       case "43":
       case "44":
       case "45":  
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
        // Adil 9303 06/06/2011 SoHo Arr Raw Search.    
        case "6":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            document.getElementById("rb_TicketNumber").checked='true';
            break;
      case "47":   //Farrukh 9618 08/22/2011 SMC Arraignment.
        trName.style.display='block';        
        tdrbCrtDate.style.display='block';
        tdtxtCrtDate.style.display='block';
        td_DOBname.style.display="block";
        td_DOB.style.display="block";
        td_MiddleName.style.display="none";
        td_txtMiddleName.style.display="none";
        tdrbTicktNo.style.display= "block";
        tdtxtTicketNo.style.display= "block";
        document.getElementById("rb_LastName_DOB").checked='true';
        document.getElementById("txt_LastName").focus();
        break;   
        // Adil 9698 09/29/2011 Jersey Village Arr Raw Search.    
        case "48":
            trName.style.display='block';
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            tdBlank.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "Block";
            tdtxtCauseNo.style.display= "Block";
            document.getElementById("rb_TicketNumber").checked='true';
            break;
        // Adil 9715 10/07/2011 Jersey Village Arr Ext Raw Search.    
        case "49":
            trName.style.display = 'block';
            tdrbCrtDate.style.display = 'block';
            tdtxtCrtDate.style.display = 'block';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            tdBlank.style.display = 'block';
            tdrbTicktNo.style.display = 'block';
            tdtxtTicketNo.style.display = 'block';
            tdrbCauseNo.style.display = 'block';
            tdtxtCauseNo.style.display = 'block';
            document.getElementById("rb_CauseNumber").checked = 'true';
            break;
            // Adil 9664 11/03/2011 Harris Business Startup Raw Search.    
        case "50":
            trName.style.display = 'block';
            tdrbCrtDate.style.display = 'block';
            tdtxtCrtDate.style.display = 'block';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            tdBlank.style.display = 'block';
            tdrbTicktNo.style.display = 'none';
            tdtxtTicketNo.style.display = 'none';
            tdrbCauseNo.style.display = 'block';
            tdtxtCauseNo.style.display = 'block';
            td_MiddleName.style.display = 'none';
            td_txtMiddleName.style.display = 'none';
            //debugger;
            var radioButton= document.getElementById("rb_CauseNumber")
            var arrLabel= radioButton.parentNode.getElementsByTagName('label');
            if(arrLabel.length>0)
            {
                arrLabel[0].innerHTML="File Number";
            }
            radioButton= document.getElementById("rb_LastName_DOB")
            arrLabel= radioButton.parentNode.getElementsByTagName('label');
            if(arrLabel.length>0)
            {
                arrLabel[0].innerHTML="Business Name";
            }
            radioButton= document.getElementById("rb_CrtDate")
            arrLabel= radioButton.parentNode.getElementsByTagName('label');
            if(arrLabel.length>0)
            {
                arrLabel[0].innerHTML="Reg Date";
            }
            document.getElementById("lbl_FirstName").innerText = 'Owner Name';
            
            document.getElementById("rb_CauseNumber").checked = 'true';
            
            break;
            // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court [Booked In] Raw Search.
        case "61":
            {
                trName.style.display = 'block';
                tdrbCrtDate.style.display = 'none';
                tdtxtCrtDate.style.display = 'none';
                td_DOBname.style.display = "none";
                td_DOB.style.display = "none";
                tdBlank.style.display = 'block';
                tdrbTicktNo.style.display = 'none';
                tdtxtTicketNo.style.display = 'none';
                tdrbCauseNo.style.display = 'block';
                tdtxtCauseNo.style.display = 'block';
                td_MiddleName.style.display = 'none';
                td_txtMiddleName.style.display = 'none';
                break;
            }
            // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court [Bond Out] Raw Search.
        case "62":
            {
                trName.style.display = 'block';
                tdrbCrtDate.style.display = 'none';
                tdtxtCrtDate.style.display = 'none';
                td_DOBname.style.display = "none";
                td_DOB.style.display = "none";
                tdBlank.style.display = 'block';
                tdrbTicktNo.style.display = 'none';
                tdtxtTicketNo.style.display = 'none';
                tdrbCauseNo.style.display = 'block';
                tdtxtCauseNo.style.display = 'block';
                td_MiddleName.style.display = 'none';
                td_txtMiddleName.style.display = 'none';
                break;
            }            
            // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court [First Setting] Raw Search.
        case "63":
            {
                trName.style.display = 'block';
                tdrbCrtDate.style.display = 'block';
                tdtxtCrtDate.style.display = 'block';
                td_DOBname.style.display = "none";
                td_DOB.style.display = "none";
                tdBlank.style.display = 'block';
                tdrbTicktNo.style.display = 'none';
                tdtxtTicketNo.style.display = 'none';
                tdrbCauseNo.style.display = 'none';
                tdtxtCauseNo.style.display = 'none';
                td_MiddleName.style.display = 'none';
                td_txtMiddleName.style.display = 'none';
                break;
            }
            // Zeeshan 10304 07/06/2012 Tarrant County Criminal Court [Pass To Hire] Raw Search.
        case "64":
            {
                trName.style.display = 'block';
                tdrbCrtDate.style.display = 'block';
                tdtxtCrtDate.style.display = 'block';
                td_DOBname.style.display = "none";
                td_DOB.style.display = "none";
                tdBlank.style.display = 'block';
                tdrbTicktNo.style.display = 'none';
                tdtxtTicketNo.style.display = 'none';
                tdrbCauseNo.style.display = 'none';
                tdtxtCauseNo.style.display = 'none';
                td_MiddleName.style.display = 'none';
                td_txtMiddleName.style.display = 'none';
                break;
            }
            // Rab Nawaz Khan 10401 08/15/2012 javascript added for the Missouri Arraignment Loader. . . 
        case "65":
            trName.style.display='block';        
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
        break;
        
         // Rab Nawaz Khan 10436 09/15/2012 javascript added for the Baytown Arraignment Loader. . . 
        case "66":
            trName.style.display='block';        
            tdrbCrtDate.style.display='block';
            tdtxtCrtDate.style.display='block';
            td_DOBname.style.display="block";
            td_DOB.style.display="block";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
            break;
        // Farrukh 10447 12/19/2012 Case added for Crash Data Texas Dept of Transportation & Person Data Texas Dept of Transportation. . . 
        case "67":
            trName.style.display = 'none';
            tdrbCrtDate.style.display = 'none';
            tdtxtCrtDate.style.display = 'none';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            td_MiddleName.style.display = "none";
            td_txtMiddleName.style.display = "none";
            tdrbTicktNo.style.display = "none";
            tdtxtTicketNo.style.display = "none";
            tdrbCrashId.style.display = "block";
            tdrbCaseId.style.display = "block";
            tdtxtCaseId.style.display = "block";
            tdtxtCrashId.style.display = "block";
            break;
        case "68":
        case "77": // Farrukh 11309 08/07/2013 Unit, Charges, Endorsements, Damages, Driver Added. . .
        case "78":
        case "79":
        case "81":
        case "83":
            trName.style.display = 'none';
            tdrbCrtDate.style.display = 'none';
            tdtxtCrtDate.style.display = 'none';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            td_MiddleName.style.display = "none";
            td_txtMiddleName.style.display = "none";
            tdrbTicktNo.style.display = "none";
            tdtxtTicketNo.style.display = "none";
            tdrbCrashId.style.display = "block";
            tdtxtCrashId.style.display = "block";
            break;
            
        // Zeeshan 10595 01/08/2013 Austin Municipal Loader
        case "74":
            trName.style.display = "block";
            tdrbCrtDate.style.display = "block"
            tdtxtCrtDate.style.display = "block";
            td_MiddleName.style.display = "block";
            td_txtMiddleName.style.display = "block";
            tdrbTicktNo.style.display = "block";
            tdtxtTicketNo.style.display = "block";
            tdrbCauseNo.style.display = "block";
            tdtxtCauseNo.style.display = "block";
            tdrbCrashId.style.display = "none";
            tdtxtCrashId.style.display = "none";
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";            
            break; 
            
       // Rab Nawaz 10729 03/21/2013 NISI Cases Loader added. . . 
       case "75":
            trName.style.display='block';
            tdrbCrtDate.style.display='none';
            tdtxtCrtDate.style.display='none';
            td_DOBname.style.display="none";
            td_DOB.style.display="none";
            td_MiddleName.style.display="block";
            td_txtMiddleName.style.display="block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            document.getElementById("rb_LastName_DOB").checked='true';
            document.getElementById("txt_LastName").focus();
        break;  
        
       // Rab Nawaz 10729 03/26/2013 Houston Docket Extract Loader added. . . 
       case "76":
            trName.style.display='None';
            tdrbCrtDate.style.display='None';
            tdtxtCrtDate.style.display='None';
            td_DOBname.style.display="None";
            td_DOB.style.display="None";
            td_MiddleName.style.display="none";
            td_txtMiddleName.style.display="none";
            tdBlank.style.display= "None";
            tdrbCauseNo.style.display= "block";
            tdtxtCauseNo.style.display= "block";
            tdrbTicktNo.style.display= "block";
            tdtxtTicketNo.style.display= "block";
            document.getElementById("rb_CauseNumber").checked = true;
            document.getElementById("txt_CauseNumber").focus();
        break;                               
        
        // Rab Nawaz Khan 11108 08/06/2013 Pearland Municipal court added. . . 
        case "84":
            trName.style.display = 'block';
            tdrbCrtDate.style.display = 'block';
            tdtxtCrtDate.style.display = 'block';
            td_DOBname.style.display = "none";
            td_DOB.style.display = "none";
            tdBlank.style.display = 'block';
            tdrbTicktNo.style.display = 'block';
            tdtxtTicketNo.style.display = 'block';
            tdrbCauseNo.style.display = 'block';
            tdtxtCauseNo.style.display = 'block';
            document.getElementById("rb_CauseNumber").checked = 'true';
        break;
            
        default:
            trName.style.display = 'block';
            tdrbCrtDate.style.display = 'block';
            tdtxtCrtDate.style.display = 'block';
            td_DOBname.style.display = 'block';
            td_DOB.style.display = 'block';
            break;       
    }
    // Noufil 5835 06/04/2009 Reset control
    //debugger;
    resettextbox(loaderId);
    
}

// Noufil 5695 04/06/2009 Make text box empty.
function resettextbox(loadervalue)
{
//debugger;
    //document.getElementById("rb_TicketNumber").checked=true;    
    document.getElementById("txt_TicketNumber").value="";
    document.getElementById("txt_CauseNumber").value="";    
    document.getElementById("txt_LastName").value="";   
    document.getElementById("txt_FirstName").value="";   
    document.getElementById("txt_MiddleName").value="";   
    document.getElementById("txt_DOB").value="";
    document.getElementById("txtFileDate").value="";        
    document.getElementById("txt_CrtDate").value="";        
    document.getElementById("ddlLoader").value=1;
    document.getElementById("lblMessage").innerText="";
    document.getElementById("ddlLoader").value=loadervalue;
}

function checkName(input) 
{
  var iChars = "123456789!@#$%^&*()+=[]\\\';,/{}|\":<>?";

  for (var i = 0; i < input.length; i++) {
  	if (iChars.indexOf(input.charAt(i)) != -1) {
  	//alert ("Your username has special characters. \nThese are not allowed.\n Please remove them and try again.");
  	return false;
  	}
  }
}
function checkNumber(input) 
{
  var iChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()+=[]\\\';,/{}|\":<>?.-";

  for (var i = 0; i < input.length; i++) {
  	if (iChars.indexOf(input.charAt(i)) != -1) {
  	//alert ("Your username has special characters. \nThese are not allowed.\n Please remove them and try again.");
  	return false;
  	}
  }
}
function SetActive(e,sec,txtval)
{
    if (e.keyCode > 31)
    {
        if (txtval != "") 
        {
            if (sec == 1) 
            {
                 document.getElementById("rb_CauseNumber").checked = 1;   
            }
            else if (sec ==2) 
            {
                document.getElementById("rb_LastName_DOB").checked = 1;
            }
            else if (sec == 3)
            {
                document.getElementById("rbFileDate").checked = 1;
            }
            else if (sec == 4) 
            {
                document.getElementById("rb_TicketNumber").checked = 1;
            }
            else if (sec == 5) 
            {
                document.getElementById("rb_CrtDate").checked = 1;
            }
            //Farrukh 10447 12/19/2012 Added New check for Crash ID and Case ID
            else if (sec == 6) {
                document.getElementById("rb_CrashId").checked = 1;
            }
            else if (sec == 7) {
                document.getElementById("rb_CaseId").checked = 1;
            }
            else 
            {
                document.getElementById("rb_TicketNumber").checked = 1;
            }
        }
    }
}

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="980" align="center"
            border="0">
            <tr>
                <td colspan="4">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td colspxan="4">
                    <table class="clsLeftPaddingTable" cellspacing="1" cellpadding="1" width="100%" align="center"
                        border="0">
                        <tr>
                            <td style="height: 11px; width: 100%" align="center" background="../Images/separator_repeat.gif"
                                colspan="8">
                            </td>
                        </tr>
                        <tr>
                            <td id="tdrbCauseNo" runat="server" width="100" class="clsLeftPaddingTable">
                                <asp:RadioButton ID="rb_CauseNumber" runat="server" CssClass="clsLeftPaddingTable"
                                    GroupName="group1" Text="Cause Number" />
                            </td>
                            <td id="tdtxtCauseNo" runat="server" width="100">
                                <asp:TextBox ID="txt_CauseNumber" runat="server" CssClass="clsInputadministration"
                                    Columns="25" MaxLength="25" onkeyup="SetActive(event,1,this.value)"></asp:TextBox>
                            </td>
                            <td id="tdrbTicktNo" runat="server" width="100" class="clsLeftPaddingTable">
                                <asp:RadioButton ID="rb_TicketNumber" runat="server" CssClass="clsLeftPaddingTable"
                                    GroupName="group1" Checked="True" Text="Ticket Number" />
                            </td>
                            <td id="tdtxtTicketNo" runat="server" width="100">
                                <asp:TextBox ID="txt_TicketNumber" runat="server" CssClass="clsInputadministration"
                                    Columns="25" MaxLength="25" onkeyup="SetActive(event,4,this.value)"></asp:TextBox>
                            </td>
                            <td id="tdrbCrashId" runat="server" style="display:none;" width="100" class="clsLeftPaddingTable">
                                <asp:RadioButton ID="rb_CrashId" runat="server" CssClass="clsLeftPaddingTable"
                                    GroupName="group1" Checked="True" Text="Crash ID" />
                            </td>
                            <td id="tdtxtCrashId" runat="server" width="100" style="display:none;">
                                <asp:TextBox ID="txt_CrashId" runat="server" CssClass="clsInputadministration"
                                    Columns="25" MaxLength="25" onkeyup="SetActive(event,6,this.value)"></asp:TextBox>
                            </td>
                            <td id="tdrbCaseId" runat="server" width="100" class="clsLeftPaddingTable" style="display:none;">
                                <asp:RadioButton ID="rb_CaseId" runat="server" CssClass="clsLeftPaddingTable"
                                    GroupName="group1" Checked="True" Text="Case ID" />
                            </td>
                            <td id="tdtxtCaseId" runat="server" width="100" style="display:none;">
                                <asp:TextBox ID="txt_CaseId" runat="server" CssClass="clsInputadministration"
                                    Columns="25" MaxLength="25" onkeyup="SetActive(event,7,this.value)"></asp:TextBox>
                            </td>
                            
                            <td id="tdBlank" runat="server" colspan="4">
                            </td>
                        </tr>
                        <tr id="trName" runat="server">
                            <td class="clsLeftPaddingTable">
                                <asp:RadioButton ID="rb_LastName_DOB" runat="server" CssClass="clsLeftPaddingTable"
                                    GroupName="group1" Text="Last Name " />
                            </td>
                            <td>
                                <asp:TextBox ID="txt_LastName" runat="server" CssClass="clsInputadministration" Columns="25"
                                    MaxLength="25" onkeyup="SetActive(event,2,this.value)"></asp:TextBox>
                            </td>
                            <td class="clsLeftPaddingTable">
                                &nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lbl_FirstName" runat="server" Text="First Name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_FirstName" runat="server" CssClass="clsInputadministration"
                                    Columns="25" MaxLength="25" onkeyup="SetActive(event,2,this.value)"></asp:TextBox>
                            </td>
                            <td id="td_MiddleName" runat="server" class="clsLeftPaddingTable">
                                M
                            </td>
                            <td id="td_txtMiddleName" runat="server">
                                <asp:TextBox ID="txt_MiddleName" runat="server" CssClass="clsInputadministration"
                                    MaxLength="25" Width="110px" Columns="1" onkeyup="SetActive(event,2,this.value)"></asp:TextBox>
                            </td>
                            <td id="td_DOBname" runat="server" class="clsLeftPaddingTable">
                                DOB (MM/DD/YYYY)
                            </td>
                            <td id="td_DOB" runat="server">
                                <asp:TextBox ID="txt_DOB" runat="server" CssClass="clsInputadministration" MaxLength="10"
                                    onkeyup="SetActive(event,2,this.value)"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable">
                                <asp:RadioButton ID="rbFileDate" runat="server" CssClass="clsLeftPaddingTable" GroupName="group1"
                                    Text="File Date" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtFileDate" runat="server" CssClass="clsinputadministration" MaxLength="10"
                                    onkeyup="SetActive(event,3,this.value)"></asp:TextBox>
                            </td>
                            <td class="clsLeftPaddingTable">
                                <span id="tdrbCrtDate" runat="server">
                                    <asp:RadioButton ID="rb_CrtDate" runat="server" CssClass="clsLeftPaddingTable" GroupName="group1"
                                        Text="Crt Date" /></span>
                            </td>
                            <td id="tdtxtCrtDate">
                                <asp:TextBox ID="txt_CrtDate" runat="server" CssClass="clsInputadministration" MaxLength="10"
                                    onkeyup="SetActive(event,5,this.value)"></asp:TextBox>
                            </td>
                            <td class="clsLeftPaddingTable">
                                Loader
                            </td>
                            <td>
                                <asp:DropDownList onchange="OnLoaderChange();" ID="ddlLoader" runat="server" CssClass="clsinputcombo"
                                    Width="110px">
                                </asp:DropDownList>
                            </td>
                            <td width="100" align="right">
                                <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="clsbutton" OnClientClick="javascript:return ValidateInput()"
                                    OnClick="btn_Submit_Click" />
                            </td>
                            <td width="100" align="left">
                                <asp:Button ID="btn_Reset" runat="server" Text="Reset" CssClass="clsbutton" OnClientClick="javascript:return ResetControls()" />
                            </td>
                        </tr>
                    </table>
                    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                        <tr>
                            <td style="height: 11px" align="center" background="../Images/separator_repeat.gif"
                                colspan="4">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px"
                                valign="middle">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <div id="divPgDropDown">
                                                <uc2:PagingControl ID="Pagingctrl" runat="server" />
                                            </div>
                                        </td>
                                        <td align="right">
                                            <div id="divExport">
                                                <asp:LinkButton ID="btnExport" runat="server" OnClick="btnExport_Click" Visible="false"
                                                    OnClientClick="javascript:return confirm('This process might take very long to complete, depending upon the number of records in the report. Are you sure you want to continue ?')">Export to Excel</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" background="../Images/separator_repeat.gif" style="width: 980px;
                                height: 11px;">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 980px;">
                                <asp:Label ID="lblMessage" runat="server" CssClass="clslabel" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div id="divgrid" runat="server" style="min-height: 300px; height: auto !important;
                        height: 600px; width: 980px; overflow: auto;">
                        <asp:GridView ID="gv_Details" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                            AllowPaging="true" PageSize="100" OnPageIndexChanging="gv_Details_PageIndexChanging"
                            HeaderStyle-CssClass="Clssubhead" RowStyle-Font-Bold="true">
                            <RowStyle CssClass="clsLeftPaddingTable" />
                            <PagerSettings Mode="NextPrevious" NextPageText="Next &amp;gt;" PreviousPageText="&amp;lt; Previous"
                                Visible="False" />
                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                        <br />
                    </div>
                    <div align="center" id="divPaging" runat="server" visible="false">
                        <asp:LinkButton ID="lbPrev" runat="server" OnClick="lbPrev_Click">&lt; Previous</asp:LinkButton>&nbsp;&nbsp;
                        <asp:LinkButton ID="lbNext" runat="server" OnClick="lbNext_Click">Next &gt;</asp:LinkButton>
                    </div>
                    <table id="Table3" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 760px" align="left" colspan="4">
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

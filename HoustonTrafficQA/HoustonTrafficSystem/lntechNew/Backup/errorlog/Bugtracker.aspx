<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" Codebehind="Bugtracker.aspx.cs" AutoEventWireup="True" Inherits="HTP.Errorlog.Bugtracker" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Bugtracker</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT SRC="BoxOver.js"></SCRIPT>
		<SCRIPT SRC="../Scripts/ClipBoard.js"></SCRIPT>
		<script language="JavaScript">
		
		/* Commented by Farhan Sabir
		
		var refreshinterval=60
		var displaycountdown="yes"
		var starttime
		var nowtime
		var reloadseconds=0
		var secondssinceloaded=0

		function starttime() {
			starttime=new Date()
			starttime=starttime.getTime()
			countdown()
		}

		function countdown() {
			nowtime= new Date()
			nowtime=nowtime.getTime()
			secondssinceloaded=(nowtime-starttime)/1000
			reloadseconds=Math.round(refreshinterval-secondssinceloaded)
			if (refreshinterval>=secondssinceloaded) {
				var timer=setTimeout("countdown()",1000)
				if (displaycountdown=="yes") {
					window.status="Page refreshing in "+reloadseconds+ " seconds"
				}
			}
			else {
				clearTimeout(timer)
				window.location.reload(true)
			} 
		}
		window.onload=starttime
		*/
		
		var err = null;
		
		function StateTracePoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg()
		     //document.getElementById("txtb_StateTraceMsg").value = err;
		     //document.getElementById("txtb_StateTraceMsg").style.visibility = 'visible';
		}
		
		
		function validate()
		{
			
			var d1 = document.getElementById("dtp_dtFrom").value;
			var d2 = document.getElementById("dtp_DtTo").value;		
			if (d1 == d2)
				return true;
			
				var diff = DateDiff(d2,d1);
				if(diff<0)
				{
					alert("Invalid date range! Please make sure that the 'From Date' is greater than 'To Date'");
					document.getElementById("dtp_DtTo").focus(); 
					return false;
				}
				
				else
					return true;
		}
		
		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
		
		//change cursor icon
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
		
		
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
            <table id="MainTable" cellSpacing="1" cellPadding="1" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <table id="Table1" align="center" border="1" cellpadding="1" cellspacing="1" width="780">
                            <tr>
                                <td style="width: 786px">
                                    <strong>&nbsp;&nbsp;
                                        <table id="Table2" border="0" cellpadding="1" cellspacing="1" width="780">
                                            <tr>
                                                <td style="width: 56px">
                                                    <strong>From:</strong></td>
                                                <td style="width: 139px">
                                                    <ew:CalendarPopup ID="dtp_dtFrom" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                                        Width="96px">
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </td>
                                                <td style="width: 31px">
                                                    To:</td>
                                                <td style="width: 127px">
                                                    <ew:CalendarPopup ID="dtp_DtTo" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                                        Width="96px">
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </td>
                                                <td>
                                                    Database: &nbsp;&nbsp;<asp:DropDownList ID="ddl_Options" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                        <asp:ListItem>Houston Traffic System</asp:ListItem>
                                                        <asp:ListItem>Dallas Traffic System</asp:ListItem>
                                                        <asp:ListItem>Public Site</asp:ListItem>
                                                        <asp:ListItem>QuickBooks</asp:ListItem>
                                                        <asp:ListItem>LoaderSservice</asp:ListItem>
                                                        <asp:ListItem>E-Signature</asp:ListItem>
                                                        <asp:ListItem>Outlook Addin</asp:ListItem>
                                                         <asp:ListItem>SulloLaw</asp:ListItem>
                                                         <asp:ListItem>POLM</asp:ListItem>
                                                         <asp:ListItem>Product Defects</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp; &nbsp;<asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClick="btn_Submit_Click"
                                                        Text="Submit" /></td>
                                            </tr>
                                        </table>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 786px">
                                    <table width="100%">
                                        <tr>
                                            <td width="70%">
                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                                            <td align="right">
                                                <asp:Label ID="lblCurrPage" runat="server" CssClass="cmdlinks" Font-Bold="True" Font-Size="Smaller"
                                                    ForeColor="#3366cc" Height="8px" Width="83px">Current Page :</asp:Label>
                                                <asp:Label ID="lblPNo" runat="server" CssClass="cmdlinks" Font-Bold="True" Font-Size="Smaller"
                                                    ForeColor="#3366cc" Height="10px" Width="9px">a</asp:Label>
                                                <asp:Label ID="lblGoto" runat="server" CssClass="cmdlinks" Font-Bold="True" Font-Size="Smaller"
                                                    ForeColor="#3366cc" Height="7px" Width="16px">Goto</asp:Label><asp:DropDownList ID="cmbPageNo"
                                                        runat="server" AutoPostBack="True" CssClass="clinputcombo" Font-Bold="True" Font-Size="Smaller"
                                                        ForeColor="#3366cc" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 786px">
                                    <asp:DataGrid ID="dg_bug" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        OnItemDataBound="dg_bug_ItemDataBound" PageSize="50" Width="780px">
                                        <AlternatingItemStyle BackColor="#EEEEEE" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Bug ID.">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_bugid" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"eventid") %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Date/Time">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_datetime" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Timestamp") %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Message">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_message" runat="server" CssClass="label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem,"message"))  %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Source">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_source" runat="server" CssClass="label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem, "source")) %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Target Site">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_targetsite" runat="server" CssClass="label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem,"targetsite")) %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Stack Trace">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
										<DIV TITLE="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='400px'><tr><td><textarea id='txt_StateTraceMsg' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
										<asp:Label id=lbl_statetrace runat="server" CssClass="label" onclick="copyToClipboard(document.getElementById('txt_StateTraceMsg').value);" >Trace</asp:Label>
                                        </DIV>
                                        <asp:HiddenField ID="hf_statetrace" runat="server" Value='<%# Server.HtmlEncode((string)DataBinder.Eval(Container, "DataItem.stacktrace")) %>' />
									</ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " />
                                    </asp:DataGrid></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td >
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
		</form>
	</body>
	<script language="javascript" >
		function ShowMsg()
        {
            document.getElementById("txt_StateTraceMsg").value=err;
        }
		</script>
</HTML>

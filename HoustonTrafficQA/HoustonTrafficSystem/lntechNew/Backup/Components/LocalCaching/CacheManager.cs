/*
 * LocalCaching library
 * 
*/

using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Collections;
//using SHI.WebTeam.SalesTools.SalesCenterPlugIns.PlugInCommon;

namespace LntechLocalCaching
{
	/// <summary>
	/// Summary description for CacheManager.
	/// </summary>
   public class CacheManager : ICacheService
   {
      private const      string    mCacheRelativeFolder = @"\LocalCaching\";
      private readonly   string    mCacheFullPath;
      //private            DSContent mContent;

      static private readonly object sIndexLock = new Object();

      /// <summary>
      /// If you don't specify a location for the cache, we'll find the current user's
      /// Local Application Data folder under My Documents, and put the cache into 
      /// a sub-folder there
      /// </summary>
      public CacheManager()
      {
         mCacheFullPath = System.Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ) + mCacheRelativeFolder; 
         //mContent = GetContentDataset();
      }

      /// <summary>
      /// If you don't want the cache to go into the user's space,
      /// you can specify an exact path for it.
      /// </summary>
      /// <param name="ContainerPath"></param>
      public CacheManager( String ContainerPath )
      {
          
          //Kazim 2665 1/30/2008 Correct the wrong path setting at instantiation of CacheManager class 
 
          mCacheFullPath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + mCacheRelativeFolder + ContainerPath + @"\"; 
      }

      //=====================================================================
      //=====================================================================


      public void clearCache()
      {
         lock( sIndexLock )
         {
            try
            {
               System.IO.DirectoryInfo di = new System.IO.DirectoryInfo( mCacheFullPath );
               if (di.Exists)
               {
                  di.Delete( true );
               }
            }
            catch
            {
               //
            }

         }
      }
   
      public void clearCache(DateTime minbirth)
      {
         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();

            DeleteMeetingCriteria( myContent, "Timestamp < '" + minbirth.ToLocalTime() + "'" );
            PersistContent(myContent);
         }
      }
   
      public void clearCache(string name)
      {
         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();

            DeleteMeetingCriteria( myContent, "Name = '" + name + "'" );
            PersistContent(myContent);
         }
      }
   
      public void clearCache(string name, string GoodVersion)
      {
         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();

            DeleteMeetingCriteria( myContent, "(Name = '" + name + "') AND (Version <> '" + GoodVersion + "')" );
            PersistContent(myContent);
         }
      }

      public void unStoreCache( string name, string paramhash )
      {
         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();
            DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );

            if (myRow != null)
            {
               PurgeFile( myRow.FileName );
               myRow.Delete();
               PersistContent(myContent);
            }
         }
      }

   
      //=====================================================================
      //=====================================================================


      public DateAndVersion getDateAndVersion(string name, string paramhash)
      {
         DateAndVersion DaV = null;

         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();
            DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );

            if (myRow != null)
            {
               DaV = new DateAndVersion( myRow.Timestamp, myRow.Version );

               //TODO: should we be counting hits at this level, or when the data's retrieved?
               myRow.HitCount += 1;
               PersistContent(myContent);
            }
         }
         return DaV;
      }
   
      public System.Data.DataSet retrieveDataset(string name, string paramhash, System.Type MyType )
      {
         DSContent myContent = GetContentDataset();

         ConstructorInfo ci = MyType.GetConstructor( System.Type.EmptyTypes );
         System.Data.DataSet mydata = (System.Data.DataSet)(ci.Invoke( null ));

         DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );

         if (myRow != null)
         {
            if (mydata == null)
            {
               mydata = new System.Data.DataSet();
            }
            mydata.ReadXml( myRow.FileName );
         }
         return mydata;
      }

	   public System.Data.DataView  retrieveDataView(string name, string paramhash, System.Type MyType )
	   {
		   MyType = typeof(System.Data.DataSet);
		   DSContent myContent = GetContentDataset();

		   ConstructorInfo ci = MyType.GetConstructor( System.Type.EmptyTypes );
		   System.Data.DataSet mydata = ((System.Data.DataSet)(ci.Invoke( null )));

		   DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );

		   if (myRow != null)
		   {
			   if (mydata == null)
			   {
				   mydata = new System.Data.DataSet();
			   }
			   mydata.ReadXml( myRow.FileName );
		   }
		   return mydata.Tables[0].DefaultView ;
	   }


      public System.IO.FileStream retrieveStream(string name, string paramhash)
      {
         DSContent myContent = GetContentDataset();

         System.IO.FileStream myFS = null;
         DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );

         if (myRow != null)
         {
            myFS = new System.IO.FileStream( myRow.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read );
         }

         return myFS;
      }
   
      public void storeDataset(string name, string paramhash, DateAndVersion freshness, System.Data.DataSet mydata)
      {
         string fname;
         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();

            DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );
            int hc = 0;

            if (myRow != null)
            {
               hc = myRow.HitCount;
               PurgeFile( myRow.FileName );
               myRow.Delete();
            }

            fname = GenerateFileName( name );
            myRow = myContent.Content.AddContentRow( name, paramhash, freshness.Timestamp, freshness.Version, fname, hc );
            PersistContent(myContent);
         }
         mydata.WriteXml( fname, System.Data.XmlWriteMode.WriteSchema );
      }
   

	   public void storeDataView(string name, string paramhash, DateAndVersion freshness, System.Data.DataView  mydata)
	   {
		   string fname;
		   System.Data.DataSet ds = new System.Data.DataSet() ;
		   lock( sIndexLock )
		   {
			   DSContent myContent = GetContentDataset();

			   DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );
			   int hc = 0;

			   if (myRow != null)
			   {
				   hc = myRow.HitCount;
				   PurgeFile( myRow.FileName );
				   myRow.Delete();
			   }

			   fname = GenerateFileName( name );
			   myRow = myContent.Content.AddContentRow( name, paramhash, freshness.Timestamp, freshness.Version, fname, hc );
			   PersistContent(myContent);
		   }
		   ds.Tables.Add(mydata.Table.Copy());
		   ds.WriteXml(fname, System.Data.XmlWriteMode.WriteSchema );
	   }

	   
	   public System.IO.FileStream getStoreStream(string name, string paramhash, DateAndVersion freshness)
      {
         System.IO.FileStream myFS = null;
         lock( sIndexLock )
         {
            DSContent myContent = GetContentDataset();

            DSContent.ContentRow myRow = FindContentRecord( myContent, name, paramhash );
            string fname;
            int hc = 0;

            if (myRow != null)
            {
               hc = myRow.HitCount;
               PurgeFile( myRow.FileName );
               myRow.Delete();
            }

            fname = GenerateFileName( name );
            myFS = new System.IO.FileStream( 
               fname, 
               System.IO.FileMode.CreateNew, System.IO.FileAccess.Write, System.IO.FileShare.None 
               );

            myRow = myContent.Content.AddContentRow( name, paramhash, freshness.Timestamp, freshness.Version, fname, hc );
            PersistContent(myContent);
         }
         return myFS;
      }

      public System.Data.DataSet RetrieveCacheIndex()
      {
         return GetContentDataset();
      }

      public System.Collections.Specialized.StringCollection ListParams( string name )
      {
         System.Collections.Specialized.StringCollection par = new System.Collections.Specialized.StringCollection();

         DSContent myContent = GetContentDataset();

         System.Data.DataView dv = new System.Data.DataView();
         dv.Table = myContent.Content;
         dv.RowFilter = "Name = '" + name + "'";

         for (int i = 0; i < dv.Count; i++)
         {
            par.Add( ((DSContent.ContentRow)(dv[i].Row)).ParamHash );
         }
         dv.Dispose();

         return par;

      }

      //=====================================================================
      //=====================================================================

      private DSContent GetContentDataset()
      {
         DSContent myContent = new DSContent();
         string myFilename = mCacheFullPath + @"ContentIndex.xml";

         try
         {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo( mCacheFullPath );
            if (!di.Exists)
            {
               di.Create();
            }
            di = null;

            System.IO.FileInfo fi = new System.IO.FileInfo( myFilename );
            
            if ( fi.Exists )
            {
               myContent.ReadXml( myFilename );
            }
            else
            {
               // do we need to pre-create the directory tree?
               myContent.WriteXml( myFilename );
            }
            fi = null;
         }
         catch
         {
            //
         }
 
         return myContent;
      }

      private string GenerateFileName( string Prefix )
      {
         return mCacheFullPath + Prefix + System.Guid.NewGuid().ToString() + ".cache";
      }

      private DSContent.ContentRow FindContentRecord(DSContent myContent, string name, string paramhash)
      {
         DSContent.ContentRow myRow = null;

         System.Data.DataView dvFilter = new System.Data.DataView( myContent.Content, 
            String.Format( "Name = '{0}' AND ParamHash = '{1}'", name, paramhash ),
            null, System.Data.DataViewRowState.CurrentRows );
         if ( dvFilter.Count == 0 )
         {
            // return the null row because nothing was found
         }
         else
         {
            Debug.Assert( dvFilter.Count == 1, "Duplicate cache entries found", 
               String.Format( "Expected 1 matching cache entry for [{0}]:[{1}] but found {2}.", name, paramhash, dvFilter.Count ) );

            myRow = (DSContent.ContentRow)(dvFilter[0].Row);
         }

         dvFilter.Dispose();
         return myRow;
      }

      private void PurgeFile( string fname )
      {
         try
         {
            FileInfo fi = new FileInfo( fname );
            {
               if (fi.Exists)
               {
                  fi.Delete();
               }
            }
         }
         catch
         {
            //
         }
      }

      private void DeleteMeetingCriteria(DSContent myContent, string Criteria)
      {
         System.Data.DataView dv = new System.Data.DataView();
         dv.Table = myContent.Content;
         dv.RowFilter = Criteria;

         for (int i = dv.Count-1; i >= 0; i--)
         {
            string fname = ((DSContent.ContentRow)(dv[i].Row)).FileName;
            PurgeFile( fname );
            dv[i].Row.Delete();
         }
         dv.Dispose();
      }
   


      private void PersistContent(DSContent myContent)
      {
         try
         {
            string myFilename = mCacheFullPath + "ContentIndex.xml";
            myContent.AcceptChanges();
            myContent.WriteXml( myFilename );
         }
         finally
         {
            //
         }
      }


   }
}

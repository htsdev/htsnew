using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components
{
    public class Discrepancy
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();

        #region Variables

        private int ocrID = 0;
        private int picID = 0;
        private string firstname = String.Empty;
        private string lastname = String.Empty;
        private string address = String.Empty;
        private string city = String.Empty;
        private string state = String.Empty;
        private int zip = 0;
        private string causeno = String.Empty;
        private string status = String.Empty;
        private string newcourtdate = String.Empty;
        private string todaydate = String.Empty;
        private string time;
        private int courtno = 0;
        private string location = String.Empty;
        private DateTime updatedatescan;
        private DateTime courtdatescan;
        private DateTime _CourtDate;
        private DateTime _CourtDateMain;
        private int _checkStatus = 0;
        #endregion
        #region Properties

        public int OCRID
        {
            get
            {
                return ocrID;
            }
            set
            {
                ocrID = value;
            }
        }
        public int PicID
        {
            get
            {
                return picID;
            }
            set
            {
                picID = value;
            }
        }
        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public int Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }
        public string CauseNo
        {
            get
            {
                return causeno;
            }
            set
            {
                causeno = value;
            }
        }
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public string NewCourtDate
        {
            get
            {
                return newcourtdate;
            }
            set
            {
                newcourtdate = value;
            }
        }
        public string TodayDate
        {
            get
            {
                return todaydate;
            }
            set
            {
                todaydate = value;
            }
        }
        public int CourtNo
        {
            get
            {
                return courtno;
            }
            set
            {
                courtno = value;
            }
        }
        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }
        public string Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }
        public DateTime ScanUpdateDate
        {
            get
            {
                return updatedatescan;
            }
            set
            {
                updatedatescan = value;
            }
        }
        public DateTime CourtDateScan
        {
            get
            {
                return courtdatescan;
            }
            set
            {
                courtdatescan = value;
            }
        }
        public DateTime CourtDate
        {
            get
            {
                return _CourtDate;
            }
            set
            {
                _CourtDate = value;
            }
        }
        public DateTime CourtDateMain
        {
            get
            {
                return _CourtDateMain;
            }
            set
            {
                _CourtDateMain = value;
            }
        }
        public int CheckStatus
        {
            get
            {
                return _checkStatus;
            }
            set
            {
                _checkStatus = value;
            }
        }
        #endregion
        #region Methods
        public void InsertPendingOCR()
        {
            try
            {
                string[] key ={ "@CauseNo", "@CourtDate", "@CourtTime", "@CourtNo", "@Status", "" };
                object[] value1 ={ PicID, CheckStatus };
                clsDb.InsertBySPArr("usp_WebScan_InsertPending", key, value1);
            }
            catch (Exception ex)
            {
            }
        }
        public int ComparisonInsert()
        {
            int i = 0;
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@CourtDate", "@CourtTime", "@CourtNo", "@Status", "@Location", "@CourtDateScan", "@Msg" };
                object[] value1 ={ PicID, CauseNo, CourtDateMain, Time, CourtNo, Status, Location ,CourtDateScan,0};
                i=(int)clsDb.InsertBySPArrRet("usp_webscan_ComparisonWithDiscrepancy", key, value1);
                return i;
            }
            catch (Exception ex)
            {
                return i;
            }
        }
        #endregion

    }
}

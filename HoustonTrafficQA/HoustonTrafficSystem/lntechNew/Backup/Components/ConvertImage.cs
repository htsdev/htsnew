
using System;
//using System.Web.Services;
//using System.Xml.Serialization;
using System.Drawing; // for Image class
using System.Drawing.Imaging; // for ImageFormat class
using System.IO; // for FileStream class

namespace lntechNew.Components
{
	/// <summary>
	/// Summary description for ImageConvert.
	/// </summary>
	public class ConvertImage
	{
		public ConvertImage()
		{
			//
			// TODO: Add constructor logic here
			//
		}
			
			
			public bool ImageConvert(byte[] bytInFile, int intToFormat, 
				out byte[] bytOutFile, string filename)
			{
				// has something been sent or not...
				bytOutFile=null;
				if (bytInFile.Length==0)
				{
					// nope.. indicate failure
					return false;
				}

				// Since webservices are stateless, and each webmethod call is
				// indepedent of another, we must have a unique file name for
				// processing each request.
				
				//string strFileName=Server.MapPath(".")+"\\"+ Guid.NewGuid().ToString();
				string strFileName = filename;

				// write the byte array sent to us as a file..
				FileStream fsFile=null;
				try
				{
					fsFile=File.Create(strFileName);
				}
				catch
				{
					// unable to create input file..
					return false;
				}

				// write the byte array to it..
				try
				{
					fsFile.Write(bytInFile,0,bytInFile.Length);
				}
				catch
				{
					// unable to write to the file..
					fsFile.Close();
					return false;
				}

				// close the file..
				fsFile.Close();

				// load the image from the file..
				Image imgInFile=Image.FromFile(strFileName);

				// save to the format specified..
				string strOutFileName=strFileName;

				switch(intToFormat)
				{
					case 1: // BMP
						strOutFileName=strOutFileName+".BMP";
						imgInFile.Save(strOutFileName,ImageFormat.Bmp); 
						break;
					case 2: // EXIF
						strOutFileName=strOutFileName+".EXIF";
						imgInFile.Save(strOutFileName,ImageFormat.Exif); 
						break;
					case 3: // EMF
						strOutFileName=strOutFileName+".EMF";
						imgInFile.Save(strOutFileName,ImageFormat.Emf); 
						break;
					case 4: // GIF
						strOutFileName=strOutFileName+".GIF";
						imgInFile.Save(strOutFileName,ImageFormat.Gif); 
						break;
					case 5: // ICO
						strOutFileName=strOutFileName+".ICO";
						imgInFile.Save(strOutFileName,ImageFormat.Icon); 
						break;
					case 6: // JPEG
						strOutFileName=strOutFileName+".JPG";
						imgInFile.Save(strOutFileName,ImageFormat.Jpeg); 
						break;
					case 7: // PNG
						strOutFileName=strOutFileName+".PNG";
						imgInFile.Save(strOutFileName,ImageFormat.Png); 
						break;
					case 8: // TIFF
						strOutFileName=strOutFileName+".TIFF";
						imgInFile.Save(strOutFileName,ImageFormat.Tiff); 
						break;
					case 9: // WMF
						strOutFileName=strOutFileName+".WMF";
						imgInFile.Save(strOutFileName,ImageFormat.Wmf); 
						break;
					default:
						strOutFileName=strOutFileName+".BMP";
						imgInFile.Save(strOutFileName,ImageFormat.Bmp); 
						break;
				}

				// read the output file..
				try
				{
					fsFile=File.Open(strOutFileName,FileMode.Open,FileAccess.Read);
				}
				catch
				{
					// unable to read output file..
					return false;
				}

				// write to the output byte array..
				try
				{
					// create array to read in image file..
					int iSize=Convert.ToInt32(fsFile.Length);
					bytOutFile = new byte[iSize];

					// read the converted image...
					fsFile.Read(bytOutFile,0,iSize);
				}
				catch
				{
					// unable to write to the array..
					fsFile.Close();
					return false;
				}

				// close the file..
				fsFile.Close();

				// delete the created files..
				try
				{
					File.Delete(strFileName);
					File.Delete(strOutFileName);
				}
				catch
				{
					// do nothing..
				}

				return true;
			}
		

	}
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace HTP.Components.Entities
{

   

    public class County
    {
        
        
        #region Variables
        private int countyID = 0;
        private string countyName = String.Empty;
        private int isActive = 0;
        private int sortOrder = 0;
        #endregion

        #region Properties

        public int CountyID
        {
            get { return countyID; }
            set { countyID = value; }
        }

        public string CountyName
        {
            get { return countyName; }
            set { countyName = value; }
        }

        public int IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public int SortOrder
        {
            get { return sortOrder; }
            set { sortOrder = value; }
        }

        #endregion
    }
    
    

}

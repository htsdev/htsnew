﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace HTP.Components.Entities
{
    public class AttorneyTrials
    {
        #region variables
        private Int64 rowId;
        private DateTime date;
        private int attorney;
        private string prosecutor;
        private string officer;
        private string defendant;
        private string caseNo;
        private string ticketNo;
        private int courtId;
        private string judge;
        private string briefFacts;
        private int verdictId;
        private string verdict;
        private Double fine;
        private string courtReport;

        #endregion


        #region properties
        public Int64 RowId
        {
            get { return rowId; }
            set { rowId = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public int Attorney
        {
            get { return attorney; }
            set { attorney = value; }
        }

        public string Prosecutor
        {
            get { return prosecutor; }
            set { prosecutor = value; }
        }

        public string Officer
        {
            get { return officer; }
            set { officer = value; }
        }

        public string Defendant
        {
            get { return defendant; }
            set { defendant = value; }
        }

        public string CaseNo
        {
            get { return caseNo; }
            set { caseNo = value; }
        }

        public string TicketNo
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }

        public int CourtId
        {
            get { return courtId; }
            set { courtId = value; }
        }

        public string Judge
        {
            get { return judge; }
            set { judge = value; }
        }

        public string BriefFacts
        {
            get { return briefFacts; }
            set { briefFacts = value; }
        }

        public int VerdictId
        {
            get { return verdictId; }
            set { verdictId = value; }
        }

        public string Verdict
        {
            get { return verdict; }
            set { verdict = value; }
        }

        public Double Fine
        {
            get { return fine; }
            set { fine = value; }
        }

        public string CourtReport
        {
            get { return courtReport; }
            set { courtReport = value; }
        }

        #endregion
    }
}

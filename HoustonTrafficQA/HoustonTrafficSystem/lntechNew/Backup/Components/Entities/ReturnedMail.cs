﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace HTP.Components.Entities
{
    // Sabir Khan 6232 07/27/2009 Created for Upload Returned Mailers
    public class ReturnedMail
    {
        #region variables
        private int mailerId;
        private ReturnMailType mailType;
        private string mailFormat;
        #endregion

        public enum ReturnMailType
        {
            ReturnMailReguler = 0,
            ReturnMailIMB = 2,
            ReturnMailUSPS = 1, // Sabir Khan 6369 08/12/2009 set the correct value for USPS Service Return Mailer...
        }

        #region properties
        public int MailerID
        {
            get { return mailerId; }
            set { mailerId = value; }
        }
        public ReturnMailType MailType
        {
            get { return mailType; }
            set { mailType = value; }
        }
        public string MailFormat
        {
            get { return mailFormat; }
            set { mailFormat = value; }
        }
        #endregion
    }
}

﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components.Entities;
namespace HTP.Components.Services
{
    public class ReturnedMailService
    {
        #region Local variables...        
        clsENationWebComponents clsdb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        #endregion

        #region Method
        /// <summary>
        /// Sabir Khan 6232 07/28/2009
        /// This method is used to check the validity of uploaded file...        
        /// </summary>
        /// <param name="rtnMail">Object of Return Mail</param>
        /// <returns>Boolean value</returns>
        public bool ValidateFileFormat(ReturnedMail rtnMail)
        {
            bool isValid = true;
            int mailerId = 0;            
            if (rtnMail.MailType == ReturnedMail.ReturnMailType.ReturnMailIMB) //For Returned Mail - USPS IMB
            {
                string[] strarr = rtnMail.MailFormat.Split(',');
                if (strarr.Length != 5)
                {
                    isValid = false;
                }
                if (strarr.Length > 3)
                {
                    if (!(Int32.TryParse(strarr[3].Trim().ToString(), out mailerId)))
                    {
                        isValid = false;
                    }
                }
                else
                {
                    isValid = false;
                }

            }
            else if (rtnMail.MailType == ReturnedMail.ReturnMailType.ReturnMailReguler) //For Returned Mail - Regular
            {

                string[] strarr = rtnMail.MailFormat.Split(',');
                if ( strarr.Length != 1 || !(Int32.TryParse(strarr[0].Trim().ToString(), out mailerId)))
                {                    
                    isValid = false;
                }
            }            
            return isValid;
        }


        /// <summary>
        /// Sabir Khan 6232 07/28/2009
        /// This method is used to insert records into bad mailer list...
        /// </summary>
        /// <param name="rtnMail">Object of Return Mail</param>
        /// <returns>Boolean value</returns>
        public bool UpdateMailingList(ReturnedMail rtnMail)
        {
            try
            {
                //Sabir Khan 6371 08/13/2009 return boolean value true if records is inserted successfully otherwise return false.                
                string[] Keys = { "@MailerID" };
                object[] Values = { rtnMail.MailerID };
                return Convert.ToBoolean(clsdb.ExecuteScalerBySp("Usp_hts_Insert_tblbadmailers", Keys, Values));                
            }
            catch (Exception exx)
            {
                bugTracker.ErrorLog(exx.Message, exx.Source, exx.TargetSite.ToString(), exx.StackTrace);
                return false;
            }
            
            
        }


        /// <summary>
        /// Sabir Khan 6232 07/28/2009
        /// This method is used to update do not mail flag...
        /// </summary>
        /// <param name="rtnMail">Object of Return Mail</param>
        /// <returns>Integer value which determine the total number of updated records.</returns>
        public int UpdateDoNotMail(ReturnedMail rtnMail)
        {
            int UpdateCount = 0;
            string[] Keys1 = { "@UpdateSource" };
            object[] Values1 = { rtnMail.MailType };
            UpdateCount = Convert.ToInt32(clsdb.ExecuteScalerBySp("USP_HTP_Update_DonotMailFlag", Keys1, Values1));
            return UpdateCount;

        }
        #endregion

    }
}

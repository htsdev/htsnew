﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FrameWorkEnation.Components;

namespace HTP.Components.Services
{
    /// <summary>
    /// Attorney scheduler is used to manage attorney schedule
    /// </summary>
    public class AttorneySchedulerService
    {
        #region variables
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        #endregion

        #region Methods

        /// <summary>
        /// get nine weeks with week first day and week last day.		
        /// </summary>
        /// <returns>Datatable</returns>
        public DataTable GetShowSettingWeeks()
        {
            return ClsDb.Get_DS_BySP("USP_HTP_Get_ShowSetting_NeenWeeks").Tables[0];
        }

        /// <summary>
        /// get attorney schedule by given given date.
        /// </summary>
        /// <param name="StartDate">docket Start Date</param>
        /// <param name="EndDate">docket End Date</param>
        /// <returns>Datatable</returns>
        public DataTable GetAttorneysWeeklySettings(string StartDate, string EndDate)
        {
            string[] key = { "@StartDate", "@EndDate" };
            object[] values = { StartDate, EndDate };
            return ClsDb.Get_DT_BySPArr("USP_HTP_Get_Attorneys_WeeklySetting", key, values);

        }

        /// <summary>
        /// insert new attorney schedule by given parameter.
        /// </summary>
        /// <param name="DocketDate">trial date</param>
        /// <param name="AttorneyID">attorney identification</param>
        /// <param name="ContactNumber">attorney contact number</param>
        /// <param name="EmployeeID">Employee identification</param>
        /// <returns>bool</returns>
        public string InsertAttorneySchedule(string DocketDate, int AttorneyID, string ContactNumber, int EmployeeID)
        {
            string[] keys = { "@DocketDate", "@AttorneyID", "@ContactNumber", "@EmployeeID","@AtttorneyScheduleID" };
            object[] values = { DocketDate, AttorneyID, ContactNumber, EmployeeID,0 };
            return ClsDb.InsertBySPArrRet("USP_HTP_Insert_AttorneySchedule", keys, values).ToString();
        }

        /// <summary>
        /// pdate attorney schedule by given parameter.
        /// </summary>
        /// <param name="AttorneyScheduleID">Attorney schedule identification</param>
        /// <param name="DocketDate">trial date</param>
        /// <param name="AttorneyID">attorney identification</param>
        /// <param name="ContactNumber">attorney contact number</param>
        /// <param name="EmployeeID">Employee identification</param>
        /// <returns>bool</returns>
        public bool UpdateAttorneySchedule(int AttorneyScheduleID, string DocketDate, int AttorneyID, string ContactNumber, int EmployeeID)
        {
            string[] keys = { "@AttorneyScheduleID", "DocketDate", "@AttorneyID", "@ContactNumber", "@EmployeeID" };
            object[] values = { AttorneyScheduleID, DocketDate, AttorneyID, ContactNumber, EmployeeID };
            ClsDb.InsertBySPArr("USP_HTP_Update_AttorneySchedule", keys, values);
            return true;
        }

        /// <summary>
        /// delete attorney schedule by given parameter.
        /// </summary>
        /// <param name="AttorneyScheduleID">Attorney schedule identification</param>
        /// <returns>bool</returns>
        public bool DeleteAttorneySchedule(int AttorneyScheduleID)
        {
            string[] keys = { "@AttorneyScheduleID" };
            object[] values = { AttorneyScheduleID };
            ClsDb.InsertBySPArr("USP_HTP_Delete_AttorneySchedule", keys, values);
            return true;
        }

        #endregion
    }
}

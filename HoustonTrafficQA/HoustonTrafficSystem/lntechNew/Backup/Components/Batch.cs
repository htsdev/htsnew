using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components
{
    public class Batch
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();

        #region Variables

        private int batchID = 0;
        private string scanType = String.Empty;
        private DateTime scanDate;
        private int empid = 0;

        #endregion 

        #region Properties
        public int BatchID
        {
            get
            {
                return batchID;
            }
            set
            {
                batchID = value;
            }
        }
        public string ScanType
        {
            get
            {
                return scanType;
            }
            set
            {
                scanType = value;
            }
        }
        public DateTime ScanDate
        {
            get
            {
                return scanDate;
            }
            set
            {
                scanDate = value;
            }
        }
        public int EmpID
        {
            get 
            {
                return empid;
            }
            set
            {
                empid = value;
            }
        }
        #endregion 

        #region Methods

        public int InsertInBatch()
        {
            int batchid = 0;
            try
            {
                string[] key ={ "@ScanType", "@ScanDate", "@EmpID", "@BatchID" };
                object[] value1 ={ScanType,ScanDate,EmpID,0};
                batchid=(int)clsDb.InsertBySPArrRet("usp_WebScan_InsertInBatch",key,value1);
                return batchid;
            }
            catch (Exception ex)
            {
                return batchid;
            }
        }
        public DataSet GetBatch()
        {
            DataSet ds = null;
            try
            {
                ds = clsDb.Get_DS_BySP("usp_WebScan_GetAdminBatch");
                return ds;

            }
            catch (Exception ex)
            {
                return ds;
            }

        }
        public DataSet GetMoveFirst()
        {
            DataSet ds = null;
            try
            {
                
                //ds = clsDb.Get_DS_BySP("usp_WebScan_GetAdminDetails");
                ds = clsDb.Get_DS_BySP("usp_WebScan_GetAdminBatch");
                //
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        public DataSet GetCounts()
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@StarDate", "@EndDate", "@checkStatus" };
                object[] value ={ System.DateTime.Today.AddDays(-1),System.DateTime.Today,0};
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_NewGetSearch", key, value);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        public DataSet GetCountsII()
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@StarDate", "@EndDate", "@checkStatus" };
                object[] value ={ System.DateTime.Today.AddDays(-1), System.DateTime.Today, 0 };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_NewGetSearchII", key, value);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        //Edited By Fahad 2615 ( 1-29-08 )
        // Use Datatable insted of Dataset
        public DataTable GetBatchSearch()
        {
            DataTable dt = null;
            try
            {
                string[] key ={ "@StarDate", "@EndDate","@checkStatus" };
                object[] value ={ System.DateTime.Today.AddDays(-1), System.DateTime.Today,1};
                dt = clsDb.Get_DT_BySPArr("usp_WebScan_NewGetSearch", key, value);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
            }
        }
        //End Fahad

        public DataSet GetBatchSearchII()
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@StarDate", "@EndDate", "@checkStatus" };
                object[] value ={ System.DateTime.Today.AddDays(-1), System.DateTime.Today, 1 };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_NewGetSearchII", key, value);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }

        public DataSet GetBadCount(string batchid)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@BatchID" };
                object[] value1 ={Convert.ToInt32(batchid)};
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_GetBadRecords", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        public DataSet GetGoodCount(string batchid)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@BatchID" };
                object[] value1 ={ Convert.ToInt32(batchid) };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_GetGoodRecords", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }

        public DataSet GetScanType(string batchid)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@BatchID" };
                object[] value1 ={ Convert.ToInt32(batchid) };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_GetScanType", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }
        //
        public DataSet GetAllRecords(string batchid)
        {
            DataSet ds = null;
            try
            {
                string[] key ={ "@BatchID" };
                object[] value1 ={ Convert.ToInt32(batchid) };
                ds = clsDb.Get_DS_BySPArr("usp_WebScan_GetAdminBatch", key, value1);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }

        public DataSet GetStatus()
        {
            DataSet ds = null;
            try
            {

                ds = clsDb.Get_DS_BySP("usp_WebScan_GetStatus");
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
        }

        #endregion
    }
}

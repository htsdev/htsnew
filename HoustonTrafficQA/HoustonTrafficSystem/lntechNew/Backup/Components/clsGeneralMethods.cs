using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.SessionState;
using MSXML2;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text;
using FrameWorkEnation.Components;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using iTextSharp.text;
using iTextSharp.text.pdf;
using AxMODI;
using System.Web;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections.Specialized;
using WebSupergoo.ABCpdf6;
//Ozair 5771 04/23/2009
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;

namespace lntechNew.Components
{

    /// <summary>
    /// This class included the general methods used througout the application.
    /// </summary>
    public class clsGeneralMethods
    {

        /// <summary>
        /// Used for connecting with database
        /// </summary>
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");

        /// <summary>
        /// Represent Postal String return by the ZP4 service.
        /// </summary>
        public string YDSstrPostalString;

        /// <summary>
        ///  Represent Address  return by the ZP4 service.
        /// </summary>
        public string YDSstraddress = "";

        /// <summary>
        ///  Represent City  return by the ZP4 service.
        /// </summary>
        public string YDSstrcity = "";

        /// <summary>
        ///  Represent State return by the ZP4 service.
        /// </summary>
        public string YDSstrstate = "";

        /// <summary>
        ///  Represent Zip Code return by the ZP4 service.
        /// </summary>
        public string YDSstrzip = "";

        /// <summary>
        /// ZP4 Address Variable.
        /// </summary>
        public string YDSstrbarcode = "";

        /// <summary>
        /// ZP4 Address Variable.
        /// </summary>
        public string YDSstrdpv = "";

        /// <summary>
        /// ZP4 Address Variable.
        /// </summary>
        public string YDSstrcrrt = "";

        /// <summary>
        /// App Setting Reader used for getting information from the Web.Config file
        /// </summary>
        public AppSettingsReader reader = new AppSettingsReader();

        /// <summary>
        /// Used For Checking Transaction Type
        /// </summary>
        bool TransType;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public clsGeneralMethods()
        {
        }

        /// <summary>
        /// This function check whether the entered date is valid or not.
        /// </summary>
        /// <param name="strDate">Takes date parameter in string format.</param>
        /// <returns>Return true if date is successfully parsed.</returns>
        /// <example>
        /// <code>
        ///  if ( isDate("11/22/2007") )
        ///  {
        ///     //Do some work if date is valid
        ///  }
        /// </code>
        /// </example>
        public static bool isDate(string strDate)
        {
            try
            {
                DateTime.Parse(strDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This function get different parts ( i.e. Month / Day / Year Or Time ) from the date.
        /// </summary>
        /// <param name="parseType">String value which should be M (Month) , D (Day) , Y (Year) Or T (Time) </param>
        /// <param name="DateSet">The Date value from which selected date part should be extracted.</param>
        /// <returns>Return the M (Month) , D (Day) , Y (Year) Or T (Time) part from the date in string format</returns>
        /// <example> 
        /// <code>
        /// 
        /// DateTime dt = DateTime.Parse("11/22/2007");
        ///   
        /// //Getting Month from the date
        /// string Month = ParseDate("M",dt);
        ///  
        /// //Getting Year from the date  
        /// string Year = ParseDate("Y",dt);
        /// 
        /// //Getting Day of the week from the date
        /// string Day =  ParseDate("D",dt);
        /// 
        /// //Getting Time from the date
        /// string Time = ParseDate("T",dt); 
        /// </code>
        /// </example>
        public string ParseDate(string parseType, DateTime DateSet)
        {
            switch (parseType.Trim())
            {
                case "M":
                    {
                        return (string)Convert.ChangeType(DateSet.Month.ToString(), typeof(string));
                    }
                case "D":
                    {
                        return (string)Convert.ChangeType(DateSet.Day.ToString(), typeof(string));
                    }
                case "Y":
                    {
                        return (string)Convert.ChangeType(DateSet.Year.ToString(), typeof(string));
                    }
                case "T":
                    {
                        string dtTime = DateSet.TimeOfDay.Hours.ToString() + ":" + DateSet.TimeOfDay.Minutes.ToString();
                        return dtTime;
                    }
                default:
                    {
                        return (string)Convert.ChangeType(DateSet.Month.ToString(), typeof(string));
                    }
            }
        }

        /// <summary>
        /// No Description Available
        /// </summary>
        /// <param name="doc">Get doc id as string value</param>
        /// <returns>return boolean value</returns>
        public bool IS_OCR(string doc)
        {
            int docC;
            string Dc;
            string[] key = { "@DocType", "@Exist" };
            object[] value1 = { doc, 0 };
            //call sP and get the book ID back from sP
            Dc = ClsDb.InsertBySPArrRet("usp_scan_is_ocr", key, value1).ToString();
            docC = (int)Convert.ChangeType(Dc, typeof(int)); //

            if (docC > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// No Description Available
        /// </summary>
        /// <param name="filePath">Take file path as a string</param>
        /// <returns>Returns layout as a string</returns>
        public string OcrIt(string filePath)
        {
            //usp_scan_insert_tblscandata//stored procedure to add OCD data
            //

            MODI.Document miDoc = new MODI.Document();
            miDoc.Create(filePath);

            miDoc.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH, true, true);

            MODI.Image image = (MODI.Image)miDoc.Images[0];
            MODI.Layout layout = image.Layout;
            return layout.Text;
        }

        /// <summary>
        /// This Method is used to create a PDF file by providing the data through DataReader object.
        /// </summary>
        /// <param name="path">Path where the file should be save.</param>
        /// <param name="DOCNO">Document Number of the file.</param>
        /// <param name="sqlDR">DataReader object which contains data to be included in the file.</param>
        /// <param name="session">Session object of the current request</param>
        /// <param name="response">Response object of the current response</param>
        /// <returns>Return the file name of the generated pdf file.</returns>
        ///<example>
        /// <code>
        /// 
        /// string FilePath = "C://Test/";
        /// 
        /// int docNo = 222;
        /// IDataReader sqlDr;
        /// 
        /// string PDFFileName = "";
        /// 
        /// // Save the file in the temp folder included in the Test folder
        /// PDFFileName = AddImageToPDF(FilePath,docNo,IDataReader,this.Session;this.Response);
        /// 
        /// //Pdf file name should be like SessionID+docNo.pdf
        /// 
        /// </code>
        ///</example>

        public string AddImageToPDF(string path, int DOCNO, IDataReader sqlDR, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            string filepath = path + "/temp/" + session.SessionID.ToString() + DOCNO + ".pdf";
            Document documentx = new Document();
            try
            {
                PdfWriter.getInstance(documentx, new FileStream(filepath, FileMode.Create));
                documentx.Open();
                while (sqlDR.Read())
                {
                    byte[] content = (byte[])sqlDR.GetValue(1);
                    MemoryStream stream = new MemoryStream(content);
                    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(stream);
                    iTextSharp.text.Image jpeg = iTextSharp.text.Image.getInstance(bm, null, true);

                    jpeg.scalePercent(20, 21); //for 300 dpi
                    jpeg.scaleToFit(PageSize.A4.Width - 16, PageSize.A4.Height);
                    documentx.Add(jpeg);


                }
            }

            catch (DocumentException de)
            {
                response.Write(de.Message);
            }

            catch (IOException ioe)
            {
                response.Write(ioe.Message);
            }

            finally
            {
                documentx.Close();
                documentx = null;
            }
            //return filepath;
            return session.SessionID.ToString() + DOCNO + ".pdf";
        }

        /// <summary>
        ///  This Method is used to create a PDF file by combining multiple jpg files provided by the DataReader object.
        /// </summary>
        /// <param name="path">Path where the file should be save.</param>
        /// <param name="DOCNO">Document Number of the file.</param>
        /// <param name="sqlDR">DataReader object which contains data to be included in the file.</param>
        /// <param name="session">Session object of the current request</param>
        /// <param name="response">Response object of the current response</param>
        /// <param name="FileGetfromPhysicalMedia">Should be null</param>
        /// <param name="server">Server of the current request</param>
        /// <returns>Return the file name of the generated pdf file</returns>
        ///<example>
        /// <code>
        /// 
        /// string FilePath = "C://Test/";
        /// 
        /// int docNo = 222;
        /// IDataReader sqlDr;
        /// 
        /// string PDFFileName = "";
        /// 
        /// // Save the file in the Test folder
        /// PDFFileName = AddImageToPDF(FilePath,docNo,IDataReader,this.Session;this.Response);
        /// 
        /// //Pdf file name should be like SessionID+docNo.pdf
        /// 
        /// </code>
        ///</example>
        public string AddImageToPDF(string path, int DOCNO, IDataReader sqlDR, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response, bool FileGetfromPhysicalMedia, System.Web.HttpServerUtility server)
        {
            Document documentx = new Document();
            try
            {
                PdfWriter.getInstance(documentx, new FileStream(path, FileMode.Create));
                documentx.Open();
                while (sqlDR.Read())
                {
                    string fn;
                    fn = sqlDR.GetValue(1).ToString() + "-" + sqlDR.GetValue(0).ToString() + ".jpg";
                    fn = server.MapPath("Images/") + fn;
                    iTextSharp.text.Image pic = iTextSharp.text.Image.getInstance(fn);
                    pic.scaleToFit(PageSize.A4.Width - 16, PageSize.A4.Height);
                    documentx.Add(pic);
                }
            }
            catch (DocumentException de)
            {
                response.Write(de.Message);
            }
            catch (IOException ioe)
            {
                response.Write(ioe.Message);
            }
            finally
            {
                documentx.Close();
                documentx = null;
            }

            return session.SessionID.ToString() + DOCNO + ".pdf";
        }

        /// <summary>
        /// Convert String to Byte Array
        /// </summary>
        /// <param name="stringToConvert">String variable to be converted to byte array</param>
        /// <returns>Returns unicode byte array</returns>
        ///<example>
        /// <code>
        /// 
        /// //Sample String
        /// string TestValue = "Test";
        /// 
        /// //Convert TestValue into byte array
        /// byte[] ByteValue = ConvertStringToByteArray(TestValue);
        /// </code>
        ///</example>
        public static byte[] ConvertStringToByteArray(string stringToConvert)
        {

            return (new UnicodeEncoding()).GetBytes(stringToConvert);

        }

        /// <summary>
        /// This Method is used to get the Harris County Criminal Site Case SPN Search Page.
        /// </summary>
        /// <param name="logonid">Login id used for logging on the court website.</param>
        /// <param name="password">Password used for logging on the website.</param>
        /// <param name="spn">SPN value of the case.</param>
        /// <returns>Return SPN search page as a string.</returns>
        /// <remarks>
        /// This Method gets response from the following URL : http://apps.jims.hctx.net/subcrim 
        /// </remarks>
        public string GetSPNSearchPage(string logonid, string password, string spn)
        {
            try
            {
                XMLHTTP ObjHttp = new XMLHTTP();
                //string url = "http://subcrim.jims.hctx.net/subcrim/subcrim_logon.do?logonid=";
                string url = "http://apps.jims.hctx.net/subcrim/subcrim_logon.do?logonid=";
                url += logonid + "&password=" + password + "&newpassword=&newpasswordverify=&submit=logon";
                //Connecting to site
                ObjHttp.open("POST", url, false, null, null);
                ObjHttp.send(null);

                // Added by Zeeshan Ahmed To Login User To SPN Site
                // Get Session ID From Cookie 
                if (ObjHttp.getResponseHeader("Set-Cookie") != "")
                {
                    string sessionidspn = ObjHttp.getResponseHeader("Set-Cookie");
                    sessionidspn = sessionidspn.Substring(sessionidspn.IndexOf("=") + 1, sessionidspn.IndexOf(";") - 11);
                    // Go To Login Page
                    ObjHttp.open("post", "http://apps.jims.hctx.net/subcrim/subcrim_logon.do;jsessionid=" + sessionidspn, false, null, null);
                    // Set Request Header
                    setRequestHeader(ObjHttp);
                    ObjHttp.setRequestHeader("Cookie", "JSESSIONID=" + sessionidspn);
                    ObjHttp.setRequestHeader("Referer", "http://apps.jims.hctx.net/subcrim/Logon.jsp");
                    ObjHttp.send("logonid=" + logonid + "&password=" + password + "&newpasswordverify=&newpassword=");
                }

                //url = "http://subcrim.jims.hctx.net/subcrim/SpnInquiry.do?spn="+spn+"&submit=submit";
                url = "http://apps.jims.hctx.net/subcrim/SpnInquiry.do?spn=" + spn + "&submit=submit";
                //Connecting to site
                ObjHttp.open("post", url, false, null, null);
                //Sending parameters
                ObjHttp.send(null);

                if (ObjHttp.status.ToString() == "200")
                {
                    return ObjHttp.responseText;
                }
                else
                {
                    return "error";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// This Method is used to get the Harris County Criminal Site Case No Search Page.
        /// </summary>
        /// <param name="logonid">Login id used for logging on the court website.</param>
        /// <param name="password">Password used for logging on the website.</param>
        /// <param name="casenumber">Case Number used for searchig on the website.</param>
        /// <param name="cdi">Court division identification of the case.</param>
        /// <returns>Return case number search page as a string.</returns>
        /// <remarks>
        /// This Method gets response from the following URL : http://apps.jims.hctx.net/subcrim
        /// </remarks>
        public string GetCaseNoSearchPage(string logonid, string password, string casenumber, string cdi)
        {
            try
            {
                XMLHTTP ObjHttp = new XMLHTTP();
                //string url = "http://subcrim.jims.hctx.net/subcrim/subcrim_logon.do?logonid=";
                string url = "http://apps.jims.hctx.net/subcrim/subcrim_logon.do?logonid=";
                url += logonid + "&password=" + password + "&newpassword=&newpasswordverify=&submit=logon";
                //Connecting to site
                ObjHttp.open("POST", url, false, null, null);
                ObjHttp.send(null);

                // Added by Zeeshan Ahmed To Login User To SPN Site
                // Get Session ID From Cookie 
                if (ObjHttp.getResponseHeader("Set-Cookie") != "")
                {
                    string sessionidspn = ObjHttp.getResponseHeader("Set-Cookie");
                    sessionidspn = sessionidspn.Substring(sessionidspn.IndexOf("=") + 1, sessionidspn.IndexOf(";") - 11);
                    // Go To Login Page
                    ObjHttp.open("post", "http://apps.jims.hctx.net/subcrim/subcrim_logon.do;jsessionid=" + sessionidspn, false, null, null);
                    // Set Request Header
                    setRequestHeader(ObjHttp);
                    ObjHttp.setRequestHeader("Cookie", "JSESSIONID=" + sessionidspn);
                    ObjHttp.setRequestHeader("Referer", "http://apps.jims.hctx.net/subcrim/Logon.jsp");
                    ObjHttp.send("logonid=" + logonid + "&password=" + password + "&newpasswordverify=&newpassword=");
                }

                //url = "http://subcrim.jims.hctx.net/subcrim/SpnInquiry.do?spn="+spn+"&submit=submit";
                url = "http://apps.jims.hctx.net/subcrim/CaseInquiry.do?caseNum=" + casenumber + "&courtDivision=" + cdi + "&submit=submit";
                //Connecting to site
                ObjHttp.open("post", url, false, null, null);
                //Sending parameters
                ObjHttp.send(null);

                if (ObjHttp.status.ToString() == "200")
                {
                    return ObjHttp.responseText;
                }
                else
                {
                    return "error";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// No Description Available
        /// </summary>
        /// <param name="strSource">No Description Available</param>
        /// <returns>Return string value</returns>
        private string CleanString(string strSource)
        {

            int idxStr = strSource.IndexOf("jsessionid=");
            string Strlast = strSource.Remove(strSource.IndexOf("\" onsubmit=\""));
            // int IntDiff = idxStrlast- idxStr ;
            if (idxStr >= 0)
            {
                // return removeTages(strSource.Substring(idxStr).Trim());
                return Strlast.Substring(idxStr + 11).Trim();
            }
            else { return ""; }
            //return strSource.Trim(); 
        }

        /// <summary>
        /// This method returns the transaction mode for the credit card transaction.
        /// </summary>
        /// <param name="Request">Takes the current request object</param>
        /// <returns>Return true if the host machine and the request machines are same.</returns>
        /// <remarks>
        /// This Methods Check the Current Request IP and Compare it with The Test IP mentioned in the project.
        /// If Current IP and The Request Host IP is same it will return true otherwise it will return false.
        ///</remarks>
        public bool SetTransactionMode(HttpRequest Request)
        {
            TransType = false;

            string localserveripaddress = ConfigurationManager.AppSettings["LocalServerIPAddress"].ToString();
            if (localserveripaddress == Request.ServerVariables["REMOTE_HOST"].ToString())
            {
                if (ConfigurationManager.AppSettings["Test_Request"].ToString() == "True")
                    TransType = true;
                else
                    TransType = false;
            }
            else
            {
                TransType = false;
            }

            return TransType;
        }

        /// <summary>
        /// This function format the phone number in the format (XXX) 
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        /// <example>
        /// <code>
        ///        
        /// string phone = "02152552525252";
        /// // Format Phone to (021)5255-2552 X-2525
        /// string formatPhone = formatPhone(phone);
        ///   
        /// </code>
        /// </example>
        public string formatPhone(string phone)
        {
            try
            {
                if (phone == null)
                    phone = "";

                string clcode = phone;
                if (clcode.Length == 10)
                {
                    string temp = clcode.Insert(0, "(");
                    temp = temp.Insert(4, ") ");
                    return temp.Insert(9, "-");
                }
                else if (clcode.Length > 10)
                {
                    string temp = clcode.Insert(0, "(");
                    temp = temp.Insert(4, ") ");
                    temp = temp.Insert(9, "-");
                    return temp.Insert(14, " X-");

                }

                return phone;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //return phone;
            }

        }

        /// <summary>
        /// Set HTTP request header variables for accessing the court website
        /// </summary>
        /// <param name="ObjHttp">Takes XMLHttp object and set the request header variables</param>
        public void setRequestHeader(XMLHTTP ObjHttp)
        {
            ObjHttp.setRequestHeader("(Request-Line)", "POST  /subcrim/subcrim_logon.do HTTP/1.1");
            ObjHttp.setRequestHeader("Accept", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
            ObjHttp.setRequestHeader("Accept-Encoding", "gzip, deflate");
            ObjHttp.setRequestHeader("Accept-Language", "en-us,fr;q=0.5");
            ObjHttp.setRequestHeader("Cache-Control", "no-cache");
            ObjHttp.setRequestHeader("Connection", "Keep-Alive");
            ObjHttp.setRequestHeader("Content-Length", "63");
            ObjHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ObjHttp.setRequestHeader("Host", "apps.jims.hctx.net");
        }

        /// <summary>
        /// This function changed the date from 'MM/DD/YYYY' forma  to 'MM/DD/YY' format
        /// </summary>
        /// <param name="date">Date in the following 'MM/DD/YYYY' format</param>
        /// <returns>Returns the date in 'MM/DD/YY' format</returns>
        public static string FormatDate(string date)
        {
            try
            {
                if (date.Length >= 8)
                {
                    date = date.Trim();
                    string UpdateDate = date.Substring(0, date.LastIndexOf('/') + 1) + date.Substring(date.Length - 2, 2);
                    return UpdateDate;
                }
                else if (date == "")
                    return "N/A";

                return date;
            }
            catch
            {
                return date;
            }
        }


        /*
        // remarked temporarily by tahir ahmed ......
        //	public bool getYDS(string strAddress,string strCity,string strState,string strZip)
        //{
        //			//	string strAddress= txtaddress.Text ; 
        //			//string strCity= txtCity.Text  ;
        //			//string strState= txtState.Text  ;
        //			//string strZip= txtZip.Text ; 
        //			bool rt;
        //			int count=0;
        //			USPS10.CUSPSCass10Class cAdd= new USPS10.CUSPSCass10Class();
        //			rt= cAdd.Process_Addresses(ref strAddress,ref strCity,ref strState,ref strZip); 
        //			
        //			//clsAddressProcess cAdd = new clsAddressProcess();
        //			//			bool rt= cAdd.Process_Addresses("701 Avion","Academy","Texas","76554","ssdf");
        //
        //			while(! rt) //retry 3 times 
        //			{
        //				if(count == 3)break;
        //				rt= cAdd.Process_Addresses(ref strAddress,ref strCity,ref strState,ref strZip); 
        //				count++;
        //			}
        //			YDSstrPostalString="";
        //
        //			if(rt )
        //			{
        //				if(cAdd.postalreturnstring.Trim() != "" )
        //				{
        //					YDSstrPostalString=cAdd.postalreturnstring;
        //					string[] aryoutputinfo;
        //					bool cr = true;
        //					aryoutputinfo = YDSstrPostalString.Split('\t');
        //                                        
        //					YDSstraddress = aryoutputinfo[0];
        //					YDSstrcity = aryoutputinfo[1];
        //					YDSstrstate = aryoutputinfo[2];
        //					YDSstrzip = aryoutputinfo[3];
        //					YDSstrbarcode = aryoutputinfo[4];
        //					YDSstrdpv = aryoutputinfo[5];
        //					YDSstrcrrt = aryoutputinfo[6].Trim(); 
        //					//YDSstrcrrt = cAdd.StripControlChars(ref YDSstrcrrt,ref cr).Trim() ; 
        //					if (YDSstrdpv.Trim() == "" ){YDSstrdpv="N";}
        //
        //					return true;
        //				}
        //				else{return false;}
        //			}
        //			return rt;
        //	}

        /*
             * aryoutputinfo = Split(stroutput, "\t")
                                        
                                            YDSstraddress = aryoutputinfo(0)
                                            YDSstrcity = aryoutputinfo(1)
                                            YDSstrstate = aryoutputinfo(2)
                                            YDSstrzip = aryoutputinfo(3)
                                            YDSstrbarcode = aryoutputinfo(4)
                                            YDSstrdpv = aryoutputinfo(5)
                                            YDSstrcrrt = Trim(aryoutputinfo(6))
                                            YDSstrcrrt = Trim(StripControlChars(strcrrt))
                                        
        //	 * */
        //Method To send Email
        /*private void GenerateMail(string TO ,string FROM, string SUBJECT, string BODY )
        {
            string[] value1={"TO","FROM","SUBJECT","BODY"};
            object[] key1={TO,FROM,SUBJECT,BODY};
            obj.ExecuteSP("usp_SMTPMail",value1,key1);

        }
        */


        // Geerate CASS File
        /* remarked temporarily by tahir ahmed........
        public bool Get_CassFile(string strAddressFilePath, string strOutputFilePath, string strCassFilePath, string strDelimitedChar, ref string strException )
        {
            //			string strAddressFilePath= "D:\\0509230625CassBatch.gac";
            //			string strOutputFilePath= "D:\\x.txt";
            //			string strCassFilePath= "D:\\x.cas";
            //			string strDelimitedChar = ",";
            try
            {			
                //Object creation of type ZP4 class
                USPS10.CUSPSCass10Class objCUSPS = new USPS10.CUSPSCass10Class();
                // pass parameters for generate CASS File
                objCUSPS.create_address_file(ref strAddressFilePath, ref strOutputFilePath , ref strCassFilePath, ref strDelimitedChar);
                return true;
            }
            catch (Exception ex)
            {
                strException= ex.Message; 
                return false;
            }
        }
*/

        /// <summary>
        /// Get Violation Fine Amount From Houston Court WebSite
        /// </summary>
        /// <param name="SPN">Case SPN Number</param>
        /// <param name="ErrorMessage">Error Message</param>
        /// <returns>Returns database containing Cause Number and Fine Amount</returns>
        public DataTable GetViolationAmount(string SPN, out string ErrorMessage)
        {
            //Zeeshan Ahmed 3534 04/15/2008
            //Get Credential To Access Houston Site
            string HSLink = "https://www.equery.mca.houstontx.gov";
            string HSUser = ConfigurationManager.AppSettings["HSUser"];
            string HSPassword = ConfigurationManager.AppSettings["HSPassword"];

            //Create Temporary DataTable
            DataTable dt = new DataTable("ViolationAmount");
            dt.Columns.Add("CauseNumber");
            dt.Columns.Add("FineAmount");
            ErrorMessage = "";

            string Response = "";
            XMLHTTP ObjHttp = new XMLHTTP();

            //Connecting to Houston Site
            ObjHttp.open("POST", HSLink, false, null, null);
            ObjHttp.send(null);

            if (ObjHttp.status.ToString() == "200")
            {
                //If WebSite Is In Maintenance
                if (ObjHttp.responseText.Contains("The eQuery system is currently undergoing daily maintenance"))
                {
                    ErrorMessage = "The court system is currently undergoing daily maintenance.";
                    return dt;
                }

                Response = ObjHttp.responseText;
                //Get Information For Login
                string p_instance = FindParameterValue("p_instance", Response, 0);
                string p_page_submission_id = FindParameterValue("p_page_submission_id", Response, 0);
                string p_arg_names = FindParameterValue("p_arg_names", Response, 0);
                string p_arg_names1 = FindParameterValue("p_arg_names", Response, 1);

                //Send Login Information           
                ObjHttp.open("POST", "https://www.equery.mca.houstontx.gov/pls/apex/wwv_flow.accept?p_arg_names=" + p_arg_names + "&p_request=LOGIN&p_arg_names=" + p_arg_names1 + "&p_flow_id=101&p_flow_step_id=101&p_instance=" + p_instance + "&p_page_submission_id=" + p_page_submission_id + "&p_request=&p_md5_checksum=&p_t01=" + HSUser + "&p_t02=" + HSPassword, false, null, null);
                ObjHttp.send(null);

                //If Login Successfully
                if (ObjHttp.status.ToString() == "200")
                {
                    if (ObjHttp.responseText.Contains("Invalid Login Credentials"))
                    {
                        ErrorMessage = "Login failed to the court website.";
                        return dt;
                    }

                    Response = ObjHttp.responseText;
                    string[] ParameterArray = new string[9];
                    //Get Parameter Values
                    for (int i = 0; i < 9; i++)
                        ParameterArray[i] = FindParameterValue("p_arg_names", Response, i);

                    p_instance = FindParameterValue("p_instance", Response, 0);
                    p_page_submission_id = FindParameterValue("p_page_submission_id", Response, 0);

                    //Search Case
                    ObjHttp.open("POST", "https://www.equery.mca.houstontx.gov/pls/apex/wwv_flow.accept?p_request=SUBMIT&p_flow_id=101&p_flow_step_id=1&p_instance=" + p_instance + "&p_page_submission_id=" + p_page_submission_id + "&p_request=&p_md5_checksum=&p_t09=-1&p_t08=" + SPN + "&p_t07=&p_t06=&p_t05=&p_t04=&p_t03=&p_t02=&p_t01=&p_arg_names=" + ParameterArray[0] + "&p_arg_names=" + ParameterArray[1] + "&p_arg_names=" + ParameterArray[2] + "&p_arg_names=" + ParameterArray[3] + "&p_arg_names=" + ParameterArray[4] + "&p_arg_names=" + ParameterArray[5] + "&p_arg_names=" + ParameterArray[6] + "&p_arg_names=" + ParameterArray[7] + "&p_arg_names=" + ParameterArray[8], false, null, null);
                    ObjHttp.setRequestHeader("Referer", "https://www.equery.mca.houstontx.gov/pls/apex/f?p=101:1:" + p_instance);
                    ObjHttp.send(null);

                    if (ObjHttp.status.ToString() == "200")
                    {
                        //Get Violations
                        Response = ObjHttp.responseText;

                        int ViolationTableStart = Response.IndexOf("<table class=\"t5StandardAlternatingRowColors\"");

                        //Record Not Found
                        if (ViolationTableStart == -1)
                        {
                            ErrorMessage = "Case information not found.";
                            return dt;
                        }

                        int ViolationTableEnd = Response.IndexOf("</table>", ViolationTableStart);
                        string ViolationTable = Response.Substring(ViolationTableStart, ViolationTableEnd + 6 - ViolationTableStart);

                        int RowIndex = ViolationTable.IndexOf("COL07");
                        RowIndex = ViolationTable.IndexOf("COL07", RowIndex + 1);

                        while (RowIndex != -1)
                        {
                            DataRow dr = dt.NewRow();

                            //Get Cause Number
                            int start = ViolationTable.IndexOf(">", RowIndex + 8);
                            string CauseNumber = ViolationTable.Substring(start + 1, ViolationTable.IndexOf("<", start) - start - 1);

                            //Getting Fine Amount
                            int startFine = ViolationTable.IndexOf("COL13", RowIndex);
                            int endFine = ViolationTable.IndexOf("</", startFine);
                            string FineAmount = ViolationTable.Substring(startFine + 9, endFine - startFine - 9);

                            //Save Violation In DataTable
                            dr[0] = CauseNumber.Replace(" ", "");
                            dr[1] = FineAmount;
                            dt.Rows.Add(dr);
                            RowIndex = ViolationTable.IndexOf("COL07", RowIndex + 1);
                        }
                        return dt;
                    }
                }
            }
            else
                ErrorMessage = "Could not found url.";
            return dt;
        }

        /// <summary>
        /// This funciton find parameter value from the html response string.
        /// </summary>
        /// <param name="ParameterName">Parameter Name</param>
        /// <param name="Response">HTML Response string</param>
        /// <param name="Position">Position of Parameter</param>
        /// <returns>Return parameter value</returns>
        public string FindParameterValue(string ParameterName, string Response, int Position)
        {
            //Zeeshan Ahmed 3534 04/15/2008
            string tempResponse = Response;
            if (Position != 0)
            {
                int index = 0;
                for (int i = 0; i <= Position; i++)
                    index = tempResponse.IndexOf(ParameterName, index + 1);

                tempResponse = tempResponse.Substring(index);
            }
            else
                tempResponse = tempResponse.Substring(tempResponse.IndexOf(ParameterName));

            int indexofValue = tempResponse.IndexOf("value=");
            tempResponse = tempResponse.Substring(indexofValue + 7, tempResponse.IndexOf("\"", indexofValue + 7) - (indexofValue + 7));
            return tempResponse;
        }


        //Sabir Khan 5084 11/10/2008 Get total days from business days...
        public int GetTotalDaysFromBusinessDays(int busnissdays)
        {

            int Totaldays = 0;
            for (int i = 0; i <= busnissdays; i++)
            {
                Totaldays += 1;
                if (i == 0 && DateTime.Now.AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                    Totaldays += 1;
                if (DateTime.Now.AddDays(i).DayOfWeek == DayOfWeek.Saturday)
                    Totaldays += 2;
            }
            return Totaldays;
        }

        // Abid Ali 4912 11/13/2008 Get next business day's date
        /// <summary>
        /// Return next business day's date from specifed fromDate to no. of business days
        /// </summary>
        /// <param name="fromDate">Start date from business date calculate</param>
        /// <param name="busnissdays">No. of days after fromDate</param>
        /// <returns>Business Day's Date</returns>
        public static DateTime GetBusinessDayDate(DateTime fromDate, int busnissdays)
        {
            DateTime businessDayDate = DateTime.Now;

            if (busnissdays >= 0)
            {
                businessDayDate = fromDate;
                int totalDays = 0;
                DayOfWeek currentDay;
                for (int i = 1; i <= busnissdays; i++)
                {
                    totalDays += 1;
                    currentDay = fromDate.AddDays(i).DayOfWeek;

                    if (currentDay == DayOfWeek.Saturday || currentDay == DayOfWeek.Sunday)
                    {
                        busnissdays++;
                    }
                }

                businessDayDate = fromDate.AddDays(totalDays);
            }

            // return next business day date
            return businessDayDate;
        }
        //Nasir 5437 01/26/2009
        /// <summary>
        ///  delete the files of all previous days according the given path and extension
        /// </summary>
        /// <param name="path">files path</param>
        /// <param name="Ext">files Extension</param>
        public bool DeleteTempFiles(string path, string Ext)
        {
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();
            string[] files = System.IO.Directory.GetFiles(path, Ext);
            for (int i = 0; i < files.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);
                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files[i]);
                }
            }
            return true;



        }

        //Ozair 5771 04/23/2009
        /// <summary>
        /// Encode the hyperlink url for the given gridview.
        /// </summary>
        /// <param name="gridView"></param>
        public static void HyperLinkFieldUrlEncodeHack(GridView gridView)
        {
            if (gridView == null)
            {
                return;
            }
            gridView.RowDataBound += delegate(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    return;
                }
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    DataControlField field = gridView.Columns[i];
                    if (field is HyperLinkField)
                    {
                        TableCell td = e.Row.Cells[i];
                        if (td.Controls.Count > 0 && td.Controls[0] is HyperLink)
                        {
                            HyperLink hyperLink = (HyperLink)td.Controls[0];
                            HyperLinkField hyperLinkField = (HyperLinkField)field;
                            if (!String.IsNullOrEmpty(hyperLinkField.DataNavigateUrlFormatString))
                            {
                                string[] dataUrlFields =
                                  new string[hyperLinkField.DataNavigateUrlFields.Length];
                                for (int j = 0; j < dataUrlFields.Length; j++)
                                {
                                    object obj = DataBinder.Eval(e.Row.DataItem,
                                        hyperLinkField.DataNavigateUrlFields[j]);
                                    dataUrlFields[j] = HttpUtility.UrlEncode(
                                        (obj == null ? "" : obj.ToString()));
                                }
                                hyperLink.NavigateUrl = String.Format(
                                    hyperLinkField.DataNavigateUrlFormatString, dataUrlFields);
                            }
                        }
                    }
                }
            };
        }

        //Nasir 5765 11/19/2009 
        /// <summary>
        /// Replace spaecial character &amp;, &apos;, &quot;, &gt;, &lt; with HTML encoding
        /// </summary>
        /// <param name="Text">Text to replace</param>
        /// <returns>string</returns>
        public static string EncodeXMLString(string Text)
        {
            Text = Text.Replace("&", "&amp;");
            Text = Text.Replace("'", "&apos;");
            Text = Text.Replace("\"", "&quot;");
            Text = Text.Replace(">", "&gt;");
            Text = Text.Replace("<", "&lt;");
            return Text;
        }

        //Sabir khan 6851 12/04/2009 
        /// <summary>
        /// This method is used to convert currency type into double.
        /// </summary>
        /// <param name="pCurrency">Currency value in string type</param>
        /// <returns>double</returns>
        public double CurrencyToDouble(string pCurrency)
        {
            if (pCurrency != "")
            {
                if (pCurrency.Contains("("))
                {
                    pCurrency = pCurrency.Replace("($", "-");
                    pCurrency = pCurrency.Remove(pCurrency.IndexOf(")"));
                }
                {
                    pCurrency = pCurrency.Replace("$", "");
                }
            }
            else
            {
                pCurrency = "0";
            }
            return Convert.ToDouble(pCurrency);

        }
    }
}

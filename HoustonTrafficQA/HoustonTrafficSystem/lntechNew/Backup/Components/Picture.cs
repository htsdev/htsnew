using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components
{
    public class Picture
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();

        #region Variables

        private int picID = 0;
        private string picturename = String.Empty;
        private int empid = 0;
        private int batchid=0;

        #endregion 
        #region Properties

        public int PicID
        {
            get
            {
                return picID;
            }
            set
            {
                picID = value;
            }
        }
        public string PictureName
        {
            get
            {
                return picturename;
            }
            set
            {
                picturename = value;
            }
        }
        public int EmpID
        {
            get
            {
                return picID;
            }
            set
            {
                picID = value;
            }
        }
        public int BatchID
        {
            get
            {
                return batchid;
            }
            set
            {
                batchid = value;
            }
        }
        #endregion

        #region Methods

        public int InsertInPicture()
        {
            int pid = 0;
            try
            {
                string[] key ={ "@BatchID", "@PicID" };
                object[] value1 ={BatchID,0};
                pid=(int)clsDb.InsertBySPArrRet("usp_WebScan_InsertInPic",key,value1);
                return pid;
            }
            catch (Exception ex)
            {
                return pid;
            }
        }
        public DataSet GetPicByBatchID(int BID)
        {
            DataSet DS=null;
            try
            {
                string[] key ={ "@BatchId" };
                object[] value1 ={ BID };
                DS = clsDb.Get_DS_BySPArr("usp_WebScan_GetPicsByBatchID",key,value1);
                return DS;
            }
            catch (Exception ex)
            {
                return DS;
            }

        }
        #endregion 
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;

namespace HTP.Components
{
    public class AutoDialer
    {
        #region Variables

        private short callDays = 0;
        private int courtID = 0;
        private short settingDateRange = 0;
        private bool eventState = false;
        private int empID = 0;
        private DateTime dialTime = DateTime.Now;
        private short eventTypeID = 0;
        private string eventTypeName = String.Empty;
        private DateTime startDate = DateTime.Now;
        private DateTime endDate = DateTime.Now;
        private bool showAll = false;
        private string responseTypeIDs = String.Empty;

        clsENationWebComponents clsDB = new clsENationWebComponents();

        #endregion

        #region Properties

        /// <summary>
        /// will be used to get and set the call days
        /// </summary>
        /// <value>
        /// CallDays no of business days
        /// </value>
        public short CallDays
        {
            get
            {
                return callDays;
            }
            set
            {
                callDays = value;
            }
        }

        /// <summary>
        /// will be used to get and set the court id
        /// </summary>
        /// <value>
        /// CourtID 0: all courts 1: hmc courts 2: non hmc courts
        /// </value>
        public int CourtID
        {
            get
            {
                return courtID;
            }
            set
            {
                courtID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the setting date range
        /// </summary>
        /// <value>
        /// SettingDateRange no of business days
        /// </value>
        public short SettingDateRange
        {
            get
            {
                return settingDateRange;
            }
            set
            {
                settingDateRange = value;
            }
        }

        /// <summary>
        /// will be used to get and set the event state
        /// </summary>
        /// <value>
        /// EventState false: stop true: start
        /// </value>
        public bool EventState
        {
            get
            {
                return eventState;
            }
            set
            {
                eventState = value;
            }
        }

        /// <summary>
        /// will be used to set and get Employee ID of current logged in user
        /// </summary>
        /// <value>
        /// Employee ID of current logged in user
        /// </value>
        public int EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the dial time 
        /// </summary>
        /// <value>
        /// DialTime
        /// </value>
        public DateTime DialTime
        {
            get
            {
                return dialTime;
            }
            set
            {
                dialTime = value;
            }
        }

        /// <summary>
        /// will be used to get and set the event type id
        /// </summary>
        /// <value>
        /// EventTypeID
        /// </value>
        public short EventTypeID
        {
            get
            {
                return eventTypeID;
            }
            set
            {
                eventTypeID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the event type name
        /// </summary>
        /// <value>
        /// EventTypeName
        /// </value>
        public string EventTypeName
        {
            get
            {
                return eventTypeName;
            }
            set
            {
                eventTypeName = value;
            }
        }

        /// <summary>
        /// will be used to get and set the Start date
        /// </summary>
        /// <value>
        /// DialTime
        /// </value>
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        /// <summary>
        /// will be used to get and set the End Date
        /// </summary>
        /// <value>
        /// DialTime
        /// </value>
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        /// <summary>
        /// will be used to get and set the show all records
        /// </summary>
        /// <value>
        /// show all false: per selected criteria true: show all
        /// </value>
        public bool ShowAll
        {
            get
            {
                return showAll;
            }
            set
            {
                showAll = value;
            }
        }

        /// <summary>
        /// will be used to get and set the comma separated Response type ids name
        /// </summary>
        /// <value>
        /// ResponseTypeIDs
        /// </value>
        public string ResponseTypeIDs
        {
            get
            {
                return responseTypeIDs;
            }
            set
            {
                responseTypeIDs = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method get the event settings detail for all event types
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        /// DataTable dt = clsAD.GetEventConfigSettings();
        /// </code>
        /// </example>
        public DataTable GetEventConfigSettings()
        {
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_EventConfigSettings");
            return dt;
        }

        //Farrukh 9332 07/28/2011 Multiple Event Call time implemented
        /// <summary>
        /// This method get the event dial time for a particular event
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        /// DataTable dt = clsAD.GetEventDialTimeByEventTypeID();
        /// </code>
        /// </example>
        public DataTable GetEventDialTimeByEventTypeID()
        {
            string[] key = { "@EventTypeID" };
            object[] value = { EventTypeID };
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_EventDialTimeByEventTypeID",key,value);
            return dt;
        }

        /// <summary>
        /// This method get the event settings detail by event type
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        /// clsAD.EventTypeID = 1;
        /// DataTable dt = clsAD.GetEventConfigSettingsByID();
        /// </code>
        /// </example>
        public DataTable GetEventConfigSettingsByID()
        {
            string[] key = { "@EventTypeID" };
            object[] value1 = { EventTypeID };
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_EventConfigSettingsByID", key, value1);
            return dt;
        }

        /// <summary>
        /// This method updates the event config settings
        /// it also inserts notes in config settings history
        /// </summary>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        /// clsAD.EventTypeID = Convert.ToInt16(hf_EventID1.Value);
        /// clsAD.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
        /// clsAD.CallDays = Convert.ToInt16(ddl_Days1.SelectedValue);
        /// clsAD.CourtID = Convert.ToInt32(ddl_Courts1.SelectedValue);
        /// clsAD.SettingDateRange = Convert.ToInt16(ddl_Range1.SelectedValue);
        /// clsAD.EventState = rb_Start1.Checked;
        /// clsAD.DialTime = tp_DialTime1.SelectedTime;
        /// clsAD.UpdateEventConfigSettingsByID();
        /// </code>
        /// </example>
        public void UpdateEventConfigSettingsByID()
        {
            string[] key = { "@EventTypeID", "@EventTypeDays", "@CourtID", "@SettingDateRange", "@EventState", "@DialTime", "@EmployeeID" };
            object[] value1 = { EventTypeID, CallDays, CourtID, SettingDateRange, EventState, DialTime, EmpID };
            clsDB.ExecuteSP("usp_AD_Update_EventConfigSettingsByID", key, value1);
        }

        /// <summary>
        /// This method updates the event config settings event state for all events
        /// it also inserts notes in config settings history
        /// </summary>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        /// clsAD.EventState = rb_StartAll.Checked;
        /// clsAD.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
        /// clsAD.UpdateAllEventState();
        /// </code>
        /// </example>
        public void UpdateAllEventState()
        {
            string[] key = { "@EventState", "@EmployeeID" };
            object[] value1 = { EventState, EmpID };
            clsDB.ExecuteSP("usp_AD_Update_AllEventState", key, value1);
        }

        /// <summary>
        /// This method get the event config settings history
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        ///  DataTable dt = clsAD.GetEventConfigSettingsHistory();
        /// </code>
        /// </example>
        public DataTable GetEventConfigSettingsHistory()
        {
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_EventConfigSettingsHistory");
            return dt;
        }

        /// <summary>
        /// This method get the event types from eventsettings 
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        ///  DataTable dt = clsAD.GetAllEventTypes();
        /// </code>
        /// </example>
        public DataTable GetAllEventTypes()
        {
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_AllEventTypes");
            return dt;
        }

        /// <summary>
        /// This method get the call out comes
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        ///  DataTable dt = clsAD.GetCallOutComes();
        /// </code>
        /// </example>
        public DataTable GetCallOutComes()
        {
            string[] key = { "@Court", "@EventType", "@StartDate", "@EndDate", "@ResponseTypeIDs", "@ShowAll" };
            object[] value1 = { CourtID, EventTypeID, StartDate, EndDate, ResponseTypeIDs, ShowAll };
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_CallOutCome", key, value1);
            return dt;
        }


        /// <summary>
        /// This method get the AutoDialerLog
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        /// AutoDialer clsAD = new AutoDialer();
        ///  DataTable dt = clsAD.GetAutoDialerLog();
        /// </code>
        /// </example>
        public DataTable GetAutoDialerLog()
        {
            string[] key = {  "@EventType", "@StartDate", "@EndDate" };
            object[] value1 = {  EventTypeID, StartDate, EndDate };
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_AutoDialerLog", key, value1);
            return dt;
        }


        /// <summary>
        /// This method get all response types
        /// </summary>
        /// <returns>DataTable</returns>
        /// <example>
        /// <code>
        ///  DataTable dt = clsAD.GetResponseTypes();
        /// </code>
        /// </example>
        public DataTable GetResponseTypes()
        {
            DataTable dt = clsDB.Get_DT_BySPArr("usp_AD_Get_ResponseTypes");
            return dt;
        }

        /// <summary>
        /// This method inserts the event call time 
        /// </summary>
        public void InsertEventCallTime()
        {
            string[] key = { "@eventtypeid", "@dialtime" };
            object[] value1 = { EventTypeID, DialTime };
            clsDB.ExecuteSP("USP_AD_INSERT_EventCallTime", key, value1);
        }

        /// <summary>
        /// This method delete the event call time 
        /// </summary>
        public void DeleteEventCallTime()
        {
            string[] key = { "@eventtypeid" };
            object[] value1 = { EventTypeID };
            clsDB.ExecuteSP("USP_AD_DELETE_EventCallTime", key, value1);
        }

        #endregion
    }
}

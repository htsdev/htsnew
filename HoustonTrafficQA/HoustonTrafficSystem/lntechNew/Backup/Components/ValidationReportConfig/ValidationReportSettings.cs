﻿using System;
using System.Data;
using FrameWorkEnation.Components;


namespace HTP.Components.ValidationReportConfig
{
    /// <summary>
    /// This class provide supportive methods for validation report settings.
    /// </summary>
    public class ValidationReportSettings
    {
        #region Vairables
        //SAEED 7791 06/26/2010 class created.
        clsENationWebComponents _clsDb = new clsENationWebComponents();
        #endregion

        #region Public Methods

        //SAEED 7791 06/14/2010 method created.
        /// <summary>
        /// This method returns list of users who are associated with all active reports.
        /// </summary>
        /// <returns>Returns datatable containing list of users who are associated with active reports.</returns>
        public DataTable GetAllSettings()
        {
            string[] key = { "@active" };
            object[] obj = { '1' };
            return _clsDb.Get_DT_BySPArr("USP_HTP_GET_VALIDATIONREPORT", key, obj);
        }

        //SAEED 7791 06/14/2010 method created.
        /// <summary>
        /// This method returns list of all active users.
        /// </summary>
        /// <returns>Returns datatable contains list of all active users.</returns>
        public DataTable GetAllUser()
        {
            return _clsDb.Get_DT_BySPArr("USP_HTP_Get_ActiveUserList");
        }

        //SAEED 7791 06/14/2010 method created.
        /// <summary>
        /// This method update primary/secondary users who are associated with the reports. 
        /// </summary>
        /// <param name="obj">Parameter array</param>
        /// <returns>Returns true if update primary/secondary users successfully, otherwise returns false.</returns>
        public bool UpdateValidationUsers(object[] obj)
        {
            string[] key = { "@ReportID", "@Pri_Mon", "@Pri_Tue", "@Pri_Wed", "@Pri_Thu", "@Pri_Fri", "@Pri_Sat", "@Sec_Mon", "@Sec_Tue", "@Sec_Wed", "@Sec_Thu", "@Sec_Fri", "@Sec_Sat" };
            return Convert.ToBoolean(_clsDb.ExecuteScalerBySp("USP_HTP_UPDATE_ValidationUser", key, obj));
        }

        //SAEED 7791 06/14/2010 method created.
        /// <summary>
        /// This method returns all available timings on which primary/secondary user can be scheduled to receive emails.
        /// </summary>
        /// <returns>Returns datatable of all available timings on which user can be scheduled to receive emails.</returns>
        public DataTable GetEmailTimings()
        {
            return _clsDb.Get_DT_BySPArr("USP_HTP_GET_EmailTimings");
        }

        //SAEED 7791 06/14/2010 method created.
        /// <summary>
        /// This method returns schedule of validation report, based on provided reportid and active flag. The schedule represents weekday & saturday timings.These are the timings when user associated with the report will receive an emails.
        /// </summary>
        /// <param name="obj">Parameter array</param>
        /// <returns>Returns datatable containing schedule of validation report.</returns>
        public DataTable GetEmailTimingsByReportId(object[] obj)
        {
            string[] key = { "@ReportID", "@isActive"};
            return _clsDb.Get_DT_BySPArr("USP_HTP_GET_ValidationSchedule", key, obj);
        }

        //SAEED 7791 06/14/2010 method created.
        /// <summary>
        /// This method is used to update eamil timings for the particular reportID which is passed as an input parameter.
        /// </summary>
        /// <param name="obj">Parameter array</param>
        /// <returns>Returns true if email timings update successfully.</returns>
        public bool UpdateValidationSchedule(object[] obj)
        {
            string[] key = { "@ReportID", "@WeekdayTiming1", "@WeekdayTiming2", "@WeekdayTiming3", "@SaturdayTiming1", "@SaturdayTiming2", "@SaturdayTiming3" ,
                               "@IdWeekdayTiming1","@IdWeekdayTiming2","@IdWeekdayTiming3","@IdSaturdayTiming1","@IdSaturdayTiming2","@IdSaturdayTiming3"};
            return Convert.ToBoolean(_clsDb.ExecuteScalerBySp("USP_HTP_Update_ValidationSchedule", key, obj));
        }

        #endregion
    }
}

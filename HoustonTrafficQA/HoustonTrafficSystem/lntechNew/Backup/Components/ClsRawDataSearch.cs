﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;

namespace HTP.Components
{
    public class ClsRawDataSearch
    {
        clsENationWebComponents clsDB = new clsENationWebComponents("LoaderFilesArchive");
        

       public enum Loaders
        {
            HCJP = 1,
            Arraignment = 2,
            DLQ = 3,
            Events = 4,
            Warrant = 5,
            sugerland = 6,
            Disposed = 7,
            FWArraignment = 8,
            DallasArraignment = 9,
            DallasBonds = 10,
            //HarrisCounty=11,
            DallasCountyJPCourt = 12,
            DallasCountyCriminalPasstoHire = 13,
            DallasCountyCriminalBookedIn = 14,
            IrvingArraignment = 15,
            IrvingBonds = 16,
            HCJPEnteredCases = 17,
            HCJPEventsCases = 18,
            HCJPDisposedCases = 19,
            HarrisCountyCriminalCourt = 20,
            PasadenaArraignment = 21,
            PasadenaWarrant = 22

        }
      public enum SearchMode
       {
           TicketNumber = 1,
           CauseNumber = 2,
           LastName = 3,
           FileDate = 4,
           CourtDate = 5
       }
   
 
      private string CauseNo = string.Empty;
      private string TicketNo = string.Empty;
      private string LastName = string.Empty;
      private string FirstName = string.Empty;
      private string MiddleName = string.Empty;
      private int loaderid = 0;
      private int mode = 0;
      DateTime filedate = new DateTime(1900, 1, 1);
      DateTime DOB = new DateTime(1900, 1, 1);
      DateTime CrtDate = new DateTime(1900, 1, 1);

      public string CauseNumber
      {
          get
          {
              return CauseNo;
          }
          set
          {
              CauseNo = value;
          }
      }
      public int LoaderId
      {
          get
          {
              return loaderid;
          }
          set
          {
              loaderid = value;
          }
 
      }
      public int Mode
      {
          get
          {
              return mode;
          }
          set
          {
              mode = value;
          }

      }
      public string TicketNumber
      {
          get
          {
              return TicketNo;
          }
          set
          {
              TicketNo = value;
          }
      }
      public string lastname
      {
          get
          {
              return LastName;
          }
          set
          {
              LastName = value;
          }
      }
      public string firstname
      {
          get
          {
              return FirstName;
          }
          set
          {
              FirstName = value;
          }
      }
      public string middlename
      {
          get
          {
              return MiddleName;
          }
          set
          {
              MiddleName = value;
          }
      }
      public DateTime FileDate
      {
          get
          {
              return filedate;
          }
          set
          {
              filedate = value;
          }
      }
      public DateTime DateOfBirth
      {
          get
          {
              return DOB;
          }
          set
          {
              DOB = value;
          }
      }
      public DateTime CourtDate
      {
          get
          {
              return CrtDate;
          }
          set
          {
              CrtDate = value;
          }
      }
        
      //Fahad 5835 05/29/2009  
      /// <summary>
      /// Returns DataTable and get Data on basis of Loader and Mode ID
      /// </summary>
      /// <param name="LoaderId"></param>
      /// <param name="Mode"></param>
      /// <returns></returns>
        public DataTable GetData(int LoaderId,int Mode)
      {
          DataTable dt = new DataTable();
          dt = null;
          if (LoaderId > 0 && Mode > 0)
          {
              string[] keys = { "@LoaderId", "@Mode", "@CauseNo", "@TicketNo", "@CrtDate", "@LastName", "@FirstName", "@MiddleName", "@DOB" };
              object[] values = { loaderid, mode, CauseNo, TicketNo, CrtDate, LastName, FirstName, MiddleName, DOB };
              dt = clsDB.Get_DT_BySPArr("USP_HTP_SearchHCJPData", keys, values);
          }
          return dt;
      }
     
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;



namespace lntechNew.Components
{
    
    public class Log
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();
#region Variables
        
        private int logID = 0;
        private int picID = 0;
        private string status = String.Empty;

#endregion

        #region Properties

        public int LogID
        {
            get
            {
                return logID;
            }
            set
            {
                logID = value;
            }
        }
        public int PicID
        {
            get
            {
                return picID;
            }
            set
            {
                picID = value;
            }
        }
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        #endregion

        #region Methods

        public void InsertInLog()
        {
            try
            {
                string[] key ={ "@PicID", "@Status" };
                object[] value1 ={ PicID,Status};
                clsDb.InsertBySPArr("usp_WebScan_InsertInLog",key,value1);
            }
            catch (Exception ex)
            {  
            }
        }
        #endregion
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;

namespace lntechNew.Components
{//----------Class Add by Azwar for Letter Printing Restrictions---------------
    /// <summary>
    /// Documents Class
    /// </summary>
    public class Documents
    {
        #region Variables



        private object _value;
        private DataSet _ds = null;
        private object[] _values;
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        clsLogger clog = new clsLogger();

        #endregion

        #region Properties




        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Parameter Value may not be null or blank");
                }
                _value = value;
            }
        }

        public object[] Values
        {
            get
            {
                return _values;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Parameter Value may not be null or blank");
                }
                _values = value;
            }
        }

        public DataSet DS
        {
            get
            {
                return _ds;
            }
            set
            {
                _ds = value;
            }
        }

        #endregion

        #region Methods
        public DataSet RestrictTrialLetterOnPaymentInfo()
        {

            try
            {
                DS = ClsDb.Get_DS_BySPByOneParmameter("usp_hts_check_NoLetter", "@ticketid", Value);


                return DS;
            }
            catch (Exception ex)
            {

                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }

        }
        public DataSet RestrictTrialLetterOnBatchPrint()
        {

            try
            {
                string[] key = { "@empid", "@LetterType", "@TicketIDList" };

                DS = ClsDb.Get_DS_BySPArr("USP_HTS_GET_TRIAL_NOTIFICATION", key, Values);


                return DS;
            }
            catch (Exception ex)
            {

                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }

        }
        public DataSet CheckNoTrialLetterFlag()
        {
            try
            {
                DS = ClsDb.Get_DS_BySPByOneParmameter("usp_hts_check_JuryStatusForTrialLetter", "@TicketID", Value);
                return DS;
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return DS;
            }
        }

        // Noufil 3962 05/07/2008 Procedures to generate file name and  get the path on doc storage where file need to be save
        /// <summary>
        /// This procedure get the path of docstorgae with the filename where document need to save
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="empId"></param>
        /// <param name="descriptionText"></param>
        /// <returns>path + document name + entension</returns>

        public string SaveAndGetDocumentPath(int ticketId, int empId, string descriptionText)
        {
            string vNTPATHScanImage = Convert.ToString(ConfigurationManager.AppSettings["NTPATHScanImage"]);
            vNTPATHScanImage = vNTPATHScanImage.Replace("\\\\", "\\");
            string picName = SaveDocument(DateTime.Now, "Jpeg", descriptionText, "other", 0, empId, ticketId, 1, 0, "Printed", "");
            return vNTPATHScanImage + picName + ".Jpeg";
        }
        /// <summary>
        /// This procedure generate and return the name of document and insert the record in scan doc
        /// </summary>
        /// <param name="updated"></param>
        /// <param name="extension"></param>
        /// <param name="Description"></param>
        /// <param name="DocType"></param>
        /// <param name="SubDocTypeID"></param>
        /// <param name="Employee"></param>
        /// <param name="TicketID"></param>
        /// <param name="Count"></param>
        /// <param name="Book"></param>
        /// <param name="Events"></param>
        /// <param name="BookID"></param>
        /// <returns>File Name</returns>
        public string SaveDocument(DateTime updated, string extension, string Description, string DocType, int SubDocTypeID, int Employee, int TicketID, int Count, int Book, string Events, string BookID)
        {
            string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
            object[] value1 = { updated, extension, Description, DocType, SubDocTypeID, Employee, TicketID, Count, Book, Events, BookID };
            return ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();

        }

        //Afaq 8521 12/08/2010 saving email in pdf format under docstorage
        /// <summary>
        /// This method is used to save potential bond forfeitures email in pdf format
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="response"></param>
        /// <param name="description"></param>
        /// <returns>bool</returns>
        public bool SaveEmailPDF(string fileName, string response, string description,int empid,int ticketid)
        {
           
                // Set File Path Variables
                string vNTPATHScanTemp = Convert.ToString(ConfigurationSettings.AppSettings["NTPATHScanTemp"]);
                string vNTPATHScanImage = Convert.ToString(ConfigurationSettings.AppSettings["NTPATHScanImage"]);
                vNTPATHScanTemp = vNTPATHScanTemp.Replace("\\\\", "\\");
                vNTPATHScanImage = vNTPATHScanImage.Replace("\\\\", "\\");
                string sourcepath = vNTPATHScanTemp + fileName;

                GeneratePDF(sourcepath, response);

                int BookId = 0, picID = 0;
                string picName, picDestination;


                string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                object[] value1 = { DateTime.Now, "PDF", description, "Other", 0, empid, ticketid, 1, 0, "Email", "" };
                picName = ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();

                //Getting File Name            
                string BookI = picName.Split('-')[0];
                string picI = picName.Split('-')[1];

                BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                picID = (int)Convert.ChangeType(picI, typeof(int)); ; //

                //Move file To Desctination File Path

                picDestination = vNTPATHScanImage + picName + ".pdf";
                System.IO.File.Copy(sourcepath, picDestination);
                System.IO.File.Delete(sourcepath);
                return true;

            

        }


        //Afaq 8521 12/08/2010 Method added for generating PDF
        /// <summary>
        /// This method is used to generate pdf.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="response"></param>
        private void GeneratePDF(string path, string response)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(0, 0);

            theDoc.Page = theDoc.AddPage();
            int theID;
            theID = theDoc.AddImageHtml(response);
            while (true)
            {
                theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }
            theDoc.Save(path);
            theDoc.Clear();
        }


        #endregion


    }
}

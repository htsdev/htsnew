﻿using System;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using FrameWorkEnation.Components;
using LNHelper;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components
{
    public class clsAddNewLead : MailClass
    {
        //clsENationWebComponents clsdbb = new clsENationWebComponents("Sullolaw");
        clsENationWebComponents clsdbb = new clsENationWebComponents("SullolawDNN");
        clsENationWebComponents _clsdb = new clsENationWebComponents("ArticleManagementSystem");
        // Rab Nawaz Khan 11473 10/30/2013 Added TrafficTickets connection string. . . 
        clsENationWebComponents ttdb = new clsENationWebComponents("Connection String");
        
        clsLogger clog = new clsLogger();

        public static string BCC = Convert.ToString(ConfigurationManager.AppSettings["MailBCc"]);

        ///<summary>
        ///</summary>
        ///<param name="practiceAreaId"></param>
        ///<returns></returns>
        public DataTable GetManufacturers(int practiceAreaId)
        {
            string[] Keys = { "@PracticeAreaId" };
            object[] Values = { practiceAreaId };
            return clsdbb.Get_DT_BySPArr("USP_PS_Get_Manufacturers", Keys, Values);
        }

        ///<summary>
        ///</summary>
        ///<param name="spName"></param>
        ///<returns></returns>
        public DataSet GetActivePracticeArea(string spName)
        {
            DataSet dsPracticeArera;
            // Rab Nawaz Khan 11473 10/31/2013 connetion string has been changed from ArticleManagementSystem to SulloLaw
            dsPracticeArera = _clsdb.Get_DS_BySP(spName);
            return dsPracticeArera;
        }


        // Rab Nawaz Khan 11473 10/22/2013 Added to get the Lead Types
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public DataSet GetLeadTypes(string spName)
        {
            return ttdb.Get_DS_BySP(spName);
        }

        /// <summary>
        /// This method is used to inser contact us online form for depuy hip in sullolaw database .
        /// </summary>
        /// <param name="lastName">Last Name</param>
        /// <param name="firstName">First Name</param>
        /// <param name="address1">Address1</param>
        /// <param name="address2">Address2</param>
        /// <param name="city">City</param>
        /// <param name="state">State</param>
        /// <param name="zip">Zip</param>
        /// <param name="phone1">Phone1</param>
        /// <param name="isInternational">is international number</param>
        /// <param name="phone2">Phone2</param>
        /// <param name="phone1Type">Phone1 Type</param>
        /// <param name="phone2Type">Phone2 Type</param>
        /// <param name="email">Email</param>
        /// <param name="dob">DOB</param>
        /// <param name="surgeryDate">Surgery Date</param>
        /// <param name="manufacturer">Manufacturer</param>
        /// <param name="problemDescription">Problem Description</param>
        /// <param name="isRecalled">is Recalled</param>
        /// <param name="painLevel">Level of Pain</param>
        /// <param name="practiceAreaId">Practice Area</param>
        /// <param name="language">Language</param>
        /// <param name="salerepid">Sale Rep Id</param>
        /// <param name="callerId">Caller ID</param>
        /// <param name="isUnknown">is Unknown</param>
        /// <param name="isNameProvided"></param>
        /// <param name="leadTypeId"></param>
        public void SaveDepuyContactUs(string lastName, string firstName, string address1, string address2, string city, string state,
           string zip, string phone1, bool isInternational, string phone2, string phone1Type, string phone2Type, string email, DateTime dob, DateTime surgeryDate,
           string manufacturer, string problemDescription, string isRecalled, string painLevel, string practiceAreaId, string language, string salerepid,
            string callerId, bool isUnknown, bool isNameProvided, int leadTypeId, int ticketId)
        {
            string ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipaddress))
                ipaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            string[] keys = {
                                "@LastName", "@FirstName", "@Address1", "@Address2", "@City", "@State", "@Zip", "@Phone1","@IsInternational",
                                "@Phone2", "@Phone1Type", "@Phone2Type", "@Email", "@DOB", "@SurgeryDate", "@Manufacturer",
                                "@ProblemDescription", "@IsRecalled","@PainLevel","@PracticeAreaId", "@Language", "@IPAddress", "@SaleRepId",
                                // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                                "@CallerId", "@isUnknown", "@isNameProvided", "@leadTypeId", "@TicketId"
                            };
            object[] values = {
                                  lastName, firstName, address1, address2, city, state, zip, phone1,isInternational, phone2, phone1Type,
                                  phone2Type, email, dob, surgeryDate, manufacturer, problemDescription,isRecalled,painLevel,practiceAreaId, language, ipaddress, salerepid,
                                  callerId, isUnknown, isNameProvided, leadTypeId, ticketId // Rab Nawaz Khan 11473 10/21/2013 Added Parameter values. . . 
                              };
            clsdbb.ExecuteSP("USP_PS_SAVE_DEPUY_CONTACTUS", keys, values);
        }

        /// <summary>
        /// This method is used to inser contact us online form for actos in sullolaw database .
        /// </summary>
        /// <param name="personName">Person Name</param>
        /// <param name="phone1">Phone1</param>
        /// <param name="isInternational">is international number</param>
        /// <param name="phone1Type">Phone1 Type</param>
        /// <param name="email">Email</param>
        /// <param name="comments">Comments</param>
        /// <param name="practiceAreaId">Practice Area</param>
        /// <param name="language">Language</param>
        /// <param name="salerepid">Sale Rep Id</param>
        /// <param name="choice1"></param>
        /// <param name="choice2"></param>
        /// <param name="choice3"></param>
        /// <param name="symptons1"></param>
        /// <param name="symptons2"></param>
        /// <param name="symptons3"></param>
        /// <param name="symptons4"></param>
        /// <param name="symptons5"></param>
        /// <param name="surgeryDate"></param>
        /// <param name="question"></param>
        /// <param name="callerId"></param>
        /// <param name="isUnknown"></param>
        /// <param name="isActiveLead"></param>
        /// <param name="leadTypeId"></param>
        // Haris Ahmed 10292 05/22/2012 surgeryDate and question parameter added
        public void SaveActosContactUs(string personName, string email, string phone1, string phone1Type, bool isInternational,
            string choice1, string choice2, string choice3, bool symptons1, bool symptons2, bool symptons3, bool symptons4, bool symptons5, 
            string comments, string practiceAreaId, string language, string salerepid, DateTime surgeryDate, string question,
            string callerId, bool isUnknown, bool isUnknownName, int leadTypeId, int ticketId) // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
        {
            string ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipaddress))
                ipaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            string[] keys = {
                                "@PersonName", "@Email", "@Phone1", "@Phone1Type", "@IsInternational", "@Choice1",
                                "@Choice2", "@Choice3",
                                "@Symptons1", "@Symptons2", "@Symptons3", "@Symptons4", "@Symptons5", "@Comments",
                                "@practiceAreaId", "@Language", "@IPAddress", "@SaleRepId", "@SurgeryDate", "@Question",
                                "@CallerId", "@isUnknown", "@isNameProvided", "@leadTypeId", "@TicketId" // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                            };
            object[] values = {
                                  personName, email, phone1, phone1Type, isInternational, choice1, choice2, choice3,
                                  symptons1, symptons2, symptons3,
                                  symptons4, symptons5, comments, practiceAreaId, language, ipaddress, salerepid,
                                  surgeryDate, question, callerId, isUnknown, isUnknownName, leadTypeId, ticketId // Rab Nawaz Khan 11473 10/21/2013 Added Parameter values. . . 
                              };
            clsdbb.ExecuteSP("USP_PS_SAVE_ACTOS_CONTACTUS", keys, values);
        }

        /// <summary>
        /// This method is used to inser contact us online form in sullolaw database .
        /// </summary>
        /// <param name="name">Person Name</param>
        /// <param name="phone">Phone1</param>
        /// <param name="isInternational">is international number</param>
        /// <param name="email">Email</param>
        /// <param name="fax">Fax</param>
        /// <param name="comments">Comments</param>
        /// <param name="TicketId"></param>
        /// <param name="language">Language</param>
        /// <param name="salerepid">Sale Rep Id</param>
        /// <param name="city">City</param>
        /// <param name="PracArea">Practice Area</param>
        /// <param name="callerId">CallerId</param>
        /// <param name="isUnknown">isUnknowon</param>
        /// <param name="isActiveLead"></param>
        public int SaveContactUsInformation(string name, string email, string phone, bool isInternational, string fax, string comments,
            string city, int PracArea, int TicketId, string language, string salerepid, string callerId, bool isUnknown, bool isUnknownName, int leadTypeId) // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
        {
            try
            {
                string ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ipaddress))
                    ipaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                string[] keys =
                {
                    "@name", "@emailaddress", "@phone", "@IsInternational", "@fax", "@comments", "@siteid",
                    "@PracArea", "@TicketId", "@Language", "@IPAddress", "@SaleRepId", "@CallerId", "@isUnknown", "@isNameProvided", "@leadTypeId" };// Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . .  };
                object[] values = { name, email, phone, isInternational, fax, comments, city, PracArea, TicketId, language, ipaddress, salerepid,
                                    callerId, isUnknown, isUnknownName, leadTypeId }; // Rab Nawaz Khan 11473 10/21/2013 Added Parameter values. . . 
                int id = Convert.ToInt32(clsdbb.ExecuteScalerBySp("USP_PS_SAVE_CONTACTUS_INFORMATION", keys, values));
                return id;
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
                return 0;
            }
        }

        ///<summary>
        ///</summary>
        ///<param name="name"></param>
        ///<param name="phone"></param>
        ///<param name="email"></param>
        ///<param name="comments"></param>
        ///<param name="manufacturer"></param>
        ///<param name="pain"></param>
        ///<param name="contactType"></param>
        ///<param name="contactTypeId"></param>
        ///<param name="fax"></param>
        ///<param name="language"></param>
        public void SendEmailContactUsFromMaster(string name, string phone, string email, string comments, string manufacturer, string pain, string contactType, string contactTypeId, string fax, string language)
        {
            string content = "<br/><b>Name :</b> " + name;
            content += "<br/><b>Language :</b> " + language;
            content += "<br/><b>Phone :</b> " + FormatPhone(phone);
            if (!string.IsNullOrEmpty(fax))
                content += "<br/><b>Fax :</b> " + FormatPhone(fax);
            content += "<br/><b>Email Address :</b> " + email;
            if (contactTypeId == "7" || contactTypeId == "28" || contactTypeId == "29" || contactTypeId == "30" || contactTypeId == "31")
                content += "<br/><b>Hip Device :</b> " + manufacturer;
            else if (contactTypeId == "1")
                content += "<br/><b>Knee Device :</b> " + manufacturer;


            if (contactTypeId == "1" || contactTypeId == "7" || contactTypeId == "28" || contactTypeId == "29" || contactTypeId == "30" || contactTypeId == "31")
                content += "<br/><b>Level of pain :</b> " + pain;

            content += "<br/><b>Practice Area :</b> " + contactType;
            content += "<br/><b>Comments :</b> " + comments;

            string subject = contactType + " Lead - [" + name + "] - [" + DateTime.Now.ToShortDateString() + "] ";
            string body = "<br/><b>Sullo Law Add New Lead Form Response</b><br/>";
            body += content;
            string to = "";
            string from = Convert.ToString(ConfigurationManager.AppSettings["MailFrom"]);
            if (contactTypeId == "7" || contactTypeId == "28" || contactTypeId == "29" || contactTypeId == "30" || contactTypeId == "31")
                to = Convert.ToString(ConfigurationManager.AppSettings["DepuyMailTO"]);
            else if (contactTypeId == "1")
                to = Convert.ToString(ConfigurationManager.AppSettings["ZimmerMailTO"]);
            else
                to = BCC;

            SendMail(from, to, subject, body, "");
        }

        ///<summary>
        ///</summary>
        ///<param name="name"></param>
        ///<param name="phone"></param>
        ///<param name="email"></param>
        ///<param name="comments"></param>
        ///<param name="contactType"></param>
        ///<param name="contactTypeId"></param>
        ///<param name="choice1"></param>
        ///<param name="choice2"></param>
        ///<param name="choice3"></param>
        ///<param name="symptons"></param>
        ///<param name="language"></param>
        // Haris Ahmed 10292 05/22/2012 Change for Transvagianal Mesh and Pradaxa
        public void SendEmailContactUsFromMaster(string name, string phone, string email, string comments, string contactType, string contactTypeId, string choice1, string choice2, string choice3, string symptons, string language, string surgeryDate)
        {
            string content = "<br/><b>Name :</b> " + name;
            content += "<br/><b>Language :</b> " + language;
            content += "<br/><b>Phone :</b> " + FormatPhone(phone);
            content += "<br/><b>Email Address :</b> " + email;
            content += "<br/><b>Practice Area :</b> " + contactType;
            if (contactTypeId == "27")
            {
                content += "<br/><b>Have you or a loved one had an Internal Bleed or Brain Hemorrhage due to Pradaxa? :</b> " + choice1;
                content += "<br/><b>Has this Internal Bleed or Brain Hemorrhage cause the death of a loved one? :</b> " +
                           choice2;
            }
            else if (contactTypeId == "24")
            {
                content += "<br/><b>When was your surgery date? :</b> " + surgeryDate;
                content += "<br/><b>Was your surgery due to the following? :</b> " + symptons;
                content += "<br/><b>Did you receive a Bladder Sling? :</b> " + choice1;
                content += "<br/><b>How was your Mesh Implanted? :</b> " +
                           choice2;
            }
            else
            {
                content += "<br/><b>Have you been diagnosed with bladder cancer? :</b> " + choice1;
                content += "<br/><b>Have you ever take Actos, Actoplus Met, Actoplus Met XR, or Duetact? :</b> " +
                           choice2;
                content +=
                    "<br/><b>How long did you ever take Actos, Actoplus Met, Actoplus Met XR, and/or Duetact? :</b> " +
                    choice3;
                content += "<br/><b>Do you have any of the following symptoms? :</b> " + symptons;
            }
            content += "<br/><b>Comments :</b> " + comments;

            string subject = contactType + " Lead - [" + name + "] - [" + DateTime.Now.ToShortDateString() + "] ";
            string body = "<br/><b>Sullo Law Add New Lead Form Response</b><br/>";
            body += content;
            string to;
            string from = Convert.ToString(ConfigurationManager.AppSettings["MailFrom"]);
            to = BCC;

            SendMail(from, to, subject, body, "");
        }

        ///<summary>
        ///</summary>
        ///<param name="strContent"></param>
        ///<returns></returns>
        public string GetUpdatedContent_byGCLID(string strContent)
        {
            if (HttpContext.Current.Request.QueryString["kid"] != null &&
                   !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["kid"]))
            {
                HttpContext.Current.Session["kid"] = HttpContext.Current.Request.QueryString["kid"];
                DataTable dtKeyWords = GetKeyword(HttpContext.Current.Request.QueryString["kid"], "-", ".");
                if (dtKeyWords != null && dtKeyWords.Rows.Count > 0)
                {
                    strContent = Regex.Replace(strContent, "713-839-9026", dtKeyWords.Rows[0]["PhoneNumber"].ToString());
                    strContent = Regex.Replace(strContent, "713.839.9026", dtKeyWords.Rows[0]["PhoneNumber"].ToString());
                    strContent = Regex.Replace(strContent, "800.730.7607", dtKeyWords.Rows[0]["TollFree"].ToString());
                    strContent = Regex.Replace(strContent, "800-730-7607", dtKeyWords.Rows[0]["TollFree"].ToString());
                }

            }
            else if (HttpContext.Current.Session["kid"] != null && !string.IsNullOrEmpty(HttpContext.Current.Session["kid"].ToString()))
            {
                DataTable dtKeyWords = GetKeyword(HttpContext.Current.Session["kid"].ToString(), "-", ".");

                if (dtKeyWords != null && dtKeyWords.Rows.Count > 0)
                {
                    strContent = Regex.Replace(strContent, "713-839-9026", dtKeyWords.Rows[0]["PhoneNumber"].ToString());
                    strContent = Regex.Replace(strContent, "713.839.9026", dtKeyWords.Rows[0]["PhoneNumber"].ToString());
                    strContent = Regex.Replace(strContent, "800.730.7607", dtKeyWords.Rows[0]["TollFree"].ToString());
                    strContent = Regex.Replace(strContent, "800-730-7607", dtKeyWords.Rows[0]["TollFree"].ToString());
                }
            }

            return strContent;
        }

        // Format Phone Number like 021-4414-15454544
        /// <summary>
        /// This method is used to formation telephone number
        /// </summary>
        /// <param name="phone">Simple Telephone Number (021441415454544) </param>
        /// <returns>Return Formatted Telephone Number (021-255-5655-X(5555))</returns>
        /// <example>
        /// <code>
        /// string FormatedPhone = GeneralMethods.FormatPhone("1234567989");
        /// </code>
        /// </example>
        public static string FormatPhone(string phone)
        {
            try
            {
                string clcode = phone;
                if (clcode.Length == 10)
                {
                    string temp = clcode.Insert(0, "(");
                    temp = temp.Insert(4, ") ");
                    return temp.Insert(9, "-");
                }
                else if (clcode.Length > 10)
                {
                    if (phone.StartsWith("00") || phone.StartsWith("+"))
                    {
                        return phone;

                    }
                    else
                    {
                        string temp = clcode.Insert(0, "(");
                        temp = temp.Insert(4, ") ");
                        temp = temp.Insert(9, "-");
                        return temp.Insert(14, " X-");
                    }

                }

                return phone;
            }
            catch
            {
                return phone;
            }

        }

        public DataTable GetKeyword(string keyWord, string formatSeparator, string formatSeparatorMain)
        {
            DataSet dsKeyword;
            DataTable dtKeyword = null;

            string strPhone;
            try
            {


                if (!string.IsNullOrEmpty(keyWord))
                {
                    string[] key = { "@ID", "@Keyword", "@WebsiteID", "@AdvWebsite" };
                    object[] value = { 0, "", 0, 0 };



                    if (HttpContext.Current.Session["dtKeyWord"] == null)
                    {
                        dsKeyword = GetKeyWordList("USP_PS_Get_KeyWords", key, value);
                        if (dsKeyword != null && dsKeyword.Tables.Count > 0 && dsKeyword.Tables[0].Rows.Count > 0)
                        {
                            if (dsKeyword.Tables[0].Select("KeywordID='" + keyWord + "'").Length > 0)
                            {
                                dtKeyword = dsKeyword.Tables[0].Select("KeywordID='" + keyWord + "'").CopyToDataTable();
                                strPhone = dtKeyword.Rows[0]["PhoneNumber"].ToString();
                                dtKeyword.Rows[0]["PhoneNumber"] = FormatPhoneNumber(strPhone, formatSeparator);

                                strPhone = dtKeyword.Rows[0]["TollFree"].ToString();
                                if (!string.IsNullOrEmpty(formatSeparatorMain))
                                    dtKeyword.Rows[0]["TollFree"] = FormatPhoneNumber(strPhone, formatSeparatorMain);
                                dtKeyword.Rows[0]["TollFree"] = FormatPhoneNumber(strPhone, ".");
                                dtKeyword.AcceptChanges();
                                HttpContext.Current.Session["dtKeyWord"] = dsKeyword.Tables[0];
                                return dtKeyword;

                            }
                        }
                    }
                    else
                    {
                        dtKeyword = (DataTable)HttpContext.Current.Session["dtKeyWord"];

                        if (dtKeyword != null && dtKeyword.Rows.Count > 0)
                        {
                            if (dtKeyword.Select("KeywordID='" + keyWord + "'").Length > 0)
                            {
                                dtKeyword = dtKeyword.Select("KeywordID='" + keyWord + "'").CopyToDataTable();
                                strPhone = dtKeyword.Rows[0]["PhoneNumber"].ToString();
                                dtKeyword.Rows[0]["PhoneNumber"] = FormatPhoneNumber(strPhone, formatSeparator);
                                strPhone = dtKeyword.Rows[0]["TollFree"].ToString();
                                if (!string.IsNullOrEmpty(formatSeparatorMain))
                                    dtKeyword.Rows[0]["TollFree"] = FormatPhoneNumber(strPhone, formatSeparatorMain);
                                dtKeyword.Rows[0]["TollFree"] = FormatPhoneNumber(strPhone, ".");
                                dtKeyword.AcceptChanges();

                            }
                            else
                            {
                                return null;
                            }

                        }
                        return dtKeyword;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);

            }
            return dtKeyword;
        }

        ///<summary>
        ///</summary>
        ///<param name="strPhoneNumber"></param>
        ///<param name="separator"></param>
        ///<returns></returns>
        public string FormatPhoneNumber(string strPhoneNumber, string separator)
        {
            string strPhone = "";
            if (!string.IsNullOrEmpty(strPhoneNumber))
            {
                strPhone = string.Format("{0}" + separator + "{1}" + separator + "{2}",
                                         strPhoneNumber.Substring(0, 3), strPhoneNumber.Substring(3, 3),
                                         strPhoneNumber.Substring(6, 4));
            }
            return strPhone;
        }

        ///<summary>
        ///</summary>
        ///<param name="spName"></param>
        ///<param name="key"></param>
        ///<param name="value"></param>
        ///<returns></returns>
        public DataSet GetKeyWordList(string spName, string[] key, object[] value)
        {
            DataSet dsKeyword;
            dsKeyword = clsdbb.Get_DS_BySPArr(spName, key, value);
            return dsKeyword;
        }

        // Rab Nawaz Khan 11473 10/30/2013 Added to get the Clients submitted Leads. . . 
        /// <summary>
        /// This Method is used to get the clients submitted Leads
        /// </summary>
        /// <param name="ticketId">int TicketId</param>
        /// <returns>DataTable</returns>
        public DataTable GetLeadsAgainstClients(int ticketId)
        {
            try
            {
                string[] key = { "@ticketId" };
                object[] value = { ticketId };
                return ttdb.Get_DT_BySPArr("USP_HTP_GetLeadsHistory", key, value);
                
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
                return new DataTable();
            }
        }

        // Rab Nawaz Khan 11473 10/30/2013 Added to Update the is lead submitted flag true. . . 
        /// <summary>
        /// This method is used to update the flag for submitted leads to true against clients. 
        /// </summary>
        /// <param name="ticketId"></param>
        public void UpdateNewLeadSubmitted(int ticketId)
        {
            try
            {
                string[] key = { "@ticketId" };
                object[] value = { ticketId };
                ttdb.ExecuteSP("USP_HTP_UpdateLeadSubmittedFlag", key, value);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }
        }
    }
}

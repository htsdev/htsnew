using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components
{
    public class PendingClass
    {
        clsENationWebComponents clsDb = new clsENationWebComponents();
        clsLogger clog = new clsLogger();
        #region Variables

        private int ocrID = 0;
        private int picID = 0;
        private string firstname = String.Empty;
        private string lastname = String.Empty;
        private string address = String.Empty;
        private string city = String.Empty;
        private string state = String.Empty;
        private int zip = 0;
        private string causeno = String.Empty;
        private string status = String.Empty;
        private string newcourtdate = String.Empty;
        private string todaydate = String.Empty;
        private string time = String.Empty;
        private int courtno = 0;
        private string location = String.Empty;
        private DateTime updatedatescan;
        private DateTime courtdatescan;
        private DateTime _CourtDate;
        private DateTime _CourtDateMain;
        private int _checkStatus = 0;
        private int _employeeid = 0;
        #endregion 
        #region Properties
        public int EmployeeID
        {
            get
            {
                return _employeeid;
            }
            set
            {
                _employeeid = value;
            }
        }

        public int OCRID
        {
            get
            {
                return ocrID;
            }
            set
            {
                ocrID = value;
            }
        }
        public int PicID
        {
            get
            {
                return picID;
            }
            set
            {
                picID = value;
            }
        }
        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public int Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }
        public string CauseNo
        {
            get
            {
                return causeno;
            }
            set
            {
                causeno = value;
            }
        }
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public string NewCourtDate
        {
            get
            {
                return newcourtdate;
            }
            set
            {
                newcourtdate = value;
            }
        }
        public string TodayDate
        {
            get
            {
                return todaydate;
            }
            set
            {
                todaydate = value;
            }
        }
        public int CourtNo
        {
            get
            {
                return courtno;
            }
            set
            {
                courtno = value;
            }
        }
        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }
        public string Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }
        public DateTime ScanUpdateDate
        {
            get
            {
                return updatedatescan;
            }
            set
            {
                updatedatescan = value;
            }
        }
        public DateTime CourtDateScan
        {
            get
            {
                return courtdatescan;
            }
            set
            {
                courtdatescan = value;
            }
        }
        public DateTime CourtDate
        {
            get
            {
                return _CourtDate;
            }
            set
            {
                _CourtDate = value;
            }
        }
        public DateTime CourtDateMain
        {
            get
            {
                return _CourtDateMain;
            }
            set
            {
                _CourtDateMain = value;
            }
        }
        public int CheckStatus
        {
            get
            {
                return _checkStatus;
            }
            set
            {
                _checkStatus = value;
            }
        }
        #endregion 
        #region Methods
        public void InsertPendingOCR()
        {
            try
            {
                string[] key ={ "@CauseNo","@CourtDate","@CourtTime","@CourtNo","@Status","" };
                object[] value1 ={ PicID, CheckStatus };
                clsDb.InsertBySPArr("usp_WebScan_InsertPending", key, value1);
            }
            catch (Exception ex)
            {
            }
        }
        public int ComparisonInsert()
        {
            int i = 0;
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@CourtDate", "@CourtTime", "@CourtNo", "@Status", "@Location", "@CourtDateScan", "@Msg" };
                object[] value1 ={ PicID, CauseNo,CourtDateMain,Time,CourtNo,Status,Location,CourtDateScan ,0};
                i=(int)clsDb.InsertBySPArrRet("usp_webscan_ComparisonWithPending", key, value1);
                return i;
            }
            catch (Exception ex)
            {
                return i;
            }
        }
        public int Verify()
        {
            int i = 0;
            try
            {
                string[] key ={ "@PicID", "@CauseNo", "@CourtDate", "@CourtTime", "@CourtNo", "@Status", "@Location", "@CourtDateScan", "@EmployeeID", "@Msg" };
                object[] value1 ={ PicID, CauseNo, CourtDateMain, Time, CourtNo, Status, Location, CourtDateScan ,EmployeeID,0};
                i=(int)clsDb.InsertBySPArrRet("usp_webscan_verified", key, value1);
                return i;
            }
            catch (Exception ex)
            {
                return i;
            }
        }

        //Added by Ozair on 02/29/2008 to restrict trial letter for sending to the batch if the case has no letter flag..
        public bool NoLetterFlagCheck(int TicketID)
        {
            try
            {
                string[] keys = { "@ticketid" };
                object[] values = { TicketID };
                DataSet DS = clsDb.Get_DS_BySPArr("usp_hts_check_NoLetter", keys, values);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    int count = Convert.ToInt32(DS.Tables[0].Rows[0]["flagcount"]);
                    if (count > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return true;

            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return true;
            }
        }
        //

        //Added by Ozair for Task 2630 on 01/19/2008
        public void SendMailOscar(int ticketid, string filename, System.Web.SessionState.HttpSessionState session, System.Web.HttpResponse response)
        {
            try
            {
                DataSet ds;

                string RptSetting = "123456";

                String[] reportname = new String[] { "Notes.rpt", "Matters.rpt" };

                string[] keyssub = { "@TicketID" };
                object[] valuesub = { ticketid };
                ds = clsDb.Get_DS_BySPArr("USP_HTS_GET_SubReports", keyssub, valuesub);

                lntechNew.Components.clsCrsytalComponent Cr = new lntechNew.Components.clsCrsytalComponent();
                string path = ConfigurationSettings.AppSettings["NTPATHCaseSummary"].ToString();
                string[] key = { "@TicketId_pk", "@Sections" };
                object[] value1 = { ticketid, RptSetting };
                string name = ticketid.ToString() + "-CoverSheet-" + DateTime.Now.ToFileTime() + ".pdf";
                Cr.CreateSubReports(filename, "usp_hts_get_casesummary_report_data", key, value1, reportname, ds, 1, session, response, path, ticketid.ToString(), false, name);
                
                //Kazim 2866 2/7/2008 replace the key "osemailto" with "ocupdateemailto"  
                
                string toUser = (string)ConfigurationSettings.AppSettings["OcUpdateEmailTo"];
                string ccUser = (string)ConfigurationSettings.AppSettings["OsEmailCC"];
                lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.", "An Oscar client updated.", Convert.ToString(path + name), toUser, ccUser);
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message.ToString();
            }
        }

        //end

        /// <summary>
        /// ozair 4400 07/12/2008
        /// this method will check for HCJP 5-1 court against cause number(s)
        /// and return 1 if found else 0
        /// </summary>
        /// <returns>1 or 0</returns>
        public int CheckForHCJP51()
        {
            int i = 0;
            try
            {
                string[] key = { "@CauseNo", "@Msg" };
                object[] value1 = { CauseNo, 0 };
                i = (int)clsDb.InsertBySPArrRet("usp_webscan_CheckForHCJP51", key, value1);
                return i;
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return i;
            }
        }

        #endregion

    }
}

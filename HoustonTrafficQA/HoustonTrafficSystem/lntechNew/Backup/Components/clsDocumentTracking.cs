using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.IO;

namespace HTP.Components
{
    /// <summary>
    /// This Class is used for Document Tracking Application
    /// </summary>
    public class clsDocumentTracking
    {
        #region Variables
        
        private int documentBatchID = 0;
        private int scanBatchID = 0;
        private int scanBatchDetailID = 0;
        private int documentBatchPageCount = 0;
        private int documentBatchPageNo = 0;
        private int empID = 0;
        private DateTime startDate = DateTime.Now;
        private DateTime endDate = DateTime.Now;
        private int checkStatus = 1;
        private DateTime batchDate = DateTime.Now;
        private int documentID = 0;
        private string ticketIDs = String.Empty;
        private DateTime scanBatchDate = DateTime.Now;
        private string ocrData = String.Empty;
        private DateTime verifiedDate = DateTime.Now;
                
        clsENationWebComponents clsDB = new clsENationWebComponents();

        #endregion

        #region Properties

        /// <summary>
        /// will be used to get and set the printed letter/document id
        /// </summary>
        /// <value>
        /// document batch id 
        /// </value>
        public int DocumentBatchID
        {
            get
            {
                return documentBatchID;
            }
            set 
            {
                documentBatchID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the scanned batch id
        /// </summary>
        /// <value>
        /// batch id of scanned documents
        /// </value>
        public int ScanBatchID
        {
            get
            {
                return scanBatchID;
            }
            set
            {
                scanBatchID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the detail scanned batch id
        /// </summary>
        /// <value>
        /// detail batch id of scanned documents
        /// </value>
        public int ScanBatchDetailID
        {
            get
            {
                return scanBatchDetailID;
            }
            set
            {
                scanBatchDetailID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the page count of the scanned document
        /// of that bacth
        /// </summary>
        /// <value>
        /// page count of scanned documents
        /// </value>
        public int DocumentBatchPageCount
        {
            get
            {
                return documentBatchPageCount;
            }
            set
            {
                documentBatchPageCount = value;
            }
        }

        /// <summary>
        /// will be used to get and set the page no of the scanned document
        /// of that bacth
        /// </summary>
        /// <value>
        /// page no of scanned document
        /// </value>
        public int DocumentBatchPageNo
        {
            get
            {
                return documentBatchPageNo;
            }
            set
            {
                documentBatchPageNo = value;
            }
        }

        /// <summary>
        /// will be used to set and get Employee ID of current logged in user
        /// </summary>
        /// <value>
        /// Employee ID of current logged in user
        /// </value>
        public int EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        /// <summary>
        /// will be used to set and get Start Date for searching scanned batch
        /// </summary>
        /// <value>
        /// start/from date to search
        /// </value>
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        /// <summary>
        /// will be used to set and get End Date for searching scanned batch
        /// </summary>
        /// <value>
        /// end/to date to search
        /// </value>
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        /// <summary>
        /// will be used to get and set the check if all non-verified batches were displayed or not
        /// </summary>
        /// <value>
        /// either 0 or 1. 1: means search all non verified.
        /// </value>
        public int CheckStatus
        {
            get
            {
                return checkStatus;
            }
            set
            {
                checkStatus = value;
            }
        }

        /// <summary>
        /// will be used to set and get Batch Date of the printed document/batch
        /// </summary>
        /// <value>
        /// batch date of the printed document
        /// </value>
        public DateTime BatchDate
        {
            get
            {
                return batchDate;
            }
            set
            {
                batchDate = value;
            }
        }

        /// <summary>
        /// will be used to set and get the document id of the printed document
        /// </summary>
        /// <value>
        /// id of document from document tracking documents table 
        /// </value>
        public int DocumentID
        {
            get
            {
                return documentID;
            }
            set
            {
                documentID = value;
            }
        }

        /// <summary>
        /// will be used to get and set the ticket id(s)
        /// </summary>
        /// <value>
        /// ticket id(s) with a leading ","
        /// </value>
        public string TicketIDs
        {
            get
            {
                return ticketIDs;
            }
            set
            {
                ticketIDs = value;
            }
        }

        /// <summary>
        /// will be used to get an set the batch scan date
        /// </summary>
        /// <value>
        /// batch scan date
        /// </value>
        public DateTime ScanBatchDate
        {
            get
            {
                return scanBatchDate;
            }
            set
            {
                scanBatchDate = value;
            }
        }

        /// <summary>
        /// will be used to get and set the data that has been fetched after ocr process
        /// </summary>
        /// <value>
        /// data that has been fetched after ocr process
        /// </value>
        public string OcrData
        {
            get
            {
                return ocrData;
            }
            set
            {
                ocrData = value;
            }
        }

        /// <summary>
        /// will be used to get and set the date at which bacth has been verified 
        /// </summary>
        /// <value>
        /// Verified date
        /// </value>
        public DateTime VerifiedDate
        {
            get
            {
                return verifiedDate;
            }
            set
            {
                verifiedDate = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// gets the details of a document which is scanned and processed by Document Tracking
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.DocumentBatchID = Convert.ToInt32(ViewState["DBID"]);
        /// DataSet ds_LetterDetail = clsDT.GetLetterDetail();
        /// </code>
        /// </example>
        public DataSet GetLetterDetail()
        {
            string[] key = { "@DocumentBatchID", "@ScanBatchID" };
            object[] value1 = { DocumentBatchID, ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_LetterDetail", key, value1);            
        }
        
        /// <summary>
        /// gets the batch detail by document/report batch id
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.DocumentBatchID = Convert.ToInt32(ViewState["DBID"]);
        /// clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
        /// DataSet ds_ScanMissing = clsDT.GetScanMissing();
        /// </code>
        /// </example>
        public DataSet GetScanMissing()
        {
            string[] key = { "@DocumentBatchID", "@ScanBatchID" };
            object[] value1 = { DocumentBatchID, ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_ScanMissing", key, value1);            
        }

        /// <summary>
        /// gets the Scan batch detail for a perticular document id
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.DocumentBatchID = Convert.ToInt32(ViewState["DBID"]);
        /// DataSet ds_ViewBatchLetters = clsDT.GetViewBatchLetters();
        /// </code>
        /// </example>
        public DataSet GetViewBatchLetters()
        {
            string[] key = { "@DocumentBatchID", "@ScanBatchID" };
            object[] value1 ={ DocumentBatchID, ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_ViewBatchLetters", key, value1);
        }

        /// <summary>
        /// fetchs the details of a perticular verified scanned batch document
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.DocumentBatchID = Convert.ToInt32(ViewState["DBID"]);
        /// DataSet DS_MoveFirst = clsDT.GetViewBatchLettersDetail();
        /// </code>
        /// </example>
        public DataSet GetViewBatchLettersDetail()
        {
            string[] key = { "@DocumentBatchID", "@ScanBatchID" };
            object[] value1 = { DocumentBatchID, ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_ViewBatchLetters_Detail", key, value1);
        }

        /// <summary>
        /// gets the details of a scanned batch
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
        /// DataSet ds_UpdateBatch = clsDT.GetUpdateBatch();
        /// </code>
        /// </example>
        public DataSet GetUpdateBatch()
        {
            string[] key ={ "@ScanBatchID" };
            object[] value1 ={ ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_UpdateBatch", key, value1);
        }

        /// <summary>
        /// fetchs the details of a perticular non verified scanned batch document
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code> 
        /// clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
        /// DataSet DS_MoveFirst = clsDT.GetUpdateBatchDetail();
        /// </code>
        /// </example>
        public DataSet GetUpdateBatchDetail()
        {
            string[] key ={ "@ScanBatchID" };
            object[] value1 ={ ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_UpdateBatch_Detail", key, value1);
        }

        /// <summary>
        /// updates the information entered from Update Batch page (No Letter ID)
        /// this information is entered by User
        /// </summary>
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
        /// clsDT.ScanBatchDetailID = Convert.ToInt32(ViewState["SBDID"]);
        /// clsDT.DocumentBatchID = Convert.ToInt32(txt_LetterID.Text.Trim());
        /// clsDT.DocumentBatchPageCount = Convert.ToInt32(txt_PageCount.Text.Trim());
        /// clsDT.DocumentBatchPageNo = Convert.ToInt32(txt_PageNo.Text.Trim());
        /// clsDT.EmpID = Convert.ToInt32(clsSessn.GetCookie("sEmpID", this.Request));
        /// clsDT.UpdateUpdateBatch();
        /// </code>
        /// </example>
        public void UpdateUpdateBatch()
        {
            string[] keys ={ "@ScanBatchDetailID", "@DocumentBatchID", "@DocumentBatchPageCount", "@DocumentBatchPageNo", "@EmployeeID" };
            object[] values ={ ScanBatchDetailID, DocumentBatchID, DocumentBatchPageCount, DocumentBatchPageNo, EmpID };

            clsDB.ExecuteSP("usp_HTP_Update_DocumentTracking_UpdateBatch", keys, values);
        }

        /// <summary>
        /// gets the Scan batch detail
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = Convert.ToInt32(ViewState["SBID"]);
        /// DataSet DS_GetBatch = clsDT.GetBatchDetail();
        /// </code>
        /// </example>
        public DataSet GetBatchDetail()
        {
            string[] key ={ "@ScanBatchID" };
            object[] value1 ={ ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_BatchDetail", key, value1);
        }

        /// <summary>
        /// gets all the Scan batches detail
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code> 
        /// clsDT.CheckStatus = 0;
        /// clsDT.StartDate = cal_StartDate.SelectedDate;
        /// clsDT.EndDate = cal_EndDate.SelectedDate;
        /// DataSet DS_GetBatch = clsDT.GetSearchBatch();
        /// </code>
        /// </example>
        public DataSet GetSearchBatch()
        {
            string[] key ={ "@StartDate", "@EndDate", "@CheckStatus" };
            object[] value1 ={ StartDate, EndDate, CheckStatus };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_SearchBatch", key, value1);
        }

        /// <summary>
        /// Inserts the information in tbl_htp_documenttracking_batch table and
        /// gets the inserted id and then inserts the (cases) tickets id for that report against that batch.
        /// it also returns the inserted id
        /// </summary>
        /// <returns>int</returns>
        /// <example>
        /// <code>
        /// clsDT.BatchDate = DateTime.Now;
        /// clsDT.DocumentID = 1; // 1: Bonds
        /// clsDT.TicketIDs = ticketno.ToString() + ",";
        /// clsDT.EmpID = empid;
        /// documentBatchID = clsDT.GetDBIDByInsertingBatchWithDetail();
        /// </code>
        /// </example>
        public int GetDBIDByInsertingBatchWithDetail()
        {
            string[] key = { "@BatchDate", "@DocumentID_FK", "@TicketIDs", "@EmployeeID" };
            object[] value1 = { BatchDate, DocumentID, TicketIDs, EmpID };

            DataSet ds = clsDB.Get_DS_BySPArr("usp_HTP_Insert_DocumentTracking_BacthWithDetail", key, value1);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0]["DocumentBatchID"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// sorts the files in assending order by date
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>string[]</returns>
        /// <example>
        /// <code>
        /// string[] filename = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpath);
        /// if (filename.Length > 1)
        /// {
        ///     filename = clsDT.SortFilesByDate(filename);
        /// }
        /// </code>
        /// </example>
        public string[] SortFilesByDate(string[] filename)
        {
            for (int i = 0; i < filename.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(filename[i]);
                for (int j = i + 1; j < filename.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(filename[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = filename[j];
                        filename[j] = filename[i];
                        filename[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return filename;
        }

        /// <summary>
        /// Inserts the information in tbl_htp_documenttracking_scanbatch table and
        /// returns the inserted id back to the calling method
        /// </summary>
        /// <returns>int</returns>
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = scanBatchID;
        /// clsDT.DocumentBatchID = 0;
        /// clsDT.DocumentBatchPageCount = 0;
        /// clsDT.DocumentBatchPageNo = 0;
        /// scanBatchDetailID = clsDT.GetSBDIDByInsertingScanBatchDetail();
        /// </code>
        /// </example>
        public int GetSBIDByInsertingScanBatch()
        {
            string[] key = { "@ScanBatchDate", "@EmployeeID" };
            object[] value1 = { ScanBatchDate, EmpID };

            DataSet ds = clsDB.Get_DS_BySPArr("usp_HTP_Insert_DocumentTracking_ScanBatch", key, value1);
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0]["ScanBatchID"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Inserts the information in tbl_htp_documenttracking_scanbatch_detail table and 
        /// returns the inserted id back to the calling method
        /// </summary>
        /// <returns>int</returns>
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = scanBatchID;
        /// clsDT.DocumentBatchID = 0;
        /// clsDT.DocumentBatchPageCount = 0;
        /// clsDT.DocumentBatchPageNo = 0;
        /// scanBatchDetailID = clsDT.GetSBDIDByInsertingScanBatchDetail();
        /// </code>
        /// </example>
        public int GetSBDIDByInsertingScanBatchDetail()
        {
            string[] key = { "@ScanBatchID_FK", "@DocumentBatchID", "@DocumentBatchPageCount", "@DocumentBatchPageNo", "@EmployeeID" };
            object[] value1 = { ScanBatchID, DocumentBatchID, DocumentBatchPageCount, DocumentBatchPageNo, EmpID };

            DataSet ds = clsDB.Get_DS_BySPArr("usp_HTP_Insert_DocumentTracking_ScanBatch_Detail", key, value1);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0]["ScanBatchDetailID"]);
            }
            else
            {
                return 0;
            }

        }

        /// <summary>
        /// gets the Scan batch detail id by Scan Batch ID
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = SBID;
        /// DataSet ds = clsDT.GetScanDocByScanBatchID();
        /// </code>
        /// </example>
        public DataSet GetScanDocByScanBatchID()
        {
            string[] key = { "@ScanBatchID_FK"};
            object[] value1 = { ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_ScanBatchDetailID", key, value1);
        }

        /// <summary>
        /// updates the information retrived after performing OCR
        /// </summary>
        /// <example>
        /// <code>
        /// clsDT.DocumentBatchID = documentBatchID;
        /// clsDT.DocumentBatchPageCount = pageCount;
        /// clsDT.DocumentBatchPageNo = pageNo;
        /// clsDT.ScanBatchDetailID = scanBatchDetailID;
        /// clsDT.OcrData = ocr_data;
        /// clsDT.UpdateScanBatchDetail();        
        /// </code>
        /// </example>
        public void UpdateScanBatchDetail()
        {
            string[] keys = { "@ScanBatchDetailID", "@DocumentBatchID", "@DocumentBatchPageCount", "@DocumentBatchPageNo", "@OCRData"};
            object[] values = { ScanBatchDetailID, DocumentBatchID, DocumentBatchPageCount, DocumentBatchPageNo, OcrData };

            clsDB.ExecuteSP("usp_HTP_Update_DocumentTracking_ScanBatch_Detail", keys, values);
        }

        /// <summary>
        /// updates the verification information the picture name stored in docstorage
        /// </summary>  
        /// <example>
        /// <code>
        /// clsDT.ScanBatchID = scanBatchID;
        /// clsDT.DocumentBatchID = documentBatchID;
        /// clsDT.VerifiedDate = DateTime.Now;
        /// clsDT.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
        /// clsDT.UpdateVerifyBatchDetail();
        /// </code>
        /// </example>
        public void UpdateVerifyBatchDetail()
        {
            string[] keys = { "@ScanBatchID", "@DocumentBatchID", "@VerfiedDate", "@EmployeeID" };
            object[] values = { ScanBatchID, DocumentBatchID, VerifiedDate, EmpID };

            clsDB.ExecuteSP("usp_HTP_Update_DocumentTracking_VeriyBatchDetail", keys, values);
        }

        /// <summary>
        /// fetchs the scan batch details ids against a scan batch to delete pictures
        /// </summary>
        /// <returns>DataSet</returns>
        /// <example>
        /// <code>     
        /// clsDT.ScanBatchID = SBID;
        /// DataSet ds_DeletePics = clsDT.GetBatchForDelete();
        /// </code>
        /// </example>
        public DataSet GetBatchForDelete()
        {
            string[] keys = { "@ScanBatchID" };
            object[] values = { ScanBatchID };

            return clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_DeletePics", keys, values);
        }

        /// <summary>
        /// deletes the scan batch along with it's details
        /// </summary>
        /// <example>
        /// <code> 
        /// clsDT.ScanBatchID = SBID;
        /// clsDT.DeleteBatch();
        /// </code>
        /// </example>
        public void DeleteBatch()
        {
            string[] keys = { "@ScanBatchID"};
            object[] values = { ScanBatchID };

            clsDB.ExecuteSP("usp_HTP_Delete_DocumentTracking_DeleteBatch", keys, values);
        }

        /// <summary>
        /// ozair 4073 05/21/2008
        /// Inserts the information in tbl_htp_documenttracking_batch table and
        /// returns the inserted id 
        /// </summary>
        /// <returns>int</returns>
        /// <example>
        /// <code>
        /// clsDT.BatchDate = DateTime.Now;
        /// clsDT.DocumentID = 3; // 3: Bond Settings
        /// clsDT.EmpID = empid;
        /// documentBatchID = clsDT.GetDBIDByInsertingBatchWithDetail();
        /// </code>
        /// </example>
        public int GetDBIDByInsertingBatch()
        {
            string[] key = { "@BatchDate", "@DocumentID_FK", "@EmployeeID" };
            object[] value1 = { BatchDate, DocumentID, EmpID };

            DataSet ds = clsDB.Get_DS_BySPArr("usp_HTP_Insert_DocumentTracking_Bacth", key, value1);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0]["DocumentBatchID"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// fetchs the scan batch details ids against a scan batch to delete pictures
        /// </summary>
        /// <returns>int</returns>
        /// <example>
        /// <code>     
        /// clsDT.DocumentBatchID = documentBatchID;
        /// int isAlreadyVerified = clsDT.CheckIsAlreadyVerified();
        /// </code>
        /// </example>
        public int CheckIsAlreadyVerified()
        {
            string[] keys = { "@DocumentBatchID" };
            object[] values = { DocumentBatchID };

            DataSet ds = clsDB.Get_DS_BySPArr("usp_HTP_Get_DocumentTracking_IsAlreadyVerified", keys, values);
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0]["IsAlreadyVerified"]);
            }
            else
            {
                return 0;
            }
        }
        #endregion
    }
}

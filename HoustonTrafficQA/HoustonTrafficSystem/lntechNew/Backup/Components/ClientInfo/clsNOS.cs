using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Data.SqlClient;
using System.Data.Common;

namespace lntechNew.Components.ClientInfo
{
    public class clsNOS
    {
        /// <summary> 
        ///  This component is created for Not On System Process.all not on system business logic is under that component
        ///  description for every methid is given below ( Fahad - 1/4/2008 )
        /// </summary>
        /// 


        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsLogger clog = new clsLogger();

        /// <summary>
        /// This Method Will Validate Cause Number 
        /// 1) Do Not Allow user to process payment if we dont have cause number unless user select NOS flag
        /// 2) Cause numbet should be greater that or is equal to 12 digits
        /// 3) Cause number should starts with TR or CR
        /// ( Fahad - 1/4/2008 )
        /// </summary>
        /// <param name="ticketid"> Ticketid</param>
        /// <returns></returns>
        public bool CheckNOS(int ticketid)
        {
            if (ticketid <= 0)
                return false;
            string flagstatus = "NONE";
            string[] keys = { "@ticketid" };
            object[] values = { Convert.ToInt32(ticketid) };
            DataTable dt = clsdb.Get_DT_BySPArr("USP_HTS_CHECK_NOS", keys, values);
            if (dt.Rows.Count == 0)
                return false;
            try
            {
                flagstatus = Convert.ToString(dt.Rows[0]["flagstatus"]).ToUpper();
                if (flagstatus == "TRUE")
                    return true;
                foreach (DataRow dRow in dt.Rows)
                {
                    // ozair 3417 on 03/12/2008 [cause no which starts with "BC" included]
                    if (!(((Convert.ToString(dRow["CauseNumber"]).ToUpper().StartsWith("TR")) || (Convert.ToString(dRow["CauseNumber"]).ToUpper().StartsWith("CR")) || (Convert.ToString(dRow["CauseNumber"]).ToUpper().StartsWith("BC"))) && (Convert.ToString(dRow["CauseNumber"]).Length >= 12)))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //Ozair 5938 06/02/2009 warning removed.
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Insert Followup date into tblticketsextension against ticketid 
        /// if followupdate is already exist then update the current followupdate
        /// else insert new followupdate into tblticketsextension - ( Fahad - 1/4/2008 )
        /// 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="ddl_nos"></param>
        public bool UpdateNOS(int ticketid, int empid)
        {
            if ((ticketid <= 0) || (empid <= 0))
            {
                return false;
            }
            try
            {
                DataSet ds = new DataSet();
                string[] keys = { "@ticketid_pk" };
                object[] values = { Convert.ToInt32(ticketid) };
                ds = clsdb.Get_DS_BySPArr("usp_SaveCaseExtentions", keys, values);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    // Sameeullah daris 8635 on 01/31/2011 Add in history Modified
                    clog.AddNote(empid, "Not On System Follow Up Date has changed from N/A to - " + Convert.ToString(ds.Tables[0].Rows[0]["NOSFollowupDate"]), "Not On System Follow Up Date has changed from N/A to - " + Convert.ToString(ds.Tables[0].Rows[0]["NOSFollowupDate"]), Convert.ToInt32(ticketid));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        public void GetFollowUp(DropDownList ddl_nos, int ticketid)
        {


            try
            {
                DataSet ds = new DataSet();
                string[] keys = { "@ticketid_pk" };
                object[] values = { Convert.ToInt32(ticketid) };
                ds = clsdb.Get_DS_BySPArr("usp_hts_get_followup", keys, values);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddl_nos.Items.Insert(0, new ListItem(Convert.ToString(ds.Tables[0].Rows[0]["NOSFollowupDate"])));
                    ddl_nos.DataBind();

                }


            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        /// <summary>
        /// Update Followup date from Report - ( Fahad - 1/4/2008 )
        /// </summary>
        /// <param name="ticketid"> Ticketid</param>
        /// <param name="empid">Employee ID</param>
        /// <param name="generalcomments">General Comments</param>
        /// <param name="followup">New Followup date</param>
        //Nasir 5938 05/25/2009 for change history notes
        public bool Save(int ticketid, int empid, string generalcomments, DateTime followupdate, string currentFollowUpDate)
        {
            try
            {
                if ((ticketid <= 0) || (empid <= 0))
                {

                    return false;
                }

                DataSet ds = new DataSet();
                DateTime testdatetime = new DateTime();
                string followup = Convert.ToString(followupdate);
                string[] keys = { "@TicketID_PK", "@Empid", "@comments", "@Followup" };
                object[] values = { ticketid, empid, generalcomments, followupdate };
                ds = clsdb.Get_DS_BySPArr("usp_update_followup", keys, values);


                //Nasir 5938 05/25/2009 for change history notes
                string notes = string.Empty;
                if (currentFollowUpDate.Length == 0)
                {
                    notes = "Not On System Follow Up Date has changed from N/A to " + followupdate.ToShortDateString();
                    clog.AddNote(empid, notes, notes, ticketid);
                }
                else
                {
                    DateTime oldContractFollowUpDate;
                    if (DateTime.TryParse(currentFollowUpDate, out oldContractFollowUpDate))
                    {
                        if (oldContractFollowUpDate.ToShortDateString() != followupdate.ToShortDateString())
                        {
                            notes = "Not On System Follow Up Date has changed from " + oldContractFollowUpDate.ToShortDateString() + " to " + followupdate.ToShortDateString();
                            clog.AddNote(empid, notes, notes, ticketid);
                        }


                    }
                }

                testdatetime = Convert.ToDateTime(ds.Tables[0].Rows[0]["NOSFollowUpDate"]);
                if (testdatetime.ToString("MM/dd/yyyy") == followupdate.ToString("MM/dd/yyyy"))
                {
                    return true;
                }
                else
                {

                    return false;
                }

            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

    }
}

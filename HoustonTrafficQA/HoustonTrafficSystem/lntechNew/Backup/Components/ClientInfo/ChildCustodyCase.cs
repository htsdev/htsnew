using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LNHelper;

namespace HTP.Components
{
    public class ChildCustodyCase
    {
        //Zeeshan Ahmed 4152 06/02/2008 Add Class For Child Custody Report
        DBComponent Clsdb = new DBComponent("Civil");


        /// <summary>
        /// This Function Is Used To Get Case Type
        /// </summary>
        /// <returns>Return Case Type</returns>
        public DataTable GetCaseType()
        {
            return Clsdb.GetDTBySPArr("USP_CC_GET_CaseType");
        }

        /// <summary>
        /// This Function Is Used To Get Setting Type
        /// </summary>
        /// <returns>Return Setting Type</returns>
        public DataTable GetSettingType()
        {
            return Clsdb.GetDTBySPArr("USP_CC_GET_SettingType");
        }

        /// <summary>
        /// This Function Is Used To Get Child Custody Courts
        /// </summary>
        /// <returns>Return Child Custody Courts</returns>
        public DataTable GetChildCustodyCourt()
        {
            return Clsdb.GetDTBySPArr("USP_CC_GET_Court");
        }

        /// <summary>
        /// This Function Is Used To Get Child Custody Cases
        /// </summary>
        /// <returns>Return Child Custody Cases</returns>
        public DataTable GetChildCustodyStatus()
        {
            return Clsdb.GetDTBySPArr("USP_CC_GET_CaseStatus");
        }


        /// <summary>
        /// This Procedure is used to get Child Custody Report Data
        /// </summary>
        /// <param name="CourtId">Court ID</param>
        /// <param name="SettingType">Setting Type</param>
        /// <param name="CaseStatus">Case Status</param>
        /// <param name="DocketDateFrom">Docket Date From</param>
        /// <param name="DocketDateTo">Docket Date To</param>
        /// <param name="LoadDateFrom">Load Date From</param>
        /// <param name="LoadDateTo">Load Date To</param>
        /// <returns>Return Child Custody Cases matching the input criteria</returns>
        public DataTable GetChildCustodyReports(int CourtId, int SettingType, int CaseStatus, string DocketDateFrom, string DocketDateTo, string LoadDateFrom, string LoadDateTo)
        {
            string[] keys = { "@CourtId", "@SettingType", "@CaseStatus", "@DocketDateFrom", "@DocketDateTo", "@LoadDateFrom", "@LoadDateTo" };
            object[] values = { CourtId, SettingType, CaseStatus, DocketDateFrom, DocketDateTo, LoadDateFrom, LoadDateTo };
            return Clsdb.GetDTBySPArr("USP_CC_ChildCustody_Report", keys, values);
        }


        /// <summary>
        /// This function is used to get child custody case detail
        /// </summary>
        /// <param name="ChildCustodyId">Case ID</param>
        /// <returns>Return case information</returns>
        public DataTable GetChildCustodyCaseInformation(string ChildCustodyId)
        {
            return Clsdb.GetDTBySPByOneParmameter("USP_CC_GET_ChildCustody_Case_Detail", "@ChildCustodyID", ChildCustodyId);
        }

        /// <summary>
        /// This Function is used to get Child Custody Case Parties
        /// </summary>
        /// <param name="ChildCustodyId">Case ID</param>
        /// <returns>Return Child Custody Parties</returns>
        public DataTable GetChildCustodyPartyInformation(string ChildCustodyId)
        {
            return Clsdb.GetDTBySPByOneParmameter("USP_CC_GET_ChildCustodies_Parties_Detail", "@ChildCustodyID", ChildCustodyId);
        }



    }
}

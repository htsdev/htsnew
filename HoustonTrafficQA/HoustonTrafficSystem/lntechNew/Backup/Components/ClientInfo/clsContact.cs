﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Components.ClientInfo
{
    //Created Waqas 5771 04/20/2009
    public class clsContact
    {
        #region Variables

        private int contactID = 0;
        private string firstName = String.Empty;
        private string middleName = String.Empty;
        private string midNumber = String.Empty;
        private string lastName = String.Empty;
        private string dlNumber = String.Empty;
        private string dlState = String.Empty;
        private string cdlFlag = String.Empty;
        private string dob = String.Empty;
        private string address = String.Empty;
        private string email = String.Empty;
        private string language = String.Empty;
        private string race = String.Empty;
        private string gender = String.Empty;
        private string height = String.Empty;
        private string weight = String.Empty;
        private string hairColor = String.Empty;
        private string eyes = String.Empty;
        private string phone1 = String.Empty;
        private string phoneType1 = String.Empty;
        private string phone2 = String.Empty;
        private string phoneType2 = String.Empty;
        private string phone3 = String.Empty;
        private string phoneType3 = String.Empty;

        private int noDL = 0;
        private int dlStateID = 0;
        private int stateID_FK = 0;
        private int cdlFlagID = 0;
        private int phoneTypeID1 = 0;
        private int phoneTypeID2 = 0;
        private int phoneTypeID3 = 0;
        private string address1 = String.Empty;
        private string address2 = String.Empty;
        private string city = String.Empty;
        private string zip = String.Empty;

        #endregion

        #region Properties

        public int ContactID
        {
            get
            {
                return contactID;
            }
            set
            {
                contactID = value;
            }
        }
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }
        public string MiddleName
        {
            get
            {
                return middleName;
            }
            set
            {
                middleName = value;
            }
        }
        public string MidNumber
        {
            get
            {
                return midNumber;
            }
            set
            {
                midNumber = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string DLNumber
        {
            get
            {
                return dlNumber;
            }
            set
            {
                dlNumber = value;
            }
        }
        public string DLState
        {
            get
            {
                return dlState;
            }
            set
            {
                dlState = value;
            }
        }
        public string CDLFlag
        {
            get
            {
                return cdlFlag;
            }
            set
            {
                cdlFlag = value;
            }
        }
        public string DOB
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public string Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
            }
        }
        public string Race
        {
            get
            {
                return race;
            }
            set
            {
                race = value;
            }
        }
        public string Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }
        public string Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }
        public string Weight
        {
            get
            {
                return weight;
            }
            set
            {
                weight = value;
            }
        }
        public string HairColor
        {
            get
            {
                return hairColor;
            }
            set
            {
                hairColor = value;
            }
        }
        public string Eyes
        {
            get
            {
                return eyes;
            }
            set
            {
                eyes = value;
            }
        }
        public string Phone1
        {
            get
            {
                return phone1;
            }
            set
            {
                phone1 = value;
            }
        }
        public string PhoneType1
        {
            get
            {
                return phoneType1;
            }
            set
            {
                phoneType1 = value;
            }
        }
        public string Phone2
        {
            get
            {
                return phone2;
            }
            set
            {
                phone2 = value;
            }
        }
        public string PhoneType2
        {
            get
            {
                return phoneType2;
            }
            set
            {
                phoneType2 = value;
            }
        }
        public string Phone3
        {
            get
            {
                return phone3;
            }
            set
            {
                phone3 = value;
            }
        }
        public string PhoneType3
        {
            get
            {
                return phoneType3;
            }
            set
            {
                phoneType3 = value;
            }
        }

        public int NoDL
        {
            get
            {
                return noDL;
            }
            set
            {
                noDL = value;
            }
        }
        public int DLStateID
        {
            get
            {
                return dlStateID;
            }
            set
            {
                dlStateID = value;
            }
        }
        public int StateID_FK
        {
            get
            {
                return stateID_FK;
            }
            set
            {
                stateID_FK = value;
            }
        }
        public int CDLFlagID
        {
            get
            {
                return cdlFlagID;
            }
            set
            {
                cdlFlagID = value;
            }
        }
        public int PhoneTypeID1
        {
            get
            {
                return phoneTypeID1;
            }
            set
            {
                phoneTypeID1 = value;
            }
        }
        public int PhoneTypeID2
        {
            get
            {
                return phoneTypeID2;
            }
            set
            {
                phoneTypeID2 = value;
            }
        }
        public int PhoneTypeID3
        {
            get
            {
                return phoneTypeID3;
            }
            set
            {
                phoneTypeID3 = value;
            }
        }
        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
            }
        }
        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        public string Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }



        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        clsCase ClsCase = new clsCase();
        #endregion

        #region Methods
        //Nasir 5771 04/14/2009
        /// <summary>
        /// Use to search contact against last name first name and Date of birth
        /// </summary>
        /// <param name="LastName"></param>
        /// <param name="FirstInitial"></param>
        /// <param name="DOB"></param>
        /// <returns>Contacts</returns>
        public DataTable GetContactLookUp(string LastName, string FirstInitial, DateTime DOB)
        {
            string[] key_1 = { "@LastName", "@FirstInitial", "@DOB" };
            object[] value_1 = { LastName, FirstInitial, DOB };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_GET_ContactLookUp", key_1, value_1);
            return ds.Tables[0];
        }

        //Nasir 5771 04/14/2009
        /// <summary>
        /// use to associate(insert) contact(ID) against ticketID
        /// </summary>
        /// <param name="ContactID"></param>
        /// <param name="TicketID"></param>
        /// <param name="EmpID"></param>
        /// <returns>boolean</returns>
        public bool AssociateContactID(int ContactID, int TicketID, int EmpID)
        {


            string[] key_1 = { "@ContactID", "@TicketID", "@EMPID" };
            object[] value_1 = { ContactID, TicketID, EmpID };
            ClsDb.ExecuteSP("USP_HTP_Associate_ContactID", key_1, value_1);

            return true;
        }

        /// <summary>
        /// Fetch contact info agaisnt each contact ID
        /// </summary>
        /// <param name="ContactID"></param>
        /// <returns>boolean</returns>
        public bool GetContactInfo(int ContactID)
        {
            string[] key_1 = { "@ContactID" };
            object[] value_1 = { ContactID };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_GET_ContactInfo", key_1, value_1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                FirstName = dr["FirstName"].ToString();
                MiddleName = dr["MiddleName"].ToString();
                MidNumber = dr["MidNumber"].ToString();
                LastName = dr["LastName"].ToString();
                DOB = dr["DOB"].ToString();
                DLNumber = dr["DLNumber"].ToString();
                DLState = dr["DLState"].ToString();

                Address = "";
                if (!dr["Address1"].ToString().Trim().Equals(""))
                {
                    Address += dr["Address1"].ToString();
                }

                if (!dr["Address2"].ToString().Trim().Equals(""))
                {
                    Address += " " + dr["Address2"].ToString();
                }

                if (!(dr["Address1"].ToString().Trim().Equals("") && dr["Address2"].ToString().Trim().Equals("")))
                {
                    Address += ", ";
                }

                if (!dr["City"].ToString().Trim().Equals(""))
                {
                    Address += dr["City"].ToString() + ", ";
                }

                if (!dr["State"].ToString().Trim().Equals(""))
                {
                    Address += dr["State"].ToString() + " ";
                }

                if (!dr["Zip"].ToString().Trim().Equals(""))
                {
                    Address += dr["Zip"].ToString() + " ";
                }

                Language = dr["language"].ToString();
                CDLFlag = dr["CDLFlag"].ToString();
                Race = dr["Race"].ToString();
                Gender = dr["Gender"].ToString();
                Height = dr["Height"].ToString();
                Email = dr["Email"].ToString();
                //if (Height.Trim().ToLower().Contains("ft"))
                //{
                //    Height = Height.Replace("ft", "' ");
                //    Height = Height + "'' ";
                //}
                Weight = dr["Weight"].ToString();
                if (Weight != "N/A" && Weight.Trim() != "")
                {
                    Weight += " lbs";
                }
                HairColor = dr["HairColor"].ToString();
                Eyes = dr["Eyes"].ToString();
                PhoneType1 = dr["PhoneType1"].ToString();
                Phone1 = dr["Phone1"].ToString();
                if (PhoneType1 != "N/A")
                {
                    if (dr["Phone1"].ToString().Length > 0)
                    {

                        if (dr["Phone1"].ToString().Length >= 3)
                            Phone1 = dr["Phone1"].ToString().Substring(0, 3);
                        if (dr["Phone1"].ToString().Length >= 6)
                            Phone1 = Phone1 + " " + dr["Phone1"].ToString().Substring(3, 3);
                        if (dr["Phone1"].ToString().Length >= 10)
                            Phone1 = Phone1 + " " + dr["Phone1"].ToString().Substring(6, 4);

                        if (dr["Phone1"].ToString().Length > 10)
                            Phone1 = Phone1 + " x " + dr["Phone1"].ToString().Substring(10);

                        Phone1 = Phone1 + " (" + PhoneType1 + ")";
                    }
                }

                PhoneType2 = dr["PhoneType2"].ToString();
                Phone2 = dr["Phone2"].ToString();
                if (PhoneType2 != "N/A")
                {
                    if (dr["Phone2"].ToString().Length > 0)
                    {
                        if (dr["Phone2"].ToString().Length >= 3)
                            Phone2 = dr["Phone2"].ToString().Substring(0, 3);
                        if (dr["Phone2"].ToString().Length >= 6)
                            Phone2 = Phone2 + " " + dr["Phone2"].ToString().Substring(3, 3);
                        if (dr["Phone2"].ToString().Length >= 10)
                            Phone2 = Phone2 + " " + dr["Phone2"].ToString().Substring(6, 4);

                        if (dr["Phone2"].ToString().Length > 10)
                            Phone2 = Phone2 + " x " + dr["Phone2"].ToString().Substring(10);

                        Phone2 = Phone2 + " (" + PhoneType2 + ")";
                    }
                }

                PhoneType3 = dr["PhoneType3"].ToString();
                Phone3 = dr["Phone3"].ToString();
                if (PhoneType3 != "N/A")
                {
                    if (dr["Phone3"].ToString().Length > 0)
                    {
                        if (dr["Phone3"].ToString().Length >= 3)
                            Phone3 = dr["Phone3"].ToString().Substring(0, 3);
                        if (dr["Phone3"].ToString().Length >= 6)
                            Phone3 = Phone3 + " " + dr["Phone3"].ToString().Substring(3, 3);
                        if (dr["Phone3"].ToString().Length >= 10)
                            Phone3 = Phone3 + " " + dr["Phone3"].ToString().Substring(6, 4);

                        if (dr["Phone3"].ToString().Length > 10)
                            Phone3 = Phone3 + " x " + dr["Phone3"].ToString().Substring(10);

                        Phone3 = Phone3 + " (" + PhoneType3 + ")";
                    }
                }


                if (HairColor.Trim().ToLower().Contains("--choose--"))
                {
                    HairColor = "N/A";
                }

                if (Eyes.Trim().ToLower().Contains("--choose--"))
                {
                    Eyes = "N/A";
                }

                if (Gender.Trim().ToLower().Contains("--choose--"))
                {
                    Gender = "N/A";
                }

            }
            return true;
        }

        //Nasir 5771 04/14/2009
        /// <summary>
        /// Unlink Potential Contact from non client
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns></returns>
        public bool UnlinkContactFromNonClient(int RecordID, int CaseTypeID)
        {
            string[] key_1 = { "@RecordID", "@CaseTypeID" };
            object[] value_1 = { RecordID, CaseTypeID };
            ClsDb.ExecuteSP("USP_HTP_Update_UnlinkContact", key_1, value_1);
            return true;
        }

        //Nasir 5771 04/14/2009
        /// <summary>
        /// use to disassociate(update null) contact(ID) against ticketID
        /// </summary>        
        /// <param name="TicketID"></param>
        /// <param name="EmpID"></param>
        /// <returns>boolean</returns>
        public bool DisassociateContactID(int TicketID, int EmpID)
        {
            string[] key_1 = { "@TicketID", "@EMPID" };
            object[] value_1 = { TicketID, EmpID };
            ClsDb.ExecuteSP("USP_HTP_Disassociate_ContactID", key_1, value_1);
            return true;
        }

        //Nasir 5771 04/14/2009
        /// <summary>
        /// used get matters of disposed case and active non disposed  case and quote client and non client which is associated with this Contact  
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns>Associated matters Dataset</returns>
        public DataSet AssociatedMatters(string TicketID)
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_Get_AssociatedMattersOfContact", keys, values);
            return ds;
        }

        //Waqas 5771 04/17/2009
        /// <summary>
        /// This procedure id used to update CID Confirmation flag.
        /// </summary>
        /// <param name="TicketID"></param>
        /// <param name="IsContactIDConfirmed"></param>
        /// <param name="EmpID"></param>
        /// <returns></returns>
        public bool UpdateCIDConfirmationFlag(int TicketID, int IsContactIDConfirmed, int EmpID)
        {
            try
            {

                string[] key_1 = { "@TicketID", "@IsContactIDConfirmed", "@EMPID" };
                object[] value_1 = { TicketID, IsContactIDConfirmed, EmpID };
                ClsDb.ExecuteSP("USP_Htp_Update_ContactIDConfirmedFlag", key_1, value_1);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Nasir 5771 04/14/2009
        /// <summary>
        /// Get first ticket id of given contact 
        /// </summary>
        /// <param name="ContactID"></param>
        /// <returns>ticketid</returns>
        public string GetTicketForCID(int ContactID)
        {
            string[] key_1 = { "@ContactID" };
            object[] value_1 = { ContactID };

            string TicketID = ClsDb.ExecuteScalerBySp("USP_HTP_Get_TicketIDForContactID", key_1, value_1).ToString();

            return TicketID;
        }

        //Nasir 5771 04/17/2009
        /// <summary>
        /// Get contact Id  of given ticketd id
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns>contact id</returns>
        public int GetContactIDByTicketID(int TicketID)
        {
            string[] key_1 = { "@TicketID" };
            object[] value_1 = { TicketID };

            int ContactID = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_Get_ContactIDByTicketID", key_1, value_1).ToString());
            return ContactID;
        }

        //Waqas 5771 04/17/2009
        /// <summary>
        /// This procedure will add new contact with lastname, firstname, dob
        /// </summary>
        /// <param name="Lastname"></param>
        /// <param name="Firstname"></param>
        /// <param name="DOB"></param>
        /// <returns></returns>
        public int CreateNewContact(string Lastname, string Firstname, string DOB, int EmpID, int TicketID)
        {
            int ContactID = 0;
            string[] key_1 = { "@FirstName", "@LastName", "@DOB", "@EmpID", "@TicketID", "@ContactID" };
            object[] value_1 = { Firstname, Lastname, DOB, EmpID, TicketID, ContactID };
            ContactID = Convert.ToInt32(ClsDb.InsertBySPArrRet("usp_htp_add_new_contact", key_1, value_1));

            return ContactID;
        }

        //Waqas 5771 04/18/2009
        /// <summary>
        /// This procedure is used to update Matter Page information if not available
        /// </summary>
        public void UpdateMatterInfoToContact()
        {
            if (ContactID != 0)
            {
                string[] key_1 = { "@ContactID", "@PhoneType1", "@Phone1", "@LanguageSpeak", "@CDLFlag", "@MidNumber" };
                object[] value_1 = { ContactID, PhoneTypeID1, Phone1, Language, CDLFlagID, MidNumber };
                ClsDb.ExecuteSP("usp_htp_update_contactinformation", key_1, value_1);
            }
        }

        //Waqas 5771 04/18/2009
        /// <summary>
        /// This procedure is used to update Contact Page information if not available
        /// </summary>
        public void UpdateContactInfoToContact()
        {
            if (ContactID != 0)
            {
                string[] key_1 = { "@ContactID", "@DLNumber", "@DLState", "@Address1", "@Address2", "@Email", "@City", "@StateID_FK", "@Zip", "@Race", "@Gender", "@Height" , 
                             "@Weight", "@HairColor", "@Eyes", "@Phone1", "@PhoneType1", "@Phone2", "@PhoneType2", "@Phone3", "@PhoneType3", "@MidNumber" , "@MiddleName" , "@NoDL" };
                object[] value_1 = { ContactID, DLNumber, DLStateID, Address1, Address2, Email, City, StateID_FK, Zip, Race, Gender, Height , 
                               Weight, HairColor, Eyes, Phone1, PhoneTypeID1, Phone2, PhoneTypeID2, Phone3, PhoneTypeID3, MidNumber, MiddleName, NoDL };
                ClsDb.ExecuteSP("usp_htp_update_contactinformation", key_1, value_1);
            }
        }
        #endregion

    }
}

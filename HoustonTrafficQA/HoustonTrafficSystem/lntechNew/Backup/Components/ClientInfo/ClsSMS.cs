﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FrameWorkEnation.Components;
using System.Net;
using System.IO;



namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    ///     Summary description for ClsSMS
    /// </summary>
    public class ClsSMS
    {
        clsENationWebComponents clsdb = new clsENationWebComponents("Connection String");

        public ClsSMS()
        {
        }

        #region Properties

        string _username;
        string _pasword;
        string _phonenumber;
        string _messagetext;
        // Noufil 6177 08/16/2009 new proprty added
        string _api_id;

        public string Api_id
        {
            get { return _api_id; }
            set { _api_id = value; }
        }

        public string Password
        {
            get { return _pasword; }
            set { _pasword = value; }
        }

        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }

        public string PhoneNumber
        {
            get { return _phonenumber; }
            set { _phonenumber = value; }
        }

        public string MessageText
        {
            get { return _messagetext; }
            set { _messagetext = value; }
        }


        #endregion


        #region Methods

        /// <summary>
        ///     This method checks whether Login to Service is succesfull or not by Username and password provided.
        /// </summary>    /// 
        /// <returns>True/False</returns>
        public string IsvalidAccount()
        {
            // Noufil 6177 08/16/2009 Use new API for SMS
            string url = "http://api.clickatell.com/http/auth?api_id=" + _api_id + "&user=" + _username + "&password=" + _pasword;
            WebRequest wb_request = WebRequest.Create(url);
            // Check Return status
            if (((HttpWebResponse)wb_request.GetResponse()).StatusCode == HttpStatusCode.OK)
            {
                return GetResponseString(wb_request);
            }
            else
            {
                clsLogger.ErrorLog(new ArgumentNullException("Account User Name or Password Are NULL or INVALID"));
                return "Error :Account User Name or Password Are NULL or INVALID";
            }
        }

        ///// <summary>
        /////     This method returns the remaining balance of sms
        ///// </summary>
        ///// <returns></returns>
        //public int CheckRemainingSms()
        //{
        //    return Messaging.MessageController.UserAccount.Balance;
        //}

        // Noufil 6177 08/16/2009 Code comment because no such method in new API
        ///// <summary>
        /////     This method Checks whether the input number is Valid for sms or not
        ///// </summary>
        ///// <param name="PhoneNumber">Phone Number</param>
        ///// <returns>True/False</returns>
        //public bool IsValidMobileNumber(string PhoneNumber)
        //{
        //    // Check whether Entered number is correct or not
        //    PhoneNumberValidity validity = Messaging.MessageController.PhoneNumberIsValid(PhoneNumber, out _phonenumber);
        //    if (validity != PhoneNumberValidity.Mobile && validity != PhoneNumberValidity.Either)
        //        return false;
        //    else
        //        return true;
        //}

        /// <summary>
        ///     THis is use to send send to the mobile number provided with message text.
        /// </summary>
        /// <returns>True/False</returns>
        public bool SendSms(string sessionid)
        {
            // Noufil 6177 08/16/2009 Use new API for SMS
            // Retrieve SessionID from Authentication process response.
            if (sessionid.Contains("OK"))
            {
                string sessionID = sessionid.Substring(sessionid.IndexOf(':') + 1).ToString().Trim();

                double numberofSMS = (Convert.ToDouble(_messagetext.Length) / 160.00) > 1.00 ? 2.00 : 1.00;

                WebRequest wb_request1;
                //Adil 8923 02/15/2011 From number changed from Sullo&Sullo to 17752374339 and MO parameter added.
                string url1 = "http://api.clickatell.com/http/sendmsg?session_id=" + sessionID + "&to=" + _phonenumber + "&text=" + _messagetext.Replace("&", "%26") + "&concat=" + numberofSMS + "&from=17752374339&MO=1";
                wb_request1 = WebRequest.Create(url1);

                //Get Reponse For Session ID            
                HttpWebResponse webresponse1 = (HttpWebResponse)wb_request1.GetResponse();
                // Check Return status
                if (webresponse1.StatusCode == HttpStatusCode.OK)
                {
                    // Get Response of SMS sent
                    string response1 = GetResponseString(wb_request1);
                    if (response1.Contains("ID"))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;



            //MessageOut message = new SMSMessage(_phonenumber, _messagetext);
            //// Add the message to the queue.
            //Messaging.MessageController.AddToQueue(message);
            //Messaging.MessageController.SendMessages();

        }

        /// <summary>
        ///     This method Add note in history for a client
        /// </summary>
        /// <param name="ticketid">Ticket ID of Client</param>
        /// <param name="message">History Note message</param>
        /// <param name="empid">Emp ID</param>
        public void AddNoteToHistory(int ticketid, string message, int empid)
        {
            if (ticketid > 0 && !string.IsNullOrEmpty(message) && empid > 0)
            {
                string[] key = { "@TicketID", "@Subject", "@EmpID", "@Note" };
                object[] value = { ticketid, message, empid, "" };
                clsdb.ExecuteSP("usp_HTS_Insert_CaseHistoryInfo", key, value);
            }
            else
                throw new ArgumentNullException("Aurgument cannot be null or empty");
        }


        /// <summary>
        ///     This method is use add sms information in TBLEMAILHISTORY.
        /// </summary>
        /// <param name="ClientName">CLient Name</param>
        /// <param name="meesagebody">SMS Body</param>
        /// <param name="ticketid">Ticket ID of Client</param>
        /// <param name="courtdate">Court Date</param>
        /// <param name="courtnumber">Court Number</param>
        /// <param name="phonenumber">Phone Number</param>
        /// <param name="smscalltype">Set Call/Reminder Call</param>
        /// <param name="ticketnumber">Cause Number</param>
        /// <param name="resendby">Resend User</param>
        public void InsertHistory(string ClientName, string meesagebody, int ticketid, DateTime courtdate, string courtnumber, string phonenumber, string smscalltype, string ticketnumber, string resendby)
        {
            if (ticketid > 0)
            {
                string[] key = { "@EmailTo", "@Client", "@Subject", "@SentInfo", "@recdate", "@Sentflag", "@TicketID", "@CourtDate", "@CourtNumber", "@resendby", "@Smstype", "@PhoneNumber", "@SmsCalltype", "@ticketnumber" };
                object[] value = { "", ClientName, "", meesagebody, DateTime.Now, 3, ticketid, courtdate, courtnumber, resendby, 1, phonenumber, smscalltype, ticketnumber };
                clsdb.ExecuteSP("USP_HTP_INSERT_INTO_TBLEMAILHISTORY", key, value);
            }
            else
                throw new ArgumentNullException("Aurgument cannot be null or empty");
        }

        /// <summary>
        /// This method get HTML response in string from Web Request Object
        /// </summary>
        /// <param name="request">Web Request Object</param>
        /// <returns>Return HTML Response String</returns>
        public string GetResponseString(WebRequest request)
        {
            try
            {
                string responseFromServer = "";
                //Get Response
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Get Response Stream
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                // Noufil 4920 10/10/2008 Check added if data Stream can read the data.
                if (dataStream.CanRead)
                {
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();
                    // Cleanup the streams and the response.
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                    //Send Response HTML                    
                }
                return responseFromServer;

            }
            catch
            {
                // Logger.Instance.LogError(ex);
                return "";
            }

        }

        #endregion
    }
}

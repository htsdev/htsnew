using System;
using System.Data;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
	/// <summary>
	/// Summary description for clsViolationCategory.
	/// </summary>
	public class clsViolationCategory
	{
		//Create an Instance of Data Access Class
		clsENationWebComponents ClsDb=new clsENationWebComponents();
clsLogger bugTracker = new clsLogger();
#region Variables

		private int categoryID=0;
		private string categoryName=String.Empty;
		private string categoryDescription=String.Empty;
        private string shortdescription = string.Empty;

#endregion

#region Properties
		public int CategoryID
		{
			get
			{
				return categoryID;
			}
			set
			{
				categoryID = value;
			}
		}
		public string CategoryName
		{
			get
			{
				return categoryName;
			}
			set
			{
				categoryName = value;
			}
		}

		public string CategoryDescription
		{
			get
			{
				return categoryDescription;
			}
			set
			{
				categoryDescription = value;
			}
		}

        public string ShortDescription
        {
            get
            {
                return shortdescription;
            }
            set
            {
                shortdescription = value;
            }
        }

#endregion 

#region Methods

		public bool AddCategory()
		{
            string[] key = { "@CategoryName", "@CategoryDescription", "@ShortDescription"};
			object[] value1 = {CategoryName,CategoryDescription,ShortDescription};
			try
			{
				string isInsert = ((string) Convert.ChangeType(ClsDb.ExecuteScalerBySp("USP_HTS_Insert_ViolationCategory",key,value1),typeof(string)));
				if(isInsert=="1")return true;
				else return false;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return false;
			}

		}

		public bool UpdateCategory()
		{
            string[] key = { "@CategoryID", "@CategoryName", "@CategoryDescription","@ShortDescription"};
			object[] value1 = { CategoryID ,CategoryName,CategoryDescription,ShortDescription};
			try
			{
				object obj  = ClsDb.ExecuteScalerBySp("USP_HTS_UpdateViolationCategory",key,value1);
				string isUpdate = ((string) Convert.ChangeType(obj,typeof(string)));
				if(isUpdate=="1") return true;
				else return false;
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				return false;
			}
		}

		/*public bool DeleteCategory(int CategoryID)
		{
			return true;
		}*/

		public DataSet GetCategory(int CategoryID)
		{
			DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetAllCategories","@CategoryID",CategoryID);
			return ds;
		}
		public DataSet GetCategory()
		{
			DataSet ds=ClsDb.Get_DS_BySP("usp_HTS_GetAllCategories");
			return ds;
		}

#endregion

		public clsViolationCategory()
		{
			//
			// TODO: Add constructor logic here
			//
		}

	}
}

using System;
using System.Data;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsViolations.
    /// </summary>
    public class clsViolations
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        #region Variables

        private int violationNumber = 0;
        private string description = String.Empty;
        private int sequenceOrder = 0;
        private double chargeAmount = 0;
        private double bondAmount = 0;
        private string shortDescription = String.Empty;
        private string violationCode = String.Empty;
        private int courtID = 0;
        private int categoryID = 0;

        #endregion

        #region Properties
        public int ViolationNumber
        {
            get
            {
                return violationNumber;
            }
            set
            {
                violationNumber = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public int SequenceOrder
        {
            get
            {
                return sequenceOrder;
            }
            set
            {
                sequenceOrder = value;
            }
        }

        public double ChargeAmount
        {
            get
            {
                return chargeAmount;
            }
            set
            {
                chargeAmount = value;
            }
        }

        public double BondAmount
        {
            get
            {
                return bondAmount;
            }
            set
            {
                bondAmount = value;
            }
        }

        public string ShortDescription
        {
            get
            {
                return shortDescription;
            }
            set
            {
                shortDescription = value;
            }
        }

        public string ViolationCode
        {
            get
            {
                return violationCode;
            }
            set
            {
                violationCode = value;
            }
        }
        public int CourtID
        {
            get
            {
                return courtID;
            }
            set
            {
                courtID = value;
            }
        }

        public int CategoryID
        {
            get
            {
                return categoryID;
            }
            set
            {
                categoryID = value;
            }
        }

        #endregion

        #region Methods

        /*public bool AddViolation(object dsViolation)
		{
			/*string[] key    = {"@firstname","@lastname","@currentcourtloc","@datetypeflag","@CurrentDateset","@currentcourtnum","@Contact1","@ContactType1","@LanguageSpeak","@bondflag","@AccidentFlag","@CDLFlag","@BondsRequiredflag","@AdSourceId","@GeneralComments","@employeeid"};

			object[] value1 = {FirstName,LastName,CurrentCourtLocation,DateTypeFlag,CurrentDateSet,CurrentCourtNo,Contact1,ContactType1,LanguageSpeak,BondFlag,AccidentFlag,CDLFlag,BondsRequiredFlag,AdSourceID,GeneralComments,EmpID};
			try
			{
				ClsDb.ExecuteSP("USP_HTS_Insert_NewCase",key,value1);
				return true;
			}
			catch(Exception ex)
			{
				return false;
			}
			return true;
		}*/

        /*public bool UpdateViolation(object dsViolation)
        {
            /*string[] key    = {"@firstname","@lastname","@currentcourtloc","@datetypeflag","@CurrentDateset","@currentcourtnum","@Contact1","@ContactType1","@LanguageSpeak","@bondflag","@AccidentFlag","@CDLFlag","@BondsRequiredflag","@AdSourceId","@GeneralComments","@employeeid"};

            object[] value1 = {FirstName,LastName,CurrentCourtLocation,DateTypeFlag,CurrentDateSet,CurrentCourtNo,Contact1,ContactType1,LanguageSpeak,BondFlag,AccidentFlag,CDLFlag,BondsRequiredFlag,AdSourceID,GeneralComments,EmpID};
            try
            {
                ClsDb.ExecuteSP("USP_HTS_Insert_NewCase",key,value1);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }*/

        public bool UpdateViolationToCategoryID(int intCategoryID, string strViolationIDs)
        {
            string[] key = { "@CategoryID", "@ViolationNumbers" };
            object[] value1 = { intCategoryID, strViolationIDs };

            try
            {
                ClsDb.ExecuteSP("USP_HTS_Update_ViolationsToCategoryID", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }




        /*public DataSet GetViolation(int ViolationNumber)
        {
            DataSet ds=ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetViolation","@ViolationNumber",ViolationNumber);
            return ds;
        }*/

        //Agha Usman 4271 07/02/2008
        public DataSet GetAllViolations()
        {
            return GetAllViolations(CaseType.Traffic, 3001);
        }

        public DataSet GetViolationsByCategoryID(int CategoryID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_Get_ViolationByCategory", "@CategoryID", CategoryID);
            return ds;
        }

        //Added By fahad for displaying violations for  Harris County Criminal Court
        public DataSet GetAllHarrisCountyViolations()
        {
            DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_Get_HarisCountyViolations");
            return ds;
        }
        //   //added khalid 22-9-07
        //public DataSet GetAllCriminalCourtViolations()
        //{
        //    DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_Get_CriminalCourtViolations");
        //    return ds;
        //}

        public DataSet GetAllChargeLevel(int courtid)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_Get_charge_level", "@Courtid", courtid);
            return ds;
        }

        public DataSet GetAllViolationsByCourtLocation(int courtid)
        {
            return GetAllViolations(CaseType.Traffic, courtid);
        }

        /// <summary>
        /// Get Violations By Case Type
        /// </summary>
        /// <param name="caseType">Case Type</param>
        /// <param name="CourtId">Court ID </param>
        /// <returns></returns>
        public DataSet GetAllViolations(int caseType, int CourtId)
        {
            //Zeeshan Ahmed 3979 05/20/2008
            return ClsDb.Get_DS_BySPByTwoParmameter("USP_HTS_GetAllViolations", "@CaseType", Convert.ToInt32(caseType), "@CourtID", CourtId);
        }



        #endregion

        public clsViolations()
        {
        }
    }
}

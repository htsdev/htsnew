////
using System;
using System.Data;
using System.Data.Common;
using FrameWorkEnation.Components;
using System.Web;
using System.Configuration;
using System.Collections;


namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsCase.
    /// </summary>
    /// 
    //Farrukh 9925 11/30/2011 Resharper executed
    public enum CourtViolationStatusType
    {
        ALR = 237,
        JUR = 4,
        JUD = 7,
        WAIT = 104,
        DISP = 80,
        ARR = 3,  // Noufil 4215 06/30/2008 Arr case type added
        ARRWAIT = 201,
        APP = 116,
        BOND = 135    //Fahad 5807 04/29/2009 Bond Added
    }

    // Agha Usman 4271 07/02/2008
    //public enum CaseType
    //{
    //    Traffic = 1,
    //    Criminal = 2,
    //    Civil = 3
    //}

    public class clsCase
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        clsSession ObjClssession = new clsSession();
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        #region Variables

        private int ticketID = 0;
        private int empID = 0;
        private int officerNo = 0;
        private int adSourceID = 0;
        private int ticketStatusType = 0;
        private int currentCourtLocation = 0;
        private int dateTypeFlag = 0;
        private int reminderStatusID = 0;
        private int activeFlag = 0;
        private int bondFlag = 0;
        private int firmID = 0;
        private int immigrationFlag = 0;
        private int bondsRequiredFlag = 0;
        private int accidentFlag = 0;
        private int cdlFlag = 0;
        private int preTrialStatus = 0;
        private int occupationLicense = 0;
        private int alrHearing = 0;
        private int empIDUpdate = 0;
        private int coveringFirmID = 0;
        private int salesEmpID = 0;
        private int callStatusForQuote = 0;
        private int stateID = 0;
        private int dlStateID = 0;
        private int occupationID = 0;
        private int contactType1 = 0;
        private int contactType2 = 0;
        private int contactType3 = 0;

        // Rab Nawaz Khan 10330 06/28/2012 Arr Status flag has been added. . .
        private bool dayOfArrFlag = false;
        // End 10330

        // Noufil 5884 07/02/2009 Sms Flag Variable declare
        private int smsflag1 = 0;
        private int smsflag2 = 0;
        private int smsflag3 = 0;
        private int casecount = 0;
        private int speedingID = 0;
        private int quoteID = 0;
        private int quoteResultID = 0;
        private int followUPID = 0;
        private int followUpYN = 0;
        private int nodl = 0;
        private int addressconfirmflag = 0;
        private double baseFeeCharge = 0;
        private double fastTrackFee = 0;
        private double bondingAmount = 0;
        private double continuanceAmount = 0;
        private double rerapAmount = 0;
        private double adjustment = 0;
        private double addition = 0;
        private double totalFeeCharged = 0;
        private double feeInitial = 0;
        private double initialBaseFee = 0;
        private double initialFastTrackFee = 0;
        private double initialAdditionFee = 0;
        private double initialBondFee = 0;
        private double calculatedTotalFee = 0;
        private string ticketNo = String.Empty;
        private string languageSpeak;
        private string occupationName = String.Empty;
        private string continuanceRequestComments = String.Empty;
        private string officerName = String.Empty;
        private string currentCourtNo = String.Empty;
        private string midNumber = String.Empty;
        private string firstName = String.Empty;
        private string middleName = String.Empty;
        private string lastName = String.Empty;
        private string dob = String.Empty;
        private string gender = String.Empty;
        private string race = String.Empty;
        private string height = String.Empty;
        private string weight = String.Empty;
        private string hairColor = String.Empty;
        private string eyeColor = String.Empty;
        private string ssn = String.Empty;
        private string dlNumber = String.Empty;
        private string address1 = String.Empty;
        private string address2 = String.Empty;
        private string city = String.Empty;
        private string zip = String.Empty;
        private string email = String.Empty;
        private string contact1 = String.Empty;
        private string contact2 = String.Empty;
        private string contact3 = String.Empty;
        private string contactComments = String.Empty;
        private string generalComments = String.Empty;
        private string settingsComments = String.Empty;
        private string setcallcomments = String.Empty;
        private string ftacallcomments = String.Empty;
        private string paymentComments = String.Empty;
        private string trialComments = String.Empty;
        private string preTrialComments = String.Empty;
        private string initialAdjustmentInitial = String.Empty;
        private string quoteResultDate = String.Empty;
        private string followUPDate = String.Empty;
        private string quoteComments = String.Empty;
        private string appointmentTime = String.Empty;
        private string yds = String.Empty;
        private string ClientCodenum = String.Empty;
        private DateTime currentDateSet = DateTime.Now;
        private DateTime recDate = DateTime.Now;
        private DateTime reminderDateTime = DateTime.Now;
        private DateTime lastContactDate = DateTime.Now;
        private DateTime contactDate = DateTime.Now;
        private DateTime jpFaxDate = DateTime.Now;
        private DateTime statusUpdateDate = DateTime.Now;
        private DateTime callBackDate;
        private DateTime appointmentDate;
        ////nasir 5381 01/15/2009  declare tostore genral comments date
        private DateTime lastGerenalcommentsUpdatedate = DateTime.Now;
        //private  DateTime callBackDate=DateTime.Now;		
        // Added By Zeeshan Ahmed To Implment Late Fee and Over Speeding Fee
        private bool islate = false;

        // Abbas Qamar 10114 04-05-2012 Adding the IsCrimianlCourt Property
        private int _SSNTypeId = 0;
        private string _SSNOtherQuestion = string.Empty;
        private int _IsCriminalCourt = 0;
        // End 10114 

        private int latefee = 0;
        private double overspeedingfee = 0;

        // Added By Zeeshan Ahmed to implement continuance status and date
        //Fahad 5753 04/04/2009

        private string continuancecomments = String.Empty;
        private int continuancestatus = 0;
        private DateTime continuancedate = DateTime.Now;
        private bool continuanceflag = false;
        private bool emailNotAvailable = false;
        ////nasir 5381 01/15/2009 
        private bool gerenalcommentsoutput = true;

        //Added by Azee for Walk in Client Flag Modification
        private string walkinclientflag = string.Empty;
        private int isPR = 2;
        private int hasConsultationComments = 2;
        //

        // Added By Zeeshan Ahmed
        string dp2 = String.Empty;
        string dpc = String.Empty;
        string flag1 = String.Empty;
        bool updatebarcodeinfo = false;
        //added khalid 24-9-07
        private string spn = string.Empty;
        private string court = string.Empty;
        private string followup = string.Empty;
        private int caseType = 1;

        // .... tahir 4786 10/08/2008 
        //
        private bool hasPaymentPlan = false;

        public int CaseType
        {
            get { return caseType; }
            set { caseType = value; }
        }

        //Waqas Javed 5771 04/16/2009
        private int recordid = 0;
        private int ContactID = 0;
        private int IsContactIDConfirmed = 0;

        //Waqas 5864 06/26/2009 ALR Criminal
        //Waqas 6342 08/25/2009 Changes for criminal and family 
        private int pHavePriorAttorney = 0;
        private string pPriorAttorneyType = string.Empty;
        private string pPriorAttorneyName = string.Empty;
        private string pCaseSummaryComments = string.Empty;
        private string pALROfficerName = string.Empty;
        private string pALROfficeBadgeNo = string.Empty;
        private string pALRPrecinct = string.Empty;
        private string pALROfficerAddress = string.Empty;
        private string pALROfficerCity = string.Empty;
        private int pALROfficerState = 0;
        private string pALROfficerZip = string.Empty;
        private string pALRContactNumber = string.Empty;
        private int pALRIntoxilyzerTakenFlag = 0;
        private string pALRArrestingAgency = string.Empty;
        private decimal pALROfficerMileage = 0;
        private string pALRIntoxilyzerResult = string.Empty;
        private string pBTOFirstName = string.Empty;
        private string pBTOLastName = string.Empty;

        //Waqas 6342 08/25/2009 Changes for criminal and family 
        private bool? pBTOBTSSameFlag = null;
        private string pBTSFirstName = string.Empty;
        private string pBTSLastName = string.Empty;

        // Noufil 5884 07/02/2009 Sms Flag property declare
        public int SmsFlag1
        {
            get { return smsflag1; }
            set { smsflag1 = value; }
        }
        public int SmsFlag2
        {
            get { return smsflag2; }
            set { smsflag2 = value; }
        }
        public int SmsFlag3
        {
            get { return smsflag3; }
            set { smsflag3 = value; }
        }
        // Afaq 7937 06/29/2010 Add property for valid email address.
        private bool isEmailVerified;
        /// <summary>
        /// Containing status of client email.
        /// </summary>
        public bool IsEmailVerified
        {
            get { return isEmailVerified; }
            set { isEmailVerified = value; }
        }

        // Abbas Qamar 10114 04-05-2012 
        /// <summary>
        /// Contating the IsCriminal Property 
        /// </summary>
        public int IsCriminalCourt
        {
            get { return _IsCriminalCourt; }
            set { _IsCriminalCourt = value; }
        }

        // Abbas Qamar 10114 04-05-2012 
        /// <summary>
        /// Contating the SSN Other Questoin Property 
        /// </summary>
        public string SSNOtherQuestion
        {
            get { return _SSNOtherQuestion; }
            set { _SSNOtherQuestion = value; }
        }


        // Abbas Qamar 10114 04-05-2012 
        /// <summary>
        /// Contating the IsCriminal Property 
        /// </summary>
        public int SSNTypeID
        {
            get { return _SSNTypeId; }
            set { _SSNTypeId = value; }
        }

        //Waqas 6342 08/12/2009 properties for observing officer
        private bool? pIsALRArrestingObservingSame = null;
        private string pALROBSOfficerName = string.Empty;
        private string pALROBSOfficeBadgeNo = string.Empty;
        private string pALROBSPrecinct = string.Empty;
        private string pALROBSOfficerAddress = string.Empty;
        private string pALROBSOfficerCity = string.Empty;
        private int pALROBSOfficerState = 0;
        private string pALROBSOfficerZip = string.Empty;
        private string pALROBSContactNumber = string.Empty;
        private string pALROBSArrestingAgency = string.Empty;
        private decimal pALROBSOfficerMileage = 0;
        private bool? pIsALRHearingRequired = null;
        private string pALRHearingRequiredAnswer = string.Empty;

        //Waqas 6599 09/19/2009 
        private string pOccupation = string.Empty;
        private string pEmployer = string.Empty;
        private bool pIsUnemployed = false;
        private int pVehicleType = 0;


        #endregion

        #region Properties

        public string Court
        {
            get
            {
                return court;
            }
            set
            {
                court = value;
            }
        }


        public string SPN
        {
            get
            {
                return spn;
            }
            set
            {
                spn = value;
            }
        }



        public int HasConsultationComments
        {
            get { return hasConsultationComments; }
            set { hasConsultationComments = value; }
        }

        //added by Azee for Walk in Client Flag Modification
        public string WalkInClientFlag
        {
            get
            {
                return walkinclientflag;
            }
            set
            {
                walkinclientflag = value;
            }
        }

        public int IsPR
        {
            get
            {
                return isPR;
            }
            set
            {
                isPR = value;
            }
        }

        public string SetcallComments
        {
            get
            {
                return setcallcomments;
            }
            set
            {
                setcallcomments = value;
            }
        }

        //Zahoor 3977 09/19/8 for FTA comments in metters page
        public string FTAcallcomments
        {
            get { return ftacallcomments; }
            set { ftacallcomments = value; }
        }

        public string ContinuanceComments
        {
            get
            {
                return continuancecomments;
            }
            set
            {
                continuancecomments = value;
            }
        }

        public int ContinuanceStatus
        {

            get
            {
                return continuancestatus;
            }
            set
            {
                continuancestatus = value;
            }
        }
        public DateTime ContinuanceDate
        {
            get
            {
                return continuancedate;
            }
            set
            {
                continuancedate = value;
            }
        }

        public bool ContinuanceFlag
        {
            get
            {
                return continuanceflag;
            }
            set
            {
                continuanceflag = value;
            }
        }

        // Added By Zeeshan Ahmed To Implment Late Fee and Over Speeding Fee

        public bool HasLateFee
        {
            set
            {
                islate = value;
            }

        }

        // tahir 4786 10/08/2008
        //
        public bool HasPaymentPlan
        {
            get
            {
                return hasPaymentPlan;
            }
            set
            {
                hasPaymentPlan = value;
            }
        }

        public bool HasOverSpeeding
        {
            set
            {
                islate = value;
            }

        }

        public int LateFee
        {
            set
            {
                latefee = value;
            }

        }

        public int OverSpeedingFee
        {
            set
            {
                overspeedingfee = value;
            }

        }
        //******************************************************************


        public int TicketID
        {
            get
            {
                return ticketID;
            }
            set
            {
                ticketID = value;
            }
        }


        public bool EmailNotAvailable
        {
            get
            {
                return emailNotAvailable;
            }
            set
            {
                emailNotAvailable = value;
            }
        }


        public int SpeedingID
        {
            get
            {
                return speedingID;
            }
            set
            {
                speedingID = value;
            }
        }

        public int AddressConfirmFlag
        {
            get
            {
                return addressconfirmflag;
            }
            set
            {
                addressconfirmflag = value;
            }
        }

        public string TicketNo
        {
            get
            {
                return ticketNo;
            }
            set
            {
                ticketNo = value;
            }
        }

        public string YDS
        {
            get
            {
                return yds;
            }
            set
            {
                yds = value;
            }
        }

        public int OfficerNo
        {
            get
            {
                return officerNo;
            }
            set
            {
                officerNo = value;
            }
        }
        public int Casecount
        {
            get
            {
                return casecount;
            }
            set
            {
                casecount = value;
            }
        }

        public string OfficerName
        {
            get
            {
                return officerName;
            }
            set
            {
                officerName = value;
            }
        }

        public int TicketStatusType
        {
            get
            {
                return ticketStatusType;
            }
            set
            {
                ticketStatusType = value;
            }
        }

        public DateTime CurrentDateSet
        {
            get
            {
                return currentDateSet;
            }
            set
            {
                currentDateSet = value;
            }
        }

        public string CurrentCourtNo
        {
            get
            {
                return currentCourtNo;
            }
            set
            {
                currentCourtNo = value;
            }
        }

        public int CurrentCourtLocation
        {
            get
            {
                return currentCourtLocation;
            }
            set
            {
                currentCourtLocation = value;
            }
        }

        public int DateTypeFlag
        {
            get
            {
                return dateTypeFlag;
            }
            set
            {
                dateTypeFlag = value;
            }
        }

        public int AdSourceID
        {
            get
            {
                return adSourceID;
            }
            set
            {
                adSourceID = value;
            }
        }

        public string LanguageSpeak
        {
            get
            {
                return languageSpeak;
            }
            set
            {
                languageSpeak = value;
            }
        }

        public double BaseFeeCharge
        {
            get
            {
                return baseFeeCharge;
            }
            set
            {
                baseFeeCharge = value;
            }
        }

        public double FastTrackFee
        {
            get
            {
                return fastTrackFee;
            }
            set
            {
                fastTrackFee = value;
            }
        }

        public double BondingAmount
        {
            get
            {
                return bondingAmount;
            }
            set
            {
                bondingAmount = value;
            }
        }

        public double ContinuanceAmount
        {
            get
            {
                return continuanceAmount;
            }
            set
            {
                continuanceAmount = value;
            }
        }

        public double RerapAmount
        {
            get
            {
                return rerapAmount;
            }
            set
            {
                rerapAmount = value;
            }
        }

        public double Adjustment
        {
            get
            {
                return adjustment;
            }
            set
            {
                adjustment = value;
            }
        }

        public double Addition
        {
            get
            {
                return addition;
            }
            set
            {
                addition = value;
            }
        }

        public double TotalFeeCharged
        {
            get
            {
                return totalFeeCharged;
            }
            set
            {
                totalFeeCharged = value;
            }
        }

        public string ContactComments
        {
            get
            {
                return contactComments;
            }
            set
            {
                contactComments = value;
            }
        }

        public string GeneralComments
        {
            get
            {
                return generalComments;
            }
            set
            {
                generalComments = value;
            }
        }



        /// <summary>
        /// Nasir 5381 01/15/2009 set and get property for last comments update date
        /// </summary>
        public DateTime LastGerenalcommentsUpdatedate
        {
            get
            {
                return lastGerenalcommentsUpdatedate;
            }
            set
            {
                lastGerenalcommentsUpdatedate = value;
            }
        }

        public bool Gerenalcommentsoutput
        {
            get
            {
                return gerenalcommentsoutput;
            }
            set
            {
                gerenalcommentsoutput = value;
            }
        }
        public string SettingsComments
        {
            get
            {
                return settingsComments;
            }
            set
            {
                settingsComments = value;
            }
        }

        public string PaymentComments
        {
            get
            {
                return paymentComments;
            }
            set
            {
                paymentComments = value;
            }
        }

        public string TrialComments
        {
            get
            {
                return trialComments;
            }
            set
            {
                trialComments = value;
            }
        }

        public string PreTrialComments
        {
            get
            {
                return preTrialComments;
            }
            set
            {
                preTrialComments = value;
            }
        }

        public int
            EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        public DateTime RecDate
        {
            get
            {
                return recDate;
            }
            set
            {
                recDate = value;
            }
        }


        //Rab Nawaz Khan 10330 06/28/2012 Arraignment Status property has been added . . . 

        public bool DayOfArrFlag
        {
            get { return dayOfArrFlag; }
            set { dayOfArrFlag = value; }
        }
        // END 10330

        public string ContinuanceRequestComments
        {
            get
            {
                return continuanceRequestComments;
            }
            set
            {
                continuanceRequestComments = value;
            }
        }

        public int ReminderStatusID
        {
            get
            {
                return reminderStatusID;
            }
            set
            {
                reminderStatusID = value;
            }
        }

        public DateTime ReminderDateTime
        {
            get
            {
                return reminderDateTime;
            }
            set
            {
                reminderDateTime = value;
            }
        }

        public int ActiveFlag
        {
            get
            {
                return activeFlag;
            }
            set
            {
                activeFlag = value;
            }
        }

        public DateTime LastContactDate
        {
            get
            {
                return lastContactDate;
            }
            set
            {
                lastContactDate = value;
            }
        }

        public DateTime ContactDate
        {
            get
            {
                return contactDate;
            }
            set
            {
                contactDate = value;
            }
        }

        public double FeeInitial
        {
            get
            {
                return feeInitial;
            }
            set
            {
                feeInitial = value;
            }
        }

        public int BondFlag
        {
            get
            {
                return bondFlag;
            }
            set
            {
                bondFlag = value;
            }
        }

        public DateTime JPFaxDate
        {
            get
            {
                return jpFaxDate;
            }
            set
            {
                jpFaxDate = value;
            }
        }

        public int FirmID
        {
            get
            {
                return firmID;
            }
            set
            {
                firmID = value;
            }
        }

        public double InitialBaseFee
        {
            get
            {
                return initialBaseFee;
            }
            set
            {
                initialBaseFee = value;
            }
        }

        public double InitialFastTrackFee
        {
            get
            {
                return initialFastTrackFee;
            }
            set
            {
                initialFastTrackFee = value;
            }
        }

        public double InitialAdditionFee
        {
            get
            {
                return initialAdditionFee;
            }
            set
            {
                initialAdditionFee = value;
            }
        }

        public double InitialBondFee
        {
            get
            {
                return initialBondFee;
            }
            set
            {
                initialBondFee = value;
            }
        }

        public string InitialAdjustmentInitial
        {
            get
            {
                return initialAdjustmentInitial;
            }
            set
            {
                initialAdjustmentInitial = value;
            }
        }

        public int CallStatusForQuote
        {
            get
            {
                return callStatusForQuote;
            }
            set
            {
                callStatusForQuote = value;
            }
        }

        public int CoveringFirmID
        {
            get
            {
                return coveringFirmID;
            }
            set
            {
                coveringFirmID = value;
            }
        }

        public double CalculatedTotalFee
        {
            get
            {
                return calculatedTotalFee;
            }
            set
            {
                calculatedTotalFee = value;
            }
        }

        public int ImmigrationFlag
        {
            get
            {
                return immigrationFlag;
            }
            set
            {
                immigrationFlag = value;
            }
        }

        public int BondsRequiredFlag
        {
            get
            {
                return bondsRequiredFlag;
            }
            set
            {
                bondsRequiredFlag = value;
            }
        }

        public int AccidentFlag
        {
            get
            {
                return accidentFlag;
            }
            set
            {
                accidentFlag = value;
            }
        }

        public int CDLFlag
        {
            get
            {
                return cdlFlag;
            }
            set
            {
                cdlFlag = value;
            }
        }

        public int PreTrialStatus
        {
            get
            {
                return preTrialStatus;
            }
            set
            {
                preTrialStatus = value;
            }
        }

        public int OccupationLicense
        {
            get
            {
                return occupationLicense;
            }
            set
            {
                occupationLicense = value;
            }
        }

        public int ALRHearing
        {
            get
            {
                return alrHearing;
            }
            set
            {
                alrHearing = value;
            }
        }

        public int EmpIDUpdate
        {
            get
            {
                return empIDUpdate;
            }
            set
            {
                empIDUpdate = value;
            }
        }

        public DateTime StatusUpdateDate
        {
            get
            {
                return statusUpdateDate;
            }
            set
            {
                statusUpdateDate = value;
            }
        }

        public int SalesEmpID
        {
            get
            {
                return salesEmpID;
            }
            set
            {
                salesEmpID = value;
            }
        }

        public string MidNumber
        {
            get
            {
                return midNumber;
            }
            set
            {
                midNumber = value;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public string MiddleName
        {
            get
            {
                return middleName;
            }
            set
            {
                middleName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public string DOB
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
            }
        }
        public int NoDL
        {
            get
            {
                return nodl;
            }
            set
            {
                nodl = value;
            }

        }

        public string Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        public string Race
        {
            get
            {
                return race;
            }
            set
            {
                race = value;
            }
        }

        public string Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        public string Weight
        {
            get
            {
                return weight;
            }
            set
            {
                weight = value;
            }
        }

        public string HairColor
        {
            get
            {
                return hairColor;
            }
            set
            {
                hairColor = value;
            }
        }

        public string EyeColor
        {
            get
            {
                return eyeColor;
            }
            set
            {
                eyeColor = value;
            }
        }

        public string SSN
        {
            get
            {
                return ssn;
            }
            set
            {
                ssn = value;
            }
        }

        public string DLNumber
        {
            get
            {
                return dlNumber;
            }
            set
            {
                dlNumber = value;
            }
        }

        public int DLStateID
        {
            get
            {
                return dlStateID;
            }
            set
            {
                dlStateID = value;
            }
        }

        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public int StateID
        {
            get
            {
                return stateID;
            }
            set
            {
                stateID = value;
            }
        }


        public string Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public int OccupationID
        {
            get
            {
                return occupationID;
            }
            set
            {
                occupationID = value;
            }
        }
        public string OccupationName
        {
            get
            {
                return occupationName;
            }
            set
            {
                occupationName = value;
            }
        }

        public string Contact1
        {
            get
            {
                return contact1;
            }
            set
            {
                contact1 = value;
            }
        }

        public int ContactType1
        {
            get
            {
                return contactType1;
            }
            set
            {
                contactType1 = value;
            }
        }

        public string Contact2
        {
            get
            {
                return contact2;
            }
            set
            {
                contact2 = value;
            }
        }

        public int ContactType2
        {
            get
            {
                return contactType2;
            }
            set
            {
                contactType2 = value;
            }
        }

        public string Contact3
        {
            get
            {
                return contact3;
            }
            set
            {
                contact3 = value;
            }
        }

        public int ContactType3
        {
            get
            {
                return contactType3;
            }
            set
            {
                contactType3 = value;
            }
        }


        public int QuoteID
        {
            get
            {
                return quoteID;
            }
            set
            {
                quoteID = value;
            }
        }

        public int QuoteResultID
        {
            get
            {
                return quoteResultID;
            }
            set
            {
                quoteResultID = value;
            }
        }

        public string QuoteResultDate
        {
            get
            {
                return quoteResultDate;
            }
            set
            {
                quoteResultDate = value;
            }
        }

        public int FollowUPID
        {
            get
            {
                return followUPID;
            }
            set
            {
                followUPID = value;
            }
        }

        public string FollowUPDate
        {
            get
            {
                return followUPDate;
            }
            set
            {
                followUPDate = value;
            }
        }

        public int FollowUpYN
        {
            get
            {
                return followUpYN;
            }
            set
            {
                followUpYN = value;
            }
        }

        public string QuoteComments
        {
            get
            {
                return quoteComments;
            }
            set
            {
                quoteComments = value;
            }
        }

        public DateTime CallBackDate
        {
            get
            {
                return callBackDate;
            }
            set
            {
                callBackDate = value;
            }
        }

        public DateTime AppointmentDate
        {
            get
            {
                return appointmentDate;
            }
            set
            {
                appointmentDate = value;
            }
        }

        public string AppointmentTime
        {
            get
            {
                return appointmentTime;
            }
            set
            {
                appointmentTime = value;
            }
        }

        public string ClientCodeNumber
        {
            get
            {
                return ClientCodenum;
            }
            set
            {
                ClientCodenum = value;
            }
        }

        // Properties For Barcode Information 

        public string DP2
        {
            set
            {
                dp2 = value;
            }
        }

        public string DPC
        {
            set
            {
                dpc = value;
            }
        }

        public string FLAG1
        {
            set
            {
                flag1 = value;
            }
        }

        public bool UpdateBarcodeInfo
        {
            set
            {
                updatebarcodeinfo = value;
            }
        }

        //Waqas Javed 5771 04/16/2009
        public int Record_Id
        {
            set
            {
                recordid = value;
            }
            get
            {
                return recordid;
            }
        }

        public int Contact_ID
        {
            set
            {
                ContactID = value;
            }
            get
            {
                return ContactID;
            }
        }

        public int IsContactIDConfirmedFlag
        {
            set
            {
                IsContactIDConfirmed = value;
            }
            get
            {
                return IsContactIDConfirmed;
            }
        }

        //Waqas 5864 06/26/2009 ALR Criminal 
        //Waqas 6342 08/25/3009 changes for criminal and family
        public int HavePriorAttorney
        {
            set
            {
                pHavePriorAttorney = value;
            }
            get
            {
                return pHavePriorAttorney;
            }
        }
        public string PriorAttorneyType
        {
            set
            {
                pPriorAttorneyType = value;
            }
            get
            {
                return pPriorAttorneyType;
            }
        }
        public string PriorAttorneyName
        {
            set
            {
                pPriorAttorneyName = value;
            }
            get
            {
                return pPriorAttorneyName;
            }
        }
        public string CaseSummaryComments
        {
            set
            {
                pCaseSummaryComments = value;
            }
            get
            {
                return pCaseSummaryComments;
            }
        }
        public string ALROfficerName
        {
            get { return pALROfficerName; }
            set { pALROfficerName = value; }
        }
        public string ALROfficeBadgeNo
        {
            get { return pALROfficeBadgeNo; }
            set { pALROfficeBadgeNo = value; }
        }
        public string ALRPrecinct
        {
            get { return pALRPrecinct; }
            set { pALRPrecinct = value; }
        }
        public string ALROfficerAddress
        {
            get { return pALROfficerAddress; }
            set { pALROfficerAddress = value; }
        }
        public string ALROfficerCity
        {
            get { return pALROfficerCity; }
            set { pALROfficerCity = value; }
        }
        public int ALROfficerState
        {
            get { return pALROfficerState; }
            set { pALROfficerState = value; }
        }
        public string ALROfficerZip
        {
            get { return pALROfficerZip; }
            set { pALROfficerZip = value; }
        }
        public string ALRContactNumber
        {
            get { return pALRContactNumber; }
            set { pALRContactNumber = value; }
        }
        public int ALRIntoxilyzerTakenFlag
        {
            get { return pALRIntoxilyzerTakenFlag; }
            set { pALRIntoxilyzerTakenFlag = value; }
        }
        public string ALRArrestingAgency
        {
            get { return pALRArrestingAgency; }
            set { pALRArrestingAgency = value; }
        }
        public decimal ALROfficerMileage
        {
            get { return pALROfficerMileage; }
            set { pALROfficerMileage = value; }
        }
        public string ALRIntoxilyzerResult
        {
            get { return pALRIntoxilyzerResult; }
            set { pALRIntoxilyzerResult = value; }
        }
        public string ALRBTOFirstName
        {
            get { return pBTOFirstName; }
            set { pBTOFirstName = value; }
        }
        public string ALRBTOLastName
        {
            get { return pBTOLastName; }
            set { pBTOLastName = value; }
        }

        //Waqas 6342 08/25/2009 Changes for criminal and family 
        public bool? ALRBTOBTSSameFlag
        {
            get { return pBTOBTSSameFlag; }
            set { pBTOBTSSameFlag = value; }
        }
        public string ALRBTSFirstName
        {
            get { return pBTSFirstName; }
            set { pBTSFirstName = value; }
        }
        public string ALRBTSLastName
        {
            get { return pBTSLastName; }
            set { pBTSLastName = value; }
        }


        //Waqas 6342 08/12/2009 properties for observing officer

        public bool? IsALRArrestingObservingSame
        {
            get { return pIsALRArrestingObservingSame; }
            set { pIsALRArrestingObservingSame = value; }
        }
        public string ALROBSOfficerName
        {
            get { return pALROBSOfficerName; }
            set { pALROBSOfficerName = value; }
        }
        public string ALROBSOfficeBadgeNo
        {
            get { return pALROBSOfficeBadgeNo; }
            set { pALROBSOfficeBadgeNo = value; }
        }
        public string ALROBSPrecinct
        {
            get { return pALROBSPrecinct; }
            set { pALROBSPrecinct = value; }
        }
        public string ALROBSOfficerAddress
        {
            get { return pALROBSOfficerAddress; }
            set { pALROBSOfficerAddress = value; }
        }
        public string ALROBSOfficerCity
        {
            get { return pALROBSOfficerCity; }
            set { pALROBSOfficerCity = value; }
        }
        public int ALROBSOfficerState
        {
            get { return pALROBSOfficerState; }
            set { pALROBSOfficerState = value; }
        }
        public string ALROBSOfficerZip
        {
            get { return pALROBSOfficerZip; }
            set { pALROBSOfficerZip = value; }
        }
        public string ALROBSContactNumber
        {
            get { return pALROBSContactNumber; }
            set { pALROBSContactNumber = value; }
        }
        public string ALROBSArrestingAgency
        {
            get { return pALROBSArrestingAgency; }
            set { pALROBSArrestingAgency = value; }
        }
        public decimal ALROBSOfficerMileage
        {
            get { return pALROBSOfficerMileage; }
            set { pALROBSOfficerMileage = value; }
        }
        public bool? IsALRHearingRequired
        {
            get { return pIsALRHearingRequired; }
            set { pIsALRHearingRequired = value; }
        }

        public string ALRHearingRequiredAnswer
        {
            get { return pALRHearingRequiredAnswer; }
            set { pALRHearingRequiredAnswer = value; }
        }


        //Waqas 6599 09/19/2009 Ocupation , employer, is unemployed
        /// <summary>
        /// It represents Occupation of the client.
        /// </summary>
        public string Occupation
        {
            get { return pOccupation; }
            set { pOccupation = value; }
        }
        /// <summary>
        /// It represents Employer of the client
        /// </summary>
        public string Employer
        {
            get { return pEmployer; }
            set { pEmployer = value; }
        }
        /// <summary>
        /// It represents client if is unemployed.
        /// </summary>
        public bool IsUnemployed
        {
            get { return pIsUnemployed; }
            set { pIsUnemployed = value; }
        }
        /// <summary>
        /// It represents Vehicle Type of the traffic case client
        /// <para>"1" for Commercial Vehicle</para>
        /// <para>"2" for Motor Cycle</para>
        /// <para>"3" for Car</para>
        /// <para>"0" for No Selection</para>
        /// </summary>
        public int VehicleType
        {
            get { return pVehicleType; }
            set { pVehicleType = value; }
        }

        // Noufil 6766 02/11/2009
        /// <summary>
        ///     Primary ID of Language
        /// </summary>
        public int LanguageID { get; set; }

        /// <summary>
        ///  Contact Type description of Client of First Number
        /// </summary>
        public string ContactTypeDescription1 { get; set; }

        /// <summary>
        ///     Contact Type description of Client of Second Number
        /// </summary>
        public string ContactTypeDescription2 { get; set; }

        /// <summary>
        ///     Contact Type description of Client of Third Number
        /// </summary>
        public string ContactTypeDescription3 { get; set; }

        /// <summary>
        ///     Description of state of Client
        /// </summary>
        public string StateDescription { get; set; }


        // Rab Nawaz Khan 11473 10/23/2013 Added Caller ID property
        /// <summary>
        /// 
        /// </summary>
        public string CallerID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsUnknownCallerId { get; set; }

        public bool IsLeadsDetailsSubmitted { get; set; }
        
        #endregion

        #region Methods
        /// <summary>
        /// Method to search Case Information
        /// </summary>
        /// <param name="TicketNumber">Specify Ticket Number</param>
        /// <param name="SearchKeyWord1"></param>
        /// <param name="SearchKeyType1"></param>
        /// <param name="SearchKeyWord2"></param>
        /// <param name="SearchKeyType2"></param>
        /// <param name="Filter Type">Specify Client,Quote,NonClient,Archive,Jims</param>	
        public DataSet SearchCaseInfo(string TicketNumber, string SearchKeyWord1, int SearchKeyType1, string SearchKeyWord2, int SearchKeyType2, string lidad, string causenumberad, int FilterType, int Courtid, string SearchKeyWord3, int SearchKeyType3)
        {
            //Zeeshan Ahmed 3013 02/12/2008
            string[] key = { "@TicketNumber", "@SearchKeyWord1", "@SearchKeyType1", "@SearchKeyWord2", "@SearchKeyType2", "@LIDad", "@Causenumberad", "@FilterType", "@courtid", "@SearchKeyWord3", "@SearchKeyType3" };
            object[] value1 = { TicketNumber, SearchKeyWord1, SearchKeyType1, SearchKeyWord2, SearchKeyType2, lidad, causenumberad, FilterType, Courtid, SearchKeyWord3, SearchKeyType3 };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_Get_SearchCaseInfo_Ver_2", key, value1);
            return ds;
        }

        public DataSet SearchCaseInfo(string TicketNumber, string SearchKeyWord1, int SearchKeyType1, string SearchKeyWord2, int SearchKeyType2, int FilterType, int Courtid)
        {
            string[] key = { "@TicketNumber", "@SearchKeyWord1", "@SearchKeyType1", "@SearchKeyWord2", "@SearchKeyType2", "@FilterType", "@courtid" };

            object[] value1 = { TicketNumber, SearchKeyWord1, SearchKeyType1, SearchKeyWord2, SearchKeyType2, FilterType, Courtid };

            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_Get_SearchCaseInfo_Ver_2", key, value1);
            return ds;
        }

        /// <summary>
        /// 
        /// Insert non client Case Info in to tbltickets
        /// </summary>
        /// <param name="TicketNumber">Specify Ticket Number</param>
        /// <param name="EmpID">Specify Employee ID</param>

        public string[] GetCaseInfoFromAllNonClients(int ClientType, string TicketNumber, int EmpID, string Add1, string ZipCode, int MailerID, string MidNum, int IsTrafficClient)
        {
            string[] ticketidFlag = new string[2];
            string ticketFlag = String.Empty;

            //Kazim 3938 6/10/2008 Add istrafficclient parameter   

            switch (ClientType)
            {
                //Non Client
                case 999:
                    string[] key = { "@TicketNumber", "@EmpID", "@midnumber", "@address", "@ZipCode", "@mailerID", "@IsTrafficClient", "@sOutPut" };
                    object[] value1 = { TicketNumber, EmpID, MidNum, Add1, ZipCode, MailerID, IsTrafficClient, "" };
                    ticketFlag = Convert.ToString(ClsDb.InsertBySPArrRet("USP_HTS_Insert_CaseInfoFromNonClients_Ver_2", key, value1));
                    ticketidFlag = ticketFlag.Split(',');
                    break;
                //Archive
                case 5:
                    string[] key1 = { "@TicketNumber", "@EmpID", "@midnumber", "@address", "@ZipCode", "@IsTrafficClient", "@sOutPut" };
                    object[] value2 = { TicketNumber, EmpID, MidNum, Add1, ZipCode, IsTrafficClient, "" };
                    ticketFlag = Convert.ToString(ClsDb.InsertBySPArrRet("usp_HTS_Insert_CaseInfoFromArchive_Ver_2", key1, value2));
                    ticketidFlag = ticketFlag.Split(',');
                    break;
                //Jims
                case 3:
                    string[] key2 = { "@vc_TicketNumber", "@employeeid", "@mailerID", "@IsTrafficClient", "@sOutPut" };
                    object[] value3 = { TicketNumber, EmpID, MailerID, IsTrafficClient, "" };
                    ticketFlag = Convert.ToString(ClsDb.InsertBySPArrRet("USP_HTS_Insert_CaseInfoFromJIMS_Ver_2", key2, value3));
                    ticketidFlag = ticketFlag.Split(',');
                    break;
                //Civil
                case 6:
                    //Zeeshan Ahmed 3979 05/20/2008 Migrate Civil Case To Traffic System 
                    //Zeeshan Ahmed 4304 6/26/2008 Send Party Id In Mid Number Field To The Profile Migration Procedure
                    string[] key4 = { "@vc_TicketNumber", "@employeeid", "@mailerID", "@IsTrafficClient", "@PartyId", "@sOutPut" };
                    object[] value4 = { TicketNumber, EmpID, MailerID, IsTrafficClient, MidNum, "" };
                    ticketFlag = Convert.ToString(ClsDb.InsertBySPArrRet("USP_HTP_Insert_CaseInfoFromCivil_Ver_2", key4, value4));
                    ticketidFlag = ticketFlag.Split(',');
                    break;
            }
            return ticketidFlag;
        }


        /// <summary>
        /// Insert non client Case Info in to tbltickets
        /// </summary>
        /// <param name="TicketNumber">Specify Ticket Number</param>
        /// <param name="EmpID">Specify Employee ID</param> 
        /*public string GetCaseInfoFromNonClient(string TicketNumber,int EmpID,string Add1,string ZipCode,string MidNum)
        {
            string[] key    = {"@TicketNumber","@EmpID","@midnumber","@address","@ZipCode","@sOutPut"};

            object[] value1 = {TicketNumber,EmpID,MidNum,Add1,ZipCode,""};
			
            string ticketidFlag=Convert.ToString(ClsDb.InsertBySPArrRet("USP_HTS_Insert_CaseInfoFromNonClients_Ver_2",key,value1));
            return ticketidFlag;			
        }*/
        /// <summary>
        /// Insert Archive Case Info in to tbltickets
        /// </summary>
        /// <param name="TicketNumber">Specify Ticket Number</param>
        /// <param name="EmpID">Specify Employee ID</param>
        /*public string GetCaseInfoFromArchive(string TicketNumber,int EmpID,string Add1,string ZipCode,string MidNum)
        {
            string[] key    = {"@TicketNumber","@EmpID","@midnumber","@address","@ZipCode","@sOutPut"};

            object[] value1 = {TicketNumber,EmpID,MidNum,Add1,ZipCode,""};
			
            string ticketidFlag=Convert.ToString(ClsDb.InsertBySPArrRet("usp_HTS_Insert_CaseInfoFromArchive_Ver_2",key,value1));
            return ticketidFlag;			
        }*/
        /// <summary>
        /// Insert Jims Case Info in to tbltickets
        /// </summary>
        /// <param name="TicketNumber">Specify Ticket Number</param>
        /// <param name="EmpID">Specify Employee ID</param>
        /*public string GetCaseInfoFromJims(string TicketNumber,int EmpID)
        {
            string[] key    = {"@vc_TicketNumber","@employeeid","@sOutPut"};

            object[] value1 = {TicketNumber,EmpID,""};
			
            string ticketidFlag=Convert.ToString(ClsDb.InsertBySPArrRet("USP_HTS_Insert_CaseInfoFromJIMS_Ver_2",key,value1));
            return ticketidFlag;			
        }
        */

        /// <summary>
        /// Gets All Speeding From tblSpeeding
        /// </summary>
        public DataSet GetAllSpeeding()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_Get_AllSpeeding");
            return ds;
        }

        /*public DataSet GetMailerInfo(string TicketNumber,int CourtID)
        {
            DataSet ds=ClsDb.Get_DS_BySPByTwoParmameter("usp_HTS_Get","@TicketNumber",TicketNumber,"@CourtID",CourtID);
            return ds;
        }*/

        public DataSet ShowFeeCalculationStructure(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_PriceCalculationStructure", "@TicketID", TicketID);
            return ds;
        }

        /// <summary>
        /// Insert New Case
        /// </summary>
        public int AddCase()
        {
            //Ozair 6599 09/19/2009 occupation , employer, isemployed
            string[] key = { "@firstname", "@MiddleName", "@lastname", "@DOB", "@Contact1", "@ContactType1", "@LanguageSpeak", "@bondflag", "@AccidentFlag", "@CDLFlag", "@BondsRequiredflag", "@AdSourceId", "@GeneralComments", "@employeeid", "@FirmID", "@SpeedingID", "@CaseTypeID", "@Occupation", "@Employer", "@IsUnemployed", "@VehicleType", "@TicketId" };

            //added by khalid bug 2575 ,17-1-08
            if (DOB == "") { DOB = "1/1/1900"; }

            object[] value1 = { FirstName, MiddleName, LastName, Convert.ToDateTime(DOB), Contact1, ContactType1, LanguageSpeak, BondFlag, AccidentFlag, CDLFlag, BondsRequiredFlag, AdSourceID, GeneralComments, EmpID, FirmID, SpeedingID, caseType, Occupation, Employer, IsUnemployed, VehicleType, 0 };

            int ticketid = (int)ClsDb.InsertBySPArrRet("USP_HTS_Insert_NewCase", key, value1);
            return ticketid;

        }

        //update general comments by Atlas.
        public DataSet UpdateGeneralComments()
        {

            string[] key = { "@TicketID_PK", "@empid", "@GeneralComments", "ContactID" };   //SAEED 7844 06/02/2010 contactID added.
            object[] value1 = { TicketID, EmpID, GeneralComments, Contact_ID };   //SAEED 7844 06/02/2010 contactID added.

            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_UpdateGeneralComments", key, value1);
            return ds;


        }
        /// <summary>
        /// This method is used to update A/w followup date of client
        /// </summary>
        /// <param name="TicketId">Case Id</param>
        /// <param name="FollowUpDate">A/W Follo Up Date</param>
        /// <param name="EmployeeId">Rep Id</param>
        public void UpdateArrWaitingFollowUpDate(int TicketId, DateTime FollowUpDate, int EmployeeId)
        {
            //Kazim   3424 3/24/2008
            //Zeeshan 4629 04/09/2008 Send Rep Id in the procedure.
            string[] key = { "@TicketID_PK", "@ArrWaitingFollowUpDate", "@EmployeeId" };
            object[] value1 = { TicketId, FollowUpDate, EmployeeId };
            ClsDb.ExecuteSP("USP_HTP_Update_ArrWaitingFollowUpDate", key, value1);
        }


        public DataSet UpdateGeneralComments(int ticketide, int empid, string comment)
        {

            string[] key = { "@TicketID_PK", "@empid", "@GeneralComments" };
            object[] value1 = { ticketide, empid, comment };


            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_UpdateGeneralComments", key, value1);
            return ds;
        }

        //Add B Zeeshan, 4481, August 06, 2008
        /// <summary>
        /// Description: This will update the followupdate on Bond Reports.
        /// </summary>
        /// <param name="TicketId"></param>
        /// <param name="FollowUpDate"></param>
        /// <param name="empid"></param>
        public void UpdateBondFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID_PK", "@BondFollowUpDate", "@EmployeeID" }; //Muhammad Muneer 8285 9/17/2010 added the EmployeeID as the extra parameter
            object[] value1 = { TicketId, FollowUpDate, empid };//Muhammad Muneer 8285 9/17/2010 added the empId as the extra parameter

            ClsDb.ExecuteSP("USP_HTP_Update_BondFollowUpDate", key, value1);

        }

        /// <summary>
        ///     Noufil 4980 10/30/2008
        ///     THis function Update bond followupdate
        /// </summary>
        /// <param name="TicketId">Ticket id of Record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID</param>
        public void UpdateBondWiatingFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID_PK", "@date", "@empid" };
            object[] value1 = { TicketId, FollowUpDate, empid };

            ClsDb.ExecuteSP("USP_HTP_UPDATE_BondWaitingFollowUpDate", key, value1);
        }

        /// <summary>
        /// Update traffic waiting follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateTrafficWiatingFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID", "@EmployeeID", "@FollowUpDate", "@IsUpdateFollowUpDate" };
            object[] value1 = { TicketId, empid, FollowUpDate, true };

            ClsDb.ExecuteSP("usp_HTP_Update_TrafficWaitingFollowUpDate", key, value1);
        }
        ///Fahad 2/1/2009 5098
        /// <summary>
        /// Update Contract follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateContractFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID", "@FollowUpDate" };
            object[] value1 = { TicketId, FollowUpDate };
            ClsDb.ExecuteSP("USP_HTP_Update_COntractFollowUpdate", key, value1);
        }
        ///Waqas 5653 03/21/2009
        /// <summary>
        /// Update No LOR follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateNoLORFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID", "@FollowUpDate" };
            object[] value1 = { TicketId, FollowUpDate };
            ClsDb.ExecuteSP("USP_HTP_Update_NOLORFollowUpdate", key, value1);
        }

        ///Muhammad Nadir Siddiqui 9134 04/29/2011
        /// <summary>
        /// Update No LOR Confirmation follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateNoLORConfirmationFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID", "@FollowUpDate" };
            object[] value1 = { TicketId, FollowUpDate };
            ClsDb.ExecuteSP("USP_HTP_Update_NOLORConfirmationFollowUpdate", key, value1);
        }

        ///Fahad 5722 04/03/2009
        /// <summary>
        /// Update Non HMC  follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateNonHMCFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            if (TicketId > 1 && empid > 1)
            {
                string[] key = { "@TicketID", "@FollowUpDate" };
                object[] value1 = { TicketId, FollowUpDate };
                ClsDb.ExecuteSP("USP_HTP_Update_NonHMCFollowUpdate", key, value1);
            }
        }
        ///Fahad 5753 04/10/2009
        /// <summary>
        /// Update Split Case follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateSplitCaseFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            if (TicketId > 1 && empid > 1)
            {
                string[] key = { "@TicketID", "@FollowUpDate" };
                object[] value1 = { TicketId, FollowUpDate };
                ClsDb.ExecuteSP("USP_HTP_Update_SplitCaseFollowUpdate", key, value1);
                //bugTracker.AddNote(empid, "subject", "Split Flad has been Added", TicketId);
            }
        }
        //Saeed 8101 09/24/2010 method created.
        /// <summary>
        /// Update HCJP Auto Update Alert Follow up Date
        /// </summary>
        /// <param name="TicketId">Ticket Id</param>
        /// <param name="FollowUpDate">Follow up date</param>
        /// <param name="empid">Employee Id</param>
        public void UpdateHCJPAutoAlertFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            if (TicketId > 1 && empid > 1)
            {
                string[] key = { "@TicketID", "@FollowUpDate" };
                object[] value1 = { TicketId, FollowUpDate };
                ClsDb.ExecuteSP("USP_HTP_Update_HCJPAutoAlertFollowUpdate", key, value1);
            }
        }

        //Muhamamd Ali 7747 07/1/2010 --> Update Missing Cause follow Update.
        /// <summary>
        /// Update Missing Cause Follow update.
        /// 
        /// </summary>
        /// <param name="TicketId"> Ticket Id</param>
        /// <param name="FollowUpDate">Follow Update</param>
        /// <param name="empid">Employ Id</param>
        public void UpdateMissingCauseFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            if (TicketId > 1 && empid > 1)
            {
                string[] key = { "@TicketID", "@FollowUpDate" };
                object[] value1 = { TicketId, FollowUpDate };
                ClsDb.ExecuteSP("USP_HTP_Update_MissingCauseFollowUpdate", key, value1);

            }
        }


        ///Fahad 5753 04/10/2009
        /// <summary>
        /// Delete No Split Flag 
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="empid">Employee ID of user</param>
        public void DeleteNoSplitFlag(int TicketId, int empid)
        {
            if (TicketId > 1 && empid > 1)
            {
                string[] key = { "@TicketID" };
                object[] value1 = { TicketId };
                ClsDb.ExecuteSP("USP_HTP_Delete_NoSplitFlag", key, value1);
                //bugTracker.AddNote(empid, "subject", " No Split Flag has been Removed", TicketId);
            }
        }

        ///Waqas 5697 03/26/2009
        /// <summary>
        /// Update HMC FTA follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdateHMCFTAFollowUpDate(int TicketId, DateTime FollowUpDate, int empid, int IsRemoved)
        {
            string[] key = { "@TicketID", "@FollowUpDate", "IsRemoved" };
            object[] value1 = { TicketId, FollowUpDate, IsRemoved };
            ClsDb.ExecuteSP("USP_HTP_Update_HMCFTAFollowUpdate", key, value1);
        }


        /// <summary>
        /// Updates Case Info
        /// </summary>
        /// Sabir Khan 4636 08/19/2008 Update Case Type
        /// Abbas Qamar 10114 04-04-2012 DlNumber,SSNumber,SSNTypeId parameter added for HCCC,Fortbend,Montgoemry courts
        public bool UpdateCase()
        {

            try
            {
                //Waqas 6342 08/25/2009 Changes for criminal and family 
                // tahir 4786 10/08/2008 added payment plan needed flag...
                //Waqas 5864 06/26/2009 ALR Criminal changes
                //Waqas 6599 09/19/2009 occupation , employer, isemployed, vehicleType
                string[] key = { "@TicketID_PK", "@Firstname", "@MiddleName", "@Lastname", "@DOB", "@Contact1", "@ContactType1", "@LanguageSpeak", "@GeneralComments", "@BondFlag", "@AccidentFlag", "@CDLFlag", "@Activeflag", "@SpeedingID", "@FirmID", "@EmpID", "@LateFlag", "@CaseTypeID", "@HasPaymentPlan", 
                           "@HavePriorAttorney", "@PriorAttorneyType", "@PriorAttorneyName", "@CaseSummaryComments",  
                         "@ALROfficerName" , "@ALROfficerbadge", "@ALRPrecinct", "@ALROfficerAddress", "@ALROfficerCity", "@ALROfficerStateID", "@ALROfficerZip",
                         "@ALROfficerContactNumber", "@ALRIsIntoxilyzerTakenFlag",  "@ALRIntoxilyzerResult",  "@ALRBTOFirstName",  "@ALRBTOLastName", "@ALRBTOBTSSameFlag" ,
                         "@ALRBTSFirstName",  "@ALRBTSLastName",  "@ALRArrestingAgency",  "@ALRMileage" , "@IsALRArrestingObservingSame", "@ALRObservingOfficerName"
                        , "@ALRObservingOfficerBadgeNo", "@ALRObservingOfficerPrecinct", "@ALRObservingOfficerAddress", "@ALRObservingOfficerCity", "@ALRObservingOfficerState"
                        , "@ALRObservingOfficerZip", "@ALRObservingOfficerContact", "@ALRObservingOfficerArrestingAgency", "@ALRObservingOfficerMileage", "@IsALRHearingRequired", "@ALRHearingRequiredAnswer"
                        , "@Occupation", "@Employer", "@IsUnemployed", "@VehicleType","@DLnumber","@NoDl","@SSNumber","@SSNTypeId","@SSNOtherQuestion","@DLState", "@ArrStatusFlag"}; // Rab Nawaz Khan 10330 07/17/2012 Arrignment Status flage has been added in the prameters. . . 

                if (DOB == "" || DOB == "//") { DOB = "1/1/1900"; }
                object[] value1 = { TicketID, FirstName, MiddleName, LastName, Convert.ToDateTime(DOB), Contact1, ContactType1, LanguageSpeak, GeneralComments, BondFlag, AccidentFlag, CDLFlag, ActiveFlag, SpeedingID, FirmID, EmpID, latefee, caseType, hasPaymentPlan 
                              , HavePriorAttorney, PriorAttorneyType, PriorAttorneyName, CaseSummaryComments, 
                              ALROfficerName, ALROfficeBadgeNo, ALRPrecinct, ALROfficerAddress, ALROfficerCity, ALROfficerState, ALROfficerZip,
                              ALRContactNumber, ALRIntoxilyzerTakenFlag, ALRIntoxilyzerResult, ALRBTOFirstName, ALRBTOLastName, ALRBTOBTSSameFlag,
                              ALRBTSFirstName, ALRBTSLastName, ALRArrestingAgency, ALROfficerMileage, IsALRArrestingObservingSame, ALROBSOfficerName, ALROBSOfficeBadgeNo, ALROBSPrecinct, ALROBSOfficerAddress, 
                              ALROBSOfficerCity, ALROBSOfficerState, ALROBSOfficerZip, ALROBSContactNumber, ALROBSArrestingAgency, ALROBSOfficerMileage, IsALRHearingRequired, ALRHearingRequiredAnswer 
                              , Occupation, Employer, IsUnemployed , VehicleType,DLNumber,NoDL,SSN,SSNTypeID,SSNOtherQuestion,DLStateID, dayOfArrFlag}; // Rab Nawaz Khan 10330 07/17/2012 Arrignment Status flage value has been added in the prameters. . . 

                ClsDb.ExecuteSP("Usp_HTS_UpdateCaseInfo", key, value1, true);
                return true;
            }


            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Nasir 5381 01/15/2009 to get Is  General Comments Update from other side
        /// </summary>
        /// <param name="LastGerenalCommentsUpdatedate"></param>
        /// <returns></returns>
        /// 
        public bool IsLastGeneralCommentsDateUpdated(String LastGerenalCommentsUpdatedate)
        {
            string[] key = { "@TicketID_PK", "@LastGeneralCommentsDate" };
            object[] value = { TicketID, LastGerenalCommentsUpdatedate };
            object retval = ClsDb.ExecuteScalerBySp("USP_HTP_IsLastGeneralCommentsDateUpdated", key, value);
            gerenalcommentsoutput = !DBNull.Value.Equals(retval) && Convert.ToBoolean(retval);

            return gerenalcommentsoutput;

        }

        public int GetSpeedingId(int intTicketID)
        {
            string[] key = { "@TicketID" };
            object[] value1 = { intTicketID };

            return (int)Convert.ChangeType(ClsDb.ExecuteScalerBySp("USP_HTS_GET_SpeedingID", key, value1), typeof(int));
        }


        public bool CalculateFee(int TicketId, bool LockFlag, int ViolCount, bool AFlag, bool CFlag, int Speeding, bool Conti, double IAdj, double FAdj, bool calForPC, int iEmpId, bool UpdateContinuance, bool HasPaymentPlan, bool ArrFlag) // Rab Nawaz Khan 10330 07/17/2012 Arraignment Status flag has been added in the method paramters. . . 
        {
            double dBasePrice = 0;
            double dSecondaryPrice = 0;
            double dBondBase = 0;
            double dBondSecondary = 0;
            double dBaseFeeCharge = 0;
            double dContinuanceAmount = 0;
            double dInitialBaseFee = 0;
            double dInitialTotalFeeCharged = 0;
            double dCalculatedTotalFee = 0;
            double dInitialAdjustment = 0;
            double dFinalAdjustment = 0;
            double dSpeedingAmount = 0;
            double dAccidentAmount = 0;
            double dCDLAmount = 0;
            int idx = 0;
            double temp = 0;
            int HighestBaseRowId;
            double HighestBase = 0;
            int HighestBaseViolationId = 0;
            double TotalSecondaryFee = 0;
            double dLateFee = 0;
            double dPlanFee = 0;
            string[] key = { "@TicketId" };
            object[] value1 = { TicketId };
            // Rab Nawaz Khan 10330 07/17/2012 Variable added for the Arraignment status flag amount calculation. . . 
            double ArrWithCurrentCourtDateAmount = 0;
            try
            {

                // getting raw data into a temporary data set......
                DataSet dsTemp1 = ClsDb.Get_DS_BySPArr("usp_hts_get_violationprices", key, value1);



                /*  DataView dv = new DataView(dsTemp1.Tables["0"],
                                 "Country = 'USA'",
                                 "ContactName",
                                 DataViewRowState.CurrentRows);
                  */


                if (dsTemp1.Tables[0].Rows.Count < ViolCount)
                {
                    throw new Exception("Pricing plan is not available for one of the violations or not tagged with any violation category. Please first update the pricing plan for selected court or the violation category.");
                }

                // temporary dataset for storing highest base violation....
                DataSet dsTemp2 = new DataSet();
                DataTable dt = new DataTable("temp2");
                dsTemp2.Tables.Add(dt);
                dsTemp2.Tables[0].Columns.Add("TicketsViolationId");
                dsTemp2.Tables[0].Columns["TicketsViolationId"].DataType = typeof(int);
                dsTemp2.Tables[0].Columns.Add("HighestBase");
                dsTemp2.Tables[0].Columns["HighestBase"].DataType = typeof(double);

                #region Creating DataSet for price calculator
                //CREATING DATASET FOR STORING PRICE CALCULATION STEPS THAT WILL BE USED FOR PRICE CALCULATOR...
                DataSet dsPrice = new DataSet();
                DataTable tblPricingCalculator = new DataTable("tblPricingCalculator");
                dsPrice.Tables.Add(tblPricingCalculator);

                dsPrice.Tables[0].Columns.Add("TicketId_pk");
                dsPrice.Tables[0].Columns["TicketId_pk"].DataType = typeof(int);

                dsPrice.Tables[0].Columns.Add("TicketsViolationID");
                dsPrice.Tables[0].Columns["TicketsViolationID"].DataType = typeof(int);

                dsPrice.Tables[0].Columns.Add("PriceType");
                dsPrice.Tables[0].Columns["PriceType"].DataType = typeof(string);

                dsPrice.Tables[0].Columns.Add("CalculationDescription");
                dsPrice.Tables[0].Columns["CalculationDescription"].DataType = typeof(string);

                dsPrice.Tables[0].Columns.Add("Amount");
                dsPrice.Tables[0].Columns["Amount"].DataType = typeof(double);


                #endregion

                #region Setting Bond Flag
                //-----------------------------------------------------------------------------------
                // CHANGED BY TAHIR AHMED DT:02/07/2007
                // IF ANY ONE OF THE UNDERLYING VIOLATION HAS BOND FLAG = TRUE 
                // THEN TREAT ALL VIOLATIONS WITH BOND FLAG = TRUE........
                bool bFlag = false;
                if (dsTemp1.Tables[0].Select("underlyingbondflag = true").Length > 0)
                    bFlag = true;

                if (bFlag)
                {
                    foreach (DataRow dr2 in dsTemp1.Tables[0].Rows)
                        dr2["underlyingbondflag"] = true;
                }
                //-----------------------------------------------------------------------------------
                #endregion

                #region Calculating Highest Base / Bond Base price
                // GETTING HIGHEST BASE / BOND BASE PRICE......................
                foreach (DataRow dr in dsTemp1.Tables[0].Rows)
                {
                    DataRow drTemp1 = dsTemp2.Tables[0].NewRow();
                    DataRow drTemp2 = dsPrice.Tables[0].NewRow();

                    // IF BOND FLAG IS 'YES' FOR A VIOLATION THEN USE BOND RELATED PRICES ELSE USE BASE PRICES...
                    bool bondflag = ((bool)(Convert.ChangeType(dr["underlyingbondflag"], typeof(bool))));
                    if (bondflag)
                    {
                        drTemp1["TicketsViolationId"] = dr["ticketsviolationid"];
                        drTemp2["TicketId_pk"] = TicketId;
                        drTemp2["TicketsViolationID"] = dr["ticketsviolationid"];


                        temp = ((double)(Convert.ChangeType(dr["bondbase"], typeof(double))));
                        // if bond base price is available........
                        if (temp != 0)
                        {
                            // Rab Nawaz Khan 10330 07/16/2012 Specified amount description has been replaced with N/A. . . 
                            if (temp == 45)
                            {
                                dBondBase = temp;
                                drTemp2["PriceType"] = "N/A";
                                drTemp2["CalculationDescription"] = "N/A";
                                drTemp2["Amount"] = temp;
                            }
                            else
                            {

                                dBondBase = temp;
                                drTemp2["PriceType"] = "Bond Base";
                                drTemp2["CalculationDescription"] = "Highest Bond Base Price for violation";
                                drTemp2["Amount"] = temp;
                            }
                        }
                        else
                        {
                            temp = ((double)(Convert.ChangeType(dr["bondamount"], typeof(double))));
                            // if bond amount is available.........
                            if (temp != 0)
                            {
                                dBondBase = ((double)(Convert.ChangeType(dr["bondbasepercentage"], typeof(double)))) / 100 * temp;
                                drTemp2["PriceType"] = "Bond Base %";
                                drTemp2["CalculationDescription"] = ((double)(Convert.ChangeType(dr["bondbasepercentage"], typeof(double)))) + "% of Bond Amount " + temp;
                                drTemp2["Amount"] = dBondBase;

                            }
                            else
                            // if bond amount amount is not available, using bond assumption amount.....
                            {
                                dBondBase = ((double)(Convert.ChangeType(dr["BondBasePercentage"], typeof(double)))) / 100 * ((double)(Convert.ChangeType(dr["BondAssumption"], typeof(double))));
                                drTemp2["PriceType"] = "Bond Base %";
                                drTemp2["CalculationDescription"] = ((double)(Convert.ChangeType(dr["bondbasepercentage"], typeof(double)))) + "% of Bond Assumption " + ((double)(Convert.ChangeType(dr["BondAssumption"], typeof(double))));
                                drTemp2["Amount"] = dBondBase;
                            }
                        }

                        drTemp1["HighestBase"] = dBondBase;
                        dsTemp2.Tables[0].Rows.Add(drTemp1);


                        if (dBondBase > HighestBase)
                        {
                            HighestBaseRowId = idx;
                            HighestBase = dBondBase;
                            HighestBaseViolationId = ((int)(Convert.ChangeType(dr["ticketsviolationid"], typeof(int))));
                            dsPrice.Tables[0].Rows.Add(drTemp2);

                        }
                        else
                        {
                            drTemp2.Delete();
                        }

                    }

                        // if bond flag  is false.......
                    else
                    {
                        drTemp1["TicketsViolationId"] = dr["ticketsviolationid"];
                        drTemp2["TicketId_pk"] = TicketId;
                        drTemp2["TicketsViolationID"] = dr["ticketsviolationid"];

                        temp = ((double)(Convert.ChangeType(dr["BasePrice"], typeof(double))));
                        // if base price is available.......
                        if (temp != 0)
                        {
                            if (temp == 45)
                            {
                                dBasePrice = temp;
                                drTemp2["PriceType"] = "N/A";
                                drTemp2["CalculationDescription"] = "N/A";
                                drTemp2["Amount"] = temp;
                            }
                            else
                            {
                                dBasePrice = temp;
                                drTemp2["PriceType"] = "Base Price";
                                drTemp2["CalculationDescription"] = "Highest Base Price";
                                drTemp2["Amount"] = temp;
                            }

                        }
                        else
                        {
                            temp = ((double)(Convert.ChangeType(dr["FineAmount"], typeof(double))));
                            // if fine amount is available, use basepercentage of fine amount....
                            if (temp != 0)
                            {
                                dBasePrice = ((double)(Convert.ChangeType(dr["BasePercentage"], typeof(double)))) / 100 * temp;
                                drTemp2["PriceType"] = "Base %";
                                drTemp2["CalculationDescription"] = ((double)(Convert.ChangeType(dr["BasePercentage"], typeof(double)))) + "% of Fine Amount " + temp;
                                drTemp2["Amount"] = dBasePrice;

                            }
                            else
                            {
                                // if fine amount is not  available, use basepercentage of fine assumption amount.......
                                dBasePrice = ((double)(Convert.ChangeType(dr["BasePercentage"], typeof(double)))) / 100 * ((double)(Convert.ChangeType(dr["FineAssumption"], typeof(double))));
                                drTemp2["PriceType"] = "Base %";
                                drTemp2["CalculationDescription"] = ((double)(Convert.ChangeType(dr["BasePercentage"], typeof(double)))) + "% of Fine Assumption " + ((double)(Convert.ChangeType(dr["FineAssumption"], typeof(double))));
                                drTemp2["Amount"] = dBasePrice;

                            }
                        }

                        drTemp1["HighestBase"] = dBasePrice;
                        dsTemp2.Tables[0].Rows.Add(drTemp1);
                        if (dBasePrice > HighestBase)
                        {
                            HighestBaseRowId = idx;
                            HighestBase = dBasePrice;
                            HighestBaseViolationId = ((int)(Convert.ChangeType(dr["ticketsviolationid"], typeof(int))));
                            if (dsPrice.Tables[0].Rows.Count > 0)
                            {
                                dsPrice.Tables[0].Rows[0].Delete();
                            }
                            dsPrice.Tables[0].Rows.Add(drTemp2);
                        }
                        else
                        {
                            drTemp2.Delete();
                        }

                    }
                    idx++;
                }

                // resetting counter......
                idx = 1;
                #endregion

                #region Calculating Secondary / Bond Secondary prices
                // CALCULATING SECONDARY PRICES FOR THE REMAINING VIOLATIONS.............
                foreach (DataRow dr in dsTemp1.Tables[0].Rows)
                {
                    if (((int)(Convert.ChangeType(dr["ticketsviolationid"], typeof(int)))) != HighestBaseViolationId)
                    {
                        DataRow drTemp1 = dsPrice.Tables[0].NewRow();
                        drTemp1["TicketId_pk"] = TicketId;
                        drTemp1["TicketsViolationID"] = dr["ticketsviolationid"];


                        bool bondflag = ((bool)(Convert.ChangeType(dr["underlyingbondflag"], typeof(bool))));
                        if (bondflag)
                        {
                            temp = ((double)(Convert.ChangeType(dr["BondSecondary"], typeof(double))));
                            if (temp != 0)
                            {
                                if (temp == 45)
                                {
                                    dBondSecondary = temp;
                                    drTemp1["PriceType"] = "N/A";
                                    drTemp1["CalculationDescription"] = "N/A";
                                    drTemp1["Amount"] = temp;
                                }
                                else
                                {
                                    dBondSecondary = temp;
                                    drTemp1["PriceType"] = "Bond Secondary";
                                    drTemp1["CalculationDescription"] = "Bond Secondary amount for the violation";
                                    drTemp1["Amount"] = temp;
                                }

                            }
                            else
                            {
                                temp = ((double)(Convert.ChangeType(dr["BondAmount"], typeof(double))));
                                if (temp != 0)
                                {
                                    dBondSecondary = ((double)(Convert.ChangeType(dr["BondSecondaryPercentage"], typeof(double)))) / 100 * temp;
                                    drTemp1["PriceType"] = "Bond Secondary %";
                                    drTemp1["CalculationDescription"] = ((double)(Convert.ChangeType(dr["BondSecondaryPercentage"], typeof(double)))) + "% of Bond  amount " + temp;
                                    drTemp1["Amount"] = dBondSecondary;

                                }
                                else
                                {
                                    dBondSecondary = ((double)(Convert.ChangeType(dr["BondSecondaryPercentage"], typeof(double))) / 100 * ((double)(Convert.ChangeType(dr["BondAssumption"], typeof(double)))));
                                    drTemp1["PriceType"] = "Bond Secondary %";
                                    drTemp1["CalculationDescription"] = ((double)(Convert.ChangeType(dr["BondSecondaryPercentage"], typeof(double)))) + "% of Bond  Assumption " + ((double)(Convert.ChangeType(dr["BondAssumption"], typeof(double))));
                                    drTemp1["Amount"] = dBondSecondary;

                                }
                            }
                            TotalSecondaryFee += dBondSecondary;
                            dsPrice.Tables[0].Rows.Add(drTemp1);
                        }
                        else
                        {
                            temp = ((double)(Convert.ChangeType(dr["SecondaryPrice"], typeof(double))));
                            if (temp != 0)
                            {
                                if (temp == 45)
                                {
                                    dSecondaryPrice = temp;
                                    drTemp1["PriceType"] = "N/A";
                                    drTemp1["CalculationDescription"] = "N/A";
                                    drTemp1["Amount"] = temp;
                                }
                                else
                                {
                                    dSecondaryPrice = temp;
                                    drTemp1["PriceType"] = "Secondary Price";
                                    drTemp1["CalculationDescription"] = "Secondary Price for the violation";
                                    drTemp1["Amount"] = temp;
                                }

                            }
                            else
                            {
                                temp = ((double)(Convert.ChangeType(dr["FineAmount"], typeof(double))));
                                if (temp != 0)
                                {
                                    dSecondaryPrice = (double)(Convert.ChangeType(dr["SecondaryPercentage"], typeof(double))) / 100 * temp;
                                    drTemp1["PriceType"] = "Secondary %";
                                    drTemp1["CalculationDescription"] = ((double)(Convert.ChangeType(dr["SecondaryPercentage"], typeof(double)))) + "% of Fine Amount " + temp;
                                    drTemp1["Amount"] = dSecondaryPrice;

                                }
                                else
                                {
                                    dSecondaryPrice = ((double)(Convert.ChangeType(dr["SecondaryPercentage"], typeof(double))) / 100 * ((double)(Convert.ChangeType(dr["FineAssumption"], typeof(double)))));
                                    drTemp1["PriceType"] = "Secondary %";
                                    drTemp1["CalculationDescription"] = ((double)(Convert.ChangeType(dr["SecondaryPercentage"], typeof(double)))) + "% of Fine Assumption " + ((double)(Convert.ChangeType(dr["FineAssumption"], typeof(double))));
                                    drTemp1["Amount"] = dSecondaryPrice;

                                }
                            }
                            TotalSecondaryFee += dSecondaryPrice;
                            dsPrice.Tables[0].Rows.Add(drTemp1);

                        }
                    }
                    idx++;
                }

                #endregion

                dBaseFeeCharge = HighestBase + TotalSecondaryFee;
                int cAmount = 0;

                try
                {
                    cAmount = (int)ClsDb.ExecuteScalerBySp("Usp_HTS_GET_ContinuanceAmount");
                }
                catch
                {
                    cAmount = 50;
                }
                if (UpdateContinuance)
                {
                    if (Conti)
                    {
                        //dContinuanceAmount = 50;

                        dContinuanceAmount = cAmount * (((int)ClsDb.ExecuteScalerBySp("USP_HTS_Get_ContinuanceCount_By_TicketID", key, value1)) + 1);
                    }
                    else
                    {
                        dContinuanceAmount = cAmount * ((int)ClsDb.ExecuteScalerBySp("USP_HTS_Get_ContinuanceCount_By_TicketID", key, value1)) - cAmount;
                        // Remove Continuance Amount Logic

                        if (dContinuanceAmount < 0)
                            dContinuanceAmount = 0;

                    }
                }
                else
                {
                    dContinuanceAmount = cAmount * (((int)ClsDb.ExecuteScalerBySp("USP_HTS_Get_ContinuanceCount_By_TicketID", key, value1)));
                }

                DataRow dr6 = dsPrice.Tables[0].NewRow();
                dr6["Ticketid_pk"] = TicketId;
                dr6["PriceType"] = "Additional Charges";
                dr6["Amount"] = dContinuanceAmount;
                dr6["CalculationDescription"] = "Continuance";
                dsPrice.Tables[0].Rows.Add(dr6);


                dInitialAdjustment = IAdj;
                DataRow dr7 = dsPrice.Tables[0].NewRow();
                dr7["Ticketid_pk"] = TicketId;
                dr7["PriceType"] = "Additional Charges";
                dr7["Amount"] = dInitialAdjustment;
                dr7["CalculationDescription"] = "InitialAdj";
                dsPrice.Tables[0].Rows.Add(dr7);


                dFinalAdjustment = FAdj;
                DataRow dr8 = dsPrice.Tables[0].NewRow();
                dr8["Ticketid_pk"] = TicketId;
                dr8["PriceType"] = "Additional Charges";
                dr8["Amount"] = dFinalAdjustment;
                dr8["CalculationDescription"] = "FinalAdj";
                dsPrice.Tables[0].Rows.Add(dr8);


                string[] key5 = { "@SpeedingID", "@AccFlag", "CDLFlag", "@LateFlag", "@HasPaymentPlan" };
                object[] value5 = { Speeding, AFlag, CFlag, latefee, HasPaymentPlan };

                //SqlDataReader drAdditional = ClsDb.Get_DR_BySPArr("USP_HTS_GET_AdditionalAmount",key5, value5);
                IDataReader drAdditional = ClsDb.Get_DR_BySPArr("USP_HTS_GET_AdditionalAmount", key5, value5);
                while (drAdditional.Read())
                {
                    if (Speeding == 0)
                        dSpeedingAmount = 0;
                    else
                        dSpeedingAmount = ((double)(Convert.ChangeType(drAdditional.GetValue(0), typeof(double))));

                    DataRow dr2 = dsPrice.Tables[0].NewRow();
                    dr2["Ticketid_pk"] = TicketId;
                    dr2["PriceType"] = "Additional Charges";
                    dr2["Amount"] = dSpeedingAmount;
                    dr2["CalculationDescription"] = "Speeding";
                    dsPrice.Tables[0].Rows.Add(dr2);

                    if (AFlag == false)
                        dAccidentAmount = 0;
                    else
                        dAccidentAmount = ((double)(Convert.ChangeType(drAdditional.GetValue(1), typeof(double))));

                    DataRow dr3 = dsPrice.Tables[0].NewRow();
                    dr3["Ticketid_pk"] = TicketId;
                    dr3["PriceType"] = "Additional Charges";
                    dr3["Amount"] = dAccidentAmount;
                    dr3["CalculationDescription"] = "Accident";
                    dsPrice.Tables[0].Rows.Add(dr3);

                    if (CFlag == false)
                        dCDLAmount = 0;
                    else
                        dCDLAmount = ((double)(Convert.ChangeType(drAdditional.GetValue(2), typeof(double))));

                    DataRow dr4 = dsPrice.Tables[0].NewRow();
                    dr4["Ticketid_pk"] = TicketId;
                    dr4["PriceType"] = "Additional Charges";
                    dr4["Amount"] = dCDLAmount;
                    dr4["CalculationDescription"] = "CDL";
                    dsPrice.Tables[0].Rows.Add(dr4);


                    if (latefee == 0)
                        dLateFee = 0;

                    else
                        dLateFee = ((double)(Convert.ChangeType(drAdditional.GetValue(3), typeof(double))));

                    DataRow dr5_ = dsPrice.Tables[0].NewRow();
                    dr5_["Ticketid_pk"] = TicketId;
                    dr5_["PriceType"] = "Additional Charges";
                    dr5_["Amount"] = dLateFee;
                    dr5_["CalculationDescription"] = "Late Fee";
                    dsPrice.Tables[0].Rows.Add(dr5_);

                    // tahir 4786 10/08/2008
                    if (!HasPaymentPlan)
                        dPlanFee = 0;

                    else
                        dPlanFee = ((double)(Convert.ChangeType(drAdditional.GetValue(4), typeof(double))));

                    DataRow dr6_ = dsPrice.Tables[0].NewRow();
                    dr6_["Ticketid_pk"] = TicketId;
                    dr6_["PriceType"] = "Additional Charges";
                    dr6_["Amount"] = dPlanFee;
                    dr6_["CalculationDescription"] = "Payment plan charges";
                    dsPrice.Tables[0].Rows.Add(dr6_);
                    // end 4786


                    //// Rab Nawaz Khan 10330 07/03/2012 if ArrWithCurrentDateStatusFlag is true the add 50 dollars in calculation. . . 
                    if (!ArrFlag)
                        ArrWithCurrentCourtDateAmount = 0;
                    else
                        ArrWithCurrentCourtDateAmount = ((double)(Convert.ChangeType(drAdditional.GetValue(5), typeof(double))));

                    DataRow dr7_ = dsPrice.Tables[0].NewRow();
                    dr7_["Ticketid_pk"] = TicketId;
                    dr7_["PriceType"] = "Additional Charges";
                    dr7_["Amount"] = ArrWithCurrentCourtDateAmount;
                    dr7_["CalculationDescription"] = "Day Of Arraignment";
                    dsPrice.Tables[0].Rows.Add(dr7_);
                    // END 10330

                }

                dBaseFeeCharge += dAccidentAmount + dCDLAmount + dSpeedingAmount + dLateFee + dPlanFee + ArrWithCurrentCourtDateAmount;

                //ROUNDING OFF THE FEE........
                //LOGIC: IF MOD OF FEE AMOUNT WITH 5 IS GREATER THAN ZERO THAN ADD 5 - MOD TO THE FEE AMOUNT.
                //IN OTHER WORDS,  FEE AMOUNT =  5 *  CEIL VALUE ( FEE / 5 )
                double dRoundOff = 0;
                double dTemp = 0;

                dTemp = (dBaseFeeCharge % 5);

                if (dTemp > 0)
                {
                    dBaseFeeCharge += (5 - dTemp);
                    dRoundOff = (5 - dTemp);
                }

                // ADDING ROUNDING OFF AMOUNT IN CALCULATION SUMMARY....
                DataRow dr5 = dsPrice.Tables[0].NewRow();
                dr5["Ticketid_pk"] = TicketId;
                dr5["PriceType"] = "Additional Charges";
                dr5["Amount"] = dRoundOff;
                dr5["CalculationDescription"] = "Round Off";
                dsPrice.Tables[0].Rows.Add(dr5);


                if (calForPC)
                {
                    //dInitialAdjustment = (dBaseFeeCharge - (ViolCount * 50)) * -1;
                    //dBaseFeeCharge-= dInitialAdjustment;
                }

                dInitialBaseFee = dBaseFeeCharge;
                dInitialTotalFeeCharged = dBaseFeeCharge + dContinuanceAmount;
                dCalculatedTotalFee = dBaseFeeCharge + dContinuanceAmount;

                string[] key2 = { "@TicketId", "@BaseFeeCharge", "@ContinuanceAmount", "@InitialBaseFee", "@InitialAdjustment", "@FinalAdjustment", "@InitialTotalFeeCharged", "@CalculatedTotalFee", "@LockFlag", "@EmpId" };
                object[] value2 = { TicketId, dBaseFeeCharge, dContinuanceAmount, dInitialBaseFee, dInitialAdjustment, dFinalAdjustment, dInitialTotalFeeCharged, dCalculatedTotalFee, LockFlag, iEmpId };

                //ClsDb.ExecuteSP( "USP_HTS_UPDATE_ViolationPrices", key2, value2);
                bool bPriceChanged = false;
                bPriceChanged = Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTS_UPDATE_ViolationPrices", key2, value2));
                UpdatePriceCalculator(dsPrice);
                //if (LockFlag == true )
                //	LockCase(TicketId);
                drAdditional.Close();
                return bPriceChanged;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }



        }

        /*private void LockCase(int TicketId)
        {
            string[] key2 = {"@TicketId"};
            object[] value2 = {TicketId};
		
            ClsDb.ExecuteSP("USP_HTS_UPDATE_LockCase", key2, value2);
        }*/

        private void UpdatePriceCalculator(DataSet dsPrice)
        {
            if (dsPrice != null && dsPrice.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsPrice.Tables[0].Rows)
                {
                    string[] key = { "@TicketId_pk", "@TicketsViolationId", "@PriceType", "@CalculationDescription", "@Amount" };
                    object[] value1 = { dr[0], dr[1], dr[2], dr[3], dr[4] };
                    ClsDb.ExecuteSP("USP_HTS_Insert_PriceCalculationSteps", key, value1);
                }
            }

        }

        /// <summary>
        /// Get Case Info by TicketID
        /// </summary>
        /// <param name="TicketID">TicketID whose Case info to get</param>
        public DataSet GetCase(int TicketID)
        {
            //ClsDb.DBBeginTransaction();
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };

            DataSet ds = ClsDb.Get_DS_BySPArr("usp_HTS_GetCaseInfo", key1, value1);
            //ClsDb.DBTransactionCommit();
            return ds;
        }
        /// <summary>
        /// Get All Orignal Quote Status
        /// </summary>
        public DataSet GetAllQuoteResult()
        {
            DataSet ds = ClsDb.Get_DS_BySP("usp_HTS_Get_AllQuoteResult");
            return ds;
        }

        /// <summary>
        /// Get Violation Quote Info
        /// </summary>
        /// <param name="TicketID">TicketID whose Quote info to get</param>
        public DataSet GetAllViolationQuote(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_AllViolationQuote", "@TicketID", TicketID);
            return ds;
        }
        /// <summary>
        /// Updates Violation Quote Info
        /// </summary>
        public bool UpdateViolationQuoteInfo()
        {
            string[] key = { "@QuoteID", "@TicketID_FK", "@QuoteResultID", "@QuoteResultDate", "@FollowUPID", "@FollowUPDate", "@FollowUpYN", "@QuoteComments", "@CallBackDate", "@AppointmentDate", "@AppointmentTime" };

            object[] value1 = { QuoteID, TicketID, QuoteResultID, QuoteResultDate, FollowUPID, FollowUPDate, FollowUpYN, QuoteComments, CallBackDate, AppointmentDate, AppointmentTime };
            try
            {
                ClsDb.ExecuteSP("Usp_HTS_Update_ViolationQuoteInfo", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        /// <summary>
        /// Inserts Violation Quote Info
        /// </summary>
        /// <param name="TicketID">TicketID whose Quote info to insert </param>
        public bool InsertViolationQuoteInfo()
        {
            string[] key = { "@TicketID_FK", "@QuoteResultID", "@QuoteResultDate", "@FollowUPID", "@FollowUPDate", "@FollowUpYN", "@QuoteComments", "@CallBackDate", "@AppointmentDate", "@AppointmentTime" };

            object[] value1 = { TicketID, QuoteResultID, QuoteResultDate, FollowUPID, FollowUPDate, FollowUpYN, QuoteComments, CallBackDate, AppointmentDate, AppointmentTime };
            try
            {
                ClsDb.ExecuteSP("USP_HTS_Insert_ViolationQuoteInfo", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        /// <summary>
        /// Update Client Information
        /// </summary>		
        public bool UpdateClientInfo()
        {
            try
            {
                ClsDb.DBBeginTransaction();


                //@LanguageSpeak,@BondFlag, @Midnum,@AccidentFlag,@PymtComments, @CDLFlag,@AdsourceID,Activeflag,@calculatedtotalfee,
                /*Remarked By Adil string[] key1 ={"@TicketID_PK","@EmpID","@Firstname","@Lastname","@MiddleName","@Address1","@Address2",
                                                    "@Contact1","@ContactType1","@Contact2","@ContactType2","@Contact3","@ContactType3",
                                                    "@DLNumber","@DLState","@DOB","@SSNumber","@Email","@City","@Stateid_FK","@Zip","@Race",
                                                    "@gender","@Height","@Weight","@HairColor","@Eyes",
                                                    "@GeneralComments","@ContactComments","@SettingComments","@TrialComments",
                                                    "@YDS","@Addressconfirmflag", "@EmailNotAvailable","@DLNotAvailable","@ContinuanceComments",
                                                    "@ContinuanceStatus","@ContinuanceDate","@FirmID","@ContinuanceFlag","@WalkInClientFlag", "@isPR", "@hasConsultationComments","@dp2","@dpc","@flag1","@updatebarcodeinfo","@Midnum"};


                                object[] value1 ={TicketID,EmpID,FirstName,LastName,MiddleName,Address1,Address2,
                                                    Contact1,ContactType1,Contact2,ContactType2,Contact3,ContactType3,
                                                    DLNumber,DLStateID,Convert.ToDateTime(DOB),SSN,Email,City,StateID,Zip,Race,
                                                    Gender,Height,Weight,HairColor,EyeColor,
                                                    GeneralComments,ContactComments,SettingsComments,TrialComments,
                                                    YDS,AddressConfirmFlag, EmailNotAvailable,NoDL,continuancecomments,continuancestatus,continuancedate,firmID,continuanceflag,WalkInClientFlag, IsPR, HasConsultationComments,dp2,dpc,flag1,updatebarcodeinfo,SPN};
                */

                string[] key1 ={"@TicketID_PK","@EmpID","@Firstname","@Lastname","@MiddleName","@Address1","@Address2",
									"@Contact1","@ContactType1","@Contact2","@ContactType2","@Contact3","@ContactType3",
									"@DLNumber","@DLState","@DOB","@SSNumber","@Email","@City","@Stateid_FK","@Zip","@Race",
									"@gender","@Height","@Weight","@HairColor","@Eyes",
									"@YDS","@Addressconfirmflag", "@EmailNotAvailable","@DLNotAvailable",
                                    "@ContinuanceStatus","@ContinuanceDate","@FirmID","@ContinuanceFlag","@WalkInClientFlag", "@isPR","@hasConsultationComments","@dp2","@dpc","@flag1","@updatebarcodeinfo","@Midnum","@SmsFlag1","@SmsFlag2","@SmsFlag3"};

                // Noufil 5884 07/03/2009 SMS FLag parameter added.
                object[] value1 ={TicketID,EmpID,FirstName,LastName,MiddleName,Address1,Address2,
									Contact1,ContactType1,Contact2,ContactType2,Contact3,ContactType3,
									DLNumber,DLStateID,Convert.ToDateTime(DOB),SSN,Email,City,StateID,Zip,Race,
									Gender,Height,Weight,HairColor,EyeColor,
									YDS,AddressConfirmFlag, EmailNotAvailable,NoDL,continuancestatus,continuancedate,firmID,continuanceflag,WalkInClientFlag, IsPR, HasConsultationComments, dp2,dpc,flag1,updatebarcodeinfo,SPN,smsflag1,smsflag2,smsflag3};


                ClsDb.ExecuteSP("USP_HTS_Update_ClientInfo", key1, value1);
                ClsDb.DBTransactionCommit();

                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                ClsDb.DBTransactionRollback();
                return false;
            }

        }
        /// <summary>
        /// Gets Client Information on TicketID
        /// </summary>
        /// <param name="TicketID">TicketID in order to get client Information</param>
        public bool GetClientInfo(int TicketID)
        {
            try
            {
                string[] key1 = { "@TicketID_PK" };
                object[] value1 = { TicketID };
                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_Get_ClientInfo", key1, value1);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FirstName = ds.Tables[0].Rows[0]["Firstname"].ToString().Trim();
                    MiddleName = ds.Tables[0].Rows[0]["MiddleName"].ToString().Trim();
                    LastName = ds.Tables[0].Rows[0]["LastName"].ToString().Trim();
                    DOB = ds.Tables[0].Rows[0]["DOB"].ToString().Trim();
                    DLNumber = ds.Tables[0].Rows[0]["DLNumber"].ToString().Trim();
                    DLStateID = Convert.ToInt32(ds.Tables[0].Rows[0]["DLState"]);
                    Gender = ds.Tables[0].Rows[0]["Gender"].ToString().Trim();
                    Race = ds.Tables[0].Rows[0]["Race"].ToString().Trim();
                    OccupationID = Convert.ToInt32(ds.Tables[0].Rows[0]["Occupationid"]);
                    Height = ds.Tables[0].Rows[0]["Height"].ToString().Trim();
                    Weight = ds.Tables[0].Rows[0]["Weight"].ToString().Trim();
                    HairColor = ds.Tables[0].Rows[0]["HairColor"].ToString().Trim();
                    EyeColor = ds.Tables[0].Rows[0]["EyeColor"].ToString().Trim();
                    SSN = ds.Tables[0].Rows[0]["SSNumber"].ToString().Trim();
                    Address1 = ds.Tables[0].Rows[0]["Address1"].ToString().Trim();
                    Address2 = ds.Tables[0].Rows[0]["Address2"].ToString().Trim();
                    City = ds.Tables[0].Rows[0]["City"].ToString().Trim();
                    StateID = Convert.ToInt32(ds.Tables[0].Rows[0]["Stateid_FK"]);
                    Zip = ds.Tables[0].Rows[0]["Zip"].ToString().Trim();
                    Contact1 = ds.Tables[0].Rows[0]["Contact1"].ToString().Trim();
                    ContactType1 = Convert.ToInt32(ds.Tables[0].Rows[0]["ContactType1"]);
                    Contact2 = ds.Tables[0].Rows[0]["Contact2"].ToString().Trim();
                    ContactType2 = Convert.ToInt32(ds.Tables[0].Rows[0]["ContactType2"]);
                    Contact3 = ds.Tables[0].Rows[0]["Contact3"].ToString().Trim();
                    ContactType3 = Convert.ToInt32(ds.Tables[0].Rows[0]["ContactType3"]);
                    Email = ds.Tables[0].Rows[0]["Email"].ToString().Trim();
                    GeneralComments = ds.Tables[0].Rows[0]["GeneralComments"].ToString().Trim();
                    ContactComments = ds.Tables[0].Rows[0]["ContactComments"].ToString().Trim();
                    TrialComments = ds.Tables[0].Rows[0]["TrialComments"].ToString().Trim();
                    SettingsComments = ds.Tables[0].Rows[0]["SettingComments"].ToString().Trim();
                    BondFlag = Convert.ToInt32(ds.Tables[0].Rows[0]["BondFlag"]);
                    MidNumber = ds.Tables[0].Rows[0]["Midnum"].ToString().Trim();
                    Casecount = Convert.ToInt32(ds.Tables[0].Rows[0]["casecount"]);
                    AddressConfirmFlag = Convert.ToInt32(ds.Tables[0].Rows[0]["Addressconfirmflag"]);
                    YDS = ds.Tables[0].Rows[0]["YDS"].ToString().Trim();
                    EmailNotAvailable = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailNotAvailable"]);
                    NoDL = Convert.ToInt32(ds.Tables[0].Rows[0]["NoDL"]);
                    continuancestatus = Convert.ToInt32(ds.Tables[0].Rows[0]["ContinuanceStatus"]);
                    continuancecomments = ds.Tables[0].Rows[0]["ContinuanceComments"].ToString().Trim();
                    continuanceAmount = Convert.ToDouble(ds.Tables[0].Rows[0]["ContinuanceAmount"]);
                    firmID = Convert.ToInt32(ds.Tables[0].Rows[0]["FirmID"]);
                    continuanceflag = Convert.ToBoolean(ds.Tables[0].Rows[0]["ContinuanceFlag"]);
                    continuancedate = Convert.ToDateTime(ds.Tables[0].Rows[0]["ContinuanceDate"]);
                    activeFlag = Convert.ToInt32(ds.Tables[0].Rows[0]["ActiveFlag"]);
                    ClientCodeNumber = ds.Tables[0].Rows[0]["ClientCourtNumber"].ToString();
                    //Abbas Qamar 10114 04-05-2012 Getting the IsCrimial Court value.
                    IsCriminalCourt = Convert.ToInt32(ds.Tables[0].Rows[0]["IsCriminalCourt"]);
                    //Added by Azee for walk in client flag modification

                    WalkInClientFlag = ds.Tables[0].Rows[0]["WalkInClientFlag"].ToString();
                    //Noufil 4172 06/12/2008 Getting setcall comments from database
                    setcallcomments = ds.Tables[0].Rows[0]["setcallcomments"].ToString();
                    //nasir
                    lastGerenalcommentsUpdatedate = Convert.ToDateTime(ds.Tables[0].Rows[0]["LastGerenalcommentsUpdatedate"].ToString().Trim());
                    ftacallcomments = ds.Tables[0].Rows[0]["ftacallcomments"].ToString();

                    //
                    IsPR = Convert.ToInt32(ds.Tables[0].Rows[0]["isPR"]);
                    HasConsultationComments = Convert.ToInt32(ds.Tables[0].Rows[0]["hasConsultationComments"]);
                    //Added By Ozair
                    LanguageSpeak = ds.Tables[0].Rows[0]["LanguageSpeak"].ToString();
                    //
                    //khalid 25-9-07
                    Court = ds.Tables[0].Rows[0]["court"].ToString();
                    SPN = ds.Tables[0].Rows[0]["spn"].ToString();

                    //Waqas 5771 04/16/2009 
                    Record_Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Recordid"]);

                    if (ds.Tables[0].Rows[0]["ContactID"] == DBNull.Value || ds.Tables[0].Rows[0]["ContactID"].ToString() == "0")
                    {
                        Contact_ID = 0;
                    }
                    else
                    {
                        Contact_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["ContactID"]);
                    }
                    IsContactIDConfirmedFlag = Convert.ToInt32(ds.Tables[0].Rows[0]["IsContactIDConfirmed"]);
                    // Noufil 5884 07/02/2009 Set values for SMS Flags
                    smsflag1 = Convert.ToInt32(ds.Tables[0].Rows[0]["SendSMSFlag1"]);
                    smsflag2 = Convert.ToInt32(ds.Tables[0].Rows[0]["SendSMSFlag2"]);
                    smsflag3 = Convert.ToInt32(ds.Tables[0].Rows[0]["SendSMSFlag3"]);
                    // Noufil 6766 02/11/2009 language ID, Contact Type and Combine Ticket No added
                    LanguageID = Convert.ToInt32(ds.Tables[0].Rows[0]["LanguageID"]);
                    ContactTypeDescription1 = Convert.ToString(ds.Tables[0].Rows[0]["contactTypeDesc1"]);
                    ContactTypeDescription2 = Convert.ToString(ds.Tables[0].Rows[0]["contactTypeDesc2"]);
                    ContactTypeDescription3 = Convert.ToString(ds.Tables[0].Rows[0]["contactTypeDesc3"]);
                    TicketNo = Convert.ToString(ds.Tables[0].Rows[0]["TicketNumber"]);
                    StateDescription = Convert.ToString(ds.Tables[0].Rows[0]["stateDescription"]);
                    // Afaq 7937 06/29/2010 Get Email status.
                    IsEmailVerified = Convert.ToBoolean(ds.Tables[0].Rows[0]["isEmailVerified"]);
                    //Abbas 10093 04/10/2012 Added Case Type ID field.
                    CaseType = Convert.ToInt32(ds.Tables[0].Rows[0]["CaseTypeId"]);
                    //Rab Nawaz Khan 11473 10/30/2013 Added for Leads history. . . 
                    IsLeadsDetailsSubmitted = Convert.ToBoolean(ds.Tables[0].Rows[0]["isLeadsDetailsSubmitted"]);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                string b = ex.Message;
                return false;
            }
        }

        public DataSet GetDispositionInfo(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_hts_Get_casedispositioninfo", "@TicketID_PK", TicketID);
            return ds;
        }
        // Rab Nawaz Khan 10330 07/17/2012 Method Added for the check payments. . . 
        /// <summary>
        /// GetClientPaymentInfo
        /// </summary>
        /// <param name="TicketID">Ticket ID</param>
        /// <returns>DataSet</returns>
        public DataSet GetClientPaymentInfo(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_Get_Payment_Info_By_MinDate", "@TicketID", TicketID);
            return ds;
        }

        public bool UpdateDispositionInfo()
        {
            //string[] key    = {"@TicketID_PK","@Firstname","@Lastname","@MidNum","@Datetypeflag","@CurrentDateset","@CurrentCourtNum","@FirmID","@GeneralComments","@TrialComments","@EmpID"};

            //object[] value1 = {ticketID,firstName,lastName,MidNumber,dateTypeFlag,currentDateSet,currentCourtNo,firmID,GeneralComments,trialComments,EmpID };
            //ClsDb.DBBeginTransaction();
            string[] key = { "@TicketID_PK", "@FirmID", "@EmpID" };

            object[] value1 = { ticketID, firmID, EmpID };

            try
            {
                ClsDb.ExecuteSP("usp_hts_Update_casedispositioninfo", key, value1);
                //ClsDb.DBTransactionCommit();
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

                //ClsDb.DBTransactionRollback();
                return false;
            }
        }
        public bool UpdateDispositionInfo(DbTransaction trans)
        {
            string[] key = { "@TicketID_PK", "@FirmID", "@GeneralComments", "@TrialComments", "@EmpID" };

            object[] value1 = { ticketID, firmID, GeneralComments, trialComments, EmpID };


            try
            {
                ClsDb.ExecuteSP("usp_hts_Update_casedispositioninfo", key, value1, trans);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        //As now we r not updating main status
        /// <summary>
        /// Method To Compare Case Violation Status
        /// </summary>
        /// <param name="ticketid">TicketID whose status to check</param>
        /*
        public int CheckCourtStatus(int ticketid)
        {
            int ret=0;
            try
            {				
                string[] key1={"@TicketID"};
                object[] value1={ticketid};
                //ClsDb.DBBeginTransaction();
                object obj=  ClsDb.ExecuteScalerBySp("sp_Check_CourtDate",key1,value1);
                //ClsDb.DBTransactionCommit();
                return  ret=Convert.ToInt32(obj); 
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);

                //ClsDb.DBTransactionRollback();
                return ret=0;
            }
        }
        */
        /// <summary>
        /// Check Clients must have 3 contacts
        /// </summary>
        /// <param name="TicketID"> ID for which to check contacts</param>		
        public bool CheckClientContact(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };
            //ClsDb.DBBeginTransaction();
            //change by ajmal
            //System.Data.SqlClient.IDataReader dr=  ClsDb.Get_DR_BySPArr("USP_HTS_CHECK_3CONTACTS",key1,value1);
            IDataReader dr = ClsDb.Get_DR_BySPArr("USP_HTS_CHECK_3CONTACTS", key1, value1);

            //ClsDb.DBTransactionCommit();
            //dr.Read();
            ////if (dr.HasRows)
            //////if(dr.)
            ////{
            ////    dr.Close();
            ////    return true;
            ////}
            ////else
            ////{
            ////    dr.Close();

            //    return false;
            ////}
            string ph = String.Empty;
            if (dr.Read())
                ph = dr.GetValue(0).ToString();

            if (ph.Length > 0)
            {
                dr.Close();
                return true;
            }
            else
            {
                dr.Close();
                return false;
            }



        }

        //Farrukh 9925 11/30/2011 Function which checks Clients must have 2 contacts
        /// <summary>
        /// Check Clients must have 2 contacts
        /// </summary>
        /// <param name="TicketID"> ID for which to check contacts</param>		
        /// <returns>True/False</returns>		
        public bool CheckNonBondClientContact(int TicketID)
        {
            IDataReader dr = null;
            try
            {
                string[] key1 = { "@TicketID" };
                object[] value1 = { TicketID };
                dr = ClsDb.Get_DR_BySPArr("USP_HTS_CHECK_2CONTACTS", key1, value1);

                string ph = String.Empty;
                if (dr.Read())
                    ph = dr.GetValue(0).ToString();

                return ph.Length > 0 ? true : false;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
            finally
            {
                dr.Close();
            }
        }

        /// <summary>
        /// Get Underlying Case Info by TicketID
        /// </summary>
        /// <param name="TicketID">TicketID whose underlying Case info to get</param>
        public DataSet GetUnderlyingCaseInfo(int TicketID)
        {
            //ClsDb.DBBeginTransaction();
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };

            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_Get_PAYMENTINFO_BUTTONSTATUS", key1, value1);
            //ClsDb.DBTransactionCommit();
            return ds;
        }

        //Waqas 5864 07/03/2009 Check on ALR Buttons
        /// <summary>
        /// This method is used to get information to check on ALR Butttons
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns></returns>
        public DataSet GetALRCaseQuestionsInfo(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };

            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_Get_PaymentInfo_ALRButtonStatus", key1, value1);
            return ds;
        }

        //Waqas 5864 07/06/2009 ALR
        /// <summary>        
        ///     This procedure checks Whether client had arrest date within 15 days to hire date.
        /// </summary>
        /// <param name="ticketid">Ticket id of Client</param>
        /// <returns>True/False</returns>
        public bool CheckALRWithArrestDateInDaysRange(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@ticketid" };
                object[] value1 = { ticketid };
                int returncount = Convert.ToInt32(ClsDb.ExecuteScalerBySp("usp_htp_CheckALRArrestDateinDaysRange", key, value1));
                if (returncount > 0)
                    return true;
                else
                    return false;
            }
            else
                throw new ArgumentNullException("Parameter cannot be null.");

        }

        //Waqas 5864 07/02/2009 update flag
        /// <summary>
        /// This method is used to update email sent flag.
        /// </summary>
        /// <param name="ticketid"></param>
        public void UpdateEmailSentFlag(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@TicketID" };
                object[] value1 = { ticketid };
                ClsDb.ExecuteSP("usp_htp_update_EmailSentFlag", key, value1);
            }
        }

        public DataSet GetClientEmail(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };

            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_Get_ClientEmailAddress", key1, value1);
            return ds;

        }

        public DataTable GetCourtForSPN(int TicketID)
        {
            string[] keys = { "@ticketid" };
            object[] values = { TicketID };
            DataTable dtSPNCourt = ClsDb.Get_DT_BySPArr("usp_hts_get_Court_for_SPN", keys, values);
            return dtSPNCourt;
        }

        public bool ValidateTrialLetterPrintOption(int TicketID)
        {
            string[] keys = { "@ticketid" };
            object[] values = { TicketID };
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_hts_trialletter_validation", keys, values);

            if (Convert.ToInt32(dt.Rows[0][0]) == 0)
                return true;
            else
                return false;

        }

        /// <summary>
        /// Get Underlying Mailer Info by TicketID
        /// </summary>
        /// <param name="TicketID">TicketID whose underlying Mailer info to get</param>
        public DataTable GetMailerDetail(int TicketID)
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            return ClsDb.Get_DT_BySPArr("USP_HTS_GET_MAILER_INFO", keys, values);
        }

        /// <summary>
        /// Get Information about all the mailers send to a particular client
        /// </summary>
        /// <param name="TicketID">TicketID whose underlying Mailer info to get</param>
        ///<returns>DataTable contains all mailer and their posted date list of particular case</returns>

        //Kazim 3411 3/13/2008 Use to get all mailer letter for particlar client 

        public DataTable GetAllMailerLetter(int TicketID)
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            return ClsDb.Get_DT_BySPArr("usp_htp_get_AllMailerLetter", keys, values);
        }


        //Save Case Summary Page At The Specifies Location
        public void CreateCaseSummary(string file, string CaseSummaryfilename)
        {

            //Set Sections 
            string RptSetting = "123456";
            //Set Report Name
            String[] reportname = new String[] { "Notes.rpt", "Matters.rpt" };
            string filename = HttpContext.Current.Server.MapPath("\\Reports") + "\\CaseSummary.rpt";

            //Get Sub Reports For Case Summary
            string[] keyssub = { "@TicketID" };
            object[] valuesub = { ticketID };
            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_GET_SubReports", keyssub, valuesub);

            string path = file;
            string[] key = { "@TicketId_pk", "@Sections" };
            object[] value1 = { ticketID, RptSetting };

            string name = ticketID + "-CoverSheet-" + DateTime.Now.ToFileTime() + ".pdf";

            // Create and Save File At the Specified Location
            Cr.CreateSubReports(filename, "usp_hts_get_casesummary_report_data", key, value1, reportname, ds, 1, HttpContext.Current.Session, HttpContext.Current.Response, path, ticketID.ToString(), false, CaseSummaryfilename);

        }

        public DataSet GetCourtsForSearch()
        {
            return ClsDb.Get_DS_BySP("USP_HTS_Get_CourtsForSearch");
        }

        /// <summary>
        /// Get Information about the status of the case
        /// </summary>
        /// <param name="TicketID">TicketID whose status to be checked</param>
        ///<returns>Returns false if case is related to QUOTE else true for CLIENTS</returns>
        // Tahir Ahmed 4402 7/15/2008 
        public bool IsClient(int TicketID)
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_Get_IsClient", keys, values));
        }

        /// <summary>
        /// Get Information about the status of the case
        /// </summary>
        /// <param name="TicketID">TicketID whose status to be checked</param>
        ///<returns>Returns true if case is related to JUD, JUR or PRE trial else false</returns>
        // Tahir Ahmed 4402 7/15/2008 
        public bool IsTrialCase(int TicketID)
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_Get_IsTrialCase", keys, values));
        }


        // Rab Nawaz 10729 04/16/2013 Added Nisi Follow up date and update Nisi notes
        /// <summary>
        ///  THis function Update bond followupdate
        /// </summary>
        /// <param name="ticketId">Ticket id of Record</param>
        /// <param name="followUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID</param>
        /// <param name="contactType">Contacted Id</param>
        /// <param name="notes">Nisi Notes</param>
        public void UpdateNisiFollowUpDate(int ticketId, DateTime followUpDate, int empid, int contactType, string notes, bool isFreezed)
        {
            string[] key = { "@TicketID_PK", "@date", "@empid", "@ContactID", "@nisiNotes", "@isFreezed" };
            object[] value1 = { ticketId, followUpDate, empid, contactType, notes, isFreezed };

            ClsDb.ExecuteSP("USP_HTP_UPDATE_NisiFollowUpDate", key, value1);
        }


        #endregion

        public clsCase()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        // Added by Asghar for checking NoCall Flag and NoLetter Flag for current case
        public bool SetFlagsStatus(string TicketID, string FlagType)
        {
            try
            {
                string[] keys2 = { "@ticketid" };
                object[] values2 = { TicketID };
                DataSet ds_flags = ClsDb.Get_DS_BySPArr("ups_hts_get_flags_status", keys2, values2);

                switch (FlagType)
                {
                    case "NoCalls":
                        if (ds_flags.Tables[0] != null)
                        {
                            return Convert.ToInt32(ds_flags.Tables[0].Rows[0]["NoCallFlag"]) > 0;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    case "NoLetters":
                        if (ds_flags.Tables[1] != null)
                        {
                            return Convert.ToInt32(ds_flags.Tables[1].Rows[0]["NoLetterFlag"]) > 0;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    case "Not On System":
                        if (ds_flags.Tables[2] != null)
                        {
                            return Convert.ToInt32(ds_flags.Tables[2].Rows[0]["NotOnSystem"]) > 0;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    default:
                        return false;
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        /// Insert note to Case History
        public bool InsertNoteToCaseHistory(DateTime caldate)
        {
            try
            {
                string[] keys = { "@ticketid", "@subject", "@notes", "@employeeid" };
                object[] values = { ticketID, "Quote Call Back date inserted " + caldate.ToString(), "Quote Call Back date inserted " + caldate.ToString(), empID };
                ClsDb.InsertBySPArr("SP_ADD_NOTES", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        //ozair 3613 on 04/02/2008
        /// <summary>
        /// This function is used to check for any case belonging to Oscar court by passing ticketid 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns>boolean value true or false</returns>
        public bool IsOscarCourt(int ticketid)
        {
            try
            {
                string[] key = { "@Ticketid" };
                object[] value1 = { ticketid };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("usp_HTP_Check_OscarCourt", key, value1));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        //ozair 3613 on 04/02/2008
        /// <summary>
        /// This function is used to check for any case belonging to HCC Haris County Criminal court by passing ticketid 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns>boolean value true or false</returns>
        public bool IsHCCCourt(int ticketid)
        {
            try
            {
                string[] key = { "@Ticketid" };
                object[] value1 = { ticketid };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("usp_HTP_Check_HCCCourt", key, value1));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        //ozair 3613 on 04/02/2008
        /// <summary>
        /// This function is used to check for any case belonging to HCJP court by passing ticketid 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns>boolean value true or false</returns>
        public bool IsHCJPCourt(int ticketid)
        {
            try
            {
                string[] key = { "@Ticketid" };
                object[] value1 = { ticketid };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("usp_HTP_Check_HCJPCourt", key, value1));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        // Noufil 4215 07/01/2008 Overloaded method added
        public bool IsHCJPCourt(int ticketid, int showall)
        {
            try
            {
                string[] key = { "@Ticketid", "@showAll" };
                object[] value1 = { ticketid, showall };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("usp_HTP_Check_HCJPCourt", key, value1));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        //Added By Zeeshan Ahmed
        //Check Whether Case Is Criminal Case Or Other Type Of Case
        public bool IsCriminalCase()
        {
            try
            {
                string[] keys = { "@TicketID" };
                object[] values = { ticketID };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTS_CHECK_Criminal_Case", keys, values));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// This method is used to get active flag of a case
        /// </summary>
        /// <returns>int 0,1</returns>
        public int GetActiveFlag()
        {
            try
            {
                string[] key_1 = { "@ticketid" };
                object[] value_1 = { ticketID };
                DataSet ds = ClsDb.Get_DS_BySPArr("usp_hts_check_activeflag", key_1, value_1);
                //Muhammad Muneer 6720 09/04/2010 returning active flag one in case of count < 0
                return ds.Tables[0].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[0].Rows[0][0]) : 1;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return 1;
            }

        }

        //date: 19-11-07 khalid
        //description: This method will be used to check
        //any case of the ticketid has specific status or not
        //currently will check ALR status
        public bool HasStatus(CourtViolationStatusType status)
        {
            try
            {
                string[] key_1 = { "@ticketid", "@Status" };
                object[] value_1 = { TicketID, Convert.ToInt32(status) };
                DataSet ds = ClsDb.Get_DS_BySPArr("usp_Varify_CourtStatus", key_1, value_1);
                return ds.Tables[0].Rows.Count > 0;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }



        public bool HasALRRequestDocument()
        {
            try
            {
                string[] keys = { "@ticketid", "@doctypeid" };
                object[] values = { ticketID, 15 };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTS_CHECK_CASE_Document", keys, values));
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }

        }

        /// <summary>
        /// This function is used to determine that the Cause no of HCJP Court for a specific case is valid or not 
        /// </summary>
        /// <param name="TicketId"></param>
        /// <returns>True for valid and false for invalid</returns>


        //Kazim 3451 3/25/2008
        //Add function to determine that a case has valid cause no or not.

        public bool IsValidHCJPCauseNumbers(int TicketId)
        {
            string[] key = { "@TicketId" };
            object[] value = { TicketId };
            return Convert.ToBoolean(Convert.ToInt16(ClsDb.ExecuteScalerBySp("USP_HTP_Get_IsValidCauseNo", key, value)));
        }

        // Noufil 3479 03/25/2008 This method use to get data for auto history in case disposition page
        public DataTable GetAutoHistory(int TicketID)
        {
            string[] keys = { "@TicketId_PK" };
            object[] values = { TicketID };
            DataTable dtSPautohistory = ClsDb.Get_DT_BySPArr("USP_HTP_Get_AutoStatusHistory", keys, values);
            return dtSPautohistory;
        }

        public DataTable GetAutodisp(int TicketID)
        {
            string[] keys = { "@TicketId_PK" };
            object[] values = { TicketID };
            DataTable dtSPautodisp = ClsDb.Get_DT_BySPArr("USP_HTP_Get_AutoStatusdisp", keys, values);
            return dtSPautodisp;
        }

        /// <summary>
        /// This Method Inside Case
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// <returns>Return True If Case Is Inside Court Case</returns>
        public bool IsInsideCourtCase(int TicketID)
        {
            string[] key = { "@TicketId" };
            object[] value = { TicketID };
            return Convert.ToBoolean(Convert.ToInt16(ClsDb.ExecuteScalerBySp("USP_HTP_Check_INSIDE_CASE", key, value)));
        }

        /// <summary>
        /// Check Case For FTA Violations
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// <returns>Return True If Case Contain FTA Violation</returns>
        public bool HasFTAViolations(int TicketID)
        {
            string[] key = { "@TicketId" };
            object[] value = { TicketID };
            return Convert.ToBoolean(Convert.ToInt16(ClsDb.ExecuteScalerBySp("USP_HTP_Check_FTA_VIOLATION_IN_CASE", key, value)));
        }

        /// <summary>
        /// This method is used to get the General Comments of teh case by passing it TicketId
        /// </summary>
        /// <returns>General Comments of particular case</returns>

        //Kazim 3640 4/11/2008 Used to get the General Comments from Ticketid
        public string GetGeneralCommentsByTicketId()
        {
            string[] key = { "@ticketid" };
            object[] value = { TicketID };
            return Convert.ToString(ClsDb.ExecuteScalerBySp("usp_getGeneralCommentsByTicketID", key, value));

        }

        /// <summary>
        /// This Function is used to get case type list
        /// </summary>
        /// <returns>Return datatable containing case types</returns>
        public DataTable GetCaseType()
        {
            //Zeeshan Ahmed 3709 04/29/2008 Get List Of Case Type
            return ClsDb.Get_DT_BySPArr("USP_HTP_GET_ALL_CASETYPE");
        }

        /// <summary>
        /// This Function Get Case Type of Case
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// <returns>Return Case Type</returns>
        public int GetCaseType(int TicketID)
        {
            //Zeeshan Ahmed 3979 05/20/2008 Get Case Type From Ticket ID 
            DataSet CaseInfo = GetCase(TicketID);
            //return ((CaseInfo.Tables.Count == 1 && CaseInfo.Tables[0].Rows.Count == 1) ? Convert.ToInt32(CaseInfo.Tables[0].Rows[0]["CaseTypeId"]) : 1);
            // Tahir Ahmed 4402 7/15/2008 
            return ((CaseInfo.Tables.Count > 0 && CaseInfo.Tables[0].Rows.Count > 0) ? Convert.ToInt32(CaseInfo.Tables[0].Rows[0]["CaseTypeId"]) : 1);
        }

        /// <summary>
        /// This Function Get Case Type of Case in String
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// <returns>Return Case Type</returns>
        public String GetCaseTypeName(int TicketID)
        {
            //Zeeshan Ahmed 3979 05/20/2008 Get Case Type From Ticket ID 
            DataSet CaseInfo = GetCase(TicketID);
            return ((CaseInfo.Tables.Count == 1 && CaseInfo.Tables[0].Rows.Count == 1) ? CaseInfo.Tables[0].Rows[0]["CaseTypeName"].ToString() : "");
        }
        /// <summary>
        /// This method is used to get the hmc arraignment waiting scanned documents of particular case
        /// </summary>
        /// <param name="ticketids"></param>
        /// <returns>DataSaet containing two tables [1st contains info from document tracking structure; 2nd contains info from old structure]
        /// which contains information about the scanned documents of hmc cases</returns>
        //ozair 4879 09/29/2008 get the hmc arraignment waiting scanned documents of particular case
        // from document tracking along with old structure.

        public DataSet GetHMCArrWaitingScannedDocuments(string ticketids)
        {
            string[] key = { "@Ticketids" };
            object[] value = { ticketids };
            return ClsDb.Get_DS_BySPArr("usp_htp_get_HMCArrWaitingDocuments", key, value);
        }

        // Noufil 4215 07/07/2008 Update court status
        public void UpdateCaseStatus(int ticketid, CourtViolationStatusType status, int empId)
        {
            string[] key = { "@ticketid_pk", "@courtviolationstatus", "@empid" };
            object[] value = { ticketid, Convert.ToInt32(status), empId };
            ClsDb.ExecuteSP("USp_Htp_UpdateStatus", key, value);
        }
        /// <summary>
        /// Get Minimum Hire Date 
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// <returns>Return Hire Date</returns>
        public DateTime GetHireDate(int TicketID)
        {
            //Noufil 4215 07/01/2008 Check SOl flag
            string[] key = { "@ticketid" };
            object[] value1 = { TicketID };
            return Convert.ToDateTime(ClsDb.ExecuteScalerBySp("USP_HTP_GetHireDate", key, value1));
        }
        /// <summary>
        /// Check Case For ALR Hearing Violation
        /// </summary>
        /// <param name="TicketID"></param>
        /// <returns>Return True If Case Contain ALR Hearing Violation</returns>
        public bool HasALRHearingViolation(int ticketID, int showdisposecase)
        {
            // Noufil 5819 05/14/2209 New parameter added to show dispose cases
            string[] key = { "@TicketId", "@Includedispose" };
            object[] value = { ticketID, showdisposecase };
            return Convert.ToBoolean(Convert.ToInt16(ClsDb.ExecuteScalerBySp("USP_HTP_Check_ALR_Hearing_Violation", key, value)));
        }

        //Noufil 4747 on 09/09/2008
        /// <summary>
        /// This function is used to check for any case belonging to Haris County TItle 4D Ct court by passing ticketid 
        /// </summary>
        /// <param name="ticketid">Ticket Id</param>
        /// <returns>boolean value true or false</returns>
        public bool IsHarrisCountyTitleCourt(int ticketid)
        {
            string[] key = { "@Ticketid" };
            object[] value1 = { ticketid };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("usp_HTP_Check_CivilCourt", key, value1));
        }


        //Sabir Khan 5266 11/28/2008
        /// <summary>
        /// This function is used to check for zip codes...
        /// </summary>
        /// <param name="zipcode">Zipcode</param>
        /// <returns>boolean value true or false</returns>
        public bool HasZipcode(string zipcode)
        {
            string zipcodes = ConfigurationManager.AppSettings["ZipCodes"];
            ArrayList zipcodelist = new ArrayList();
            zipcodelist.AddRange(zipcodes.Split(new[] { ',' }));
            foreach (string item in zipcodelist)
            {
                if (zipcode.Substring(0, 5) == item.Trim().Substring(0, 5))
                    return true;
            }
            return false;
        }

        //Sabir Khan 5338 12/26/2008
        /// <summary>
        /// this function use to check future court date...
        /// </summary>
        /// <param name="TicketID">TicketID</param>
        /// <returns>String value in ("0" or "1")</returns>
        public String HasFutureCourtdate(int TicketID)
        {
            DataSet ds = GetUnderlyingCaseInfo(TicketID);
            string HasFutureDate = "0";
            foreach (DataRow drow in ds.Tables[0].Rows)
            {
                if (Convert.ToDateTime(drow["CourtDateMain"].ToString()) > DateTime.Now)
                    HasFutureDate = "1";

            }
            return HasFutureDate;
        }

        //----------------------------------------------------------------------
        //Sabir Khan 5449 01/23/2009
        /// <summary>
        /// this function return the relevent promotional message...
        /// </summary>
        /// <param name="ZipCode">ZipCode</param>
        /// <param name="NoteID">NoteID</param>
        /// <returns>Promotional Message</returns>
        public string GetPromotionalMessage()
        {
            string[] key = { "@TicketId" };
            object[] value = { TicketID };
            return Convert.ToString(ClsDb.ExecuteScalerBySp("USP_HTP_Get_PromotionalMessage", key, value));
        }
        /// <summary>
        /// Sabir Khan 11460 10/04/2013
        /// This method is used to check the client for promotional prices
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable IsPromoClient()
        {

            DataTable dt = new DataTable();
            string[] key = { "@TicketId" };
            object[] value = { TicketID };
            //Sabir Khan 11560 11/26/2013 new promo logic has been implemented.
            dt = ClsDb.Get_DT_BySPArr("USP_HTP_Check_IsPromoClient", key, value);
            return dt;



        }


        //Sabir Khan 5449 01/23/2009
        /// <summary>
        /// this function return the relevent zipcodes...
        /// </summary>
        /// <param name="ZipCode">ZipCode</param>
        /// <param name="NoteID">NoteID</param>
        /// <returns>zipcodes</returns>
        public string GetPromotionalZipCodes()
        {
            DataSet ds = new DataSet();
            string zipcodes = string.Empty;
            string[] key = { "@TicketId" };
            object[] value = { TicketID };
            ds = ClsDb.Get_DS_BySPArr("USP_HTP_Get_PromotionalZipcodes", key, value);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["zipcodes"] != null)
                    {
                        zipcodes = zipcodes + dr["zipcodes"] + ", ";
                    }
                }
            }
            zipcodes = zipcodes.Substring(0, zipcodes.Length - 2) + ".";
            return zipcodes;
        }
        //--------------------------------------------------------------------------------

        ///Noufil 5691 04/06/2009
        /// <summary>
        ///         Update Past court HMC follow up date
        /// </summary>
        /// <param name="TicketId">Ticket Id of record</param>
        /// <param name="FollowUpDate">Follow up date to update</param>
        /// <param name="empid">Employee ID of user</param>
        public void UpdatePastcourtHMCFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@ticketid", "@PastCourtDateHMCFollowUpDate" };
            object[] value1 = { TicketId, FollowUpDate };
            ClsDb.ExecuteSP("USP_HTP_Update_PastCourtDateFollowUpdate", key, value1);
        }

        //Ozair 5799 04/19/2009
        /// <summary>
        /// Returns IsCourtSettingInfoChangedDuringBSDA Field
        /// </summary>
        /// <param name="ticketID"></param>
        /// <returns>bool</returns>
        public bool IsCourtSettingInfoChangedDuringBSDA(int ticketID)
        {
            bool isChanged = false;
            string[] key = { "@ticketID" };
            object[] value1 = { ticketID };
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTP_Get_IsCourtSettingInfoChangedDuringBSDA", key, value1);
            if (dt.Rows.Count > 0)
            {
                isChanged = Convert.ToBoolean(dt.Rows[0]["IsCourtSettingInfoChangedDuringBSDA"]);
            }
            return isChanged;
        }
        ///// Fahad 5807 05/14/2009
        ///// Check Case For FTA Violations
        ///// <returns>Return True If Case Contain FTA Violation</returns>
        ///// <param name="Recordid"></param>
        ///// <param name="MidNumber"></param>
        ///// <returns></returns>
        //public bool HasFTAViolationsNonClient(int Recordid, string MidNumber)
        //{
        //    string[] key = { "@Recordid", "@Midnumber" };
        //    object[] value = { Recordid, MidNumber };
        //    return Convert.ToBoolean(Convert.ToInt16(ClsDb.ExecuteScalerBySp("USP_HTP_Check_FTA_VIOLATION_NON_CLIENT", key, value)));
        //}

        /// Fahad 5807 05/14/2009
        /// Add Case For FTA Violations for both cases if Client FTA exist in Non-Client Or not
        /// </summary>
        /// <param name="TicketID">Case Number</param>
        /// </summary>
        /// <param name="Recordid"></param>
        /// <param name="MidNumber"></param>
        /// <param name="NewCauseNo"></param>
        /// <param name="ticketid"></param>
        /// <param name="PlanId"></param>
        /// <param name="status"></param>
        /// <param name="empId"></param>
        public void AddFTAViolationsNonClient(int Recordid, string MidNumber, string NewCauseNo, int ticketid, int PlanId, CourtViolationStatusType status, int empId)
        {
            if (ticketid > 0 && empId > 0)
            {
                string[] key = { "@Recordid", "@Midnumber", "@NewCauseNo", "@Ticketid", "@PlanId", "@courtviolationstatus", "@empid" };
                object[] value1 = { Recordid, MidNumber, NewCauseNo, ticketid, PlanId, Convert.ToInt32(status), empId };
                ClsDb.ExecuteSP("USP_HTP_Add_FTA_VIOLATION", key, value1);
            }

        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Get Last Row from the Client RecordSet
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public DataTable GetLastViolation(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@Ticketid" };
                object[] value = { ticketid };
                return ClsDb.Get_DT_BySPArr("USP_HTP_GET_LAST_VIOLATION", key, value);
            }
            else
                throw new ArgumentNullException("Parameter cannot be null.");

        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Check in record set that either bond falg Updated or not
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public bool IsBondFlagUpdated(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@TicketID" };
                object[] value1 = { ticketid };
                ClsDb.ExecuteSP("USP_HTP_UpdateBondFlagByTicketId", key, value1);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Check in record set that either under line bond falg Updated or not
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public bool UnderLineBondFlagUpdated(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@TicketID" };
                object[] value1 = { ticketid };
                ClsDb.ExecuteSP("USP_HTP_UpdateUnderLinedBondFlagByTicketId", key, value1);
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Check in record set that either Matter Description Updated or not
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public bool IsMatterDescriptionUpdated(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@TicketID" };
                object[] value1 = { ticketid };
                ClsDb.ExecuteSP("USP_HTP_UpdateMatterDescriptionByTicketId", key, value1);
                return true;

            }
            else
                return false;
        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Get all the Ticket Number on the basis of Ticket id
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public DataTable GetTicketNumber(int ticketid)
        {
            if (ticketid > 0)
            {
                DataTable dt = new DataTable();
                string[] key = { "@TicketID" };
                object[] value1 = { ticketid };
                dt = ClsDb.Get_DT_BySPArr("USP_HTP_Get_TicketNumber", key, value1);
                return dt;
            }
            else
                throw new ArgumentNullException("Parameter cannot be null.");

        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Check in record set that either bond falg Exist or not
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public bool GetBondFlag(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@ticketid_pk" };
                object[] value1 = { ticketid };
                return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_Get_BondFlagFromTblTickets", key, value1));
                //dt = ClsDb.Get_DT_BySPArr("USP_HTP_Get_BondFlagFromTblTickets", key, value1);
                //if (dt.Rows.Count > 0)
                //   return true;
                //else
                //    return false;

            }
            else
                return false;



        }


        /// <summary>
        ///     // Noufil 5819 05/15/2009 
        ///     THis procedure checks Whether client has null arrest date with ALR violation
        /// </summary>
        /// <param name="ticketid">Ticket id of Client</param>
        /// <returns>True/False</returns>
        public bool CheckALRWithNullArrestDate(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@ticketid" };
                object[] value1 = { ticketid };
                int returncount = Convert.ToInt32(ClsDb.ExecuteScalerBySp("USP_HTP_CHECKALRNULLARRESTDATE", key, value1));
                return returncount > 0;
            }
            else
                throw new ArgumentNullException("Parameter cannot be null.");

        }

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Set bond is for HMC and Update Bond price as Zero
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public void UpdateBondforHMC(int ticketid)
        {
            if (ticketid > 0)
            {
                string[] key = { "@ticketid_pk" };
                object[] value1 = { ticketid };
                ClsDb.ExecuteSP("USP_HTP_UPDATE_BondAmountToZero", key, value1);
            }

        }


        /// <summary>
        /// Yasir Kamal 5948 06/04/09 
        /// This Method Checks for violations other Than Disposed
        /// </summary>
        /// <returns></returns>
        public bool HasDisposedViolations(int ticketid)
        {

            string[] keys = { "@ticketid" };
            object[] values = { ticketid };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_Check_Violations_Not_Disposed_Status", keys, values));

        }

        //Yasir Kamal 6109 07/17/2009 do not allow hiring if no court is assigned to one or more violations
        /// <summary>
        /// check if one or more violation(s) has no court assigned to it.
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public bool IsNoCourtAssigned(int ticketid)
        {

            string[] keys = { "@ticketid" };
            object[] values = { ticketid };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_Check_Violation_NoCourtAssigned", keys, values));

        }

        /// <summary>
        /// Fahad 6036 06/16/09 
        /// This Method Checks for Status on the basis of CourtId and TicketId
        /// </summary>
        /// <returns></returns>
        public bool HasViolationByCourID(int Ticketid, int CourtId, int Status)
        {

            string[] keys = { "@Ticketid", "@CourtId", "@Status" };
            object[] values = { Ticketid, CourtId, Status };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_GET_CaseStatusByCourtId", keys, values));

        }

        /// <summary>
        ///     Noufil 5884 07/13/09 
        ///     This Method Checks for nearest future court date
        /// </summary>
        /// <returns></returns>
        public DataTable GetNearestFutureCourtDate()
        {
            string[] key1 = { "@ticketid" };
            object[] value1 = { ticketID };
            return ClsDb.Get_DT_BySPArr("USP_HTP_GetNearestFutureCourtDateOfClient", key1, value1);
        }

        //Waqas 6599 09/19/2009 Occupations List
        /// <summary>
        /// This function returns distinct occupation.
        /// </summary>
        public string GetCaseOccupation()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_htp_get_clientOccupation");
            string strOccupation = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                strOccupation += HttpUtility.HtmlEncode(dr[0].ToString());

                if (i != dt.Rows.Count - 1)
                {
                    strOccupation += ",";
                }
            }
            return strOccupation;
        }

        //Waqas 6666 10/26/2009 with selection creteria.
        /// <summary>
        /// This function returns distinct occupation.
        /// </summary>
        public string[] GetCaseOccupation(string searchCriteria)
        {
            string[] key = { "criteria" };
            object[] obj = { searchCriteria };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_htp_get_clientOccupation", key, obj);
            string[] cntName = new string[ds.Tables[0].Rows.Count];
            int i = 0;
            try
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    cntName.SetValue(rdr["Occupation"].ToString(), i);
                    i++;
                }
            }
            catch { }
            return cntName;
        }

        //Waqas 6599 09/19/2009 Employer List
        /// <summary>
        /// This function returns distinct occupation.
        /// </summary>
        public string GetCaseEmployer()
        {
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_htp_get_clientEmployer");
            string strEmployer = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                strEmployer += dr[0].ToString();

                if (i != dt.Rows.Count - 1)
                {
                    strEmployer += ",";
                }
            }
            return strEmployer;
        }

        //Waqas 6666 10/26/2009 with selection creteria.
        /// <summary>
        /// This function returns distinct occupation.
        /// </summary>
        public string[] GetCaseEmployer(string searchCriteria)
        {
            DataSet dtst = new DataSet();
            string[] key = { "criteria" };
            object[] obj = { searchCriteria };
            dtst = ClsDb.Get_DS_BySPArr("usp_htp_get_clientEmployer", key, obj);
            string[] cntName = new string[dtst.Tables[0].Rows.Count];
            int i = 0;
            try
            {
                foreach (DataRow rdr in dtst.Tables[0].Rows)
                {
                    cntName.SetValue(rdr["Employer"].ToString(), i);
                    i++;
                }
            }
            catch { }
            return cntName;
        }

        /// <summary>
        ///     Noufil 6126 07/23/09 
        ///     This method update client new hire status.
        /// </summary>
        /// <param name="ticketid">Ticket ID of Client</param>
        /// <param name="empid">Emp ID of User</param>
        /// <param name="newhirestatus">Client New Hire Status</param>
        /// <param name="updatewithbusinessdays">Update with business days or not</param>

        public void UpdateClientNewHireStatus(int ticketid, int empid, int newhirestatus, bool updatewithbusinessdays)
        {
            if (ticketid > 0 && empid > 0)
            {
                string[] key1 = { "@newhirestatus", "@ticketid", "@empid", "@updatewithbusinessdays" };
                object[] value1 = { newhirestatus, ticketid, empid, Convert.ToInt32(updatewithbusinessdays) };
                ClsDb.ExecuteScalerBySp("USP_HTP_UPDATE_NEWHIRESTATUS", key1, value1);
            }
            else
                throw new ArgumentNullException("TicketID and EmpID cannot be zero");

        }

        //Nasir 6483 10/06/2009 
        /// <summary>       
        /// get data after Checking for bond report 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <returns></returns>
        public DataTable CheckForBondReport(int ticketid)
        {
            if (ticketid > 0)
            {
                DataTable dt = new DataTable();
                string[] key = { "@TicketID" };
                object[] value1 = { ticketid };
                dt = ClsDb.Get_DT_BySPArr("USP_HTP_Check_BondReportPrint", key, value1);
                return dt;
            }
            else
                throw new ArgumentNullException("Parameter cannot be null.");

        }

        //Ozair 6934 01/22/2010 Added GetContactNo line wise
        /// <summary>
        /// This method is used to return contact number in html format(line wise) for displaying purpose
        /// </summary>
        /// <param name="ticketId">Ticket ID of the Case</param>
        /// <returns>string</returns>
        public string GetContactNo(int ticketId)
        {
            string contactNo = String.Empty;
            try
            {
                clsGeneralMethods gm = new clsGeneralMethods();
                DataSet dsContactNo = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_Get_TicketsContactByTicketID", "@TicketID_PK", ticketId);

                foreach (DataRow dr in dsContactNo.Tables[0].Rows)
                {
                    if (dr["phno"].ToString().Trim() == "")
                    {
                        contactNo += dr["phno"].ToString();
                    }
                    else
                    {
                        contactNo += gm.formatPhone(dr["phno"].ToString()) + "(" + dr["contacttype"] + ")";
                        contactNo += "<BR />";
                    }
                }
            }
            catch
            {
            }
            return contactNo;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="HasConsultationFlag"></param>
        /// <returns></returns>
        public bool UpdateHasConsultationFlag(int ticketid, Nullable<bool> HasConsultationFlag)
        {
            string[] key = { "@TicketId", "@HasConsultationFlag" };
            object[] value = { ticketid, HasConsultationFlag };
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("[dbo].[USP_HTP_UpdateHasConsultationFlag]", key, value));
        }
        // 7752 05/004/2010 Afaq Update followup date of disposed alert
        /// <summary>
        /// <Update followup date of disposed alert>
        /// <param name="TicketId"></param>
        /// <param name="empid"></param>
        /// <param name="FollowUpdate"></param> 
        /// <returns></returns>.
        /// </summary>
        public void UpdateDisposedAlertFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            string[] key = { "@TicketID", "@empid", "@FollowUpDate" };
            object[] value1 = { TicketId, empid, FollowUpDate };

            ClsDb.ExecuteSP("USP_HTP_Update_DisposedAlertFollowUpDate", key, value1);
        }

        /// <summary>
        /// Update bad email address followup date.
        /// </summary>
        /// <param name="TicketId">Auto generated primary key which uniquly identified ticket number</param>
        /// <param name="FollowUpDate">Follow up date</param>
        /// <param name="empid">Employee ID of the client</param>
        public void UpdateBadEmailAddressFollowupDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            //SAEED 7844 06/23/2010 method created.
            string[] key = { "@TicketID", "@empid", "@FollowUpDate" };
            object[] value = { TicketId, empid, FollowUpDate };
            ClsDb.ExecuteSP("USP_HTP_Update_BadEmailAddressFollowupDate", key, value);
        }
        /// <summary>
        /// Update address validation report followup date.
        /// </summary>
        /// <param name="TicketId">Auto generated primary key which uniquly identified ticket number</param>
        /// <param name="FollowUpDate">Follow up date</param>
        /// <param name="empid">Employee ID of the client</param>
        public void UpdateAddressValidationReportFollowupDate(int contactID, int TicketId, DateTime FollowUpDate, int empid)
        {
            //SAEED 7844 06/23/2010 method created.
            string[] key = { "@TicketID", "@empid", "@FollowUpDate", "@contactID" };
            object[] value = { TicketId, empid, FollowUpDate, contactID };
            ClsDb.ExecuteSP("USP_HTP_Update_AddressValidationFollowupDate", key, value);
        }
        // Afaq 7937 06/29/2010
        /// <summary>
        /// Update email status after verification.
        /// </summary>
        /// <param name="TicketId">TicketId</param>
        /// <param name="isEmailVerified">Email verification status</param>
        public void UpdateEmailVerifiedStatus(int TicketId, bool isEmailVerified)
        {
            string[] key = { "@ticketId", "@isEmailVerified" };
            object[] value = { TicketId, isEmailVerified };
            ClsDb.ExecuteSP("USP_HTP_IsEmailVerified", key, value);
        }
        //Asad Ali 8502 11/03/2010 revert changes as per requirement
        //// Muhammad Muneer 8276 09/23/2010 PMCValidation method
        ///// <summary>
        ///// getting the flags for PMC validation
        ///// </summary>
        ///// <param name="ticketid"> ticket id </param>
        ///// <returns>DataSet</returns>
        //public DataSet PMCValidation(int ticketid)
        //{
        //    string[] key = { "@ticketid" };
        //    object[] value1 = { ticketid };
        //    DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_PMCValidation", key, value1);
        //    return ds;
        //}
        // Afaq 8311 09/30/2010 
        /// <summary>
        /// Return the number of violations in a case profile for a particular status.
        /// </summary>
        /// <param name="status">Enumeration of type CourtViolationStatusType</param>
        /// <returns>Violation Count</returns>
        public int ViolationCountByStatus(CourtViolationStatusType status)
        {
            string[] key_1 = { "@ticketid", "@Status" };
            object[] value_1 = { TicketID, Convert.ToInt32(status) };
            DataSet ds = ClsDb.Get_DS_BySPArr("usp_Varify_CourtStatus", key_1, value_1);

            //Cheking table count
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0].Rows.Count;
            }
            else
            {
                return -1;
            }
        }

        //Sabir Khan 8488 12/24/2010 method created.
        /// <summary>
        /// Update Bad Addresses Follow up Date
        /// </summary>
        /// <param name="TicketId">Ticket Id</param>
        /// <param name="FollowUpDate">Follow up date</param>
        /// <param name="empid">Employee Id</param>
        public void UpdateBadAddressesFollowUpDate(int TicketId, DateTime FollowUpDate, int empid)
        {
            if (TicketId > 1 && empid > 1)
            {
                string[] key = { "@TicketID", "@FollowUpDate" };
                object[] value1 = { TicketId, FollowUpDate };
                ClsDb.ExecuteSP("USP_HTP_Update_BadAddressFollowUpdate", key, value1);
            }
        }

        // Rab Nawaz 9961 01/03/2012 Method added to check the NISI cases 
        ///<summary>
        /// This Method return the true if the Nisi case associated against the pertricular client 
        ///</summary>
        ///<param name="ticketId">Integer TicketId</param>
        ///<returns>Boolean</returns>
        public bool IsNisiAssociatedCase (int ticketId)
        {
            string[] key = {"@TicketId"};
            object[] value = {ticketId};
            return Convert.ToBoolean(ClsDb.ExecuteScalerBySp("USP_HTP_IsNisiCaseAssociated", key, value));
        }

        ///<summary>
        ///Haris Ahmed 10381 08/29/2102 Check as it can be Potential Visa Clients
        ///</summary>
        ///<returns></returns>
        public DataSet CanBePotentialVisaClients(int TicketID)
        {
            string[] key1 = { "@TicketID" };
            object[] value1 = { TicketID };

            DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTS_CanBePotentialVisaClients", key1, value1);
            return ds;
        }

        ///Haris Ahmed 10381 08/29/2102  Insert note to Immigration Comments
        /// Farrukh 11180 06/28/2013 Added "IsImmigration" field in db
        public bool InsertImmigrationComments(int TicketID, string immigrationComments, bool isImmigration)
        {
            try
            {
                string[] keys = { "@TicketID", "@StatusID", "@ImmigrationComments", "@IsImmigration" };
                object[] values = { TicketID, 1, immigrationComments, isImmigration };
                ClsDb.InsertBySPArr("USP_HTS_Insert_ImmigrationComments", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        ///Haris Ahmed 10381 08/29/2102  Insert note to Immigration Comments
        public bool UpdateImmigrationCaseStatus(int TicketID, int StatusID)
        {
            try
            {
                string[] keys = { "@TicketID", "@StatusID" };
                object[] values = { TicketID, StatusID };
                ClsDb.InsertBySPArr("USP_HTS_Update_ImmigrationCaseStatus", keys, values);
                return true;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        ///Haris Ahmed 10381 08/29/2102  Get Immigration Comment Status
        public DataSet GetImmigrationCommentStatus()
        {
            try
            {
                DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_Get_ImmigrationCommentStatus");
                return ds;
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return null;
            }
        }

        ///Haris Ahmed 10381 08/31/2102 
        /// <summary>
        /// This method used to Send Immigration Letter to the Batch Print.
        /// </summary>
        /// <param name="ticketno">Ticket Id of the Client</param>
        /// <param name="empid">Employee Id of the Login User</param>
        /// <param name="lettertype">Type of the Letter(Type of LOR is 6 )</param>
        /// <param name="isBatch">Either send to Batch Or Not.</param>
        public void SendImmigrationLetterToBatch(int ticketno, int empid, int lettertype, bool isBatch, string docPath)
        {
            string[] keys = {
                                "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID",
                                "@DocPath", "@PageCount"
                            };
            object[] values = { ticketno, System.DateTime.Now, "1/1/1900", 0, lettertype, empid, docPath, 1 };
            ClsDb.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);
        }

        ///<summary>
        ///Hafiz 10288 07/18/2102 ALR Post Hire questions Update regarding the Ticket ID's.
        ///</summary>
        ///<returns></returns>
        public bool UpdateALRPostHireQuestions()
        {

            try
            {

                string[] key = { "@TicketID_PK", "@EmpID", "@ALROfficerName" , "@ALROfficerbadge", "@ALRPrecinct", "@ALROfficerAddress", "@ALROfficerCity", "@ALROfficerStateID", "@ALROfficerZip",
                         "@ALROfficerContactNumber", "@ALRIsIntoxilyzerTakenFlag",  "@ALRIntoxilyzerResult",  "@ALRBTOFirstName",  "@ALRBTOLastName", "@ALRBTOBTSSameFlag" ,
                         "@ALRBTSFirstName",  "@ALRBTSLastName",  "@ALRArrestingAgency",  "@ALRMileage" , "@IsALRArrestingObservingSame", "@ALRObservingOfficerName"
                        , "@ALRObservingOfficerBadgeNo", "@ALRObservingOfficerPrecinct", "@ALRObservingOfficerAddress", "@ALRObservingOfficerCity", "@ALRObservingOfficerState"
                        , "@ALRObservingOfficerZip", "@ALRObservingOfficerContact", "@ALRObservingOfficerArrestingAgency", "@ALRObservingOfficerMileage", "@IsALRHearingRequired", "@ALRHearingRequiredAnswer"
                        };

                //if (DOB == "" || DOB == "//") { DOB = "1/1/1900"; }
                object[] value1 = { TicketID,  EmpID, ALROfficerName, ALROfficeBadgeNo, ALRPrecinct, ALROfficerAddress, ALROfficerCity, ALROfficerState, ALROfficerZip,
                              ALRContactNumber, ALRIntoxilyzerTakenFlag, ALRIntoxilyzerResult, ALRBTOFirstName, ALRBTOLastName, ALRBTOBTSSameFlag,
                              ALRBTSFirstName, ALRBTSLastName, ALRArrestingAgency, ALROfficerMileage, IsALRArrestingObservingSame, ALROBSOfficerName, ALROBSOfficeBadgeNo, ALROBSPrecinct, ALROBSOfficerAddress, 
                              ALROBSOfficerCity, ALROBSOfficerState, ALROBSOfficerZip, ALROBSContactNumber, ALROBSArrestingAgency, ALROBSOfficerMileage, IsALRHearingRequired, ALRHearingRequiredAnswer 
                               };

                ClsDb.ExecuteSP("Usp_HTS_UpdateALRPostHireQuestions", key, value1, true);
                return true;
            }


            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        // Rab Nawaz Khan 11473 10/23/2013 Added to update the Caller ID info. . .
        /// <summary>
        /// This Method is used to update the client caller ID info.
        /// </summary>
        public void UpdateCallerIdInfo()
        {
            string[] key = { "@callerId", "@isUnknonwn", "@ticketId", "@empId" };
            object[] values = {CallerID , IsUnknownCallerId, TicketID, EmpID };
            ClsDb.ExecuteSP("USP_HTP_InsertLeadCallerIdInfo", key, values);
        }
    }
}

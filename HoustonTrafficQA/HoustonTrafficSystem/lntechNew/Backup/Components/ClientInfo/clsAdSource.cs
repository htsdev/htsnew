using System;
using System.Data;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
	/// <summary>
	/// Summary description for clsAdSource.
	/// </summary>
	public class clsAdSource
	{
		//Create an Instance of Data Access Class
		clsENationWebComponents ClsDb=new clsENationWebComponents();

#region Variables

		private int adsourceID=0;
		private string description=String.Empty;

#endregion

#region Properties

		public int AdsourceID
		{
			get
			{
				return adsourceID;
			}
			set
			{
				adsourceID = value;
			}
		}

		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}

#endregion

#region Methods

		public DataSet GetAdSource()
		{
			DataSet ds=ClsDb.Get_DS_BySP("usp_HTS_GetAdSource");
			return ds;
		}
#endregion

		public clsAdSource()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}

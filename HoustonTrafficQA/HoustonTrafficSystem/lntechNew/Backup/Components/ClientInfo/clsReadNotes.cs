
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Components.ClientInfo
{
       public class clsReadNotes
    {
           /// <summary>
           /// Class Created by Fahad For Read Notes Flag on (12-24-2007)
           /// </summary>
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();


        #region Methods
        /// <summary>
        /// I called USP_HTS_Insert_Comments 2 times because first time will update general comments
        /// & then update ReadComments field in tbltickets (Fahad- 12/24/2007)
        /// </summary>
        /// <param name="ticketid"> Ticketid</param>
        /// <param name="empid">Employee ID</param>
        /// <param name="comments">Read CommentsComments </param>
        public void UpdateReadComments(int ticketid, int empid, string comments)
        {
            try
            {
                string[] keys = {"@TicketID","@EmployeeID","@CommentID","@Comments"};
                object[] values = { ticketid, empid, 1, comments };
                object[] values2 = { ticketid, empid, 8, comments };
                clsdb.ExecuteSP("USP_HTS_Insert_Comments",keys,values);
                clsdb.ExecuteSP("USP_HTS_Insert_Comments", keys,values2);
            }

            catch(Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
           /// <summary>
           /// Insert ReadNote Flag in tblticketsFlag (Fahad- 12/24/2007)
           /// </summary>
           /// <param name="ticketid">Ticket ID</param>
           /// <param name="empid">Employee ID</param>
           public void InsertFlag(int ticketid, int empid)
           {
               try
               {
                   string[] keys = { "@TicketID","@FlagID","EmpID"};
                   object[] values = { ticketid,31,empid};
                   clsdb.ExecuteSP("USP_HTS_Insert_ReadNoteFlag", keys, values);
               }
               catch (Exception ex)
               {
                   bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
               }
           }
           /// <summary>
           /// Delete ReadNote Flag from tblticketsflag (Fahad- 12/24/2007)
           /// </summary>
           /// <param name="ticketid"></param>

           public void DeleteFlag(int ticketid,int empid)
           {
               try
               {
                   string[] keys = { "@ticketid", "@empid" };
                   object[] values = { ticketid,empid};
                   clsdb.ExecuteSP("usp_hts_delete_ReadNotes", keys, values);
               }
               catch (Exception ex)
               {
                   bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
               }
           }
           /// <summary>
           /// Check Weather ReadNote Flag Exist Or Not (Fahad- 12/24/2007)
           /// </summary>
           /// <param name="ticketid"></param>
           /// <returns></returns>
           public bool CheckReadNoteComments(int ticketid)
           {
               DataSet ds = new DataSet();
               int count = 0;
               try
               {
                   string[] keys = {"@ticketid" };
                   object[] values = { ticketid};
                   ds = clsdb.Get_DS_BySPArr("USP_HTS_Get_ReadNoteFlag", keys, values);
                   if (ds.Tables[0].Rows.Count > 0)
                   {
                       count = Convert.ToInt32(ds.Tables[0].Rows[0]["checkreadnotes"]);

                       if (count > 0)
                       {
                           return true;
                       }
                       else
                       {
                           return false;
                       }
                   }
                   else
                   {
                       return false;
                   }

               }
               catch (Exception ex)
               {
                   bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                   return false;
               }
           }

           public void FillReadNotes(TextBox txt,int ticketid)
           {
               DataSet ds = new DataSet();
               try
               {
                   string[] keys = { "@ticketid" };
                   object[] values = { ticketid };
                   ds = clsdb.Get_DS_BySPArr("USP_HTS_Get_ReadNoteFlag", keys, values);
                   //comments by khalid check added by fahad for task 2616 24-1-08
                   if (ds.Tables[1].Rows.Count > 0)
                   {
                       txt.Text = Convert.ToString(ds.Tables[1].Rows[0]["ReadComments"]).ToUpper();
                   }
               }
               catch (Exception ex)
               {
                   
                   
               }


           }
        #endregion

    }
}


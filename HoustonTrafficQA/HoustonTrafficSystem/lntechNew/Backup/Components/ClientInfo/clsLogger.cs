using System;
using System.Data;
using FrameWorkEnation.Components;
//using System.Web.Mail;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using Configuration.Client;
using Configuration.Helper;
namespace lntechNew.Components.ClientInfo
{
    /// <summary>
    /// Summary description for clsLogger.
    /// </summary>
    public class clsLogger
    {
        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        #region Variables

        private int notesID = 0;
        private string subject = String.Empty;
        private string notes = String.Empty;
        private int empID = 0;

        #endregion

        #region Properties

        public int NotesID
        {
            get
            {
                return notesID;
            }
            set
            {
                notesID = value;
            }
        }

        public string Subject
        {
            get
            {
                return subject;
            }
            set
            {
                subject = value;
            }
        }
        public string Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }
        public int EmpID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }
        #endregion

        #region Methods
        public bool AddNote(int EmpID, string Subject, string Note, int TicketID)
        {
            string[] key = { "@EmpID", "@Subject", "@Note", "@TicketID" };

            object[] value1 = { EmpID, Subject, Note, TicketID };
            try
            {
                ClsDb.ExecuteSP("usp_HTS_Insert_CaseHistoryInfo", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        
        /// <summary>
        /// This method is used to store history in tblticketsnotes table, whenever there is a change in followup date.
        /// </summary>
        /// <param name="EmpID">Employee ID of the client</param>
        /// <param name="Subject">Title</param>
        /// <param name="Note">Note or comments which will store as client history.</param>
        /// <param name="TicketID">Ticket Id against which the history will update.</param>
        /// <returns></returns>
        public bool AddNote(int ContactID,int EmpID, string Subject, string Note, int TicketID)
        {
            //SAEED 7844 06/23/2010 method created.
            string[] key = { "@EmpID", "@Subject", "@Note", "@TicketID","@ContactID" };

            object[] value1 = { EmpID, Subject, Note, TicketID,ContactID };
            try
            {
                ClsDb.ExecuteSP("usp_HTS_Insert_CaseHistoryInfo", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }
        public bool AddEmailNote(int EmpID, string Subject, int TicketID, int lettertype, int emailid)
        {
            string[] key = { "@EmpID", "@Subject", "@TicketID", "@lettertype", "@emailid" };

            object[] value1 = { EmpID, Subject, TicketID, lettertype, emailid };
            try
            {
                ClsDb.ExecuteSP("usp_HTS_Insert_CaseHistoryEmailInfo", key, value1);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }

        public DataSet GetNotes(int TicketID)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_HTS_GetNotes", "@TicketID", TicketID);
            return ds;
        }

        public DataTable GetStatusHistory(int TicketID)
        {
            //Aziz 3203 02/20/08 Return type changed to DataTable
            string[] key = { "@TicketID" };
            object[] value1 = { TicketID };
            return ClsDb.Get_DT_BySPArr("usp_hts_get_StatusHistory", key, value1);

        }

        public void ErrorLog(string Message, string Source, string TargetSite, string StackTrace)
        {
            string[] key1 = { "@Message", "@Source", "@TargetSite", "@StackTrace" };
            object[] value11 = { Message.ToString(), Source.ToString(), TargetSite.ToString(), StackTrace.ToString() };
            ClsDb.InsertBySPArr("usp_hts_ErrorLog", key1, value11);
        }
        //Muhammad Ali 7925 07/05/2010 --> Add XML Docmentation Comments.
        /// <summary>
        /// This Method is use for Email Reports.  
        /// </summary>
        /// <param name="To">Email To:</param>
        /// <param name="From">Email From:</param>
        /// <param name="CC">Email CC:</param>
        /// <param name="BCC">Email BCC</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="body">Email Body</param>
        /// <param name="attchfile">Email attached File</param>
        public void EmailReport(string To, string From, string CC, string BCC, string subject, string body, string attchfile)
        {
            //Asad Ali 7925 07/21/2010 Get reply to value from configuration Services...
            IConfigurationClient Client = ConfigurationClientFactory.GetConfigurationClient();

            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            string userName = ConfigurationManager.AppSettings["SMTPUser"];
            string password = ConfigurationManager.AppSettings["SMTPPassword"];
            //Sabir Khan 7925 06/18/2010 Get reply to value from configuration...
            //string replyTo = ConfigurationManager.AppSettings["ReplyTo"];
            string replyTo=Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.EMAIL, Key.EMAIL_REPLYTO_ADDRESS);
            //Sabir Khan 7925 06/18/2010 Get alias to value from configuration...
            //string alias = ConfigurationManager.AppSettings["Alias"];
            string alias=Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.EMAIL, Key.EMAIL_SENDER_NAME); ;
            SmtpClient client = new SmtpClient();
            MailMessage msg = new MailMessage();
            //Sabir Khan 7925 06/18/2010 Added reply to to the email message object...
            msg.ReplyTo = new MailAddress(replyTo);

            if (To.Contains(","))
                msg.To.Add(To);
            else
                msg.To.Add(To);
            //Sabir Khan 7925 06/18/2010 Added alias with from address...
            msg.From = new MailAddress(From, alias);
            //Yasir Kamal 7148 01/08/2010 email subject issue fixed.
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;

            if (!string.IsNullOrEmpty(CC))
            {
                msg.CC.Add(CC);
            }

            if (!string.IsNullOrEmpty(BCC))
            {
                msg.Bcc.Add(BCC);
            }

            if (!string.IsNullOrEmpty(attchfile))
                msg.Attachments.Add(new System.Net.Mail.Attachment(attchfile));


            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                NetworkCredential Credentials = new NetworkCredential(userName, password);
                client.Credentials = Credentials;
            }
            client.Host = smtpServer;
            client.Send(msg);
            msg.Dispose();

        }
        public string SalesRepEmail(int empid)
        {
            try
            {
                string[] key1 = { "@Empid" };
                object[] value11 = { empid };
                string email = (string)ClsDb.ExecuteScalerBySp("usp_hts_get_Rep_email", key1, value11);
                return email;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void UpdateClientEmail(int ticketid, string email)
        {
            try
            {
                string[] key1 = { "@ticketid", "@email" };
                object[] value11 = { ticketid, email };
                ClsDb.ExecuteSP("usp_hts_update_clientEmail", key1, value11);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SaveEmailInfo(int ticketid, int lettertype, string attchfile, string To, string From, string CC, string BCC, string subject, string body)
        {
            try
            {
                string[] key1 = { "@ticketid", "@lettertype", "@attchfile", "@To", "@From", "@CC", "@BCC", "@subject", "@body" };
                object[] value11 = { ticketid, lettertype, attchfile, To, From, CC, BCC, subject, body };
                int emailid = (int)ClsDb.ExecuteScalerBySp("usp_hts_insert_clientemailinfo", key1, value11);
                return emailid;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Sabir Khan 5963 06/02/2009 Get Tickler event history...
        public DataTable GetTicklerEventHistory(int TicketID)
        {
            string[] key = { "@tickeid" };
            object[] value1 = { TicketID };
            return ClsDb.Get_DT_BySPArr("USP_HTP_Get_TicklerCourtHistory", key, value1);
        }

        //Noufil 5884 07/02/2009 Get SMS history...
        public DataTable GetSMSHistory(int TicketID, int smstype)
        {
            string[] key = { "@ticketid", "@smstype" };
            object[] value1 = { TicketID, smstype };
            return ClsDb.Get_DT_BySPArr("USP_HTP_GET_SMSHISTORY", key, value1);
        }



        #endregion

        public clsLogger()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// This Static Method Is User To Log Error Information In Database
        /// </summary>
        /// <param name="ex">Exception</param>
        public static void ErrorLog(Exception ex)
        {
            //Aziz 3203 02/20/08 Static method with one argument for simple logging
            clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
            string[] key1 = { "@Message", "@Source", "@TargetSite", "@StackTrace" };
            object[] value11 = { ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString() };
            ClsDb.InsertBySPArr("usp_hts_ErrorLog", key1, value11);
        }
    }
}

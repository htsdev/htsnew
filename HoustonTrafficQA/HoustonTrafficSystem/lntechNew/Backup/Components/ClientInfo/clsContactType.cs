using System;
using System.Data;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
	/// <summary>
	/// Summary description for clsContactType.
	/// </summary>
	public class clsContactType
	{
		//Create an Instance of Data Access Class
		clsENationWebComponents ClsDb=new clsENationWebComponents();

		#region Variables

		private int contactType=0;
		private string description=String.Empty;

		#endregion

		#region Properties

		public int ContactType
		{
			get
			{
				return contactType;
			}
			set
			{
				contactType = value;
			}
		}

		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}

		#endregion

		#region Methods

		public DataSet GetContactType()
		{
			DataSet ds=ClsDb.Get_DS_BySP("usp_HTS_Get_ContactType");
			return ds;
		}

		#endregion

		public clsContactType()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}

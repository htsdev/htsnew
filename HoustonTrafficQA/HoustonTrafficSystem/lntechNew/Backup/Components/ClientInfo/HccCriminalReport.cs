using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Components.ClientInfo
{
    public class HccCriminalReport
    {

        #region variables

        private string searchType = string.Empty;
        public string error;
        public clsENationWebComponents ClsDb = new clsENationWebComponents("JIMS");
        public clsENationWebComponents ClsDbt = new clsENationWebComponents("Connection String");

        #region Parameter Type1
        // Parameter For Search Type 1   
        private string cDI = string.Empty;
        private string caseNumber = string.Empty;
        private string lastName = string.Empty;
        private string firstName = string.Empty;
        private string spn = string.Empty;
        private string dOB = string.Empty;
        private string attorney = string.Empty;
        // Agha Usman 4239 06/25/2008
        private int pageSize = 1000;
        #endregion

        #region Parameter Type2

        // Parameter For Search Type 2  
        private string settingReason = string.Empty;
        private string nextSettingDateFrom = string.Empty;
        private string nextSettingDateTo = string.Empty;

        private string sentenceEndDateFrom = string.Empty;
        private string sentenceEndDateTo = string.Empty;

        private string bookingDateFrom = string.Empty;
        private string bookingDateTo = string.Empty;

        private string arrestDateFrom = string.Empty;
        private string arrestDateTo = string.Empty;

        private string releaseDateFrom = string.Empty;
        private string releaseDateTo = string.Empty;

        private string dataLoadDateFrom = string.Empty;
        private string dataLoadDateTo = string.Empty;
        #endregion

        #region Parameter Type3
        // Parameter For Search Type 3   
        private string violationDescription = string.Empty;
        private string disposition = string.Empty;
        private string addressVerification = string.Empty;
        private string zipcode = string.Empty;
        private string courtRoom = string.Empty;
        private string floor = string.Empty;
        private string level = String.Empty;
        #endregion


        #endregion

        #region Propoerties

        // Agha Usman 4239 06/25/2008 - Create New property for Page Size
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = value;
            }
        }

        public string Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
            }
        }
       
        public string SearchType
        {
            get
            {
                return searchType;
            }
            set
            {
                searchType = value;
            }
        }

        public string CDI
        {
            get
            {
                return cDI;
            }
            set
            {
                cDI = value;
            }
        }

        public string CaseNumber
        {
            get
            {
                return caseNumber;
            }
            set
            {
                caseNumber = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public string Spn
        {
            get
            {
                return spn;
            }
            set
            {
                spn = value;
            }
        }

        public string DOB
        {
            get
            {
                return dOB;
            }
            set
            {
                dOB = value;
            }
        }

        public string Attorney
        {
            get
            {
                return attorney;
            }
            set
            {
                attorney = value;
            }
        }

        public string SettingReason
        {
            get
            {
                return settingReason;
            }
            set
            {
                settingReason = value;
            }
        }

        public string NextSettingDateFrom
        {
            get
            {
                return nextSettingDateFrom;
            }
            set
            {
                nextSettingDateFrom = value;
            }
        }
        public string NextSettingDateTo
        {
            get
            {
                return nextSettingDateTo;
            }
            set
            {
                nextSettingDateTo = value;
            }
        }

        public string SentenceEndDateFrom
        {
            get
            {
                return sentenceEndDateFrom;
            }
            set
            {
                sentenceEndDateFrom = value;
            }
        }
        public string SentenceEndDateTo
        {
            get
            {
                return sentenceEndDateTo;
            }
            set
            {
                sentenceEndDateTo = value;
            }
        }

        public string BookingDateFrom
        {
            get
            {
                return bookingDateFrom;
            }
            set
            {
                bookingDateFrom = value;
            }
        }
        public string BookingDateTo
        {
            get
            {
                return bookingDateTo;
            }
            set
            {
                bookingDateTo = value;
            }
        }

        public string ArrestDateFrom
        {
            get
            {
                return arrestDateFrom;
            }
            set
            {
                arrestDateFrom = value;
            }
        }
        public string ArrestDateTo
        {
            get
            {
                return arrestDateTo;
            }
            set
            {
                arrestDateTo = value;
            }
        }

        public string ReleaseDateFrom
        {
            get
            {
                return releaseDateFrom;
            }
            set
            {
                releaseDateFrom = value;
            }
        }
        public string ReleaseDateTo
        {
            get
            {
                return releaseDateTo;
            }
            set
            {
                releaseDateTo = value;
            }
        }

        public string DataLoadDateFrom
        {
            get
            {
                return dataLoadDateFrom;
            }
            set
            {
                dataLoadDateFrom = value;
            }
        }
        public string DataLoadDateTo
        {
            get
            {
                return dataLoadDateTo;
            }
            set
            {
                dataLoadDateTo = value;
            }
        }

        public string ViolationDescription
        {
            get
            {
                return violationDescription;
            }
            set
            {
                violationDescription = value;
            }
        }

        public string Disposition
        {
            get
            {
                return disposition;
            }
            set
            {
                disposition = value;
            }
        }

        public string AddressVerification
        {
            get
            {
                return addressVerification;
            }
            set
            {
                addressVerification = value;
            }
        }

        public string Zipcode
        {
            get
            {
                return zipcode;
            }
            set
            {
                zipcode = value;
            }
        }

        public string CourtRoom
        {
            get
            {
                return courtRoom;
            }
            set
            {
                courtRoom = value;
            }
        }

        public string Floor
        {
            get
            {
                return floor;
            }
            set
            {
                floor = value;
            }
        }


        #endregion

        #region Methods

        public DataSet SearchRecords()
        {

                //Kazim 3569 4/7/2008 Add field Level and Disposition
                //Kazim 3735 5/13/2008 Add field Violation Description 

                // Agha Usman 4239 06/25/2008 - Add New parameter called @rowcount

                string[] searchkey ={"@CDI","@CaseNumber","@LastName","@FirstName","@Spn","@DOB","@CourtRoom","@Attorney",
                "@NextSettingDateFrom","@NextSettingDateTo","@DataLoadDateFrom","@DataLoadDateTo","@Level","@Disposition","@ViolationDesc","@rowcount"};

                string[] values ={CDI,CaseNumber,LastName,FirstName,Spn,DOB,CourtRoom,Attorney,
                           NextSettingDateFrom,NextSettingDateTo,DataLoadDateFrom,DataLoadDateTo,level ,disposition,violationDescription,pageSize.ToString() };
                //DataSet ds = new DataSet();
                //getting data from jims database
                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HCC_Get_Report_By_Personal_Information", searchkey, values);
                return ds;
            
            
        }

        public DataTable Fill_ddl_list(string spname, string db)
        {
            DataTable dt = new DataTable();

            try
            {
                if (db == "JIMS")
                {
                    dt = ClsDb.Get_DT_BySPArr(spname);

                    return dt;
                }
                else
                {
                    dt = ClsDbt.Get_DT_BySPArr(spname);
                    return dt;
                }

            }

            catch (Exception e)
            {
                throw new ApplicationException(e.Message, e);

            }
           // return dt;

        }

        public DataSet FillDropDownList(string spname, string key, string val)
        {
            DataSet Ds = new DataSet();
            try
            {
                Ds = ClsDbt.Get_DS_BySPByOneParmameter(spname, key, val);
                return Ds;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message, e);
            }
            //return Ds;
        }

        public DataSet CaseDetail(string bookingnumber, string casenumber)
        {
            DataSet Ds = new DataSet();
            try
            {
                string key = "@BookingNumber";

                Ds = ClsDb.Get_DS_BySPByTwoParmameter("usp_hcc_get_criminaldetails", key, bookingnumber, "@casenumber", casenumber);
                return Ds;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message, e);
            }
            //return Ds;
        }

        public DataSet GetConnectedPersons(string casenumber)
        {

            DataSet Ds = new DataSet();
            try
            {
                Ds = ClsDb.Get_DS_BySPByOneParmameter("USP_HTS_Get_Connection_Information", "@CaseNumber", casenumber);
                return Ds;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message, e);
            }
            //return Ds;

        }
        public DataSet Get_Attorney_List() 
        {
            DataSet ds;
            ds=ClsDb.Get_DS_BySP("USP_HCC_GET_ATTORNEYS");
            return ds;
        
        }
        // Noufil 3589 05/14/2008 Function to get Criminal Follow Up Records
        /// <summary>
        /// This Function get Criminal FollowUp Records
        /// </summary>
        /// <param name="courtid">Search with respect to court</param>
        /// <param name="datefrom">Date range from which searching starts with</param>
        /// <param name="dateto">Date range to which searching ends</param>
        /// ozair 4412 07/14/2008
        /// <param name="showall">check for show all records</param>
        /// <returns>DataTable</returns>
        public DataTable Get_CriminalFollowUp(int courtid,DateTime datefrom,DateTime dateto,bool showall)
        {
            string[] key = { "@courtid", "@datefrom", "@dateto", "@showAll" };
            object[] value = { courtid, datefrom, dateto, showall };
            //end ozair 4412
            //ozair 4389 07/12/2008 removed dbo. from sp name.
            DataTable dt = ClsDbt.Get_DT_BySPArr("USP_HTP_GetCriminalFollow", key, value);
            //end ozair 4389
            return dt;
        }
        // Fahad 5098 01/01/2009 Function to get Contract Follow Up Records  
        /// <summary>
        /// This Function get Contract FollowUp Records
        /// </summary>
        /// <param name="courtid">Search with respect to court for now we are working for just AG Court</param>
        /// <param name="datefrom">Date range from which searching starts with</param>
        /// <param name="dateto">Date range to which searching ends</param>+
        /// <returns>DataTable</returns>
        /// Fahad 5477 Not Null check Added
        public DataTable Get_ContractFollowUp()
        {
                DataTable dt = ClsDbt.Get_DT_BySPArr("USP_HTP_GetContractFollowUp");
                if (dt != null)
                    return dt;
                else
                    throw new NullReferenceException("Data Table Contains Null Values");
               
           
        }
        /// <summary>
        /// This fuction returns all Criminal Courts
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetCriminalCOurt()
        {
            DataTable dt = ClsDbt.Get_DT_BySPArr("USP_HTP_GetCriminalCourts");
            return dt;
        }

        /// <summary>
        /// This function Update the Followupdate
        /// </summary>
        /// <param name="date">Updated Follow up date </param>
        /// <param name="ticketid">Ticket ID of user</param>
        /// <returns>True/False</returns>       
        public void UpdateFollowupdate(DateTime date, int ticketid)
        {
            string[] key = { "@follow", "@ticketid" };
            object[] value = { date, ticketid };
            ClsDbt.ExecuteSP("USP_HTP_Update_FollowUpdate", key, value);          
        }
        #endregion


    }
}

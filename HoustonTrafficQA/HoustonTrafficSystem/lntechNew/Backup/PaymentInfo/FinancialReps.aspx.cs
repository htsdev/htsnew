using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using System.IO;
//using System.Web.Mail;
using System.Net.Mail;
using System.Text;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using System.Configuration;
//using lntechNew.Components.ClientInfo;
//using lntechDallasNew.Components.ClientInfo;




//using iTextSharp.text;
//using iTextSharp.text.pdf;


namespace HTP.PaymentInfo
{
    /// <summary>
    /// Summary description for PaymentInfo.
    /// </summary>
    public partial class FinancialReps : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.DropDownList cmbType;


        clsENationWebComponents objEnationFramework = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();   //added 23-8-07
        FinancialReports FillData = new FinancialReports(); //added 23-8-07 to use in void payment details

        protected System.Web.UI.WebControls.TextBox lblHeading1;
        protected System.Web.UI.WebControls.TextBox lblHeading1_1;
        protected System.Web.UI.WebControls.DataGrid dgrdPayType;
        protected System.Web.UI.WebControls.DropDownList cmbRep;
        protected System.Web.UI.WebControls.DropDownList cmbPayType;
        protected System.Web.UI.WebControls.DataGrid dgrdPayDetail;
        protected System.Web.UI.WebControls.TextBox Textbox1;
        protected System.Web.UI.WebControls.TextBox Textbox2;
        protected eWorld.UI.CalendarPopup calQueryDate;
        //string sqlQuery;
        DataSet dsPaymentInfo;
        protected System.Web.UI.WebControls.TextBox txtPC;
        protected eWorld.UI.CalendarPopup calTo;
        protected System.Web.UI.WebControls.ImageButton imgbtnGo;
        protected System.Web.UI.WebControls.LinkButton lnkbtnPrint;
        protected System.Web.UI.WebControls.TextBox txtPR;
        //string pdfile;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.TextBox txtRS;
        protected System.Web.UI.WebControls.TextBox Textbox3;
        protected System.Web.UI.WebControls.TextBox Textbox4;
        protected System.Web.UI.WebControls.DataGrid dgrdPayByRep;
        protected System.Web.UI.WebControls.Label lblTotalCashSys;
        protected System.Web.UI.WebControls.Label lblTotalCashActual;
        protected System.Web.UI.WebControls.Label lblTotalCheckSys;
        protected System.Web.UI.WebControls.Label lblTotalCheckActual;
        protected System.Web.UI.WebControls.Label lblTotalSystem;
        protected System.Web.UI.WebControls.Label lblTotalActual;
        protected System.Web.UI.WebControls.TextBox txtRemarks;


        //string strMailTo="";
        //string strMailCC="";
        //string strMailBCC="";

        protected System.Web.UI.WebControls.TextBox Textbox5;
        protected System.Web.UI.WebControls.TextBox Textbox6;
        protected System.Web.UI.WebControls.DataGrid dgrdCourt;
        protected System.Web.UI.WebControls.TextBox txtCS;
        protected System.Web.UI.WebControls.TextBox txtCSS;
        double TotalAmount = 0;
        double TotalCourtFee = 0;
        Int32 TotalCount = 0;
        protected System.Web.UI.WebControls.DropDownList cmbCourt;
        protected System.Web.UI.WebControls.TextBox Textbox7;
        protected System.Web.UI.WebControls.DataGrid dgCategoryType;
        Int32 TotalCourtTrans = 0;

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        protected System.Web.UI.WebControls.TextBox Textbox8;
        protected System.Web.UI.WebControls.TextBox txtCT;
        protected System.Web.UI.WebControls.TextBox txtCC;
        protected System.Web.UI.WebControls.TextBox iCount;
        DataView dv_Result;

        protected System.Web.UI.WebControls.TextBox txtCheck;
        protected System.Web.UI.WebControls.TextBox Textbox9;
        protected System.Web.UI.WebControls.TextBox Textbox10;
        protected System.Web.UI.WebControls.DataGrid dgCredit;
        long sumCount = 0;

        double sumAmount = 0.00;
        long CreditsumCount = 0;
        double CreditSumAmount = 0.00;
        //Ozair 5057 03/24/2009 Warnings Removed



        private void Page_Load(object sender, System.EventArgs e)
        {

            // imgbtnGo.Attributes.Add("onClick","return isDate();");

            lblMessage.Text = "";

            string cookyval = "0";

            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                //else if (ClsSession.GetCookie("sAccessType", this.Request).ToString() != "2")
                //{
                //    Response.Redirect("../LoginAccesserror.aspx", false);
                //    Response.End();
                //}
                else //To stop page further execution
                {
                    ValidateRequest();
                    // Put user code to initialize the page here
                    if (!Page.IsPostBack)
                    {
                        ClsSession.CreateCookie("sMoved", "False", this.Request, this.Response);
                        // Sabir Khan 10920 05/27/2013 Fill Branch Drop down.
                        GetBranchesName();
                        LoadInital();
                        //FillCategoryType();
                        //FillCreditType();
                        BindControls();


                        //GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
                        //GetPaymentTypeSumByDate();

                        //GetCourtSummary();
                        //GetVoidPaymentRecords();//added 24-8-07 to  fill void payment  records--khalid
                        //GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);

                        Session["RepID"] = cmbRep.SelectedIndex;
                        Session["PaymentID"] = cmbPayType.SelectedIndex;
                        Session["CourtID"] = cmbCourt.SelectedIndex;

                        //GetPaymentDetailReport(calQueryDate.SelectedDate);


                        //GetAttorneyCredit();
                        //GetAttorneyCredit2();


                        //Added by Adil - for 3309 Iteration # 8 point # 4
                        btn_update1_Click(null, null);


                    }


                    if (Request.Cookies["CanUpdateCloseOutLog"] != null)
                    {
                        cookyval = Server.HtmlEncode(Request.Cookies["CanUpdateCloseOutLog"].Value);
                    }


                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

        }

        private void GetVoidPaymentRecords()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
                FillData.Values = values;
                DataSet DS = FillData.GetData();
                //khalid 3309  3/6/08 to fix binding bug
                if (DS.Tables.Count > 0)
                {
                    DG_voidTransactions.DataSource = DS;
                    ClsSession.SetSessionVariable("dvVoidResult", DS.Tables[0].DefaultView, this.Session);
                    DG_voidTransactions.DataBind();
                    BindPaymentVoidDetail();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetAttorneyCredit()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
                FillData.Values = values;
                DataSet DS = FillData.GetAttorneyCredit();
                //khalid 3309  3/6/08 to fix binding bug
                if (DS.Tables.Count > 0)
                {
                    DG_AttorneyCredit.DataSource = DS;
                    ClsSession.SetSessionVariable("dvattResult", DS.Tables[0].DefaultView, this.Session);
                    DG_AttorneyCredit.DataBind();
                    BindAttorneyCredit();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetAttorneyCredit2()
        {
            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue,Convert.ToInt32(ddl_Branch.SelectedValue) };
                FillData.Values = values;
                DataSet DS = FillData.GetFeeWeived();
                //khalid 3309  3/6/08 to fix binding bug
                if (DS.Tables.Count > 0)
                {
                    DG_AttorneyCredit2.DataSource = DS;
                    ClsSession.SetSessionVariable("dvattResult2", DS.Tables[0].DefaultView, this.Session);
                    DG_AttorneyCredit2.DataBind();
                    BindAttorneyCredit2();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        private void BindPaymentVoidDetail()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in DG_voidTransactions.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblNo"))).Text = sNo.ToString();



                    // CardType Column					
                    ((Label)(ItemX.FindControl("lblCardType"))).Text = GetCardType(((Label)(ItemX.FindControl("lblCardTypeID"))).Text.ToString());

                    // RED MARKING THE REFUND TRANSACTIONS.......
                    if (((Label)(ItemX.FindControl("lblPTypeId"))).Text == "8")
                    {
                        ((Label)(ItemX.FindControl("lblNo"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblDate"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblTime"))).ForeColor = System.Drawing.Color.Red;
                        ((LinkButton)(ItemX.FindControl("lnkCustomer"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblRep"))).ForeColor = System.Drawing.Color.Red;


                        ((Label)(ItemX.FindControl("lblPayTypeFR"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCardType"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCourt"))).ForeColor = System.Drawing.Color.Red;

                    }



                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindAttorneyCredit()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in DG_AttorneyCredit.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblNo"))).Text = sNo.ToString();



                    // CardType Column					


                    // RED MARKING THE REFUND TRANSACTIONS.......
                    if (((Label)(ItemX.FindControl("lblPTypeId"))).Text == "8")
                    {
                        ((Label)(ItemX.FindControl("lblNo"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblDate"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblTime"))).ForeColor = System.Drawing.Color.Red;
                        ((LinkButton)(ItemX.FindControl("lnkCustomer"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblRep"))).ForeColor = System.Drawing.Color.Red;


                        ((Label)(ItemX.FindControl("lblPayTypeFR"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCardType"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCourt"))).ForeColor = System.Drawing.Color.Red;

                    }



                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindAttorneyCredit2()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in DG_AttorneyCredit2.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblNo"))).Text = sNo.ToString();



                    // CardType Column					


                    // RED MARKING THE REFUND TRANSACTIONS.......
                    if (((Label)(ItemX.FindControl("lblPTypeId"))).Text == "8")
                    {
                        ((Label)(ItemX.FindControl("lblNo"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblDate"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblTime"))).ForeColor = System.Drawing.Color.Red;
                        ((LinkButton)(ItemX.FindControl("lnkCustomer"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblRep"))).ForeColor = System.Drawing.Color.Red;


                        ((Label)(ItemX.FindControl("lblPayTypeFR"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCardType"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCourt"))).ForeColor = System.Drawing.Color.Red;

                    }



                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void FillCategoryType()
        {
            //string[] key1 = { "@sDate", "@eDate", "@firmId" };

            ////khalid 3309 3/11/08 to handle page load category type summary bug
            //int selectedval;
            //if (ddlOutSideFirm.SelectedValue == "")
            //    selectedval = 0;
            //else
            //{
            //    selectedval = Convert.ToInt32(ddlOutSideFirm.SelectedValue);
            //}

            //object[] value1 = { calQueryDate.SelectedDate, calTo.SelectedDate, selectedval == 0 ? (object)DBNull.Value : (object)selectedval };

            //dgCategoryType.DataSource = objEnationFramework.Get_DS_BySPArr("USP_HTS_Get_FinReport_BalancePaid_Ver2", key1, value1);
            //dgCategoryType.DataBind();
            //Sabir Khan 5418 01/26/2009 Move the method to FinancialReps class...
            FillData = new FinancialReports();
            // Sabir Khan 10920 05/27/2013 Branch ID added
            object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
            FillData.Values = values;
            dgCategoryType.DataSource = FillData.FillCategoryType();
            dgCategoryType.DataBind();
        }

        
        private void FillCreditType()
        {
            //string[] key1 = { "@sDate", "@eDate", "@firmId" };
            ////khalid 3309 3/11/08 to handle page load category type summary bug
            //int selectedval;
            //if (ddlOutSideFirm.SelectedValue == "")
            //    selectedval = 0;
            //else
            //{
            //    selectedval = Convert.ToInt32(ddlOutSideFirm.SelectedValue);
            //}

            //object[] value1 = { calQueryDate.SelectedDate, calTo.SelectedDate, selectedval == 0 ? (object)DBNull.Value : (object)selectedval };

            //// object[] value1 = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue };
            //dgCredit.DataSource = objEnationFramework.Get_DS_BySPArr("USP_HTS_Get_FinReport_CreditType", key1, value1);
            //dgCredit.DataBind();
            //Sabir Khan 5418 01/26/2009 Move the method to FinancialReps class...
            FillData = new FinancialReports();
            // Sabir Khan 10920 05/27/2013 Branch ID added
            object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
            FillData.Values = values;
            dgCredit.DataSource = FillData.FillCreditType();
            dgCredit.DataBind();
        }


        private void GetPaymentTypeSumByDate()
        {
            ////Change by Ajmal
            ////SqlDataReader DRPaySum;
            //IDataReader DRPaySum;
            //string[] key1 = { "@RecDate", "@RecTo", "@firmId" };
            //object[] value1 = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue };
            ////DRPaySum = objEnationFramework.Get_DR_BySPByTwoParmameter("usp_Get_All_PaymentSumByRecDateRange", "RecDate", calQueryDate.SelectedDate, "RecTo", calTo.SelectedDate);
            ////DRPaySum =  objEnationFramework.Get_DR_BySPByOneParmameter ("usp_Get_All_PaymentSumByRecDate","RecDate",calQueryDate.SelectedDate );
            //Sabir Khan 5418 01/26/2009 Move the method to FinancialReps class...
            FillData = new FinancialReports();
            // Sabir Khan 10920 05/27/2013 Branch ID added
            object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
            FillData.Values = values;
            dgrdPayType.DataSource = FillData.GetPaymentTypeSumByDate();
            dgrdPayType.DataBind();
            //DRPaySum = objEnationFramework.Get_DR_BySPArr("usp_Get_All_PaymentSumByRecDaterange", key1, value1);
            //dgrdPayType.DataSource = DRPaySum;
            //dgrdPayType.DataBind();

            iCount.Text = dgrdPayType.Items.Count.ToString();

            foreach (DataGridItem ItemX in dgrdPayType.Items)
            {
                string PayType = ((Label)(ItemX.FindControl("lblPayType"))).Text;
                if ((PayType == "Visa") || (PayType == "Master Card") || (PayType == "American Express") || (PayType == "Discover"))
                    ((Label)(ItemX.FindControl("lblPayTypeID"))).Text = "50" + ((Label)(ItemX.FindControl("lblPayTypeID"))).Text;


                double dAmount = Convert.ToDouble(((Label)(ItemX.FindControl("lblAmount"))).Text.ToString());
                ((Label)(ItemX.FindControl("lblAmount"))).Text = String.Format("{0:c}", dAmount);

            }

        }


        private void GetCourtSummary()
        {
            try
            {
                ////Change by Ajmal
                ////SqlDataReader drCourt;
                //IDataReader drCourt;


                //string[] key1 = { "@RecDate", "@RecTo", "@firmId" };
                //object[] value1 = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue };

                //drCourt = objEnationFramework.Get_DR_BySPArr("usp_Get_All_CourtInfoByRecDateRange", key1, value1);
                //dgrdCourt.DataSource = drCourt;
                //dgrdCourt.DataBind();

                //tahir ahmed 3736 05/02/2008
                //for grouping of courts....
                //Sabir Khan 5418 01/26/2009 Move the method to FinancialReps class...
                FillData = new FinancialReports();
                // Sabir Khan 10920 05/27/2013 Branch ID added
                object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
                FillData.Values = values;
                dgrdCourt.DataSource = FillData.GetCourtSummary();
                dgrdCourt.DataBind();
                iCourtCount.Text = dgrdCourt.Items.Count.ToString();



            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void LoadInital()
        {
            try
            {
                calQueryDate.SelectedDate = DateTime.Now.Date;
                calQueryDate.VisibleDate = calQueryDate.SelectedDate;

                calTo.SelectedDate = calQueryDate.SelectedDate;
                calTo.VisibleDate = calTo.SelectedDate;

                objEnationFramework.FetchValuesInWebControlBysp(cmbCourt, "usp_Get_All_Court", "ShortName", "Courtid");
                cmbCourt.Items[0].Text = "All Courts";

                cmbCourt.Items.Remove(cmbCourt.Items.FindByValue("3001"));
                cmbCourt.Items.Remove(cmbCourt.Items.FindByValue("3002"));
                cmbCourt.Items.Remove(cmbCourt.Items.FindByValue("3003"));
                // tahir 4225 07/16/2008
                cmbCourt.Items.Insert(1, new ListItem("All HMC", "1"));
                cmbCourt.Items.Insert(2, new ListItem("All HCJP", "2"));
                cmbCourt.Items.Insert(3, new ListItem("All Other", "100"));
                // end 4225


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

        }

        public void GetReps()
        {
            DataSet dsRepList = new DataSet();
            string[] key1 = { "@sDate", "@eDate" };
            object[] value1 = { calQueryDate.SelectedDate, calTo.SelectedDate };
            dgrdPayByRep.DataSource = objEnationFramework.Get_DS_BySPArr("usp_get_all_replist", key1, value1);
            dgrdPayByRep.DataBind();

            string[] key2 = { "@sDate", "@eDate" };
            object[] value2 = { calQueryDate.SelectedDate, calTo.SelectedDate };
            dsRepList = objEnationFramework.Get_DS_BySPArr("usp_Get_All_RepList", key2, value2);


            cmbRep.DataSource = dsRepList;
            cmbRep.DataTextField = dsRepList.Tables[0].Columns[0].ColumnName;
            cmbRep.DataValueField = dsRepList.Tables[0].Columns[1].ColumnName;
            cmbRep.DataBind();
            cmbRep.Items.Insert(0, "All Reps");
        }

        private void BindControls()
        {
            //Change by Ajmal
            //SqlDataReader drRepList = null;

            IDataReader drRepList = null;

            try
            {
                GetReps();

                //objEnationFramework.FetchValuesInWebControlBysp( cmbRep ,"usp_Get_All_RepList","RepName","RepID");
                drRepList = objEnationFramework.Get_DR_BySP("usp_Get_All_CCType");
                //Fahad 6054 07/30/2009 Comment the following 4 items of the List
                //System.Web.UI.WebControls.ListItem Item10 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney/Friend Credit)", "-1");
                //cmbPayType.Items.Add(Item10);

                //System.Web.UI.WebControls.ListItem Item11 = new System.Web.UI.WebControls.ListItem("Show All (Incl. Attorney Credit)", "-200");
                //cmbPayType.Items.Add(Item11);

                //System.Web.UI.WebControls.ListItem Item1 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney/Friend Credit)", "0");
                //cmbPayType.Items.Add(Item1);

                //System.Web.UI.WebControls.ListItem Item2 = new System.Web.UI.WebControls.ListItem("Show All (Excl. Attorney Credit)", "200");
                //cmbPayType.Items.Add(Item2);

                System.Web.UI.WebControls.ListItem Item10 = new System.Web.UI.WebControls.ListItem("Show All (With All Credit)", "1");
                cmbPayType.Items.Add(Item10);

                System.Web.UI.WebControls.ListItem Item11 = new System.Web.UI.WebControls.ListItem("Show All (Without Credit)", "200");
                cmbPayType.Items.Add(Item11);

                System.Web.UI.WebControls.ListItem Item3 = new System.Web.UI.WebControls.ListItem("CC All (Incl. Manual)", "300");
                cmbPayType.Items.Add(Item3);

                System.Web.UI.WebControls.ListItem Item4 = new System.Web.UI.WebControls.ListItem("CC All (Excl. Manual)", "301");
                cmbPayType.Items.Add(Item4);



                while (drRepList.Read())
                {
                    //MyContainer container = new MyContainer();
                    //container.SomeString = reader.GetString(0);
                    //container.SomeInt = reader.GetInt(1);
                    //alReps.Add(container);
                    System.Web.UI.WebControls.ListItem ItemY = new System.Web.UI.WebControls.ListItem();

                    ItemY.Text = drRepList["Description"].ToString();
                    ItemY.Value = drRepList["PaymentType_PK"].ToString();
                    cmbPayType.Items.Add(ItemY);
                }

                // CC Types
                System.Web.UI.WebControls.ListItem Item5 = new System.Web.UI.WebControls.ListItem("CC (VISA)", "501");
                cmbPayType.Items.Add(Item5);
                System.Web.UI.WebControls.ListItem Item6 = new System.Web.UI.WebControls.ListItem("CC (DISCOVER)", "504");
                cmbPayType.Items.Add(Item6);
                System.Web.UI.WebControls.ListItem Item7 = new System.Web.UI.WebControls.ListItem("CC (MASTER CARD)", "502");
                cmbPayType.Items.Add(Item7);
                System.Web.UI.WebControls.ListItem Item8 = new System.Web.UI.WebControls.ListItem("CC (AMEX)", "503");
                cmbPayType.Items.Add(Item8);
                //System.Web.UI.WebControls.ListItem Item9 = new System.Web.UI.WebControls.ListItem("CC (MANUAL)", "500" );				
                //cmbPayType.Items.Add(Item9);
                FillFirms(ddlOutSideFirm);
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
            finally
            {
                drRepList.Close();
            }


        }


        private void GetPaymentDetailByDateByRep(DateTime QueryDate)
        {
            DataSet dsWeeklyPaymentDetailsByRep;
            string strEmployeeID;
            //string Temp;
            Decimal Total;
            Decimal TotalActual = 0;
            string strExpr;

            Decimal CashSysTotal = 0;
            Decimal CashActualTotal = 0;
            Decimal CheckSysTotal = 0;
            Decimal CheckActualTotal = 0;
            Decimal TotalSysTotal = 0;
            Decimal TotalActualTotal = 0;
            //bool bAlternate = false;


            try
            {
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] key1 = { "@RecDate", "@RecDateTo", "@firmId", "@BranchID" };
                object[] value1 = { QueryDate, calTo.SelectedDate, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };

                dsPaymentInfo = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByRecDateRange", key1, value1);
                string[] key2 = { "@TransDate", "@TransDateTo", "@BranchID" };
                object[] value2 = { QueryDate, calTo.SelectedDate, Convert.ToInt32(ddl_Branch.SelectedValue) };
                dsWeeklyPaymentDetailsByRep = objEnationFramework.Get_DS_BySPArr("usp_Get_All_tblPaymentDetailWeeklyByDateRange", key2, value2);
                //dsWeeklyPaymentDetailsByRep = objEnationFramework.Get_DS_BySPByTwoParmameter("usp_Get_All_tblPaymentDetailWeeklyByDateRange", "TransDate", QueryDate, "TransDateTo", calTo.SelectedDate);


                /* Get Day End Notes
                if(dsWeeklyPaymentDetailsByRep.Tables[0].Rows.Count > 0)
                {
                    txtRemarks.Text = dsWeeklyPaymentDetailsByRep.Tables[0].Rows[0]["Notes"].ToString();
                }
                else
                {
                    txtRemarks.Text ="";
                }
                */

                //create and populate a DataColumn to set the PK
                //DataColumn [] pkColumn = new DataColumn[1];
                //pkColumn[0] = dsPaymentInfo.Tables[0].Columns["EmployeeID"];
                //set the primary key 
                //dsWeeklyPaymentDetailsByRep.Tables[0].PrimaryKey = pkColumn;			

                foreach (DataGridItem ItemX in dgrdPayByRep.Items)
                {
                    DataRow[] foundRows;
                    DataRow[] PaymentRows;
                    //string txtboxID;
                    //string fxName;
                    Decimal dActualCash = 0;
                    Decimal dActualCheck = 0;
                    Decimal dSysCash = 0;
                    Decimal dSysCheck = 0;


                    /*
                                        // js Validation Bind to textboxes
                                        txtboxID= ((TextBox)(ItemX.FindControl("txtActualCash"))).ClientID.ToString(); 
                                        fxName = "return ValidateMe(" + txtboxID + ");" ;
                                        ((TextBox)(ItemX.FindControl("txtActualCash"))).Attributes.Add("Onblur", fxName); 


                                        txtboxID= ((TextBox)(ItemX.FindControl("txtActualCheck"))).ClientID.ToString(); 
                                        fxName = "return ValidateMe(" + txtboxID + ");" ;
                                        ((TextBox)(ItemX.FindControl("txtActualCheck"))).Attributes.Add("Onblur", fxName); 
                                        //  Bind End

                    */
                    strEmployeeID = ((Label)(ItemX.FindControl("lblEmployeeID"))).Text.ToString();
                    //find a row based on the value entered in the textbox
                    //DataRow rowWeeklyPay = dsWeeklyPaymentDetailsByRep.Tables[0].Rows.Find(EmployeeID);
                    dsWeeklyPaymentDetailsByRep.Tables[0].Select();

                    strExpr = "EmployeeID =" + strEmployeeID;
                    foundRows = dsWeeklyPaymentDetailsByRep.Tables[0].Select(strExpr);


                    if (foundRows.Length > 0)
                    {
                        ((TextBox)(ItemX.FindControl("txtActualCash"))).Text = String.Format("{0:#.##}", (foundRows[0]["ActualCash"]));
                        ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text = String.Format("{0:#.##}", (foundRows[0]["ActualCheck"]));

                        TotalActual = Convert.ToDecimal(foundRows[0]["ActualCash"]) + Convert.ToDecimal(foundRows[0]["ActualCheck"]);
                        dActualCash = Convert.ToDecimal(foundRows[0]["ActualCash"]);
                        dActualCheck = Convert.ToDecimal(foundRows[0]["ActualCheck"]);

                        CashActualTotal += dActualCash;
                        CheckActualTotal += dActualCheck;
                    }
                    else
                    {
                        ((TextBox)(ItemX.FindControl("txtActualCash"))).Text = "";
                        ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text = "";
                        TotalActual = 0;
                    }


                    dsPaymentInfo.Tables[0].Select();

                    strExpr = "EmpID =" + strEmployeeID;
                    PaymentRows = dsPaymentInfo.Tables[0].Select(strExpr);

                    if (PaymentRows.Length > 0)
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).Text = String.Format("{0:c}", PaymentRows[0]["CashAmount"]);
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).Text = String.Format("{0:c}", PaymentRows[0]["CheckAmount"]);

                        Total = Convert.ToDecimal(PaymentRows[0]["CashAmount"]) + Convert.ToDecimal(PaymentRows[0]["CheckAmount"]);
                        dSysCash = Convert.ToDecimal(PaymentRows[0]["CashAmount"]);
                        dSysCheck = Convert.ToDecimal(PaymentRows[0]["CheckAmount"]);

                        CashSysTotal += dSysCash;
                        CheckSysTotal += dSysCheck;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).Text = "";
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).Text = "";
                        Total = 0;
                    }



                    // Check difference and highlight
                    if (dSysCash != dActualCash)
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).BackColor = Color.Red;
                        ((Label)(ItemX.FindControl("lblSystemCash"))).ForeColor = Color.White;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemCash"))).BackColor = Color.White;
                        ((Label)(ItemX.FindControl("lblSystemCash"))).ForeColor = Color.Black;
                    }


                    if (dSysCheck != dActualCheck)
                    {
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).BackColor = Color.Red;
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).ForeColor = Color.White;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).BackColor = Color.White;
                        ((Label)(ItemX.FindControl("lblSystemCheck"))).ForeColor = Color.Black;
                    }



                    // Row System Total 
                    if (Total != 0)
                    {
                        ((Label)(ItemX.FindControl("lblSystemTotal"))).Text = String.Format("{0:c}", Total);
                        TotalSysTotal += Total;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblSystemTotal"))).Text = "";
                    }

                    // Row Actual Total 
                    if (TotalActual != 0)
                    {
                        ((Label)(ItemX.FindControl("lblActualTotal"))).Text = String.Format("{0:c}", TotalActual);
                        TotalActualTotal += TotalActual;
                    }
                    else
                    {
                        ((Label)(ItemX.FindControl("lblActualTotal"))).Text = "";
                    }

                    // If Total is zero so hide the row
                    if ((Total + TotalActual) == 0)
                    {
                        ItemX.Visible = false;
                    }
                    else
                    {
                        ItemX.Visible = true;
                    }

                }

                lblTotalCashSys.Text = String.Format("{0:c}", CashSysTotal);
                lblTotalCashActual.Text = String.Format("{0:c}", CashActualTotal);
                lblTotalCheckSys.Text = String.Format("{0:c}", CheckSysTotal);
                lblTotalCheckActual.Text = String.Format("{0:c}", CheckActualTotal);
                lblTotalSystem.Text = String.Format("{0:c}", TotalSysTotal);
                lblTotalActual.Text = String.Format("{0:c}", TotalActualTotal);

                //dgrdPayByRep.AlternatingItemStyle.BackColor= Color.FromArgb(238,238,238) ;

                //			foreach (DataGridItem ItemX in dgrdPayByRep.Items) 
                //			{
                //				if ( bAlternate==true )
                //				{
                //					ItemX.BackColor = Color.FromArgb(238,238,238) ;
                //					//bAlternate = ! bAlternate;
                //				}			
                //				bAlternate = ! bAlternate;
                //			}
                //			
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

        }

        private void GetPaymentDetailReport(DateTime QueryDate)
        {
            DataSet dsFinReport;
            int RepID = 0;
            int PayID = 0;
            int CourtID = 0;

            try
            {
                if (cmbRep.SelectedIndex != 0)
                {
                    RepID = Convert.ToInt32(cmbRep.SelectedValue);
                }

                //if (cmbPayType.SelectedIndex!=0 ) 						
                PayID = Convert.ToInt32(cmbPayType.SelectedValue);


                if (cmbCourt.SelectedIndex != 0)
                {
                    CourtID = Convert.ToInt32(cmbCourt.SelectedValue);
                }

                //PayID = Convert.ToInt32(cmbPayType.SelectedValue); 
                if (PayID > 500) // IF Credit Card
                {
                    // Sabir Khan 10920 05/27/2013 Branch ID added
                    //dsFinReport = objEnationFramework.Get_DS_BySPByFourParmameter("usp_Get_All_PaymentDetailOfCCByCriteriaRange","RecDate",QueryDate,"RecDateTo", calTo.SelectedDate,"EmployeeID", RepID, "PaymentType", PayID);
                    string[] keys = { "RecDate", "RecDateTo", "EmployeeID", "PaymentType", "CourtID", "@firmId", "@BranchID" };
                    object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, RepID, PayID, CourtID, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
                    dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailOfCCByCriteriaRange", keys, values);
                }
                else
                {
                    // Sabir Khan 10920 05/27/2013 Branch ID added
                    //dsFinReport = objEnationFramework.Get_DS_BySPByFourParmameter("usp_Get_All_PaymentDetailByCriteriaRange","RecDate",QueryDate,"RecDateTo", calTo.SelectedDate,"EmployeeID", RepID, "PaymentType", PayID);
                    string[] keys = { "RecDate", "RecDateTo", "EmployeeID", "PaymentType", "CourtID", "@firmId", "@BranchID" };
                    object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, RepID, PayID, CourtID, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
                    dsFinReport = objEnationFramework.Get_DS_BySPArr("usp_Get_All_PaymentDetailByCriteriaRange", keys, values);
                }

                //objEnationFramework.FetchValuesInWebControlBysp(dgrdPayDetail,"usp_Get_All_PaymentDetailByCriteria");  
                //dsFinReport = objEnationFramework.Get_DS_BySPByFourParmameter("usp_Get_All_PaymentDetailByCriteriaRange","RecDate",QueryDate,"RecDateTo", calTo.SelectedDate,"EmployeeID", RepID, "PaymentType", PayID);
                dgrdPayDetail.DataSource = dsFinReport;

                // STORING DATAVIEW IN SESSION THAT WILL BE USED FOR SORTING .....
                ClsSession.SetSessionVariable("dvResult", dsFinReport.Tables[0].DefaultView, this.Session);
                dgrdPayDetail.DataBind();

                BindPaymentDetailReport();
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

        }
        private void GetMailerSummary(DateTime QueryDate)
        {
            DataSet dsFinReport;
            //int FirmId = 0;
            int PayID = -1;
            int CourtID = 0;

            try
            {
                //ozair 4111 05/23/2008 added firm id as parameter and value against it.
                // Sabir Khan 10920 05/27/2013 Branch ID added
                string[] keys = { "@RecDate", "@RecDateTo", "@EmployeeID", "@PaymentType", "@CourtID", "@firmId", "@BranchID" };
                object[] values = { calQueryDate.SelectedDate, calTo.SelectedDate, 0, PayID, CourtID, ddlOutSideFirm.SelectedValue == "0" ? (object)DBNull.Value : (object)ddlOutSideFirm.SelectedValue, Convert.ToInt32(ddl_Branch.SelectedValue) };
                //end ozair 4111
                //Sabir Khan 6047 06/19/2009 Separate sp has been bind to the report....
                dsFinReport = objEnationFramework.Get_DS_BySPArr("USP_HTP_Get_MailerSummaryDetail", keys, values);

                dgMailerSummary.DataSource = dsFinReport;
                dgMailerSummary.DataBind();

                iMailerSummaryCount.Text = dgMailerSummary.Items.Count.ToString();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }

        }

        private void BindPaymentDetailReport()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in dgrdPayDetail.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblNo"))).Text = sNo.ToString();

                    //Bond Column 
                    if (((Label)(ItemX.FindControl("lblBond"))).Text == Convert.ToString('0'))
                    {
                        ((Label)(ItemX.FindControl("lblBond"))).Text = "";
                    }

                    // CardType Column					
                    ((Label)(ItemX.FindControl("lblCardType"))).Text = GetCardType(((Label)(ItemX.FindControl("lblCardTypeID"))).Text.ToString());

                    //Modified By Zeeshan Ahmed
                    // RED MARKING THE REFUND AND Bounce Check TRANSACTIONS.......
                    string PaymentType = ((Label)(ItemX.FindControl("lblPTypeId"))).Text;

                    if (PaymentType == "8" || PaymentType == "103")
                    {
                        ((Label)(ItemX.FindControl("lblNo"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblDate"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblTime"))).ForeColor = System.Drawing.Color.Red;
                        ((LinkButton)(ItemX.FindControl("lnkCustomer"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblRep"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblBond"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblPaidAmount"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblPayTypeFR"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCardType"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblCourt"))).ForeColor = System.Drawing.Color.Red;
                        ((Label)(ItemX.FindControl("lblListDate"))).ForeColor = System.Drawing.Color.Red;
                    }



                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void BindMailerSummaryReport()
        {
            int sNo = 0;
            try
            {
                foreach (DataGridItem ItemX in dgMailerSummary.Items)
                {
                    // Serial No.
                    //((LinkButton)(dgItem.FindControl("lblCCTypeID"))).Attributes.Add("Onclick", "LoadDocument();"); 
                    sNo += 1;
                    ((Label)(ItemX.FindControl("lblSNo"))).Text = sNo.ToString();

                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private string GetCardType(string CardTypeID)
        {
            switch (CardTypeID)
            {
                case "1":
                    return ("VISA");
                case "2":
                    return ("MC");
                case "3":
                    return ("AMEX");
                case "4":
                    return ("DISC");
                default:
                    return ("");
            }

        }


        private void GetPaymentSummary(int CCTypeID)
        {


            //DR  = objEnationFramework.Get_All_RecordsBySPByOneParmameter("usp_Get_All_PaymentDetailByDate","Name",CCTypeID); 

        }

        public void DoGetQueryString(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string strID;
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;



            try
            {
                if (e.CommandName == "DoGetPayment")
                {
                    strID = ((Label)(e.Item.FindControl("lblPayTypeID"))).Text.ToString();

                    cmbPayType.SelectedValue = strID.ToString();
                    //cmbRep.SelectedIndex=0;
                    //cmbCourt.SelectedIndex=0;
                    Session["RepID"] = cmbRep.SelectedIndex;
                    Session["PaymentID"] = cmbPayType.SelectedIndex;
                    Session["CourtID"] = cmbCourt.SelectedIndex;
                    GetPaymentDetailReport(calQueryDate.SelectedDate);
                }

                else if (e.CommandName == "DoGetRep")
                {
                    strID = ((Label)(e.Item.FindControl("lblEmployeeID"))).Text.ToString();

                    //cmbCourt.SelectedIndex=0;
                    cmbRep.SelectedValue = strID.ToString();
                    Session["RepID"] = cmbRep.SelectedIndex;
                    Session["PaymentID"] = cmbPayType.SelectedIndex;
                    Session["CourtID"] = cmbCourt.SelectedIndex;

                    GetPaymentDetailReport(calQueryDate.SelectedDate);
                }


                else if (e.CommandName == "DoGetCustomer")
                {
                    strID = ((Label)(e.Item.FindControl("lblTicketID"))).Text.ToString();
                    //strID = "../../ClientInfo/violationsfees.asp?caseNumber=" + strID;
                    strID = "../ClientInfo/violationfeeold.aspx?search=0&caseNumber=" + strID;
                    Response.Redirect(strID, false);
                }

                else if (e.CommandName == "DoGetCourt")
                {
                    strID = ((Label)(e.Item.FindControl("lblCourtID"))).Text.ToString();

                    cmbCourt.SelectedValue = strID.ToString();
                    //cmbPayType.SelectedIndex=0;  
                    //cmbRep.SelectedIndex=0;
                    Session["RepID"] = cmbRep.SelectedIndex;
                    Session["PaymentID"] = cmbPayType.SelectedIndex;
                    Session["CourtID"] = cmbCourt.SelectedIndex;

                    //GetPaymentDetailReportByCourtID(calQueryDate.SelectedDate);
                    GetPaymentDetailReport(calQueryDate.SelectedDate);
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            this.lnkbtnPrint.Click += new System.EventHandler(this.lnkbtnPrint_Click);
            this.dgCategoryType.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCategoryType_ItemDataBound);
            this.dgCredit.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCredit_ItemDataBound);
            this.dgrdPayType.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdPayType_ItemDataBound);
            this.dgrdCourt.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdCourt_ItemDataBound);
            this.cmbRep.SelectedIndexChanged += new System.EventHandler(this.cmbRep_SelectedIndexChanged);
            this.cmbPayType.SelectedIndexChanged += new System.EventHandler(this.cmbPayType_SelectedIndexChanged);
            this.cmbCourt.SelectedIndexChanged += new System.EventHandler(this.cmbCourt_SelectedIndexChanged);
            this.dgrdPayDetail.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdPayDetail_ItemCreated);
            this.dgrdPayDetail.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgrdPayDetail_SortCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            this.lnkBack.Click += new EventHandler(lnkBack_Click);

        }


        #endregion



        private void btnUpdate_Click(object sender, System.EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            SaveReport();

            //GetPaymentSummaryByDate(calQueryDate.SelectedDate); 
            GetPaymentTypeSumByDate();
            GetCourtSummary();
            GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
            //GetPaymentDetailReport(calQueryDate.SelectedDate);
        }

        private bool SaveReport()
        {
            try
            {
                //objEnationFramework.DeleteBySPByOneParmameter("usp_Del_tblPaymentDetailWeeklyByDate", "TransDate", calQueryDate.SelectedDate);
                FillData = new FinancialReports();
                FillData.DeletePaymentDetailByDate(calQueryDate.SelectedDate,Convert.ToInt32(ddl_Branch.SelectedValue));

                foreach (DataGridItem ItemX in dgrdPayByRep.Items)
                {
                    Decimal dCheck = 0;
                    Decimal dCash = 0;
                    string TempValue = "";

                    string EmployeeID = ((Label)(ItemX.FindControl("lblEmployeeID"))).Text.ToString();
                    TempValue = ((TextBox)(ItemX.FindControl("txtActualCash"))).Text;
                    if (TempValue != "")
                    {
                        dCash = Convert.ToDecimal(TempValue);
                    }

                    TempValue = ((TextBox)(ItemX.FindControl("txtActualCheck"))).Text;
                    if (TempValue != "")
                    {
                        dCheck = Convert.ToDecimal(TempValue);
                    }

                    if ((dCash + dCheck) > 0)
                    {
                        //objEnationFramework.InsertBySP("usp_Add_tblPaymentDetailWeekly", "EmployeeID", Convert.ToInt32(EmployeeID), "TransDate", calQueryDate.SelectedDate, "ActualCash", dCash, "ActualCheck", dCheck, "Notes", txtRemarks.Text.ToString());
                        FillData.InsertPaymentDetailByDate(Convert.ToInt32(EmployeeID), calQueryDate.SelectedDate, dCash, dCheck, txtRemarks.Text.ToString(),Convert.ToInt32(ddl_Branch.SelectedValue));

                    }
                }
                return true;
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                return false;
            }
        }

        private void dgrdPayDetail_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //			ListItemType lit = e.Item.ItemType; 
            //			if(ListItemType.Header == lit) 
            //			{ 
            //				//*** Redirect the default header rendering method to our own method 
            //				//e.Item.SetRenderMethodDelegate(new RenderMethod(NewRenderMethod)); 				
            //				//clsENationWebComponents obj  = new clsENationWebComponents();
            //				
            //				//obj.FetchValuesInWebControlBysp( e.Item.FindControl("cmbRep"),"usp_Get_All_RepList","RepName","RepID");
            //			} 
        }




        private void lnkbtnPrint_Click(object sender, System.EventArgs e)
        {
            //Response.Redirect(Session["DocID"].ToString()) ;
            //Response.Redirect("PreviewMain.aspx");
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            Session["ReportDate"] = calQueryDate.SelectedDate;
            Session["ReportDateTo"] = calTo.SelectedDate;

            //Response.Redirect("FinancialRepsPreview.aspx");
            clsFirms ClsFirms = new clsFirms();

            Table5.Visible = false;
            ActiveMenu1.Visible = false;
            tblPrintHeader.Visible = true;
            tblMain.Align = "left";
            lblDate.Text = calQueryDate.SelectedDate.ToShortDateString() + " To " + calTo.SelectedDate.ToShortDateString();
            if (ddlOutSideFirm.SelectedValue != "0")
            {
                DataTable dt = ClsFirms.GetFirmInfo(int.Parse(ddlOutSideFirm.SelectedValue)).Tables[0];
                lblFirm.Text = " of (" + dt.Rows[0]["FirmName"].ToString() + ")";
            }
            else
            {
                lblFirm.Text = "";
            }

        }

        private void calQueryDate_DateChanged(object sender, System.EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            cmbPayType.SelectedIndex = 0;
            cmbRep.SelectedIndex = 2;
            cmbCourt.SelectedIndex = 0;
            //GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
            GetPaymentTypeSumByDate();
            GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
            //GetPaymentDetailReport(calQueryDate.SelectedDate);		
        }

        private void cmbRep_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //cmbCourt.SelectedIndex=0;
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            GetPaymentDetailReport(calQueryDate.SelectedDate);
        }

        private void cmbPayType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //cmbCourt.SelectedIndex=0;
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            GetPaymentDetailReport(calQueryDate.SelectedDate);
        }



        private void calTo_DateChanged(object sender, System.EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            cmbPayType.SelectedIndex = 0;
            cmbRep.SelectedIndex = 0;
            cmbCourt.SelectedIndex = 0;
            //GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
            GetPaymentTypeSumByDate();
            GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
            //GetPaymentDetailReport(calQueryDate.SelectedDate);		

        }

        private void btnGo_Click(object sender, System.EventArgs e)
        {
            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;

            cmbPayType.SelectedIndex = 0;
            cmbRep.SelectedIndex = 0;
            cmbCourt.SelectedIndex = 0;
            //GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
            GetPaymentTypeSumByDate();
            GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
            GetPaymentDetailReport(calQueryDate.SelectedDate);
            GetMailerSummary(calQueryDate.SelectedDate);

            //////////////

            /*lblMessage.Text="";
            Session["flagPC"]=txtPC.Text ;
            Session["flagPR"]=txtPR.Text ;
            GetReps();
            cmbPayType.SelectedIndex=0;
            cmbRep.SelectedIndex=0;
            cmbCourt.SelectedIndex=0;
            //GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
            FillCreditType();
            GetPaymentTypeSumByDate();
            GetCourtSummary(); 
            GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
            GetPaymentDetailReport(calQueryDate.SelectedDate);*/
            GetVoidPaymentRecords();


            ////////////////




        }


        private void tDetail_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            GetPaymentDetailReport(DateTime.Now.Date);
        }

        protected void btn_update1_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";

            //khalid 3210 3/7/08 to validate selected date are not future
            if ((calQueryDate.SelectedDate > DateTime.Now.Date) || (calTo.SelectedDate > DateTime.Now.Date))
            {
                lblMessage.Text = "Please Select past or current Date";
                return;
            }

            Session["flagPC"] = txtPC.Text;
            Session["flagPR"] = txtPR.Text;
            GetReps();
            cmbPayType.SelectedIndex = 0;
            cmbRep.SelectedIndex = 0;
            cmbCourt.SelectedIndex = 0;
            //GetPaymentSummaryByDate(calQueryDate.SelectedDate);  
            FillCategoryType();
            FillCreditType();
            GetPaymentTypeSumByDate();
            GetCourtSummary();
            GetPaymentDetailByDateByRep(calQueryDate.SelectedDate);
            GetPaymentDetailReport(calQueryDate.SelectedDate);
            GetVoidPaymentRecords();
            GetAttorneyCredit();
            GetAttorneyCredit2();

            //Kazim 3299 4/30/2008 Bind Mailer Summary Section 

            GetMailerSummary(calQueryDate.SelectedDate);
        }

        private void lnkbtnBack_Click(object sender, System.EventArgs e)
        {
            //Response.Redirect ("../../Activities/ReminderCalls.asp");
            Response.Redirect("../../backroom/Pricing.asp");
        }


        private void dgrdPayType_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string PayType = ((Label)(e.Item.FindControl("lblPayType"))).Text;
                string PayTypeID = ((Label)(e.Item.FindControl("lblPayTypeID"))).Text.ToString();
                //if (PayTypeID != "2" && PayTypeID != "4")
                {
                    //TotalCount += Convert.ToInt32( ((Label)(e.Item.FindControl("lblCount"))).Text.ToString()) ;
                    //TotalAmount += Convert.ToDouble ( ((Label)(e.Item.FindControl("lblAmount"))).Text.ToString()) ;
                }
                //Sabir 5755 04/06/2009 Check for prepaid legal
                if ((PayType != "Visa") && (PayType != "Master Card") && (PayType != "American Express") && (PayType != "Discover") && (PayTypeID != "2") && (PayTypeID != "4") && (PayTypeID != "3"))
                {
                    TotalCount += Convert.ToInt32(((Label)(e.Item.FindControl("lblCount"))).Text.ToString());
                    TotalAmount += Convert.ToDouble(((Label)(e.Item.FindControl("lblAmount"))).Text.ToString());
                }




                ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent"))).Attributes.Add("onclick", "ShowHideCCDetail('" + ((Label)(e.Item.FindControl("lblPayTypeID"))).Text + "');");

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                ((Label)(e.Item.FindControl("lbl_Count"))).Text = TotalCount.ToString();
                ((Label)(e.Item.FindControl("lbl_Amount"))).Text = (string.Format("{0:c}", TotalAmount) + "&nbsp;&nbsp;&nbsp;");

                ViewState.Add("TotalPM", TotalCount);
                ViewState.Add("TotalPMAmount", TotalAmount);
            }

        }

        private void dgrdCourt_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (((Label)(e.Item.FindControl("lblTrans"))).Text.ToString() != " " && ((Label)(e.Item.FindControl("lblFee"))).Text.ToString() != " ")
                    {
                        if (((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text.ToString() == "0")
                        {
                            TotalCourtTrans += Convert.ToInt32(((Label)(e.Item.FindControl("lblTrans"))).Text.ToString());
                            TotalCourtFee += Convert.ToDouble(((Label)(e.Item.FindControl("lblFee"))).Text.ToString());
                        }

                        double dAmount = Convert.ToDouble(((Label)(e.Item.FindControl("lblFee"))).Text.ToString());
                        ((Label)(e.Item.FindControl("lblFee"))).Text = String.Format("{0:c}", dAmount);
                    }


                    //TAHIR AHMED 3736 05/02/2008
                    //SETTING JAVA SCRIPT FUNCTION FOR THE IMAGE IN COURT GROUP

                    Int16 iGroup = Convert.ToInt16(((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text);
                    Int16 iCategory = Convert.ToInt16(((Label)(e.Item.FindControl("lblCategoryNumber"))).Text);

                    //if (((Label)(e.Item.FindControl("lblIsCourtCategory"))).Text.ToString() == "1")
                    if (iGroup == 1 && (iCategory == 2 || iCategory == 100))
                    {
                        //((LinkButton)(e.Item.FindControl("lnkbtnCourt"))).CommandName = "";

                        System.Web.UI.WebControls.Image iFolderOpen = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent")));
                        System.Web.UI.WebControls.Image iFolderClose = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgChild")));

                        iFolderOpen.Attributes.Add("onclick", "ShowHideCourtDetail('" + iCategory.ToString() + "');");
                        iFolderClose.Attributes.Add("onclick", "ShowHideCourtDetail('" + iCategory.ToString() + "');");
                    }


                }
                else if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lCount = (Label)(e.Item.FindControl("lbl_Count"));
                    Label lAmount = (Label)(e.Item.FindControl("lbl_Amount"));

                    lCount.Text = TotalCourtTrans.ToString();
                    lCount.CssClass = "GrdLbl";
                    lAmount.Text = (string.Format("{0:c}", TotalCourtFee) + "&nbsp;&nbsp;&nbsp;");
                    lAmount.CssClass = "GrdLbl";

                    ViewState.Add("TotalCT", TotalCourtTrans);
                    ViewState.Add("TotalCTAmount", TotalCourtFee);
                }
            }
            catch { }

        }

        private void cmbCourt_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //cmbPayType.SelectedIndex=0;
            //cmbRep.SelectedIndex=0;
            //GetPaymentDetailReportByCourtID(calQueryDate.SelectedDate);
            Session["RepID"] = cmbRep.SelectedIndex;
            Session["PaymentID"] = cmbPayType.SelectedIndex;
            Session["CourtID"] = cmbCourt.SelectedIndex;
            GetPaymentDetailReport(calQueryDate.SelectedDate);
        }

        private void dgrdPayDetail_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            SortGrid(e.SortExpression);

        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                dv_Result = (DataView)(ClsSession.GetSessionObject("dvResult", this.Session));
                dv_Result.Sort = StrExp + " " + StrAcsDec;
                ClsSession.SetSessionVariable("dvResult", dv_Result, this.Session);
                dgrdPayDetail.DataSource = dv_Result;
                dgrdPayDetail.DataBind();
                BindPaymentDetailReport();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = ClsSession.GetSessionVariable("StrExp", this.Session);
                StrAcsDec = ClsSession.GetSessionVariable("StrAcsDec", this.Session);
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session);
                }
                else
                {
                    StrAcsDec = "ASC";
                    ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session);
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                ClsSession.SetSessionVariable("StrExp", StrExp, this.Session);
                ClsSession.SetSessionVariable("StrAcsDec", StrAcsDec, this.Session);
            }
        }

        private void dgCategoryType_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string sCategoryType = ((Label)e.Item.FindControl("lblCategoryType")).Text;
                if (((Label)(e.Item.FindControl("lblDisplayCol"))).Text == "1")
                {
                    sumCount = sumCount + Convert.ToInt64(((Label)e.Item.FindControl("lblCount2")).Text);
                    string amount = ((Label)e.Item.FindControl("lblAmount2")).Text;
                    if (sCategoryType == "Refunds")
                    {
                        amount = amount.Substring(2);
                        amount = "-" + amount.Substring(0, amount.Length - 1);
                        sumAmount = sumAmount + Convert.ToDouble(amount);
                    }
                    else
                    {
                        //Modified by Ozair 2983 on 07/08/2008 input string was not in a correct format.
                        if (amount.Contains("($"))
                        {
                            amount = amount.Substring(2);
                            amount = "-" + amount.Substring(0, amount.Length - 1);
                        }
                        else
                        {
                            amount = amount.Substring(1);
                        }
                        //end
                        sumAmount = sumAmount + Convert.ToDouble(amount);
                    }
                    if (sCategoryType != "Refunds" && sCategoryType != "Balance Paid")
                        ((Label)e.Item.FindControl("Label2")).Visible = true;

                }
                else
                {
                    ((Label)e.Item.FindControl("lblCount2")).Text = "";
                    ((Label)e.Item.FindControl("lblAmount2")).Text = "";

                }


            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                ((Label)e.Item.FindControl("lbl_SumCount")).Text = sumCount.ToString();
                ((Label)e.Item.FindControl("lbl_SumAmount")).Text = String.Format("{0:C}", sumAmount);
            }
        }

        private void dgCredit_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string sCategoryType = ((Label)e.Item.FindControl("Label5")).Text;
                CreditsumCount = CreditsumCount + Convert.ToInt64(((Label)e.Item.FindControl("Label7")).Text);
                string amount = ((Label)e.Item.FindControl("Label10")).Text;

                //Modified By Zeeshan Ahmed On 1/17/2007
                //Add Bounce Payment Modifications
                if (sCategoryType == "Refund" || sCategoryType == "Bounce Check")
                {
                    amount = amount.Substring(2);
                    amount = "-" + amount.Substring(0, amount.Length - 1);
                    CreditSumAmount = CreditSumAmount + Convert.ToDouble(amount);
                }
                else
                {
                    amount = amount.Substring(1);
                    CreditSumAmount = CreditSumAmount + Convert.ToDouble(amount);
                }

            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                ((Label)e.Item.FindControl("Label9")).Text = CreditsumCount.ToString();
                ((Label)e.Item.FindControl("Label11")).Text = String.Format("{0:C}", CreditSumAmount);
            }
        }

        protected void DG_VoidTransaction_SortCommand(object sender, DataGridSortCommandEventArgs e)
        {
            SortGridVoidTrans(e.SortExpression);
        }

        private void SortGridVoidTrans(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                dv_Result = (DataView)(ClsSession.GetSessionObject("dvVoidResult", this.Session));
                dv_Result.Sort = StrExp + " " + StrAcsDec;
                ClsSession.SetSessionVariable("dvVoidResult", dv_Result, this.Session);
                DG_voidTransactions.DataSource = dv_Result;
                DG_voidTransactions.DataBind();
                BindPaymentVoidDetail();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DG_AttorneyCredit_SortCommand(object sender, DataGridSortCommandEventArgs e)
        {
            SortGridAttorney(e.SortExpression);
        }

        private void SortGridAttorney(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                dv_Result = (DataView)(ClsSession.GetSessionObject("dvattResult", this.Session));
                dv_Result.Sort = StrExp + " " + StrAcsDec;
                ClsSession.SetSessionVariable("dvattResult", dv_Result, this.Session);
                DG_AttorneyCredit.DataSource = dv_Result;
                DG_AttorneyCredit.DataBind();
                BindAttorneyCredit();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void DG_AttorneyCredit2_SortCommand(object sender, DataGridSortCommandEventArgs e)
        {
            SortGridAttorney2(e.SortExpression);
        }

        private void SortGridAttorney2(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                dv_Result = (DataView)(ClsSession.GetSessionObject("dvattResult2", this.Session));
                dv_Result.Sort = StrExp + " " + StrAcsDec;
                ClsSession.SetSessionVariable("dvattResult2", dv_Result, this.Session);
                DG_AttorneyCredit2.DataSource = dv_Result;
                DG_AttorneyCredit2.DataBind();
                BindAttorneyCredit2();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Added By Agha Usman Task Id 3210 On 02/21/2008

        private void ValidateRequest()
        {
            clsSession objSession = new clsSession();
            int accessType = int.Parse(objSession.GetCookie("sAccessType", Request).ToString());
            if (accessType == 1) // if user type is secondary
            {
                string empId = objSession.GetCookie("sEmpID", Request);
                bool hasMatched = false;

                foreach (string str in ConfigurationManager.AppSettings["secpoweruser"].Split(','))
                {
                    if (string.Compare(str, empId, true) == 0)
                    {
                        hasMatched = true;
                    }
                }

                if (hasMatched == false)
                {
                    Response.Redirect("../LoginAccesserror.aspx", false);
                    Response.End();
                }
                //else
                //{
                //    isPrimary = true;
                //}
            }
            //Ozair 5057 03/24/2009 Warnings Removed
        }
        private void FillFirms(DropDownList ddl_FirmAbbreviation)
        {
            try
            {
                clsFirms ClsFirms = new clsFirms();
                DataSet ds_Firms = ClsFirms.GetActiveFirms(1);
                //khalid 3309  3/6/08 to fix binding bug
                if ((ds_Firms.Tables.Count > 0) && (ds_Firms.Tables[0].Rows.Count > 0))
                {
                    ddl_FirmAbbreviation.DataSource = ds_Firms.Tables[0];
                    ddl_FirmAbbreviation.DataTextField = "FirmAbbreviation";
                    ddl_FirmAbbreviation.DataValueField = "FirmID";
                    ddl_FirmAbbreviation.DataBind();

                    //ozair 5384 01/09/2009 removed buggy code and included new checks
                    if (!Request.Url.ToString().Contains("161"))
                    {
                        ddl_FirmAbbreviation.Items.Insert(0, new ListItem("-ALL Firms-", "0"));
                    }
                    else
                    {
                        ddl_FirmAbbreviation.Items.Remove(new ListItem("SULL", "3000"));
                        ddl_FirmAbbreviation.Items.Insert(0, new ListItem("-Choose Firms-", "-1"));
                    }
                    ddl_FirmAbbreviation.SelectedIndex = 0;
                    //end ozair 5384
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                //ozair 5384 01/09/2009 error logged in bug tracker
                clsLogger.ErrorLog(ex);
            }
        }
        void lnkBack_Click(object sender, EventArgs e)
        {
            Table5.Visible = true;
            ActiveMenu1.Visible = true;
            tblPrintHeader.Visible = false;
            tblMain.Align = "center";
        }

        protected void dgMailerSummary_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            // tahir 4171 06/28/2008
            // to provide expand/collapse functionality for each court category.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                bool IsGroup = Convert.ToBoolean(((Label)(e.Item.FindControl("lbl_IsGroup"))).Text);

                HtmlTable tblSummary = (HtmlTable)(e.Item.FindControl("tblSummary"));
                HtmlTableCell tdCategory = (HtmlTableCell)(e.Item.FindControl("tdCategory"));
                HtmlTableCell tdMailer = (HtmlTableCell)(e.Item.FindControl("tdMailer"));

                System.Web.UI.WebControls.Image imgFolderOpen = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgParent")));
                System.Web.UI.WebControls.Image imgFolderClose = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgChild")));
                System.Web.UI.WebControls.Image imgLink = ((System.Web.UI.WebControls.Image)(e.Item.FindControl("imgLink")));

                System.Web.UI.WebControls.TextBox txtToggle = ((System.Web.UI.WebControls.TextBox)(e.Item.FindControl("txtToggleImage")));

                // if current row is the court category header row........
                // hide the mailer type and add javascript function call on expand/collapse image
                if (IsGroup)
                {
                    tdCategory.Style["Display"] = "block";
                    tdMailer.Style["Display"] = "none";
                    imgLink.Style["display"] = "none";
                    tdCategory.Width = "60%";
                    tdMailer.Width = "0%";

                    ((Label)(e.Item.FindControl("lblTotalClients"))).Font.Bold = true;
                    ((Label)(e.Item.FindControl("lblTotalRev"))).Font.Bold = true;

                    imgFolderOpen.Attributes.Add("onclick", "ShowHideMailerDetail('" + txtToggle.ClientID + "');");
                    imgFolderClose.Attributes.Add("onclick", "ShowHideMailerDetail('" + txtToggle.ClientID + "');");
                }

                // if current row is the detail of the parent court category ........
                // hide the court category name & expand/collapse iamge. 
                else
                {
                    tdMailer.Style["Display"] = "block";
                    tdCategory.Style["Display"] = "none";
                    imgLink.Style["display"] = "block";
                    tdMailer.Width = "60%";
                    tdCategory.Width = "0%";

                    ((Label)(e.Item.FindControl("lblTotalClients"))).Font.Bold = false;
                    ((Label)(e.Item.FindControl("lblTotalRev"))).Font.Bold = false;
                }

            }
        }
        /// <summary>
        /// Method to get branches name with branches Ids...
        /// </summary>
        private void GetBranchesName()
        {
            DataTable dtBranches = objEnationFramework.Get_DT_BySPArr("USP_HTP_Get_BranchesName");
            if (dtBranches.Rows.Count > 0)
            {
                ddl_Branch.Items.Clear();
                foreach (DataRow dr in dtBranches.Rows)
                {
                    ListItem item = new ListItem(dr["BranchName"].ToString(), dr["Branchid"].ToString());
                    ddl_Branch.Items.Add(item);
                }

            }
        }

    }
}

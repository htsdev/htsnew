<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.ClientInfo.frmCaseSummary" Codebehind="frmCaseSummary.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Case Summary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <SCRIPT src="../Scripts/boxover.js" type="text/javascript"></SCRIPT>        
       
        
        <script type="text/jscript">
            function Validate()
            {          
              if((document.getElementById("chk_contact").checked==false) && (document.getElementById("chk_matter").checked==false) && 
              (document.getElementById("chk_flag").checked==false) && (document.getElementById("chk_comments").checked==false) && 
              (document.getElementById("chk_history").checked==false) && (document.getElementById("chk_billing").checked==false)) 
              {
                alert("Please check mark on any section of this report.");
                return false;
              }
              else
              {
                return true;
              }             
                
            }
        </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		     <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780" align="center"
                 border="0">
                <tr>
                    <td  height= "14px">
                        <table id="Table1" style="font-weight: bold; font-size: 11px; width: 100%; color: white;
                            font-family: aRIAL, Sans-Serif; height: 30px" cellspacing="0" cellpadding="1"
                            align="left" border="0">
                            <tr>
                                <td  valign="top">
                                    <asp:ImageButton ID="img_pdf" runat="server" ImageUrl="../Images/pdf.jpg" OnClick="img_pdf_Click"
                                        ToolTip="Print Single" /></td>
                                <td align="center" >
                                    <p align="center">
                                        <font color="#0000FF" size="6" face="Arial">                                
                                            <asp:LinkButton ID="lblkbtn_name" runat="server" Font-Size="22pt" OnClick="lblkbtn_name_Click"></asp:LinkButton></font></p>
                                   </td>
                            </tr>
                            <tr>
                                <td style="height:30px">                                
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    <font face="Arial">
                                        <asp:CheckBox ID="chk_contact" runat="server" Checked="True" CssClass="clslabelnew" /></font></td>
                                <td bgcolor="#D1D1D1" style="font-weight: bold; ">
                                    <font face="Arial" size="4" color="#000000">CONTACT</font></td>
                            </tr>
                            <tr style="font-weight: bold">
                        <td  valign="top">
                            &nbsp;</td>
                        <td >
                            <table border="0" width="100%" style="border-collapse: collapse">
                                <tr>
                                            
					<td>
					    <table border="1" width="100%"  cellpadding="0" cellspacing="0" style="border-collapse:collapse">
						    <tr>
							    <td ><b><font face="Arial" size="2">
							    ADDRESS</font></b></td>
							    <td ><font face="Arial"><b><font size="2">
							    DOB</font></b></td>
							    <td ><b><font size="2">DL</font></b></td>
							    <td ><font size="2"><b>X-REF</b></font></td>
							    <td><b><font size="2">LANG</font></b></td>
						    </tr>
						    <tr>
							    <td ><font face="Arial">
							    <span class="label" id="lblAd"><font size="2">
                                    <asp:Label ID="lbladd" runat="server"></asp:Label></font></span></td>
							    <td ><font size="2">
                                    <asp:Label ID="labeldob" runat="server"></asp:Label></font></td>
							    <td ><font size="2">
                                    <asp:Label ID="labeldl1" runat="server"></asp:Label></font></td>
							    <td  ><font size="2">
                                    <asp:Label ID="lbl_midnumber" runat="server"></asp:Label></font></td>
							    <td ><font size="2">
                                    <asp:Label ID="lbllang1" runat="server"></asp:Label></font></td>
						    </tr>
					    </table>
                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                           
                            <tr>
                                <td valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                    <hr size="1">
                                    <table border="1" width="100%"  style="border-collapse: collapse">
                                        <tr>
                                            <td>
                                                <b><font size="2">TEL 1</font></b></td>
                                            <td>
                                                <b><font size="2">TEL 2</font></b></td>
                                            <td>
                                                <font face="Arial"><b><font size="2">TEL 3</font></b></font></td>
                                            <td>
                                                <b><font face="Arial" size="2">E-MAIL</font></b></td>
                                        </tr>
                                        <tr style="font-size: 12pt">
                                            <td>
                                                <asp:Label ID="lbltel1" runat="server"></asp:Label></td>
                                            <td>
                                                <font face="Arial" size="2">
                                                    <asp:Label ID="lbltel2" runat="server"></asp:Label></font></td>
                                            <td>
                                                <font face="Arial" size="2">
                                                    <asp:Label ID="lbltel3" runat="server"></asp:Label></font></td>
                                            <td>
                                                <font face="Arial" size="2">
                                                    <asp:Label ID="lblemail" runat="server"></asp:Label></font></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    <font face="Arial">
                                        <asp:CheckBox ID="chk_matter" runat="server" Checked="True" CssClass="clslabelnew" /></font></td>
                                <td valign="top" bgcolor="#D1D1D1" style="font-weight: bold; ">
                                    <font face="Arial" size="4" color="#000000">MATTER</font></td>
                            </tr>
                            <tr style="font-weight: bold">
                                <td  valign="top">
                                    <p>
                                    &nbsp;</td>
                                <td valign="top">
                                    <table border="1" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                       <tr>
                                        <td>
                                            <asp:GridView ID="GV_matter" runat="server"  AutoGenerateColumns="False" BorderWidth="1" Width="100%" OnRowDataBound="GV_matter_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField  HeaderText="CAUSE NO" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="X-Small" HeaderStyle-Font-Bold="true">                                                       
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcnum" runat="server" Text='<%#Bind("causeNum")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="TICKET NO" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="X-Small" HeaderStyle-Font-Bold="true">                                                       
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrnum" runat="server" Text='<%#Bind("RefNum")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VIOLATION DESCRIPTION" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="X-Small" HeaderStyle-Font-Bold="true">                                                       
                                                        
                                                        <ItemTemplate>
                                                            <DIV runat="server" id="div_status">
                                                            <asp:Label ID="lbl_vdescription" runat="server" Text='<%#Bind("ViolationDescription")%>'></asp:Label>
                                                            </DIV> 
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="X-Small" HeaderStyle-Font-Bold="true">                                                       
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsd" runat="server" Text='<%#Bind("ShortDescription")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SETTING INFO" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="X-Small" HeaderStyle-Font-Bold="true">                                                       
                                                        
                                                        <ItemTemplate>                                                            
                                                            <asp:Label ID="lblcd" runat="server" Text='<%# Eval("CourtDateMain")+ " #" + Eval("CourtNumber") %>'></asp:Label>                                                            
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CRT" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="X-Small" HeaderStyle-Font-Bold="true">                                                       
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsn" runat="server" Text='<%#Bind("ShortName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                       </tr>                                        
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    <font face="Arial">
                                        <asp:CheckBox ID="chk_flag" runat="server" Checked="True" CssClass="clslabelnew" /></font></td>
                                <td valign="top" bgcolor="#D1D1D1" style="font-weight: bold; ">
                                    <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <font face="Arial" size="4" color="#000000"><strong>FLAGS</strong></font>
                                            </td>
                                            <td runat="server" id="td_lblofficername" align="right">
                                                <strong>  Officer Name :</strong>&nbsp;
                                                <asp:Label ID="lblofficername" runat="server" Font-Bold="True"></asp:Label>
                                                </td>                                                                                            
                                        </tr>
                                    </table>
                                    
                                        
                                </td>
                            </tr>
                            <tr style="font-weight: bold">
                                <td  valign="top">
                                  </td>
                                <td valign="top" >
                                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" runat="server" id="tdbonds">
                                              <asp:DataList ID="DL_Flag" runat="server" RepeatColumns="3" Width="100%">
                                                <ItemTemplate>
                                                <table id="Table13" border="0"; cellpadding="0"  cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" align="left" >
                                                            <asp:Image ID="Image1" ImageUrl="~/Images/new su2.gif" runat="server" />
                                                            <asp:Label ID="lblDate" runat="server" CssClass="label" Font-Size="16px" Font-Underline="False" Text='<%# DataBinder.Eval(Container, "DataItem.Flags") %>'>                                                                
											            </asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                               </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    <font face="Arial">
                                        <asp:CheckBox ID="chk_comments" runat="server" Checked="True" CssClass="clslabelnew" /></font></td>
                                <td valign="top" bgcolor="#D1D1D1" style="font-weight: bold; ">
                                    <font color="#000000" size="4" face="Arial">COMMENTS</font></td>
                            </tr>
                            <tr style="font-weight: bold">                             
                                <td  valign="top">                                
                                </td>                                
                                <td valign="top" >
                                  <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                                   <tr runat="server" id="tr_genral">
                                    <td>
                                        <font face="Arial" size="2" color="#000000"><u><strong> Genral Comments</strong></u>
                                        <span style="font-weight: 400"></span></font>                                        
                                    </td>                                   
                                   </tr>
                                   <tr runat="server" id="tr_lblGenral">
                                    <td>
                                        <asp:Label ID="lblGeneral" runat="server" BackColor="white" CssClass="label"
                                             Width="100%"></asp:Label>
                                    </td>
                                   </tr>
                                              <tr style="height:10px"><td></td></tr>
                                   <tr runat="server" id="tr_contactnotes">
                                    <td>                                    
                                         <p>  <u><font face="Arial" size="2" color="#000000"><strong> Contact Notes </strong></font></u></p>
                                    </td>
                                   </tr>
                                   <tr runat="server" id="tr_lblContact">
                                    <td>
                                        <asp:Label ID="lblContact" runat="server" BackColor="white" CssClass="label"
                                             Width="100%"></asp:Label>
                                     </td>
                                     
                                   </tr>
                                              <tr style="height:10px"><td></td></tr>
                                   <tr runat="server" id="tr_trialcomments">
                                    <td>                                        
                                        <p>
                                        <u><font face="Arial" size="2" color="#000000"><strong> Trial Comments</strong> </font></u></p>
                                    </td>
                                   </tr>
                                   <tr runat="server" id="tr_lbltrial">
                                    <td>
                                        <asp:Label ID="lblTrial" runat="server" BackColor="white" CssClass="label" 
                                            Width="100%"></asp:Label>
                                    </td>
                                    
                                   </tr>
                                             <tr style="height:10px"><td></td></tr>
                                   <tr runat="server" id="tr_settingcomments">
                                    <td>                                    
                                    <p>
                                        <u><font face="Arial" size="2" color="#000000"><strong> Setting Comments</strong></font></u></p>                                   
                                    </td>
                                   </tr>
                                   <tr runat="server" id="tr_lblSetting">
                                    <td>
                                        <asp:Label ID="lblSetting" runat="server" BackColor="white" CssClass="label"
                                             Width="100%"></asp:Label>
                                    </td>                                     
                                   </tr>
                                              <tr style="height:10px"><td></td></tr>
                                   </table>  
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    <font face="Arial">
                                        <asp:CheckBox ID="chk_history" runat="server" Checked="True" CssClass="clslabelnew" /></font></td>
                                <td valign="top" >
                                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td  bgcolor="#D1D1D1">
                                                <b><font color="#000000" face="Arial" size="4">HISTORY</font></b></td>
                                            <td bgcolor="#D1D1D1">
                                                <p align="right">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                  <table border="0" width="100%" style="border-collapse: collapse">
                                    <tr>
                                        <td>
                                                                                
                                    <table border="0" width="100%" style="border-collapse: collapse">
                                        <tr>
                                            <td >
                                                <font face="Arial"><b><font size="2">&nbsp;Date</font></b></td>
                                            <td >
                                                <b><font size="2">Time</font></b></td>
                                            <td>
                                                <b><font size="2">Note</font></b></td>
                                        </tr>
                                    </table>
                                     </td>
                                    </tr>
                                    <tr>
                                        <td align="left">                                        
                                    <asp:DataList ID="DLNote" runat="server" BackColor="white" CellPadding="0" CellSpacing="0"
                                        RepeatColumns="1" Width="100%">
                                        <ItemTemplate>
                                            <table id="Table13" border="0"; cellpadding="0" cellspacing="8" width="100%">
                                                <tr>
                                                    <td valign="top" align="center" >
                                                        <asp:Label ID="lblDate" runat="server" CssClass="label" Font-Underline="False" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate","{0:MM/dd/yyyy}") %>'>
											        </asp:Label>
                                                    </td>
                                                 
                                                    <td valign="top" align="center" >
                                                        <asp:Label ID="lblTime" runat="server" CssClass="label" Font-Underline="False" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate","{0:t}") %>'>
											        </asp:Label>
                                                    </td>
                                                    
                                                    <td >
                                                        <asp:Label ID="lblsubject" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Subject") %>'>
											        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                   </td>
                                    </tr>
                                  </table>
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td  valign="top">
                                    <font face="Arial">
                                        <asp:CheckBox ID="chk_billing" runat="server" Checked="True" CssClass="clslabelnew" /></font></td>
                                <td valign="top" bgcolor="#D1D1D1" style="font-weight: bold; ">
                                    <font size="4" color="#000000"><span style="font-family: Arial">BILLING</span></font></td>
                            </tr>
                            <tr style="font-weight: bold">
                                <td  valign="top">
                                    &nbsp;</td>
                                <td valign="top" >
                                    <table id="tblPayments" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                        <tr>
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font face="Arial"><font size="2">&nbsp; </font><span class="label" id="lblFee"><font
                                                    size="2">Initial Fee</font></span></td>
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblCon"><font size="2">Continuance</font></span></td>
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblRep"><font size="2">Re-Rep</font></span></td>
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="Span1"><font size="2">Adj1</font></span></td>
                                                
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblAdj"><font size="2">Adj2</font></span></td>
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblTotFee"><font size="2">Total
                                                    Fee</font></span></td>
                                            <td style="height: 18px"  valign="top" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblPay"><font size="2">Paid</font></span></td>
                                            <td style="height: 18px" valign="top" align="left" >
                                                <font size="2">&nbsp; </font><span class="label" id="lblOwe"><font size="2">Owes</font></span></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <font face="Arial"/><font size="2">&nbsp; </font><span class="label" id="lblInitialFee">
                                                    <font size="2"><asp:Label ID="lblInitialFee" runat="server" CssClass="label"></asp:Label></font></span></td>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblContinuance"><font size="2">
                                                    <asp:Label ID="lblContinuance" runat="server" CssClass="label"></asp:Label></font></span></td>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblReRep"><font size="2"><asp:Label
                                                    ID="lblReRep" runat="server" CssClass="label"></asp:Label></font></span></td>
                                            <td bgcolor="whitesmoke" style="height: 17px" align="left">
                                                &nbsp;
                                                <asp:Label ID="lbladj1" runat="server"></asp:Label></td>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblAdjustment"><font size="2"><asp:Label
                                                    ID="lbladjustment1" runat="server" CssClass="label"></asp:Label></font></span></td>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <span class="label" id="lblTotalFee"><font size="2">&nbsp;<asp:Label
                                                    ID="lbltotalfee1" runat="server" CssClass="label"></asp:Label></font></span></td>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <font size="2">&nbsp; </font><span class="label" id="lblPaid"><font size="2"><asp:Label
                                                    ID="lblpaidamount1" runat="server" CssClass="label"></asp:Label></font></span></td>
                                            <td style="height: 17px" bgcolor="whitesmoke" align="left">
                                                <font size="2">&nbsp;</font><span class="label" id="lblwes" style="font-weight: bold"><font size="2"><asp:Label
                                                    ID="lblowes1" runat="server" CssClass="label" Font-Bold="True"></asp:Label></font></span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
		</form>
	</body>
</HTML>

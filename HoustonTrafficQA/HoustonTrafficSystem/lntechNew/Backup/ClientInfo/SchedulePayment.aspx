<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ClientInfo.SchedulePayment"
    CodeBehind="SchedulePayment.aspx.cs" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>Schedule Payment</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
	    
	   function ValidateForm()
	   {
			var active=0;
			active=document.getElementById("lbl_activeflag").innerText;		
			
			if(active=='0')
			{
			    doyou = confirm("Are you sure you want to schedule a payment for a client that has no initial down payment. Please click OK for Yes and Cancel for No"); 
				if (doyou == true)
				{				
					if (check()==false)
					{
						return false;
					}
					else if(document.getElementById("txt_amount").value!=(document.getElementById("lbl_owes").innerText)*1)
					{
						alert('Scheduled amount is less than owed amount, Please schedule the full owed amount');
					}
				}				
				else
				{				
				window.close();
				return false;   
				}
			}
			else
			if(check() == false)
			return false;	
			
			// Noufil 5802 05/21/2009 Check Date for Business Days
			if (CheckScheduledate() == false)
			    return false;				
	 }		 
	 
	 function check()
	 {
		var intamount=0;
	    intamount=document.getElementById("txt_amount").value;
	        
	    if (isNaN(intamount) == false && intamount!="" && intamount>0)
		{			
	      if ((document.getElementById("lbl_owes").innerText)*1 < intamount)
	      {
			alert ("Incorrect payment amount. A client cannot pay more than he owes");
		    document.getElementById("txt_amount").focus(); 
			return false;				
		  }	      
	    } 
	    else
			{
				alert ("Incorrect payment amount.");
				document.getElementById("txt_amount").focus(); 
				return false;							
			}
			
			// Noufil 5802 05/21/2009 code comment
			
//			var date =document.getElementById("date_payment").value;		
//			if (date=="")
//			{
//				alert ("Please specify payment date");			
//				return false;
//			}	
	 } 
	 
	 // Noufil 5802 05/21/2009 Check Date for Business Days
	 function CheckScheduledate()
	 {	 
	    var scheduledate = document.getElementById("date_payment").value;
	    
	    if (scheduledate == "")
		{
			alert ("Please specify payment date");			
			return false;
		}	
	    
	    var weekday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
        var newseldatestr = formatDate((Date.parseInvariant(scheduledate).getMonth()+1)+"/"+Date.parseInvariant(scheduledate).getDate() + "/"+Date.parseInvariant(scheduledate).getFullYear(),"MM/dd/yyyy");                
        newseldate = Date.parseInvariant(newseldatestr,"MM/dd/yyyy");	  
	    if ((weekday[newseldate.getDay()] == "Sunday"))
	    {
	        alert("Please select any business day.");
	        return false;
	    }
	 }
	 
	
	 
	   
	    
    </script>

</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" cellspacing="0" cellpadding="0" width="350" align="center"
        border="0">
        <tr>
            <td background="../../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <table class="clsLeftPaddingTable" id="tblsub" height="20" cellspacing="0" cellpadding="0"
                    width="350" align="center" border="1" bordercolor="#ffffff">
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            <strong>Schedule Payment</strong>
                        </td>
                        <td valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                        </td>
                        <td valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            Schedule Upto
                        </td>
                        <td valign="top">
                            $
                            <asp:Label ID="lbl_owes" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            Schedule Amount($)
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="txt_amount" runat="server" CssClass="clsinputadministration" Width="48px"
                                MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            Payment&nbsp;Date
                        </td>
                        <td valign="top">
                            <ew:CalendarPopup ID="date_payment" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="-80" ToolTip="Payment Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
                                ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Nullable="True">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="right">
                            <asp:Button ID="btn_submit" runat="server" CssClass="clsbutton" Text="Save"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
            <tr>
                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                </td>
            </tr>
            <tr>
                <td style="visibility: hidden">
                    <asp:Label ID="lbl_activeflag" runat="server"></asp:Label>
                </td>
            </tr>
    </table>
    </form>
</body>
</html>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.WebControls;

//Yasir Kamal 6029 06/17/2009 namespace changed.
namespace HTP.ClientInfo
{
    /// <summary>
    /// Summary description for CaseHistory.
    /// </summary>
    public partial class CaseHistory : HTP.WebComponents.MatterBasePage
    {
        protected System.Web.UI.WebControls.DataGrid dg_caseHistory;
        protected System.Web.UI.WebControls.Button btnInsertNote;
        clsLogger cLog = new clsLogger();
        clsSession cSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsCase ClsCase = new clsCase();
        DataView dvNOS;
        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;

        int EmpID;
        int accesstype;

        //string Subject;
        protected System.Web.UI.WebControls.TextBox txtSubject;
        protected System.Web.UI.WebControls.Label lblmsg;
        protected System.Web.UI.WebControls.Label lbl_LastName;
        protected System.Web.UI.WebControls.Label lbl_FirstName;
        protected System.Web.UI.WebControls.Label lbl_CaseCount;
        protected System.Web.UI.WebControls.HyperLink hlnk_MidNo;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Aziz 3203 02/20/08 Session related code is in BasePage.cs                
                btnInsertNote.Attributes.Add("onclick", "javascript: return DoValidate();");
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count >= 2)
                    {
                        ViewState["vSearch"] = Request.QueryString["search"];
                    }
                    else
                    {
                        Response.Redirect("../frmMain.aspx", false);
                        goto SearchPage;
                    }

                    //Aziz 3203 02/20/08 TicketId related code has been moved to MatterBasePage.cs
                    ViewState["vEmpID"] = cSession.GetCookie("sEmpID", this.Request);
                    EmpID = Convert.ToInt32(ViewState["vEmpID"]);

                    // khalid to get access type
                    ViewState["vAccessType"] = cSession.GetCookie("sAccessType", this.Request);
                    accesstype = Convert.ToInt32(ViewState["vAccessType"]);
                    //for displaying case status for primary user access type=2
                    if (accesstype == 2)
                    {
                        forprimaryuser.Style[HtmlTextWriterStyle.Display] = "block";

                    }
                    

                    FillGrid();

                    // Noufil  3479 03/25/2008 
                    // Getting AutoHistory record in grid
                    DataTable dt = ClsCase.GetAutoHistory(this.TicketId);
                    dg_autohistory.DataSource = dt;
                    dg_autohistory.DataBind();


                }

                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");
                txt1.Text = this.TicketId.ToString();

                TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                txt2.Text = ViewState["vSearch"].ToString();




            SearchPage:
                { }
            }

            catch (Exception ex)
            {
                //Aziz 3203 02/20/08 Static method with one argument for simple logging
                clsLogger.ErrorLog(ex);
                lblmsg.Text = ex.Message;
            }

        }

        // Fill datagrid
        private void FillGrid()
        {
            try
            {
                //// Status History
                //Aziz 3203 02/20/08 Return type changed to DataTable
                DataTable dt_statushistory = cLog.GetStatusHistory(this.TicketId);
                if (dt_statushistory != null)
                {
                    dg_statushistory.DataSource = dt_statushistory;
                    dg_statushistory.DataBind();
                }

                DataSet ds_History = cLog.GetNotes(this.TicketId);

                if (ds_History == null)
                    return;
                // checked by khalid for bug 2366 2-1-08
                if (ds_History.Tables[0].Rows.Count > 0)
                {
                    dg_caseHistory.DataSource = ds_History.Tables[0];
                    dg_caseHistory.DataBind();
                }

                if (ds_History.Tables[2].Rows.Count > 0)
                {
                    dg_SystemHistory.DataSource = ds_History.Tables[2];
                    dg_SystemHistory.DataBind();
                }
                if (ds_History.Tables[1].Rows.Count > 0)
                {
                    lbl_FirstName.Text = ds_History.Tables[1].Rows[0]["Firstname"].ToString();
                    lbl_LastName.Text = ds_History.Tables[1].Rows[0]["Lastname"].ToString();
                    lbl_CaseCount.Text = ds_History.Tables[1].Rows[0]["CaseCount"].ToString();
                    hlnk_MidNo.Text = ds_History.Tables[1].Rows[0]["Midnum"].ToString().Trim();
                    if (hlnk_MidNo.Text != "")
                    {
                        //In order to redirect to frmMain when midnumber is clicked

                        //string casetype=cSession.GetCookie("sSearch",this.Request);
                        string casetype = Request.QueryString["search"].ToString();
                        //Aziz 3203 02/20/08 no need of switch case
                        hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=" + casetype + "&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                    }
                }

                //Sabir Khan 5963 06/02/2009 Get Tickler history...
                DataTable dt_TicklerHistory = cLog.GetTicklerEventHistory(this.TicketId);
                if (dt_TicklerHistory != null)
                {
                    gv_tickler.DataSource = dt_TicklerHistory;
                    gv_tickler.DataBind();
                }
                //Yasir Kamal 6086 07/29/2009 allow sorting 
                if (dt_TicklerHistory.Rows.Count > 0)
                {
                    dvNOS = new DataView(dt_TicklerHistory);
                    Session["dvNOS"] = dvNOS;
                    this.lblmsg.Text = "";
                }

                //Noufil 5884 07/02/2009 Get SMS HISTORY
                DataTable dt_SMSHistory = cLog.GetSMSHistory(this.TicketId, 1);
                if (dt_SMSHistory != null)
                {
                    GV_SMSHistory.DataSource = dt_SMSHistory;
                    GV_SMSHistory.DataBind();
                }

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblmsg.Text = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInsertNote.Click += new System.EventHandler(this.btnInsertNote_Click);
            this.dg_caseHistory.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_caseHistory_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion


        // Insert notes
        private void btnInsertNote_Click(object sender, System.EventArgs e)
        {
            try
            {
                //Aziz 3203 02/20/08 TicketId related code has been moved to MatterBasePage.cs
                cLog.AddNote(Convert.ToInt32(ViewState["vEmpID"]), txtSubject.Text.Trim(), "", this.TicketId);
                FillGrid();
                txtSubject.Text = " ";

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblmsg.Text = ex.Message;
            }
        }

        //Added By Ozair to change the row color on mouse over
        private void dg_caseHistory_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");

                //Nasir 6483 10/07/2009 disable hyperlink when secondary user
                if (accesstype.ToString() != "2")
                {
                    string str = e.Item.Cells[2].Text;

                    if (str.Contains("BOND Printed Now"))
                    {
                        e.Item.Cells[2].Text = str.Substring(str.IndexOf('>') + 1, str.IndexOf('<', str.IndexOf('>') + 1) - str.IndexOf('>') - 1);
                    }
                    else if (str.Contains("ESignature - Bond Docs Saved"))
                    {
                        e.Item.Cells[2].Text = str.Substring(str.IndexOf('>') + 1, str.IndexOf('<', str.IndexOf('>') + 1) - str.IndexOf('>') - 1);
                    }                                        
                }
                
            }


            //Aziz 3203 02/20/08 Same color on both if and else so check removed
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");


        }

        protected void dg_SystemHistory_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
            }

            //Aziz 3203 02/20/08 Same color on both if and else so check removed
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");


        }


        //Yasir Kamal 6086 07/29/2009 allow sorting 
        private void SetAcsDesc(string Val)
        {
            try
            {
                if (Session["StrExp"] != null && Session["StrAcsDec"] != null)
                {
                    StrExp = Session["StrExp"].ToString();
                    StrAcsDec = Session["StrAcsDec"].ToString();
                }

                if (StrExp == Val)
                {
                    if (StrAcsDec == "ASC")
                    {
                        StrAcsDec = "DESC";
                        Session["StrAcsDec"] = StrAcsDec;
                    }
                    else
                    {
                        StrAcsDec = "ASC";
                        Session["StrAcsDec"] = StrAcsDec;
                    }
                }
                else
                {
                    StrExp = Val;
                    StrAcsDec = "ASC";
                    Session["StrExp"] = StrExp;
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                DataTable dtt;
                SetAcsDesc(SortExp);
                if (Session["dvNOS"] != null)
                {
                    dvNOS = (DataView)Session["dvNOS"];
                    dvNOS.Sort = StrExp + " " + StrAcsDec;
                    dtt = dvNOS.ToTable();

                    gv_tickler.DataSource = dtt;
                    gv_tickler.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_tickler_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
    }
}

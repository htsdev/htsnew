using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.ClientInfo
{
	/// <summary>
	/// Summary description for CalculationSummary.
	/// </summary>
	public partial class CalculationSummary : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgResult;
		protected System.Web.UI.WebControls.Label lblMessage;
		clsSession ClsSession=new clsSession();
		protected System.Web.UI.WebControls.Label lblGrandTotal;
		clsCase ClsCase=new clsCase();
		DataSet dsResult;
		double dSpeeding=0;
		double dAccident=0;
		double dCDL=0;
		double dRoundOff=0;
		double dContinuance= 0;
		double dInitialAdj = 0;
		double dFinalAdj = 0;
		double AddTotal=0;
        double dLateFee = 0;
        double dPlanFee = 0;
        double dayOfArrFee = 0; // Rab Nawaz Khan 10330 07/03/2012 Amount for the Day Of Arr has been added. . . 
		clsLogger bugTracker = new clsLogger();
		protected System.Web.UI.HtmlControls.HtmlTable tblAdditional;
		protected System.Web.UI.WebControls.Label lblAddTotal;
		protected System.Web.UI.WebControls.Label lblSpeeding;
		protected System.Web.UI.WebControls.Label lblAccident;
		protected System.Web.UI.WebControls.Label lbl_Total;
		protected System.Web.UI.WebControls.Label lblRound;
		protected System.Web.UI.WebControls.Label lblContinuance;
		protected System.Web.UI.WebControls.Label lblInitialAdj;
		protected System.Web.UI.WebControls.Label lblFinalAdj;
		protected System.Web.UI.WebControls.Label lblCDL;
        //protected System.Web.UI.WebControls.Label lbllatefee;
		
		
        
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Check for valid Session if not then redirect to login page
            if (ClsSession.IsValidSession(this.Request)==false)
			{
				//Response.Redirect("frmlogin.aspx");
				HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
			}
			if(!IsPostBack)
			{
				try
				{
					//dsResult = ClsCase.ShowFeeCalculationStructure(Convert.ToInt32(ClsSession.GetCookie("sTicketID",this.Request)));
					dsResult = ClsCase.ShowFeeCalculationStructure(Convert.ToInt32(Request.QueryString["casenumber"]));
					//dsResult = ClsCase.ShowFeeCalculationStructure(8879);
					SeparateAdditionalCharges(dsResult);
					if (dsResult.Tables[0].Rows.Count == 0)
					{
						lbl_Total.Visible=false;
                        //Code Commented by Fahad we didnt require to log custom messages. now the label will just display & didnt log (Fahad - 26th-jan-2008 )
						//throw new Exception("No Record found. Please first calculate the price.");
                        
                        lblMessage.Text = "Please First Calculate The Price";
                        dgResult.Visible = false;
                        lblGrandTotal.Visible = false;
                        // End Fahad
					}
                    dgResult.DataSource = dsResult;
                    dgResult.DataBind();
                    CalculateTotal();
					
				}
				catch(Exception ex)
				{
					lblMessage.Text = ex.Message ;

					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				}
			}
		}

		private void SeparateAdditionalCharges( DataSet ds)
		{	int idx=0;
			string RowsToRemove=String.Empty;
			foreach(DataRow dr in ds.Tables[0].Rows)
			{	
				if (dr["RefCaseNumber"] == System.DBNull.Value)
				{
					if (dr["CalculationDescription"].ToString().ToUpper() == "SPEEDING")
						dSpeeding = Convert.ToDouble(dr["Amount"]);
					else if (dr["CalculationDescription"].ToString().ToUpper() == "ACCIDENT")
						dAccident = Convert.ToDouble(dr["Amount"]);
					else if (dr["CalculationDescription"].ToString().ToUpper() == "CDL")
						dCDL = Convert.ToDouble(dr["Amount"]);
					else if (dr["CalculationDescription"].ToString().ToUpper() == "ROUND OFF")
						dRoundOff = Convert.ToDouble(dr["Amount"]);
					else if (dr["CalculationDescription"].ToString().ToUpper() == "CONTINUANCE")
						dContinuance = Convert.ToDouble(dr["Amount"]);
					else if (dr["CalculationDescription"].ToString().ToUpper() == "INITIALADJ")
						dInitialAdj = Convert.ToDouble(dr["Amount"]);
					else if (dr["CalculationDescription"].ToString().ToUpper() == "FINALADJ")
						dFinalAdj = Convert.ToDouble(dr["Amount"]);
                    else if (dr["CalculationDescription"].ToString().ToUpper() == "LATE FEE")
                        dLateFee = Convert.ToDouble(dr["Amount"]);
                    else if (dr["CalculationDescription"].ToString().ToUpper() == "PAYMENT PLAN CHARGES")
                        dPlanFee  = Convert.ToDouble(dr["Amount"]);
                    // Rab Nawaz Khan 10330 07/03/2012 Calculate the Amount for the Day Of Arr if Description Found in Dataset. . . 
                    else if (dr["CalculationDescription"].ToString().ToUpper() == "DAY OF ARRAIGNMENT")
                        dayOfArrFee = Convert.ToDouble(dr["Amount"]);



					RowsToRemove= RowsToRemove+ idx.ToString() +",";
				}
				idx++;
			}

			if (RowsToRemove.Length > 0 )
			{
				RowsToRemove = RowsToRemove.Substring(0,RowsToRemove.Length -1);

				string [] arrRows = RowsToRemove.Split(',');
				for (idx =0 ; idx < arrRows.Length ; idx++)
					dsResult.Tables[0].Rows[Convert.ToInt16(arrRows[idx])].Delete();

				dsResult.AcceptChanges();

				if (dSpeeding != 0 )
					lblSpeeding.Text = dSpeeding.ToString("$#,##0;($#,##0)");
				
				if (dAccident !=  0 )
					lblAccident.Text = dAccident.ToString("$#,##0;($#,##0)");

				if (dCDL != 0 )
					lblCDL.Text = dCDL.ToString("$#,##0;($#,##0)");

				if (dRoundOff != 0 )
					lblRound.Text = dRoundOff.ToString("$#,##0;($#,##0)");

				if (dContinuance != 0 )
					lblContinuance.Text = dContinuance.ToString("$#,##00;($#,##0)");

				if (dInitialAdj != 0 )
					lblInitialAdj.Text = dInitialAdj.ToString("$#,##0;($#,##0)");

				if (dFinalAdj != 0 )
					lblFinalAdj.Text = dFinalAdj.ToString("$#,##0;($#,##0)");

                if (dLateFee != 0)
                    lbllatefee.Text = dLateFee.ToString("$#,##0;($#,##0)");

                // tahir 4786 10/08/2008
                if (dPlanFee  != 0)
                    lblPlanFee.Text = dPlanFee.ToString("$#,##0;($#,##0)");

                // Rab Nawaz Khan 10330 07/03/2012 Added the Amount for the Day Of Arr Amount found. . . 
                if (dayOfArrFee != 0)
                    lbl_ArrDayOf.Text = dayOfArrFee.ToString("$#,##0;($#,##0)");
                
                AddTotal = dSpeeding + dAccident + dCDL + dLateFee + dRoundOff + dContinuance + dInitialAdj + dFinalAdj + dPlanFee + dayOfArrFee;

				if (AddTotal != 0 )
					lblAddTotal.Text = AddTotal.ToString("$#,##0;($#,##0)");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	

		//calculate the total fee for all violations.
		private void CalculateTotal()
		{
			try
			{
				double gTotal = 0;
				foreach(DataGridItem ItemX in dgResult.Items)
				{
					string sAmount = ((Label) (ItemX.FindControl("lblAmount"))).Text;	
					sAmount = sAmount.Substring(1,sAmount.Length -1);
					gTotal +=  ((double)  (Convert.ChangeType( sAmount ,typeof(double))));
				}
				gTotal+= AddTotal;
				lblGrandTotal.Text = gTotal.ToString("$#,##0;($#,##0)");
			}
			catch(Exception ex)
			{

				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}
		
		
	}
}

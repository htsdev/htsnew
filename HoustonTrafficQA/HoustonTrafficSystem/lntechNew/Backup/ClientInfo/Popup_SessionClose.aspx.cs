using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.ClientInfo
{
    public partial class Popup_SessionClose : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btn_Yes.Attributes.Add("onclick", "CloseSession(1);");
            btn_No.Attributes.Add("onclick", "CloseSession(0);");
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewLead.aspx.cs" Inherits="lntechNew.ClientInfo.AddNewLead" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add New Lead</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script src="../Scripts/GeneralValidations.js" type="text/javascript">
    </script>
    <style type="text/css">
        .style1
        {
            width: 80px;
        }
        .clsLeftPaddingTable TD
        {
	        font-size: 8pt;
	        color: #123160;
	        font-family: Tahoma;
	        background-color: #EFF4FB;
        }
    </style>
</head>
<body id="bd_addnewlead" runat="server" style="margin: 0px">

    <script src="/Scripts/jquery-1.4.4.js" type="text/javascript"></script>

    <script src="/Scripts/jquery.maskedinput-1.2.2.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        FocusPhone = function(ids, source) {
            var tdbox = document.getElementById(ids);

            var chk = document.getElementById("<%=isContactInternational.ClientID%>");

            if (chk.checked)
            { $(tdbox).unmask(); }
            else {
                $(tdbox).unmask();
                $(tdbox).mask("999-999-9999?X9999");

            }
            if (source == "checkbox") {
                tdbox.value = "";
                tdbox.focus();
            }

        };

        FocusDate = function(ids) {
            var tdbox = document.getElementById(ids);
            $(tdbox).unmask();
            $(tdbox).mask("99/99/9999");

        };
    </script>

    <script type="text/javascript">

        function isEmail(emails) {
            var regex = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
            if (!emails.match(regex)) {
                return false;
            }


            return true;

        }
        function ValidateDepuyContactUs() {
            
            // Rab Nawaz Khan 11473 10/21/2013 Added Caller Id validation . . .
            if ((document.getElementById("<%=txt_CallerId.ClientID%>").value == "") && (!document.getElementById("<%=chk_unknown.ClientID%>").checked)) {
                alert('Please enter Caller ID of the caller or select Unknown');
                document.getElementById("<%=txt_CallerId.ClientID%>").focus();
                return false;
            }
            if ((document.getElementById("<%=txt_Name.ClientID%>").value == "") && (!document.getElementById("<%=chk_unknownPerson.ClientID%>").checked)) {
                alert('Please enter Name or select Unknown');
                document.getElementById("<%=txt_Name.ClientID%>").focus();
                return false;
            }
            if (!CheckName(document.getElementById("<%=txt_Name.ClientID%>").value)) {
                alert("Please enter Name in alphabets");
                document.getElementById("<%=txt_Name.ClientID%>").focus();
                return false;
            }
            if (document.getElementById('<%= ddl_leadStatus.ClientID %>').value == '0') {
                var ddlLanguage = document.getElementById("<%=ddlLanguage.ClientID %>");
                if (ddlLanguage.options[ddlLanguage.selectedIndex].value == "--Select any Language--") {
                    alert("Please select Language");
                    ddlLanguage.focus();
                    return false;
                }
            }
            if (document.getElementById('<%= ddl_leadStatus.ClientID %>').value == '0') {
                if (document.getElementById("<%=txt_Email.ClientID%>").value == "") {
                    alert('Please enter email address');
                    document.getElementById("<%=txt_Email.ClientID%>").focus();
                    return false;
                }

                if (checkValidation(document.getElementById("<%=txt_Email.ClientID%>")) == false) {
                    document.getElementById("<%=txt_Email.ClientID%>").focus();
                    return false;
                }
            }
            if (document.getElementById("<%=isContactInternational.ClientID%>").checked) {
                if (document.getElementById("<%=txt_Phone.ClientID%>").value != "" && isNaN(document.getElementById("<%=txt_Phone.ClientID%>").value)) {
                    alert("Phone number entered is in incorrect format");
                    document.getElementById("<%=txt_Phone.ClientID%>").value = ""
                    document.getElementById("<%=txt_Phone.ClientID%>").focus();
                    return false;
                }

                var strphone = document.getElementById("<%=txt_Phone.ClientID%>").value;
                if (document.getElementById("<%=txt_Phone.ClientID%>").value != "" && strphone.length < 10) {
                    alert("Your telephone number should include the area code.");
                    document.getElementById("<%=txt_Phone.ClientID%>").focus();
                    return false;
                }
            }


            var ddlQuestion = document.getElementById("<%=ddlQuestion.ClientID %>");
            if (document.getElementById('ddl_leadStatus').value == '0') {
                var ddlQuestion = document.getElementById("<%=ddlQuestion.ClientID %>");
                if (ddlQuestion.options[ddlQuestion.selectedIndex].value == "0") {
                    alert("Please select valid Practice Area ");
                    ddlQuestion.focus();
                    return false;
                } 
            }
           if (ddlQuestion.options[ddlQuestion.selectedIndex].value == "7" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "28" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "29" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "30" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "31" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "1") {
                if (document.getElementById("<%=ddlZimmer.ClientID %>").value == "0" && ddlQuestion.options[ddlQuestion.selectedIndex].value == "1") {
                    alert("Please select zimmer device.");
                    document.getElementById("<%=ddlZimmer.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%=ddlManufacturer.ClientID%>").value == "0" && (ddlQuestion.options[ddlQuestion.selectedIndex].value == "7" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "28" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "29" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "30" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "31")) {
                    alert("Please select hip device.");
                    document.getElementById("<%=ddlManufacturer.ClientID%>").focus();
                    return false;
                }
                
                if (document.getElementById("<%=ddlIsPain.ClientID%>").value == "0" && (ddlQuestion.options[ddlQuestion.selectedIndex].value == "7" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "28" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "29" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "30" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "31" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "1")) {
                    alert("Please select level of pain.");
                    document.getElementById("<%=ddlIsPain.ClientID%>").focus();
                    return false;
                }
            }
            //Haris Ahmed 10292 05/21/2012 Validate date in Transvagianal Mesh Question
            if (ddlQuestion.options[ddlQuestion.selectedIndex].value == "24") {
                if (!checkdate(document.getElementById('<%=txtTMDate1.ClientID %>').value)) {
                    return false;
                }
            }
            if (document.getElementById("<%=txtComments.ClientID%>").value == "") {
                alert('Please enter comments');
                document.getElementById("<%=txtComments.ClientID%>").focus();
                return false;
            }

            
            document.getElementById("<%=btn_submit.ClientID%>").style.display = "none";
            return true;
        }
        function CheckComments(name) {
            for (i = 0; i < name.length; i++) {
                var asciicode = name.charCodeAt(i)
                if ((asciicode == 60) || (asciicode == 62))
                    return false;
            }
            return true;
        }

        function showHidContactUs(id) {
            if (id == "0") {
                document.getElementById('<%=tbl_ProductDefect.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Actos.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Pradaxa.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_TransvagianalMesh.ClientID%>').style.display = "none";
            }
            else if (id == "7" || id == "28" || id == "29" || id == "30" || id == "31" || id == "1") {
                document.getElementById('<%=tbl_ProductDefect.ClientID%>').style.display = "";
                document.getElementById('<%=tbl_Actos.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Pradaxa.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_TransvagianalMesh.ClientID%>').style.display = "none";
                if (id == "7" || id == "28" || id == "29" || id == "30" || id == "31") {
                    document.getElementById('<%=lbl_Manufacturer.ClientID %>').innerHTML = "What hip device do you have? ";
                    document.getElementById('<%=ddlManufacturer.ClientID %>').style.display = "";
                    document.getElementById('<%=ddlZimmer.ClientID %>').style.display = "none";
                }
                else if (id == "1") {
                    document.getElementById('<%=lbl_Manufacturer.ClientID %>').innerHTML = "What knee device do you have? ";
                    document.getElementById('<%=ddlManufacturer.ClientID %>').style.display = "none";
                    document.getElementById('<%=ddlZimmer.ClientID %>').style.display = "";
                }
            }
            else if (id == "23") {
                document.getElementById('<%=tbl_ProductDefect.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Actos.ClientID%>').style.display = "";
                document.getElementById('<%=tbl_Pradaxa.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_TransvagianalMesh.ClientID%>').style.display = "none";
            }
            //Haris Ahmed 10292 05/21/2012 Setting controls when selecting Transvagianal Mesh
            else if (id == "24") {
                document.getElementById('<%=tbl_ProductDefect.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Actos.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Pradaxa.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_TransvagianalMesh.ClientID%>').style.display = "";
            }
            //Haris Ahmed 10292 05/21/2012 Setting controls when selecting Pradaxa
            else if (id == "27") {
                document.getElementById('<%=tbl_ProductDefect.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Actos.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Pradaxa.ClientID%>').style.display = "";
                document.getElementById('<%=tbl_TransvagianalMesh.ClientID%>').style.display = "none";
            }
            else {
                document.getElementById('<%=tbl_ProductDefect.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Actos.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_Pradaxa.ClientID%>').style.display = "none";
                document.getElementById('<%=tbl_TransvagianalMesh.ClientID%>').style.display = "none";
            }
        }
        
        function CheckName(name) {
            for (i = 0; i < name.length; i++) {
                var asciicode = name.charCodeAt(i)
                if (!((asciicode > 64 && asciicode <= 90) || (asciicode >= 97 && asciicode <= 122) || (asciicode == 32) || (asciicode == 46)))
                    return false;
            }
            return true;
        }
        //Haris Ahmed 10292 05/21/2012 Add function to validate Date
        function checkdate(input) {
            var validformat = /^\d{2}\/\d{2}\/\d{4}$/;  //Basic check for format validity
            var givenDate = new Date(input);
            var toDate = new Date();
            var returnval = false;
            if (!validformat.test(input))
                alert("Please enter valid date.");
            else { //Detailed check for valid date ranges
                var monthfield = input.split("/")[0];
                var dayfield = input.split("/")[1];
                var yearfield = input.split("/")[2];
                var dayobj = new Date(yearfield, monthfield - 1, dayfield);
                if ((dayobj.getMonth() + 1 != monthfield) || (dayobj.getDate() != dayfield) || (dayobj.getFullYear() != yearfield))
                    alert("Please enter valid date.");
                else if (yearfield < 1900 || givenDate > toDate) {
                    alert('Please enter date between 01/01/1900 and today.');
                    // myDate is between startDate and endDate
                }
                else
                    returnval = true;
            }
            if (returnval == false) document.getElementById("<%=txtTMDate1.ClientID%>").focus();
            return returnval;
        }
        function pad(number, length) {

            var str = '' + number;
            while (str.length < length) {
                str = '0' + str;
            }

            return str;

        }
        // Rab Nawaz Khan 11473 10/25/2013 if Unknown check marked then clear the text box values. . .
        function ClearValues() {
            if (document.getElementById("<%=chk_unknown.ClientID%>").checked) {
                document.getElementById("<%=txt_CallerId.ClientID%>").value = "";
                document.getElementById("<%=txt_CallerId.ClientID%>").disabled = true;
            }
            else
                document.getElementById("<%=txt_CallerId.ClientID%>").disabled = false;
            if (document.getElementById("<%=chk_unknownPerson.ClientID%>").checked) {
                document.getElementById("<%=txt_Name.ClientID%>").value = "";
                document.getElementById("<%=txt_Name.ClientID%>").disabled = true;
            }
            else
                document.getElementById("<%=txt_Name.ClientID%>").disabled = false;
            return false;
        }
        
        // Rab Nawaz Khan 11473 10/25/2013 used to refresh the parent page if close button clicked
        function closeAddNewLeads() {
            window.opener.location.reload();
            window.close();
        }
        
        // Rab Nawaz Khan 11473 11/11/2013 used to change the required fields. . .
        function ChangeRequiredFields(value) {
            if(value == '9' || value == '10') {
                document.getElementById('<%= dv_language.ClientID %>').style.display = "none";
                document.getElementById('<%= dv_email.ClientID %>').style.display = "none";
                document.getElementById('<%= dv_prcArea.ClientID %>').style.display = "none";
            }
            else {
                document.getElementById('<%= dv_language.ClientID %>').style.display = "block";
                document.getElementById('<%= dv_language.ClientID %>').style.display = "inline";
                document.getElementById('<%= dv_email.ClientID %>').style.display = "block";
                document.getElementById('<%= dv_email.ClientID %>').style.display = "inline";
                document.getElementById('<%= dv_prcArea.ClientID %>').style.display = "block";
                document.getElementById('<%= dv_prcArea.ClientID %>').style.display = "inline";
            }
        }
    </script>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div>
        <asp:UpdatePanel ID="upnlResult" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="2" height="11" 
                            width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="left"
                            colspan="2">
                            &nbsp;Add New Lead
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="2" height="11" 
                            width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                        CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                </table>
                <table class="clsLeftPaddingTable" width="100%">
                    <tr>
                        <td class="style1">
                            <span class="clssubhead">Caller ID&nbsp;</span>
                        </td>
                        <td >
                            <asp:TextBox ID="txt_CallerId" runat="server" CssClass="clsInputadministration" MaxLength="50"
                                Text="" Width="180px"></asp:TextBox>&nbsp;
                                <asp:CheckBox runat="server" onclick="ClearValues(this);" ID="chk_unknown" CssClass="clsLabelNew" Text="Unknown"/>
                            <span style="color: Red">&nbsp;*</span>
                        </td>
                        </tr>
                    <tr>
                        <td class="style1">
                            <span class="clssubhead">Name&nbsp;</span>
                        </td>
                        <td >
                            <asp:TextBox ID="txt_Name" runat="server" CssClass="clsInputadministration" MaxLength="50"
                                Text="" Width="180px"></asp:TextBox>&nbsp;
                                <asp:CheckBox runat="server" onclick="ClearValues(this);" ID="chk_unknownPerson" 
                                CssClass="clsLabelNew" Text="Unknown"/>
                            <span style="color: Red">&nbsp;*</span>
                        </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span class="clssubhead">Language&nbsp;</span>
                            </td>
                            <td >
                                <asp:DropDownList ID="ddlLanguage" 
                                    onchange="showHidContactUs(<%=ddlQuestion.ClientID%>);" CssClass="clsInputCombo"
                                    runat="server" Width="254px">
                                    <asp:ListItem>--Select any Language--</asp:ListItem>
                                    <asp:ListItem>English</asp:ListItem>
                                    <asp:ListItem>Spanish</asp:ListItem>
                                    <asp:ListItem>Other</asp:ListItem>
                                </asp:DropDownList>
                                <div id = "dv_language" style="display:inline" runat = "server"><span style="color: Red">&nbsp;*</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span class="clssubhead">Email&nbsp;</span>
                            </td>
                            <td >
                                <asp:TextBox ID="txt_Email" runat="server" CssClass="clsInputadministration" MaxLength="200"
                                    Text="" Width="250px"></asp:TextBox>
                                <div id = "dv_email" style="display:inline" runat = "server"><span style="color: Red">&nbsp;*</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span class="clssubhead">Phone&nbsp;</span>
                            </td>
                            <td >
                                <asp:TextBox ID="txt_Phone" runat="server" onfocus="FocusPhone(this.id,'textbox');"
                                    CssClass="clsInputadministration" Width="250px"
                                    MaxLength="30"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                &nbsp;</td>
                            <td>
                                <asp:CheckBox ID="isContactInternational" runat="server" CssClass="clsLabelNew" 
                                    onclick="javascript:FocusPhone('txt_Phone','checkbox');" 
                                    Text="Outside USA" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span class="clssubhead">Practice Area</span>
                            </td>
                            <td >
                                <asp:DropDownList ID="ddlQuestion" onchange="showHidContactUs(this.value);" CssClass="clsInputCombo"
                                    runat="server" Width="254px">
                                </asp:DropDownList>
                                <div id = "dv_prcArea" style="display:inline" runat = "server"><span style="color: Red">&nbsp;*</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                
                            </td>
                            <td align="left" valign="middle">
                                <table runat="server" id="tbl_ProductDefect" style="display: none" class="clsLeftPaddingTable">
                                    <tr>
                                        <td align="left" class="clsLeftPaddingTable">
                                            <asp:Label ID="lbl_Manufacturer" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:DropDownList ID="ddlManufacturer" CssClass="clsInputCombo" runat="server">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlZimmer" CssClass="clsInputCombo" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clsLeftPaddingTable">
                                            Level of Pain [1=lowest - 10=highest]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:DropDownList ID="ddlIsPain" CssClass="clsInputCombo" runat="server">
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <table runat="server" id="tbl_Actos" style="display: none" cellspacing="0" class="clsLeftPaddingTable">
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Have you been diagnosed with bladder cancer?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblChoice1" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Do you have any of the following symptoms? (check all that apply)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:CheckBoxList ID="cblSymptoms" runat="server">
                                                <asp:ListItem Value="1">Blood in the urine (bright red, dark yellow, or cola colored)</asp:ListItem>
                                                <asp:ListItem Value="2">Frequent or painful urination</asp:ListItem>
                                                <asp:ListItem Value="3">Frequent urinary tract infection</asp:ListItem>
                                                <asp:ListItem Value="4">Lower back or abdominal pain</asp:ListItem>
                                                <asp:ListItem Value="5">Not sure</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Have you ever take Actos, Actoplus Met, Actoplus Met XR, or Duetact?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblChoice2" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            How long did you ever take Actos, Actoplus Met, Actoplus Met XR, and/or Duetact?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblChoice3" runat="server">
                                                <asp:ListItem Value="1">More than one year</asp:ListItem>
                                                <asp:ListItem Value="0">Less than 1 year</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" runat="server" id="tbl_Pradaxa" class="clsLeftPaddingTable"
                                    style="display: none">
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Have you or a loved one had an Internal Bleed or Brain Hemorrhage due to Pradaxa?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblPradaxaC1" runat="server">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Has this Internal Bleed or Brain Hemorrhage cause the death of a loved one?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblPradaxaC2" runat="server">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" runat="server" id="tbl_TransvagianalMesh" class="clsLeftPaddingTable"
                                    style="display: none">
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            When was your surgery date?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:TextBox ID="txtTMDate1" CssClass="clsInputadministration" Width="100px" runat="server"
                                                onfocus="FocusDate(this.id);"></asp:TextBox>&nbsp;<asp:Label
                                                ID="lblDateFormat" Text="mm/dd/yyyy" runat="server" CssClass="lbl"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Was your surgery due to the following?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:CheckBoxList ID="cblTMSymptoms" runat="server">
                                                <asp:ListItem Value="1">Pelvic Organ Prolapse (POP)</asp:ListItem>
                                                <asp:ListItem Value="2">Stress Urinary Incontinence (SUI)</asp:ListItem>
                                                <asp:ListItem Value="3">Hernia</asp:ListItem>
                                                <asp:ListItem Value="4">Other</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            Did you receive a Bladder Sling?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblTMChoice1" runat="server">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            How was your Mesh Implanted?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" class="clsLeftPaddingTable">
                                            <asp:RadioButtonList ID="rblTMChoice2" runat="server">
                                                <asp:ListItem Value="1">Transvaginally</asp:ListItem>
                                                <asp:ListItem Value="0">Abdominally</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="2">Other</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span class="clssubhead">Lead Status</span>
                            </td>
                            <td >
                                <asp:DropDownList ID="ddl_leadStatus" onchange="ChangeRequiredFields(this.value)" CssClass="clsInputCombo" runat="server" Width="100px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1" valign="top">
                                <span class="clssubhead">Comments&nbsp;</span>
                            </td>
                            <td  valign="top">
                                <asp:TextBox ID="txtComments" runat="server" CssClass="clsInputadministration" Height="60px"
                                    Text="" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                <span style="color: Red; vertical-align: top;">&nbsp;*</span>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="2" height="11" 
                                >
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2" >
                               <asp:HiddenField ID = "hf_ticketId" runat = "Server" value = "0" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2" >
                                <asp:Button ID="btn_submit" runat="server" class="clsbutton" Text="Submit" 
                                    OnClientClick="return ValidateDepuyContactUs();" 
                                    OnClick="btn_submit_Click" Width="60px" />
                                &nbsp;<asp:Button ID="btn_Clear" runat="server" class="clsbutton" Text="Clear" 
                                    Width="60px" onclick="btn_Clear_Click" />
                                &nbsp;<asp:Button ID="btn_Close" runat="server" class="clsbutton" 
                                    onclientclick="return closeAddNewLeads();" TabIndex="9" Text="Close" Width="60px" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="2" height="11">
                            </td>
                        </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

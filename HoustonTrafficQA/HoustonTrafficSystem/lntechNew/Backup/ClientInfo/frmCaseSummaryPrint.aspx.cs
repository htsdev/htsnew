using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.WebControls;
using FrameWorkEnation.Components;

namespace lntechNew.ClientInfo
{
    public partial class frmCaseSummaryPrint : System.Web.UI.Page
    {
        int srch;
        int TicketID;
        clsLogger clog = new clsLogger();
        //clsCase	cCaseHistory =new clsCase();
        clsSession cSession = new clsSession();
        clsCaseDetail cCaseDetail = new clsCaseDetail();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsGeneralMethods clsgenral = new clsGeneralMethods();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                    SetDisplaySections();                  
                        pload();
                  
               
               
            }
            catch (Exception ex)
            {
             //   lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }		
        }

        void pload()
        {
            if (IsPostBack != true)
            {
                //srch   = Convert.ToInt32(cSession.GetCookie("sSearch",this.Request));
                srch = Convert.ToInt32(Request.QueryString["search"]);
                //ViewState["vTicketId"] = cSession.GetCookie("sTicketID" ,this.Request);
                ViewState["vTicketId"] = Request.QueryString["casenumber"];
                ViewState["vSearch"] = Request.QueryString["search"];


                TicketID = Convert.ToInt32(ViewState["vTicketId"]);

                if (TicketID.ToString() == null)  // If TicketID is null
                {
                 //   lblmsg.Text = "There is no TicketID";
                //    lblb.Visible = false;
                }
                else if (TicketID == 0)    //96892;	
                {
                    Response.Redirect("ViolationFeeold.aspx?newt=1", false);
                }
                else
                {
                    //TicketID = Convert.ToInt32(cSession.GetSessionVariable("sticketID" ,this.Session));
                    GetValue();
                    // Fill Notes in datagrid
                    //DataSet	DsGetNotes= clog.GetNotes(TicketID);  //commented by ozair

                    // Fill Summary InformationDetail
                    //DataSet	DsSummaryDetail=cCaseDetail.GetSummaryInfoDetail(TicketID);//commented by ozair
                    //DataSet DsSummaryDetail = cCaseDetail.GetSummaryInfoDetail(Convert.ToInt32(ViewState["vTicketId"]));
                    //DGDesc.DataSource = DsSummaryDetail;
                    //DGDesc.DataBind();

                    ////fill flags

                    //string[] key2 ={ "@ticketid" };
                    ////object[] value1={TicketID};//commented by ozair
                    //object[] value1 ={ Convert.ToInt32(ViewState["vTicketId"]) };
                    //DataSet Dsflag = ClsDb.Get_DS_BySPArr("usp_HTS_Get_flags_Info_by_TicketNumber ", key2, value1);
                    //DLFlag.DataSource = Dsflag;
                    //DLFlag.DataBind();
                }
            }
        }

        // Fill Summary Information
        private void GetValue()
        {
            try
            {
                string[] key1 ={ "@TicketId_pk" };
                //object[] value1={TicketID};//commneted by ozair
                object[] value1 ={ Convert.ToInt32(ViewState["vTicketId"]) };
                string strdate;
                string middate;
                string strtime;


                // GridView Contact Information
                //DataSet ds_contact = cCaseDetail.GetSummaryInformation(Convert.ToInt32(ViewState["vTicketId"]));
                //GV_contact.DataSource = ds_contact.Tables[0];
                //GV_contact.DataBind();

                DataSet dsDetail = ClsDb.Get_DS_BySPArr("USP_HTS_GET_SummaryInfoDetail ", key1, value1);
                if (dsDetail.Tables[0].Rows.Count > 0)
                {
                    if ((dsDetail.Tables[0].Rows[0]["officername"].ToString() != null) || (dsDetail.Tables[0].Rows[0]["officername"].ToString() != ""))
                    {
                        td_lblofficername.Visible = true;
                        lblofficername.Text = dsDetail.Tables[0].Rows[0]["officername"].ToString();
                    }
                    else
                        td_lblofficername.Visible = false;
                    GV_matter.DataSource = dsDetail.Tables[0].DefaultView;
                    GV_matter.DataBind();
                }
                else
                    td_lblofficername.Visible = false;

                // History
                DataSet DsGetNotes = clog.GetNotes(Convert.ToInt32(ViewState["vTicketId"]));
                DLNote.DataSource = DsGetNotes;
                DLNote.DataBind();

                // Flags
                DataSet DsGetFlags = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ALLFLAGS ", key1, value1);
                if (DsGetFlags.Tables[0].Rows.Count > 0)
                {
                    DL_Flag.DataSource = DsGetFlags;
                    DL_Flag.DataBind();
                }


                IDataReader dr = cCaseDetail.GetSummaryInfo(Convert.ToInt32(ViewState["vTicketId"]));
                if (dr.Read()) //  Read data in DataReader
                {

                    lblkbtn_name.Text = dr["lastName"].ToString() + ", " + dr["FirstName"].ToString();

                    // Address
                    lbladd.Text = dr["Address"].ToString();
                    labeldob.Text = dr["DOBirth"].ToString();
                    labeldl1.Text = dr["DLNum"].ToString();
                    lbl_midnumber.Text = dr["MidNumber"].ToString();
                    lbllang1.Text = dr["Language"].ToString();

                    // Telephone numbers
                    if (dr["Contact1"].ToString() != "")
                    {
                        lbltel1.Text = clsgenral.formatPhone(dr["Contact1"].ToString());
                        if ((dr["Contactype1"].ToString() != null) && (dr["Contactype1"].ToString() != ""))
                            lbltel1.Text += "(" + clsgenral.formatPhone(dr["Contactype1"].ToString()) + ")";
                    }

                    if (dr["Contact2"].ToString() != "")
                    {
                        lbltel2.Text = clsgenral.formatPhone(dr["Contact2"].ToString());
                        if ((dr["Contactype2"].ToString() != null) && (dr["Contactype2"].ToString() != ""))
                            lbltel2.Text += "(" + clsgenral.formatPhone(dr["Contactype2"].ToString()) + ")";
                    }
                    if (dr["Contact3"].ToString() != "")
                    {
                        lbltel3.Text = clsgenral.formatPhone(dr["Contact3"].ToString());
                        if ((dr["Contactype3"].ToString() != null) && (dr["Contactype3"].ToString() != ""))
                            lbltel3.Text += "(" + clsgenral.formatPhone(dr["Contactype3"].ToString()) + ")";
                    }


                    if ((dr["email"].ToString() != "") && (dr["email"].ToString() != ""))
                    {
                        lblemail.Text = dr["email"].ToString();
                    }

                    // Comments
                    if ((dr["GenComments"].ToString() != "") && dr["GenComments"].ToString() != "")
                    {
                        tr_lblGenral.Visible = true;
                        lblGeneral.Text = dr["GenComments"].ToString();
                        tr_genral.Visible = true;
                    }
                    else
                    {
                        tr_genral.Visible = false;
                        tr_lblGenral.Visible = false;
                    }
                    if ((dr["ConComments"].ToString() != "") && (dr["ConComments"].ToString() != ""))
                    {
                        tr_lblContact.Visible = true;
                        lblContact.Text = dr["ConComments"].ToString();
                        tr_contactnotes.Visible = true;

                    }
                    else
                    {
                        tr_lblContact.Visible = false;
                        tr_contactnotes.Visible = false;
                    }
                    if ((dr["TriComments"].ToString() != "") && (dr["TriComments"].ToString() != ""))
                    {
                        tr_lbltrial.Visible = true;
                        lblTrial.Text = dr["TriComments"].ToString();
                        tr_trialcomments.Visible = true;
                    }
                    else
                    {
                        tr_lbltrial.Visible = false;
                        tr_trialcomments.Visible = false;
                    }
                    if ((dr["SetComments"].ToString() != "") && (dr["SetComments"].ToString() != ""))
                    {
                        tr_lblSetting.Visible = true;
                        lblSetting.Text = dr["SetComments"].ToString();
                        tr_settingcomments.Visible = true;
                    }
                    else
                    {
                        tr_lblSetting.Visible = false;
                        tr_settingcomments.Visible = false;
                    }

                    // Billing
                    lblInitialFee.Text = Convert.ToDouble(dr["Initial"].ToString()).ToString("$###0;($###0)"); //"$" + dr["Initial"].ToString(); 
                    lblContinuance.Text = Convert.ToDouble(dr["Continuance"].ToString()).ToString("$###0;($###0)");
                    lblReRep.Text = Convert.ToDouble(dr["Rerap"].ToString()).ToString("$###0;($###0)");
                    lbladjustment1.Text = Convert.ToDouble(dr["Adjust"].ToString()).ToString("$###0;($###0)");
                    lbltotalfee1.Text = Convert.ToDouble(dr["TotFee"].ToString()).ToString("$###0;($###0)");
                    lbladj1.Text = Convert.ToDouble(dr["InitialAdjustment"].ToString()).ToString("$###0;($###0)");
                    lblowes1.Text = Convert.ToDouble(dr["AmountOwes"].ToString()).ToString("$###0;($###0)");
                    lblpaidamount1.Text = Convert.ToDouble(dr["PaidAmount"].ToString()).ToString("$###0;($###0)");
			


                    // Flags
                    //if (dr["AccidentFlag"].ToString() == "Yes")
                    //{
                    //    tdacc.Visible = true;
                    //    lblac1.Text = "ACC";
                    //}
                    //else
                    //    tdacc.Visible = false;
                    //if (dr["CDLFlag"].ToString() == "Yes")
                    //{
                    //    tdcdl.Visible = true;
                    //    lblcd1.Text = "CDL";
                    //}
                    //else
                    //    tdcdl.Visible = false;
                    //if ((dr["FirmAbb"].ToString() != null) && (dr["FirmAbb"].ToString() != ""))
                    //{
                    //    tdfirms.Visible = true;
                    //    lblfirms1.Text = "Outside Firms Client";
                    //}
                    //else
                    //    tdfirms.Visible = false;
                    //if ((dr["bond"].ToString() != null) && (dr["bond"].ToString() != ""))
                    //{
                    //    tdbonds.Visible = true;
                    //    lblbonds1.Text = "Bonds";
                    //}
                    //else
                    //    tdbonds.Visible = false;

                }

                dr.Close();
            }
            catch (Exception ex)
            {
              //  lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetDisplaySections()
        {
            try
            {
                tr_contact.Visible = false;
                tr_matter.Visible = false;
                tr_flags.Visible = false;               
                tr_comments.Visible = false;
                tr_history.Visible = false;
                tr_billing.Visible =false;   
                
               

                char[] sp = { ',' };
                string[] dsection = Request.QueryString["DisplaySections"].Split(sp);
                             
                foreach(string display in dsection)
                {

                    if (display == "1")
                    {
                        tr_contact.Visible = true;
                    }
                    else if (display == "2")
                    {
                        tr_matter.Visible = true;
                    }
                    else if (display == "3")
                    {
                        tr_flags.Visible = true;                        
                    }
                    else if (display == "4")
                    {
                        tr_comments.Visible = true;                      
                    }
                    else if (display == "5")
                    {
                        tr_history.Visible = true;
                    }
                    else if (display == "6")
                    {
                        tr_billing.Visible = true;
                       
                    }                   
                }

            }
            catch (Exception ex)
            {
               // lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void lblkbtn_name_Click(object sender, EventArgs e)
        {

        }

        // Format Phone Number like 021-4414-15454544
        public static string formatPhone(string phone)
        {
            try
            {

                string clcode = phone;
                if (clcode.Length == 10)
                {
                    string temp = clcode.Insert(0, "(");
                    temp = temp.Insert(4, ") ");
                    return temp.Insert(9, "-");
                }
                else if (clcode.Length > 10)
                {
                    string temp = clcode.Insert(0, "(");
                    temp = temp.Insert(4, ") ");
                    temp = temp.Insert(9, "-");
                    return temp.Insert(14, " X-");

                }

                return phone;
            }
            catch (Exception ex)
            {
                return phone;
            }

        }

        protected void GV_matter_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

    }
}

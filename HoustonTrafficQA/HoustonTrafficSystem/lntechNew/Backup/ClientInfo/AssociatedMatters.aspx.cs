﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using HTP.Components.ClientInfo;
using System.IO;
using lntechNew.Components;

namespace HTP.ClientInfo
{
    public partial class AssociatedMatters : System.Web.UI.Page
    {
        #region variables
        string strExpr;
        string TicketID;
        string ContactID;
        clsCase ClsCase = new clsCase();
        clsLogger cLog;
        clsSession ClsSession = new clsSession();
        clsContact ClsContact = new clsContact();
        /// <summary>
        /// Associated Case Types
        /// </summary>
        public enum CaseStatus
        {
            Active = 1,
            Quotes = 2,
            Potential = 3,
            Disposed = 4
        }
        #endregion
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (Request.QueryString.Count >= 2)
                    {
                        ViewState["vTicketId"] = Request.QueryString["casenumber"];
                        ViewState["vSearch"] = Request.QueryString["search"];
                    }
                    else if (Request.QueryString["cid"] == null)
                    {
                        Response.Redirect("../frmMain.aspx", false);
                        goto SearchPage;
                    }
                    if (!IsPostBack)
                    {
                        ContactID = Request.QueryString["cid"];
                        TicketID = Request.QueryString["casenumber"]; ;

                        if (Request.QueryString["type"] == null)
                        {
                            if (ContactID != null || TicketID != null)
                            {
                                if (ContactID != null)
                                {
                                    TicketID = ClsContact.GetTicketForCID(Convert.ToInt32(Request.QueryString["cid"]));
                                    ViewState["vTicketId"] = TicketID;
                                    ViewState["vSearch"] = "0";
                                }

                                if (TicketID != null && TicketID != string.Empty && TicketID != "0")
                                {
                                    ContactID = ClsContact.GetContactIDByTicketID(Convert.ToInt32(TicketID)).ToString();
                                    ClsCase.GetClientInfo(Convert.ToInt32(TicketID));
                                }
                                //Calling Java Script method
                                if (ContactID == null || ContactID == string.Empty || ContactID == "0" || TicketID == null || TicketID == string.Empty || TicketID == "0")
                                {
                                    HttpContext.Current.Response.Write("<script>alert('No matter is associated with this CID#" + Request.QueryString["cid"] + "' );window.close();</script>");
                                    goto SearchPage;
                                }

                                //set the values in label
                                lbl_FirstName.Text = ClsCase.FirstName.ToUpper();
                                lbl_LastName.Text = ClsCase.LastName.ToUpper();
                                lbl_CaseCount.Text = ClsCase.Casecount.ToString();
                                hlnk_MidNo.Text = ClsCase.MidNumber;
                                string casetype = ViewState["vSearch"].ToString();

                                switch (casetype)
                                {
                                    case "0":
                                        hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                                        break;
                                    case "1":
                                        hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                                        break;
                                }
                                BindNodes();
                            }
                            else
                            {
                                HttpContext.Current.Response.Write("<script>alert('No matter is associated with this CID#" + Request.QueryString["cid"] + "' );window.close();</script>");
                                goto SearchPage;
                            }
                        }
                        //hiring the client here for given ticket number by moving data from non client to client table
                        else if (Request.QueryString["type"] == "SignUp")
                        {
                            string ticketnumber = Request.QueryString["TicketNo"];
                            string Address = Request.QueryString["Address"];
                            string ZipCode = Request.QueryString["ZipCode"];
                            string MidNum = Request.QueryString["MidNum"];
                            string CaseTypeID = Request.QueryString["CaseType"];

                            string[] ticketidFlag;
                            if (CaseTypeID == "4")
                            {
                                ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(6, ticketnumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), "", "", 0, MidNum, 0);
                            }
                            else
                            {
                                ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(999, ticketnumber, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), Address, ZipCode, 0, MidNum, 0);
                            }
                            Response.Redirect("ViolationFeeold.aspx?search=1&casenumber=" + ticketidFlag[0], false);
                        }

                        //Disassociate the contact id by removing potential ID from non client table
                        else if (Request.QueryString["type"] == "UnLink")
                        {
                            if (Request.QueryString["RecordID"] != null)
                            {
                                clsContact clscontact = new clsContact();
                                int RecordID = Convert.ToInt32(Request.QueryString["RecordID"]);
                                string CaseTypeID = Request.QueryString["CaseType"];
                                if (CaseTypeID != null || CaseTypeID != string.Empty)
                                {
                                    clscontact.UnlinkContactFromNonClient(RecordID, Convert.ToInt32(CaseTypeID));
                                }
                                string CID = Request.QueryString["cid"];

                                if (CID != null)
                                {
                                    Response.Redirect("AssociatedMatters.aspx?search=0&cid=" + CID, false);
                                }
                                else
                                {
                                    string caseNumber = Request.QueryString["casenumber"];
                                    if (caseNumber != null && caseNumber != string.Empty && caseNumber != "0")
                                        Response.Redirect("AssociatedMatters.aspx?search=0&casenumber=" + caseNumber, false);
                                    else
                                    {
                                        HttpContext.Current.Response.Write("<script>alert('No matter is associated with this CID#" + Request.QueryString["cid"] + "' );window.close();</script>");
                                        goto SearchPage;
                                    }
                                }
                            }
                        }
                    }
                    ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                    TextBox txt1 = (TextBox)am.FindControl("txtid");
                    txt1.Text = Convert.ToString(ViewState["vTicketId"]);
                    TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                    txt2.Text = Convert.ToString(ViewState["vSearch"]);
                SearchPage:
                    { }
                }
            }
            catch (Exception e_pload)
            {
                cLog = new clsLogger();
                cLog.ErrorLog(e_pload.Message, e_pload.Source, e_pload.TargetSite.ToString(), e_pload.StackTrace);
                lblMessage.Text = e_pload.Message;
            }
        }

        #endregion
        #region Methods

        /// <summary>
        /// Use to add Status nodes in Tree
        /// </summary>
        public void BindNodes()
        {
            DataSet ds = ClsContact.AssociatedMatters(TicketID);
            TreeNode tnCaseStatus;

            String[] names = Enum.GetNames(typeof(CaseStatus));
            Array values = Enum.GetValues(typeof(CaseStatus));
            TreeNode tnRoot = new TreeNode("Associated Matters (CID# " + ds.Tables[4].Rows[0]["ContactID"] + ")");

            TreeView1.Nodes.Add(tnRoot);
            //Add the above four type of status enum to tree view 
            for (int i = 0; i <= names.Length - 1; i++)
            {
                tnCaseStatus = new TreeNode();
                tnCaseStatus.Text = names[i].ToString();
                tnCaseStatus.Value = Convert.ToInt32(values.GetValue(i)).ToString();
                //pass the the to add the case type as child in given node
                BindChildNodes(tnCaseStatus, ds.Tables[i]);
                int checknode=0;
                for (int j = 0; j <= tnCaseStatus.ChildNodes.Count - 1; j++)
                {
                    if (tnCaseStatus.ChildNodes[j].ChildNodes.Count > 0)
                    {
                        checknode++;
                    }
                        
                }
                if (checknode==0)
                {
                    tnCaseStatus.Collapse();
                }
                tnRoot.ChildNodes.Add(tnCaseStatus);
            }
        }

        /// <summary>
        /// Add case type in each status nodes
        /// </summary>
        /// <param name="tnParent"></param>
        /// <param name="dtMatters"></param>
        public void BindChildNodes(TreeNode tnParent, DataTable dtMatters)
        {
            TreeNode tn;
            DataTable dt = new DataTable();
            DataTable dtFilter = new DataTable();

            DataRow[] FoundRows;
            //Get case types
            dt = ClsCase.GetCaseType();


            foreach (DataRow row in dt.Rows)
            {
                tn = new TreeNode();
                tn.Text = row[1].ToString();
                tn.Value = row[0].ToString();
                tnParent.ChildNodes.Add(tn);
                //make clone of matter table
                dtFilter = dtMatters.Clone();

                strExpr = "CaseTypeId = " + row[0].ToString();

                //Filter from matter table according to case type
                FoundRows = dtMatters.Select(strExpr);
                //add rows in clone table of each case type
                foreach (DataRow dr in FoundRows)
                {
                    dtFilter.ImportRow(dr);// or you can use dtvalid.Rows.Add(dr);
                }
                dtFilter.AcceptChanges();
                //pass the case type node to add matter gridview as child node in this node 
                BindGridAsChildNode(tn, dtFilter);

            }
        }

        /// <summary>
        /// create each case type matter gridview dynamically 
        /// </summary>
        /// <param name="tnParent"></param>
        /// <param name="dtMatters"></param>
        public void BindGridAsChildNode(TreeNode tnParent, DataTable dtMatters)
        {
            if (dtMatters.Rows.Count > 0)
            {
                //Extract distinct tickek id from matter table
                DataTable dt = dtMatters.DefaultView.ToTable(true, "TicketID_PK", "CaseTypeID");

                foreach (DataRow dr in dt.Rows)
                {
                    GridView gv = new GridView();
                    gv.AutoGenerateColumns = false;
                    gv.CssClass = "clsLeftPaddingTable";
                    gv.CellPadding = 0;
                    gv.CellSpacing = 0;
                    gv.Width = new Unit(850);

                    //bound field in grid view
                    //Cause Number Col
                    BoundField bfCauseNumber = new BoundField();
                    bfCauseNumber.HeaderStyle.CssClass = "clssubhead";
                    bfCauseNumber.HeaderStyle.Width = new Unit(85);
                    bfCauseNumber.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfCauseNumber.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCauseNumber.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCauseNumber.HeaderText = "Cause Number";
                    bfCauseNumber.DataField = "CauseNumber";
                    gv.Columns.Add(bfCauseNumber);

                    //Ticket Number Col
                    if (tnParent.Parent.Value == "3")
                    {
                        BoundField bfTicketNumber = new BoundField();
                        bfTicketNumber.HeaderText = "Ticket Number";
                        bfTicketNumber.HeaderStyle.CssClass = "clssubhead";
                        bfTicketNumber.HeaderStyle.Width = new Unit(85);
                        bfTicketNumber.ItemStyle.CssClass = "clsLeftPaddingTable";
                        bfTicketNumber.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                        bfTicketNumber.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        bfTicketNumber.DataField = "TicketNumber";
                        gv.Columns.Add(bfTicketNumber);
                    }
                    else
                    {

                        HyperLinkField hplnkTicketNumber = new HyperLinkField();
                        hplnkTicketNumber.HeaderText = "Ticket Number";
                        hplnkTicketNumber.HeaderStyle.CssClass = "clssubhead";
                        hplnkTicketNumber.HeaderStyle.Width = new Unit(85);
                        hplnkTicketNumber.ItemStyle.CssClass = "clsLeftPaddingTable";
                        hplnkTicketNumber.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                        hplnkTicketNumber.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        hplnkTicketNumber.DataNavigateUrlFormatString = "ViolationFeeold.aspx?sMenu=61&casenumber={0}&search=0";
                        hplnkTicketNumber.DataNavigateUrlFields = new string[] { "TicketID_PK" };
                        hplnkTicketNumber.DataTextField = "TicketNumber";
                        gv.Columns.Add(hplnkTicketNumber);
                    }



                    //Matter Description Col
                    BoundField bfMatterDescription = new BoundField();
                    bfMatterDescription.HeaderText = "Matter Description";
                    bfMatterDescription.HeaderStyle.CssClass = "clssubhead";
                    bfMatterDescription.HeaderStyle.Width = new Unit(170);
                    bfMatterDescription.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfMatterDescription.ItemStyle.BorderWidth = 1;
                    bfMatterDescription.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfMatterDescription.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfMatterDescription.DataField = "MatterDescription";
                    gv.Columns.Add(bfMatterDescription);

                    //Crt Location Col
                    BoundField bfCourtName = new BoundField();
                    bfCourtName.HeaderText = "Crt Loc";
                    bfCourtName.HeaderStyle.CssClass = "clssubhead";
                    bfCourtName.HeaderStyle.Width = new Unit(50);
                    bfCourtName.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfCourtName.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtName.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtName.DataField = "CourtLocation";
                    gv.Columns.Add(bfCourtName);

                    //Crt Date Col
                    BoundField bfCourtDate = new BoundField();
                    bfCourtDate.HeaderText = "Crt Date";
                    bfCourtDate.HeaderStyle.CssClass = "clssubhead";
                    bfCourtDate.HeaderStyle.Width = new Unit(60);
                    bfCourtDate.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfCourtDate.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtDate.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtDate.DataFormatString = "{0:d}";
                    bfCourtDate.DataField = "CourtDate";
                    gv.Columns.Add(bfCourtDate);

                    //Crt time Col
                    BoundField bfCourtTime = new BoundField();
                    bfCourtTime.HeaderText = "Crt Time";
                    bfCourtTime.HeaderStyle.CssClass = "clssubhead";
                    bfCourtTime.HeaderStyle.Width = new Unit(50);
                    bfCourtTime.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfCourtTime.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtTime.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtTime.DataFormatString = "{0:t}";
                    bfCourtTime.DataField = "CourtDate";
                    gv.Columns.Add(bfCourtTime);

                    BoundField bfStatus = new BoundField();
                    bfStatus.HeaderText = "Status";
                    bfStatus.HeaderStyle.CssClass = "clssubhead";
                    bfStatus.HeaderStyle.Width = new Unit(40);
                    bfStatus.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfStatus.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfStatus.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfStatus.DataField = "CourtStatus";
                    gv.Columns.Add(bfStatus);

                    //Room No Col
                    BoundField bfCourtRoomNumber = new BoundField();
                    bfCourtRoomNumber.HeaderText = "Crt";
                    bfCourtRoomNumber.HeaderStyle.CssClass = "clssubhead";
                    bfCourtRoomNumber.HeaderStyle.Width = new Unit(22);
                    bfCourtRoomNumber.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfCourtRoomNumber.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtRoomNumber.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfCourtRoomNumber.DataField = "RoomNumber";
                    gv.Columns.Add(bfCourtRoomNumber);

                    //Insert Date Col
                    BoundField bfInsertDate = new BoundField();
                    bfInsertDate.HeaderText = "Insert Date";
                    bfInsertDate.HeaderStyle.CssClass = "clssubhead";
                    bfInsertDate.HeaderStyle.Width = new Unit(65);
                    bfInsertDate.ItemStyle.CssClass = "clsLeftPaddingTable";
                    bfInsertDate.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfInsertDate.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bfInsertDate.DataFormatString = "{0:d}";
                    bfInsertDate.DataField = "InsertDate";
                    gv.Columns.Add(bfInsertDate);

                    //for potential
                    if (tnParent.Parent.Value == "3")
                    {
                        //Bound hyperlink of sign up when click move non client to client and redirect to matter page
                        HyperLinkField hplnkSignUp = new HyperLinkField();
                        hplnkSignUp.Text = "Sign Up";
                        hplnkSignUp.HeaderStyle.CssClass = "clssubhead";
                        hplnkSignUp.HeaderStyle.Width = new Unit(55);
                        hplnkSignUp.ItemStyle.CssClass = "clsLeftPaddingTable";
                        hplnkSignUp.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                        hplnkSignUp.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        hplnkSignUp.DataNavigateUrlFormatString = "AssociatedMatters.aspx?type=SignUp&TicketNo={0}&Address={1}&ZipCode={2}&MidNum={3}&CaseType={4}";
                        hplnkSignUp.DataNavigateUrlFields = new string[] { "TicketNumber", "Address1", "ZipCode", "MidNumber", "CaseTypeID" };
                        gv.Columns.Add(hplnkSignUp);

                        //Bound hyperlink fof un link when click ask for un link
                        HyperLinkField hplnkUnlink = new HyperLinkField();
                        hplnkUnlink.Text = "Disassociate";
                        hplnkUnlink.HeaderStyle.Width = new Unit(75);
                        hplnkUnlink.ItemStyle.CssClass = "clsLeftPaddingTable";
                        hplnkUnlink.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                        hplnkUnlink.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        hplnkUnlink.NavigateUrl = "javascript:Confirmation('" + dr["TicketID_PK"] + "','" + dr["CaseTypeID"] + "')";
                        hplnkUnlink.Target = "_top";
                        gv.Columns.Add(hplnkUnlink);
                    }

                    //filter table to show data for each ticket id
                    dtMatters.DefaultView.RowFilter = "TicketID_PK=" + dr["TicketID_PK"];
                    gv.DataSource = dtMatters.DefaultView; //drMatters[0].Table;
                    //Encode Navigate URL for potential
                    if (tnParent.Parent.Value == "3")
                    {
                        clsGeneralMethods.HyperLinkFieldUrlEncodeHack(gv);
                    }
                    gv.DataBind();
                    //Adding binded Grid to node
                    AddGridtoNode(gv, tnParent);

                    //if two ticket id in each table then insert blank row between grid
                    if (dt.Rows.Count > 1)
                    {
                        StringWriter sw = new StringWriter();
                        sw.Write("<table> <tr><td style='width: 100%' background='../Images/separator_repeat.gif' height='11'></td></tr></table>");
                        Html32TextWriter w = new Html32TextWriter(sw);

                        TreeNode tn = new TreeNode(sw.ToString());
                        tnParent.ChildNodes.Add(tn);
                    }
                }
            }
        }

        /// <summary>
        /// add the provided gridview in provided tree node as child
        /// </summary>
        /// <param name="gv"></param>
        /// <param name="tnParent"></param>
        private void AddGridtoNode(GridView gv, TreeNode tnParent)
        {
            StringWriter sw = new StringWriter();
            Html32TextWriter w = new Html32TextWriter(sw);
            gv.RenderControl(w);

            TreeNode tn = new TreeNode(sw.ToString());
            tnParent.ChildNodes.Add(tn);
        }

        #endregion

    }

}

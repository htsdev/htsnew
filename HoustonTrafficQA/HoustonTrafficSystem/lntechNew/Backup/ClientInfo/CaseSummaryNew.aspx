<%@ Page Language="C#" AutoEventWireup="true" Codebehind="CaseSummaryNew.aspx.cs"
    Inherits="lntechNew.ClientInfo.CaseSummaryNew" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Summary Crystal Report Version</title>
    <script type="text/javascript">
        
        function ValidateSelection()
            {
                var chk_ctc = document.getElementById('Contact');
                var chk_mtr = document.getElementById('Matter');
                var chk_flg = document.getElementById('Flags');
                var chk_com = document.getElementById('Comments');
                var chk_his = document.getElementById('History');
                var chk_bil = document.getElementById('Billing');
                var selected = 0;
                               
                if(chk_ctc.checked || chk_mtr.checked || chk_flg.checked || chk_com.checked || chk_his.checked ||chk_bil.checked)
                {
                    return(true);
                }                
                else
                {
                    alert('Please select at least one section');
                    return(false);
                }               
                               
             }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td class="clsleftpaddingtable">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%" class="clsleftpaddingtable">
                                            <tr>
                                                <td align="center">
                                                    <asp:CheckBox ID="Contact" runat="server" Font-Bold="True" Text="Contact" CssClass="clsleftpaddingtable"
                                                        Checked="True" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Matter" runat="server" Font-Bold="True" Text="Matter" CssClass="clsleftpaddingtable"
                                                        Checked="True" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Flags" runat="server" Font-Bold="True" Text="Flags" CssClass="clsleftpaddingtable"
                                                        Checked="True" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Comments" runat="server" Font-Bold="True" Text="Comments" CssClass="clsleftpaddingtable"
                                                        Checked="True" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="History" runat="server" Font-Bold="True" Text="History" Width="83px"
                                                        CssClass="clsleftpaddingtable" Checked="True" /></td>
                                                <td align="center">
                                                    <asp:CheckBox ID="Billing" runat="server" Font-Bold="True" Text="Billing" Width="83px"
                                                        CssClass="clsleftpaddingtable" Checked="True" /></td>
                                                <td align="center">
                                                    <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClick="btn_Submit_Click"
                                                      OnClientClick = "return ValidateSelection()"  Text="Submit" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" height="700">
                                        <iframe tabindex="0" src="../Reports/RptCaseSummary.aspx?ticketID=<%= Request.QueryString["casenumber"]%>&setting=<%=ViewState["setting"]%> "
                                            frameborder="1" width="100%" scrolling="auto" height="98%"></iframe>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <td>
                                </td>
                    <tr>
                    </tr>
            </tbody>
        </table>
    </form>
</body>
</html>

﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.ClientInfo
{
    public partial class AddNewLead : System.Web.UI.Page
    {
        clsAddNewLead objAddNewLead = new clsAddNewLead();
        clsLogger clog = new clsLogger();
        clsSession cSession = new clsSession();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetPracticeArea();
                GetManufacturers();
                GetLeadTypes();
                if (Request.QueryString.Count == 5)
                {
                    if (Request.QueryString["languageSpk"] != null && Request.QueryString["completeName"] != null &&
                        Request.QueryString["contactNum"] != null && Request.QueryString["email"] != null && Request.QueryString["caseNum"] != null)
                    {
                        if (Request.QueryString["languageSpk"].ToString().ToUpper().Equals("SPANISH"))
                            ddlLanguage.SelectedIndex = 2;
                        if (Request.QueryString["languageSpk"].ToString().ToUpper().Equals("ENGLISH"))
                            ddlLanguage.SelectedIndex = 1;
                        txt_Name.Text = Request.QueryString["completeName"].ToString();
                        txt_Email.Text = Request.QueryString["email"].ToString();
                        if (Request.QueryString["contactNum"].ToString().Length > 10)
                            txt_Phone.Text = Request.QueryString["contactNum"].ToString().Substring(0, 3) + "-" +
                                Request.QueryString["contactNum"].ToString().Substring(3, 3) + "-" +
                                Request.QueryString["contactNum"].ToString().Substring(6, 4) + "X" +
                                Request.QueryString["contactNum"].ToString().Substring(10);
                        else if (Request.QueryString["contactNum"].ToString().Length > 9)
                            txt_Phone.Text = Request.QueryString["contactNum"].ToString().Substring(0, 3) + "-" +
                                Request.QueryString["contactNum"].ToString().Substring(3, 3) + "-" +
                                Request.QueryString["contactNum"].ToString().Substring(6, 4);
                        else
                            txt_Phone.Text = Request.QueryString["contactNum"].ToString();
                        hf_ticketId.Value = Request.QueryString["caseNum"].ToString(); 
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void GetPracticeArea()
        {
            DataSet dsPracticeArea = objAddNewLead.GetActivePracticeArea("USP_PS_Get_ActivePracticeArea");
            ddlQuestion.DataSource = dsPracticeArea;
            ddlQuestion.DataTextField = "Description";
            ddlQuestion.DataValueField = "ID";
            ddlQuestion.DataBind();
            ddlQuestion.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// used to get the Lead types from DB
        /// </summary>
        protected void GetLeadTypes()
        {

            DataSet dsLeads = objAddNewLead.GetLeadTypes("USP_PS_Get_LeadTypes");
            ddl_leadStatus.DataSource = dsLeads;
            ddl_leadStatus.DataTextField = "lead";
            ddl_leadStatus.DataValueField = "leadId";
            ddl_leadStatus.DataBind();
            ddl_leadStatus.SelectedValue = "0";
        }

        /// <summary>
        /// Get Hip Manufacturer
        /// </summary>
        protected void GetManufacturers(int id)
        {
            DataTable dt = objAddNewLead.GetManufacturers(id);
            ddlManufacturer.DataSource = dt;
            ddlManufacturer.DataTextField = "Name";
            ddlManufacturer.DataValueField = "Id";
            ddlManufacturer.DataBind();
            ddlManufacturer.Items.Insert(0, new ListItem("--Select--", "0"));

        }

        /// <summary>
        /// 
        /// </summary>
        protected void GetManufacturers()
        {
            DataTable dt = objAddNewLead.GetManufacturers(7);
            ddlManufacturer.DataSource = dt;
            ddlManufacturer.DataTextField = "Name";
            ddlManufacturer.DataValueField = "Id";
            ddlManufacturer.DataBind();
            ddlManufacturer.Items.Insert(0, new ListItem("--Select--", "0"));
            dt = objAddNewLead.GetManufacturers(1);
            ddlZimmer.DataSource = dt;
            ddlZimmer.DataTextField = "Name";
            ddlZimmer.DataValueField = "Id";
            ddlZimmer.DataBind();
            ddlZimmer.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {
                int index = txt_Name.Text.IndexOf(' ', 0);
                string fName;
                string lName;
                string mailTo = string.Empty;

                string phone = "";
                if (string.IsNullOrEmpty(txt_Phone.Text.Trim()))//Fahad 11463 09/25/2013 Phone Number issue fixed
                    phone = "";
                else if (isContactInternational.Checked)
                {
                    phone = txt_Phone.Text;
                }
                else
                {
                    string[] phonesnumbers = txt_Phone.Text.Split('-');

                    if (phonesnumbers.Length >= 2) //xxx-xxx-xxxxXxxxx
                    {
                        string ext;
                        string number;
                        if (phonesnumbers[2].Contains("X"))
                        {
                            ext = phonesnumbers[2].Substring(phonesnumbers[2].IndexOf("X") + 1);
                            number = phonesnumbers[2].Substring(0, phonesnumbers[2].IndexOf("X"));
                        }
                        else
                        {
                            number = phonesnumbers[2];
                            ext = "";
                        }

                        phone = phonesnumbers[0] + phonesnumbers[1] + number + ext;
                    }
                }
                fName = index != -1 ? txt_Name.Text.Substring(0, index) : txt_Name.Text;
                lName = index != -1 ? txt_Name.Text.Substring(index + 1) : "";
                string manufecturer = "";
                // Rab Nawaz Khan 11473 10/21/2013 Getting Caller Id Text and unknown checkbox state. . . 
                int ticketId = Convert.ToInt32(hf_ticketId.Value);
                string callerId = txt_CallerId.Text.Trim();
                bool isUnknown = chk_unknown.Checked;
                bool isUnknowName = chk_unknownPerson.Checked;
                int leadTypeId = Convert.ToInt32(ddl_leadStatus.SelectedValue);
                if (ddlQuestion.SelectedValue != "0")
                {
                    if (ddlQuestion.SelectedValue == "7" || ddlQuestion.SelectedValue == "28" || ddlQuestion.SelectedValue == "29" || ddlQuestion.SelectedValue == "30" || ddlQuestion.SelectedValue == "31")
                    {
                        manufecturer = ddlManufacturer.SelectedItem.Text;
                    }
                    else if (ddlQuestion.SelectedValue == "1")
                    {
                        manufecturer = ddlZimmer.SelectedItem.Text;
                    }
                    else
                    {
                        manufecturer = ddlQuestion.SelectedItem.Text;
                    }
                }
                string strChoice1 = "";
                string strChoice2 = "";
                string strChoice3 = "";
                string symptons = "";

                if (Convert.ToInt16(ddlQuestion.SelectedValue) == 1 ||
                   Convert.ToInt16(ddlQuestion.SelectedValue) == 7 ||
                   Convert.ToInt16(ddlQuestion.SelectedValue) == 28 ||
                   Convert.ToInt16(ddlQuestion.SelectedValue) == 29 ||
                   Convert.ToInt16(ddlQuestion.SelectedValue) == 30 ||
                   Convert.ToInt16(ddlQuestion.SelectedValue) == 31)
                {
                    objAddNewLead.SaveDepuyContactUs(lName, fName, "", "", "", "", "", phone,
                                                                   isContactInternational.Checked, "", "",
                                                                   "", txt_Email.Text,
                                                                   Convert.ToDateTime("1/1/1900"),
                                                                   Convert.ToDateTime("1/1/1900"),
                                                                   manufecturer,
                                                                   txtComments.Text,
                                                                   "",
                                                                   ddlIsPain.SelectedItem.Text,
                                                                   ddlQuestion.SelectedItem.Value,
                                                                   ddlLanguage.SelectedValue, cSession.GetCookie("sEmpID", Request),
                                                                   callerId, isUnknown, isUnknowName, leadTypeId, ticketId // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                        );
                }
                else if (Convert.ToInt16(ddlQuestion.SelectedValue) == 23)
                {
                    string choice1 = "2";
                    string Question = "";

                    foreach (ListItem li in rblChoice1.Items)
                    {
                        if (li.Selected)
                        {
                            choice1 = li.Value;
                            strChoice1 = li.Text;
                            Question += "Have you been diagnosed with bladder cancer? : " + li.Text + "; ";
                        }
                    }

                    string choice2 = "2";
                    foreach (ListItem li in rblChoice2.Items)
                    {
                        if (li.Selected)
                        {
                            choice2 = li.Value;
                            strChoice2 = li.Text;
                            Question += "Have you ever take Actos, Actoplus Met, Actoplus Met XR, or Duetact? : " + li.Text + "; ";
                        }
                    }

                    string choice3 = "2";
                    foreach (ListItem li in rblChoice3.Items)
                    {
                        if (li.Selected)
                        {
                            choice3 = li.Value;
                            strChoice3 = li.Text;
                            Question += "How long did you ever take Actos, Actoplus Met, Actoplus Met XR, and/or Duetact? : " + li.Text + "; ";
                        }
                    }
                    bool symptons1 = false,
                         symptons2 = false,
                         symptons3 = false,
                         symptons4 = false,
                         symptons5 = false;
                    foreach (ListItem li in cblSymptoms.Items)
                    {
                        if (li.Selected && li.Value == "1")
                        {
                            symptons1 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "2")
                        {
                            symptons2 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "3")
                        {
                            symptons3 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "4")
                        {
                            symptons4 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "5")
                        {
                            symptons5 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                    }
                    if (symptons == "")
                        symptons = "Not sure";
                    else
                        symptons = symptons.Substring(0, symptons.Length - 2);

                    Question += "Do you have any of the following symptoms? : " + symptons + "; ";

                    objAddNewLead.SaveActosContactUs(txt_Name.Text, txt_Email.Text,
                                                     txt_Phone.Text,
                                                     "Home",
                                                     isContactInternational.Checked,
                                                     choice1,
                                                     choice2, choice3,
                                                     symptons1, symptons2, symptons3,
                                                     symptons4,
                                                     symptons5, txtComments.Text,
                                                     ddlQuestion.SelectedItem.Value,
                                                     ddlLanguage.SelectedValue, cSession.GetCookie("sEmpID", Request),
                                                     DateTime.Now, Question,
                                                     callerId, isUnknown, isUnknowName, leadTypeId, ticketId // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                                                     );
                }
                else if (Convert.ToInt16(ddlQuestion.SelectedValue) == 27)
                // Haris Ahmed 10292 05/22/2012 changes for Pradaxa Contact Us
                {
                    string choice1 = "2";
                    string Question = "";

                    foreach (ListItem li in rblPradaxaC1.Items)
                    {
                        if (li.Selected)
                        {
                            choice1 = li.Value;
                            strChoice1 = li.Text;
                            Question += "Have you or a loved one had an Internal Bleed or Brain Hemorrhage due to Pradaxa? : " + li.Text + "; ";
                        }
                    }

                    string choice2 = "2";
                    foreach (ListItem li in rblPradaxaC2.Items)
                    {
                        if (li.Selected)
                        {
                            choice2 = li.Value;
                            strChoice2 = li.Text;
                            Question += "Has this Internal Bleed or Brain Hemorrhage cause the death of a loved one? : " + li.Text + "; ";
                        }
                    }

                    objAddNewLead.SaveActosContactUs(txt_Name.Text, txt_Email.Text,
                                                     txt_Phone.Text,
                                                     "Home",
                                                     isContactInternational.Checked,
                                                     choice1,
                                                     choice2, "2",
                                                     false, false, false,
                                                     false,
                                                     false, txtComments.Text,
                                                     ddlQuestion.SelectedItem.Value,
                                                     ddlLanguage.SelectedValue,
                                                     cSession.GetCookie("sEmpID", Request), DateTime.Now, Question,
                                                     callerId, isUnknown, isUnknowName, leadTypeId, ticketId // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                                                     );
                }
                else if (Convert.ToInt16(ddlQuestion.SelectedValue) == 24)
                // Haris Ahmed 10292 05/22/2012 changes for Transvagianal Mesh Contact Us
                {
                    string choice1 = "2";
                    string Question = "";

                    Question += "When was your surgery date? : " + txtTMDate1.Text + "; ";

                    bool symptons1 = false,
                         symptons2 = false,
                         symptons3 = false,
                         symptons4 = false;
                    foreach (ListItem li in cblTMSymptoms.Items)
                    {
                        if (li.Selected && li.Value == "1")
                        {
                            symptons1 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "2")
                        {
                            symptons2 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "3")
                        {
                            symptons3 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                        if (li.Selected && li.Value == "4")
                        {
                            symptons4 = li.Selected;
                            symptons += @"<br/>" + li.Text + ", ";
                        }
                    }
                    if (symptons == "")
                        symptons = "Other";
                    else
                        symptons = symptons.Substring(0, symptons.Length - 2);

                    Question += "Was your surgery due to the following? : " + symptons + "; ";

                    foreach (ListItem li in rblTMChoice1.Items)
                    {
                        if (li.Selected)
                        {
                            choice1 = li.Value;
                            strChoice1 = li.Text;
                            Question += "Did you receive a Bladder Sling? : " + li.Text + "; ";
                        }
                    }

                    string choice2 = "2";
                    foreach (ListItem li in rblTMChoice2.Items)
                    {
                        if (li.Selected)
                        {
                            choice2 = li.Value;
                            strChoice2 = li.Text;
                            Question += "How was your Mesh Implanted? : " + li.Text + "; ";
                        }
                    }

                    objAddNewLead.SaveActosContactUs(txt_Name.Text, txt_Email.Text,
                                                     txt_Phone.Text,
                                                     "Home",
                                                     isContactInternational.Checked,
                                                     choice1,
                                                     choice2, "2",
                                                     symptons1, symptons2, symptons3,
                                                     symptons4,
                                                     false, txtComments.Text,
                                                     ddlQuestion.SelectedItem.Value,
                                                     ddlLanguage.SelectedValue,
                                                     cSession.GetCookie("sEmpID", Request),
                                                     Convert.ToDateTime(txtTMDate1.Text), Question,
                                                     callerId, isUnknown, isUnknowName, leadTypeId, ticketId // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                                                     );
                }
                else
                {
                    objAddNewLead.SaveContactUsInformation(txt_Name.Text, txt_Email.Text,
                                                                         phone,
                                                                         isContactInternational.Checked,
                                                                         "",
                                                                         txtComments.Text, "1",
                                                                         Convert.ToInt16(
                                                                             ddlQuestion.SelectedItem.
                                                                                 Value),
                                                                         ticketId, ddlLanguage.SelectedValue, cSession.GetCookie("sEmpID", Request),
                                                                         callerId, isUnknown, isUnknowName, leadTypeId // Rab Nawaz Khan 11473 10/21/2013 Added Parameters. . . 
                                                                         );
                }

                if (Convert.ToInt16(ddlQuestion.SelectedValue) == 23 || Convert.ToInt16(ddlQuestion.SelectedValue) == 24 || Convert.ToInt16(ddlQuestion.SelectedValue) == 27)
                // Haris Ahmed 10292 05/22/2012 changes for Pradaxa and Transvagianal Mesh Contact Us
                {
                    objAddNewLead.SendEmailContactUsFromMaster(txt_Name.Text, phone, txt_Email.Text,
                                                             txtComments.Text, ddlQuestion.SelectedItem.Text,
                                                             ddlQuestion.SelectedItem.Value, strChoice1,
                                                             strChoice2, strChoice3, symptons, ddlLanguage.SelectedValue, txtTMDate1.Text);
                }
                else
                {
                    objAddNewLead.SendEmailContactUsFromMaster(txt_Name.Text, phone, txt_Email.Text,
                                                             txtComments.Text, manufecturer,
                                                             ddlIsPain.SelectedItem.Text,
                                                             ddlQuestion.SelectedItem.Text,
                                                             ddlQuestion.SelectedItem.Value, "",
                                                             ddlLanguage.SelectedValue);
                }

                lbl_error.Text = "Record Added Successfully.";
                // Rab Nawaz Khan 11473 10/30/2013 Updating the Flage for submitted Leads. . . 
                if (ticketId > 0)
                    objAddNewLead.UpdateNewLeadSubmitted(ticketId);
                Clear();
            }
            catch (Exception ePload)
            {
                clog.ErrorLog(ePload.Message, ePload.Source, ePload.TargetSite.ToString(), ePload.StackTrace);
                lbl_error.Text = ePload.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Clear_Click(object sender, EventArgs e)
        {
            lbl_error.Text = "";
            Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            txt_Name.Text = "";
            txt_Email.Text = "";
            txt_Phone.Text = "";
            txtComments.Text = "";
            ddlLanguage.SelectedIndex = -1;
            ddlQuestion.SelectedIndex = -1;
            ddlLanguage.SelectedValue = "0";
            ddlQuestion.SelectedValue = "0";
            isContactInternational.Checked = false;
            ddlIsPain.SelectedIndex = -1;
            ddlIsPain.SelectedValue = "0";
            rblChoice1.Items[2].Selected = true;
            cblSymptoms.SelectedIndex = -1;
            rblChoice2.Items[2].Selected = true;
            rblChoice3.Items[2].Selected = true;
            // Rab Nawaz Khan 11473 10/21/2013 Clearing the values after adding the record. . . 
            txt_CallerId.Text = "";
            if (chk_unknown.Checked)
                chk_unknown.Checked = false;
            if (chk_unknownPerson.Checked)
                chk_unknownPerson.Checked = false;
            ddl_leadStatus.SelectedValue = "0";
        }
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourtDetail.aspx.cs" Inherits="HTP.ClientInfo.CourtDetail" EnableViewState ="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Court Detail</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAp3emSG1MlsDZIjgNlK3U-xQZ9FmlFZPu9FT8VFF5QiOydCV_IxTezfuonxslcx2coXLBwyvSxsOBZg"
        type="text/javascript"></script>

    <style type="text/css">
        v:
        {
            behavior: url(#default#VML);
        body
        {
            font-family: Verdana, Arial, sans serif;
            font-size: 11px;
            margin: 2px;
        }
        table.directions th
        {
            background-color: #EEEEEE;
        }
        img
        {
            color: #000000;
        }
        
        .style13
        {
            border-width: 0;
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            text-align: left;
            width: 96px;
        }
        .style14
        {
            border-width: 0;
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            text-align: left;
            width: 25%;
        }
    </style>

   

</head>
<body onload="load()" onunload="GUnload()">
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table width="350px">
            <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="clsLabel"
                ForeColor="Red"></asp:Label>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="clsLabel" valign="middle">
                            </td>
                            <td class="style13" valign="middle">
                                Court Name
                            </td>
                            <td class="style10">
                                <font color="#3366cc">
                                    <asp:Label ID="lblCourtName" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                </font>&nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLabel" valign="middle">
                                &nbsp;
                            </td>
                            <td class="style13" valign="middle">
                                Court Address
                            </td>
                            <td class="style10">
                                <font color="#3366cc">
                                    <asp:Label ID="lblCourtAddress" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLabel" valign="middle">
                                &nbsp;
                            </td>
                            <td class="style13" valign="middle">
                                Phone No
                            </td>
                            <td class="style10">
                                <font color="#3366cc">
                                    <asp:Label ID="lblCourtPhoneNo" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                </font>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <ajaxToolkit:TabContainer ID="Tabs" runat="server" ActiveTabIndex ="1">
                        <ajaxToolkit:TabPanel ID="pnl_ByDescription" runat="server" HeaderText="Case Processing Detail"
                            TabIndex="0">
                            <ContentTemplate>
                                <br />
                                <br />
                                <table class="clsleftpaddingtable " width="100%">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="5" style="height: 23px;">
                                            &nbsp;Case Processing Detail
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" height="20" valign="middle">
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Court Contact
                                        </td>
                                        <td class="style14"  height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblCourtContact" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td  class="style14"  height="20" valign="middle">
                                            County
                                        </td>
                                        <td class="style14"  height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblcountyname" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" height="20" valign="middle">
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Case Identification
                                        </td>
                                        <td class="style14" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblCaseIdentification" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20">
                                            Accept Appeals
                                        </td>
                                        <td class="style14" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblAcceptAppeals" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" height="20" valign="middle">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Appeal Location
                                        </td>
                                        <td class="style14" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblAppealLocation" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20">
                                            Initial Setting
                                        </td>
                                        <td class="style11" height="20">
                                            <font class="S" color="#3366cc">
                                                <asp:Label ID="lblInitialSetting" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" height="20" valign="middle">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            LOR
                                        </td>
                                        <td class="style14" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblLOR" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20">
                                            Appearance Hire
                                        </td>
                                        <td class="style11" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblAppearanceHire" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" height="20" valign="middle">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Judge Trial Hire
                                        </td>
                                        <td class="style14" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblJudgeTrialHire" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20">
                                            Jury Trial Hire
                                        </td>
                                        <td class="style11" height="20">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblJuryTrialHire" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" height="20" valign="middle">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Bond Type
                                        </td>
                                        <td class="style14" height="20">
                                            <font color="#3366cc" style="text-align: left">
                                                <asp:Label ID="lblBondType" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Handwriting Allowed
                                        </td>
                                        <td class="style12" valign="middle">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblHandwritingAllowed" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" valign="middle">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Additional FTA Issued
                                        </td>
                                        <td class="style14">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblAdditinalFTAIssued" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20">
                                            Grace Period
                                        </td>
                                        <td class="style11">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblGracePeriod" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Continuance
                                        </td>
                                        <td class="style14">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblContinuance" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Supporting Documents
                                        </td>
                                        <td class="style12" valign="middle">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblSupportingDocuments" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLabel" valign="middle">
                                            &nbsp;
                                        </td>
                                        <td class="style14" height="20" valign="middle">
                                            Comments
                                        </td>
                                        <td class="style3" colspan="3">
                                            <font color="#3366cc">
                                                <asp:Label ID="lblComments" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table width="570px" class="clsleftpaddingtable ">
                                    <tr>
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 23px">
                                            &nbsp;Uploaded Files
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvCourtFiles" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                Font-Names="Verdana" Font-Size="2px" OnRowDataBound="gvCourtFiles_RowDataBound"
                                                Width="100%">
                                                <FooterStyle CssClass="GrdFooter" />
                                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-CssClass="GrdLbl" HeaderStyle-CssClass="GrdHeader" DataField="Uploaded Date"
                                                        HeaderText="Uploaded Date" />
                                                    <asp:BoundField ItemStyle-CssClass="GrdLbl" HeaderStyle-CssClass="GrdHeader" DataField="DocumentName"
                                                        HeaderText="Document Name" />
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgPreviewDoc" runat="server" ImageUrl="../images/Preview.gif"
                                                                Width="19" />
                                                            <asp:HiddenField ID="hfFileId" runat="server" Value='<%# bind("id") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="pnl_ByDescriptione" runat="server" HeaderText="Map" TabIndex="1">
                            <ContentTemplate>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Directions
                                        </th>
                                        <th>
                                            Map
                                        </th>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <div id="directions" style="width: 250px">
                                            </div>
                                        </td>
                                        <td valign="top">
                                            <div id="map" style="width: 310px; height: 400px">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                </td> </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" colspan="2" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" style="height: 23px">
                                        <asp:Button ID="btn_Close" runat="server" CssClass="clsbutton" Text="Close" OnClientClick="window.close();"
                                            Width="94px"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" colspan="2" height="11">
                                    </td>
                                </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                    <asp:HiddenField ID="hfClientAddress" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
     <script type="text/javascript">
    //<![CDATA[
    
    //Kazim 4071 5/21/2008 Get Client and Court Addresses 
 
    var ClientAddress = document.getElementById("hfClientAddress").value;
    var CourtAddress =  document.getElementById("lblCourtAddress").innerText;
   
    var map;
    var gdir;
    var geocoder = null;
    var addressMarker;
    
    function OpenPopUpNew(path)  //(var path,var name)
    {
	    
	    window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
	    return false;
	}
	
    function querySt(ji)
    {
        hu = window.location.search.substring(1);
        gy = hu.split("&");
        for (i=0;i<gy.length;i++)
        {
            ft = gy[i].split("=");
            if (ft[0] == ji)
            {return ft[1];}
        }
    }

    function load() {
      
      if (GBrowserIsCompatible()) {      
        map = new GMap2(document.getElementById("map"));

        gdir = new GDirections(map, document.getElementById("directions"));
        GEvent.addListener(gdir, "load", onGDirectionsLoad);
        GEvent.addListener(gdir, "error", handleErrors);

        setDirections(ClientAddress, CourtAddress, "en_US");
      }
    }
    
    function setDirections(fromAddress, toAddress, locale) {
      gdir.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": locale });
    }

    function handleErrors(){
	   if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
	     alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
	   else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
	     alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
	   
	   else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
	     alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);

	//   else if (gdir.getStatus().code == G_UNAVAILABLE_ADDRESS)  <--- Doc bug... this is either not defined, or Doc is wrong
	//     alert("The geocode for the given address or the route for the given directions query cannot be returned due to legal or contractual reasons.\n Error code: " + gdir.getStatus().code);
	     
	   else if (gdir.getStatus().code == G_GEO_BAD_KEY)
	     alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);

	   else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
	     alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
	    
	   else alert("An unknown error occurred.");
	   
	}

	function onGDirectionsLoad(){ 
          // Use this function to access information about the latest load()
          // results.

          // e.g.
	  // document.getElementById("getStatus").innerHTML = gdir.getStatus().code;
	  // and yada yada yada...
	}


    //]]>
    </script>
</body>
</html>

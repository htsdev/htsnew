using System;  
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;

namespace HTP.WebComponents
{
    public class MatterBasePage : BasePage
    {
        private int _TicketId = 0;
        public int TicketId
        {
            get
            {
                if (this._TicketId == 0)
                {
                    if ((this.Request.QueryString["casenumber"] == null) ||
                        (this.Request.QueryString["casenumber"] == "") ||
                        (!int.TryParse(this.Request.QueryString["casenumber"], out this._TicketId)))
                    {
                        Response.Redirect("../frmMain.aspx", false);
                    }
                }
                return this._TicketId;
            }
            //Kazim 2729 2/27/2008 include set property of TicketId 
            set { this._TicketId = value; }
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;

using System.Data;
using System.Drawing;

using System.Web;
using System.Web.SessionState;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;


namespace Chap0601
{
	/// <summary>
	/// Summary description for WebForm2.
	/// </summary>
	public partial class WebForm2 : System.Web.UI.Page
	{	
		String DocNm;
		private void Page_Load(object sender, System.EventArgs e)
		{
		
		Int16 DocId =(Int16) Convert.ChangeType(Session["docid"].ToString(),typeof(Int16)) ;
		
		DocNm =Server.MapPath("") + "/temp/"+ Session.SessionID.ToString() + DocId.ToString() + ".pdf";
			
		
		Session["DocNm"] = DocNm.ToString();
		LoadPDF(DocNm);	
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Unload += new System.EventHandler(this.WebForm2_Unload);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void LoadPDF(string strFileName )
		{
			Response.ContentType = "Application/pdf";
			Response.WriteFile (strFileName);
			Response.End();
		}

		private void CleanPdf()
		{
			string[] dirs = Directory.GetFiles(@DocNm);
			foreach (string dir in dirs) 
			{
				if  (1 >= DateTime.Today.CompareTo(File.GetCreationTime(dir.ToString())))
				{
					File.Delete(dir.ToString());
				}
			}
		}


		private void WebForm2_Unload(object sender, System.EventArgs e)
		{
		//Response.Write("");
		}
	}
}

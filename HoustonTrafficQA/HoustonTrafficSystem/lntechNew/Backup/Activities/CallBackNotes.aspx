<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.CallBackNotes" Codebehind="CallBackNotes.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Call Back Notes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">
	       function checkGComments()
		{
		    //General Comments Check
			if (document.getElementById("txtComments").value.length > 2000-27) //-27 bcoz to show last time partconcatenated with comments
			{
				alert("You Cannot type more than 2000 characters ");
				document.getElementById("txtComments").focus();
				return false;
			}
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="350" align="center" border="0">
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<td colSpan="2">
						<TABLE class="clsLeftPaddingTable" id="tblsub" height="20" cellSpacing="1" cellPadding="0"
							width="350" align="center" border="0">
							<TR>
							</TR>
							<tr>
								<td class="clsaspcolumnheader" height="22">Name
								</td>
								<TD height="22"><asp:label id="lbl_name" runat="server" CssClass="label"></asp:label></TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" height="22">Telephone
								</td>
								<TD height="22"><asp:label id="lbl_telephone" runat="server" CssClass="label"></asp:label></TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" height="22">Appearance
								</td>
								<TD height="22"><asp:label id="lbl_appdate" runat="server" CssClass="label"></asp:label></TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader">Call Back Date
								</td>
								<TD>
									<ew:calendarpopup id="CallBackDate" runat="server" Width="90px" DisableTextboxEntry="False" ToolTip="Select Report Date Range"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
										ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" Nullable="True" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
							</tr>
							<tr>
								<TD class="clsaspcolumnheader">General Comments
								</TD>
								<TD><asp:textbox id="txtComments" runat="server" CssClass="clstextarea" Height="48px" Width="230px"
										TextMode="MultiLine" MaxLength="500"></asp:textbox></TD>
							</tr>
							<tr>
								<TD class="clsaspcolumnheader" height="22">Status</TD>
								<TD height="22"><asp:dropdownlist id="ddl_callstatus" runat="server" CssClass="clsinputcombo">
										<asp:ListItem Value="0" Selected="True">Pending</asp:ListItem>
										<asp:ListItem Value="1">Contacted</asp:ListItem>
										<asp:ListItem Value="2">Message</asp:ListItem>
									</asp:dropdownlist></TD>
							</tr>
							<TR>
								<TD align="right"></TD>
								<TD align="right"><asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Submit" OnClientClick="return checkGComments();"></asp:button></TD>
							</TR>
						</TABLE>
					</td>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>

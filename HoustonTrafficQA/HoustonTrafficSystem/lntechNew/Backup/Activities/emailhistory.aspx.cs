using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Configuration;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Activities
{
    /// <summary>
    /// Summary description for emailhistory.
    /// </summary>
    public partial class emailhistory : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label lbl_error;
        protected System.Web.UI.WebControls.HyperLink HyperLink1;
        protected System.Web.UI.WebControls.GridView gvresult;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.Label Label5;
        protected System.Web.UI.WebControls.Label Label6;
        protected System.Web.UI.WebControls.Button btn_send;
        bool isDone = false;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            this.gvresult.RowCommand += new System.Web.UI.WebControls.GridViewCommandEventHandler(this.gvresult_RowCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        //Yasir Kamal 6064 07/01/2009 allow Searching for past dates and Pagging control added.

        #region Declaration
        clsENationWebComponents ClsDB = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        clsSession ClsSession = new clsSession();
        string MessageCategory = string.Empty;
        #endregion

        #region Events
        private void Page_Load(object sender, System.EventArgs e)
        {

            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    lbl_error.Text = string.Empty;

                    if (!IsPostBack)
                    {
                        //    calFrom.SelectedDate = DateTime.Now.Date;
                        //    calTo.SelectedDate = DateTime.Now.Date;

                        //    FillGrid();
                        //Sabir Khan 6160 By default 50 records should be displayed in the grid...
                        Pagingctrl.Size = lntechNew.WebControls.RecordsPerPage.Fifty;
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gvresult;
                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        /// <summary>
        /// handling pageIndexChange  of pagging control
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gvresult.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// handling PageSizeChanged  of pagging control
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gvresult.PageIndex = 0;
                    gvresult.PageSize = pageSize;
                    gvresult.AllowPaging = true;
                }
                else
                    gvresult.AllowPaging = false;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void btn_send_Click(object sender, System.EventArgs e)
        {
            try
            {

                // Check If Resend Button Is Clicked Then Send Emails 


                foreach (GridViewRow item in gvresult.Rows)
                {

                    string[] Keys = { "@MsgId" };
                    CheckBox chk = (CheckBox)item.FindControl("chkbox_resend");
                    string messageID = ((Label)item.FindControl("lbl_id")).Text;
                    object[] values = { messageID };

                    if (chk.Checked)
                    {
                        // Check If CheckBox is Selected Then send Mails 
                        ClsDB.ExecuteSP("USP_HTS_resend_text_message  ", Keys, values);
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        protected void gvresult_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // NOufil 5884 07/14/2009 Variable added to stop event bubbling
            if (e.CommandName == "resend")
            {
                if (!isDone)
                {
                    resend();
                    isDone = true;
                }

            }
        }

        protected void gvresult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvresult.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        #endregion

        #region Methods
        /// <summary>
        ///  Getting Email List From Databse
        /// </summary>
        private void FillGrid()
        {
            try
            {
                MessageCategory = dd_sendsmsto.SelectedValue;
                ViewState["MessageCategory"] = dd_sendsmsto.SelectedValue;
                string[] keys = { "@startdate", "@enddate", "@messagecategory" };
                object[] values = { calFrom.SelectedDate, calTo.SelectedDate, dd_sendsmsto.SelectedValue };
                DataTable dt = ClsDB.Get_DT_BySPArr("USP_HTS_list_all_email_send", keys, values);

                if (dt.Rows.Count > 0)
                {

                    GenerateSerialNo(dt);
                    gvresult.DataSource = dt;
                }
                else
                {
                    lbl_error.Text = "No Record Found";
                }

                // Noufil 5884 07/03/2009 Chnage Header of Column according to selected message category
                if (dd_sendsmsto.SelectedValue == "0")
                    gvresult.Columns[2].HeaderText = "Subject";
                else
                    gvresult.Columns[2].HeaderText = "Alert Type";

                gvresult.DataBind();

                Pagingctrl.PageCount = gvresult.PageCount;
                Pagingctrl.PageIndex = gvresult.PageIndex;
                Pagingctrl.SetPageIndex();
            }

            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        /// <summary>
        /// Generating Serial Numbers
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (!dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void resend()
        {

            try
            {
                string[] srl = hf_isRecordCheck.Value.Split(',');

                if (Convert.ToString(ViewState["MessageCategory"]) == "0")
                {
                    // Noufil 5884 07/07/2009  Server side check box checked was not working sor eplace it with javascript logic.
                    foreach (string s in srl)
                    {
                        if (s.Trim() != "")
                        {
                            int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                            string[] Keys = { "@MsgId", "@empid" };
                            string messageID = ((Label)gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("lbl_id")).Text;
                            object[] values = { messageID, empid };
                            // Check If CheckBox is Selected Then send Mails 
                            ClsDB.ExecuteSP("USP_HTS_resend_text_message", Keys, values);
                        }
                    }
                }
                // Noufil 5884 07/07/2009  Clause added for SMS to client.
                else if (Convert.ToString(ViewState["MessageCategory"]) == "1")
                {
                    ClsSMS Clssms = new ClsSMS();

                    Clssms.UserName = Convert.ToString(ConfigurationManager.AppSettings["SmsServiceUserID"]);
                    Clssms.Password = Convert.ToString(ConfigurationManager.AppSettings["SmsServicePassword"]);
                    // Noufil 6177 08/16/2009 API new property use
                    Clssms.Api_id = Convert.ToString(ConfigurationManager.AppSettings["SmsServiceAPI_ID"]);

                    string response = Clssms.IsvalidAccount();

                    if (!response.Contains("Error"))
                    {
                        string Alertype, ClientName, courtdatetime, courtname, courtnumber, PhoneNumber = "";
                        string textMessage = string.Empty;
                        int ticketid = 0;

                        foreach (string s in srl)
                        {
                            if (s.Trim() != "")
                            {
                                // Rab Nawaz Khan 10914 06/28/2013 modified the Resend SMS logic. . . 
                                textMessage = ((Label)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("lbl_subject"))).Text;
                                Alertype = ((Label)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("lbl_client"))).Text;
                                ClientName = ((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_Client"))).Value;
                                // Noufil 6177 08/16/2009 Date format updated to MM/dd/yy
                                if (!Alertype.Equals("Invalid Response Text"))
                                {
                                    DateTime dt;
                                    if (
                                        DateTime.TryParse(
                                            ((HiddenField)
                                             (gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_CourtDate"))).Value,
                                            out dt))
                                    {
                                        courtdatetime =
                                            String.Format("{0:MM/dd/yy}",
                                                          Convert.ToDateTime(
                                                              ((HiddenField)
                                                               (gvresult.Rows[Convert.ToInt32(s) - 1].FindControl(
                                                                   "hf_CourtDate"))).Value)) + " @ " +
                                            String.Format("{0:hh:mm tt}",
                                                          Convert.ToDateTime(
                                                              ((HiddenField)
                                                               (gvresult.Rows[Convert.ToInt32(s) - 1].FindControl(
                                                                   "hf_CourtDate"))).Value));
                                    }
                                    else
                                    {
                                        courtdatetime = "";
                                    }
                                }
                                courtname = ((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_courtname"))).Value;
                                courtnumber = ((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_CourtNumber"))).Value;
                                PhoneNumber = ((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_phonenumber"))).Value;
                                ticketid = Convert.ToInt32(((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_ticketid"))).Value);

                                Clssms.MessageText = textMessage.Replace("<br />", "\r\n");

                                //"Sullo & Sullo Alert\r\nDear " + Convert.ToString(dr["firstname"]).Trim() + ", you have the following court setting: " + String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dr["courtdate"])) + " @ " + String.Format("{0:hh:mm tt}", Convert.ToDateTime(dr["courtdate"])) + " RM " + dr["courtroom"].ToString() + " @ " + dr["courtshortname"].ToString() + " Court.";
                                //string [] sms = System.Text.RegularExpressions.Regex.Split(textMessage, ":");
                                //if (sms.Length > 1)
                                //{
                                //    textMessage = sms[0] + "\n";
                                //}
                                //if (Alertype == "Set Call")
                                // Noufil 6177 08/16/2009 message body Update
                                //Ozair 6472 08/27/2009 colon added
                                // Noufil 7149 02/26/2009 Rename RM to Court Room#
                                //Clssms.MessageText = "Sullo & Sullo Alert:\r\nDear " + ClientName.Trim().Substring(0, ClientName.IndexOf(" ")).Trim() + ", you have the following court setting: " + courtdatetime + " Court Room: " + courtnumber + " @ " + courtname + ".";
                                //if (Clssms.MessageText.Length < 128)
                                //{
                                //    Clssms.MessageText += " Please arrive 15 minutes early.";
                                //}
                                //else
                                //    Clssms.MessageText = "Sullo $ Sullo Alert\nDear " + ClientName + ",\nYour court date is set for " + courtdatetime + " at " + courtname + " in room number " + courtnumber + ". Please arrive 15 minutes before court time.";

                                if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsKey"]).Trim() == "ON")
                                {
                                    Clssms.PhoneNumber = Convert.ToString(ConfigurationManager.AppSettings["TestPhoneNumbertoSMS"]).Trim();
                                    //Clssms.MessageText = TxtMessage.Text;
                                    if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                                        Clssms.SendSms(response);
                                    lbl_error.Text = "SMS Successfully send to Test Mobile Number.";
                                }
                                else
                                {
                                    // Clssms.PhoneNumber = (PhoneNumber.Contains("+")) ? PhoneNumber : "1" + PhoneNumber;
                                    // Noufil 6177 08/16/2009 Remove 1 from Phone number 
                                    Clssms.PhoneNumber = PhoneNumber;
                                    // Rab Nawaz Khan 10914 06/28/2013 Getting SMS type. . . 
                                    string notes = string.Empty;
                                    if (Alertype.Equals("Invalid Response Text"))
                                        notes = "Invalid Response Text resent to client via SMS on " + Clssms.PhoneNumber;
                                    else if (Alertype.Equals("Manual"))
                                        notes = "Court setting information resent to client via SMS on " + Clssms.PhoneNumber;
                                    else if (Alertype.Equals("Reminder Text"))
                                        notes = "Court Date Reminder Text resent to client via SMS on " + Clssms.PhoneNumber;
                                    else 
                                        notes = "Court Date Reset Text resent to client via SMS on " + Clssms.PhoneNumber;
                                    
                                    if (Convert.ToString(ConfigurationManager.AppSettings["TestSmsSendKey"]).Trim() == "ON")
                                        Clssms.SendSms(response);
                                    Clssms.MessageText = textMessage;
                                    Clssms.AddNoteToHistory(ticketid, notes, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)));
                                    Clssms.InsertHistory(ClientName, Clssms.MessageText, ticketid, Convert.ToDateTime(((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_CourtDate"))).Value), courtnumber, Clssms.PhoneNumber, Alertype, ((HiddenField)(gvresult.Rows[Convert.ToInt32(s) - 1].FindControl("hf_TicketNumber"))).Value, Convert.ToString(ClsSession.GetCookie("sEmpID", this.Request)));
                                }
                            }
                        }
                        FillGrid();
                    }
                    else
                        lbl_error.Text = "API Account not verified.";
                }
            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

    }
}
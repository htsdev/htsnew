using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace HTP.Activities
{
    // Noufil 5618 04/02/2009 Code refactor According to requirement.

    public partial class PaymentPlans : System.Web.UI.Page
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();
        lntechNew.Components.PaymentPlan plan = new lntechNew.Components.PaymentPlan();
        clsSession cSession = new clsSession();
        NewClsPayments ClsPayments = new NewClsPayments();
        DataTable dslan = new DataTable();
        DataView Dv;
        double amount;
        DataTable tempdt;

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Pagingctrl.Visible = true;
                if (!IsPostBack)
                {
                    tempdt = new DataTable();
                    tbl_paymentdetail.Style["display"] = "none";
                    tbl_paymplan.Style["display"] = "none";
                    //tbl_paymentdetail.Visible = false;
                    //tbl_paymplan.Visible = false;
                    //tbl_generalcomments.Visible = false;
                    tbl_generalcomments.Style["display"] = "none";
                    ViewState["empid"] = cSession.GetCookie("sEmpID", this.Request);
                    sdate.SelectedDate = DateTime.Today;
                    edate.SelectedDate = DateTime.Today;
                    //wctl_GeneralComments.btn_AddComments.Visible = false;
                    //wctl_GeneralComments.Button_Visible = false;
                    cal_followup.LowerBoundDate = DateTime.Today;
                    cal_followup.SelectedDate = DateTime.Today;
                    wctl_GeneralComments.btn_AddComments.Visible = false;
                    wctl_GeneralComments.Button_Visible = false;
                    //Nasir 6049 07/11/2009 Page size control
                    Pagingctrl.Size = lntechNew.WebControls.RecordsPerPage.Thirty;

                }
                //Nasir 6049 07/11/2009 Page size control
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_paymentplan;
                wctl_GeneralComments.btn_AddComments.Attributes.Add("onclick", " return ValidateCommentControls();");
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_paymentdetail.Style["display"] = "none";
                tbl_paymplan.Style["display"] = "none";
                //tbl_paymentdetail.Visible = false;
                //tbl_paymplan.Visible = false;
                //tbl_generalcomments.Visible = false;
                tbl_generalcomments.Style["display"] = "none";
                // Noufil 6014 08/11/2009 Variable add to reset pageindex 
                ViewState["Currentpage"] = "0";
                BindGrid(true, true);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_paymentplan_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            try
            {
                //Dv = (DataView)Session["Dv_Payment"];

                //if (e.SortExpression != null)
                //{
                //    if (Session["sortexpression"] == null)
                //    {
                //        Session["sortexpression"] = "ASC";
                //    }

                //    Dv.Sort = e.SortExpression + ' ' + Session["sortexpression"].ToString();
                //    gv_paymentplan.DataSource = Dv;

                //    if (Session["sortexpression"] == "ASC")
                //        Session["sortexpression"] = "DESC";
                //    else
                //        Session["sortexpression"] = "ASC";

                //}
                //else
                //    gv_paymentplan.DataSource = Dv;

                //gv_paymentplan.DataBind();                
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                wctl_GeneralComments.AddComments();
                //if (td_followcal.Style["display"] == "block")

                if (hf_showhide.Value == "1")
                    plan.UpdatePaymentFollowUpdate(Convert.ToInt32(ViewState["ticketid"]), cal_followup.SelectedDate, Convert.ToInt32(ViewState["empid"]));

                // Noufil 6295 11/10/2009 if Session doesnt contains data then get data from Database.                
                if (Session["tempdt"] == null)
                {
                    BindGrid(false, true);
                }

                // Noufil 5830 06/01/2009 dont refresh the grid
                DataTable dt = (DataTable)Session["tempdt"];

                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr["TicketID_PK"]) == Convert.ToInt32(ViewState["ticketid"]))
                    {
                        dr["PaymentFollowUpDate"] = cal_followup.SelectedDate.ToString("MM/dd/yyyy");
                    }
                }

                Session["tempdt"] = dt;
                BindGrid(false, false);

                foreach (GridViewRow gvrow in gv_paymentplan.Rows)
                {

                    if (Convert.ToInt32(((Label)(gvrow.FindControl("lbl_ticketid"))).Text) == Convert.ToInt32(ViewState["ticketid"]))
                    {
                        gv_paymentplan.Rows[gvrow.RowIndex].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF2D9");
                    }
                }

                // Noufil 5830 06/01/2009 dont hide the payment detail on update button.
                //cal_followup.Visible = false;
                //td_followcal.Style["display"] = "none";
                //cal_followup.SelectedDate = DateTime.Today;
                //cal_followup.LowerBoundDate = DateTime.Today;
                lbl_Paymentfollowupdate.Visible = true;
                //tbl_generalcomments.Visible = false;
                tbl_generalcomments.Style["display"] = "block";
                lbl_Paymentfollowupdate.Text = cal_followup.SelectedDate.ToShortDateString();
                //tbl_paymentdetail.Visible = false;
                //tbl_paymplan.Visible = false;
                //tbl_paymentdetail.Style["display"] = "none";
                //tbl_paymplan.Style["display"] = "none";
                //Nasir 6098 08/24/2009 hide is compaint check box after update
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideCheckbox", "HideCheckBox();", true);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        //Nasir 6049 07/15/2009 remove Row databound event

        protected void gv_paymentplan_PageIndexChanged(object sender, EventArgs e)
        {

        }

        //Nasir 6049 07/11/2009 Page Size control
        /// <summary>
        /// Page Size control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_paymentplan_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_paymentplan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_paymentplan.PageIndex = e.NewPageIndex;
                    // Noufil 6014 08/11/2009 Variable add to reset pageindex 
                    ViewState["Currentpage"] = "1";
                    BindGrid(true, false);
                    //tbl_paymentdetail.Visible = false;
                    //tbl_paymplan.Visible = false;
                    tbl_paymentdetail.Style["display"] = "none";
                    tbl_paymplan.Style["display"] = "none";
                    //tbl_generalcomments.Visible = false;
                    tbl_generalcomments.Style["display"] = "none";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_paymentplan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //Nasir 5816 04/28/2009 increase the scope of command payment detail 
                if (e.CommandName == "paymentdetail")
                {
                    ViewState["ticketid"] = Convert.ToInt32(e.CommandArgument);
                    BindPaymentDetails(Convert.ToInt32(e.CommandArgument));

                    BindPaymentPlan(Convert.ToInt32(e.CommandArgument)); // Payment Plans
                    //cal_followup.Visible = false;
                    lbl_Paymentfollowupdate.Visible = true;
                    Pagingctrl.Visible = true;
                    Pagingctrl.PageCount = Convert.ToInt32(ViewState["GridPageCount"]);

                    foreach (GridViewRow gvrow in gv_paymentplan.Rows)
                    {
                        if (Convert.ToInt32(((Label)(gvrow.FindControl("lbl_ticketid"))).Text) == Convert.ToInt32(e.CommandArgument))
                        {
                            gv_paymentplan.Rows[gvrow.RowIndex].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF2D9");
                        }
                        else
                            gv_paymentplan.Rows[gvrow.RowIndex].BackColor = System.Drawing.ColorTranslator.FromHtml("#EFF4FB");
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void GV_paymplan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Footer))
                {
                    Label lblamount = (Label)e.Row.FindControl("lbl_amount");
                    if (lblamount.Text != "")
                    {
                        amount += Convert.ToDouble(lblamount.Text);
                        if (Convert.ToDouble(lbl_owes.Text) > 0)
                        {
                            lbl_plan.Text = Convert.ToInt32(amount).ToString();
                            lbl_defrence.Text = Convert.ToInt32((Convert.ToDouble(lbl_owes.Text) - amount)).ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void ImgBtn_Followup_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (td_followcal.Style["display"] == "none")
                {
                    lbl_Paymentfollowupdate.Visible = false;
                    //cal_followup.Visible = true;
                    td_followcal.Style["display"] = "block";
                }
                else
                {
                    lbl_Paymentfollowupdate.Visible = true;
                    td_followcal.Style["display"] = "none";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnkbtn_Next_Click(object sender, EventArgs e)
        {
            try
            {
                int index = -1;
                int ticketid = Convert.ToInt32(((HiddenField)(GV_paymentdetail.Rows[0].FindControl("hf_ticketid"))).Value);

                foreach (GridViewRow gvrow in gv_paymentplan.Rows)
                {

                    if (Convert.ToInt32(((Label)(gvrow.FindControl("lbl_ticketid"))).Text) == ticketid)
                    {
                        index = gvrow.RowIndex;
                    }
                }

                if (index == (Convert.ToInt32(ViewState["RowsCountperpage"]) - 1))
                {

                    if (gv_paymentplan.PageIndex != Convert.ToInt32(ViewState["GridPageCount"]) - 1)
                    {
                        gv_paymentplan.PageIndex = gv_paymentplan.PageIndex + 1;
                    }
                    else
                        gv_paymentplan.PageIndex = 0;
                    BindGrid(true, false);
                    index = 0;
                }
                else
                    index = index + 1;

                gv_paymentplan.Rows[index].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF2D9");
                if (index != 0)
                    gv_paymentplan.Rows[index - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#EFF4FB");
                ticketid = Convert.ToInt32(((Label)(gv_paymentplan.Rows[index].FindControl("lbl_ticketid"))).Text);
                BindPaymentDetails(ticketid);
                BindPaymentPlan(ticketid);
                Pagingctrl.Visible = true;
                Pagingctrl.PageCount = Convert.ToInt32(ViewState["GridPageCount"]);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnkbtn_Last_Click(object sender, EventArgs e)
        {
            try
            {
                gv_paymentplan.PageIndex = Convert.ToInt32(ViewState["GridPageCount"]) - 1;
                BindGrid(true, false);
                gv_paymentplan.Rows[Convert.ToInt32(ViewState["RowsCountperpage"]) - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF2D9");
                BindPaymentDetails(Convert.ToInt32(((Label)(gv_paymentplan.Rows[Convert.ToInt32(ViewState["RowsCountperpage"]) - 1].FindControl("lbl_ticketid"))).Text));
                BindPaymentPlan(Convert.ToInt32(((Label)(gv_paymentplan.Rows[Convert.ToInt32(ViewState["RowsCountperpage"]) - 1].FindControl("lbl_ticketid"))).Text));
                Pagingctrl.Visible = true;
                Pagingctrl.PageCount = Convert.ToInt32(ViewState["GridPageCount"]);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnkbtn_Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int index = -1;
                int ticketid = Convert.ToInt32(((HiddenField)(GV_paymentdetail.Rows[0].FindControl("hf_ticketid"))).Value);

                foreach (GridViewRow gvrow in gv_paymentplan.Rows)
                {
                    if (Convert.ToInt32(((Label)(gvrow.FindControl("lbl_ticketid"))).Text) == ticketid)
                    {
                        index = gvrow.RowIndex;
                    }
                }

                if (index == 0)
                {
                    if (gv_paymentplan.PageIndex != 0)
                    {
                        gv_paymentplan.PageIndex = gv_paymentplan.PageIndex - 1;
                    }
                    else
                        gv_paymentplan.PageIndex = Convert.ToInt32(ViewState["GridPageCount"]) - 1;
                    BindGrid(true, false);
                    index = Convert.ToInt32(ViewState["RowsCountperpage"]) - 1;
                }
                else
                    index = index - 1;

                gv_paymentplan.Rows[index].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF2D9");
                if (index != Convert.ToInt32(ViewState["RowsCountperpage"]) - 1)
                    gv_paymentplan.Rows[index + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#EFF4FB");
                ticketid = Convert.ToInt32(((Label)(gv_paymentplan.Rows[index].FindControl("lbl_ticketid"))).Text);
                BindPaymentDetails(ticketid);
                BindPaymentPlan(ticketid);
                Pagingctrl.Visible = true;
                Pagingctrl.PageCount = Convert.ToInt32(ViewState["GridPageCount"]);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void lnkbtn_First_Click(object sender, EventArgs e)
        {
            try
            {
                gv_paymentplan.PageIndex = 0;
                BindGrid(true, false);
                gv_paymentplan.Rows[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF2D9");
                BindPaymentDetails(Convert.ToInt32(((Label)(gv_paymentplan.Rows[0].FindControl("lbl_ticketid"))).Text));
                BindPaymentPlan(Convert.ToInt32(((Label)(gv_paymentplan.Rows[0].FindControl("lbl_ticketid"))).Text));
                Pagingctrl.Visible = true;
                Pagingctrl.PageCount = Convert.ToInt32(ViewState["GridPageCount"]);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        private void BindPaymentPlan(int ticketid)
        {
            plan.TicketID = ticketid;
            DataTable dt_plan = plan.GetPPlan().Tables[0];

            if (dt_plan != null)
            {
                if (dt_plan.Rows.Count > 0)
                {
                    Pagingctrl.Visible = true;
                    //tbl_paymplan.Visible = true;                    
                    tbl_paymplan.Style["display"] = "block";
                    //int owes = Convert.ToInt32(dt_plan.Rows[0]["Owes"]);
                    //lbl_owes.Text = owes.ToString();
                    lbl_owes.Text = Convert.ToString(Convert.ToInt32(dt_plan.Rows[0]["Owes"]));
                    lbl_Paymentfollowupdate.Text = Convert.ToString(dt_plan.Rows[0]["PaymentFollowUpDate"]);
                    cal_followup.SelectedDate = (string.IsNullOrEmpty(lbl_Paymentfollowupdate.Text) ? DateTime.Today : Convert.ToDateTime(lbl_Paymentfollowupdate.Text));
                    GV_paymplan.DataSource = dt_plan;
                    GV_paymplan.DataBind();
                }
                else
                {
                    ClsPayments.GetPaymentInfo(ticketid);
                    lbl_owes.Text = ClsPayments.Owes.ToString();
                    lbl_plan.Text = "0";
                    lbl_defrence.Text = ClsPayments.Owes.ToString();
                    lbl_Paymentfollowupdate.Text = ClsPayments.PaymentFollowUpdate;
                    cal_followup.SelectedDate = (string.IsNullOrEmpty(lbl_Paymentfollowupdate.Text) ? DateTime.Today : Convert.ToDateTime(lbl_Paymentfollowupdate.Text));
                    tbl_paymplan.Style["display"] = "block";
                    GV_paymplan.DataSource = null;
                    GV_paymplan.DataBind();
                }
            }
        }

        private void BindPaymentDetails(int TicketID)
        {
            int empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
            plan.TicketID = TicketID;
            lbl_clientname.Text = "";

            DataTable dt_plandetail = plan.GetPaymentDetail().Tables[0];

            if (dt_plandetail.Rows.Count > 0)
            {
                Pagingctrl.Visible = true;
                //tbl_paymentdetail.Visible = true;
                tbl_paymentdetail.Style["display"] = "block";
                lbl_clientname.Text = dt_plandetail.Rows[0]["clientname"].ToString();
                lblContact1.Text = dt_plandetail.Rows[0]["contact1"].ToString();
                lblContact2.Text = dt_plandetail.Rows[0]["contact2"].ToString();
                lblContact3.Text = dt_plandetail.Rows[0]["contact3"].ToString();

                //tbl_generalcomments.Visible = false;                
                GV_paymentdetail.DataSource = dt_plandetail;
                GV_paymentdetail.DataBind();
                //Nasir 6098 08/21/2009 general comment initialize overload method
                wctl_GeneralComments.Initialize(TicketID, empid, 1, "Label", "clsinputadministration", "clsbutton", "Label");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideCheckbox", "HideCheckBox();", true);
                tbl_generalcomments.Style["display"] = "block";
                //tbl_generalcomments.Visible = true;
                wctl_GeneralComments.btn_AddComments.Visible = false;
                wctl_GeneralComments.Button_Visible = false;
            }
            else
            {
                //tbl_paymentdetail.Visible = false;
                //tbl_paymentdetail.Style["display"] = "none";
                //tbl_generalcomments.Style["display"] = "none";
                //GV_paymentdetail.Visible = false;
                GV_paymentdetail.DataSource = null;
                GV_paymentdetail.DataBind();
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_paymentplan.PageIndex = Pagingctrl.PageIndex - 1;
            // Noufil 6014 08/11/2009 Variable add to reset pageindex 
            ViewState["Currentpage"] = "1";
            BindGrid(true, false);
        }

        private void BindGrid(bool showpaymentdetails, bool showupdatedrecords)
        {
            try
            {
                /// set parameter values
                //tbl_generalcomments.Visible = false;
                tbl_generalcomments.Style["display"] = "none";
                lblMessage.Text = "";
                plan.DsDate = sdate.SelectedDate;
                plan.DeDate = edate.SelectedDate;
                plan.CourtType = Convert.ToInt32(ddlCourtLocation.SelectedValue);
                plan.Criteria = Convert.ToInt32(ddl_criteria.SelectedValue);
                if (chk_pplan.Checked)
                {
                    plan.CHK = 1;
                }
                else
                {
                    plan.CHK = 0;
                }

                if (showupdatedrecords)
                {
                    dslan = plan.GetPaymentPlans().Tables[0];
                    Session["tempdt"] = dslan;
                    //Nasir 6049 07/11/2009 Page Size control
                    Session["Dv_Payment"] = dslan.DefaultView;
                }
                else
                {
                    //dslan = null;
                    //dslan = new DataSet();
                    //tempdt = (DataTable)Session["tempdt"];
                    dslan = ((DataTable)Session["tempdt"]);
                    //dslan = tempdt;
                }

                //Nasir 6049 07/11/2009 Page Size control
                if (Session["Dv_Payment"] == null)
                {
                    Dv = new DataView(dslan);
                    Session["Dv_Payment"] = Dv;
                }
                else
                {
                    Dv = (DataView)Session["Dv_Payment"];
                    string SortExp = Dv.Sort;
                    Dv = new DataView(dslan);
                    Dv.Sort = SortExp;
                    dslan = Dv.ToTable();
                    if (dslan.Columns.Contains("sno"))
                    {
                        dslan.Columns.Remove("sno");
                    }
                }

                if (dslan != null)
                {
                    if (dslan.Rows.Count > 0)
                    {
                        Pagingctrl.Visible = true;
                        if (dslan.Columns.Contains("sno") == false)
                        {
                            dslan.Columns.Add("sno");
                            int sno = 1;
                            if (dslan.Rows.Count >= 1)
                                dslan.Rows[0]["sno"] = 1;
                            if (dslan.Rows.Count >= 2)
                            {
                                for (int i = 1; i < dslan.Rows.Count; i++)
                                {
                                    if (dslan.Rows[i - 1]["TicketID_PK"].ToString() != dslan.Rows[i]["TicketID_PK"].ToString())
                                    {
                                        dslan.Rows[i]["sno"] = ++sno;
                                    }
                                }
                            }
                        }


                        //Nasir 6049 07/16/2009 sorting
                        Session["Dv_Payment"] = dslan.DefaultView;
                        gv_paymentplan.Visible = true;
                        //Nasir 6049 07/11/2009 Page Size control
                        gv_paymentplan.DataSource = dslan.DefaultView;
                        //Nasir 6049 07/11/2009 Adjust column for New Column Crt Type also changed Grid Header Text
                        if (ddl_criteria.SelectedValue == "1")
                        {
                            gv_paymentplan.Columns[9].HeaderText = "<u>Days Left</u>";
                        }
                        else
                        {
                            gv_paymentplan.Columns[9].HeaderText = "<u>Past Days</u>";
                        }

                        // Noufil 6014 08/11/2009 Variable add to reset pageindex 
                        if (Convert.ToString(ViewState["Currentpage"]) == "0")
                            gv_paymentplan.PageIndex = 0;

                        gv_paymentplan.DataBind();

                        ViewState["GridPageCount"] = gv_paymentplan.PageCount;
                        ViewState["GridPageIndex"] = gv_paymentplan.PageIndex;

                        Pagingctrl.PageCount = gv_paymentplan.PageCount;
                        Pagingctrl.PageIndex = gv_paymentplan.PageIndex;
                        Pagingctrl.SetPageIndex();
                        ViewState["RowsCountperpage"] = gv_paymentplan.Rows.Count;
                    }
                    else
                    {
                        //gv_paymentplan.DataSource = null;
                        //gv_paymentplan.DataBind();
                        lblMessage.Text = "No Record found.";
                        gv_paymentplan.Visible = false;
                        Pagingctrl.Visible = false;
                    }
                }

                if (showpaymentdetails)
                {
                    //tbl_generalcomments.Visible = false;
                    tbl_generalcomments.Style["display"] = "none";
                    wctl_GeneralComments.Button_Visible = false;
                    wctl_GeneralComments.btn_AddComments.Visible = false;
                    //tbl_paymentdetail.Visible = false;
                    tbl_paymentdetail.Style["display"] = "none";
                    //tbl_paymplan.Visible = false;                
                    tbl_paymplan.Style["display"] = "none";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }
        //Nasir 6049 07/11/2009 Page size change
        /// <summary>
        /// Page size change
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_paymentplan.PageIndex = 0;
                gv_paymentplan.PageSize = pageSize;
                gv_paymentplan.AllowPaging = true;
            }
            else
            {
                gv_paymentplan.AllowPaging = false;
            }
            // Noufil 6014 08/11/2009 Variable add to reset pageindex 
            ViewState["Currentpage"] = "1";
            BindGrid(true, false);
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {

            Dv = (DataView)Session["Dv_Payment"];
            Dv.Sort = sortExpression + " " + direction;
            Session["Dv_Payment"] = Dv;
            // Noufil 6014 08/11/2009 Variable add to reset pageindex 
            ViewState["Currentpage"] = "1";
            BindGrid(true, false);
            Pagingctrl.PageCount = gv_paymentplan.PageCount;
            Pagingctrl.PageIndex = gv_paymentplan.PageIndex;

            ViewState["sortExpression"] = sortExpression;
            Session["SortDirection"] = direction;
            Session["SortExpression"] = sortExpression;

        }
        #endregion


    }
}
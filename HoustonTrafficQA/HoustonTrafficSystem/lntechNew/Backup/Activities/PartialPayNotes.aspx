<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="HTP.Activities.PartialPayNotes" Codebehind="PartialPayNotes.aspx.cs" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Partial Pay Notes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script src="../Scripts/jsDate.js" type="text/javascript"></script>
		<script>
		function Validate()
		{
		
			var date =document.getElementById("PayDueDate").value;		
			var today;
			var expiry;
			if (!MMDDYYYYDate(date))
			{				
				return false;
			}
			else
			{				
						today = new Date();
						expiry = new Date(date);
						if (today.getTime() >= expiry.getTime())
						{
						alert("Please select future Date");
						return false;					
						}
			}
			
						
		}
	   
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="350" align="center" border="0">
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<td colSpan="2">
						<TABLE class="clsLeftPaddingTable" id="tblsub" height="20" cellSpacing="1" cellPadding="0"
							width="350" align="center" border="1" borderColor="#ffffff">
							<TR>
							</TR>
							<tr>
								<td class="clsaspcolumnheader" height="22" style="width: 116px">
                                    Name:
								</td>
								<TD height="22"><asp:label id="lbl_name" runat="server" CssClass="label"></asp:label></TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" height="49" style="HEIGHT: 49px; width: 116px;">
                                    Telephone:</td>
								<TD height="49" style="HEIGHT: 49px">
									<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD><asp:label id="lbl_telephone1" runat="server" CssClass="label"></asp:label></TD>
										</TR>
										<TR>
											<TD>
												<asp:label id="lbl_telephone2" runat="server" CssClass="label"></asp:label></TD>
										</TR>
										<TR>
											<TD>
												<asp:label id="lbl_telephone3" runat="server" CssClass="label"></asp:label></TD>
										</TR>
									</TABLE>
									&nbsp;
								</TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" height="22" style="width: 116px">
                                    Appearance:</td>
								<TD height="22"><asp:label id="lbl_appdate" runat="server" CssClass="label"></asp:label></TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" style="HEIGHT: 21px; width: 116px;" height="21">Owed Amount($):</td>
								<TD style="HEIGHT: 21px" height="21">
									<asp:label id="lbl_oweamount" runat="server" CssClass="label"></asp:label></TD>
							</tr>
							<TR>
								<TD class="clsaspcolumnheader" style="width: 116px">Due&nbsp;Date:</TD>
								<TD>
									<ew:calendarpopup id="PayDueDate" runat="server" Width="90px" DisableTextboxEntry="False" ToolTip="Select Report Date Range"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
										ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" Nullable="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
							</TR>
							<TR>
								<TD class="clsaspcolumnheader" style="width: 116px">
									<P>
                                        General Comments:&nbsp;</P>
								</TD>
								<TD>
                                    <cc2:wctl_comments id="WCC_GeneralComments" runat="server" height="50px" width="300px"></cc2:wctl_comments>
                                </TD>
							</TR>
							<tr>
								<TD class="clsaspcolumnheader" height="22" style="width: 116px"></TD>
								<TD height="22" vAlign="top" align="center"><asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
							</tr>
						</TABLE>
					</td>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

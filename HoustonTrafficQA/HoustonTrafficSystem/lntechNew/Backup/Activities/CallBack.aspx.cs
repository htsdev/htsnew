using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for CallBack.
	/// </summary>
	public partial class CallBack : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btn_update1;
		protected System.Web.UI.WebControls.DropDownList ddl_emplist;
		protected System.Web.UI.WebControls.DropDownList ddl_callstatus;
		protected System.Web.UI.WebControls.DataGrid dg_callback;
		protected eWorld.UI.CalendarPopup AppSDate;
		protected eWorld.UI.CalendarPopup AppEDate;
		protected eWorld.UI.CalendarPopup ContactSDate;
		protected eWorld.UI.CalendarPopup ContactEDate;
		clsENationWebComponents clsdb = new clsENationWebComponents();
		clsSession cSession = new clsSession();
		clsLogger clog = new clsLogger();
		int Userid=0;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.Label lblPNo;
		string Callstatus="0";
		string StrExp="" ;
		string StrAcsDec="" ;		
		string SortNm="" ;
		DataView Dv;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{

				if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					
				
					//Lower Bound Date				
					//AppSDate.LowerBoundDate=DateTime.Now.Date;
					//AppEDate.LowerBoundDate=DateTime.Now.Date;
					if(!IsPostBack)
					{
						DataSet ds_emp= clsdb.Get_DS_BySP("USP_HTS_Get_QCB_Users");
						ddl_emplist.DataSource=ds_emp;
						ddl_emplist.DataTextField= ds_emp.Tables[0].Columns[1].ColumnName;
						ddl_emplist.DataValueField= ds_emp.Tables[0].Columns[0].ColumnName;
						ddl_emplist.DataBind();
						ddl_emplist.Items.Insert(0,"--Choose--");
						ddl_emplist.Items[0].Value="0";
						ddl_emplist.SelectedValue="0";				
						//Intializing 							
						//AppSDate.SelectedDate=DateTime.Now.Date;
						//AppEDate.SelectedDate=DateTime.Now.Date;
						//ContactSDate.SelectedDate=DateTime.Now.Date;
						//ContactEDate.SelectedDate=DateTime.Now.Date;
						SetDefaultDates();
                       
						FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,Callstatus);                
					}
					lblMessage.Text="";
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}


		private void SetDefaultDates()
		{

			if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
			{
				AppSDate.SelectedDate = DateTime.Now.AddDays(2);
				AppEDate.SelectedDate = DateTime.Now.AddDays(3);
			}
			else
			{
				AppSDate.SelectedDate = DateTime.Now.AddDays(1);
				AppEDate.SelectedDate = DateTime.Now.AddDays(2);
			}

			ContactSDate.SelectedDate = DateTime.Now.AddYears(-1);

			if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
				ContactEDate.SelectedDate = DateTime.Now.AddDays(-2);
			else
				ContactEDate.SelectedDate = DateTime.Now.AddDays(-1);


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddl_emplist.SelectedIndexChanged += new System.EventHandler(this.ddl_emplist_SelectedIndexChanged);
			this.ddl_callstatus.SelectedIndexChanged += new System.EventHandler(this.ddl_callstatus_SelectedIndexChanged);
			this.btn_update1.Click += new System.EventHandler(this.btn_update1_Click);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
			this.dg_callback.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_callback_PageIndexChanged);
			this.dg_callback.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_callback_SortCommand);
			this.dg_callback.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_callback_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		//Method To Fill Grid
		private void FillGrid(DateTime AppSDate,DateTime AppEDate, DateTime ContactSDate,DateTime ContactEDate,int UserID,string CallStatus)
		{
			//Appearance Date should not be less than todays date
		//	if (AppSDate.Date<DateTime.Now.Date || AppEDate.Date<DateTime.Now.Date)
		//	{	
		//			lblMessage.Text="Appearance Date should be greater or equal to Todays Date.";
		//	}

			//In order to have search criteria on Date
			int intdateorder=0;
			//In order to check  blank date inputed
			DateTime dt=Convert.ToDateTime("1/1/0001"); 

			if( ContactEDate.Date!= dt.Date && AppEDate.Date == dt.Date)
			{
					intdateorder = 1;
			}
			if (ContactEDate.Date== dt.Date && AppEDate.Date != dt.Date)
			{
				 intdateorder = 2;
			}
			if( ContactEDate.Date != dt.Date && AppEDate.Date != dt.Date)
			{
				intdateorder = 3;
			}
			//in order to avoid date overflow
			if (intdateorder==0)
			{
				AppEDate=Convert.ToDateTime("01/01/1900");
				AppSDate=Convert.ToDateTime("01/01/1900");
				ContactEDate=Convert.ToDateTime("01/01/1900");
				ContactSDate=Convert.ToDateTime("01/01/1900");
			}

			if (intdateorder==1)
			{
				AppEDate=Convert.ToDateTime("01/01/1900");
				AppSDate=Convert.ToDateTime("01/01/1900");
			}
			if (intdateorder==2)
			{
				ContactEDate=Convert.ToDateTime("01/01/1900");
				ContactSDate=Convert.ToDateTime("01/01/1900");
			}

			string[] key1  = {"@stCdate","@edCdate","@stDate","@edDate","@casestatus","@userID","@dateOrder"};
			object[] value1= {AppSDate,AppEDate,ContactSDate,ContactEDate,CallStatus,UserID,intdateorder}; 
			DataSet ds_callback= clsdb.Get_DS_BySPArr("USP_HTS_GET_call_back_report",key1,value1);
			int a =ds_callback.Tables[0].Rows.Count;
			Dv = new DataView(ds_callback.Tables[0]);
			dg_callback.DataSource=Dv;
			Session["dv"]= Dv;
			dg_callback.DataBind();
			if(dg_callback.Items.Count<=0)
			{
				lblMessage.Visible=true;
				lblMessage.Text="No Record Found";
			}
			
			BindReport();
			FillPageList();
			SetNavigation();

		}
		//Method To Generate Serial Number
		private void BindReport()
		{
			long sNo=(dg_callback.CurrentPageIndex)*(dg_callback.PageSize);									
			try
			{
				foreach (DataGridItem ItemX in dg_callback.Items) 
				{ 					
					sNo +=1 ;
      
					((HyperLink)(ItemX.FindControl("hlnk_Sno"))).Text= sNo.ToString();
			//		()
			//		((LinkButton)ItemX.FindControl("lnkb_clientname")).Attributes.Add("OnClick","return OpenEditWin(" +  + ");"); 
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		//Event Fired When Employee Name is Selected 
		private void ddl_emplist_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg_callback.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);			
			FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
		}

		//Event fired when call status is selected
		private void ddl_callstatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg_callback.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
		}

		//Event Fired When Search Button is clicked
		private void btn_update1_Click(object sender, System.EventArgs e)
		{
			dg_callback.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			string con=ContactSDate.Text;
			string con2= ContactEDate.Text;
			FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
		}

		private void dg_callback_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DataRowView drv = (DataRowView) e.Item.DataItem;
			if (drv == null)
				return;

			try
			{
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					//In order to redirect to violation fees page
                   
                    ((HyperLink) e.Item.FindControl("hlnk_Sno")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=1&casenumber="+ (int) drv["TicketID_PK"];

					//Assigning call status based on status id
					string callstatusid=((Label)(e.Item.FindControl("lbl_statusid"))).Text;
					Label callstatus=(Label)(e.Item.FindControl("lbl_status"));
					switch (callstatusid)
					{
						case "0":
							callstatus.Text ="Pending";
							break;
						case "1":
							callstatus.Text="Contacted";
							break;
						case "2":
							callstatus.Text="Message";
							break;
					}
					//formating Date
					string crtdate=((Label)(e.Item.FindControl("lbl_contactdate"))).Text;
					Label lbl_crtdate=(Label)(e.Item.FindControl("lbl_contactdate"));
					lbl_crtdate.Text= crtdate.Substring(0,crtdate.IndexOf(" "));

					string duedate=((Label)(e.Item.FindControl("lbl_appeardate"))).Text;
					Label lbl_duedate=(Label)(e.Item.FindControl("lbl_appeardate"));
					lbl_duedate.Text= duedate.Substring(0,duedate.IndexOf(" "));
				
					//Mouse Hover		
					e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFF2D9'");
		
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#EFF4FB'");		
					//Open Comments Popup				
					((LinkButton)e.Item.FindControl("lnkb_clientname")).Attributes.Add("OnClick","return OpenEditWin(" +(int)drv["TicketID_PK"]+ ");"); 
				}
			}
			catch(Exception ex )
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}
		#region Navigation logic
		//----------------------------------Navigational LOGIC-----------------------------------------//
		
		// Procedure for filling page numbers in page number combo........
		private void FillPageList()
		{
			Int16 idx;

			cmbPageNo.Items.Clear();
			for (idx=1; idx<=dg_callback.PageCount;idx++)
			{
				cmbPageNo.Items.Add ("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value=idx.ToString();
			}
		}	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(dg_callback.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;

				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();

				cmbPageNo.SelectedIndex =dg_callback.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message ;
			}
		}
		//Event fired When page changed from drop down list
		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{				
				dg_callback.CurrentPageIndex= cmbPageNo.SelectedIndex ;		
				Dv=(DataView) Session["DV"]; 
				if(Session["StrExp"] !=null)
				{
					SortDataGrid(Session["StrExp"].ToString() );
				}
				else
				{
					dg_callback.DataSource=Dv;
					dg_callback.DataBind();	
					BindReport();
					FillPageList();
					SetNavigation();						
				}
			//	Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			//	FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
			}
			catch (Exception ex)
			{
				lblMessage.Text=ex.Message;
			}		
		}
		#endregion

		//Event fired When Page is changed 
		private void dg_callback_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg_callback.CurrentPageIndex=e.NewPageIndex;
			Dv=(DataView) Session["DV"]; 
			if(Session["StrExp"] !=null)
			{
				SortDataGrid(Session["StrExp"].ToString() );
			}
			else
			{
				dg_callback.DataSource=Dv;
				dg_callback.DataBind();	
				BindReport();
				FillPageList();
				SetNavigation();
			}
			
			
			//FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
		}
		#region Sort logic
		private void dg_callback_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			SortNm = e.SortExpression;				
			SetAcsDesc(SortNm);
			SortDataGrid(SortNm);			
		}
		//Method to set 'ASC' 'DESC' 
		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp = Session["StrExp"].ToString();
				StrAcsDec = Session["StrAcsDec"].ToString();
			}
			catch
			{

			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
				else 
				{
					StrAcsDec = "ASC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				Session["StrExp"] = StrExp ;
				Session["StrAcsDec"] = StrAcsDec ;				
			}
		}
		//Method to sort Data Grid
		private void SortDataGrid(string Sortnm)
		{
			StrExp=Sortnm;
			Dv=(DataView )Session["dv"];	

			//Dv = ds.Tables[0].DefaultView;
			Dv.Sort = Sortnm + " " +  Session["StrAcsDec"].ToString();
			dg_callback.DataSource= Dv;
			dg_callback.DataBind();	
			BindReport();	
			FillPageList();
			SetNavigation();
		}

#endregion


		
	}
}

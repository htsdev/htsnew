<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MissingCauses.aspx.cs" Inherits="lntechNew.Activities.MissingCauses" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Missing Causes</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
    <tr>
    <td>
        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
    
    </td>
    </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gv_Data" runat="server"
                    AutoGenerateColumns="False" CssClass="clsleftpaddingtable" Width="100%" OnRowDataBound="gv_Data_RowDataBound" AllowPaging="True" OnPageIndexChanged="gv_Data_PageIndexChanged" OnPageIndexChanging="gv_Data_PageIndexChanging" PageSize="30">
                    <Columns>
                        <asp:TemplateField HeaderText="S. No">
                            <ItemTemplate>
                                <asp:HyperLink ID="hl_SNo" runat="server" Text='<%# Container.DataItemIndex +1  %>' ></asp:HyperLink>
                                <asp:HiddenField ID="hf_TicketID" runat ="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cause Number">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Cause Number") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ticket Number">
                            <ItemTemplate>
                                <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Ticket Number") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Name" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DOB">
                            <ItemTemplate>
                                <asp:Label ID="lbl_DOB" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Date Of Birth") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mid Number">
                            <ItemTemplate>
                                <asp:Label ID="lbl_MidNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Mid Number") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Court Date">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Verified Court Date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Violation Desc">
                            <ItemTemplate>
                                <asp:Label ID="lbl_ViolationDesc" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Violation Description") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server" />
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

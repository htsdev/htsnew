<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoNotMail.aspx.cs" Inherits="HTP.Reports.DoNotMail" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Do Not Mail Flag Update</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
    
function ClearForm()
{
    document.getElementById('Tabs_pnl_ByDescription_txt_FirstName').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_LastName').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_Address').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_ZipCode').value='';
    document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').value='';
    document.getElementById('Tabs_pnl_ByTicketID_txt_TicketID').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_City').value='';
    document.getElementById('Tabs_pnl_ByDescription_ddl_States').value=0;
    document.getElementById('lbl_Message').innerText='';
    document.getElementById('Tabs_pnl_ByAddress_txtAddress').value='';
    document.getElementById('Tabs_pnl_ByAddress_txtCity').value='';
    document.getElementById('Tabs_pnl_ByAddress_txtZipCode').value='';
    document.getElementById('Tabs_pnl_ByAddress_ddlStates').value=0;

    var grid = document.getElementById('gv')
    if (grid != null)
        grid.style.display ='none';
    return(false);
}

// Abbas Qamar 9726 11-03-2011 Validation only Address and zipcode for Address Tab.
function ValidateAddressForm(){
	
    var var2 = document.getElementById('Tabs_pnl_ByAddress_txtAddress');
    var var4 = document.getElementById('Tabs_pnl_ByAddress_txtZipCode');

    if(var2.value == '')
    {
        alert('Please enter an Address')
        var2.focus();
        return(false);
    }

    if(var4.value == '')
    {
        alert('Please enter zip code');
        var4.focus();
        return(false);
    }

    return(true);
}

function ValidateForm(){

    var var2 = document.getElementById('Tabs_pnl_ByDescription_txt_LastName');
    var var3 = document.getElementById('Tabs_pnl_ByDescription_txt_Address');
    var var4 = document.getElementById('Tabs_pnl_ByDescription_txt_ZipCode');

	if(var2.value == '')
	{
	    alert('Please enter Last Name');
        var2.focus();
        return(false);
    }

    if(var3.value == '')
    {
	    alert('Please enter an address');
        var3.focus();
        return(false);
    }

    if(var4.value == '')
    {
        alert('Please enter zip code');
        var4.focus();
        return(false);
    }

    return(true);
}

function ValidateLetterID() {

	if(document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').value=='')
    {
        alert('Please enter a letter id');
        document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').focus();
        return(false);
    }

    var letterid=document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').value;
    if(isNaN(letterid)==true)
    {
        alert('Please enter correct letter id');
        document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').focus();
        return(false);
    }
}

function ValidateTicketID()
{
/*
var ddl = document.getElementById('ddl_DB');

if(ddl.value == '-1')
{
    alert('Please select a database');
    ddl.focus();
    return(false);
}

if(ddl.value == '1')
{
    alert('You can not update a record by \'Ticket Number\' when \'Criminal Client\' is selected');
    ddl.focus();
    return(false);
}
*/
    if(document.getElementById('Tabs_pnl_ByTicketID_txt_TicketID').value=='')
    {
        alert('Please enter a ticket id');
        document.getElementById('Tabs_pnl_ByTicketID_txt_TicketID').focus();
        return(false);
    }
    var tr=document.getElementById('Tabs_pnl_ByTicketID_plzwait');
    tr.style.display='block';
    var trsearch=document.getElementById('Tabs_pnl_ByTicketID_searcharea');
    trsearch.style.display='none';
}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <div>
        <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="background-image: url(../Images/subhead_bg.gif)" height="34" class="clssubhead"
                    align="right">
                    <table width="100%">
                        <tr>
                            <td width="50%" align="left" class="clssubhead">
                                &nbsp;
                            </td>
                            <td width="50%" align="right">
                                <asp:HyperLink ID="hl_records" runat="server" NavigateUrl="~/Activities/NoMailFlagRecords.aspx">No Mail Flag Records</asp:HyperLink>
                                |
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="NoMailFlagRecordsByAddress.aspx?sMenu=121">No Mail Address</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red" CssClass="clsLabel"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 65px">
                    <table width="100%">
                        <tr>
                            <td>
                                <ajaxToolkit:TabContainer ID="Tabs" runat="server" Width="100%" ScrollBars="Auto"
                                    OnClientActiveTabChanged="ClearForm">
                                    <ajaxToolkit:TabPanel ID="pnl_ByDescription" runat="server" HeaderText="By Description"
                                        TabIndex="0">
                                        <ContentTemplate>
                                            <table cellpadding="1" cellspacing="1" width="100%" border="0">
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        First Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_FirstName" runat="server" CssClass="clsInputadministration"
                                                            Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        <font color="red">*</font> Last Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_LastName" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        <font color="red">*</font> Address:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_Address" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        City:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_City" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        State:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddl_States" runat="server" CssClass="clsInputCombo">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        <font color="red">*</font> Zip Code:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_ZipCode" runat="server" CssClass="clsInputadministration" Width="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="btnSearch_Details" runat="server" CssClass="clsbutton" Text="Search"
                                                            OnClick="btnSearch_Details_Click" OnClientClick="return ValidateForm();" Width="50px">
                                                        </asp:Button>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_Reset" runat="server" CssClass="clsbutton" Text="Reset" Width="50px"
                                                            OnClientClick="return ClearForm();"></asp:Button>
                                                        <asp:Button ID="BtnAdd" runat="server" Text="Add" OnClick="BtnAdd_Click" CssClass="clsbutton"
                                                            OnClientClick="return ValidateForm();" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="pnl_ByLetterID" runat="server" HeaderText="By Letter ID">
                                        <ContentTemplate>
                                            <table cellpadding="1" cellspacing="1" width="100%" border="0">
                                                <tr>
                                                    <td align="right" class="clsLabel" style="width: 10%">
                                                        Letter ID:
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="txt_LetterID" runat="server" CssClass="clsInputadministration" MaxLength="8"></asp:TextBox>
                                                    </td>
                                                    <td align="right" style="width: 8%">
                                                        <asp:Button ID="btnSearch_LID" runat="server" CssClass="clsbutton" Text="Search"
                                                            OnClick="btnSearchByLetterID_Click" OnClientClick="return ValidateLetterID();"
                                                            Width="50px"></asp:Button>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_Reset_LetterID" runat="server" CssClass="clsbutton" Text="Reset"
                                                            OnClientClick="return ClearForm();" Width="50px"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="pnl_ByTicketID" runat="server" HeaderText="By Ticket Number">
                                        <ContentTemplate>
                                            <table cellpadding="1" cellspacing="1" width="100%" border="0">
                                                <tr id="searcharea" runat="server">
                                                    <td class="clsLabel" width="10%">
                                                        Ticket Number:
                                                    </td>
                                                    <td align="left" width="15%">
                                                        <asp:TextBox ID="txt_TicketID" runat="server" CssClass="clsInputadministration"></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="8%">
                                                        <asp:Button ID="btn_Save_TicketID" runat="server" CssClass="clsbutton" Text="Search"
                                                            OnClick="btnSaveByTicketNum_Click" OnClientClick="return ValidateTicketID();"
                                                            Width="50px"></asp:Button>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Button ID="btn_Reset_TicketID" runat="server" CssClass="clsbutton" Text="Reset"
                                                            OnClientClick="return ClearForm();" Width="50px"></asp:Button>
                                                    </td>
                                                </tr>
                                                <tr id="plzwait" style="display: none" runat="server">
                                                    <td valign="middle" align="center" class="clssubhead" colspan="4">
                                                        <img src="../Images/plzwait.gif" alt="" />
                                                        &nbsp; Please wait while we process your request.
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="pnl_ByAddress" runat="server" HeaderText="By Addresses" TabIndex="0">
                                        <ContentTemplate>
                                            <table cellpadding="1" cellspacing="1" width="100%" border="0">
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        Address:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        City:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCity" runat="server" CssClass="clsInputadministration" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        State:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlStates" runat="server" CssClass="clsInputCombo">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="clsLabel">
                                                        Zip Code:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtZipCode" runat="server" CssClass="clsInputadministration" Width="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="UpdateByAddress" runat="server" CssClass="clsbutton" Text="Update"
                                                            OnClick="UpdateByAddress_Click" OnClientClick="return ValidateAddressForm();"
                                                            Width="60px"></asp:Button>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_Reset_address" runat="server" CssClass="clsbutton" Text="Reset"
                                                            Width="50px" OnClientClick="return ClearForm();"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                    Width="100%" AllowPaging="false" OnRowCommand="gv_command">
                                    <Columns>
                                        <asp:TemplateField HeaderText="DBId" Visible="false">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_DBId" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.dbid") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RecordId" Visible="false">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_RecordId" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.recordid") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Case Type">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CaseType" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.casetype") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Ticket Number">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="First Name">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbllastName" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbladdress" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.state") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Zip Code">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblzipcode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zipcode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--             <asp:TemplateField HeaderText="Do Not Mail">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_DoNotMail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.nomailflag") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Do Not Mail Flag Date">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblinsertdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DoNotMailUpdateDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="HP" runat="server" CommandArgument='<%# bind("sno") %>'>Update</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" style="background-image: url(../Images/separator_repeat.gif)" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

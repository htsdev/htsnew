<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.Arraignment" Codebehind="Arraignment.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Arraignment</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body>
		<DIV ms_positioning="FlowLayout">
			<FORM id="Form2" method="post" runat="server">
				<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
					<TR>
						<TD bgColor="white" colSpan="5">
							<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR >
									<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" 
										height="9"></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
								<tr>
									<td align="center">
                                        &nbsp;<asp:datagrid id="dg_Arraignment" runat="server" Width="780px" CssClass="clsLeftPaddingTable"
											AutoGenerateColumns="False" OnSelectedIndexChanged="dg_Arraignment_SelectedIndexChanged">
											<Columns>
												<asp:TemplateColumn HeaderText="Last Name">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_LastName runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="First Name">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_FirstName runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Case Number">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
													<ItemTemplate>
														<asp:HyperLink id=hyp_CaseNum runat="server" CssClass="label" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.ticketid_pk"),"&search=0") %>' Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumber")%>'></asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="MID #">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_MidNum runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Arraignment">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_Arraignment runat="server" CssClass="label" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.courtdatemain"),"#",DataBinder.Eval(Container, "DataItem.courtnumbermain")) %>'>
														</asp:Label>
														<asp:Label id=LblCourtDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.courtdatemain") %>' Visible="False">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Location">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="#3366CC" CssClass="label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_Location runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
			<asp:label id="lblMessage" runat="server" Width="351px" CssClass="label" ForeColor="Red" EnableViewState="False"></asp:label></td>
								</tr>
								<TR background="../../images/separator_repeat.gif">
									<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" 
										height="5"></TD>
								</TR>
							</TABLE>
							<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
					</TR>
				</TABLE>
			</FORM>
		</DIV>
	</body>
</HTML>

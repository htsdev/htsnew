using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;


namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for RemingerCalls.
	/// </summary>
	public partial class ReminderCalls : System.Web.UI.Page
	{
        protected System.Web.UI.WebControls.Label lblMessage;
        protected eWorld.UI.CalendarPopup cal_EffectiveFrom;
        protected System.Web.UI.WebControls.DataGrid dg_ReminderCalls;
        protected System.Web.UI.HtmlControls.HtmlForm Form2;
        protected System.Web.UI.WebControls.DropDownList ddl_rStatus;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        protected System.Web.UI.WebControls.Button btn_update1;
        protected System.Web.UI.WebControls.TextBox txt_totalrecords;
        clsSession ClsSession = new clsSession();
		clsLogger clog = new clsLogger();
		string SessionRecid;
        private DataTable dtReminderStatus;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{					
					SessionRecid= Convert.ToString(Session["ReminID"]); 
					
					if (Page.IsPostBack != true)
					{
                        cal_EffectiveFrom.SelectedDate = DateTime.Today; //setting the date
						if (SessionRecid=="")					 //If not redirecting from Notes Page
							Session["ReminID"]="1";		 //initialize Session RecID	
                        GetReminderStatus(); //fills reminder status drop down list
				
					}
                    
					//FillGrid(); //filling the grid
					//SetUrl();	//	putting validations on grid
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddl_rStatus.SelectedIndexChanged += new System.EventHandler(this.ddl_rStatus_SelectedIndexChanged);
			this.btn_update1.Click += new System.EventHandler(this.btn_update1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void  FillGrid() // Filling the Grid
		{
			try
			{	//"@employeeid"  ClsSession.GetSessionVariable("sEmpID",this.Session)
				String[] parameters  = {"@reminderdate","@status"};
				object[] values = {cal_EffectiveFrom.SelectedDate,ddl_rStatus.SelectedValue};			

				DataSet ds_Reminder = ClsDb.Get_DS_BySPArr("USP_HTS_Get_remainderCalls_Update",parameters,values);
				
				dg_ReminderCalls.DataSource = ds_Reminder;
				dg_ReminderCalls.DataBind();

				//S No
				BindReport();
				if (dg_ReminderCalls.Items.Count == 0)
				{
					lblMessage.Text = "No Records";
					dg_ReminderCalls.Visible = false;
				}
				else
				{
					dg_ReminderCalls.Visible = true;
					
				}	
			}
			catch(Exception ex )
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		//Method To Generate Serial Number
		private void BindReport()
		{
			long sNo=(dg_ReminderCalls.CurrentPageIndex)*(dg_ReminderCalls.PageSize);									
			try
			{
				foreach (DataGridItem ItemX in dg_ReminderCalls.Items) 
				{ 					
					sNo +=1 ;
      
					((TextBox)(ItemX.FindControl("txt_sno"))).Text= sNo.ToString();			
				}
				//To assign total num of records
				txt_totalrecords.Text= sNo.ToString();			
				
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}
		//
		private void SetUrl() // setting the grid
		{
			try
			{
				foreach ( DataGridItem  items in dg_ReminderCalls.Items)
				{
					// setting hyperlink
					((HyperLink) (items.FindControl("lnkName"))).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" + ((Label) (items.FindControl("lbl_TicketID"))).Text ;
			
					//setting Callbacks
					Label lblCalls = (Label) (items.FindControl("lbl_CallBacks"));
                    GetAllReminderStatuses();
                    DataRow dr = dtReminderStatus.Rows.Find(Convert.ToInt32(lblCalls.Text));
                    ((Label)(items.FindControl("lbl_CallBacks"))).Text = dr["Description"].ToString();
					/*if (lblCalls.Text == "0")
					{
						((Label) (items.FindControl("lbl_CallBacks"))).Text = "Pending";
					}
					else if (lblCalls.Text == "1")
					{
						((Label) (items.FindControl("lbl_CallBacks"))).Text = "Contacted";
					}
					else if (lblCalls.Text == "2")
					{
						((Label) (items.FindControl("lbl_CallBacks"))).Text = "Left Message";
					}

                    else if (lblCalls.Text == "3")
                    {
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = "No Answer";
                    }

                    else if (lblCalls.Text == "4")
                    {
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = "Wrong Number";
                    }
					else 
					{
						((Label) (items.FindControl("lbl_CallBacks"))).Text = "None";
					}*/
					// Link to move back to violation fee page
					((HyperLink) (items.FindControl("lnk_Comments"))).NavigateUrl = "javascript:window.open('remindernotes.aspx?casenumber=" +  ((Label) (items.FindControl("lbl_TicketID"))).Text + "&violationID=" + ((Label) (items.FindControl("lbl_TicketViolationID"))).Text+"&searchdate=" + cal_EffectiveFrom.SelectedDate +"&Recid="+((TextBox) (items.FindControl("txt_sno"))).Text+  "','','status=yes,width=450,height=310');void('');";
					// Comments setting			 
					if (((Label) (items.FindControl("lbl_Comments"))).Text == "")
					{
						((Label) (items.FindControl("lbl_Comments"))).Text = "No Comments";
					}
					/*
					// Status setting
					if(((Label) (items.FindControl("lbl_Status"))).Text == "---Choose----")
					{
						((Label) (items.FindControl("lbl_Status"))).Text = "";
					}
					*/
                     
					// Firm setting
					if (((Label) (items.FindControl("lbl_Firm"))).Text != "SULL")
					{
						((Label) (items.FindControl("lbl_contact1"))).Text  =((Label) (items.FindControl("lbl_Firm"))).Text  ;						
						((Label) (items.FindControl("lbl_contact1"))).Font.Bold=true;
						((Label) (items.FindControl("lbl_contact2"))).Visible = false;;
						((Label) (items.FindControl("lbl_contact3"))).Visible = false;
					}
					// Contact no setting
					else if
						(((Label) (items.FindControl("lbl_contact1"))).Text.StartsWith("(")	&& 
						((Label) (items.FindControl("lbl_contact2"))).Text.StartsWith("(")  &&
						((Label) (items.FindControl("lbl_contact3"))).Text.StartsWith("("))
					{
						((Label) (items.FindControl("lbl_contact1"))).Text = "No contact";
						((Label) (items.FindControl("lbl_contact2"))).Visible = false;;
						((Label) (items.FindControl("lbl_contact3"))).Visible = false;
					}
					else 
					{
						if (((Label) (items.FindControl("lbl_contact1"))).Text.StartsWith("(") ||
							((Label) (items.FindControl("lbl_contact1"))).Text.StartsWith("000"))	
						{
							((Label) (items.FindControl("lbl_contact1"))).Visible  = false;
						}
						if (((Label) (items.FindControl("lbl_contact2"))).Text.StartsWith("(")||
							((Label) (items.FindControl("lbl_contact2"))).Text.StartsWith("000"))
						{
							((Label) (items.FindControl("lbl_contact2"))).Visible  = false;
						}
						if (((Label) (items.FindControl("lbl_contact3"))).Text.StartsWith("(") ||
							((Label) (items.FindControl("lbl_contact3"))).Text.StartsWith("000"))
						{
							((Label) (items.FindControl("lbl_contact3"))).Visible  = false;
						}
					}
					// Bond flag setting
						if( ((Label) items.FindControl("lbl_BondFlag")).Text == "1")
						{
							((Label) items.FindControl("lbl_Bond")).Visible = true;
						}
					// Owes setting
					if (((Label) (items.FindControl("lbl_owes"))).Text.StartsWith("$0")==false)
					{
						((Label) (items.FindControl("lbl_owes"))).Text="("+((Label) (items.FindControl("lbl_owes"))).Text+")";
						((Label) (items.FindControl("lbl_owes"))).Visible  = true;
					}
					
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void ddl_rStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
			//	if (SessionRecid=="")					 
			//	Session["ReminID"]="1";              //initialize Session RecID	
				FillGrid(); //filling the grid
				SetUrl();  // setting the grid contants
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		

		private void btn_update1_Click(object sender, System.EventArgs e)
		{
			try
			{
			//	if (SessionRecid=="")					 
			//	Session["ReminID"]="1";			 //initialize Session RecID	
				FillGrid(); //filling the grid
				SetUrl();  // setting the grid contants
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

        private void GetReminderStatus()
        {
            DataTable dtStatus = ClsDb.Get_DT_BySPArr("USP_Get_Reminder_Status_Update");

            if (dtStatus.Rows.Count > 0)
            {
                ddl_rStatus.Items.Clear();
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                    ddl_rStatus.Items.Add(item);
                }
                ListItem itm = new ListItem("All", "5");
                ddl_rStatus.Items.Add(itm);
            }
        }

        private void GetAllReminderStatuses()
        {
            dtReminderStatus = ClsDb.Get_DT_BySPArr("USP_HTS_GET_ALL_REMINDERCALL_STATUS_Update");

            dtReminderStatus.Constraints.Add("PrimaryKeyConstraint", dtReminderStatus.Columns[0], true);

        }

        protected void btn_update1_Click1(object sender, EventArgs e)
        {

        }
	}
}

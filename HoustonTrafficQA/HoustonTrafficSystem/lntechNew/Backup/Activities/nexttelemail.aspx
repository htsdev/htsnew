<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.nexttelemail"
    CodeBehind="nexttelemail.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>HTP Nexttel Email</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
    <meta content="C#" name="CODE_LANGUAGE"/>
    <meta content="JavaScript" name="vs_defaultClientScript"/>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
    <link href="../Styles.css" type="text/css" rel="stylesheet"/>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
        border="0">
        <tr>
            <td style="width: 815px" colspan="4">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td style="width: 816px" colspan="4">
                <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="10">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 39px" width="100%" background="../Images/subhead_bg.gif" colspan="4"
                            height="39">
                            <table id="Table3" style="width: 788px; height: 24px" cellspacing="1" cellpadding="1"
                                width="788" border="0">
                                <tr>
                                    <td class="clssubhead" style="width: 233px">
                                        &nbsp;Global Settings
                                    </td>
                                    <td align="right">
                                        <asp:HyperLink ID="HyperLink1" runat="server" BackColor="Transparent" NavigateUrl="emailhistory.aspx?sMenu=26">History</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="clsleftpaddingtable" width="32%">
                                        &nbsp;<asp:Label ID="Label4" runat="server" CssClass="label">Notice</asp:Label>
                                    </td>
                                    <td class="clsleftpaddingtable" width="70%">
                                        <asp:Label ID="Label6" runat="server" CssClass="label">From</asp:Label>&nbsp;
                                        <asp:DropDownList ID="ddl_noticefrom" runat="server" CssClass="clsinputadministration"
                                            Width="48px">
                                            <asp:ListItem Value="0" Selected ="True">0</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:Label ID="Label5" runat="server" CssClass="label">To</asp:Label>&nbsp;
                                        <asp:DropDownList ID="ddl_noticeto" runat="server" CssClass="clsinputadministration"
                                            Width="48px">
                                            <asp:ListItem Value="0" Selected ="True">0</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsleftpaddingtable" align="left" colspan="4">
                            <asp:DataGrid ID="dgresult" runat="server" BorderColor="White" CssClass="clsleftpaddingtable"
                                Width="784px" CellPadding="0" ShowHeader="False" ShowFooter="True" BorderStyle="None"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle HorizontalAlign="Left" Width="100px" CssClass="clsaspcolumnheader">
                                        </HeaderStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkbox_name" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                Checked='<%# DataBinder.Eval(Container, "DataItem.defaultflag") %>'></asp:CheckBox>
                                            <asp:Label ID="lbl_uid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderStyle HorizontalAlign="Left" Width="168px" CssClass="clsaspcolumnheader">
                                        </HeaderStyle>
                                        <HeaderTemplate>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="tb_email" runat="server" CssClass="clsinputadministration" Width="495px"
                                                Text='<%# DataBinder.Eval(Container, "DataItem.email") %>' MaxLength="200"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Update" CommandName="update">
                                            </asp:Button>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="100%" colspan="4">
                            <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 760px" align="left" colspan="4">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    &nbsp;
    </form>
</body>
</html>

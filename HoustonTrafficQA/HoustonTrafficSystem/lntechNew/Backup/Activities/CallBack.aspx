<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.CallBack" Codebehind="CallBack.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CallBack</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
			// Open CallBackNotes Popup
	      function OpenEditWin(violationid)
	      {

	          var PDFWin
		      PDFWin = window.open("CallBackNotes.aspx?casenumber="+violationid,"","fullscreen=no,toolbar=no,width=400,height=300,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	  
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px" colSpan="4">
							<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Call Back </font></STRONG>
									</td>
								</tr>-->
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<tr class="clsLeftPaddingTable">
									<td align="center">
										<table id="TableSearchCriteria" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%"
											align="center" border="1">
											<tr>
												<td class="clsaspcolumnheader">&nbsp;Search By Arraignment</td>
											</tr>
											<tr>
												<td style="HEIGHT: 22px"><strong>&nbsp;Start Date</strong></td>
												<TD style="HEIGHT: 22px">&nbsp;<ew:calendarpopup id="AppSDate" runat="server" Width="90px" ToolTip="Select Report Date Range"
														PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
														CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif"
														Nullable="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD style="HEIGHT: 22px"><strong>&nbsp;End Date </strong>
												</TD>
												<TD style="HEIGHT: 22px">&nbsp;<ew:calendarpopup id="AppEDate" runat="server" Width="90px" ToolTip="Select Report Date Range"
														PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
														CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma" ImageUrl="../images/calendar.gif"
														Nullable="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD style="HEIGHT: 22px"><strong>&nbsp;Employee Name </strong>
												</TD>
												<TD style="HEIGHT: 22px">&nbsp;<asp:dropdownlist id="ddl_emplist" runat="server" AutoPostBack="True" CssClass="clsinputcombo" Css></asp:dropdownlist></TD>
												<TD style="HEIGHT: 22px"></TD>
											</tr>
											<tr>
												<td class="clsaspcolumnheader">&nbsp;Search By Contact</td>
											</tr>
											<tr>
												<td><STRONG>&nbsp;Start Date</STRONG></td>
												<TD>&nbsp;<ew:calendarpopup id="ContactSDate" runat="server" Width="90px" ToolTip="Select Report Date Range"
														PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
														ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma"
														ImageUrl="../images/calendar.gif" Nullable="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD><STRONG>&nbsp;End Date</STRONG></TD>
												<TD>&nbsp;<ew:calendarpopup id="ContactEDate" runat="server" Width="90px" ToolTip="Select Report Date Range"
														PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
														ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Font-Size="8pt" Font-Names="Tahoma"
														ImageUrl="../images/calendar.gif" Nullable="True">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup></TD>
												<TD><STRONG>&nbsp;Status</STRONG></TD>
												<TD>&nbsp;<asp:dropdownlist id="ddl_callstatus" runat="server" AutoPostBack="True" CssClass="clsinputcombo"
														Css>
														<asp:ListItem Value="0" Selected="True">Pending</asp:ListItem>
														<asp:ListItem Value="1">Contacted</asp:ListItem>
														<asp:ListItem Value="2">Message</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD align="right"><asp:button id="btn_update1" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
											</tr>
											<TR>
												<TD vAlign="top" align="left" colSpan="1" rowSpan="1"></TD>
												<TD></TD>
												<TD></TD>
												<TD></TD>
												<TD></TD>
												<TD></TD>
												<TD align="right"></TD>
											</TR>
										</table>
									<asp:label id="lblMessage" runat="server" Font-Size="X-Small" Font-Names="Verdana" ForeColor="Red"></asp:label></td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif" colSpan="7" height="11"></td>
								</tr>
								<tr>
									<td align="right"><asp:label id="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" CssClass="cmdlinks"
											ForeColor="#3366cc" Height="8px" Font-Bold="True">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Width="9px" Font-Size="Smaller" CssClass="cmdlinks" ForeColor="#3366cc"
											Height="10px" Font-Bold="True">a</asp:label>&nbsp;
										<asp:label id="lblGoto" runat="server" Width="16px" Font-Size="Smaller" CssClass="cmdlinks"
											ForeColor="#3366cc" Height="7px" Font-Bold="True">Goto</asp:label><asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" AutoPostBack="True" CssClass="clinputcombo"
											ForeColor="#3366cc" Font-Bold="True"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif" colSpan="7" height="11"></td>
								</tr>
								<tr>
									<td vAlign="top" align="center">
										<table id="TblGrid" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td><asp:datagrid id="dg_callback" runat="server" Width="100%" CssClass="clsLeftPaddingTable" PageSize="50"
														AllowPaging="True" AutoGenerateColumns="False" BackColor="#EFF4FB" BorderColor="White" AllowSorting="True">
														<Columns>
															<asp:TemplateColumn HeaderText="S.No.">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:HyperLink id="hlnk_Sno" runat="server">HyperLink</asp:HyperLink>
																	<asp:Label id=lbl_ticketid runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"ticketid_pk") %>' Visible="False">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Rep">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_Username runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Abbreviation") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Name">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id=lnkb_clientname runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ClientName") %>'>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn Visible="False" HeaderText="Telephone">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_telephone runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contactnumber") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="contactdate" HeaderText="Contact">
																<HeaderStyle HorizontalAlign="Center" Width="14%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<TABLE id="tbl_contact" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%"
																		border="0">
																		<TR>
																			<TD>
																				<asp:Label id=lbl_contactdate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contactdate") %>'>
																				</asp:Label></TD>
																			<TD>
																				<asp:Label id=lbl_contactdatediff runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contactdiff") %>'>
																				</asp:Label></TD>
																		</TR>
																	</TABLE>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="appeardate" HeaderText="Appearance">
																<HeaderStyle HorizontalAlign="Center" Width="14%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<TABLE id="tbl_appearance" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="100%"
																		align="left" border="0">
																		<TR>
																			<TD>
																				<asp:Label id=lbl_appeardate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"appeardate") %>'>
																				</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id=lbl_appeardatediff runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"appeardiff") %>'>
																				</asp:Label></TD>
																		</TR>
																	</TABLE>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Quote">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_quote runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Quote") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="General Comments">
																<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_comments runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"generalcomments") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Status">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_statusid runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"callstatus") %>' Visible="False">
																	</asp:Label>
																	<asp:Label id="lbl_status" runat="server" CssClass="label"></asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " HorizontalAlign="Center"></PagerStyle>
													</asp:datagrid></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<TR>
									<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</tbody>
			</TABLE>
		</form>
	</body>
</HTML>

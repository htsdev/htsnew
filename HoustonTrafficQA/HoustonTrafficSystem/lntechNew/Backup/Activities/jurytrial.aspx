<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jurytrial.aspx.cs" Inherits="HTP.Activities.jurytrial" %>

<%@ Register TagName="ActiveMenu" TagPrefix="uc3" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Attorney Trials</title>
    <SCRIPT src="../Scripts/validatecreditcard.js" type="text/javascript"></SCRIPT>
    
<script language="javascript" type="text/javascript">

function ValidateForm()
{
 
     
    var input;
    input =document.getElementById("ddl_attorney").selectedIndex;
    if(input ==0)
    {
        alert("Please Select Attorney");
        document.getElementById("ddl_attorney").focus();
         return false;
    }
    input =document.getElementById("txt_prosecutor").value;
    if(input.length ==0)
    {
        alert("Please Enter Prosecutor");
        document.getElementById("txt_prosecutor").focus();
         return false;
    }
    input =document.getElementById("txt_officer").value;
    if(input.length ==0)
    {
        alert("Please Enter Officer Name");
        document.getElementById("txt_officer").focus();
         return false;
    }
    input =document.getElementById("txt_defendant").value;
    if(input.length ==0)
    {
        alert("Please Enter Defendant");
        document.getElementById("txt_defendant").focus();
         return false;
    }
    input =document.getElementById("txt_caseno").value;
    if(input.length ==0)
    {
        alert("Please Enter Case No");
        document.getElementById("txt_caseno").focus();
         return false;
    }
    input =document.getElementById("txt_ticketno").value;
    if(input.length ==0)
    {
        alert("Please Enter Ticket No");
        document.getElementById("txt_ticketno").focus();
         return false;
    }
    input =document.getElementById("ddl_court").selectedIndex;
    if(input ==0)
    {
        alert("Please select Court");
        document.getElementById("ddl_court").focus();
         return false;
    }
    input =document.getElementById("txt_judge").value;
    if(input.length ==0)
    {
        alert("Please Enter Judge Name");
        document.getElementById("txt_judge").focus();
        return false;
    }
    input =document.getElementById("txt_brieffacts").value;
    if(input.length ==0)
    {
        alert("Please Enter Brief Facts of the Case");
        document.getElementById("txt_brieffacts").focus();
        return false;
    }
    input =document.getElementById("ddl_verdict").selectedIndex;
    if(input ==0)
    {
        alert("Please select Verdict");
        document.getElementById("ddl_verdict").focus();
         return false;
    }
    
    input =document.getElementById("txt_fine").value;
    if(input.length ==0)
    {
        alert("Please Enter Fine Amount");
        document.getElementById("txt_fine").focus();
        return false;
    }
     ///Checking Valid amount    
       if(isDecimalNum(document.getElementById("txt_fine").value)!=true)
       {
         alert("Sorry, This is not Valid amount");
         document.getElementById("txt_fine").focus();
         return false;
       }        
       
    input =document.getElementById("txt_creporter").value;
    if(input.length ==0)
    {
        alert("Please Enter Court Reporter");
        document.getElementById("txt_creporter").focus();
        return false;
    }
    
    //Yasir Kamal 6279 08/05/2009 javascript bug fixed.
//   var dt =document.form1.calQueryFrom.value;
//    var dtarray =new Array(3);
//    dtarray =dt.split("/");   
//    
//   if(isValidDate(dtarray[2],dtarray[0],dtarray[1]) == false)
//    {
//     alert("Sorry, This is not a Valid Date");
//     return false;
//     }
        
      var con =confirm("Are you sure you want to save this record (OK = Yes Cancel = No)? ");
      if(con == false)  
      return false;
      
     plzwait();
  //   ClearControls();
}

function ClearControls()
{
    document.getElementById("ddl_attorney").selectedIndex=0;
    document.getElementById("txt_prosecutor").value="";
    document.getElementById("txt_officer").value="";
    document.getElementById("txt_defendant").value="";
    document.getElementById("txt_caseno").value="";
    document.getElementById("txt_ticketno").value="";
    document.getElementById("ddl_court").selectedIndex=0;
    document.getElementById("txt_judge").value="";
    document.getElementById("txt_brieffacts").value="";
    document.getElementById("ddl_verdict").selectedIndex=0;
    document.getElementById("txt_fine").value="";
    document.getElementById("txt_creporter").value="";
    document.getElementById("lblMessage").innerText="";
  
    document.getElementById("btn_submit").value="Submit";
    document.getElementById("hf_rowid").value="";
    document.getElementById("hf_status").value="New";
    return false;
}

 function plzwait()
		{
		
		document.getElementById("tbl_plzwait").style.display = 'block';
		document.getElementById("allcreditcard").style.display = 'none';
		
		}
		
		
</script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <SCRIPT src ="../Scripts/Validationfx.js" type="text/javascript">function Table1_onclick() {

}

</SCRIPT>
    

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
</head>
<body>
    <form id="form1" runat="server">
     <aspnew:ScriptManager ID="ScriptManager1" runat="server"/>
       
    <TABLE id="Table2" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD colSpan="7" style="height: 136px">
                        &nbsp;<uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </TD>
				</TR>				
					
					<TR>
						<TD style="HEIGHT: 10px" background="../Images/separator_repeat.gif" colSpan="4"></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="4">
                            <div style="text-align: center">
                            
                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            <table id="allcreditcard" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable" style="width: 780px">
                                
                                <%--<TR>
						        <td style="HEIGHT: 10px" background="../Images/separator_repeat.gif" colSpan="4"></td>
					            </TR>--%>
					            
                                <tr align="center">                                                                   
                                    
                                <tr>
                                    <td align="center" rowspan="1" valign="top" style="height: 16px">
                                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Width="675px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="height: 16px">
                                        <table border="0" cellpadding="0" cellspacing="4">
                                            <tr>
                                                <td>
                                <table id="tblall" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable" width="100%">
                                    <tr class="clsLeftPaddingTable">
                                        <td class="clsaspcolumnheader" valign="top" style="width: 87px">
                                        </td>
                                        <td valign="top" style="width: 244px">
                                        </td>
                                        <td valign="top" style="width: 53px">
                                        </td>
                                        <td valign="top" style="width: 329px">
                                        </td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="left" class="clsleftpaddingtable" style="height: 23px;" valign="top">
                                            Date</td>
                                        <td align="left" valign="top" style="width: 244px">
                                            <ew:CalendarPopup ID="calQueryFrom" runat="server" AllowArbitraryText="False"
                                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                ShowGoToToday="True" ToolTip="Select end date" UpperBoundDate="12/31/9999 23:59:00"
                                                Width="80px">
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                            </ew:CalendarPopup>
                                        </td>
                                        <td align="left" valign="top" style="width: 53px" class="clsleftpaddingtable">
                                            Attorney</td>
                                        <td align="left" valign="top" style="width: 329px">
                                            <asp:DropDownList ID="ddl_attorney" runat="server" CssClass="clsinputcombo" Width="106px">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="left" class="clsleftpaddingtable" style="width: 87px; height: 23px;" valign="top">
                                            Prosecutor</td>
                                        <td align="left" valign="top" style="width: 244px">
                                            <asp:TextBox ID="txt_prosecutor" runat="server" CssClass="clsinputadministration" Width="196px" MaxLength="50"></asp:TextBox></td>
                                        <td align="left" valign="top" style="width: 53px" class="clsleftpaddingtable">
                                            Officer</td>
                                        <td align="left" valign="top" style="width: 329px">
                                            <asp:TextBox ID="txt_officer" runat="server" CssClass="clsinputadministration" Width="196px" MaxLength="50"></asp:TextBox></td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="left" class="clsleftpaddingtable" style="width: 87px; height: 23px;" valign="top">
                                            Defendant</td>
                                        <td align="left" valign="top" style="width: 244px">
                                            <asp:TextBox ID="txt_defendant" runat="server" CssClass="clsinputadministration" Width="196px" MaxLength="50"></asp:TextBox></td>
                                        <td align="left" valign="top" style="width: 53px" class="clsleftpaddingtable">
                                            Case No</td>
                                        <td align="left" valign="top" style="width: 329px">
                                            <asp:TextBox ID="txt_caseno" runat="server" CssClass="clsinputadministration" Width="147px" MaxLength="25"></asp:TextBox></td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="left" class="clsleftpaddingtable" style="width: 87px; height: 23px" valign="top">
                                            Ticket No</td>
                                        <td align="left" style="width: 244px" valign="top">
                                            <asp:TextBox ID="txt_ticketno" runat="server" CssClass="clsinputadministration" Width="147px" MaxLength="25"></asp:TextBox></td>
                                        <td align="left" class="clsleftpaddingtable" style="width: 53px" valign="top">
                                        </td>
                                        <td align="left" valign="top" style="width: 329px">
                                        </td>
                                    </tr>
                                    <tr class="clsLeftPaddingTable">
                                        <td align="left" class="clsleftpaddingtable" style="width: 87px; height: 25px;" valign="top">
                                            Court</td>
                                        <td align="left" valign="top" style="width: 244px">
                                            <asp:DropDownList ID="ddl_court" runat="server" CssClass="clsinputcombo" Width="235px">
                                            </asp:DropDownList></td>
                                        <td align="left" valign="top" style="width: 53px" class="clsleftpaddingtable">
                                            Judge</td>
                                        <td align="left" valign="top" style="width: 329px">
                                            <asp:TextBox ID="txt_judge" runat="server" CssClass="clsinputadministration" Width="196px" MaxLength="50"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="clsleftpaddingtable" height="20" style="display: none; width: 87px;" valign="top">
                                            Card Type</td>
                                        <td height="20" style="display: none; width: 244px;" valign="top">
                                            &nbsp;
                                            <asp:DropDownList ID="ddl_cardtype" runat="server" CssClass="clsinputcombo" Width="176px">
                                            </asp:DropDownList></td>
                                        <td height="20" style="display: none; width: 53px;" valign="top">
                                        </td>
                                        <td height="20" style="display: none; width: 329px;" valign="top">
                                        </td>
                                    </tr>
                                </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                        <table id="Table1" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable">
                              
                                            <tr>
                                                <td align="left" class="clsleftpaddingtable" valign="top" style="width: 87px">
                                                    Brief Facts
                                                    </td>
                                                <td valign="top" style="height: 1px">
                                                    <asp:TextBox ID="txt_brieffacts" runat="server" CssClass="clsinputadministration"
                                                        Height="200px" MaxLength="2000" TextMode="MultiLine"
                                                        Width="650px"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table id="Table3" border="0" cellpadding="0" cellspacing="2" class="clsleftpaddingtable" width="100%">
                                       
                                                        <tr>
                                                            <td align="left" class="clsleftpaddingtable" height="1" style="width: 87px; height: 23px;" valign="top">
                                                                Verdict</td>
                                                            <td align="left" height="1" valign="top" style="width: 224px"><asp:DropDownList ID="ddl_verdict" runat="server" CssClass="clsinputcombo" Width="159px">
                                                                <asp:ListItem Selected="True" Value="0">--------- Select ---------</asp:ListItem>
                                                                <asp:ListItem Value="1">Guilty</asp:ListItem>
                                                                <asp:ListItem Value="2">Not Guilty</asp:ListItem>
                                                                <asp:ListItem Value="3">Hung Jury - Case Dismissed</asp:ListItem>
                                                                <asp:ListItem Value="4">Hung Jury - Case Reset</asp:ListItem>
                                                                <asp:ListItem Value="5">Mistrial</asp:ListItem>
                                                                <asp:ListItem Value="6">Other</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                            <td align="left" height="1" valign="top" style="width: 53px" class="clsleftpaddingtable">
                                                                Fine ($)</td>
                                                            <td align="left" height="1" valign="top">
                                                                <asp:TextBox ID="txt_fine" runat="server" CssClass="clsinputadministration" Width="96px" MaxLength="10"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="clsleftpaddingtable" height="1" style="width: 87px; height: 23px;" valign="top">
                                                                Court Reporter &nbsp;&nbsp;
                                                            </td>
                                                            <td align="left" height="1" valign="top" style="width: 247px">
                                                                <asp:TextBox ID="txt_creporter" runat="server" CssClass="clsinputadministration" Width="196px" MaxLength="50"></asp:TextBox>
                                                               
                                                            </td>
                                                           
                                                        </tr>
                                                </table>
                                             </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Button ID="btn_submit" runat="server" CssClass="clsbutton" Text="Submit" Width="80px" OnClick="btn_submit_Click" />&nbsp;
                                                    <asp:Button ID="btn_reset" runat="server" CssClass="clsbutton" EnableViewState="False"
                                                        Text="Reset" Width="46px" CausesValidation="False" OnClick="btn_reset_Click" /></td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hf_rowid" runat="server" />
                                        <asp:HiddenField ID="hf_status" runat="server" Value="New" />
							</td>
                                </tr>
                            </table>
                                <table id="tbl_plzwait" class="clssubhead" style="display: none; height: 60px" width="100%">
                                    <tr>
                                        <td align="center" class="clssubhead" valign="middle">
                                            <img src="../Images/plzwait.gif" />
                                            &nbsp; Please wait while we process your request.
                                        </td>
                                    </tr>
                                </table>
                          </ContentTemplate>
            <Triggers>
                
            </Triggers>
   </aspnew:UpdatePanel>
   
                            </div>
                            <table>
                                <tr>
                                    <td width="780" align="left" style="height: 66px">
                                        <br />
                                        <uc2:Footer ID="Footer1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;&nbsp; &nbsp;
                        </TD>
					</TR>
				</TABLE>
    </form>
</body>
</html>

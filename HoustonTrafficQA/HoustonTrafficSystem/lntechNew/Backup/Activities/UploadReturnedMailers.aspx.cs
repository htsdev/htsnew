using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;
using HTP.Components.Entities;
using HTP.Components.Services;

namespace HTP.Activities
{
    public partial class UploadReturnedMailers: System.Web.UI.Page
    {
        
        clsENationWebComponents clsdb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        ReturnedMailService rtnMailService = new ReturnedMailService();
        ReturnedMail rtnMail = new ReturnedMail();
        int TotalRecords = 0;
        int GoodCount = 0;
        int Badcount = 0;
        int UpdateCount = 0;
        int alreadyUpdated = 0;
        bool isValidFormat = false;       
        //Sabir Khan 6371 08/13/2009 Check for records that already updated.
        int alreadyUploaded = 0;
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                btnsave.Attributes.Add("OnClick", " return Validate();");
                lblMessage.Text = "";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        /// <summary>
        /// Save button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string str="";
                if (FileUpload.HasFile)
                {  
                        string filetype = FileUpload.FileName.Substring(FileUpload.FileName.LastIndexOf(".")+1, 3);
                        if (filetype != "txt")
                        {
                            lblMessage.Text = "This is not a valid file.";
                            return;
                        }
                        FileUpload.SaveAs(MapPath("~/Uploads/" + FileUpload.FileName));
                    
                    StreamReader reader = new StreamReader(MapPath("~/Uploads/" + FileUpload.FileName));
                    //Sabir Kha 6232 07/28/2009 Set object properties...
                    rtnMail.MailType = ((ReturnedMail.ReturnMailType) Convert.ToInt32( (ddl_uploadmailer.SelectedValue)) );
                    rtnMail.MailFormat = reader.ReadLine().ToString();
                    reader.Close();
                    //Sabir Khan 6232 07/28/2009 check file format...
                    isValidFormat = rtnMailService.ValidateFileFormat(rtnMail);
                    rtnMail = null;
                    if (isValidFormat == false)
                    {
                        if(Convert.ToInt32(ddl_uploadmailer.SelectedValue) == 2)
                            lblMessage.Text = "The file format is not correct for USPS IMB Returned mail. Please try to upload using 'Returned Mail - Regular'";
                        else if(Convert.ToInt32(ddl_uploadmailer.SelectedValue) == 0)
                            lblMessage.Text = "The file format is not correct for selected loader type. Please try to upload using 'Returned Mail - USPS IMB'";
                        File.Delete(MapPath("~/Uploads/" + FileUpload.FileName));
                        return;
                    }
                    else
                    {
                        //Sabir Khan 6232 07/28/2009 Process file...
                        //lblMessage.Text = "File has been uploaded";
                        reader = new StreamReader(MapPath("~/Uploads/" + FileUpload.FileName));
                        int mailerid;
                        while (str != null)
                        {
                            try
                            {
                                str = reader.ReadLine();
                                rtnMail = new ReturnedMail();
                                if (str != null)
                                {
                                    if (str.Trim().Length > 0)
                                    {
                                        //Sabir Khan 6232 07/28/2009 Set Object properties...
                                        rtnMail.MailType = ((ReturnedMail.ReturnMailType)Convert.ToInt32((ddl_uploadmailer.SelectedValue)));
                                        TotalRecords++;
                                        if (rtnMail.MailType == ReturnedMail.ReturnMailType.ReturnMailIMB) //For Returned Mail - USPS IMB
                                        {
                                            //Sabir Khan 6232 07/28/2009 get mailer id from file...
                                            string[] strarr = str.Split(',');
                                            //Sabir Khan 6371 08/13/2009 increment bad count if mailer id in not converting into integer...
                                            //Sameeullah Daris 6615 12/22/2010 increment bad count if format of data row is invalid.checked length of strarr is equal to 5

                                            if (strarr.Length==5 && int.TryParse(strarr[3], out mailerid))
                                            {
                                                rtnMail.MailerID = Convert.ToInt32(strarr[3]);
                                                //Sabir Khan 6232 07/28/2009 Add mailer id in Mailing List...                                            
                                                if (rtnMailService.UpdateMailingList(rtnMail))
                                                    GoodCount++;
                                                else
                                                    alreadyUploaded++;
                                            }
                                            else
                                            {
                                                Badcount++; 
                                            }
                                            
                                        }
                                        else if (rtnMail.MailType == ReturnedMail.ReturnMailType.ReturnMailReguler)  //For Returned Mail - Regular
                                        {
                                            //Sabir Khan 6232 07/28/2009 Get mailer id...
                                            if (int.TryParse(str, out mailerid))
                                            {
                                                rtnMail.MailerID = Convert.ToInt32(str);
                                                //Sabir Khan 6232 07/28/2009 Add mailer is into mailing list...
                                                if (rtnMailService.UpdateMailingList(rtnMail))
                                                    GoodCount++;
                                                else
                                                    alreadyUploaded++;
                                            }
                                            else
                                            { 
                                                Badcount++;
                                            }
                                        }

                                    }
                                }
                            }
                            catch (Exception exx)
                            {
                                lblMessage.Text = exx.Message;
                                bugTracker.ErrorLog(exx.Message, exx.Source, exx.TargetSite.ToString(), exx.StackTrace);                                
                            }
                        }
                        reader.Close();
                        //Sabir Khan 6232 07/28/2009  update donot mail flag ...
                        //Sabir Khan 6369  08/12/2009 Fixed IMB Loader issue...
                        rtnMail = new ReturnedMail();
                        rtnMail.MailType = ((ReturnedMail.ReturnMailType)Convert.ToInt32((ddl_uploadmailer.SelectedValue)));

                        UpdateCount = rtnMailService.UpdateDoNotMail(rtnMail);
                        //Sabir Khan 6371 08/13/2009 get count of records that already updated...
                        alreadyUpdated = GoodCount - UpdateCount;
                        File.Delete(MapPath("~/Uploads/" + FileUpload.FileName));
                        SendMail();

                        lblMessage.Text = "File has been added to No Mailing List and an Email sent to " + ConfigurationManager.AppSettings["RMAmailTo"].ToString();
                    }    
                    
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SendMail()
        {
                //Sabir Khan 6371 08/13/2009 Add anthoer figures Total Records that already uploaded and Total Records that already updated.
                string toUser = ConfigurationManager.AppSettings["RMAmailTo"].ToString();
                string ccUser = ConfigurationManager.AppSettings["RMAmailCC"].ToString();
                lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email." +
                    "<br><br> File has been added to No Mailing List." +
                    "<br><br> Total Records Processed = " + TotalRecords.ToString() +
                    "<br><br> Total Records Uploaded = " + GoodCount.ToString() +
                    "<br> Total Records That Already Uploaded = " + alreadyUploaded.ToString() +
                    "<br> Total Bad Mailer Ids = " + Badcount.ToString() +
                    "<br><br> Total Records Updated = " + UpdateCount.ToString() +
                    "<br> Total Records That Already Updated = " + alreadyUpdated.ToString() ,
                     ddl_uploadmailer.SelectedItem.ToString() + " loader alert " +  DateTime.Now.ToString("MM/dd/yyyy") + "@" + DateTime.Now.ToString("HH:mm tt"),
                    "", toUser, ccUser);
           
        }
    }
}

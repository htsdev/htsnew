using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for ReminderNotes.
	/// </summary>
	public partial class ReminderNotes : System.Web.UI.Page
	{
        protected System.Web.UI.WebControls.Label lblMessage;
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        protected System.Web.UI.WebControls.Label lblComment;
        protected System.Web.UI.WebControls.Label lblStatus;
        int TicketID = 0;
        protected System.Web.UI.WebControls.Label lbl_dateupdate;
        int TicketViolationID = 0;
        protected System.Web.UI.WebControls.Label lbl_Name;
        protected System.Web.UI.WebControls.Label lbl_rid;
        protected System.Web.UI.WebControls.Label lbl_Status;
        protected System.Web.UI.WebControls.Label lbl_location;
        protected System.Web.UI.WebControls.DropDownList ddl_rStatus;
        protected System.Web.UI.WebControls.Label lbl_contact1;
        protected System.Web.UI.WebControls.Label lbl_Contact2;
        protected System.Web.UI.WebControls.Label lbl_Contact3;
        protected System.Web.UI.WebControls.Label lbl_phone;
        protected System.Web.UI.WebControls.Label lbl_Fax;
        protected System.Web.UI.WebControls.Label lbl_outsidefirm;
        protected System.Web.UI.WebControls.Label lbl_Firm;
        protected System.Web.UI.WebControls.Button btn_update;
        protected System.Web.UI.WebControls.Label lbl_Language;
        protected System.Web.UI.WebControls.TextBox txt_RComments;
        protected System.Web.UI.WebControls.Label lbl_contacttext;
        protected System.Web.UI.WebControls.Label lbl_Bond;
		DateTime SearchDate;

		
		
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try 
			{						
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					//sending back to parent page.	
					HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
				}
				// Assign case number & violation ID 
				 TicketID =Convert.ToInt32(Request.QueryString["casenumber"]);
				 TicketViolationID =Convert.ToInt32(Request.QueryString["violationID"]);
				  SearchDate=Convert.ToDateTime(Request.QueryString["searchdate"]);
				string recid=Request.QueryString["Recid"];
				Session["ReminID"]=recid;

				if (Page.IsPostBack != true)
				{
                    GetReminderStatus();
					// preparing Query
					String[] keys = {"@ticketid","@TicketsViolationID","@courtdate"};
					Object[] values ={TicketID,TicketViolationID,SearchDate};
			
					DataSet ds_ReminderNotes = ClsDb.Get_DS_BySPArr("usp_HTS_Get_reminderCall_comments",keys,values);
					
					// filling the contants on page
					lbl_Firm.Text = ds_ReminderNotes.Tables[0].Rows[0]["FirmAbbreviation"].ToString() ;

					lbl_Name.Text =  ds_ReminderNotes.Tables[0].Rows[0]["LastName"].ToString()+ ", "+ds_ReminderNotes.Tables[0].Rows[0]["firstName"].ToString();
					
					/*
					lbl_date.Text =  String.Format("{0: MM/dd/yyyy}",ds_ReminderNotes.Tables[0].Rows[0]["CourtDateMain"]);		
					lbl_time.Text = String.Format("{0:t}",ds_ReminderNotes.Tables[0].Rows[0]["CourtDateMain"]); 	
					*/		
					lbl_Status.Text = ds_ReminderNotes.Tables[0].Rows[0]["trialdesc"].ToString();		
					lbl_location.Text = ds_ReminderNotes.Tables[0].Rows[0]["courtaddress"].ToString();		
					ddl_rStatus.SelectedValue =  ds_ReminderNotes.Tables[0].Rows[0]["status"].ToString();
					lblStatus.Text =ds_ReminderNotes.Tables[0].Rows[0]["status"].ToString(); 					
					lbl_contact1.Text =  ds_ReminderNotes.Tables[0].Rows[0]["Contact1"].ToString();
					lbl_Contact2.Text =  ds_ReminderNotes.Tables[0].Rows[0]["Contact2"].ToString();
					lbl_Contact3.Text =  ds_ReminderNotes.Tables[0].Rows[0]["Contact3"].ToString();			
					txt_RComments.Text  = ds_ReminderNotes.Tables[0].Rows[0]["comments"].ToString();
					lblComment.Text  = ds_ReminderNotes.Tables[0].Rows[0]["comments"].ToString();
					lbl_Language.Text = ds_ReminderNotes.Tables[0].Rows[0]["LanguageSpeak"].ToString();					
					
					// filling the firm & according to firm contact nos are displayed
					if (lbl_Firm.Text != "SULL")
					{
						lbl_outsidefirm.Visible=true;
						lbl_Firm.Visible=true;						
					}
					// there is no cantact no; formatting of page
					if(	lbl_contact1.Text.StartsWith("(")&& 
						lbl_Contact2.Text.StartsWith("(")&&
						lbl_Contact3.Text.StartsWith("("))
					{
						lbl_contact1.Text = "No contact";
						lbl_Contact2.Visible = false;
						lbl_Contact3.Visible = false;
					}
					else 
					{
						if (lbl_contact1.Text.StartsWith("(") || lbl_contact1.Text.StartsWith("000"))	
						{
							lbl_contact1.Visible  = false;
						}
						if( lbl_Contact2.Text.StartsWith("(") || lbl_Contact2.Text.StartsWith("000"))
						{
							lbl_Contact2.Visible  = false;
						}
						if (lbl_Contact3.Text.StartsWith("(") || lbl_Contact3.Text.StartsWith("000"))
						{
							lbl_Contact3.Visible  = false;
						}
					}
					// setting bond flag
					if (ds_ReminderNotes.Tables[0].Rows[0]["bondflag"].ToString() == "1")
					{
						lbl_Bond.Visible = true;
					}								
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btn_update_Click(object sender, System.EventArgs e)
		{	// Update procedure
			try
			{
				if ((lblStatus.Text != ddl_rStatus.SelectedValue) || (lblComment.Text  != txt_RComments.Text) )
				{
					// getting employee ID from session 
					int EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID",this.Request));
					
					// preparing Query for update
					//alter by zee
					String[] parameters  = {"@comments","@status","@ticketid","@employeeid"};
					object[] values = {txt_RComments.Text,ddl_rStatus.SelectedValue,TicketID,EmpID};
				
					ClsDb.InsertBySPArr("usp_HTS_update_ReminderCall_status",parameters,values);
					lblMessage.Text = "Comments Updated";
					// refresh parent page
					HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
				}
				else
				{
					HttpContext.Current.Response.Write("<script language='javascript'>  self.close();   </script>");
				}
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

        private void GetReminderStatus()
        {
            DataTable dtStatus = ClsDb.Get_DT_BySPArr("USP_Get_Reminder_Status");

            if (dtStatus.Rows.Count > 0)
            {
                ddl_rStatus.Items.Clear();
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                    ddl_rStatus.Items.Add(item);
                }
            }
        }
	}
}

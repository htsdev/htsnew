﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddPopUpComment.ascx.cs"
    Inherits="lntechNew.WebControls.AddPopUpComment" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">

// implementing a trim function for strings in javascript
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}



    function ClosePopUp(backdiv,frontdiv)
    {
        
        document.getElementById(backdiv).style.display="none";        
        document.getElementById(frontdiv).style.display="none";        
        return false;
    }

    function closeModalPopup(popupid, associatedControlId)
    {
        var ddl = document.getElementById(associatedControlId);       
        
        if( ddl != null && ddl.type == "select-one" )
        {
          for (var i = 0; i < ddl.length; i++)
          {
               ddl.options[i].selected = (i == 0);
          }
        }
         
//        var modalPopupBehavior = $find(popupid);
//        modalPopupBehavior.hide();
        return true;
    }


    function ValidateInput( errorMessage, txtClientId)
    {
    
        var updatebutton = '<%= btnsave.ClientID %>';
        //Nasir 7157 12/21/2009 add dummy button when update
        var updatebuttondummy = '<%= btnsavedummy.ClientID %>';
        var cancelbutton = '<%= btnCancel.ClientID %>';
        var td_showwaiting = '<%= td_showwaiting.ClientID %>';
        
        
        // Noufil 7157 21/12/2009 show plz wait image and disable button
        document.getElementById(updatebuttondummy).disabled = true;
        document.getElementById(updatebuttondummy).style.display = "";
        document.getElementById(updatebutton).style.display = "none";
        document.getElementById(cancelbutton).disabled = true;
        document.getElementById(td_showwaiting).style.display = "";
        
        var txtComments = document.getElementById(txtClientId);
        
        if( txtComments != null )
        {
            if(txtComments.value.trim() == "")
            {
                alert(errorMessage);
                // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                document.getElementById(updatebutton).disabled = false;
                document.getElementById(cancelbutton).disabled = false;
                document.getElementById(td_showwaiting).style.display = "none";
                return false;    
            }
        } // end of if
               
       return true;        
    }		
  

    // Set Popup Control Location
    function setpopuplocation(backdiv,frontdiv)
	{
        var top  = 400;
        var left = 400;
        var height = document.body.offsetHeight;
        var width  = document.body.offsetWidth
        
        // Setting Panel Location
        if ( width > 1100 || width <= 1280) left = 575;
        if ( width < 1100) left = 500;
	    
	    // Setting popup display
        document.getElementById(frontdiv).style.top =top;
	    document.getElementById(frontdiv).style.left =left;
						
	    if (  document.body.offsetHeight > document.body.scrollHeight )
		    document.getElementById(backdiv).style.height = document.body.offsetHeight;
	    else
	    document.getElementById(backdiv).style.height = document.body.scrollHeight ;
			
	    document.getElementById(backdiv).style.width = document.body.offsetWidth;
	    document.getElementById(backdiv).style.display = 'block'
	    document.getElementById(frontdiv).style.display = 'block'
	    
	    document.body.scrollTop= 0;
	}
	
		

</script>

<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50);
    left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<div id="pnl_control" runat="server">
    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
        width: 342px">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 26px">
                            <asp:Label ID="lbl_head" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server" OnClick="lbtn_close_Click">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" width="100%">
                    <tr>
                        <td class="clssubhead" style="width: 100%;">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 100%; height: 110">
                            <cc2:WCtl_Comments ID="WCC_Comments" runat="server" Width="587px" Height="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 20px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnsave" runat="server" OnClick="btnsave_Click" CssClass="clsbutton"
                                            Text="Update" Width="53px" />
                                            <asp:Button ID="btnsavedummy" runat="server" CssClass="clsbutton"
                                            Text="Update" Width="53px" style="display:none" />&nbsp;
                                        <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" Text="Cancel" Width="53px"
                                            OnClick="btnCancel_Click" />
                                        &nbsp;&nbsp;
                                    </td>
                                    <td id="td_showwaiting" style="display: none" runat="server">
                                        <img src="../Images/plzwait.gif" alt="../Images/plzwait.gif" />
                                        <span class="clssubhead">Please Wait...</span> &nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Web.UI.WebControls;


namespace lntechNew.WebControls
{

    public partial class UpdateEmailAddress : System.Web.UI.UserControl
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        #region Variables

        clsLogger bugTracker = new clsLogger();

        /// <summary>
        /// 
        /// </summary>
        public event PageMethodHandler PageMethod = null;

        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set
            {
                lbl_head.Text = value;
            }
        }
        public string CallFrom { get; set; }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                btnUpdate.Attributes.Add("onClick", "ClosePopUp('" + this.Disable.ClientID + "','" + this.pnl_control.ClientID + "');");
                lbtn_close.Attributes.Add("onClick", "return ClosePopUp('" + this.Disable.ClientID + "','" + this.pnl_control.ClientID + "');");
                pnl_control.Attributes.Add("style", "z-index: 3; display: none; position: absolute; background-color: transparent; left: 0px; top: 0px;");
                GetReminderStatus();
                lbl_head.Text = "Update Email Address";
            }

        }
        // Update Email address and general comments.
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                int ticketId = int.Parse(this.hf_ticketid.Value);
                int isContacted = Convert.ToInt32(this.hf_isContacted.Value);
                clsCase cCase = new clsCase();
                clsSession cSession = new clsSession();
                clsLogger cLog = new clsLogger();
                cCase.TicketID = ticketId;
                int EmpId = int.Parse(cSession.GetCookie("sEmpID", this.Request));
                cCase.EmpID = EmpId;
                cCase.Email = txt_Email.Text;
                // Babar Ahmad 9676 10/26/2011 Updated general comments. 
                cCase.GeneralComments = txt_GeneralComments.Text;
                if (txt_GeneralComments.Text != "")
                {
                    cCase.UpdateGeneralComments();
                }

                string CallBackStatus;

                if (Convert.ToString(hf_VerifyEmailCallBackStatus.Value) != "Contacted")
                    CallBackStatus = ddl_callback.SelectedItem.Text;
                else
                    CallBackStatus = "Contacted";

                //Abbas Qamar 9676 12-01-2011  setting Callback status contacted if isContacted value is 0 then call back status should be contacted. This value coming from General.aspx.cs page.
                if (isContacted == 0)
                    CallBackStatus = "Contacted";

                UpdateRecords(cCase.TicketID, cCase.EmpID, CallBackStatus, cCase.Email, isContacted);

                //Abbas Qamar 9676 12-01-2011  calline page method to refresh the page.
                PageMethod();

            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        public void GetReminderStatus()
        {
            try
            {
                DataTable dtStatus = ClsDb.Get_DT_BySPArr("USP_Get_Reminder_Status");

                if (dtStatus.Rows.Count > 0)
                {
                    ddl_callback.Items.Clear();
                    foreach (DataRow dr in dtStatus.Rows)
                    {
                        ListItem item = new ListItem(dr["Description"].ToString(), dr["Reminderid_PK"].ToString());
                        ddl_callback.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Abbas Qamar 9676 12-01-2011  Updaing the record against the ticket Id for Bad Email Address.
        public void UpdateRecords(int TicketID, int EmpID, string CallBackStatus, string email, int isContacted)
        {
            string[] keys = { "@ticketid", "@empID", "@CallBackStatus", "@email", "@isContactedPage" };
            object[] values = { TicketID, EmpID, CallBackStatus, email, isContacted };
            ClsDb.ExecuteScalerBySp("usp_htp_update_BadEmailRecords", keys, values);


        }

    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttorneyScheduler.ascx.cs"
    Inherits="HTP.WebControls.AttorneyScheduler" %>

<script type="text/javascript" language="javascript">

    //validate contact number
    function validateNumber(btnID)
    {
    
        var doyou = confirm("Are you sure, you want to update?");
        if(!doyou)
        {
            return false;
        }
    
        var index= (document.getElementById(btnID.id).id).substring(38,40);
        var CommanID = "AttorneySchedulerID_rptShowSetting_ctl";
         for(var i=2; i<=7; i++)
        {
        
            var AttorneyID=document.getElementById(CommanID+index+'_gvShowSetting_ctl0'+i+'_ddlAttorney')
            
            var txtContactID1 = document.getElementById(CommanID+ index +'_gvShowSetting_ctl0'+ i +'_txt_CC11');
            var txtContactID2 = document.getElementById(CommanID+ index +'_gvShowSetting_ctl0'+ i +'_txt_CC12');
            var txtContactID3 = document.getElementById(CommanID+ index +'_gvShowSetting_ctl0'+ i +'_txt_CC13');
            var txtContactID4 = document.getElementById(CommanID+ index +'_gvShowSetting_ctl0'+ i +'_txt_CC14');                    
            var ContactNumber = txtContactID1.value + txtContactID2.value+ txtContactID3.value;
            
            if(AttorneyID[AttorneyID.selectedIndex].index!=0)
            {                                                       
                   if(ContactNumber.length < 10 || isNaN(txtContactID1.value) || isNaN(txtContactID2.value) || isNaN(txtContactID3.value) ||isNaN(txtContactID4.value))
                   {
                    alert("Please Enter Correct Phone Number");
                    txtContactID1.focus();
                    return false;
                   }
                     
            }
            if(ContactNumber.length > 0 && AttorneyID[AttorneyID.selectedIndex].index==0)
            {
                alert("Please select Attorney");
                    AttorneyID.focus();
                    return false;
            }
        }
         $find("MPELoding").show();
    }
    
            //Set contact number in contact text box on ddl change
            function SetContactNumber(ddlAttorney,txt1,txt2,txt3,txt4)
              {
             
                var DDLAttorney = document.getElementById(ddlAttorney.id);
                var txtC1 =  document.getElementById(txt1.id);
                var txtC2 =  document.getElementById(txt2.id);
                var txtC3 =  document.getElementById(txt3.id);
                var txtC4 =  document.getElementById(txt4.id);
                var contact = DDLAttorney[DDLAttorney.selectedIndex].value;
                contact = contact.split('-');
              
              
                if (contact[1] != '' &&  DDLAttorney[DDLAttorney.selectedIndex].value!= '-1')
                {
                    if (contact[1].length >= 3)
                        txtC1.value = contact[1].substring(0, 3);
                    if (contact[1].length >= 6)
                        txtC2.value = contact[1].substring(3, 6);
                    if (contact[1].length >= 10)
                        txtC3.value = contact[1].substring(6, 10);
                    if (contact[1].length > 10)
                        txtC4.value = contact[1].substring(10,contact[1].length);
                }
                else
                {
                    txtC1.value="";
                    txtC2.value="";
                    txtC3.value="";
                    txtC4.value="";
                }
              }
    
    //hide modal popup
    function HidePopup()
    {        
    
        if(!$get('reportFrame').attributes.src.value=="")
        {
            $get('td_iframe').style.display = '';
        }
        $find("MPEShowSettingPopup").hide();
        return false;
    }
    
    function OnExpand()
    {       
    
       if ($find("AttorneySchedulerID_rptShowSetting_ctl00_ColPnlAttorneySetting_Behaviour")!=null)
            $find("AttorneySchedulerID_rptShowSetting_ctl00_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl01_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl02_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl03_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl04_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl05_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl06_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl07_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
       $find("AttorneySchedulerID_rptShowSetting_ctl08_ColPnlAttorneySetting_Behaviour").add_expandComplete(ClosePanel);
        
    }
    
    function ClosePanel(sender  , args)
    {
    
    smoothAnimation();
    
       
        //alert("IDs" + ID);
        var index = sender._id.substring(38,40);
        
        var ColPnlID =  "AttorneySchedulerID_rptShowSetting_ctl" + index + "_ColPnlAttorneySetting_Behaviour"
        
//       if( ColPnlID != "BehAttorneySchedulerID_rptShowSetting_ctl00_btn_update" )
//       {
//         $find("BehAttorneySchedulerID_rptShowSetting_ctl00_btn_update")._doClose();
//       }
        try
        {
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl00_ColPnlAttorneySetting_Behaviour" )
           {
              $find("AttorneySchedulerID_rptShowSetting_ctl00_ColPnlAttorneySetting_Behaviour")._doClose();
           }  
       
       }
         catch(e) {}
        
        try
       {    
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl01_ColPnlAttorneySetting_Behaviour" )
           {
              $find("AttorneySchedulerID_rptShowSetting_ctl01_ColPnlAttorneySetting_Behaviour")._doClose();
           }
       
       }
         catch(e) {}
       
       try
       {
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl02_ColPnlAttorneySetting_Behaviour" )
           {
             $find("AttorneySchedulerID_rptShowSetting_ctl02_ColPnlAttorneySetting_Behaviour")._doClose();
           }
       
       }
         catch(e) {}
         
         
       try
       {
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl03_ColPnlAttorneySetting_Behaviour" )
           {
             $find("AttorneySchedulerID_rptShowSetting_ctl03_ColPnlAttorneySetting_Behaviour")._doClose();
           }
       
       }
         catch(e) {}
       
       try
       {
       
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl04_ColPnlAttorneySetting_Behaviour" )
           {
            $find("AttorneySchedulerID_rptShowSetting_ctl04_ColPnlAttorneySetting_Behaviour")._doClose();
           }
       
        }
         catch(e) {}
       
       try
       {
       
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl05_ColPnlAttorneySetting_Behaviour" )
           {
            $find("AttorneySchedulerID_rptShowSetting_ctl05_ColPnlAttorneySetting_Behaviour")._doClose();
           }
        }
         catch(e) {}
       
       
       try
       {
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl06_ColPnlAttorneySetting_Behaviour" )
           {
            $find("AttorneySchedulerID_rptShowSetting_ctl06_ColPnlAttorneySetting_Behaviour")._doClose();
           }
       
       }
         catch(e) {}
    
       
       
       try
       {
       
       if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl07_ColPnlAttorneySetting_Behaviour" )
       {
        $find("AttorneySchedulerID_rptShowSetting_ctl07_ColPnlAttorneySetting_Behaviour")._doClose();
       }
        
//            $find("AttorneySchedulerID_rptShowSetting_ctl00_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl01_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl02_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl03_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl04_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl05_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl06_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl07_ColPnlAttorneySetting_Behaviour")._doClose();
//            $find("AttorneySchedulerID_rptShowSetting_ctl08_ColPnlAttorneySetting_Behaviour")._doClose();        
        }
         catch(e) {}
         
         
         try
       {
           if( ColPnlID != "AttorneySchedulerID_rptShowSetting_ctl08_ColPnlAttorneySetting_Behaviour" )
           {
            $find("AttorneySchedulerID_rptShowSetting_ctl08_ColPnlAttorneySetting_Behaviour")._doClose();
           }
       
       }
         catch(e) {}
    }
    
    
      function smoothAnimation()
    {                               
            var collPanel = $find("AttorneySchedulerID_rptShowSetting_ctl00_ColPnlAttorneySetting_Behaviour");
            collPanel._animation._fps = 45;
            collPanel._animation._duration = 0.20;
            
            var collPanel1 = $find("AttorneySchedulerID_rptShowSetting_ctl01_ColPnlAttorneySetting_Behaviour");
            collPanel1._animation._fps = 45;
            collPanel1._animation._duration = 0.20;
            
             var collPanel = $find("AttorneySchedulerID_rptShowSetting_ctl02_ColPnlAttorneySetting_Behaviour");
            collPanel._animation._fps = 45;
            collPanel._animation._duration = 0.20;
            
            var collPanel1 = $find("AttorneySchedulerID_rptShowSetting_ctl03_ColPnlAttorneySetting_Behaviour");
            collPanel1._animation._fps = 45;
            collPanel1._animation._duration = 0.20;
            
             var collPanel = $find("AttorneySchedulerID_rptShowSetting_ctl04_ColPnlAttorneySetting_Behaviour");
            collPanel._animation._fps = 45;
            collPanel._animation._duration = 0.20;
            
            var collPanel1 = $find("AttorneySchedulerID_rptShowSetting_ctl05_ColPnlAttorneySetting_Behaviour");
            collPanel1._animation._fps = 45;
            collPanel1._animation._duration = 0.20;
            
             var collPanel = $find("AttorneySchedulerID_rptShowSetting_ctl06_ColPnlAttorneySetting_Behaviour");
            collPanel._animation._fps = 45;
            collPanel._animation._duration = 0.20;
            
            var collPanel1 = $find("AttorneySchedulerID_rptShowSetting_ctl07_ColPnlAttorneySetting_Behaviour");
            collPanel1._animation._fps = 45;
            collPanel1._animation._duration = 0.20;
            
            var collPanel1 = $find("AttorneySchedulerID_rptShowSetting_ctl08_ColPnlAttorneySetting_Behaviour");
            collPanel1._animation._fps = 45;
            collPanel1._animation._duration = 0.20;
    }
    
  
    
    
</script>

<table border="1" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
    border-top-color: navy; border-collapse: collapse; border-right-color: navy"
    width="580px">
    <tr>
        <td valign="top" align="center">
            <table class="clsLeftPaddingTable" width="570px">
                <tr>
                    <td class="clssubhead" background="../images/subhead_bg.gif" style="height: 35px;">
                        <table width="580px">
                            <tr>
                                <td class="clssubhead" align="left">
                                    &nbsp;Show Setting
                                </td>
                                <td align="right">
                                    &nbsp; &nbsp;<asp:LinkButton ID="lnkbtnCancelPopUp" runat="server" OnClientClick="return HidePopup();">X</asp:LinkButton>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <script type="text/javascript" language="javascript">       
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(OnExpand);
                </script>

                <tr>
                    <td align="left">
                        <asp:Repeater ID="rptShowSetting" runat="server" OnItemDataBound="rptShowSetting_ItemDataBound">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="ColPnlAttorneySetting" runat="Server" TargetControlID="pnlShowSetting"
                                                Collapsed="true" ExpandControlID="PnlControlID" CollapseControlID="PnlControlID"
                                                CollapsedText="Show Details..." ExpandedText="Hide Details" ImageControlID="imgGInfo"
                                                SuppressPostBack="true" ExpandDirection="Vertical" CollapsedImage="../Images/collapse.jpg"
                                                ExpandedImage="../Images/expand.jpg" />
                                            <asp:Panel ID="PnlControlID" runat="server">
                                                <table class="clsLeftPaddingTable" style="height: 35px; width: 570px">
                                                    <tr>
                                                        <td align="left" style="width: 30px">
                                                            &nbsp;<asp:Image ID="imgGInfo" runat="server" ImageUrl="../images/collapse.jpg" Style="cursor: pointer;" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblWeeks" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StartDate")%>'
                                                                CssClass="clssubhead"></asp:Label>
                                                            ------
                                                            <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EndDate")%>'
                                                                CssClass="clssubhead"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Panel ID="pnlShowSetting" runat="server">
                                                <table class="clsLeftPaddingTable" style="height: 75px; width: 520px">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="UpdateAll"
                                                                OnClick="btn_update_Click"></asp:Button>
                                                            <table style="display: none;" id="tblProgressUpdatePage" runat="server" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="height: 10px" class="clssubhead" valign="middle" align="center">
                                                                            <img src="../Images/plzwait.gif" />
                                                                            Please wait while we process your request...
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:GridView ID="gvShowSetting" runat="server" Style="height: 90px; overflow: auto"
                                                                AllowSorting="True" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="2px"
                                                                Width="540px" OnRowDataBound="gvShowSetting_RowDataBound" ShowHeader="false">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hf_AttorneyScheduleID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "AttorneyScheduleID")%>' />
                                                                            <asp:Label ID="lblDateTime" runat="server" CssClass="clssubhead" Text='<%# DataBinder.Eval(Container.DataItem, "DocketDate")%>'
                                                                                Font-Size="Smaller"></asp:Label>                                                                            
                                                                        </ItemTemplate>
                                                                        
                                                                        
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hf_AttorneyID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "EmpIDContact")%>' />
                                                                            <asp:DropDownList ID="ddlAttorney" DataSource='<%# GetAttorneys()%>' DataTextField="NAME"
                                                                                DataValueField="IDAndContact" runat="server" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td id="tdContNum" class="Label" style="width: 126px;">
                                                                                        <span id="Span1" runat="server" class="clssubhead">Contact Number</span>
                                                                                    </td>
                                                                                    <asp:HiddenField ID="hf_ContactNumber" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ContactNumber")%>' />
                                                                                    <td id="tdContactNum" colspan="2" style="width: 330px;">
                                                                                        <asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                            Width="32px" MaxLength="3" CssClass="clsInputadministration" ></asp:TextBox>
                                                                                        -
                                                                                        <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                            Width="32px" CssClass="clsInputadministration" MaxLength="3" ></asp:TextBox>
                                                                                        -
                                                                                        <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                            Width="32px" CssClass="clsInputadministration" MaxLength="4" ></asp:TextBox>
                                                                                        x
                                                                                        <asp:TextBox ID="txt_CC14" runat="server" Width="40px" CssClass="clsInputadministration"
                                                                                             MaxLength="4"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

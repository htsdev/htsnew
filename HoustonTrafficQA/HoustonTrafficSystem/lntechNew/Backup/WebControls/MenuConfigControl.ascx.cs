﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using HTP.Components;
using System.Data;
using lntechNew.Components.ClientInfo;

//Waqas 7077 12/14/2009
namespace HTP.WebControls
{
    /// <summary>
    /// MenuConfigControl class file.
    /// </summary>
    public partial class MenuConfigControl : System.Web.UI.UserControl
    {

        clsENationWebComponents clsDB = new clsENationWebComponents();
        MenuSettings MenuDB = new MenuSettings();
        clsLogger LogDB = new clsLogger();

        /// <summary>
        /// PageMethod
        /// </summary>
        public event PageMethodHandler PageMethod;
        private string error;
        /// <summary>
        /// Property which hold error message.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return error;
            }
        }
        
        /// <summary>
        /// This event is called on loading of the page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// This event is called to show popup on adding the menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAdd_Click(object sender, EventArgs e)
        {

            try
            {
                //Add onclick event attribute 
                lst_MenuItems.Attributes.Add("onClick", "return DbClickList();");


                hf_TimeStamp.Value = System.DateTime.Now.TimeOfDay.ToString();
                Session["TimeStamp"] = hf_TimeStamp.Value;

                txt_Title.Text = "";
                txt_URL.Text = "";
                chk_Active.Checked = false;
                chk_Selected.Checked = false;
                chk_Logging.Checked = false;
                rbCategory.SelectedIndex = -1;  //SAEED 7791 05/25/2010 reset selected to null, so that nothing is selected in report category.
                

                chk_Active.Enabled = true;
                txt_URL.Enabled = true;

                //Check first level menu
                if (hf_LevelID.Value == "1")
                {
                    lbl_header.Text = "Add First Level Menu";
                    tr_ListMenuItems.Style["display"] = "none";
                    tr_Url.Style["display"] = "none";
                    tr_Selected.Style["display"] = "none";
                    tr_Logging.Style["display"] = "none";
                    btnAdd.Style["display"] = "";
                    btnUpdate.Style["display"] = "none";
                    //Nasir 7077 02/20/2010 Show if level 1
                    tr_IsSubMenuHidden.Style["display"] = "";
                    tr_Category.Style["display"] = "none";  //SAEED 7791 05/25/2010 hide category row.
                }
                //Check Second level menu
                else if (hf_LevelID.Value == "2")
                {
                    lbl_header.Text = "Add Second Level Menu";
                    tr_ListMenuItems.Style["display"] = "none";
                    tr_Url.Style["display"] = "";
                    tr_Selected.Style["display"] = "";
                    tr_Logging.Style["display"] = "";
                    btnAdd.Style["display"] = "";
                    btnUpdate.Style["display"] = "none";
                    //Nasir 7077 02/20/2010 Hide if level 2
                    tr_IsSubMenuHidden.Style["display"] = "none";
                    DisplayCategoryRow();  //SAEED 7791 05/25/2010 show/hide category
                }

                Label lblMsg = (Label)Page.FindControl("lbl_Message");
                lblMsg.Text = "";

                AjaxControlToolkit.ModalPopupExtender mx = (AjaxControlToolkit.ModalPopupExtender)Page.FindControl("ModalPopup1");
                mx.Show();
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        /// <summary>
        /// This event is called to show popup on updating the menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void btnShowUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //Add click event on Ist level menu item
                lst_MenuItems.Attributes.Add("onClick", "return DbClickList();");

                //save Time session and hidden field to stop event bubbling
                hf_TimeStamp.Value = System.DateTime.Now.TimeOfDay.ToString();
                Session["TimeStamp"] = hf_TimeStamp.Value;

                txt_Title.Text = "";
                txt_URL.Text = "";
                chk_Active.Checked = false;
                chk_Selected.Checked = false;
                chk_Logging.Checked = false;
                rbCategory.SelectedIndex = -1;  //SAEED 7791 05/25/2010 reset selected to null, so that nothing is selected in report category.

                chk_Active.Enabled = true;
                txt_URL.Enabled = true;

                //Check first level menu
                if (hf_LevelID.Value == "1")
                {
                    lbl_header.Text = "Update First Level Menu";
                    tr_ListMenuItems.Style["display"] = "";
                    tr_Url.Style["display"] = "none";
                    tr_Selected.Style["display"] = "none";
                    tr_Logging.Style["display"] = "none";
                    btnAdd.Style["display"] = "none";
                    //Nasir 7077 02/20/2010 Show if level 1
                    tr_IsSubMenuHidden.Style["display"] = "";
                    btnUpdate.Style["display"] = "";
                    //Get first level menu Item data
                    DataSet ds = MenuDB.GetMenuItems(Convert.ToInt16(hf_LevelID.Value), Convert.ToInt16(hf_MenuID.Value));
                    Session["MenuList"] = ds.Tables[0];
                    tr_Category.Style["display"] = "none";  //SAEED 7791 05/25/2010 hide category row

                    //Check Data set record count
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        //Fill list box item
                        lst_MenuItems.DataSource = ds.Tables[0];

                        lst_MenuItems.DataValueField = "ID";
                        lst_MenuItems.DataTextField = "TITLE";
                        lst_MenuItems.DataBind();

                        //Encode list box item text
                        foreach (ListItem l in lst_MenuItems.Items)
                        {
                            l.Text = HttpUtility.HtmlDecode(l.Text);
                        }

                        int MenuID = Convert.ToInt16(hf_MenuID.Value);

                        lst_MenuItems.SelectedValue = MenuID.ToString();
                        DataRow dr = GetMenuItem(ds.Tables[0], MenuID);
                        if (dr != null)
                        {
                            txt_Title.Text = HttpUtility.HtmlDecode(dr["TITLE"].ToString());
                            chk_Active.Checked = Convert.ToBoolean(dr["active"].ToString());
                        }
                        chkIsSubMenuHidden.Checked = Convert.ToBoolean(dr["IsSubMenuHidden"].ToString());
                    }

                }
                //Check Second level menu
                else if (hf_LevelID.Value == "2")
                {
                    lbl_header.Text = "Update Second Level Menu";
                    tr_ListMenuItems.Style["display"] = "";
                    tr_Url.Style["display"] = "";
                    tr_Selected.Style["display"] = "";
                    tr_Logging.Style["display"] = "";
                    btnAdd.Style["display"] = "none";
                    //Nasir 7077 02/20/2010 
                    tr_IsSubMenuHidden.Style["display"] = "none";
                    btnUpdate.Style["display"] = "";
                    DisplayCategoryRow();  //SAEED 7791 05/25/2010 show/hide category row.
                    //Get first level menu Item data
                    DataSet ds = MenuDB.GetMenuItems(Convert.ToInt16(hf_LevelID.Value), Convert.ToInt16(hf_MenuID.Value));

                    //Check Data set record count
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        //Fill list box item
                        lst_MenuItems.DataSource = ds.Tables[0];
                        Session["MenuList"] = ds.Tables[0];

                        lst_MenuItems.DataValueField = "ID";
                        lst_MenuItems.DataTextField = "TITLE";
                        lst_MenuItems.DataBind();

                        //HTML Encoding of list box item text
                        foreach (ListItem l in lst_MenuItems.Items)
                        {
                            l.Text = HttpUtility.HtmlDecode(l.Text);
                        }

                        int MenuID = Convert.ToInt16(hf_MenuID.Value);

                        lst_MenuItems.SelectedValue = MenuID.ToString();
                        DataRow dr = GetMenuItem(ds.Tables[0], MenuID);
                        if (dr != null)
                        {
                            txt_Title.Text = HttpUtility.HtmlDecode(dr["TITLE"].ToString());
                            chk_Active.Checked = Convert.ToBoolean(dr["active"].ToString());
                            chk_Selected.Checked = Convert.ToBoolean(Convert.ToInt16(dr["SELECTED"].ToString()));
                            txt_URL.Text = HttpUtility.HtmlDecode(dr["URL"].ToString());
                            rbCategory.SelectedValue = dr["Category"].ToString(); //SAEED 7791 05/25/2010 set category value.
                        }

                        if (dr["URL"].ToString().Contains("MenuConfiguration.aspx"))
                        {
                            chk_Active.Enabled = false;
                            txt_URL.Enabled = false;
                        }
                        else
                        {
                            chk_Active.Enabled = true;
                            txt_URL.Enabled = true;
                        }
                    }


                }

                Label lblMsg = (Label)Page.FindControl("lbl_Message");
                lblMsg.Text = "";

                AjaxControlToolkit.ModalPopupExtender mx = (AjaxControlToolkit.ModalPopupExtender)Page.FindControl("ModalPopup1");
                mx.Show();
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        

        /// <summary>
        /// This event is called to add menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {

                string strReq = Request.Form["MenuControl1$hf_TimeStamp"].ToString();
                if (Session["TimeStamp"] != null && Session["TimeStamp"].ToString() == strReq.ToString())
                {

                    hf_TimeStamp.Value = System.DateTime.Now.TimeOfDay.ToString();
                    Session["TimeStamp"] = hf_TimeStamp.Value;

                    //Check first level menu
                    if (hf_LevelID.Value == "1")
                    {
                        MenuDB.AddFirstLevelMenuItems(Convert.ToInt16(hf_LevelID.Value), txt_Title.Text, Convert.ToInt16(chk_Active.Checked),Convert.ToBoolean(chkIsSubMenuHidden.Checked));
                        error = "First Level Menu Inserted";

                        //delegate event Page method when menu insert it refresh whole page
                        if (PageMethod != null)
                            PageMethod();
                    }
                    //Check Second level menu
                    else if (hf_LevelID.Value == "2")
                    {
                        MenuDB.AddSecondLevelMenuItems(Convert.ToInt16(hf_LevelID.Value), txt_Title.Text, Convert.ToInt16(chk_Active.Checked), Convert.ToInt16(hf_MenuID.Value), txt_URL.Text, Convert.ToInt16(chk_Selected.Checked), Convert.ToInt16(chk_Logging.Checked), rbCategory.SelectedValue.Trim() == "" ? -1 : Convert.ToInt16(rbCategory.SelectedValue));    //SAEED 7791 05/25/2010 new parameter added for report category.

                        error = "Second Level Menu Inserted";

                        if (PageMethod != null)
                            PageMethod();
                    }

                    //Update application session after insertion menu
                    RestoreMenus();
                }

            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// This event is called to update menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                string strReq = Request.Form["MenuControl1$hf_TimeStamp"].ToString();
                if (Session["TimeStamp"] != null && Session["TimeStamp"].ToString() == strReq.ToString())
                {

                    hf_TimeStamp.Value = System.DateTime.Now.TimeOfDay.ToString();
                    Session["TimeStamp"] = hf_TimeStamp.Value;

                    string OrderedIds = hf_OrderedIds.Value;

                    //Check first level menu
                    if (hf_LevelID.Value == "1")
                    {
                        MenuDB.UpdateFirstLevelMenuItem(Convert.ToInt16(hf_MenuID.Value), Convert.ToInt16(hf_LevelID.Value), HttpUtility.HtmlEncode(txt_Title.Text), Convert.ToInt16(chk_Active.Checked), OrderedIds, Convert.ToBoolean(chkIsSubMenuHidden.Checked), rbCategory.SelectedValue.Trim() == "" ? -1 : Convert.ToInt16(rbCategory.SelectedValue)); //SAEED 7791 05/25/2010 new parameter added for report category.
                        error = "First Level Menu Updated";

                        if (PageMethod != null)
                            PageMethod();
                    }
                    //Check Second level menu
                    else if (hf_LevelID.Value == "2")
                    {
                        MenuDB.UpdateSecondLevelMenuItem(Convert.ToInt16(hf_MenuID.Value), Convert.ToInt16(hf_LevelID.Value), HttpUtility.HtmlEncode(txt_Title.Text), Convert.ToInt16(chk_Active.Checked), OrderedIds, HttpUtility.HtmlEncode(txt_URL.Text), Convert.ToInt16(chk_Selected.Checked), Convert.ToInt16(chk_Logging.Checked), rbCategory.SelectedValue.Trim() == "" ? -1 : Convert.ToInt16(rbCategory.SelectedValue));    //SAEED 7791 05/25/2010 new parameter added for report category.
                        
                        error = "Second Level Menu Updated";

                        if (PageMethod != null)
                            PageMethod();
                    }

                    RestoreMenus();

                }

            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// This method is used to restore all menus
        /// </summary>
        private void RestoreMenus()
        {
            try
            {
                //Get menu from data base
                DataSet ds = MenuDB.GetMenuData();

                if (ds.Tables.Count != 0)
                {
                    DataTable dt = ds.Tables[0];
                    Application["MENU"] = dt;
                }

                ds = MenuDB.GetSubMenuData();

                if (ds.Tables.Count != 0)
                {
                    DataTable dt = ds.Tables[0];
                    Application["SUBMENU"] = dt;
                }
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// This procedure is used to get menu item through menu id
        /// </summary>
        /// <param name="dataTable">It represents the datatable containing menu items </param>
        /// <param name="MenuID">It represents the menu id</param>
        /// <returns></returns>
        private DataRow GetMenuItem(DataTable dataTable, int MenuID)
        {
            try
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    if (dr["ID"].ToString().Equals(MenuID.ToString()))
                    {
                        return dr;
                    }

                }                
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }
            return null;
        }

        /// <summary>
        /// This event is called on double click of the menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListBox_Click(object sender, EventArgs e)
        {
            try
            {
                ListItem lst = lst_MenuItems.SelectedItem;
                DataTable dt = (DataTable)Session["MenuList"];

                //Check first level menu
                if (hf_LevelID.Value == "1")
                {
                    hf_MenuID.Value = lst.Value;
                    DataRow dr = GetMenuItem(dt, Convert.ToInt32(lst.Value));
                    if (dr != null)
                    {
                        txt_Title.Text = HttpUtility.HtmlDecode(dr["TITLE"].ToString());
                        chk_Active.Checked = Convert.ToBoolean(dr["active"].ToString());
                    }
                }
                //Check Second level menu
                else if (hf_LevelID.Value == "2")
                {

                    hf_MenuID.Value = lst.Value;
                    DataRow dr = GetMenuItem(dt, Convert.ToInt32(lst.Value));
                    if (dr != null)
                    {
                        txt_Title.Text = HttpUtility.HtmlDecode(dr["TITLE"].ToString());
                        chk_Active.Checked = Convert.ToBoolean(dr["active"].ToString());
                        chk_Selected.Checked = Convert.ToBoolean(Convert.ToInt16(dr["SELECTED"].ToString()));
                        txt_URL.Text = HttpUtility.HtmlDecode(dr["URL"].ToString());
                        //SAEED 7791 05/25/2010 if report category is -1 then it sets the report category radion buttons to null, other wise set the selected value [Alert=1,Reprot=2]
                        if (dr["Category"].ToString() != "-1")
                            rbCategory.SelectedValue = dr["Category"].ToString();  
                        else rbCategory.SelectedIndex = -1;
                        //SAEED 7791 05/25/2010 END 
                        
                    }

                    if (dr["URL"].ToString().Contains("MenuConfiguration.aspx"))
                    {
                        chk_Active.Enabled = false;
                        txt_URL.Enabled = false;
                    }
                    else
                    {
                        chk_Active.Enabled = true;
                        txt_URL.Enabled = true;
                    }

                }
                AjaxControlToolkit.ModalPopupExtender mx = (AjaxControlToolkit.ModalPopupExtender)Page.FindControl("ModalPopup1");
                mx.Show();
            }
            catch (Exception ex)
            {
                LogDB.ErrorLog(ex.Message.ToString(), ex.Source.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
            }

        }
        
        /// <summary>
        /// Show/Hide category row based on hidden filed value. If the value is null hide category row, otherwise display the category row.
        /// </summary>
        private void DisplayCategoryRow()
        {
            //SAEED 7791 05/25/2010 method created.
            if (!string.IsNullOrEmpty(hf_MenuID.Value))
            {
                if (MenuDB.CheckIfValidationMenu(Convert.ToInt16(hf_MenuID.Value)))    
                {
                    tr_Category.Style["display"] = "";
                }
                else
                {
                    tr_Category.Style["display"] = "none";
                }
            }

        }
        
    }
}
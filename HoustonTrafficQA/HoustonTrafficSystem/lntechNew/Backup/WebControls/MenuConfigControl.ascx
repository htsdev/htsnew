﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuConfigControl.ascx.cs"
    Inherits="HTP.WebControls.MenuConfigControl" %>
<link href="../Styles.css" type="text/css" rel="Stylesheet" />

<script language="javascript" type="text/javascript">
    //Waqas 7077 12/15/2009
    function DbClickList() {

        document.getElementById("MenuControl1_btnListBox").click();
        return false;
    }

    function ValidateAdd() {
        var levelid = document.getElementById("MenuControl1_hf_LevelID").value;
        
        if (levelid == 1) {
        //validate menu item title
            if (document.getElementById("MenuControl1_txt_Title").value.trim() == '') {
                alert("Please enter Title.");
                return false;
            }//Title Length Should be less than 100
             else if (document.getElementById("MenuControl1_txt_Title").value.trim().length > 100) {
                alert("Title length should be less that 100.");
                return false;
            }
        }
        else if (levelid == 2) {
        //validate menu item title
            if (document.getElementById("MenuControl1_txt_Title").value.trim() == '') {
                alert("Please enter Title.");
                return false;
            }//Title Length Should be less than 100
            else if (document.getElementById("MenuControl1_txt_Title").value.trim().length > 100) {
                alert("Title length should be less that 100.");
                return false;
            }

            if (document.getElementById("MenuControl1_txt_URL").value.trim() == '') {
            //validate menu item URL
                alert("Please enter URL.");
                return false;
            }
             else if (document.getElementById("MenuControl1_txt_URL").value.trim().length > 1000) {
            //menu item URL should be less than 1000 character
                alert("Report URL length should be less that 1000.");
                return false;
            }
            //validate URL should starting with ' / '
            else if (document.getElementById("MenuControl1_txt_URL").value.trim().charAt(0) != '/') {
                alert("Please enter URL starting with ' / '.");
                return false;
            }
        }

    }

    function ValidateUpdate() {
    
        var levelid = document.getElementById("MenuControl1_hf_LevelID").value;
        if (levelid == 1) {
            if (document.getElementById("MenuControl1_txt_Title").value.trim() == '') {
                alert("Please enter Title.");
                return false;
            }
             else if (document.getElementById("MenuControl1_txt_Title").value.trim().length > 100) {
                alert("Title length should be less that 100.");
                return false;
            }
        }
        else if (levelid == 2) {
            if (document.getElementById("MenuControl1_txt_Title").value.trim() == '') {
                alert("Please enter Title.");
                return false;
            }
            //Title Length Should be less than 100
            else if (document.getElementById("MenuControl1_txt_Title").value.trim().length > 100) {
                alert("Title length should be less that 100.");
                return false;
            }
            
            if (document.getElementById("MenuControl1_txt_URL").value.trim() == '') {
                alert("Please enter URL.");
                return false;
            }            
            else if (document.getElementById("MenuControl1_txt_URL").value.trim().length > 1000) {
            //menu item URL should be less than 1000 character
                alert("Report URL length should be less that 1000.");
                return false;
            }
            else if (document.getElementById("MenuControl1_txt_URL").value.trim().charAt(0) != '/') {
                alert("Please enter URL starting with ' / '.");
                return false;
            }

        }

    }

    function moveItem(i) {
        
        
        //Move list box Item Down
        var b, o = document.getElementById("MenuControl1_lst_MenuItems"), p, q;
        if (i == 0 && o.selectedIndex > 0) {
            p = o.options[o.selectedIndex];
            q = o.options[o.selectedIndex - 1]
            o.removeChild(p);
            o.insertBefore(p, q);
        }
        //Move list box Item up
        if (i == 1 && o.selectedIndex < o.options.length - 1) {
            b = o.selectedIndex < o.options.length - 2;
            p = o.options[o.selectedIndex];
            if (b) q = o.options[o.selectedIndex + 2]
            o.removeChild(p);
            if (b) o.insertBefore(p, q); else o.appendChild(p);
        }
        
        //Store Ids in hidden field
        var Ids = "";
        for (var i = 0; i < o.options.length; i++) {
            Ids += o.options[i].value;

            if (i != o.options.length - 1) {
                Ids += ',';
            }
        }
        document.getElementById("MenuControl1_hf_OrderedIds").value = Ids;
        return false;
    }
        
</script>

<div id="pnl_MenuControl" runat="server">
    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
        width: 400px" class="clsLeftPaddingTable">
        <tr>
            <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="left"
                colspan="3">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 24px" align="left">
                            &nbsp;<asp:Label ID="lbl_header" Text="Menu" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server" OnClientClick="return closePopup();">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="tr_ListMenuItems" runat="server" width="380px">
            <td colspan="3">
                <table border="0" enableviewstate="true" style="border-color: #123160; border-collapse: collapse;"
                    width="100%">
                    <tr>
                        <td align="left" colspan="2">
                            <asp:ListBox ID="lst_MenuItems" runat="server" Height="90px" Width="300px" CssClass="clsLabel">
                            </asp:ListBox>
                        </td>
                        <td align="center">
                            <asp:HiddenField ID="hf_LevelID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hf_MenuID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hf_Title" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hf_URL" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hf_TimeStamp" runat="server" />
                            <asp:HiddenField ID="hf_OrderedIds" runat="server" />
                            <asp:Button ID="btn_Up" runat="server" Text="Up" Width="80" CssClass="clsbutton"
                                OnClientClick="return moveItem(0);" />
                            <br />
                            <asp:Button ID="btn_Down" runat="server" Text="Down" Width="80" CssClass="clsbutton"
                                OnClientClick="return moveItem(1);" />
                            <asp:Button ID="btnShowAdd" runat="server" Text="" Width="50" CssClass="clsbutton"
                                Style="display: none" OnClick="btnShowAdd_Click" />
                            <asp:Button ID="btnShowUpdate" runat="server" Text="" Width="50" CssClass="clsbutton"
                                Style="display: none" OnClick="btnShowUpdate_Click" />
                            <asp:Button ID="btnListBox" runat="server" Text="" Width="50" CssClass="clsbutton"
                                Style="display: none" OnClick="btnListBox_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="tr_Title" runat="server">
            <td>
                &nbsp;<asp:Label ID="lbl_Title" runat="server" Text="Title" CssClass="clsLabel"></asp:Label>
            </td>
            <td colspan="2">
                &nbsp;<asp:TextBox ID="txt_Title" runat="server" Width="200" CssClass="clsInputadministration"></asp:TextBox>
            </td>
        </tr>
        <tr id="tr_Url" runat="server">
            <td>
                &nbsp;<asp:Label ID="lbl_URL" runat="server" Text="URL" CssClass="clsLabel"></asp:Label>
            </td>
            <td colspan="2">
                &nbsp;<asp:TextBox ID="txt_URL" runat="server" Width="200" CssClass="clsInputadministration"></asp:TextBox>
            </td>
        </tr>
        <tr id="tr_Active" runat="server">
            <td>
                &nbsp;<asp:Label ID="Label1" runat="server" Text="Active" CssClass="clsLabel"></asp:Label>
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chk_Active" Text="" runat="server" CssClass="clsLabel" />
            </td>
        </tr>
        <tr id="tr_IsSubMenuHidden" runat="server">
            <td>
                &nbsp;<asp:Label ID="Label4" runat="server" Text="Hide SubMenu" CssClass="clsLabel"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkIsSubMenuHidden" runat="server" CssClass="clsLabel" />
            </td>
        </tr>
        <tr id="tr_Selected" runat="server">
            <td>
                &nbsp;<asp:Label ID="Label2" runat="server" Text="Selected" CssClass="clsLabel"></asp:Label>
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chk_Selected" Text="" runat="server" CssClass="clsLabel" />
            </td>
        </tr>
        <tr id="tr_Logging" runat="server">
            <td>
                &nbsp;<asp:Label ID="Label3" runat="server" Text="Allow Logging" CssClass="clsLabel"></asp:Label>
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chk_Logging" Text="" runat="server" CssClass="clsLabel" />
            </td>
        </tr>
        
        <tr id="tr_Category" runat="server">
            <td>
                &nbsp;<asp:Label ID="lblCategory" runat="server" Text="Category" CssClass="clsLabel"></asp:Label>
            </td>
            <td colspan="2" class="clsLabel">
                <asp:RadioButtonList ID="rbCategory" runat="server" CssClass="clsLeftPaddingTable" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Alert" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Report" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        
        <tr>
            <td colspan="3" height="20" align="right">
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="80" CssClass="clsbutton"
                    OnClick="btnAdd_Click" Style="display: block" OnClientClick="return ValidateAdd();" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="80" CssClass="clsbutton"
                    OnClick="btnUpdate_Click" Style="display: block" OnClientClick="return ValidateUpdate();" />
            </td>
        </tr>
    </table>
</div>

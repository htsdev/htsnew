﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigation.ascx.cs"
    Inherits="HTP.WebControls.Navigation" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet">

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    function ValidateControl()
	{
	    var callBackDate=document.getElementById("<%= this.ClientID %>_CallBackDate");
	    if(callBackDate.style.display !="none" )
	    {
	    //Yasir Kamal 7654 04/05/2010 don't check callback date when 'No' is selected as follow-up.
	        if(document.getElementById("<%= this.ClientID %>_optFollowUp_0").checked == true)
	        {
		    dayofweek=weekdayName(callBackDate.value,true); 
            if (dayofweek=="Sat" || dayofweek=="Sun")
            {
                alert("Please select any working day.");
                callBackDate.focus();
                return false;
            }
            }
        }
        
        var cmbFollowUp = document.getElementById("<%= this.ClientID %>_cmbFollowUp");
        
        if (document.getElementById("<%= this.ClientID %>_optFollowUp_0").checked == true)
        {
            if(cmbFollowUp.value == 1 || cmbFollowUp.value ==5 || cmbFollowUp.value == 14)
            {
                alert("Please Select Follow Up Call");
                cmbFollowUp.focus();
                return false;
            }
        }
        
        var gComments=document.getElementById("<%= this.ClientID %>_WCC_GeneralComments_txt_Comments");
        var comments = document.getElementById("<%= this.ClientID %>_WCC_GeneralComments");
	   
        gComments.value=trim(gComments.value);
		 
        if(gComments.value.length==0)
        {
            alert("Please enter General Comments.");
		     
            if (gComments.offsetHeight ==0)
            {		 
                var com =  comments.getElementsByTagName("a");
	
                if(com[0].innerHTML=='Edit')
                {		    
                    com[0].innerHTML='Cancel';
                }
            
                gComments.style.display = 'block';
            }
		     
            if (gComments.offsetHeight > 0)
            {
                gComments.focus();
            }
            return false;		    
        }
		 	    			    
        // Prompt for future date if follow-up = Yes.
        if (document.getElementById("<%= this.ClientID %>_optFollowUp_0").checked == true)
        {
            var d1 = callBackDate.value;
			var d2 = document.getElementById("<%= this.ClientID %>_hf_CurrentDate").value;
            if (compareDates(d1,'MM/dd/yyyy',d2,'MM/dd/yyyy')==false)
            {
                alert("Please enter valid future date for callback");
                callBackDate.focus();
                return false;
            }
        }
        
        return true;
	}	
</script>

<style type="text/css">
    .style1
    {
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 140px;
    }
    .style2
    {
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 140px;
    }
</style>
<div id="pnl_control" runat="server">
    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table border="2" class="clsLeftPaddingTable" enableviewstate="true" style="border-color: navy;
                border-collapse: collapse; width: 610px;">
                <tr>
                    <td colspan="2" background="../Images/subhead_bg.gif" valign="top">
                        <table width="100%" height="31px">
                            <tr>
                                <td align="left" style="width: 50%">
                                    &nbsp;<asp:Label ID="lbl_head" runat="server" CssClass="clssubhead"></asp:Label>
                                </td>
                                <td align="right" style="width: 50%">
                                    <asp:LinkButton ID="lbtn_close" runat="server" OnClientClick="return HidePopup();"
                                        OnClick="lbtn_close_Click">X</asp:LinkButton>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="2" height="11">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" valign="middle">
                        <asp:Label ID="lblMessage" Visible="false" runat="server" Text="" Font-Names="Arial"
                            Font-Size="X-Small" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style1" style="font-weight: bold;" valign="middle">
                        &nbsp;Client Name:
                    </td>
                    <td class="clsLeftPaddingTable" valign="middle">
                        <asp:Label ID="lblClientName" runat="server" CssClass="clsLabel"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle">
                        &nbsp;Contact No:
                    </td>
                    <td class="clsLeftPaddingTable" valign="middle">
                        <asp:Label ID="lblClientContact" runat="server" CssClass="clsLabel"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle">
                        &nbsp;Language:
                    </td>
                    <td class="clsLeftPaddingTable" valign="middle">
                        <asp:Label ID="lblClientLanguage" runat="server" CssClass="clsLabel"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle">
                        &nbsp;Quote Amount:
                    </td>
                    <td class="clsLeftPaddingTable" valign="middle">
                        <asp:Label ID="lblQuoteAmount" runat="server" CssClass="clsLabel"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="11">
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <div id="divGrid" runat="server" style="overflow: auto; height: 100px; width: 600px;">
                            <asp:GridView ID="gvClientInfo" runat="server" CssClass="clsLeftPaddingTable" Width="580px"
                                BorderColor="Gray" BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Cause Number">
                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" Width="9%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CauseNo" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("CauseNumber") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ticket Number">
                                        <HeaderStyle Width="9%" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Labe66" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Matter Description">
                                        <HeaderStyle Width="13%" HorizontalAlign="Center" CssClass="clsaspcolumnheader">
                                        </HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Crt Date">
                                        <HeaderStyle Width="7%" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_courtdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDate","{0:d}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Crt">
                                        <HeaderStyle Width="3%" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Crt" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container,"DataItem.CourtNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Status" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container,"DataItem.AutoCourtDesc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Crt Location">
                                        <HeaderStyle Width="8%" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Loc" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                            <asp:HiddenField ID="hfTicketId" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="11">
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle">
                        &nbsp;Follow-Up Call:
                    </td>
                    <td class="clsLeftPaddingTable" valign="middle">
                        <asp:DropDownList ID="cmbFollowUp" runat="server" CssClass="clsInputCombo" Width="180px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle">
                        &nbsp;Follow-Up:
                    </td>
                    <td class="clsLeftPaddingTable" valign="middle">
                        <asp:RadioButtonList ID="optFollowUp" runat="server" Width="104px" RepeatDirection="Horizontal"
                            Font-Names="Tahoma" Font-Size="8pt">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle" >
                        &nbsp;General Comments:
                    </td>
                    <td class="clsLeftPaddingTable" valign="top" >                    
                        <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Width="455px" Height="120px" />                    
                    </td>
                </tr>

                <tr>
                    <td class="style2" style="font-weight: bold;" valign="middle">
                        &nbsp;Call Back Date:
                    </td>
                    <td class="clsLeftPaddingTable" style="width: 339px">
                        &nbsp;<ew:CalendarPopup ID="CallBackDate" runat="server" AllowArbitraryText="False"
                            CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="True"
                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                            PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" ToolTip="Call Back Date"
                            UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                        </ew:CalendarPopup>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" Width="60px" Text="Update"
                            OnClick="btnUpdate_Click" OnClientClick="return ValidateControl();"></asp:Button>&nbsp;&nbsp;
                        <asp:HiddenField ID="hf_CurrentDate" runat="server" />
                        <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="2" height="11">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:ImageButton ID="imgFirst" runat="server" ImageUrl="../Images/MoveFirst.gif"
                            OnClick="imgFirst_Click" ToolTip="First Record" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="imgPrev" runat="server" ImageUrl="../Images/MovePrevious.gif"
                            OnClick="imgPrev_Click" ToolTip="Previous Record" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="imgNext" runat="server" ImageUrl="../Images/MoveNext.gif" OnClick="imgNext_Click"
                            ToolTip="Next Record" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="imgLast" runat="server" ImageUrl="../Images/MoveLast.gif" OnClick="imgLast_Click"
                            ToolTip="Last Record" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="2" height="11">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="imgFirst" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="imgPrev" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="imgNext" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="imgLast" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
</div>

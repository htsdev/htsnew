﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using lntechNew.com.legalhouston.zp4;
using DataTable = System.Data.DataTable;

namespace lntechNew.WebControls
{
    // Rab Nawaz Khan 10431 10/02/2013 Added the User control to update the Bad mailer Marked non-client addresses.
    /// <summary>
    /// BadAddressUpdate Class
    /// </summary>
    public partial class BadAddressUpdate : System.Web.UI.UserControl
    {

        /// <summary>
        /// PageMethodHandler event
        /// </summary>
        public event PageMethodHandler PageMethod = null;
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_zip1.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            txt_zip2.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
        }

        /// <summary>
        /// Used to display the Popup on report to display the control
        /// </summary>
        /// <param name="mailerDate">string Mailer Date</param>
        /// <param name="letterId">int LetterId</param>
        /// <param name="recordId">int RecordId</param>
        /// <param name="newAddress1">string Address1</param>
        /// <param name="newCity1">string city1</param>
        /// <param name="newState1">string state1</param>
        /// <param name="newZipcode1">string zipcode1</param>
        /// <param name="newAddress2">string address2</param>
        /// <param name="newCity2">string city2</param>
        /// <param name="newState2">string state2</param>
        /// <param name="newZipcode2">string zipcode2</param>
        /// <param name="lastName">string lastName</param>
        /// <param name="firstName">string firstName</param>
        /// <param name="existingAddress">string ExistingAddress</param>
        /// <param name="noteId">int NoteId</param>
        /// <param name="dt">DataTable dt_states</param>
        /// <param name="modelPopupId">string modelPopupClientId</param>
        /// <param name="employeeId">int employeeId</param>
        /// <param name="isNewAddress2Updated">bit isAddress2Toupdate</param>
        /// <param name="causeNumbers">string CauseNumbers</param>
        public void CallPopUp(string mailerDate, int letterId, int recordId, string newAddress1, string newCity1, int newState1, string newZipcode1,
            string newAddress2, string newCity2, int newState2, string newZipcode2, string lastName, string firstName, string existingAddress,
            int noteId, DataTable dt, string modelPopupId, int employeeId, bool isNewAddress2Updated, string causeNumbers)
        {
            lbl_FullName.Text = firstName + " " + lastName;
            lbl_existingAddress.Text = existingAddress;
            txt_Address1.Text = newAddress1.Equals("N/A") ? "" : newAddress1;
            txt_city1.Text = newCity1.Equals("N/A") ? "" : newCity1;
            ddl_stat1.DataSource = dt;
            ddl_stat1.DataTextField = "State";
            ddl_stat1.DataValueField = "stateID";
            ddl_stat1.DataBind();
            ddl_stat1.SelectedIndex = newState1;
            txt_zip1.Text = newZipcode1.Equals("N/A") ? "" : newZipcode1;

            txt_Address2.Text = newAddress2.Equals("N/A") ? "" : newAddress2;
            txt_city2.Text = newCity2.Equals("N/A") ? "" : newCity2;
            ddl_stat2.DataSource = dt;
            ddl_stat2.DataTextField = "State";
            ddl_stat2.DataValueField = "stateID";
            ddl_stat2.DataBind();
            ddl_stat2.SelectedIndex = newState2;
            txt_zip2.Text = newZipcode2.Equals("N/A") ? "" : newZipcode2;
            lbl_CauseNumbers.Text = causeNumbers;
            txt_Address2.Enabled = false;
            txt_city2.Enabled = false;
            ddl_stat2.Enabled = false;
            txt_zip2.Enabled = false;
            chk_Address2Exists.Checked = false;
            chk_Address1Exists.Checked = true;
            
            // Enable the control if we found the address to exists. . . 
            if (isNewAddress2Updated)
            {
                txt_Address2.Enabled = true;
                txt_city2.Enabled = true;
                ddl_stat2.Enabled = true;
                txt_zip2.Enabled = true;
                chk_Address2Exists.Checked = true;
            }

            hf_firstName.Value = firstName;
            hf_lastName.Value = lastName;
            hf_recordid.Value = recordId.ToString(CultureInfo.InvariantCulture);
            hf_newAddress1.Value = newAddress1;
            hf_newCity1.Value = newCity1;
            hf_newState1.Value = newState1.ToString(CultureInfo.InvariantCulture);
            hf_newZip1.Value = newZipcode1;
            hf_newAddress2.Value = newAddress2;
            hf_newCity2.Value = newCity2;
            hf_newState2.Value = newState2.ToString(CultureInfo.InvariantCulture);
            hf_newZip2.Value = newZipcode2;
            hf_modelPopupId.Value = modelPopupId;
            hf_employeeId.Value = employeeId.ToString(CultureInfo.InvariantCulture);
            lbtn_close.OnClientClick = "return CloseModalPopupExtender('" + modelPopupId + "');"; 
        }

        /// <summary>
        /// Update Button Click Event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Update_Click(object sender, EventArgs e)
        {
            
            ClsNonClientsManager mgr = new ClsNonClientsManager();

            string address2 = string.Empty;
            string city2 = string.Empty;
            int state2 = 0;
            string zipcode2 = string.Empty;
            string state2ZP4 = string.Empty;
            string DPC_1 = string.Empty;
            string DP2_1 = string.Empty;
            string DPC_2 = string.Empty;
            string DP2_2 = string.Empty;
            string zp4AddressStatus1 = string.Empty;
            string zp4AddressStatus2 = string.Empty;

            int recordid = Convert.ToInt32(hf_recordid.Value);
            string firstName = hf_firstName.Value;
            string lastName = hf_lastName.Value;
            string address1 = txt_Address1.Text.Trim();
            string city1 = txt_city1.Text.Trim();
            int state1 = Convert.ToInt32(ddl_stat1.SelectedValue);
            string zipcode1 = txt_zip1.Text.Trim();
            string state1ZP4 = ddl_stat1.SelectedItem.Text;
            int employeeId = Convert.ToInt32(hf_employeeId.Value);
            bool isAddress2Exists = chk_Address2Exists.Checked;


            System.Collections.Generic.Dictionary<string, string> dictionaryAddress1 = ProcessAddressesThroughZP4(address1, city1, state1ZP4, zipcode1);

            if (dictionaryAddress1.ContainsKey("AddressStatus"))
                zp4AddressStatus1 = dictionaryAddress1["AddressStatus"];

            if (dictionaryAddress1["AddressStatus"] != "N")
            {
                if (dictionaryAddress1.ContainsKey("Address"))
                    address1 = dictionaryAddress1["Address"];
                if (dictionaryAddress1.ContainsKey("City"))
                    city1 = dictionaryAddress1["City"];
                if (dictionaryAddress1.ContainsKey("State"))
                {
                    state1ZP4 = dictionaryAddress1["State"];
                    if (ddl_stat1.SelectedItem.Text.Equals(state1ZP4))
                        state1 = Convert.ToInt32(ddl_stat1.SelectedItem.Value);
                }
                if (dictionaryAddress1.ContainsKey("ZipCode"))
                    zipcode1 = dictionaryAddress1["ZipCode"];
                if (dictionaryAddress1.ContainsKey("DP2"))
                    DP2_1 = dictionaryAddress1["DP2"];
                if (dictionaryAddress1.ContainsKey("DPC"))
                    DPC_1 = dictionaryAddress1["DPC"];
            }
            
            if (chk_Address2Exists.Checked)
            {
                address2 = txt_Address2.Text.Trim();
                city2 = txt_city2.Text.Trim();
                state2 = Convert.ToInt32(ddl_stat2.SelectedValue);
                zipcode2 = txt_zip2.Text.Trim();
                state2ZP4 = ddl_stat2.SelectedItem.Text;
                System.Collections.Generic.Dictionary<string, string> dictionaryAddress2 = ProcessAddressesThroughZP4(address2, city2, state2ZP4, zipcode2);

                if (dictionaryAddress2.ContainsKey("AddressStatus"))
                    zp4AddressStatus2 = dictionaryAddress2["AddressStatus"];

                if (dictionaryAddress2["AddressStatus"] != "N")
                {
                    if (dictionaryAddress2.ContainsKey("Address"))
                        address2 = dictionaryAddress2["Address"];
                    if (dictionaryAddress2.ContainsKey("City"))
                        city2 = dictionaryAddress2["City"];
                    if (dictionaryAddress2.ContainsKey("State"))
                    {
                        state2ZP4 = dictionaryAddress2["State"];
                        if(ddl_stat2.SelectedItem.Text.Equals(state2ZP4))
                            state2 = Convert.ToInt32(ddl_stat2.SelectedItem.Value);
                    }
                        
                    if (dictionaryAddress2.ContainsKey("ZipCode"))
                        zipcode2 = dictionaryAddress2["ZipCode"];
                    if (dictionaryAddress2.ContainsKey("DP2"))
                        DP2_2 = dictionaryAddress2["DP2"];
                    if (dictionaryAddress2.ContainsKey("DPC"))
                        DPC_2 = dictionaryAddress2["DPC"];
                }
            }
            
            mgr.UpdateNewAddress(recordid, firstName, lastName, address1, city1, state1, zipcode1, DP2_1, DPC_1, zp4AddressStatus1, 
                address2, city2, state2, zipcode2, DP2_2, DPC_2, zp4AddressStatus2, employeeId, isAddress2Exists);

            if (PageMethod != null)
                PageMethod();

        }

        /// <summary>
        /// Used to Process the Address Verification through ZP4 service. . . 
        /// </summary>
        /// <param name="address">string Address</param>
        /// <param name="city">string CityName</param>
        /// <param name="state">string State</param>
        /// <param name="zipcode">string Zipcode</param>
        /// <returns>System.Collections.Generic.Dictionary string, string</returns>
        private System.Collections.Generic.Dictionary<string, string> ProcessAddressesThroughZP4(string address,
            string city, string state, string zipcode)
        {
            System.Collections.Generic.Dictionary<string, string> dictionary =
                    new System.Collections.Generic.Dictionary<string, string>();
            try
            {
                Service1 zp4ServiceProcessing = new Service1();
                DataSet dsAdresses = zp4ServiceProcessing.getYDS(address, city, state, zipcode);

                if (dsAdresses.Tables[0].Rows[0]["success"].ToString() == "true")
                {
                    DataRow drYds  = dsAdresses.Tables[0].Rows[0];
                    if (drYds["YDSstrdpv"].ToString() != "N")
                    {
                        if (drYds["YDSstraddress"].ToString() != "")
                            dictionary.Add("Address", drYds["YDSstraddress"].ToString());

                        if (drYds["YDSstrcity"].ToString() != "")
                            dictionary.Add("City", drYds["YDSstrcity"].ToString());

                        if (drYds["YDSstrstate"].ToString() != "")
                            dictionary.Add("State", drYds["YDSstrstate"].ToString());

                        if (drYds["YDSstrzip"].ToString() != "")
                            dictionary.Add("ZipCode", drYds["YDSstrzip"].ToString());
                    }
                    if (drYds["YDSstrbarcode"].ToString() != "")
                    {
                        try
                        {
                            dictionary.Add("DP2", drYds["YDSstrbarcode"].ToString().Substring(9, 2));
                        }
                        catch
                        {
                            if (dictionary.ContainsKey("DP2"))
                                dictionary.Remove("DP2");
                            dictionary.Add("DP2", "0");
                        }

                        try
                        {
                            dictionary.Add("DPC", drYds["YDSstrbarcode"].ToString().Substring(11, 1));
                        }
                        catch
                        {
                            if (dictionary.ContainsKey("DPC"))
                                dictionary.Remove("DPC");
                            dictionary.Add("DPC", "0");
                        }
                    }
                    dictionary.Add("AddressStatus", drYds["YDSstrdpv"].ToString());
                }
                else
                {
                    if (dictionary.ContainsKey("AddressStatus"))
                        dictionary.Remove("AddressStatus");
                    dictionary.Add("AddressStatus", "N");
                }
            }
            catch (Exception ex)
            {
                if (dictionary.ContainsKey("AddressStatus"))
                    dictionary.Remove("AddressStatus");
                dictionary.Add("AddressStatus", "N");
            }
            return dictionary;
        }
    }
}
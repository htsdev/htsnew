﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateEmailAddress.ascx.cs"
    Inherits="lntechNew.WebControls.UpdateEmailAddress" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">


function ClosePopUp(backdiv,frontdiv)
{
	
    document.getElementById(backdiv).style.display="none";        
    document.getElementById(frontdiv).style.display="none";
	
    return false;
}


function trimAll(sString) 
{
	while (sString.substring(0,1) == ' ')
    {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length-1, sString.length) == ' ')
    {
        sString = sString.substring(0,sString.length-1);
    }
    return sString;
}

function ShowPopUp(ticketid)
{
	
	var backdiv = '<%=this.Disable.ClientID %>';
	var frontdiv = '<%= this.pnl_control.ClientID %>';

   document.getElementById('<%= this.ClientID %>_txt_GeneralComments').value = "";      

   setpopuplocation(backdiv,frontdiv);

	var dt = new Date();
   document.getElementById('<%= this.ClientID %>_cal_followupdate').value=newdate;
   document.getElementById('<%=this.hf_ticketid.ClientID %>').value = ticketid;
   return false;  
}

function verifyEmail()
{
	
    var status = false;     
    
	var email = document.getElementById('<%= this.ClientID %>_txt_Email').value;
	var AtPos = email.indexOf("@");
	var StopPos = email.lastIndexOf(".");
    
	if (AtPos == -1 || StopPos == -1) {
          alert("Please enter a valid email address.");
         status = false;
      }
     else {
          status = true;
     }
     return status;
}

function Validateinput()
{

	var checkmail = verifyEmail();

	if (checkmail == true)
	{
		var Gen = document.getElementById('<%= this.ClientID %>_txt_GeneralComments').value;

		if (Gen.trim() == "")
		{
			alert("Please add comments.");
			document.getElementById('<%= this.ClientID %>_txt_GeneralComments').focus();
			return false;
		}

		return true;
	}
	else
	{
		return false;
	}
}

function Setpopup(Type,ticketid,firstname,lastname,email,contactnum1,contactnum2,contactnum3,genComments,isContacted,reviewedEmailStatus)
{
	
	
	var backdiv = '<%=this.Disable.ClientID %>';
	var frontdiv = '<%= this.pnl_control.ClientID %>';
      
   
   if(Type == "UpdateEmailAddress")
   {
       
       document.getElementById('<%= this.ClientID %>_trClientName').style.display = "";
       document.getElementById('<%= this.ClientID %>_trEmail').style.display = "";
       document.getElementById('<%= this.ClientID %>_trContactNumber1').style.display = "";
   	   document.getElementById('<%= this.ClientID %>_trContactNumber2').style.display = "";
       document.getElementById('<%= this.ClientID %>_trContactNumber3').style.display = "";
       
       //For Ticket ID
       document.getElementById('<%=this.hf_ticketid.ClientID %>').value = ticketid;
       //For First Last Name
       document.getElementById('<%= this.ClientID %>_lblClientName').innerHTML = firstname + " " + lastname;
       
       //For Ticket Number

       
       // For Email Address
       document.getElementById('<%= this.ClientID %>_txt_Email').value = email;
       
       // For Contact Numbers
       document.getElementById('<%= this.ClientID %>_lblContactNumber1').innerHTML = contactnum1;
       document.getElementById('<%= this.ClientID %>_lblContactNumber2').innerHTML = contactnum2;
       document.getElementById('<%= this.ClientID %>_lblContactNumber3').innerHTML = contactnum3;
       
       //For General Comments
       document.getElementById('<%= this.ClientID %>_lbl_GeneralComments').innerHTML = genComments;
   	   
   	  //Check for contacted Page for GeneralInfo and bad email page
       document.getElementById('<%= this.ClientID %>_hf_isContacted').value = isContacted;
   	
   	  //For Reviewed Email Status 
       document.getElementById('<%= this.ClientID %>_ddl_callback').value = reviewedEmailStatus;
   	
   	
   	
   	

   	
       setpopuplocation(backdiv,frontdiv);
   }
}


function SetpopupForVerifyEmail(Type,ticketid,firstname,lastname,email,genComments,isContacted)
{
	
	var backdiv = '<%=this.Disable.ClientID %>';
	var frontdiv = '<%= this.pnl_control.ClientID %>';
    
    if(Type == "VerifyEmailAddress")
    {    
    
        
        var row = document.getElementById("UpdateEmailAddress1_trCallBack");
        row.style.display = 'none';
        document.getElementById('UpdateEmailAddress1_hf_VerifyEmailCallBackStatus').value = '<%=this.CallFrom %>';

        document.getElementById('<%= this.ClientID %>_trClientName').style.display = "";
        document.getElementById('<%= this.ClientID %>_trEmail').style.display = "";
        //For Ticket ID
        document.getElementById('<%=this.hf_ticketid.ClientID %>').value = ticketid;
        //For First Last Name
        document.getElementById('<%= this.ClientID %>_lblClientName').innerHTML = firstname + " " + lastname;
        // For Email Address
        document.getElementById('<%= this.ClientID %>_txt_Email').value = email;
       
    	//For General Comments
        document.getElementById('<%= this.ClientID %>_lbl_GeneralComments').innerHTML = genComments;
        
    	//Check for contacted Page for GeneralInfo and bad email page
        document.getElementById('<%= this.ClientID %>_hf_isContacted').value = isContacted;

        setpopuplocation(backdiv,frontdiv);
    }
        
}

function ShowPopUpForAll(Type,ticketid,firstname,lastname,email,contactnum1,contactnum2,contactnum3,genComments,isContacted,reviewedEmailStatus)

{
	
	
	var backdiv = '<%=this.Disable.ClientID %>';
	var frontdiv = '<%= this.pnl_control.ClientID %>';
   Setpopup(Type,ticketid,firstname,lastname,email,contactnum1,contactnum2,contactnum3,genComments,isContacted,reviewedEmailStatus);
   setpopuplocation(backdiv,frontdiv);
   return false;
     
}

function ShowPopUpForVerifyEmail(Type,ticketid,firstname,lastname,email,genComments,isContacted)
{

	var backdiv = '<%=this.Disable.ClientID %>';
	var frontdiv = '<%= this.pnl_control.ClientID %>';
   SetpopupForVerifyEmail(Type,ticketid,firstname,lastname,email,genComments,isContacted);
   setpopuplocation(backdiv,frontdiv);
   return false;
     
}





// Set Popup Control Location
    function setpopuplocation(backdiv,frontdiv)
	{	
    	
        var top  = 200;
        var left = 200;
        var height = document.body.offsetHeight;
        var width  = document.body.offsetWidth;
        

        if ( width > 1100 || width <= 1280) left = 475;
        if ( width < 1100) left = 500;

        document.getElementById(frontdiv).style.top =top+'px';
	    document.getElementById(frontdiv).style.left =left+'px';
				
	    if (  document.body.offsetHeight > document.body.scrollHeight )
	    {
		    document.getElementById(backdiv).style.height = document.body.offsetHeight;		    
		}
	    else
	    {
	        document.getElementById(backdiv).style.height = document.body.scrollHeight;	        
		}	
	    document.getElementById(backdiv).style.width = document.body.offsetWidth;
    	document.getElementById(backdiv).style.display = '';
    	document.getElementById(frontdiv).style.display = '';
	    
	    document.body.scrollTop= 0;
	}
	
	


    

</script>

<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50);
    left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<div id="pnl_control" runat="server">
    <table border="2" style="border-left-color: navy; border-bottom-color: navy; border-top-color: navy;
        border-collapse: collapse; border-right-color: navy" enableviewstate="true" style="border-color: navy;
        border-collapse: collapse; width: 425px;">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="top">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 26px" align="left">
                            <asp:Label ID="lbl_head" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" width="100%">
                    <tr id="trClientName" runat="server" style="display: none" align="left">
                        <td class="clssubhead">
                            Name :
                        </td>
                        <td style="height: 30px">
                            <asp:Label ID="lblClientName" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trEmail" runat="server" style="display: none" align="left">
                        <td class="clssubhead">
                            Email Address :
                        </td>
                        <td style="height: 30px">
                            <asp:TextBox ID="txt_Email" runat="server" CssClass="clsInputadministration" Width="200px"
                                CausesValidation="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trContactNumber1" runat="server" style="display: none" align="left">
                        <td class="clssubhead">
                            Contact Numbers :
                        </td>
                        <td style="height: 30px">
                            <asp:Label ID="lblContactNumber1" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trContactNumber2" runat="server" style="display: none" align="left">
                        <td class="clssubhead">
                        </td>
                        <td style="height: 30px">
                            <asp:Label ID="lblContactNumber2" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trContactNumber3" runat="server" style="display: none" align="left">
                        <td class="clssubhead">
                        </td>
                        <td style="height: 30px">
                            <asp:Label ID="lblContactNumber3" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trCallBack" runat="server" align="left">
                        <td class="clssubhead">
                            Call Back Status :
                        </td>
                        <td style="height: 30px" class="clssubhead">
                            <asp:DropDownList ID="ddl_callback" runat="server" CssClass="clsInputCombo">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="clssubhead" style="height: 30px">
                            Comments :
                        </td>
                    </tr>
                    <tr id="tr1" runat="server" align="left">
                        <td style="height: 85px" colspan="2">
                            <table style="border-color: navy; border-collapse: collapse;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_GeneralComments" CssClass="clsLeftPaddingTable" runat="server"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 425px" align="left">
                                        <asp:TextBox ID="txt_GeneralComments" runat="server" Height="50px" TextMode="MultiLine"
                                            Width="420px" CssClass="clsInputadministration"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="center" style="height: 30px" colspan="2">
                            &nbsp;<asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" Text="Update"
                                Width="55px" OnClick="btnUpdate_Click" OnClientClick="return Validateinput();" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" Text="Cancel" Width="53px" />&nbsp;
                            <%--  OnClick="btnsave_Click" OnClientClick="return Validateinput();"   OnClientClick="return Validateinput();" OnClick="btnsave_Click" OnClientClick="return Validateinput();"--%>
                            <asp:HiddenField ID="hf_VerifyEmailCallBackStatus" runat="server" Value=" " />
                            <asp:HiddenField ID="hf_ticketid" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_isContacted" runat="server" Value="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

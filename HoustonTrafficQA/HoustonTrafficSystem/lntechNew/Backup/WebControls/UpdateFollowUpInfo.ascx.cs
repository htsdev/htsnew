using System;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
// Abid Ali 5425 1/20/2009 adding namespace
using HTP.Components;


namespace HTP.WebControls
{
    //Ozair 8101 09/29/2010 code refactored where required
    /// <summary>
    /// 
    /// </summary>
    public delegate void PageMethodHandler();
    public partial class UpdateFollowUpInfo : System.Web.UI.UserControl
    {
        #region Variables
        bool _freezecalender;

        /// <summary>
        /// 
        /// </summary>
        public event PageMethodHandler PageMethod = null;

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set
            {
                lbl_head.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Freezecalender
        {
            get { return _freezecalender; }
            set { _freezecalender = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public FollowUpType followUpType
        {
            set
            {
                hf_FollowUpType.Value = value.ToString();
            }
        }
        //Zeeahan 4420 08/06/2008 Add Property to Avoid Drop Down Checking by Calender Control
        /// <summary>
        /// 
        /// </summary>
        public bool EnableHideDropDown
        {
            set
            {
                this.cal_followupdate.EnableHideDropDown = value;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnsave.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                btnCancel.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                lbtn_close.Attributes.Add("onClick", "return ClosePopUp('" + Disable.ClientID + "','" + pnl_control.ClientID + "');");
                pnl_control.Attributes.Add("style", "z-index: 3; display: none; position: absolute; background-color: transparent; left: 0px; top: 0px;");
            }

            if (DesignMode)
            {
                cal_followupdate.Visible = false;
            }
            else
            {
                cal_followupdate.Visible = true;
            }

        }
        //Noufil 3589 05/27/2008 Function use to set lower and upper date
        /// <summary>
        /// 
        /// </summary>
        /// <param name="upp"></param>
        /// <param name="low"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="ticketno"></param>
        /// <param name="causeno"></param>
        /// <param name="comments"></param>
        /// <param name="PanelID"></param>
        /// <param name="court"></param>
        /// <param name="courtname"></param>
        /// <param name="followUpDate"></param>
        public void binddate(DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string causeno, string comments, string PanelID, string court, string courtname, string followUpDate)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }
                //Ozair 5723 04/03/2009 Full Name in First Last format
                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = ticketno;
                //Zeeshan 4679 09/08/2008  Display Follow up date in a separate label.
                lblCurrentFollup.Text = followUpDate;
                hf_ticid.Value = ticketno;
                hf_ticketid.Value = ticketno;
                lblCauseNo.Text = causeno;
                lblComments.Text = comments;
                cal_followupdate.SelectedDate = upp;
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;
                hf_court.Value = court;
                Hf_field.Value = "1";
                hf_courtname.Value = courtname;
                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        // Added Zeeshan 4481 8/7/2008
        /// <summary>
        /// 
        /// </summary>
        /// <param name="upp"></param>
        /// <param name="low"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="ticketno"></param>
        /// <param name="causeno"></param>
        /// <param name="comments"></param>
        /// <param name="PanelID"></param>
        /// <param name="court"></param>
        /// <param name="ticketid"></param>
        public void binddate(DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string causeno, string comments, string PanelID, string court, int ticketid)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }
                //Ozair 5723 04/03/2009 Full Name in First Last format
                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = ticketno;
                hf_ticid.Value = ticketno;
                hf_ticketid.Value = ticketid.ToString();
                lblCauseNo.Text = causeno;
                // Noufil 5001 10/24/2008 current follow up date set
                //Fahad 8011,8010 07/14/2010  Allow Empty String in Currentfollow update
                lblCurrentFollup.Text = (low.ToShortDateString() == "1/1/1900") ? string.Empty : low.ToShortDateString();
                lblComments.Text = comments;
                // Abid Ali 5018 11/24/2008 - Family Waiting follow up date
                // Abid Ali 4912 11/13/2008 .. Traffic follow up date
                if (court == "Traffic Waiting" || court == "Family Waiting")
                {
                    upp = clsGeneralMethods.GetBusinessDayDate(DateTime.Now, 5);
                }
                cal_followupdate.SelectedDate = upp;
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;
                hf_court.Value = court;
                Hf_field.Value = "1";
                //   this.hf_courtname.Value = courtname;
                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        //Saeed 8101 09/28/2010 method created.
        /// <summary>
        /// This methos is called from the pages where follow up control is used. It used to set values which will be avaible when follow up conrol popup is visible.
        /// </summary>
        /// <param name="noOfBusinessDays">Max number of follow-up date can be set for business days.</param>
        /// <param name="upp">Default follow up date</param>
        /// <param name="low">Next follow up date</param>
        /// <param name="firstname">First Name</param>
        /// <param name="lastname">Last Name</param>
        /// <param name="ticketno">Ticket id</param>
        /// <param name="causeno">Cause number</param>
        /// <param name="comments">Comments</param>
        /// <param name="PanelID">Panel contorl id used to show/hide panel</param>
        /// <param name="court">Court name </param>
        /// <param name="ticketid">Ticket id</param>
        public void binddate(int noOfBusinessDays, DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string causeno, string comments, string PanelID, string court, int ticketid)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }

                //Full Name in First Last format
                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = ticketno;
                hf_ticid.Value = ticketno;
                hf_ticketid.Value = ticketid.ToString();
                lblCauseNo.Text = causeno;

                //Allow Empty String in Currentfollow update
                lblCurrentFollup.Text = (low.ToShortDateString() == "1/1/1900") ? string.Empty : low.ToShortDateString();
                lblComments.Text = comments;
                //Family Waiting follow up date; Traffic follow up date
                if (court == "Traffic Waiting" || court == "Family Waiting")
                {
                    upp = clsGeneralMethods.GetBusinessDayDate(DateTime.Now, 5);
                }
                BusinessDay.Value = noOfBusinessDays.ToString();
                cal_followupdate.SelectedDate = upp;
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;
                hf_court.Value = court;
                Hf_field.Value = "1";

                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        //Waqas 5653 03/21/2009 Binding for No LOR
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BusinessDay"></param>
        /// <param name="upp"></param>
        /// <param name="low"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="ticketno"></param>
        /// <param name="ticketid"></param>
        /// <param name="causeno"></param>
        /// <param name="comments"></param>
        /// <param name="PanelID"></param>
        /// <param name="court"></param>
        /// <param name="courtname"></param>
        /// <param name="followUpDate"></param>
        public void binddate(string BusinessDay, DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string ticketid, string causeno, string comments, string PanelID, string court, string courtname, string followUpDate)
        {
            if (_freezecalender == true)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }
                //Ozair 5723 04/03/2009 Full Name in First Last format
                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = ticketno;
                lblCurrentFollup.Text = followUpDate;
                hf_ticid.Value = ticketno;
                hf_ticketid.Value = ticketid;
                this.BusinessDay.Value = BusinessDay;
                lblCauseNo.Text = causeno;
                lblComments.Text = comments;
                cal_followupdate.SelectedDate = upp;
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;
                //Waqas 5697 03/26/2009 For only HMC FTA Follow up
                if (court == "HMC-FTA")
                {
                    chk_IsHMCFTARemoved.Visible = true;
                    chk_IsHMCFTARemoved.Checked = false;
                }
                hf_court.Value = court;
                Hf_field.Value = "1";
                hf_courtname.Value = courtname;
                // Noufil 5819 05/13/2009 Fetching Subdoctype in Courtname variable to update followupdate.
                ViewState["freestate"] = courtname;
                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        /// <summary>
        /// This is a wrapper method which set the vlaues in hdden field. This method only sets the contactId and then call the already created binddate method to bind other fields.
        /// </summary>
        /// <param name="ContactID">Represents client contact ID</param>
        /// <param name="BusinessDay">Represents maximum number of days which can be set as a follow up date. </param>
        /// <param name="upp">To date range.</param>
        /// <param name="low">From date range.</param>
        /// <param name="firstname">First Name of the client.</param>
        /// <param name="lastname">Last Name of the client</param>
        /// <param name="ticketno">Cleint's ticket number.</param>
        /// <param name="ticketid">Auto generated primary key which uniquly identified ticket number.</param>
        /// <param name="causeno">Cause number</param>
        /// <param name="comments">User comments</param>
        /// <param name="PanelID">Use for show/hide panel.</param>
        /// <param name="court">Internal variable use to handle client side validation, e.g for address validation report its value is 'AddressValidation'</param>
        /// <param name="courtname">Court name</param>
        /// <param name="followUpDate">Follow up date</param>
        public void binddate(string ContactID, string BusinessDay, DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string ticketid, string causeno, string comments, string PanelID, string court, string courtname, string followUpDate)
        {
            this.hf_ContactID.Value = ContactID;
            binddate(BusinessDay, upp, low, firstname, lastname, ticketno, ticketid, causeno, comments, PanelID, court, courtname, followUpDate);
        }

        //Sabir Khan 8488 12/24/2010  method created.
        /// <summary>
        /// This methos is called from the pages where follow up control is used. It used to set values which will be avaible when follow up conrol popup is visible.
        /// </summary>
        /// <param name="noOfBusinessDays">Max number of follow-up date can be set for business days.</param>
        /// <param name="upp">Default follow up date</param>
        /// <param name="low">Next follow up date</param>
        /// <param name="firstname">First Name</param>
        /// <param name="lastname">Last Name</param>               
        /// <param name="comments">Comments</param>
        /// <param name="PanelID">Panel contorl id used to show/hide panel</param>
        /// <param name="court">Court name </param>
        /// <param name="ticketid">Ticket id</param>
        public void binddate(string BusinessDay, DateTime upp, DateTime low, string lastname, string comments, string PanelID, string court, int ticketid)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }

                //Full Name in First Last format
                lbl_FullName.Text = lastname;
                hf_ticketid.Value = ticketid.ToString();
                this.BusinessDay.Value = BusinessDay;
                tr_CauseNumber.Style["Display"] = "none;";
                tr_TicketNumber.Style["Display"] = "none;";
                //Allow Empty String in Currentfollow update
                // Sadaf 9641 12/19/2011 Change the Format From "MM/dd/yyyy".
                lblCurrentFollup.Text = (low.ToShortDateString() == "1/1/1900") ? string.Empty : low.ToString("MM/dd/yyyy");
                lblComments.Text = comments;
                cal_followupdate.SelectedDate = upp;
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;
                hf_court.Value = court;
                Hf_field.Value = "1";

                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        //Zeeshan Haider 10286 08/16/2012  method created.
        /// <summary>
        /// This methos is called from the pages where follow up control is used. It used to set values which will be avaible when follow up conrol popup is visible.
        /// </summary>
        /// <param name="noOfBusinessDays">Max number of follow-up date can be set for business days.</param>
        /// <param name="upp">Default follow up date</param>
        /// <param name="low">Next follow up date</param>
        /// <param name="firstname">First Name</param>
        /// <param name="lastname">Last Name</param>               
        /// <param name="comments">Comments</param>
        /// <param name="PanelID">Panel contorl id used to show/hide panel</param>
        /// <param name="court">Court name </param>
        /// <param name="ticketid">Ticket id</param>
        /// <param name="FollowUpDays">FollowUp Days</param>
        /// <param name="FirstFollowUpDays">First FollowUp Days</param>
        public void binddate(int businessDay, DateTime upp, DateTime low, string firstname, string lastname, string ticketno, string causeno, string comments, string PanelID, string court, int ticketid, int FollowUpDays, int FirstFollowUpDays)
        {
            if (_freezecalender)
            {
                ViewState["freestate"] = _freezecalender;
                if (tb_GeneralComments.Text != string.Empty)
                {
                    tb_GeneralComments.Text = "";
                }
                //Ozair 5723 04/03/2009 Full Name in First Last format
                lbl_FullName.Text = firstname + " " + lastname;
                lblTicketNumber.Text = ticketno;
                hf_ticid.Value = ticketno;
                hf_ticketid.Value = ticketid.ToString();
                lblCauseNo.Text = causeno;
                // Noufil 5001 10/24/2008 current follow up date set
                //Fahad 8011,8010 07/14/2010  Allow Empty String in Currentfollow update
                lblCurrentFollup.Text = (low.ToShortDateString() == "1/1/1900") ? string.Empty : low.ToShortDateString();
                lblComments.Text = comments;
                // Abid Ali 5018 11/24/2008 - Family Waiting follow up date
                // Abid Ali 4912 11/13/2008 .. Traffic follow up date
                if (court == "Traffic Waiting" || court == "Family Waiting")
                {
                    upp = clsGeneralMethods.GetBusinessDayDate(DateTime.Now, businessDay);
                }
                cal_followupdate.SelectedDate = upp;
                pnl_control.Style["display"] = "";
                pnl_control.Style["position"] = "";
                chk_UpdateAll.Visible = false;
                hf_court.Value = court;
                Hf_field.Value = "1";

                // Zeeshan 10286 08/15/2012 Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses
                hf_FollowUpDate.Value = lblCurrentFollup.Text.ToString();
                BusinessDay.Value = businessDay.ToString();
                hf_FollowUpDays.Value = FollowUpDays.ToString();
                hf_FirstFollowUpDays.Value = FirstFollowUpDays.ToString();

                //   this.hf_courtname.Value = courtname;
                btnCancel.Attributes.Remove("onClick");
                lbtn_close.Attributes.Remove("onClick");

                btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
                lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            // Abid Ali 5425 1/20/2009 Refactor all logic in one class
            var cSession = new clsSession();
            var EmpId = int.Parse(cSession.GetCookie("sEmpID", Request));

            var objUpdateFolloUp = new ClsUpdateFollowUpInfo();
            var freeState = (ViewState["freestate"] == null ? string.Empty : ViewState["freestate"].ToString());
            //Waqas 5697 03/26/2009 For only HMC FTA Follow up

            //SAEED 7844 06/02/2010 if FollowUpType is 'AddressValidationReport', call newly created method UpdateAddressValidationFollowUpDate which takes an extra parameter ContactID. 
            if (hf_FollowUpType.Value == FollowUpType.AddressValidationReport.ToString())
            {
                objUpdateFolloUp.UpdateAddressValidationFollowUpDate(Convert.ToInt32(hf_ContactID.Value), Convert.ToInt32(hf_ticketid.Value), EmpId, lblCurrentFollup.Text, cal_followupdate.SelectedDate, tb_GeneralComments.Text);
            }
            else
            {
                objUpdateFolloUp.UpdateFollowUpDate(this.hf_ticketid.Value, EmpId, lblCurrentFollup.Text, hf_FollowUpType.Value, cal_followupdate.SelectedDate, tb_GeneralComments.Text, freeState, hf_ticid.Value, chk_UpdateAll.Checked, chk_IsHMCFTARemoved.Checked);
            }



            if (PageMethod != null)
                PageMethod();
        }

        #endregion

    }

}
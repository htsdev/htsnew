﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInfo.ascx.cs"
    Inherits="HTP.WebControls.ContactInfo" %>

<script language="javascript" type="text/javascript">
 function closeModalPopup(popupid)
    {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }
 function ConfirmClient()
 {
        alert("CID for an active client can not be disassociated.");
        return false;
 }   
 function ConfirmDisAssociation()
 {
        check = confirm("Removing CID. Would you like to continue? (OK = Yes , Cancel = No)"); 
	    if (check == false) 
	    {
		    return false;
	    }
 }
</script>

<link href="../Styles.css" type="text/css" rel="Stylesheet" />
<div id="pnl_CID" runat="server">
    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
        width: 600px">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 24px">
                            <asp:Label ID="lbl_head" Text="Contact Information" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 100%">
                <table cellpadding="1" cellspacing="0" border="1" style="border-collapse: collapse"
                    width="100%" class="clsLeftPaddingTable">
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;CID :
                        </td>
                        <td style="height: 13px; width: 200px;">
                            &nbsp;<asp:Label ID="lblCID" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 100px">
                            &nbsp;Mid Number :
                        </td>
                        <td style="height: 13px; width: 200px;">
                            &nbsp;<asp:Label ID="lblMidNumber" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;First Name :
                        </td>
                        <td style="height: 13px; width: 200px;">
                            &nbsp;<asp:Label ID="lblFirstName" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 100px">
                            &nbsp;Gender :
                        </td>
                        <td style="height: 13px; width: 200px;">
                            &nbsp;<asp:Label ID="lblGenderRace" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 120px">
                            &nbsp;Middle Name :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblMiddleName" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Race :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblRace" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;Last Name :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblLastName" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Height :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblHeight" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;DOB :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblDOB" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Weight :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblWeight" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;DL Number :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblDLNumber" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Hair Color :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblHair" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;DL State :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblDLState" runat="server" CssClass="clsLabel"></asp:Label>                            
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Eye Color :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblEye" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Phone 1 :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblPhone1" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Email :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblEmail" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Phone 2 :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblPhone2" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Language :
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblLanguage" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;Phone 3 :
                        </td>
                        <td style="height: 13px;">
                            &nbsp;<asp:Label ID="lblPhone3" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                        <td class="clssubhead" style="width: 150px">
                            &nbsp;CDL Flag:
                        </td>
                        <td style="height: 13px; width: 225px;">
                            &nbsp;<asp:Label ID="lblCDLFlag" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 70px">
                            &nbsp;Address :
                        </td>
                        <td style="height: 13px;" colspan="3">
                            &nbsp;<asp:Label ID="lblAddress" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 100%" align="center">
                            <asp:HiddenField ID="hdnTicketID" runat="server" />
                            <asp:Button ID="btnDisAssociate" runat="server" CssClass="clsbutton" Text="Disassociate"
                                Width="93px" OnClick="btnDisAssociate_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" Text="Never mind" />
                            <asp:Button ID="btnClose" runat="server" CssClass="clsbutton" Text="Close" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

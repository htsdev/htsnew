<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.AccessDeny" Codebehind="LetterAccessDeny.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Page Access Denied...</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Label id="Label1" style="Z-INDEX: 103; LEFT: 48px; POSITION: absolute; TOP: 368px" runat="server"
				Height="96px" Width="496px"></asp:Label>
			<asp:Image id="Image1" style="Z-INDEX: 104; LEFT: 352px; POSITION: absolute; TOP: 80px" runat="server"
				ImageUrl="Images/accessDenied.jpg" ToolTip="Access denied"></asp:Image>
			<asp:LinkButton id="lnkLogin" style="Z-INDEX: 101; LEFT: 456px; POSITION: absolute; TOP: 20px" runat="server"
				CssClass="normallink">Login</asp:LinkButton>
			<asp:LinkButton id="lnkDefault" style="Z-INDEX: 102; LEFT: 404px; POSITION: absolute; TOP: 20px"
				runat="server" CssClass="normallink">Home</asp:LinkButton>
		</form>
	</body>
</HTML>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanDocket.aspx.cs" Inherits="lntechNew.ReturnedDockets.ScanDocket" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Scan Dockets</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <%=Session["objTwain"].ToString()%>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../Images/separator_repeat.gif" colspan="2" height="11" width="780">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="2" style="width: 780px;
                        height: 35px">
                        &nbsp; Scan Dockets</td>
                </tr>
                <tr>
                    <td align="left" class="clsLeftPaddingTable" colspan="2" height="11">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td id="new" colspan="2">
                        <table width="780">
                            <tr>
                                <td class="clsLeftPaddingTable" style="width: 909px; height: 22px">
                                    Date of Docket:&nbsp;<ew:calendarpopup id="cal_DocketDate" runat="server" allowarbitrarytext="False"
                                        calendarlocation="Bottom" controldisplay="TextBoxImage" culture="(Default)" enablehidedropdown="True"
                                        font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" padsingledigits="True"
                                        showcleardate="True" showgototoday="True" text=" " tooltip="Call Back Date" upperbounddate="12/31/9999 23:59:00"
                                        width="80px"> <TEXTBOXLABELSTYLE CssClass="clstextarea" /><WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Yellow" /><OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray" BackColor="AntiqueWhite" /><GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="LightGoldenrodYellow" /><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Orange" /><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="LightGray" /><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Yellow" /><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /></ew:calendarpopup>
                                    &nbsp;&nbsp; Attorney:
                                    <asp:DropDownList ID="ddl_Attorney" runat="server" CssClass="clsinputcombo">
                                    </asp:DropDownList></td>
                                <td align="left" class="clsLeftPaddingTable" style="width: 380px; height: 22px">
                                    <asp:Button ID="btn_Scan" OnClientClick="return StartScan();" runat="server"
                                            Text="Scan Now" CssClass="clsbutton" OnClick="btn_Scan_Click" />
                                    &nbsp;&nbsp;
                                    <input id="Adf" runat="server" checked="checked" name="Adf" type="checkbox" />Use
                                    Feeder
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="clsLeftPaddingTable" colspan="2">
                                    &nbsp;<asp:Label ID="lblMessage" runat="server" ForeColor="Red" Height="16px"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;
                        </td>
                </tr>
                <tr>
                    <td background="../Images/separator_repeat.gif" colspan="2" height="11" width="780">
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" >
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="visibility: hidden">
                        <asp:TextBox ID="txtImageCount" runat="server"></asp:TextBox><asp:TextBox ID="txtQuery"
                            runat="server"></asp:TextBox><asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox
                                ID="txtsessionid" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtbID" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox></td>
                </tr>
            </table>
    </div>
    </form>
    <DIV id="ErrorString"></DIV>
		<IMG height="1" src="" width="1" name="Img1"> 
		<!-- added by ozair-->
		<SCRIPT language="JavaScript">
function StartScan()
{
    document.getElementById("lblMessage").innerText="";
	if(document.getElementById("ddl_Attorney").value=="-1")
	{
		alert("Plese Select Attorney.");
		document.getElementById("ddl_Attorney").focus();
		return false;
	}

    var sid= document.getElementById("txtsessionid").value;
	var eid= document.getElementById("txtempid").value;
	var path="<%=ViewState["vNTPATHScanDocketTemp"]%>";
	var Type='network';		
	var AutoFeed=1;
	var sel=OZTwain1.SelectScanner();
	if(sel=="Success")
	{
	    if (document.getElementById("Adf").checked == true)
	    {
	        AutoFeed=-1;
	    }	    
	    
	    OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
	    //
	    if (document.getElementById("Adf").checked == false)
	      {
		    var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		    if(x)
		    {
			    //OZTwain1.Acquire(sid,eid,path,Type);
			    StartScan();
		    }	
		    else
		    {
		        document.getElementById("Adf").checked = true;
		    }		
	      }
    }
    else if(sel=="Cancel")
    {
        alert("Operation canceled by user!");
        return false;
    }
    else 
    {
        alert("Scanner not installed.");
	    return false;
    }
    return true;
}
		</SCRIPT>
		<!-- added by ozair-->
</body>
</html>



//// REPORT CREATED BY FAHAD FOR DIPLAYING CRYSTAL REPORT DOCUMENT
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Collections.Specialized;
using System.Configuration;

namespace lntechNew.Reports
{
    public partial class RptCaseSummary : System.Web.UI.Page
    {
        string ticketID;
        string RptSetting;
        int empid;
        
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsENationWebComponents cls_db = new clsENationWebComponents();
        clsSession uSession = new clsSession();
        clsLogger clog = new clsLogger();
    
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["NTPATHSummary"] = ConfigurationSettings.AppSettings["NTPATHBatchPrint"].ToString();
            ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH"].ToString();

            ticketID = Convert.ToString(Request.QueryString["ticketID"]);

            RptSetting = Convert.ToString(Request.QueryString["setting"]);

            ViewState["vEmpID"] = uSession.GetCookie("sEmpID", this.Request);

          



           
            CreateReport();


            

        }

       
        public void CreateReport()
        {

            DataSet ds;
            string name = ticketID.ToString() + "-CoverSheet-" + DateTime.Now.ToFileTime() + ".pdf";
          
            String[] reportname = new String[] { "Notes.rpt", "Matters.rpt" };
          
            string filename = Server.MapPath("") + "\\CaseSummary.rpt";
           

            string[] keyssub = { "@TicketID"};
            object[] valuesub = { ticketID };
            ds = cls_db.Get_DS_BySPArr("USP_HTS_GET_SubReports", keyssub, valuesub);


            string path = ViewState["NTPATHSummary"].ToString();
            string[] key = { "@TicketId_pk", "@Sections" };
            object[] value1 = { ticketID, RptSetting };

            

            Cr.CreateSubReports(filename, "usp_hts_get_casesummary_report_data", key, value1, reportname, ds, 1, this.Session, this.Response,path,ticketID,true,name.ToString());

           
             




        }
    }
}

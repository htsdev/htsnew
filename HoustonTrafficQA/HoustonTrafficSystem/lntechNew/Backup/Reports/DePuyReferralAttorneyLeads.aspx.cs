﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    /// <summary>
    /// Depuy Referral attorney class
    /// </summary>
    public partial class DePuyReferralAttorneyLeads : Page
    {
        #region Variable
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsSession _clsSession = new clsSession();
        DataSet _ds;
        #endregion
        
        #region Events
        
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Args</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    {
                        if (!IsPostBack)
                        {
                            chkShowAll.Checked = false;
                            cal_FromDateFilter.SelectedDate = DateTime.Now;

                            cal_ToDateFilter.SelectedDate = DateTime.Now;
                            FillGrid();
                        }
                    }

                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_Records;
                    const string javascript = "EnabledPaymentsFilterControls('{0}','{1}','{2}');";
                    chkShowAll.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkShowAll.ClientID,1));


                    if (chkShowAll.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledPaymentsFilterControls('{0}','{1}','{2}');</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkShowAll.ClientID));
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Args</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion
        
        #region Methods
        
        /// <summary>
        /// Populate gridview by fatching data from trafficTickets db
        /// </summary>
        private void FillGrid()
        {
            string[] keys = { "@ShowAllRecords", "@fromDate", "@toDate" };
            object[] values = { chkShowAll.Checked, cal_FromDateFilter.SelectedDate, cal_ToDateFilter.SelectedDate };
            _reports.getRecords("USP_HTP_GetDePuyReferralAttorneyLeads", keys, values);
            _ds = _reports.records;
            if (_ds.Tables[0].Rows.Count > 0)
            {
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = _ds.Tables[0];
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                
            }
            else
            {
                gv_Records.DataSource = null;
                gv_Records.DataBind();
                
            }
        }
        
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();

        }
        
        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Page Event Args</param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        
        /// <summary>
        /// Explicitly binding rows with database fields.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Row Event Args</param>
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string Email = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "EmailAddress"));
            Label lbl_Email = (Label)e.Row.FindControl("lbl_Email");
            if (lbl_Email != null)
            {
                if (Email.Length > 18)
                {
                    lbl_Email.Text = Email.Substring(0, 16) + "..";
                    lbl_Email.ToolTip = Email;
                }
                else
                {
                    lbl_Email.Text = Email;
                }
            }
        }
        #endregion
    }
}

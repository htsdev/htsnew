﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    // Noufil 5618 04/02/2009 New report for Past Payment follow update.
    public partial class PastDueCalls : System.Web.UI.Page
    {//Nasir 6049 07/10/2009 add Region
        #region Properties

        PaymentPlan Clspaymentplan = new PaymentPlan();
        //Nasir 6049 07/10/2009 add DV
        DataView dv;

        #endregion

        //Nasir 6049 07/10/2009 add Region
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ddlCaseTypes.SelectedValue = "0";
                    //Fahad 6638 11/10/2009 Parameter added in FillGrid Method
                    fillgrid(Convert.ToInt32(ddlCaseTypes.SelectedValue));
                    //Nasir 6049 07/11/2009 Page size control
                    Pagingctrl.Size = lntechNew.WebControls.RecordsPerPage.Twenty;
                    
                   
                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lblMessage.Text = ex.Message;
            }
        }

        //Nasir 6049 07/10/2009 Page index change
        /// <summary>
        /// Call when Page index change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    //Fahad 6638 11/10/2009 Parameter added in FillGrid Method
                    fillgrid(Convert.ToInt32(ddlCaseTypes.SelectedValue));

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, "DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, "ASC");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }


        #endregion

        #region Methods

        public void fillgrid(int Criteria)
        {
            //Fahad 6638 Criteria Prameter added in GetPastDueReports method
            DataTable dt = Clspaymentplan.GetPastDueReports(Criteria);

            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No Records Found";
                Pagingctrl.Visible = false;
            }
            else
            {
                lblMessage.Text = string.Empty;
                Pagingctrl.Visible = true;
                //Nasir 6049 07/10/2009 For Page size change
                if (Session["DV_PastDueCall"] == null)
                {
                    dv = new DataView(dt);
                    Session["DV_PastDueCall"] = dv;
                }
                else
                {
                    dv = (DataView)Session["DV_PastDueCall"];
                    string SortExp = dv.Sort;
                    dv = new DataView(dt);
                    dv.Sort = SortExp;
                    dt = dv.ToTable();
                    if (dt.Columns.Contains("SNo"))
                    {
                        dt.Columns.Remove("SNo");
                    }
                }


                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["TicketID_PK"].ToString() != dt.Rows[i]["TicketID_PK"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }

                //Nasir 6049 07/16/2009 sorting
                dv = dt.DefaultView;

                gv_records.Visible = true;
                //Nasir 6049 07/10/2009 For Page size change
                gv_records.DataSource = dv;
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
        }

        //Nasir 6049 07/10/2009 For Page index cance
        /// <summary>
        /// when page index change
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            //Fahad 6638 11/10/2009 Parameter added in FillGrid Method
            fillgrid(Convert.ToInt32(ddlCaseTypes.SelectedValue));
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            //Fahad 6638 11/10/2009 Parameter added in FillGrid Method
            fillgrid(Convert.ToInt32(ddlCaseTypes.SelectedValue));
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private void SortGridView(string sortExpression, string direction)
        {

            dv = (DataView)Session["DV_PastDueCall"];
            dv.Sort = sortExpression + " " + direction;
            Session["DV_PastDueCall"] = dv;

            //Fahad 6638 11/10/2009 Parameter added in FillGrid Method
            fillgrid(Convert.ToInt32(ddlCaseTypes.SelectedValue));
            Pagingctrl.PageCount = gv_records.PageCount;
            Pagingctrl.PageIndex = gv_records.PageIndex;

            ViewState["sortExpression"] = sortExpression;
            Session["SortDirection"] = direction;
            Session["SortExpression"] = sortExpression;

        }

        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
              
                //Fahad 6638 11/10/2009 Parameter added in FillGrid Method
                gv_records.PageIndex = 0;
                fillgrid(Convert.ToInt32(ddlCaseTypes.SelectedValue));
               
 
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }




    }
}

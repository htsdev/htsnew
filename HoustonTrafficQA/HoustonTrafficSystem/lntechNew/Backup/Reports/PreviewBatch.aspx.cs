using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

//Nasir 5681 05/20/2009 change name space
namespace HTP.Reports
{
	/// <summary>
	/// Viewing Batch Reports
	/// </summary>
	public partial class PreviewBatch : System.Web.UI.Page
	{	
		
	
		private void Page_Load(object sender, System.EventArgs e)
		{
            //Aziz 3220 02/22/08 Change to session due to problem in query string length no need of viewstate
            //ViewState["LetterType"] = Session["Lettertype"].ToString();           
            //ViewState["TicketID"] = Session["bTicketID"].ToString();
            //ViewState["TicketIDBatch"] = Session["bTicketIDBatch"].ToString(); 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{ 	
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
	}	
}

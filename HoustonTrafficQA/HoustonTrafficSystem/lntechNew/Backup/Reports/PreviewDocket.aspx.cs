using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace lntechNew.Reports
{
    public partial class PreviewDocket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ShowPDF();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

        }

        private void ShowPDF()
        {
            /*Document doc = new Document();
            //DataTable dtTemp;
            AppSettingsReader re = new AppSettingsReader();

            //string filename = re.GetValue("NPATHDocketCloseOutTemp", typeof(string)).ToString() + Session.SessionID.ToString() + "-" + Request.QueryString["ImgPath"].Remove(0, Request.QueryString["ImgPath"].LastIndexOf("\\") + 1).Replace(".", "-") + ".pdf";
            //filename = filename.Replace("\\\\", "\\");

            string filename = Server.MapPath("") + "\\Temp\\" + Session.SessionID.ToString() + "-" + Request.QueryString["ImgPath"].Remove(0, Request.QueryString["ImgPath"].LastIndexOf("\\") + 1).Replace(".", "-") + ".pdf";
            PdfWriter.getInstance(doc, new FileStream(filename, FileMode.Create));
            doc.Open();

            doc.setMargins(1, 0, 0, 0);

            string fn = Request.QueryString["ImgPath"];

            iTextSharp.text.Image pic = iTextSharp.text.Image.getInstance(fn);
            pic.scaleToFit(PageSize.A4.Width - 50, PageSize.A4.Height - 50);
            doc.Add(pic);
            doc.Close();*/
            string filename = Session["DocketPDFPath"].ToString();
            FileInfo fi = new FileInfo(filename);
            fi.CopyTo(Server.MapPath("").ToString() + "\\temp" + filename.Remove(0, filename.LastIndexOf("\\")),true);

            Response.Redirect("Temp/" + filename.Remove(0,filename.LastIndexOf("\\")),false);
            //Response.Redirect(filename);

        }
    }
}

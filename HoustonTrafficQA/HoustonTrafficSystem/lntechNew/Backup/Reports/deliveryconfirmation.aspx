﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="deliveryconfirmation.aspx.cs"
    Inherits="HTP.Reports.deliveryconfirmation" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Delivery Confirmation</title>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../ErrorLog/BoxOver.js" type="text/javascript"></script>

    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
    function StateTracePoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg()		     
		}
    function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
    
    function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 527px;
        }
        .style3
        {
            width: 504px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="7" height="11">
                    </td>
                </tr>
                <tr>
                    <td class="clssubhead" align="left">
                        <asp:DropDownList ID="dd_status" runat="server" Height="16px" OnSelectedIndexChanged="dd_status_SelectedIndexChanged"
                            AutoPostBack="true" CssClass="clsinputcombo">
                            <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Pending" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Delivered" Value="1"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" colspan="7" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="Left" style="height: 34px">
                        <table class="style1">
                            <tr>
                                <td class="clssubhead">
                                    &nbsp;Delivery Confrimation
                                </td>
                                <td class="label" align="right">
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                            CellPadding="3" AllowPaging="true" PageSize="30" CssClass="clsleftpaddingtable"
                            OnPageIndexChanging="gv_records_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lbl_name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'
                                            ToolTip='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="label"
                                            NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid") %>'>
                                        </asp:HyperLink>
                                        <asp:HiddenField ID="hf_BatchID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BatchID_Pk") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="courtdate" HeaderText="Crt Status Trial Date & Room" ControlStyle-CssClass="label"
                                    HeaderStyle-CssClass="clssubhead" />
                                <asp:BoundField DataField="DeliveryStatus" HeaderText="Status" ControlStyle-CssClass="label"
                                    HeaderStyle-CssClass="clssubhead" />
                                <asp:BoundField DataField="TrackingNumber" HeaderText="Tracking Number" ControlStyle-CssClass="label"
                                    HeaderStyle-CssClass="clssubhead" />                                    
                                <asp:TemplateField HeaderText="Track" ItemStyle-HorizontalAlign="Center"> 
                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>                               
                                    <ItemTemplate>
                                        <div style="overflow: scroll;" title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off]header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close.jpg' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()'onclick='hideBox()'></td></tr></table>]body=[<table border='0' width='400px' height='100px'><tr><td><textarea id='txt_StateTraceMsg' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">                                            
                                            <asp:Label ID="lbltrack" runat="server" Text="Track" CssClass="label" onclick="copyToClipboard(document.getElementById('txt_StateTraceMsg').value);"></asp:Label>
                                            <asp:HiddenField ID="hf_response" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.trackResponse") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" />
                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                        </asp:GridView>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>

<script language="javascript" type="text/javascript">
		function ShowMsg()
        {
            document.getElementById("txt_StateTraceMsg").value=err;
        }
</script>

</html>

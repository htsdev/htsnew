<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HMCBadData.aspx.cs" Inherits="lntechNew.Reports.HMCBadData" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>HMC Bad Data</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
        <tr>
            <td align="center">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
            </td>
        </tr>
    <tr>
    <td align="center">
        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red" Text="Label"></asp:Label></td>
    </tr>
        <tr>
            <td width="100%">
                <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Cause Number">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'></asp:Label>
                                <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ticket Number">
                            <ItemTemplate>
                                <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lastname">
                            <ItemTemplate>
                                <asp:HyperLink ID="hl_LastName" runat="server" CssClass="label" Text ='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=1" %>' ></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Firstname">
                            <ItemTemplate>
                                <asp:Label ID="lbl_FirstName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Status" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Court Date">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Court Time">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CourtTime" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:t}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Court Room">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CourtRoom" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtRoom") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <uc2:Footer ID="Footer1" runat="server" />
            </td>
        </tr>
    
    </table>
    </div>
    </form>
</body>
</html>

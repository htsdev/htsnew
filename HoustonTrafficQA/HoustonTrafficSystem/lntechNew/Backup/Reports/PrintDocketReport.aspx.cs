using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;

namespace lntechNew.Reports
{
    public partial class PrintDocketReport : System.Web.UI.Page
    {

        clsENationWebComponents ClsDb = new clsENationWebComponents();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblPrintDate.Text = "Print Date:" + DateTime.Now.ToString("MM/d/yyyy hh:mm tt");
                /*if (Session["SortDirection"] != null && Session["SortExpression"] != null)
                {
                    SortGridView(Session["SortExpression"].ToString(), Session["SortDirection"].ToString());
                }*/
                if (Request.QueryString["SortExpression"] != null && Request.QueryString["SortDirection"] != null)
                {
                    SortGridView(Request.QueryString["SortExpression"].ToString(), Request.QueryString["SortDirection"].ToString());
                }
                else
                {
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        private void FillGrid()
        {


            //string[] keys = { "@CourtID", "@Type", "@datefrom", "@dateto", "@AllDates", "@status" };
            //object[] values = { Request.QueryString["CourtHouse"], Request.QueryString["DateType"], Request.QueryString["From"], Request.QueryString["To"], Request.QueryString["AllDates"], Request.QueryString["Status"] };
            DataTable dtRecords = GetRecords();//ClsDb.Get_DT_BySPArr("usp_hts_Search_DocketReport", keys, values);
            gv_Data.DataSource = dtRecords;
            gv_Data.DataBind();
            GenerateSerial();
        }

        private void GenerateSerial()
        {

            try
            {
                int no = 1;
                string ticketid = "";
                foreach (GridViewRow ItemX in gv_Data.Rows)
                {
                    HyperLink sno = (HyperLink)ItemX.FindControl("hf_SNo");
                    Label tid = (Label)ItemX.FindControl("lbl_Ticketid");
                    if (tid.Text != "")
                    {
                        if (no == 1)
                        {
                            sno.Text = no.ToString();
                            ticketid = tid.Text;
                            no = no + 1;
                        }
                        else
                        {
                            if (ticketid != tid.Text)
                            {
                                sno.Text = Convert.ToString(no);
                                ticketid = tid.Text;
                                no = no + 1;
                            }
                            else
                            {
                                sno.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                Response.Write(ex.Message);
            }
        }

        private void SortGridView(string sortExpression, string direction)
        {


            DataTable dt = GetRecords();

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + " " + direction;

            gv_Data.DataSource = dv;

            gv_Data.DataBind();

            ViewState["sortExpression"] = sortExpression;
            //Session["SortDirection"] = direction;
            //Session["SortExpression"] = sortExpression;

        }

        private DataTable GetRecords()
        {
            string[] keys = { "@CourtID", "@Type", "@datefrom", "@dateto", "@AllDates", "@status" };
            object[] values = { Request.QueryString["CourtHouse"], Request.QueryString["DateType"], Request.QueryString["From"], Request.QueryString["To"], Request.QueryString["AllDates"], Request.QueryString["Status"] };

            //Generating serial numbers
            DataTable dt = ClsDb.Get_DT_BySPArr("usp_hts_Search_DocketReport", keys, values);
            DataColumn dc = new DataColumn("S No", typeof(int));
            dt.Columns.Add(dc);
            dc.SetOrdinal(0);

            int no = 1;
            string ticketid = "";

            foreach (DataRow dr in dt.Rows)
            {
                string tid = dr["TicketID_PK"].ToString();
                if (tid != "")
                {
                    if (no == 1)
                    {
                        ticketid = tid;
                        dr["S No"] = no;
                        no = no + 1;
                    }
                    else
                    {
                        if (ticketid != tid)
                        {
                            dr["S No"] = no;
                            ticketid = tid;
                            no = no + 1;
                        }
                        else
                        {
                            dr["S No"] = no - 1;
                        }

                    }

                }
            }
            //end

            return dt;
        }

        private DataView GetRecords(string sd, string sortExpression)
        {

            DataTable dt = GetRecords();

            DataView dv = new DataView(dt);

            dv.Sort = sortExpression + " " + sd;

            return dv;
        }

        protected void gv_Data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex > 0)
                {
                    GridViewRow gvr = gv_Data.Rows[e.Row.RowIndex - 1];
                    HyperLink hl_prevSno = (HyperLink)gvr.FindControl("hf_SNo");
                    HyperLink hl_currSno = (HyperLink)e.Row.FindControl("hf_SNo");

                    if (hl_currSno != null && hl_prevSno != null)
                    {
                        if (hl_prevSno.Text == hl_currSno.Text)
                        {
                            //hl_currSno.Attributes.Add("Display", "none");
                            hl_currSno.Style[HtmlTextWriterStyle.Display] = "none";
                        }
                    }

                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string hireDate = ((Label)e.Row.FindControl("lbl_Hire")).Text;
                    if (hireDate.Length == 10)
                    {
                        DateTime hDate = Convert.ToDateTime(hireDate);
                        ((Label)e.Row.FindControl("lbl_Hire")).Text = hDate.ToString("MM/dd/yy");
                    }

                    string UpdateDate = ((Label)e.Row.FindControl("lbl_Update")).Text;
                    if (UpdateDate.Length == 10)
                    {
                        DateTime upDate = Convert.ToDateTime(UpdateDate);
                        ((Label)e.Row.FindControl("lbl_Update")).Text = upDate.ToString("MM/dd/yy");
                    }

                    string dOb = ((Label)e.Row.FindControl("lbl_DOB")).Text;
                    if (dOb.Length == 10)
                    {
                        DateTime doDate = Convert.ToDateTime(dOb);
                        ((Label)e.Row.FindControl("lbl_DOB")).Text = doDate.ToString("MM/dd/yy");
                    }

                    string CrtDate = ((Label)e.Row.FindControl("lbl_CourtDate")).Text;
                    if (CrtDate.Length == 10)
                    {
                        DateTime cDate = Convert.ToDateTime(CrtDate);
                        ((Label)e.Row.FindControl("lbl_CourtDate")).Text = cDate.ToString("MM/dd/yy");
                    }

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using HTP.WebComponents;
using HTP.Components;

namespace HTP.Reports
{
    public partial class frmRptLegalConsultationReport : System.Web.UI.Page
    {

        ValidationReports VReport = new ValidationReports();
        clscalls LegalHostonReport = new clscalls();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {


                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    //SessionRecid = Convert.ToString(Session["ReminID"]);

                    if (Page.IsPostBack != true)
                    {                                                
                        fillgrid();
                    }

                    //FillGrid(); //filling the grid
                    //SetUrl();	//	putting validations on grid
                    //FAHAD 5533 02/25/2009 Page Size Change method added.
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_records;
                }
                Pagingctrl.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

            //Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
        }

        //Nasir 5256 12/12/2008 fill the grid
        public void fillgrid()
        {
            DataTable dt = LegalHostonReport.GetLegalConsultationData(DateTime.Today, DateTime.Today, 0, true); //  .GetVisitorReport();

            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No records Found";
            }
            else
            {
                lblMessage.Text = "";
                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["TicketID_PK"].ToString() != dt.Rows[i]["TicketID_PK"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }
                Pagingctrl.Visible = true;
                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();
                Pagingctrl.GridView = gv_records;
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();

            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    fillgrid();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        //FAHAD 5533 02/25/2009 Page Size Change method added.
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gv_records.PageIndex = 0;
                    gv_records.PageSize = pageSize;
                    gv_records.AllowPaging = true;
                }
                else
                {
                    gv_records.AllowPaging = false;
                }
                fillgrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }                       
  
    }

}

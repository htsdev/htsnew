<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BondWaiting.aspx.cs" Inherits="HTP.Reports.BondWaiting" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%--Yasir Kamal 5423 02/12/2009 Follow Up date in Next Six Business days --%>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HMC Bond Waiting Alert</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="800" align="center" border="0">
            <tbody>
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="1" cellspacing="0" style="width: 100%">
                                        <tr class="clsleftpaddingtable">
                                            <td style="width: 600px;" align="left">
                                                <uc5:ShowSetting ID="ShowSetting" lbl_TextAfter="(Business Days)" lbl_TextBefore="Follow Up Date in Next"
                                                    runat="server" Attribute_Key="Days" />
                                            </td>
                                        </tr>
                                        <tr>
                                           <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                                                <table>
                                                    <tr>
                                                        
                                                        <td>
                                                            <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                                <ContentTemplate>
                                                                    <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                                                </ContentTemplate>
                                                            </aspnew:UpdatePanel>
                                                        </td>
                                                        <td align="right">
                                                            <asp:CheckBox TextAlign="Left" CssClass="clssubhead" AutoPostBack="True" ID="ShowAll"
                                                                Text="ShowAll" runat="server" OnCheckedChanged="ShowAll_CheckedChanged" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                                width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" valign="top">
                                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                    <ProgressTemplate>
                                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                            CssClass="clsLabel"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            CssClass="clsleftpaddingtable" AllowSorting="False" OnRowDataBound="gv_Records_RowDataBound"
                                                            AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="30"
                                                            OnRowCommand="gv_Records_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="SNo">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                    <%--<ControlStyle Width="15%" />--%>
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                                    <ItemTemplate>
                                                                        <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                                    </ItemTemplate>
                                                                    <ControlStyle Width="10%" />
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Bond" SortExpression="bondflag">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_bondflag" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ControlStyle Width="10%" />
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket No" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Causeno" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cause No" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Causenumber" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Last Name" SortExpression="lastname">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name" SortExpression="firstname">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Crt" SortExpression="courtname">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_cloc" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Crt.No" SortExpression="courtnumber">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Cno" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" Width="40px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status" SortExpression="verifiedcourtstatus">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_verstatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.verifiedcourtstatus") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Days" SortExpression="DaysOver">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label1" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.DaysOver") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" Width="30px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Bond Waiting Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBondwaitingdate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.BondDate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" Width="120px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Follow Up Date">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" Width="90px" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.bondwaitingfollow") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                                        <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                                        <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                                        <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                                        <asp:HiddenField ID="hf_loc" runat="server" Value='<%#Eval("shortname") %>' />
                                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("shortname") %>' />
                                                                        <asp:HiddenField ID="hf_bonddate" runat="server" Value='<%#Eval("BondDate") %>' />
                                                                        <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                                    <ProgressTemplate>
                                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                            CssClass="clsLabel"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlFollowup" runat="server">
                                                            <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Bond Waiting Follow Up Date" />
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                                <%-- <asp:Panel ID="pnlFollowup" runat="server">
                                    <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Bond Waiting Follow Up Date" />
                                </asp:Panel>--%>
                                                <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                                                    <ContentTemplate>
                                                        <ajaxToolkit:ModalPopupExtender ID="mpeBondwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="display: none;">
                                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>
                                            </td>
                                            <td>
                                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
    <%-- Noufil 4980 10/30/2008 Calender Z-index addded --%>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using HTP.WebControls;
using FrameWorkEnation.Components;

namespace HTP.Reports
{
    public partial class BondReport : System.Web.UI.Page
    {
        /// <summary>
        /// One of Validation Report ( Developed By Zeeshan Zahoor - 8/6/2008)
        /// BASIC LOGIC :
        /// 1) The report will display bond status of active clients 
        /// 2) popup will display followup date that defaults ton seven days from today's date
        /// 3) User will be able to sort by court location, bond followup date
        /// 4) Paging control is already registered at this report 
        /// 5) if followup date is not available then report should display "Not Available" in GridView
        /// </summary>
        /// 



        #region Variable Declaration
        // Validation Reports object
        ValidationReports report = new ValidationReports();
        // session object.
        clsSession cSession = new clsSession();
        // error loging class object
        clsLogger clog = new clsLogger();
             
        string StrExp = String.Empty;   // grid sorting expression variable declaration.
        string StrAcsDec = String.Empty;    // grid sorting type variable decalation.
        DataSet DS;                         // data set variable for poplulating data into it.
        DataView dvBR;                      // data view variable for storing data view in to session.

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {               
                BindData();     // binding data in grid.
            }
            // declaring page level delegate for update follow up control.
            UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);            
            // declaring page level delegate for paging control.
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            //Nasir 5564 03/16/2009 control page size change
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            // getting employee id from cookies into session.
            ViewState["empid"] = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
            //Nasir 5564 03/16/2009 display paging control on page load
            Pagingctrl.GridView = gv_records;
        }

        
        // Zahoor 4481 08/07/2008
        // Grid paging Index changing event.
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {           
            if (e.NewPageIndex != -1)   // checking condition index out of range.
            {
                gv_records.PageIndex = e.NewPageIndex; // setting grid page index.

                gv_records.DataSource = (DataView)Session["dvBR"];   // setting data source for grid
                gv_records.DataBind();                              // binding grid with data source.

                Pagingctrl.PageCount = gv_records.PageCount;    // setting grid and paging control page counts.
                Pagingctrl.PageIndex = gv_records.PageIndex;    // setting grid page index to paging control.
                Pagingctrl.SetPageIndex();                      // setting paging control page index
            }          
          
        }

        //  zahoor 4481 8/07/2008
        // grid row command event for getting specifi row information.
        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                // getting grid row iud.
                int RowID = Convert.ToInt32(e.CommandArgument);                            
                
                // getting required field from grid rows.
                string firstname = ((Label)gv_records.Rows[RowID].Cells[1].FindControl("lbl_lastname")).Text;
                string lastname = ((Label)gv_records.Rows[RowID].Cells[2].FindControl("lbl_firstname")).Text;
                string causeno = ((Label)gv_records.Rows[RowID].Cells[3].FindControl("lbl_causenum")).Text;
                string ticketno = ((Label)gv_records.Rows[RowID].Cells[4].FindControl("lbl_ticketnumber")).Text;
                string ticketid = ((HiddenField)gv_records.Rows[RowID].Cells[0].FindControl("hf_TicketID")).Value;
                string courtname = ((HiddenField)gv_records.Rows[RowID].Cells[8].FindControl("hf_CourtCode")).Value;
                //Sabir Khan 6706 10/07/2009 Set court variable for follow up date...
                string court = "Bond Alert";  // empty for one week followupdate case.
                string gComments = ((HiddenField)gv_records.Rows[RowID].Cells[8].FindControl("hf_GeneralComments")).Value;
                // Abid Ali 5425 2/3/2009 empty date has space so it should be trim
                string sfuDate = ((Label)gv_records.Rows[RowID].Cells[7].FindControl("lbl_followup")).Text.Trim();

                
                DateTime fuDate, follow;   // local variables for follow up date condition checking.
                int Days = 7;              // local varibale for number of days to be added.
                
                // checking condition for old follo up date for the client.                
                if (sfuDate.Trim().Length != 0)
                {
                    fuDate = DateTime.Parse(sfuDate);

                }
                else
                    fuDate = DateTime.Today.AddDays(Days);

                // Noufil 5001 10/24/2008 logical date added for empty string
                if (sfuDate == "")
                    sfuDate = "1/1/1900";
                // Assigning condition for follow up date to client case.
                if (fuDate < DateTime.Today)
                { follow = DateTime.Today.AddDays(Days); }
                else
                { follow = fuDate; }

                UpdateFollowUpInfo2.Freezecalender = true;  // freezing the follow up popup.
                // Abid Ali 5425 1/20/2009 update FollowUpType referrence
                UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.BondFollowUpDate; // assining the follow up type.
                // binding hte values to the follow up date control.
                UpdateFollowUpInfo2.binddate(follow, Convert.ToDateTime(sfuDate), firstname, lastname, ticketno, causeno, gComments, ModalPopupExtender1.ClientID, court, int.Parse(ticketid.ToString().Trim()));

                // displaying the follow up control.
                ModalPopupExtender1.Show();

                // displaying 
                //Pagingctrl.Visible = true;                
            }
        }
        
        // zahoor 4481 08/07/2008
        // paging control event
        protected void Pagingctrl_PageIndexChanged()
        {
            
            try
            {
                DataTable dtView;       // decalring data table local varibale 
                dvBR = (DataView)Session["dvBR"];       // getting data view from sessions 
                dtView = dvBR.ToTable();                // creating new instance of data table.
             
                gv_records.PageIndex = Pagingctrl.PageIndex - 1;  // set page index to grid.
                gv_records.DataSource = dtView;                    // set grid data source
                gv_records.DataBind();                              // bind grid.

                // setting paging control.
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                Session["dvBR"] = dtView.DefaultView;           // assing default view into session.
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // zahoor 4481 08/7/2008
        // Grid row data bound event for row level operations.
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    // setting folow up date control style.
                    pnlFollowup.Style["display"] = "none";
                    // Zahoor 4650 comment this line added previous task 4481
                    //string firstname = ((Label)e.Row.FindControl("hf_TicketNumber")).Text;
                }
            }

            catch (Exception ex)
            {
              //  lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }           
        }

        // Zeeshan 4481 08/06/2008 
        // Its purpose is to get data, bind the grid.
        public void BindData()
        {
            try
            {
                string[] keys = { "@validation"};   // SP parameters list. 
                object[] values = { 0 };            // sp parameter's values list.
                report.getRecords("usp_htp_Bond_Report",keys,values);   // populating data from data grid.                
                DS = report.records;                   // referencing dataset.
                generateSerialNo(DS.Tables[0]);         // generating serial number for data grid.
                gv_records.DataSource = DS.Tables[0];   // 
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (Session[""] != null) Session.Clear();

                    dvBR = new DataView(DS.Tables[0]);
                    Session["dvBR"] = dvBR;
                    //Sabir Khan 4635 09/19/2008
                    //---------------------------
                    this.lbl_message.Text = "";                                  
                }
                else
                {
                    this.lbl_message.Text = "No records found";
                }
                //---------------------------------
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
         
        }

        // Zeeshan 4481 08/06/2008 
        // Its purpose is sort the grid.
        private void SortGrid(string SortExp)
        {
            DataTable dtView;
            
            try
            {
                SetAcsDesc(SortExp);
                dvBR = (DataView)Session["dvBR"];
                dvBR.Sort = StrExp + " " + StrAcsDec;
                dtView = dvBR.ToTable();
                generateSerialNo(dtView);
                gv_records.DataSource = dtView;
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                Session["dvBR"] = dtView.DefaultView;

            }
            catch (Exception ex)
            {
             lbl_message.Text = ex.Message;
             clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Zeeshan 4481 08/06/2008 
        // Its purpose is Asc and desc order in the grid.
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        // Zeeshan 4481 08/06/2008 
        // grid sorting event. 
        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        //Nasir 5564 03/16/2009 insert page size
        /// <summary>
        /// Use for set page size
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gv_records.PageIndex = 0;
                    gv_records.PageSize = pageSize;
                    gv_records.AllowPaging = true;
                }
                else
                {
                    gv_records.AllowPaging = false;
                }
                BindData();
            }
            catch (Exception ex)
            {
                lbl_message.Visible = true;
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        void UpdateFollowUpInfo2_PageMethod()
        {
            BindData();
            //throw new NotImplementedException();
        }

        // Zahoor 4481 08/07/2008
        // For generating serial number on desired time.
        public void generateSerialNo(DataTable dtRecords)
        {
            
            
            int sno=1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");                
                //sno = 1;
            }

            
                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                        {
                            dtRecords.Rows[i]["sno"] = ++sno;
                        }
                    }
                }
            
        }
       
    } 
}

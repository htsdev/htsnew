using System;
using System.Data;
using System.Web.UI.WebControls;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    /// <summary>
    /// WaitingFollowUpCriminal event.
    /// </summary>
    public partial class WaitingFollowUpCriminal : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 7791 07/24/2010  grouping variables
        #region Variables

        readonly clsSession _clsSession = new clsSession();
        readonly clsCase _cCase = new clsCase();

        #endregion

        // Fahad Muhammad Qureshi 7791 07/24/2010 grouping Report Properties
        #region Properties

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }

        private bool ShowAllRecords
        {
            get
            {
                if (ViewState["ShowAllRecords"] == null)
                    ViewState["ShowAllRecords"] = false;

                return Convert.ToBoolean(ViewState["ShowAllRecords"]);
            }
            set
            {
                ViewState["ShowAllRecords"] = value;
            }
        }

        private bool ShowPastRecords
        {
            set
            {
                ViewState["ShowPastRecords"] = value;
            }
        }

        private DateTime FromDate
        {
            get
            {
                if (ViewState["FromDate"] == null)
                    ViewState["FromDate"] = new DateTime(1900, 1, 1);

                return Convert.ToDateTime(ViewState["FromDate"]);
            }
            set
            {
                ViewState["FromDate"] = value;
            }
        }

        private DateTime ToDate
        {
            get
            {
                if (ViewState["ToDate"] == null)
                    ViewState["ToDate"] = DateTime.Now;

                return Convert.ToDateTime(ViewState["ToDate"]);
            }
            set
            {
                ViewState["ToDate"] = value;
            }
        }

        #endregion

        // Fahad Muhammad Qureshi 7791 07/24/2010 grouping Event Handlers
        #region Event Handler

        /// <summary>
        /// Page_Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    lblMessage.Text = string.Empty;
                    if (!IsPostBack)
                    {
                        chkFamilyShowAll.Checked = false;
                        ShowAllRecords = false;
                        chkFamilyShowPast.Checked = true;
                        ShowPastRecords = true;
                        cal_FromDateFilter.SelectedDate = DateTime.Now;
                        FromDate = DateTime.Now;
                        cal_ToDateFilter.SelectedDate = DateTime.Now;
                        ToDate = DateTime.Now;
                        gv_CriminalFollowUp.Visible = false;

                    }
                    UpdateFollowUpInfo2.PageMethod += UpdateFollowUpInfo2_PageMethod;
                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_CriminalFollowUp;
                    const string javascript = "EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});";
                    chkFamilyShowAll.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 2));
                    chkFamilyShowPast.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 3));
                    if (chkFamilyShowAll.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 2));
                    }
                    else if (chkFamilyShowPast.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledFamilyFilterControls('{0}','{1}','{2}','{3}',{4});</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkFamilyShowAll.ClientID, chkFamilyShowPast.ClientID, 3));
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// btnFamilySubmit_Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ShowAllRecords = chkFamilyShowAll.Checked;
                ShowPastRecords = chkFamilyShowPast.Checked;
                FromDate = cal_FromDateFilter.SelectedDate;
                ToDate = cal_ToDateFilter.SelectedDate;
                gv_CriminalFollowUp.PageIndex = 0;
                GridViewSortDirection = SortDirection.Descending;
                GridViewSortExpression = "FollowUpDate";
                FillCriminalGrid();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Gridview Sorting event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillCriminalGrid();
                Pagingctrl.PageCount = gv_CriminalFollowUp.PageCount;
                Pagingctrl.PageIndex = gv_CriminalFollowUp.PageIndex;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Gridview RowCommand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {

                    var rowId = Convert.ToInt32(e.CommandArgument);
                    var firstname = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_fname")).Value);
                    var causeno = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_causeno")).Value);
                    var ticketno = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_ticketno")).Value);
                    var lastname = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_lname")).Value);
                    var courtname = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_loc")).Value);
                    var court = (((HiddenField)gv_CriminalFollowUp.Rows[rowId].FindControl("hf_criminal_courtid")).Value);
                    var followupDate = (((Label)gv_CriminalFollowUp.Rows[rowId].FindControl("lbl_FollowUpD")).Text);
                    _cCase.TicketID = Convert.ToInt32(ticketno);
                    var comm = _cCase.GetGeneralCommentsByTicketId();
                    var days = (court == "3047" || court == "3048" || court == "3070") ? 7 : 14;
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Criminal Follow Up Date";
                    UpdateFollowUpInfo2.followUpType = Components.FollowUpType.CriminalFollowUpDate;
                    UpdateFollowUpInfo2.binddate(DateTime.Today.AddDays(days), DateTime.Today, firstname, lastname, ticketno, causeno, comm, mpeTrafficwaiting.ClientID, court, courtname, followupDate);
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CriminalFollowUp.PageIndex = e.NewPageIndex;
                FillCriminalGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Gridview RowDataBound event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_CriminalFollowUp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_AddCriminal")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        // Fahad Muhammad Qureshi 7791 07/24/2010 grouping Methods
        #region Methods

        /// <summary>
        /// This method used to fetch the data which will display on the Gridview.
        /// </summary>
        private void FillCriminalGrid()
        {
            var hccCrim = new HccCriminalReport();
            DataView dv;
            const int courtid = -1;

            if (chkFamilyShowPast.Checked)
            {
                FromDate = new DateTime(1900, 1, 1);
                ToDate = DateTime.Now;
            }

            var dt = hccCrim.Get_CriminalFollowUp(courtid, FromDate, ToDate, ShowAllRecords);
            if (dt.Rows.Count > 0)
            {
                gv_CriminalFollowUp.Visible = true;
                lblMessage.Text = "";
                dt.Columns.Add("sno");
                dv = dt.DefaultView;
                dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                dt = dv.ToTable();
                GenerateSerialNo(dt);
                Pagingctrl.GridView = gv_CriminalFollowUp;
                gv_CriminalFollowUp.DataSource = dt;
                gv_CriminalFollowUp.DataBind();
                Pagingctrl.PageCount = gv_CriminalFollowUp.PageCount;
                Pagingctrl.PageIndex = gv_CriminalFollowUp.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            else
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lblMessage.Text = "No Records Found";
                gv_CriminalFollowUp.Visible = false;
            }

        }

        /// <summary>
        /// This method used to generate the Serial Number to the given DataTable.
        /// </summary>
        /// <param name="dtRecords">DataTable on which serial number will generate.</param>
        private void GenerateSerialNo(DataTable dtRecords)
        {
            var sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (var i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        /// <summary>
        /// This method is supporting method to Update Follow Up information.
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            FillCriminalGrid();
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {

            gv_CriminalFollowUp.PageIndex = Pagingctrl.PageIndex - 1;
            FillCriminalGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_CriminalFollowUp.PageIndex = 0;
                gv_CriminalFollowUp.PageSize = pageSize;
                gv_CriminalFollowUp.AllowPaging = true;
            }
            else
            {
                gv_CriminalFollowUp.AllowPaging = false;
            }
            FillCriminalGrid();

        }

        #endregion

    }
}

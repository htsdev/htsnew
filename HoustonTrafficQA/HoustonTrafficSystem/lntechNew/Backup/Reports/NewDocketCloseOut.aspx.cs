using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class testpopup : System.Web.UI.Page
    {
        //This datatable will store records from the snapshot table
        private DataSet dsRecords;
        
        //This datatable will be used to get scanned dockets
        private DataTable dtDockets;

        //Database object for TrafficTickets Database
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");

        //Database object for ReturnDockets Database
        clsENationWebComponents clsDB_RD = new clsENationWebComponents("returnDockets");

        protected GroupingHelper m_oGroupingHelper = null;
        clsSession ClsSession = new clsSession();

        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";
            if (!IsPostBack)
            {
                ViewState["vEmpID"] = ClsSession.GetCookie("sEmpID", this.Request).ToString();
            }
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords("", "", 0);

                   if (dsRecords.Tables[0].Rows.Count > 0)
                    {
                        fillSortingLists(dsRecords);
                        tbl_sort.Visible = true;
                        btn_updatestatus.Visible = true;
                        //btn_DisposeSelected.Visible = true;
                    }
                    else
                    {
                        tbl_sort.Visible = false;
                        btn_updatestatus.Visible = false;
                        //btn_DisposeSelected.Visible = false;
                    }
                    bindGrid();
                               
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Gets all the records of the supplied date from the snapshot table
        private void GetRecords(string courtname,string status,int showclosedDocket)
        {
            try
            {
                string[] keys = { "@CourtDate", "@CourtName", "@Status", "@ShowClosedDocket" };
                object[] values = { cal_Date.SelectedDate, courtname,status ,showclosedDocket};
                //dtRecords = clsDb.Get_DT_BySPArr("USP_HTS_DOCKETCLOSEOUT_GETRECORDS", keys, values);
                //dsRecords = clsDb.Get_DS_BySPArr("USP_HTS_DOCKETCLOSEOUT_GETRECORDS", keys, values);
                dsRecords = clsDb.Get_DS_BySPArr("USP_HTS_DOCKETCLOSEOUT_Filter_RECORDS", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {

                if (e.Item.ItemType != ListItemType.Header &&  e.Item.ItemType != ListItemType.Footer)
                {


                    try
                    {
                        Label astatus = (Label)e.Item.FindControl("lbl_astatus");
                        Label vstatus = (Label)e.Item.FindControl("lbl_vstatus");
                        Image img = (Image)e.Item.FindControl("img_dis");


                        if (((HiddenField)e.Item.FindControl("hf_discrepency")).Value == "False")
                        {
                            img.ImageUrl = "../Images/right.gif";
                        }
                        else
                        {
                            img.ImageUrl = "../Images/cross.gif";
                        }

                        if (((HiddenField)e.Item.FindControl("hf_showquestionmark")).Value == "True")
                            img.ImageUrl = "../Images/questionmark.gif";



                        string status = astatus.Text.Substring(0, 4);
                        string courtdate=astatus.Text.Substring(4,10);
                        if (status[3] != ' ')
                           courtdate = astatus.Text.Substring(5, 10);

                       DateTime cdate = DateTime.Parse(courtdate);

                       if (cdate > DateTime.Now && (status.Trim()=="ARR"  || status.Trim()=="JUR" || status.Trim()=="PRE" || status.Trim()=="JUD" || status.Trim()=="WAIT"))
                       {
                           ((HtmlControl)e.Item.FindControl("td_astatus")).Style.Add("background-color", "Yellow");
                       }


                        
                    }
                    catch { }

                    
                    
                    HtmlTableRow rowHeader = e.Item.FindControl("rowGroupHeader") as HtmlTableRow;
                    HtmlTableRow rowItem = e.Item.FindControl("rowItem") as HtmlTableRow;
                    HtmlTableRow RowRoomFooter = e.Item.FindControl("RowRoomFooter") as HtmlTableRow;

                    //Check if it is groupheader
                    DataRowView oRow = e.Item.DataItem as DataRowView;
                    if (oRow["poorman_type"].ToString() == "groupheader")
                    {
                        System.Web.UI.WebControls.Image oImage = e.Item.FindControl("idImage") as System.Web.UI.WebControls.Image;
                        m_oGroupingHelper.AddGroup(rowHeader.ClientID, oImage.ClientID);
                        HtmlGenericControl oCtrl = e.Item.FindControl("idClickable") as HtmlGenericControl;
                        oCtrl.Attributes["onclick"] = "javascript:" + m_oGroupingHelper.CurrentGroupItem.FunctionName + "();";


                        string sThisGroupValue = oRow["CourtNameComplete"].ToString();

                        rowHeader.Visible = true;
                        rowItem.Visible = false;
                        RowRoomFooter.Visible = false;
                        //Bind group stuff
                        Label lblGroupname = e.Item.FindControl("lblGroupname") as Label;
                        lblGroupname.Text = sThisGroupValue;
                        if (lblGroupname.Text.Contains("Houston Municipal"))
                        {
                            HtmlTableCell td_HMC = (HtmlTableCell)Page.FindControl("td_HMC");
                            if (td_HMC != null)
                                td_HMC.Visible = true;
                        }
                    }
                    else if (oRow["poorman_type"].ToString() == "CourtRoomChangefooter")
                    {



                        m_oGroupingHelper.CurrentGroupItem.AddClientRow(RowRoomFooter.ClientID);
                        string sThisGroupValue = oRow["CourtNameComplete"].ToString();

                        rowHeader.Visible = false;
                        rowItem.Visible = false;
                        RowRoomFooter.Visible = true;
                        //Bind group stuff
                        Label lblGroupname = e.Item.FindControl("lblGroupname") as Label;
                        lblGroupname.Text = sThisGroupValue;
                    }
                    else
                    {
                        //if (DropDownList1.SelectedValue != "None")
                        m_oGroupingHelper.CurrentGroupItem.AddClientRow(rowItem.ClientID);
                        rowHeader.Visible = false;
                        rowItem.Visible = true;
                        RowRoomFooter.Visible = false;
                    }

                }

                ImageButton ibtn = (ImageButton)e.Item.FindControl("ImageButton1");
                if (ibtn != null)
                {
                    if (ibtn.Visible == true)
                    {
                        HiddenField hf_date = (HiddenField)e.Item.FindControl("hf_CourtDate");

                        if (hf_date.Value != "")
                        {
                            DateTime dt = Convert.ToDateTime(hf_date.Value);

                            HiddenField hf_MM = (HiddenField)e.Item.FindControl("hf_MM");
                            hf_MM.Value = dt.Month.ToString();

                            HiddenField hf_DD = (HiddenField)e.Item.FindControl("hf_DD");
                            hf_DD.Value = dt.Day.ToString();

                            HiddenField hf_YY = (HiddenField)e.Item.FindControl("hf_YY");
                            hf_YY.Value = dt.Year.ToString().Substring(2, 2);


                            Label lbl_CourtNumber = (Label)e.Item.FindControl("lbl_CourtNumber");
                            Label lbl_CourtTime = (Label)e.Item.FindControl("lbl_CourtTime");

                            HiddenField hf_CourtID = (HiddenField)e.Item.FindControl("hf_CourtID");
                            HiddenField hf_TicketsViolationID = (HiddenField)e.Item.FindControl("hf_TicketsViolationID");
                            HiddenField hf_BondFlag = (HiddenField)e.Item.FindControl("hf_BondFlag");
                            HiddenField hf_TicketID = (HiddenField)e.Item.FindControl("hf_TicketID");



                            //   ibtn.Attributes.Add("onClick", "ProcessData('" + e.Item.ClientID.ToString() + "');");
                            ibtn.Attributes.Add("onClick", "ProcessData('" + lbl_CourtNumber.Text + "' , '" + lbl_CourtTime.Text + "' , '" + hf_MM.Value + "' ,'" + hf_DD.Value + "' ,'" + hf_YY.Value + "' ,'" + hf_TicketsViolationID.Value + "','" + hf_RecordID.Value + "' , '" + hf_CourtID.Value + "' ,'" + hf_BondFlag.Value + "' ,'" + hf_TicketID.Value + "');");
                        }
                    }

                }
                Image ibtn2 = (ImageButton)e.Item.FindControl("ImageButton2");
                Label lbl = (Label)e.Item.FindControl("lbl_LastUpdate");
                if (ibtn2 != null)
                {
                    if (lbl != null)
                    {
                        if (lbl.Text.Contains("/"))
                        {
                            ibtn2.Visible = true;
                            ibtn.Visible = false;
                        }
                        else
                        {
                            ibtn2.Visible = false;
                            ibtn.Visible = true;
                        }
                    }

                }
                /*HyperLink hlnk = e.Item.FindControl("hlnk_Record");
                if (hlnk != null)
                {
                    
                }*/
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        //GetCourtStatus() : Fills the drop downlist with court statuses and their IDs
        private void GetCourtStatus()
        {
            try
            {
                DataTable dtStatus = clsDb.Get_DT_BySPArr("USP_HTS_DOCKETCLOSEOUT_GET_ALL_COURTSTATUS");
                ddl_QuickUpdate_Status.Items.Clear();
                ddl_QuickUpdate_Status.Items.Add("<-- Choose -->");
                foreach (DataRow dr in dtStatus.Rows)
                {
                    ListItem li = new ListItem(dr["Description"].ToString(), dr["CourtViolationStatusID"].ToString());
                    ddl_QuickUpdate_Status.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void lbtn_close_Click(object sender, EventArgs e)
        {
            //pnl_Update.Visible = false;
            pnl_Update.Style["Display"] = "none";
        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            try
            {
                string updateline = string.Empty;
             
                    if (rb_Disposed.Checked) //If Disposed is seleted, Update the status by its status id, which in this case is 80.
                    {
                        updateline = "DISPOSED";
                        UpdateStatus("80", chk_UpdateAll.Checked);
                        UpdateSnapshotData(updateline,chk_UpdateAll.Checked); //Update its snapshot table field with "DISPOSED"
                        btn_Submit_Click(null, null);  //Refresh records

                    }
                    else if (rb_Missed.Checked)
                    {
                        updateline = "MISSED"; //If Disposed is seleted, Update the status by its status id, which in this case is 105.
                        UpdateStatus("105", chk_UpdateAll.Checked);
                        UpdateSnapshotData(updateline,chk_UpdateAll.Checked); //Update its snapshot table field with "DISPOSED"
                        btn_Submit_Click(null, null);   //Refresh records
                    }
                    else
                    {
                        UpdateData(chk_UpdateAll.Checked); //The user wants to 'RESET' data, so update all its required fields (Violation Status, court date, court number and court time)
                        updateline = ddl_QuickUpdate_Status.SelectedItem.ToString() + " " + txt_MM.Text + "/" + txt_DD.Text + "/" + txt_YY.Text + " " + txt_CourtNumber.Text + " " + ddl_CourtTime.SelectedItem.ToString();
                        UpdateSnapshotData(updateline,chk_UpdateAll.Checked); //
                        btn_Submit_Click(null, null); //Refresh reords
                    }

                
                rb_Disposed.Checked = true;
                rb_Missed.Checked = false;
                rb_Reset.Checked = false;
                


            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //The following method updates court date, location and violation status
        private void UpdateData(bool UpdateAll)
        {
            try
            {
                string[] keys = { "@TicketsViolationID", "@CourtDate", "@CourtLoc", "@EmpID", "@Status", "@UpdateAll", "@TicketID", "@Date" };
                object[] values = { hf_TicketViolationID.Value, txt_MM.Text + "/" + txt_DD.Text + "/" + txt_YY.Text + " " + Convert.ToDateTime(ddl_CourtTime.SelectedValue).ToShortTimeString(), txt_CourtNumber.Text.Replace("#", ""), ClsSession.GetCookie("sEmpID", this.Request).ToString(), ddl_QuickUpdate_Status.SelectedValue, Convert.ToInt16(UpdateAll), hf_TicketIDMain.Value, cal_Date.SelectedDate  };
                clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_QuickUpdate", keys, values);
                UpdateCaseHistory(Convert.ToInt32(hf_TicketIDMain.Value), "Status Updated: " + txt_MM.Text + "/" + txt_DD.Text + "/" + txt_YY.Text + " " + Convert.ToDateTime(ddl_CourtTime.SelectedValue).ToShortTimeString() + " #" + txt_CourtNumber.Text + " " + ddl_QuickUpdate_Status.SelectedItem.ToString(), " ", Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString()));
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //The following method updates only the status id by its TicketViolationID
        private void UpdateStatus(string statusID, bool UpdateAll)
        {
            try
            {
                string[] keys = { "@TicketsViolationID", "@StatusID", "@EmpID", "@Date", "@UpdateAllFlag", "@TicketID"};
                object[] values = { hf_TicketViolationID.Value, statusID, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request).ToString()), cal_Date.SelectedDate, Convert.ToInt16(UpdateAll), hf_TicketIDMain.Value};
                clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_STATUS", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //The following method updates the 'LastUpdate' field of snapshot table by its record id
        //takes one parameter 'UpdateText' which contains the text that would appear on the 'LastUpdate' field
        private void UpdateSnapshotData(string UpdateText, bool UpdateAll)
        {
            try
            {
                if (UpdateAll)
                {
                    string[] keys = { "@Date", "@LastUpdate", "@TicketID" };
                    object[] values = { cal_Date.SelectedDate, UpdateText, hf_TicketIDMain.Value };
                    clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATEALL_SNAPSHOT", keys, values);
                }
                else
                {
                    string[] keys = { "@RecordID", "@LastUpdate" };
                    object[] values = { hf_RecordID.Value, UpdateText };
                    clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_SNAPSHOT", keys, values);
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //The following method fills the datatable for scanned dockets pop-up
        private void FillDockets()
        {
            try
            {
                string[] keys = { "@stDate", "@endDate", "@StrAtoreny" };
                object[] values = { /*Convert.ToDateTime("12/16/2004") */cal_Date.SelectedDate, /*Convert.ToDateTime("12/16/2004")*/cal_Date.SelectedDate, "%" };
                dtDockets = clsDB_RD.Get_DT_BySPArr("USP_HTS_List_Docket", keys, values);
                gv_ScannedDockets.DataSource = dtDockets;
                gv_ScannedDockets.DataBind();
                ddl_dockets.Items.Clear();
                if (dtDockets.Rows.Count == 0)
                    lbl_Popup_Message.Text = "No dockets uploaded on this date";
                else
                {
                    lbl_Popup_Message.Text = "";
                    ddl_dockets.DataSource = dtDockets;
                    ddl_dockets.DataTextField = "IMPORTANCE";
                    ddl_dockets.DataValueField = "docid";

                }

                ddl_dockets.Items.Insert(0, new ListItem("<Scanned Dockets>", "-1"));
                ddl_dockets.Attributes.Add("onchange", "return PopUpCallBack(this.value);");
            
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void DeleteStatus(string statusID)
        {
            try
            {
                string[] keys = { "@UploadDate", "@TicketsViolationID", "@EmpID" };
                object[] values = { cal_Date.SelectedDate, statusID, ClsSession.GetCookie("sEmpID", this.Request).ToString() };
                clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_DELETE_STATUS", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            
        }

        protected void rptrtable_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                //This code will get executed once the user clicks 'x' button on the repeater control
                //and status will be deleted
                if (e.CommandName == "Delete") 
                {
                    DeleteStatus(e.CommandArgument.ToString());
                    btn_Submit_Click(null, null);
                    
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void btn_Dispose_Click(object sender, EventArgs e)
        {
            try
            {
                if (chk_DisposeAll.Checked)
                {
                    string[] keys = { "@CourtDate", "@EmpID" };
                    object[] values = { cal_Date.SelectedDate, ClsSession.GetCookie("sEmpID", this.Request).ToString() };
                    clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_DISPOSE_HMC_CASES", keys, values);

                }
                btn_Submit_Click(null, null);
                chk_DisposeAll.Checked = false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_ScannedDockets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //Find the document ID and attach the JavaScript accordingly to open the scanned dockets
                Label docid = (Label)e.Row.FindControl("lbl_docid");
                if (docid != null)
                {
                    LinkButton lbtn = (LinkButton)e.Row.FindControl("lbtn_DocketDate");
                    lbtn.Attributes.Add("onClick", "return PopUpCallBack(" + docid.Text + "  );");
                }


                //Label astatus = (Label)e.Row.FindControl("lbl_astatus"); 
                //Label vstatus = (Label)e.Row.FindControl("lbl_vstatus");
                //Image img = (Image)e.Row.FindControl("img_dis");

                //if (astatus.Text.Trim() == vstatus.Text.Trim())
                //{
                //    img.ImageUrl = "../Images/right.gif";
                //}
                //else
                //{
                //    img.ImageUrl = "../Images/cross.gif";
                //}
            
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void UpdateCaseHistory(int ticketno, string sub, string notes, int empid)
        {
            try
            {
                string[] keys = { "@ticketid", "@subject", "@notes", "@employeeid" };
                object[] values = { ticketno, sub, DBNull.Value, empid };
                clsDb.InsertBySPArr("SP_ADD_NOTES", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void fillSortingLists(DataSet ds_records)
        {
           
            ddl_courts.Items.Clear();
            ddl_courts.Items.Add(new ListItem("ALL", ""));
            
            object[] rows = GetDistinctValues(ds_records.Tables[0], "ShortName");
            
            foreach (object OBJ in rows)
                ddl_courts.Items.Add((string)OBJ);
                   
        }

        public object[] GetDistinctValues(DataTable dtable, string colName)
        {
            Hashtable hTable = new Hashtable();
            foreach (DataRow drow in dtable.Rows)
            {
                try
                {
                    hTable.Add(drow[colName], string.Empty);
                }
                catch { }
            }
            object[] objArray = new object[hTable.Keys.Count];
            hTable.Keys.CopyTo(objArray, 0);
            return objArray;
        }

        protected void btn_updatestatus_Click(object sender, EventArgs e)
        {
            try
            {

                string[] keys ={ "@TicketsViolationID" };
                object[] values = {""};
               


                foreach (RepeaterItem itm in rptrtable.Items)
                {

                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;                           
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS", keys, values);
                        }
                    }
                    
                }

                foreach (RepeaterItem itm in rptr_table.Items)
                {
                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Verified_With_Auto_STATUS", keys, values);
                        }                   
                    }

                }
                GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                bindGrid();
            }
            catch { }


        }

        protected void btn_DisposeSelected_Click(object sender, EventArgs e)
        {
            try
            {

                string[] keys ={ "@TicketsViolationID", "@EmpID" };
                object[] values = { "","" };



                foreach (RepeaterItem itm in rptrtable.Items)
                {

                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            values[1] = Convert.ToInt32(ViewState["vEmpID"]);
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected", keys, values);
                        }
                    }

                }

                foreach (RepeaterItem itm in rptr_table.Items)
                {
                    CheckBox chk = (CheckBox)itm.FindControl("cb_view");

                    if (chk != null)
                    {
                        if (chk.Checked)
                        {
                            values[0] = ((HiddenField)itm.FindControl("hf_TicketsViolationID")).Value;
                            values[1] = Convert.ToInt32(ViewState["vEmpID"]);
                            clsDb.ExecuteSP("USP_HTS_DOCKETCLOSEOUT_UPDATE_Dispose_Selected", keys, values);
                        }
                    }

                }
                GetRecords(ddl_courts.SelectedValue, ddl_status.SelectedIndex.ToString(), 0);
                bindGrid();
            }
            catch { }
        }

        private void bindGrid()
        {
            try
            {
                if (dsRecords.Tables[0].Rows.Count == 0)
                {
                    
                    if( dsRecords.Tables[1].Rows.Count == 0)
                    lbl_Message.Text = "No records found!";
                    
                    
                    td_HMC.Visible = false;
                    td_Title.Visible = false;
                    btn_updatestatus.Visible = false;
                }

                else
                {
                    td_Title.Visible = true;
                    btn_updatestatus.Visible = true;
                    lbl_Title.Text = cal_Date.SelectedDate.ToShortDateString() + " Docket";
                }
                
                m_oGroupingHelper = new GroupingHelper();
                m_oGroupingHelper.SetImageSwapper("../Images/collapse.gif", "../Images/expand.gif");
                               

                DataView dv = new DataView();
                dv.RowFilter = "ShortName='HMC-L'";
                dv.Table = dsRecords.Tables[0];
                //dtRecords.Columns.Add("blankspace");
                dsRecords.Tables[0].Columns.Add("blankspace");
                //DataTable dt = m_oGroupingHelper.SetupGroupedTable(dsRecords.Tables[0], "CourtNameComplete");
                DataTable dt = m_oGroupingHelper.SetupGroupedTable(dv.Table, "CourtNameComplete");
                rptrtable.DataSource = dt;
                rptrtable.DataBind();


               // if (dsRecords.Tables[1].Rows.Count > 0)
                {
                    dsRecords.Tables[1].Columns.Add("blankspace");
                    DataTable dt2 = m_oGroupingHelper.SetupGroupedTable(dsRecords.Tables[1], "CourtNameComplete");
                    rptr_table.DataSource = dt2;
                    rptr_table.DataBind();
                }


                if (ClientScript.IsClientScriptBlockRegistered("poorman_" + rptrtable.ID) == false)
                {
                    ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page),
                        "poorman_" + rptrtable.ID, m_oGroupingHelper.GetJS());
                }
                GetCourtStatus();
                FillDockets();


            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRecords(ddl_courts.SelectedValue,ddl_status.SelectedIndex.ToString(), 0);
           //fillSortingLists(dsRecords);
            bindGrid();
        }

        protected void lnkbtn_showall_Click(object sender, EventArgs e)
        {
            GetRecords("","",1);
            bindGrid();
        }

    }
}

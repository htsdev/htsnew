﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRptDisposedAlert.aspx.cs" Inherits="HTP.Reports.frmRptDisposedAlert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc41" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Disposed Alert</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">    
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
    
             <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <div>            
            
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                  
                   
                    <tr>
                        <td style="height: 34px" background="../Images/subhead_bg.gif">
                            <table>
                                <tr>
                                    <td align="left" class="clssubhead" width="50%">
                                        Disposed Alert
                                    </td>
                                    <td align="right" width="50%">
                                        
                                        <uc41:PagingControl ID="Pagingctrl" runat="server"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" >
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                        CssClass="clsLabel"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                          
                            <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="True" OnRowDataBound="gv_Records_RowDataBound"
                                            AllowPaging="True"  PageSize="30" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                             OnSorting="gv_Records_Sorting" OnRowCommand="gv_Records_RowCommand" EmptyDataText="No Record Found"> 
                                            <Columns>
                                            <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ticket No"  HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                    <!-- SAEED-7752-05/06/2010, replace TicketID_PK with RefCaseNumber: On user interface this filed is being displayed-->
                                                      <asp:Label ID="lbl_TicketNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber")%>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cause Number"  HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CauseNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber")%>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Left" />
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name"  HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name"  HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Auto Status</u>" SortExpression="AutoStatus" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_AutoStatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.AutoStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Verified Status"  HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_VerifiedStatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Auto Court Date</u>" SortExpression="AutoCourtDate" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_AutoCourtDate" runat="server" Width="110px" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.AutoCourtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Verified Court Date</u>" SortExpression="VerifiedCourtDate" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_VerifiedCourtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedCourtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                
                                                
                                                
                                                <asp:TemplateField HeaderText="<u>FollowUp Date</u>" SortExpression="FollowUpDate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FollowUpDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                        <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FirstName") %>' />
                                                        <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LastName") %>' />
                                                        <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("TicketID_PK") %>' />
                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("CauseNumber") %>' />
                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("ShortName") %>' />
                                                        
                                                        <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />                                                
                                        </asp:GridView>
                                        
                        </td>
                    </tr>
                    
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                        </td>
                    </tr>
                    <tr>
                            <td>
                               
                               
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            
                                            <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Traffic Waiting Follow Up(Non HMC and HCJP)" />
                                        </asp:Panel>
                                    
                               
                               
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                 
                            </td>
                        </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                    
                    
    
                   
                </table>
            </div>
                    </ContentTemplate>
                    </aspnew:UpdatePanel>
        
   
    
                            
    </form>
     <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>
</body>
</html>
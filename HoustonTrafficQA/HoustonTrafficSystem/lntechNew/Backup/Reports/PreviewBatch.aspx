<%@ Page language="c#" AutoEventWireup="false" Inherits="HTP.Reports.PreviewBatch" Codebehind="PreviewBatch.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Preview Batch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style type="text/css">.style1 { COLOR: #ffffff }
		</style>
		<script type="text/javascript">
		function refresh()
		{
		    
		    //Agha Usman Task 4044 16/05/2008
		    if (opener.document.Form1.txtStopBubbling != null) 
		    {
			    opener.document.Form1.txtStopBubbling.value = "1";		    
		    } 
		    //Nasir 5681 05/19/2009 reload page
		    //Saeed 8088 07/28/2010 replace !== with !=
		    if( window.opener != null && !window.opener.closed )
            {
            opener.location.href = opener.location;                                    
            }		
		     //window.returnValue = true;	
//			 opener.location.href = opener.location;
            
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="refresh();" >
		<form id="Form1" method="post" runat="server">
			<table height="100%" width="100%" border="0">
				<TR>
					<TD style="HEIGHT: 34px" background="../Images/subhead_bg.gif" colSpan="2"></TD>
				</TR>
				<tr>
					<td style="HEIGHT: 33px" vAlign="baseline" align="right"><span class="style1">.</span>
						<table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="185" align="left"
							border="0">
							<tr>
								<td width="31"><IMG height="17" src="../Images/head_icon.gif" width="31"></td>
								<td width="140" valign="baseline" class="clssubhead">Batch Report
								</td>
							</tr>
						</table>
					</td>
					<td style="HEIGHT: 33px" align="right"></td>
				</tr>
				<tr>
					<td height="9" colspan="2" background="../Images/separator_repeat.gif" style="HEIGHT: 14px">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<iframe src="frmBatchReport.aspx" width="100%" height="95%" scrolling="auto" frameborder="1">
						</iframe>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

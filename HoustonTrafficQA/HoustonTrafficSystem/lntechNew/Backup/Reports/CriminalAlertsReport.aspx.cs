﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class CriminalAlertsReport : System.Web.UI.Page
    {
        #region Variable
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsSession _clsSession = new clsSession();
        DataSet _ds;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        ResetGrid();
                        cal_UploadDateFilter.SelectedDate = DateTime.Now;
                        ddlCaseStatus.SelectedIndex = 0;
                    }

                    Pagingctrl1.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl1_PageIndexChanged);
                    Pagingctrl1.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl1_PageSizeChanged);
                    Pagingctrl1.GridView = gv_Records1;

                    Pagingctrl2.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl2_PageIndexChanged);
                    Pagingctrl2.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl2_PageSizeChanged);
                    Pagingctrl2.GridView = gv_Records2;

                    Pagingctrl3.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl3_PageIndexChanged);
                    Pagingctrl3.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl3_PageSizeChanged);
                    Pagingctrl3.GridView = gv_Records3;


                    Pagingctrl4.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl4_PageIndexChanged);
                    Pagingctrl4.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl4_PageSizeChanged);
                    Pagingctrl4.GridView = gv_Records4;
                }
            }
            catch (Exception ex)
            {
                lblMessage1.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ResetGrid();
                FillGrid(gv_Records1, 1);
                FillGrid(gv_Records3, 3);
                FillGrid(gv_Records2, 2);   
                //Muhammad Nasir 10528 11/13/2012 Commented as this is no more needed
                //Farrukh 10528 05/30/2013 Open for LastName & ZipCode Section
                FillGrid(gv_Records4, 4);
            }
            catch (Exception ex)
            {
                lblMessage1.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populate gridview by fatching data from trafficTickets db
        /// </summary>
        private void FillGrid(GridView _gv, int SearchType)
        {
            bool getData = ViewState[SearchType.ToString()] == null;

            // Load data from DataBase
            if (getData == true)
            {
                _gv.DataSource = null;
                _gv.DataBind();
                string[] keys = { "@UploadDate", "@CaseType", "@SearchType" };
                object[] values = { cal_UploadDateFilter.SelectedDate, Convert.ToInt32(ddlCaseStatus.SelectedValue), SearchType };
                _reports.getRecords("[USP_HTP_GetCriminalAlerts]", keys, values);
                _ds = _reports.records;
                generateSerialNo(_ds.Tables[0]);
                ViewState[SearchType.ToString()] = _ds;
            }
            else
            {
                // Get DataSet from ViewState
                _ds = (DataSet)ViewState[SearchType.ToString()];
            }

            if (_ds.Tables[0].Rows.Count > 0)
            {
                switch (SearchType)
                {
                    case 1:
                        Pagingctrl1.GridView = _gv;
                        _gv.DataSource = _ds.Tables[0];
                        _gv.DataBind();
                        Pagingctrl1.PageCount = _gv.PageCount;
                        Pagingctrl1.PageIndex = _gv.PageIndex;
                        Pagingctrl1.SetPageIndex();
                        Pagingctrl1.Visible = _ds.Tables[0].Rows.Count > 10;
                        break;
                    case 2:
                        Pagingctrl2.GridView = _gv;
                        _gv.DataSource = _ds.Tables[0];
                        _gv.DataBind();
                        Pagingctrl2.PageCount = _gv.PageCount;
                        Pagingctrl2.PageIndex = _gv.PageIndex;
                        Pagingctrl2.SetPageIndex();
                        Pagingctrl2.Visible = _ds.Tables[0].Rows.Count > 10;
                        break;
                    case 3:
                        Pagingctrl3.GridView = _gv;
                        _gv.DataSource = _ds.Tables[0];
                        _gv.DataBind();
                        Pagingctrl3.PageCount = _gv.PageCount;
                        Pagingctrl3.PageIndex = _gv.PageIndex;
                        Pagingctrl3.SetPageIndex();
                        Pagingctrl3.Visible = _ds.Tables[0].Rows.Count > 10;
                        break;
                    case 4:
                        Pagingctrl4.GridView = _gv;
                        _gv.DataSource = _ds.Tables[0];
                        _gv.DataBind();
                        Pagingctrl4.PageCount = _gv.PageCount;
                        Pagingctrl4.PageIndex = _gv.PageIndex;
                        Pagingctrl4.SetPageIndex();
                        Pagingctrl4.Visible = _ds.Tables[0].Rows.Count > 10;
                        break;
                }
            }
            else
            {
                _gv.DataSource = null;
                _gv.DataBind();
            }
        }

        /// <summary>
        /// Generate the serial no on the basis of Ticket Id
        /// </summary>
        /// <param name="dtRecords"></param>
        private void generateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
            }

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["TICKETID_PK"].ToString() != dtRecords.Rows[i]["TICKETID_PK"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
        }

        /// <summary>
        /// Reset Page Count and Index Count
        /// </summary>
        private void ResetGrid()
        {
            Pagingctrl1.PageCount = Pagingctrl1.PageIndex = 0;
            Pagingctrl1.SetPageIndex();

            Pagingctrl2.PageCount = Pagingctrl2.PageIndex = 0;
            Pagingctrl2.SetPageIndex();

            Pagingctrl3.PageCount = Pagingctrl3.PageIndex = 0;
            Pagingctrl3.SetPageIndex();

            Pagingctrl4.PageCount = Pagingctrl4.PageIndex = 0;
            Pagingctrl4.SetPageIndex();

            // Reset ViewState
            ViewState["1"] = ViewState["2"] = ViewState["3"] = ViewState["4"] = null;
        }

        #region Grid1
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl1_PageIndexChanged()
        {

            gv_Records1.PageIndex = Pagingctrl1.PageIndex - 1;
            FillGrid(gv_Records1, 1);

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl1_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records1.PageIndex = 0;
                gv_Records1.PageSize = pageSize;
                gv_Records1.AllowPaging = true;
            }
            else
            {
                gv_Records1.AllowPaging = false;
            }
            FillGrid(gv_Records1, 1);

        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Page Event Args</param>
        protected void gv_Records1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records1.PageIndex = e.NewPageIndex;
                FillGrid(gv_Records1, 1);
            }
            catch (Exception ex)
            {
                lblMessage1.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Explicitly binding rows with database fields.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Row Event Args</param>
        /*protected void gv_Records1_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }*/
        #endregion

        #region Grid2
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl2_PageIndexChanged()
        {

            gv_Records2.PageIndex = Pagingctrl2.PageIndex - 1;
            FillGrid(gv_Records2, 2);

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl2_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records2.PageIndex = 0;
                gv_Records2.PageSize = pageSize;
                gv_Records2.AllowPaging = true;
            }
            else
            {
                gv_Records2.AllowPaging = false;
            }
            FillGrid(gv_Records2, 2);

        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Page Event Args</param>
        protected void gv_Records2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records2.PageIndex = e.NewPageIndex;
                FillGrid(gv_Records2, 2);
            }
            catch (Exception ex)
            {
                lblMessage2.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Explicitly binding rows with database fields.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Row Event Args</param>
        /*protected void gv_Records2_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }*/
        #endregion

        #region Grid3
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl3_PageIndexChanged()
        {
            gv_Records3.PageIndex = Pagingctrl3.PageIndex - 1;
            FillGrid(gv_Records3, 3);
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl3_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records3.PageIndex = 0;
                gv_Records3.PageSize = pageSize;
                gv_Records3.AllowPaging = true;
            }
            else
            {
                gv_Records3.AllowPaging = false;
            }
            FillGrid(gv_Records3, 3);
        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Page Event Args</param>
        protected void gv_Records3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records3.PageIndex = e.NewPageIndex;
                FillGrid(gv_Records3, 3);
            }
            catch (Exception ex)
            {
                lblMessage3.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Explicitly binding rows with database fields.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Row Event Args</param>
        /*protected void gv_Records3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }*/
        #endregion

        #region Grid4
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl4_PageIndexChanged()
        {


            gv_Records4.PageIndex = Pagingctrl4.PageIndex - 1;
            FillGrid(gv_Records4, 4);
        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl4_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records4.PageIndex = 0;
                gv_Records4.PageSize = pageSize;
                gv_Records4.AllowPaging = true;
            }
            else
            {
                gv_Records4.AllowPaging = false;
            }
            FillGrid(gv_Records4, 4);
        }

        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Page Event Args</param>
        protected void gv_Records4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records4.PageIndex = e.NewPageIndex;
                FillGrid(gv_Records4, 4);
            }
            catch (Exception ex)
            {
                lblMessage4.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Explicitly binding rows with database fields.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">GridView Row Event Args</param>
        /*protected void gv_Records4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }*/
        #endregion

        #endregion
    }
}


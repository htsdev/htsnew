<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ReadComments.aspx.cs" Inherits="HTP.Reports.ReadComments" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Read Comments</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <script language="jscript" type="text/jscript">
    function Validation()
    {
      var text = document.getElementById("txtreadcomments").value;
      if(text == "")
      {
       alert("Please Enter Read Comments");
       return false;
      }
      
      //Kazim 3985 5/26/2008 Change the max length of textbox
      
      if(text.length > 950)
      {
           
           alert("Sorry You cannot type in more than 1000 characters in Read comments box")
			return false;		  
      }
      return true;
    }
    </script>

</head>
<body  class="clsleftpaddingtable">
    <form id="form1" runat="server">
        <table border="2" style="border-left-color: navy; border-bottom-color: navy; border-top-color: navy;
            border-collapse: collapse; border-right-color: navy">
            <tr>
                <td style="height: 175px">
                    <table border="1" cellpadding="0" cellspacing="0" class="clsleftpaddingtable" style="border-top-style: none;
                        border-collapse: collapse; border-bottom-style: none" width="350">
                        <tr id="Tr1" runat="server">
                        </tr>
                    </table>
                    <table style="width: 100%; height: 100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" valign="middle" style="height: 36px">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td align="left" class="clssubhead" style="width: 50%">
                                            Read Comments&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" class="clsleftpaddingtable">
                                <asp:TextBox ID="txtreadcomments" runat="server" Height="116px" TextMode="MultiLine"
                                    Width="348px" CssClass="clstextarea" MaxLength="500"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="center" class="clsleftpaddingtable">
                                <asp:Button ID="Button1" runat="server" Text="Submit " CssClass="clsbutton" OnClick="Button1_Click" OnClientClick="return Validation();" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

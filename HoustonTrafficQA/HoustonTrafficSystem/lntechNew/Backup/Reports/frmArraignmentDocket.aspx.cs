using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using System.IO;
using Configuration.Client;

namespace lntechNew.Reports
{
    public partial class frmArraignmentDocket : System.Web.UI.Page
    {
        clsCrsytalComponent ClsCr = new clsCrsytalComponent();
        clsLogger clog = new clsLogger();
        clsSession uSession = new clsSession();
        clsENationWebComponents clsDB = new clsENationWebComponents();
        // Asad Ali 7629 06/03/2010 Creating object IConfigruation 
        IConfigurationClient Client = ConfigurationClientFactory.GetConfigurationClient();
        //End 
        string Filename = "";
        string Name = "";
        string Path = "";
        string strPath = "";
        int day = 0;
        int flag = 0;
        // Asad Ali 7629 06/03/2010 getting Report Path From Congfiguration Services 
        public string ReportServerPath
        {
            get { return Client.GetValueOfKey(global::Configuration.Helper.Division.HOUSTON, global::Configuration.Helper.Module.TRAFFIC_PROGRAM, global::Configuration.Helper.SubModule.GENERAL, global::Configuration.Helper.Key.REPORT_FILE_PATH); }
        }
        //End 7629
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                    ViewState["vNTPATH"] = ConfigurationSettings.AppSettings["NTPath"].ToString();

                    CreateReport();

                }
            }
            catch (Exception ex)
            {
                Response.Write("Report Cannot be Loaded Session Timeoout Expired.");
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void CreateReport()
        {
            DataSet ds = null;

            try
            {
                day = Convert.ToInt32(Request.QueryString["Day"]);
                flag = Convert.ToInt32(Request.QueryString["Flag"]);
                Name = Session.SessionID + ".pdf";
                Path = ViewState["vNTPATHDocketPDF"].ToString() + Name;
                strPath = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH"].ToString().Length) - 5) + Name;

                if (flag == 1)
                {
                    if (day == 2)
                        Filename = Server.MapPath("") + "\\Monday.rpt";
                    else if (day == 3)
                        Filename = Server.MapPath("") + "\\Tuesday.rpt";
                    else if (day == 4)
                        Filename = Server.MapPath("") + "\\Wednesday.rpt";
                    else if (day == 5)
                        Filename = Server.MapPath("") + "\\Thursday.rpt";
                    else if (day == 6)
                        Filename = Server.MapPath("") + "\\Friday.rpt";
                    else if (day == 100)
                        // Asad Ali 7629 06/03/2010 Set Report Path
                        //Filename = Server.MapPath("") + "\\JudgeTrials.rpt";
                        Filename = ReportServerPath+ "\\JudgeTrials.rpt";
                    //End 7629
                    else if(day == 50)
                        Filename = Server.MapPath("") + "\\ArraignmentSnapShot_Single.rpt";
                    else
                        Filename = Server.MapPath("") + "\\NotAssigned.rpt";

                   
                    if (day != 100 && day != 99 && day!=50)//for Daywise Report...
                    {
                        string[] key ={ "@ReportWeekDay" };
                        object[] values ={ day };

                        ClsCr.CreateReport(Filename, "usp_HTS_ArraignmentSnapshotLong_ver_1_Update", key, values, this.Session, this.Response, Path);

                    }
                    else if (day == 100) //for Judge Trial /PR
                    {
                        // Afaq 8213 11/01/2010 Create both ProbabtionRequestReport and Continuance Report.
                        String[] subreportname = new String[] { "ProbabtionRequestReport.rpt","ContinuanceReport.rpt" };
                        ds = clsDB.Get_DS_BySP("usp_htp_get_JudgeTrial");
                        ClsCr.CreateSubReports(Filename, "usp_HTS_ArraignmentSnapshotLongFooterJury_ver_1_Update", subreportname, ds, this.Session, this.Response, Path,1);

                    }
                    else if (day == 50)
                    {
                        String[] subreportname = new String[] { "ArraignmentSnapShot_Mond.rpt", "ArraignmentSnapShot_Tuesday.rpt", "ArraignmentSnapShot_Wednesday.rpt", "ArraignmentSnapShot_Thursday.rpt", "ArraignmentSnapShot_Friday.rpt" };
                        ds = clsDB.Get_DS_BySP("Usp_HTS_GET_ArraignmentSnapShot_Single");
                        ClsCr.CreateSubReports(Filename, "usp_hts_arraigmentsnapshot_monday", subreportname, ds, this.Session, this.Response, Path,0);

                    }
                    else
                    {
                        String[] subreportname = new String[] { "OutsideTrials.rpt" };

                        string[] key ={ "@ReportWeekDay" };
                        object[] values ={ day };

                        ds = clsDB.Get_DS_BySP("usp_HTS_ArraignmentSnapshotLongFooterOutsideArraignment_ver_1_Update");
                        ClsCr.CreateSubReports(Filename, "usp_HTS_ArraignmentSnapshotLong_ver_1_Update", subreportname, key, values, ds, this.Session, this.Response, Path);
                    }
                }
                else// for print all...
                {


                    String[] subreportsname = new String[] { "ArraignmentSnapShot_Mond.rpt", "ArraignmentSnapShot_Tuesday.rpt", "ArraignmentSnapShot_Wednesday.rpt", "ArraignmentSnapShot_Thursday.rpt", "ArraignmentSnapShot_Friday.rpt", "Monday.rpt", "Tuesday.rpt", "Wednesday.rpt", "Thursday.rpt", "Friday.rpt", "JudgeTrials.rpt", "ProbabtionRequestReport.rpt", "NotAssigned.rpt", "OutsideTrials.rpt" };
                    ds = clsDB.Get_DS_BySP("Usp_HTS_GET_ArraignmentDocket_SubReport");
                    Filename = Server.MapPath("") + "\\ArraignmentSnapShot_Monday.rpt";

                    ClsCr.CreateSubReports(Filename, "usp_hts_arraigmentsnapshot_monday", subreportsname, ds, this.Session, this.Response, Path,0);
                }
                // Response.Redirect("../Docstorage" + strPath, false); // Adil 7629 05/28/2010 code commented
                // Adil 7629 05/28/2010 new code implemented to write report response in a better way.
                Response.ClearContent();

                FileInfo file = new FileInfo(ViewState["vNTPATH"].ToString() + strPath);

                Response.AddHeader("Content-Disposition", "inline; filename=" + file.Name);

                // Add the file size into the response header
                Response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                Response.ContentType = "application/pdf";

                // Write the file into the response
                Response.WriteFile(file.FullName);

                Response.Flush();

                Response.Close();

                file.Delete();
                 

            }
            catch (Exception ex)
            {
                Response.Write("Report Cannot be Loaded Session Timeoout Expired.");
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
    public partial class ContinuanceReport : System.Web.UI.Page
    {

        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        string[] TicketidArry;
        int cticket = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbl_message.Text = "";
                fillStatus();

                datefrom.SelectedDate = DateTime.Now;
                dateto.SelectedDate = DateTime.Now;

                Session["cstartdate"]="1/1/1900";
                Session["cenddate"] = "1/1/1900";

                FillCourts();
                if (datefrom.SelectedDate.ToShortDateString() == "1/1/0001" || dateto.SelectedDate.ToShortDateString() == "1/1/0001")
                {
                    Session["cstartdate"] = "01/01/1900";
                    Session["cenddate"] = "01/01/1900";
                    datefrom.Clear();
                    dateto.Clear();
                }
                else
                {
                    Session["cstartdate"] = datefrom.SelectedDate.ToShortDateString();
                    Session["cenddate"] = dateto.SelectedDate.ToShortDateString();
                }

                fillGrid("","");
            }
        }


        private void fillStatus()
        {

            try
            {
                DataSet ds = clsdb.Get_DS_BySP("USP_HTS_GetContinousStatus");
                cbl_status.DataSource = ds.Tables[0];
                cbl_status.DataTextField = "Description";
                cbl_status.DataValueField = "StatusID";
                cbl_status.DataBind();                        
                            
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }

        }

        private void fillGrid(string sortExpression ,string status)
        {
            try
            {
                string[] keys = { "@StartDate", "@EndDate", "@CourtID", "@ViewAll", "@Status" };
                object[] values = { Session["cstartdate"].ToString(), Session["cenddate"].ToString(),ddl_courts.SelectedValue,chk_viewall.Checked, status };


                DataSet ds = clsdb.Get_DS_BySPArr("USP_HTS_Get_ContinuanceReport", keys, values);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    lbl_message.Text = "No Record Found";
                }
                else
                {
                    GenerateSerial(ds.Tables[0]);
                    TicketidArry = new string[ds.Tables[0].Rows.Count];

                    if (sortExpression != "")
                    {

                        if (Session[sortExpression] == null)
                        {
                            Session[sortExpression] = " ASC";
                        }


                        DataView dv = new DataView(ds.Tables[0]);
                        dv.Sort = sortExpression + Session[sortExpression].ToString();
                        gv_results.DataSource = dv;

                        if (Session[sortExpression] == " ASC")
                            Session[sortExpression] = " DESC";
                        else
                            Session[sortExpression] = " ASC";


                    }
                    else
                    {
                        gv_results.DataSource = ds.Tables[0];
                    }
                }
                    gv_results.DataBind();
                
                

            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
            
        }


        private string getStatus()
        {
            string statues = "";

            foreach (ListItem item in cbl_status.Items)
            {
                if (item.Selected)
                    statues += item.Value + ",";
            }

            if (statues != "")
                statues = statues.Remove(statues.Length - 1, 1);
            
            return statues;

        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            lbl_message.Text = "";
            if (datefrom.SelectedDate.ToShortDateString() == "1/1/0001" || dateto.SelectedDate.ToShortDateString() == "1/1/0001")
            {
                Session["cstartdate"] = "01/01/1900";
                Session["cenddate"] = "01/01/1900";
                datefrom.Clear();
                dateto.Clear();
            }
            else 
            {
                Session["cstartdate"] = datefrom.SelectedDate.ToShortDateString();
                Session["cenddate"] = dateto.SelectedDate.ToShortDateString();
            }

            
            
            fillGrid("",getStatus());
        }

        protected void gv_results_Sorting(object sender, GridViewSortEventArgs e)
        {        
          
            fillGrid(e.SortExpression,  getStatus());
          
        }

      
        protected void gv_results_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string sticketid = "";
            if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            {    
                Label ticketid = (Label)e.Row.FindControl("lbl_ticketid");
                
                sticketid = ((Label)e.Row.FindControl("lbl_ticketid")).Text.ToString();
                ((ImageButton)e.Row.FindControl("imgPreviewDoc")).Attributes.Add("onclick", "javascript:return PopUp(" + sticketid.ToString() + ");");



            }
        }

        protected void FillCourts()
        {
            try
            {
                lntechNew.Components.ClientInfo.clsCourts clscourts = new lntechNew.Components.ClientInfo.clsCourts();
                DataSet ds = clscourts.GetShortCourtName();

                ddl_courts.DataSource = ds;
                ddl_courts.DataTextField = ds.Tables[0].Columns["ShortName"].ColumnName; ;
                ddl_courts.DataValueField = ds.Tables[0].Columns["courtid"].ColumnName;
                ddl_courts.DataBind();
              
                ddl_courts.Items.Insert(1, "All Courts");
                ddl_courts.Items[1].Value  = "1";
                ddl_courts.Items.Insert(2, "Inside Courts");
                ddl_courts.Items[2].Value  = "2";
                ddl_courts.Items.Insert(3, "Outside Courts");
                ddl_courts.Items[3].Value  = "3";


            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
            }
        }

        private void GenerateSerial(DataTable dt)
        {

            try
            {


                int no = 1;
                string ticketid = "";
                int sno = 1;
                if (dt.Columns["SNO"] == null)
                {
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                else
                {
                    dt.Columns.Remove("SNO");
                    dt.Columns.Add("SNO");
                    dt.Rows[0]["SNO"] = no;
                }
                ticketid = dt.Rows[0]["Ticketid_pk"].ToString();

                for (int i = 1; i < dt.Rows.Count; i++)
                {

                    string tid = dt.Rows[i]["Ticketid_pk"].ToString();
                    if (tid != "")
                    {
                        if (ticketid != tid)
                        {
                            no++;
                            dt.Rows[i]["SNO"] = no;
                            ticketid = tid;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lbl_message.Text = ex.Message;
            }


        }
    }
}

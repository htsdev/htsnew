using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LNHelper;
using HTP.Components;

namespace HTP.Reports
{
    public partial class ChildCustodyCaseDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Display();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }
        }

        //Zeeshan Ahmed 4152 06/02/2008
        private void Display()
        {
            if (Request.QueryString["childcustodyid"] != null)
            {
                lblmain.Visible = false;
                TableMain.Visible = true;

                ChildCustodyCase childCustodyCase = new ChildCustodyCase();

                string childcustodyid = Request.QueryString["childcustodyid"];

                DataTable childCustodyCaseDetail = childCustodyCase.GetChildCustodyCaseInformation(childcustodyid);

                if (childCustodyCaseDetail.Rows.Count > 0)
                {
                    lblError1.Visible = false;
                    tblCaseInformation.Visible = true;

                    DataRow CaseInformation = childCustodyCaseDetail.Rows[0];
                    lblstyle.Text = Convert.ToString(CaseInformation["Style"]);
                    lblcasenumber.Text = Convert.ToString(CaseInformation["CaseNumber"]);
                    lblpartyrequest.Text = Convert.ToString(CaseInformation["PartyRequest"]);
                    lblresult.Text = Convert.ToString(CaseInformation["Result"]);
                    lblsettingdate.Text = Convert.ToString(CaseInformation["SettingDate"]);
                    lblcurrentcourt.Text = Convert.ToString(CaseInformation["CurrentCourt"]);
                    lblfilecourt.Text = Convert.ToString(CaseInformation["FileCourt"]);
                    lblnextsettingdate.Text = Convert.ToString(CaseInformation["NextSettingDate"]);
                    lblvolume.Text = Convert.ToString(CaseInformation["Volume"]);
                    lblfilelocation.Text = Convert.ToString(CaseInformation["FileLocation"]);
                    lblplaintiffs.Text = Convert.ToString(CaseInformation["plaintiffs"]);
                    lbldefendants.Text = Convert.ToString(CaseInformation["Defendants"]);
                    lbljudgementfor.Text = Convert.ToString(CaseInformation["JudgementFor"]);
                    lbljudgementdate.Text = Convert.ToString(CaseInformation["JudgementDate"]);
                    lblcasestatus.Text = Convert.ToString(CaseInformation["CaseStatus"]);
                    lblcasetype.Text = Convert.ToString(CaseInformation["CaseType"]);
                    lblsettingtype.Text = Convert.ToString(CaseInformation["SettingType"]);
                    lblcourtlocation.Text = Convert.ToString(CaseInformation["CourtName"]);
                    lblrecloaddate.Text = Convert.ToString(CaseInformation["RecLoadDate"]);
                }
                else
                {
                    lblError1.Visible = true;
                    tblCaseInformation.Visible = false;
                }

                DataTable childCustodyParties = childCustodyCase.GetChildCustodyPartyInformation(childcustodyid);

                if (childCustodyParties.Rows.Count > 0)
                {
                    lblerror2.Visible = false;
                    gvParties.Visible = true;
                    gvParties.DataSource = childCustodyParties;
                    gvParties.DataBind();
                }
                else
                {
                    gvParties.Visible = false;
                    lblerror2.Visible = true;
                }
            }
            else
            {
                lblmain.Visible = true;
                TableMain.Visible = false;
            }
        }

    }
}

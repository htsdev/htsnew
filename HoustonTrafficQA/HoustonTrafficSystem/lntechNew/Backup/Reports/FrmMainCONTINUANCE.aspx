<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.FrmMainCONTINUANCE" Codebehind="FrmMainCONTINUANCE.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CONTINUANCE REPORT</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style type="text/css">.style1 { COLOR: #ffffff }
		</style>
<script>
		function refresh()
		{		
		 //window.opener.location.reload();
		 opener.location.href = opener.location;
		}
</script>
</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout" onload="refresh();">
		<form id="Form1" method="post" runat="server">
			<table height="100%" width="100%" border="0">
				<TR>
					<TD style="HEIGHT: 34px" background="../Images/subhead_bg.gif" colSpan="2"></TD>
				</TR>
				<tr>
					<td style="HEIGHT: 33px" vAlign="baseline" align="right"><span class="style1">.</span>
						<table class="clsmainhead" id="Table3" cellSpacing="0" cellPadding="0" width="185" align="left"
							border="0">
							<tr>
								<td width="31"><IMG height="17" src="../Images/head_icon.gif" width="31"></td>
								<td width="140" valign="baseline" class="clssubhead">Continuance Report
								</td>
							</tr>
						</table>
					</td>
					<td style="HEIGHT: 33px" align="right"><asp:ImageButton ID="IBtn" runat="server" Width="78px" ImageUrl="../Images/MSSmall.gif" Height="23px"></asp:ImageButton></td>
				</tr>
				<tr>
					<td height="9" colspan="2" background="../Images/separator_repeat.gif" style="HEIGHT: 14px">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<iframe src="frmCONTINUANCE.aspx?casenumber=<%=ViewState["vTicketId"]%>" width="100%" height="95%" scrolling="auto" frameborder="1">
						</iframe>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

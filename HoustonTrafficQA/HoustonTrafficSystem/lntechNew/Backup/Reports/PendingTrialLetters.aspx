<%@ Page Language="C#" AutoEventWireup="true" Codebehind="PendingTrialLetters.aspx.cs"
    Inherits="lntechNew.Reports.PendingTrialLetters" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No Trial letter Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript">
    function DeleteConfirm()
    {
        var isDelete=confirm("Are you sure you want to remove this client from this report?");
        if(isDelete== true)
        {
           return true;
        }
        
        else
        {
            return false;
        }
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tr>
                    <td>
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../../images/separator_repeat.gif"  height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                </tr>
                <br />
                       <tr>
                      
                  <td style="text-align: right" class="clsleftpaddingtable">
                  <uc3:PagingControl ID="Pagingctrl" runat="server" />
                             
                  </td>
                </tr>
            
                <tr>
                    <td background="../../images/separator_repeat.gif"  height="11">
                    </td>
                </tr>
                <tr>
                  <td style="text-align: right">
                     
                  </td>
                </tr>
                <tr>
                                      
                    <td>
                        <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                            Width="100%" OnRowDataBound="gv_Data_RowDataBound" AllowPaging="True" OnPageIndexChanging="gv_Data_PageIndexChanging" OnRowCommand="gv_Data_RowCommand" AllowSorting="True" OnSorting="gv_Data_Sorting" PageSize="30">
                            <Columns>
                                <asp:TemplateField HeaderText="S#">
                                 <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                       <ItemTemplate>
                                        <asp:HyperLink ID="hl_SNo" runat="server" Text='<%# Container.DataItemIndex +1  %>'></asp:HyperLink>
                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Number">
                                 	 <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                           			
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Name&lt;/u&gt;" SortExpression="clientName">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Name" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.clientName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Crt Date &amp; Time&lt;/u&gt;" SortExpression="CourtDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="label" Text='<%# bind("CourtDate","{0:MM/dd/yyyy @hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Crt Loc &lt;/u&gt;" SortExpression="crtlocation">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcrtloc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.crtlocation") %>' CssClass="label"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="&lt;u&gt;Status&lt;/u&gt;" SortExpression="status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>' CssClass="label"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rep">
                             	 <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                           				
                                    <ItemTemplate>
                                        <asp:Label ID="lblRepName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>' CssClass="label"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandArgument='<%# bind("ticketid_pk") %>'
                                            CommandName="Del" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NextPrevious" NextPageText="Next &gt;" PreviousPageText="&lt; Previous" />
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
                <TR>
				<TD background="../images/separator_repeat.gif" height="11"></TD>
			</TR>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

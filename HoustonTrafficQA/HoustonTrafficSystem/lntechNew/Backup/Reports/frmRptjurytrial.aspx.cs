using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
namespace lntechNew.Reports
{
    public partial class frmRptjurytrial : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_court.Items.Clear();
                    ddl_attorney.Items.Clear();

                    ///////////  Fill drop down with cournames
                    DataSet ds = ClsDb.Get_DS_BySP("usp_Get_CourtName");
                    int i = 1;
                    ddl_court.Items.Add("------------- Select ------------");
                    ddl_court.Items[0].Value = "0";

                    foreach (DataRow rw in ds.Tables[0].Rows)
                    {
                        ddl_court.Items.Add(rw["courtname"].ToString());
                        ddl_court.Items[i].Value = rw["courtid"].ToString();
                        i++;
                    }

                    ///////// fill drop down with firtsname+lastname from tblusers
                    DataSet dsattorney = ClsDb.Get_DS_BySP("usp_Get_Attorney");
                    i = 1;
                    ddl_attorney.Items.Add("---- Select ----");
                    ddl_attorney.Items[0].Value = "0";

                    foreach (DataRow rw in dsattorney.Tables[0].Rows)
                    {
                        ddl_attorney.Items.Add(rw["name"].ToString());
                        ddl_attorney.Items[i].Value = rw["EmployeeID"].ToString();
                        i++;
                    }

                    FillGrid();
                    SetNavigation();
                }
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
            }
        }
        private void FillGrid()
        {
            try
            {
                
                    string[] key1 = { "@stdate", "@enddate", "@attorney", "@Court" };
                    object[] value1 = { dtFrom.SelectedDate, dtTo.SelectedDate, ddl_attorney.SelectedValue, ddl_court.SelectedValue };
                                    
                    DataTable dt = ClsDb.Get_DT_BySPArr("usp_Get_jurytrial", key1, value1);
                    if (dt.Rows.Count > 0)
                    {

                        dgJuryTrial.DataSource = dt;
                        dgJuryTrial.DataBind();
                        FillPageList();
                        SetNavigation();
                    }
                    else
                    {
                        dgJuryTrial.DataSource = null;
                        dgJuryTrial.DataBind();
                        lblMessage.Text = "No Record found";
                        lblMessage.Visible = true;
                    }
                

            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
            }
        }

        protected void dgJuryTrial_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
                    return;

                //DataRowView drv = (DataRowView)e.Item.DataItem;
                Label lbldes = (Label)e.Item.FindControl("lblbrieffacts");
                if (lbldes.Text != "")
                {
                   // HtmlControl ctrl = (HtmlControl)e.Item.FindControl("div_status");
                    

                    //string descript = lbldes.Text;
                    //ctrl.Attributes.Add("Title", "hideselects=[on] offsetx=[-200] offsety=[0] singleclickstop=[on] requireclick=[off] header=[] body=[<table width='225px' border='0' cellspacing='0'  class='label' style='BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none'><tr><td class='Label'>" + descript + "</td></tr></table>]");

                    ImageButton img = (ImageButton)e.Item.FindControl("ImageButton1");

                    img.Attributes.Add("OnMouseOver", "StateTracePoupup('" + e.Item.ClientID + "_lblbrieffacts" + "');");
                   img.Attributes.Add("OnMouseOut", "CursorIcon2()");
                   img.Attributes.Add("onClick", "copyToClipboard(document.getElementById('" + e.Item.ClientID + "_lblbrieffacts').innerText);return false");
                   //copyToClipboard(document.getElementById('txt_StateTraceMsg').value);
                        }
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
            }
        }
       
        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblMessage.Text = "";
                FillGrid();               
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message + Ex.InnerException;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        // GotoPage to navigation 
        private void GotoPage(int pageno)
        {
            try
            {
                dgJuryTrial .CurrentPageIndex = pageno;
                lblPNo.Text = Convert.ToString(pageno);
                FillGrid();
                FillPageList();
                SetNavigation();
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;                
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
        // Fill Dropdown with all page number of grid
        private void FillPageList()
        {
            try
            {
                Int16 idx;

                cmbPageNo.Items.Clear();
                for (idx = 1; idx <= dgJuryTrial.PageCount; idx++)
                {
                    cmbPageNo.Items.Add("Page - " + idx.ToString());
                    cmbPageNo.Items[idx - 1].Value = idx.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void SetNavigation()
        {
            try
            {
                // setting current page number
                lblPNo.Text = Convert.ToString(dgJuryTrial.CurrentPageIndex + 1);
                lblCurrPage.Visible = true;
                lblPNo.Visible = true;

                // filling combo with page numbers
                lblGoto.Visible = true;
                cmbPageNo.Visible = true;
               
                    cmbPageNo.SelectedIndex = dgJuryTrial.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }       
        
        protected void dgJuryTrial_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                this.lblMessage.Text = "";
                GotoPage(e.NewPageIndex);

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void cmbPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.lblMessage.Text = "";
                GotoPage(cmbPageNo.SelectedIndex);

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

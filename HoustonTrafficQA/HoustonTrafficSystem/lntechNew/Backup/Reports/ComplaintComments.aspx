﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplaintComments.aspx.cs"
    Inherits="HTP.Reports.ComplaintComments" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Namespace="eWorld.UI" TagPrefix="ew" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="obout" Namespace="OboutInc.Flyout2" Assembly="obout_Flyout2_NET"%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Complaint Comment Report</title>
    <link href="../Styles.css" type="text/css" rel="Stylesheet" />
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script src="../Scripts/boxover.js" type="text/javascript"></script>
    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>
   
    <script language="JavaScript" type="text/javascript">
		
	    //to show commment on mouse hower
		function CommentPoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg()
		     
		}
		
		
		//change cursor icon
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
		function ShowMsg()
        {   
            document.getElementById("txt_CommentsMsg").value=err;
        }
        
        
      //check if update with out selection and list of comma seperate ticket ids
    function get_check_value()
        {
                                
            var a = 0;        
                        
            var c_value = "";
            var grid = document.getElementById("<%= gv_Records.ClientID%>");
            var cell;
            if(grid != null && grid.rows.length > 0)
            {
            
                
               for(i = 2; i<= grid.rows.length;i++)
                {
                    if(i<10)
                    {
                    var chk = 'gv_Records_ctl0'+i.toString()+'_chkReview';
                    var hf='gv_Records_ctl0'+i.toString()+'_hf_NoteID';
                    }
                    else
                    {
                    var chk = 'gv_Records_ctl'+i.toString()+'_chkReview';
                    var hf='gv_Records_ctl'+i.toString()+'_hf_NoteID';
                    }
                    var chkbox=document.getElementById(chk);
                    if(chkbox != null && chkbox.checked)
                    {
                        a=1;
                       c_value = c_value + document.getElementById(hf).value + ",";
                    }
                    
                  }
               }
                                              
               if(a==0)
                    {
                        alert("Please select a contact to associate.");
                        return false;
                    }
               document.getElementById("hf_NoteIDs").value=c_value;
               
        }
        
        
        function CheckDateValidation()
        { 
            // abid ali 5387 1/15/2009 date comparision
            if (IsDatesEqualOrGrater(document.form1.cal_FromDateFilter.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, Start Date must be grater then or equal to 1/1/1900");
			    // Fahad 4354 1/16/2009 comment out
				//document.form1.cal_FromDateFilter.focus(); 
				return false;
			}
			
			if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal Start Date 1/1/1900");
				// Fahad 4354 1/16/2009 comment out
				//document.form1.cal_ToDateFilter.focus(); 
				return false;
			}
            if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value,'MM/dd/yyyy',document.form1.cal_FromDateFilter.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to Start Date");
			    // Fahad 4354 1/16/2009 comment out
				//document.form1.cal_ToDateFilter.focus(); 
				return false;
			}
			else
			{
			    return true;
			}
        }
		
    </script>
    

</head>
<body>
 
    <form id="form1" runat="server">
    
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellpadding="0" cellspacing="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:activemenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellpadding="0" width="100%">
                        <tr style="width: 100%">
                            <td style="width: 8%">
                                <span class="clssubhead">Start Date :</span>
                            </td>
                            <td style="width: 10%">
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">End Date :</span>
                            </td>
                            <td style="width: 10%">
                                <ew:CalendarPopup Visible="true" ID="cal_ToDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 10%">
                                <asp:CheckBox ID="chkShowAll" class="clssubhead" Text="Show All" runat="server" />
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">Attorneys :</span>
                            </td>
                            <td style="width: 46%">
                                <asp:DropDownList ID="ddlAttorneys" CssClass="clsInputCombo" runat="server">
                                </asp:DropDownList>
                            </td>
                            
                        </tr>
                    </table>
                    <table width="100%">
                        <tr style="width: 100%">
                            <td style="width: 8%">
                                <span class="clssubhead">Case Type :</span>
                            </td>
                            <td style="width: 10%">
                                <asp:DropDownList ID="ddlCaseType" CssClass="clsInputCombo" DataMember="CaseTypeName"
                                    DataValueField="CaseTypeId" DataTextField="CaseTypeName" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 10%">
                                <asp:Button ID="btnSearch" Text="Search" OnClientClick="return CheckDateValidation();" OnClick="btnSearch_Click" CssClass="clsbutton"
                                    runat="server" />
                                    
                            </td>
                            <td style="width: 72%" align="right">
                                                            <asp:Button ID="btnUpdate" Text="Update"  CssClass="clsbutton"
                                    runat="server" onclick="btnUpdate_Click" Visible="false" OnClientClick="return get_check_value()" />
                                    
                            
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <uc3:pagingcontrol ID="Pagingctrl" runat="server" />
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="20"
                                            OnPageIndexChanging="gv_Records_PageIndexChanging" OnSorting="gv_Records_Sorting"
                                            OnRowDataBound="gv_Records_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Name" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>' ></asp:Label>
                                                       <%--Asad Ali 7480 04/06/2010 using obout tool tip--%>
                                                        <obout:Flyout runat="server" ID="FlyoutToolTipFullName"  AttachTo="lbl_Name" Align="TOP" FadingEffect="true" Position="TOP_RIGHT" >
                                                               <asp:Panel ID="pnlTooltipFullName" runat="server" CssClass="QuickLinksFlyoutPopup">
                                                               </asp:Panel>                                                               
                                                            </obout:Flyout>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Crt Loc</u>" SortExpression="CourtLocation" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CrtLoc" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtLocation") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="Status" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Crt Date & Time</u>" SortExpression="CourtDateMain" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Courtdate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Courtdate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Attorney" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>C-Date</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Complaint" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>   
                                                    <asp:Label ID="lbl_Complaint" CssClass="GridItemStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Complaint") %>' ></asp:Label>
                                                    <%--Asad Ali 7480 04/06/2010 using obout tool tip--%>
                                                           <obout:Flyout runat="server" ID="FlyoutToolTip"  AttachTo="lbl_Complaint" Align="TOP" FadingEffect="false" FlyingEffect="BOTTOM_LEFT" Position="TOP_LEFT" >
                                                               <asp:Panel ID="pnlTooltip" GroupingText="Complaint Comments" runat="server" CssClass="QuickLinksFlyoutPopup"  Height="210px" Width="425" ScrollBars="Auto" >
                                                               </asp:Panel>
	                                                        </obout:Flyout>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <%-- Abid Ali 5018 12/24/2008 Criminal Report Addition --%>
                                                <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                      <%--Asad Ali 7480 04/06/2010 using obout tool tip--%>
                                                          <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Comments") %>' />
                                                          <asp:HiddenField ID="hf_NoteID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.NotesID_PK") %>' />                                         
                                                          <asp:Image ID="imgComments" ImageUrl="~/Images/comments.png" runat="server" />
                                                          <obout:Flyout runat="server" ID="ToolTipComments"  AttachTo="imgComments" Align="TOP" FadingEffect="false" FlyingEffect="BOTTOM_LEFT" Position="TOP_LEFT" >
                                                               <asp:Panel ID="pnlTooltipComments" runat="server" CssClass="QuickLinksFlyoutPopup"  Height="190px" Width="420px" ScrollBars="Auto" >
                                                               </asp:Panel>
                                                            </obout:Flyout>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Review" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkReview"  runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"  />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <asp:HiddenField ID="hf_NoteIDs" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                                    <td background="../../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:Footer ID="Footer1" runat="server" />
                                        
                                    </td>
                                </tr>
        </table>
    </div>
    </form>
    <script language="JavaScript" type="text/javascript" src="../Scripts/wz_tooltip.js"></script>
</body>
</html>

using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using lntechNew.Components;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using Configuration.Client;
using Configuration.Helper;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Reports
{
    /// <summary>
    /// Summary description for Word_Receipt.
    /// </summary>
    public partial class Word_Receipt : System.Web.UI.Page
    {
        int ticketno;
        int empid;
        //SqlDataReader SqlDr;


        //protected System.Web.UI.WebControls.ImageButton IBtn;
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession uSession = new clsSession();
        protected System.Web.UI.WebControls.ImageButton imgbtn_email;
        protected System.Web.UI.WebControls.ImageButton imgbtn_send;
        protected System.Web.UI.WebControls.ImageButton imgbtn_cancel;
        protected System.Web.UI.WebControls.TextBox txt_to;
        protected System.Web.UI.WebControls.TextBox txt_cc;
        protected System.Web.UI.WebControls.TextBox txt_bcc;
        protected System.Web.UI.WebControls.TextBox txt_subject;
        protected System.Web.UI.WebControls.TextBox txt_message;
        clsLogger clog = new clsLogger();
        clsSession ClsSession = new clsSession();
        clsCase cCase = new clsCase();
        //Zeeshan Haider 11021 05/13/2013 Remove export functionality of contract in traffic program to other than admin users
        clsUser clsUser = new clsUser();

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Noufil 3546 03/27/2008 Session sometime bocome null
            try
            {
                //Validating session
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    //ticketno =Convert.ToInt32(uSession.GetCookie("sTicketID",this.Request));
                    ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
                    ViewState["vTicketId"] = ticketno;
                    DataSet ds = cCase.GetClientEmail(ticketno);
                    ViewState["vEmail"] = ds.Tables[0].Rows[0][0].ToString();
                    empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                    //Session["Batch"] = Request.QueryString["Batch"].ToString();
                    imgbtn_email.Attributes.Add("onclick", "return ShowEmail();");
                    imgbtn_cancel.Attributes.Add("onclick", "return HideEmail();");
                    //Zeeshan Haider 11021 05/13/2013 Remove export functionality of contract in traffic program to other than admin users
                    //Faique Ali 11226 06/21/2013 export to word show for all user in criminal cases and primary user for Traffic cases..
                    //Zeeshan Haider 11021 05/13/2013 Remove export functionality of contract in traffic program to other than admin users
                    //IBtnExportWord.Visible = ((clsUser.GetSystemUserInfo(empid).AccessType == 2 && clsUser.GetSystemUserInfo(empid).Status == 1));
                    cCase.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
                    if (cCase.IsCriminalCase()) //For Criminal Case
                    {
                        IBtnExportWord.Visible = true;
                    }
                    else //For Non-Criminal Case
                    {
                        IBtnExportWord.Visible = ((clsUser.GetSystemUserInfo(empid).AccessType == 2 && clsUser.GetSystemUserInfo(empid).Status == 1));
                    }

                    //Zeeshan Haider 11072 05/15/2013 Enable controls once iframe loaded
                    IBtnExportWord.Enabled = false;
                    btn_SendToBatch.Enabled = false;
                    imgbtn_email.Enabled = false;

                    //Comment By Zeeshan Ahmed
                    //btn_SendToBatch.Attributes.Add("onclick","return SendToBatch();");

                    //if (IsAlreadyInBatch(ticketno))
                    //    ViewState["InBatch"] = 1;
                    //else
                    //    ViewState["InBatch"] = 0;

                    //Zeeshan Ahmed 3268 02/27/2008
                    ViewState["InBatch"] = 0;
                    btn_SendToBatch.Visible = false;
                    //Mohammad Ali 8132 23/02/2012 If user input e-mail address in invalid format then need to display the message. 
                    imgbtn_send.Attributes.Add("onclick", "return Validate2();");
                    //assigning email
                    if (txt_to.Text == "")
                    {
                        txt_to.Text = ViewState["vEmail"].ToString();
                    }
                }
            }

            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        //public void CreateBondReportEng()
        //{
        //    try
        //    {
        //        string[] key = { "@ticketid", "@employeeid" };
        //        object[] value1 = { ticketno, empid };
        //        //		int   LetterType = 3;				
        //        string filename = Server.MapPath("") + "\\receipt.rpt";
        //        Cr.CreateReportWord(filename, "USP_HTS_get_receipt_info", key, value1, this.Session, this.Response);
        //    }
        //    catch (Exception ex)
        //    {
        //        clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }
        //}


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.IBtn.Click += new System.Web.UI.ImageClickEventHandler(this.IBtn_Click);
            this.imgbtn_send.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtn_send_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        //private void IBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        //{
        //    CreateBondReportEng();
        //}

        private void imgbtn_send_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                //Zeeshan Haider 11323 08/02/2013 Get latest cover sheet name from database.
                //Zeeshan Haider 11367 08/29/2013 Made changes for Trial Notification letter.
                string[] key = { "@ticketID", "@letterID" };
                object[] value = { ticketno, 3 };
                DataSet ds = ClsDb.Get_DS_BySPArr("USP_HTP_GET_Updated_Cover_Sheet_Name", key, value);
                if (ds.Tables[0].Rows.Count > 0 && !(string.IsNullOrEmpty(Convert.ToString(Session["Tfilepath"]))))
                {
                    //Asad Ali 7925 07/21/2010 Get reply to value from configuration Services...
                    IConfigurationClient Client = ConfigurationClientFactory.GetConfigurationClient();
                    //string filename = Session["Tfilepath"].ToString();
                    string docName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                    string vpath = ConfigurationManager.AppSettings["NTPATHBatchPrint"].ToString();
                    vpath = vpath.Replace("\\\\", "\\");
                    string filename = vpath + docName;
                    //Sabir Khan 7148 12/16/2009 send email from info@sullolaw instead of logged in user...
                    //Getting sales rep email
                    //string emailrep = clog.SalesRepEmail(empid);
                    //Getting sales rep email
                    //if (emailrep.Length == 0)
                    //string emailrep = (string)ConfigurationSettings.AppSettings["MailFrom"];
                    //Asad Ali 7925 07/21/2010 Get reply to value from configuration Services...
                    string emailrep = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.EMAIL,
                                                           Key.EMAIL_FROM_ADDRESS);
                    //Calling send email function


                    clog.EmailReport(txt_to.Text, emailrep, txt_cc.Text, txt_bcc.Text, txt_subject.Text,
                                     txt_message.Text, filename);
                    //Add email info to database
                    int emailid = clog.SaveEmailInfo(ticketno, 3, filename, txt_to.Text, emailrep, txt_cc.Text,
                                                     txt_bcc.Text, txt_subject.Text, txt_message.Text);
                    //Adding To notes
                    clog.AddEmailNote(empid, txt_to.Text, ticketno, 3, emailid);
                    //If email addres not already exist of client then save this new one 
                    if (ViewState["vEmail"].ToString().Length == 0)
                    {
                        clog.UpdateClientEmail(ticketno, txt_to.Text);
                    }
                    //UpdateCaseHistory();
                    // Afaq 7961 07/26/2010 display alert message after sending email.
                    HttpContext.Current.Response.Write(
                        "<script language='javascript'> alert('Email has been sent.');   </script>");
                    HttpContext.Current.Response.Write("<script language='javascript'> self.close();   </script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }



        private void UpdateCaseHistory()
        {

            string[] keys = { "@ticketid", "@subject", "@notes", "@employeeid" };
            //Fahad 
            object[] values = { ticketno, "Contract/Receipt emailed", DBNull.Value, empid };
            ClsDb.InsertBySPArr("SP_ADD_NOTES", keys, values);
        }

        private bool IsAlreadyInBatch(int ticketid)
        {
            string[] keys = { "@TicketID" };
            object[] values = { ticketid };
            DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_BATCHLETTER_CHECK_RECEIPT", keys, values);
            if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                return true;
            else
                return false;
        }



        /// <summary>
        /// Fahad 5525 02/13/2009
        /// This Button display .rpt file in word format 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void IBtnExportWord_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CreateWordReport();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }


        /// <summary>
        /// Fahad 5525 02/13/2009
        /// This method show the .rpt file in word format 
        /// </summary>
        public void CreateWordReport()
        {
            if (Convert.ToInt32(ViewState["vTicketId"]) > 0 && empid > 0)
            {
                clsCrsytalComponent cr = new clsCrsytalComponent();
                clsLogger log = new clsLogger();
                string[] key = { "@ticketid", "@employeeid" };
                object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]), empid };
                cCase.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
                string filename = string.Empty;
                // Mohammad Ali 9949 12/29/2011 Check when case is criminal the execute CriminalReceipt.rpt else receipt.rpt
                if (cCase.IsCriminalCase())
                {
                    filename = Server.MapPath("") + "\\CriminalReceipt.rpt";
                    cr.CreateReportWord(filename, "USP_HTP_get_Criminal_receipt_info", key, value1, this.Session, this.Response);
                    log.AddNote(empid, "Criminal Contract/Receipt Printed with Changes", "Criminal Contract/Receipt Printed with Changes", Convert.ToInt32(ViewState["vTicketId"]));
                }
                else
                {
                    filename = Server.MapPath("") + "\\receipt.rpt";
                    cr.CreateReportWord(filename, "USP_HTS_get_receipt_info", key, value1, this.Session, this.Response);
                    log.AddNote(empid, "Contract/Receipt Printed with Changes", "Contract/Receipt Printed with Changes", Convert.ToInt32(ViewState["vTicketId"]));
                }

                //clsCrsytalComponent Cr = new clsCrsytalComponent();
                //string[] key = { "@ticketid", "@employeeid" };
                //object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]), empid };
                ////Ozair 5057 03/24/2009 Warnings Removed
                //string filename = Server.MapPath("") + "\\receipt.rpt";
                //Cr.CreateReportWord(filename, "USP_HTS_get_receipt_info", key, value1, this.Session, this.Response);
                //clsLogger log = new clsLogger();
                ////Fahad 5978 06/02/2009 Reciept chages to Contract/Receipt
                //log.AddNote(empid, "Contract/Receipt Printed with Changes", "Contract/Receipt Printed with Changes", Convert.ToInt32(ViewState["vTicketId"]));
            }
            else
            {
                throw new Exception("Value Can't be neagative nor Zero");

            }
        }
    }
}
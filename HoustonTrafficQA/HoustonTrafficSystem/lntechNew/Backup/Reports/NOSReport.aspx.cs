using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
//Nasir 5938 05/25/2009 for Nos report
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class NOSReport : System.Web.UI.Page
    {


        /// <summary>
        /// Not On System Report Report ( Developed By Fahad - 1/4/2008 )
        /// BISIC LOGIC :
        /// 1) The report will display all cases that have Not On System Flag activated 
        /// 2) popup will display followup date that defaults 2 business days from today's date
        /// 3) User will be able to sort by court location,hire date,followup date
        /// 4) Paging control is already registered at this report 
        /// 5) if followup date is not available then report should display "Not Available" in GridView
        /// </summary>

        #region Variables
        ValidationReports report = new ValidationReports();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsNOS NOS = new clsNOS();
        //Nasir 5938 05/25/2009 create object
        clsCase cCase = new clsCase();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        DataSet DS;
        DataView dvNOS;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindData();
            }
            //Nasir 5938 05/25/2009 for followup control and paging control
            UpdateFollowUpInfo.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo_PageMethod);
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            Pagingctrl.GridView = gv_records;
            ViewState["empid"] = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));

        }

        //Nasir 5938 05/25/2009 for paging control
        /// <summary>
        /// handle page index changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();

                gv_records.PageIndex = e.NewPageIndex;
                gv_records.DataSource = (DataView)Session["dvNOS"];
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        /// <summary>
        /// when command fires handle by this event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Nasir 5938 05/25/2009 Add followup control
            if (e.CommandName == "btnclick")
            {
                int RowID = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_causeno")).Value);
                string ticketno = (((Label)gv_records.Rows[RowID].FindControl("lbl_ticketno")).Text);
                string lastname = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_lname")).Value);
                string courtname = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_courtid")).Value);
                string CaseType = (((HiddenField)gv_records.Rows[RowID].FindControl("hf_casetypeid")).Value);
                string followupDate = (((Label)gv_records.Rows[RowID].FindControl("lbl_followup")).Text);

                followupDate = (followupDate.Trim() == "") ? "" : followupDate;

                cCase.TicketID = Convert.ToInt32((((HiddenField)gv_records.Rows[RowID].FindControl("hf_ticketid_pk")).Value));
                string comm = cCase.GetGeneralCommentsByTicketId();
                UpdateFollowUpInfo.Freezecalender = true;

                UpdateFollowUpInfo.Title = "NOS Follow-up";
                UpdateFollowUpInfo.followUpType = HTP.Components.FollowUpType.NOSFollowUpDate;

                string day = "0";
                //Not inside court and case type is trafiic
                if (!(courtname == "3001" || courtname == "3002" || courtname == "3003") && CaseType == "1")
                {
                    day = "22";
                }
                else
                {
                    day = "5";
                }

                DateTime NextDate = new DateTime();

                NextDate = clsGeneralMethods.GetBusinessDayDate(DateTime.Now, 2);

                UpdateFollowUpInfo.binddate(day.ToString(), NextDate, DateTime.Today, firstname, lastname, ticketno, ((HiddenField)gv_records.Rows[RowID].FindControl("hf_ticketid_pk")).Value, causeno, comm, mpeTrafficwaiting.ClientID, "NOS", "NOS", followupDate);
                mpeTrafficwaiting.Show();
                Pagingctrl.Visible = true;
            }
        }
        //Nasir 5938 05/25/2009 for paging control
        /// <summary>
        /// Add cmommand argument 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {//Nasir 5938 05/25/2009 SET COMMAND ANRGUMENT
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Sort the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);
        }



        #endregion

        # region Methods

        //Nasir 5938 05/25/2009 for paging control
        /// <summary>
        /// handle page index changed
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_records.PageIndex = Pagingctrl.PageIndex - 1;
                gv_records.DataSource = (DataView)Session["dvNOS"];
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Bind Data in grid
        /// </summary>
        public void BindData()
        {
            try
            {//Nasir 5938 05/25/2009 for follow report
                string[] keys = { "@validation" };
                object[] values = { 0 };
                report.getRecords("usp_hts_NOS_Report", keys, values);
                report.generateSerialNo();
                DS = report.records;
                gv_records.DataSource = DS.Tables[0];
                gv_records.DataBind();
                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    dvNOS = new DataView(DS.Tables[0]);
                    Session["dvNOS"] = dvNOS;
                    //Sabir Khan 4635 09/19/2008
                    //------------------------
                    this.lbl_message.Text = "";
                }
                else
                {
                    this.lbl_message.Text = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        /// <summary>
        /// Sort the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // Agha Usman 4426 08/04/2008 - Implement paging on Sort
        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                if (Session["dvNOS"] != null)
                {
                    dvNOS = (DataView)Session["dvNOS"];
                    dvNOS.Sort = StrExp + " " + StrAcsDec;
                    gv_records.DataSource = dvNOS;
                    gv_records.DataBind();
                    Pagingctrl.PageCount = gv_records.PageCount;
                    Pagingctrl.PageIndex = gv_records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Sort the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        //Nasir 5938 05/25/2009 for followup control
        /// <summary>
        /// Method to be called by UpdateFollowUpControl
        /// </summary>
        void UpdateFollowUpInfo_PageMethod()
        {
            BindData();
        }

        //Nasir 5938 05/25/2009 for paging
        /// <summary>
        /// Method when page size will be changed
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            BindData();

        }
        #endregion
    }


}

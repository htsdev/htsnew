using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Reports
{
	
	public partial class SplitReport : System.Web.UI.Page
	{
        
		clsSession ClsSession = new clsSession(); 
		clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase cCase = new clsCase();
        ValidationReports objValidation = new ValidationReports();
		DataSet ds_val;				
	
		private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        FillGrid(); // binding data in grid.
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_SplitReport;
                    ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();

            }

			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			//this.gv_SplitReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gv_SplitReport_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
        //Method to fill the grid
		private void FillGrid()
		{
			try
			{
                //string[] Keys = { "@ShowAll" };
                //object[] Values = { chkShowAllUserRecords.Checked };
                //ds_val = ClsDb.Get_DS_BySPArr("usp_splitreport_new", Keys, Values);
                ds_val = objValidation.GetSplitReport(chkShowAllUserRecords.Checked);
                generateSerialNo(ds_val.Tables[0]); // generating serial number for data grid.
                Pagingctrl.GridView = gv_SplitReport;
                gv_SplitReport.DataSource = ds_val.Tables[0];
                gv_SplitReport.DataBind();
                Pagingctrl.PageCount = gv_SplitReport.PageCount;
                Pagingctrl.PageIndex = gv_SplitReport.PageIndex;
                Pagingctrl.SetPageIndex();
				lbl_message.Text="";
				if(ds_val.Tables[0].Rows.Count<1)
				{
					lbl_message.Text="No Records Found";				
				}
               
			}
			catch(Exception ex)
			{
				lbl_message.Text=ex.Message;
                clsLogger.ErrorLog(ex);

			}
		}

        /// <summary>
        /// Fahad 5753 04/02/2009
        /// Generate the serial no on the basis of Ticket Id
        /// </summary>
        /// <param name="dtRecords"></param>
        public void generateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (dtRecords.Columns.Contains("sno") == false)
            {
                dtRecords.Columns.Add("sno");
            }

            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["TICKETID_PK"].ToString() != dtRecords.Rows[i]["TICKETID_PK"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

		
        /// <summary>
        /// Fahad 5753 04/08/2009
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_SplitReport.PageIndex = Pagingctrl.PageIndex - 1;  // set page index to grid.
                FillGrid();
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }

        }
        /// <summary>
        /// Fahad 5753 04/08/2009
        /// Maintained the data in update followUp date Control
        /// </summary>
        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();
        }

        /// <summary>
        /// Fahad 5722 04/02/2009
        /// Maintained the page size
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_SplitReport.PageIndex = 0;
                gv_SplitReport.PageSize = pageSize;
                gv_SplitReport.AllowPaging = true;
            }
            else
            {
                gv_SplitReport.AllowPaging = false;
            }
            FillGrid();
        }
		
       
		private void gv_SplitReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
            
            
			try
			{
                if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
                    return;

                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv["TicketID_PK"] == DBNull.Value)
                {
                    ((ImageButton)(e.Item.FindControl("imgRemove"))).Visible = false;
                    return;
                }
				((HyperLink) e.Item.FindControl("HLTicketno")).NavigateUrl ="../ClientInfo/ViolationFeeold.aspx?search=0&casenumber="+ (int) drv["TicketID_PK"];				
                string ticketid = drv["TicketID_PK"].ToString();
                if ( ticketid.Length == 0 || ticketid == "0"  )
                    ((ImageButton)(e.Item.FindControl("imgRemove"))).Visible = false;
			}
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
		}

        protected void gv_SplitReport_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                string sAlert = "<script> alert('All violations of the selected client will be removed from Split Report. You can restore its status from general info page by resetting the NO SPLIT flag.');</script>";
                HttpContext.Current.Response.Write(sAlert);
                int TicketViolationId = Convert.ToInt32(((Label)(e.Item.FindControl("lbl_violationid"))).Text);
                int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                objValidation.UpdateNosplitFlag(TicketViolationId, empid);
                gv_SplitReport.PageIndex = 0;
                FillGrid();	
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        //protected void MarkCaseAsNoSplit(int ticketViolationId)
        //{
            
        //}

        protected void chkShowAllUserRecords_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
            
        }

        protected void gv_SplitReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_SplitReport.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();

            }

        }

        protected void gv_SplitReport_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "btnclick")
                {

                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string firstname = ((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_fname")).Value;
                    string lastname = ((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_lname")).Value;
                    string causeno = ((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_causeno")).Value;
                    string ticketno = ((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_ticketno")).Value;
                    string ticketid = ((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_TicketID")).Value;
                    ViewState["TicketId"] = ticketid;
                    string followupDate = (((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_FollowUpDate")).Value);
                    string nextfollowupDate = (((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_NextFollowUpdate")).Value);
                    string court = "Split";
                    cCase.TicketID = Convert.ToInt32(ticketid);
                    string commments = cCase.GetGeneralCommentsByTicketId();
                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Split Follow Up Date";
                    UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.SplitCaseFollowUpDate;
                    DateTime nextfollow = (followupDate == string.Empty) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(followupDate);
                    UpdateFollowUpInfo2.binddate(Convert.ToDateTime(nextfollowupDate), nextfollow, firstname, lastname, ticketno, causeno, commments, mpeTrafficwaiting.ClientID, court, Convert.ToInt32(ticketid));
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
                else if (e.CommandName == "btnRemove")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string ticketid = ((HiddenField)gv_SplitReport.Rows[RowID].FindControl("hf_TicketID")).Value;
                    int empid = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    objValidation.UpdateNosplitFlag(Convert.ToInt32(ticketid),empid);
                    FillGrid();
 
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
        }

        protected void gv_SplitReport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                    ((ImageButton)e.Row.FindControl("imgRemove")).CommandArgument = e.Row.RowIndex.ToString();
                    pnlFollowup.Style["display"] = "none";
                    Label followup = (Label)e.Row.FindControl("lbl_followup");

                }
            }

            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                lbl_message.Text = ex.ToString();
            }
        }

       
	}
}

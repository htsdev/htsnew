﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;

namespace lntechNew.Reports
{
    public partial class LmsAddressCheckView : System.Web.UI.Page
    {
        readonly clsSession _clsSession = new clsSession();
        readonly ClsNonClientsManager _clsNonclients = new ClsNonClientsManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    ViewState["vEmpID"] = _clsSession.GetCookie("sEmpID", this.Request);
                    if (Page.IsPostBack != true)
                    {
                        cal_EffectiveFrom.SelectedDate = DateTime.Today.Date; //setting the date 
                        cal_EffectiveTo.SelectedDate = DateTime.Today.Date;
                        ddl_letterType.DataSource = _clsNonclients.FillLetterTypes();
                        ddl_letterType.DataTextField = "LetterName";
                        ddl_letterType.DataValueField = "ID";
                        ddl_letterType.DataBind();
                        if (Session["lmsDataView"] != null)
                            Session["lmsDataView"] = null;
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    PagingControl.grdType = GridType.GridView;
                    Pagingctrl.GridView = dg_lmsAddresses;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// This methoed is used to Fill the Grid View
        /// </summary>
        private void FillGrid()
        {
            try
            {
                DataTable dt;
                if (Session["lmsDataView"] == null)
                {
                    dt = _clsNonclients.GetBadMarkedRecords(ddl_letterType.SelectedValue,
                        cal_EffectiveFrom.SelectedDate, cal_EffectiveTo.SelectedDate, false, true);
                    Session["lmsDataView"] = dt.DefaultView;
                }
                else
                {
                    DataView dw = Session["lmsDataView"] as DataView;
                    dt = dw.ToTable();
                }
                if (dt.Rows.Count > 0)
                {
                    dg_lmsAddresses.DataSource = dt;
                    dg_lmsAddresses.DataBind();
                    dg_lmsAddresses.Visible = true;
                    Pagingctrl.Visible = true;
                    Pagingctrl.PageCount = dg_lmsAddresses.PageCount;
                    Pagingctrl.PageIndex = dg_lmsAddresses.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    dg_lmsAddresses.Visible = false;
                    Pagingctrl.Visible = false;
                    lblMessage.Text = @"No Records Found";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle Pagging Control pageIndexChange Event.
        /// </summary>
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                dg_lmsAddresses.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_PageIndexChanged(object source, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_lmsAddresses.PageIndex = e.NewPageIndex;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Method to handle DataGrid pageIndexChange Event.
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                dg_lmsAddresses.PageIndex = 0;
                dg_lmsAddresses.PageSize = pageSize;
                dg_lmsAddresses.AllowPaging = true;

            }
            else
            {
                dg_lmsAddresses.AllowPaging = false;
            }

            FillGrid();
        }

        /// <summary>
        /// Submit Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            if (Session["lmsDataView"] != null)
                Session["lmsDataView"] = null;
            FillGrid();
        }

        /// <summary>
        /// Grid View Page Index changing methoed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    dg_lmsAddresses.PageIndex = e.NewPageIndex;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Row Data Bound Event of Grid View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_lmsAddresses_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "border:1px solid DarkGray");
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "border:1px solid DarkGray");
                    }

                    Label lbllastName = ((Label)e.Row.FindControl("lbl_lastName"));
                    if (lbllastName.Text.Length > 10)
                    {

                        lbllastName.ToolTip = lbllastName.Text;
                        lbllastName.Text = lbllastName.Text.Substring(0, 10) + @"...";
                    }

                    Label lblFirstName = ((Label)e.Row.FindControl("lbl_firstName"));
                    if (lblFirstName.Text.Length > 10)
                    {
                        lblFirstName.ToolTip = lblFirstName.Text;
                        lblFirstName.Text = lblFirstName.Text.Substring(0, 10) + @"...";
                    }

                    Label lblexistingAddress = ((Label)e.Row.FindControl("lbl_existingAddress"));
                    if (lblexistingAddress.Text.Length > 45)
                    {
                        lblexistingAddress.ToolTip = lblexistingAddress.Text;
                        lblexistingAddress.Text = lblexistingAddress.Text.Substring(0, 45) + @"...";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
    }
}

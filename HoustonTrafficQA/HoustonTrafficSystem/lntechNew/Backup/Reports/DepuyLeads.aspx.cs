﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    ///<summary>
    /// This Form used to show all DePuy Leads which we are getting from Public WebSite.
    ///</summary>
    public partial class DepuyLeads : Page
    {
        #region Variable
        readonly ValidationReports _reports = new ValidationReports();
        readonly clsSession _clsSession = new clsSession();
        DataSet _ds;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_clsSession.IsValidSession(Request, Response, Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    {
                        if (!IsPostBack)
                        {
                            chkShowAll.Checked = false;
                            cal_FromDateFilter.SelectedDate = DateTime.Now;

                            cal_ToDateFilter.SelectedDate = DateTime.Now;
                            FillGrid();
                        }
                    }

                    Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                    Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                    Pagingctrl.GridView = gv_Records;
                    const string javascript = "EnabledPaymentsFilterControls('{0}','{1}','{2}');";
                    chkShowAll.Attributes.Add("onclick", string.Format(javascript, cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkShowAll.ClientID, 1));


                    if (chkShowAll.Checked)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "javascript", string.Format("<script language=\"javascript\" type=\"text/javascript\"> EnabledPaymentsFilterControls('{0}','{1}','{2}');</script>", cal_FromDateFilter.ClientID, cal_ToDateFilter.ClientID, chkShowAll.ClientID));
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        private void FillGrid()
        {
            string[] keys = { "@ShowAllRecords", "@fromDate", "@toDate" };
            object[] values = { chkShowAll.Checked, cal_FromDateFilter.SelectedDate, cal_ToDateFilter.SelectedDate };
            _reports.getRecords("USP_HTP_GetDepuyLeads", keys, values);
            _ds = _reports.records;
            if (_ds.Tables[0].Rows.Count > 0)
            {
                Pagingctrl.GridView = gv_Records;
                gv_Records.DataSource = _ds.Tables[0];
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();

            }
            else
            {
                gv_Records.DataSource = null;
                gv_Records.DataBind();

            }
        }
        /// <summary>
        /// This method is used when Paging Control changes the page Index.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        /// <summary>
        /// This method is used when Paging Control changes the page Size.
        /// </summary>
        /// <param name="pageSize">Size of the Page</param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();

        }
        /// <summary>
        /// Gridview PageIndexChanging event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Gridview RowDataBound event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.ToString().ToLower() == "datarow")
            {

                //Muhammad Nadir Siddiqui 9158 04/15/2011

                string email = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "EmailAddress"));
                Label lblEmail = (Label)e.Row.FindControl("lbl_Email");
                //Muhammad Nadir Siddiqui 9027 03/16/2011
                string practiceArea = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PracticeArea"));
                Label lblPracticeArea = (Label)e.Row.FindControl("lbl_PracticeArea");
                if (lblEmail != null)
                {
                    if (email.Length > 18)
                    {
                        lblEmail.Text = email.Substring(0, 16) + "..";
                        lblEmail.ToolTip = email;
                    }
                    else
                    {
                        lblEmail.Text = email;
                    }

                }
                if (lblPracticeArea != null)
                {
                    if (practiceArea.Length > 18)
                    {
                        lblPracticeArea.Text = practiceArea.Substring(0, 16) + "..";
                        lblPracticeArea.ToolTip = practiceArea;
                    }
                    else
                    {
                        lblPracticeArea.Text = practiceArea;
                    }

                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using lntechNew.Components.ClientInfo;

//Waqas 5864 07/02/2009 ALR Letters
namespace HTP.Reports
{
    public partial class RptALRLetters : System.Web.UI.Page
    {
        #region Variables 
        clsSession uSession = new clsSession();
        clsLogger clog = new clsLogger();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (uSession.GetCookie("sEmpID", this.Request) != "")
                {
                    int ticketno = Convert.ToInt32(Request.QueryString["casenumber"]);
                    ViewState["vTicketId"] = ticketno;
                    Session["sTicketId"] = ticketno;

                    if (uSession.GetCookie("sEmpID", this.Request) != "")
                    {
                        int empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));
                        ViewState["vEmpId"] = empid;
                    }

                    int lettertype = Convert.ToInt32(Request.QueryString["lettertype"]);
                    ViewState["vLetterType"] = lettertype;


                    CreateALRReport();
                }
                else
                {
                    Response.Redirect("../frmlogin.aspx");
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method is used to generate and print ALR letters
        /// </summary>
        public void CreateALRReport() 
        {
            clsCrsytalComponent Cr = new clsCrsytalComponent();

            //string[] key = { "@ticketid", "@empid" };
            //object[] value1 = { Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ViewState["vEmpid"]) };

            string[] key = { "@ticketid"};
            object[] value1 = { Convert.ToInt32(ViewState["vTicketId"])};

            int lettertype = Convert.ToInt32(ViewState["vLetterType"]);
            int empid = Convert.ToInt32(uSession.GetCookie("sEmpID", this.Request));

            string filename = "";

            if (lettertype == 26) //ALR Check Request
            {
                filename = Server.MapPath("") + "\\ALR_Check_Request.rpt";

                string[] keys = { "@ticketid", "@EmpID" };
                object[] values = { Convert.ToInt32(ViewState["vTicketId"]), empid };

                Cr.CreateReport(filename, "USP_HTP_GET_ALR_Check_Request_Letter", keys, values, "false", lettertype, empid, this.Session, this.Response);
            }

            if (lettertype == 27) //ALR Continuance
            {
                filename = Server.MapPath("") + "\\ALR_Continuance.rpt";
                Cr.CreateReport(filename, "USP_HTP_Get_ALR_Mandatory_Continuance_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }

            if (lettertype == 28) //ALR Hearing Request
            {
                filename = Server.MapPath("") + "\\ALR_Hearing_Request.rpt";
                Cr.CreateReport(filename, "USP_HTP_Get_ALR_Hearing_Request_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }

            if (lettertype == 29) //ALR Mandatory Continuance
            {
                filename = Server.MapPath("") + "\\ALR_Mandatory_Continuance.rpt";
                Cr.CreateReport(filename, "USP_HTP_Get_ALR_Mandatory_Continuance_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }


            if (lettertype == 30) //ALR Motion To Request
            {
                filename = Server.MapPath("") + "\\ALR_Motion_To_Request.rpt";
                Cr.CreateReport(filename, "USP_HTP_Get_ALR_Motion_To_Request_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }

            if (lettertype == 31) //ALR Subpeona
            {
                filename = Server.MapPath("") + "\\ALR_Subpeona.rpt";
                Cr.CreateReport(filename, "USP_HTP_Get_ALR_Subpeona_Letter", key, value1, "false", lettertype, empid, this.Session, this.Response);
            }


        }
        #endregion
    }
}

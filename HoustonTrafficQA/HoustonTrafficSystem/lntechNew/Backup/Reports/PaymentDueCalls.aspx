﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentDueCalls.aspx.cs"
    Inherits="HTP.Reports.PaymentDueCalls" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Payment Due Calls Alert</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function showhide(ele,caller) 
        {
            var srcElement = document.getElementById(ele);
            if(srcElement != null) {
            if(srcElement.style.display == "block") {
               caller.innerHTML = "Show Settings";
               srcElement.style.display= 'none';
            }
            else {
               caller.innerHTML = "Hide Settings";
               srcElement.style.display='block';
            }
            return false;
          }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="780">
                    <tbody>
                        <tr>
                            <td colspan="4" style="height: 14px">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr class="clsLeftPaddingTable">
                            <td align="left" style="height: 25px">
                                <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of Business Days after case reset:"
                                    runat="server" Attribute_Key="Days" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" align="right" valign="middle" style="height: 34px">
                                <table>
                                    <tr>
                                        <td colspan="2" style="text-align: right;" valign="middle">
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                    <ProgressTemplate>
                                        <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                            CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                    AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                    PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="True" 
                                    onsorting="gv_records_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" runat="server" Text='<%# Eval("SNo") %>'
                                                    NavigateUrl='<%# "../clientinfo/ViolationFeeold.aspx?casenumber=" + Eval("TicketID_PK")+"&search=0" %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="ClientName">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ClientName", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt</u>" SortExpression="crt">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("crt", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt Type</u>" SortExpression="CrtType">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("CrtType", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt Date</u>" SortExpression="CourtDate">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" 
                                                    Text='<%# Bind("CourtDate", "{0:MM/dd/yy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Total Fee</u>" SortExpression="TotalFeeCharged">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" 
                                                    Text='<%# Bind("TotalFeeCharged", "{0:C0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Amt Due</u>" SortExpression="Owes">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("Owes", "{0:C0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Payment Due</u>" SortExpression="PaymentDate">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" 
                                                    Text='<%# Bind("PaymentDate", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Follow up</u>" SortExpression="PaymentFollowUpdate">                                            
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" 
                                                    Text='<%# Bind("PaymentFollowUpdate", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Remaining Days</u>" SortExpression="Remainingdays">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Text='<%# Bind("Remainingdays") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" 
                                                HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

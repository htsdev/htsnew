using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
	/// <summary>
	/// Summary description for FrmExportToExcel.
	/// </summary>
	public partial class FrmExportToExcel : System.Web.UI.Page
	{
		clsSession ClsSession=new clsSession();
		clsLogger bugTracker = new clsLogger();

		protected System.Web.UI.WebControls.DataGrid DgResults;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{

				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					if (Session["MoveDataSet"] != null)
					{
						DataSet ds = (DataSet) Session["MoveDataSet"];
						DgResults.DataSource = ds;
						DgResults.DataBind();
						cmpDataGridToExcel.DataGridToExcel(DgResults, Response);
				
					}
					else
					{
						Response.Redirect("../frmlogin.aspx",false);
					}
				}
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

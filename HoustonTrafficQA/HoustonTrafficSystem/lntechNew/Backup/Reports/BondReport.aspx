<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BondReport.aspx.cs" Inherits="HTP.Reports.BondReport"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="updatefollowupinfo"
    TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bond Alert</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11" 
                                    align="center">
                                    <asp:Label ID="lbl_message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="text-align: right">
                                                <aspnew:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%" class="clsLeftPaddingTable">
                                        <tr>
                                            <td width="780">
                                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                    <ProgressTemplate>
                                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                            CssClass="clsLabel"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                                                            CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand" OnRowDataBound="gv_records_RowDataBound"
                                                            AllowSorting="True" OnSorting="gv_records_Sorting">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S#">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_TicketID" runat="server" 
                                                                            Value="<%# Bind('TicketID_PK') %>" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Last Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="clsLabel" Text='<%# Bind("lastname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="clsLabel" Text='<%# Bind("firstname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cause No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_causenum" runat="server" CssClass="clsLabel" Text='<%# Bind("CauseNumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_ticketnumber" runat="server" CssClass="clsLabel" Text='<%# Bind("TicketNumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Court Loc</u>" SortExpression="CRT_Location">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_crtloc" runat="server" CssClass="clsLabel" Text='<%# Bind("CRT_Location") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lb_Status" runat="server" CssClass="clsLabel" Text='<%# Bind("Status") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="FollowUpDate">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="clsLabel" Text='<%# Bind("followupdate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <input type="hidden" id="hfTicketId" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                                                                        <input type="hidden" id="hfComments" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.GeneralComments") %>' />
                                                                        <input type="hidden" id="hf_courtid" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.Courtid") %>' />
                                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif"
                                                                            CommandArgument='<%# Eval("TicketID_PK") %>' />
                                                                        <asp:HiddenField ID="hf_CourtCode" runat="server" Value="<%# Bind('courtid') %>" />
                                                                        <asp:HiddenField ID="hf_GeneralComments" runat="server" Value="<%# Bind('generalcomments') %>" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../images/separator_repeat.gif" height="11" width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:Footer ID="Footer1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlFollowup" runat="server">
                                                            <uc3:updatefollowupinfo ID="UpdateFollowUpInfo2" runat="server" Title="Bond Follow Up Date" />
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                    PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
            </ContentTemplate>
        </aspnew:UpdatePanel>
        <br />
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ALRRequest.aspx.cs" Inherits="lntechNew.Reports.ALRRequest" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ALR Request Confirmation Alert</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                             <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                        <table style="width: 100%">
                            <tr>
                                <td>
                                </td>
                                <td style="text-align: right">
                                    <uc3:PagingControl ID="Pagingctrl" runat="server"  />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                            <tr>
                                <td align="center" >
                                    <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td colspan="5" width="780">
                                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="30" CssClass="clsleftpaddingtable">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ticket Number">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ticketno" runat="server" CssClass="label" Text='<%# Bind("ticketnumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last Name">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_lastname" runat="server" CssClass="label" Text='<%# Bind("lname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="First Name">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_firstname" runat="server" CssClass="label" Text='<%# Bind("fname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Crt.Date/Time" >
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_courtdate" runat="server" CssClass="label" Text='<%# Bind("courtdatemain") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Crt.No" >
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_courtno" runat="server" CssClass="label" Text='<%# Bind("courtnumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_location" runat="server" CssClass="label" Text='<%# Bind("status") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Crt.Name">
                                                            <HeaderStyle CssClass="clssubhead" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_courtname" runat="server" CssClass="label" Text='<%# Bind("shortname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                        
                   
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:Footer ID="Footer1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    
    </div>
    </form>
</body>
</html>
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class HMCAWDLQCalls : System.Web.UI.Page
    {
        ValidationReports vreports = new ValidationReports();
        DataTable dt = new DataTable();
        #region events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    fillgrid();

                }
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_records;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gv_records.PageIndex = e.NewPageIndex;
                    fillgrid();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }



        #endregion

        #region Methods
        public void fillgrid()
        {            
            dt = vreports.GetHMCAWDLQCalls();
            if (dt.Rows.Count == 0)
            {
                gv_records.Visible = false;
                lblMessage.Text = "No reocrds Found";

            }
            else
            {

                if (dt.Columns.Contains("SNo") == false)
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                        dt.Rows[0]["SNo"] = 1;
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i - 1]["TicketID_PK"].ToString() != dt.Rows[i]["TicketID_PK"].ToString())
                            {
                                dt.Rows[i]["SNo"] = ++sno;
                            }
                        }
                    }
                }
                
                gv_records.Visible = true;
                gv_records.DataSource = dt;
                gv_records.DataBind();

                Pagingctrl.PageCount = gv_records.PageCount;
                Pagingctrl.PageIndex = gv_records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_records.PageIndex = Pagingctrl.PageIndex - 1;
            fillgrid();
        }

        /// <summary>
        /// Use for set page size
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gv_records.PageIndex = 0;
                    gv_records.PageSize = pageSize;
                    gv_records.AllowPaging = true;
                }
                else
                {
                    gv_records.AllowPaging = false;
                }
                fillgrid();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion
    }
}

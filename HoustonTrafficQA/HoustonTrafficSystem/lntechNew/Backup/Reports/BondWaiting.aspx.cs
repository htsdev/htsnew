using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace HTP.Reports
{
    // Noufil 4980 10/30/2008 code modified and followupdate control added
    public partial class BondWaiting : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        DataSet DS;
        ValidationReports reports = new ValidationReports();

       //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days

        protected override void OnInit(EventArgs e)
        {
            ShowSetting.OnErr += new HTP.WebControls.ShowSetting.ErrHandler(ShowSetting_OnErr);
            ShowSetting.dbBind += new HTP.WebControls.ShowSetting.Databind(ShowSetting_dbBind);
            base.OnInit(e);
        }

       //5423 end 

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        FillGrid();
                    }
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod);
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);

                    //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;

                }
                Pagingctrl.Visible = true;

                //5423 end
                
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }

        private void FillGrid()
        {
            // Noufil 5131 11/13/2008 SHow only past date followupdate
            //Yasir 5423 02/11/2009 cases having follow up date in next 6 business days
            string[] keys = { "@Validation","@ShowAll" };
            // tahir 5165 11/18/2008 to show all bond waiting cases...
            //object[] Values = { 1 };
            object[] values = { 0,ShowAll.Checked};
            reports.HasParameter = true;
            reports.keys = keys;
            reports.values = values;
            reports.getReportData(gv_Records, lblMessage, ValidationReport.BondWaitingViolations);
            //5423 end
            Pagingctrl.PageCount = gv_Records.PageCount;
            Pagingctrl.PageIndex = gv_Records.PageIndex;
            Pagingctrl.SetPageIndex();
        }

        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }

                    Label followup = (Label)e.Row.FindControl("lbl_followup");
                    followup.Text = (followup.Text.Trim() == "01/01/1900") ? "" : followup.Text;
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception)
            {

            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnclick")
            {
                int RowID = Convert.ToInt32(e.CommandArgument);
                string firstname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_fname")).Value);
                string causeno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_causeno")).Value);
                string ticketno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketnumber")).Value);
                string lastname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_lname")).Value);
                string courtname = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_loc")).Value);
                string followupDate = (((Label)gv_Records.Rows[RowID].FindControl("lbl_followup")).Text);
                followupDate = (followupDate.Trim() == "") ? "1/1/1900" : followupDate;
                string bonddate = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_bonddate")).Value);
                bonddate = bonddate.Substring(0, bonddate.IndexOf("@")).Trim();
                cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketno")).Value));
                string comm = cCase.GetGeneralCommentsByTicketId();
                UpdateFollowUpInfo2.Freezecalender = true;
                UpdateFollowUpInfo2.binddate(DateTime.Today, Convert.ToDateTime(followupDate), firstname, lastname, ticketno, causeno, comm, mpeBondwaiting.ClientID, "Waiting", Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketno")).Value)));
                // Abid Ali 5425 1/20/2009 update FollowUpType referrence
                UpdateFollowUpInfo2.followUpType = HTP.Components.FollowUpType.BondWaitingFollowUpDate;
                mpeBondwaiting.Show();
                Pagingctrl.Visible = true;
            }

        }
        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();
        }

        void Pagingctrl_PageIndexChanged()
        {
            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();
        }

        /// <summary>
        /// Show Setting Control Databind
        /// </summary>
        void ShowSetting_dbBind()
        {
            FillGrid();

        }
        /// <summary>
        /// ShowSetting display message on error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ErrorMsg"></param>
        void ShowSetting_OnErr(object sender, string ErrorMsg)
        {
            lblMessage.Text = ErrorMsg;

        }

        /// <summary>
        /// change event of ShowAll checkBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowAll_CheckedChanged(object sender, EventArgs e)
        {
            FillGrid();
        }

        /// <summary>
        /// Handling PaggingControl page Size Change event
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;

            }
            else
            {
                gv_Records.AllowPaging = false;
            }

            FillGrid();
         

        }


  
    }
}

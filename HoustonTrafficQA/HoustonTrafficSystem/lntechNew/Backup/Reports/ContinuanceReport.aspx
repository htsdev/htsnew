<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContinuanceReport.aspx.cs" Inherits="lntechNew.Reports.ContinuanceReport" %>

<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Continuance Report</title>
         <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
      <link href="../Styles.css" rel="stylesheet" type="text/css" />
   <script language=javascript>
    
    
    function PopUp(ticketid)
		{
		    var path="PopupContinuanceReport.aspx?ticketid="+ticketid;
		    window.open(path,'',"fullscreen=no,scrollbars=yes,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes,width=590,height=270");
		    return false;
		}
   </script>
   </head>
   
   
<body>
    <form id="form1" runat="server">
               <table cellpadding="0" cellspacing="0" border="0" align="center" >
                <tr>
                    <td  > 
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                      
                    </td>
                </tr>
                <tr>
                    <td style="height: 15px" background="../images/separator_repeat.gif">
                    </td>
                </tr>
                <tr class="clsLeftPaddingTable">
                <td>
                <table style="width: 780px" cellpadding="0" cellspacing="0">
                <tr>
                
                    <td style="height: 27px;" class="clssubhead">
                        &nbsp; Start Date</td>
                    <td style="height: 27px;">
                        &nbsp;<ew:CalendarPopup ID="datefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                            ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                            PadSingleDigits="True" SelectedDate="2003-01-11" ShowClearDate="True" ShowGoToToday="True"
                            ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                        </ew:CalendarPopup>
                    </td>
                    <td style="height: 27px;" class="clssubhead">
                        End Date</td>
                    <td style="height: 27px;">
                        <ew:CalendarPopup ID="dateto" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                            ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                            PadSingleDigits="True" SelectedDate="2007-12-11" ShowClearDate="True" ShowGoToToday="True"
                            ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                        </ew:CalendarPopup>
                    </td>
                    <td style="width: 2px; height: 27px">
                        <asp:DropDownList ID="ddl_courts" runat="server" CssClass="clsinputcombo">
                        </asp:DropDownList>
                    </td>                    
                    <td style="height: 26px;" align="right" colspan="2">
                        <asp:CheckBox ID="chk_viewall" runat="server" Text="View all future dates" CssClass="label" Checked="True" />&nbsp;</td>
                    <td align="right" colspan="1" style="height: 26px">
                        <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" Text="Search" OnClick="btn_search_Click" />
                    </td>
                </tr>
                <tr class="clsLeftPaddingTable">
                    <td style="height: 30px;" class="clssubhead">Status
                    </td>
                    <td colspan="7" style="height: 30px;" valign="middle">
                        <asp:CheckBoxList ID="cbl_status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="label">
                        </asp:CheckBoxList>
                    </td>                                     
                </tr>
                       
             </table>
                </td>
                </tr>
                <tr>
                    <td style="height: 9px" background="../images/separator_repeat.gif">
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td valign="top" align="center">
                        <asp:GridView ID="gv_results" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False" Width="780px" AllowSorting="True" OnSorting="gv_results_Sorting" OnRowDataBound="gv_results_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="S.No">
                                    <HeaderStyle CssClass="clssubhead" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "../clientinfo/violationfeeold.aspx?search=0&casenumber="+DataBinder.Eval(Container,"DataItem.Ticketid_pk" ) %>' Text='<%# DataBinder.Eval(Container,"DataItem.SNO" ) %>'></asp:HyperLink>&nbsp;
                                        <asp:Label ID="lbl_ticketid" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.Ticketid_pk" ) %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.FirstName" ) %>'></asp:Label>&nbsp;
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.LastName") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                <asp:TemplateField HeaderText="Continuance Status" SortExpression="ContinuanceStatus">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_continuancestatus" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.ContinuanceStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                 <asp:TemplateField HeaderText="Continuance Date" SortExpression="ContinuanceDate">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_continuancedate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.ContinuanceDate","{0:d}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Court Name" SortExpression="shortname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_shortname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.shortname") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                  <asp:TemplateField HeaderText="Crt." SortExpression="courtnumber">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_courtnumber" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.courtnumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    
                                      <asp:TemplateField HeaderText="Rep" SortExpression="Rep">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_rep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.Rep") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                   <asp:TemplateField HeaderText="View">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif" Width="19" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
               
                <tr>
                    <td  valign="top" background="../images/separator_repeat.gif"  style="height: 9px" >
                    </td>
                </tr>
                <tr>
                    <td  valign="top">
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
              
            </table>
    </form>
</body>
</html>

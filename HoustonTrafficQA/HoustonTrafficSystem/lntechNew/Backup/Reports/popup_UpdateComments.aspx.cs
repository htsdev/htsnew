using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class popup_UpdateComments : System.Web.UI.Page
    {
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    DataTable dtRecord;
                    string[] keys = { "@RecordID" };
                    object[] values = { Request.QueryString["RecordID"].ToString() };
                    dtRecord = clsDb.Get_DT_BySPArr("USP_HTS_POST_TRIAL_GET_COMMENTS", keys, values);
                    txt_Comments.Text = dtRecord.Rows[0]["Comments"].ToString();
                    lbl_Name.Text = dtRecord.Rows[0]["Fullname"].ToString();
                }
                catch (Exception ex)
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    lbl_Message.Text = ex.Message;
                }
            }
        }
    }
}

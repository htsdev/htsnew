using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;


namespace lntechNew.Reports
{
    public partial class PaymentDiscrepancyReport : System.Web.UI.Page
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        DataView DV;

        string StrExp = String.Empty;
        string StrAcsDec = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (cSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        lblMessage.Text = "";
                        cal_fromDate.SelectedDate = DateTime.Today;
                        cal_todate.SelectedDate = DateTime.Today;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void BindGrid()
        {
            try
            {
                lblMessage.Text = "";
                int showall = 0;
                if (chkAll.Checked == true)
                {
                    showall = 1;
                }
                string[] key ={ "@psdate", "@pedate", "@showall" };
                object[] value ={ cal_todate.SelectedDate.ToShortDateString(), cal_fromDate.SelectedDate.ToShortDateString(), showall };
                DataSet DS = clsdb.Get_DS_BySPArr("usp_hts_PaymentDiscrepancyReport",key,value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DV = new DataView(DS.Tables[0]);
                    Session["DS"] = DV;
                    gv.DataSource = DS;
                    gv.DataBind();
                    SerialNumber();
                }
                else 
                {
                    gv.DataSource = DS;
                    gv.DataBind();
                    lblMessage.Text = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region SerialNumber
        private void SerialNumber()
        {
            
            try
            {
                long sNo = (gv.PageIndex) * (gv.PageSize);
                foreach (GridViewRow row in gv.Rows)
                {
                    sNo += 1;
                    ((HyperLink)(row.FindControl("HPSNo"))).Text = sNo.ToString();
                   
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string ticketid=((Label)e.Row.FindControl("lblticketid")).Text.ToString();
                    ((HyperLink)e.Row.FindControl("HPSNo")).NavigateUrl = "../clientinfo/newpaymentinfo.aspx?search=0&caseNumber=" + ticketid;


                    ((HyperLink)e.Row.FindControl("HPTicketNo")).NavigateUrl = "../clientinfo/violationfeeold.aspx?search=0&caseNumber=" + ticketid;

                    string contact1 = ((Label)e.Row.FindControl("lblContact1")).Text.ToString();
                    string contact2 = ((Label)e.Row.FindControl("lblcontact2")).Text.ToString();
                    string contact3 = ((Label)e.Row.FindControl("lblcontact3")).Text.ToString();

                    if (contact1.StartsWith("("))
                    {
                        ((Label)e.Row.FindControl("lblContact1")).Visible = false;
                    }
                    if (contact2.StartsWith("("))
                    {
                        ((Label)e.Row.FindControl("lblcontact2")).Visible = false;
                    }
                    if (contact3.StartsWith("("))
                    {
                        ((Label)e.Row.FindControl("lblcontact3")).Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv.PageIndex = e.NewPageIndex;
                DV = (DataView)Session["DS"];
                gv.DataSource = DV;
                gv.DataBind();
                SerialNumber();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGrid(e.SortExpression);
        }

        private void SortGrid(string SortExp)
        {
            try
            {
                SetAcsDesc(SortExp);
                DV = (DataView)Session["DS"];
                DV.Sort = StrExp + " " + StrAcsDec;
                gv.DataSource = DV;
                gv.DataBind();
                SerialNumber();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        private void SetAcsDesc(string Val)
        {
            try
            {
                StrExp = Session["StrExp"].ToString();
                StrAcsDec = Session["StrAcsDec"].ToString();
            }
            catch
            {

            }

            if (StrExp == Val)
            {
                if (StrAcsDec == "ASC")
                {
                    StrAcsDec = "DESC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
                else
                {
                    StrAcsDec = "ASC";
                    Session["StrAcsDec"] = StrAcsDec;
                }
            }
            else
            {
                StrExp = Val;
                StrAcsDec = "ASC";
                Session["StrExp"] = StrExp;
                Session["StrAcsDec"] = StrAcsDec;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

       
       

        

        

    }
}

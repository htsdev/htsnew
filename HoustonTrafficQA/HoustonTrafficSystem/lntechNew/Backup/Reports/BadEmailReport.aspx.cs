using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Components;
using lntechNew.WebControls;
using System.Text;

//SAEED 7844 06/03/2010 BussinessLogic object need reference of this namespce.

namespace HTP.Reports
{
    public partial class BadEmailReport : System.Web.UI.Page
    {
        // Fahad Muhammad Qureshi 6429 09/01/2009  grouping variables
        #region Variables
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase cCase = new clsCase();
        clsLogger bugTracker = new clsLogger();
        DataSet DS;
        ValidationReports reports = new ValidationReports();
        //SAEED 7844 06/03/2010 varaibles need for followup date functionality. 
        BussinessLogic Blogic = new BussinessLogic();
        int Report_ID = 0;
        int day = 0;
        #endregion

        // Fahad Muhammad Qureshi 6429 09/01/2009 grouping Report Properties
        #region Properties


        // Fahad Muhammad Qureshi 6429 09/01/2009 Show All property
        private bool ShowAllRecords
        {
            get
            {
                if (ViewState["ShowAllRecords"] == null)
                    ViewState["ShowAllRecords"] = false;

                return Convert.ToBoolean(ViewState["ShowAllRecords"]);
            }
            set
            {
                ViewState["ShowAllRecords"] = value;
            }
        }
        //Muhammad Muneer 8247 10/20/2010 add property to handle sorting
        private string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }
        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }
        //Muhammad Muneer 8247 10/20/2010 declare property for sorting purpose
        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                { ViewState["sortOrder"] = "asc"; }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }
                return ViewState["sortOrder"].ToString();
            }
            set { ViewState["sortOrder"] = value; }
        }




        //SAEED 7844 06/03/2010 not using from/to date.
        // Fahad Muhammad Qureshi 6429 09/01/2009 From Date property
        //private DateTime FromDate
        //{
        //    get
        //    {
        //        if (ViewState["FromDate"] == null)
        //            ViewState["FromDate"] = new DateTime(1900, 1, 1);

        //        return Convert.ToDateTime(ViewState["FromDate"]);
        //    }
        //    set
        //    {
        //        ViewState["FromDate"] = value;
        //    }
        //}
        // Fahad Muhammad Qureshi 6429 09/01/2009 To Date property
        //private DateTime ToDate
        //{
        //    get
        //    {
        //        if (ViewState["ToDate"] == null)
        //            ViewState["ToDate"] = DateTime.Now;

        //        return Convert.ToDateTime(ViewState["ToDate"]);
        //    }
        //    set
        //    {
        //        ViewState["ToDate"] = value;
        //    }
        //}
        //7844 END

        #endregion

        // Fahad Muhammad Qureshi 6429 09/01/2009 grouping Event Handlers
        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        chkFamilyShowAll.Checked = false;
                        ShowAllRecords = false;
                        //SAEED 7844 06/03/2010 not using from/to date.
                        //cal_FromDate.SelectedDate = DateTime.Now;
                        //FromDate = DateTime.Now;
                        //cal_ToDate.SelectedDate = DateTime.Now;
                        //ToDate = DateTime.Now;                        
                        //gv_Records.Visible = false;
                        //Muhammad Muneer 8247 10/20/2010 comments session code b/c now handling from viewstate
                        //  Session["BadEmailData"]= null;
                        FillGrid();

                        ViewState["sortOrder"] = ""; bindGridView("", "");
                        //7844 END
                    }
                    //UpdateEmailAddress1.PageMethod += delegate()
                    //{
                    //     FillGrid();
                    //};




                    //Abbas 9676 01/12/2011  attaching update email address control method with 'PageMethod' event.
                    UpdateFollowUpInfo2.PageMethod += new HTP.WebControls.PageMethodHandler(UpdateFollowUpInfo2_PageMethod); //SAEED 7844 06/03/2010  attaching update control followup method with 'PageMethod' event.
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Records;
                    ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                    Pagingctrl.Visible = true;


                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        //Muhammad Muneer 8247 9/16/2010 added the functionality for the sorting of the grid
        /// <summary>
        /// Gridview Sorting event.
        /// </summary>
        /// <param name="sortExp"></param>
        /// <param name="sortDir"></param>

        public void bindGridView(string sortExp, string sortDir)
        {
            // string variable to store the connection string       
            // defined in ConnectionStrings section of web.config file.        


            DataView myDataView = Session["BadEmailData"] as DataView;

            if (sortExp != string.Empty)
            {
                myDataView.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gv_Records.DataSource = myDataView;
            gv_Records.DataBind();
            Session["BadEmailData"] = myDataView;

            // if condition that can be used to check the sql connection        
            // if it is open then close it.        
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Muhammad Muneer 8247 9/16/2010 added the functionality for the sorting of the grid
            bindGridView(e.SortExpression, sortOrder);
        }


        protected void gv_Records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label desc = (Label)e.Row.FindControl("lbl_LastName");

                    if (desc.Text.Length > 10)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    Label desce = (Label)e.Row.FindControl("lbl_FirstName");

                    if (desce.Text.Length > 10)
                    {
                        desce.ToolTip = desce.Text;
                        desce.Text = desce.Text.Substring(0, 10).ToUpperInvariant() + "...";
                    }
                    ((ImageButton)e.Row.FindControl("img_Add")).CommandArgument = e.Row.RowIndex.ToString();    //SAEED 7844 06/03/2010 attaching command argument with the followup date image. 

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                //Muhammad Muneer 8247 9/16/2010 added the functionality for the sorting of the grid
                if (Session["BadEmailData"] != null)
                {
                    DataView myDataView = Session["BadEmailData"] as DataView;

                    gv_Records.DataSource = myDataView;
                    gv_Records.DataBind();
                    //   Session["BadEmailData"] = myDataView;

                }
                else
                {
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "btnRemove")
                {
                    ViewState["Ticketid"] = Convert.ToInt32(e.CommandArgument);
                    Pagingctrl.Visible = true;
                }
                //SAEED 7844 06/03/2010 handling followup date functionlity here. 
                else if (e.CommandName == "AddFollowupDate")
                {
                    int RowID = Convert.ToInt32(e.CommandArgument);
                    string firstName = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_fname")).Value);
                    string lastName = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Lname")).Value);
                    string email = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Email")).Value);
                    string courtDate = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Courtdate")).Value);
                    string hireDate = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_HireDate")).Value);
                    string followupDate = (((Label)gv_Records.Rows[RowID].FindControl("lbl_FollowUpDate")).Text);
                    string causeno = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_causeno")).Value);
                    string refCaseNumber = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_refCaseNumber")).Value);
                    string ContactNumber1 = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Contact1"))).Value;
                    string ContactNumber2 = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Contact2"))).Value;
                    string ContactNumber3 = (((HiddenField)gv_Records.Rows[RowID].FindControl("hf_Contact3"))).Value;

                    cCase.TicketID = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_ticketid")).Value));
                    string comm = cCase.GetGeneralCommentsByTicketId();

                    UpdateFollowUpInfo2.Freezecalender = true;
                    UpdateFollowUpInfo2.Title = "Follow up & Update Email Address";
                    UpdateFollowUpInfo2.Freezecalender = true;
                    string url = Request.Url.AbsolutePath.ToString();

                    DataTable dtDays = Blogic.GetBusinessLogicInformationByURL(url, "Days");
                    if (dtDays.Rows.Count > 0)
                        Report_ID = Convert.ToInt32(dtDays.Rows[0]["Report_ID"]);

                    dtDays = Blogic.GetBusinessLogicSetDaysInformation("Days", Report_ID.ToString());
                    if (dtDays.Rows.Count > 0)
                        day = Convert.ToInt32(dtDays.Rows[0]["Attribute_Value"].ToString());

                    int reviewedEmailStatus = Convert.ToInt32((((HiddenField)gv_Records.Rows[RowID].FindControl("hf_reviewedEmailStatus"))).Value);

                    UpdateFollowUpInfo2.binddate(day.ToString(), clsGeneralMethods.GetBusinessDayDate(DateTime.Now, day), DateTime.Today, firstName, lastName, refCaseNumber, cCase.TicketID.ToString(), causeno, comm, mpeTrafficwaiting.ClientID, "BadEmailAddress", FollowUpType.BadEmailAddress.ToString(), followupDate, email, ContactNumber1, ContactNumber2, ContactNumber3, "1", reviewedEmailStatus);
                    UpdateFollowUpInfo2.followUpType = FollowUpType.BadEmailAddress;
                    mpeTrafficwaiting.Show();
                    Pagingctrl.Visible = true;
                }
                //SAEED 7844 END
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }


        /*//Muhammad Muneer 8247 10/20/2010 comment code for sorting 
        /// <summary>
        /// Gridview Sorting event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }

                GridViewSortExpression = sortExpression;
                FillGrid();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
            }
            catch (Exception ex)
            {
              //  lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }
        */
        void Pagingctrl_PageIndexChanged()
        {

            gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
            FillGrid();

        }

        // Fahad 6429 09/10/2009 page size change event handler
        /// <summary>
        /// Page Size change event handler
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Records.PageIndex = 0;
                gv_Records.PageSize = pageSize;
                gv_Records.AllowPaging = true;
            }
            else
            {
                gv_Records.AllowPaging = false;
            }
            FillGrid();

        }

        // Fahad 6429 09/10/2009 Filter family report
        /// <summary>
        /// Event handler to filter report according to criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFamilySubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ShowAllRecords = chkFamilyShowAll.Checked;
                //SAEED 7844 06/03/2010 not using from/to date.
                //FromDate = cal_FromDate.SelectedDate;
                //ToDate = cal_ToDate.SelectedDate;
                gv_Records.PageIndex = 0;
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        // Fahad Muhammad Qureshi 6429 09/01/2009 grouping Methods
        #region Methods
        private void FillGrid()
        {
            // Fahad Muhammad Qureshi 6429 09/01/2009 Show Record according to report selection
            try
            {
                lblMessage.Text = string.Empty;
                gv_Records.Visible = true;
                string[] keys = { "@ShowAll" };  //SAEED 7844 06/03/2010 remove from/to date paramter.
                object[] values = { ShowAllRecords };
                reports.getRecords("USP_HTP_Get_BadEmailAdressReport", keys, values);
                DS = reports.records;

                if (DS.Tables[0].Rows.Count > 0)
                {
                    // Fahad Muhammad Qureshi 6429 09/01/2009 Set grivview page size 
                    DS.Tables[0].Columns.Add("sno");
                    DataView dv = DS.Tables[0].DefaultView;
                    DataTable dt = dv.ToTable();
                    GenerateSerialNo(dt); ;

                    // Fahad Muhammad Qureshi 6429 09/01/2009 GridView Name Changed in PaggingControl
                    Pagingctrl.GridView = gv_Records;
                    gv_Records.DataSource = dt;
                    gv_Records.DataBind();
                    Pagingctrl.PageCount = gv_Records.PageCount;
                    Pagingctrl.PageIndex = gv_Records.PageIndex;
                    Pagingctrl.SetPageIndex();
                    Session["BadEmailData"] = dt.DefaultView;

                }
                else
                {
                    // Fahad Muhammad Qureshi 6429 09/01/2009 Set grivview page size
                    Pagingctrl.PageCount = 0;
                    Pagingctrl.PageIndex = 0;
                    Pagingctrl.SetPageIndex();
                    lblMessage.Text = "No Records Found";
                    gv_Records.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        // Fahad Muhammad Qureshi 6429 09/01/2009 Generate Serial Number
        /// <summary>
        /// Generate Serial Number
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            int sno = 1;
            if (!dtRecords.Columns.Contains("sno"))
                dtRecords.Columns.Add("sno");


            if (dtRecords.Rows.Count >= 1)
                dtRecords.Rows[0]["sno"] = 1;

            if (dtRecords.Rows.Count >= 2)
            {
                for (int i = 1; i < dtRecords.Rows.Count; i++)
                {
                    if (dtRecords.Rows[i - 1]["ticketid_pk"].ToString() != dtRecords.Rows[i]["ticketid_pk"].ToString())
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }

        }

        //SAEED 7844 06/03/2010 Show record according to report selection
        void UpdateFollowUpInfo2_PageMethod()
        {
            FillGrid();
        }

        //Abbas Qamar 9676 12/01/2011 Show record according to report selection

        void UpdateEmailAddress1_PageMethod()
        {
            FillGrid();
        }
        
        //Abbas Qamar 9676 12/12/2011 encode the string for java script to remove the "unterminated constant string".
        /// <summary>     
        /// Encodes a string to be represented as a string literal. The format  
        /// is essentially a JSON string.  
        /// The string returned includes outer quotes    
        /// Example Output: "Hello \"Rick\"!\r\nRock on"    
        /// </summary>     /// <param name="s"></param>   
        /// <returns></returns> 
        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\'':
                        sb.Append("\\\'");
                        break;
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");

            return sb.ToString();
        }
        #endregion

    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Reports
{
    public partial class PostTrialSatisfactionReport : System.Web.UI.Page
    {
        private DataSet dsRecords;
        //Database object for TrafficTickets Database
        private clsENationWebComponents clsDb = new clsENationWebComponents("Connection String");
        clsLogger bugTracker = new clsLogger();
        clsSession uuSession = new clsSession();	

        protected void Page_Load(object sender, EventArgs e)
        {
            if (uuSession.GetCookie("sEmpID", this.Request) == null)
            {
                Response.Redirect("../frmLogin.aspx");
            }
            lbl_Message.Text = "";
        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetRecords("", "", 0);
                //if (dsRecords.Tables[0].Rows.Count > 0)
                {
                    if (dsRecords.Tables[0].Rows.Count > 0)
                        formatDataSet(dsRecords, 0);
                    gv_records.DataSource = dsRecords;
                    gv_records.DataBind();
                }
                //if (dsRecords.Tables[1].Rows.Count > 0)
                {
                    if (dsRecords.Tables[1].Rows.Count > 0)
                        formatDataSet(dsRecords, 1);
                    gv_records_1.DataSource = dsRecords.Tables[1];
                    gv_records_1.DataBind();
                }

                if (dsRecords.Tables[0].Rows.Count + dsRecords.Tables[1].Rows.Count == 0)
                {
                    lbl_Message.Text = "No records!";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetRecords(string courtname, string status, int showclosedDocket)
        {
            try
            {
                string[] keys = { "@CourtDate", "@CourtName", "@Status", "@ShowClosedDocket" };
                object[] values = { cal_Date.SelectedDate, courtname, status, showclosedDocket };
                dsRecords = clsDb.Get_DS_BySPArr("USP_HTS_PostTrialReport", keys, values);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        private void formatDataSet(DataSet ds_records, int tableno)
        {
            try
            {
                int sno = 1;

                DataColumn dc = new DataColumn("RowType");
                dc.DefaultValue = 1;

                ds_records.Tables[tableno].Columns.Add("SNO");
                ds_records.Tables[tableno].Columns.Add(dc);
                ds_records.Tables[tableno].Rows[0]["SNO"] = sno;
                DataRow dr1 = ds_records.Tables[tableno].NewRow();
                dr1["SNO"] = ds_records.Tables[tableno].Rows[0]["courtnamecomplete"].ToString();
                dr1["RowType"] = 3;
                ds_records.Tables[tableno].Rows.InsertAt(dr1, 0);
                sno++;

                for (int i = 1; i < ds_records.Tables[tableno].Rows.Count; i++)
                {

                    if (ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"] != null)
                    {
                        try
                        {

                            if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["ticketid_pk"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["ticketid_pk"]))
                            {
                                ds_records.Tables[tableno].Rows[i]["SNO"] = sno;
                                sno++;
                            }

                            if (ds_records.Tables[tableno].Rows[i]["courtnamecomplete"].ToString() != ds_records.Tables[tableno].Rows[i - 1]["courtnamecomplete"].ToString())
                            {
                                DataRow dr = ds_records.Tables[tableno].NewRow();
                                dr["RowType"] = 3;
                                dr["SNO"] = ds_records.Tables[tableno].Rows[i]["courtnamecomplete"].ToString();
                                ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                            }
                            else if (Convert.ToInt32(ds_records.Tables[tableno].Rows[i]["courtnumber"]) != Convert.ToInt32(ds_records.Tables[tableno].Rows[i - 1]["courtnumber"]))
                            {
                                DataRow dr = ds_records.Tables[tableno].NewRow();
                                dr["RowType"] = 2;
                                dr["SNO"] = "\n";
                                ds_records.Tables[tableno].Rows.InsertAt(dr, i);

                            }
                        }
                        catch { }
                    }

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField rowtype = (HiddenField)e.Row.FindControl("hf_rowtype");

                    if (rowtype != null)
                    {
                        if (rowtype.Value == "1")
                        {
                            try
                            {
                                Label lbl1 = (Label)e.Row.FindControl("lbl_sno");
                                Label astatus = (Label)e.Row.FindControl("lbl_astatus");
                                Label vstatus = (Label)e.Row.FindControl("lbl_vstatus");
                                Image img = (Image)e.Row.FindControl("img_dis");

                               

                                string status = astatus.Text.Substring(0, 4);
                                string courtdate = astatus.Text.Substring(4, 10);

                                if (status[3] != ' ')
                                    courtdate = astatus.Text.Substring(5, 10);

                                DateTime cdate = DateTime.Parse(courtdate);

                            }
                            catch { }

                            ImageButton ibtn = (ImageButton)e.Row.FindControl("ibtn_Comments");
                            if (ibtn != null)
                            {
                                if (ibtn.Visible == true)
                                {
                                    HiddenField hf_date = (HiddenField)e.Row.FindControl("hf_CourtDate");

                                    if (hf_date.Value != "")
                                    {
                                        DateTime dt = Convert.ToDateTime(hf_date.Value);

                                        Label lbl_CourtNumber = (Label)e.Row.FindControl("lbl_CourtNumber");
                                        Label lbl_CourtTime = (Label)e.Row.FindControl("lbl_CourtTime");
                                        ImageButton ibtn_Comments = (ImageButton)e.Row.FindControl("ibtn_Comments");
                                        HyperLink hl = (HyperLink)e.Row.FindControl("hl_Sno");
                                        if (hl.Text == "" || hl.Text == String.Empty)
                                        {
                                            ibtn_Comments.Visible = false;
                                        }
                                        HiddenField hf_recID = (HiddenField)e.Row.FindControl("hf_RecordID");
                                        ibtn_Comments.Attributes.Add("onClick", "return ShowPopup('" + hf_recID.Value + "','" + cal_Date.SelectedDate.ToShortDateString() + "');");

                                        HyperLink hl_name = (HyperLink)e.Row.FindControl("lbl_name");

                                        if (hl_name.Text.Length > 17)
                                        {
                                            string name = hl_name.Text;
                                            hl_name.Text = hl_name.Text.Substring(0, 15) + "...";
                                            hl_name.ToolTip = name;
                                        }
                                        Label comm = (Label)e.Row.FindControl("lbl_comments");
                                        if (comm.Text.Length > 30)
                                        {
                                            string comments = comm.Text;
                                            comm.Text = comm.Text.Substring(0, 27) + "...";
                                            comm.ToolTip = comments;
                                        }
                                    }
                                }

                            }
                            Image ibtn2 = (ImageButton)e.Row.FindControl("ImageButton2");
                            Label lbl = (Label)e.Row.FindControl("lbl_LastUpdate");
                            if (ibtn2 != null)
                            {
                                if (lbl != null)
                                {
                                    if (lbl.Text.Contains("/"))
                                    {
                                        ibtn2.Visible = true;
                                        ibtn.Visible = false;
                                    }
                                    else
                                    {
                                        ibtn2.Visible = false;
                                        ibtn.Visible = true;
                                    }
                                }

                            }
                        }

                        else if (rowtype.Value == "2")
                        {

                            for (int i = 1; i < e.Row.Cells.Count; i++)
                                e.Row.Cells[i].Visible = false;

                            e.Row.Cells[0].ColumnSpan = 10;
                            e.Row.Cells[0].Style.Add("height", "20px");
                            e.Row.Cells[0].BackColor = System.Drawing.Color.Gray;

                        }

                        else if (rowtype.Value == "3")
                        {
                            for (int i = 1; i < e.Row.Cells.Count; i++)
                                e.Row.Cells[i].Visible = false;


                            e.Row.Cells[0].Style.Add("height", "20px");
                            e.Row.Cells[0].ColumnSpan = 10;

                            Label lbl = (Label)e.Row.FindControl("lbl_sno");

                            if (lbl.Text != "")
                            {
                                lbl.ForeColor = System.Drawing.Color.White;
                                e.Row.Cells[0].BackColor = System.Drawing.Color.Black;
                                e.Row.Cells[0].Font.Bold = true;
                                lbl.Visible = true;
                                ((HyperLink)e.Row.FindControl("hl_sno")).Visible = false; ;

                            }
                            if (lbl.Text == " , ")
                                e.Row.Visible = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] keys = { "@TicketID", "@Date", "@EmpID", "@Comments" };
                object[] values = { e.CommandArgument.ToString(), cal_Date.SelectedDate, uuSession.GetCookie("sEmpID", this.Request), hf_comments.Value };
                clsDb.ExecuteSP("USP_HTS_POST_TRIAL_COMMENTS", keys, values);
                GetRecords("", "", 0);
                if (dsRecords.Tables[0].Rows.Count > 0)
                {
                    formatDataSet(dsRecords, 0);
                    gv_records.DataSource = dsRecords.Tables[0];
                    gv_records.DataBind();
                }
                if (dsRecords.Tables[1].Rows.Count > 0)
                {
                    formatDataSet(dsRecords, 1);
                    gv_records_1.DataSource = dsRecords.Tables[1];
                    gv_records_1.DataBind();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

    }
}

﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using System.IO;
using System.Configuration;
using System.Web;
using Microsoft.Office.Interop.Excel;
using System.Linq;

namespace HTP.Reports
{
    ///<summary>
    ///</summary>
    public partial class CrashAlertsReport : System.Web.UI.Page
    {
        #region Variables
        faxclass _fclass = new faxclass();
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Bindgrid();
                Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
                Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
                Pagingctrl.GridView = gvDocs;
                if (!Page.IsPostBack)
                {
                    calFrom1.SelectedDate = DateTime.Now.Date.AddDays(-15);
                    calTo1.SelectedDate = DateTime.Now.Date;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        #region Commented Code

        ////Sabir Khan 11110 06/26/2013 download excel file
        private void ExporttoExcel(string filepath, string filename)
        {
            FileInfo f = new FileInfo(filepath + filename);
            Response.Clear();
            Response.AddHeader("Content-Disposition", ("attachment; filename=" + filename));
            Response.AddHeader("Content-Length", f.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(filepath + filename);
            Response.End();
        }

        //Sabir Khan 11110 06/26/2013 Download event
        #endregion

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            string fileName = ((LinkButton)sender).CommandArgument.ToString();
            string filePath = ConfigurationSettings.AppSettings["CrashFilePath"].ToString();
            ExporttoExcel(filePath, fileName);
        }
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gvDocs.PageIndex = e.NewPageIndex;
                    Bindgrid();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gvDocs.PageIndex = 0;
                gvDocs.PageSize = pageSize;
                gvDocs.AllowPaging = true;
            }
            else
            {
                gvDocs.AllowPaging = false;
            }
            Bindgrid();
        }

        #endregion

        #region Methods

        void Pagingctrl_PageIndexChanged()
        {
            gvDocs.PageIndex = Pagingctrl.PageIndex - 1;
            Bindgrid();
        }

        protected void Bindgrid()
        {
            string dirPath = ConfigurationManager.AppSettings["CrashFilePath"].ToString();
            DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
            if (dd_StatusType.SelectedItem.Text.Trim() == "--- All ---")
            {
                if (dirInfo.GetFiles("*_All.xlsx").Length > 0)
                {
                    gvDocs.DataSource = dirInfo.GetFiles("*All.xlsx");
                    gvDocs.DataBind();
                    lbl_Message.Text = "";
                }
                else
                {
                    gvDocs.DataSource = null;
                    gvDocs.DataBind();
                    lbl_Message.Text = "File(s) not found for the selected filter";
                }
            }
            else
            {
                if (rdoStatusType.Checked)
                {
                    if (dirInfo.GetFiles("*_" + dd_StatusType.SelectedItem.Text.Trim().Replace(",", "-") + ".xlsx").Length > 0)
                    {
                        gvDocs.DataSource = dirInfo.GetFiles("*_" + dd_StatusType.SelectedItem.Text.Trim().Replace(",", "-") + ".xlsx");
                        gvDocs.DataBind();
                        lbl_Message.Text = "";
                    }
                    else
                    {
                        gvDocs.DataSource = null;
                        gvDocs.DataBind();
                        lbl_Message.Text = "File(s) not found for the selected filter";
                    }
                }
                else if (rdoCreationDate.Checked)
                {
                    gvDocs.DataSource = dirInfo.GetFiles().Where(ss => ss.FullName.Contains("Crash Alerts Creation date") && Convert.ToDateTime(ss.Name.Substring(ss.Name.IndexOf("date") + 5, 10).Replace("-", "/")) >= calFrom1.SelectedDate.Date && Convert.ToDateTime(ss.Name.Substring(ss.Name.IndexOf("date") + 5, 10).Replace("-", "/")) <= calTo1.SelectedDate.Date).ToList();
                    gvDocs.DataBind();
                    lbl_Message.Text = "";
                }
                else
                {
                    gvDocs.DataSource = null;
                    gvDocs.DataBind();
                    lbl_Message.Text = "File(s) not found for the selected filter";
                }
            }

        }

        #endregion

        protected void dd_StatusType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dd_StatusType.SelectedValue == "-1")
            {
                DropDownList ddl = (DropDownList)Pagingctrl.Controls[1];
                ddl.Enabled = false;
            }
            else
            {
                DropDownList ddl = (DropDownList)Pagingctrl.Controls[1];
                ddl.Enabled = true;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Bindgrid();
        }
    }
}